﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Tesla;
using Tesla.Graphics;
using Tesla.Direct3D11.Graphics;

namespace TEFXBatchCompiler
{
  class Program
  {
    static void Main(String[] args)
    {
      //IN: Folder path
      //OUT: Output path

      if (args.Length != 2)
        return;

      String inputPath = args[0];
      String outputPath = args[1];

      if (!Directory.Exists(inputPath))
        return;

      if (!Directory.Exists(outputPath))
        Directory.CreateDirectory(outputPath);

      IEnumerable<String> files = Directory.EnumerateFiles(inputPath, "*.fx", SearchOption.TopDirectoryOnly);
      DefaultIncludeHandler includeHandler = new DefaultIncludeHandler();
      includeHandler.AddIncludeDirectory(inputPath);

      EffectCompiler compiler = new EffectCompiler();
      compiler.SetIncludeHandler(includeHandler);

      foreach (String file in files)
      {

        EffectCompilerResult result = compiler.CompileFromFile(file, CompileFlags.None);

        if (result.HasCompileErrors)
        {
          StringBuilder errorString = new StringBuilder();
          foreach (String text in result.CompileErrors)
            errorString.AppendLine(text);

          Console.WriteLine("ERROR COMPILING FILE!!!\n");
          Console.WriteLine(file);
          Console.WriteLine("\n");
          Console.WriteLine(errorString);
          Console.WriteLine("\n================================================================================================\n\n");
        }
        else
        {
          String newFileName = Path.GetFileNameWithoutExtension(file) + ".tefx";
          using (FileStream fs = File.Create(Path.Combine(outputPath, newFileName)))
          {
            EffectData.Write(result.EffectData, fs, DataCompressionMode.Gzip);
          }
        }
      }
    }
  }
}
