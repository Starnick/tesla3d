﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.IO;
using Tesla;
using Tesla.Content;
using System.Threading;
using System.Threading.Tasks;
using Tesla.Direct3D11.Graphics;
using Tesla.Graphics;

using System.Reflection;
using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace DefaultContentCompiler
{
  class Program
  {
    static void Main(string[] args)
    {
      String contentRootFolder = Path.GetFullPath(Path.Combine(ContentHelper.AppLocation, "../../../../../../Content/"));
      if (!Directory.Exists(contentRootFolder))
        return;

      Engine.Initialize(Platforms.CreateOnlyGraphics(SubSystems.Graphics.Direct3D11));

      List<Task> tasks = new List<Task>();

      //Shaders
      tasks.Add(Task.Run(() =>
      {
        new D3D11ShaderCompilerTaskGroup().Run(new String[] { Path.Combine(contentRootFolder, "Shaders/D3D11/"), Path.Combine(contentRootFolder, "Shaders/CompiledD3D11/") });
      }));

      //SpriteFonts
      tasks.Add(Task.Run(() =>
      {
        new SpriteFontCompilerTaskGroup().Run(new String[] { Path.Combine(contentRootFolder, "SpriteFonts"), Path.Combine(contentRootFolder, "SpriteFonts/Compiled/") });
      }));

      //UI
      tasks.Add(Task.Run(() =>
      {
        new UISkinCompilerTaskGroup().Run(new String[] { Path.Combine(contentRootFolder, "UI"), Path.Combine(contentRootFolder, "UI/Compiled/") });
      }));

      foreach (Task t in tasks)
        t.Wait();

      Engine.Destroy();
    }
  }
}
