﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Tesla.Graphics;
using Tesla.Direct3D11.Graphics;
using Tesla.Content;
using System.Threading.Tasks;
using Tesla.SquidUI;

namespace DefaultContentCompiler
{
  public class UISkinCompilerTaskGroup : ITaskGroup
  {
    public void Run(String[] args)
    {
      //IN: Folder path
      //OUT: Output path

      if (args.Length != 2)
        return;

      String inputPath = args[0];
      String outputPath = args[1];

      if (!Directory.Exists(inputPath))
        return;

      if (!Directory.Exists(outputPath))
        Directory.CreateDirectory(outputPath);

      IEnumerable<String> texAtlasFiles = Directory.EnumerateFiles(inputPath, "*_TextureAtlas.json", SearchOption.TopDirectoryOnly);
      IEnumerable<String> skinFiles = Directory.EnumerateFiles(inputPath, "*Skin.json", SearchOption.TopDirectoryOnly);

      ContentManager cm = new ContentManager(new FileResourceRepository(inputPath));
      cm.ResourceImporters.Add(new TeximpTextureImporter());

      IResourceRepository outputRepo = new FileResourceRepository(outputPath);
      outputRepo.OpenConnection(ResourceFileMode.Create, ResourceFileShare.ReadWrite);

      try
      {
        foreach (String texAtlasPath in texAtlasFiles)
        {
          TextureAtlas atlas = cm.Load<TextureAtlas>(texAtlasPath);
          if (atlas == null)
            continue;

          IExternalReferenceHandler handler = ExternalReferenceHandler.CreateBinaryDefaultHandler(outputRepo.GetResourceFile(Path.GetFileNameWithoutExtension(texAtlasPath) + ".tebo"), SavableWriteFlags.OverwriteExistingResource | SavableWriteFlags.UseCompression);

          BinaryResourceExporter exporter = new BinaryResourceExporter();
          exporter.Save(handler, atlas, SavableWriteFlags.UseCompression);
        }

        foreach (String skinFilePath in skinFiles)
        {
          UISkin skin = cm.Load<UISkin>(skinFilePath);
          if (skin == null)
            continue;

          IExternalReferenceHandler handler = ExternalReferenceHandler.CreateBinaryDefaultHandler(outputRepo.GetResourceFile(Path.GetFileNameWithoutExtension(skinFilePath) + ".tebo"), SavableWriteFlags.OverwriteExistingResource | SavableWriteFlags.UseCompression);

          BinaryResourceExporter exporter = new BinaryResourceExporter();
          exporter.Save(handler, skin, SavableWriteFlags.UseCompression);
        }
      }
      finally
      {
        outputRepo.CloseConnection();
      }
    }
  }
}
