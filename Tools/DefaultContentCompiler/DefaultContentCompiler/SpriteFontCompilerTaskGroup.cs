﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Tesla.Graphics;
using Tesla.Direct3D11.Graphics;
using Tesla.Content;
using System.Threading;
using System.Threading.Tasks;

namespace DefaultContentCompiler
{
  public class SpriteFontCompilerTaskGroup : ITaskGroup
  {
    public void Run(String[] args)
    {
      //IN: Folder path
      //OUT: Output path

      if (args.Length != 2)
        return;

      String inputPath = args[0];
      String outputPath = args[1];

      if (!Directory.Exists(inputPath))
        return;

      if (!Directory.Exists(outputPath))
        Directory.CreateDirectory(outputPath);

      IEnumerable<String> subFolders = Directory.EnumerateDirectories(inputPath);
      List<Task> tasks = new List<Task>();

      foreach (String subFolder in subFolders)
      {
        tasks.Add(Task.Run(() =>
        {
          IEnumerable<String> fontFiles = Directory.EnumerateFiles(subFolder, "*.fnt", SearchOption.TopDirectoryOnly);

          ContentManager cm = new ContentManager(new FileResourceRepository(subFolder));
          cm.ResourceImporters.Add(new TeximpTextureImporter());
          cm.ResourceImporters.Add(new BMFontImporter());

          String subFolderOutput = Path.Combine(outputPath, new DirectoryInfo(subFolder).Name);

          IResourceRepository outputRepo = new FileResourceRepository(subFolderOutput);
          outputRepo.OpenConnection(ResourceFileMode.Create, ResourceFileShare.ReadWrite);

          try
          {
            foreach (String fontFilePath in fontFiles)
            {
              SpriteFont font = cm.Load<SpriteFont>(fontFilePath);
              if (font == null)
                continue;

              IExternalReferenceHandler handler = ExternalReferenceHandler.CreateBinaryDefaultHandler(outputRepo.GetResourceFile(Path.GetFileNameWithoutExtension(fontFilePath) + ".tebo"), SavableWriteFlags.OverwriteExistingResource | SavableWriteFlags.UseCompression);

              BinaryResourceExporter exporter = new BinaryResourceExporter();
              exporter.Save(handler, font);
            }
          }
          finally
          {
            outputRepo.CloseConnection();
          }
        }));
      }

      foreach (Task t in tasks)
        t.Wait();
    }
  }
}
