/*
* The shader code below is licensed under the Creative Commons Attribution-Share Alike 3.0 
* license. Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
*
* You are free:
*	To Share: to copy, distribute, display, and perform the work
*	To Remix: to make derivative works 
*
*	Under the following conditions:
*		* Attribution - You must attribute the work in the manner specified by the author or
*			licensor (but not in any way that suggests that they endorse you or your use of the work).
*		* Share Alike - If you alter, transform, or build upon this work, you may distribute
*			the resulting work only under the same, similar or a compatible license.
*
*		For any reuse or distribution, you must make clear to others the license terms of this work.
*		The best way to do this is with a link to the above web page (http;//www.tesla-engine.net)
*
*		Any of the above conditions can be waived if you get permission from the copyright holder.
*
*		Apart from the remix rights granted under this license, nothing in this license impairs or
*		restricts the author's moral rights.
*/

#include "Structures.fxh"

//
// Variables
//

cbuffer PerFrame 
{
  float4x4 SpriteTransform;
};

cbuffer BlurOptions
{
  float2 CenterUV = { 0.5, 0.5 };
  float BlurStart = 1.0f;
  float BlurWidth = -0.1;
  int SampleCount = 10;
};

//Vertex shader and parameter compatible with SpriteBatch
Texture2D SpriteMap;
SamplerState SpriteMapSampler 
{
  Filter = MIN_MAG_MIP_LINEAR;
  AddressU = Clamp;
  AddressV = Clamp;
  AddressW = Clamp;
};

//
// **** RadialBlur ****
//

VSOutputVcTx VS_RadialBlur(VSInputVcTx input) 
{
	VSOutputVcTx output;
	output.PositionPS = mul(float4(input.Position, 1.0), SpriteTransform);
	output.VertColor = input.VertColor;
	output.TexCoord = input.TexCoord;
	return output;
}

float4 PS_RadialBlur(VSOutputVcTx input) : SV_TARGET
{
  float2 texCoords = input.TexCoord;
  texCoords -= CenterUV;
  
  float4 color = float4(0, 0, 0, 0);
  float2 dx = ddx(texCoords);
  float2 dy = ddy(texCoords);
  
  for(int i = 0; i < SampleCount; i++)
  {
    float scale = BlurStart + BlurWidth * (i / (float) (SampleCount - 1));
    color += SpriteMap.SampleGrad(SpriteMapSampler, texCoords * scale + CenterUV, dx, dy);
  }
  
  color /= (float) SampleCount;
  
  return color;
}

technique11 RadialBlur
{
  pass
  {
		SetVertexShader(CompileShader(vs_5_0, VS_RadialBlur()));
		SetPixelShader(CompileShader(ps_5_0, PS_RadialBlur()));
  }
}