/*
* The shader code below is licensed under the Creative Commons Attribution-Share Alike 3.0 
* license. Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
*
* You are free:
*	To Share: to copy, distribute, display, and perform the work
*	To Remix: to make derivative works 
*
*	Under the following conditions:
*		* Attribution - You must attribute the work in the manner specified by the author or
*			licensor (but not in any way that suggests that they endorse you or your use of the work).
*		* Share Alike - If you alter, transform, or build upon this work, you may distribute
*			the resulting work only under the same, similar or a compatible license.
*
*		For any reuse or distribution, you must make clear to others the license terms of this work.
*		The best way to do this is with a link to the above web page (http;//www.tesla-engine.net)
*
*		Any of the above conditions can be waived if you get permission from the copyright holder.
*
*		Apart from the remix rights granted under this license, nothing in this license impairs or
*		restricts the author's moral rights.
*/

#include "Structures.fxh"

//------------------------------------------------------------------------------------------------------------------------------//
//  Common file for ALL "Standard" shaders so we can avoid duplicate code and have separate effect files for each technique     //
//------------------------------------------------------------------------------------------------------------------------------//

//
// Variables
//

cbuffer PerFrame 
{
	float4x4 WVP; //WorldViewProjection
};

cbuffer PerFrame_Instanced
{
  float4x4 ViewProjection;
};

cbuffer PerMaterial 
{
  float3 MatDiffuse = { 1.0f, 1.0f, 1.0f };
  float Alpha = 1.0f;
};

Texture2D DiffuseMap;
SamplerState MapSampler 
{
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = Wrap;
	AddressV = Wrap;
	AddressW = Wrap;
};

//-----------------------------------------------------------------------------//
//                      **** Vertex Shaders ****                               //
//-----------------------------------------------------------------------------//

//
//  **** Uniform color ****
//
float4 VS_Standard_Color(float3 Position : POSITION) : SV_POSITION
{
  return mul(float4(Position, 1.0f), WVP);
}

//
//  **** Instanced Uniform color ****
//
float4 VS_Standard_Color_Instanced(float3 Position : POSITION, float4x4 World : TEXCOORD) : SV_POSITION 
{
  float4x4 wvp = mul(World, ViewProjection);

	return mul(float4(Position, 1.0), wvp);
}

//
//  **** Uniform color with vertex coloring ****
//
VSOutputVc VS_Standard_Color_Vc(VSInputVc input)
{
	VSOutputVc output;
	output.PositionPS = mul(float4(input.Position, 1.0), WVP);
	output.VertColor = input.VertColor;
	
	return output;
}

//
//  **** Instanced uniform color with vertex coloring ****
//
VSOutputVc VS_Standard_Color_Vc_Instanced(VSInputVc_Instanced input) 
{
  float4x4 wvp = mul(input.World, ViewProjection);

	VSOutputVc output;
	output.PositionPS = mul(float4(input.Position, 1.0), wvp);
	output.VertColor = input.VertColor;
	
	return output;
}

//
//  **** Texture ****
//
VSOutputTx VS_Standard_Texture(VSInputTx input)
{
	VSOutputTx output;
	output.PositionPS = mul(float4(input.Position, 1.0), WVP);
	output.TexCoord = input.TexCoord;
	
	return output;
}

//
//  **** Instanced Texture ****
//
VSOutputTx VS_Standard_Texture_Instanced(VSInputTx_Instanced input)
{
  float4x4 wvp = mul(input.World, ViewProjection);

	VSOutputTx output;
	output.PositionPS = mul(float4(input.Position, 1.0), wvp);
	output.TexCoord = input.TexCoord;
	
	return output;
}

//
//  **** Texture with vertex coloring ****
//
VSOutputVcTx VS_Standard_Texture_Vc(VSInputVcTx input)
{
	VSOutputVcTx output;
	output.PositionPS = mul(float4(input.Position, 1.0), WVP);
	output.VertColor = input.VertColor;
	output.TexCoord = input.TexCoord;
	
	return output;
}

//
//  **** Instanced texture with vertex coloring ****
//
VSOutputVcTx VS_Standard_Texture_Vc_Instanced(VSInputVcTx_Instanced input)
{
  float4x4 wvp = mul(input.World, ViewProjection);

	VSOutputVcTx output;
	output.PositionPS = mul(float4(input.Position, 1.0), wvp);
	output.VertColor = input.VertColor;
	output.TexCoord = input.TexCoord;
	
	return output;
}

//-----------------------------------------------------------------------------//
//                      **** Pixel Shaders ****                                //
//-----------------------------------------------------------------------------//

//
//  **** Uniform color ****
//
float4 PS_Standard_Color() : SV_TARGET
{
  return float4(MatDiffuse, Alpha);
}

//
//  **** Uniform color with vertex coloring ****
//
float4 PS_Standard_Color_Vc(VSOutputVc input) : SV_TARGET
{
  return float4(MatDiffuse * input.VertColor.rgb, Alpha * input.VertColor.a);
}

//
//  **** Texture ****
//
float4 PS_Standard_Texture(VSOutputTx input) : SV_TARGET
{
	float4 albedo = DiffuseMap.Sample(MapSampler, input.TexCoord);
	
	return float4(MatDiffuse * albedo.rgb, Alpha * albedo.a);
}

//
//  **** Texture with vertex coloring ****
//
float4 PS_Standard_Texture_Vc(VSOutputVcTx input) : SV_TARGET
{
	float4 albedo = DiffuseMap.Sample(MapSampler, input.TexCoord);
	
	return float4(MatDiffuse * albedo.rgb * input.VertColor.rgb, Alpha * albedo.a * input.VertColor.a);
}