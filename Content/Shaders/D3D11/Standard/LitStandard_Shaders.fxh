/*
* The shader code below is licensed under the Creative Commons Attribution-Share Alike 3.0 
* license. Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
*
* You are free:
*	To Share: to copy, distribute, display, and perform the work
*	To Remix: to make derivative works 
*
*	Under the following conditions:
*		* Attribution - You must attribute the work in the manner specified by the author or
*			licensor (but not in any way that suggests that they endorse you or your use of the work).
*		* Share Alike - If you alter, transform, or build upon this work, you may distribute
*			the resulting work only under the same, similar or a compatible license.
*
*		For any reuse or distribution, you must make clear to others the license terms of this work.
*		The best way to do this is with a link to the above web page (http;//www.tesla-engine.net)
*
*		Any of the above conditions can be waived if you get permission from the copyright holder.
*
*		Apart from the remix rights granted under this license, nothing in this license impairs or
*		restricts the author's moral rights.
*/

#include "Structures.fxh"
#include "Lighting.fxh"
#include "NormalUtils.fxh"

//------------------------------------------------------------------------------------------------------------------------------//
//  Common file for ALL "LitStandard" shaders so we can avoid duplicate code and have separate effect files for each technique  //
//------------------------------------------------------------------------------------------------------------------------------//

//
// Variables
//

cbuffer PerFrame
{
	float4x4 WVP; //WorldViewProjection
	float4x4 World;
	float4x4 WorldIT; //WorldInverseTranspose
};

cbuffer PerFrame_Instanced
{
  float4x4 ViewProjection;
};

cbuffer PerFrame_Camera
{
  float3 EyePos;
};

//CBuffer for lighting values
cbuffer Lighting 
{
	float3 GlobalAmbient = { 1.0f, 1.0f, 1.0f };
	int NumLights = 0;
	Light Lights[4];
};

//CBuffer for material properties
cbuffer PerMaterial 
{
	float3 MatDiffuse = { 1.0f, 1.0f, 1.0f};
	float3 MatAmbient = { 0.2f, 0.2f, 0.2f};
	float3 MatEmissive = { 0.0f, 0.0f, 0.0f};
	float3 MatSpecular = { 0.0f, 0.0f, 0.0f};
	float MatShininess = 16.0f;
	float Alpha = 1.0f;
};

//CBuffer for normal mapping options
cbuffer NormalMapOptions
{
	bool UseDiffuseMap = true;
	bool UseSpecularMap = false;
	
	#ifdef PARALLAX
    bool ClampParallaxTexture = false;
    float HeightScale = 0.1;
    int MaxParallaxSamples = 20;
    int MinParallaxSamples = 4;
  #endif
};

Texture2D DiffuseMap;
Texture2D NormalMap;
Texture2D SpecularMap;
Texture2D EmissiveMap;

SamplerState MapSampler 
{
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = Wrap;
	AddressV = Wrap;
	AddressW = Wrap;
};


//-----------------------------------------------------------------------------//
//                      **** Vertex Shaders ****                               //
//-----------------------------------------------------------------------------//


//
//  **** Uniform color****
//
VSOutputNm VS_LitStandard_Color(VSInputNm input)
{
	float4 pos = float4(input.Position, 1.0);

	VSOutputNm output;
	output.PositionPS = mul(pos, WVP);
	output.PositionWS = mul(pos, World).xyz;
	output.NormalWS = normalize(mul(float4(input.Normal, 0.0), WorldIT).xyz);
	
	return output;
}

//
//  **** Instanced uniform color****
//
VSOutputNm VS_LitStandard_Color_Instanced(VSInputNm_Instanced input)
{
	float4 pos = float4(input.Position, 1.0);
	float4x4 wvp = mul(input.World, ViewProjection);

	VSOutputNm output;
	output.PositionPS = mul(pos, wvp);
	output.PositionWS = mul(pos, input.World).xyz;
	output.NormalWS = normalize(mul(float4(input.Normal, 0.0), input.World).xyz); //Non uniform scaling not supported
	
	return output;
}

//
//  **** Uniform color with vertex coloring ****
//
VSOutputVcNm VS_LitStandard_Color_Vc(VSInputVcNm input)
{
	float4 pos = float4(input.Position, 1.0);

	VSOutputVcNm output;
	output.PositionPS = mul(pos, WVP);
	output.PositionWS = mul(pos, World).xyz;
	output.VertColor = input.VertColor;
	output.NormalWS = normalize(mul(float4(input.Normal, 0.0), WorldIT).xyz);
	
	return output;
}

//
//  **** Instanced uniform color with vertex coloring ****
//
VSOutputVcNm VS_LitStandard_Color_Vc_Instanced(VSInputVcNm_Instanced input)
{
	float4 pos = float4(input.Position, 1.0);
	float4x4 wvp = mul(input.World, ViewProjection);

	VSOutputVcNm output;
	output.PositionPS = mul(pos, wvp);
	output.PositionWS = mul(pos, input.World).xyz;
	output.VertColor = input.VertColor;
	output.NormalWS = normalize(mul(float4(input.Normal, 0.0), input.World).xyz); //Non uniform scaling not supported
	
	return output;
}

//
//  **** Texture ****
//
VSOutputTxNm VS_LitStandard_Texture(VSInputTxNm input)
{
	float4 pos = float4(input.Position, 1.0);

	VSOutputTxNm output;
	output.PositionPS = mul(pos, WVP);
	output.PositionWS = mul(pos, World).xyz;
	output.TexCoord = input.TexCoord;
	output.NormalWS = normalize(mul(float4(input.Normal, 0.0), WorldIT).xyz);
	
	return output;
}

//
//  **** Instanced Texture ****
//
VSOutputTxNm VS_LitStandard_Texture_Instanced(VSInputTxNm_Instanced input)
{
	float4 pos = float4(input.Position, 1.0);
	float4x4 wvp = mul(input.World, ViewProjection);

	VSOutputTxNm output;
	output.PositionPS = mul(pos, wvp);
	output.PositionWS = mul(pos, input.World).xyz;
	output.TexCoord = input.TexCoord;
	output.NormalWS = normalize(mul(float4(input.Normal, 0.0), input.World).xyz); //Non uniform scaling not supported
	
	return output;
}

//
//  **** Texture with normal mapping ****
//

#ifdef PARALLAX
  VSOutputTxNmTnBt_Parallax
#else
  VSOutputTxNmTnBt
#endif

VS_LitStandard_Texture_Nmm(VSInputTxNmTnBt input)
{
	float4 pos = float4(input.Position, 1.0);

#ifdef PARALLAX
  VSOutputTxNmTnBt_Parallax output;
#else
  VSOutputTxNmTnBt output;
#endif

	output.PositionPS = mul(pos, WVP);
	output.PositionWS = mul(pos, World).xyz;
	output.TexCoord = input.TexCoord;
	
	//Transform tangent frame to world space
	output.NormalWS = normalize(mul(float4(input.Normal, 0.0f), WorldIT).xyz);
	output.TangentWS = normalize(mul(float4(input.Tangent, 0.0f), WorldIT).xyz);
	output.BitangentWS = normalize(mul(float4(input.Bitangent, 0.0f), WorldIT).xyz);
	
#ifdef PARALLAX
	//Transpose of rotation matrix is the inverse
	float3x3 worldToTangent = transpose(float3x3(output.TangentWS, output.BitangentWS, output.NormalWS));
	
  float3 viewDir = normalize(EyePos - output.PositionWS);
  output.ViewDirectionTS = normalize(mul(viewDir, worldToTangent));
#endif
	
	return output;
}

//
//  **** Instanced Texture with normal mapping ****
//

#ifdef PARALLAX
  VSOutputTxNmTnBt_Parallax
#else
  VSOutputTxNmTnBt
#endif

VS_LitStandard_Texture_Nmm_Instanced(VSInputTxNmTnBt_Instanced input)
{
	float4 pos = float4(input.Position, 1.0);
	float4x4 wvp = mul(input.World, ViewProjection);

#ifdef PARALLAX
  VSOutputTxNmTnBt_Parallax output;
#else
	VSOutputTxNmTnBt output;
#endif

	output.PositionPS = mul(pos, wvp);
	output.PositionWS = mul(pos, input.World).xyz;
	output.TexCoord = input.TexCoord;
	
	//Transform tangent frame to world space
	output.NormalWS = normalize(mul(float4(input.Normal, 0.0f), input.World).xyz);
	output.TangentWS = normalize(mul(float4(input.Tangent, 0.0f), input.World).xyz);
	output.BitangentWS = normalize(mul(float4(input.Bitangent, 0.0f), input.World).xyz);
	
#ifdef PARALLAX
	//Transpose of rotation matrix is the inverse
	float3x3 worldToTangent = transpose(float3x3(output.TangentWS, output.BitangentWS, output.NormalWS));
	
  float3 viewDir = normalize(EyePos - output.PositionWS);
  output.ViewDirectionTS = normalize(mul(viewDir, worldToTangent));
#endif

	return output;
}

//
//  **** Texture with Vertex Coloring ****
//
VSOutputVcTxNm VS_LitStandard_Texture_Vc(VSInputVcTxNm input)
{
	float4 pos = float4(input.Position, 1.0);

	VSOutputVcTxNm output;
	output.PositionPS = mul(pos, WVP);
	output.PositionWS = mul(pos, World).xyz;
	output.VertColor = input.VertColor;
	output.TexCoord = input.TexCoord;
	output.NormalWS = normalize(mul(float4(input.Normal, 0.0), WorldIT).xyz);
	
	return output;
}

//
//  **** Instanced Texture with Vertex Coloring ****
//
VSOutputVcTxNm VS_LitStandard_Texture_Vc_Instanced(VSInputVcTxNm_Instanced input)
{
	float4 pos = float4(input.Position, 1.0);
	float4x4 wvp = mul(input.World, ViewProjection);

	VSOutputVcTxNm output;
	output.PositionPS = mul(pos, wvp);
	output.PositionWS = mul(pos, input.World).xyz;
	output.VertColor = input.VertColor;
	output.TexCoord = input.TexCoord;
	output.NormalWS = normalize(mul(float4(input.Normal, 0.0), input.World).xyz); //Non uniform scaling not supported
	
	return output;
}

//
//  **** Texture with Vertex Coloring/Normal maps ****
//
#ifdef PARALLAX
  VSOutputVcTxNmTnBt_Parallax
#else
  VSOutputVcTxNmTnBt
#endif
VS_LitStandard_Texture_VcNmm(VSInputVcTxNmTnBt input)
{
	float4 pos = float4(input.Position, 1.0);

#ifdef PARALLAX
  VSOutputVcTxNmTnBt_Parallax output;
#else
	VSOutputVcTxNmTnBt output;
#endif

	output.PositionPS = mul(pos, WVP);
	output.PositionWS = mul(pos, World).xyz;
	output.VertColor = input.VertColor;
	output.TexCoord = input.TexCoord;
	
	//Transform tangent frame to world space
	output.NormalWS = normalize(mul(float4(input.Normal, 0.0f), WorldIT).xyz);
	output.TangentWS = normalize(mul(float4(input.Tangent, 0.0f), WorldIT).xyz);
	output.BitangentWS = normalize(mul(float4(input.Bitangent, 0.0f), WorldIT).xyz);
	
#ifdef PARALLAX
	//Transpose of rotation matrix is the inverse
	float3x3 worldToTangent = transpose(float3x3(output.TangentWS, output.BitangentWS, output.NormalWS));
	
  float3 viewDir = normalize(EyePos - output.PositionWS);
  output.ViewDirectionTS = normalize(mul(viewDir, worldToTangent));
#endif
	
	return output;
}

//
//  **** Instanced Texture with Vertex Coloring/Normal maps ****
//
#ifdef PARALLAX
  VSOutputVcTxNmTnBt_Parallax
#else
  VSOutputVcTxNmTnBt
#endif
VS_LitStandard_Texture_VcNmm_Instanced(VSInputVcTxNmTnBt_Instanced input)
{
	float4 pos = float4(input.Position, 1.0);
	float4x4 wvp = mul(input.World, ViewProjection);
	
#ifdef PARALLAX
  VSOutputVcTxNmTnBt_Parallax output;
#else
	VSOutputVcTxNmTnBt output;
#endif

	output.PositionPS = mul(pos, wvp);
	output.PositionWS = mul(pos, input.World).xyz;
	output.VertColor = input.VertColor;
	output.TexCoord = input.TexCoord;
	
	//Transform tangent frame to world space
	output.NormalWS = normalize(mul(float4(input.Normal, 0.0f), input.World).xyz);
	output.TangentWS = normalize(mul(float4(input.Tangent, 0.0f), input.World).xyz);
	output.BitangentWS = normalize(mul(float4(input.Bitangent, 0.0f), input.World).xyz);
	
#ifdef PARALLAX
	//Transpose of rotation matrix is the inverse
	float3x3 worldToTangent = transpose(float3x3(output.TangentWS, output.BitangentWS, output.NormalWS));
	
  float3 viewDir = normalize(EyePos - output.PositionWS);
  output.ViewDirectionTS = normalize(mul(viewDir, worldToTangent));
#endif
	
	return output;
}


//-----------------------------------------------------------------------------//
//                       **** Pixel Shaders ****                               //
//-----------------------------------------------------------------------------//

//
//  **** Uniform Color ****
//
float4 PS_LitStandard_Color(VSOutputNm input) : SV_TARGET
{
	float3 normalWS = normalize(input.NormalWS);

	//Calculate the V vector for specular highlights
	float3 V = normalize(EyePos - input.PositionWS);
  float3 lightContribs = float3(0,0,0);
  float3 ambientTerm = float3(0.0f, 0.0f, 0.0f);
    
  for(int i = 0; i < NumLights; i++)
  {
    LightResult result = ComputeSingleLight(Lights[i], input.PositionWS, normalWS, V, MatShininess);
    
    //Sum ambient results
    ambientTerm += result.Ambient;
    
    //Get the diffuse term for this light (all diffuse constants modulated by the intensity)
    float3 diffuseTerm = result.Diffuse * MatDiffuse;
    
    //Get the specular term for this light (all specular constants modulated by the intensity)
    float3 specularTerm = result.Specular * MatSpecular;
    
    //Sum contributions with the running total and apply attenuation and spot effects to diminish as necessary
    lightContribs += result.Attenuation * (diffuseTerm + specularTerm);
  }
  
  //Final RGB color is the total light ambient modulated by ambient constants, then the summation of the diffuse/specular terms, then a constant emissive term
  float3 finalColor = (ambientTerm * MatAmbient * GlobalAmbient) + lightContribs + MatEmissive;
  
  return float4(finalColor, Alpha);
}

//
//  **** Uniform Color with vertex coloring ****
//
float4 PS_LitStandard_Color_Vc(VSOutputVcNm input) : SV_TARGET
{
	float3 normalWS = normalize(input.NormalWS);

	//Calculate the V vector for specular highlights
	float3 V = normalize(EyePos - input.PositionWS);
  float3 lightContribs = float3(0,0,0);
  float3 ambientTerm = float3(0.0f, 0.0f, 0.0f);
    
  for(int i = 0; i < NumLights; i++)
  {
    LightResult result = ComputeSingleLight(Lights[i], input.PositionWS, normalWS, V, MatShininess);
    
    //Sum ambient results
    ambientTerm += result.Ambient;
    
    //Get the diffuse term for this light (all diffuse constants modulated by the intensity)
    float3 diffuseTerm = result.Diffuse * MatDiffuse * input.VertColor.rgb;
    
    //Get the specular term for this light (all specular constants modulated by the intensity)
    float3 specularTerm = result.Specular * MatSpecular;
    
    //Sum contributions with the running total and apply attenuation and spot effects to diminish as necessary
    lightContribs += result.Attenuation * (diffuseTerm + specularTerm);
  }
  
  //Final RGB color is the total light ambient modulated by ambient constants, then the summation of the diffuse/specular terms, then a constant emissive term
  float3 finalColor = (ambientTerm * MatAmbient * GlobalAmbient) + lightContribs + MatEmissive;
  
  return float4(finalColor, Alpha * input.VertColor.a);
}

//
//  **** Texture ****
//
float4 PS_LitStandard_Texture(VSOutputTxNm input) : SV_TARGET
{
	//Get the diffuse map color
	float4 albedo = DiffuseMap.Sample(MapSampler, input.TexCoord);
	
	float3 normalWS = normalize(input.NormalWS);

  float3 emissive = MatEmissive; 
  #ifdef EMISSIVE
    emissive *= EmissiveMap.Sample(MapSampler, input.TexCoord).rgb;
  #endif

	//Calculate the V vector for specular highlights
	float3 V = normalize(EyePos - input.PositionWS);
  float3 lightContribs = float3(0,0,0);
  float3 ambientTerm = float3(0.0f, 0.0f, 0.0f);
    
  for(int i = 0; i < NumLights; i++)
  {
    LightResult result = ComputeSingleLight(Lights[i], input.PositionWS, normalWS, V, MatShininess);
    
    //Sum ambient results
    ambientTerm += result.Ambient;
    
    //Get the diffuse term for this light (all diffuse constants modulated by the intensity)
    float3 diffuseTerm = result.Diffuse * MatDiffuse * albedo.rgb;
    
    //Get the specular term for this light (all specular constants modulated by the intensity)
    float3 specularTerm = result.Specular * MatSpecular;
    
    //Sum contributions with the running total and apply attenuation and spot effects to diminish as necessary
    lightContribs += result.Attenuation * (diffuseTerm + specularTerm);
  }
  
  //Final RGB color is the total light ambient modulated by ambient constants, then the summation of the diffuse/specular terms, then a constant emissive term
  float3 finalColor = (ambientTerm * MatAmbient * GlobalAmbient) + lightContribs + emissive;
  
  return float4(finalColor, Alpha * albedo.a);
}

//
//  **** Texture with normal mapping ****
//
float4 PS_LitStandard_Texture_Nmm(
#ifdef PARALLAX
  VSOutputTxNmTnBt_Parallax input
#else
  VSOutputTxNmTnBt input
#endif
) : SV_TARGET
{

#ifdef PARALLAX
    //Offset texture coordinate using parallax occlusion displacement
    float3 viewDir = normalize(input.ViewDirectionTS);
   
   input.TexCoord = CalculateParallaxOcclusionTextureCoordinates(input.TexCoord, NormalMap, MapSampler, viewDir, HeightScale, MinParallaxSamples, MaxParallaxSamples);
    
    if(ClampParallaxTexture && (input.TexCoord.x < 0.0 || input.TexCoord.x > 1.0 || input.TexCoord.y < 0.0 || input.TexCoord.y > 1.0))
      discard;
#endif
      
    //Get the normal
    float3 texNormal = ExpandNormal(NormalMap.Sample(MapSampler, input.TexCoord).xyz);
    
    //Transform normal from tangent space to world space
    float3x3 tangentToWorld;
    tangentToWorld[0] = normalize(input.TangentWS);
    tangentToWorld[1] = normalize(input.BitangentWS);
    tangentToWorld[2] = normalize(input.NormalWS);

    float3 normalWS = normalize(mul(texNormal, tangentToWorld));

    //Get the diffuse map color
    float4 albedo = float4(1.0f, 1.0f, 1.0f, 1.0f);
    if(UseDiffuseMap) 
        albedo = DiffuseMap.Sample(MapSampler, input.TexCoord);

    //Get the specular map color
    float4 spec = float4(1.0f, 1.0f, 1.0f, 1.0f);
    if(UseSpecularMap) 
        spec = SpecularMap.Sample(MapSampler, input.TexCoord);
        
    float3 emissive = MatEmissive;
    #ifdef EMISSIVE
      emissive *= EmissiveMap.Sample(MapSampler, input.TexCoord).rgb;
    #endif
  
    //Calculate the V vector for specular highlights
    float3 V = normalize(EyePos - input.PositionWS);
    float3 lightContribs = float3(0,0,0);
    float3 ambientTerm = float3(0.0f, 0.0f, 0.0f);
      
    for(int i = 0; i < NumLights; i++)
    {
      LightResult result = ComputeSingleLight(Lights[i], input.PositionWS, normalWS, V, MatShininess);
      
      //Sum ambient results
      ambientTerm += result.Ambient;
      
      //Get the diffuse term for this light (all diffuse constants modulated by the intensity)
      float3 diffuseTerm = result.Diffuse * MatDiffuse * albedo.rgb;
      
      //Get the specular term for this light (all specular constants modulated by the intensity)
      float3 specularTerm = result.Specular * MatSpecular * spec.rgb;
      
      //Sum contributions with the running total and apply attenuation and spot effects to diminish as necessary
      lightContribs += result.Attenuation * (diffuseTerm + specularTerm);
    }
    
    //Final RGB color is the total light ambient modulated by ambient constants, then the summation of the diffuse/specular terms, then a constant emissive term
    float3 finalColor = (ambientTerm * MatAmbient * GlobalAmbient) + lightContribs + emissive;
    
    return float4(finalColor, Alpha * albedo.a);
}

//
//  **** Texture with Vertex Coloring ****
//
float4 PS_LitStandard_Texture_Vc(VSOutputVcTxNm input) : SV_TARGET
{
	//Get the diffuse map color
	float4 albedo = DiffuseMap.Sample(MapSampler, input.TexCoord);
	
  float3 emissive = MatEmissive;
  #ifdef EMISSIVE
    emissive *= EmissiveMap.Sample(MapSampler, input.TexCoord).rgb;
  #endif
	
	float3 normalWS = normalize(input.NormalWS);

	//Calculate the V vector for specular highlights
	float3 V = normalize(EyePos - input.PositionWS);
  float3 lightContribs = float3(0,0,0);
  float3 ambientTerm = float3(0.0f, 0.0f, 0.0f);
    
  for(int i = 0; i < NumLights; i++)
  {
    LightResult result = ComputeSingleLight(Lights[i], input.PositionWS, normalWS, V, MatShininess);
    
    //Sum ambient results
    ambientTerm += result.Ambient;
    
    //Get the diffuse term for this light (all diffuse constants modulated by the intensity)
    float3 diffuseTerm = result.Diffuse * MatDiffuse * albedo.rgb * input.VertColor.rgb;
    
    //Get the specular term for this light (all specular constants modulated by the intensity)
    float3 specularTerm = result.Specular * MatSpecular;
    
    //Sum contributions with the running total and apply attenuation and spot effects to diminish as necessary
    lightContribs += result.Attenuation * (diffuseTerm + specularTerm);
  }
  
  //Final RGB color is the total light ambient modulated by ambient constants, then the summation of the diffuse/specular terms, then a constant emissive term
  float3 finalColor = (ambientTerm * MatAmbient * GlobalAmbient) + lightContribs + emissive;
  
  return float4(finalColor, Alpha * albedo.a * input.VertColor.a);
}

//
//  **** Texture with Vertex Coloring/Normal maps ****
//
float4 PS_LitStandard_Texture_VcNmm(
#ifdef PARALLAX
  VSOutputVcTxNmTnBt_Parallax input
#else
  VSOutputVcTxNmTnBt input
#endif
) : SV_TARGET
{

#ifdef PARALLAX
    //Offset texture coordinate using parallax occlusion displacement
    float3 viewDir = normalize(input.ViewDirectionTS);
   
   input.TexCoord = CalculateParallaxOcclusionTextureCoordinates(input.TexCoord, NormalMap, MapSampler, viewDir, HeightScale, MinParallaxSamples, MaxParallaxSamples);
    
    if(ClampParallaxTexture && (input.TexCoord.x < 0.0 || input.TexCoord.x > 1.0 || input.TexCoord.y < 0.0 || input.TexCoord.y > 1.0))
      discard;
#endif

    //Get the normal
    float3 texNormal = ExpandNormal(NormalMap.Sample(MapSampler, input.TexCoord).xyz);
    
    //Transform normal from tangent space to world space
    float3x3 tangentToWorld;
    tangentToWorld[0] = normalize(input.TangentWS);
    tangentToWorld[1] = normalize(input.BitangentWS);
    tangentToWorld[2] = normalize(input.NormalWS);

    float3 normalWS = normalize(mul(texNormal, tangentToWorld));

    //Get the diffuse map color
    float4 albedo = float4(1.0f, 1.0f, 1.0f, 1.0f);
    if(UseDiffuseMap) 
        albedo = DiffuseMap.Sample(MapSampler, input.TexCoord);

    //Get the specular map color
    float4 spec = float4(1.0f, 1.0f, 1.0f, 1.0f);
    if(UseSpecularMap) 
        spec = SpecularMap.Sample(MapSampler, input.TexCoord);
        
    float3 emissive = MatEmissive;
    #ifdef EMISSIVE
      emissive *= EmissiveMap.Sample(MapSampler, input.TexCoord).rgb;
    #endif

    //Calc the view V vector for specular highlights
    float3 V = normalize(EyePos - input.PositionWS);
    float3 lightContribs = float3(0.0f, 0.0f, 0.0f);
    float3 ambientTerm = float3(0.0f, 0.0f, 0.0f);
    
    for(int i = 0; i < NumLights; i++)
    {
      LightResult result = ComputeSingleLight(Lights[i], input.PositionWS, normalWS, V, MatShininess);
      
      //Sum ambient results
      ambientTerm += result.Ambient;
      
      //Get the diffuse term for this light (all diffuse constants modulated by the intensity)
      float3 diffuseTerm = result.Diffuse * MatDiffuse * albedo.rgb * input.VertColor.rgb;
      
      //Get the specular term for this light (all specular constants modulated by the intensity)
      float3 specularTerm = result.Specular * MatSpecular * spec.rgb;
      
      //Sum contributions with the running total and apply attenuation and spot effects to diminish as necessary
      lightContribs += result.Attenuation * (diffuseTerm + specularTerm);
    }
    
    //Final RGB color is the total light ambient modulated by ambient constants, then the summation of the diffuse/specular terms, then a constant emissive term
    float3 finalColor = (ambientTerm * MatAmbient * GlobalAmbient) + lightContribs + emissive;
    
    return float4(finalColor, Alpha * albedo.a * input.VertColor.a);
}