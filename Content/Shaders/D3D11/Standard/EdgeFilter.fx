/*
* The shader code below is licensed under the Creative Commons Attribution-Share Alike 3.0 
* license. Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
*
* You are free:
*	To Share: to copy, distribute, display, and perform the work
*	To Remix: to make derivative works 
*
*	Under the following conditions:
*		* Attribution - You must attribute the work in the manner specified by the author or
*			licensor (but not in any way that suggests that they endorse you or your use of the work).
*		* Share Alike - If you alter, transform, or build upon this work, you may distribute
*			the resulting work only under the same, similar or a compatible license.
*
*		For any reuse or distribution, you must make clear to others the license terms of this work.
*		The best way to do this is with a link to the above web page (http;//www.tesla-engine.net)
*
*		Any of the above conditions can be waived if you get permission from the copyright holder.
*
*		Apart from the remix rights granted under this license, nothing in this license impairs or
*		restricts the author's moral rights.
*/

#include "Structures.fxh"

//
// Variables
//

cbuffer PerFrame 
{
  float4x4 SpriteTransform;
};

cbuffer FilterOptions
{
  float2 TextureDimensions;
};

//Vertex shader and parameter compatible with SpriteBatch
Texture2D SpriteMap;
SamplerState SpriteMapSampler 
{
  Filter = MIN_MAG_MIP_LINEAR;
  AddressU = Clamp;
  AddressV = Clamp;
  AddressW = Clamp;
};

//
// **** Edge Filter ****
//

VSOutputVcTx VS_EdgeFilter(VSInputVcTx input) 
{
	VSOutputVcTx output;
	output.PositionPS = mul(float4(input.Position, 1.0), SpriteTransform);
	output.VertColor = input.VertColor;
	output.TexCoord = input.TexCoord;
	return output;
}

float4 PS_EdgeFilter(VSOutputVcTx input) : SV_TARGET
{
  float width = TextureDimensions.x;
  float height = TextureDimensions.y;
  
  float4 lum = float4(0.30, 0.59, 0.11, 1);
  
  //Top row
  float s11 = dot(SpriteMap.Sample(SpriteMapSampler, input.TexCoord + float2(-1.0f / width, -1.0f / height)), lum); //Left
  float s12 = dot(SpriteMap.Sample(SpriteMapSampler, input.TexCoord + float2(0, -1.0f / height)), lum);             //Middle
  float s13 = dot(SpriteMap.Sample(SpriteMapSampler, input.TexCoord + float2(1.0f / width, -1.0f / height)), lum);    //Right
 
  //Middle row
  float s21 = dot(SpriteMap.Sample(SpriteMapSampler, input.TexCoord + float2(-1.0f / width, 0)), lum);                //Left
  // Omit center
  float s23 = dot(SpriteMap.Sample(SpriteMapSampler, input.TexCoord + float2(-1.0f / width, 0)), lum);                //Right
 
  //Last row
  float s31 = dot(SpriteMap.Sample(SpriteMapSampler, input.TexCoord + float2(-1.0f / width, 1.0f / height)), lum);    //Left
  float s32 = dot(SpriteMap.Sample(SpriteMapSampler, input.TexCoord + float2(0, 1.0f / height)), lum);              //Middle
  float s33 = dot(SpriteMap.Sample(SpriteMapSampler, input.TexCoord + float2(1.0f / width, 1.0f / height)), lum); //Right
  
  //Filter
  float t1 = s13 + s33 + (2 * s23) - s11 - (2 * s21) - s31;
  float t2 = s31 + (2 * s32) + s33 - s11 - (2 * s12) - s13;
  
  float4 color;
  
  if (((t1 * t1) + (t2 * t2)) > 0.05)
    color = float4(0, 0, 0, 1);
  else
    color = float4(1, 1, 1, 1);
    
  return color;
}

technique11 EdgeFilter
{
  pass
  {
		SetVertexShader(CompileShader(vs_5_0, VS_EdgeFilter()));
		SetPixelShader(CompileShader(ps_5_0, PS_EdgeFilter()));
  }
}