/*
* The shader code below is licensed under the Creative Commons Attribution-Share Alike 3.0 
* license. Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
*
* You are free:
*	To Share: to copy, distribute, display, and perform the work
*	To Remix: to make derivative works 
*
*	Under the following conditions:
*		* Attribution - You must attribute the work in the manner specified by the author or
*			licensor (but not in any way that suggests that they endorse you or your use of the work).
*		* Share Alike - If you alter, transform, or build upon this work, you may distribute
*			the resulting work only under the same, similar or a compatible license.
*
*		For any reuse or distribution, you must make clear to others the license terms of this work.
*		The best way to do this is with a link to the above web page (http;//www.tesla-engine.net)
*
*		Any of the above conditions can be waived if you get permission from the copyright holder.
*
*		Apart from the remix rights granted under this license, nothing in this license impairs or
*		restricts the author's moral rights.
*/

#include "Structures.fxh"

//
// Variables
//

cbuffer PerFrame 
{
  float4x4 SpriteTransform;
};

cbuffer FilterOptions
{
  float2 TextureDimensions;
};

//Vertex shader and parameter compatible with SpriteBatch
Texture2D SpriteMap;
SamplerState SpriteMapSampler 
{
  Filter = MIN_MAG_MIP_LINEAR;
  AddressU = Clamp;
  AddressV = Clamp;
  AddressW = Clamp;
};

//
// **** Edge Filter ****
//

VSOutputVcTx VS_EmbossFilter(VSInputVcTx input) 
{
	VSOutputVcTx output;
	output.PositionPS = mul(float4(input.Position, 1.0), SpriteTransform);
	output.VertColor = input.VertColor;
	output.TexCoord = input.TexCoord;
	return output;
}

float4 PS_EmbossFilter(VSOutputVcTx input) : SV_TARGET
{
  float width = TextureDimensions.x;
  float height = TextureDimensions.y;
  
  float4 s22 = SpriteMap.Sample(SpriteMapSampler, input.TexCoord);
  float4 s11 = SpriteMap.Sample(SpriteMapSampler, input.TexCoord + float2(-1.0 / width, -1.0 / height));
  float4 s33 = SpriteMap.Sample(SpriteMapSampler, input.TexCoord + float2(1.0 / width, 1.0 / height));
  
  s11.rgb = (s11.r + s11.g + s11.b);
  s22.rgb = (s22.r + s22.g + s22.b) * -0.5;
  s33.rgb = (s22.r + s22.g + s22.b) * 0.2;
  
  return (s11 + s22 + s33);
}

technique11 EmbossFilter
{
  pass
  {
		SetVertexShader(CompileShader(vs_5_0, VS_EmbossFilter()));
		SetPixelShader(CompileShader(ps_5_0, PS_EmbossFilter()));
  }
}