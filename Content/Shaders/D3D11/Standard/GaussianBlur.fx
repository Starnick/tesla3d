/*
* The shader code below is licensed under the Creative Commons Attribution-Share Alike 3.0 
* license. Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
*
* You are free:
*	To Share: to copy, distribute, display, and perform the work
*	To Remix: to make derivative works 
*
*	Under the following conditions:
*		* Attribution - You must attribute the work in the manner specified by the author or
*			licensor (but not in any way that suggests that they endorse you or your use of the work).
*		* Share Alike - If you alter, transform, or build upon this work, you may distribute
*			the resulting work only under the same, similar or a compatible license.
*
*		For any reuse or distribution, you must make clear to others the license terms of this work.
*		The best way to do this is with a link to the above web page (http;//www.tesla-engine.net)
*
*		Any of the above conditions can be waived if you get permission from the copyright holder.
*
*		Apart from the remix rights granted under this license, nothing in this license impairs or
*		restricts the author's moral rights.
*/

#include "Structures.fxh"

//
// Variables
//

cbuffer PerFrame 
{
  float4x4 SpriteTransform;
};

cbuffer BlurOptions
{
	float2 SampleOffset;
	float Attenuation;
};

//Vertex shader and parameter compatible with SpriteBatch
Texture2D SpriteMap;
SamplerState SpriteMapSampler 
{
  Filter = MIN_MAG_MIP_LINEAR;
  AddressU = Clamp;
  AddressV = Clamp;
  AddressW = Clamp;
};

//
// **** Gaussian Blur ****
//

VSOutputVcTx VS_GaussianBlur(VSInputVcTx input) 
{
	VSOutputVcTx output;
	output.PositionPS = mul(float4(input.Position, 1.0), SpriteTransform);
	output.VertColor = input.VertColor;
	output.TexCoord = input.TexCoord;
	return output;
}

float4 PS_GaussianBlur(VSOutputVcTx input) : SV_TARGET
{
	float4 color = float4(0, 0, 0, 0);

	color += SpriteMap.Sample(SpriteMapSampler, input.TexCoord - 10.0 * SampleOffset).rgba * 0.009167927656011385;
	color += SpriteMap.Sample(SpriteMapSampler, input.TexCoord - 9.0 * SampleOffset).rgba * 0.014053461291849008;
	color += SpriteMap.Sample(SpriteMapSampler, input.TexCoord - 8.0 * SampleOffset).rgba * 0.020595286319257878;
	color += SpriteMap.Sample(SpriteMapSampler, input.TexCoord - 7.0 * SampleOffset).rgba * 0.028855245532226279;
	color += SpriteMap.Sample(SpriteMapSampler, input.TexCoord - 6.0 * SampleOffset).rgba * 0.038650411513543079;
	color += SpriteMap.Sample(SpriteMapSampler, input.TexCoord - 5.0 * SampleOffset).rgba * 0.049494378859311142;
	color += SpriteMap.Sample(SpriteMapSampler, input.TexCoord - 4.0 * SampleOffset).rgba * 0.060594058578763078;
	color += SpriteMap.Sample(SpriteMapSampler, input.TexCoord - 3.0 * SampleOffset).rgba * 0.070921288047096992;
	color += SpriteMap.Sample(SpriteMapSampler, input.TexCoord - 2.0 * SampleOffset).rgba * 0.079358891804948081;
	color += SpriteMap.Sample(SpriteMapSampler, input.TexCoord - 1.0 * SampleOffset).rgba * 0.084895951965930902;
	color += SpriteMap.Sample(SpriteMapSampler, input.TexCoord + 0.0 * SampleOffset).rgba * 0.086826196862124602;
	color += SpriteMap.Sample(SpriteMapSampler, input.TexCoord + 1.0 * SampleOffset).rgba * 0.084895951965930902;
	color += SpriteMap.Sample(SpriteMapSampler, input.TexCoord + 2.0 * SampleOffset).rgba * 0.079358891804948081;
	color += SpriteMap.Sample(SpriteMapSampler, input.TexCoord + 3.0 * SampleOffset).rgba * 0.070921288047096992;
	color += SpriteMap.Sample(SpriteMapSampler, input.TexCoord + 4.0 * SampleOffset).rgba * 0.060594058578763078;
	color += SpriteMap.Sample(SpriteMapSampler, input.TexCoord + 5.0 * SampleOffset).rgba * 0.049494378859311142;
	color += SpriteMap.Sample(SpriteMapSampler, input.TexCoord + 6.0 * SampleOffset).rgba * 0.038650411513543079;
	color += SpriteMap.Sample(SpriteMapSampler, input.TexCoord + 7.0 * SampleOffset).rgba * 0.028855245532226279;
	color += SpriteMap.Sample(SpriteMapSampler, input.TexCoord + 8.0 * SampleOffset).rgba * 0.020595286319257878;
	color += SpriteMap.Sample(SpriteMapSampler, input.TexCoord + 9.0 * SampleOffset).rgba * 0.014053461291849008;
	color += SpriteMap.Sample(SpriteMapSampler, input.TexCoord + 10.0 * SampleOffset).rgba * 0.009167927656011385;

	color *= Attenuation;

	return color;
}

technique11 GaussianBlur
{
  pass
  {
		SetVertexShader(CompileShader(vs_5_0, VS_GaussianBlur()));
		SetPixelShader(CompileShader(ps_5_0, PS_GaussianBlur()));
  }
}