/*
* The shader code below is licensed under the Creative Commons Attribution-Share Alike 3.0 
* license. Copyright (c) 2010-2015 Tesla 3D Engine - Nicholas Woodfield
*
* You are free:
*	To Share: to copy, distribute, display, and perform the work
*	To Remix: to make derivative works 
*
*	Under the following conditions:
*		* Attribution - You must attribute the work in the manner specified by the author or
*			licensor (but not in any way that suggests that they endorse you or your use of the work).
*		* Share Alike - If you alter, transform, or build upon this work, you may distribute
*			the resulting work only under the same, similar or a compatible license.
*
*		For any reuse or distribution, you must make clear to others the license terms of this work.
*		The best way to do this is with a link to the above web page (http;//www.tesla-engine.net)
*
*		Any of the above conditions can be waived if you get permission from the copyright holder.
*
*		Apart from the remix rights granted under this license, nothing in this license impairs or
*		restricts the author's moral rights.
*/

//
// Common listing for vertex shader inputs and outputs. Abbrevations are used for what
// type of vertex attribute (Vc = Vertex Color, Tx = Texture Coordinate, Nm = Normal,
// Tn = Tangent, Bt = Bitangent, Plx = Parallax). Combinations for normal mapping, unlit, lit, and instancing are defined.
//

//
// **** Vertex Shader Inputs ****
//

//
// Position/Vertex Color
//
struct VSInputVc 
{
	float3 Position : POSITION;
	float4 VertColor : COLOR;
};

struct VSInputVc_Instanced 
{
  //VertexBuffer 0
	float3 Position : POSITION;
	float4 VertColor : COLOR;
	
	//VertexBuffer 1
	float4x4 World : TEXCOORD0;
};

//
// Position/Texture Coordinate
//
struct VSInputTx 
{
	float3 Position : POSITION;
	float2 TexCoord : TEXCOORD;
};

struct VSInputTx_Instanced
{
  //VertexBuffer 0
	float3 Position : POSITION;
	float2 TexCoord : TEXCOORD0;
	
	//VertexBuffer 1
	float4x4 World : TEXCOORD1;
};

//
// Position/Vertex Color/Texture Coordinate
//
struct VSInputVcTx 
{
	float3 Position : POSITION;
	float4 VertColor : COLOR;
	float2 TexCoord : TEXCOORD;
};

struct VSInputVcTx_Instanced
{
  //VertexBuffer 0
  float3 Position : POSITION;
	float4 VertColor : COLOR;
	float2 TexCoord : TEXCOORD0;
	
  //VertexBuffer 1
  float4x4 World : TEXCOORD1;
};

//
// Position/Normal
//
struct VSInputNm 
{
	float3 Position : POSITION;
	float3 Normal : NORMAL;
};

struct VSInputNm_Instanced
{
  //VertexBuffer 0
	float3 Position : POSITION;
	float3 Normal : NORMAL;
	
	//VertexBuffer 1
	float4x4 World : TEXCOORD0;
};

//
// Position/Vertex Color/Normal
//
struct VSInputVcNm
{
	float3 Position : POSITION;
	float4 VertColor : COLOR;
	float3 Normal : NORMAL;
};

struct VSInputVcNm_Instanced
{
  //VertexBuffer 0
  float3 Position : POSITION;
  float4 VertColor : COLOR;
	float3 Normal : NORMAL;
	
	//VertexBuffer 1
	float4x4 World : TEXCOORD0;
};

//
// Position/Texture Coordinate/Normal
//
struct VSInputTxNm 
{
	float3 Position : POSITION;
	float2 TexCoord : TEXCOORD;
	float3 Normal : NORMAL;
};

struct VSInputTxNm_Instanced
{
  //VertexBuffer 0
  float3 Position : POSITION;
	float2 TexCoord : TEXCOORD0;
	float3 Normal : NORMAL;
	
  //VertexBuffer 1
  float4x4 World : TEXCOORD1;
};

//
// Position/Vertex Color/Texture Coordinate/Normal
//
struct VSInputVcTxNm 
{
	float3 Position : POSITION;
	float4 VertColor : COLOR;
	float2 TexCoord : TEXCOORD;
	float3 Normal : NORMAL;
};

struct VSInputVcTxNm_Instanced
{
  //VertexBuffer 0
  float3 Position : POSITION;
	float4 VertColor : COLOR;
	float2 TexCoord : TEXCOORD0;
	float3 Normal : NORMAL;
	
  //VertexBuffer 1
  float4x4 World : TEXCOORD1;
};

//
// Position/Texture Coordinate/Normal/Tangent/Bitangent
//
struct VSInputTxNmTnBt 
{
	float3 Position : POSITION;
	float2 TexCoord : TEXCOORD;
	float3 Normal : NORMAL;
	float3 Tangent : TANGENT;
	float3 Bitangent : BITANGENT;
};

struct VSInputTxNmTnBt_Instanced
{
  //VertexBuffer 0
	float3 Position : POSITION;
	float2 TexCoord : TEXCOORD0;
	float3 Normal : NORMAL;
	float3 Tangent : TANGENT;
	float3 Bitangent : BITANGENT;
	
	//VertexBuffer 1
	float4x4 World : TEXCOORD1;
};

//
// Position/Texture Coordinate/Normal/Tangent (Bitangent should be calculated based on normal/tangent)
//
struct VSInputTxNmTn 
{
	float3 Position : POSITION;
	float2 TexCoord : TEXCOORD;
	float3 Normal : NORMAL;
	float3 Tangent : TANGENT;
};

struct VSInputTxNmTn_Instanced
{
  //VertexBuffer 0
	float3 Position : POSITION;
	float2 TexCoord : TEXCOORD0;
	float3 Normal : NORMAL;
	float3 Tangent : TANGENT;
	
	//VertexBuffer 1
	float4x4 World : TEXCOORD1;
};

//
// Position/Vertex Color/Texture Coordinate/Normal/Tangent/Bitangent
//
struct VSInputVcTxNmTnBt
{
  //VertexBuffer 0
	float3 Position : POSITION;
	float4 VertColor : COLOR;
	float2 TexCoord : TEXCOORD0;
	float3 Normal : NORMAL;
	float3 Tangent : TANGENT;
	float3 Bitangent : BITANGENT;
	
	//VertexBuffer 1
	float4x4 World : TEXCOORD1;
};

struct VSInputVcTxNmTnBt_Instanced
{
  //VertexBuffer 0
	float3 Position : POSITION;
	float4 VertColor : COLOR;
	float2 TexCoord : TEXCOORD0;
	float3 Normal : NORMAL;
	float3 Tangent : TANGENT;
	float3 Bitangent : BITANGENT;
	
  //VertexBuffer 1
  float4x4 World : TEXCOORD1;
};

//
// Position/Vertex Color/Texture Coordinate/Tangent (Bitangent should be calculated based on normal/tangent)
//
struct VSInputVcTxNmTn
{
	float3 Position : POSITION;
	float4 VertColor : COLOR;
	float2 TexCoord : TEXCOORD;
	float3 Normal : NORMAL;
	float3 Tangent : TANGENT;
};

struct VSInputVcTxNmTn_Instanced
{
  //VertexBuffer 0
	float3 Position : POSITION;
	float4 VertColor : COLOR;
	float2 TexCoord : TEXCOORD0;
	float3 Normal : NORMAL;
	float3 Tangent : TANGENT;
	
	//VertexBuffer 1
  float4x4 World : TEXCOORD1;
};



//
// **** Vertex Outputs ****
//

struct VSOutputVc 
{
	float4 PositionPS : SV_POSITION;
	float4 VertColor : COLOR;
};

struct VSOutputTx 
{
	float4 PositionPS : SV_POSITION;
	float2 TexCoord : TEXCOORD;
};

struct VSOutputVcTx
{
	float4 PositionPS : SV_POSITION;
	float4 VertColor : COLOR;
	float2 TexCoord : TEXCOORD;
};

struct VSOutputNm
{
	float4 PositionPS : SV_POSITION;
	float3 PositionWS : POSITION;
	float3 NormalWS : NORMAL;
};

struct VSOutputVcNm
{
	float4 PositionPS : SV_POSITION;
	float3 PositionWS : POSITION;
	float4 VertColor : COLOR;
	float3 NormalWS : NORMAL;
};

struct VSOutputTxNm
{
	float4 PositionPS : SV_POSITION;
	float3 PositionWS : POSITION;
	float2 TexCoord : TEXCOORD;
  float3 NormalWS : NORMAL;
};

struct VSOutputVcTxNm
{
	float4 PositionPS : SV_POSITION;
	float3 PositionWS : POSITION;
	float4 VertColor : COLOR;
	float2 TexCoord : TEXCOORD;
	float3 NormalWS : NORMAL;
};

struct VSOutputTxNmTnBt
{
	float4 PositionPS : SV_POSITION;
	float3 PositionWS : POSITION;
	float2 TexCoord : TEXCOORD;
	float3 NormalWS : NORMAL;
	float3 TangentWS : TANGENT;
	float3 BitangentWS : BITANGENT;
};

struct VSOutputTxNmTnBt_Parallax
{
	float4 PositionPS : SV_POSITION;
	float3 PositionWS : POSITION;
	float2 TexCoord : TEXCOORD0;
	float3 NormalWS : NORMAL;
	float3 TangentWS : TANGENT;
	float3 BitangentWS : BITANGENT;
	float3 ViewDirectionTS : TEXCOORD1;
};

struct VSOutputVcTxNmTnBt
{
	float4 PositionPS : SV_POSITION;
	float3 PositionWS : POSITION;
	float4 VertColor : COLOR;
	float2 TexCoord : TEXCOORD;
	float3 NormalWS : NORMAL;
	float3 TangentWS : TANGENT;
	float3 BitangentWS : BITANGENT;
};

struct VSOutputVcTxNmTnBt_Parallax
{
	float4 PositionPS : SV_POSITION;
	float3 PositionWS : POSITION;
	float4 VertColor : COLOR;
	float2 TexCoord : TEXCOORD0;
	float3 NormalWS : NORMAL;
	float3 TangentWS : TANGENT;
	float3 BitangentWS : BITANGENT;
	float3 ViewDirectionTS : TEXCOORD1;
};