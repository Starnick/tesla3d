/*
* The shader code below is licensed under the Creative Commons Attribution-Share Alike 3.0 
* license. Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
*
* You are free:
*	To Share: to copy, distribute, display, and perform the work
*	To Remix: to make derivative works 
*
*	Under the following conditions:
*		* Attribution - You must attribute the work in the manner specified by the author or
*			licensor (but not in any way that suggests that they endorse you or your use of the work).
*		* Share Alike - If you alter, transform, or build upon this work, you may distribute
*			the resulting work only under the same, similar or a compatible license.
*
*		For any reuse or distribution, you must make clear to others the license terms of this work.
*		The best way to do this is with a link to the above web page (http;//www.tesla-engine.net)
*
*		Any of the above conditions can be waived if you get permission from the copyright holder.
*
*		Apart from the remix rights granted under this license, nothing in this license impairs or
*		restricts the author's moral rights.
*/

#include "Structures.fxh"

//
// Variables
//

cbuffer PerFrame 
{
  float4x4 SpriteTransform;
};

Texture2D SpriteMap;
SamplerState SpriteMapSampler 
{
  Filter = MIN_MAG_MIP_LINEAR;
  AddressU = Clamp;
  AddressV = Clamp;
  AddressW = Clamp;
};

//
// **** SpriteTexture ****
//

VSOutputVcTx VS_SpriteTexture(VSInputVcTx input) 
{
	VSOutputVcTx output;
	output.PositionPS = mul(float4(input.Position, 1.0), SpriteTransform);
	output.VertColor = input.VertColor;
	output.TexCoord = input.TexCoord;
	return output;
}

float4 PS_SpriteTexture(VSOutputVcTx input) : SV_TARGET 
{
  return SpriteMap.Sample(SpriteMapSampler, input.TexCoord) * input.VertColor;
}


technique11 SpriteTexture 
{
  pass
  {
    SetVertexShader(CompileShader(vs_5_0, VS_SpriteTexture()));
    SetPixelShader(CompileShader(ps_5_0, PS_SpriteTexture()));
  }
}

//
// **** SpriteNoTexture ****
//

VSOutputVc VS_SpriteNoTexture(VSInputVc input)
{
	VSOutputVc output;
	output.PositionPS = mul(float4(input.Position, 1.0), SpriteTransform);
	output.VertColor = input.VertColor;
	return output;
}

float4 PS_SpriteNoTexture(VSOutputVc input) : SV_TARGET
{
	return input.VertColor;
}

technique11 SpriteNoTexture
{
	pass
	{
		SetVertexShader(CompileShader(vs_5_0, VS_SpriteNoTexture()));
		SetPixelShader(CompileShader(ps_5_0, PS_SpriteNoTexture()));
	}
}