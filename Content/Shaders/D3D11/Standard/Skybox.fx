/*
* The shader code below is licensed under the Creative Commons Attribution-Share Alike 3.0 
* license. Copyright (c) 2010-2015 Tesla 3D Engine - Nicholas Woodfield
*
* You are free:
*	To Share: to copy, distribute, display, and perform the work
*	To Remix: to make derivative works 
*
*	Under the following conditions:
*		* Attribution - You must attribute the work in the manner specified by the author or
*			licensor (but not in any way that suggests that they endorse you or your use of the work).
*		* Share Alike - If you alter, transform, or build upon this work, you may distribute
*			the resulting work only under the same, similar or a compatible license.
*
*		For any reuse or distribution, you must make clear to others the license terms of this work.
*		The best way to do this is with a link to the above web page (http;//www.tesla-engine.net)
*
*		Any of the above conditions can be waived if you get permission from the copyright holder.
*
*		Apart from the remix rights granted under this license, nothing in this license impairs or
*		restricts the author's moral rights.
*/

//
// Shaders
//

cbuffer cbPerFrame 
{
	float3 CamPos;
	float4x4 View;
	float4x4 Proj;
};

TextureCube DiffuseMap;
SamplerState MapSampler 
{
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = Wrap;
	AddressV = Wrap;
	AddressW = Wrap;
};

struct VSInputTx 
{
	float3 Position : POSITION;
	float2 TexCoord : TEXCOORD;
};

struct VSOutputTx 
{
	float4 PositionPS : SV_POSITION;
	float3 TexCoord : TEXCOORD;   //3D texture coordinate for TexCube
};

//
// Shaders
//

VSOutputTx VS_Skybox(float3 Position : POSITION) 
{
	float4x4 world = { 1, 0, 0, CamPos.x,
                     0, 1, 0, CamPos.y,
                     0, 0, 1, CamPos.z,
                     0, 0, 0, 1 };

	VSOutputTx output;

	//Rotate the position via the view matrix, then translate it
	//via the camera position so the cube is always centered to the camera
	float3 posWS = mul(mul(Position, View), world);

	output.PositionPS = mul(float4(posWS, 1.0f), Proj).xyww;
	output.TexCoord = Position;

	return output;
}

float4 PS_Skybox(VSOutputTx input) : SV_Target 
{
	return DiffuseMap.Sample(MapSampler, input.TexCoord);
}

//
// Shader Groups
//

technique11 Skybox 
{
	pass
	{
		SetVertexShader(CompileShader(vs_5_0, VS_Skybox()));
		SetPixelShader(CompileShader(ps_5_0, PS_Skybox()));
	}
}