/*
* The shader code below is licensed under the Creative Commons Attribution-Share Alike 3.0 
* license. Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
*
* You are free:
*	To Share: to copy, distribute, display, and perform the work
*	To Remix: to make derivative works 
*
*	Under the following conditions:
*		* Attribution - You must attribute the work in the manner specified by the author or
*			licensor (but not in any way that suggests that they endorse you or your use of the work).
*		* Share Alike - If you alter, transform, or build upon this work, you may distribute
*			the resulting work only under the same, similar or a compatible license.
*
*		For any reuse or distribution, you must make clear to others the license terms of this work.
*		The best way to do this is with a link to the above web page (http;//www.tesla-engine.net)
*
*		Any of the above conditions can be waived if you get permission from the copyright holder.
*
*		Apart from the remix rights granted under this license, nothing in this license impairs or
*		restricts the author's moral rights.
*/

//Expand a normal from a sampled normal map
float3 ExpandNormal(float3 normal) 
{
	return (normal * 2) - 1;
}

//Expand a normal from a sampled normal map
float4 ExpandNormal(float4 normal) 
{
	return (normal * 2) - 1;
}

//Encodes a normal using spheremap encoding
float2 SpheremapEncodeNormal(float3 normal)
{
	return normalize(normal.xy) * sqrt(-normal.z * 0.5f + 0.5f);
}

//Decodes a normal using spheremap encoding
float3 SpheremapDecodeNormal(float2 encodedNormal)
{
	float4 nn = float4(encodedNormal, 1, -1);
	float l = dot(nn.xyz, -nn.xyw);
	nn.z = l;
	nn.xy *= sqrt(l);

	return nn.xyz * 2.0f + float3(0, 0, -1);
}

float2 CalculateParallaxOcclusionTextureCoordinates(float2 texCoords, Texture2D normalMap, SamplerState mapSampler, float3 tangentViewDir, float heightScale, int minSamples, int maxSamples)
{
  //Calculate parallax offset vector max length. Equivalent to the tangent of the angle between the viewer position and fragment location.
  float parallaxLimit = -length(tangentViewDir.xy) / tangentViewDir.z;
 
  //Scale the limit
  parallaxLimit *= heightScale;
  
  //Calculate parallax offset vector direction and max offset
  float2 offsetDir = normalize(tangentViewDir.xy);
  float2 maxOffset = offsetDir * parallaxLimit;
  
  //Determine the number of samples
  int numSamples = (int) lerp(maxSamples, minSamples, abs(dot(float3(0, 0, 1), tangentViewDir)));
  
  //Determine view ray step size
  float stepSize = 1.0 / (float) numSamples;
  
  //Calculate tex coord partial derivatives to sample in a loop
  float2 dx = ddx(texCoords);
  float2 dy = ddy(texCoords);
  
  //Starting view ray height and offsets
  int currSample = 0;
  float currRayHeight = 1.0;
  float2 currOffset = float2(0, 0);
  float2 lastOffset = float2(0, 0);
  float lastSampledHeight = 1.0;
  float currSampledHeight = 1.0;
  
  while(currSample < numSamples)
  {
    //Sample normal map's alpha channel which contains the parallax occlusion map
    currSampledHeight = normalMap.SampleGrad(mapSampler, texCoords + currOffset, dx, dy).a;
    
    //Test if the view ray has intersected the surface
    if(currSampledHeight > currRayHeight)
    {
      //Find relative height delta before and after the intersection. This gives a measure of how close the intersection is to the final sample location
      float afterDepth = currSampledHeight - currRayHeight;
      float beforeDepth = (currRayHeight + stepSize) - lastSampledHeight;
      float ratio = afterDepth / (afterDepth + beforeDepth);
      
      //Interpolate between the final two segments to find the true intersection offset
      currOffset = lerp(currOffset, lastOffset, ratio);
      
      //Force the exit of the while loop
      currSample = numSamples + 1;
    }
    else
    {
      //Intersection was not found. Set up the next iteration
      currSample++;
      currRayHeight -= stepSize;
      
      //Save current parameters
      lastOffset = currOffset;
      currOffset += stepSize * maxOffset;
      lastSampledHeight = currSampledHeight;
    }
  }
  
  //Final texture coordinates
  return texCoords + currOffset;
}