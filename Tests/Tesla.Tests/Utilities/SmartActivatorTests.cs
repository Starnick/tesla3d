﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using NUnit.Framework;
using Tesla.Content;

namespace Tesla.Utilities
{
  [TestFixture]
  public class SmartActivatorTests
  {
    [TestCase]
    public void TestGetAssemblyQualifiedName()
    {
      string ass = typeof(Vector3).AssemblyQualifiedName;
      AssertRoundTripType(typeof(Vector3));
      AssertRoundTripType(typeof(DataBuffer<float>));
      AssertRoundTripType(typeof(TypeVersionTable.Entry));
      AssertRoundTripType(typeof(MultiKeyDictionary<float, float, string>.MajorKeyValueEnumerator));
      AssertRoundTripType(typeof(MultiKeyDictionary<int, float, string>.MajorKeyValueEnumerator));
    }

    private void AssertRoundTripType(Type type)
    {
      string typeName = SmartActivator.GetAssemblyQualifiedName(type);
      Type rtt = SmartActivator.GetType(typeName);
      Assert.IsNotNull(rtt);
      Assert.IsTrue(rtt == type);
    }

    [TestCase]
    public void ActivatePublic()
    {
      PublicConstructor ctor = (PublicConstructor) SmartActivator.CreateInstance(typeof(PublicConstructor));

      Assert.IsNotNull(ctor);

      ctor = SmartActivator.CreateInstance<PublicConstructor>();

      Assert.IsNotNull(ctor);

      PublicConstructor ctor2;
      SmartActivator.CreateInstance<PublicConstructor>(out ctor2);

      Assert.IsNotNull(ctor2);
    }

    [TestCase]
    public void ActivateStruct()
    {
      Vector3 v = SmartActivator.CreateInstance<Vector3>();
    }

    [TestCase]
    public void FailInterface()
    {
      Assert.Throws<InvalidOperationException>(() =>
      {
        SmartActivator.CreateInstance<ISavable>();
      });
    }

    [TestCase]
    public void FailNonParameterless()
    {
      Assert.Throws<InvalidOperationException>(() =>
      {
        SmartActivator.CreateInstance<NoParameterlessConstructor>();
      });
    }

    [TestCase]
    public void ActivatePrivate()
    {
      PrivateConstructor ctor = (PrivateConstructor) SmartActivator.CreateInstance(typeof(PrivateConstructor));

      Assert.IsNotNull(ctor);

      ctor = SmartActivator.CreateInstance<PrivateConstructor>();

      Assert.IsNotNull(ctor);

      PublicConstructor ctor2;
      SmartActivator.CreateInstance<PublicConstructor>(out ctor2);

      Assert.IsNotNull(ctor2);
    }

    public class PublicConstructor
    {
      public PublicConstructor() { }
    }

    public class PrivateConstructor
    {
      private PrivateConstructor() { }
    }

    public class NoParameterlessConstructor
    {
      public NoParameterlessConstructor(int x, int y) { }
    }
  }
}
