﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using NUnit.Framework;

#nullable enable

namespace Tesla.Utilities
{
  [TestFixture]
  public class SpanCharBuilderTests
  {
    [TestCase]
    public void TestCtor()
    {
      int capacity = 100;
      SpanCharBuilder builder = new SpanCharBuilder(capacity);
      Assert.IsTrue(builder.Capacity == 100);

      builder.Dispose();
      Assert.IsTrue(builder.IsDisposed);
    }

    [TestCase]
    public void TestAppendSimple()
    {
      SpanCharBuilder builder = new SpanCharBuilder();
      int capacity = builder.Capacity;
      builder.Append(false);
      Assert.IsTrue(builder.Span.Equals(bool.FalseString.AsSpan(), StringComparison.InvariantCulture));
      Assert.IsTrue(builder.Bytes.SequenceEqual(bool.FalseString.AsSpan().AsBytes()));
      Assert.IsTrue(builder.Length == bool.FalseString.Length);

      builder.Clear();
      Assert.IsTrue(builder.Length == 0);
      Assert.IsTrue(builder.Capacity == capacity);

      builder.Capacity = 100;
      Assert.IsTrue(builder.Capacity == 100);

      builder.Append(25);
      Assert.IsTrue(builder.Span.Equals(25.ToString().AsSpan(), StringComparison.InvariantCulture));

      builder.Clear();
      builder.Append('[');
      Assert.IsTrue(builder.ToString() == "[");

      builder.Dispose();
    }

    [TestCase]
    public void TestAppendString()
    {
      string str = "testdata_ ggada gobblygook";
      SpanCharBuilder builder = new SpanCharBuilder();
      builder.Append(str);
      Assert.IsTrue(builder.ToString() == str);

      builder.Clear();
      builder.Append(str.AsSpan());
      Assert.IsTrue(builder.ToString() == str);

      builder.Dispose();
    }

    [TestCase]
    public void TestAppendObject()
    {
      string str = Vector3.One.ToString();
      SpanCharBuilder builder = new SpanCharBuilder();
      builder.Append(Vector3.One as object);
      Assert.IsTrue(builder.ToString() == str);

      builder.Dispose();
    }
  }
}
