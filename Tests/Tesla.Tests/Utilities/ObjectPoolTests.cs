﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using NUnit.Framework;
using System;
using Tesla.Content;
using Tesla.Utilities;

namespace Tesla.Utilities
{
  [TestFixture]
  public class ObjectPoolTests
  {
    [TestCase]
    public void TestSimplePool()
    {
      ObjectPool<Transform> pool = CreateTransformPool(false, 0);
      pool.SweepActiveList = false;

      Transform t1, t2, t3;
      bool success = pool.TryFetch(out t1);
      Assert.IsTrue(success && pool.TotalCount == 1);

      success = pool.TryFetch(out t2);
      Assert.IsTrue(success && pool.TotalCount == 2);

      success = pool.TryFetch(out t3);
      Assert.IsTrue(success && pool.TotalCount == 3);

      pool.Return(t2);
      Assert.IsTrue(pool.TotalCount == 3 && pool.FreeCount == 1);

      pool.Return(t1);
      Assert.IsTrue(pool.TotalCount == 3 && pool.FreeCount == 2);
    }

    [TestCase]
    public void TestSimplePoolAutoSweep()
    {
      ObjectPool<Transform> pool = CreateTransformPool(true, 0);
      pool.SweepActiveList = true;

      Transform t1, t2, t3;
      bool success = pool.TryFetch(out t1);
      Assert.IsTrue(success && pool.ActiveCount == 1);
      t1.SetTranslation(5, 0, 0);

      success = pool.TryFetch(out t2);
      Assert.IsTrue(success && pool.ActiveCount == 2);

      //We got it...set to identity, which means in active. Now we want to get a new instance, so t2 should be recycled and actives should still be 2
      t2.SetIdentity();

      success = pool.TryFetch(out t3);
      Assert.IsTrue(success && pool.ActiveCount == 2 && pool.FreeCount == 0);
      t3.SetTranslation(5, 0, 0);
    }

    [TestCase]
    public void TestSimplePoolForceSweep()
    {
      ObjectPool<Transform> pool = CreateTransformPool(true, 0);
      pool.SweepActiveList = true;

      Transform t1, t2, t3;
      bool success = pool.TryFetch(out t1);
      Assert.IsTrue(success && pool.ActiveCount == 1);
      t1.SetTranslation(5, 0, 0);

      success = pool.TryFetch(out t2);
      Assert.IsTrue(success && pool.ActiveCount == 2);
      t2.SetTranslation(5, 0, 0);

      success = pool.TryFetch(out t3);
      Assert.IsTrue(success && pool.ActiveCount == 3);
      t3.SetTranslation(5, 0, 0);

      t2.SetIdentity();
      pool.ReturnInActives();
      Assert.IsTrue(pool.ActiveCount == 2 && pool.FreeCount == 1);
    }

    [TestCase]
    public void TestMaxCapacityCreation()
    {
      int poolCap = 5;
      ObjectPool<Transform> pool = CreateTransformPool(false, poolCap);

      DoTestMaxCapacity(pool, poolCap);
    }

    [TestCase]
    public void TestMaxCapacityCreationNoSweep()
    {
      int poolCap = 5;
      ObjectPool<Transform> pool = CreateTransformPool(false, poolCap);
      pool.SweepActiveList = false;

      DoTestMaxCapacity(pool, poolCap);
    }

    private void DoTestMaxCapacity(ObjectPool<Transform> pool, int poolCap)
    {
      Transform[] temps = new Transform[poolCap];
      bool success = pool.TryFetch(out temps[0]);
      Assert.IsTrue(success && pool.TotalCount == 1);
      temps[0].SetTranslation(5, 0, 0);

      success = pool.TryFetch(out temps[1]);
      Assert.IsTrue(success && pool.TotalCount == 2);
      temps[1].SetTranslation(5, 0, 0);

      success = pool.TryFetch(out temps[2]);
      Assert.IsTrue(success && pool.TotalCount == 3);
      temps[2].SetTranslation(5, 0, 0);

      success = pool.TryFetch(out temps[3]);
      Assert.IsTrue(success && pool.TotalCount == 4);
      temps[3].SetTranslation(5, 0, 0);

      success = pool.TryFetch(out temps[4]);
      Assert.IsTrue(success && pool.TotalCount == 5);
      temps[4].SetTranslation(5, 0, 0);

      //We should have reached capacity
      Transform temp;
      success = pool.TryFetch(out temp);
      Assert.IsFalse(success);

      pool.Return(temps[4]);

      foreach (Transform t in temps)
      {
        t.SetIdentity();
        pool.Return(t);
      }

      Assert.IsTrue(pool.ActiveCount == 0 && pool.FreeCount == poolCap);
    }

    [TestCase]
    public void TestMaxCapacityChangeWithSweep()
    {
      ObjectPool<Transform> pool = CreateTransformPool(true, 5);
      pool.SweepActiveList = true;

      Transform[] temps = new Transform[5];
      bool success = pool.TryFetch(out temps[0]);
      Assert.IsTrue(success && pool.TotalCount == 1);
      temps[0].SetTranslation(5, 0, 0);

      success = pool.TryFetch(out temps[1]);
      Assert.IsTrue(success && pool.TotalCount == 2);
      temps[1].SetTranslation(5, 0, 0);

      success = pool.TryFetch(out temps[2]);
      Assert.IsTrue(success && pool.TotalCount == 3);
      temps[2].SetTranslation(5, 0, 0);

      success = pool.TryFetch(out temps[3]);
      Assert.IsTrue(success && pool.TotalCount == 4);
      temps[3].SetTranslation(5, 0, 0);

      success = pool.TryFetch(out temps[4]);
      Assert.IsTrue(success && pool.TotalCount == 5);
      temps[4].SetTranslation(5, 0, 0);

      pool.PoolCapacity = 2;
      //All are considered active, so auto sweep wont work
      Assert.IsTrue(pool.ActiveCount == 5 && pool.FreeCount == 0);

      foreach (Transform t in temps)
        t.SetIdentity();

      //All transforms now considered inactive, so next fetch should auto sweep and leave just 2 in free list, but return one so one in each
      Transform temp;
      success = pool.TryFetch(out temp);
      Assert.IsTrue(success && pool.ActiveCount == 1 && pool.FreeCount == 1);
    }

    [TestCase]
    public void TestMaxCapacityChange()
    {
      ObjectPool<Transform> pool = CreateTransformPool(false, 5);

      Transform[] temps = new Transform[5];
      bool success = pool.TryFetch(out temps[0]);
      Assert.IsTrue(success && pool.TotalCount == 1);

      success = pool.TryFetch(out temps[1]);
      Assert.IsTrue(success && pool.TotalCount == 2);

      success = pool.TryFetch(out temps[2]);
      Assert.IsTrue(success && pool.TotalCount == 3);

      success = pool.TryFetch(out temps[3]);
      Assert.IsTrue(success && pool.TotalCount == 4);

      success = pool.TryFetch(out temps[4]);
      Assert.IsTrue(success && pool.TotalCount == 5);

      pool.PoolCapacity = 2;
      //All are considered active, so auto sweep wont work
      Assert.IsTrue(pool.ActiveCount == 5 && pool.FreeCount == 0);

      Transform temp;
      success = pool.TryFetch(out temp);
      Assert.IsFalse(success);

      //Return...but exceeds capacity so the returned object should be freed
      pool.Return(temps[0]);
      Assert.IsTrue(pool.ActiveCount == 4 && pool.FreeCount == 0);

      pool.Return(temps[1]);
      Assert.IsTrue(pool.ActiveCount == 3 && pool.FreeCount == 0);

      pool.Return(temps[2]);
      Assert.IsTrue(pool.ActiveCount == 2 && pool.FreeCount == 0);

      pool.Return(temps[3]);
      Assert.IsTrue(pool.ActiveCount == 1 && pool.FreeCount == 1);

      pool.Return(temps[4]);
      Assert.IsTrue(pool.ActiveCount == 0 && pool.FreeCount == 2);
    }

    private static ObjectPool<Transform> CreateTransformPool(bool useIdentityAsInactive, int poolCap = 0)
    {
      SimplePoolManager<Transform> poolBoy;

      if (useIdentityAsInactive)
      {
        poolBoy = new SimplePoolManager<Transform>(
            delegate ()
            {
              return new Transform();
            },
            delegate (Transform isActive)
            {
              return !isActive.Matrix.IsIdentity;
            },
            null);
      }
      else
      {
        poolBoy = new SimplePoolManager<Transform>(
            delegate ()
            {
              return new Transform();
            });
      }

      return new ObjectPool<Transform>(poolBoy, poolCap, true, new ReferenceEqualityComparer<Transform>());
    }
  }
}
