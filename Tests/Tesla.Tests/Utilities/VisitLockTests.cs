﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using NUnit.Framework;
using System;
using System.Threading;
using System.Threading.Tasks;
using Tesla.Utilities;

namespace Tesla.Utilities
{
  [TestFixture]
  public class VisitLockTests
  {
    [TestCase]
    public void TestVisit()
    {
      VisitLock vLock = new VisitLock();
      int sequenceNum = 1;

      Task[] tasks = new Task[4];
      Random rand = new Random();
      int startWait;
      int numIterations = 5;

      while (numIterations > 0)
      {
        startWait = rand.Next(1, 5);

        tasks[0] = new Task(() =>
        {
          Thread.Sleep(startWait);
          int count = 2;

          while (count >= 0)
          {
            if (vLock.IsFirstVisit(sequenceNum))
            {
              Console.WriteLine("Task #1 won - sequence: {0}", sequenceNum.ToString());
              break;
            }
            else
            {
              count--;
              Console.WriteLine("Task #1 lost - sequence: {0}, retrying...", sequenceNum.ToString());
              Thread.Sleep(1);
            }
          }
        });

        startWait = rand.Next(1, 5);

        tasks[1] = new Task(() =>
        {
          Thread.Sleep(startWait);
          int count = 2;

          while (count >= 0)
          {
            if (vLock.IsFirstVisit(sequenceNum))
            {
              Console.WriteLine("Task #2 won - sequence: {0}", sequenceNum.ToString());
              break;
            }
            else
            {
              count--;
              Console.WriteLine("Task #2 lost - sequence: {0}, retrying...", sequenceNum.ToString());
              Thread.Sleep(1);
            }
          }
        });

        startWait = rand.Next(1, 5);

        tasks[2] = new Task(() =>
        {
          Thread.Sleep(startWait);
          int count = 2;

          while (count >= 0)
          {
            if (vLock.IsFirstVisit(sequenceNum))
            {
              Console.WriteLine("Task #3 won - sequence: {0}", sequenceNum.ToString());
              break;
            }
            else
            {
              count--;
              Console.WriteLine("Task #3 lost - sequence: {0}, retrying...", sequenceNum.ToString());
              Thread.Sleep(1);
            }
          }
        });

        startWait = rand.Next(1, 5);

        tasks[3] = new Task(() =>
        {
          Thread.Sleep(startWait);
          int count = 2;

          while (count >= 0)
          {
            if (vLock.IsFirstVisit(sequenceNum))
            {
              Console.WriteLine("Task #4 won - sequence: {0}", sequenceNum.ToString());
              break;
            }
            else
            {
              count--;
              Console.WriteLine("Task #4 lost - sequence: {0}, retrying...", sequenceNum.ToString());
              Thread.Sleep(1);
            }
          }
        });

        foreach (Task t in tasks)
          t.Start();

        Task.WaitAll(tasks);

        Console.WriteLine();

        numIterations--;
        sequenceNum++;
      }
    }
  }
}
