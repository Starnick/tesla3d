﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using NUnit.Framework;
using Tesla;

#nullable enable

namespace Tesla.Collections
{
  [TestFixture]
  public class FeatureIndexTests
  {
    [TestCase]
    public void CreateFeatureStringIndex()
    {
      FeatureStringIndex indexMap = new FeatureStringIndex();
      int emissiveIndex = indexMap.Add("Emissive");
      int glowIndex = indexMap.Add("Glow");
      int normalIndex = indexMap.Add("Normal");
      int colorIndex = indexMap.Add("Color");

      Assert.IsTrue(emissiveIndex == 0);
      Assert.IsTrue(glowIndex == 1);
      Assert.IsTrue(normalIndex == 2);
      Assert.IsTrue(colorIndex == 3);

      Assert.IsTrue(indexMap.TryGetFeature(normalIndex, out string? normalFeature));
      Assert.IsNotNull(normalFeature);

      Assert.IsTrue(indexMap.IndexOf("Glow") == glowIndex);
      Assert.IsTrue(indexMap[2] == "Normal");
      Assert.IsTrue(indexMap.IndexOf("Unknown") == -1);
      Assert.IsTrue(indexMap.Count == 4);

      foreach (FeatureIndex<string>.Entry entry in indexMap)
      {
        switch (entry.Index)
        {
          case 0:
            Assert.IsTrue(entry.Feature == "Emissive");
            break;
          case 1:
            Assert.IsTrue(entry.Feature == "Glow");
            break;
          case 2:
            Assert.IsTrue(entry.Feature == "Normal");
            break;
          case 3:
            Assert.IsTrue(entry.Feature == "Color");
            break;
          default:
            Assert.IsTrue(false);
            break;
        }
      }
    }

    [TestCase]
    public void CreateBitArrays()
    {
      FeatureStringIndex indexMap = new FeatureStringIndex();
      int emissiveIndex = indexMap.Add("Emissive");
      int glowIndex = indexMap.Add("Glow");
      int normalIndex = indexMap.Add("Normal");
      int colorIndex = indexMap.Add("Color");

      BitArray bits0 = indexMap.CreateBitArray(new List<int>(new[] { 1, 2 }));
      BitArray bits1 = indexMap.CreateBitArray(new[] { 1, 2 }.AsSpan());
      BitArray bits2 = indexMap.CreateBitArray(new[] { "Glow", "Normal" });

      BitArray[] bits = new[] { bits0, bits1, bits2 };

      foreach (BitArray b in bits)
      {
        Assert.IsTrue(b.Length == indexMap.Count);
        Assert.IsFalse(b.Get(emissiveIndex));
        Assert.IsTrue(b.Get(glowIndex));
        Assert.IsTrue(b.Get(normalIndex));
        Assert.IsFalse(b.Get(colorIndex));
      }
    }

    [TestCase]
    public void CreateBitVectors()
    {
      FeatureStringIndex indexMap = new FeatureStringIndex();
      int emissiveIndex = indexMap.Add("Emissive");
      int glowIndex = indexMap.Add("Glow");
      int normalIndex = indexMap.Add("Normal");
      int colorIndex = indexMap.Add("Color");

      BitVector32 bits0 = indexMap.CreateBitVector32(new List<int>(new[] { 1, 2 }));
      BitVector32 bits1 = indexMap.CreateBitVector32(new[] { 1, 2 }.AsSpan());
      BitVector32 bits2 = indexMap.CreateBitVector32(new[] { "Glow", "Normal" });

      BitVector32[] bits = new[] { bits0, bits1, bits2 };
      foreach (BitVector32 b in bits)
      {
        Assert.IsTrue(b.Data == bits0.Data);
        Assert.IsFalse(b[1 << emissiveIndex]);
        Assert.IsTrue(b[1 << glowIndex]);
        Assert.IsTrue(b[1 << normalIndex]);
        Assert.IsFalse(b[1 << colorIndex]);
      }
    }
  }
}
