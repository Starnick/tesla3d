﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Tesla.Utilities;

namespace Tesla.Content
{
  [TestFixture]
  public class TypeVersionTableTests
  {
    [TestCase]
    public void TestAddRuntime()
    {
      TypeVersionTable table = new TypeVersionTable();

      // Adding the subclass should have added the hierarchy with the base class having the lower ID
      int index = table.AddOrGetFromRuntime(typeof(SavableSub));
      Assert.IsTrue(index == 1);
      Assert.IsTrue(table.Count == 2);

      Assert.IsTrue(table.AddOrGetFromRuntime(typeof(SavableBase)) == 0);

      var vEntry = table.GetVersion(typeof(Vector3));
      Assert.IsNull(vEntry);

      table.AddOrGetFromRuntime(typeof(Vector3));
      vEntry = table.GetVersion(typeof(Vector3));
      Assert.IsNotNull(vEntry);
      Assert.IsTrue(vEntry.Value.ID == 2);
      Assert.IsFalse(vEntry.Value.IsSavable);

      vEntry = table.GetVersion(typeof(SavableSub));
      Assert.IsNotNull(vEntry);
      Assert.IsTrue(vEntry.Value.ID == 1);
      Assert.IsTrue(vEntry.Value.IsSavable);

      table.Clear();
      Assert.IsTrue(table.Count == 0);
    }

    [TestCase]
    public void TestAddHierarchy()
    {
      TypeVersionTable table = new TypeVersionTable();

      int index = table.AddOrGetFromRuntime(typeof(SavableSubSub));
      Assert.IsTrue(index == 2);

      index = table.AddOrGetFromRuntime(typeof(SavableSub));
      Assert.IsTrue(index == 1);

      index = table.AddOrGetFromRuntime(typeof(SavableBase));
      Assert.IsTrue(index == 0);

      index = table.AddOrGetFromRuntime(typeof(SavableSub2));
      Assert.IsTrue(index == 3);
    }

    [TestCase]
    public void TestWhiteList()
    {
      TypeVersionTable.EnableWhiteListing = true;

      TypeVersionTable table = new TypeVersionTable();

      // Test 1 - Should throw since nothing is whitelisted
      Assert.Throws<InvalidOperationException>(() =>
      {
        table.AddFromPersistence(SmartActivator.GetAssemblyQualifiedName(typeof(SavableBase)), 1);
      });

      // Whitelist the subclass, it will automatically subclass the type hierarchy
      TypeVersionTable.AddToWhiteList(typeof(SavableSub));

      // Test 2 - now should be able to add the base class
      table.AddFromPersistence(SmartActivator.GetAssemblyQualifiedName(typeof(SavableBase)), 1);

      TypeVersionTable.EnableWhiteListing = false;
    }

    [TestCase]
    public void TestUnresolvedType()
    {
      TypeVersionTable table = new TypeVersionTable();
      int index = table.AddFromPersistence("Tesla.SubclassNoLongerExists, Tesla", 5);
      Assert.IsTrue(index == 0);

      index = table.AddFromPersistence(SmartActivator.GetAssemblyQualifiedName(typeof(Vector3)), 1);
      Assert.IsTrue(index == 1);

      TypeVersionTable.Entry vEntry = table[0];
      Assert.IsFalse(vEntry.IsResolved);
      Assert.IsNull(vEntry.RuntimeType);

      vEntry = table[1];
      Assert.IsTrue(vEntry.IsResolved);
      Assert.IsNotNull(vEntry.RuntimeType);

      Assert.Throws<ArgumentOutOfRangeException>(() =>
      {
        table.AddFromPersistence(SmartActivator.GetAssemblyQualifiedName(typeof(Vector2)), 2);
      });
    }
  }

  [SavableVersion(2)]
  internal class SavableBase : ISavable
  {
    public virtual void Read(ISavableReader input)
    {
    }

    public virtual void Write(ISavableWriter output)
    {
    }
  }

  [SavableVersion(3)]
  internal class SavableSub : SavableBase
  {
    public override void Read(ISavableReader input)
    {
    }

    public override void Write(ISavableWriter output)
    {
    }
  }

  [SavableVersion(4)]
  internal class SavableSubSub : SavableSub
  {
    public override void Read(ISavableReader input)
    {
    }

    public override void Write(ISavableWriter output)
    {
    }
  }

  [SavableVersion(6)]
  internal class SavableSub2 : SavableBase
  {
    public override void Read(ISavableReader input)
    {
    }

    public override void Write(ISavableWriter output)
    {
    }
  }
}
