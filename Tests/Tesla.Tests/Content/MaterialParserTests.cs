﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.IO;
using NUnit.Framework;
using Tesla.Graphics;

namespace Tesla.Content
{
  [TestFixture]
  public sealed class MaterialParserTests
  {
    private ContentManager m_content;

    [OneTimeSetUp]
    public void Setup()
    {
      //Maybe in the future Mock out the entire render system
      Engine.Initialize(Platforms.CreateOnlyGraphics());

      m_content = new ContentManager(new FileResourceRepository("Data"));
    }

    [OneTimeTearDown]
    public void Teardown()
    {
      m_content.Dispose();
      Engine.Destroy();
    }

    [TestCase]
    public void ParseAnonymousMaterialTest()
    {
      MaterialParser parser = new MaterialParser();

      String scriptText;

      using (StreamReader reader = new StreamReader(m_content.OpenStream("Materials/PirateShip.temd")))
      {
        scriptText = reader.ReadToEnd();
      }

      MaterialDefinition matDef = parser.ParseDefinition(scriptText, "PirateShip", m_content);
      Assert.IsNotNull(matDef);
      Assert.IsTrue(matDef.Count == 3);

      foreach (KeyValuePair<RenderBucketID, Material> kv in matDef)
      {
        switch (kv.Value.Name)
        {
          case "PirateShip_Opaque":
          case "PirateShip_Transparent":
          case "PirateShip_Ortho":
            continue;
          default:
            Assert.IsTrue(false, "Names do not match");
            break;
        }
      }
    }
  }
}
