﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Text.Json;
using NUnit.Framework;
using Tesla.Direct3D11.Graphics;
using Tesla.Graphics;

namespace Tesla.Content
{
  [TestFixture]
  public class ImageExporterTests : BaseIOTestFixture
  {
    [TestCase]
    public void TestExportTexture_Teximp()
    {
      using IRenderSystem renderSystem = new D3D11RenderSystem();

      TextureImporterParameters importParams = new TextureImporterParameters();
      importParams.GenerateMipMaps = true;

      using Texture2D tex = ReadImage(renderSystem, importParams, "Textures/Meriwether.png");

      TeximpTexture2DExporter exporter = new JpgTextureExporter();
      using (Stream output = File.OpenWrite(GetOutputFile("Meriwether_ConvertedJPG.jpg")))
        exporter.Save(output, tex);

      exporter = new PngTextureExporter();
      using (Stream output = File.OpenWrite(GetOutputFile("Meriwether_ConvertedPNG.png")))
        exporter.Save(output, tex);
    }

    private Texture2D ReadImage(IRenderSystem renderSystem, TextureImporterParameters importParams, String inputFilename)
    {
      using (Stream input = File.OpenRead(GetInputFile(inputFilename)))
      {
        Texture2D tex = TeximpTextureImporter.Load(input, renderSystem, importParams);
        Assert.IsNotNull(tex);

        return tex;
      }
    }

    [TestCase]
    public void TestExport_TextureAtlas()
    {
      bool tearDownEngine = false;

      if (!Engine.IsInitialized)
      {
        Engine.Initialize(Platforms.CreateOnlyGraphics());
        tearDownEngine = true;
      }

      try
      {

        using Texture2D tex = new Texture2D(IRenderSystem.Current, 1, 1);
        tex.Name = "DefaultSkin_TextureAtlas";

        TextureAtlas atlas = new TextureAtlas(tex);
        atlas.Name = "DefaultSkin_TextureAtlas";
        atlas.Add("blank", new Rectangle(0, 237, 1, 1));
        atlas.Add("button_default", new Rectangle(0, 0, 157, 35));
        atlas.Add("button_hot", new Rectangle(0, 41, 157, 35));
        atlas.Add("button_down", new Rectangle(0, 82, 157, 35));
        atlas.Add("input_default", new Rectangle(0, 123, 222, 35));
        atlas.Add("input_focused", new Rectangle(0, 164, 222, 35));
        atlas.Add("window", new Rectangle(163, 0, 64, 64));
        atlas.Add("frame", new Rectangle(233, 0, 64, 64));
        atlas.Add("checkbox_default", new Rectangle(0, 205, 26, 26));
        atlas.Add("checkbox_hot", new Rectangle(163, 70, 26, 26));
        atlas.Add("checkbox_down", new Rectangle(32, 205, 26, 26));
        atlas.Add("checkbox_checked", new Rectangle(195, 70, 26, 26));
        atlas.Add("checkbox_checked_hot", new Rectangle(64, 205, 26, 26));
        atlas.Add("combo_default", new Rectangle(227, 70, 188, 35));
        atlas.Add("combo_hot", new Rectangle(96, 205, 188, 35));
        atlas.Add("combo_down", new Rectangle(303, 0, 188, 35));
        atlas.Add("combo_button_default", new Rectangle(228, 111, 32, 35));
        atlas.Add("combo_button_hot", new Rectangle(266, 111, 32, 35));
        atlas.Add("combo_button_down", new Rectangle(228, 152, 32, 35));
        atlas.Add("vscroll_track", new Rectangle(421, 41, 14, 141));
        atlas.Add("vscroll_button", new Rectangle(304, 111, 14, 58));
        atlas.Add("vscroll_button_hot", new Rectangle(324, 111, 14, 58));
        atlas.Add("vscroll_button_down", new Rectangle(344, 111, 14, 58));
        atlas.Add("vscrollUp_default", new Rectangle(303, 41, 14, 20));
        atlas.Add("vscrollUp_hot", new Rectangle(323, 41, 14, 20));
        atlas.Add("vscrollUp_down", new Rectangle(343, 41, 14, 20));
        atlas.Add("hscroll_track", new Rectangle(290, 188, 141, 14));
        atlas.Add("hscroll_button", new Rectangle(163, 102, 58, 14));
        atlas.Add("hscroll_button_hot", new Rectangle(441, 41, 58, 14));
        atlas.Add("hscroll_button_down", new Rectangle(290, 208, 58, 14));
        atlas.Add("hscrollUp_default", new Rectangle(363, 41, 20, 14));
        atlas.Add("hscrollUp_hot", new Rectangle(266, 152, 20, 14));
        atlas.Add("hscrollUp_down", new Rectangle(389, 41, 20, 14));

        JsonResourceExporter exporter = new JsonResourceExporter();
        IResourceFile file = FileResourceFile.CreateOrphan(GetOutputFile("DefaultSkin_TextureAtlas.json"));
        IExternalReferenceHandler handler = ExternalReferenceHandler.CreateJsonDefaultHandler(file);
        handler.RegisterWriter(TeximpTexture2DExternalWriter.CreatePngWriter());
        exporter.Save(handler, atlas, SavableWriteFlags.SharedAsExternal);

        JsonResourceImporter importer = new JsonResourceImporter();

        TextureAtlas atlas2 = importer.Load(file, new ContentManager(file.Repository), ImporterParameters.None) as TextureAtlas;
        Debug.Assert(atlas2 is not null);
        Debug.Assert(atlas.Count == atlas2.Count);
      }
      finally
      {
        if (tearDownEngine)
          Engine.Destroy();
      }
    }
  }
}
