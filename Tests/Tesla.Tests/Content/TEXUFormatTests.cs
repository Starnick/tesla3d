﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System.Collections.Generic;
using System.IO;
using NUnit.Framework;
using Tesla.Direct3D11.Graphics;
using Tesla.Graphics;

namespace Tesla.Content
{
  [TestFixture]
  public class TEXUFormatTests
  {
    private D3D11RenderSystem m_renderSystem;

    [OneTimeSetUp]
    public void Setup()
    {
      m_renderSystem = new D3D11RenderSystem();
    }

    [OneTimeTearDown]
    public void Teardown()
    {
      m_renderSystem.Dispose();
    }

    [TestCase]
    public void Test1DTexture()
    {
      DataBuffer<Color> testData0 = new DataBuffer<Color>(new Color[] { new Color(5, 10, 15), new Color(10, 20, 25) });
      DataBuffer<Color> testData1 = new DataBuffer<Color>(new Color[] { new Color(20, 20, 20) });

      TextureData texData = new TextureData();
      texData.Width = 2;
      texData.Height = 1;
      texData.Depth = 1;
      texData.Format = SurfaceFormat.Color;
      texData.TextureType = TextureType.Texture1D;
      texData.MipChains = new List<TextureData.MipChain>();
      texData.MipChains.Add(new TextureData.MipChain(new TextureData.MipSurface[] { new TextureData.MipSurface(2, testData0), new TextureData.MipSurface(1, testData1) }));

      Assert.IsTrue(texData.Validate());

      //Test writing out compressed
      MemoryStream stream = new MemoryStream();
      TextureData.Write(texData, stream, DataCompressionMode.Gzip);

      stream.Position = 0;

      TextureData texDataRoundTrip = TextureData.Read(stream);
      Assert.IsTrue(AreEqual(texData, texDataRoundTrip));

      //Test writing out non-compressed
      stream.Position = 0;
      TextureData.Write(texData, stream, DataCompressionMode.None);

      stream.Position = 0;

      texDataRoundTrip = TextureData.Read(stream);
      Assert.IsTrue(AreEqual(texData, texDataRoundTrip));

      //Test creating a texture with it
      using (Texture1D gpuTex = Texture.From(texDataRoundTrip, m_renderSystem) as Texture1D)
      {
        Assert.IsNotNull(gpuTex);
        Assert.IsTrue(gpuTex.MipCount == texData.MipChains[0].Count);
        Assert.IsTrue(gpuTex.Width == texData.Width);
        Assert.IsTrue(gpuTex.Format == texData.Format);

        //And test retrieving data from the texture
        TextureData dataFromGpuTex = TextureData.From(gpuTex);
        Assert.IsTrue(AreEqual(texData, dataFromGpuTex));
      }
    }

    [TestCase]
    public void Test1DArrayTexture()
    {
      DataBuffer<Color> testData0 = new DataBuffer<Color>(new Color[] { new Color(5, 10, 15), new Color(10, 20, 25) });
      DataBuffer<Color> testData1 = new DataBuffer<Color>(new Color[] { new Color(20, 20, 20) });

      TextureData texData = new TextureData();
      texData.Width = 2;
      texData.Height = 1;
      texData.Depth = 1;
      texData.Format = SurfaceFormat.Color;
      texData.TextureType = TextureType.Texture1DArray;
      texData.MipChains = new List<TextureData.MipChain>();
      texData.MipChains.Add(new TextureData.MipChain(new TextureData.MipSurface[] { new TextureData.MipSurface(2, testData0), new TextureData.MipSurface(1, testData1) }));
      texData.MipChains.Add(new TextureData.MipChain(new TextureData.MipSurface[] { new TextureData.MipSurface(2, testData0), new TextureData.MipSurface(1, testData1) }));
      texData.MipChains.Add(new TextureData.MipChain(new TextureData.MipSurface[] { new TextureData.MipSurface(2, testData0), new TextureData.MipSurface(1, testData1) }));

      Assert.IsTrue(texData.Validate());

      //Test writing out compressed
      MemoryStream stream = new MemoryStream();
      TextureData.Write(texData, stream, DataCompressionMode.Gzip);

      stream.Position = 0;

      TextureData texDataRoundTrip = TextureData.Read(stream);
      Assert.IsTrue(AreEqual(texData, texDataRoundTrip));

      //Test writing out non-compressed
      stream.Position = 0;
      TextureData.Write(texData, stream, DataCompressionMode.None);

      stream.Position = 0;

      texDataRoundTrip = TextureData.Read(stream);
      Assert.IsTrue(AreEqual(texData, texDataRoundTrip));

      //Test creating a texture with it
      using (Texture1DArray gpuTex = Texture.From(texDataRoundTrip, m_renderSystem) as Texture1DArray)
      {
        Assert.IsNotNull(gpuTex);
        Assert.IsTrue(gpuTex.MipCount == texData.MipChains[0].Count);
        Assert.IsTrue(gpuTex.ArrayCount == texData.MipChains.Count);
        Assert.IsTrue(gpuTex.Width == texData.Width);
        Assert.IsTrue(gpuTex.Format == texData.Format);

        //And test retrieving data from the texture
        TextureData dataFromGpuTex = TextureData.From(gpuTex);
        Assert.IsTrue(AreEqual(texData, dataFromGpuTex));
      }
    }

    [TestCase]
    public void Test2DTexture()
    {
      DataBuffer<Color> testData0 = new DataBuffer<Color>(new Color[] { new Color(5, 10, 15), new Color(10, 20, 25), new Color(30, 12, 250), new Color(40, 60, 75) });
      DataBuffer<Color> testData1 = new DataBuffer<Color>(new Color[] { new Color(20, 20, 20) });

      TextureData texData = new TextureData();
      texData.Width = 2;
      texData.Height = 2;
      texData.Depth = 1;
      texData.Format = SurfaceFormat.Color;
      texData.TextureType = TextureType.Texture2D;
      texData.MipChains = new List<TextureData.MipChain>();
      texData.MipChains.Add(new TextureData.MipChain(new TextureData.MipSurface[] { new TextureData.MipSurface(2, 2, testData0), new TextureData.MipSurface(1, 1, testData1) }));

      Assert.IsTrue(texData.Validate());

      //Test writing out compressed
      MemoryStream stream = new MemoryStream();
      TextureData.Write(texData, stream, DataCompressionMode.Gzip);

      stream.Position = 0;

      TextureData texDataRoundTrip = TextureData.Read(stream);
      Assert.IsTrue(AreEqual(texData, texDataRoundTrip));

      //Test writing out non-compressed
      stream.Position = 0;
      TextureData.Write(texData, stream, DataCompressionMode.None);

      stream.Position = 0;

      texDataRoundTrip = TextureData.Read(stream);
      Assert.IsTrue(AreEqual(texData, texDataRoundTrip));

      //Test creating a texture with it
      using (Texture2D gpuTex = Texture.From(texDataRoundTrip, m_renderSystem) as Texture2D)
      {
        Assert.IsNotNull(gpuTex);
        Assert.IsTrue(gpuTex.MipCount == texData.MipChains[0].Count);
        Assert.IsTrue(gpuTex.Width == texData.Width);
        Assert.IsTrue(gpuTex.Height == texData.Height);
        Assert.IsTrue(gpuTex.Format == texData.Format);

        //And test retrieving data from the texture
        TextureData dataFromGpuTex = TextureData.From(gpuTex);
        Assert.IsTrue(AreEqual(texData, dataFromGpuTex));
      }
    }

    [TestCase]
    public void Test2DArrayTexture()
    {
      DataBuffer<Color> testData0 = new DataBuffer<Color>(new Color[] { new Color(5, 10, 15), new Color(10, 20, 25), new Color(30, 12, 250), new Color(40, 60, 75) });
      DataBuffer<Color> testData1 = new DataBuffer<Color>(new Color[] { new Color(20, 20, 20) });

      TextureData texData = new TextureData();
      texData.Width = 2;
      texData.Height = 2;
      texData.Depth = 1;
      texData.Format = SurfaceFormat.Color;
      texData.TextureType = TextureType.Texture2DArray;
      texData.MipChains = new List<TextureData.MipChain>();
      texData.MipChains.Add(new TextureData.MipChain(new TextureData.MipSurface[] { new TextureData.MipSurface(2, 2, testData0), new TextureData.MipSurface(1, 1, testData1) }));
      texData.MipChains.Add(new TextureData.MipChain(new TextureData.MipSurface[] { new TextureData.MipSurface(2, 2, testData0), new TextureData.MipSurface(1, 1, testData1) }));
      texData.MipChains.Add(new TextureData.MipChain(new TextureData.MipSurface[] { new TextureData.MipSurface(2, 2, testData0), new TextureData.MipSurface(1, 1, testData1) }));

      Assert.IsTrue(texData.Validate());

      //Test writing out compressed
      MemoryStream stream = new MemoryStream();
      TextureData.Write(texData, stream, DataCompressionMode.Gzip);

      stream.Position = 0;

      TextureData texDataRoundTrip = TextureData.Read(stream);
      Assert.IsTrue(AreEqual(texData, texDataRoundTrip));

      //Test writing out non-compressed
      stream.Position = 0;
      TextureData.Write(texData, stream, DataCompressionMode.None);

      stream.Position = 0;

      texDataRoundTrip = TextureData.Read(stream);
      Assert.IsTrue(AreEqual(texData, texDataRoundTrip));

      //Test creating a texture with it
      using (Texture2DArray gpuTex = Texture.From(texDataRoundTrip, m_renderSystem) as Texture2DArray)
      {
        Assert.IsNotNull(gpuTex);
        Assert.IsTrue(gpuTex.MipCount == texData.MipChains[0].Count);
        Assert.IsTrue(gpuTex.ArrayCount == texData.MipChains.Count);
        Assert.IsTrue(gpuTex.Width == texData.Width);
        Assert.IsTrue(gpuTex.Height == texData.Height);
        Assert.IsTrue(gpuTex.Format == texData.Format);

        //And test retrieving data from the texture
        TextureData dataFromGpuTex = TextureData.From(gpuTex);
        Assert.IsTrue(AreEqual(texData, dataFromGpuTex));
      }
    }

    [TestCase]
    public void Test3DTexture()
    {
      DataBuffer<Color> testData0 = new DataBuffer<Color>(new Color[] { new Color(5, 10, 15), new Color(10, 20, 25), new Color(30, 12, 250), new Color(40, 60, 75),
                                                                          new Color(5, 10, 15), new Color(10, 20, 25), new Color(30, 12, 250), new Color(40, 60, 75) });
      DataBuffer<Color> testData1 = new DataBuffer<Color>(new Color[] { new Color(20, 20, 20) });

      TextureData texData = new TextureData();
      texData.Width = 2;
      texData.Height = 2;
      texData.Depth = 2;
      texData.Format = SurfaceFormat.Color;
      texData.TextureType = TextureType.Texture3D;
      texData.MipChains = new List<TextureData.MipChain>();
      texData.MipChains.Add(new TextureData.MipChain(new TextureData.MipSurface[] { new TextureData.MipSurface(2, 2, 2, testData0), new TextureData.MipSurface(1, 1, 1, testData1) }));

      Assert.IsTrue(texData.Validate());

      //Test writing out compressed
      MemoryStream stream = new MemoryStream();
      TextureData.Write(texData, stream, DataCompressionMode.Gzip);

      stream.Position = 0;

      TextureData texDataRoundTrip = TextureData.Read(stream);
      Assert.IsTrue(AreEqual(texData, texDataRoundTrip));

      //Test writing out non-compressed
      stream.Position = 0;
      TextureData.Write(texData, stream, DataCompressionMode.None);

      stream.Position = 0;

      texDataRoundTrip = TextureData.Read(stream);
      Assert.IsTrue(AreEqual(texData, texDataRoundTrip));

      //Test creating a texture with it
      using (Texture3D gpuTex = Texture.From(texDataRoundTrip, m_renderSystem) as Texture3D)
      {
        Assert.IsNotNull(gpuTex);
        Assert.IsTrue(gpuTex.MipCount == texData.MipChains[0].Count);
        Assert.IsTrue(gpuTex.Width == texData.Width);
        Assert.IsTrue(gpuTex.Height == texData.Height);
        Assert.IsTrue(gpuTex.Depth == texData.Depth);
        Assert.IsTrue(gpuTex.Format == texData.Format);

        //And test retrieving data from the texture
        TextureData dataFromGpuTex = TextureData.From(gpuTex);
        Assert.IsTrue(AreEqual(texData, dataFromGpuTex));
      }
    }

    [TestCase]
    public void TestCubeTexture()
    {
      DataBuffer<Color> testData0 = new DataBuffer<Color>(new Color[] { new Color(5, 10, 15), new Color(10, 20, 25), new Color(30, 12, 250), new Color(40, 60, 75) });
      DataBuffer<Color> testData1 = new DataBuffer<Color>(new Color[] { new Color(20, 20, 20) });

      TextureData texData = new TextureData();
      texData.Width = 2;
      texData.Height = 2;
      texData.Depth = 1;
      texData.Format = SurfaceFormat.Color;
      texData.TextureType = TextureType.TextureCube;
      texData.MipChains = new List<TextureData.MipChain>();
      texData.MipChains.Add(new TextureData.MipChain(new TextureData.MipSurface[] { new TextureData.MipSurface(2, 2, testData0), new TextureData.MipSurface(1, 1, testData1) }));
      texData.MipChains.Add(new TextureData.MipChain(new TextureData.MipSurface[] { new TextureData.MipSurface(2, 2, testData0), new TextureData.MipSurface(1, 1, testData1) }));
      texData.MipChains.Add(new TextureData.MipChain(new TextureData.MipSurface[] { new TextureData.MipSurface(2, 2, testData0), new TextureData.MipSurface(1, 1, testData1) }));
      texData.MipChains.Add(new TextureData.MipChain(new TextureData.MipSurface[] { new TextureData.MipSurface(2, 2, testData0), new TextureData.MipSurface(1, 1, testData1) }));
      texData.MipChains.Add(new TextureData.MipChain(new TextureData.MipSurface[] { new TextureData.MipSurface(2, 2, testData0), new TextureData.MipSurface(1, 1, testData1) }));
      texData.MipChains.Add(new TextureData.MipChain(new TextureData.MipSurface[] { new TextureData.MipSurface(2, 2, testData0), new TextureData.MipSurface(1, 1, testData1) }));

      Assert.IsTrue(texData.Validate());

      //Test writing out compressed
      MemoryStream stream = new MemoryStream();
      TextureData.Write(texData, stream, DataCompressionMode.Gzip);

      stream.Position = 0;

      TextureData texDataRoundTrip = TextureData.Read(stream);
      Assert.IsTrue(AreEqual(texData, texDataRoundTrip));

      //Test writing out non-compressed
      stream.Position = 0;
      TextureData.Write(texData, stream, DataCompressionMode.None);

      stream.Position = 0;

      texDataRoundTrip = TextureData.Read(stream);
      Assert.IsTrue(AreEqual(texData, texDataRoundTrip));

      //Test creating a texture with it
      using (TextureCube gpuTex = Texture.From(texDataRoundTrip, m_renderSystem) as TextureCube)
      {
        Assert.IsNotNull(gpuTex);
        Assert.IsTrue(gpuTex.MipCount == texData.MipChains[0].Count);
        Assert.IsTrue(gpuTex.Size == texData.Width);
        Assert.IsTrue(gpuTex.Format == texData.Format);

        //And test retrieving data from the texture
        TextureData dataFromGpuTex = TextureData.From(gpuTex);
        Assert.IsTrue(AreEqual(texData, dataFromGpuTex));
      }
    }

    private bool AreEqual(TextureData tex0, TextureData tex1)
    {
      if (tex0.Width != tex1.Width || tex0.Height != tex1.Height || tex0.Depth != tex1.Depth || tex0.Format != tex1.Format || tex0.TextureType != tex1.TextureType)
        return false;

      if (tex0.MipChains == null || tex1.MipChains == null || tex0.MipChains.Count != tex1.MipChains.Count)
        return false;

      for (int i = 0; i < tex0.MipChains.Count; i++)
      {
        if (!AreEqual(tex0.MipChains[i], tex1.MipChains[i]))
          return false;
      }

      return true;
    }

    private bool AreEqual(TextureData.MipChain tex0, TextureData.MipChain tex1)
    {
      if (tex0 == null || tex1 == null || tex0.Count != tex1.Count)
        return false;

      for (int i = 0; i < tex0.Count; i++)
      {
        if (!AreEqual(tex0[i], tex1[i]))
          return false;
      }

      return true;
    }

    private bool AreEqual(TextureData.MipSurface tex0, TextureData.MipSurface tex1)
    {
      if (tex0.Width != tex1.Width || tex0.Height != tex1.Height || tex0.Depth != tex1.Depth)
        return false;

      if (tex0.Data == null || tex1.Data == null || tex0.Data.SizeInBytes != tex1.Data.SizeInBytes)
        return false;

      int sizeInbytes = tex0.Data.SizeInBytes;
      if (!BufferHelper.Compare(tex0.Data.Bytes, tex1.Data.Bytes))
        return false;

      return true;
    }
  }
}
