﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.IO.Compression;
using System.Xml;
using NUnit.Framework;
using Tesla.Graphics;

#nullable enable

namespace Tesla.Content
{
  internal class ReaderWriterTestInput
  {
    private MemoryStream m_ms;
    private IPrimitiveWriter? m_lastWriter;
    private IPrimitiveReader? m_lastReader;
    private Func<Stream, IPrimitiveWriter> m_createWriter;
    private Func<Stream, IPrimitiveReader> m_createReader;

    public ReaderWriterTestInput(Func<Stream, IPrimitiveWriter> createWriter, Func<Stream, IPrimitiveReader> createReader)
    {
      m_ms = new MemoryStream();
      m_createWriter = createWriter;
      m_createReader = createReader;
    }

    public IPrimitiveWriter CreateWriter()
    {
      m_lastWriter?.Dispose();
      m_lastReader?.Dispose();

      m_lastWriter = null;
      m_lastReader = null;

      if (!m_ms.CanWrite)
        m_ms = new MemoryStream();

      m_ms.SetLength(0);
      m_lastWriter = m_createWriter(m_ms);
      return m_lastWriter;
    }

    public IPrimitiveReader CreateReader()
    {
      m_lastWriter?.Flush();
      m_lastWriter?.Dispose();
      m_lastWriter = null;

      m_ms.Position = 0;
      m_lastReader = m_createReader(m_ms);
      return m_lastReader;
    }

    public void Dispose()
    {
      m_lastWriter?.Dispose();
      m_lastReader?.Dispose();
      m_ms.Dispose();
    }
  }

  [TestFixture]
  public class PrimitiveWriterReaderTests
  {
    private ReaderWriterTestInput CreateBinaryInputOutput()
    {
      return new ReaderWriterTestInput((ms) => new BinaryPrimitiveWriter(ms, true), (ms) => new BinaryPrimitiveReader(ms, true));
    }

    private ReaderWriterTestInput CreateJsonInputOutput()
    {
      return new ReaderWriterTestInput((ms) => new JsonPrimitiveWriter(ms, true), (ms) => new JsonPrimitiveReader(ms, true));
    }

    private void RunTest(Action<ReaderWriterTestInput> test)
    {
      var inputs = CreateBinaryInputOutput();
      test(inputs);
      inputs.Dispose();

      inputs = CreateJsonInputOutput();
      test(inputs);
      inputs.Dispose();
    }

    private void AssertArrayEquals<T>(ReadOnlySpan<T> outgoing, Span<T> incoming, int numRead) where T : struct, IEquatable<T>
    {
      for (int i = 0; i < numRead; i++)
        Assert.IsTrue(outgoing[i].Equals(incoming[i]));

      // Any more in incoming should be defaults
      T zeroVal = default;
      for (int j = numRead; j < incoming.Length; j++)
        Assert.IsTrue(zeroVal.Equals(incoming[j]));

      incoming.Clear();
    }

    private void AssertObjArrayEquals<T>(ReadOnlySpan<T> outgoing, Span<T> incoming, int numRead) where T : class?
    {
      for (int i = 0; i < numRead; i++)
        Assert.IsTrue(Object.Equals(outgoing[i], incoming[i]));

      // Any more in incoming should be null
      for (int j = numRead; j < incoming.Length; j++)
        Assert.IsTrue(incoming[j] == null);

      incoming.Clear();
    }


    private void AssertNullableEquals<T>(T? outgoing, T? incoming) where T : struct, IEquatable<T>
    {
      Assert.IsTrue(outgoing.HasValue == incoming.HasValue);

      if (outgoing.HasValue && incoming.HasValue)
        Assert.IsTrue(outgoing.Value.Equals(incoming.Value));
    }

    private void AssertEquals<T>(T outgoing, T incoming) where T : IEquatable<T>
    {
      Assert.IsTrue(outgoing.Equals(incoming));
    }

    private void AssertObjEquals<T>(T? outgoing, T? incoming) where T : class
    {
      Assert.IsTrue(Object.Equals(outgoing, incoming));
    }

    [TestCase]
    public void ReadWrite_Byte()
    {
      RunTest((input) =>
      {
        byte endOfTest = 42;

        // Test data
        byte data = 52;
        byte? dataNullable = 122;
        byte? dataNullableActuallyNull = null;
        byte[] dataArray = new byte[] { 32, 25, 51, 20 };

        Span<byte> receivedArray = stackalloc byte[4];
        byte defaultValue = 2;
        int sliceCount = 2;

        IPrimitiveWriter writer = input.CreateWriter();
        writer.Write("Test1", data);
        writer.Write("Test2", dataNullable);
        writer.Write("Test2_1", dataNullable);
        writer.Write("Test2_2", dataNullable);
        writer.Write("Test3", dataNullableActuallyNull);
        writer.Write("Test3_1", dataNullableActuallyNull);
        writer.Write("Test3_2", dataNullableActuallyNull);
        writer.Write("Test4", dataArray);
        writer.Write("Test5", dataArray.AsSpan().Slice(0, sliceCount));
        writer.Write("Test6", dataArray);
        writer.Write("Test7", dataArray);
        writer.Write("TestNullArray", Span<byte>.Empty);
        writer.Write("EndOfTest", endOfTest);

        IPrimitiveReader reader = input.CreateReader();

        // Read value that must exist
        byte receivedData = reader.ReadByte("Test1");
        AssertEquals(data, receivedData);

        // Read value that may exist, but will exist
        byte? receivedNullable1_1 = reader.ReadNullableByte("Test2");
        AssertNullableEquals(dataNullable, receivedNullable1_1);

        byte? receivedNullable1_2 = reader.ReadByteOrDefault("Test2_1", defaultValue);
        AssertNullableEquals(dataNullable, receivedNullable1_1);

        Assert.IsTrue(reader.TryReadByte("Test2_2", out byte receivedNullable1_3));
        AssertNullableEquals(dataNullable, receivedNullable1_3);

        // read value that may exist, but won't exist
        byte? receivedNullable2_1 = reader.ReadNullableByte("Test3");
        AssertNullableEquals(dataNullableActuallyNull, receivedNullable2_1);

        byte? receivedNullable2_2 = reader.ReadByteOrDefault("Test3_1", defaultValue);
        AssertNullableEquals(receivedNullable2_2, defaultValue);

        Assert.IsFalse(reader.TryReadByte("Test3_2", out byte receivedNullable2_3));

        // Read whole array
        int numRead = reader.ReadByteArray("Test4", receivedArray);
        Assert.IsTrue(numRead == dataArray.Length);
        AssertArrayEquals(dataArray, receivedArray, numRead);

        // Read whole array using a span that is larger
        numRead = reader.ReadByteArray("Test5", receivedArray);
        Assert.IsTrue(numRead == sliceCount);
        AssertArrayEquals(dataArray, receivedArray, numRead);

        // Read partial array using a span that is smaller
        Assert.IsTrue(reader.PeekArrayCount("Test6") == dataArray.Length);
        numRead = reader.ReadByteArray("Test6", receivedArray.Slice(0, sliceCount));
        Assert.IsTrue(numRead == sliceCount);
        Assert.IsTrue(numRead != dataArray.Length);
        AssertArrayEquals(dataArray, receivedArray, numRead);

        // Read whole array extension
        byte[] readArray = reader.ReadByteArray("Test7");
        Assert.IsTrue(readArray.Length == dataArray.Length);
        AssertArrayEquals(dataArray.AsSpan(), readArray.AsSpan(), readArray.Length);

        Assert.IsTrue(reader.PeekArrayCount("TestNullArray") == 0);
        Assert.IsTrue(reader.ReadByteArray("TestNullArray").Length == 0);

        Assert.IsTrue(reader.PeekArrayCount("EndOfTest") == 0);
        Assert.IsTrue(reader.ReadByte("EndOfTest") == endOfTest);
      });
    }

    [TestCase]
    public void ReadWrite_SByte()
    {
      RunTest((input) =>
      {
        byte endOfTest = 42;

        // Test data
        sbyte data = 52;
        sbyte? dataNullable = 122;
        sbyte? dataNullableActuallyNull = null;
        sbyte[] dataArray = new sbyte[] { 32, -25, 51, -20 };

        Span<sbyte> receivedArray = stackalloc sbyte[4];
        sbyte defaultValue = 2;
        int sliceCount = 2;

        IPrimitiveWriter writer = input.CreateWriter();
        writer.Write("Test1", data);
        writer.Write("Test2", dataNullable);
        writer.Write("Test2_1", dataNullable);
        writer.Write("Test2_2", dataNullable);
        writer.Write("Test3", dataNullableActuallyNull);
        writer.Write("Test3_1", dataNullableActuallyNull);
        writer.Write("Test3_2", dataNullableActuallyNull);
        writer.Write("Test4", dataArray);
        writer.Write("Test5", dataArray.AsSpan().Slice(0, sliceCount));
        writer.Write("Test6", dataArray);
        writer.Write("Test7", dataArray);
        writer.Write("TestNullArray", Span<sbyte>.Empty);
        writer.Write("EndOfTest", endOfTest);

        IPrimitiveReader reader = input.CreateReader();

        // Read value that must exist
        sbyte receivedData = reader.ReadSByte("Test1");
        AssertEquals(data, receivedData);

        // Read value that may exist, but will exist
        sbyte? receivedNullable1_1 = reader.ReadNullableSByte("Test2");
        AssertNullableEquals(dataNullable, receivedNullable1_1);

        sbyte? receivedNullable1_2 = reader.ReadSByteOrDefault("Test2_1", defaultValue);
        AssertNullableEquals(dataNullable, receivedNullable1_1);

        Assert.IsTrue(reader.TryReadSByte("Test2_2", out sbyte receivedNullable1_3));
        AssertNullableEquals(dataNullable, receivedNullable1_3);

        // read value that may exist, but won't exist
        sbyte? receivedNullable2_1 = reader.ReadNullableSByte("Test3");
        AssertNullableEquals(dataNullableActuallyNull, receivedNullable2_1);

        sbyte? receivedNullable2_2 = reader.ReadSByteOrDefault("Test3_1", defaultValue);
        AssertNullableEquals(receivedNullable2_2, defaultValue);

        Assert.IsFalse(reader.TryReadSByte("Test3_2", out sbyte receivedNullable2_3));

        // Read whole array
        int numRead = reader.ReadSByteArray("Test4", receivedArray);
        Assert.IsTrue(numRead == dataArray.Length);
        AssertArrayEquals(dataArray, receivedArray, numRead);

        // Read whole array using a span that is larger
        numRead = reader.ReadSByteArray("Test5", receivedArray);
        Assert.IsTrue(numRead == sliceCount);
        AssertArrayEquals(dataArray, receivedArray, numRead);

        // Read partial array using a span that is smaller
        Assert.IsTrue(reader.PeekArrayCount("Test6") == dataArray.Length);
        numRead = reader.ReadSByteArray("Test6", receivedArray.Slice(0, sliceCount));
        Assert.IsTrue(numRead == sliceCount);
        Assert.IsTrue(numRead != dataArray.Length);
        AssertArrayEquals(dataArray, receivedArray, numRead);

        // Read whole array extension
        sbyte[] readArray = reader.ReadSByteArray("Test7");
        Assert.IsTrue(readArray.Length == dataArray.Length);
        AssertArrayEquals(dataArray.AsSpan(), readArray.AsSpan(), readArray.Length);

        Assert.IsTrue(reader.PeekArrayCount("TestNullArray") == 0);
        Assert.IsTrue(reader.ReadSByteArray("TestNullArray").Length == 0);

        Assert.IsTrue(reader.PeekArrayCount("EndOfTest") == 0);
        Assert.IsTrue(reader.ReadByte("EndOfTest") == endOfTest);
      });
    }

    [TestCase]
    public void ReadWrite_Char()
    {
      RunTest((input) =>
      {
        byte endOfTest = 42;

        // Test data
        char data = '5';
        char? dataNullable = '2';
        char? dataNullableActuallyNull = null;
        char[] dataArray = new char[] { '3', '5', 'a', 'c' };

        Span<char> receivedArray = stackalloc char[4];
        char defaultValue = '_';
        int sliceCount = 2;

        IPrimitiveWriter writer = input.CreateWriter();
        writer.Write("Test1", data);
        writer.Write("Test2", dataNullable);
        writer.Write("Test2_1", dataNullable);
        writer.Write("Test2_2", dataNullable);
        writer.Write("Test3", dataNullableActuallyNull);
        writer.Write("Test3_1", dataNullableActuallyNull);
        writer.Write("Test3_2", dataNullableActuallyNull);
        writer.Write("Test4", dataArray);
        writer.Write("Test5", dataArray.AsSpan().Slice(0, sliceCount));
        writer.Write("Test6", dataArray);
        writer.Write("Test7", dataArray);
        writer.Write("TestNullArray", Span<char>.Empty);
        writer.Write("EndOfTest", endOfTest);

        IPrimitiveReader reader = input.CreateReader();

        // Read value that must exist
        char receivedData = reader.ReadChar("Test1");
        AssertEquals(data, receivedData);

        // Read value that may exist, but will exist
        char? receivedNullable1_1 = reader.ReadNullableChar("Test2");
        AssertNullableEquals(dataNullable, receivedNullable1_1);

        char? receivedNullable1_2 = reader.ReadCharOrDefault("Test2_1", defaultValue);
        AssertNullableEquals(dataNullable, receivedNullable1_1);

        Assert.IsTrue(reader.TryReadChar("Test2_2", out char receivedNullable1_3));
        AssertNullableEquals(dataNullable, receivedNullable1_3);

        // read value that may exist, but won't exist
        char? receivedNullable2_1 = reader.ReadNullableChar("Test3");
        AssertNullableEquals(dataNullableActuallyNull, receivedNullable2_1);

        char? receivedNullable2_2 = reader.ReadCharOrDefault("Test3_1", defaultValue);
        AssertNullableEquals(receivedNullable2_2, defaultValue);

        Assert.IsFalse(reader.TryReadChar("Test3_2", out char receivedNullable2_3));

        // Read whole array
        int numRead = reader.ReadCharArray("Test4", receivedArray);
        Assert.IsTrue(numRead == dataArray.Length);
        AssertArrayEquals(dataArray, receivedArray, numRead);

        // Read whole array using a span that is larger
        numRead = reader.ReadCharArray("Test5", receivedArray);
        Assert.IsTrue(numRead == sliceCount);
        AssertArrayEquals(dataArray, receivedArray, numRead);

        // Read partial array using a span that is smaller
        Assert.IsTrue(reader.PeekArrayCount("Test6") == dataArray.Length);
        numRead = reader.ReadCharArray("Test6", receivedArray.Slice(0, sliceCount));
        Assert.IsTrue(numRead == sliceCount);
        Assert.IsTrue(numRead != dataArray.Length);
        AssertArrayEquals(dataArray, receivedArray, numRead);

        // Read whole array extension
        char[] readArray = reader.ReadCharArray("Test7");
        Assert.IsTrue(readArray.Length == dataArray.Length);
        AssertArrayEquals(dataArray.AsSpan(), readArray.AsSpan(), readArray.Length);

        Assert.IsTrue(reader.PeekArrayCount("TestNullArray") == 0);
        Assert.IsTrue(reader.ReadCharArray("TestNullArray").Length == 0);

        Assert.IsTrue(reader.PeekArrayCount("EndOfTest") == 0);
        Assert.IsTrue(reader.ReadByte("EndOfTest") == endOfTest);
      });
    }

    [TestCase]
    public void ReadWrite_UInt16()
    {
      RunTest((input) =>
      {
        byte endOfTest = 42;

        // Test data
        ushort data = 52;
        ushort? dataNullable = 122;
        ushort? dataNullableActuallyNull = null;
        ushort[] dataArray = new ushort[] { 5, 1500, 255, 1556 };

        Span<ushort> receivedArray = stackalloc ushort[4];
        ushort defaultValue = 2;
        int sliceCount = 2;

        IPrimitiveWriter writer = input.CreateWriter();
        writer.Write("Test1", data);
        writer.Write("Test2", dataNullable);
        writer.Write("Test2_1", dataNullable);
        writer.Write("Test2_2", dataNullable);
        writer.Write("Test3", dataNullableActuallyNull);
        writer.Write("Test3_1", dataNullableActuallyNull);
        writer.Write("Test3_2", dataNullableActuallyNull);
        writer.Write("Test4", dataArray);
        writer.Write("Test5", dataArray.AsSpan().Slice(0, sliceCount));
        writer.Write("Test6", dataArray);
        writer.Write("Test7", dataArray);
        writer.Write("TestNullArray", Span<ushort>.Empty);
        writer.Write("EndOfTest", endOfTest);

        IPrimitiveReader reader = input.CreateReader();

        // Read value that must exist
        ushort receivedData = reader.ReadUInt16("Test1");
        AssertEquals(data, receivedData);

        // Read value that may exist, but will exist
        ushort? receivedNullable1_1 = reader.ReadNullableUInt16("Test2");
        AssertNullableEquals(dataNullable, receivedNullable1_1);

        ushort? receivedNullable1_2 = reader.ReadUInt16OrDefault("Test2_1", defaultValue);
        AssertNullableEquals(dataNullable, receivedNullable1_1);

        Assert.IsTrue(reader.TryReadUInt16("Test2_2", out ushort receivedNullable1_3));
        AssertNullableEquals(dataNullable, receivedNullable1_3);

        // read value that may exist, but won't exist
        ushort? receivedNullable2_1 = reader.ReadNullableUInt16("Test3");
        AssertNullableEquals(dataNullableActuallyNull, receivedNullable2_1);

        ushort? receivedNullable2_2 = reader.ReadUInt16OrDefault("Test3_1", defaultValue);
        AssertNullableEquals(receivedNullable2_2, defaultValue);

        Assert.IsFalse(reader.TryReadUInt16("Test3_2", out ushort receivedNullable2_3));

        // Read whole array
        int numRead = reader.ReadUInt16Array("Test4", receivedArray);
        Assert.IsTrue(numRead == dataArray.Length);
        AssertArrayEquals(dataArray, receivedArray, numRead);

        // Read whole array using a span that is larger
        numRead = reader.ReadUInt16Array("Test5", receivedArray);
        Assert.IsTrue(numRead == sliceCount);
        AssertArrayEquals(dataArray, receivedArray, numRead);

        // Read partial array using a span that is smaller
        Assert.IsTrue(reader.PeekArrayCount("Test6") == dataArray.Length);
        numRead = reader.ReadUInt16Array("Test6", receivedArray.Slice(0, sliceCount));
        Assert.IsTrue(numRead == sliceCount);
        Assert.IsTrue(numRead != dataArray.Length);
        AssertArrayEquals(dataArray, receivedArray, numRead);

        // Read whole array extension
        ushort[] readArray = reader.ReadUInt16Array("Test7");
        Assert.IsTrue(readArray.Length == dataArray.Length);
        AssertArrayEquals(dataArray.AsSpan(), readArray.AsSpan(), readArray.Length);

        Assert.IsTrue(reader.PeekArrayCount("TestNullArray") == 0);
        Assert.IsTrue(reader.ReadUInt16Array("TestNullArray").Length == 0);

        Assert.IsTrue(reader.PeekArrayCount("EndOfTest") == 0);
        Assert.IsTrue(reader.ReadByte("EndOfTest") == endOfTest);
      });
    }

    [TestCase]
    public void ReadWrite_UInt32()
    {
      RunTest((input) =>
      {
        byte endOfTest = 42;

        // Test data
        uint data = 52;
        uint? dataNullable = 150;
        uint? dataNullableActuallyNull = null;
        uint[] dataArray = new uint[] { 535, 25500, 5522, 8918 };

        Span<uint> receivedArray = stackalloc uint[4];
        uint defaultValue = 2;
        int sliceCount = 2;

        IPrimitiveWriter writer = input.CreateWriter();
        writer.Write("Test1", data);
        writer.Write("Test2", dataNullable);
        writer.Write("Test2_1", dataNullable);
        writer.Write("Test2_2", dataNullable);
        writer.Write("Test3", dataNullableActuallyNull);
        writer.Write("Test3_1", dataNullableActuallyNull);
        writer.Write("Test3_2", dataNullableActuallyNull);
        writer.Write("Test4", dataArray);
        writer.Write("Test5", dataArray.AsSpan().Slice(0, sliceCount));
        writer.Write("Test6", dataArray);
        writer.Write("Test7", dataArray);
        writer.Write("TestNullArray", Span<uint>.Empty);
        writer.Write("EndOfTest", endOfTest);

        IPrimitiveReader reader = input.CreateReader();

        // Read value that must exist
        uint receivedData = reader.ReadUInt32("Test1");
        AssertEquals(data, receivedData);

        // Read value that may exist, but will exist
        uint? receivedNullable1_1 = reader.ReadNullableUInt32("Test2");
        AssertNullableEquals(dataNullable, receivedNullable1_1);

        uint? receivedNullable1_2 = reader.ReadUInt32OrDefault("Test2_1", defaultValue);
        AssertNullableEquals(dataNullable, receivedNullable1_1);

        Assert.IsTrue(reader.TryReadUInt32("Test2_2", out uint receivedNullable1_3));
        AssertNullableEquals(dataNullable, receivedNullable1_3);

        // read value that may exist, but won't exist
        uint? receivedNullable2_1 = reader.ReadNullableUInt32("Test3");
        AssertNullableEquals(dataNullableActuallyNull, receivedNullable2_1);

        uint? receivedNullable2_2 = reader.ReadUInt32OrDefault("Test3_1", defaultValue);
        AssertNullableEquals(receivedNullable2_2, defaultValue);

        Assert.IsFalse(reader.TryReadUInt32("Test3_2", out uint receivedNullable2_3));

        // Read whole array
        int numRead = reader.ReadUInt32Array("Test4", receivedArray);
        Assert.IsTrue(numRead == dataArray.Length);
        AssertArrayEquals(dataArray, receivedArray, numRead);

        // Read whole array using a span that is larger
        numRead = reader.ReadUInt32Array("Test5", receivedArray);
        Assert.IsTrue(numRead == sliceCount);
        AssertArrayEquals(dataArray, receivedArray, numRead);

        // Read partial array using a span that is smaller
        Assert.IsTrue(reader.PeekArrayCount("Test6") == dataArray.Length);
        numRead = reader.ReadUInt32Array("Test6", receivedArray.Slice(0, sliceCount));
        Assert.IsTrue(numRead == sliceCount);
        Assert.IsTrue(numRead != dataArray.Length);
        AssertArrayEquals(dataArray, receivedArray, numRead);

        // Read whole array extension
        uint[] readArray = reader.ReadUInt32Array("Test7");
        Assert.IsTrue(readArray.Length == dataArray.Length);
        AssertArrayEquals(dataArray.AsSpan(), readArray.AsSpan(), readArray.Length);

        Assert.IsTrue(reader.PeekArrayCount("TestNullArray") == 0);
        Assert.IsTrue(reader.ReadUInt32Array("TestNullArray").Length == 0);

        Assert.IsTrue(reader.PeekArrayCount("EndOfTest") == 0);
        Assert.IsTrue(reader.ReadByte("EndOfTest") == endOfTest);
      });
    }

    [TestCase]
    public void ReadWrite_UInt64()
    {
      RunTest((input) =>
      {
        byte endOfTest = 42;

        // Test data
        ulong data = 52;
        ulong? dataNullable = 150;
        ulong? dataNullableActuallyNull = null;
        ulong[] dataArray = new ulong[] { 535, 25500, 5522, 8918 };

        Span<ulong> receivedArray = stackalloc ulong[4];
        ulong defaultValue = 2;
        int sliceCount = 2;

        IPrimitiveWriter writer = input.CreateWriter();
        writer.Write("Test1", data);
        writer.Write("Test2", dataNullable);
        writer.Write("Test2_1", dataNullable);
        writer.Write("Test2_2", dataNullable);
        writer.Write("Test3", dataNullableActuallyNull);
        writer.Write("Test3_1", dataNullableActuallyNull);
        writer.Write("Test3_2", dataNullableActuallyNull);
        writer.Write("Test4", dataArray);
        writer.Write("Test5", dataArray.AsSpan().Slice(0, sliceCount));
        writer.Write("Test6", dataArray);
        writer.Write("Test7", dataArray);
        writer.Write("TestNullArray", Span<ulong>.Empty);
        writer.Write("EndOfTest", endOfTest);

        IPrimitiveReader reader = input.CreateReader();

        // Read value that must exist
        ulong receivedData = reader.ReadUInt64("Test1");
        AssertEquals(data, receivedData);

        // Read value that may exist, but will exist
        ulong? receivedNullable1_1 = reader.ReadNullableUInt64("Test2");
        AssertNullableEquals(dataNullable, receivedNullable1_1);

        ulong? receivedNullable1_2 = reader.ReadUInt64OrDefault("Test2_1", defaultValue);
        AssertNullableEquals(dataNullable, receivedNullable1_1);

        Assert.IsTrue(reader.TryReadUInt64("Test2_2", out ulong receivedNullable1_3));
        AssertNullableEquals(dataNullable, receivedNullable1_3);

        // read value that may exist, but won't exist
        ulong? receivedNullable2_1 = reader.ReadNullableUInt64("Test3");
        AssertNullableEquals(dataNullableActuallyNull, receivedNullable2_1);

        ulong? receivedNullable2_2 = reader.ReadUInt64OrDefault("Test3_1", defaultValue);
        AssertNullableEquals(receivedNullable2_2, defaultValue);

        Assert.IsFalse(reader.TryReadUInt64("Test3_2", out ulong receivedNullable2_3));

        // Read whole array
        int numRead = reader.ReadUInt64Array("Test4", receivedArray);
        Assert.IsTrue(numRead == dataArray.Length);
        AssertArrayEquals(dataArray, receivedArray, numRead);

        // Read whole array using a span that is larger
        numRead = reader.ReadUInt64Array("Test5", receivedArray);
        Assert.IsTrue(numRead == sliceCount);
        AssertArrayEquals(dataArray, receivedArray, numRead);

        // Read partial array using a span that is smaller
        Assert.IsTrue(reader.PeekArrayCount("Test6") == dataArray.Length);
        numRead = reader.ReadUInt64Array("Test6", receivedArray.Slice(0, sliceCount));
        Assert.IsTrue(numRead == sliceCount);
        Assert.IsTrue(numRead != dataArray.Length);
        AssertArrayEquals(dataArray, receivedArray, numRead);

        // Read whole array extension
        ulong[] readArray = reader.ReadUInt64Array("Test7");
        Assert.IsTrue(readArray.Length == dataArray.Length);
        AssertArrayEquals(dataArray.AsSpan(), readArray.AsSpan(), readArray.Length);

        Assert.IsTrue(reader.PeekArrayCount("TestNullArray") == 0);
        Assert.IsTrue(reader.ReadUInt64Array("TestNullArray").Length == 0);

        Assert.IsTrue(reader.PeekArrayCount("EndOfTest") == 0);
        Assert.IsTrue(reader.ReadByte("EndOfTest") == endOfTest);
      });
    }

    [TestCase]
    public void ReadWrite_Int16()
    {
      RunTest((input) =>
      {
        byte endOfTest = 42;

        // Test data
        short data = 52;
        short? dataNullable = 150;
        short? dataNullableActuallyNull = null;
        short[] dataArray = new short[] { -535, -250, 5522, 8918 };

        Span<short> receivedArray = stackalloc short[4];
        short defaultValue = -5;
        int sliceCount = 2;

        IPrimitiveWriter writer = input.CreateWriter();
        writer.Write("Test1", data);
        writer.Write("Test2", dataNullable);
        writer.Write("Test2_1", dataNullable);
        writer.Write("Test2_2", dataNullable);
        writer.Write("Test3", dataNullableActuallyNull);
        writer.Write("Test3_1", dataNullableActuallyNull);
        writer.Write("Test3_2", dataNullableActuallyNull);
        writer.Write("Test4", dataArray);
        writer.Write("Test5", dataArray.AsSpan().Slice(0, sliceCount));
        writer.Write("Test6", dataArray);
        writer.Write("Test7", dataArray);
        writer.Write("TestNullArray", Span<short>.Empty);
        writer.Write("EndOfTest", endOfTest);

        IPrimitiveReader reader = input.CreateReader();

        // Read value that must exist
        short receivedData = reader.ReadInt16("Test1");
        AssertEquals(data, receivedData);

        // Read value that may exist, but will exist
        short? receivedNullable1_1 = reader.ReadNullableInt16("Test2");
        AssertNullableEquals(dataNullable, receivedNullable1_1);

        short? receivedNullable1_2 = reader.ReadInt16OrDefault("Test2_1", defaultValue);
        AssertNullableEquals(dataNullable, receivedNullable1_1);

        Assert.IsTrue(reader.TryReadInt16("Test2_2", out short receivedNullable1_3));
        AssertNullableEquals(dataNullable, receivedNullable1_3);

        // read value that may exist, but won't exist
        short? receivedNullable2_1 = reader.ReadNullableInt16("Test3");
        AssertNullableEquals(dataNullableActuallyNull, receivedNullable2_1);

        short? receivedNullable2_2 = reader.ReadInt16OrDefault("Test3_1", defaultValue);
        AssertNullableEquals(receivedNullable2_2, defaultValue);

        Assert.IsFalse(reader.TryReadInt16("Test3_2", out short receivedNullable2_3));

        // Read whole array
        int numRead = reader.ReadInt16Array("Test4", receivedArray);
        Assert.IsTrue(numRead == dataArray.Length);
        AssertArrayEquals(dataArray, receivedArray, numRead);

        // Read whole array using a span that is larger
        numRead = reader.ReadInt16Array("Test5", receivedArray);
        Assert.IsTrue(numRead == sliceCount);
        AssertArrayEquals(dataArray, receivedArray, numRead);

        // Read partial array using a span that is smaller
        Assert.IsTrue(reader.PeekArrayCount("Test6") == dataArray.Length);
        numRead = reader.ReadInt16Array("Test6", receivedArray.Slice(0, sliceCount));
        Assert.IsTrue(numRead == sliceCount);
        Assert.IsTrue(numRead != dataArray.Length);
        AssertArrayEquals(dataArray, receivedArray, numRead);

        // Read whole array extension
        short[] readArray = reader.ReadInt16Array("Test7");
        Assert.IsTrue(readArray.Length == dataArray.Length);
        AssertArrayEquals(dataArray.AsSpan(), readArray.AsSpan(), readArray.Length);

        Assert.IsTrue(reader.PeekArrayCount("TestNullArray") == 0);
        Assert.IsTrue(reader.ReadInt16Array("TestNullArray").Length == 0);

        Assert.IsTrue(reader.PeekArrayCount("EndOfTest") == 0);
        Assert.IsTrue(reader.ReadByte("EndOfTest") == endOfTest);
      });
    }

    [TestCase]
    public void ReadWrite_Int32()
    {
      RunTest((input) =>
      {
        byte endOfTest = 42;

        // Test data
        int data = 52;
        int? dataNullable = 150;
        int? dataNullableActuallyNull = null;
        int[] dataArray = new int[] { -535, -250, 5522, 8918 };

        Span<int> receivedArray = stackalloc int[4];
        int defaultValue = -5;
        int sliceCount = 2;

        IPrimitiveWriter writer = input.CreateWriter();
        writer.Write("Test1", data);
        writer.Write("Test2", dataNullable);
        writer.Write("Test2_1", dataNullable);
        writer.Write("Test2_2", dataNullable);
        writer.Write("Test3", dataNullableActuallyNull);
        writer.Write("Test3_1", dataNullableActuallyNull);
        writer.Write("Test3_2", dataNullableActuallyNull);
        writer.Write("Test4", dataArray);
        writer.Write("Test5", dataArray.AsSpan().Slice(0, sliceCount));
        writer.Write("Test6", dataArray);
        writer.Write("Test7", dataArray);
        writer.Write("TestNullArray", Span<int>.Empty);
        writer.Write("EndOfTest", endOfTest);

        IPrimitiveReader reader = input.CreateReader();

        // Read value that must exist
        int receivedData = reader.ReadInt32("Test1");
        AssertEquals(data, receivedData);

        // Read value that may exist, but will exist
        int? receivedNullable1_1 = reader.ReadNullableInt32("Test2");
        AssertNullableEquals(dataNullable, receivedNullable1_1);

        int? receivedNullable1_2 = reader.ReadInt32OrDefault("Test2_1", defaultValue);
        AssertNullableEquals(dataNullable, receivedNullable1_1);

        Assert.IsTrue(reader.TryReadInt32("Test2_2", out int receivedNullable1_3));
        AssertNullableEquals(dataNullable, receivedNullable1_3);

        // read value that may exist, but won't exist
        int? receivedNullable2_1 = reader.ReadNullableInt32("Test3");
        AssertNullableEquals(dataNullableActuallyNull, receivedNullable2_1);

        int? receivedNullable2_2 = reader.ReadInt32OrDefault("Test3_1", defaultValue);
        AssertNullableEquals(receivedNullable2_2, defaultValue);

        Assert.IsFalse(reader.TryReadInt32("Test3_2", out int receivedNullable2_3));

        // Read whole array
        int numRead = reader.ReadInt32Array("Test4", receivedArray);
        Assert.IsTrue(numRead == dataArray.Length);
        AssertArrayEquals(dataArray, receivedArray, numRead);

        // Read whole array using a span that is larger
        numRead = reader.ReadInt32Array("Test5", receivedArray);
        Assert.IsTrue(numRead == sliceCount);
        AssertArrayEquals(dataArray, receivedArray, numRead);

        // Read partial array using a span that is smaller
        Assert.IsTrue(reader.PeekArrayCount("Test6") == dataArray.Length);
        numRead = reader.ReadInt32Array("Test6", receivedArray.Slice(0, sliceCount));
        Assert.IsTrue(numRead == sliceCount);
        Assert.IsTrue(numRead != dataArray.Length);
        AssertArrayEquals(dataArray, receivedArray, numRead);

        // Read whole array extension
        int[] readArray = reader.ReadInt32Array("Test7");
        Assert.IsTrue(readArray.Length == dataArray.Length);
        AssertArrayEquals(dataArray.AsSpan(), readArray.AsSpan(), readArray.Length);

        Assert.IsTrue(reader.PeekArrayCount("TestNullArray") == 0);
        Assert.IsTrue(reader.ReadInt32Array("TestNullArray").Length == 0);

        Assert.IsTrue(reader.PeekArrayCount("EndOfTest") == 0);
        Assert.IsTrue(reader.ReadByte("EndOfTest") == endOfTest);
      });
    }

    [TestCase]
    public void ReadWrite_Int64()
    {
      RunTest((input) =>
      {
        byte endOfTest = 42;

        // Test data
        long data = 52;
        long? dataNullable = 150;
        long? dataNullableActuallyNull = null;
        long[] dataArray = new long[] { -535, -250, 5522, 8918 };

        Span<long> receivedArray = stackalloc long[4];
        long defaultValue = -5;
        int sliceCount = 2;

        IPrimitiveWriter writer = input.CreateWriter();
        writer.Write("Test1", data);
        writer.Write("Test2", dataNullable);
        writer.Write("Test2_1", dataNullable);
        writer.Write("Test2_2", dataNullable);
        writer.Write("Test3", dataNullableActuallyNull);
        writer.Write("Test3_1", dataNullableActuallyNull);
        writer.Write("Test3_2", dataNullableActuallyNull);
        writer.Write("Test4", dataArray);
        writer.Write("Test5", dataArray.AsSpan().Slice(0, sliceCount));
        writer.Write("Test6", dataArray);
        writer.Write("Test7", dataArray);
        writer.Write("TestNullArray", Span<long>.Empty);
        writer.Write("EndOfTest", endOfTest);

        IPrimitiveReader reader = input.CreateReader();

        // Read value that must exist
        long receivedData = reader.ReadInt64("Test1");
        AssertEquals(data, receivedData);

        // Read value that may exist, but will exist
        long? receivedNullable1_1 = reader.ReadNullableInt64("Test2");
        AssertNullableEquals(dataNullable, receivedNullable1_1);

        long? receivedNullable1_2 = reader.ReadInt64OrDefault("Test2_1", defaultValue);
        AssertNullableEquals(dataNullable, receivedNullable1_1);

        Assert.IsTrue(reader.TryReadInt64("Test2_2", out long receivedNullable1_3));
        AssertNullableEquals(dataNullable, receivedNullable1_3);

        // read value that may exist, but won't exist
        long? receivedNullable2_1 = reader.ReadNullableInt64("Test3");
        AssertNullableEquals(dataNullableActuallyNull, receivedNullable2_1);

        long? receivedNullable2_2 = reader.ReadInt64OrDefault("Test3_1", defaultValue);
        AssertNullableEquals(receivedNullable2_2, defaultValue);

        Assert.IsFalse(reader.TryReadInt64("Test3_2", out long receivedNullable2_3));

        // Read whole array
        int numRead = reader.ReadInt64Array("Test4", receivedArray);
        Assert.IsTrue(numRead == dataArray.Length);
        AssertArrayEquals(dataArray, receivedArray, numRead);

        // Read whole array using a span that is larger
        numRead = reader.ReadInt64Array("Test5", receivedArray);
        Assert.IsTrue(numRead == sliceCount);
        AssertArrayEquals(dataArray, receivedArray, numRead);

        // Read partial array using a span that is smaller
        Assert.IsTrue(reader.PeekArrayCount("Test6") == dataArray.Length);
        numRead = reader.ReadInt64Array("Test6", receivedArray.Slice(0, sliceCount));
        Assert.IsTrue(numRead == sliceCount);
        Assert.IsTrue(numRead != dataArray.Length);
        AssertArrayEquals(dataArray, receivedArray, numRead);

        // Read whole array extension
        long[] readArray = reader.ReadInt64Array("Test7");
        Assert.IsTrue(readArray.Length == dataArray.Length);
        AssertArrayEquals(dataArray.AsSpan(), readArray.AsSpan(), readArray.Length);

        Assert.IsTrue(reader.PeekArrayCount("TestNullArray") == 0);
        Assert.IsTrue(reader.ReadInt64Array("TestNullArray").Length == 0);

        Assert.IsTrue(reader.PeekArrayCount("EndOfTest") == 0);
        Assert.IsTrue(reader.ReadByte("EndOfTest") == endOfTest);
      });
    }

    [TestCase]
    public void ReadWrite_Half()
    {
      RunTest((input) =>
      {
        byte endOfTest = 42;

        // Test data
        Half data = (Half) 52f;
        Half? dataNullable = (Half) 150f;
        Half? dataNullableActuallyNull = null;
        Half[] dataArray = new Half[] { (Half) (-535f), (Half) (-250f), (Half) 5522f, (Half) 8918f };

        Span<Half> receivedArray = stackalloc Half[4];
        Half defaultValue = (Half) 5f;
        int sliceCount = 2;

        IPrimitiveWriter writer = input.CreateWriter();
        writer.Write("Test1", data);
        writer.Write("Test2", dataNullable);
        writer.Write("Test2_1", dataNullable);
        writer.Write("Test2_2", dataNullable);
        writer.Write("Test3", dataNullableActuallyNull);
        writer.Write("Test3_1", dataNullableActuallyNull);
        writer.Write("Test3_2", dataNullableActuallyNull);
        writer.Write("Test4", dataArray);
        writer.Write("Test5", dataArray.AsSpan().Slice(0, sliceCount));
        writer.Write("Test6", dataArray);
        writer.Write("Test7", dataArray);
        writer.Write("TestNullArray", Span<Half>.Empty);
        writer.Write("EndOfTest", endOfTest);

        IPrimitiveReader reader = input.CreateReader();

        // Read value that must exist
        Half receivedData = reader.ReadHalf("Test1");
        AssertEquals(data, receivedData);

        // Read value that may exist, but will exist
        Half? receivedNullable1_1 = reader.ReadNullableHalf("Test2");
        AssertNullableEquals(dataNullable, receivedNullable1_1);

        Half? receivedNullable1_2 = reader.ReadHalfOrDefault("Test2_1", defaultValue);
        AssertNullableEquals(dataNullable, receivedNullable1_1);

        Assert.IsTrue(reader.TryReadHalf("Test2_2", out Half receivedNullable1_3));
        AssertNullableEquals(dataNullable, receivedNullable1_3);

        // read value that may exist, but won't exist
        Half? receivedNullable2_1 = reader.ReadNullableHalf("Test3");
        AssertNullableEquals(dataNullableActuallyNull, receivedNullable2_1);

        Half? receivedNullable2_2 = reader.ReadHalfOrDefault("Test3_1", defaultValue);
        AssertNullableEquals(receivedNullable2_2, defaultValue);

        Assert.IsFalse(reader.TryReadHalf("Test3_2", out Half receivedNullable2_3));

        // Read whole array
        int numRead = reader.ReadHalfArray("Test4", receivedArray);
        Assert.IsTrue(numRead == dataArray.Length);
        AssertArrayEquals(dataArray, receivedArray, numRead);

        // Read whole array using a span that is larger
        numRead = reader.ReadHalfArray("Test5", receivedArray);
        Assert.IsTrue(numRead == sliceCount);
        AssertArrayEquals(dataArray, receivedArray, numRead);

        // Read partial array using a span that is smaller
        Assert.IsTrue(reader.PeekArrayCount("Test6") == dataArray.Length);
        numRead = reader.ReadHalfArray("Test6", receivedArray.Slice(0, sliceCount));
        Assert.IsTrue(numRead == sliceCount);
        Assert.IsTrue(numRead != dataArray.Length);
        AssertArrayEquals(dataArray, receivedArray, numRead);

        // Read whole array extension
        Half[] readArray = reader.ReadHalfArray("Test7");
        Assert.IsTrue(readArray.Length == dataArray.Length);
        AssertArrayEquals(dataArray.AsSpan(), readArray.AsSpan(), readArray.Length);

        Assert.IsTrue(reader.PeekArrayCount("TestNullArray") == 0);
        Assert.IsTrue(reader.ReadHalfArray("TestNullArray").Length == 0);

        Assert.IsTrue(reader.PeekArrayCount("EndOfTest") == 0);
        Assert.IsTrue(reader.ReadByte("EndOfTest") == endOfTest);
      });
    }

    [TestCase]
    public void ReadWrite_Single()
    {
      RunTest((input) =>
      {
        byte endOfTest = 42;

        // Test data
        float data = 52.25f;
        float? dataNullable = 150.55f;
        float? dataNullableActuallyNull = null;
        float[] dataArray = new float[] { -535.256f, -250.6f, 5522f, 8918f };

        Span<float> receivedArray = stackalloc float[4];
        float defaultValue = -5;
        int sliceCount = 2;

        IPrimitiveWriter writer = input.CreateWriter();
        writer.Write("Test1", data);
        writer.Write("Test2", dataNullable);
        writer.Write("Test2_1", dataNullable);
        writer.Write("Test2_2", dataNullable);
        writer.Write("Test3", dataNullableActuallyNull);
        writer.Write("Test3_1", dataNullableActuallyNull);
        writer.Write("Test3_2", dataNullableActuallyNull);
        writer.Write("Test4", dataArray);
        writer.Write("Test5", dataArray.AsSpan().Slice(0, sliceCount));
        writer.Write("Test6", dataArray);
        writer.Write("Test7", dataArray);
        writer.Write("TestNullArray", Span<float>.Empty);
        writer.Write("EndOfTest", endOfTest);

        IPrimitiveReader reader = input.CreateReader();

        // Read value that must exist
        float receivedData = reader.ReadSingle("Test1");
        AssertEquals(data, receivedData);

        // Read value that may exist, but will exist
        float? receivedNullable1_1 = reader.ReadNullableSingle("Test2");
        AssertNullableEquals(dataNullable, receivedNullable1_1);

        float? receivedNullable1_2 = reader.ReadSingleOrDefault("Test2_1", defaultValue);
        AssertNullableEquals(dataNullable, receivedNullable1_1);

        Assert.IsTrue(reader.TryReadSingle("Test2_2", out float receivedNullable1_3));
        AssertNullableEquals(dataNullable, receivedNullable1_3);

        // read value that may exist, but won't exist
        float? receivedNullable2_1 = reader.ReadNullableSingle("Test3");
        AssertNullableEquals(dataNullableActuallyNull, receivedNullable2_1);

        float? receivedNullable2_2 = reader.ReadSingleOrDefault("Test3_1", defaultValue);
        AssertNullableEquals(receivedNullable2_2, defaultValue);

        Assert.IsFalse(reader.TryReadSingle("Test3_2", out float receivedNullable2_3));

        // Read whole array
        int numRead = reader.ReadSingleArray("Test4", receivedArray);
        Assert.IsTrue(numRead == dataArray.Length);
        AssertArrayEquals(dataArray, receivedArray, numRead);

        // Read whole array using a span that is larger
        numRead = reader.ReadSingleArray("Test5", receivedArray);
        Assert.IsTrue(numRead == sliceCount);
        AssertArrayEquals(dataArray, receivedArray, numRead);

        // Read partial array using a span that is smaller
        Assert.IsTrue(reader.PeekArrayCount("Test6") == dataArray.Length);
        numRead = reader.ReadSingleArray("Test6", receivedArray.Slice(0, sliceCount));
        Assert.IsTrue(numRead == sliceCount);
        Assert.IsTrue(numRead != dataArray.Length);
        AssertArrayEquals(dataArray, receivedArray, numRead);

        // Read whole array extension
        float[] readArray = reader.ReadSingleArray("Test7");
        Assert.IsTrue(readArray.Length == dataArray.Length);
        AssertArrayEquals(dataArray.AsSpan(), readArray.AsSpan(), readArray.Length);

        Assert.IsTrue(reader.PeekArrayCount("TestNullArray") == 0);
        Assert.IsTrue(reader.ReadSingleArray("TestNullArray").Length == 0);

        Assert.IsTrue(reader.PeekArrayCount("EndOfTest") == 0);
        Assert.IsTrue(reader.ReadByte("EndOfTest") == endOfTest);
      });
    }

    [TestCase]
    public void ReadWrite_Double()
    {
      RunTest((input) =>
      {
        byte endOfTest = 42;

        // Test data
        double data = 52.0;
        double? dataNullable = 150.0;
        double? dataNullableActuallyNull = null;
        double[] dataArray = new double[] { -535.60, -250.77, 5522.25, 8918.62 };

        Span<double> receivedArray = stackalloc double[4];
        double defaultValue = -5.25;
        int sliceCount = 2;

        IPrimitiveWriter writer = input.CreateWriter();
        writer.Write("Test1", data);
        writer.Write("Test2", dataNullable);
        writer.Write("Test2_1", dataNullable);
        writer.Write("Test2_2", dataNullable);
        writer.Write("Test3", dataNullableActuallyNull);
        writer.Write("Test3_1", dataNullableActuallyNull);
        writer.Write("Test3_2", dataNullableActuallyNull);
        writer.Write("Test4", dataArray);
        writer.Write("Test5", dataArray.AsSpan().Slice(0, sliceCount));
        writer.Write("Test6", dataArray);
        writer.Write("Test7", dataArray);
        writer.Write("TestNullArray", Span<double>.Empty);
        writer.Write("EndOfTest", endOfTest);

        IPrimitiveReader reader = input.CreateReader();

        // Read value that must exist
        double receivedData = reader.ReadDouble("Test1");
        AssertEquals(data, receivedData);

        // Read value that may exist, but will exist
        double? receivedNullable1_1 = reader.ReadNullableDouble("Test2");
        AssertNullableEquals(dataNullable, receivedNullable1_1);

        double? receivedNullable1_2 = reader.ReadDoubleOrDefault("Test2_1", defaultValue);
        AssertNullableEquals(dataNullable, receivedNullable1_1);

        Assert.IsTrue(reader.TryReadDouble("Test2_2", out double receivedNullable1_3));
        AssertNullableEquals(dataNullable, receivedNullable1_3);

        // read value that may exist, but won't exist
        double? receivedNullable2_1 = reader.ReadNullableDouble("Test3");
        AssertNullableEquals(dataNullableActuallyNull, receivedNullable2_1);

        double? receivedNullable2_2 = reader.ReadDoubleOrDefault("Test3_1", defaultValue);
        AssertNullableEquals(receivedNullable2_2, defaultValue);

        Assert.IsFalse(reader.TryReadDouble("Test3_2", out double receivedNullable2_3));

        // Read whole array
        int numRead = reader.ReadDoubleArray("Test4", receivedArray);
        Assert.IsTrue(numRead == dataArray.Length);
        AssertArrayEquals(dataArray, receivedArray, numRead);

        // Read whole array using a span that is larger
        numRead = reader.ReadDoubleArray("Test5", receivedArray);
        Assert.IsTrue(numRead == sliceCount);
        AssertArrayEquals(dataArray, receivedArray, numRead);

        // Read partial array using a span that is smaller
        Assert.IsTrue(reader.PeekArrayCount("Test6") == dataArray.Length);
        numRead = reader.ReadDoubleArray("Test6", receivedArray.Slice(0, sliceCount));
        Assert.IsTrue(numRead == sliceCount);
        Assert.IsTrue(numRead != dataArray.Length);
        AssertArrayEquals(dataArray, receivedArray, numRead);

        // Read whole array extension
        double[] readArray = reader.ReadDoubleArray("Test7");
        Assert.IsTrue(readArray.Length == dataArray.Length);
        AssertArrayEquals(dataArray.AsSpan(), readArray.AsSpan(), readArray.Length);

        Assert.IsTrue(reader.PeekArrayCount("TestNullArray") == 0);
        Assert.IsTrue(reader.ReadDoubleArray("TestNullArray").Length == 0);

        Assert.IsTrue(reader.PeekArrayCount("EndOfTest") == 0);
        Assert.IsTrue(reader.ReadByte("EndOfTest") == endOfTest);
      });
    }

    [TestCase]
    public void ReadWrite_Decimal()
    {
      RunTest((input) =>
      {
        byte endOfTest = 42;

        // Test data
        decimal data = 52m;
        decimal? dataNullable = 150m;
        decimal? dataNullableActuallyNull = null;
        decimal[] dataArray = new decimal[] { -535m, -250m, 5522m, 8918m };

        Span<decimal> receivedArray = stackalloc decimal[4];
        decimal defaultValue = -5m;
        int sliceCount = 2;

        IPrimitiveWriter writer = input.CreateWriter();
        writer.Write("Test1", data);
        writer.Write("Test2", dataNullable);
        writer.Write("Test2_1", dataNullable);
        writer.Write("Test2_2", dataNullable);
        writer.Write("Test3", dataNullableActuallyNull);
        writer.Write("Test3_1", dataNullableActuallyNull);
        writer.Write("Test3_2", dataNullableActuallyNull);
        writer.Write("Test4", dataArray);
        writer.Write("Test5", dataArray.AsSpan().Slice(0, sliceCount));
        writer.Write("Test6", dataArray);
        writer.Write("Test7", dataArray);
        writer.Write("TestNullArray", Span<decimal>.Empty);
        writer.Write("EndOfTest", endOfTest);

        IPrimitiveReader reader = input.CreateReader();

        // Read value that must exist
        decimal receivedData = reader.ReadDecimal("Test1");
        AssertEquals(data, receivedData);

        // Read value that may exist, but will exist
        decimal? receivedNullable1_1 = reader.ReadNullableDecimal("Test2");
        AssertNullableEquals(dataNullable, receivedNullable1_1);

        decimal? receivedNullable1_2 = reader.ReadDecimalOrDefault("Test2_1", defaultValue);
        AssertNullableEquals(dataNullable, receivedNullable1_1);

        Assert.IsTrue(reader.TryReadDecimal("Test2_2", out decimal receivedNullable1_3));
        AssertNullableEquals(dataNullable, receivedNullable1_3);

        // read value that may exist, but won't exist
        decimal? receivedNullable2_1 = reader.ReadNullableDecimal("Test3");
        AssertNullableEquals(dataNullableActuallyNull, receivedNullable2_1);

        decimal? receivedNullable2_2 = reader.ReadDecimalOrDefault("Test3_1", defaultValue);
        AssertNullableEquals(receivedNullable2_2, defaultValue);

        Assert.IsFalse(reader.TryReadDecimal("Test3_2", out decimal receivedNullable2_3));

        // Read whole array
        int numRead = reader.ReadDecimalArray("Test4", receivedArray);
        Assert.IsTrue(numRead == dataArray.Length);
        AssertArrayEquals(dataArray, receivedArray, numRead);

        // Read whole array using a span that is larger
        numRead = reader.ReadDecimalArray("Test5", receivedArray);
        Assert.IsTrue(numRead == sliceCount);
        AssertArrayEquals(dataArray, receivedArray, numRead);

        // Read partial array using a span that is smaller
        Assert.IsTrue(reader.PeekArrayCount("Test6") == dataArray.Length);
        numRead = reader.ReadDecimalArray("Test6", receivedArray.Slice(0, sliceCount));
        Assert.IsTrue(numRead == sliceCount);
        Assert.IsTrue(numRead != dataArray.Length);
        AssertArrayEquals(dataArray, receivedArray, numRead);

        // Read whole array extension
        decimal[] readArray = reader.ReadDecimalArray("Test7");
        Assert.IsTrue(readArray.Length == dataArray.Length);
        AssertArrayEquals(dataArray.AsSpan(), readArray.AsSpan(), readArray.Length);

        Assert.IsTrue(reader.PeekArrayCount("TestNullArray") == 0);
        Assert.IsTrue(reader.ReadDecimalArray("TestNullArray").Length == 0);

        Assert.IsTrue(reader.PeekArrayCount("EndOfTest") == 0);
        Assert.IsTrue(reader.ReadByte("EndOfTest") == endOfTest);
      });
    }

    [TestCase]
    public void ReadWrite_Boolean()
    {
      RunTest((input) =>
      {
        byte endOfTest = 42;

        // Test data
        bool data = true;
        bool? dataNullable = true;
        bool? dataNullableActuallyNull = null;
        bool[] dataArray = new bool[] { true, false, false, true };

        Span<bool> receivedArray = stackalloc bool[4];
        bool defaultValue = true;
        int sliceCount = 2;

        IPrimitiveWriter writer = input.CreateWriter();
        writer.Write("Test1", data);
        writer.Write("Test2", dataNullable);
        writer.Write("Test2_1", dataNullable);
        writer.Write("Test2_2", dataNullable);
        writer.Write("Test3", dataNullableActuallyNull);
        writer.Write("Test3_1", dataNullableActuallyNull);
        writer.Write("Test3_2", dataNullableActuallyNull);
        writer.Write("Test4", dataArray);
        writer.Write("Test5", dataArray.AsSpan().Slice(0, sliceCount));
        writer.Write("Test6", dataArray);
        writer.Write("Test7", dataArray);
        writer.Write("TestNullArray", Span<bool>.Empty);
        writer.Write("EndOfTest", endOfTest);

        IPrimitiveReader reader = input.CreateReader();

        // Read value that must exist
        bool receivedData = reader.ReadBoolean("Test1");
        AssertEquals(data, receivedData);

        // Read value that may exist, but will exist
        bool? receivedNullable1_1 = reader.ReadNullableBoolean("Test2");
        AssertNullableEquals(dataNullable, receivedNullable1_1);

        bool? receivedNullable1_2 = reader.ReadBooleanOrDefault("Test2_1", defaultValue);
        AssertNullableEquals(dataNullable, receivedNullable1_1);

        Assert.IsTrue(reader.TryReadBoolean("Test2_2", out bool receivedNullable1_3));
        AssertNullableEquals(dataNullable, receivedNullable1_3);

        // read value that may exist, but won't exist
        bool? receivedNullable2_1 = reader.ReadNullableBoolean("Test3");
        AssertNullableEquals(dataNullableActuallyNull, receivedNullable2_1);

        bool? receivedNullable2_2 = reader.ReadBooleanOrDefault("Test3_1", defaultValue);
        AssertNullableEquals(receivedNullable2_2, defaultValue);

        Assert.IsFalse(reader.TryReadBoolean("Test3_2", out bool receivedNullable2_3));

        // Read whole array
        int numRead = reader.ReadBooleanArray("Test4", receivedArray);
        Assert.IsTrue(numRead == dataArray.Length);
        AssertArrayEquals(dataArray, receivedArray, numRead);

        // Read whole array using a span that is larger
        numRead = reader.ReadBooleanArray("Test5", receivedArray);
        Assert.IsTrue(numRead == sliceCount);
        AssertArrayEquals(dataArray, receivedArray, numRead);

        // Read partial array using a span that is smaller
        Assert.IsTrue(reader.PeekArrayCount("Test6") == dataArray.Length);
        numRead = reader.ReadBooleanArray("Test6", receivedArray.Slice(0, sliceCount));
        Assert.IsTrue(numRead == sliceCount);
        Assert.IsTrue(numRead != dataArray.Length);
        AssertArrayEquals(dataArray, receivedArray, numRead);

        // Read whole array extension
        bool[] readArray = reader.ReadBooleanArray("Test7");
        Assert.IsTrue(readArray.Length == dataArray.Length);
        AssertArrayEquals(dataArray.AsSpan(), readArray.AsSpan(), readArray.Length);

        Assert.IsTrue(reader.PeekArrayCount("TestNullArray") == 0);
        Assert.IsTrue(reader.ReadBooleanArray("TestNullArray").Length == 0);

        Assert.IsTrue(reader.PeekArrayCount("EndOfTest") == 0);
        Assert.IsTrue(reader.ReadByte("EndOfTest") == endOfTest);
      });
    }

    [TestCase]
    public void ReadWrite_String()
    {
      RunTest((input) =>
      {
        byte endOfTest = 42;

        // Test data
        string data = "test data";
        string? dataNullable = "test ";
        string? dataNullableActuallyNull = null;
        string?[] dataArray = new string?[] { "testy", "5252a", null, "525 a fox jumped" };

        Span<string?> receivedArray = new string?[4];
        string defaultValue = "default value";
        int sliceCount = 2;

        IPrimitiveWriter writer = input.CreateWriter();
        writer.Write("Test1", data);
        writer.Write("Test2", dataNullable);
        writer.Write("Test2_1", dataNullable);
        writer.Write("Test2_2", dataNullable);
        writer.Write("Test3", dataNullableActuallyNull);
        writer.Write("Test3_1", dataNullableActuallyNull);
        writer.Write("Test3_2", dataNullableActuallyNull);
        writer.Write("Test4", dataArray);
        writer.Write("Test5", dataArray.AsSpan().Slice(0, sliceCount));
        writer.Write("Test6", dataArray);
        writer.Write("Test7", dataArray);
        writer.Write("TestNullArray", Span<string?>.Empty);
        writer.Write("EndOfTest", endOfTest);

        IPrimitiveReader reader = input.CreateReader();

        // Read value that must exist
        string? receivedData = reader.ReadString("Test1");
        AssertObjEquals(data, receivedData);

        // Read value that may exist, but will exist
        string? receivedNullable1_1 = reader.ReadString("Test2");
        AssertObjEquals(dataNullable, receivedNullable1_1);

        string? receivedNullable1_2 = reader.ReadStringOrDefault("Test2_1", defaultValue);
        AssertObjEquals(dataNullable, receivedNullable1_1);

        Assert.IsTrue(reader.TryReadString("Test2_2", out string? receivedNullable1_3));
        AssertObjEquals(dataNullable, receivedNullable1_3);

        // read value that may exist, but won't exist
        string? receivedNullable2_1 = reader.ReadString("Test3");
        AssertObjEquals(dataNullableActuallyNull, receivedNullable2_1);

        string? receivedNullable2_2 = reader.ReadStringOrDefault("Test3_1", defaultValue);
        AssertObjEquals(receivedNullable2_2, defaultValue);

        Assert.IsFalse(reader.TryReadString("Test3_2", out string? receivedNullable2_3));

        // Read whole array
        int numRead = reader.ReadStringArray("Test4", receivedArray);
        Assert.IsTrue(numRead == dataArray.Length);
        AssertObjArrayEquals(dataArray, receivedArray, numRead);

        // Read whole array using a span that is larger
        numRead = reader.ReadStringArray("Test5", receivedArray);
        Assert.IsTrue(numRead == sliceCount);
        AssertObjArrayEquals(dataArray, receivedArray, numRead);

        // Read partial array using a span that is smaller
        Assert.IsTrue(reader.PeekArrayCount("Test6") == dataArray.Length);
        numRead = reader.ReadStringArray("Test6", receivedArray.Slice(0, sliceCount));
        Assert.IsTrue(numRead == sliceCount);
        Assert.IsTrue(numRead != dataArray.Length);
        AssertObjArrayEquals(dataArray, receivedArray, numRead);

        // Read whole array extension
        string?[] readArray = reader.ReadStringArray("Test7");
        Assert.IsTrue(readArray.Length == dataArray.Length);
        AssertObjArrayEquals(dataArray.AsSpan(), readArray.AsSpan(), readArray.Length);

        Assert.IsTrue(reader.PeekArrayCount("TestNullArray") == 0);
        Assert.IsTrue(reader.ReadStringArray("TestNullArray").Length == 0);

        Assert.IsTrue(reader.PeekArrayCount("EndOfTest") == 0);
        Assert.IsTrue(reader.ReadByte("EndOfTest") == endOfTest);
      });
    }

    [TestCase]
    public void ReadWrite_PrimitiveValue()
    {
      RunTest((input) =>
      {
        byte endOfTest = 42;

        // Test data
        Vector3 data = new Vector3(259, 102, 5235);
        Vector3? dataNullable = new Vector3(204, 124, 111);
        Vector3? dataNullableActuallyNull = null;
        Vector3[] dataArray = new Vector3[] { new Vector3(525, 25, 10), Vector3.One, Vector3.UnitY, new Vector3(10, 525, 105) };

        Span<Vector3> receivedArray = stackalloc Vector3[4];
        Vector3 defaultValue = new Vector3(-2, 5, 10);
        int sliceCount = 2;

        IPrimitiveWriter writer = input.CreateWriter();
        writer.Write<Vector3>("Test1", data);
        writer.Write<Vector3>("Test2", dataNullable);
        writer.Write<Vector3>("Test2_1", dataNullable);
        writer.Write<Vector3>("Test2_2", dataNullable);
        writer.Write<Vector3>("Test3", dataNullableActuallyNull);
        writer.Write<Vector3>("Test3_1", dataNullableActuallyNull);
        writer.Write<Vector3>("Test3_2", dataNullableActuallyNull);
        writer.Write<Vector3>("Test4", dataArray);
        writer.Write<Vector3>("Test5", dataArray.AsSpan().Slice(0, sliceCount));
        writer.Write<Vector3>("Test6", dataArray);
        writer.Write<Vector3>("Test7", dataArray);
        writer.Write<Vector3>("TestNullArray", Span<Vector3>.Empty);
        writer.Write("EndOfTest", endOfTest);

        IPrimitiveReader reader = input.CreateReader();

        // Read value that must exist
        Vector3 receivedData = reader.Read<Vector3>("Test1");
        AssertEquals(data, receivedData);

        // Read value that may exist, but will exist
        Vector3? receivedNullable1_1 = reader.ReadNullable<Vector3>("Test2");
        AssertNullableEquals(dataNullable, receivedNullable1_1);

        Vector3? receivedNullable1_2 = reader.ReadOrDefault<Vector3>("Test2_1", defaultValue);
        AssertNullableEquals(dataNullable, receivedNullable1_1);

        Assert.IsTrue(reader.TryRead("Test2_2", out Vector3 receivedNullable1_3));
        AssertNullableEquals(dataNullable, receivedNullable1_3);

        // read value that may exist, but won't exist
        Vector3? receivedNullable2_1 = reader.ReadNullable<Vector3>("Test3");
        AssertNullableEquals(dataNullableActuallyNull, receivedNullable2_1);

        Vector3? receivedNullable2_2 = reader.ReadOrDefault<Vector3>("Test3_1", defaultValue);
        AssertNullableEquals(receivedNullable2_2, defaultValue);

        Assert.IsFalse(reader.TryRead<Vector3>("Test3_2", out Vector3 receivedNullable2_3));

        // Read whole array
        int numRead = reader.ReadArray<Vector3>("Test4", receivedArray);
        Assert.IsTrue(numRead == dataArray.Length);
        AssertArrayEquals(dataArray, receivedArray, numRead);

        // Read whole array using a span that is larger
        numRead = reader.ReadArray<Vector3>("Test5", receivedArray);
        Assert.IsTrue(numRead == sliceCount);
        AssertArrayEquals(dataArray, receivedArray, numRead);

        // Read partial array using a span that is smaller
        Assert.IsTrue(reader.PeekArrayCount("Test6") == dataArray.Length);
        numRead = reader.ReadArray<Vector3>("Test6", receivedArray.Slice(0, sliceCount));
        Assert.IsTrue(numRead == sliceCount);
        Assert.IsTrue(numRead != dataArray.Length);
        AssertArrayEquals(dataArray, receivedArray, numRead);

        // Read whole array extension
        Vector3[] readArray = reader.ReadArray<Vector3>("Test7");
        Assert.IsTrue(readArray.Length == dataArray.Length);
        AssertArrayEquals(dataArray.AsSpan(), readArray.AsSpan(), readArray.Length);

        Assert.IsTrue(reader.PeekArrayCount("TestNullArray") == 0);
        Assert.IsTrue(reader.ReadArray<Vector3>("TestNullArray").Length == 0);

        Assert.IsTrue(reader.PeekArrayCount("EndOfTest") == 0);
        Assert.IsTrue(reader.ReadByte("EndOfTest") == endOfTest);
      });
    }

    [TestCase]
    public void ReadWrite_Enum()
    {
      RunTest((input) =>
      {
        byte endOfTest = 42;

        EnumByte val0 = EnumByte.Test1;
        EnumSByte val1 = EnumSByte.Test2;
        EnumInt16 val2 = EnumInt16.Test1;
        EnumUInt16 val3 = EnumUInt16.Test2;
        EnumInt32 val4 = EnumInt32.Test1;
        EnumUInt32 val5 = EnumUInt32.Test2;
        EnumInt64 val6 = EnumInt64.Test1;
        EnumUInt64 val7 = EnumUInt64.Test2;
        EnumUInt64? val8 = EnumUInt64.Test1;
        EnumUInt64? val9 = null;

        IPrimitiveWriter writer = input.CreateWriter();
        writer.WriteEnum("Val0", val0);
        writer.WriteEnum("Val1", val1);
        writer.WriteEnum("Val2", val2);
        writer.WriteEnum("Val3", val3);
        writer.WriteEnum("Val4", val4);
        writer.WriteEnum("Val5", val5);
        writer.WriteEnum("Val6", val6);
        writer.WriteEnum("Val7", val7);
        writer.WriteEnum("Val8", val8);
        writer.WriteEnum("Val8_1", val8);
        writer.WriteEnum("Val8_2", val8);
        writer.WriteEnum("Val9", val9);
        writer.WriteEnum("Val9_1", val9);
        writer.WriteEnum("Val9_2", val9);
        writer.WriteEnum("Val10", val8);
        writer.Write("EndOfTest", endOfTest);

        IPrimitiveReader reader = input.CreateReader();

        Assert.IsTrue(reader.ReadEnum<EnumByte>("Val0") == val0);
        Assert.IsTrue(reader.ReadEnum<EnumSByte>("Val1") == val1);
        Assert.IsTrue(reader.ReadEnum<EnumInt16>("Val2") == val2);
        Assert.IsTrue(reader.ReadEnum<EnumUInt16>("Val3") == val3);
        Assert.IsTrue(reader.ReadEnum<EnumInt32>("Val4") == val4);
        Assert.IsTrue(reader.ReadEnum<EnumUInt32>("Val5") == val5);
        Assert.IsTrue(reader.ReadEnum<EnumInt64>("Val6") == val6);
        Assert.IsTrue(reader.ReadEnum<EnumUInt64>("Val7") == val7);
        Assert.IsTrue(reader.ReadNullableEnum<EnumUInt64>("Val8") == val8);
        Assert.IsTrue(reader.ReadEnumOrDefault<EnumUInt64>("Val8_1", EnumUInt64.Test2) == val8);
        Assert.IsTrue(reader.TryReadEnum<EnumUInt64>("Val8_2", out EnumUInt64 readVal8));
        Assert.IsTrue(readVal8 == val8);

        Assert.IsTrue(reader.ReadNullableEnum<EnumUInt64>("Val9") == val9);
        Assert.IsTrue(reader.ReadEnumOrDefault<EnumUInt64>("Val9_1", EnumUInt64.Test2) == EnumUInt64.Test2);
        Assert.IsFalse(reader.TryReadEnum<EnumUInt64>("Val9_2", out EnumUInt64 readVal9));

        // Read non-nullable from nullable
        Assert.IsTrue(reader.ReadEnum<EnumUInt64>("Val10") == val8.Value);

        Assert.IsTrue(reader.PeekArrayCount("EndOfTest") == 0);
        Assert.IsTrue(reader.ReadByte("EndOfTest") == endOfTest);
      });
    }

    [TestCase]
    public void ReadWrite_Blob()
    {
      RunTest((input) =>
      {
        byte endOfTest = 42;

        Span<Vector3> spanOfVectors = new Vector3[] { new Vector3(525, 25, 10), Vector3.One, Vector3.UnitY, new Vector3(10, 525, 105) };
        DataBuffer<Vector3> dbOfVector3s = new DataBuffer<Vector3>(spanOfVectors);

        IPrimitiveWriter writer = input.CreateWriter();
        writer.WriteBlob<Vector3>("Test1", spanOfVectors);
        writer.WriteBlob<Vector3>("Test2", spanOfVectors.Slice(0, 2));
        writer.WriteBlob<Vector3>("Test3", spanOfVectors);
        writer.WriteBlob<Vector3>("TestNull", Span<Vector3>.Empty);
        writer.WriteBlob("TestNull2", null);
        writer.WriteBlob("Test4", dbOfVector3s);
        writer.WriteBlob("Test5", dbOfVector3s);
        writer.WriteBlob("Test6", dbOfVector3s);
        writer.WriteBlob("Test7", dbOfVector3s);
        writer.WriteBlob("Test8", dbOfVector3s);
        writer.WriteBlob("Test9", dbOfVector3s);
        writer.Write("EndOfTest", endOfTest);

        IPrimitiveReader reader = input.CreateReader();

        Assert.IsTrue(reader.PeekBlobCount<Vector3>("Test1") == spanOfVectors.Length);
        Assert.IsTrue(reader.PeekBlobCount<float>("Test1") == spanOfVectors.Length * 3);

        PrimitiveBlobHeader? blobHeader = reader.PeekBlob("Test1");
        Assert.IsTrue(blobHeader.HasValue);
        if (blobHeader.HasValue)
        {
          Assert.IsTrue(blobHeader.Value.ElementCount == spanOfVectors.Length);
          Assert.IsTrue(blobHeader.Value.ElementType == typeof(Vector3));
          Assert.IsTrue(blobHeader.Value.ElementSizeInBytes == Vector3.SizeInBytes);
          Assert.IsTrue(blobHeader.Value.BlobSizeInBytes == spanOfVectors.Length * Vector3.SizeInBytes);
          Assert.IsTrue(blobHeader.Value.GetElementCount<float>() == spanOfVectors.Length * 3);

          Span<Vector3> readSpan = stackalloc Vector3[blobHeader.Value.ElementCount];
          int numRead = reader.ReadBlob<Vector3>("Test1", readSpan);
          Assert.IsTrue(readSpan.Length == numRead);
          AssertArrayEquals<Vector3>(spanOfVectors, readSpan, numRead);
        }

        IDataBuffer? readDb = reader.ReadBlob("Test2");
        Assert.IsTrue(readDb != null);
        Assert.IsTrue(readDb!.Length == 2);
        Assert.IsTrue(readDb!.ElementType == typeof(Vector3));
        AssertArrayEquals<Vector3>(spanOfVectors, readDb!.Bytes.Reinterpret<byte, Vector3>(), 2);
        readDb!.Dispose();

        DataBuffer<Vector3>? readDb2 = reader.ReadBlob<Vector3>("Test3");
        Assert.IsTrue(readDb2 != null);
        Assert.IsTrue(readDb2!.Length == spanOfVectors.Length);
        Assert.IsTrue(readDb2!.ElementType == typeof(Vector3));
        AssertArrayEquals<Vector3>(spanOfVectors, readDb2!.Span, readDb2!.Length);
        readDb2!.Dispose();

        Assert.IsTrue(reader.PeekBlob("TestNull") == null);
        Assert.IsTrue(reader.PeekBlobCount<Vector3>("TestNull") == 0);

        IDataBuffer nullDb = reader.ReadBlob("TestNull");
        Assert.IsNotNull(nullDb);
        Assert.IsTrue(nullDb.Length == 0);
        Assert.IsFalse(reader.TryReadBlob("TestNull", out IDataBuffer? nullDb2));
        Assert.IsNull(nullDb2);

        // Read into a buffer that is larger
        DataBuffer<Vector3> bigDb = DataBuffer.Create<Vector3>(10);
        int numReadIntoBigDb = reader.ReadBlob("Test4", bigDb);
        Assert.IsTrue(numReadIntoBigDb == spanOfVectors.Length);
        AssertArrayEquals<Vector3>(spanOfVectors, bigDb.Span.Slice(0, numReadIntoBigDb), numReadIntoBigDb);
        bigDb.Dispose();


        // Read into a buffer that is smaller
        Span<Vector3> smallerArray = stackalloc Vector3[2];
        int numReadSmaller = reader.ReadBlob("Test5", smallerArray);
        Assert.IsTrue(numReadSmaller == 2);
        AssertArrayEquals<Vector3>(spanOfVectors, smallerArray, numReadSmaller);

        // Read into a buffer that will be resized
        DataBuffer<Vector3> smallDb = DataBuffer.Create<Vector3>(2);
        int numReadSmallDb = reader.ReadBlob("Test6", smallDb, true);
        Assert.IsTrue(numReadSmallDb == spanOfVectors.Length);
        Assert.IsTrue(smallDb.Length == spanOfVectors.Length);
        AssertArrayEquals<Vector3>(spanOfVectors, smallDb.Span, numReadSmallDb);
        smallDb.Dispose();

        // Reinterpret the data
        DataBuffer<float> floatDb = DataBuffer.Create<float>(4 * 3);
        int numFloatsRead = reader.ReadBlob("Test7", floatDb, false);
        Assert.IsTrue(numFloatsRead == floatDb.Length);
        AssertArrayEquals<Vector3>(spanOfVectors, floatDb.Span.Reinterpret<float, Vector3>(), numFloatsRead / 3);
        floatDb.Dispose();

        // Try reading
        Assert.IsTrue(reader.TryReadBlob("Test8", out IDataBuffer? tryDb));
        Assert.IsTrue(tryDb != null);
        Assert.IsTrue(tryDb!.Length == spanOfVectors.Length);
        Assert.IsTrue(tryDb!.ElementType == typeof(Vector3));
        AssertArrayEquals<Vector3>(spanOfVectors, tryDb.Bytes.Reinterpret<byte, Vector3>(), spanOfVectors.Length);
        tryDb!.Dispose();

        // Try reading 2
        Assert.IsTrue(reader.TryReadBlob<Vector3>("Test9", null, out DataBuffer<Vector3>? tryDb2));
        Assert.IsTrue(tryDb2 != null);
        Assert.IsTrue(tryDb2!.Length == spanOfVectors.Length);
        Assert.IsTrue(tryDb2!.ElementType == typeof(Vector3));
        AssertArrayEquals<Vector3>(spanOfVectors, tryDb2!.Bytes.Reinterpret<byte, Vector3>(), spanOfVectors.Length);
        tryDb2!.Dispose();

        Assert.IsTrue(reader.PeekArrayCount("EndOfTest") == 0);
        Assert.IsTrue(reader.ReadByte("EndOfTest") == endOfTest);

        dbOfVector3s.Dispose();

      });
    }

    [TestCase]
    public void ReadWrite_Grouping()
    {
      RunTest((input) =>
      {
        byte endOfTest = 42;

        IPrimitiveWriter writer = input.CreateWriter();
        writer.BeginWriteGroup("TestGroup");

        writer.Write("Key", "testA");
        writer.Write("Value", 52526);

        writer.EndWriteGroup();

        writer.BeginWriteGroup("GroupArray", 5);

        for (int i = 0; i < 5; i++)
        {
          writer.BeginWriteGroup("GroupItem");
          writer.Write("Key", i.ToString());
          writer.Write("Value", i + 1);
          writer.EndWriteGroup();
        }

        writer.EndWriteGroup();

        writer.BeginWriteGroup("NullArray", 0);
        writer.EndWriteGroup();

        writer.BeginWriteGroup("HeteroArray", 4);
        writer.Write("Single", 5f);
        writer.Write("Primitive", Vector3.UnitX);
        writer.Write("Int", (int) 2);
        writer.Write("String", "Hello World");
        writer.EndWriteGroup();

        writer.Write("EndOfTest", endOfTest);

        IPrimitiveReader reader = input.CreateReader();

        Assert.IsTrue(reader.PeekArrayCount("TestGroup") == 0);
        Assert.IsTrue(reader.BeginReadGroup("TestGroup") == 0);

        string? key = reader.ReadString("Key");
        int value = reader.ReadInt32("Value");
        Assert.IsTrue(key == "testA");
        Assert.IsTrue(value == 52526);

        reader.EndReadGroup();

        Assert.IsTrue(reader.PeekArrayCount("GroupArray") == 5);
        int count = reader.BeginReadGroup("GroupArray");
        Assert.IsTrue(count == 5);

        for (int i = 0; i < 5; i++)
        {
          reader.BeginReadGroup("GroupItem");
          key = reader.ReadString("Key");
          value = reader.ReadInt32("Value");
          reader.EndReadGroup();

          Assert.IsTrue(key == i.ToString());
          Assert.IsTrue(value == (i + 1));
        }

        reader.EndReadGroup();

        Assert.IsTrue(reader.BeginReadGroup("NullArray") == 0);
        reader.EndReadGroup();

        Assert.IsTrue(reader.BeginReadGroup("HeteroArray") == 4);
        Assert.IsTrue(reader.ReadSingle("Single") == 5f);
        Assert.IsTrue(reader.Read<Vector3>("Primitive").Equals(Vector3.UnitX));
        Assert.IsTrue(reader.ReadInt32("Int") == 2);
        Assert.IsTrue(reader.ReadString("String") == "Hello World");
        reader.EndReadGroup();

        Assert.IsTrue(reader.PeekArrayCount("EndOfTest") == 0);
        Assert.IsTrue(reader.ReadByte("EndOfTest") == endOfTest);
      });
    }

    [TestCase]
    public void ReadWrite_NullableExchange()
    {
      RunTest((input) =>
      {
        float? f1 = 52.2f;
        float? f2 = null;

        IPrimitiveWriter writer = input.CreateWriter();
        writer.Write("Test1", f1);
        writer.Write("Test2", f2);

        IPrimitiveReader reader = input.CreateReader();

        Assert.IsTrue(reader.ReadSingle("Test1") == f1);
        Assert.Throws<InvalidCastException>(() =>
        {
          reader.ReadSingle("Test2");
        });
      });
    }

    [TestCase]
    public void ReadWrite_Skip()
    {
      RunTest((input) =>
      {
        IPrimitiveWriter writer = input.CreateWriter();
        writer.Write<Vector3>("Position", Vector3.One);

        NestedPrimitive prim = new NestedPrimitive();
        prim.Position = Vector3.One;
        prim.Rotation = Triad.UnitAxes;
        prim.Instances = new Vector3[] { Vector3.One, Vector3.UnitZ };
        writer.Write<NestedPrimitive>("Nested", prim);
        writer.Write("RotationSpeed", 25f);

        // custom array
        writer.BeginWriteGroup("Array", 3);
        writer.Write("Item", 25f);
        writer.Write("Item", 52f);
        writer.Write("Item", 100f);
        writer.EndWriteGroup();

        IPrimitiveReader reader = input.CreateReader();

        reader.Skip("Position");
        reader.Skip("Nested");
        float rot = reader.ReadSingle("RotationSpeed");
        Assert.IsTrue(rot == 25f);

        reader.BeginReadGroup("Array");
        reader.Skip("Item");
        reader.Skip("Item");
        Assert.IsTrue(reader.ReadSingle("Item") == 100f);
        reader.EndReadGroup();

        reader = input.CreateReader();

        reader.Skip("Position");
        NestedPrimitive prim2 = reader.Read<NestedPrimitive>("Nested");
        reader.Skip("RotationSpeed");

        reader.BeginReadGroup("Array");
        Assert.IsTrue(reader.ReadSingle("Item") == 25f);
        reader.Skip("Item");
        Assert.IsTrue(reader.ReadSingle("Item") == 100f);
        reader.EndReadGroup();

        AssertArrayEquals<Vector3>(prim2.Instances, prim.Instances, prim.Instances.Length);
        Assert.IsTrue(prim2.GridCoords?.Length == 0);
      });
    }

    private enum EnumByte : byte
    {
      Test1 = 25,
      Test2 = 50
    }

    private enum EnumSByte : sbyte
    {
      Test1 = -25,
      Test2 = 10
    }

    private enum EnumInt16 : short
    {
      Test1 = -6,
      Test2 = 7
    }

    private enum EnumUInt16 : ushort
    {
      Test1 = 6,
      Test2 = 7
    }

    private enum EnumInt32 : int
    {
      Test1 = -23,
      Test2 = 56
    }

    private enum EnumUInt32 : uint
    {
      Test1 = 100,
      Test2 = 150
    }

    private enum EnumInt64 : long
    {
      Test1 = -250,
      Test2 = 500
    }

    private enum EnumUInt64 : ulong
    {
      Test1 = 251,
      Test2 = 100
    }

    private struct NestedPrimitive : IPrimitiveValue
    {
      public Vector3 Position;
      public Triad Rotation;
      public Vector3[]? Instances;
      public Int4[]? GridCoords;

      public void Read(IPrimitiveReader input)
      {
        Position = input.Read<Vector3>("Position");
        Rotation = input.Read<Triad>("Rotation");
        Instances = input.ReadArray<Vector3>("Instances");
        GridCoords = input.ReadArray<Int4>("GridCoords");
      }

      public void Write(IPrimitiveWriter output)
      {
        output.Write<Vector3>("Position", Position);
        output.Write<Triad>("Rotation", Rotation);
        output.Write<Vector3>("Instances", Instances);
        output.Write<Int4>("GridCoords", GridCoords);
      }
    }
  }
}