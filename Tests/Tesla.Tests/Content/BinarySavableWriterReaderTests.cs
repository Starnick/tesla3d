﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.IO.Compression;
using System.Reflection;
using System.Xml;
using NUnit.Framework;
using Tesla.Direct3D11.Graphics;
using Tesla.Graphics;

#nullable enable

namespace Tesla.Content
{
  [TestFixture]
  public class BinarySavableWriterReaderTests
  {
    [OneTimeSetUp]
    public void Setup()
    {
      Engine.Initialize();
      Engine.Instance.Services.AddService(typeof(IRenderSystem), new D3D11RenderSystem());
    }

    [OneTimeTearDown]
    public void Teardown()
    {
      Engine.Destroy();
    }

    private (Func<SavableWriteFlags, ISavableWriter> getWriter, Func<ISavableReader> getReader) CreateBinaryInputOutput()
    {
      MemoryStream ms = new MemoryStream();
      Func<SavableWriteFlags, ISavableWriter> getWriter = (SavableWriteFlags flags) =>
      {
        ms.Position = 0;
        ms.SetLength(0);
        return new BinarySavableWriter(ms, flags, true);
      };

      Func<ISavableReader> getReader = () =>
      {
        ms.Position = 0;
        return new BinarySavableReader(Engine.Instance.Services, ms, true);
      };

      return (getWriter, getReader);
    }

    private void RunTest(Action<Func<SavableWriteFlags, ISavableWriter>, Func<ISavableReader>> test)
    {
      var (getWriter, getReader) = CreateBinaryInputOutput();
      test(getWriter, getReader);
    }

    [TestCase]
    public void TestWrite()
    {
      RunTest((getWriter, getReader) =>
      {
        BoundingBox box = new BoundingBox();
        box.Center = new Vector3(15, 52, 50);
        box.Extents = new Vector3(10, 20, 50);
        BoundingSphere sphere = new BoundingSphere();
        sphere.Radius = 100;
        BoundingCapsule capsule = new BoundingCapsule();
        capsule.Radius = 50;

        BoundingVolume? bv = null;

        ISavableWriter writer = getWriter(SavableWriteFlags.None);
        writer.WriteSavable<BoundingBox>("Box", box);
        writer.WriteSavable<BoundingVolume?>("Optional", bv);
        writer.WriteSharedSavable<BoundingSphere>("Sphere", sphere);
        writer.WriteExternalSavable<BoundingCapsule>("Capsule", capsule);
        writer.Flush();

        ISavableReader reader = getReader();
        BoundingBox bb1 = reader.ReadSavable<BoundingBox>("Box");
        BoundingVolume? bv1 = reader.ReadSavable<BoundingVolume?>("Optional");
        BoundingSphere bs1 = reader.ReadSharedSavable<BoundingSphere>("Sphere");
        BoundingCapsule bc1 = reader.ReadExternalSavable<BoundingCapsule>("Capsule");
      });
    }

    [TestCase]
    public void TestWriteModel()
    {
      RunTest((getWriter, getReader) =>
      {
        FileResourceRepository repo = new FileResourceRepository("Data");
        repo.OpenConnection();
        ContentManager cm = new ContentManager(repo);
        cm.ResourceImporters.Add(new Tesla.Content.AssimpModelImporter());
        cm.ResourceImporters.Add(new Tesla.Content.TeximpTextureImporter());

        var scene = cm.Load<Scene.Node>("Models/A21_Viper/A21_Viper.obj");

        MemoryStream jsonStream = new MemoryStream();
        JsonSavableWriter jsonSavWriter = new JsonSavableWriter(jsonStream, SavableWriteFlags.UseCompression | SavableWriteFlags.VerboseTypes, true);
        jsonSavWriter.WriteRootSavable<Scene.Node>(scene);
        jsonStream.Position = 0;

        string jsonText = new StreamReader(jsonStream).ReadToEnd();
        jsonStream.Position = 0;

        JsonSavableReader jsonSavReader = new JsonSavableReader(Engine.Instance.Services, jsonStream, true);
        var sceneFromJson = jsonSavReader.ReadRootSavable<Scene.Node>();

        ISavableWriter writer = getWriter(SavableWriteFlags.UseCompression);
        writer.WriteSavable<Scene.Node>("Object", scene);
        writer.Flush();

        ISavableReader reader = getReader();
        var scene2 = reader.ReadSavable<Scene.Node>("Object");
      });
    }

    [TestCase]
    public void TestPrimJson()
    {
      MemoryStream ms = new MemoryStream();
      JsonPrimitiveWriter writer = new JsonPrimitiveWriter(ms, true);
      writer.Write("Position", new Vector3(1, 2, 3));
      writer.Write("Radius", 5f);
      writer.Write("UpVectir", new Vector3(0, 1, 0));

      writer.Flush();

      ms.Position = 0;
      JsonPrimitiveReader reader = new JsonPrimitiveReader(ms);
      reader.Read<Vector3>("Position");
      reader.Read<Vector3>("UpVectir");
      reader.ReadSingle("Radius");
    }
  }
}
