﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using NUnit.Framework;
using Tesla.Content;
using Tesla.Direct3D11.Graphics;
using Tesla.Framework;
using Tesla.Graphics;
using Tesla.Scene;

namespace Tesla.Content
{
  [TestFixture]
  public class SavableReaderWriterTests
  {
    private ContentManager m_content;

    [OneTimeSetUp]
    public void Setup()
    {
      Engine.Initialize();
      Engine.Instance.Services.AddService(typeof(IRenderSystem), new D3D11RenderSystem());

      FileResourceRepository repo = new FileResourceRepository("Temp");
      if (!repo.Exists)
        Directory.CreateDirectory(repo.RootPath);

      m_content = new ContentManager(new FileResourceRepository("Temp"));
      m_content.AddAdditionalRepository(new FileResourceRepository("Data"), true);
    }

    [OneTimeTearDown]
    public void Teardown()
    {
      ClearTempDir();
      m_content.Dispose();
      Engine.Destroy();
    }

    public void ClearTempDir()
    {
      foreach (IResourceFile file in m_content.ResourceRepository.EnumerateResourceFiles(true))
        file.Delete();
    }

    public class VertexLayouts : ISavable
    {
      private VertexLayout[] m_layouts;
      private BoundingBox m_bb;

      private VertexLayouts() { }

      public VertexLayouts(VertexLayout[] layouts, BoundingBox abb)
      {
        m_layouts = layouts;
        m_bb = abb;
      }

      public void Read(ISavableReader input)
      {
        m_bb = input.ReadSavable<BoundingBox>("Bounds");
        m_layouts = input.ReadSharedSavableArray<VertexLayout>("Layouts");
      }

      public void Write(ISavableWriter output)
      {
        output.WriteSavable<BoundingBox>("Bounds", m_bb);
        output.WriteSharedSavable<VertexLayout>("Layouts", m_layouts);
      }

      public void AssertNotSame(VertexLayouts other)
      {
        Assert.IsNotNull(other.m_layouts);
        Assert.IsNotNull(other.m_bb);

        Assert.IsTrue(m_layouts.Length == other.m_layouts.Length);

        for (int i = 0; i < m_layouts.Length; i++)
          Assert.IsTrue(m_layouts[i].Equals(other.m_layouts[i]));

        Assert.IsTrue(m_bb.Equals(other.m_bb));
      }
    }

    /*
    [TestCase]
    public void TestXmlSavable_Simple()
    {
      StringBuilder xml = new StringBuilder();
      XmlSavableWriter writer = new XmlSavableWriter(xml);

      VertexLayout layout = new VertexLayout(new VertexElement(VertexSemantic.Position, 0, VertexFormat.Float3),
          new VertexElement(VertexSemantic.TextureCoordinate, 0, VertexFormat.Float2),
          new VertexElement(VertexSemantic.Normal, 0, VertexFormat.Float3));

      VertexLayout layout2 = new VertexLayout(new VertexElement(VertexSemantic.TextureCoordinate, 0, VertexFormat.Float4), new VertexElement(VertexSemantic.TextureCoordinate, 1, VertexFormat.Float4),
          new VertexElement(VertexSemantic.TextureCoordinate, 2, VertexFormat.Float4), new VertexElement(VertexSemantic.TextureCoordinate, 3, VertexFormat.Float4));

      VertexLayouts testObj = new VertexLayouts(new VertexLayout[] { layout, layout2 }, new BoundingBox(new Vector3(1, 52, 2), new Vector3(1, 2, 3)));

      IResourceFile file = m_content.ResourceRepository.GetResourceFile("Layouts.xml");
      XmlResourceExporter exporter = new XmlResourceExporter();
      exporter.Save(file, testObj);

      using (XmlSavableReader reader = new XmlSavableReader(m_content.ServiceProvider, file.OpenRead(), new ExternalReferenceResolver(m_content, file)))
      {
        VertexLayouts returnedTestObj = reader.ReadSavable<VertexLayouts>("Object");

        testObj.AssertNotSame(returnedTestObj);
      }
    }

    [TestCase]
    public void TestXmlSavable_Simple2()
    {
      StringBuilder xml = new StringBuilder();
      XmlSavableWriter writer = new XmlSavableWriter(xml);

      VertexLayout layout = new VertexLayout(new VertexElement(VertexSemantic.Position, 0, VertexFormat.Float3),
          new VertexElement(VertexSemantic.TextureCoordinate, 0, VertexFormat.Float2),
          new VertexElement(VertexSemantic.Normal, 0, VertexFormat.Float3));

      VertexLayout layout2 = new VertexLayout(new VertexElement(VertexSemantic.TextureCoordinate, 0, VertexFormat.Float4), new VertexElement(VertexSemantic.TextureCoordinate, 1, VertexFormat.Float4),
          new VertexElement(VertexSemantic.TextureCoordinate, 2, VertexFormat.Float4), new VertexElement(VertexSemantic.TextureCoordinate, 3, VertexFormat.Float4));

      VertexLayouts testObj = new VertexLayouts(new VertexLayout[] { layout, layout2 }, new BoundingBox(new Vector3(1, 52, 2), new Vector3(1, 2, 3)));

      IResourceFile file = m_content.ResourceRepository.GetResourceFile("Layouts2.xml");
      IExternalReferenceHandler handler = ExternalReferenceHandler.CreateXmlDefaultHandler(file, SavableWriteFlags.OverwriteExistingResource | SavableWriteFlags.SharedAsExternal);

      XmlResourceExporter exporter = new XmlResourceExporter();
      exporter.Save(handler, testObj);

      using (XmlSavableReader reader = new XmlSavableReader(m_content.ServiceProvider, file.OpenRead(), new ExternalReferenceResolver(m_content, file)))
      {
        VertexLayouts returnedTestObj = reader.ReadSavable<VertexLayouts>("Object");

        testObj.AssertNotSame(returnedTestObj);
      }
    }

    [TestCase]
    [Ignore("refactoring")]
    public void TestXmlSavable_Scene()
    {
      ContentManager dataCm = new ContentManager(new FileResourceRepository("Data"));

      Node root = new Node("Starship");

      Mesh msh = new Mesh("Starship_Geo");
      msh.MaterialDefinition = dataCm.Load<MaterialDefinition>("Materials/PirateShip.temd");

      BoxGenerator boxGen = new BoxGenerator();
      boxGen.Extents = new Vector3(5, 10, 100);
      boxGen.BuildMeshData(msh.MeshData, GenerateOptions.All);

      msh.MeshData.Compile();
      msh.Translation = new Vector3(100, 50, 22);
      msh.Rotation = Quaternion.FromAngleAxis(Angle.Pi, Vector3.UnitZ);
      root.Children.Add(msh);
      root.Update(GameTime.ZeroTime);

      //test extended properties
      root.ExtendedProperties.Add(0, new SceneHints(root));

      IResourceFile file = m_content.ResourceRepository.GetResourceFile("Starship.xml");
      IExternalReferenceHandler handler = ExternalReferenceHandler.CreateBinaryDefaultHandler(file, SavableWriteFlags.OverwriteExistingResource | SavableWriteFlags.SharedAsExternal, new ExternalComparer());

      handler.RegisterWriter(new XmlMaterialWriter());
      handler.RegisterWriter(new XmlMaterialDefinitionWriter());
      handler.SetGetResourceNameDelegate<MaterialDefinition>((IExternalReferenceHandler exthandler, ISavable savable) =>
      {
        return (savable as MaterialDefinition).ScriptFileName + "_MaterialDef";
      });

      XmlResourceExporter exporter = new XmlResourceExporter();
      exporter.Save(handler, root);

      Node returnedRoot = m_content.Load<Node>("Starship.xml");

      Assert.IsTrue(root.ExtendedProperties.GetCountOf<SceneHints>() == 1);

      SceneHints hintTest;
      Assert.IsTrue(root.ExtendedProperties.TryGetValueAs<SceneHints>(0, out hintTest));
      Assert.IsNotNull(hintTest);
    }

    [TestCase]
    public void TestXmlSavable_Group()
    {
      SavableWithCollections testObj = new SavableWithCollections(new Vector3[] { new Vector3(1, 2, 10), new Vector3(25.5f, 1005.2f, 5f), new Vector3(5, 10, 2) });

      IResourceFile file = m_content.ResourceRepository.GetResourceFile("ObjectWithGrouping.xml");
      IExternalReferenceHandler handler = ExternalReferenceHandler.CreateXmlDefaultHandler(file, SavableWriteFlags.OverwriteExistingResource | SavableWriteFlags.SharedAsExternal);

      XmlResourceExporter exporter = new XmlResourceExporter();
      exporter.Save(handler, testObj);

      using (XmlSavableReader reader = new XmlSavableReader(m_content.ServiceProvider, file.OpenRead(), new ExternalReferenceResolver(m_content, file)))
      {
        SavableWithCollections newObj = reader.ReadSavable<SavableWithCollections>("Object");

        Assert.IsTrue(newObj.IsSame(testObj));
      }
    }
    

    [TestCase]
    public void TestBinarySavable_Group()
    {
      SavableWithCollections testObj = new SavableWithCollections(new Vector3[] { new Vector3(1, 2, 10), new Vector3(25.5f, 1005.2f, 5f), new Vector3(5, 10, 2) });

      IResourceFile file = m_content.ResourceRepository.GetResourceFile("ObjectWithGrouping.tebo");
      IExternalReferenceHandler handler = ExternalReferenceHandler.CreateXmlDefaultHandler(file, SavableWriteFlags.OverwriteExistingResource | SavableWriteFlags.SharedAsExternal);

      BinaryResourceExporter exporter = new BinaryResourceExporter();
      exporter.Save(handler, testObj);

      using (BinarySavableReader reader = new BinarySavableReader(m_content.ServiceProvider, file.OpenRead(), new ExternalReferenceResolver(m_content, file)))
      {
        SavableWithCollections newObj = reader.ReadSavable<SavableWithCollections>("Object");

        Assert.IsTrue(newObj.IsSame(testObj));
      }
    }
    */

    [TestCase]
    [Ignore("refactoring")]
    public void TestBinary_LoadVersion1()
    {
      IResourceFile file = m_content.QueryResourceFile("ContentVersioning/Arial_10_V1TEBO.tebo");

      BinaryResourceImporter importer = new BinaryResourceImporter();
      ISavable obj = importer.Load(file, m_content, null);
      Assert.IsNotNull(obj);
      Assert.IsTrue(obj is SpriteFont);
    }

    [TestCase]
    [Ignore("refactoring")]
    public void TestBinary_LoadObjectWithOldVersion()
    {
      //uncomment out to create a new version of the file
      // ObjectWithNewProperties testObj = new ObjectWithNewProperties() { Translation = new Vector3(100, 50, 25), Rotation = Quaternion.FromAngleAxis(Angle.Pi, Vector3.UnitY) };

      //  IResourceFile file = m_content.ResourceRepository.GetResourceFile("ObjectVersion1.tebo");
      //    BinaryResourceExporter exporter = new BinaryResourceExporter();
      //   exporter.Save(file, testObj);

      //Load two different versions of the same object, version 1 has just one property, version 2 added a new one

      IResourceFile file = m_content.QueryResourceFile("ContentVersioning/ObjectVersion1.tebo");

      BinaryResourceImporter importer = new BinaryResourceImporter();
      ObjectWithNewProperties loadedObj = importer.Load(file, m_content, null) as ObjectWithNewProperties;

      Assert.IsNotNull(loadedObj);

      IResourceFile file2 = m_content.QueryResourceFile("ContentVersioning/ObjectVersion2.tebo");

      ObjectWithNewProperties loadedObj2 = importer.Load(file2, m_content, null) as ObjectWithNewProperties;

      Assert.IsNotNull(loadedObj2);
    }

    [SavableVersion(2)]
    //[SavableVersion(1)]
    public class ObjectWithNewProperties : ISavable
    {
      public Vector3 Translation; //Present since version 1
      public Quaternion Rotation; //Added in version 2

      public void Read(ISavableReader input)
      {
        int versionNum = input.GetVersion(typeof(ObjectWithNewProperties));

        input.Read<Vector3>("Translation", out Translation);

        if (versionNum >= 2)
          input.Read<Quaternion>("Rotation", out Rotation);
      }

      public void Write(ISavableWriter output)
      {
        output.Write<Vector3>("Translation", Translation);
        output.Write<Quaternion>("Rotation", Rotation);
      }
    }

    /*
    [TestCase]
    public void TestEntity_Xml()
    {
      Entity gun = new Entity("Turret_PulseCannon");
      gun.Add(new TransformProperty());
      gun.Add(new HealthProperty(100));
      gun.Add(new AmmoProperty(25));

      IResourceFile file = m_content.ResourceRepository.GetResourceFile("Turret_PulseCannon_Template.xml");
      IExternalReferenceHandler handler = ExternalReferenceHandler.CreateXmlDefaultHandler(file, SavableWriteFlags.OverwriteExistingResource | SavableWriteFlags.SharedAsExternal);

      XmlResourceExporter exporter = new XmlResourceExporter();
      exporter.Save(handler, gun);

      Type t = Type.GetType("Tesla.Tests.SavableReaderWriterTestFixture+HealthProperty, Tesla.Tests");

      using (XmlSavableReader reader = new XmlSavableReader(m_content.ServiceProvider, file.OpenRead(), new ExternalReferenceResolver(m_content, file)))
      {
        Entity newObj = reader.ReadSavable<Entity>("Object");
      }
    }
    */

    private class ExternalComparer : IEqualityComparer<ISavable>
    {
      public bool Equals(ISavable x, ISavable y)
      {
        if (x is Effect && y is Effect)
        {
          Effect ex = x as Effect;
          Effect ey = y as Effect;

          byte[] exdata = ex.EffectByteCode;
          byte[] eydata = ex.EffectByteCode;

          if (exdata.Length == eydata.Length)
          {
            for (int i = 0; i < exdata.Length; i++)
            {
              if (exdata[i] != eydata[i])
                return false;
            }

            return true;
          }

          return false;
        }

        return Object.ReferenceEquals(x, y);
      }

      public int GetHashCode(ISavable obj)
      {
        if (obj is Effect)
          return (obj as Effect).Name.GetHashCode();

        unchecked
        {
          return obj.GetHashCode();
        }
      }
    }

    /*
    private class XmlMaterialDefinitionWriter : IExternalReferenceWriter
    {
      private Type m_targetType;

      public Type TargetType
      {
        get
        {
          return m_targetType;
        }
      }

      public string ResourceExtension
      {
        get
        {
          return ".xml";
        }
      }

      public XmlMaterialDefinitionWriter()
      {
        m_targetType = typeof(MaterialDefinition);
      }

      public void WriteSavable<T>(IResourceFile outputResourceFile, IExternalReferenceHandler externalHandler, T value) where T : class, ISavable
      {
        if (outputResourceFile == null)
          throw new ArgumentNullException("outputResourceFile");

        if (externalHandler == null)
          throw new ArgumentNullException("externalHandler");

        if (value == null)
          throw new ArgumentNullException("savable");

        if (!m_targetType.IsAssignableFrom(value.GetType()))
          throw new InvalidCastException("TypeMismatch");

        if (outputResourceFile.Extension != ResourceExtension)
          throw new ArgumentException("InvalidResourceExtension");

        Stream output = outputResourceFile.OpenWrite();

        using (XmlSavableWriter xmlWriter = new XmlSavableWriter(output, externalHandler.WriteFlags, externalHandler))
          xmlWriter.WriteSavable<T>("Object", value);
      }
    }
    */

    public class HealthProperty : Property.Float
    {
      public HealthProperty() : base(0, true) { }

      public HealthProperty(float value) : base(value, true) { }

      protected override void OnChanged()
      {
      }
    }

    public class AmmoProperty : Property.Int
    {
      public AmmoProperty() : base(0, true) { }

      public AmmoProperty(int value) : base(value, true) { }

      protected override void OnChanged()
      {
      }
    }

    /*
    private class XmlMaterialWriter : IExternalReferenceWriter
    {
      private Type m_targetType;

      public Type TargetType
      {
        get
        {
          return m_targetType;
        }
      }

      public string ResourceExtension
      {
        get
        {
          return ".xml";
        }
      }

      public XmlMaterialWriter()
      {
        m_targetType = typeof(Material);
      }

      public void WriteSavable<T>(IResourceFile outputResourceFile, IExternalReferenceHandler externalHandler, T value) where T : class, ISavable
      {
        if (outputResourceFile == null)
          throw new ArgumentNullException("outputResourceFile");

        if (externalHandler == null)
          throw new ArgumentNullException("externalHandler");

        if (value == null)
          throw new ArgumentNullException("savable");

        if (!m_targetType.IsAssignableFrom(value.GetType()))
          throw new InvalidCastException("TypeMismatch");

        if (outputResourceFile.Extension != ResourceExtension)
          throw new ArgumentException("InvalidResourceExtension");

        Stream output = outputResourceFile.OpenWrite();

        using (XmlSavableWriter xmlWriter = new XmlSavableWriter(output, externalHandler.WriteFlags, externalHandler))
          xmlWriter.WriteSavable<T>("Object", value);
      }
    }
    */

    private class SavableWithCollections : ISavable
    {
      private List<Vector3> m_values;

      public SavableWithCollections()
      {
        m_values = new List<Vector3>();
      }

      public SavableWithCollections(params Vector3[] values)
      {
        m_values = new List<Vector3>(values);
      }

      public bool IsSame(SavableWithCollections obj)
      {
        if (m_values.Count != obj.m_values.Count)
          return false;

        for (int i = 0; i < m_values.Count; i++)
        {
          if (!m_values[i].Equals(obj.m_values[i]))
            return false;
        }

        return true;
      }

      public void Read(ISavableReader input)
      {
        int count = input.BeginReadGroup("Vector3Array");

        for (int i = 0; i < count; i++)
        {
          m_values.Add(input.Read<Vector3>("Item"));
        }

        input.EndReadGroup();
      }

      public void Write(ISavableWriter output)
      {
        output.BeginWriteGroup("Vector3Array", m_values.Count);

        for (int i = 0; i < m_values.Count; i++)
        {
          output.Write<Vector3>("Item", m_values[i]);
        }

        output.EndWriteGroup();
      }
    }
  }
}
