﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using NUnit.Framework;
using Tesla.Direct3D11.Graphics;
using Tesla.Graphics;

namespace Tesla.Content
{
  [TestFixture]
  public class ImageImporterTests : BaseIOTestFixture
  {
    [TestCase]
    public void TestImageHelper_GenerateMipMapsBitmap()
    {
      Image bitmap = Image.FromFile(GetInputFile("Textures/Meriwether.png"));
      Assert.IsNotNull(bitmap);

      DataBuffer<Color> data = ImageHelper.FromBitmap(bitmap as Bitmap);
      Assert.IsNotNull(data);

      DataBuffer<Color>[] mipmaps = ImageHelper.GenerateMipChain(data, bitmap.Width, bitmap.Height);

      for (int i = 0; i < mipmaps.Length; i++)
      {
        int width = bitmap.Width;
        int height = bitmap.Height;

        Texture.CalculateMipLevelDimensions(i, ref width, ref height);

        Image mipBitmap = ImageHelper.ToBitmap(mipmaps[i], width, height);
        Assert.IsNotNull(mipBitmap);

        String outPath = GetOutputFile(String.Format("Meriwether_Bitmap_{0}.png", i));
        mipBitmap.Save(outPath);
      }
    }

    [TestCase]
    public void TestImageHelper_GenerateMipMapsSpriteBatch()
    {
      Image bitmap = Image.FromFile(GetInputFile("Textures/Meriwether.png"));
      Assert.IsNotNull(bitmap);

      DataBuffer<Color> data = ImageHelper.FromBitmap(bitmap as Bitmap);
      Assert.IsNotNull(data);

      using IRenderSystem renderSystem = new D3D11RenderSystem();

      DataBuffer<Color>[] mipmaps = ImageHelper.GenerateMipChain(renderSystem.ImmediateContext, data, bitmap.Width, bitmap.Height);

      for (int i = 0; i < mipmaps.Length; i++)
      {
        int width = bitmap.Width;
        int height = bitmap.Height;

        Texture.CalculateMipLevelDimensions(i, ref width, ref height);

        Image mipBitmap = ImageHelper.ToBitmap(mipmaps[i], width, height);
        Assert.IsNotNull(mipBitmap);

        String outPath = GetOutputFile(String.Format("Meriwether_Spritebatch_{0}.png", i));
        mipBitmap.Save(outPath);
      }
    }

    [TestCase]
    public void TestImportTexture_Teximp()
    {
      using IRenderSystem renderSystem = new D3D11RenderSystem();

      TextureImporterParameters importParams = new TextureImporterParameters();
      importParams.GenerateMipMaps = true;

      ReadWriteImage(renderSystem, importParams, "Textures/Meriwether.png", "Meriwether.texu");

      importParams.ResizePowerOfTwo = true;

      ReadWriteImage(renderSystem, importParams, "Textures/Meriwether.png", "Meriwether_resized.texu");

      importParams.TextureFormat = TextureConversionFormat.DXTCompression;

      ReadWriteImage(renderSystem, importParams, "Textures/Meriwether.png", "Meriwether_dxt_resized.texu");

      importParams.FlipImage = true;

      ReadWriteImage(renderSystem, importParams, "Textures/Meriwether.png", "Meriwether_dxt_resized_flipped.texu");
    }

    [TestCase]
    public void TestImportTexture_LoadPerformance()
    {
      System.Threading.Tasks.Parallel.For(0, 4, (int i) =>
      {
        TextureImporterParameters importParams = new TextureImporterParameters();

        switch (i)
        {
          case 0:
            importParams.GenerateMipMaps = true;
            Performance_TestCase(importParams, "Mips", false);
            break;
          case 1:
            importParams.GenerateMipMaps = false;
            //Performance_TestCase(importParams, "NoMips", false);
            break;
          case 2:
            importParams.GenerateMipMaps = true;
            importParams.TextureFormat = TextureConversionFormat.DXTCompression;
            //Performance_TestCase(importParams, "DXTMips", false);
            break;
          case 3:
            importParams.GenerateMipMaps = false;
            importParams.TextureFormat = TextureConversionFormat.DXTCompression;
            //Performance_TestCase(importParams, "DXTNoMips", true); //Only do 2k, 4k tests without mipmaps
            break;
        }
      });

      String dir = Path.GetDirectoryName(GetOutputFile("dummy.txt"));
      foreach (String file in Directory.EnumerateFiles(dir))
      {
        if (Path.GetExtension(file) != ".texu")
          continue;

        Stopwatch watch = new Stopwatch();
        watch.Start();

        using (Stream input = File.OpenRead(file))
          TextureData.Read(input);

        watch.Stop();

        TestContext.Out.WriteLine("Loaded {0} in {1} ms.", Path.GetFileNameWithoutExtension(file), watch.ElapsedMilliseconds);
      }
    }

    private void Performance_TestCase(TextureImporterParameters importParams, String testCase, bool stressTest)
    {
      if (stressTest)
      {
        String meriwether4k = GetInputFile("Textures/Meriwether_4096x4096.jpg");
        String meriwether2k = GetInputFile("Textures/Meriwether_2048x2048.jpg");

        var tex1 = LoadFile(meriwether4k, importParams);
        var tex2 = LoadFile(meriwether2k, importParams);

        String meriwether4k_out = GetOutputFile(String.Format("Meriwether_4096x4096_{0}.texu", testCase));
        String meriwether2k_out = GetOutputFile(String.Format("Meriwether_2048x2048_{0}.texu", testCase));

        WriteFile(meriwether4k_out, tex1.texData, DataCompressionMode.None);
        WriteFile(meriwether2k_out, tex2.texData, DataCompressionMode.None);

        String meriwether4k_out_comp = GetOutputFile(String.Format("Meriwether_4096x4096_{0}_compressed.texu", testCase));
        String meriwether2k_out_comp = GetOutputFile(String.Format("Meriwether_2048x2048_{0}_compressed.texu", testCase));

        WriteFile(meriwether4k_out_comp, tex1.texData, DataCompressionMode.Gzip);
        WriteFile(meriwether2k_out_comp, tex2.texData, DataCompressionMode.Gzip);
      }

      String meriwether1k = GetInputFile("Textures/Meriwether_1024x1024.jpg");
      String meriwether512 = GetInputFile("Textures/Meriwether_512x512.jpg");
      String meriwether256 = GetInputFile("Textures/Meriwether_256x256.jpg");

      var tex3 = LoadFile(meriwether1k, importParams);
      var tex4 = LoadFile(meriwether512, importParams);
      var tex5 = LoadFile(meriwether256, importParams);

      String meriwether1k_out = GetOutputFile(String.Format("Meriwether_1024x1024_{0}.texu", testCase));
      String meriwether512_out = GetOutputFile(String.Format("Meriwether_512x512_{0}.texu", testCase));
      String meriwether256_out = GetOutputFile(String.Format("Meriwether_256x256_{0}.texu", testCase));

      WriteFile(meriwether1k_out, tex3.texData, DataCompressionMode.None);
      WriteFile(meriwether512_out, tex4.texData, DataCompressionMode.None);
      WriteFile(meriwether256_out, tex5.texData, DataCompressionMode.None);

      String meriwether1k_out_comp = GetOutputFile(String.Format("Meriwether_1024x1024_{0}_compressed.texu", testCase));
      String meriwether512_out_comp = GetOutputFile(String.Format("Meriwether_512x512_{0}_compressed.texu", testCase));
      String meriwether256_out_comp = GetOutputFile(String.Format("Meriwether_256x256_{0}_compressed.texu", testCase));

      WriteFile(meriwether1k_out_comp, tex3.texData, DataCompressionMode.Gzip);
      WriteFile(meriwether512_out_comp, tex4.texData, DataCompressionMode.Gzip);
      WriteFile(meriwether256_out_comp, tex5.texData, DataCompressionMode.Gzip);
    }

    private void WriteFile(String filePath, TextureData texData, DataCompressionMode mode)
    {
      using (Stream str = File.Create(filePath))
        TextureData.Write(texData, str, mode);
    }

    private (TextureData texData, double elapsedMs) LoadFile(String filePath, TextureImporterParameters importParams)
    {
      Stopwatch watch = new Stopwatch();
      TextureData texData;
      double elapsed;

      using (Stream str = File.OpenRead(filePath))
      {
        watch.Start();

        texData = TeximpTextureImporter.Load(str, importParams);

        watch.Stop();
        elapsed = watch.ElapsedMilliseconds;

        TestContext.Out.WriteLine("Loaded {0} in {1} ms.", filePath, elapsed);
      }

      return (texData, elapsed);
    }

    private void ReadWriteImage(IRenderSystem renderSystem, TextureImporterParameters importParams, String inputFilename, String outputFilename)
    {
      using (Stream input = File.OpenRead(GetInputFile(inputFilename)))
      {
        using (Texture2D tex = TeximpTextureImporter.Load(input, renderSystem, importParams))
        {
          Assert.IsNotNull(tex);

          TextureData texData = TextureData.From(tex);
          Assert.IsTrue(texData.Validate());

          String outPath = GetOutputFile(outputFilename);
          using (Stream output = File.Create(outPath))
            Assert.IsTrue(TextureData.Write(texData, output, DataCompressionMode.Gzip));
        }
      }
    }
  }
}
