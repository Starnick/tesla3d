﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System.IO;
using NUnit.Framework;

#nullable enable

namespace Tesla.Content
{
  [TestFixture]
  public class BinarySavableCompatabilityTests
  {
    [OneTimeSetUp]
    public void Setup()
    {
      Engine.Initialize();
    }

    [OneTimeTearDown]
    public void Teardown()
    {
      Engine.Destroy();
    }

    [TestCase]
    public void TestCaseOne()
    {
      BinarySavableReader reader = new BinarySavableReader(Engine.Instance.Services, new MemoryStream(CaseOneSub.OldData));
      CaseOneSub oldSub = reader.ReadSavable<CaseOneSub>("Root");
      Assert.IsNotNull(oldSub);

      // Round trip
      reader.Dispose();
      MemoryStream ms = new MemoryStream();
      BinarySavableWriter writer = new BinarySavableWriter(ms);
      writer.WriteSavable<CaseOneSub>("Root", oldSub);
      writer.Flush();

      ms.Position = 0;
      reader = new BinarySavableReader(Engine.Instance.Services, ms);
      CaseOneSub newSub = reader.ReadSavable<CaseOneSub>("Root");
      Assert.IsNotNull(newSub);
    }

    [TestCase]
    public void TestCaseTwo()
    {
      BinarySavableReader reader = new BinarySavableReader(Engine.Instance.Services, new MemoryStream(CaseTwoSub.OldData));
      CaseTwoSub oldSub = reader.ReadSavable<CaseTwoSub>("Root");
      Assert.IsNotNull(oldSub);

      // Round trip
      reader.Dispose();
      MemoryStream ms = new MemoryStream();
      BinarySavableWriter writer = new BinarySavableWriter(ms);
      writer.WriteSavable<CaseTwoSub>("Root", oldSub);
      writer.Flush();

      ms.Position = 0;
      reader = new BinarySavableReader(Engine.Instance.Services, ms);
      CaseTwoSub newSub = reader.ReadSavable<CaseTwoSub>("Root");
      Assert.IsNotNull(newSub);
    }

    [TestCase]
    public void TestCaseThree()
    {
      BinarySavableReader reader = new BinarySavableReader(Engine.Instance.Services, new MemoryStream(CaseThreeRoot.OldData));
      CaseThreeRoot oldSub = reader.ReadSavable<CaseThreeRoot>("Root");
      Assert.IsNotNull(oldSub);

      // Round trip
      reader.Dispose();
      MemoryStream ms = new MemoryStream();
      BinarySavableWriter writer = new BinarySavableWriter(ms);
      writer.WriteSavable<CaseThreeRoot>("Root", oldSub);
      writer.Flush();

      ms.Position = 0;
      reader = new BinarySavableReader(Engine.Instance.Services, ms);
      CaseThreeRoot newSub = reader.ReadSavable<CaseThreeRoot>("Root");
      Assert.IsNotNull(newSub);
    }
  }

  #region Case One - Base class deleted, property type deleted

  /*
  [SavableVersion(1)]
  internal class CaseOneDeleted : ISavable
  {
    public void Read(ISavableReader input)
    {
      string str = input.ReadString("UserName")!;
    }

    public void Write(ISavableWriter output)
    {
      output.Write("UserName", "User.At.SomethingEmail");
    }
  }

  [SavableVersion(1)]
  internal class CaseOneBase : ISavable
  {
    public virtual void Read(ISavableReader input)
    {
      Vector3 center = input.Read<Vector3>("Center");
      CaseOneDeleted[] userDetails = input.ReadSavableArray<CaseOneDeleted>("UserDetails");
    }

    public virtual void Write(ISavableWriter output)
    {
      output.Write<Vector3>("Center", new Vector3(1, 5, 10));
      output.WriteSavable<CaseOneDeleted>("userDetails", new[] { new CaseOneDeleted() });
    }
  }

  [SavableVersion(1)]
  internal class CaseOneSub : CaseOneBase
  {
    public override void Read(ISavableReader input)
    {
      base.Read(input);

      CaseOneDeleted userDetails = input.ReadSavable<CaseOneDeleted>("UserDetails");
      input.Read<Vector3>("Extents");
    }

    public override void Write(ISavableWriter output)
    {
      base.Write(output);

      output.WriteSavable<CaseOneDeleted>("UserDetails", new CaseOneDeleted());
      output.Write<Vector3>("Extents", new Vector3(50, 50, 20));
    }

    public static void GenerateCase()
    {
      DataBufferStream ms = new DataBufferStream();
      BinarySavableWriter writer = new BinarySavableWriter(ms, SavableWriteFlags.None, true);
      writer.WriteSavable<CaseOneSub>("Root", new CaseOneSub());
      writer.Flush();

      StringBuilder builder = new StringBuilder();
      builder.Append("new byte[] {");
      builder.Append(ms.AsSpan()[0]);

      foreach (byte b in ms.AsSpan(1))
      {
        builder.Append(",");
        builder.Append(b);
      }

      builder.Append("};");

      string str = builder.ToString();
    }
  }
  */

  [SavableVersion(2)]
  internal class CaseOneSub : ISavable
  {
    public static readonly byte[] OldData = new byte[] { 84, 69, 66, 79, 3, 0, 123, 0, 0, 95, 0, 3, 38, 84, 101, 115, 108, 97, 46, 67, 111, 110, 116, 101, 110, 116, 46, 67, 97, 115, 101, 79, 110, 101, 66, 97, 115, 101, 44, 32, 84, 101, 115, 108, 97, 46, 84, 101, 115, 116, 115, 1, 37, 84, 101, 115, 108, 97, 46, 67, 111, 110, 116, 101, 110, 116, 46, 67, 97, 115, 101, 79, 110, 101, 83, 117, 98, 44, 32, 84, 101, 115, 108, 97, 46, 84, 101, 115, 116, 115, 1, 41, 84, 101, 115, 108, 97, 46, 67, 111, 110, 116, 101, 110, 116, 46, 67, 97, 115, 101, 79, 110, 101, 68, 101, 108, 101, 116, 101, 100, 44, 32, 84, 101, 115, 108, 97, 46, 84, 101, 115, 116, 115, 1, 17, 1, 15, 11, 0, 0, 128, 63, 11, 0, 0, 160, 64, 11, 0, 0, 32, 65, 16, 1, 1, 17, 17, 2, 7, 22, 85, 115, 101, 114, 46, 65, 116, 46, 83, 111, 109, 101, 116, 104, 105, 110, 103, 69, 109, 97, 105, 108, 18, 2, 17, 2, 7, 22, 85, 115, 101, 114, 46, 65, 116, 46, 83, 111, 109, 101, 116, 104, 105, 110, 103, 69, 109, 97, 105, 108, 18, 15, 11, 0, 0, 72, 66, 11, 0, 0, 72, 66, 11, 0, 0, 160, 65, 16, 18, 127 };

    public virtual void Read(ISavableReader input)
    {
      // Version change, base class no longer exists, userdetails was deprecated. Base userdetails also deprecated
      int version = input.GetVersion(this); // Note: this is dangerous
      if (version == 1)
      {
        // Properties in base that we need to skip
        Vector3 center = input.Read<Vector3>("Center");
        input.Skip("UserDetails");

        // Deprecated in version 1
        input.Skip("UserDetails");
      }
      else
      {
        // In version 2 this is now our property
        Vector3 center = input.Read<Vector3>("Center");
      }

      Vector3 extents = input.Read<Vector3>("Extents");
    }

    public virtual void Write(ISavableWriter output)
    {
      output.Write<Vector3>("Center", new Vector3(1, 5, 10));
      output.Write<Vector3>("Extents", new Vector3(50, 50, 20));
    }
  }

  #endregion

  #region Case Two - Base class was added, need to skip new stuff

  /*
  [SavableVersion(1)]
  internal class CaseTwoSub : ISavable
  {
    public void Read(ISavableReader input)
    {
      Vector3 center = input.Read<Vector3>("Center");
      int radius = input.ReadInt32("Radius");
    }

    public void Write(ISavableWriter output)
    {
      output.Write<Vector3>("Center", new Vector3(50, 25, 1));
      output.Write("Radius", 25);
    }

    public static void GenerateCase()
    {
      DataBufferStream ms = new DataBufferStream();
      BinarySavableWriter writer = new BinarySavableWriter(ms, SavableWriteFlags.None, true);
      writer.WriteSavable<CaseTwoSub>("Root", new CaseTwoSub());
      writer.Flush();

      StringBuilder builder = new StringBuilder();
      builder.Append("new byte[] {");
      builder.Append(ms.AsSpan()[0]);

      foreach (byte b in ms.AsSpan(1))
      {
        builder.Append(",");
        builder.Append(b);
      }

      builder.Append("};");

      string str = builder.ToString();
    }
  }
  */

  [SavableVersion(1)]
  internal class CaseTwoBase : ISavable
  {
    public virtual void Read(ISavableReader input)
    {
      // Won't have a version since we added this in version 2 of the subclass. Probably is better that subclass conditionally calls the base.Read
      // but maybe it needs to populate something...?
      int version = input.GetVersion<CaseTwoBase>();
      if (version == -1)
        return;

      Vector3 center = input.Read<Vector3>("Center");
    }

    public virtual void Write(ISavableWriter output)
    {
      output.Write<Vector3>("Center", new Vector3(50, 25, 1));
    }
  }

  [SavableVersion(2)]
  internal class CaseTwoSub : CaseTwoBase
  {
    public static readonly byte[] OldData = new byte[] { 84, 69, 66, 79, 3, 0, 40, 0, 0, 25, 0, 1, 37, 84, 101, 115, 108, 97, 46, 67, 111, 110, 116, 101, 110, 116, 46, 67, 97, 115, 101, 84, 119, 111, 83, 117, 98, 44, 32, 84, 101, 115, 108, 97, 46, 84, 101, 115, 116, 115, 1, 17, 0, 15, 11, 0, 0, 72, 66, 11, 0, 0, 200, 65, 11, 0, 0, 128, 63, 16, 11, 25, 0, 0, 0, 18, 76 };

    public override void Read(ISavableReader input)
    {
      base.Read(input);

      // Center was moved to base class in version 2. Alternatively could read base conditionally. Probably is better to do that
      int version = input.GetVersion<CaseTwoSub>();
      if (version == 1)
      {
        Vector3 center = input.Read<Vector3>("Center");
      }

      int radius = input.ReadInt32("Radius");
    }

    public override void Write(ISavableWriter output)
    {
      base.Write(output);

      output.Write("Radius", 25);
    }
  }

  #endregion

  #region Case Three - Hierarchy changed a bit

  /*
  [SavableVersion(1)]
  internal class CaseThreeBase : ISavable
  {
    public virtual void Read(ISavableReader input)
    {
      Vector3 center = input.Read<Vector3>("Center");
    }

    public virtual void Write(ISavableWriter output)
    {
      output.Write<Vector3>("Center", new Vector3(50, 20, 15));
    }
  }

  [SavableVersion(2)]
  internal class CaseThreeSubA : CaseThreeBase
  {
    public override void Read(ISavableReader input)
    {
      base.Read(input);

      float radius = input.ReadSingle("Radius");
    }

    public override void Write(ISavableWriter output)
    {
      base.Write(output);

      output.Write("Radius", 25f);
    }
  }

  [SavableVersion(1)]
  internal class CaseThreeSubB : ISavable
  {
    public void Read(ISavableReader input)
    {
      Vector3 center = input.Read<Vector3>("Center");
    }

    public void Write(ISavableWriter output)
    {
      output.Write<Vector3>("Center", new Vector3(50, 20, 15));
    }
  }

  [SavableVersion(1)]
  internal class CaseThreeRoot : ISavable
  {
    public void Read(ISavableReader input)
    {
      CaseThreeBase[] bases = input.ReadSharedSavableArray<CaseThreeBase>("Subs");
      CaseThreeSubB subb = input.ReadSharedSavable<CaseThreeSubB>("SubB");
      CaseThreeSubA suba = input.ReadSavable<CaseThreeSubA>("SubA");
    }

    public void Write(ISavableWriter output)
    {
      output.WriteSharedSavable<CaseThreeBase>("Subs", new[] { new CaseThreeBase(), new CaseThreeSubA() });
      output.WriteSharedSavable<CaseThreeSubB>("SubB", new CaseThreeSubB());
      output.WriteSavable<CaseThreeSubA>("SubA", new CaseThreeSubA());
    }

    public static void GenerateCase()
    {
      DataBufferStream ms = new DataBufferStream();
      BinarySavableWriter writer = new BinarySavableWriter(ms, SavableWriteFlags.None, true);
      writer.WriteSavable<CaseThreeRoot>("Root", new CaseThreeRoot());
      writer.Flush();

      StringBuilder builder = new StringBuilder();
      builder.Append("new byte[] {");
      builder.Append(ms.AsSpan()[0]);

      foreach (byte b in ms.AsSpan(1))
      {
        builder.Append(",");
        builder.Append(b);
      }

      builder.Append("};");

      string str = builder.ToString();
    }
  }
  */
  
  [SavableVersion(1)]
  internal class CaseThreeBase : ISavable
  {
    public virtual void Read(ISavableReader input)
    {
      Vector3 center = input.Read<Vector3>("Center");
    }

    public virtual void Write(ISavableWriter output)
    {
      output.Write<Vector3>("Center", new Vector3(50, 20, 15));
    }
  }

  [SavableVersion(2)]
  internal class CaseThreeSubA : CaseThreeBase
  {
    public override void Read(ISavableReader input)
    {
      base.Read(input);

      float radius = input.ReadSingle("Radius");
    }

    public override void Write(ISavableWriter output)
    {
      base.Write(output);

      output.Write("Radius", 25f);
    }
  }

  [SavableVersion(2)]
  internal class CaseThreeSubB : CaseThreeBase
  {
    public override void Read(ISavableReader input)
    {
      int version = input.GetVersion<CaseThreeSubB>();
      // Version 1 we just had center. Version 2 we changed this to subclass another type that has Center and added a new property
      if (version >= 2)
      {
        base.Read(input);

        int extent = input.ReadInt32("Extent");
      }
      else
      {
        Vector3 center = input.Read<Vector3>("Center");
      }
    }

    public override void Write(ISavableWriter output)
    {
      base.Write(output);

      output.Write("Extent", 5);
    }
  }

  [SavableVersion(1)]
  internal class CaseThreeRoot : ISavable
  {
    public static readonly byte[] OldData = new byte[] { 84, 69, 66, 79, 3, 0, 169, 1, 0, 69, 38, 0, 4, 40, 84, 101, 115, 108, 97, 46, 67, 111, 110, 116, 101, 110, 116, 46, 67, 97, 115, 101, 84, 104, 114, 101, 101, 82, 111, 111, 116, 44, 32, 84, 101, 115, 108, 97, 46, 84, 101, 115, 116, 115, 1, 40, 84, 101, 115, 108, 97, 46, 67, 111, 110, 116, 101, 110, 116, 46, 67, 97, 115, 101, 84, 104, 114, 101, 101, 66, 97, 115, 101, 44, 32, 84, 101, 115, 108, 97, 46, 84, 101, 115, 116, 115, 1, 40, 84, 101, 115, 108, 97, 46, 67, 111, 110, 116, 101, 110, 116, 46, 67, 97, 115, 101, 84, 104, 114, 101, 101, 83, 117, 98, 65, 44, 32, 84, 101, 115, 108, 97, 46, 84, 101, 115, 116, 115, 2, 40, 84, 101, 115, 108, 97, 46, 67, 111, 110, 116, 101, 110, 116, 46, 67, 97, 115, 101, 84, 104, 114, 101, 101, 83, 117, 98, 66, 44, 32, 84, 101, 115, 108, 97, 46, 84, 101, 115, 116, 115, 1, 3, 20, 17, 1, 15, 11, 0, 0, 72, 66, 11, 0, 0, 160, 65, 11, 0, 0, 112, 65, 16, 18, 25, 17, 2, 15, 11, 0, 0, 72, 66, 11, 0, 0, 160, 65, 11, 0, 0, 112, 65, 16, 11, 0, 0, 200, 65, 18, 20, 17, 3, 15, 11, 0, 0, 72, 66, 11, 0, 0, 160, 65, 11, 0, 0, 112, 65, 16, 18, 17, 0, 1, 2, 19, 19, 0, 19, 1, 2, 19, 2, 17, 2, 15, 11, 0, 0, 72, 66, 11, 0, 0, 160, 65, 11, 0, 0, 112, 65, 16, 11, 0, 0, 200, 65, 18, 18, 0 };
    
    public void Read(ISavableReader input)
    {
      CaseThreeBase[] bases = input.ReadSharedSavableArray<CaseThreeBase>("Subs");
      CaseThreeSubB subb = input.ReadSharedSavable<CaseThreeSubB>("SubB");
      CaseThreeSubA suba = input.ReadSavable<CaseThreeSubA>("SubA");
    }

    public void Write(ISavableWriter output)
    {
      output.WriteSharedSavable<CaseThreeBase>("Subs", new[] { new CaseThreeBase(), new CaseThreeSubA() });
      output.WriteSharedSavable<CaseThreeSubB>("SubB", new CaseThreeSubB());
      output.WriteSavable<CaseThreeSubA>("SubA", new CaseThreeSubA());
    }
  }
  
  #endregion
}
