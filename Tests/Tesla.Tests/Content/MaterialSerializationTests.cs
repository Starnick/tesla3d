﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.IO;
using NUnit.Framework;
using Tesla.Direct3D11.Graphics;
using Tesla.Graphics;

namespace Tesla.Content
{
  [TestFixture]
  public class MaterialSerializationTests
  {
    private ContentManager m_content;

    [OneTimeSetUp]
    public void Setup()
    {
      Engine.Initialize();
      Engine.Instance.Services.AddService(typeof(IRenderSystem), new D3D11RenderSystem());

      FileResourceRepository repo = new FileResourceRepository("Temp");
      if (!repo.Exists)
        Directory.CreateDirectory(repo.RootPath);

      m_content = new ContentManager(new FileResourceRepository("Temp"));
      m_content.AddAdditionalRepository(new FileResourceRepository("Data"), true);
    }

    [OneTimeTearDown]
    public void Teardown()
    {
      ClearTempDir();
      m_content.Dispose();
      Engine.Destroy();
    }

    public void ClearTempDir()
    {
      foreach (IResourceFile file in m_content.ResourceRepository.EnumerateResourceFiles(true))
        file.Delete();
    }

    [TestCase]
    public void TestSerializeMaterialDefinition()
    {
      //Pirate ship material is entirely made of anonymous materials
      ContentManager dataCm = new ContentManager(new FileResourceRepository("Data"));

      MaterialDefinition pirateShipMatDef = dataCm.Load<MaterialDefinition>("Materials/PirateShip.temd");

      foreach (Material m in pirateShipMatDef.Values)
        Assert.True(m.IsAnonymous);

      MaterialSerializer serializer = new MaterialSerializer();
      String text = serializer.Serialize(pirateShipMatDef);

      Assert.IsTrue(!String.IsNullOrEmpty(text));

      MaterialDefinition roundTripMatDef = new MaterialParser().ParseDefinition(text, null, dataCm);

      EnsureSame(pirateShipMatDef, roundTripMatDef);
    }

    [TestCase]
    public void TestSerializeMaterialDefinition_SeperateMaterialsScript()
    {
      //Pirate ship material is entirely made of anonymous materials, but we will make all of the materials not anonymous
      ContentManager dataCm = new ContentManager(new FileResourceRepository("Data"));

      MaterialDefinition pirateShipMatDef = dataCm.Load<MaterialDefinition>("Materials/PirateShip.temd");

      foreach (Material m in pirateShipMatDef.Values)
      {
        m.IsAnonymous = false;
        m.ScriptFileName = "PirateShip_Materials";
      }

      //Create the same script in the temp folder and load it using the other content manager. The materials should
      //be the same
      IResourceFile outputFile = m_content.ResourceRepository.GetResourceFile("PirateShip.temd");
      IExternalReferenceHandler handler = ExternalReferenceHandler.CreateBinaryDefaultHandler(outputFile);

      MaterialDefinitionResourceExporter matDefExporter = new MaterialDefinitionResourceExporter();
      matDefExporter.Save(handler, pirateShipMatDef);

      MaterialDefinition roundTripMatDef = m_content.Load<MaterialDefinition>("PirateShip.temd");

      EnsureSame(pirateShipMatDef, roundTripMatDef);
    }

    [TestCase]
    public void TestSerializeMaterialDefinition_SeparateMaterialsScript_Textures()
    {
      //Ship material is entirely made of anonymous materials, but we will make all of the materials not anonymous. The materials also
      //will have textures that should get written out
      ContentManager dataCm = new ContentManager(new FileResourceRepository("Data"));
      dataCm.ResourceImporters.Add(new TeximpTextureImporter());

      MaterialDefinition shipMatDef = dataCm.Load<MaterialDefinition>("Models/A21_Viper/A21_Viper_MaterialDefinition.temd");

      foreach (Material m in shipMatDef.Values)
      {
        m.IsAnonymous = false;
        m.ScriptFileName = "A21_Viper_Materials";
      }

      //Create the same script in the temp folder and load it using the other content manager. The materials should
      //be the same
      IResourceFile outputFile = m_content.ResourceRepository.GetResourceFile("A21_Viper_MaterialDefinition.temd");
      IExternalReferenceHandler handler = ExternalReferenceHandler.CreateBinaryDefaultHandler(outputFile);

      MaterialDefinitionResourceExporter matDefExporter = new MaterialDefinitionResourceExporter();
      matDefExporter.Save(handler, shipMatDef);

      MaterialDefinition roundTripMatDef = m_content.Load<MaterialDefinition>("A21_Viper_MaterialDefinition.temd");

      EnsureSame(shipMatDef, roundTripMatDef);
    }

    [TestCase]
    public void TestSerializeMaterialDefinition_NullMaterial()
    {
      String testMatDef = @"MaterialDefinition TestMatDef
                                    {
                                      Opaque : null
                                    }";

      MaterialParser parser = new MaterialParser();
      MaterialDefinition matDef = parser.ParseDefinition(testMatDef, null, m_content);

      Assert.IsNotNull(matDef);
      Assert.IsTrue(matDef.ContainsKey(RenderBucketID.Opaque));
      Assert.IsTrue(matDef[RenderBucketID.Opaque] == null);

      //Roundtrip the serialization
      MaterialSerializer serializer = new MaterialSerializer();
      String roundTripScript = serializer.Serialize(matDef);

      Assert.IsTrue(!String.IsNullOrEmpty(roundTripScript));
    }

    [TestCase]
    public void TestSerializeMaterialDefinition_NullTexture()
    {
      String testMatDef = @"MaterialDefinition TestMatDef
                                    {
                                       Opaque => LitStandard_Texture
                                      {
                                        ParameterBindings
                                        {
                                          Texture2D DiffuseMap : null
                                          Vector3 MatDiffuse : .75f .35f .5f
                                          Vector3 MatSpecular : .2f .2f .2f
                                          float MatShininess : 20
                                        }
                                      }
                                    }";

      MaterialParser parser = new MaterialParser();
      MaterialDefinition matDef = parser.ParseDefinition(testMatDef, null, m_content);

      Assert.IsNotNull(matDef);
      Assert.IsTrue(matDef.ContainsKey(RenderBucketID.Opaque));
      Assert.IsTrue(matDef[RenderBucketID.Opaque] != null);
      Assert.IsTrue(matDef[RenderBucketID.Opaque].GetParameterBinding("DiffuseMap") != null);
      Assert.IsTrue(matDef[RenderBucketID.Opaque].GetParameterBinding("DiffuseMap").Parameter.GetResource<Texture>() == null);

      //Roundtrip the serialization
      IExternalReferenceHandler extHandler = ExternalReferenceHandler.CreateBinaryDefaultHandler(m_content.QueryResourceFile("Temp.temd"));
      MaterialSerializer serializer = new MaterialSerializer();
      String roundTripScript = serializer.Serialize(matDef, false, extHandler);

      Assert.IsTrue(!String.IsNullOrEmpty(roundTripScript));

      //Setting no handler should still work since we're dealing with a null texture
      roundTripScript = serializer.Serialize(matDef);
      Assert.IsTrue(!String.IsNullOrEmpty(roundTripScript));
    }

    [TestCase]
    public void TestLoadMaterialDefinition_SingleMaterialScript()
    {
      MaterialDefinition matDef = m_content.Load<MaterialDefinition>("Materials/SingleMaterialDefinition.temd");

      Assert.IsNotNull(matDef);
      Assert.IsTrue(matDef[RenderBucketID.Opaque] != null);
    }

    [TestCase]
    public void TestLoadMaterialDefinition_NonAnonymousMaterials()
    {
      MaterialDefinition matDef = m_content.Load<MaterialDefinition>("Materials/PirateShip_NoAnon.temd");

      Assert.IsNotNull(matDef);
      Assert.IsTrue(matDef.Count == 3);
      Assert.IsTrue(matDef[RenderBucketID.Opaque] != null);
      Assert.IsTrue(matDef[RenderBucketID.Transparent] != null);
      Assert.IsTrue(matDef[RenderBucketID.Ortho] != null);
    }

    private void EnsureSame(MaterialDefinition a, MaterialDefinition b)
    {
      Assert.IsTrue(a.Count == b.Count);

      int numProcessed = 0;
      foreach (RenderBucketID key in a.Keys)
      {
        Material matA = null;
        Material matB = null;

        matA = a[key];
        matB = b[key];

        Assert.IsNotNull(matA);
        Assert.IsNotNull(matB);

        EnsureSame(matA, matB);
        numProcessed++;
      }

      Assert.IsTrue(a.Count == numProcessed);
    }

    private void EnsureSame(Material a, Material b)
    {
      Assert.IsTrue(a.Name == b.Name);
      Assert.IsTrue(a.ParentScriptName == b.ParentScriptName);
      Assert.IsTrue(a.ShadowMode.Value == b.ShadowMode.Value);
      Assert.IsTrue(a.ShadowMode.IsInherited == b.ShadowMode.IsInherited);
      Assert.IsTrue(a.TransparencyMode.Value == b.TransparencyMode.Value);
      Assert.IsTrue(a.TransparencyMode.IsInherited == b.TransparencyMode.IsInherited);
      Assert.IsTrue(a.IsAnonymous == b.IsAnonymous);
      Assert.IsTrue(a.IsStandardContent == b.IsStandardContent);
      Assert.IsTrue(a.IsValid == b.IsValid);
      Assert.IsTrue(a.ScriptFileName == b.ScriptFileName);

      Assert.IsTrue(a.Effect.StandardContentName == b.Effect.StandardContentName);
      Assert.IsTrue(a.Effect.ResourceID != b.Effect.ResourceID);
      Assert.IsTrue(a.Effect.SortKey == b.Effect.SortKey);

      Assert.IsTrue(a.ScriptParameterBindingCount == b.ScriptParameterBindingCount);

      foreach (Material.ScriptParameterBinding aBinding in a.ScriptParameterBindings)
      {
        EnsureSame(aBinding, b.GetParameterBinding(aBinding.Name));
      }

      Assert.IsTrue(a.ComputedParameterBindingCount == b.ComputedParameterBindingCount);

      foreach (Material.ComputedParameterBinding aBinding in a.ComputedParameterBindings)
      {
        EnsureSame(aBinding, b.GetComputedParameterBinding(aBinding.Name));
      }

      Assert.IsTrue(a.Passes.Count == b.Passes.Count);

      for (int i = 0; i < a.Passes.Count; i++)
        EnsureSame(a.Passes[i], b.Passes[i]);
    }

    private void EnsureSame(Material.ScriptParameterBinding a, Material.ScriptParameterBinding b)
    {
      Assert.IsTrue(a.DataType == b.DataType);
      Assert.IsTrue(a.IsInherited == b.IsInherited);
      Assert.IsTrue(a.IsValid == b.IsValid);
      Assert.IsTrue(a.Name == b.Name);
    }

    private void EnsureSame(Material.ComputedParameterBinding a, Material.ComputedParameterBinding b)
    {
      Assert.IsTrue(a.IsValid == b.IsValid);
      Assert.IsTrue(a.Name == b.Name);
      Assert.IsTrue(a.Provider.ComputedParameterName == b.Provider.ComputedParameterName);
    }

    private void EnsureSame(MaterialPass a, MaterialPass b)
    {
      Assert.IsTrue(a.Name == b.Name);
      Assert.IsTrue(a.IsInherited == b.IsInherited);
      Assert.IsTrue(a.IsOverride == b.IsOverride);
      Assert.IsTrue(a.PassIndex == b.PassIndex);
      Assert.IsTrue(a.RenderStatesToApply == b.RenderStatesToApply);
      Assert.IsTrue(a.ShaderGroup.Name == b.ShaderGroup.Name);
      Assert.IsTrue(a.ShaderGroup.ShaderGroupIndex == b.ShaderGroup.ShaderGroupIndex);
      Assert.IsTrue(RenderState.IsSameState(a.BlendState, b.BlendState));
      Assert.IsTrue(RenderState.IsSameState(a.DepthStencilState, b.DepthStencilState));
      Assert.IsTrue(RenderState.IsSameState(a.RasterizerState, b.RasterizerState));
    }
  }
}
