struct VsInput
{
  float3 Position : POSITION;
  float2 TexCoords : TEXCOORD;
  float3 Normal : NORMAL;
};

struct VsInputMixed
{
  float3 Normal : NORMAL;
  float3 Position : POSITION;
  float2 TexCoords : TEXCOORD;
};

float4 VS_ZeroInputs(): SV_Position
{
return float4(0, 0, 0, 1);
}

float4 VS_SystemOnlyInputs(uint vId : SV_VertexID): SV_Position
{
  switch (vId % 4)
  {
    case 0:
      return float4(0, 0, 0, 1);
    case 1:
      return float4(1, 0, 0, 1);
    case 2:
      return float4(1, 1, 0, 1);
    case 3:
      return float4(0, 1, 0, 1);
  }
}

float4 VS_WithSystemInputs(VsInput input, uint vId : SV_VertexID): SV_Position
{
  float3 p = input.Position + input.Normal + float3(input.TexCoords, (float) vId);
  return float4(p, 1);

}

float4 VS_Simple(VsInput input): SV_Position
{
  return float4(input.Position, 1);
}

float4 VS_SimpleMixed(VsInputMixed input): SV_Position
{
  return float4(input.Position, 1);
}

float4 PS_White(): SV_Target
{
  return float4(1, 1, 1, 1);
}