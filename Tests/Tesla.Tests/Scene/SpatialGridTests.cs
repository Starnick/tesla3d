﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.IO;
using System.Text;
using NUnit.Framework;
using Tesla.Content;

namespace Tesla.Scene
{
  [TestFixture]
  public class SpatialGridTests
  {
    [TestCase]
    public void TestTileRowExtentList()
    {
      TileRowExtentList list = new TileRowExtentList();
      list.AddOrUpdate(5, 10);
      list.AddOrUpdate(6, 10);
      list.AddOrUpdate(5, 11);

      //Not yet a rectangle...check before and after
      Assert.IsFalse(list.IsRectangle);
      list.AddOrUpdate(6, 11);
      Assert.IsTrue(list.IsRectangle);

      Assert.IsTrue(list.Bounds.Min == new Int2(5, 10));
      Assert.IsTrue(list.Bounds.Max == new Int2(6, 11));

      Vector3 v = new Vector3(1, 2, 3);

      Assert.IsTrue((-v) == new Vector3(-1, -2, -3));
    }

    [TestCase]
    public void TestExtentCreation()
    {
      TileRowExtentList list = new TileRowExtentList();
      list.AddOrUpdate(5, 10);
      list.AddOrUpdate(6, 10);
      list.AddOrUpdate(5, 11);

      GridExtent extent1;
      GridExtent.FromCoveredTiles(list, out extent1, false);

      Assert.IsTrue(!Object.ReferenceEquals(list, extent1.CoveredTiles));
      Assert.IsFalse(extent1.IsRectangle);
      Assert.IsTrue(extent1.Equals(new GridExtent(list)));

      //Mutate already created extent...maybe should have a read-only base class / interface for this
      extent1.CoveredTiles.AddOrUpdate(8, 12);

      list.AddOrUpdate(6, 11);

      GridExtent extent2;
      GridExtent.FromCoveredTiles(list, out extent2, false);

      Assert.IsTrue(list.Count > 0); //Assert that this hasn't been recycled
      Assert.IsTrue(extent2.IsRectangle);

      GridExtent extent3;
      GridExtent.FromCoveredTiles(list, out extent3);

      Assert.IsTrue(list.Count == 0);
      Assert.IsTrue(extent3.IsRectangle);
    }

    [TestCase]
    public void TestSerializeExtent()
    {
      TileRowExtentList list = new TileRowExtentList();
      list.AddOrUpdate(5, 10);
      list.AddOrUpdate(6, 10);
      list.AddOrUpdate(5, 11);

      GridExtent extent = new GridExtent(list);

      Assert.IsFalse(extent.IsRectangle);

      MemoryStream stream = new MemoryStream();
      BinaryPrimitiveWriter writer = new BinaryPrimitiveWriter(stream, true);
      writer.Write<GridExtent>("Extent", extent);

      stream.Position = 0;
      BinaryPrimitiveReader reader = new BinaryPrimitiveReader(stream);
      GridExtent readExtent = reader.Read<GridExtent>("Extent");

      Assert.IsTrue(extent.IsRectangle == readExtent.IsRectangle);
      Assert.IsTrue(extent.CoveredTiles.Count == readExtent.CoveredTiles.Count);
      Assert.IsTrue(extent.Equals(readExtent));

      for (int i = 0; i < extent.CoveredTiles.Count; i++)
      {
        Assert.IsTrue(extent.CoveredTiles[i].Equals(readExtent.CoveredTiles[i]));
      }

      Assert.IsTrue(extent.CoveredRange.Equals(readExtent.CoveredRange));

      /*
      StringBuilder stringOut = new StringBuilder();
      XmlPrimitiveWriter xmlWriter = new XmlPrimitiveWriter(stringOut);

      xmlWriter.Write<GridExtent>("Extent", extent);

      //Mutate list...causes extent to also mutate (maybe change this?)
      list.AddOrUpdate(6, 11);
      Assert.IsTrue(extent.IsRectangle);

      xmlWriter.Write<GridExtent>("AnotherExtent", extent);

      xmlWriter.Flush();

      Console.WriteLine("Test XML output:\n\n");
      Console.WriteLine(stringOut.ToString());*/
    }
  }
}
