﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Tesla.Core.Collections
{
  [TestFixture]
  public class RefListPerformanceTests
  {
    [TestCase]
    public void TestPolygonClip()
    {
      //Test the same polygon clip algorithm using regular list vs reflist

      Vector3 a = new Vector3(-100, 0, 0);
      Vector3 b = new Vector3(20, 0, -100);
      Vector3 c = new Vector3(100, 0, 100);

      Plane plane = new Plane(new Vector3(-1, 0, 0), 0);

      int numIterations = 500000;
      Stopwatch watch = new Stopwatch();
      watch.Start();

      BenchmarkList(numIterations, ref a, ref b, ref c, ref plane);

      watch.Stop();

      Console.WriteLine("Regular list clip: {0} - {1} ms", numIterations, watch.ElapsedMilliseconds);

      watch.Restart();

      BenchmarkRefList(numIterations, ref a, ref b, ref c, ref plane);

      watch.Stop();

      Console.WriteLine("Ref list clip: {0} - {1} ms", numIterations, watch.ElapsedMilliseconds);
    }

    public static void BenchmarkRefList(int numIterations, ref Vector3 a, ref Vector3 b, ref Vector3 c, ref Plane plane)
    {
      RefList<Vector3> subjectPoly = new RefList<Vector3>(3);
      subjectPoly.Add(in a);
      subjectPoly.Add(in b);
      subjectPoly.Add(in c);

      RefList<Vector3> outputPoly = new RefList<Vector3>(4);
      RefList<float> temp = new RefList<float>(3);

      for (int i = 0; i < numIterations; i++)
      {
        Clip3DPolygon2(subjectPoly, ref plane, outputPoly, temp);
        outputPoly.Clear();
      }
    }

    public static void BenchmarkList(int numIterations, ref Vector3 a, ref Vector3 b, ref Vector3 c, ref Plane plane)
    {
      List<Vector3> subjectPoly = new List<Vector3>(new Vector3[] { a, b, c });
      List<Vector3> outputPoly = new List<Vector3>(4);
      List<float> temp = new List<float>(3);

      for (int i = 0; i < numIterations; i++)
      {
        Clip3DPolygon(subjectPoly, ref plane, outputPoly, temp);
        outputPoly.Clear();
      }
    }

    public static ContainmentType Clip3DPolygon2(IReadOnlyRefList<Vector3> subjectPoly, ref Plane plane, IRefList<Vector3> outputPoly, RefList<float> tempList = null)
    {
      if (subjectPoly.Count < 3)
        return ContainmentType.Outside;

      if (tempList == null)
        tempList = new RefList<float>(subjectPoly.Count);
      else
        tempList.Clear();

      outputPoly.Clear();

      float tolerance = MathHelper.ZeroTolerance;

      //First check if all points are on one side of the plane or not
      bool allIn = true;
      bool allOut = true;

      int numVertices = subjectPoly.Count;
      if (tempList.Capacity < numVertices)
        tempList.Capacity = numVertices;

      for (int i = 0; i < numVertices; i++)
      {
        ref readonly Vector3 pt = ref subjectPoly[i];
        float dist = plane.SignedDistanceTo(pt);

        tempList.Add(in dist);

        if (dist < -tolerance)
          allIn = false;

        if (dist >= tolerance)
          allOut = false;
      }

      //Check if can early out
      if (allIn)
      {
        outputPoly.AddRange(subjectPoly);
        return ContainmentType.Inside;
      }

      if (allOut)
        return ContainmentType.Outside;

      //Clip polygon against the plane
      bool wasClipped = false;

      Vector3 v1 = subjectPoly[0];
      float dist1 = tempList[0];
      float dist2;
      bool isInside = dist1 >= 0.0;

      for (int i = 1; i <= numVertices; i++)
      {
        //Look at edge defined by v1-v2
        int index = i % numVertices;
        ref readonly Vector3 v2 = ref subjectPoly[index];
        dist2 = tempList[index];

        //Both in
        if (isInside && (dist2 >= 0.0f))
        {
          outputPoly.Add(v2);
        }
        //Coming in
        else if (!isInside && (dist2 >= tolerance))
        {
          wasClipped = true;
          isInside = true;

          //Interpolate
          Vector3 v;
          float d = dist1 / (dist1 - dist2);
          Vector3.Lerp(v1, v2, d, out v);

          outputPoly.Add(v);
          outputPoly.Add(v2);
        }
        //Going out
        else if (isInside && (dist2 < -tolerance))
        {
          wasClipped = true;
          isInside = false;

          //Interpolate
          Vector3 v;
          float d = dist1 / (dist1 - dist2);
          Vector3.Lerp(v1, v2, d, out v);

          outputPoly.Add(in v);

        }
        //Both outside
        else
        {
          wasClipped = true;
        }

        v1 = v2;
        dist1 = dist2;
      }

      //Return clip results
      if (!wasClipped)
      {
        outputPoly.AddRange(subjectPoly);
        return ContainmentType.Inside;
      }

      if (outputPoly.Count < 3)
      {
        outputPoly.Clear();
        return ContainmentType.Outside;
      }

      return ContainmentType.Intersects;
    }

    public static ContainmentType Clip3DPolygon(IReadOnlyList<Vector3> subjectPoly, ref Plane plane, List<Vector3> outputPoly, List<float> tempList = null)
    {
      if (subjectPoly.Count < 3)
        return ContainmentType.Outside;

      if (tempList == null)
        tempList = new List<float>();
      else
        tempList.Clear();

      outputPoly.Clear();

      float tolerance = MathHelper.ZeroTolerance;

      //First check if all points are on one side of the plane or not
      bool allIn = true;
      bool allOut = true;

      int numVertices = subjectPoly.Count;
      if (tempList.Capacity < numVertices)
        tempList.Capacity = numVertices;

      for (int i = 0; i < numVertices; i++)
      {
        Vector3 pt = subjectPoly[i];
        float dist = plane.SignedDistanceTo(pt);

        tempList.Add(dist);

        if (dist < -tolerance)
          allIn = false;

        if (dist >= tolerance)
          allOut = false;
      }

      //Check if can early out
      if (allIn)
      {
        outputPoly.AddRange(subjectPoly);
        return ContainmentType.Inside;
      }

      if (allOut)
        return ContainmentType.Outside;

      //Clip polygon against the plane
      bool wasClipped = false;

      Vector3 v1 = subjectPoly[0];
      float dist1 = tempList[0];
      float dist2;
      bool isInside = dist1 >= 0.0;

      for (int i = 1; i <= numVertices; i++)
      {
        //Look at edge defined by v1-v2
        int index = i % numVertices;
        Vector3 v2 = subjectPoly[index];
        dist2 = tempList[index];

        //Both in
        if (isInside && (dist2 >= 0.0f))
        {
          outputPoly.Add(v2);
        }
        //Coming in
        else if (!isInside && (dist2 >= tolerance))
        {
          wasClipped = true;
          isInside = true;

          //Interpolate
          Vector3 v;
          float d = dist1 / (dist1 - dist2);
          Vector3.Lerp(v1, v2, d, out v);

          outputPoly.Add(v);
          outputPoly.Add(v2);
        }
        //Going out
        else if (isInside && (dist2 < -tolerance))
        {
          wasClipped = true;
          isInside = false;

          //Interpolate
          Vector3 v;
          float d = dist1 / (dist1 - dist2);
          Vector3.Lerp(v1, v2, d, out v);

          outputPoly.Add(v);

        }
        //Both outside
        else
        {
          wasClipped = true;
        }

        v1 = v2;
        dist1 = dist2;
      }

      //Return clip results
      if (!wasClipped)
      {
        outputPoly.AddRange(subjectPoly);
        return ContainmentType.Inside;
      }

      if (outputPoly.Count < 3)
      {
        outputPoly.Clear();
        return ContainmentType.Outside;
      }

      return ContainmentType.Intersects;
    }
  }
}
