﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using NUnit.Framework;
using System;

namespace Tesla.Core.Collections
{
  public struct TestRefCompareStruct : IRefComparable<TestRefCompareStruct>, IRefEquatable<TestRefCompareStruct>
  {
    private int m_id;

    public TestRefCompareStruct(int id)
    {
      m_id = id;
    }

    public int CompareTo(in TestRefCompareStruct other)
    {
      return m_id.CompareTo(other.m_id);
    }

    public bool Equals(in TestRefCompareStruct other)
    {
      return m_id == other.m_id;
    }
  }

  public struct TestCompareStruct : IComparable<TestCompareStruct>, IEquatable<TestCompareStruct>
  {
    private int m_id;

    public TestCompareStruct(int id)
    {
      m_id = id;
    }

    public int CompareTo(TestCompareStruct other)
    {
      return m_id.CompareTo(other.m_id);
    }

    public bool Equals(TestCompareStruct other)
    {
      return m_id == other.m_id;
    }
  }

  public struct TestRefCompareStruct2 : IComparable, IEquatable<TestRefCompareStruct2>
  {
    private int m_id;

    public TestRefCompareStruct2(int id)
    {
      m_id = id;
    }

    public int CompareTo(object obj)
    {
      return m_id.CompareTo(((TestRefCompareStruct2) obj).m_id);
    }

    public bool Equals(TestRefCompareStruct2 other)
    {
      return m_id == other.m_id;
    }
  }

  [TestFixture]
  public class RefComparerTests
  {
    [TestCase]
    public void TestDefaultRefComparer()
    {
      //Test a custom struct that implements IRefComparable<T>
      IRefComparer<TestRefCompareStruct> comparer = RefComparer<TestRefCompareStruct>.Default;
      Assert.IsNotNull(comparer);

      TestRefCompareStruct id1 = new TestRefCompareStruct(5);
      TestRefCompareStruct id2 = new TestRefCompareStruct(10);

      int compareResult = comparer.Compare(in id1, in id2);
      Assert.IsTrue(compareResult < 0);
    }

    [TestCase]
    public void TestDefaultRefComparerCompatability()
    {
      //Test an existing struct that doesn't implement IRefComparable<T>, but does implement IComparable<T>, so we have a
      //compatibility shim
      IRefComparer<int> comparer = RefComparer<int>.Default;
      Assert.IsNotNull(comparer);

      int compareResult = comparer.Compare(5, 10);
      Assert.IsTrue(compareResult < 0);
    }

    [TestCase]
    public void TestDefaultRefComparerUnknown()
    {
      //If the struct doesn't implement the generic IComparable<T> or IRefComparable<T> interfaces, but maybe the non-generic IComparable...
      //highly unlikely...but still supported...
      IRefComparer<TestRefCompareStruct2> comparer = RefComparer<TestRefCompareStruct2>.Default;
      Assert.IsNotNull(comparer);

      TestRefCompareStruct2 id1 = new TestRefCompareStruct2(5);
      TestRefCompareStruct2 id2 = new TestRefCompareStruct2(10);

      int compareResult = comparer.Compare(in id1, in id2);
      Assert.IsTrue(compareResult < 0);
    }

    [TestCase]
    public void TestDefaultRefEqualityComparer()
    {
      //Test a custom struct that implements IRefEquatable<T>
      IRefEqualityComparer<TestRefCompareStruct> comparer = RefEqualityComparer<TestRefCompareStruct>.Default;
      Assert.IsNotNull(comparer);

      TestRefCompareStruct id1 = new TestRefCompareStruct(5);
      TestRefCompareStruct id2 = new TestRefCompareStruct(10);
      TestRefCompareStruct id3 = new TestRefCompareStruct(5);

      bool compareResult = comparer.Equals(in id1, in id2);
      bool compareResult2 = comparer.Equals(in id1, in id3);

      Assert.IsFalse(compareResult);
      Assert.IsTrue(compareResult2);
    }

    [TestCase]
    public void TestDefaultRefByteEqualityComparer()
    {
      //Special case for bytes, that expose optimal IndexOf methods, so we piggyback on the non-ref equality comparer
      IRefEqualityComparer<byte> comparer = RefEqualityComparer<byte>.Default;
      Assert.IsNotNull(comparer);

      Assert.IsFalse(comparer.Equals(5, 10));
      Assert.IsTrue(comparer.Equals(5, 5));
    }

    [TestCase]
    public void TestDefaultRefEqualityComparerCompatability()
    {
      //Test an existing struct that doesn't implement IRefEquatable<T>, but does implement IEquatable<T>, so we have a
      //compatibility shim
      IRefEqualityComparer<int> comparer = RefEqualityComparer<int>.Default;
      Assert.IsNotNull(comparer);

      Assert.IsFalse(comparer.Equals(5, 10));
      Assert.IsTrue(comparer.Equals(5, 5));
    }

    [TestCase]
    public void TestDefaultRefEnumCompatability()
    {
      //Will forward the calls to the non-ref equality comparer         
      IRefEqualityComparer<DayOfWeek> comparer = RefEqualityComparer<DayOfWeek>.Default;
      Assert.IsNotNull(comparer);

      Assert.IsFalse(comparer.Equals(DayOfWeek.Monday, DayOfWeek.Friday));
      Assert.IsTrue(comparer.Equals(DayOfWeek.Wednesday, DayOfWeek.Wednesday));
    }
  }
}
