﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using NUnit.Framework;

namespace Tesla.Core.Collections
{
  [TestFixture]
  public class IDListTests
  {
    [TestCase]
    public void TestAddRemove()
    {
      IDList<int> list = new IDList<int>();

      list.Add(0);
      list.Add(1);
      list.Add(2);
      list.Add(3);

      list.RemoveByID(1);

      list.Add(5);
      list.Add(6);

      list.RemoveByID(2);
      list.RemoveByID(4);
      list.Add(10);
      list.Add(11);
      list.Add(12);
      list.Add(13);
      list.RemoveByID(5);
      list.Add(14);
      list.Add(15);
      list.Add(16);

      int count = 0;
      foreach (int val in list)
      {
        if (count > 0)
          Assert.IsTrue(val > 0);

        count++;
      }
      Assert.IsTrue(count == list.Count);

      int[] copy = new int[list.Count];
      list.CopyTo(copy, 2);
    }

    [TestCase]
    public void TestTryGet()
    {
      IDList<int> list = new IDList<int>();

      int id0 = list.Add(10);
      int id1 = list.Add(12);
      int id2 = list.Add(14);

      Assert.IsTrue(list.ContainsID(id0));
      Assert.IsTrue(list.Contains(14));

      Assert.IsTrue(list.LookupID(14) == id2);

      int val;
      Assert.IsTrue(list.TryGetValue(id1, out val));
      Assert.IsTrue(val == 12);

      Assert.IsTrue(list.Count == 3);
      list.Clear();
      Assert.IsTrue(list.Count == 0);
      Assert.IsTrue(list.UpperBoundExclusive == 0);
    }

    [TestCase]
    public void TestRemove()
    {
      IDList<int> list = new IDList<int>();

      int id0 = list.Add(10);
      int id1 = list.Add(12);
      int id2 = list.Add(14);

      Assert.IsTrue(list.Remove(12));

      Assert.IsFalse(list.Contains(12));
      Assert.IsFalse(list.ContainsID(id1));

      int val;
      Assert.IsFalse(list.TryGetValue(id1, out val));
    }

    [TestCase]
    public void TestCreate()
    {
      int[] arr = new int[] { 3, 5, 1 };
      IDList<int> list = new IDList<int>(arr);

      Assert.IsTrue(list.Count == 3);
      Assert.IsTrue(list.Contains(1));
      Assert.IsTrue(list.Contains(3));
      Assert.IsTrue(list.Contains(5));
    }

    [TestCase]
    public void TestIndexer()
    {
      IDList<int> list = new IDList<int>(4);
      int id0 = list.Add(2);
      int id1 = list.Add(3);
      int id2 = list.Add(4);

      Assert.IsTrue(list[id1] == 3);

      list[id1] = 5;
      Assert.IsTrue(list.Count == 3);
      Assert.IsTrue(list[id1] == 5);

      list[3] = 6; //Initial capacity set to 4, first 3 slots have something
      Assert.IsTrue(list.Count == 4);
      Assert.IsTrue(list[3] == 6);
    }

    [TestCase]
    public void TestCompact_Trim()
    {
      IDList<int> list = new IDList<int>(10);
      list.Add(2);
      list.Add(3); //Removed
      list.Add(4);
      list.Add(5); //Removed
      list.Add(7); //Removed
      list.Add(10);
      list.Add(11);

      list.RemoveByID(1);
      list.RemoveByID(3);
      list.RemoveByID(4);

      list.Compact((int oldID, int newID, ref int val) =>
      {
        System.Console.WriteLine(string.Format("ID {0} => New ID {1}, Value: {2}", oldID, newID, val));
      }, true);
    }

    [TestCase]
    public void TestCompact_NoTrim()
    {
      IDList<int> list = new IDList<int>(10);
      list.Add(2);
      list.Add(3); //Removed
      list.Add(4);
      list.Add(5); //Removed
      list.Add(7); //Removed
      list.Add(10);
      list.Add(11);

      list.RemoveByID(1);
      list.RemoveByID(3);
      list.RemoveByID(4);

      list.Compact((int oldID, int newID, ref int val) =>
      {
        System.Console.WriteLine(string.Format("ID {0} => New ID {1}, Value: {2}", oldID, newID, val));
      }, false);
    }
  }
}
