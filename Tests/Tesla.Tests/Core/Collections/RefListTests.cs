﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Tesla.Core.Collections
{
  [StructLayout(LayoutKind.Sequential)]
  public struct TestLargeStruct : IRefComparable<TestLargeStruct>, IComparable<TestLargeStruct>
  {
    public static int NumComparisons = 0;

    public int ID;
    public Matrix Forward;
    public Matrix Back;

    public TestLargeStruct(int id)
    {
      ID = id;
      Forward = Matrix.Identity;
      Back = Matrix.Identity;
    }

    public readonly int CompareTo(in TestLargeStruct other)
    {
      NumComparisons++;

      if (ID < other.ID)
        return -1;

      if (ID > other.ID)
        return 1;

      return 0;
    }

    readonly int IComparable<TestLargeStruct>.CompareTo(TestLargeStruct other)
    {
      NumComparisons++;

      if (ID < other.ID)
        return -1;

      if (ID > other.ID)
        return 1;

      return 0;
    }
  }

  [StructLayout(LayoutKind.Sequential)]
  public struct TestLargeStructNoRef : IComparable<TestLargeStructNoRef>
  {
    public static int NumComparisons = 0;

    public int ID;
    public Matrix Forward;
    public Matrix Back;

    public TestLargeStructNoRef(int id)
    {
      ID = id;
      Forward = Matrix.Identity;
      Back = Matrix.Identity;
    }

    public readonly int CompareTo(TestLargeStructNoRef other)
    {
      NumComparisons++;

      if (ID < other.ID)
        return -1;

      if (ID > other.ID)
        return 1;

      return 0;
    }
  }

  public class TestLargeStructRefComparer : IRefComparer<TestLargeStruct>
  {
    public int Compare(in TestLargeStruct x, in TestLargeStruct y)
    {
      return x.CompareTo(in y);
    }
  }

  public class TestComparer<T> : IComparer<T> where T : struct, IComparable<T>
  {
    public int Compare(T x, T y)
    {
      return x.CompareTo(y);
    }
  }

  [TestFixture]
  public class RefListTests
  {
    [TestCase]
    public void TestCtorEmptyAdd()
    {
      //Start from an empty list, ensure its empty and then add something
      RefList<int> list = new RefList<int>();

      Assert.IsTrue(list.Count == 0);
      Assert.IsTrue(list.Capacity == 0);

      list.Add(5);

      Assert.IsTrue(list.Count == 1);
      Assert.IsTrue(list.Capacity > 1); //Default is a few more, 4 but just make sure it's not 1
    }

    [TestCase]
    public void TestCtor()
    {
      RefList<int> list = new RefList<int>(10);

      Assert.IsTrue(list.Count == 0);
      Assert.IsTrue(list.Capacity == 10);
      Assert.IsTrue(list.Span.Length == 0);
    }

    [TestCase]
    public void TestInsert()
    {
      RefList<int> list = new RefList<int>(10);

      list.Add(-1);
      list.Add(-2);
      list.Add(-3);

      list.Insert(1, 5);

      Assert.IsTrue(list.Count == 4);

      Assert.IsTrue(list[1] == 5);
      Assert.IsTrue(list[2] == -2);
      Assert.IsTrue(list[3] == -3);
      Assert.IsTrue(list.Span.Length == 4);
    }

    [TestCase]
    public void TestInsertRange()
    {
      RefList<int> list = new RefList<int>(10);

      list.Add(-1);
      list.Add(-2);

      int[] toInsert = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
      list.InsertRange(1, toInsert);

      Assert.IsTrue(list[0] == -1);

      for (int i = 1, j = 0; i < list.Count - 1; i++, j++)
        Assert.IsTrue(toInsert[j] == list[i]);

      Assert.IsTrue(list[list.Count - 1] == -2);
    }

    [TestCase]
    public void TestGetter()
    {
      RefList<int> list = new RefList<int>();
      list.Add(5);

      Assert.IsTrue(list[0] == 5);
    }

    [TestCase]
    public void TestSetter()
    {
      RefList<int> list = new RefList<int>();
      list.Add(5);

      list[0] = 10;

      Assert.IsTrue(list[0] == 10);
    }

    [TestCase]
    public void TestNoRefGetter()
    {
      RefList<int> list = new RefList<int>();
      list.Add(5);

      int v = ((IList<int>) list)[0];

      Assert.IsTrue(v == 5);
    }

    [TestCase]
    public void TestReadOnlyNoRefGetter()
    {
      RefList<int> list = new RefList<int>();
      list.Add(5);

      int v = ((IReadOnlyList<int>) list)[0];

      Assert.IsTrue(v == 5);
    }

    [TestCase]
    public void TestCapacity()
    {
      RefList<int> list = new RefList<int>();

      Assert.IsTrue(list.Capacity == 0);

      list.Capacity = 10;

      Assert.IsTrue(list.Capacity == 10);

      for (int i = 0; i < 11; i++)
        list.Add(i);

      Assert.IsTrue(list.Count == 11);
      Assert.IsTrue(list.Capacity == 20);
    }

    [TestCase]
    public void TestAddEnumerable()
    {
      String letters = "ABCD";
      RefList<char> list = new RefList<char>();
      list.AddRange(letters.AsSpan());

      for (int i = 0; i < list.Count; i++)
        Assert.IsTrue(list[i] == letters[i]);

      //Test the ctor addrange too
      list = new RefList<char>(letters);

      for (int i = 0; i < list.Count; i++)
        Assert.IsTrue(list[i] == letters[i]);
    }

    [TestCase]
    public void TestAddRemove()
    {
      RefList<int> list = new RefList<int>();
      list.Add(5);
      list.Add(10);
      list.Add(6);

      list.Remove(5);

      Assert.IsTrue(list[0] == 10);

      list.Clear();

      Assert.IsTrue(list.Count == 0);
    }

    [TestCase]
    public void TestRemoveAll()
    {
      RefList<TestLargeStruct> list = new RefList<TestLargeStruct>();
      list.Add(new TestLargeStruct(5));
      list.Add(new TestLargeStruct(10));
      list.Add(new TestLargeStruct(11));
      list.Add(new TestLargeStruct(10));
      list.Add(new TestLargeStruct(12));
      list.Add(new TestLargeStruct(5));

      list.RemoveAll((in TestLargeStruct s) =>
      {
        return s.ID == 5 || s.ID == 10;
      });

      Assert.IsTrue(list.Count == 2);

      list.RemoveRange(0, list.Count);

      Assert.IsTrue(list.Count == 0);
    }

    [TestCase]
    public void TestReverse()
    {
      RefList<int> list = new RefList<int>();
      list.Add(5);
      list.Add(4);
      list.Add(3);
      list.Add(2);
      list.Add(1);
      list.Add(0);

      list.Reverse();

      for (int i = 0; i < list.Count; i++)
        Assert.IsTrue(list[i] == i);

      int[] array = list.ToArray();

      Assert.IsNotNull(array);
      Assert.IsTrue(array.Length == list.Count);

      for (int i = 0; i < list.Count; i++)
        Assert.IsTrue(array[i] == i);
    }

    [TestCase]
    public void TestGetRange()
    {
      RefList<int> list = new RefList<int>();
      list.Add(5);
      list.Add(4);
      list.Add(3);
      list.Add(2);
      list.Add(1);
      list.Add(0);

      RefList<int> otherList = list.GetRange(1, 2);

      Assert.IsTrue(otherList.Count == 2);
      Assert.IsTrue(otherList[0] == 4);
      Assert.IsTrue(otherList[1] == 3);
    }

    [TestCase]
    public void TestInsertSelf()
    {
      RefList<int> list = new RefList<int>();
      list.Add(5);
      list.Add(4);
      list.Add(3);

      list.AddRange(list);

      Assert.IsTrue(list.Count == 6);

      Assert.IsTrue(list[0] == 5);
      Assert.IsTrue(list[1] == 4);
      Assert.IsTrue(list[2] == 3);
      Assert.IsTrue(list[3] == 5);
      Assert.IsTrue(list[4] == 4);
      Assert.IsTrue(list[5] == 3);
    }

    [TestCase]
    public void TestConvertAll()
    {
      RefList<int> list = new RefList<int>();
      list.Add(2);
      list.Add(5);
      list.Add(10);

      RefList<float> list2 = list.ConvertAll<float>((in int v, out float result) =>
      {
        result = (float) v;
      });

      Assert.IsTrue(list2.Count == 3);

      for (int i = 0; i < list.Count; i++)
        Assert.IsTrue(list[i] == (int) list2[i]);
    }

    [TestCase]
    public void TestForEach()
    {
      RefList<int> list = new RefList<int>();
      list.Add(2);
      list.Add(5);
      list.Add(10);

      list.ForEach((in int v) =>
      {

      });
    }

    [TestCase]
    public void TestTrueForAll()
    {
      RefList<int> list = new RefList<int>();
      list.Add(2);
      list.Add(2);
      list.Add(2);

      Assert.IsTrue(list.TrueForAll((in int v) =>
      {
        return v == 2;
      }));

      list.Add(3);


      Assert.IsFalse(list.TrueForAll((in int v) =>
      {
        return v == 2;
      }));
    }

    [TestCase]
    public void TestExists()
    {
      RefList<int> list = new RefList<int>();
      list.Add(5);
      list.Add(4);
      list.Add(3);

      Assert.IsTrue(list.Exists((in int v) =>
      {
        return v == 5;
      }));

      Assert.IsFalse(list.Exists((in int v) =>
      {
        return v == 10;
      }));
    }

    [TestCase]
    public void TestEnumerator()
    {
      RefList<int> list = new RefList<int>();
      list.Add(5);
      list.Add(4);
      list.Add(3);
      list.Add(52);
      list.Add(434);
      list.Add(31);

      int index = 0;
      foreach (int val in list)
      {
        Assert.IsTrue(val == list[index]);
        index++;
      }
    }

    [TestCase]
    public void TestTrim()
    {
      RefList<int> list = new RefList<int>(10);
      list.Add(2);

      list.TrimExcess();

      Assert.IsTrue(list.Capacity == 1);
    }

    [TestCase]
    public void TestContains()
    {
      RefList<TestLargeStruct> list = new RefList<TestLargeStruct>();
      list.Add(new TestLargeStruct(5));
      list.Add(new TestLargeStruct(10));
      list.Add(new TestLargeStruct(11));

      Assert.IsTrue(list.Contains(new TestLargeStruct(11)));
      Assert.IsFalse(list.Contains(new TestLargeStruct(12)));
    }

    [TestCase]
    public void TestCopyTo()
    {
      RefList<int> list = new RefList<int>();
      list.Add(5);
      list.Add(10);
      list.Add(6);

      int[] values = new int[3];
      list.CopyTo(values);

      for (int i = 0; i < list.Count; i++)
        Assert.IsTrue(list[i] == values[i]);
    }

    [TestCase]
    public void TestIndexOf()
    {
      RefList<int> list = new RefList<int>();
      list.Add(5);
      list.Add(10);
      list.Add(10);
      list.Add(6);
      list.Add(5);
      list.Add(2);

      Assert.IsTrue(list.IndexOf(10) == 1);
      Assert.IsTrue(list.IndexOf(5) == 0);
      Assert.IsTrue(list.LastIndexOf(10) == 2);
      Assert.IsTrue(list.LastIndexOf(5) == 4);

      Assert.IsTrue(list.IndexOf(10, 3, 3) == -1);
    }

    [TestCase]
    public void TestFind()
    {
      RefList<int> list = new RefList<int>();
      list.Add(5);
      list.Add(10);
      list.Add(10);
      list.Add(6);
      list.Add(5);
      list.Add(2);

      RefList<int> tens = list.FindAll((in int v) =>
      {
        return v == 10;
      });

      Assert.IsTrue(tens.Count == 2);
      Assert.IsTrue(tens[0] == 10 && tens[1] == 10);

      int ten;
      Assert.IsTrue(list.Find((in int v) => { return v == 10; }, out ten));
      Assert.IsTrue(ten == 10);

      int five;
      Assert.IsTrue(list.FindLast((in int v) => { return v == 5; }, out five));
      Assert.IsTrue(five == 5);
    }

    [TestCase]
    public void TestFindIndex()
    {
      RefList<int> list = new RefList<int>();
      list.Add(5);
      list.Add(10);
      list.Add(10);
      list.Add(6);
      list.Add(5);
      list.Add(2);
      list.Add(3);
      list.Add(100);
      list.Add(25);

      Assert.IsTrue(list.FindIndex(1, 5, (in int v) => { return v == 100; }) == -1);
      Assert.IsTrue(list.FindIndex((in int v) => { return v == 100; }) == 7);

      Assert.IsTrue(list.FindLastIndex(6, 5, (in int v) => { return v == 6; }) == 3);
      Assert.IsTrue(list.FindLastIndex((in int v) => { return v == 3; }) == 6);
    }

    [TestCase]
    public void TestListInterop()
    {
      RefList<int> list = new RefList<int>();
      List<int> bclList = new List<int>();

      DoStuffToList(list);
      DoStuffToList(bclList);

      Assert.IsTrue(list.Count == bclList.Count);
      for (int i = 0; i < list.Count; i++)
        Assert.IsTrue(list[i] == bclList[i]);

      bclList.AddRange(list);

      Assert.IsTrue(bclList.Count == (list.Count * 2));
    }

    private static void DoStuffToList(IList<int> list)
    {
      list.Add(10);
      list.Add(20);
      list.Add(50);
    }

    [TestCase]
    public void TestBinarySearchImplicitRef()
    {
      //int doesn't implement IRefComparable, just IComparable, but we handle that case behind the scens silently
      RefList<int> list = new RefList<int>(new int[] { 1, 2, 3, 4, 5, 6 });

      int index = list.BinarySearch(4);

      Assert.IsTrue(index == 3);
    }

    [TestCase]
    public void TestBinarySearchExplicitRef()
    {
      //this struct implements IRefComparable, which should be handled
      RefList<TestRefCompareStruct> list = new RefList<TestRefCompareStruct>(new TestRefCompareStruct[]
                              { new TestRefCompareStruct(1), new TestRefCompareStruct(2), new TestRefCompareStruct(3),
                                      new TestRefCompareStruct(4), new TestRefCompareStruct(5), new TestRefCompareStruct(6)
                              });

      int index = list.BinarySearch(new TestRefCompareStruct(4));

      Assert.IsTrue(index == 3);
    }

    [TestCase]
    public void TestBinarySearchExplicitComparer()
    {
      //create a non-default comparer which should use the "slow" path for searching
      RefList<int> list = new RefList<int>(new int[] { 1, 2, 3, 4, 5, 6 });

      int index = list.BinarySearch(4, RefComparer<int>.Create(delegate (in int x, in int y)
      {
        return x.CompareTo(y);
      }));

      Assert.IsTrue(index == 3);
    }

    [TestCase]
    public void TestBinarySearchRange()
    {
      RefList<int> list = new RefList<int>(new int[] { 1, 2, 3, 4, 5, 6 });

      int index = list.BinarySearch(2, 3, 4); //start at index 2, go until the end of the list (3 elements), search for value 4, use default comparison

      Assert.IsTrue(index == 3);
    }

    [TestCase]
    public void TestSort()
    {
      int[] sorted = new int[] { 1, 2, 2, 3, 5, 8, 8, 9, 10, 10, 65 };

      RefList<int> list = new RefList<int>(new int[] { 1, 5, 9, 10, 2, 3, 8, 10, 65, 8, 2 });

      list.Sort();

      for (int i = 0; i < list.Count; i++)
        Assert.IsTrue(sorted[i] == list[i]);
    }

    [TestCase]
    public void TestSortLargeDataset_BadCase()
    {
      //Test List<T> and RefList<T> with a small struct, both cases should use what platform uses for sorting so speed should be approximately the same
      Random random = new Random();
      int numElements = 500_000;

      int[] initial = new int[numElements];

      for (int i = 0; i < numElements; i++)
      {
        int id = random.Next();
        if (i % 2 == 0)
          id = -id;

        initial[i] = id;
      }

      double refSortTime = 0;
      double bclSortTime = 0;

      int numRuns = 3;
      Stopwatch watch = new Stopwatch();

      for (int i = 0; i < numRuns; i++)
      {
        RefList<int> refList = new RefList<int>(initial);

        watch.Restart();

        refList.Sort();

        watch.Stop();

        refSortTime += watch.ElapsedMilliseconds;

        List<int> bclList = new List<int>(initial);

        watch.Restart();

        bclList.Sort();

        watch.Stop();

        bclSortTime += watch.ElapsedMilliseconds;

        for (int j = 0; j < numElements; j++)
          Assert.IsTrue(refList[j] == bclList[j]);
      }

      Console.WriteLine("TIME FOR REF LIST (implicit IRefComparable): avg {0} ms", refSortTime / numRuns);
      Console.WriteLine("TIME FOR BCL LIST: avg {0} ms", bclSortTime / numRuns);
    }

    [TestCase]
    public void TestSortLargeDataset()
    {
      //Test List<T> and RefList<T> with a small struct, both cases should use what platform uses for sorting so speed should be approximately the same
      Random random = new Random();
      int numElements = 500_000;

      TestLargeStruct[] initial = new TestLargeStruct[numElements];

      for (int i = 0; i < numElements; i++)
      {
        int id = random.Next();
        if (i % 2 == 0)
          id = -id;

        initial[i] = new TestLargeStruct(id);
      }

      double refSortTime = 0;
      double bclSortTime = 0;

      int numRuns = 3;
      Stopwatch watch = new Stopwatch();

      for (int i = 0; i < numRuns; i++)
      {
        RefList<TestLargeStruct> refList = new RefList<TestLargeStruct>(initial);

        watch.Restart();

        refList.Sort();

        watch.Stop();

        refSortTime += watch.ElapsedMilliseconds;

        List<TestLargeStruct> bclList = new List<TestLargeStruct>(initial);

        watch.Restart();

        bclList.Sort();

        watch.Stop();

        bclSortTime += watch.ElapsedMilliseconds;

        for (int j = 0; j < numElements; j++)
          Assert.IsTrue(refList[j].ID == bclList[j].ID);
      }

      Console.WriteLine("TIME FOR REF LIST (implicit IRefComparable): avg {0} ms", refSortTime / numRuns);
      Console.WriteLine("TIME FOR BCL LIST: avg {0} ms", bclSortTime / numRuns);
    }

    [TestCase]
    public void TestSortLargeStruct()
    {
      //Test List<T> vs RefList<T> sorting when it comes to a large struct (>64 bytes)
      Random random = new Random();
      int numElements = 50_000;

      TestLargeStruct[] initial = new TestLargeStruct[numElements];

      for (int i = 0; i < numElements; i++)
      {
        // int id = numElements - i;
        int id = random.Next();
        if (i % 2 == 0)
          id = -id;

        initial[i] = new TestLargeStruct(id);
      }

      double refSortTime = 0;
      double bclSortTime = 0;
      int refNumComparisons = 0;
      int bclNumComparisons = 0;

      int numRuns = 3;
      Stopwatch watch = new Stopwatch();

      for (int i = 0; i < numRuns; i++)
      {
        List<TestLargeStruct> bclList = new List<TestLargeStruct>(initial);
        TestLargeStruct.NumComparisons = 0;

        watch.Restart();

        bclList.Sort();

        bclSortTime += watch.ElapsedMilliseconds;
        bclNumComparisons = TestLargeStruct.NumComparisons;

        RefList<TestLargeStruct> refList = new RefList<TestLargeStruct>(initial);
        TestLargeStruct.NumComparisons = 0;

        watch.Restart();

        refList.Sort();

        watch.Stop();

        refSortTime += watch.ElapsedMilliseconds;
        refNumComparisons = TestLargeStruct.NumComparisons;

        for (int j = 0; j < numElements; j++)
          Assert.IsTrue(refList[j].ID == bclList[j].ID);
      }

      Console.WriteLine("TIME FOR BCL LIST: avg {0} ms, {1} comparisons", bclSortTime / numRuns, bclNumComparisons);
      Console.WriteLine("TIME FOR REF LIST (explicit IRefComparable): avg {0} ms, {1} comparisons", refSortTime / numRuns, refNumComparisons);
    }

    [TestCase]
    public void TestSortLargeStructAndNoRef()
    {
      //Test List<T> and RefList<T> when dealing with a large struct (>64 bytes), and test a version with RefList<T> that only implements
      //IComparable<T>, because it should fallback to what List<T> does, so it should be similar in performance.
      Random random = new Random();
      int numElements = 50_000;

      TestLargeStruct[] initial = new TestLargeStruct[numElements];
      TestLargeStructNoRef[] initialNoRef = new TestLargeStructNoRef[numElements];

      for (int i = 0; i < numElements; i++)
      {
        int id = random.Next();
        if (i % 2 == 0)
          id = -id;

        initial[i] = new TestLargeStruct(id);
        initialNoRef[i] = new TestLargeStructNoRef(id);
      }

      double refSortTime = 0;
      double hybridSortTime = 0;
      double bclSortTime = 0;
      int refNumComparisons = 0;
      int hybridNumComparisons = 0;
      int bclNumComparisons = 0;

      int numRuns = 3;
      Stopwatch watch = new Stopwatch();

      for (int i = 0; i < numRuns; i++)
      {
        List<TestLargeStruct> bclList = new List<TestLargeStruct>(initial);
        TestLargeStruct.NumComparisons = 0;

        watch.Restart();

        bclList.Sort();

        watch.Stop();

        bclSortTime += watch.ElapsedMilliseconds;
        bclNumComparisons = TestLargeStruct.NumComparisons;

        RefList<TestLargeStructNoRef> hybridList = new RefList<TestLargeStructNoRef>(initialNoRef);
        TestLargeStructNoRef.NumComparisons = 0;

        watch.Restart();

        hybridList.Sort();

        watch.Stop();

        hybridSortTime += watch.ElapsedMilliseconds;
        hybridNumComparisons = TestLargeStructNoRef.NumComparisons;

        RefList<TestLargeStruct> refList = new RefList<TestLargeStruct>(initial);
        TestLargeStruct.NumComparisons = 0;

        watch.Restart();

        refList.Sort();

        watch.Stop();

        refSortTime += watch.ElapsedMilliseconds;
        refNumComparisons = TestLargeStruct.NumComparisons;
      }

      Console.WriteLine("TIME FOR BCL LIST: avg {0} ms, {1} comparisons", bclSortTime / numRuns, bclNumComparisons);
      Console.WriteLine("TIME FOR REF LIST (implicit IRefComparable): avg {0} ms, {1} comparisons", hybridSortTime / numRuns, hybridNumComparisons);
      Console.WriteLine("TIME FOR REF LIST (explicit IRefComparable): avg {0} ms, {1} comparisons", refSortTime / numRuns, refNumComparisons);
    }

    [TestCase]
    public void TestWrappedComparer()
    {
      RefList<int> list = new RefList<int>();
      list.Add(4);
      list.Add(53);
      list.Add(6);
      list.Add(5);

      list.Sort(RefComparer<int>.Default);
    }

    [TestCase]
    public void TestSortLargeWithComparer()
    {
      //Test List<T> and RefList<T> when dealing with a large struct (>64 bytes), using custom comparers
      Random random = new Random();
      int numElements = 50_000;

      TestLargeStruct[] initial = new TestLargeStruct[numElements];

      for (int i = 0; i < numElements; i++)
      {
        int id = random.Next();
        if (i % 2 == 0)
          id = -id;

        initial[i] = new TestLargeStruct(id);
      }

      double refSortTime = 0;
      double bclSortTime = 0;
      int refNumComparisons = 0;
      int bclNumComparisons = 0;

      int numRuns = 3;
      Stopwatch watch = new Stopwatch();

      for (int i = 0; i < numRuns; i++)
      {
        RefList<TestLargeStruct> refList = new RefList<TestLargeStruct>(initial);
        TestLargeStruct.NumComparisons = 0;

        watch.Restart();

        refList.Sort(new TestLargeStructRefComparer());

        watch.Stop();

        refSortTime += watch.ElapsedMilliseconds;
        refNumComparisons = TestLargeStruct.NumComparisons;

        List<TestLargeStruct> bclList = new List<TestLargeStruct>(initial);
        TestLargeStruct.NumComparisons = 0;

        watch.Restart();

        bclList.Sort(new TestComparer<TestLargeStruct>());

        watch.Stop();

        bclSortTime += watch.ElapsedMilliseconds;
        bclNumComparisons = TestLargeStruct.NumComparisons;
      }

      Console.WriteLine("TIME FOR REF LIST: avg {0} ms, {1} comparisons", refSortTime / numRuns, refNumComparisons);
      Console.WriteLine("TIME FOR BCL LIST: avg {0} ms, {1} comparisons", bclSortTime / numRuns, bclNumComparisons);
    }

    [TestCase]
    public void TestRefArrayPlatformSwitch()
    {
      int[] arry = new int[] { 5, 2, 3, 10, 5, 9, 1, 22, 52, 101 };

      RefArray.Sort<int>(arry);

      RefArray.BinarySearch<int>(arry, 10);

      int[] arry2 = new int[] { 5, 2, 3, 10, 5, 9, 1, 22, 52, 101 };


      RefArray.Sort<int>(arry2, RefComparer<int>.Default);

      RefArray.BinarySearch<int>(arry2, 10, RefComparer<int>.Default);
    }
  }
}
