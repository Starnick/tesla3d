﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using NUnit.Framework;
using System;
using System.IO;
using System.Collections.Generic;

namespace Tesla.Core.Collections
{
  [TestFixture]
  public sealed class MultiKeyDictionaryTests
  {
    [TestCase]
    public void AddRemoveTest()
    {
      MultiKeyDictionary<int, int, String> dict = new MultiKeyDictionary<int, int, string>();
      dict.Add(1, 5, "HiFive");

      Assert.IsTrue(dict.Count == 1);

      dict.Add(new MultiKey<int, int>(1, 7), "LowFive");

      Assert.IsTrue(dict.Count == 2);
      Assert.IsTrue(dict.TotalMajorKeyCount == 1);

      dict.Add(new MultiKey<int, int>(5, 2), "HiFour");

      Assert.IsTrue(dict.Count == 3);
      Assert.IsTrue(dict.TotalMajorKeyCount == 2);

      Assert.IsTrue(dict.Remove(1, 7));
      Assert.IsTrue(dict.Count == 2);
      Assert.IsTrue(dict.TotalMajorKeyCount == 2);

      //Try again, make sure can't find it
      Assert.IsFalse(dict.Remove(1, 7));
      Assert.IsTrue(dict.Count == 2);
      Assert.IsTrue(dict.TotalMajorKeyCount == 2);

      Assert.IsTrue(dict.Remove(new MultiKey<int, int>(5, 2)));
      Assert.IsTrue(dict.Count == 1);
      Assert.IsTrue(dict.TotalMajorKeyCount == 1);

      dict.Clear();
      Assert.IsTrue(dict.Count == 0);
      Assert.IsTrue(dict.TotalMajorKeyCount == 0);
    }

    [TestCase]
    public void EnumerateClearTest()
    {
      MultiKeyDictionary<int, int, String> dict = new MultiKeyDictionary<int, int, string>();
      dict.Add(1, 5, "HiFive");
      dict.Add(new MultiKey<int, int>(1, 7), "LowFive");
      dict.Add(new MultiKey<int, int>(5, 2), "HiFour");
      dict.Add(25, 2, "Stuff");
      dict.Add(25, 3, "MoreStuff");
      dict.Add(25, 4, "EvenMoreStuff");

      Assert.IsTrue(dict.Count == 6);

      int count = 0;
      foreach (KeyValuePair<MultiKey<int, int>, String> kv in dict)
        count++;

      Assert.IsTrue(dict.Count == count);
      Assert.IsTrue(dict.Keys.Count == count);
      Assert.IsTrue(dict.Values.Count == count);

      int count2 = 0;
      foreach (MultiKey<int, int> kv in dict.Keys)
        count2++;

      Assert.IsTrue(count2 == count);

      count2 = 0;
      foreach (String v in dict.Values)
        count2++;

      Assert.IsTrue(count2 == count);

      dict.Clear();

      Assert.IsTrue(dict.Count == 0);
    }

    [TestCase]
    public void ReplaceTest()
    {
      MultiKeyDictionary<int, int, String> dict = new MultiKeyDictionary<int, int, string>();
      dict.Add(new MultiKey<int, int>(1, 5), "HiFive");

      dict[new MultiKey<int, int>(1, 5)] = "HiFour";

      Assert.AreEqual(dict[new MultiKey<int, int>(1, 5)], "HiFour");
    }

    [TestCase]
    public void GetterTest()
    {
      MultiKeyDictionary<int, int, String> dict = new MultiKeyDictionary<int, int, string>();

      Assert.Catch<KeyNotFoundException>(() =>
      {
        String str = dict[1, 6];
        Console.WriteLine(str);
      });
    }
  }
}
