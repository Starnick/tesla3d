﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using NUnit.Framework;
using System;
using System.IO;
using Tesla.Scene;
using Tesla.Content;
using System.Text;
using Tesla.Graphics;

namespace Tesla.Core
{
  [TestFixture]
  public class EngineEventsTests
  {
    [TestCase]
    public void TestEngineEvents()
    {
      int initCount = 0;
      int serviceAddedCount = 0;
      int destroyCount = 0;

      TypedEventHandler<Engine> onIniting = (Engine e, EventArgs args) =>
      {
        initCount++;
      };

      TypedEventHandler<Engine> onInited = (Engine e, EventArgs args) =>
      {
        initCount++;
      };

      TypedEventHandler<Engine> onDestroying = (Engine e, EventArgs args) =>
      {
        destroyCount++;
      };

      TypedEventHandler<Engine> onDestroyed = (Engine e, EventArgs args) =>
      {
        destroyCount++;
      };

      TypedEventHandler<Engine, EngineServiceChangedEventArgs> onServiceAdded = (Engine e, EngineServiceChangedEventArgs args) =>
      {
        serviceAddedCount++;
      };

      Engine.Initializing += onIniting;
      Engine.Initialized += onInited;
      Engine.Destroying += onDestroying;
      Engine.Destroyed += onDestroyed;
      Engine.ServiceChanged += onServiceAdded;

      Engine.Initialize();

      try
      {
        Assert.IsTrue(initCount == 2);

        Engine.Instance.Services.AddService<ServiceA>(new ServiceA());
        Engine.Instance.Services.ReplaceService<ServiceA>(new ServiceA());

        Assert.IsTrue(serviceAddedCount == 2);
      }
      finally
      {
        Engine.Destroy();
        Assert.IsTrue(destroyCount == 2);
      }
    }

    [TestCase]
    public void TestEngineEvents_Tracker()
    {
      bool engineDestroyed = false;
      EngineServiceTracker<ServiceA> tracker = new EngineServiceTracker<ServiceA>();
      tracker.EngineDestroyed += (Engine sender, EventArgs args) =>
      {
        engineDestroyed = true;
      };

      Engine.Initialize();

      try
      {
        Engine.Instance.Services.AddService<ServiceA>(new ServiceA());
        Assert.IsNotNull(tracker.Service);

        EngineServiceTracker<ServiceA> tracker2 = new EngineServiceTracker<ServiceA>();
        Assert.IsNotNull(tracker2.Service);

        bool calledInitialHandler = false;
        EngineServiceTracker<ServiceA> tracker3 = new EngineServiceTracker<ServiceA>((Engine sender, EngineServiceChangedEventArgs args) =>
        {
          calledInitialHandler = true;
        });

        Assert.IsTrue(calledInitialHandler);
      }
      finally
      {
        Engine.Destroy();

        Assert.IsTrue(engineDestroyed);
      }
    }

    [TestCase]
    public void TestDependencies_Add()
    {
      Engine.Initialize();

      try
      {
        // Test checking for dependencies on add
        Engine.Instance.Services.AddService<ServiceA>(new ServiceA());
        Engine.Instance.Services.AddService<ServiceB>(new ServiceB());
        Engine.Instance.Services.AddService<ServiceC>(new ServiceC());

        Engine.Destroy();

        Assert.Throws<ArgumentException>(() =>
        {
          Engine.Initialize();

          // Ok without A, just optional
          Engine.Instance.Services.AddService<ServiceB>(new ServiceB());

          // Requires A, so throws
          Engine.Instance.Services.AddService<ServiceC>(new ServiceC());
        });
      }
      finally
      {
        Engine.Destroy();
      }
    }

    [TestCase]
    public void TestDependencies_Replace()
    {
      Engine.Initialize();

      try
      {
        // Test checking for dependencies and replacing
        Engine.Instance.Services.AddService<ServiceA>(new ServiceA());
        Engine.Instance.Services.AddService<ServiceB>(new ServiceB());
        Engine.Instance.Services.AddService<ServiceC>(new ServiceC());

        Engine.Instance.Services.ReplaceService<ServiceA>(new ServiceA());

        // Both should have been notified
        Assert.IsTrue(Engine.Instance.Services.GetService<ServiceB>().ChangedServiceA == 1);
        Assert.IsTrue(Engine.Instance.Services.GetService<ServiceC>().ChangedServiceA == 1);

        Engine.Instance.Services.ReplaceService<ServiceB>(new ServiceB());

        Assert.IsTrue(Engine.Instance.Services.GetService<ServiceB>().ChangedServiceA == 0);
        Assert.IsTrue(Engine.Instance.Services.GetService<ServiceC>().ChangedServiceA == 1);
      }
      finally
      {
        Engine.Destroy();
      }
    }

    private class ServiceA : IEngineService
    {
      public string Name { get { return "ServiceA"; } }

      public void Initialize(Engine engine) { }
    }

    [OptionalDependency(typeof(ServiceA))]
    private class ServiceB : IEngineService, IEngineServiceDependencyChanged
    {
      public int ChangedServiceA = 0;
      public string Name { get { return "ServiceB"; } }

      public void Initialize(Engine engine) { }

      public void OnDependencyChanged(EngineServiceChangedEventArgs args)
      {
        if (args.IsService<ServiceA>())
          ChangedServiceA += 1;
      }
    }

    [RequiredDependency(typeof(ServiceA))]
    [OptionalDependency(typeof(ServiceB))]
    private class ServiceC : IEngineService, IEngineServiceDependencyChanged
    {
      public int ChangedServiceA = 0;
      public string Name { get { return "ServiceB"; } }

      public void Initialize(Engine engine) { }

      public void OnDependencyChanged(EngineServiceChangedEventArgs args)
      {
        if (args.IsService<ServiceA>())
          ChangedServiceA += 1;
      }
    }
  }
}
