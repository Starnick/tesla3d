﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using NUnit.Framework;
using System;
using System.IO;
using Tesla.Scene;
using Tesla.Content;
using System.Text;
using Tesla.Graphics;

namespace Tesla.Core.Math
{
  [TestFixture]
  public class AngleTests
  {
    [TestCase]
    public void Lerp()
    {
      Angle fromAngle = Angle.FromDegrees(180);
      Angle toAngle = Angle.FromDegrees(300);
      Angle lerpedAngle1 = Angle.Lerp(fromAngle, toAngle, .5f);
      Assert.IsTrue(lerpedAngle1.Equals(Angle.FromDegrees(240)));

      Angle toAngle2 = Angle.FromDegrees(400);
      Angle lerpedAngle2 = Angle.Lerp(fromAngle, toAngle2, 1.0f);
      Assert.IsTrue(lerpedAngle2.Equals(Angle.FromDegrees(40)));

    }
  }
}
