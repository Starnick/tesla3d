﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using NUnit.Framework;
using System;
using System.IO;
using Tesla.Scene;
using Tesla.Content;
using System.Text;
using Tesla.Graphics;

namespace Tesla.Core.Math
{
  [TestFixture]
  public class MeshClipperTests
  {
    [TestCase]
    public void TestClipperRedundantPoint()
    {
      //This configuration, found that the clip planes slice along the triangle along an edge or at a single vertex. We end up with 4 points, when it really
      //should be 3, because the last edge going out has a distance of zero -- so the points are right on top of each other

      MeshClipper clipper = new MeshClipper();
      clipper.ClipPlanes.Add(new Plane(new Vector3(1, 0, 0), new Vector3(225, 0, 0)));
      clipper.ClipPlanes.Add(new Plane(new Vector3(-1, 0, 0), new Vector3(250, 0, 0)));
      clipper.ClipPlanes.Add(new Plane(new Vector3(0, 0, 1), new Vector3(0, 0, -250)));
      clipper.ClipPlanes.Add(new Plane(new Vector3(0, 0, -1), new Vector3(0, 0, -225)));

      MeshData md = new MeshData();
      md.Positions = new DataBuffer<Vector3>(new Vector3[] { new Vector3(200, 2.769643f, -200), new Vector3(250, 2.769643f, -200), new Vector3(250, 2.769643f, -250) });
      md.UpdateVertexAndPrimitiveCount();

      MeshData result = clipper.Clip(md);
      Assert.IsNotNull(result);

      for (int i = 0; i < result.PrimitiveCount; i++)
      {
        Triangle tri;
        result.GetPrimitive(i, out tri);

        Assert.IsFalse(tri.IsDegenerate);
      }
    }
  }
}
