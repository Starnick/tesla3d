﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using NUnit.Framework;

namespace Tesla.Core.Math
{
  [TestFixture]
  public class QuaternionTests
  {
    [TestCase]
    public void FromToRotation()
    {
      Vector3 from = new Vector3(0, 1, 0);
      Vector3 to = new Vector3(1, 0, 0);
      Quaternion rot = Quaternion.FromToRotation(from, to);

      Vector3 transformedDir = Vector3.Transform(from, rot);
      Assert.IsTrue(to.Equals(transformedDir));

      // Parallel case
      rot = Quaternion.FromToRotation(from, from);
      Assert.IsTrue(rot.IsIdentity);

      // Parallel but opposite case
      Vector3 negFrom = Vector3.Negate(from);
      rot = Quaternion.FromToRotation(from, negFrom);
      transformedDir = Vector3.Transform(from, rot);
      Assert.IsTrue(negFrom.Equals(transformedDir));
    }
  }
}
