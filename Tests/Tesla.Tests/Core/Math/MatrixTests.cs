﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using NUnit.Framework;
using System;
using System.IO;
using Tesla.Scene;
using Tesla.Content;
using System.Text;
using Tesla.Graphics;

namespace Tesla.Core.Math
{
  [TestFixture]
  public class MatrixTests
  {
    [TestCase]
    public void FromFixedPoint()
    {
      Matrix rot = Matrix.FromAngleAxis(Angle.FromDegrees(45), Vector3.Normalize(Vector3.One));
      Vector3 pt = new Vector3(25, 35, -15);

      Matrix transform = Matrix.FromFixedPoint(pt, rot);
      Matrix.FromFixedPoint(pt, rot, out Matrix transform2);
      Assert.IsTrue(transform.Equals(transform2));

      Vector3 transformedPt = Vector3.Transform(pt, transform);
      Assert.IsTrue(transformedPt.Equals(pt));
    }
  }
}
