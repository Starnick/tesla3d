﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using NUnit.Framework;

namespace Tesla.Core.Buffers
{
  [TestFixture]
  public class MemoryAllocatorTests
  {
    private void RunAllocateTest(IMemoryAllocator<Vector3> allocator)
    {
      int numElements = 5;
      IMemoryOwnerEx<Vector3> mem = allocator.Allocate(numElements);
      Assert.IsTrue(mem.Memory.Length == numElements);
      Assert.IsFalse(mem.IsDisposed);

      Span<Vector3> span = mem.Memory.Span;
      Vector3 v1 = new Vector3(5, 2, 3);
      span[2] = v1;
      Assert.IsTrue(v1.Equals(span[2]));
      Assert.IsTrue(v1.Equals(mem[2]));

      IMemoryOwnerEx<Vector3> cloned = mem.Clone();
      Assert.IsNotNull(cloned);
      Assert.IsTrue(cloned.Memory.Length == numElements);
      Assert.IsTrue(cloned.Memory.Span[2].Equals(v1));
      Assert.IsTrue(cloned[2].Equals(v1));

      mem.Dispose();
      Assert.IsTrue(mem.IsDisposed);

      cloned.Dispose();
    }

    private void RunEmptyTest(IMemoryAllocator<Vector3> allocator)
    {
      IMemoryOwnerEx<Vector3> emptyMem = allocator.Allocate(0);
      Assert.IsNotNull(emptyMem);
      Assert.IsTrue(emptyMem.Memory.Length == 0);
      Assert.IsFalse(emptyMem.IsDisposed);

      emptyMem.Reallocate(5);
      Assert.IsTrue(emptyMem.Memory.Length == 5);

      emptyMem.Reallocate(0);
      Assert.IsTrue(emptyMem.Memory.Length == 0);

      emptyMem.Dispose();

      // Allocate an empty and dispose without reallocation just to ensure jiggling it didn't do anything unexpected
      IMemoryOwnerEx<Vector3> empty = allocator.Allocate(0);
      Assert.IsTrue(empty.Memory.Length == 0);
      empty.Dispose();

      // Allocate non-empty and 'reallocate' it to zero
      IMemoryOwnerEx<Vector3> nonEmpty = allocator.Allocate(5);
      Assert.IsTrue(nonEmpty.Memory.Length == 5);
      nonEmpty.Dispose();
    }

    private void RunReallocateTest(IMemoryAllocator<Vector3> allocator)
    {
      int numElements = 5;
      IMemoryOwnerEx<Vector3> mem = allocator.Allocate(numElements);
      mem.Memory.Span.Fill(new Vector3(1, 1, 1));

      Vector3 v1 = new Vector3(5, 2, 3);
      mem.Memory.Span[2] = v1;

      mem.Reallocate(10);
      Assert.IsTrue(mem.Memory.Length == 10);
      Assert.IsTrue(v1.Equals(mem.Memory.Span[2]));
      Assert.IsTrue(mem.Memory.Span[9].IsAlmostZero()); // Should have zeroed out larger memory

      mem.Reallocate(9, false);
      Assert.IsTrue(mem.Memory.Length == 9);
      Assert.IsTrue(v1.Equals(mem.Memory.Span[2]));

      mem.Reallocate(5, true);
      Assert.IsTrue(mem.Memory.Length == 5);
      Assert.IsTrue(v1.Equals(mem.Memory.Span[2]));

      mem.Dispose();
    }

    [TestCase]
    public void Default_Allocate()
    {
      RunAllocateTest(MemoryAllocator<Vector3>.Default);
    }

    [TestCase]
    public void Default_Reallocate()
    {
      RunReallocateTest(MemoryAllocator<Vector3>.Default);
    }

    [TestCase]
    public void Default_EmptyMemory()
    {
      RunEmptyTest(MemoryAllocator<Vector3>.Default);
    }

    [TestCase]
    public void Pinned_Allocate()
    {
      RunAllocateTest(MemoryAllocator<Vector3>.PinnedManaged);
    }

    [TestCase]
    public void Pinned_Reallocate()
    {
      RunReallocateTest(MemoryAllocator<Vector3>.PinnedManaged);
    }

    [TestCase]
    public void Pinned_EmptyMemory()
    {
      RunEmptyTest(MemoryAllocator<Vector3>.PinnedManaged);
    }

    [TestCase]
    public void Pooled_Allocate()
    {
      RunAllocateTest(MemoryAllocator<Vector3>.PooledManaged);
    }

    [TestCase]
    public void Pooled_Reallocate()
    {
      RunReallocateTest(MemoryAllocator<Vector3>.PooledManaged);
    }

    [TestCase]
    public void Pooled_EmptyMemory()
    {
      RunEmptyTest(MemoryAllocator<Vector3>.PooledManaged);
    }

    [TestCase]
    public void Native_Allocate()
    {
      RunAllocateTest(MemoryAllocator<Vector3>.Native);
    }

    [TestCase]
    public void Native_Reallocate()
    {
      RunReallocateTest(MemoryAllocator<Vector3>.Native);
    }

    [TestCase]
    public void Native_EmptyMemory()
    {
      RunEmptyTest(MemoryAllocator<Vector3>.Native);
    }
  }
}
