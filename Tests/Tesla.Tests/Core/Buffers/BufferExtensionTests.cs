﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using NUnit.Framework;

namespace Tesla.Core.Buffers
{
  [TestFixture]
  public class BufferExtensionTests
  {
    [TestCase]
    public void CopyBytesArray()
    {
      int[] valuesIn = new int[] { 5, 2, 1 };
      int[] valuesOut = new int[3];

      valuesIn.AsSpan().CopyTo(valuesOut.AsBytes());

      for (int i = 0; i < valuesIn.Length; i++)
        Assert.IsTrue(valuesIn[i] == valuesOut[i]);
    }

    [TestCase]
    public void CopyToIntPtr()
    {
      RawBuffer<int> buf = new RawBuffer<int>(10);
      Span<int> span = stackalloc int[10];
      for (int i = 0; i < span.Length; i++)
        span[i] = i + 1;

      // Copy to a bag of bytes
      span.CopyTo(buf.Bytes);
      for (int i = 0; i < span.Length; i++)
        Assert.IsTrue(span[i] == buf[i]);

      buf.Span.Clear();

      // Copy to an intptr
      span.CopyTo(buf.Pointer, buf.SizeInBytes);
      for (int i = 0; i < span.Length; i++)
        Assert.IsTrue(span[i] == buf[i]);

      buf.Span.Clear();

      // Copy from a smaller buffer
      span.Slice(5).CopyTo(buf.Pointer, buf.SizeInBytes);
      for (int i = 5; i < span.Length; i++)
        Assert.IsTrue(span[i] == buf[i - 5]);

      buf.Dispose();
    }

    [TestCase]
    public void CopyFromIntPtr()
    {
      RawBuffer<int> buf = new RawBuffer<int>(10);
      Span<int> span = stackalloc int[10];
      for (int i = 0; i < buf.Length; i++)
        buf[i] = i + 1;

      // Copy from intptr
      span.CopyFrom(buf.Pointer, buf.SizeInBytes);
      for (int i = 0; i < span.Length; i++)
        Assert.IsTrue(span[i] == buf[i]);

      span.Clear();

      // Copy from intptr at a specified index
      span.CopyFrom(2, buf.Pointer, buf.ElementSizeInBytes * (buf.Length - 2));
      for (int i = 2; i < span.Length; i++)
        Assert.IsTrue(span[i] == buf[i - 2]);

      buf.Dispose();
    }

    [TestCase]
    public void CopyToFromBytes()
    {
      Span<Vector3> span = stackalloc[] { new Vector3(2, 2, 2), new Vector3(3, 3, 3), new Vector3(5, 5, 5) };
      Span<byte> bytes = stackalloc byte[span.Length * Vector3.SizeInBytes];
      span.CopyTo(bytes); // T to bytes
      Span<Vector3> span2 = stackalloc Vector3[3];
      span2.CopyFrom(bytes); // T from bytes

      for (int i = 0; i < span2.Length; i++)
        Assert.IsTrue(span2[i].Equals(span[i]));

      Span<byte> singleElemBytes = stackalloc byte[Vector3.SizeInBytes];
      singleElemBytes.CopyFrom(bytes.Slice(2 * Vector3.SizeInBytes)); // T from bytes
      ref Vector3 singleElem = ref singleElemBytes.AsRef<Vector3>();
      Assert.IsTrue(singleElem.Equals(span[2]));

      span2[1] = new Vector3(10, 10, 10);
      bytes.Slice(1 * Vector3.SizeInBytes).CopyFrom<Vector3>(span2.Slice(1, 1)); // bytes from T

      ref Vector3 elem = ref bytes.Slice(1 * Vector3.SizeInBytes).AsRef<Vector3>();
      Assert.IsTrue(elem.Equals(span2[1]));

      bytes.Slice(1 * Vector3.SizeInBytes).CopyTo<Vector3>(span); // bytes to T
      Assert.IsTrue(elem.Equals(span[0]));
    }

    [TestCase]
    public void ReadAndStep()
    {
      Span<float> span = new float[] { 1, 2, 3, 4, 5 };
      Span<byte> bytes = span.AsBytes();
      for (int i = 0; i < span.Length; i++)
      {
        Assert.IsFalse(bytes.IsEmpty);
        float val = bytes.ReadAndStep<float>();
        Assert.IsTrue(val == span[i]);
      }

      Assert.IsTrue(bytes.IsEmpty);

      // Read-only flavor
      ReadOnlySpan<byte> robytes = span.AsBytes();
      for (int i = 0; i < span.Length; i++)
      {
        Assert.IsFalse(robytes.IsEmpty);
        float val = robytes.ReadAndStep<float>();
        Assert.IsTrue(val == span[i]);
      }

      Assert.IsTrue(robytes.IsEmpty);
    }

    [TestCase]
    public void ReadAndStepOffset()
    {
      // Read all the 1's from the buffer while skipping over 2's
      Span<long> span = new long[] { 1, 2, 2, 1, 2, 2, 1 };
      Span<byte> bytes = span.AsBytes();

      for (int i = 0; i < 3; i++)
      {
        Assert.IsFalse(bytes.IsEmpty);
        long val = bytes.ReadAndStep<long>(sizeof(long) * 2);
        Assert.IsTrue(val == 1);
      }

      Assert.IsTrue(bytes.IsEmpty);

      // Read-only flavor

      ReadOnlySpan<byte> robytes = span.AsBytes();

      for (int i = 0; i < 3; i++)
      {
        Assert.IsFalse(robytes.IsEmpty);
        long val = robytes.ReadAndStep<long>(sizeof(long) * 2);
        Assert.IsTrue(val == 1);
      }

      Assert.IsTrue(robytes.IsEmpty);
    }
  }
}
