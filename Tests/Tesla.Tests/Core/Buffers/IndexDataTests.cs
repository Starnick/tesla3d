﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Buffers;
using System.Collections.Generic;
using NUnit.Framework;

namespace Tesla.Core.Buffers
{
  [TestFixture]
  public class IndexDataTests
  {
    [TestCase]
    public void ConstructorAndPropertyTests()
    {
      List<IndexData> list = new List<IndexData>();
      List<IndexFormat> formats = new List<IndexFormat>();
      List<int> elemSizes = new List<int>();
      List<Type> elemTypes = new List<Type>();

      int numElems = 5;

      Action<IndexData> addInts = (IndexData ib) =>
      {
        list.Add(ib);
        formats.Add(IndexFormat.ThirtyTwoBits);
        elemSizes.Add(sizeof(int));
        elemTypes.Add(typeof(int));
      };

      Action<IndexData> addShorts = (IndexData ib) =>
      {
        list.Add(ib);
        formats.Add(IndexFormat.SixteenBits);
        elemSizes.Add(sizeof(ushort));
        elemTypes.Add(typeof(ushort));
      };

      addInts(new IndexData(numElems));
      addShorts(new IndexData(numElems, IndexFormat.SixteenBits));
      addInts(new IndexData(numElems, IndexFormat.ThirtyTwoBits));
      addInts(new IndexData(new int[] { 1, 2, 3, 4, 5 }));
      addShorts(new IndexData(new ushort[] { 1, 2, 3, 4, 5 }));
      addInts(new IndexData(new DataBuffer<int>(numElems)));
      addShorts(new IndexData(new DataBuffer<ushort>(numElems)));

      // Test implicit ops
      addShorts(new DataBuffer<ushort>(numElems));
      addInts(new DataBuffer<int>(numElems));

      // Test default ctor
      IndexData empty = new IndexData();
      Assert.IsFalse(empty.IsValid);
      Assert.IsTrue(empty.UnderlyingDataBuffer == null);

      // Test different ctor variations
      for(int i = 0; i < list.Count; i++)
      {
        IndexData db = list[i];
        IndexFormat iformat = formats[i];
        int elemSize = elemSizes[i];
        Type elemType = elemTypes[i];
        int size = elemSize * numElems;

        Assert.IsTrue(db.ElementSizeInBytes == elemSize);
        Assert.IsTrue(db.SizeInBytes == size);
        Assert.IsTrue(db.ElementType == elemType);
        Assert.IsTrue(db.Length == numElems);
        Assert.IsTrue(db.UnderlyingDataBuffer != null);

        if (db.Is32Bit)
          Assert.IsTrue(db.DataBuffer32 != null);
        else
          Assert.IsTrue(db.DataBuffer16 != null);

        ReadOnlySpan<byte> roBytes = db.Bytes;
        Assert.IsTrue(roBytes.Length == size);

        // Look at pinning and writing data
        MemoryHandle handle = db.Pin();
        Assert.IsTrue(handle.AsIntPointer() != IntPtr.Zero);

        Span<int> vals = stackalloc[] { 15, 5, 2, 1, 6 };
        Span<ushort> valShorts = stackalloc ushort[] { 15, 5, 2, 1, 6 };
        if (db.IndexFormat == IndexFormat.SixteenBits)
        {
          valShorts.CopyTo<ushort>(handle.AsIntPointer(), size);
        }
        else
        {
          vals.CopyTo<int>(handle.AsIntPointer(), size);
        }

        for (int j = 0; j < vals.Length; j++)
          Assert.IsTrue(vals[j] == db[j]);

        handle.Dispose();

        // Copy bytes
        vals[0] = 1;
        valShorts[0] = 1;

        if (db.Is32Bit)
        {
          Span<byte> valBytes = vals.AsBytes();
          valBytes.CopyTo(db.Bytes);
        } 
        else
        {
          Span<byte> valShortsBytes = valShorts.AsBytes();
          valShortsBytes.CopyTo(db.Bytes);
        }

        Assert.IsTrue(db[0] == 1);

        db.Dispose();
        Assert.IsTrue(db.IsDisposed);
      }
    }

    [TestCase]
    public void CloneClear()
    {
      IndexData db = new DataBuffer<int>(10);
      for (int i = 0; i < db.Length; i++)
        db[i] = i + 1;

      IndexData db2 = db.Clone();
      Assert.IsTrue(db.Length == db2.Length);

      for (int i = 0; i < db2.Length; i++)
        Assert.IsTrue(db[i] == db2[i]);

      db2.Clear();
      foreach (int db2val in db2)
        Assert.IsTrue(db2val == 0);

      db.Dispose();
      db2.Dispose();
    }

    [TestCase]
    public void TestEnumerator()
    {
      var runTests = (IndexData db) =>
      {
        for (int i = 0; i < db.Length; i++)
          db[i] = i + 1;

        int val = 1;
        foreach (int v in db)
          Assert.IsTrue(val++ == v);

        db.Dispose();
      };

      runTests(new DataBuffer<int>(10));
      runTests(new DataBuffer<ushort>(10));
    }

    [TestCase]
    public void Resize()
    {
      IndexData db = DataBuffer.Create<int>(10);
      db.Resize(20);
      Assert.IsTrue(db.Length == 20);
      db.Resize(5);
      Assert.IsTrue(db.Length == 5);
      db.Dispose();
    }

    [TestCase]
    public void PromoteTo32Bit()
    {
      IndexData db = DataBuffer.Create<ushort>(10);
      for (int i = 0; i < db.Length; i++)
        db[i] = i + 1;

      Assert.IsFalse(db.Is32Bit);
      IDataBuffer oldDb = db.UnderlyingDataBuffer;
      db.PromoteTo32Bits();
      Assert.IsTrue(oldDb.IsDisposed);
      Assert.IsTrue(db.Is32Bit);
      Assert.IsTrue(db.ElementSizeInBytes == sizeof(int));
      Assert.IsTrue(db.ElementType == typeof(int));

      for (int i = 0; i < db.Length; i++)
        Assert.IsTrue(db[i] == (i + 1));

      db.Dispose();
    }

    [TestCase]
    public void ResizeAndPromoteTo32Bit()
    {
      int oldLength = 10;
      IndexData db = DataBuffer.Create<ushort>(oldLength);
      for (int i = 0; i < db.Length; i++)
        db[i] = i + 1;

      Assert.IsFalse(db.Is32Bit);
      IDataBuffer oldDb = db.UnderlyingDataBuffer;

      int newLength = 20;
      db.ResizeAndPromoteTo32Bits(newLength);
      Assert.IsTrue(oldDb.IsDisposed);
      Assert.IsTrue(db.Is32Bit);
      Assert.IsTrue(db.ElementSizeInBytes == sizeof(int));
      Assert.IsTrue(db.ElementType == typeof(int));
      Assert.IsTrue(db.Length == newLength);
      Assert.IsTrue(db.Length > oldLength);

      for (int i = 0; i < oldLength; i++)
        Assert.IsTrue(db[i] == (i + 1));

      db.Dispose();

      // Case when buffer shrinks
      db = DataBuffer.Create<ushort>(oldLength);
      for (int i = 0; i < db.Length; i++)
        db[i] = i + 1;

      newLength = 5;
      db.ResizeAndPromoteTo32Bits(newLength);
      Assert.IsTrue(db.Length == newLength);
      Assert.IsTrue(db.Length < oldLength);

      for (int i = 0; i < newLength; i++)
        Assert.IsTrue(db[i] == (i + 1));

      db.Dispose();
    }

    [TestCase]
    public void CopySlice()
    {
      var runTests = (IndexData db) =>
      {
        IndexData db2 = db.CopySlice(5, 2);
        Assert.IsTrue(db2.Length == 2);
        Assert.IsTrue(db2[0] == db[5]);
        Assert.IsTrue(db2[1] == db[6]);
        Assert.IsTrue(db2.IndexFormat == db.IndexFormat);

        db.Dispose();
        db2.Dispose();
      };

      runTests(new DataBuffer<int>(new int[] { 5, 6, 2, 1, 10, 22, 525, 5, 25, 1 }));
      runTests(new DataBuffer<ushort>(new ushort[] { 5, 6, 2, 1, 10, 22, 525, 5, 25, 1 }));
    }

    [TestCase]
    public void ContainsIndexOf()
    {
      var runTests = (IndexData db) =>
      {
        Assert.IsTrue(db.Contains(5));
        Assert.IsTrue(db.IndexOf(5, 4) != -1);
        Assert.IsTrue(db.IndexOf(10, 5) == -1);

        db.Dispose();
      };

      runTests(new DataBuffer<int>(new int[] { 5, 6, 2, 1, 10, 22, 525, 5, 25, 1 }));
      runTests(new DataBuffer<ushort>(new ushort[] { 5, 6, 2, 1, 10, 22, 525, 5, 25, 1 }));
    }

    [TestCase]
    public void CopyFrom_Span()
    {
      var runTests = (IndexData db) =>
      {
        Span<int> originData = new int[] { 5, 6, 2, 1, 10, 22, 525, 5, 25, 1 };

        Assert.IsTrue(db.CopyFrom(originData, false) == 6);
        Assert.IsTrue(db.CopyFrom(originData, true) == 10);

        db.Clear();
        db.Resize(10);

        // Overwrite the last index with the copy
        Assert.IsTrue(db.CopyFrom(0, originData, true) == 10);
        Assert.IsTrue(db.CopyFrom(9, originData, true) == 10);
        Assert.IsTrue(db.Length == 19);

        // Append the buffers
        db.Resize(10);
        Assert.IsTrue(db.CopyFrom(10, originData, true) == 10);
        Assert.IsTrue(db.Length == 20);

        // Start an index that is one beyond the current size
        db.Resize(10);
        Assert.IsTrue(db.CopyFrom(11, originData, true) == 10);
        Assert.IsTrue(db.Length == 21);
        Assert.IsTrue(db[10] == 0);

        db.Dispose();
      };

      var runTestsShort = (IndexData db) =>
      {
        Span<ushort> originData = new ushort[] { 5, 6, 2, 1, 10, 22, 525, 5, 25, 1 };

        Assert.IsTrue(db.CopyFrom(originData, false) == 6);
        Assert.IsTrue(db.CopyFrom(originData, true) == 10);

        db.Clear();
        db.Resize(10);

        // Overwrite the last index with the copy
        Assert.IsTrue(db.CopyFrom(0, originData, true) == 10);
        Assert.IsTrue(db.CopyFrom(9, originData, true) == 10);
        Assert.IsTrue(db.Length == 19);

        // Append the buffers
        db.Resize(10);
        Assert.IsTrue(db.CopyFrom(10, originData, true) == 10);
        Assert.IsTrue(db.Length == 20);

        // Start an index that is one beyond the current size
        db.Resize(10);
        Assert.IsTrue(db.CopyFrom(11, originData, true) == 10);
        Assert.IsTrue(db.Length == 21);
        Assert.IsTrue(db[10] == 0);

        db.Dispose();
      };

      runTests(new DataBuffer<int>(6));
      runTestsShort(new DataBuffer<int>(6));

      runTests(new DataBuffer<ushort>(6));
      runTestsShort(new DataBuffer<ushort>(6));
    }

    [TestCase]
    public void CopyFrom_Array()
    {
      var runTests = (IndexData db) =>
      {
        int[] originData = new int[] { 5, 6, 2, 1, 10, 22, 525, 5, 25, 1 };

        Assert.IsTrue(db.CopyFrom(originData, false) == 6);
        Assert.IsTrue(db.CopyFrom(originData, true) == 10);

        db.Clear();
        db.Resize(10);

        // Overwrite the last index with the copy
        Assert.IsTrue(db.CopyFrom(0, originData, true) == 10);
        Assert.IsTrue(db.CopyFrom(9, originData, true) == 10);
        Assert.IsTrue(db.Length == 19);

        // Append the buffers
        db.Resize(10);
        Assert.IsTrue(db.CopyFrom(10, originData, true) == 10);
        Assert.IsTrue(db.Length == 20);

        // Start an index that is one beyond the current size
        db.Resize(10);
        Assert.IsTrue(db.CopyFrom(11, originData, true) == 10);
        Assert.IsTrue(db.Length == 21);
        Assert.IsTrue(db[10] == 0);

        db.Dispose();
      };

      var runTestsShort = (IndexData db) =>
      {
        ushort[] originData = new ushort[] { 5, 6, 2, 1, 10, 22, 525, 5, 25, 1 };

        Assert.IsTrue(db.CopyFrom(originData, false) == 6);
        Assert.IsTrue(db.CopyFrom(originData, true) == 10);

        db.Clear();
        db.Resize(10);

        // Overwrite the last index with the copy
        Assert.IsTrue(db.CopyFrom(0, originData, true) == 10);
        Assert.IsTrue(db.CopyFrom(9, originData, true) == 10);
        Assert.IsTrue(db.Length == 19);

        // Append the buffers
        db.Resize(10);
        Assert.IsTrue(db.CopyFrom(10, originData, true) == 10);
        Assert.IsTrue(db.Length == 20);

        // Start an index that is one beyond the current size
        db.Resize(10);
        Assert.IsTrue(db.CopyFrom(11, originData, true) == 10);
        Assert.IsTrue(db.Length == 21);
        Assert.IsTrue(db[10] == 0);

        db.Dispose();
      };



      runTests(new DataBuffer<int>(6));
      runTestsShort(new DataBuffer<int>(6));

      runTests(new DataBuffer<ushort>(6));
      runTestsShort(new DataBuffer<ushort>(6));
    }

    [TestCase]
    public void CopyFrom_Enumerable()
    {
      TestCopyFrom(new List<int>(new int[] { 5, 6, 2, 1, 10, 22, 525, 5, 25, 1 }), new List<ushort>(new ushort[] { 5, 6, 2, 1, 10, 22, 525, 5, 25, 1 }));
      TestCopyFrom(new int[] { 5, 6, 2, 1, 10, 22, 525, 5, 25, 1 }, new ushort[] { 5, 6, 2, 1, 10, 22, 525, 5, 25, 1 });
      TestCopyFrom(new DataBuffer<int>(new int[] { 5, 6, 2, 1, 10, 22, 525, 5, 25, 1 }), new DataBuffer<ushort>(new ushort[] { 5, 6, 2, 1, 10, 22, 525, 5, 25, 1 }));
      TestCopyFrom(GetEnumerable(new int[] { 5, 6, 2, 1, 10, 22, 525, 5, 25, 1 }), GetEnumerableShort(new ushort[] { 5, 6, 2, 1, 10, 22, 525, 5, 25, 1 }));

    }

    private IEnumerable<int> GetEnumerable(int[] arr)
    {
      foreach (int val in arr)
        yield return val;
    }

    private IEnumerable<ushort> GetEnumerableShort(ushort[] arr)
    {
      foreach (ushort val in arr)
        yield return val;
    }

    private void TestCopyFrom(IEnumerable<int> originData, IEnumerable<ushort> originDataShort)
    {
      var runTests = (IndexData db) =>
      {
        Assert.IsTrue(db.CopyFrom(originData, false) == 6);
        Assert.IsTrue(db.CopyFrom(originData, true) == 10);

        db.Clear();
        db.Resize(10);

        // Overwrite the last index with the copy
        Assert.IsTrue(db.CopyFrom(0, originData, true) == 10);
        Assert.IsTrue(db.CopyFrom(9, originData, true) == 10);
        Assert.IsTrue(db.Length == 19);

        // Append the buffers
        db.Resize(10);
        Assert.IsTrue(db.CopyFrom(10, originData, true) == 10);
        Assert.IsTrue(db.Length == 20);

        // Start an index that is one beyond the current size
        db.Resize(10);
        Assert.IsTrue(db.CopyFrom(11, originData, true) == 10);
        Assert.IsTrue(db.Length == 21);
        Assert.IsTrue(db[10] == 0);

        db.Dispose();
      };

      var runTestsShort = (IndexData db) =>
      {
        Assert.IsTrue(db.CopyFrom(originDataShort, false) == 6);
        Assert.IsTrue(db.CopyFrom(originDataShort, true) == 10);

        db.Clear();
        db.Resize(10);

        // Overwrite the last index with the copy
        Assert.IsTrue(db.CopyFrom(0, originDataShort, true) == 10);
        Assert.IsTrue(db.CopyFrom(9, originDataShort, true) == 10);
        Assert.IsTrue(db.Length == 19);

        // Append the buffers
        db.Resize(10);
        Assert.IsTrue(db.CopyFrom(10, originDataShort, true) == 10);
        Assert.IsTrue(db.Length == 20);

        // Start an index that is one beyond the current size
        db.Resize(10);
        Assert.IsTrue(db.CopyFrom(11, originDataShort, true) == 10);
        Assert.IsTrue(db.Length == 21);
        Assert.IsTrue(db[10] == 0);

        db.Dispose();
      };

      runTests(new DataBuffer<int>(6));
      runTestsShort(new DataBuffer<int>(6));

      runTests(new DataBuffer<ushort>(6));
      runTestsShort(new DataBuffer<ushort>(6));
    }
  }
}
