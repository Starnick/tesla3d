﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using NUnit.Framework;

namespace Tesla.Core.Buffers
{
  [TestFixture]
  public class IndexDataBuilderTests
  {
    [TestCase]
    public void ConstructorSimpleTest()
    {
      IndexDataBuilder builder = new IndexDataBuilder();
      Assert.IsTrue(builder.Count == 0);

      builder.Set(5);
      builder.Set(10);
      builder.Set(11);

      Assert.IsTrue(builder.Count == 3);
      Assert.IsTrue(builder.Position == 3);
      Assert.IsTrue(builder.Capacity == 4);
      Assert.IsTrue(builder.RemainingCount == 0);
      Assert.IsFalse(builder.HasNext);
      builder.Position = 0;
      Assert.IsTrue(builder.Position == 0);
      Assert.IsTrue(builder.HasNext);
      Assert.IsTrue(builder.SizeInBytes == (sizeof(int) * 3));
      Assert.IsTrue(builder.IndexFormat == IndexFormat.ThirtyTwoBits);
      Assert.IsTrue(builder.Is32Bit);
      Assert.IsTrue(!builder.RemainingSpan32.IsEmpty);
      Assert.IsTrue(builder.RemainingSpan16.IsEmpty);

      IndexData db = builder.Claim(true);
      Assert.IsTrue(db.Length == 3);
      Assert.IsTrue(builder.Capacity == 0);
      Assert.IsTrue(db.ElementSizeInBytes == builder.ElementSizeInBytes);
      Assert.IsTrue(db.ElementType == builder.ElementType);
      db.Dispose();
    }

    [TestCase]
    public void Constructor16BitAutoPromote()
    {
      IndexDataBuilder builder = IndexDataBuilder.CreateInitially16Bit();
      Assert.IsTrue(builder.IndexFormat == IndexFormat.SixteenBits);
      Assert.IsFalse(builder.Is32Bit);
      Assert.IsTrue(builder.AutoPromote);

      builder.Set(5);

      Assert.IsTrue(builder.SizeInBytes == (1 * sizeof(ushort)));

      builder.Set(ushort.MaxValue);

      Assert.IsTrue(builder.Is32Bit);
      Assert.IsTrue(builder.SizeInBytes == (2 * sizeof(int)));

      builder.IndexFormat = IndexFormat.SixteenBits;
      Assert.IsFalse(builder.Is32Bit);

      builder.Position = 0;
      builder.SetRange(new int[] { ushort.MaxValue, ushort.MaxValue + 5, 1 });
      Assert.IsTrue(builder.Is32Bit);

      builder.IndexFormat = IndexFormat.SixteenBits;
      Assert.IsFalse(builder.Is32Bit);

      builder.Position = 0;
      builder.SetRange(new ReadOnlyCollection<int>(new int[] { ushort.MaxValue, ushort.MaxValue + 5, 1 }));
      Assert.IsTrue(builder.Is32Bit);
    }

    [TestCase]
    public void GetSet()
    {
      IndexDataBuilder builder = new IndexDataBuilder();
      Assert.IsTrue(builder.Set(10) == 0);
      Assert.IsTrue(builder.Get(0) == 10);
      Assert.IsTrue(builder.Position == 1);
      Assert.IsTrue(builder.Count == 1);
      Assert.IsTrue(builder.RemainingCount == 0);
      Assert.IsFalse(builder.HasNext);

      builder.Position = 0;
      Assert.IsTrue(builder.Set(5) == 0);
      builder.Position = 0;
      Assert.IsTrue(builder.Get(0) == 5);
      Assert.IsTrue(builder.Get() == 5);
      Assert.IsTrue(builder.Position == 1);

      Assert.IsTrue(builder.Set(10) == 1);
      Assert.IsTrue(builder.Set(15) == 2);
      Assert.IsTrue(builder.Get(1) == 10);
      builder.Set(2, 25);
      Assert.IsTrue(builder.Get(2) == 25);

      Assert.Throws<ArgumentOutOfRangeException>(() =>
      {
        builder.Set(10, 10);
      });

      Assert.Throws<ArgumentOutOfRangeException>(() =>
      {
        builder.Set(-1, 10);
      });

      Assert.Throws<ArgumentOutOfRangeException>(() =>
      {
        builder.Position = builder.Count;
        builder.Get();
      });
    }

    [TestCase]
    public void TestCapture()
    {
      IndexDataBuilder builder = new IndexDataBuilder();
      IndexData ibuffer = new IndexData(10, IndexFormat.SixteenBits);
      builder.CaptureBuffer(ibuffer);
      Assert.IsTrue(builder.Capacity == 10);
      Assert.IsTrue(builder.IndexFormat == IndexFormat.SixteenBits);

      builder.Set(5);
      builder.Set(10);
      builder.Set(15);
      builder.Set(20);
      builder.Set(15);
      builder.Set(25);
      builder.Set(35);
      builder.Set(36);
      builder.Set(2);
      builder.Set(1);
      builder.Set(5);

      Assert.IsTrue(builder.Capacity > 10);
      Assert.IsTrue(builder.Count == 11);

      ibuffer = builder.Claim(true);
      Assert.IsTrue(ibuffer.Length == 11);
    }

    [TestCase]
    public void CopyRange()
    {
      IndexDataBuilder builder = new IndexDataBuilder();
      builder.Set(10);
      builder.Set(15);
      builder.Set(20);
      builder.Set(25);
      builder.Set(30);

      Span<int> range = stackalloc int[3];
      builder.CopyRange(1, range);
      Assert.IsTrue(range[0] == 15);
      Assert.IsTrue(range[1] == 20);
      Assert.IsTrue(range[2] == 25);

      Span<ushort> shortRange = stackalloc ushort[3];
      builder.CopyRange(1, shortRange);
      Assert.IsTrue(shortRange[0] == 15);
      Assert.IsTrue(shortRange[1] == 20);
      Assert.IsTrue(shortRange[2] == 25);

      Assert.Throws<ArgumentOutOfRangeException>(() =>
      {
        builder.CopyRange(-1, new int[2]);
      });

      Assert.Throws<ArgumentOutOfRangeException>(() =>
      {
        builder.CopyRange(1, new int[10]);
      });
    }

    [TestCase]
    public void HasNextFor()
    {
      IndexDataBuilder builder = IndexDataBuilder.CreateInitially16Bit();
      builder.Set(5);
      builder.Set(10);
      builder.Set(2);
      builder.Set(120);
      builder.Set(5);
      builder.Position = 1;
      Assert.IsTrue(builder.HasNext);
      Assert.IsTrue(builder.HasNextFor(3));
      Assert.IsTrue(builder.HasNextFor(4));
      Assert.IsFalse(builder.HasNextFor(5));

      int prevCapacity = builder.Capacity;

      // overwrite 4 times, should not grow
      builder.Set(5);
      builder.Set(5);
      builder.Set(5);
      builder.Set(5);

      Assert.IsTrue(builder.Capacity == prevCapacity);
      Assert.IsTrue(builder.Position == 5);

      IndexData db = builder.Claim(true);
      Assert.IsTrue(builder.IndexFormat == IndexFormat.SixteenBits);
      Assert.IsTrue(db.IndexFormat == IndexFormat.SixteenBits);
      Assert.IsTrue(db.Length == 5);
      foreach (int v in db)
        Assert.IsTrue(v == 5);
    }


    [TestCase]
    public void TestSetCount()
    {
      IndexDataBuilder builder = new IndexDataBuilder(10, IndexFormat.SixteenBits);
      builder.Count = 5;
      Assert.IsTrue(builder.Count == 5);

      builder.Position = 3;

      builder.Count = -5;
      Assert.IsTrue(builder.Count == 0);
      Assert.IsTrue(builder.Position == 0);

      builder.Count = 5;
      builder.Position = 3;

      builder.Count = 0;
      Assert.IsTrue(builder.Count == 0);
      Assert.IsTrue(builder.Position == 0);

      builder.Count = 20;
      builder.Position = 5;
      Assert.IsTrue(builder.Count == 20);
      Assert.IsTrue(builder.Position == 5);

      IndexData db = builder.Claim(true);
      Assert.IsTrue(db.Length == 20);
      Assert.IsTrue(builder.Count == 0);
      Assert.IsTrue(builder.Position == 0);

      db.Dispose();
      builder.Clear();
    }

    [TestCase]
    public void Span()
    {
      IndexDataBuilder builder = new IndexDataBuilder(0, IndexFormat.SixteenBits);
      builder.Set(5);
      builder.Set(10);
      builder.Set(2);
      builder.Set(120);
      builder.Set(5);
      builder.Position = 1;

      // Remaining spans
      Span<ushort> spanShort = builder.RemainingSpan16;
      Assert.IsTrue(spanShort.Length == 4);
      Assert.IsTrue(spanShort[0] == 10);

      Span<byte> spanShortByte = builder.RemainingBytes;
      Assert.IsTrue(spanShortByte.Length == (4 * sizeof(ushort)));

      Span<int> spanInt = builder.RemainingSpan32;
      Assert.IsTrue(spanInt.IsEmpty);

      // Span from start
      spanShort = builder.Span16;
      Assert.IsFalse(spanShort.IsEmpty);
      Assert.IsTrue(spanShort.Length == builder.Count);

      spanShortByte = builder.Bytes;
      Assert.IsTrue(spanShortByte.Length == (builder.Count * sizeof(ushort)));

      //
      // Test for 32bit
      //

      builder.IndexFormat = IndexFormat.ThirtyTwoBits;

      // Remaining spans
      spanShort = builder.RemainingSpan16;
      Assert.IsTrue(spanShort.IsEmpty);

      Span<byte> spanIntByte = builder.RemainingBytes;
      Assert.IsTrue(spanIntByte.Length == (4 * sizeof(int)));

      spanInt = builder.RemainingSpan32;
      Assert.IsTrue(spanInt.Length == 4);
      Assert.IsTrue(spanInt[0] == 10);

      builder.Position = 0;
      spanInt = builder.RemainingSpan32;
      Assert.IsTrue(spanInt.Length == 5);
      Assert.IsTrue(spanInt[0] == 5);

      builder.Position = builder.Count;
      spanInt = builder.RemainingSpan32;
      Assert.IsTrue(spanInt.IsEmpty);

      // Span from start
      // Span from start
      spanInt = builder.Span32;
      Assert.IsFalse(spanInt.IsEmpty);
      Assert.IsTrue(spanInt.Length == builder.Count);

      spanIntByte = builder.Bytes;
      Assert.IsTrue(spanIntByte.Length == (builder.Count * sizeof(int)));
    }

    [TestCase]
    public void Claim()
    {
      IndexDataBuilder builder = IndexDataBuilder.CreateInitially16Bit();
      builder.Set(5);
      builder.Set(10);
      builder.Set(2);
      builder.Set(120);
      builder.Set(5);

      IndexData db = builder.Claim(false);
      Assert.IsTrue(builder.IndexFormat == IndexFormat.SixteenBits);
      Assert.IsFalse(builder.Is32Bit);
      Assert.IsTrue(db.IndexFormat == IndexFormat.SixteenBits);
      Assert.IsTrue(db.Length != 5);
      Assert.IsTrue(db[0] == 5);

      Assert.IsTrue(builder.Count == 0);
      Assert.IsTrue(builder.Position == 0);

      builder.IndexFormat = IndexFormat.ThirtyTwoBits;

      builder.Set(5);
      builder.Set(10);
      builder.Set(2);
      builder.Set(120);
      builder.Set(5);

      Assert.IsTrue(builder.Count == 5);

      IndexData db2 = builder.Claim(true);
      Assert.IsTrue(builder.IndexFormat == IndexFormat.ThirtyTwoBits);
      Assert.IsTrue(builder.Is32Bit);
      Assert.IsTrue(builder.Position == 0);
      Assert.IsTrue(db2.IndexFormat == IndexFormat.ThirtyTwoBits);
      Assert.IsTrue(db2.Length == 5);
      Assert.IsTrue(db2[0] == 5);
      Assert.IsTrue(db2[1] == 10);
      Assert.IsTrue(db2[2] == 2);
      Assert.IsTrue(db2[3] == 120);
      Assert.IsTrue(db2[4] == 5);
    }

    [TestCase]
    public void SetRange()
    {
      TestSetRange(new List<int>(new int[] { 5, 6, 2, 1, 10, 22, 525, 5, 25, 1 }), new List<ushort>(new ushort[] { 5, 6, 2, 1, 10, 22, 525, 5, 25, 1 }));
      TestSetRange((IEnumerable<int>) new int[] { 5, 6, 2, 1, 10, 22, 525, 5, 25, 1 }, (IEnumerable<ushort>) new ushort[] { 5, 6, 2, 1, 10, 22, 525, 5, 25, 1 });
      TestSetRange(new int[] { 5, 6, 2, 1, 10, 22, 525, 5, 25, 1 }, new ushort[] { 5, 6, 2, 1, 10, 22, 525, 5, 25, 1 });
      TestSetRange(new ReadOnlySpan<int>(new int[] { 5, 6, 2, 1, 10, 22, 525, 5, 25, 1 }), new ReadOnlySpan<ushort>(new ushort[] { 5, 6, 2, 1, 10, 22, 525, 5, 25, 1 }));
      TestSetRange((ReadOnlySpan<int>) new DataBuffer<int>(new int[] { 5, 6, 2, 1, 10, 22, 525, 5, 25, 1 }), (ReadOnlySpan<ushort>) new DataBuffer<ushort>(new ushort[] { 5, 6, 2, 1, 10, 22, 525, 5, 25, 1 }));
      TestSetRange(GetEnumerable(new int[] { 5, 6, 2, 1, 10, 22, 525, 5, 25, 1 }), GetEnumerableShort(new ushort[] { 5, 6, 2, 1, 10, 22, 525, 5, 25, 1 }));
    }

    private IEnumerable<int> GetEnumerable(int[] arr)
    {
      foreach (int val in arr)
        yield return val;
    }

    private IEnumerable<ushort> GetEnumerableShort(ushort[] arr)
    {
      foreach (ushort val in arr)
        yield return val;
    }

    private void TestSetRange(IEnumerable<int> originData, IEnumerable<ushort> originDataShort)
    {
      DataBufferBuilder<int> builder = new DataBufferBuilder<int>();
      var runTests = (IndexDataBuilder builder) =>
      {
        builder.SetRange(originData);
        Assert.IsTrue(builder.Count > 0);
        Assert.IsTrue(builder.Position == builder.Count);

        int capacity = builder.Capacity;
        int count = builder.Count;

        builder.SetRange(originData, 10);
        Assert.IsTrue(builder.Capacity > capacity);
        Assert.IsTrue(builder.Count > count);

        builder.Position = 0;

        foreach (int v in originData)
          Assert.IsTrue(builder.Get() == v);

        foreach (int v in originData)
          Assert.IsTrue(builder.Get() == (v + 10));

        builder.Claim().Dispose();
      };

      var runTestsShort = (IndexDataBuilder builder) =>
      {
        builder.SetRange(originData);
        Assert.IsTrue(builder.Count > 0);
        Assert.IsTrue(builder.Position == builder.Count);

        int capacity = builder.Capacity;
        int count = builder.Count;

        builder.SetRange(originData, 10);
        Assert.IsTrue(builder.Capacity > capacity);
        Assert.IsTrue(builder.Count > count);

        builder.Position = 0;

        foreach (int v in originData)
          Assert.IsTrue(builder.Get() == v);

        foreach (int v in originData)
          Assert.IsTrue(builder.Get() == (v + 10));

        builder.Claim().Dispose();
      };

      runTests(new IndexDataBuilder());
      runTestsShort(new IndexDataBuilder());

      runTests(IndexDataBuilder.CreateInitially16Bit());
      runTestsShort(IndexDataBuilder.CreateInitially16Bit());
    }

    private void TestSetRange(int[] originData, ushort[] originDataShort)
    {
      var runTests = (IndexDataBuilder builder) =>
      {
        builder.SetRange(originData);
        Assert.IsTrue(builder.Count > 0);
        Assert.IsTrue(builder.Position == builder.Count);

        int capacity = builder.Capacity;
        int count = builder.Count;

        builder.SetRange(originData, 10);
        Assert.IsTrue(builder.Capacity > capacity);
        Assert.IsTrue(builder.Count > count);

        builder.Position = 0;

        foreach (int v in originData)
          Assert.IsTrue(builder.Get() == v);

        foreach (int v in originData)
          Assert.IsTrue(builder.Get() == (v + 10));

        builder.Claim().Dispose();
      };

      var runTestsShort = (IndexDataBuilder builder) =>
      {
        builder.SetRange(originDataShort);
        Assert.IsTrue(builder.Count > 0);
        Assert.IsTrue(builder.Position == builder.Count);

        int capacity = builder.Capacity;
        int count = builder.Count;

        builder.SetRange(originDataShort, 10);
        Assert.IsTrue(builder.Capacity > capacity);
        Assert.IsTrue(builder.Count > count);

        builder.Position = 0;

        foreach (int v in originDataShort)
          Assert.IsTrue(builder.Get() == v);

        foreach (int v in originDataShort)
          Assert.IsTrue(builder.Get() == (v + 10));

        builder.Claim().Dispose();
      };

      runTests(new IndexDataBuilder());
      runTestsShort(new IndexDataBuilder());

      runTests(IndexDataBuilder.CreateInitially16Bit());
      runTestsShort(IndexDataBuilder.CreateInitially16Bit());
    }

    private void TestSetRange(ReadOnlySpan<int> originDataSpan, ReadOnlySpan<ushort> originDataShortSpan)
    {
      var runTests = (IndexDataBuilder builder, ReadOnlySpan<int> originData) =>
      {
        builder.SetRange(originData);
        Assert.IsTrue(builder.Count > 0);
        Assert.IsTrue(builder.Position == builder.Count);

        int capacity = builder.Capacity;
        int count = builder.Count;

        builder.SetRange(originData, 10);
        Assert.IsTrue(builder.Capacity > capacity);
        Assert.IsTrue(builder.Count > count);

        builder.Position = 0;

        foreach (int v in originData)
          Assert.IsTrue(builder.Get() == v);

        foreach (int v in originData)
          Assert.IsTrue(builder.Get() == (v + 10));

        builder.Claim().Dispose();
      };

      var runTestsShort = (IndexDataBuilder builder, ReadOnlySpan<ushort> originData) =>
      {
        builder.SetRange(originData);
        Assert.IsTrue(builder.Count > 0);
        Assert.IsTrue(builder.Position == builder.Count);

        int capacity = builder.Capacity;
        int count = builder.Count;

        builder.SetRange(originData, 10);
        Assert.IsTrue(builder.Capacity > capacity);
        Assert.IsTrue(builder.Count > count);

        builder.Position = 0;

        foreach (int v in originData)
          Assert.IsTrue(builder.Get() == v);

        foreach (int v in originData)
          Assert.IsTrue(builder.Get() == (v + 10));

        builder.Claim().Dispose();
      };

      runTests(new IndexDataBuilder(), originDataSpan);
      runTestsShort(new IndexDataBuilder(), originDataShortSpan);

      runTests(IndexDataBuilder.CreateInitially16Bit(), originDataSpan);
      runTestsShort(IndexDataBuilder.CreateInitially16Bit(), originDataShortSpan);
    }
  }
}
