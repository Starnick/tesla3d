﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Runtime.InteropServices;
using NUnit.Framework;

namespace Tesla.Core.Buffers
{

  [TestFixture]
  public class RawBufferTests
  {
    [TestCase]
    public unsafe void TestRawBuffer()
    {
      int numElements = 100;
      int elemSize = Vector3.SizeInBytes;

      using (RawBuffer<Vector3> buffer = new RawBuffer<Vector3>(numElements))
      {
        Assert.IsTrue(numElements == buffer.Length);
        Assert.IsTrue(elemSize == buffer.ElementSizeInBytes);
        Assert.IsTrue((elemSize * numElements) == buffer.SizeInBytes);
        Assert.IsTrue(buffer.Pointer != IntPtr.Zero);
        Assert.IsTrue(buffer.IsBufferOwned);
        Assert.IsFalse(buffer.IsDisposed);

        Vector3 v1 = new Vector3(2502.5f, 222.12f, 150.25f);
        Vector3 v2 = new Vector3(50, 10, 15000);

        buffer[5] = v1;
        buffer[1] = v2;

        ref Vector3 test1 = ref buffer[5];
        ref Vector3 test2 = ref buffer[1];

        Assert.IsTrue(v1.Equals(test1));
        Assert.IsTrue(v2.Equals(test2));

        test1 = ref buffer.Span[5];
        test2 = ref buffer.Span[1];

        Assert.IsTrue(v1.Equals(test1));
        Assert.IsTrue(v2.Equals(test2));

        Span<Vector3> pinnedSpan = new Span<Vector3>(buffer.Pin(5).Pointer, 1);
        Assert.IsTrue(v1.Equals(pinnedSpan[0]));

        buffer.Resize(2);
        Assert.IsTrue(buffer.Length == 2);
        Assert.IsTrue(v2.Equals(buffer[1]));

        buffer.Resize(10);
        Assert.IsTrue(buffer.Length == 10);
        Assert.IsTrue(v2.Equals(buffer[1]));
      }
    }

    [TestCase]
    public void TestRawBuffer_NotOwned()
    {
      int numElements = 100;
      int elemSize = Vector3.SizeInBytes;
      IntPtr ptr = Marshal.AllocHGlobal(elemSize * numElements);

      try
      {
        RawBuffer<Vector3> buffer = new RawBuffer<Vector3>(ptr, 100);
        Assert.IsTrue(numElements == buffer.Length);
        Assert.IsTrue(elemSize == buffer.ElementSizeInBytes);
        Assert.IsTrue((elemSize * numElements) == buffer.SizeInBytes);
        Assert.IsTrue(buffer.Pointer != IntPtr.Zero);
        Assert.IsTrue(buffer.Pointer == ptr);
        Assert.IsFalse(buffer.IsBufferOwned);
        Assert.IsFalse(buffer.IsDisposed);

        buffer.Dispose();
        Assert.IsTrue(buffer.IsDisposed);
        Assert.IsTrue(buffer.Pointer == IntPtr.Zero);

        using(buffer = new RawBuffer<Vector3>(ptr, 100))
        {
          buffer.Resize(2);
          Assert.IsTrue(buffer.Length == 2);
          Assert.IsTrue(buffer.Pointer != ptr);

          buffer.Resize(10);
          Assert.IsTrue(buffer.Length == 10);
          Assert.IsTrue(buffer.Pointer != ptr);
        }
      } 
      finally
      {
        Marshal.FreeHGlobal(ptr);
      }
    }

    [TestCase]
    public void TestRawBuffer_NullPointer()
    {
      // Allocate zero
      RawBuffer<int> b1 = new RawBuffer<int>(0);
      Assert.IsTrue(b1.IsNullPointer);
      Assert.IsTrue(b1.Length == 0);
      Assert.IsTrue(b1.IsBufferOwned);
      b1.Dispose();

      RawBuffer<int> b1sketchy = new RawBuffer<int>(-1);
      Assert.IsTrue(b1sketchy.IsNullPointer);
      Assert.IsTrue(b1sketchy.Length == 0);
      Assert.IsTrue(b1sketchy.IsBufferOwned);
      b1sketchy.Dispose();

      // Wrap a null user buffer
      RawBuffer<int> b2 = new RawBuffer<int>(IntPtr.Zero, 0);
      Assert.IsTrue(b2.IsNullPointer);
      Assert.IsTrue(b2.Length == 0);
      Assert.IsFalse(b2.IsBufferOwned);
      b2.Dispose();

      RawBuffer<int> b2sketchy = new RawBuffer<int>(IntPtr.Zero, 2);
      Assert.IsTrue(b2sketchy.IsNullPointer);
      Assert.IsTrue(b2sketchy.Length == 0);
      Assert.IsFalse(b2sketchy.IsBufferOwned);
      b2sketchy.Dispose();

      // User buffer but sketchy length
      RawBuffer<int> b3 = new RawBuffer<int>(new IntPtr(5), -2);
      Assert.IsTrue(b3.IsNullPointer);
      Assert.IsTrue(b3.Length == 0);
      Assert.IsFalse(b3.IsBufferOwned);

      b3.Resize(5);
      Assert.IsFalse(b3.IsNullPointer);
      Assert.IsTrue(b3.Length == 5);
      Assert.IsTrue(b3.IsBufferOwned);

      b3.Resize(0);
      Assert.IsTrue(b3.IsNullPointer);
      Assert.IsTrue(b3.Length == 0);

      b3.Resize(2);
      Assert.IsFalse(b3.IsNullPointer);
      Assert.IsTrue(b3.Length == 2);
      b3.Dispose();

      // Empty memory
      RawBuffer<int> b4 = new RawBuffer<int>(new Memory<int>(Array.Empty<int>()));
      Assert.IsTrue(b4.IsNullPointer);
      Assert.IsTrue(b4.Length == 0);
      Assert.IsTrue(b4.IsBufferOwned);
      b4.Dispose();
    }

    [TestCase]
    public void TestRawBuffer_MakeCopy()
    {
      RawBuffer<Vector3> buf1 = new RawBuffer<Vector3>(5);
      buf1.Span.Fill(new Vector3(54, 5, 2));
      
      RawBuffer<Vector3> buf2 = new RawBuffer<Vector3>(buf1.Pointer, buf1.Length, true);
      Assert.IsTrue(buf2.Span[2].Equals(buf1.Span[2]));
      Assert.IsTrue(buf2.Pointer != buf1.Pointer);

      RawBuffer<Vector3> buf3 = new RawBuffer<Vector3>(new Memory<Vector3>(new[]{ new Vector3(54, 5, 2), new Vector3(54, 5, 2) }));
      Assert.IsTrue(buf3.Span[1].Equals(buf1.Span[1]));


      buf1.Dispose();
      buf2.Dispose();
      buf3.Dispose();
    }
  }
}
