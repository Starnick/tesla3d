﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Buffers.Binary;
using Tesla.Content;
using NUnit.Framework;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text.Json;

#nullable enable

namespace Tesla.Core.Buffers
{
  internal class PooledArrayTests
  {
    [TestCase]
    public void TestDisposeContents()
    {
      IReadOnlyDataBuffer dbShouldBeDisposed;
      using (PooledArray<IReadOnlyDataBuffer> arr = new PooledArray<IReadOnlyDataBuffer>(2) { DisposeContents = true })
      {
        Assert.IsTrue(arr.Length == 2);
        arr[0] = new DataBuffer<byte>(10);
        arr[1] = new DataBuffer<byte>(10);
        dbShouldBeDisposed = arr[1];
      }

      Assert.IsTrue(dbShouldBeDisposed.IsDisposed);
    }

    [TestCase]
    public void TestCtors()
    {
      using (PooledArray<int> arr = new PooledArray<int>(10))
      {
        Assert.IsTrue(arr.Length == 10);
        Assert.IsTrue(arr.IsValid);
      }

      using (PooledArray<int> arr = new PooledArray<int>(0))
      {
        Assert.IsTrue(arr.Length == 0);
        Assert.IsTrue(arr.IsValid);
      }

      PooledArray<int> invalid = default;
      Assert.IsFalse(invalid.IsValid);

      using (PooledArray<int> arr = new PooledArray<int>(5, 15))
      {
        Assert.IsTrue(arr.Length == 2);
        Assert.IsTrue(arr[0] == 5);
        Assert.IsTrue(arr[1] == 15);

        var enumerator = arr.GetEnumerator();
        Assert.IsTrue(enumerator.MoveNext());
        Assert.IsTrue(enumerator.Current == 5);
        Assert.IsTrue(enumerator.MoveNext());
        Assert.IsTrue(enumerator.Current == 15);
      }

      using (PooledArray<int> arr = new PooledArray<int>(5, 15, 20))
      {
        Assert.IsTrue(arr.Length == 3);
        Assert.IsTrue(arr[0] == 5);
        Assert.IsTrue(arr[1] == 15);
        Assert.IsTrue(arr[2] == 20);
      }

      using (PooledArray<int> arr = new PooledArray<int>(5, 15, 20, 25))
      {
        Assert.IsTrue(arr.Length == 4);
        Assert.IsTrue(arr[0] == 5);
        Assert.IsTrue(arr[1] == 15);
        Assert.IsTrue(arr[2] == 20);
        Assert.IsTrue(arr[3] == 25);
      }

      using (PooledArray<int> arr = new PooledArray<int>(5, 15, 20, 25, 30))
      {
        Assert.IsTrue(arr.Length == 5);
        Assert.IsTrue(arr[0] == 5);
        Assert.IsTrue(arr[1] == 15);
        Assert.IsTrue(arr[2] == 20);
        Assert.IsTrue(arr[3] == 25);
        Assert.IsTrue(arr[4] == 30);
      }

      using (PooledArray<int> arr = new PooledArray<int>(5, 15, 20, 25, 30, 35))
      {
        Assert.IsTrue(arr.Length == 6);
        Assert.IsTrue(arr[0] == 5);
        Assert.IsTrue(arr[1] == 15);
        Assert.IsTrue(arr[2] == 20);
        Assert.IsTrue(arr[3] == 25);
        Assert.IsTrue(arr[4] == 30);
        Assert.IsTrue(arr[5] == 35);
      }

      using (PooledArray<int> arr = new PooledArray<int>(5, 15, 20, 25, 30, 35, 40))
      {
        Assert.IsTrue(arr.Length == 7);
        Assert.IsTrue(arr[0] == 5);
        Assert.IsTrue(arr[1] == 15);
        Assert.IsTrue(arr[2] == 20);
        Assert.IsTrue(arr[3] == 25);
        Assert.IsTrue(arr[4] == 30);
        Assert.IsTrue(arr[5] == 35);
        Assert.IsTrue(arr[6] == 40);
      }

      using (PooledArray<int> arr = new PooledArray<int>(5, 15, 20, 25, 30, 35, 40, 45))
      {
        Assert.IsTrue(arr.Length == 8);
        Assert.IsTrue(arr[0] == 5);
        Assert.IsTrue(arr[1] == 15);
        Assert.IsTrue(arr[2] == 20);
        Assert.IsTrue(arr[3] == 25);
        Assert.IsTrue(arr[4] == 30);
        Assert.IsTrue(arr[5] == 35);
        Assert.IsTrue(arr[6] == 40);
        Assert.IsTrue(arr[7] == 45);
      }

      PooledArray<int> disposed = new PooledArray<int>(5);
      Assert.IsTrue(disposed.Length == 5);
      disposed.Dispose();
      Assert.IsTrue(disposed.Length == 0);
    }
  }
}
