﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Buffers;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using NUnit.Framework;

namespace Tesla.Core.Buffers
{
  public class TestClass<T>
  {
    private T[] m_data;

    public TestClass(int length)
    {
      m_data = new T[length];
    }

    public TestClass(ReadOnlySpan<T> data)
    {
      m_data = data.ToArray();
    }
  }

  [TestFixture]
  public class DataBufferTests
  {
    [TestCase]
    public void CreateFromType()
    {
      Type testType = typeof(TestClass<Vector3>);
      TestClass<Vector3> dest = Activator.CreateInstance(testType, new object?[] { 5 }) as TestClass<Vector3>;

      Type type = typeof(Vector3);
      IDataBuffer db = DataBuffer.Create(10, type);
      Assert.IsNotNull(db);
      Assert.IsTrue(db.ElementType == type);
      Assert.IsTrue(db.Length == 10);
      db.Dispose();

      Assert.IsTrue(db.IsDisposed);
    }

    [TestCase]
    public void CreateEmpty()
    {
      List<DataBuffer<int>> list = new List<DataBuffer<int>>();
      list.Add(new DataBuffer<int>(0, MemoryAllocator<int>.Default));
      list.Add(new DataBuffer<int>(0, MemoryAllocator<int>.Native));
      list.Add(new DataBuffer<int>(0, MemoryAllocator<int>.PinnedManaged));
      list.Add(new DataBuffer<int>(0, MemoryAllocator<int>.PooledManaged));

      foreach (var db in list)
      {
        Assert.IsTrue(db.Length == 0);
        db.Dispose();
      }
    }

    [TestCase]
    public void ConstructorAndPropertyTests()
    {
      List<IReadOnlyDataBuffer<int>> list = new List<IReadOnlyDataBuffer<int>>();

      int numElems = 5;
      int elemSize = sizeof(int);
      int size = elemSize * numElems;
      Type elemType = typeof(int);

      DataBuffer<int> db1 = new DataBuffer<int>(numElems);
      list.Add(db1);

      DataBuffer<int> mdb = new DataBuffer<int>(numElems, MemoryAllocator<int>.Default);
      list.Add(mdb);

      DataBuffer<int> ndb = new DataBuffer<int>(numElems, MemoryAllocator<int>.Native);
      list.Add(ndb);

      DataBuffer<int> pdb = new DataBuffer<int>(numElems, MemoryAllocator<int>.PooledManaged);
      list.Add(pdb);

      DataBuffer<int> pidb = new DataBuffer<int>(numElems, MemoryAllocator<int>.PinnedManaged);
      list.Add(pidb);
      
      foreach (var db in list)
      {
        Assert.IsTrue(db.Length == numElems);
        Assert.IsTrue(db.ElementSizeInBytes == elemSize);
        Assert.IsTrue(db.SizeInBytes == size);
        Assert.IsTrue(db.ElementType == elemType);

        // Look at properties of read-only versions of buffers
        ReadOnlySpan<int> roSpan = db.Span;
        Assert.IsTrue(roSpan.Length == numElems);

        ReadOnlySpan<byte> roBytes = db.Bytes;
        Assert.IsTrue(roBytes.Length == size);

        ReadOnlyMemory<int> roMem = db.Memory;
        Assert.IsTrue(roMem.Length == numElems);

        // Look at pinning and writing data
        MemoryHandle handle = db.Pin();
        Assert.IsTrue(handle.AsIntPointer() != IntPtr.Zero);
        Span<int> vals = stackalloc[] { 15, 5, 2, 1, 6 };
        vals.CopyTo<int>(handle.AsIntPointer(), size);

        for (int i = 0; i < vals.Length; i++)
          Assert.IsTrue(vals[i] == db[i]);

        handle.Dispose();

        // Look at properties of writable versions of buffers
        DataBuffer<int> wdb = db as DataBuffer<int>;

        Span<int> span = wdb.Span;
        Assert.IsTrue(span.Length == numElems);

        Span<byte> bytes = wdb.Bytes;
        Assert.IsTrue(bytes.Length == size);

        Memory<int> mem = wdb.Memory;
        Assert.IsTrue(mem.Length == numElems);

        // Make sure the writable ref works
        for (int i = 0; i < wdb.Length; i++)
          Assert.IsTrue(wdb[i] == vals[i]);

        // Copy bytes
        vals[0] = 1;
        Span<byte> valBytes = vals.AsBytes();
        valBytes.CopyTo(wdb.Bytes);
        Assert.IsTrue(wdb[0] == 1);

        db.Dispose();
        Assert.IsTrue(db.IsDisposed);
      }
    }

    [TestCase]
    public void CreateTests()
    {
      DataBuffer<int> lohDb = DataBuffer.Create<int>(100_000);
      MemoryMarshal.TryGetMemoryManager<int, RawBuffer<int>>(lohDb.Memory, out RawBuffer<int> rawBuf);
      Assert.IsNotNull(rawBuf);
      lohDb.Dispose();

      DataBuffer<int> db = DataBuffer.Create<int>(20_000);
      MemoryMarshal.TryGetMemoryManager<int, RawBuffer<int>>(db.Memory, out RawBuffer<int> rawBuf2);
      Assert.IsNull(rawBuf2);
      db.Dispose();

      DataBuffer<int> custom = DataBuffer.Create<int>(100_000, MemoryAllocator<int>.Default);
      MemoryMarshal.TryGetMemoryManager<int, RawBuffer<int>>(custom.Memory, out RawBuffer<int> rawBuf3);
      Assert.IsNull(rawBuf3);
      custom.Dispose();

      List<DataBuffer<int>> list = new List<DataBuffer<int>>();
      list.Add(DataBuffer.CreateManaged<int>(100));
      list.Add(DataBuffer.CreatePooled<int>(100));
      list.Add(DataBuffer.CreateNative<int>(100));
      list.Add(DataBuffer.CreatePinned<int>(100));

      foreach (var ldb in list)
      {
        Assert.IsNotNull(ldb);
        Assert.IsTrue(ldb.Length == 100);
        ldb.Dispose();
      }
    }

    [TestCase]
    public void WrapMemory()
    {
      IMemoryOwnerEx<int> mem = MemoryAllocator<int>.PooledManaged.Allocate(100);
      DataBuffer<int> db = new DataBuffer<int>(mem, false);
      Assert.IsTrue(db.Length == mem.Length);
      db.Dispose();
      Assert.IsTrue(db.IsDisposed);
      Assert.IsFalse(mem.IsDisposed);
      mem.Dispose();
    }

    [TestCase]
    public void CloneClear()
    {
      DataBuffer<int> db = new DataBuffer<int>(10, MemoryAllocator<int>.Default);
      for (int i = 0; i < db.Length; i++)
        db[i] = i + 1;

      DataBuffer<int> db2 = db.Clone();
      Assert.IsTrue(db.Length == db2.Length);

      for (int i = 0; i < db2.Length; i++)
        Assert.IsTrue(db[i] == db2[i]);

      db2.Clear();
      foreach (int db2val in db2)
        Assert.IsTrue(db2val == 0);

      db.Dispose();
      db2.Dispose();
    }

    [TestCase]
    public void TestEnumerator()
    {
      DataBuffer<int> db = new DataBuffer<int>(10, MemoryAllocator<int>.Default);
      for (int i = 0; i < db.Length; i++)
        db[i] = i + 1;

      int val = 1;
      foreach (int v in db)
        Assert.IsTrue(val++ == v);

      db.Dispose();
    }

    [TestCase]
    public void Slice()
    {
      DataBuffer<int> db = DataBuffer.Create<int>(10);
      db[4] = 15;
      Span<int> span = db.Slice(4);
      Assert.IsTrue(span.Length == 6);

      Span<int> span2 = db.Slice(4, 2);
      Assert.IsTrue(span2.Length == 2);
      Assert.IsTrue(span2[0] == db[4]);
      db.Dispose();
    }

    [TestCase]
    public void Resize()
    {
      DataBuffer<int> db = DataBuffer.Create<int>(10);
      db.Resize(20);
      Assert.IsTrue(db.Length == 20);
      db.Resize(5);
      Assert.IsTrue(db.Length == 5);
      db.Dispose();
    }

    [TestCase]
    public void CopyToArray()
    {
      DataBuffer<int> db = DataBuffer.Create<int>(10);
      int[] arr = new int[] { 5, 6, 2, 1, 10, 22, 525, 5, 25, 1 };
      db.CopyFrom(arr);
      IReadOnlyRefList<int> list = db;
      int[] arr2 = new int[10];
      list.CopyTo(arr2, 0);
      for (int i = 0; i < arr2.Length; i++)
        Assert.IsTrue(db[i] == arr2[i]);

      db.Dispose();
    }

    [TestCase]
    public void CopySlice()
    {
      DataBuffer<int> db = new DataBuffer<int>(new int[] { 5, 6, 2, 1, 10, 22, 525, 5, 25, 1 });
      DataBuffer<int> db2 = db.CopySlice(5, 2, MemoryAllocator<int>.Native);
      Assert.IsTrue(db2.Length == 2);
      Assert.IsTrue(db2[0] == db[5]);
      Assert.IsTrue(db2[1] == db[6]);

      db.Dispose();
      db2.Dispose();
    }

    [TestCase]
    public void ContainsIndexOf()
    {
      DataBuffer<int> db = new DataBuffer<int>(new int[] { 5, 6, 2, 1, 10, 22, 525, 5, 25, 1 });
      Assert.IsTrue(db.Contains(5));
      Assert.IsTrue(db.IndexOf(5, 4) != -1);
      Assert.IsTrue(db.IndexOf(10, 5) == -1);

      db.Dispose();
    }

    [TestCase]
    public void CopyFrom_Span()
    {
      Span<int> originData = new int[] { 5, 6, 2, 1, 10, 22, 525, 5, 25, 1 };
      DataBuffer<int> db = new DataBuffer<int>(6);
      Assert.IsTrue(db.CopyFrom(originData, false) == 6);
      Assert.IsTrue(db.CopyFrom(originData, true) == 10);

      db.Clear();
      db.Resize(10);

      // Overwrite the last index with the copy
      Assert.IsTrue(db.CopyFrom(0, originData, true) == 10);
      Assert.IsTrue(db.CopyFrom(9, originData, true) == 10);
      Assert.IsTrue(db.Length == 19);

      // Append the buffers
      db.Resize(10);
      Assert.IsTrue(db.CopyFrom(10, originData, true) == 10);
      Assert.IsTrue(db.Length == 20);

      // Start an index that is one beyond the current size
      db.Resize(10);
      Assert.IsTrue(db.CopyFrom(11, originData, true) == 10);
      Assert.IsTrue(db.Length == 21);
      Assert.IsTrue(db[10] == 0);

      Assert.Throws<ArgumentOutOfRangeException>(() =>
      {
        db.CopyFrom(-1, new int[] { 5, 6, 2, 1, 10, 22, 525, 5, 25, 1 }.AsSpan());
      });

      db.Dispose();
    }

    [TestCase]
    public void CopyFrom_Array()
    {
      int[] originData = new int[] { 5, 6, 2, 1, 10, 22, 525, 5, 25, 1 };
      DataBuffer<int> db = new DataBuffer<int>(6);
      Assert.IsTrue(db.CopyFrom(originData, false) == 6);
      Assert.IsTrue(db.CopyFrom(originData, true) == 10);

      db.Clear();
      db.Resize(10);

      // Overwrite the last index with the copy
      Assert.IsTrue(db.CopyFrom(0, originData, true) == 10);
      Assert.IsTrue(db.CopyFrom(9, originData, true) == 10);
      Assert.IsTrue(db.Length == 19);

      // Append the buffers
      db.Resize(10);
      Assert.IsTrue(db.CopyFrom(10, originData, true) == 10);
      Assert.IsTrue(db.Length == 20);

      // Start an index that is one beyond the current size
      db.Resize(10);
      Assert.IsTrue(db.CopyFrom(11, originData, true) == 10);
      Assert.IsTrue(db.Length == 21);
      Assert.IsTrue(db[10] == 0);

      Assert.Throws<ArgumentOutOfRangeException>(() =>
      {
        db.CopyFrom(-1, originData);
      });

      db.Dispose();
    }

    [TestCase]
    public void CopyFrom_Enumerable()
    {
      TestCopyFrom(new List<int>(new int[] { 5, 6, 2, 1, 10, 22, 525, 5, 25, 1 }));
      TestCopyFrom(new int[] { 5, 6, 2, 1, 10, 22, 525, 5, 25, 1 });
      TestCopyFrom(new DataBuffer<int>(new int[] { 5, 6, 2, 1, 10, 22, 525, 5, 25, 1 }));
      TestCopyFrom(GetEnumerable(new int[] { 5, 6, 2, 1, 10, 22, 525, 5, 25, 1 }));
    }

    private IEnumerable<int> GetEnumerable(int[] arr)
    {
      foreach (int val in arr)
        yield return val;
    }

    private void TestCopyFrom(IEnumerable<int> originData)
    {
      DataBuffer<int> db = new DataBuffer<int>(6);
      Assert.IsTrue(db.CopyFrom(originData, false) == 6);
      Assert.IsTrue(db.CopyFrom(originData, true) == 10);

      db.Clear();
      db.Resize(10);

      // Overwrite the last index with the copy
      Assert.IsTrue(db.CopyFrom(0, originData, true) == 10);
      Assert.IsTrue(db.CopyFrom(9, originData, true) == 10);
      Assert.IsTrue(db.Length == 19);

      // Append the buffers
      db.Resize(10);
      Assert.IsTrue(db.CopyFrom(10, originData, true) == 10);
      Assert.IsTrue(db.Length == 20);

      // Start an index that is one beyond the current size
      db.Resize(10);
      Assert.IsTrue(db.CopyFrom(11, originData, true) == 10);
      Assert.IsTrue(db.Length == 21);
      Assert.IsTrue(db[10] == 0);

      Assert.Throws<ArgumentOutOfRangeException>(() =>
      {
        db.CopyFrom(-1, originData);
      });

      db.Dispose();
    }
  }
}
