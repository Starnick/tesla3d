﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.IO;
using NUnit.Framework;

namespace Tesla.Core.Buffers
{
  [TestFixture]
  public sealed class DataBufferStreamTests
  {
    [TestCase]
    public void TestWrite()
    {
      DataBuffer<Vector3> ds1 = new DataBuffer<Vector3>(3);
      DataBufferStream dsStream = new DataBufferStream(ds1, true, true);

      WriteThenCheck(ds1, dsStream);

      DataBuffer<Vector3> ds2 = new DataBuffer<Vector3>(3);
      dsStream = new DataBufferStream(ds2, true, true);

      WriteThenCheck(ds2, dsStream);
    }

    private static void WriteThenCheck(DataBuffer<Vector3> ds, DataBufferStream dsStream)
    {
      using (BinaryWriter writer = new BinaryWriter(dsStream))
      {
        Vector3 a = new Vector3(2, 5, 10);
        Vector3 b = new Vector3(10, 25, 3);
        Vector3 c = new Vector3(520.25f, 55.98f, 852.42f);

        writer.Write(a.X);
        writer.Write(a.Y);
        writer.Write(a.Z);

        writer.Write(b.X);
        writer.Write(b.Y);
        writer.Write(b.Z);

        writer.Write(c.X);
        writer.Write(c.Y);
        writer.Write(c.Z);

        Assert.IsTrue(ds[0].Equals(a));
        Assert.IsTrue(ds[1].Equals(b));
        Assert.IsTrue(ds[2].Equals(c));
      }
    }

    [TestCase]
    public void TestRead()
    {
      DataBuffer<Vector3> readOnly = new DataBuffer<Vector3>(new Vector3[]
      {
                new Vector3(2, 5, 10),
                new Vector3(10, 25, 3),
                new Vector3(520.25f, 55.98f, 852.42f)
      });

      DataBufferStream dsStream = new DataBufferStream(readOnly, true, true);
    }

    private static void ReadThenCheck(DataBuffer<Vector3> ds, DataBufferStream dsStream)
    {
      using (BinaryReader reader = new BinaryReader(dsStream))
      {
        Vector3 a, b, c;

        a.X = reader.ReadSingle();
        a.Y = reader.ReadSingle();
        a.Z = reader.ReadSingle();

        b.X = reader.ReadSingle();
        b.Y = reader.ReadSingle();
        b.Z = reader.ReadSingle();

        c.X = reader.ReadSingle();
        c.Y = reader.ReadSingle();
        c.Z = reader.ReadSingle();

        Assert.IsTrue(ds[0].Equals(a));
        Assert.IsTrue(ds[1].Equals(b));
        Assert.IsTrue(ds[2].Equals(c));
      }
    }

    [TestCase]
    public void TestReadOnlyWriteOnly()
    {
      DataBuffer<Vector3> readOnly = new DataBuffer<Vector3>(new Vector3[] { Vector3.Zero, Vector3.One, new Vector3(2, 2, 2) });
      DataBuffer<Vector3> writeOnly = new DataBuffer<Vector3>(3);

      DataBufferStream readDS = new DataBufferStream(readOnly, false);

      Assert.Throws<NotSupportedException>(() =>
      {
        readDS.Write(new byte[3], 0, 3);
      });

      DataBufferStream writeDS = new DataBufferStream(writeOnly, true, true);

      Assert.DoesNotThrow(() =>
      {
        writeDS.Write(new byte[3], 0, 3);
      });
    }

    [TestCase]
    public void TestOwnership()
    {
      DataBuffer<Vector3> ds = new DataBuffer<Vector3>(3);
      DataBufferStream dsStream = new DataBufferStream(ds, true, true);

      dsStream.Dispose();
      Assert.IsTrue(ds.IsDisposed);
    }

    [TestCase]
    public void TestResize()
    {
      DataBuffer<Vector3> ds = new DataBuffer<Vector3>(3);
      using (DataBufferStream dsStream = new DataBufferStream(ds, true, true))
      {
        dsStream.SetLength(ds.ElementSizeInBytes * 10);
        Assert.IsTrue(ds.Length == 10);
      }

      Assert.Throws<NotSupportedException>(() =>
      {
        DataBuffer<Vector3> ds1 = new DataBuffer<Vector3>(3);
        using (DataBufferStream dsStream1 = new DataBufferStream(ds1, false, true))
          dsStream1.SetLength(ds1.ElementSizeInBytes * 10);
      });
    }
  }
}
