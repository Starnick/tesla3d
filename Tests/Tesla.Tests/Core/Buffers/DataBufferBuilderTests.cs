﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace Tesla.Core.Buffers
{
  [TestFixture]
  public class DataBufferBuilderTests
  {
    [TestCase]
    public void ConstructorSimpleTest()
    {
      DataBufferBuilder<float> builder = new DataBufferBuilder<float>();
      Assert.IsTrue(builder.Count == 0);

      builder.Set(5);
      builder.Set(10);
      builder.Set(11);

      Assert.IsTrue(builder.Count == 3);
      Assert.IsTrue(builder.Position == 3);
      Assert.IsTrue(builder.Capacity == 4);
      Assert.IsTrue(builder.RemainingCount == 0);
      Assert.IsFalse(builder.HasNext);
      builder.Position = 0;
      Assert.IsTrue(builder.Position == 0);
      Assert.IsTrue(builder.HasNext);
      Assert.IsTrue(builder.SizeInBytes == (sizeof(float) * 3));

      DataBuffer<float> db = builder.Claim(true);
      Assert.IsTrue(db.Length == 3);
      Assert.IsTrue(builder.Capacity == 0);
      Assert.IsTrue(db.ElementSizeInBytes == builder.ElementSizeInBytes);
      Assert.IsTrue(db.ElementType == builder.ElementType);
      db.Dispose();
    }

    [TestCase]
    public void GetSet()
    {
      DataBufferBuilder<float> builder = new DataBufferBuilder<float>();
      Assert.IsTrue(builder.Get() == 0);
      Assert.IsTrue(builder.Position == 1);
      Assert.IsTrue(builder.Count == 1);
      Assert.IsTrue(builder.RemainingCount == 0);
      Assert.IsFalse(builder.HasNext);

      builder.Position = 0;
      Assert.IsTrue(builder.Set(5) == 0);
      builder.Position = 0;
      Assert.IsTrue(builder.Get() == 5);

      Assert.IsTrue(builder.Set(10) == 1);
      Assert.IsTrue(builder.Set(15) == 2);
      Assert.IsTrue(builder.Get(1) == 10);
      builder.Set(2, 25);
      Assert.IsTrue(builder.Get(2) == 25);

      Assert.Throws<ArgumentOutOfRangeException>(() =>
      {
        builder.Set(10, 10);
      });

      Assert.Throws<ArgumentOutOfRangeException>(() =>
      {
        builder.Set(-1, 10);
      });
    }

    [TestCase]
    public void TestCapture()
    {
      DataBufferBuilder<int> builder = new DataBufferBuilder<int>();
      DataBuffer<int> buffer = new DataBuffer<int>(10);
      builder.CaptureBuffer(buffer);
      Assert.IsTrue(builder.Capacity == 10);

      builder.Set(5);
      builder.Set(10);
      builder.Set(15);
      builder.Set(20);
      builder.Set(15);
      builder.Set(25);
      builder.Set(35);
      builder.Set(36);
      builder.Set(2);
      builder.Set(1);
      builder.Set(5);

      Assert.IsTrue(builder.Capacity > 10);
      Assert.IsTrue(builder.Count == 11);

      buffer = builder.Claim(true);
      Assert.IsTrue(buffer.Length == 11);
    }

    [TestCase]
    public void CopyRange()
    {
      DataBufferBuilder<int> builder = new DataBufferBuilder<int>();
      builder.Set(10);
      builder.Set(15);
      builder.Set(20);
      builder.Set(25);
      builder.Set(30);

      Span<int> range = stackalloc int[3];
      builder.CopyRange(1, range);

      Assert.IsTrue(range[0] == 15);
      Assert.IsTrue(range[1] == 20);
      Assert.IsTrue(range[2] == 25);

      Assert.Throws<ArgumentOutOfRangeException>(() =>
      {
        builder.CopyRange(-1, new int[2]);
      });

      Assert.Throws<ArgumentOutOfRangeException>(() =>
      {
        builder.CopyRange(1, new int[10]);
      });
    }

    [TestCase]
    public void HasNextFor()
    {
      DataBufferBuilder<float> builder = new DataBufferBuilder<float>();
      builder.Set(5);
      builder.Set(10);
      builder.Set(2);
      builder.Set(120);
      builder.Set(5);
      builder.Position = 1;
      Assert.IsTrue(builder.HasNext);
      Assert.IsTrue(builder.HasNextFor(3));
      Assert.IsTrue(builder.HasNextFor(4));
      Assert.IsFalse(builder.HasNextFor(5));

      int prevCapacity = builder.Capacity;

      // overwrite 4 times, should not grow
      builder.Set(5);
      builder.Set(5);
      builder.Set(5);
      builder.Get() = 5;

      Assert.IsTrue(builder.Capacity == prevCapacity);
      Assert.IsTrue(builder.Position == 5);

      DataBuffer<float> db = builder.Claim(true);
      Assert.IsTrue(db.Length == 5);
      foreach (float v in db)
        Assert.IsTrue(v == 5);
    }

    [TestCase]
    public void TestSetCount()
    {
      DataBufferBuilder<float> builder = new DataBufferBuilder<float>(10);
      builder.Count = 5;
      Assert.IsTrue(builder.Count == 5);

      builder.Position = 3;

      builder.Count = -5;
      Assert.IsTrue(builder.Count == 0);
      Assert.IsTrue(builder.Position == 0);

      builder.Count = 5;
      builder.Position = 3;

      builder.Count = 0;
      Assert.IsTrue(builder.Count == 0);
      Assert.IsTrue(builder.Position == 0);

      builder.Count = 20;
      builder.Position = 5;
      Assert.IsTrue(builder.Count == 20);
      Assert.IsTrue(builder.Position == 5);

      DataBuffer<float> db = builder.Claim(true);
      Assert.IsTrue(db.Length == 20);
      Assert.IsTrue(builder.Count == 0);
      Assert.IsTrue(builder.Position == 0);

      db.Dispose();
      builder.Clear();
    }

    [TestCase]
    public void Span()
    {
      DataBufferBuilder<float> builder = new DataBufferBuilder<float>();
      builder.Set(5);
      builder.Set(10);
      builder.Set(2);
      builder.Set(120);
      builder.Set(5);
      builder.Position = 1;

      Span<float> span = builder.RemainingSpan;
      Assert.IsTrue(span.Length == 4);
      Assert.IsTrue(span[0] == 10);

      Span<byte> byteSpan = builder.RemainingBytes;
      Assert.IsTrue(byteSpan.Length == (4 * sizeof(float)));

      builder.Position = 0;
      span = builder.RemainingSpan;
      Assert.IsTrue(span.Length == 5);
      Assert.IsTrue(span[0] == 5);

      builder.Position = builder.Count;
      span = builder.RemainingSpan;
      Assert.IsTrue(span.IsEmpty);

      span = builder.Span;
      Assert.IsFalse(span.IsEmpty);
      Assert.IsTrue(span.Length == builder.Count);

      byteSpan = builder.Bytes;
      Assert.IsTrue(byteSpan.Length == (builder.Count * sizeof(float)));
    }

    [TestCase]
    public void Claim()
    {
      DataBufferBuilder<float> builder = new DataBufferBuilder<float>();
      builder.Set(5);
      builder.Set(10);
      builder.Set(2);
      builder.Set(120);
      builder.Set(5);

      DataBuffer<float> db = builder.Claim(false);
      Assert.IsTrue(db.Length != 5);
      Assert.IsTrue(db[0] == 5);

      Assert.IsTrue(builder.Count == 0);
      Assert.IsTrue(builder.Position == 0);

      builder.Set(5);
      builder.Set(10);
      builder.Set(2);
      builder.Set(120);
      builder.Set(5);

      Assert.IsTrue(builder.Count == 5);

      DataBuffer<float> db2 = builder.Claim(true);
      Assert.IsTrue(builder.Position == 0);
      Assert.IsTrue(db2.Length == 5);
      Assert.IsTrue(db2[0] == 5);
      Assert.IsTrue(db2[1] == 10);
      Assert.IsTrue(db2[2] == 2);
      Assert.IsTrue(db2[3] == 120);
      Assert.IsTrue(db2[4] == 5);
    }

    [TestCase]
    public void SetRange()
    {
      TestSetRange(new List<int>(new int[] { 5, 6, 2, 1, 10, 22, 525, 5, 25, 1 }));
      TestSetRange((IEnumerable<int>)new int[] { 5, 6, 2, 1, 10, 22, 525, 5, 25, 1 });
      TestSetRange(new int[] { 5, 6, 2, 1, 10, 22, 525, 5, 25, 1 });
      TestSetRange(new ReadOnlySpan<int>(new int[] { 5, 6, 2, 1, 10, 22, 525, 5, 25, 1 }));
      TestSetRange((ReadOnlySpan<int>) new DataBuffer<int>(new int[] { 5, 6, 2, 1, 10, 22, 525, 5, 25, 1 }));
      TestSetRange(GetEnumerable(new int[] { 5, 6, 2, 1, 10, 22, 525, 5, 25, 1 }));
    }

    private IEnumerable<int> GetEnumerable(int[] arr)
    {
      foreach (int val in arr)
        yield return val;
    }

    private void TestSetRange(IEnumerable<int> originData)
    {
      DataBufferBuilder<int> builder = new DataBufferBuilder<int>();
      builder.SetRange(originData);
      Assert.IsTrue(builder.Count > 0);
      Assert.IsTrue(builder.Position == builder.Count);

      int capacity = builder.Capacity;
      int count = builder.Count;

      builder.SetRange(originData);
      Assert.IsTrue(builder.Capacity > capacity);
      Assert.IsTrue(builder.Count > count);

      builder.Position = 0;

      foreach (int v in originData)
        Assert.IsTrue(builder.Get() == v);

      foreach (int v in originData)
        Assert.IsTrue(builder.Get() == v);

      builder.Clear();
    }

    private void TestSetRange(int[] originData)
    {
      DataBufferBuilder<int> builder = new DataBufferBuilder<int>();
      builder.SetRange(originData);
      Assert.IsTrue(builder.Count > 0);
      Assert.IsTrue(builder.Position == builder.Count);

      int capacity = builder.Capacity;
      int count = builder.Count;

      builder.SetRange(originData);
      Assert.IsTrue(builder.Capacity > capacity);
      Assert.IsTrue(builder.Count > count);

      builder.Position = 0;

      foreach (int v in originData)
        Assert.IsTrue(builder.Get() == v);

      foreach (int v in originData)
        Assert.IsTrue(builder.Get() == v);

      builder.Clear();
    }

    private void TestSetRange(ReadOnlySpan<int> originData)
    {
      DataBufferBuilder<int> builder = new DataBufferBuilder<int>();
      builder.SetRange(originData);
      Assert.IsTrue(builder.Count > 0);
      Assert.IsTrue(builder.Position == builder.Count);

      int capacity = builder.Capacity;
      int count = builder.Count;

      builder.SetRange(originData);
      Assert.IsTrue(builder.Capacity > capacity);
      Assert.IsTrue(builder.Count > count);

      builder.Position = 0;

      foreach (int v in originData)
        Assert.IsTrue(builder.Get() == v);

      foreach (int v in originData)
        Assert.IsTrue(builder.Get() == v);

      builder.Clear();
    }
  }
}
