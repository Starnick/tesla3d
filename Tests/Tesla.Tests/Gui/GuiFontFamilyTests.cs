﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using NUnit.Framework;
using Tesla.Content;

#nullable enable

namespace Tesla.Gui
{
  [TestFixture]
  public class GuiFontFamilyTests
  {
    [TestCase]
    public void TestFontFamily_CreationIteration()
    {
      Dictionary<FontSetKey, IReadOnlyList<int>> fonts = new Dictionary<FontSetKey, IReadOnlyList<int>>();
      fonts.Add(new FontSetKey(FontWeight.Bold), new[] { 13 });
      fonts.Add(new FontSetKey(FontWeight.SemiBold), new[] { 14, 13 });
      fonts.Add(new FontSetKey(FontWeight.Normal), new[] { 15, 18 });

      GuiFontFamily family = new GuiFontFamily("Cascadia Mono", fonts);

      Assert.IsTrue(family.IsStyleAvailable(new FontSetKey(FontWeight.Bold)));
      Assert.IsFalse(family.IsStyleAvailable(new FontSetKey(FontWeight.ExtraBold)));

      List<GuiFont> iteratedfonts = new List<GuiFont>(family);
      Assert.IsTrue(iteratedfonts.Count == 5);
      Assert.IsTrue(iteratedfonts[0].Weight == FontWeight.Bold);
      Assert.IsTrue(iteratedfonts[1].Weight == FontWeight.SemiBold);
      Assert.IsTrue(iteratedfonts[1].Size == 13);
      Assert.IsTrue(iteratedfonts[4].Weight == FontWeight.Normal);
    }
  }
}
