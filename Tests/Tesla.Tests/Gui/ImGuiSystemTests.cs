﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.IO;
using System.Text;
using NUnit.Framework;
using Tesla.Content;
using Tesla.Gui;
using Tesla.Graphics;
using ImGuiNET;

#nullable enable

namespace Tesla.Gui
{
  [TestFixture]
  public class ImGuiSystemTests
  {
    [OneTimeSetUp]
    public void ImGuiSetup()
    {
      Engine.Initialize(Platforms.CreateOnlyGraphics());
      Engine.Instance.Services.AddService<IGuiSystem>(new ImGuiSystem());

      AppGui.FontManager.AddFontPath(new FileResourceRepository("Data/Fonts/"));
    }

    [OneTimeTearDown]
    public void ImGuiDestroy()
    {
      Engine.Destroy();
    }

    [TestCase]
    public void TestLoadFont_Reload()
    {
      // try to load a font but have it fail, then set to correct name and try again
      GuiFont font = new GuiFont("Droid-Sans");
      FontLoadStatus status = AppGui.FontManager.LoadFont(font);
      Assert.IsTrue(status == FontLoadStatus.Scheduled);

      ForceRebuildFonts();

      status = AppGui.FontManager.GetFontLoadStatus(font);
      Assert.IsTrue(status == FontLoadStatus.Missing);

      font = new GuiFont("Droid Sans");

      status = AppGui.FontManager.LoadFont(font);
      Assert.IsTrue(status == FontLoadStatus.Scheduled);

      ForceRebuildFonts();

      status = AppGui.FontManager.GetFontLoadStatus(font);
      Assert.IsTrue(status == FontLoadStatus.Loaded);

      Assert.IsTrue(IsInLoadedList(font));
    }

    [TestCase]
    public void TestActiveFont()
    {
      GuiFont font = new GuiFont("Droid Sans", 15);
      AppGui.FontManager.LoadFont(font);
      ForceRebuildFonts();

      // Need a imgui context, create a dummy one
      IntPtr ctx = ImGui.CreateContext((AppGui.FontManager as ImGuiFontManager)!.DefaultFontAtlas);
      ImGui.SetCurrentContext(ctx);

      try
      {
        ImGui.NewFrame();

        AppGui.PushFont(font);
        Assert.IsTrue(AppGui.FontManager.ActiveFont == font);
        AppGui.PopFont();

        Assert.IsTrue(AppGui.FontManager.ActiveFont == AppGui.FontManager.DefaultFont);

        ImGui.EndFrame();
      }
      finally
      {
        ImGui.DestroyContext();
      }
    }

    private void ForceRebuildFonts()
    {
      (IGuiSystem.Current as ImGuiSystem)!.WhenPendingFontsLoad().Wait();
    }

    private bool IsInLoadedList(GuiFont font)
    {
      foreach (GuiFontFamily family in AppGui.FontManager.LoadedFonts)
      {
        if (family.FontFamily == font.Family)
        {
          foreach (GuiFont familyFont in family)
          {
            if (familyFont == font)
              return true;
          }
        }
      }

      return false;
    }
  }
}
