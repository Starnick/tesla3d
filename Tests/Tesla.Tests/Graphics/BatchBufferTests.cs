﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using NUnit.Framework;

#nullable enable

namespace Tesla.Graphics
{
  [TestFixture]
  public class BatchBufferTests
  {
    [TestCaseSource(typeof(RenderSystemTestSource))]
    public void TestAppend(IRenderSystem rs)
    {
      using BatchVertexBuffer<VertexPosition> buffer = new BatchVertexBuffer<VertexPosition>(rs, 10);

      Span<VertexPosition> vertices = stackalloc VertexPosition[3];
      bool hasBatch = buffer.Append(rs.ImmediateContext, vertices, out Batch<VertexBuffer> batch);
      Assert.IsFalse(hasBatch);
      Assert.IsTrue(buffer.IsBatchStarted);
      Assert.IsTrue(batch.ElementCount == 0);
      Assert.IsFalse(batch.IsValid);
      Assert.IsTrue(buffer.RemainingCapacity == 7);

      vertices = stackalloc VertexPosition[10];
      hasBatch = buffer.Append(rs.ImmediateContext, vertices, out batch);
      Assert.IsTrue(hasBatch);
      Assert.IsTrue(batch.IsValid);
      Assert.IsTrue(batch.StartIndex == 0);
      Assert.IsTrue(batch.ElementCount == 3);
      Assert.IsTrue(buffer.IsBatchStarted);

      hasBatch = buffer.Finish(rs.ImmediateContext, out batch);
      Assert.IsTrue(hasBatch);
      Assert.IsFalse(buffer.IsBatchStarted);
      Assert.IsTrue(batch.IsValid);
      Assert.IsTrue(batch.StartIndex == 0);
      Assert.IsTrue(batch.ElementCount == 10);
    }

    [TestCaseSource(typeof(RenderSystemTestSource))]
    public void TestAppendBatch(IRenderSystem rs)
    {
      using BatchVertexBuffer<VertexPosition> buffer = new BatchVertexBuffer<VertexPosition>(rs, 10);
      Span<VertexPosition> vertices = stackalloc VertexPosition[3];

      Batch<VertexBuffer> batch = buffer.AppendBatch(rs.ImmediateContext, vertices);
      Assert.IsTrue(batch.IsValid);
      Assert.IsFalse(buffer.IsBatchStarted);
      Assert.IsTrue(batch.StartIndex == 0);
      Assert.IsTrue(batch.ElementCount == 3);

      batch = buffer.AppendBatch(rs.ImmediateContext, vertices);
      Assert.IsTrue(batch.IsValid);
      Assert.IsFalse(buffer.IsBatchStarted);
      Assert.IsTrue(batch.StartIndex == 3);
      Assert.IsTrue(batch.ElementCount == 3);

      // Ensure a batch to be opened
      vertices = stackalloc VertexPosition[1];
      buffer.Append(rs.ImmediateContext, vertices, out _);

      batch = buffer.AppendBatch(rs.ImmediateContext, vertices);
      Assert.IsTrue(batch.IsValid);
      Assert.IsFalse(buffer.IsBatchStarted);
      Assert.IsTrue(batch.StartIndex == 6);
      Assert.IsTrue(batch.ElementCount == 2);
    }

    [TestCaseSource(typeof(RenderSystemTestSource))]
    public void TestIndexed(IRenderSystem rs)
    {
      using IndexedBatchVertexBuffer<VertexPosition> buffer = new IndexedBatchVertexBuffer<VertexPosition>(rs, 5, IndexFormat.ThirtyTwoBits, 5);
      Span<VertexPosition> vertices = stackalloc VertexPosition[4];

      // Use shorts to force conversion
      Span<ushort> indices = stackalloc ushort[6];

      var batch = buffer.AppendBatch(rs.ImmediateContext, vertices, indices);
      Assert.IsTrue(batch.IsValid);
      Assert.IsFalse(buffer.IsBatchStarted);
      Assert.IsTrue(batch.IndexCount == 6);
      Assert.IsTrue(batch.BaseVertexOffset == 0);
      Assert.IsTrue(batch.StartIndex == 0);

      // Index buffer should have been resized
      Assert.IsTrue(buffer.IndexCapacity > 5);
      Assert.IsTrue(buffer.VertexCapacity == 5);
      Assert.IsTrue(buffer.RemainingVertexCapacity == 1);

      batch = buffer.AppendBatch(rs.ImmediateContext, vertices, indices);
      Assert.IsTrue(batch.IsValid);
      Assert.IsFalse(buffer.IsBatchStarted);
      Assert.IsTrue(batch.IndexCount == 6);
      Assert.IsTrue(batch.BaseVertexOffset == 0);
      Assert.IsTrue(batch.StartIndex == 0);
    }
  }
}
