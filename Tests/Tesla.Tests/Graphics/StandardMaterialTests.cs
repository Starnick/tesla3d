﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using NUnit.Framework;
using Tesla.Content;

#nullable enable

namespace Tesla.Graphics
{
  [TestFixture]
  public class StandardMaterialTests
  {
    [OneTimeSetUp]
    public void Setup()
    {
      Engine.Initialize(Platforms.CreateOnlyGraphics(SubSystems.Graphics.Direct3D11));
    }

    [OneTimeTearDown]
    public void Teardown()
    {
      Engine.Destroy();
    }

    [TestCase]
    public void TestDirect3D11StandardMaterials()
    {
      ContentManager contentMan = new ContentManager();

      List<Pair<String, String>> scripts = StandardMaterialScriptLibrary.GetProvider(String.Empty).GetMaterialScripts();

      foreach (Pair<String, String> script in scripts)
      {
        Material material = StandardMaterialScriptLibrary.CreateStandardMaterial(script.First, contentMan);

        if (material is null)
          throw new InvalidOperationException(String.Format("Script failed to load: {0}", script.First));
      }
    }
  }
}
