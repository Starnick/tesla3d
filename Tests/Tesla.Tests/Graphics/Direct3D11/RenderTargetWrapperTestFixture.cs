﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using NUnit.Framework;
using Tesla.Direct3D11.Graphics;

namespace Tesla.Graphics.Direct3D11
{
  [TestFixture]
  public sealed class RenderTargetWrapperTestFixture
  {
    [TestCaseSource(typeof(D3D11OnlyRenderSystemTestSource))]
    public void DepthSharing1DTest(D3D11RenderSystem rs)
    {
      int width = 200;
      int mipCount = Texture.CalculateMipMapCount(200);

      using D3D11RenderTargetWrapper rt = new D3D11RenderTargetWrapper(rs.D3DDevice, width, 5, mipCount, SurfaceFormat.Color, new MSAADescription(2, 0, true), DepthFormat.Depth24Stencil8, false, true, RenderTargetUsage.PreserveContents);
      using D3D11RenderTargetWrapper rt2 = new D3D11RenderTargetWrapper(SurfaceFormat.Color, mipCount, rt.DepthStencilBuffer as D3D11DepthStencilBufferWrapper);

      Assert.IsTrue(object.ReferenceEquals(rt.DepthStencilBuffer, rt2.DepthStencilBuffer));
      Assert.IsTrue(rt.ArrayCount == rt2.ArrayCount);
      Assert.IsTrue(rt.Depth == rt2.Depth);
      Assert.IsTrue(rt.Height == rt2.Height);
      Assert.IsTrue(rt.Width == rt2.Width);
      Assert.IsTrue(rt.IsArrayResource == rt2.IsArrayResource);
      Assert.IsTrue(rt.IsCubeResource == rt2.IsCubeResource);
      Assert.IsTrue(rt.MipCount == rt2.MipCount);
      Assert.IsTrue(rt.MultisampleDescription == rt2.MultisampleDescription);
      Assert.IsTrue(rt.ResourceType == rt2.ResourceType);
      Assert.IsTrue(rt.SubResourceIndex == rt2.SubResourceIndex);
      Assert.IsTrue(rt.TargetUsage == rt2.TargetUsage);

    }
  }
}
