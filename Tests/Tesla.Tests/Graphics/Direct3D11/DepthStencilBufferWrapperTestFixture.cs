﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using NUnit.Framework;
using Tesla.Direct3D11.Graphics;
using D3D = SharpDX.Direct3D;
using D3D11 = SharpDX.Direct3D11;

namespace Tesla.Graphics.Direct3D11
{
  [TestFixture]
  public sealed class DepthStencilBufferWrapperTestFixture
  {
    private static DepthFormat[] s_formats = Enum.GetValues(typeof(DepthFormat)) as DepthFormat[];

    [TestCaseSource(typeof(D3D11OnlyRenderSystemTestSource))]
    public void TestOneDimensionalDepthBuffer(D3D11RenderSystem rs)
    {
      int width = 256;
      MSAADescription msaa = new MSAADescription(1, 0, true);
      RenderTargetUsage targetUsage = RenderTargetUsage.PreserveContents;
      D3D11DepthStencilBufferWrapper dsb;

      foreach (DepthFormat format in s_formats)
      {
        if (format == DepthFormat.None)
          continue;

        dsb = new D3D11DepthStencilBufferWrapper(rs.D3DDevice, width, format, 1, false, false, msaa, targetUsage);

        Assert.IsTrue(dsb.Width == width);
        Assert.IsTrue(dsb.Height == 1);
        Assert.IsTrue(dsb.ArrayCount == 1);
        Assert.IsTrue(dsb.SubResourceIndex == -1);
        Assert.IsTrue(dsb.DepthStencilFormat == format);
        Assert.IsTrue(dsb.ResourceType == ShaderResourceType.Texture1D);
        Assert.IsTrue(dsb.MultisampleDescription == msaa);
        Assert.IsTrue(dsb.IsShareable);

        Assert.IsFalse(dsb.IsArrayResource);
        Assert.IsFalse(dsb.IsCubeResource);
        Assert.IsFalse(dsb.IsReadable);
        Assert.IsFalse(dsb.IsOptimizedForSingleSurface);

        Assert.IsNotNull(dsb.D3DDepthStencilView);
        Assert.IsNotNull(dsb.D3DTexture);
        Assert.IsNull(dsb.Parent);
        Assert.IsNull(dsb.D3DShaderResourceView);

        dsb.Dispose();
      }

      foreach (DepthFormat format in s_formats)
      {
        if (format == DepthFormat.None)
          continue;

        dsb = new D3D11DepthStencilBufferWrapper(rs.D3DDevice, width, format, 1, true, false, msaa, targetUsage);

        Assert.IsTrue(dsb.Width == width);
        Assert.IsTrue(dsb.Height == 1);
        Assert.IsTrue(dsb.ArrayCount == 1);
        Assert.IsTrue(dsb.SubResourceIndex == -1);
        Assert.IsTrue(dsb.DepthStencilFormat == format);
        Assert.IsTrue(dsb.ResourceType == ShaderResourceType.Texture1D);
        Assert.IsTrue(dsb.MultisampleDescription == msaa);
        Assert.IsTrue(dsb.IsShareable);
        Assert.IsTrue(dsb.IsReadable);

        Assert.IsFalse(dsb.IsArrayResource);
        Assert.IsFalse(dsb.IsCubeResource);
        Assert.IsFalse(dsb.IsOptimizedForSingleSurface);

        Assert.IsNotNull(dsb.D3DDepthStencilView);
        Assert.IsNotNull(dsb.D3DTexture);
        Assert.IsNull(dsb.Parent);
        Assert.IsNotNull(dsb.D3DShaderResourceView);

        dsb.Dispose();
      }
    }

    [TestCaseSource(typeof(D3D11OnlyRenderSystemTestSource))]
    public void TestOneDimensionalDepthBufferArray(D3D11RenderSystem rs)
    {
      int width = 256;
      int arrayCount = 12;
      MSAADescription msaa = new MSAADescription(1, 0, true);
      RenderTargetUsage targetUsage = RenderTargetUsage.PreserveContents;
      D3D11DepthStencilBufferWrapper dsb;

      foreach (DepthFormat format in s_formats)
      {
        if (format == DepthFormat.None)
          continue;

        dsb = new D3D11DepthStencilBufferWrapper(rs.D3DDevice, width, format, arrayCount, false, false, msaa, targetUsage);

        Assert.IsTrue(dsb.Width == width);
        Assert.IsTrue(dsb.Height == 1);
        Assert.IsTrue(dsb.ArrayCount == arrayCount);
        Assert.IsTrue(dsb.SubResourceIndex == -1);
        Assert.IsTrue(dsb.DepthStencilFormat == format);
        Assert.IsTrue(dsb.ResourceType == ShaderResourceType.Texture1DArray);
        Assert.IsTrue(dsb.MultisampleDescription == msaa);
        Assert.IsTrue(dsb.IsShareable);
        Assert.IsTrue(dsb.IsArrayResource);

        Assert.IsFalse(dsb.IsCubeResource);
        Assert.IsFalse(dsb.IsReadable);
        Assert.IsFalse(dsb.IsOptimizedForSingleSurface);

        Assert.IsNotNull(dsb.D3DDepthStencilView);
        Assert.IsNotNull(dsb.D3DTexture);
        Assert.IsNull(dsb.Parent);
        Assert.IsNull(dsb.D3DShaderResourceView);

        //Get each sub-view
        for (int i = 0; i < arrayCount; i++)
        {
          D3D11DepthStencilBufferWrapper subDSB = dsb.GetSubBuffer(i);

          Assert.IsTrue(subDSB.Width == width);
          Assert.IsTrue(subDSB.Height == 1);
          Assert.IsTrue(subDSB.ArrayCount == arrayCount);
          Assert.IsTrue(subDSB.SubResourceIndex == i);
          Assert.IsTrue(subDSB.DepthStencilFormat == format);
          Assert.IsTrue(subDSB.ResourceType == ShaderResourceType.Texture1DArray);
          Assert.IsTrue(subDSB.MultisampleDescription == msaa);
          Assert.IsTrue(subDSB.IsShareable);
          Assert.IsTrue(subDSB.IsArrayResource);

          Assert.IsFalse(subDSB.IsCubeResource);
          Assert.IsFalse(subDSB.IsReadable);
          Assert.IsFalse(dsb.IsOptimizedForSingleSurface);

          Assert.IsNotNull(subDSB.D3DDepthStencilView);
          Assert.IsNotNull(subDSB.D3DTexture);
          Assert.IsNotNull(subDSB.Parent);
          Assert.IsNull(subDSB.D3DShaderResourceView);

          Assert.IsTrue(object.ReferenceEquals(dsb.D3DTexture, subDSB.D3DTexture));

          subDSB.Dispose();
        }

        dsb.Dispose();
      }

      foreach (DepthFormat format in s_formats)
      {
        if (format == DepthFormat.None)
          continue;

        dsb = new D3D11DepthStencilBufferWrapper(rs.D3DDevice, width, format, arrayCount, true, false, msaa, targetUsage);

        Assert.IsTrue(dsb.Width == width);
        Assert.IsTrue(dsb.Height == 1);
        Assert.IsTrue(dsb.ArrayCount == arrayCount);
        Assert.IsTrue(dsb.SubResourceIndex == -1);
        Assert.IsTrue(dsb.DepthStencilFormat == format);
        Assert.IsTrue(dsb.ResourceType == ShaderResourceType.Texture1DArray);
        Assert.IsTrue(dsb.MultisampleDescription == msaa);
        Assert.IsTrue(dsb.IsShareable);
        Assert.IsTrue(dsb.IsReadable);
        Assert.IsTrue(dsb.IsArrayResource);

        Assert.IsFalse(dsb.IsCubeResource);
        Assert.IsFalse(dsb.IsOptimizedForSingleSurface);

        Assert.IsNotNull(dsb.D3DDepthStencilView);
        Assert.IsNotNull(dsb.D3DTexture);
        Assert.IsNull(dsb.Parent);
        Assert.IsNotNull(dsb.D3DShaderResourceView);

        //Get each sub-view
        for (int i = 0; i < arrayCount; i++)
        {
          D3D11DepthStencilBufferWrapper subDSB = dsb.GetSubBuffer(i);

          Assert.IsTrue(subDSB.Width == width);
          Assert.IsTrue(subDSB.Height == 1);
          Assert.IsTrue(subDSB.ArrayCount == arrayCount);
          Assert.IsTrue(subDSB.SubResourceIndex == i);
          Assert.IsTrue(subDSB.DepthStencilFormat == format);
          Assert.IsTrue(subDSB.ResourceType == ShaderResourceType.Texture1DArray);
          Assert.IsTrue(subDSB.MultisampleDescription == msaa);
          Assert.IsTrue(subDSB.IsShareable);
          Assert.IsTrue(subDSB.IsArrayResource);
          Assert.IsTrue(subDSB.IsReadable);

          Assert.IsFalse(subDSB.IsCubeResource);
          Assert.IsFalse(dsb.IsOptimizedForSingleSurface);

          Assert.IsNotNull(subDSB.D3DDepthStencilView);
          Assert.IsNotNull(subDSB.D3DTexture);
          Assert.IsNotNull(subDSB.Parent);
          Assert.IsNotNull(subDSB.D3DShaderResourceView);

          Assert.IsTrue(object.ReferenceEquals(dsb.D3DTexture, subDSB.D3DTexture));

          subDSB.Dispose();
        }

        dsb.Dispose();
      }
    }

    [TestCaseSource(typeof(D3D11OnlyRenderSystemTestSource))]
    public void TestOneDimensionalDepthBufferArrayOptimized(D3D11RenderSystem rs)
    {
      int width = 256;
      int arrayCount = 12;
      MSAADescription msaa = new MSAADescription(1, 0, true);
      RenderTargetUsage targetUsage = RenderTargetUsage.PreserveContents;
      D3D11DepthStencilBufferWrapper dsb;

      foreach (DepthFormat format in s_formats)
      {
        if (format == DepthFormat.None)
          continue;

        dsb = new D3D11DepthStencilBufferWrapper(rs.D3DDevice, width, format, arrayCount, false, true, msaa, targetUsage);

        Assert.IsTrue(dsb.IsOptimizedForSingleSurface);
        Assert.IsTrue(dsb.D3DDepthStencilView.Description.Dimension == D3D11.DepthStencilViewDimension.Texture1D);
        Assert.IsTrue((dsb.D3DTexture as D3D11.Texture1D).Description.ArraySize == 1);
        Assert.IsNull(dsb.GetSubBuffer(0));

        dsb.MakeOptimizedToFull();

        Assert.IsFalse(dsb.IsOptimizedForSingleSurface);
        Assert.IsTrue(dsb.D3DDepthStencilView.Description.Dimension == D3D11.DepthStencilViewDimension.Texture1DArray);
        Assert.IsTrue((dsb.D3DTexture as D3D11.Texture1D).Description.ArraySize == arrayCount);
        Assert.IsNotNull(dsb.GetSubBuffer(0));

        dsb.Dispose();
      }

      foreach (DepthFormat format in s_formats)
      {
        if (format == DepthFormat.None)
          continue;

        dsb = new D3D11DepthStencilBufferWrapper(rs.D3DDevice, width, format, arrayCount, true, true, msaa, targetUsage);

        Assert.IsTrue(dsb.IsOptimizedForSingleSurface);
        Assert.IsTrue(dsb.D3DDepthStencilView.Description.Dimension == D3D11.DepthStencilViewDimension.Texture1D);
        Assert.IsTrue(dsb.D3DShaderResourceView.Description.Dimension == D3D.ShaderResourceViewDimension.Texture1D);
        Assert.IsTrue((dsb.D3DTexture as D3D11.Texture1D).Description.ArraySize == 1);
        Assert.IsNull(dsb.GetSubBuffer(0));

        dsb.MakeOptimizedToFull();

        Assert.IsFalse(dsb.IsOptimizedForSingleSurface);
        Assert.IsTrue(dsb.D3DDepthStencilView.Description.Dimension == D3D11.DepthStencilViewDimension.Texture1DArray);
        Assert.IsTrue(dsb.D3DShaderResourceView.Description.Dimension == D3D.ShaderResourceViewDimension.Texture1DArray);
        Assert.IsTrue((dsb.D3DTexture as D3D11.Texture1D).Description.ArraySize == arrayCount);
        Assert.IsNotNull(dsb.GetSubBuffer(0));

        dsb.Dispose();
      }
    }

    [TestCaseSource(typeof(D3D11OnlyRenderSystemTestSource))]
    public void TestTwoDimensionalDepthBuffer(D3D11RenderSystem rs)
    {
      int width = 256;
      int height = 512;
      MSAADescription msaa = new MSAADescription(1, 0, true);
      RenderTargetUsage targetUsage = RenderTargetUsage.PreserveContents;
      D3D11DepthStencilBufferWrapper dsb;

      foreach (DepthFormat format in s_formats)
      {
        if (format == DepthFormat.None)
          continue;

        dsb = new D3D11DepthStencilBufferWrapper(rs.D3DDevice, width, height, false, format, 1, false, false, msaa, targetUsage);

        Assert.IsTrue(dsb.Width == width);
        Assert.IsTrue(dsb.Height == height);
        Assert.IsTrue(dsb.ArrayCount == 1);
        Assert.IsTrue(dsb.SubResourceIndex == -1);
        Assert.IsTrue(dsb.DepthStencilFormat == format);
        Assert.IsTrue(dsb.ResourceType == ShaderResourceType.Texture2D);
        Assert.IsTrue(dsb.MultisampleDescription == msaa);
        Assert.IsTrue(dsb.IsShareable);

        Assert.IsFalse(dsb.IsArrayResource);
        Assert.IsFalse(dsb.IsCubeResource);
        Assert.IsFalse(dsb.IsReadable);
        Assert.IsFalse(dsb.IsOptimizedForSingleSurface);

        Assert.IsNotNull(dsb.D3DDepthStencilView);
        Assert.IsNotNull(dsb.D3DTexture);
        Assert.IsNull(dsb.Parent);
        Assert.IsNull(dsb.D3DShaderResourceView);

        dsb.Dispose();
      }

      foreach (DepthFormat format in s_formats)
      {
        if (format == DepthFormat.None)
          continue;

        dsb = new D3D11DepthStencilBufferWrapper(rs.D3DDevice, width, height, false, format, 1, true, false, msaa, targetUsage);

        Assert.IsTrue(dsb.Width == width);
        Assert.IsTrue(dsb.Height == height);
        Assert.IsTrue(dsb.ArrayCount == 1);
        Assert.IsTrue(dsb.SubResourceIndex == -1);
        Assert.IsTrue(dsb.DepthStencilFormat == format);
        Assert.IsTrue(dsb.ResourceType == ShaderResourceType.Texture2D);
        Assert.IsTrue(dsb.MultisampleDescription == msaa);
        Assert.IsTrue(dsb.IsShareable);
        Assert.IsTrue(dsb.IsReadable);

        Assert.IsFalse(dsb.IsArrayResource);
        Assert.IsFalse(dsb.IsCubeResource);
        Assert.IsFalse(dsb.IsOptimizedForSingleSurface);

        Assert.IsNotNull(dsb.D3DDepthStencilView);
        Assert.IsNotNull(dsb.D3DTexture);
        Assert.IsNull(dsb.Parent);
        Assert.IsNotNull(dsb.D3DShaderResourceView);

        dsb.Dispose();
      }
    }

    [TestCaseSource(typeof(D3D11OnlyRenderSystemTestSource))]
    public void TestTwoDimensionalDepthBufferArray(D3D11RenderSystem rs)
    {
      int width = 256;
      int height = 512;
      int arrayCount = 12;
      MSAADescription msaa = new MSAADescription(1, 0, true);
      RenderTargetUsage targetUsage = RenderTargetUsage.PreserveContents;
      D3D11DepthStencilBufferWrapper dsb;

      foreach (DepthFormat format in s_formats)
      {
        if (format == DepthFormat.None)
          continue;

        dsb = new D3D11DepthStencilBufferWrapper(rs.D3DDevice, width, height, false, format, arrayCount, false, false, msaa, targetUsage);

        Assert.IsTrue(dsb.Width == width);
        Assert.IsTrue(dsb.Height == height);
        Assert.IsTrue(dsb.ArrayCount == arrayCount);
        Assert.IsTrue(dsb.SubResourceIndex == -1);
        Assert.IsTrue(dsb.DepthStencilFormat == format);
        Assert.IsTrue(dsb.ResourceType == ShaderResourceType.Texture2DArray);
        Assert.IsTrue(dsb.MultisampleDescription == msaa);
        Assert.IsTrue(dsb.IsShareable);
        Assert.IsTrue(dsb.IsArrayResource);

        Assert.IsFalse(dsb.IsCubeResource);
        Assert.IsFalse(dsb.IsReadable);
        Assert.IsFalse(dsb.IsOptimizedForSingleSurface);

        Assert.IsNotNull(dsb.D3DDepthStencilView);
        Assert.IsNotNull(dsb.D3DTexture);
        Assert.IsNull(dsb.Parent);
        Assert.IsNull(dsb.D3DShaderResourceView);

        //Get each sub-view
        for (int i = 0; i < arrayCount; i++)
        {
          D3D11DepthStencilBufferWrapper subDSB = dsb.GetSubBuffer(i);

          Assert.IsTrue(subDSB.Width == width);
          Assert.IsTrue(subDSB.Height == height);
          Assert.IsTrue(subDSB.ArrayCount == arrayCount);
          Assert.IsTrue(subDSB.SubResourceIndex == i);
          Assert.IsTrue(subDSB.DepthStencilFormat == format);
          Assert.IsTrue(subDSB.ResourceType == ShaderResourceType.Texture2DArray);
          Assert.IsTrue(subDSB.MultisampleDescription == msaa);
          Assert.IsTrue(subDSB.IsShareable);
          Assert.IsTrue(subDSB.IsArrayResource);

          Assert.IsFalse(subDSB.IsCubeResource);
          Assert.IsFalse(subDSB.IsReadable);
          Assert.IsFalse(dsb.IsOptimizedForSingleSurface);

          Assert.IsNotNull(subDSB.D3DDepthStencilView);
          Assert.IsNotNull(subDSB.D3DTexture);
          Assert.IsNotNull(subDSB.Parent);
          Assert.IsNull(subDSB.D3DShaderResourceView);

          Assert.IsTrue(object.ReferenceEquals(dsb.D3DTexture, subDSB.D3DTexture));

          subDSB.Dispose();
        }

        dsb.Dispose();
      }

      foreach (DepthFormat format in s_formats)
      {
        if (format == DepthFormat.None)
          continue;

        dsb = new D3D11DepthStencilBufferWrapper(rs.D3DDevice, width, height, false, format, arrayCount, true, false, msaa, targetUsage);

        Assert.IsTrue(dsb.Width == width);
        Assert.IsTrue(dsb.Height == height);
        Assert.IsTrue(dsb.ArrayCount == arrayCount);
        Assert.IsTrue(dsb.SubResourceIndex == -1);
        Assert.IsTrue(dsb.DepthStencilFormat == format);
        Assert.IsTrue(dsb.ResourceType == ShaderResourceType.Texture2DArray);
        Assert.IsTrue(dsb.MultisampleDescription == msaa);
        Assert.IsTrue(dsb.IsShareable);
        Assert.IsTrue(dsb.IsReadable);
        Assert.IsTrue(dsb.IsArrayResource);

        Assert.IsFalse(dsb.IsCubeResource);
        Assert.IsFalse(dsb.IsOptimizedForSingleSurface);

        Assert.IsNotNull(dsb.D3DDepthStencilView);
        Assert.IsNotNull(dsb.D3DTexture);
        Assert.IsNull(dsb.Parent);
        Assert.IsNotNull(dsb.D3DShaderResourceView);

        //Get each sub-view
        for (int i = 0; i < arrayCount; i++)
        {
          D3D11DepthStencilBufferWrapper subDSB = dsb.GetSubBuffer(i);

          Assert.IsTrue(subDSB.Width == width);
          Assert.IsTrue(subDSB.Height == height);
          Assert.IsTrue(subDSB.ArrayCount == arrayCount);
          Assert.IsTrue(subDSB.SubResourceIndex == i);
          Assert.IsTrue(subDSB.DepthStencilFormat == format);
          Assert.IsTrue(subDSB.ResourceType == ShaderResourceType.Texture2DArray);
          Assert.IsTrue(subDSB.MultisampleDescription == msaa);
          Assert.IsTrue(subDSB.IsShareable);
          Assert.IsTrue(subDSB.IsArrayResource);
          Assert.IsTrue(subDSB.IsReadable);

          Assert.IsFalse(subDSB.IsCubeResource);
          Assert.IsFalse(dsb.IsOptimizedForSingleSurface);

          Assert.IsNotNull(subDSB.D3DDepthStencilView);
          Assert.IsNotNull(subDSB.D3DTexture);
          Assert.IsNotNull(subDSB.Parent);
          Assert.IsNotNull(subDSB.D3DShaderResourceView);

          Assert.IsTrue(object.ReferenceEquals(dsb.D3DTexture, subDSB.D3DTexture));

          subDSB.Dispose();
        }

        dsb.Dispose();
      }
    }

    [TestCaseSource(typeof(D3D11OnlyRenderSystemTestSource))]
    public void TestTwoDimensionalDepthBufferArrayOptimized(D3D11RenderSystem rs)
    {
      int width = 256;
      int height = 512;
      int arrayCount = 12;
      MSAADescription msaa = new MSAADescription(1, 0, true);
      RenderTargetUsage targetUsage = RenderTargetUsage.PreserveContents;
      D3D11DepthStencilBufferWrapper dsb;

      foreach (DepthFormat format in s_formats)
      {
        if (format == DepthFormat.None)
          continue;

        dsb = new D3D11DepthStencilBufferWrapper(rs.D3DDevice, width, height, false, format, arrayCount, false, true, msaa, targetUsage);

        Assert.IsTrue(dsb.IsOptimizedForSingleSurface);
        Assert.IsTrue(dsb.D3DDepthStencilView.Description.Dimension == D3D11.DepthStencilViewDimension.Texture2D);
        Assert.IsTrue((dsb.D3DTexture as D3D11.Texture2D).Description.ArraySize == 1);
        Assert.IsNull(dsb.GetSubBuffer(0));

        dsb.MakeOptimizedToFull();

        Assert.IsFalse(dsb.IsOptimizedForSingleSurface);
        Assert.IsTrue(dsb.D3DDepthStencilView.Description.Dimension == D3D11.DepthStencilViewDimension.Texture2DArray);
        Assert.IsTrue((dsb.D3DTexture as D3D11.Texture2D).Description.ArraySize == arrayCount);
        Assert.IsNotNull(dsb.GetSubBuffer(0));

        dsb.Dispose();
      }

      foreach (DepthFormat format in s_formats)
      {
        if (format == DepthFormat.None)
          continue;

        dsb = new D3D11DepthStencilBufferWrapper(rs.D3DDevice, width, height, false, format, arrayCount, true, true, msaa, targetUsage);

        Assert.IsTrue(dsb.IsOptimizedForSingleSurface);
        Assert.IsTrue(dsb.D3DDepthStencilView.Description.Dimension == D3D11.DepthStencilViewDimension.Texture2D);
        Assert.IsTrue(dsb.D3DShaderResourceView.Description.Dimension == D3D.ShaderResourceViewDimension.Texture2D);
        Assert.IsTrue((dsb.D3DTexture as D3D11.Texture2D).Description.ArraySize == 1);
        Assert.IsNull(dsb.GetSubBuffer(0));

        dsb.MakeOptimizedToFull();

        Assert.IsFalse(dsb.IsOptimizedForSingleSurface);
        Assert.IsTrue(dsb.D3DDepthStencilView.Description.Dimension == D3D11.DepthStencilViewDimension.Texture2DArray);
        Assert.IsTrue(dsb.D3DShaderResourceView.Description.Dimension == D3D.ShaderResourceViewDimension.Texture2DArray);
        Assert.IsTrue((dsb.D3DTexture as D3D11.Texture2D).Description.ArraySize == arrayCount);
        Assert.IsNotNull(dsb.GetSubBuffer(0));

        dsb.Dispose();
      }
    }

    [TestCaseSource(typeof(D3D11OnlyRenderSystemTestSource))]
    public void TestTwoDimensionalDepthBufferMS(D3D11RenderSystem rs)
    {
      int width = 256;
      int height = 512;
      MSAADescription msaa = new MSAADescription(2, 0, true);
      RenderTargetUsage targetUsage = RenderTargetUsage.PreserveContents;
      D3D11DepthStencilBufferWrapper dsb;

      foreach (DepthFormat format in s_formats)
      {
        if (format == DepthFormat.None)
          continue;

        dsb = new D3D11DepthStencilBufferWrapper(rs.D3DDevice, width, height, false, format, 1, false, false, msaa, targetUsage);

        Assert.IsTrue(dsb.Width == width);
        Assert.IsTrue(dsb.Height == height);
        Assert.IsTrue(dsb.ArrayCount == 1);
        Assert.IsTrue(dsb.SubResourceIndex == -1);
        Assert.IsTrue(dsb.DepthStencilFormat == format);
        Assert.IsTrue(dsb.ResourceType == ShaderResourceType.Texture2DMS);
        Assert.IsTrue(dsb.MultisampleDescription == msaa);
        Assert.IsTrue(dsb.IsShareable);

        Assert.IsFalse(dsb.IsArrayResource);
        Assert.IsFalse(dsb.IsCubeResource);
        Assert.IsFalse(dsb.IsReadable);
        Assert.IsFalse(dsb.IsOptimizedForSingleSurface);

        Assert.IsNotNull(dsb.D3DDepthStencilView);
        Assert.IsNotNull(dsb.D3DTexture);
        Assert.IsNull(dsb.Parent);
        Assert.IsNull(dsb.D3DShaderResourceView);

        dsb.Dispose();
      }

      foreach (DepthFormat format in s_formats)
      {
        if (format == DepthFormat.None)
          continue;

        dsb = new D3D11DepthStencilBufferWrapper(rs.D3DDevice, width, height, false, format, 1, true, false, msaa, targetUsage);

        Assert.IsTrue(dsb.Width == width);
        Assert.IsTrue(dsb.Height == height);
        Assert.IsTrue(dsb.ArrayCount == 1);
        Assert.IsTrue(dsb.SubResourceIndex == -1);
        Assert.IsTrue(dsb.DepthStencilFormat == format);
        Assert.IsTrue(dsb.ResourceType == ShaderResourceType.Texture2DMS);
        Assert.IsTrue(dsb.MultisampleDescription == msaa);
        Assert.IsTrue(dsb.IsShareable);
        Assert.IsTrue(dsb.IsReadable);

        Assert.IsFalse(dsb.IsArrayResource);
        Assert.IsFalse(dsb.IsCubeResource);
        Assert.IsFalse(dsb.IsOptimizedForSingleSurface);

        Assert.IsNotNull(dsb.D3DDepthStencilView);
        Assert.IsNotNull(dsb.D3DTexture);
        Assert.IsNull(dsb.Parent);
        Assert.IsNotNull(dsb.D3DShaderResourceView);

        dsb.Dispose();
      }
    }

    [TestCaseSource(typeof(D3D11OnlyRenderSystemTestSource))]
    public void TestTwoDimensionalDepthBufferMSArray(D3D11RenderSystem rs)
    {
      int width = 256;
      int height = 512;
      int arrayCount = 12;
      MSAADescription msaa = new MSAADescription(2, 0, true);
      RenderTargetUsage targetUsage = RenderTargetUsage.PreserveContents;
      D3D11DepthStencilBufferWrapper dsb;

      foreach (DepthFormat format in s_formats)
      {
        if (format == DepthFormat.None)
          continue;

        dsb = new D3D11DepthStencilBufferWrapper(rs.D3DDevice, width, height, false, format, arrayCount, false, false, msaa, targetUsage);

        Assert.IsTrue(dsb.Width == width);
        Assert.IsTrue(dsb.Height == height);
        Assert.IsTrue(dsb.ArrayCount == arrayCount);
        Assert.IsTrue(dsb.SubResourceIndex == -1);
        Assert.IsTrue(dsb.DepthStencilFormat == format);
        Assert.IsTrue(dsb.ResourceType == ShaderResourceType.Texture2DMSArray);
        Assert.IsTrue(dsb.MultisampleDescription == msaa);
        Assert.IsTrue(dsb.IsShareable);
        Assert.IsTrue(dsb.IsArrayResource);

        Assert.IsFalse(dsb.IsCubeResource);
        Assert.IsFalse(dsb.IsReadable);
        Assert.IsFalse(dsb.IsOptimizedForSingleSurface);

        Assert.IsNotNull(dsb.D3DDepthStencilView);
        Assert.IsNotNull(dsb.D3DTexture);
        Assert.IsNull(dsb.Parent);
        Assert.IsNull(dsb.D3DShaderResourceView);

        //Get each sub-view
        for (int i = 0; i < arrayCount; i++)
        {
          D3D11DepthStencilBufferWrapper subDSB = dsb.GetSubBuffer(i);

          Assert.IsTrue(subDSB.Width == width);
          Assert.IsTrue(subDSB.Height == height);
          Assert.IsTrue(subDSB.ArrayCount == arrayCount);
          Assert.IsTrue(subDSB.SubResourceIndex == i);
          Assert.IsTrue(subDSB.DepthStencilFormat == format);
          Assert.IsTrue(subDSB.ResourceType == ShaderResourceType.Texture2DMSArray);
          Assert.IsTrue(subDSB.MultisampleDescription == msaa);
          Assert.IsTrue(subDSB.IsShareable);
          Assert.IsTrue(subDSB.IsArrayResource);

          Assert.IsFalse(subDSB.IsCubeResource);
          Assert.IsFalse(subDSB.IsReadable);
          Assert.IsFalse(dsb.IsOptimizedForSingleSurface);

          Assert.IsNotNull(subDSB.D3DDepthStencilView);
          Assert.IsNotNull(subDSB.D3DTexture);
          Assert.IsNotNull(subDSB.Parent);
          Assert.IsNull(subDSB.D3DShaderResourceView);

          Assert.IsTrue(object.ReferenceEquals(dsb.D3DTexture, subDSB.D3DTexture));

          subDSB.Dispose();
        }

        dsb.Dispose();
      }

      foreach (DepthFormat format in s_formats)
      {
        if (format == DepthFormat.None)
          continue;

        dsb = new D3D11DepthStencilBufferWrapper(rs.D3DDevice, width, height, false, format, arrayCount, true, false, msaa, targetUsage);

        Assert.IsTrue(dsb.Width == width);
        Assert.IsTrue(dsb.Height == height);
        Assert.IsTrue(dsb.ArrayCount == arrayCount);
        Assert.IsTrue(dsb.SubResourceIndex == -1);
        Assert.IsTrue(dsb.DepthStencilFormat == format);
        Assert.IsTrue(dsb.ResourceType == ShaderResourceType.Texture2DMSArray);
        Assert.IsTrue(dsb.MultisampleDescription == msaa);
        Assert.IsTrue(dsb.IsShareable);
        Assert.IsTrue(dsb.IsReadable);
        Assert.IsTrue(dsb.IsArrayResource);

        Assert.IsFalse(dsb.IsCubeResource);
        Assert.IsFalse(dsb.IsOptimizedForSingleSurface);

        Assert.IsNotNull(dsb.D3DDepthStencilView);
        Assert.IsNotNull(dsb.D3DTexture);
        Assert.IsNull(dsb.Parent);
        Assert.IsNotNull(dsb.D3DShaderResourceView);

        //Get each sub-view
        for (int i = 0; i < arrayCount; i++)
        {
          D3D11DepthStencilBufferWrapper subDSB = dsb.GetSubBuffer(i);

          Assert.IsTrue(subDSB.Width == width);
          Assert.IsTrue(subDSB.Height == height);
          Assert.IsTrue(subDSB.ArrayCount == arrayCount);
          Assert.IsTrue(subDSB.SubResourceIndex == i);
          Assert.IsTrue(subDSB.DepthStencilFormat == format);
          Assert.IsTrue(subDSB.ResourceType == ShaderResourceType.Texture2DMSArray);
          Assert.IsTrue(subDSB.MultisampleDescription == msaa);
          Assert.IsTrue(subDSB.IsShareable);
          Assert.IsTrue(subDSB.IsArrayResource);
          Assert.IsTrue(subDSB.IsReadable);

          Assert.IsFalse(subDSB.IsCubeResource);
          Assert.IsFalse(dsb.IsOptimizedForSingleSurface);

          Assert.IsNotNull(subDSB.D3DDepthStencilView);
          Assert.IsNotNull(subDSB.D3DTexture);
          Assert.IsNotNull(subDSB.Parent);
          Assert.IsNotNull(subDSB.D3DShaderResourceView);

          Assert.IsTrue(object.ReferenceEquals(dsb.D3DTexture, subDSB.D3DTexture));

          subDSB.Dispose();
        }

        dsb.Dispose();
      }
    }

    [TestCaseSource(typeof(D3D11OnlyRenderSystemTestSource))]
    public void TestTwoDimensionalDepthBufferMSArrayOptimized(D3D11RenderSystem rs)
    {
      int width = 256;
      int height = 512;
      int arrayCount = 12;
      MSAADescription msaa = new MSAADescription(2, 0, true);
      RenderTargetUsage targetUsage = RenderTargetUsage.PreserveContents;
      D3D11DepthStencilBufferWrapper dsb;

      foreach (DepthFormat format in s_formats)
      {
        if (format == DepthFormat.None)
          continue;

        dsb = new D3D11DepthStencilBufferWrapper(rs.D3DDevice, width, height, false, format, arrayCount, false, true, msaa, targetUsage);

        Assert.IsTrue(dsb.IsOptimizedForSingleSurface);
        Assert.IsTrue(dsb.D3DDepthStencilView.Description.Dimension == D3D11.DepthStencilViewDimension.Texture2DMultisampled);
        Assert.IsTrue((dsb.D3DTexture as D3D11.Texture2D).Description.ArraySize == 1);
        Assert.IsNull(dsb.GetSubBuffer(0));

        dsb.MakeOptimizedToFull();

        Assert.IsFalse(dsb.IsOptimizedForSingleSurface);
        Assert.IsTrue(dsb.D3DDepthStencilView.Description.Dimension == D3D11.DepthStencilViewDimension.Texture2DMultisampledArray);
        Assert.IsTrue((dsb.D3DTexture as D3D11.Texture2D).Description.ArraySize == arrayCount);
        Assert.IsNotNull(dsb.GetSubBuffer(0));

        dsb.Dispose();
      }

      foreach (DepthFormat format in s_formats)
      {
        if (format == DepthFormat.None)
          continue;

        dsb = new D3D11DepthStencilBufferWrapper(rs.D3DDevice, width, height, false, format, arrayCount, true, true, msaa, targetUsage);

        Assert.IsTrue(dsb.IsOptimizedForSingleSurface);
        Assert.IsTrue(dsb.D3DDepthStencilView.Description.Dimension == D3D11.DepthStencilViewDimension.Texture2DMultisampled);
        Assert.IsTrue(dsb.D3DShaderResourceView.Description.Dimension == D3D.ShaderResourceViewDimension.Texture2DMultisampled);
        Assert.IsTrue((dsb.D3DTexture as D3D11.Texture2D).Description.ArraySize == 1);
        Assert.IsNull(dsb.GetSubBuffer(0));

        dsb.MakeOptimizedToFull();

        Assert.IsFalse(dsb.IsOptimizedForSingleSurface);
        Assert.IsTrue(dsb.D3DDepthStencilView.Description.Dimension == D3D11.DepthStencilViewDimension.Texture2DMultisampledArray);
        Assert.IsTrue(dsb.D3DShaderResourceView.Description.Dimension == D3D.ShaderResourceViewDimension.Texture2DMultisampledArray);
        Assert.IsTrue((dsb.D3DTexture as D3D11.Texture2D).Description.ArraySize == arrayCount);
        Assert.IsNotNull(dsb.GetSubBuffer(0));

        dsb.Dispose();
      }
    }

    [TestCaseSource(typeof(D3D11OnlyRenderSystemTestSource))]
    public void TestCubeDepthBuffer(D3D11RenderSystem rs)
    {
      int size = 256;
      MSAADescription msaa = new MSAADescription(1, 0, true);
      RenderTargetUsage targetUsage = RenderTargetUsage.PreserveContents;
      D3D11DepthStencilBufferWrapper dsb;

      foreach (DepthFormat format in s_formats)
      {
        if (format == DepthFormat.None)
          continue;

        dsb = new D3D11DepthStencilBufferWrapper(rs.D3DDevice, size, size, true, format, 1, false, false, msaa, targetUsage);

        Assert.IsTrue(dsb.Width == size);
        Assert.IsTrue(dsb.Height == size);
        Assert.IsTrue(dsb.ArrayCount == 1);
        Assert.IsTrue(dsb.SubResourceIndex == -1);
        Assert.IsTrue(dsb.DepthStencilFormat == format);
        Assert.IsTrue(dsb.ResourceType == ShaderResourceType.TextureCube);
        Assert.IsTrue(dsb.MultisampleDescription == msaa);
        Assert.IsTrue(dsb.IsShareable);
        Assert.IsTrue(dsb.IsCubeResource);

        Assert.IsFalse(dsb.IsArrayResource);
        Assert.IsFalse(dsb.IsReadable);
        Assert.IsFalse(dsb.IsOptimizedForSingleSurface);

        Assert.IsNotNull(dsb.D3DDepthStencilView);
        Assert.IsNotNull(dsb.D3DTexture);
        Assert.IsNull(dsb.Parent);
        Assert.IsNull(dsb.D3DShaderResourceView);

        //Get each sub-view
        for (int i = 0; i < 6; i++)
        {
          D3D11DepthStencilBufferWrapper subDSB = dsb.GetSubBuffer(i);

          Assert.IsTrue(subDSB.Width == size);
          Assert.IsTrue(subDSB.Height == size);
          Assert.IsTrue(subDSB.ArrayCount == 1);
          Assert.IsTrue(subDSB.SubResourceIndex == i);
          Assert.IsTrue(subDSB.DepthStencilFormat == format);
          Assert.IsTrue(subDSB.ResourceType == ShaderResourceType.TextureCube);
          Assert.IsTrue(subDSB.MultisampleDescription == msaa);
          Assert.IsTrue(subDSB.IsShareable);
          Assert.IsTrue(subDSB.IsCubeResource);

          Assert.IsFalse(dsb.IsArrayResource);
          Assert.IsFalse(subDSB.IsReadable);
          Assert.IsFalse(dsb.IsOptimizedForSingleSurface);

          Assert.IsNotNull(subDSB.D3DDepthStencilView);
          Assert.IsNotNull(subDSB.D3DTexture);
          Assert.IsNotNull(subDSB.Parent);
          Assert.IsNull(subDSB.D3DShaderResourceView);

          Assert.IsTrue(object.ReferenceEquals(dsb.D3DTexture, subDSB.D3DTexture));

          subDSB.Dispose();
        }

        dsb.Dispose();
      }

      foreach (DepthFormat format in s_formats)
      {
        if (format == DepthFormat.None)
          continue;

        dsb = new D3D11DepthStencilBufferWrapper(rs.D3DDevice, size, size, true, format, 1, true, false, msaa, targetUsage);

        Assert.IsTrue(dsb.Width == size);
        Assert.IsTrue(dsb.Height == size);
        Assert.IsTrue(dsb.ArrayCount == 1);
        Assert.IsTrue(dsb.SubResourceIndex == -1);
        Assert.IsTrue(dsb.DepthStencilFormat == format);
        Assert.IsTrue(dsb.ResourceType == ShaderResourceType.TextureCube);
        Assert.IsTrue(dsb.MultisampleDescription == msaa);
        Assert.IsTrue(dsb.IsShareable);
        Assert.IsTrue(dsb.IsCubeResource);
        Assert.IsTrue(dsb.IsReadable);

        Assert.IsFalse(dsb.IsArrayResource);
        Assert.IsFalse(dsb.IsOptimizedForSingleSurface);

        Assert.IsNotNull(dsb.D3DDepthStencilView);
        Assert.IsNotNull(dsb.D3DTexture);
        Assert.IsNotNull(dsb.D3DShaderResourceView);
        Assert.IsNull(dsb.Parent);

        //Get each sub-view
        for (int i = 0; i < 6; i++)
        {
          D3D11DepthStencilBufferWrapper subDSB = dsb.GetSubBuffer(i);

          Assert.IsTrue(subDSB.Width == size);
          Assert.IsTrue(subDSB.Height == size);
          Assert.IsTrue(subDSB.ArrayCount == 1);
          Assert.IsTrue(subDSB.SubResourceIndex == i);
          Assert.IsTrue(subDSB.DepthStencilFormat == format);
          Assert.IsTrue(subDSB.ResourceType == ShaderResourceType.TextureCube);
          Assert.IsTrue(subDSB.MultisampleDescription == msaa);
          Assert.IsTrue(subDSB.IsShareable);
          Assert.IsTrue(subDSB.IsCubeResource);
          Assert.IsTrue(subDSB.IsReadable);

          Assert.IsFalse(dsb.IsArrayResource);
          Assert.IsFalse(dsb.IsOptimizedForSingleSurface);

          Assert.IsNotNull(subDSB.D3DDepthStencilView);
          Assert.IsNotNull(subDSB.D3DTexture);
          Assert.IsNotNull(dsb.D3DShaderResourceView);
          Assert.IsNotNull(subDSB.Parent);

          Assert.IsTrue(object.ReferenceEquals(dsb.D3DTexture, subDSB.D3DTexture));

          subDSB.Dispose();
        }

        dsb.Dispose();
      }
    }

    [TestCaseSource(typeof(D3D11OnlyRenderSystemTestSource))]
    public void TestCubeDepthBufferOptimized(D3D11RenderSystem rs)
    {
      int size = 256;
      MSAADescription msaa = new MSAADescription(1, 0, true);
      RenderTargetUsage targetUsage = RenderTargetUsage.PreserveContents;
      D3D11DepthStencilBufferWrapper dsb;

      foreach (DepthFormat format in s_formats)
      {
        if (format == DepthFormat.None)
          continue;

        dsb = new D3D11DepthStencilBufferWrapper(rs.D3DDevice, size, size, true, format, 1, false, true, msaa, targetUsage);

        Assert.IsTrue(dsb.IsOptimizedForSingleSurface);
        Assert.IsTrue(dsb.D3DDepthStencilView.Description.Dimension == D3D11.DepthStencilViewDimension.Texture2D);
        Assert.IsTrue((dsb.D3DTexture as D3D11.Texture2D).Description.ArraySize == 1);
        Assert.IsNull(dsb.GetSubBuffer(0));

        dsb.MakeOptimizedToFull();

        Assert.IsFalse(dsb.IsOptimizedForSingleSurface);
        Assert.IsTrue(dsb.D3DDepthStencilView.Description.Dimension == D3D11.DepthStencilViewDimension.Texture2DArray);
        Assert.IsTrue((dsb.D3DTexture as D3D11.Texture2D).Description.ArraySize == 6);
        Assert.IsNotNull(dsb.GetSubBuffer(0));

        dsb.Dispose();
      }

      foreach (DepthFormat format in s_formats)
      {
        if (format == DepthFormat.None)
          continue;

        dsb = new D3D11DepthStencilBufferWrapper(rs.D3DDevice, size, size, true, format, 1, true, true, msaa, targetUsage);

        Assert.IsTrue(dsb.IsOptimizedForSingleSurface);
        Assert.IsTrue(dsb.D3DDepthStencilView.Description.Dimension == D3D11.DepthStencilViewDimension.Texture2D);
        Assert.IsTrue(dsb.D3DShaderResourceView.Description.Dimension == D3D.ShaderResourceViewDimension.Texture2D);
        Assert.IsTrue((dsb.D3DTexture as D3D11.Texture2D).Description.ArraySize == 1);
        Assert.IsNull(dsb.GetSubBuffer(0));

        dsb.MakeOptimizedToFull();

        Assert.IsFalse(dsb.IsOptimizedForSingleSurface);
        Assert.IsTrue(dsb.D3DDepthStencilView.Description.Dimension == D3D11.DepthStencilViewDimension.Texture2DArray);
        Assert.IsTrue(dsb.D3DShaderResourceView.Description.Dimension == D3D.ShaderResourceViewDimension.TextureCube);
        Assert.IsTrue((dsb.D3DTexture as D3D11.Texture2D).Description.ArraySize == 6);
        Assert.IsNotNull(dsb.GetSubBuffer(0));

        dsb.Dispose();
      }
    }

    [TestCaseSource(typeof(D3D11OnlyRenderSystemTestSource))]
    public void TestCubeDepthBufferMS(D3D11RenderSystem rs)
    {
      int size = 256;
      MSAADescription msaa = new MSAADescription(2, 0, true);
      RenderTargetUsage targetUsage = RenderTargetUsage.PreserveContents;
      D3D11DepthStencilBufferWrapper dsb;

      foreach (DepthFormat format in s_formats)
      {
        if (format == DepthFormat.None)
          continue;

        dsb = new D3D11DepthStencilBufferWrapper(rs.D3DDevice, size, size, true, format, 1, false, false, msaa, targetUsage);

        Assert.IsTrue(dsb.Width == size);
        Assert.IsTrue(dsb.Height == size);
        Assert.IsTrue(dsb.ArrayCount == 1);
        Assert.IsTrue(dsb.SubResourceIndex == -1);
        Assert.IsTrue(dsb.DepthStencilFormat == format);
        Assert.IsTrue(dsb.ResourceType == ShaderResourceType.TextureCubeMS);
        Assert.IsTrue(dsb.MultisampleDescription == msaa);
        Assert.IsTrue(dsb.IsShareable);
        Assert.IsTrue(dsb.IsCubeResource);

        Assert.IsFalse(dsb.IsArrayResource);
        Assert.IsFalse(dsb.IsReadable);
        Assert.IsFalse(dsb.IsOptimizedForSingleSurface);

        Assert.IsNotNull(dsb.D3DDepthStencilView);
        Assert.IsNotNull(dsb.D3DTexture);
        Assert.IsNull(dsb.Parent);
        Assert.IsNull(dsb.D3DShaderResourceView);

        //Get each sub-view
        for (int i = 0; i < 6; i++)
        {
          D3D11DepthStencilBufferWrapper subDSB = dsb.GetSubBuffer(i);

          Assert.IsTrue(subDSB.Width == size);
          Assert.IsTrue(subDSB.Height == size);
          Assert.IsTrue(subDSB.ArrayCount == 1);
          Assert.IsTrue(subDSB.SubResourceIndex == i);
          Assert.IsTrue(subDSB.DepthStencilFormat == format);
          Assert.IsTrue(subDSB.ResourceType == ShaderResourceType.TextureCubeMS);
          Assert.IsTrue(subDSB.MultisampleDescription == msaa);
          Assert.IsTrue(subDSB.IsShareable);
          Assert.IsTrue(subDSB.IsCubeResource);

          Assert.IsFalse(dsb.IsArrayResource);
          Assert.IsFalse(subDSB.IsReadable);
          Assert.IsFalse(dsb.IsOptimizedForSingleSurface);

          Assert.IsNotNull(subDSB.D3DDepthStencilView);
          Assert.IsNotNull(subDSB.D3DTexture);
          Assert.IsNotNull(subDSB.Parent);
          Assert.IsNull(subDSB.D3DShaderResourceView);

          Assert.IsTrue(object.ReferenceEquals(dsb.D3DTexture, subDSB.D3DTexture));

          subDSB.Dispose();
        }

        dsb.Dispose();
      }

      foreach (DepthFormat format in s_formats)
      {
        if (format == DepthFormat.None)
          continue;

        dsb = new D3D11DepthStencilBufferWrapper(rs.D3DDevice, size, size, true, format, 1, true, false, msaa, targetUsage);

        Assert.IsTrue(dsb.Width == size);
        Assert.IsTrue(dsb.Height == size);
        Assert.IsTrue(dsb.ArrayCount == 1);
        Assert.IsTrue(dsb.SubResourceIndex == -1);
        Assert.IsTrue(dsb.DepthStencilFormat == format);
        Assert.IsTrue(dsb.ResourceType == ShaderResourceType.TextureCubeMS);
        Assert.IsTrue(dsb.MultisampleDescription == msaa);
        Assert.IsTrue(dsb.IsShareable);
        Assert.IsTrue(dsb.IsCubeResource);
        Assert.IsTrue(dsb.IsReadable);

        Assert.IsFalse(dsb.IsArrayResource);
        Assert.IsFalse(dsb.IsOptimizedForSingleSurface);

        Assert.IsNotNull(dsb.D3DDepthStencilView);
        Assert.IsNotNull(dsb.D3DTexture);
        Assert.IsNotNull(dsb.D3DShaderResourceView);
        Assert.IsNull(dsb.Parent);

        //Get each sub-view
        for (int i = 0; i < 6; i++)
        {
          D3D11DepthStencilBufferWrapper subDSB = dsb.GetSubBuffer(i);

          Assert.IsTrue(subDSB.Width == size);
          Assert.IsTrue(subDSB.Height == size);
          Assert.IsTrue(subDSB.ArrayCount == 1);
          Assert.IsTrue(subDSB.SubResourceIndex == i);
          Assert.IsTrue(subDSB.DepthStencilFormat == format);
          Assert.IsTrue(subDSB.ResourceType == ShaderResourceType.TextureCubeMS);
          Assert.IsTrue(subDSB.MultisampleDescription == msaa);
          Assert.IsTrue(subDSB.IsShareable);
          Assert.IsTrue(subDSB.IsCubeResource);
          Assert.IsTrue(subDSB.IsReadable);

          Assert.IsFalse(dsb.IsArrayResource);
          Assert.IsFalse(dsb.IsOptimizedForSingleSurface);

          Assert.IsNotNull(subDSB.D3DDepthStencilView);
          Assert.IsNotNull(subDSB.D3DTexture);
          Assert.IsNotNull(dsb.D3DShaderResourceView);
          Assert.IsNotNull(subDSB.Parent);

          Assert.IsTrue(object.ReferenceEquals(dsb.D3DTexture, subDSB.D3DTexture));

          subDSB.Dispose();
        }

        dsb.Dispose();
      }
    }

    [TestCaseSource(typeof(D3D11OnlyRenderSystemTestSource))]
    public void TestCubeDepthBufferMSOptimized(D3D11RenderSystem rs)
    {
      int size = 256;
      MSAADescription msaa = new MSAADescription(2, 0, true);
      RenderTargetUsage targetUsage = RenderTargetUsage.PreserveContents;
      D3D11DepthStencilBufferWrapper dsb;

      foreach (DepthFormat format in s_formats)
      {
        if (format == DepthFormat.None)
          continue;

        dsb = new D3D11DepthStencilBufferWrapper(rs.D3DDevice, size, size, true, format, 1, false, true, msaa, targetUsage);

        Assert.IsTrue(dsb.IsOptimizedForSingleSurface);
        Assert.IsTrue(dsb.D3DDepthStencilView.Description.Dimension == D3D11.DepthStencilViewDimension.Texture2DMultisampled);
        Assert.IsTrue((dsb.D3DTexture as D3D11.Texture2D).Description.ArraySize == 1);
        Assert.IsNull(dsb.GetSubBuffer(0));

        dsb.MakeOptimizedToFull();

        Assert.IsFalse(dsb.IsOptimizedForSingleSurface);
        Assert.IsTrue(dsb.D3DDepthStencilView.Description.Dimension == D3D11.DepthStencilViewDimension.Texture2DMultisampledArray);
        Assert.IsTrue((dsb.D3DTexture as D3D11.Texture2D).Description.ArraySize == 6);
        Assert.IsNotNull(dsb.GetSubBuffer(0));

        dsb.Dispose();
      }

      foreach (DepthFormat format in s_formats)
      {
        if (format == DepthFormat.None)
          continue;

        dsb = new D3D11DepthStencilBufferWrapper(rs.D3DDevice, size, size, true, format, 1, true, true, msaa, targetUsage);

        Assert.IsTrue(dsb.IsOptimizedForSingleSurface);
        Assert.IsTrue(dsb.D3DDepthStencilView.Description.Dimension == D3D11.DepthStencilViewDimension.Texture2DMultisampled);
        Assert.IsTrue(dsb.D3DShaderResourceView.Description.Dimension == D3D.ShaderResourceViewDimension.Texture2DMultisampled);
        Assert.IsTrue((dsb.D3DTexture as D3D11.Texture2D).Description.ArraySize == 1);
        Assert.IsNull(dsb.GetSubBuffer(0));

        dsb.MakeOptimizedToFull();

        Assert.IsFalse(dsb.IsOptimizedForSingleSurface);
        Assert.IsTrue(dsb.D3DDepthStencilView.Description.Dimension == D3D11.DepthStencilViewDimension.Texture2DMultisampledArray);
        Assert.IsTrue(dsb.D3DShaderResourceView.Description.Dimension == D3D.ShaderResourceViewDimension.Texture2DMultisampledArray);
        Assert.IsTrue((dsb.D3DTexture as D3D11.Texture2D).Description.ArraySize == 6);
        Assert.IsNotNull(dsb.GetSubBuffer(0));

        dsb.Dispose();
      }
    }

    [TestCaseSource(typeof(D3D11OnlyRenderSystemTestSource))]
    public void TestCubeDepthBufferArray(D3D11RenderSystem rs)
    {
      int size = 256;
      int arrayCount = 4;
      MSAADescription msaa = new MSAADescription(1, 0, true);
      RenderTargetUsage targetUsage = RenderTargetUsage.PreserveContents;
      D3D11DepthStencilBufferWrapper dsb;

      foreach (DepthFormat format in s_formats)
      {
        if (format == DepthFormat.None)
          continue;

        dsb = new D3D11DepthStencilBufferWrapper(rs.D3DDevice, size, size, true, format, arrayCount, false, false, msaa, targetUsage);

        Assert.IsTrue(dsb.Width == size);
        Assert.IsTrue(dsb.Height == size);
        Assert.IsTrue(dsb.ArrayCount == arrayCount);
        Assert.IsTrue(dsb.SubResourceIndex == -1);
        Assert.IsTrue(dsb.DepthStencilFormat == format);
        Assert.IsTrue(dsb.ResourceType == ShaderResourceType.TextureCubeArray);
        Assert.IsTrue(dsb.MultisampleDescription == msaa);
        Assert.IsTrue(dsb.IsShareable);
        Assert.IsTrue(dsb.IsCubeResource);
        Assert.IsTrue(dsb.IsArrayResource);

        Assert.IsFalse(dsb.IsReadable);
        Assert.IsFalse(dsb.IsOptimizedForSingleSurface);

        Assert.IsNotNull(dsb.D3DDepthStencilView);
        Assert.IsNotNull(dsb.D3DTexture);
        Assert.IsNull(dsb.Parent);
        Assert.IsNull(dsb.D3DShaderResourceView);

        //Get each sub-view
        for (int i = 0; i < arrayCount; i++)
        {
          D3D11DepthStencilBufferWrapper subDSB = dsb.GetSubBuffer(i);

          Assert.IsTrue(subDSB.Width == size);
          Assert.IsTrue(subDSB.Height == size);
          Assert.IsTrue(subDSB.ArrayCount == arrayCount);
          Assert.IsTrue(subDSB.SubResourceIndex == i);
          Assert.IsTrue(subDSB.DepthStencilFormat == format);
          Assert.IsTrue(subDSB.ResourceType == ShaderResourceType.TextureCubeArray);
          Assert.IsTrue(subDSB.MultisampleDescription == msaa);
          Assert.IsTrue(subDSB.IsShareable);
          Assert.IsTrue(subDSB.IsCubeResource);
          Assert.IsTrue(dsb.IsArrayResource);

          Assert.IsFalse(subDSB.IsReadable);
          Assert.IsFalse(dsb.IsOptimizedForSingleSurface);

          Assert.IsNotNull(subDSB.D3DDepthStencilView);
          Assert.IsNotNull(subDSB.D3DTexture);
          Assert.IsNotNull(subDSB.Parent);
          Assert.IsNull(subDSB.D3DShaderResourceView);

          Assert.IsTrue(object.ReferenceEquals(dsb.D3DTexture, subDSB.D3DTexture));

          subDSB.Dispose();
        }

        dsb.Dispose();
      }

      foreach (DepthFormat format in s_formats)
      {
        if (format == DepthFormat.None)
          continue;

        dsb = new D3D11DepthStencilBufferWrapper(rs.D3DDevice, size, size, true, format, arrayCount, true, false, msaa, targetUsage);

        Assert.IsTrue(dsb.Width == size);
        Assert.IsTrue(dsb.Height == size);
        Assert.IsTrue(dsb.ArrayCount == arrayCount);
        Assert.IsTrue(dsb.SubResourceIndex == -1);
        Assert.IsTrue(dsb.DepthStencilFormat == format);
        Assert.IsTrue(dsb.ResourceType == ShaderResourceType.TextureCubeArray);
        Assert.IsTrue(dsb.MultisampleDescription == msaa);
        Assert.IsTrue(dsb.IsShareable);
        Assert.IsTrue(dsb.IsCubeResource);
        Assert.IsTrue(dsb.IsReadable);
        Assert.IsTrue(dsb.IsArrayResource);

        Assert.IsFalse(dsb.IsOptimizedForSingleSurface);

        Assert.IsNotNull(dsb.D3DDepthStencilView);
        Assert.IsNotNull(dsb.D3DTexture);
        Assert.IsNotNull(dsb.D3DShaderResourceView);
        Assert.IsNull(dsb.Parent);

        //Get each sub-view
        for (int i = 0; i < arrayCount; i++)
        {
          D3D11DepthStencilBufferWrapper subDSB = dsb.GetSubBuffer(i);

          Assert.IsTrue(subDSB.Width == size);
          Assert.IsTrue(subDSB.Height == size);
          Assert.IsTrue(subDSB.ArrayCount == arrayCount);
          Assert.IsTrue(subDSB.SubResourceIndex == i);
          Assert.IsTrue(subDSB.DepthStencilFormat == format);
          Assert.IsTrue(subDSB.ResourceType == ShaderResourceType.TextureCubeArray);
          Assert.IsTrue(subDSB.MultisampleDescription == msaa);
          Assert.IsTrue(subDSB.IsShareable);
          Assert.IsTrue(subDSB.IsCubeResource);
          Assert.IsTrue(subDSB.IsReadable);
          Assert.IsTrue(dsb.IsArrayResource);

          Assert.IsFalse(dsb.IsOptimizedForSingleSurface);

          Assert.IsNotNull(subDSB.D3DDepthStencilView);
          Assert.IsNotNull(subDSB.D3DTexture);
          Assert.IsNotNull(dsb.D3DShaderResourceView);
          Assert.IsNotNull(subDSB.Parent);

          Assert.IsTrue(object.ReferenceEquals(dsb.D3DTexture, subDSB.D3DTexture));

          subDSB.Dispose();
        }

        dsb.Dispose();
      }
    }

    [TestCaseSource(typeof(D3D11OnlyRenderSystemTestSource))]
    public void TestCubeDepthBufferMSArray(D3D11RenderSystem rs)
    {
      int size = 256;
      int arrayCount = 4;
      MSAADescription msaa = new MSAADescription(2, 0, true);
      RenderTargetUsage targetUsage = RenderTargetUsage.PreserveContents;
      D3D11DepthStencilBufferWrapper dsb;

      foreach (DepthFormat format in s_formats)
      {
        if (format == DepthFormat.None)
          continue;

        dsb = new D3D11DepthStencilBufferWrapper(rs.D3DDevice, size, size, true, format, arrayCount, false, false, msaa, targetUsage);

        Assert.IsTrue(dsb.Width == size);
        Assert.IsTrue(dsb.Height == size);
        Assert.IsTrue(dsb.ArrayCount == arrayCount);
        Assert.IsTrue(dsb.SubResourceIndex == -1);
        Assert.IsTrue(dsb.DepthStencilFormat == format);
        Assert.IsTrue(dsb.ResourceType == ShaderResourceType.TextureCubeMSArray);
        Assert.IsTrue(dsb.MultisampleDescription == msaa);
        Assert.IsTrue(dsb.IsShareable);
        Assert.IsTrue(dsb.IsCubeResource);
        Assert.IsTrue(dsb.IsArrayResource);

        Assert.IsFalse(dsb.IsReadable);
        Assert.IsFalse(dsb.IsOptimizedForSingleSurface);

        Assert.IsNotNull(dsb.D3DDepthStencilView);
        Assert.IsNotNull(dsb.D3DTexture);
        Assert.IsNull(dsb.Parent);
        Assert.IsNull(dsb.D3DShaderResourceView);

        //Get each sub-view
        for (int i = 0; i < arrayCount; i++)
        {
          D3D11DepthStencilBufferWrapper subDSB = dsb.GetSubBuffer(i);

          Assert.IsTrue(subDSB.Width == size);
          Assert.IsTrue(subDSB.Height == size);
          Assert.IsTrue(subDSB.ArrayCount == arrayCount);
          Assert.IsTrue(subDSB.SubResourceIndex == i);
          Assert.IsTrue(subDSB.DepthStencilFormat == format);
          Assert.IsTrue(subDSB.ResourceType == ShaderResourceType.TextureCubeMSArray);
          Assert.IsTrue(subDSB.MultisampleDescription == msaa);
          Assert.IsTrue(subDSB.IsShareable);
          Assert.IsTrue(subDSB.IsCubeResource);
          Assert.IsTrue(dsb.IsArrayResource);

          Assert.IsFalse(subDSB.IsReadable);
          Assert.IsFalse(dsb.IsOptimizedForSingleSurface);

          Assert.IsNotNull(subDSB.D3DDepthStencilView);
          Assert.IsNotNull(subDSB.D3DTexture);
          Assert.IsNotNull(subDSB.Parent);
          Assert.IsNull(subDSB.D3DShaderResourceView);

          Assert.IsTrue(object.ReferenceEquals(dsb.D3DTexture, subDSB.D3DTexture));

          subDSB.Dispose();
        }

        dsb.Dispose();
      }

      foreach (DepthFormat format in s_formats)
      {
        if (format == DepthFormat.None)
          continue;

        dsb = new D3D11DepthStencilBufferWrapper(rs.D3DDevice, size, size, true, format, arrayCount, true, false, msaa, targetUsage);

        Assert.IsTrue(dsb.Width == size);
        Assert.IsTrue(dsb.Height == size);
        Assert.IsTrue(dsb.ArrayCount == arrayCount);
        Assert.IsTrue(dsb.SubResourceIndex == -1);
        Assert.IsTrue(dsb.DepthStencilFormat == format);
        Assert.IsTrue(dsb.ResourceType == ShaderResourceType.TextureCubeMSArray);
        Assert.IsTrue(dsb.MultisampleDescription == msaa);
        Assert.IsTrue(dsb.IsShareable);
        Assert.IsTrue(dsb.IsCubeResource);
        Assert.IsTrue(dsb.IsReadable);
        Assert.IsTrue(dsb.IsArrayResource);

        Assert.IsFalse(dsb.IsOptimizedForSingleSurface);

        Assert.IsNotNull(dsb.D3DDepthStencilView);
        Assert.IsNotNull(dsb.D3DTexture);
        Assert.IsNotNull(dsb.D3DShaderResourceView);
        Assert.IsNull(dsb.Parent);

        //Get each sub-view
        for (int i = 0; i < arrayCount; i++)
        {
          D3D11DepthStencilBufferWrapper subDSB = dsb.GetSubBuffer(i);

          Assert.IsTrue(subDSB.Width == size);
          Assert.IsTrue(subDSB.Height == size);
          Assert.IsTrue(subDSB.ArrayCount == arrayCount);
          Assert.IsTrue(subDSB.SubResourceIndex == i);
          Assert.IsTrue(subDSB.DepthStencilFormat == format);
          Assert.IsTrue(subDSB.ResourceType == ShaderResourceType.TextureCubeMSArray);
          Assert.IsTrue(subDSB.MultisampleDescription == msaa);
          Assert.IsTrue(subDSB.IsShareable);
          Assert.IsTrue(subDSB.IsCubeResource);
          Assert.IsTrue(subDSB.IsReadable);
          Assert.IsTrue(dsb.IsArrayResource);

          Assert.IsFalse(dsb.IsOptimizedForSingleSurface);

          Assert.IsNotNull(subDSB.D3DDepthStencilView);
          Assert.IsNotNull(subDSB.D3DTexture);
          Assert.IsNotNull(dsb.D3DShaderResourceView);
          Assert.IsNotNull(subDSB.Parent);

          Assert.IsTrue(object.ReferenceEquals(dsb.D3DTexture, subDSB.D3DTexture));

          subDSB.Dispose();
        }

        dsb.Dispose();
      }
    }

    [TestCaseSource(typeof(D3D11OnlyRenderSystemTestSource))]
    public void TestThreeDimensionalDepthBuffer(D3D11RenderSystem rs)
    {
      int width = 256;
      int height = 512;
      int depth = 16;
      MSAADescription msaa = new MSAADescription(1, 0, true);
      RenderTargetUsage targetUsage = RenderTargetUsage.PreserveContents;
      D3D11DepthStencilBufferWrapper dsb;

      foreach (DepthFormat format in s_formats)
      {
        if (format == DepthFormat.None)
          continue;

        dsb = new D3D11DepthStencilBufferWrapper(rs.D3DDevice, width, height, depth, format, false, false, msaa, targetUsage);

        Assert.IsTrue(dsb.Width == width);
        Assert.IsTrue(dsb.Height == height);
        Assert.IsTrue(dsb.ArrayCount == depth);
        Assert.IsTrue(dsb.SubResourceIndex == -1);
        Assert.IsTrue(dsb.DepthStencilFormat == format);
        Assert.IsTrue(dsb.ResourceType == ShaderResourceType.Texture3D);
        Assert.IsTrue(dsb.MultisampleDescription == msaa);
        Assert.IsTrue(dsb.IsShareable);
        Assert.IsTrue(dsb.IsArrayResource);

        Assert.IsFalse(dsb.IsCubeResource);
        Assert.IsFalse(dsb.IsReadable);
        Assert.IsFalse(dsb.IsOptimizedForSingleSurface);

        Assert.IsNotNull(dsb.D3DDepthStencilView);
        Assert.IsNotNull(dsb.D3DTexture);
        Assert.IsNull(dsb.Parent);
        Assert.IsNull(dsb.D3DShaderResourceView);

        //Get each sub-view
        for (int i = 0; i < depth; i++)
        {
          D3D11DepthStencilBufferWrapper subDSB = dsb.GetSubBuffer(i);

          Assert.IsTrue(subDSB.Width == width);
          Assert.IsTrue(subDSB.Height == height);
          Assert.IsTrue(subDSB.ArrayCount == depth);
          Assert.IsTrue(subDSB.SubResourceIndex == i);
          Assert.IsTrue(subDSB.DepthStencilFormat == format);
          Assert.IsTrue(subDSB.ResourceType == ShaderResourceType.Texture3D);
          Assert.IsTrue(subDSB.MultisampleDescription == msaa);
          Assert.IsTrue(subDSB.IsShareable);
          Assert.IsTrue(subDSB.IsArrayResource);

          Assert.IsFalse(subDSB.IsCubeResource);
          Assert.IsFalse(subDSB.IsReadable);
          Assert.IsFalse(dsb.IsOptimizedForSingleSurface);

          Assert.IsNotNull(subDSB.D3DDepthStencilView);
          Assert.IsNotNull(subDSB.D3DTexture);
          Assert.IsNotNull(subDSB.Parent);
          Assert.IsNull(subDSB.D3DShaderResourceView);

          Assert.IsTrue(object.ReferenceEquals(dsb.D3DTexture, subDSB.D3DTexture));

          subDSB.Dispose();
        }

        dsb.Dispose();
      }

      foreach (DepthFormat format in s_formats)
      {
        if (format == DepthFormat.None)
          continue;

        dsb = new D3D11DepthStencilBufferWrapper(rs.D3DDevice, width, height, depth, format, true, false, msaa, targetUsage);

        Assert.IsTrue(dsb.Width == width);
        Assert.IsTrue(dsb.Height == height);
        Assert.IsTrue(dsb.ArrayCount == depth);
        Assert.IsTrue(dsb.SubResourceIndex == -1);
        Assert.IsTrue(dsb.DepthStencilFormat == format);
        Assert.IsTrue(dsb.ResourceType == ShaderResourceType.Texture3D);
        Assert.IsTrue(dsb.MultisampleDescription == msaa);
        Assert.IsTrue(dsb.IsShareable);
        Assert.IsTrue(dsb.IsReadable);
        Assert.IsTrue(dsb.IsArrayResource);

        Assert.IsFalse(dsb.IsCubeResource);
        Assert.IsFalse(dsb.IsOptimizedForSingleSurface);

        Assert.IsNotNull(dsb.D3DDepthStencilView);
        Assert.IsNotNull(dsb.D3DTexture);
        Assert.IsNull(dsb.Parent);
        Assert.IsNotNull(dsb.D3DShaderResourceView);

        //Get each sub-view
        for (int i = 0; i < depth; i++)
        {
          D3D11DepthStencilBufferWrapper subDSB = dsb.GetSubBuffer(i);

          Assert.IsTrue(subDSB.Width == width);
          Assert.IsTrue(subDSB.Height == height);
          Assert.IsTrue(subDSB.ArrayCount == depth);
          Assert.IsTrue(subDSB.SubResourceIndex == i);
          Assert.IsTrue(subDSB.DepthStencilFormat == format);
          Assert.IsTrue(subDSB.ResourceType == ShaderResourceType.Texture3D);
          Assert.IsTrue(subDSB.MultisampleDescription == msaa);
          Assert.IsTrue(subDSB.IsShareable);
          Assert.IsTrue(subDSB.IsArrayResource);
          Assert.IsTrue(subDSB.IsReadable);

          Assert.IsFalse(subDSB.IsCubeResource);
          Assert.IsFalse(dsb.IsOptimizedForSingleSurface);

          Assert.IsNotNull(subDSB.D3DDepthStencilView);
          Assert.IsNotNull(subDSB.D3DTexture);
          Assert.IsNotNull(subDSB.Parent);
          Assert.IsNotNull(subDSB.D3DShaderResourceView);

          Assert.IsTrue(object.ReferenceEquals(dsb.D3DTexture, subDSB.D3DTexture));

          subDSB.Dispose();
        }

        dsb.Dispose();
      }
    }

    [TestCaseSource(typeof(D3D11OnlyRenderSystemTestSource))]
    public void TestThreeDimensionalDepthBufferOptimized(D3D11RenderSystem rs)
    {
      int width = 256;
      int height = 512;
      int depth = 16;
      MSAADescription msaa = new MSAADescription(1, 0, true);
      RenderTargetUsage targetUsage = RenderTargetUsage.PreserveContents;
      D3D11DepthStencilBufferWrapper dsb;

      foreach (DepthFormat format in s_formats)
      {
        if (format == DepthFormat.None)
          continue;

        dsb = new D3D11DepthStencilBufferWrapper(rs.D3DDevice, width, height, depth, format, false, true, msaa, targetUsage);

        Assert.IsTrue(dsb.IsOptimizedForSingleSurface);
        Assert.IsTrue(dsb.D3DDepthStencilView.Description.Dimension == D3D11.DepthStencilViewDimension.Texture2D);
        Assert.IsTrue((dsb.D3DTexture as D3D11.Texture2D).Description.ArraySize == 1);
        Assert.IsNull(dsb.GetSubBuffer(0));

        dsb.MakeOptimizedToFull();

        Assert.IsFalse(dsb.IsOptimizedForSingleSurface);
        Assert.IsTrue(dsb.D3DDepthStencilView.Description.Dimension == D3D11.DepthStencilViewDimension.Texture2DArray);
        Assert.IsTrue((dsb.D3DTexture as D3D11.Texture2D).Description.ArraySize == depth);
        Assert.IsNotNull(dsb.GetSubBuffer(0));

        dsb.Dispose();
      }

      foreach (DepthFormat format in s_formats)
      {
        if (format == DepthFormat.None)
          continue;

        dsb = new D3D11DepthStencilBufferWrapper(rs.D3DDevice, width, height, depth, format, true, true, msaa, targetUsage);

        Assert.IsTrue(dsb.IsOptimizedForSingleSurface);
        Assert.IsTrue(dsb.D3DDepthStencilView.Description.Dimension == D3D11.DepthStencilViewDimension.Texture2D);
        Assert.IsTrue(dsb.D3DShaderResourceView.Description.Dimension == D3D.ShaderResourceViewDimension.Texture2D);
        Assert.IsTrue((dsb.D3DTexture as D3D11.Texture2D).Description.ArraySize == 1);
        Assert.IsNull(dsb.GetSubBuffer(0));

        dsb.MakeOptimizedToFull();

        Assert.IsFalse(dsb.IsOptimizedForSingleSurface);
        Assert.IsTrue(dsb.D3DDepthStencilView.Description.Dimension == D3D11.DepthStencilViewDimension.Texture2DArray);
        Assert.IsTrue(dsb.D3DShaderResourceView.Description.Dimension == D3D.ShaderResourceViewDimension.Texture2DArray);
        Assert.IsTrue((dsb.D3DTexture as D3D11.Texture2D).Description.ArraySize == depth);
        Assert.IsNotNull(dsb.GetSubBuffer(0));

        dsb.Dispose();
      }
    }
  }
}
