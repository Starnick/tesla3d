﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using NUnit.Framework;

#nullable enable

namespace Tesla.Graphics
{
  [TestFixture]
  public class TextureCubeTests
  {
    [TestCaseSource(typeof(RenderSystemTestSource))]
    public void ConstructorTests(IRenderSystem rs)
    {
      TextureCube tex = new TextureCube(rs, 10);
      Assert.IsNotNull(tex);
      Assert.IsTrue(tex.ResourceType == ShaderResourceType.TextureCube);
      Assert.IsTrue(tex.Dimension == TextureDimension.Cube);
      Assert.IsTrue(tex.Size == 10);
      tex.Dispose();
      Assert.IsTrue(tex.IsDisposed);

      // Dynamic cube textures not allowed
      Assert.Throws<TeslaGraphicsException>(() =>
      {
        new TextureCube(rs, 10, ResourceUsage.Dynamic);
      }, "Should have thrown dynamic texture cube");

      using DataBuffer<Color> immutableData = new DataBuffer<Color>(10 * 10);
      DataBuffer<Color>[] arrayData = new[] { immutableData, immutableData, immutableData, immutableData, immutableData, immutableData };
      tex = new TextureCube(rs, 10, TextureOptions.Init(arrayData, SurfaceFormat.Color, ResourceUsage.Immutable));
      Assert.IsNotNull(tex);
      Assert.IsTrue(tex.ResourceUsage == ResourceUsage.Immutable);
      tex.Dispose();
    }

    [TestCaseSource(typeof(RenderSystemTestSource))]
    public void ConstructorArrayTests(IRenderSystem rs)
    {
      TextureCubeArray tex = new TextureCubeArray(rs, 10, 3);
      Assert.IsNotNull(tex);
      Assert.IsTrue(tex.ResourceType == ShaderResourceType.TextureCubeArray);
      Assert.IsTrue(tex.Dimension == TextureDimension.Cube);
      Assert.IsTrue(tex.Size == 10);
      Assert.IsTrue(tex.ArrayCount == 3);
      tex.Dispose();
      Assert.IsTrue(tex.IsDisposed);

      // Dynamic cube textures not allowed
      Assert.Throws<TeslaGraphicsException>(() =>
      {
        new TextureCubeArray(rs, 10, 3, ResourceUsage.Dynamic);
      }, "Should have thrown dynamic texture cube");

      using DataBuffer<Color> immutableData = new DataBuffer<Color>(10 * 10);
      DataBuffer<Color>[] arrayData = new DataBuffer<Color>[6 * 3]; // 6 faces for 3 cubemaps
      for (int i = 0; i < arrayData.Length; i++)
        arrayData[i] = immutableData;

      tex = new TextureCubeArray(rs, 10, 3, TextureOptions.Init(arrayData, SurfaceFormat.Color, ResourceUsage.Immutable));
      Assert.IsNotNull(tex);
      Assert.IsTrue(tex.ResourceUsage == ResourceUsage.Immutable);
      tex.Dispose();
    }
  }
}
