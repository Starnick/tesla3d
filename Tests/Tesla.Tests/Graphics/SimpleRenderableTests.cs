﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using NUnit.Framework;

#nullable enable

namespace Tesla.Graphics
{
  [TestFixture]
  public class SimpleRenderableTests
  {
    [TestCase]
    public void TestSimpleRenderable()
    {
      DelegateRenderable renderable = new DelegateRenderable((IRenderContext context, RenderBucketID bucketId, MaterialPass pass) =>
      {
        Assert.IsNull(pass);
        Console.WriteLine("Inside SetupDrawCall");
      });

      renderable.MaterialDefinition.Add(RenderBucketID.PostOpaque, null);

      Assert.IsTrue(renderable.MaterialDefinition.Count == 1);

      RenderBucket bucket = new RenderBucket(RenderBucketID.PostOpaque, new OpaqueRenderBucketComparer(), 32);
      RenderQueue queue = new RenderQueue();
      queue.AddBucket(bucket);

      Assert.IsTrue(queue.Enqueue(renderable));

      //TODO: Null render context should be ok since we don't check for it here (normally render buckets aren't called directly). Should think about getting a dummy render system together.
      queue.RenderBuckets(null);
    }
  }
}
