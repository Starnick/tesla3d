﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using NUnit.Framework;

#nullable enable

namespace Tesla.Graphics
{
  [TestFixture]
  public class Texture1DTests
  {
    [TestCaseSource(typeof(RenderSystemTestSource))]
    public void ConstructorTests(IRenderSystem rs)
    {
      Texture1D tex = new Texture1D(rs, 10);
      Assert.IsNotNull(tex);
      Assert.IsTrue(tex.ResourceType == ShaderResourceType.Texture1D);
      Assert.IsTrue(tex.Dimension == TextureDimension.One);
      Assert.IsTrue(tex.Width == 10);
      tex.Dispose();
      Assert.IsTrue(tex.IsDisposed);

      tex = new Texture1D(rs, 10, ResourceUsage.Dynamic);
      Assert.IsNotNull(tex);
      Assert.IsTrue(tex.ResourceUsage == ResourceUsage.Dynamic);
      tex.Dispose();

      using DataBuffer<Color> immutableData = new DataBuffer<Color>(10);
      tex = new Texture1D(rs, 10, TextureOptions.Init(immutableData, SurfaceFormat.Color, ResourceUsage.Immutable));
      Assert.IsNotNull(tex);
      Assert.IsTrue(tex.ResourceUsage == ResourceUsage.Immutable);
      tex.Dispose();
    }

    [TestCaseSource(typeof(RenderSystemTestSource))]
    public void ConstructorArrayTests(IRenderSystem rs)
    {
      Texture1DArray tex = new Texture1DArray(rs, 10, 3);
      Assert.IsNotNull(tex);
      Assert.IsTrue(tex.ResourceType == ShaderResourceType.Texture1DArray);
      Assert.IsTrue(tex.ArrayCount == 3);
      tex.Dispose();
      Assert.IsTrue(tex.IsDisposed);

      // Only single subresource allowed
      tex = new Texture1DArray(rs, 10, 1, ResourceUsage.Dynamic);
      Assert.IsNotNull(tex);
      Assert.IsTrue(tex.ResourceUsage == ResourceUsage.Dynamic);
      tex.Dispose();

      Assert.Throws<TeslaGraphicsException>(() =>
      {
        new Texture1DArray(rs, 10, 2, ResourceUsage.Dynamic);
      }, "Should have thrown dynamic texture array");

      using DataBuffer<Color> immutableData = new DataBuffer<Color>(10);
      DataBuffer<Color>[] arrayData = new[] { immutableData, immutableData, immutableData };
      tex = new Texture1DArray(rs, 10, 3, TextureOptions.Init(arrayData, SurfaceFormat.Color, ResourceUsage.Immutable));
      Assert.IsNotNull(tex);
      Assert.IsTrue(tex.ResourceUsage == ResourceUsage.Immutable);
      tex.Dispose();
    }
  }
}
