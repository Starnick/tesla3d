﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using NUnit.Framework;

#nullable enable

namespace Tesla.Graphics
{
  [TestFixture]
  public sealed class EffectCollectionsTests
  {
    #region Dummy Mocks

    public class DummyEffectParameter : IEffectParameter
    {
      private String m_name;

      public DummyEffectParameter(String name)
      {
        m_name = name;
      }

      public String Name
      {
        get
        {
          return m_name;
        }
      }

      public IEffectConstantBuffer ContainingBuffer
      {
        get { throw new NotImplementedException(); }
      }

      public EffectParameterCollection Elements
      {
        get { throw new NotImplementedException(); }
      }

      public EffectParameterCollection StructureMembers
      {
        get { throw new NotImplementedException(); }
      }

      public EffectParameterClass ParameterClass
      {
        get { throw new NotImplementedException(); }
      }

      public EffectParameterType ParameterType
      {
        get { throw new NotImplementedException(); }
      }

      public Type DefaultNetType { get { return null; } }

      public int ColumnCount
      {
        get { throw new NotImplementedException(); }
      }

      public int RowCount
      {
        get { throw new NotImplementedException(); }
      }

      public int SizeInBytes
      {
        get { throw new NotImplementedException(); }
      }

      public int StartOffsetInBytes
      {
        get { throw new NotImplementedException(); }
      }

      public int ElementIndex
      {
        get { throw new NotImplementedException(); }
      }

      public int StructureMemberIndex
      {
        get { throw new NotImplementedException(); }
      }

      public bool IsArray
      {
        get { throw new NotImplementedException(); }
      }

      public bool IsElementInArray
      {
        get { throw new NotImplementedException(); }
      }

      public bool IsStructureMember
      {
        get { throw new NotImplementedException(); }
      }

      public bool IsValueType
      {
        get { throw new NotImplementedException(); }
      }

      public T GetValue<T>() where T : unmanaged
      {
        throw new NotImplementedException();
      }

      public T[] GetValueArray<T>(int count) where T : unmanaged
      {
        throw new NotImplementedException();
      }

      public void SetValue<T>(in T value) where T : unmanaged
      {
        throw new NotImplementedException();
      }

      public void SetValue<T>(params T[] values) where T : unmanaged
      {
        throw new NotImplementedException();
      }

      public Matrix GetMatrixValue()
      {
        throw new NotImplementedException();
      }

      public Matrix GetMatrixValueTranspose()
      {
        throw new NotImplementedException();
      }

      public Matrix[] GetMatrixValueArray(int count)
      {
        throw new NotImplementedException();
      }

      public Matrix[] GetMatrixValueArrayTranspose(int count)
      {
        throw new NotImplementedException();
      }

      public void SetValue(in Matrix value)
      {
        throw new NotImplementedException();
      }

      public void SetValue(params Matrix[] values)
      {
        throw new NotImplementedException();
      }

      public void SetValueTranspose(in Matrix value)
      {
        throw new NotImplementedException();
      }

      public void SetValueTranspose(params Matrix[] values)
      {
        throw new NotImplementedException();
      }

      public T GetResource<T>() where T : IShaderResource
      {
        throw new NotImplementedException();
      }

      public T[] GetResourceArray<T>(int count) where T : IShaderResource
      {
        throw new NotImplementedException();
      }

      public void SetResource<T>(T resource) where T : IShaderResource
      {
        throw new NotImplementedException();
      }

      public void SetResource<T>(params T[] resources) where T : IShaderResource
      {
        throw new NotImplementedException();
      }

      public bool IsPartOf(Effect effect)
      {
        throw new NotImplementedException();
      }
    }

    public class DummyEffectConstantBuffer : IEffectConstantBuffer
    {
      private String m_name;
      private EffectParameterCollection m_parameters;

      public DummyEffectConstantBuffer(String name, EffectParameterCollection parameters)
      {
        m_name = name;
        m_parameters = parameters;
      }

      public EffectParameterCollection Parameters
      {
        get { return m_parameters; }
      }

      public void Set<T>(int offsetInBytes, in T value) where T : unmanaged
      {
        throw new NotImplementedException();
      }

      public void Set<T>(in T value) where T : unmanaged
      {
        throw new NotImplementedException();
      }

      public void Set<T>(params T[] values) where T : unmanaged
      {
        throw new NotImplementedException();
      }

      public void Set<T>(int offsetInBytes, params T[] values) where T : unmanaged
      {
        throw new NotImplementedException();
      }

      public int SizeInBytes
      {
        get { throw new NotImplementedException(); }
      }

      public bool IsPartOf(Effect effect)
      {
        throw new NotImplementedException();
      }

      public string Name
      {
        get { return m_name; }
      }


      public T Get<T>() where T : unmanaged
      {
        throw new NotImplementedException();
      }

      public T Get<T>(int offsetInBytes) where T : unmanaged
      {
        throw new NotImplementedException();
      }

      public T[] GetRange<T>(int count) where T : unmanaged
      {
        throw new NotImplementedException();
      }

      public T[] GetRange<T>(int offsetInBytes, int count) where T : unmanaged
      {
        throw new NotImplementedException();
      }
    }

    #endregion

    [TestCase]
    public void TestCompositeEnumerator()
    {
      String[] parameterNames = { "CB1Param1", "CB1Param2", "CB1Param3", "CB2Param1", "CB2Param2", "CB2Param3", "CB2Param4", "CB3Param1", "CB3Param2", "Texture1", "Texture2", "Texture3", "Sampler1" };
      List<IEffectParameter> temp = new List<IEffectParameter>();
      List<IEffectConstantBuffer> temp2 = new List<IEffectConstantBuffer>();

      temp.Add(new DummyEffectParameter(parameterNames[0]));
      temp.Add(new DummyEffectParameter(parameterNames[1]));
      temp.Add(new DummyEffectParameter(parameterNames[2]));
      temp2.Add(new DummyEffectConstantBuffer("CB1", new EffectParameterCollection(temp)));
      temp.Clear();

      temp.Add(new DummyEffectParameter(parameterNames[3]));
      temp.Add(new DummyEffectParameter(parameterNames[4]));
      temp.Add(new DummyEffectParameter(parameterNames[5]));
      temp.Add(new DummyEffectParameter(parameterNames[6]));
      temp2.Add(new DummyEffectConstantBuffer("CB2", new EffectParameterCollection(temp)));
      temp.Clear();

      temp.Add(new DummyEffectParameter(parameterNames[7]));
      temp.Add(new DummyEffectParameter(parameterNames[8]));
      temp2.Add(new DummyEffectConstantBuffer("CB3", new EffectParameterCollection(temp)));
      temp.Clear();

      temp.Add(new DummyEffectParameter(parameterNames[9]));
      temp.Add(new DummyEffectParameter(parameterNames[10]));
      temp.Add(new DummyEffectParameter(parameterNames[11]));
      temp.Add(new DummyEffectParameter(parameterNames[12]));
      EffectParameterCollection resources = new EffectParameterCollection(temp);
      temp.Clear();

      EffectConstantBufferCollection constantBuffers = new EffectConstantBufferCollection(temp2);
      temp2.Clear();

      CompositeEffectParameterEnumerator enumerator = new CompositeEffectParameterEnumerator(constantBuffers, resources);
      int index = 0;

      while (enumerator.MoveNext())
      {
        IEffectParameter param = enumerator.Current;
        Assert.IsNotNull(param);
        Assert.IsTrue(param.Name == parameterNames[index]);
        Console.WriteLine(param.Name);
        index++;
      }

      Assert.IsTrue(index == parameterNames.Length);
    }
  }
}
