﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.IO;
using Tesla.Graphics;

namespace Tesla.Content
{
  /// <summary>
  /// Creates <see cref="MaterialDefinition"/> objects from Assimp materials for the built-in standard material library.
  /// </summary>
  public sealed class StandardAssimpMaterialCreator : IAssimpMaterialCreator
  {
    /// <summary>
    /// Creates a full material definition from the Assimp material.
    /// </summary>
    /// <param name="cm">Content manager loading the model.</param>
    /// <param name="parser">Material parser if any material scripts need parsing, this allows for objects to be shared between materials.</param>
    /// <param name="parentFile">Model file being loaded, may be null if loading from a stream.</param>
    /// <param name="modelName">Name of the model.</param>
    /// <param name="assimpMat">Assimp material that needs to be processed.</param>
    /// <param name="meshData">Mesh the material will be used for, useful for querying the presence of vertex attributes.</param>
    /// <param name="importParams">Optional model importers, may contain material/texture import parameters.</param>
    /// <returns>Created material definition.</returns>
    public MaterialDefinition CreateMaterialDefinition(ContentManager cm, MaterialParser parser, IResourceFile parentFile, String modelName, Assimp.Material assimpMat, MeshData meshData, ModelImporterParameters importParams)
    {
      bool preferLit = ((importParams != null) ? importParams.PreferLitStandardMaterials : true) && meshData.ContainsBuffer(VertexSemantic.Normal, 0);
      String overrideTexPath = (importParams != null) ? importParams.MaterialParameters.OverrideTexturePath : null;

      TextureImporterParameters texImportParams = (importParams != null) ? importParams.MaterialParameters.TextureParameters : null;

      bool hasVertexColoring = meshData.ContainsBuffer(VertexSemantic.Color, 0);
      bool hasTextureCoordinates = meshData.ContainsBuffer(VertexSemantic.TextureCoordinate, 0);
      bool hasTangents = meshData.ContainsBuffer(VertexSemantic.Tangent, 0);
      bool hasBitangents = meshData.ContainsBuffer(VertexSemantic.Bitangent, 0);
      bool onlyWantTangents = (importParams != null) ? importParams.TangentBasisGenerationOptions == TangentBasisGeneration.TangentOnly : false;
      bool wantNormalMap = hasTextureCoordinates && preferLit && (hasTangents && hasBitangents && !onlyWantTangents); //Standard mats only support Tangent/Bitangents
      bool wantEmissiveMap = hasTextureCoordinates && preferLit && assimpMat.HasTextureEmissive;
      bool wantParallaxMap = wantNormalMap && assimpMat.HasTextureDisplacement;
      bool setSampler = false;
      bool hasTransparency = false;

      String matScript = null;

      if (preferLit)
      {
        if (wantNormalMap && HasTextureNormal(assimpMat))
        {
          setSampler = true;

          if (wantEmissiveMap)
          {
            if (wantParallaxMap)
            {
              matScript = (hasVertexColoring) ? StandardMaterialScriptLibrary.GetMaterialScript("LitStandard_Texture_VcNmmPlx_Glow")
                  : StandardMaterialScriptLibrary.GetMaterialScript("LitStandard_Texture_NmmPlx_Glow");
            }
            else
            {
              matScript = (hasVertexColoring) ? StandardMaterialScriptLibrary.GetMaterialScript("LitStandard_Texture_VcNmm_Glow")
                  : StandardMaterialScriptLibrary.GetMaterialScript("LitStandard_Texture_Nmm_Glow");
            }
          }
          else
          {
            if (wantParallaxMap)
            {
              matScript = (hasVertexColoring) ? StandardMaterialScriptLibrary.GetMaterialScript("LitStandard_Texture_VcNmmPlx")
                  : StandardMaterialScriptLibrary.GetMaterialScript("LitStandard_Texture_NmmPlx");
            }
            else
            {
              matScript = (hasVertexColoring) ? StandardMaterialScriptLibrary.GetMaterialScript("LitStandard_Texture_VcNmm")
                  : StandardMaterialScriptLibrary.GetMaterialScript("LitStandard_Texture_Nmm");
            }
          }
        }
        else if (hasTextureCoordinates && assimpMat.HasTextureDiffuse)
        {
          setSampler = true;

          if (wantEmissiveMap)
          {
            matScript = (hasVertexColoring) ? StandardMaterialScriptLibrary.GetMaterialScript("LitStandard_Texture_Vc_Glow")
                : StandardMaterialScriptLibrary.GetMaterialScript("LitStandard_Texture_Glow");
          }
          else
          {
            matScript = (hasVertexColoring) ? StandardMaterialScriptLibrary.GetMaterialScript("LitStandard_Texture_Vc")
                : StandardMaterialScriptLibrary.GetMaterialScript("LitStandard_Texture");
          }
        }
        else
        {
          matScript = (hasVertexColoring) ? StandardMaterialScriptLibrary.GetMaterialScript("LitStandard_Color_Vc")
              : StandardMaterialScriptLibrary.GetMaterialScript("LitStandard_Color");
        }
      }
      else
      {
        setSampler = true;

        if (hasTextureCoordinates && assimpMat.HasTextureDiffuse)
        {
          matScript = (hasVertexColoring) ? StandardMaterialScriptLibrary.GetMaterialScript("Standard_Texture_Vc")
              : StandardMaterialScriptLibrary.GetMaterialScript("Standard_Texture");
        }
        else
        {
          matScript = (hasVertexColoring) ? StandardMaterialScriptLibrary.GetMaterialScript("Standard_Color_Vc")
              : StandardMaterialScriptLibrary.GetMaterialScript("Standard_Color");
        }
      }

      Tesla.Graphics.Material mat = parser.ParseMaterial(matScript, null, cm);

      if (setSampler)
        mat.SetParameterResource<SamplerState>("MapSampler", SamplerState.AnisotropicWrap);

      if (hasTextureCoordinates && assimpMat.HasTextureDiffuse)
      {
        String diffuseTexPath = GetTextureFilePath(overrideTexPath, assimpMat.TextureDiffuse.FilePath);
        Texture2D tex = cm.LoadOptionalRelativeTo<Texture2D>(diffuseTexPath, parentFile, texImportParams);

        if (tex == null)
          EngineLog.Log(LogLevel.Info, "Missing Diffuse Texture: {0}", diffuseTexPath);

        mat.SetParameterResource<Texture2D>("DiffuseMap", tex);

        if (wantNormalMap)
          mat.SetParameterValue<Bool>("UseDiffuseMap", true);
      }
      else if (wantNormalMap)
      {
        mat.SetParameterValue<Bool>("UseDiffuseMap", false);
      }

      if (wantNormalMap && HasTextureNormal(assimpMat))
      {
        //wantNormalMap would be false if there was no normal map texture
        String normalTexPath = GetTextureFilePath(overrideTexPath, GetTextureNormalPath(assimpMat));
        Texture2D normTex = cm.LoadOptionalRelativeTo<Texture2D>(normalTexPath, parentFile, texImportParams);

        if (normTex == null)
          EngineLog.Log(LogLevel.Info, "Missing Normal Texture: {0}", normalTexPath);

        mat.SetParameterResource<Texture2D>("NormalMap", normTex);

        if (assimpMat.HasTextureSpecular)
        {
          String specTexPath = GetTextureFilePath(overrideTexPath, assimpMat.TextureSpecular.FilePath);
          Texture2D specTex = cm.LoadOptionalRelativeTo<Texture2D>(specTexPath, parentFile, texImportParams);

          if (specTex == null)
            EngineLog.Log(LogLevel.Info, "Missing Specular Texture: {0}", specTexPath);

          mat.SetParameterResource<Texture2D>("SpecularMap", specTex);
          mat.SetParameterValue<Bool>("UseSpecularMap", true);
        }
        else
        {
          mat.SetParameterValue<Bool>("UseSpecularMap", false);
        }
      }

      if (wantEmissiveMap && assimpMat.HasTextureEmissive)
      {
        String emissiveTexPath = GetTextureFilePath(overrideTexPath, assimpMat.TextureEmissive.FilePath);
        Texture2D emissiveTex = cm.LoadOptionalRelativeTo<Texture2D>(emissiveTexPath, parentFile, texImportParams);

        if (emissiveTex == null)
          EngineLog.Log(LogLevel.Info, "Missing Emissive Texture: {0}", emissiveTexPath);

        mat.SetParameterResource<Texture2D>("EmissiveMap", emissiveTex);
      }

      if (assimpMat.HasColorDiffuse)
      {
        Vector3 diffColor = ConvertColorToVector3(assimpMat.ColorDiffuse);
        if (diffColor.IsAlmostZero())
          diffColor = Vector3.One;

        mat.SetParameterValue<Vector3>("MatDiffuse", diffColor);
      }

      if (preferLit)
      {
        if (assimpMat.HasColorAmbient)
          mat.SetParameterValue<Vector3>("MatAmbient", ConvertColorToVector3(assimpMat.ColorAmbient)); //new Color4D(0, 0, 0)));

        if (assimpMat.HasColorEmissive)
          mat.SetParameterValue<Vector3>("MatEmissive", ConvertColorToVector3(assimpMat.ColorEmissive));

        if (assimpMat.HasColorSpecular)
          mat.SetParameterValue<Vector3>("MatSpecular", ConvertColorToVector3(assimpMat.ColorSpecular)); //new Color4D(0,0,0)));

        if (assimpMat.HasShininess)
          mat.SetParameterValue<float>("MatShininess", MathF.Max(1, assimpMat.Shininess));
      }

      if (assimpMat.HasOpacity && !MathHelper.IsNearlyOne(assimpMat.Opacity))
      {
        hasTransparency = true;
        mat.SetParameterValue<float>("Alpha", assimpMat.Opacity);
      }

      if (assimpMat.HasTwoSided && assimpMat.IsTwoSided)
        mat.TransparencyMode = TransparencyMode.TwoSided;

      mat.Name = assimpMat.Name;
      mat.ScriptFileName = modelName + "_Materials";
      mat.Effect.CurrentShaderGroup = mat.Passes[0].ShaderGroup;

      //Note: Use default render states. We ensure the meshes are in CW winding by default. CCW if input parameters set to be flipped, in either
      //case normals - generated or otherwise - should have been flipped appropiately.

      MaterialDefinition matDef = new MaterialDefinition();
      matDef.Name = assimpMat.Name;
      matDef.ScriptFileName = modelName + "_MatDefs";
      matDef.Add((hasTransparency) ? RenderBucketID.Transparent : RenderBucketID.Opaque, mat);

      return matDef;
    }

    private static String GetTextureFilePath(String overrideDir, String path)
    {
      if (String.IsNullOrEmpty(overrideDir))
        return ContentHelper.TrimRootedPath(path);

      String fileName = Path.GetFileName(path);
      return Path.Combine(overrideDir, fileName);
    }

    private static Vector3 ConvertColorToVector3(Assimp.Color4D c)
    {
      return new Vector3(c.R, c.G, c.B);
    }

    private static bool HasTextureNormal(Assimp.Material mat)
    {
      //Sometimes the heightmap is the bump map..e.g. OBJ format
      return mat.HasTextureNormal || mat.HasTextureHeight;
    }

    private static String GetTextureNormalPath(Assimp.Material mat)
    {
      if (mat.HasTextureNormal)
        return mat.TextureNormal.FilePath;

      if (mat.HasTextureHeight)
        return mat.TextureHeight.FilePath;

      return String.Empty;
    }
  }
}
