﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using Assimp;
using Assimp.Configs;
using System;
using System.Collections.Generic;
using System.IO;
using Tesla.Graphics;
using Tesla.Scene;

namespace Tesla.Content
{
  /// <summary>
  /// Model importer that uses the AssimpNet library for loading and proessing. This importer supports a variety of formats and can do a number of mesh
  /// processing / optimization (such as merging meshes, calculating normals), and returns the data in <see cref="Spatial"/> scenegraph data structure.
  /// </summary>
  public class AssimpModelImporter : ResourceImporter<Spatial>
  {
    /// <summary>
    /// Boolean parameter. Simplifies the node hierarchy where appropiate.
    /// </summary>
    public static readonly String ExtendedParam_OptimizeGraph = "OptimizeGraph";

    /// <summary>
    /// Boolean parameter. Reduces the number of meshes by combining meshes with the same materials, can result in mesh name loss.
    /// </summary>
    public static readonly String ExtendedParam_OptimizeMeshes = "OptimizeMeshes";

    /// <summary>
    /// Boolean parameter. Fixes normals if they're pointing in the wrong direction.
    /// </summary>
    public static readonly String ExtendedParam_FixInFacingNormals = "FixInFacingNormals";

    /// <summary>
    /// Int parameter. Specifies the max number of triangles each mesh should have. If a mesh goes over this limit it is split. This can
    /// result in mesh name loss.
    /// </summary>
    public static readonly String ExtendedParam_SplitLargeMeshes = "SplitLargeMeshes";

    /// <summary>
    /// String parameter. If importing from a stream, a format hint must be supplied to determine what type of importer is to be used.
    /// </summary>
    public static readonly String ExtendedParam_FormatHint = "FormatHint";

    private IAssimpMaterialCreator m_standardCreator;

    /// <summary>
    /// Gets or sets the standard material creator. This is the default material creation process if no material paths are set in the importer
    /// parameters.
    /// </summary>
    public IAssimpMaterialCreator StandardMaterialCreator
    {
      get
      {
        return m_standardCreator;
      }
      set
      {
        if (value == null)
          m_standardCreator = new StandardAssimpMaterialCreator();
        else
          m_standardCreator = value;
      }
    }

    static AssimpModelImporter()
    {
      //TEMP - We use the default library location, but loading the library isnt threadsafe in the currently released assimpnet version. Next version there will be a fix
      if (!Assimp.Unmanaged.AssimpLibrary.Instance.IsLibraryLoaded)
        Assimp.Unmanaged.AssimpLibrary.Instance.LoadLibrary();
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="AssimpModelImporter"/> class.
    /// </summary>
    public AssimpModelImporter()
        : base(".dae", ".fbx", ".blend", ".3ds", ".ase", ".obj", ".ifc", ".xgl", ".zgl", ".ply", ".dxf", ".lwo", ".lws", ".lxo", ".stl", ".x", ".ac", ".ms3d", ".cob", ".scn",
               ".bvh", ".csm", ".mdl", ".md2", ".md3", ".pk3", ".mdc", ".md5", ".smd", ".vta", ".m3", ".3d", ".b3d")
    {
      m_standardCreator = new StandardAssimpMaterialCreator();
    }

    /// <summary>
    /// Loads content from the specified resource as the target runtime type.
    /// </summary>
    /// <param name="resourceFile">Resource file to read from</param>
    /// <param name="contentManager">Calling content manager</param>
    /// <param name="parameters">Optional loading parameters</param>
    /// <returns>The loaded object or null if it could not be loaded</returns>
    public override Spatial Load(IResourceFile resourceFile, ContentManager contentManager, ImporterParameters parameters)
    {
      ValidateParameters(resourceFile, contentManager, ref parameters);

      AssimpContext context = new AssimpContext();
      context.SetIOSystem(new ContentIOSystem(contentManager, resourceFile));

      PostProcessSteps importFlags;
      SetConfigs(parameters as ModelImporterParameters, context, out importFlags);

      Assimp.Scene scene = context.ImportFile(resourceFile.FullName, importFlags);

      if (scene == null)
        return null;

      SceneData data = new SceneData(parameters as ModelImporterParameters, contentManager, resourceFile, resourceFile.Name, m_standardCreator, importFlags);
      data.LoadDataFromScene(scene);

      return data.Root;
    }

    /// <summary>
    /// Loads content from the specified stream as the target runtime type.
    /// </summary>
    /// <param name="input">Stream to read from.</param>
    /// <param name="contentManager">Calling content manager.</param>
    /// <param name="parameters">Optional loading parameters.</param>
    /// <returns>The loaded object or null if it could not be loaded.</returns>
    public override Spatial Load(Stream input, ContentManager contentManager, ImporterParameters parameters)
    {
      ValidateParameters(input, contentManager, ref parameters);

      AssimpContext context = new AssimpContext();
      context.SetIOSystem(new ContentIOSystem(contentManager, null));

      PostProcessSteps importFlags;
      SetConfigs(parameters as ModelImporterParameters, context, out importFlags);

      //A format hint is required, else we don't know how to import the file
      String format;
      if (parameters == null || !parameters.ExtendedParameters.TryGetValue(ExtendedParam_FormatHint, out format))
        throw new TeslaContentException(StringLocalizer.Instance.GetLocalizedString("AssimpFormatHintNull"));

      Assimp.Scene scene = context.ImportFileFromStream(input, importFlags, format);

      if (scene == null)
        return null;

      SceneData data = new SceneData(parameters as ModelImporterParameters, contentManager, null, scene.RootNode.Name, m_standardCreator, importFlags);
      data.LoadDataFromScene(scene);

      return data.Root;
    }

    private void SetConfigs(ModelImporterParameters parameters, AssimpContext context, out PostProcessSteps importFlags)
    {
      //By default we want to make the meshes as quality as possible for rendering.
      importFlags = PostProcessSteps.FindInvalidData | PostProcessSteps.FindDegenerates | PostProcessSteps.LimitBoneWeights |
          PostProcessSteps.Triangulate | PostProcessSteps.JoinIdenticalVertices | PostProcessSteps.ValidateDataStructure |
          PostProcessSteps.SortByPrimitiveType | PostProcessSteps.RemoveRedundantMaterials | PostProcessSteps.FindInstances |
          PostProcessSteps.FlipWindingOrder; //By default Assimp is CCW winding order, but the engine always assumes CW

      if (parameters == null)
        return;

      if (IsParameterTrue(ExtendedParam_OptimizeGraph, parameters))
        importFlags |= PostProcessSteps.OptimizeGraph;

      if (IsParameterTrue(ExtendedParam_OptimizeMeshes, parameters))
        importFlags |= PostProcessSteps.OptimizeMeshes;

      if (IsParameterTrue(ExtendedParam_FixInFacingNormals, parameters))
        importFlags |= PostProcessSteps.FixInFacingNormals;

      int triangleMax;
      if (IsParameterTrueWithValue(ExtendedParam_SplitLargeMeshes, parameters, out triangleMax))
      {
        importFlags |= PostProcessSteps.SplitLargeMeshes;
        context.SetConfig(new MeshTriangleLimitConfig(triangleMax));
      }

      context.Scale = parameters.Scale;
      context.XAxisRotation = parameters.XAngle.Degrees;
      context.YAxisRotation = parameters.YAngle.Degrees;
      context.ZAxisRotation = parameters.ZAngle.Degrees;

      //By DEFAULT, the engine assumes a Clockwise Winding order!! Assimp assumes a CCW winding order by default. So unset the flip winding order
      if (parameters.SwapWindingOrder)
        importFlags &= ~PostProcessSteps.FlipWindingOrder;

      if (parameters.FlipUVs)
        importFlags |= PostProcessSteps.FlipUVs;

      if (parameters.TangentBasisGenerationOptions != TangentBasisGeneration.None)
        importFlags |= PostProcessSteps.CalculateTangentSpace;

      switch (parameters.NormalGenerationOptions)
      {
        case NormalGeneration.Face:
          importFlags |= PostProcessSteps.GenerateNormals;
          importFlags &= ~PostProcessSteps.JoinIdenticalVertices; //According to docs this wont apply when generating normals
          break;
        case NormalGeneration.Smooth:
        case NormalGeneration.Crease:
          importFlags |= PostProcessSteps.GenerateSmoothNormals;

          //Smooth uses Assimp's default crease angle, which is 175.0 Degrees.
          if (parameters.NormalGenerationOptions == NormalGeneration.Crease)
            context.SetConfig(new NormalSmoothingAngleConfig(parameters.CreaseAngle.Degrees));

          break;
      }

      //If generate normals, make sure we remove any existing
      if (parameters.NormalGenerationOptions != NormalGeneration.None)
      {
        importFlags |= PostProcessSteps.RemoveComponent;
        context.SetConfig(new RemoveComponentConfig(ExcludeComponent.Normals | ExcludeComponent.TangentBasis));
      }
    }

    #region Helper methods

    private static Tesla.Graphics.Light ConvertLight(Assimp.Light l)
    {
      Tesla.Graphics.Light light = null;
      switch (l.LightType)
      {
        case LightSourceType.Directional:
          {
            DirectionalLight dl = new DirectionalLight();
            dl.Name = l.Name;
            dl.Direction = ConvertVector3(l.Direction);
            dl.Ambient = ConvertColor3(l.ColorAmbient);
            dl.Diffuse = ConvertColor3(l.ColorDiffuse);
            dl.Specular = ConvertColor3(l.ColorSpecular);

            light = dl;
          }
          break;
        case LightSourceType.Point:
          {
            PointLight pl = new PointLight();
            pl.Name = l.Name;
            pl.Position = ConvertVector3(l.Position);
            pl.Ambient = ConvertColor3(l.ColorAmbient);
            pl.Diffuse = ConvertColor3(l.ColorDiffuse);
            pl.Specular = ConvertColor3(l.ColorSpecular);

            pl.SetAttenuation(l.AttenuationConstant, l.AttenuationLinear, l.AttenuationQuadratic, true);
            light = pl;
          }
          break;
        case LightSourceType.Spot:
          {
            SpotLight sl = new SpotLight();
            sl.Name = l.Name;
            sl.Position = ConvertVector3(l.Position);
            sl.Direction = ConvertVector3(l.Direction);
            sl.Ambient = ConvertColor3(l.ColorAmbient);
            sl.Diffuse = ConvertColor3(l.ColorDiffuse);
            sl.Specular = ConvertColor3(l.ColorSpecular);

            sl.SetAttenuation(l.AttenuationConstant, l.AttenuationLinear, l.AttenuationQuadratic, true);
            light = sl;
          }
          break;
        default:
          return null;
      }

      return light;
    }

    private static String GetTextureFilePath(String overrideDir, String path)
    {
      if (String.IsNullOrEmpty(overrideDir))
        return path;

      String fileName = Path.GetFileName(path);
      return Path.Combine(overrideDir, fileName);
    }

    private static Vector3 ConvertVector3(Vector3D vector)
    {
      return new Vector3(vector.X, vector.Y, vector.Z);
    }

    private static void ConvertVector3(ref Vector3D vector, out Vector3 result)
    {
      result = new Vector3(vector.X, vector.Y, vector.Z);
    }

    private static Tesla.Quaternion ConvertQuaternion(Assimp.Quaternion q)
    {
      return new Tesla.Quaternion(q.X, q.Y, q.Z, q.W);
    }

    private static void ConvertQuaternion(ref Assimp.Quaternion q, out Tesla.Quaternion result)
    {
      result = new Tesla.Quaternion(q.X, q.Y, q.Z, q.W);
    }

    private static Tesla.Matrix ConvertMatrix(Assimp.Matrix4x4 m)
    {
      //Assimp matrices are column vector
      return new Tesla.Matrix(m.A1, m.B1, m.C1, m.D1, m.A2, m.B2, m.C2, m.D2,
          m.A3, m.B3, m.C3, m.D3, m.A4, m.B4, m.C4, m.D4);
    }

    private static void ConvertMatrix(ref Assimp.Matrix4x4 m, out Tesla.Matrix result)
    {
      //Assimp matrices are column vector
      result = new Tesla.Matrix(m.A1, m.B1, m.C1, m.D1, m.A2, m.B2, m.C2, m.D2,
          m.A3, m.B3, m.C3, m.D3, m.A4, m.B4, m.C4, m.D4);
    }

    private static Color ConvertColor3(Assimp.Color3D c)
    {
      return new Color(c.R, c.G, c.B, 1.0f);
    }

    private static void ConvertColor3(ref Assimp.Color3D c, out Color result)
    {
      result = new Color(c.R, c.G, c.B, 1.0f);
    }

    private static Color ConvertColor(Assimp.Color4D c)
    {
      return new Color(c.R, c.G, c.B, c.A);
    }

    private static void ConvertColor(ref Assimp.Color4D c, out Color result)
    {
      result = new Color(c.R, c.G, c.B, c.A);
    }

    private static Vector3 ConvertColorToVector3(Assimp.Color4D c)
    {
      return new Vector3(c.R, c.G, c.B);
    }

    private static void ConvertColorToVector3(ref Assimp.Color4D c, out Vector3 result)
    {
      result = new Vector3(c.R, c.G, c.B);
    }

    private static void CreateModelTransform(float scale, Angle xAngle, Angle yAngle, Angle zAngle, out Matrix transform)
    {
      Matrix sm, xRot, yRot, zRot;

      Matrix.FromScale(scale, out sm);
      Matrix.FromRotationX(xAngle, out xRot);
      Matrix.FromRotationY(yAngle, out yRot);
      Matrix.FromRotationZ(zAngle, out zRot);

      transform = (scale) * (xRot * yRot * zRot);
    }

    private static bool IsParameterTrue(String paramName, ModelImporterParameters param)
    {
      String val;
      if (param.ExtendedParameters.TryGetValue(paramName, out val))
      {
        bool boolVal;
        if (bool.TryParse(val, out boolVal))
          return boolVal;
      }

      return false;
    }

    private static bool IsParameterTrueWithValue(String paramName, ModelImporterParameters param, out int value)
    {
      value = 0;

      String val;
      if (param.ExtendedParameters.TryGetValue(paramName, out val))
      {
        if (int.TryParse(val, out value))
          return true;
      }

      return false;
    }

    #endregion

    #region Data structures

    #region Material Management

    private class MaterialMapper
    {
      private List<MaterialEntry> m_materials; //In order of assimp mats
      private ContentManager m_cm;
      private MaterialParser m_parser;
      private MaterialEntry m_defaultMat;
      private IAssimpMaterialCreator m_standardCreator;
      private ModelImporterParameters m_modelParams;
      private IResourceFile m_parentFile;
      private String m_modelName;

      public MaterialParser Parser
      {
        get
        {
          return m_parser;
        }
      }

      public MaterialMapper(ContentManager cm, IAssimpMaterialCreator standardCreator, ModelImporterParameters modelParams, IResourceFile parentFile, String modelName)
      {
        m_cm = cm;
        m_parser = new MaterialParser(GetParserCache(cm));
        m_standardCreator = standardCreator;
        m_modelParams = modelParams;
        m_parentFile = parentFile;
        m_modelName = modelName;

        m_materials = new List<MaterialEntry>();
        CreateDefaultMaterial();
      }

      public void AddMaterial(Assimp.Material originalMat, String overrideScript = null)
      {
        m_materials.Add(new MaterialEntry(originalMat, overrideScript));
      }

      public MaterialDefinition GetMaterialDefinition(int index, MeshData msh)
      {
        MaterialEntry entry = m_defaultMat;

        if (index >= 0 || index < m_materials.Count)
          entry = m_materials[index];

        MaterialDefinition matDef = entry.GetMaterialDefinition(m_parser, m_parentFile, m_modelName, m_cm, m_modelParams, m_standardCreator, msh);

        //If null for whatever reason, try and create the default material...
        if (matDef == null)
          matDef = m_defaultMat.GetMaterialDefinition(m_parser, m_parentFile, m_modelName, m_cm, m_modelParams, m_standardCreator, msh);

        return matDef;
      }

      private MaterialParserCache GetParserCache(ContentManager cm)
      {
        foreach (IResourceImporter importer in cm.ResourceImporters)
        {
          if (importer is MaterialDefinitionResourceImporter)
          {
            return (importer as MaterialDefinitionResourceImporter).ParserCache;
          }
          else if (importer is MaterialResourceImporter)
          {
            return (importer as MaterialResourceImporter).ParserCache;
          }
        }

        return new MaterialParserCache();
      }

      private void CreateDefaultMaterial()
      {
        Assimp.Material mat = new Assimp.Material();
        mat.ColorDiffuse = new Color4D(1.0f, 1.0f, 1.0f, 1.0f);

        m_defaultMat = new MaterialEntry(mat);
      }
    }

    private class MaterialEntry
    {
      private Assimp.Material m_assimpMat;
      private String m_matScriptPath;
      private bool m_isStandard;
      private bool m_isInitialized;
      private MaterialDefinition m_matDef;

      public MaterialEntry(Assimp.Material mat)
          : this(mat, null)
      {
      }

      public MaterialEntry(Assimp.Material mat, String matScriptPathWithName)
      {
        m_assimpMat = mat;
        m_isStandard = String.IsNullOrEmpty(matScriptPathWithName); //Assumed to exist
        m_isInitialized = false;
        m_matDef = null;
        m_matScriptPath = matScriptPathWithName;
      }

      public MaterialDefinition GetMaterialDefinition(MaterialParser parser, IResourceFile parentFile, String modelName, ContentManager cm,
          ModelImporterParameters modelParams, IAssimpMaterialCreator standardCreator, MeshData mesh)
      {
        //If already created, just clone the definition
        if (m_isInitialized)
        {
          //If we have a standard mat, call into the creator to return us a new one
          if (m_isStandard)
          {
            //Generally shouldn't happen...
            if (m_assimpMat == null)
              return null;

            return standardCreator.CreateMaterialDefinition(cm, parser, parentFile, modelName, m_assimpMat, mesh, modelParams);
          }

          return m_matDef.Clone();
        }

        //First check if we have a material definition script to load
        if (!String.IsNullOrEmpty(m_matScriptPath))
        {
          String resourceFilePath, subresourceName;
          ContentHelper.ParseSubresourceName(m_matScriptPath, out resourceFilePath, out subresourceName);
          String ext = Path.GetExtension(resourceFilePath);

          if (".tem".Equals(ext, StringComparison.InvariantCultureIgnoreCase))
          {
            Tesla.Graphics.Material mat = cm.Load<Tesla.Graphics.Material>(m_matScriptPath, modelParams.MaterialParameters);
            m_matDef = new MaterialDefinition();
            m_matDef.Name = mat.Name + "_MatDef";
            m_matDef.ScriptFileName = modelName + "_MatDefs";
            m_matDef.Add(RenderBucketID.Opaque, mat);
          }
          else if (".temd".Equals(ext, StringComparison.InvariantCultureIgnoreCase))
          {
            m_matDef = cm.Load<MaterialDefinition>(m_matScriptPath, modelParams.MaterialParameters);
          }

          //If successully loaded, we have our matdef and can return. May throw an exception, but we probably want that to bubble up.
          if (m_matDef != null)
          {
            m_isStandard = false;
            m_isInitialized = true;
            return m_matDef;
          }
        }

        //Otherwise this will only create a standard material based on meshes and the assimp material
        m_isStandard = true;
        m_isInitialized = true;

        if (m_assimpMat == null)
          return null;

        return standardCreator.CreateMaterialDefinition(cm, parser, parentFile, modelName, m_assimpMat, mesh, modelParams);
      }
    }

    #endregion

    #region Scene Management

    private class SceneData
    {
      private MaterialMapper m_materialMapper;
      private String m_materialScriptFile;
      private HashSet<String> m_materialScriptFileNames;
      private Dictionary<String, String> m_matNamesToMatFiles;
      private ModelImporterParameters m_modelParams;
      private ContentManager m_cm;

      private List<MeshInfo> m_meshDatas;
      private bool m_flipNormals;

      public Spatial Root;

      public SceneData(ModelImporterParameters modelParams, ContentManager cm, IResourceFile parentFile, String modelName, IAssimpMaterialCreator standardCreator, PostProcessSteps importFlags)
      {
        m_modelParams = modelParams;
        m_materialMapper = new MaterialMapper(cm, standardCreator, modelParams, parentFile, modelName);
        m_cm = cm;

        //We do some tricks to ensure meshes are in Clockwise winding (default is CCW), if the flip winding flag is NOT present then we preserve normals. If we generate normals,
        //we need to flip the normals too...for some reason.
        m_flipNormals = HasFlag(importFlags, PostProcessSteps.GenerateNormals) || HasFlag(importFlags, PostProcessSteps.GenerateSmoothNormals) || !HasFlag(importFlags, PostProcessSteps.FlipWindingOrder);

        if (modelParams != null)
        {
          m_materialScriptFile = modelParams.MaterialScriptPath;
          m_materialScriptFileNames = new HashSet<String>();

          QueryMaterialNames();

          m_matNamesToMatFiles = modelParams.MaterialsToMaterialScriptMapping;
        }
        else
        {
          m_materialScriptFile = String.Empty;
          m_materialScriptFileNames = new HashSet<String>();
          m_matNamesToMatFiles = new Dictionary<String, String>();
        }
      }

      public void LoadDataFromScene(Assimp.Scene scene)
      {
        IRenderSystem renderSystem = Tesla.Graphics.GraphicsHelper.GetRenderSystem(m_cm.ServiceProvider);

        LoadMaterials(scene);
        LoadMeshes(renderSystem, scene);
        LoadSceneGraph(scene);
        LoadLights(scene);
      }

      private bool HasFlag(PostProcessSteps importFlags, PostProcessSteps flag)
      {
        return (importFlags & flag) == flag;
      }

      private void QueryMaterialNames()
      {
        if (!String.IsNullOrEmpty(m_materialScriptFile))
        {
          IResourceFile scriptFile = m_cm.QueryResourceFile(m_materialScriptFile);
          if (scriptFile != null && scriptFile.Exists)
          {
            String scriptText = new StreamReader(scriptFile.OpenRead()).ReadToEnd();
            List<String> nameList = new List<String>();
            if (m_materialMapper.Parser.ParseNamesFromScript(scriptText, nameList))
            {
              foreach (String name in nameList)
                m_materialScriptFileNames.Add(name);
            }
          }
        }
      }

      private void LoadMaterials(Assimp.Scene scene)
      {
        if (!scene.HasMaterials)
          return;

        for (int i = 0; i < scene.MaterialCount; i++)
        {
          Assimp.Material mat = scene.Materials[i];
          String overrideScriptPath = null;

          //Try and get override material based on name, first the explicit mapping then if the name is contained
          //in the implicit listing, concat the supplied material script file with that name as a subresource
          if (!m_matNamesToMatFiles.TryGetValue(mat.Name, out overrideScriptPath))
          {
            if (m_materialScriptFileNames.Contains(mat.Name))
              overrideScriptPath = m_materialScriptFile + "::" + mat.Name;
          }

          m_materialMapper.AddMaterial(mat, overrideScriptPath);
        }
      }

      private void LoadSceneGraph(Assimp.Scene scene)
      {
        //If root node is one node, with no children and one mesh, then return the mesh as the root
        if (scene.RootNode.ChildCount == 0 && scene.RootNode.MeshCount == 1)
        {
          Root = CreateMesh(scene.RootNode, 0);
        }
        else
        {
          Root = CreateNode(scene.RootNode, null);
        }
      }

      private bool HasMeshesInGraph(Assimp.Node n)
      {
        if (n.MeshCount > 0)
          return true;

        for (int i = 0; i < n.ChildCount; i++)
        {
          if (HasMeshesInGraph(n.Children[i]))
            return true;
        }

        return false;
      }

      private Tesla.Scene.Node CreateNode(Assimp.Node n, Tesla.Scene.Node parent)
      {
        Tesla.Scene.Node node = new Scene.Node(n.Name, n.ChildCount + n.MeshCount);

        Matrix transform = ConvertMatrix(n.Transform);
        node.Transform.Set(transform); //Decompose transform

        for (int i = 0; i < n.MeshIndices.Count; i++)
          node.Children.Add(CreateMesh(n, n.MeshIndices[i]));

        for (int i = 0; i < n.ChildCount; i++)
        {
          //Because Assimp sometimes keeps node branches for animations, make sure each child we add actually has meshes...
          Assimp.Node child = n.Children[i];
          if (HasMeshesInGraph(child))
            node.Children.Add(CreateNode(child, node));
        }

        return node;
      }

      private Tesla.Scene.Mesh CreateMesh(Assimp.Node n, int meshIndex)
      {
        MeshInfo info = m_meshDatas[meshIndex];

        String meshName = String.IsNullOrEmpty(info.Name) ? n.Name : info.Name;

        Tesla.Scene.Mesh msh = new Scene.Mesh(meshName, info.Data);

        //Track usage count for the already loaded matdef, we don't want to waste the first instance
        msh.MaterialDefinition = (info.UseCount == 0) ? info.MatDef : info.MatDef.Clone();

        info.UseCount += 1;

        Matrix transform = ConvertMatrix(n.Transform);
        msh.Transform.Set(transform); //Decompose transform

        msh.SetModelBounding(new BoundingBox(info.Bounding));

        return msh;
      }

      private void LoadMeshes(IRenderSystem renderSystem, Assimp.Scene scene)
      {
        m_meshDatas = new List<MeshInfo>(scene.MeshCount);

        bool discardBinormals = (m_modelParams != null) ? m_modelParams.TangentBasisGenerationOptions == TangentBasisGeneration.TangentOnly : false;

        for (int i = 0; i < scene.MeshCount; i++)
        {
          Assimp.Mesh msh = scene.Meshes[i];

          bool hasNormals = msh.HasNormals;
          bool hasTangentBasis = msh.HasTangentBasis;
          bool hasTextureCoordinates = msh.HasTextureCoords(0);
          bool hasVertexColors = msh.HasVertexColors(0);

          MeshData md = new MeshData();

          switch (msh.PrimitiveType)
          {
            case Assimp.PrimitiveType.Line:
              md.PrimitiveType = Tesla.Graphics.PrimitiveType.LineList;
              break;
            case Assimp.PrimitiveType.Point:
              md.PrimitiveType = Tesla.Graphics.PrimitiveType.PointList;
              break;
            default:
              md.PrimitiveType = Tesla.Graphics.PrimitiveType.TriangleList;
              break;
          }

          md.Indices = LoadIndexData((m_modelParams == null) ? IndexFormat.ThirtyTwoBits : m_modelParams.IndexFormat, msh);
          md.Positions = LoadVertexData(msh.Vertices);

          if (hasNormals)
            md.Normals = LoadVertexData(msh.Normals, m_flipNormals);

          //Do we need to flip the tangent basis??
          if (hasTangentBasis)
          {
            md.Tangents = LoadVertexData(msh.Tangents);

            if (!discardBinormals)
              md.Bitangents = LoadVertexData(msh.BiTangents);
          }

          //Parse in all the texture coordinates...might have multiple for stuff like light mapping
          for (int j = 0; j < msh.TextureCoordinateChannelCount; j++)
          {
            if (!msh.HasTextureCoords(j))
              break;

            md.AddBuffer(VertexSemantic.TextureCoordinate, j, LoadTexCoordData(msh.TextureCoordinateChannels[j]));
          }

          //Only take the first vertex color channel
          if (hasVertexColors)
            md.Colors = LoadVertexData(msh.VertexColorChannels[0]);

          md.Compile(renderSystem);

          MeshInfo info = new MeshInfo();
          info.Name = msh.Name;
          info.UseCount = 0;

          //Create a material based on the assimp index (it may have an override material) and hold onto the matdef for whereever this mesh
          //is used
          info.MatDef = m_materialMapper.GetMaterialDefinition(msh.MaterialIndex, md);
          info.Data = md;
          info.Bounding = (md.UseIndexedPrimitives) ? BoundingBox.Data.FromIndexedPoints(md.Positions, md.Indices.Value)
              : BoundingBox.Data.FromPoints(md.Positions);

          m_meshDatas.Add(info);
        }
      }

      private DataBuffer<Vector3> LoadVertexData(List<Vector3D> verts, bool negate = false)
      {
        DataBuffer<Vector3> db = DataBuffer.Create<Vector3>(verts.Count);

        if (negate)
        {
          for (int i = 0; i < verts.Count; i++)
          {
            Vector3D av = verts[i];
            Vector3 v = new Vector3(av.X, av.Y, av.Z);
            v.Negate();
            db[i] = v;
          }
        }
        else
        {
          for (int i = 0; i < verts.Count; i++)
          {
            Vector3D av = verts[i];
            Vector3 v = new Vector3(av.X, av.Y, av.Z);
            db[i] = v;
          }
        }

        return db;
      }

      private DataBuffer<Vector2> LoadTexCoordData(List<Vector3D> verts)
      {
        DataBuffer<Vector2> db = DataBuffer.Create<Vector2>(verts.Count);

        for (int i = 0; i < verts.Count; i++)
        {
          Vector3D av = verts[i];
          Vector2 v = new Vector2(av.X, 1 - av.Y);
          db[i] = v;
        }

        return db;
      }

      private DataBuffer<Color> LoadVertexData(List<Color4D> verts)
      {
        DataBuffer<Color> db = DataBuffer.Create<Color>(verts.Count);

        for (int i = 0; i < verts.Count; i++)
        {
          Color4D ac = verts[i];
          Color c = new Color(ac.R, ac.G, ac.B, ac.A);
          db[i] = c;
        }

        return db;
      }

      private IndexData? LoadIndexData(IndexFormat format, Assimp.Mesh msh)
      {
        int numPerFace = 0;
        switch (msh.PrimitiveType)
        {
          case Assimp.PrimitiveType.Line:
            numPerFace = 2;
            break;
          case Assimp.PrimitiveType.Point:
            numPerFace = 1;
            break;
          case Assimp.PrimitiveType.Triangle:
            numPerFace = 3;
            break;
          default:
            return null;
        }

        List<Face> faces = msh.Faces;

        IndexDataBuilder builder = new IndexDataBuilder(faces.Count * numPerFace, format);
        for (int i = 0; i < faces.Count; i++)
        {
          Face f = faces[i];
          List<int> indices = faces[i].Indices;
          for (int j = 0; j < indices.Count; j++)
            builder.Set(indices[j]);
        }

        return builder.Claim(true);
      }

      private void LoadLights(Assimp.Scene scene)
      {
        if (m_modelParams == null || !m_modelParams.ImportLights || !scene.HasLights)
          return;

        for (int i = 0; i < scene.LightCount; i++)
        {
          Tesla.Graphics.Light l = ConvertLight(scene.Lights[i]);
          if (l != null)
            Root.Lights.Add(l);
        }
      }
    }

    //Used for capturing mesh data so we can re-use it between different mesh instances
    private class MeshInfo
    {
      public String Name;
      public MaterialDefinition MatDef;
      public MeshData Data;
      public BoundingBox.Data Bounding;
      public int UseCount;
    }

    #endregion

    #endregion

    #region Content IOSystem

    private sealed class ContentIOSystem : IOSystem
    {
      private ContentManager m_content;
      private IResourceFile m_relativeTo;

      public ContentIOSystem(ContentManager cm, IResourceFile relativeTo)
      {
        m_content = cm;
        m_relativeTo = relativeTo;
      }

      public override IOStream OpenFile(String pathToFile, FileIOMode fileMode)
      {
        return new ContentIOStream(m_content, m_relativeTo, pathToFile, fileMode);
      }
    }

    //Custom IO stream implementation so Assimp can resolve secondary files if data is from a stream. We're not using the default file IO stream because
    //a content manager may have a repository that is not a regular file system.
    private sealed class ContentIOStream : IOStream
    {
      private Stream m_stream;

      public ContentIOStream(ContentManager cm, IResourceFile relativeTo, String pathToFile, FileIOMode mode)
          : base(pathToFile, mode)
      {
        switch (mode)
        {
          case FileIOMode.Write:
          case FileIOMode.WriteBinary:
          case FileIOMode.WriteText:
            throw new InvalidOperationException("DuringImportContentIsReadOnly");
        }

        if (relativeTo != null && relativeTo.Exists)
          m_stream = cm.OpenStreamRelativeTo(pathToFile, relativeTo);
        else
          m_stream = cm.OpenStream(pathToFile);
      }

      public override void Flush()
      {
        if (m_stream != null)
          m_stream.Flush();
      }

      public override long GetFileSize()
      {
        if (m_stream == null)
          return 0L;

        return m_stream.Length; //Not sure if this is always reliable...
      }

      public override long GetPosition()
      {
        if (m_stream == null)
          return -1L;

        return m_stream.Position;
      }

      public override bool IsValid
      {
        get
        {
          return m_stream != null;
        }
      }

      public override long Read(byte[] dataRead, long count)
      {
        m_stream.Read(dataRead, (int) m_stream.Position, (int) count);

        return count;
      }

      public override ReturnCode Seek(long offset, Origin seekOrigin)
      {
        SeekOrigin begin = SeekOrigin.Begin;
        switch (seekOrigin)
        {
          case Origin.Set:
            begin = SeekOrigin.Begin;
            break;

          case Origin.Current:
            begin = SeekOrigin.Current;
            break;

          case Origin.End:
            begin = SeekOrigin.End;
            break;
        }

        m_stream.Seek(offset, begin);
        return ReturnCode.Success;
      }

      public override long Write(byte[] dataToWrite, long count)
      {
        throw new InvalidOperationException("DuringImportContentIsReadOnly");
      }

      protected override void Dispose(bool disposing)
      {
        if (!IsDisposed && disposing)
        {
          if (m_stream != null)
          {
            m_stream.Close();
            m_stream = null;
          }

          base.Dispose(disposing);
        }
      }
    }

    #endregion
  }
}
