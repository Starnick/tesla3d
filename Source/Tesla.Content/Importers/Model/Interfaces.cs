﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Graphics;

namespace Tesla.Content
{
  /// <summary>
  /// Represents a factory for creating materials out of Assimp materials. This allows for custom shaders/materials to be outputted from <see cref="AssimpModelImporter"/>, which is useful if
  /// you are using custom shaders/materials. Every built-in material library is expected to implement a creator.
  /// </summary>
  public interface IAssimpMaterialCreator
  {
    /// <summary>
    /// Creates a full material definition from the Assimp material.
    /// </summary>
    /// <param name="cm">Content manager loading the model.</param>
    /// <param name="parser">Material parser if any material scripts need parsing, this allows for objects to be shared between materials.</param>
    /// <param name="parentFile">Model file being loaded, may be null if loading from a stream.</param>
    /// <param name="modelName">Name of the model.</param>
    /// <param name="assimpMat">Assimp material that needs to be processed.</param>
    /// <param name="meshData">Mesh the material will be used for, useful for querying the presence of vertex attributes.</param>
    /// <param name="importParams">Optional model importers, may contain material/texture import parameters.</param>
    /// <returns>Created material definition.</returns>
    MaterialDefinition CreateMaterialDefinition(ContentManager cm, MaterialParser parser, IResourceFile parentFile, String modelName, Assimp.Material assimpMat, MeshData meshData, ModelImporterParameters importParams);
  }
}
