﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.IO;
using Tesla.Graphics;
using TeximpNet;
using TeximpNet.Compression;
using TeximpNet.DDS;

namespace Tesla.Content
{
  /// <summary>
  /// Image importer that uses the TeximpNet library for loading and proessing. This importer supports a variety of formats and a number of texture processing (such as resizing / mip map generation),
  /// and returns the image data as GPU texture resources.
  /// </summary>
  public class TeximpTextureImporter : ResourceImporter<Texture2D>
  {
    /// <summary>
    /// Constructs a new instance of <see cref="TeximpTextureImporter"/>.
    /// </summary>
    public TeximpTextureImporter()
        : base(".bmp", //Bitmap image
               ".gif", //Graphics InterChange Format
               ".ico", //Microsoft Windows Icon
               ".png", //Portable Network Graphics
               ".tga", //Truevision TGA/TARGA
               ".tiff", ".tif", //Tagged Image File format
               ".hdr", //RGBE high dynamic range format
               ".psd", //Photoshop format
               ".jng", ".jpg", ".jpeg", ".jpe", ".jif", ".jfif", ".jfi", // JPEG
               ".jp2", ".j2k", ".jpf", ".jpx", ".jpm", ".mj2", // JPEG 2000
                                                               //Note: TeximpNet through FreeImage supports many other formats, so if needed feel free to add more extensions as needed.
               ".dds" //DDS format, handled by TeximpNet's custom importer
               )
    { }

    /// <summary>
    /// Loads content from the specified resource as the target runtime type.
    /// </summary>
    /// <param name="resourceFile">Resource file to read from</param>
    /// <param name="contentManager">Calling content manager</param>
    /// <param name="parameters">Optional loading parameters</param>
    /// <returns>The loaded object or null if it could not be loaded</returns>
    public override Texture2D Load(IResourceFile resourceFile, ContentManager contentManager, ImporterParameters parameters)
    {
      using (Stream str = resourceFile.OpenRead())
        return Load(str, GraphicsHelper.GetRenderSystem(contentManager.ServiceProvider), parameters as TextureImporterParameters, resourceFile.Name);
    }

    /// <summary>
    /// Loads content from the specified stream as the target runtime type.
    /// </summary>
    /// <param name="input">Stream to read from.</param>
    /// <param name="contentManager">Calling content manager.</param>
    /// <param name="parameters">Optional loading parameters.</param>
    /// <returns>The loaded object or null if it could not be loaded.</returns>
    public override Texture2D Load(Stream input, ContentManager contentManager, ImporterParameters parameters)
    {
      return Load(input, GraphicsHelper.GetRenderSystem(contentManager.ServiceProvider), parameters as TextureImporterParameters, "Texture");
    }

    /// <summary>
    /// Loads an image from the specified stream and returns it as a <see cref="TextureData"/> rather than a GPU resource. This does not
    /// require a <see cref="IRenderSystem"/> to render.
    /// </summary>
    /// <param name="input">Stream to read the texture data from.</param>
    /// <param name="parameters">Optional import parameters.</param>
    /// <returns>Texture data object.</returns>
    public static TextureData Load(Stream input, TextureImporterParameters parameters = null)
    {
      //Always copy data, the owner list should be null on return
      DDSContainer compressedImage;
      TextureData texData = LoadInternal(input, parameters, out compressedImage, true);

      return texData;
    }

    /// <summary>
    /// Loads an image from the specified stream and returns it as a <see cref="Texture2D"/> GPU resource.
    /// </summary>
    /// <param name="input">Stream to read the texture data from.</param>
    /// <param name="renderSystem">Rendersystem used to create the GPU resource.</param>
    /// <param name="parameters">Optional import parameters.</param>
    /// <param name="name">Optional name of the GPU resource.</param>
    /// <returns>Texture GPU resource.</returns>
    public static Texture2D Load(Stream input, IRenderSystem renderSystem, TextureImporterParameters parameters = null, String name = "Texture")
    {
      if (renderSystem is null)
        return null;

      //Since we're creating the texture, avoid having to duplicate any data...
      DDSContainer compressedImage;

      TextureData texData = LoadInternal(input, parameters, out compressedImage, false);

      try
      {
        if (!ImageHelper.ValidateTextureData(ref texData, true))
          return null;

        Texture2D tex = Texture.From(texData, renderSystem) as Texture2D;
        tex.Name = name;

        return tex;
      }
      finally
      {
        if (compressedImage is not null)
          compressedImage.Dispose();
      }
    }

    private static TextureData LoadInternal(Stream input, TextureImporterParameters parameters, out DDSContainer compressedImageContainer, bool copyData = false)
    {
      compressedImageContainer = null;

      //Validate stream
      if (input is null || !input.CanRead)
        return new TextureData(); //Invalid tex data

      //Gather up parameters
      TextureConversionFormat convFormat;
      Color colorKey;
      bool flipImage;
      bool genMipMaps;
      bool resizeNearestPowerOfTwo;
      bool colorKeyEnabled;
      bool preMultiplyAlpha;
      bool isNormalMap;

      ImageHelper.ExtractParameters(parameters, out flipImage, out genMipMaps, out resizeNearestPowerOfTwo, out convFormat, out colorKey, out colorKeyEnabled, out preMultiplyAlpha, out isNormalMap);

      Surface image = null;

      //Check if DDS file
      if (DDSFile.IsDDSFile(input))
      {
        compressedImageContainer = DDSFile.Read(input, DDSFlags.ForceRgb | DDSFlags.No16Bpp);

        if (compressedImageContainer is null)
          return new TextureData(); //Invalid tex data

        //If the surface is a single color image, run it through the pipeline. Otherwise, return the DDS image data as loaded
        if (IsDDSColorData(compressedImageContainer) && compressedImageContainer.MipChains.Count == 1 && compressedImageContainer.MipChains[0].Count == 1)
        {
          MipData mip0 = compressedImageContainer.MipChains[0][0];
          bool isBgra = compressedImageContainer.Format == DXGIFormat.B8G8R8A8_UNorm;

          image = Surface.LoadFromRawData(mip0.Data, mip0.Width, mip0.Height, mip0.RowPitch, isBgra, true);

          compressedImageContainer.Dispose();
          compressedImageContainer = null;
        }
        else
        {
          return ConvertDDSContainer(compressedImageContainer, copyData, out compressedImageContainer);
        }
      }
      else
      {
        //Load the image
        image = Surface.LoadFromStream(input);
        if (image is null)
          return new TextureData(); //Invalid tex data
      }

      try
      {
        bool originalHadAlpha = image.IsTransparent;
        ImageType originalType = image.ImageType;
        int originalBpp = image.BitsPerPixel;

        //Do image flipping IF we specified NOT to flip. FreeImage's origin is lower left (not upper left), so by default images are already flipped.
        if (!flipImage)
          image.FlipVertically();

        //Need to convert to color if: colorKey, premultiply alpha, compress to DXT or if bitmap and not 32 bits. If we don't need to use the compressor,
        //we can do resize and mipmap generation for non-color/non-compressed using just FreeImage
        if (colorKeyEnabled || preMultiplyAlpha || convFormat != TextureConversionFormat.NoChange || (originalType == ImageType.Bitmap && originalBpp != 32))
        {
          //If already a bitmap and 32 bits, no conversion necessary
          if (!(originalType == ImageType.Bitmap && originalBpp == 32))
          {
            if (!image.ConvertTo(ImageConversion.To32Bits))
            {
              System.Diagnostics.Debug.Assert(false, "Image conversion failed.");
              return new TextureData(); //Invalid tex data
            }
          }
        }

        //Do color key remapping
        if (colorKeyEnabled)
        {
          RGBAQuad colorToReplace = new RGBAQuad(colorKey.R, colorKey.G, colorKey.B, colorKey.A);
          RGBAQuad transparentBlack = new RGBAQuad(0, 0, 0, 0);

          image.SwapColors(colorToReplace, transparentBlack, false);
        }

        //Do premultiply alpha
        if (preMultiplyAlpha)
          image.PreMultiplyAlpha();

        bool useCompressor = false;

        //Use compressor if we want to convert to GPU compressed format -or- running other operations and the surface is marked as a normal map. Otherwise, use FreeImage
        //routines to avoid extra copies...
        if ((convFormat == TextureConversionFormat.DXTCompression) || (isNormalMap && (genMipMaps || resizeNearestPowerOfTwo)))
          useCompressor = true;

        //Use compressor only if image is 32-bit color
        if (useCompressor)
        {
          using (Compressor compressor = new Compressor())
          {
            compressor.Input.GenerateMipmaps = genMipMaps;
            if (genMipMaps)
              compressor.Input.MipmapFilter = MipmapFilter.Kaiser;

            compressor.Input.RoundMode = (resizeNearestPowerOfTwo) ? RoundMode.ToNearestPowerOfTwo : RoundMode.None;
            compressor.Input.IsNormalMap = isNormalMap;
            compressor.Input.SetData(image);
            compressor.Compression.Quality = CompressionQuality.Production;
            compressor.Output.OutputHeader = false;

            switch (convFormat)
            {
              case TextureConversionFormat.DXTCompression:
                //Guess the DXT format based on alpha presence. DXT1 can have 1 bit, so either 255 or 0 is valid. Any other value, then DXT5.

                //If image never had alpha to begin with, any alpha channel created during conversion can be ignored
                if (!originalHadAlpha)
                  compressor.Compression.Format = CompressionFormat.DXT1;
                else
                  compressor.Compression.Format = ImageHelper.GuessCompressionFormat(image);

                break;
              default:
                compressor.Compression.Format = CompressionFormat.BGRA;
                compressor.Compression.SetRGBAPixelFormat(); //Output will be RGBA color order always
                break;
            }

            //Execute the processor
            DDSContainer ddsContainer;
            if (!compressor.Process(out ddsContainer))
            {
              System.Diagnostics.Debug.Assert(false, "NVTT compressor failed");
              return new TextureData(); //Invalid tex data
            }

            return ConvertDDSContainer(ddsContainer, copyData, out compressedImageContainer);
          }
        }
        else
        {
          List<Surface> mipmaps = null;

          //Do resize to nearest power of two
          if (resizeNearestPowerOfTwo)
          {
            int width = image.Width;
            int height = image.Height;

            //Only if non-power of 2
            if (!Texture.IsPowerOfTwo(width) || !Texture.IsPowerOfTwo(height))
            {
              width = Texture.NearestPowerOfTwo(width);
              height = Texture.NearestPowerOfTwo(height);

              if (!image.Resize(width, height, ImageFilter.Lanczos3))
              {
                System.Diagnostics.Debug.Assert(false, "Resize failed.");
                return new TextureData(); //Invalid texture data
              }
            }
          }

          //Do generate mipmaps
          if (genMipMaps)
          {
            mipmaps = new List<Surface>(Texture.CalculateMipMapCount(image.Width, image.Height));
            if (!image.GenerateMipMaps(mipmaps, ImageFilter.Lanczos3, true))
            {
              System.Diagnostics.Debug.Assert(false, "Generate Mipmaps failed.");
            }
          }
          else
          {
            mipmaps = new List<Surface>(1);
            mipmaps.Add(image);
          }

          //Create a texture data object from the 1 or more mipmaps
          return ImageHelper.ConvertToTextureData(mipmaps, true);
        }
      }
      finally
      {
        image.Dispose();
      }
    }

    /// <summary>
    /// Converts the DDS container.
    /// </summary>
    /// <param name="ddsContainer">DDs Container to convert</param>
    /// <param name="copyData">True if image data should be copied, false if wrapped. If the data is padded then the data may be copied.</param>
    /// <param name="cachedContainer">The cached container, if the data is wrapped.</param>
    /// <returns>Texture data</returns>
    private static TextureData ConvertDDSContainer(DDSContainer ddsContainer, bool copyData, out DDSContainer cachedContainer)
    {
      cachedContainer = null;

      //If the image has any padding...make sure we don't wrap the data, since we're expecting non-padded. Should be non-padded coming from the compressor...
      if (ImageHelper.HasPadding(ddsContainer) && !copyData)
        copyData = true;

      //Create a texture data object from the output
      if (copyData)
      {
        return ImageHelper.ConvertToTextureData(ddsContainer, false, true);
      }
      else
      {
        cachedContainer = ddsContainer;
        return ImageHelper.ConvertToTextureData(ddsContainer, true, false);
      }
    }

    /// <summary>
    /// Determines whether the dds container has color data.
    /// </summary>
    /// <param name="ddsContainer">The DDS container.</param>
    /// <returns>True if the dds contains color data.</returns>
    private static bool IsDDSColorData(DDSContainer ddsContainer)
    {
      return ddsContainer.Format == DXGIFormat.R8G8B8A8_UNorm || ddsContainer.Format == DXGIFormat.B8G8R8A8_UNorm;
    }
  }
}
