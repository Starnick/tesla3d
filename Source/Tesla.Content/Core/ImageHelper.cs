﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Buffers;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using Assimp.Unmanaged;
using Tesla.Graphics;
using TeximpNet;
using TeximpNet.Compression;
using TeximpNet.DDS;

namespace Tesla.Content
{
  /// <summary>
  /// Helper for image importing and processing.
  /// </summary>
  public static class ImageHelper
  {
    /// <summary>
    /// Creates a <see cref="Bitmap"/> from the color data.
    /// </summary>
    /// <param name="data">Color data</param>
    /// <param name="width">Width of the image.</param>
    /// <param name="height">Height of the image.</param>
    /// <returns>Bitmap containing the image.</returns>
    public static Bitmap ToBitmap(IReadOnlyDataBuffer<Color> data, int width, int height)
    {
      if (data is null || width <= 0 || height <= 0)
        return null;

      Bitmap image = new Bitmap(width, height, PixelFormat.Format32bppArgb);
      BitmapData lockedData = image.LockBits(new System.Drawing.Rectangle(0, 0, width, height), ImageLockMode.WriteOnly, PixelFormat.Format32bppArgb);

      try
      {
        using (MemoryHandle dbPtr = data.Pin())
        {
          int srcPitch = width * Color.SizeInBytes;
          int dstPitch = lockedData.Stride;

          IntPtr srcPtr = dbPtr.AsIntPointer();
          IntPtr dstPtr = lockedData.Scan0;

          //Iterate through each scanline and copy the line from RGBA to BGRA
          for (int y = 0; y < height; y++)
          {
            TeximpNet.ImageHelper.CopyRGBALine(dstPtr, srcPtr, width, true);

            srcPtr += srcPitch;
            dstPtr += dstPitch;
          }
        }
      }
      finally
      {
        image.UnlockBits(lockedData);
      }

      return image;
    }

    /// <summary>
    /// Extracts color data from a <see cref="Bitmap"/>.
    /// </summary>
    /// <param name="data">Bitmap to retrieve color data from.</param>
    /// <param name="allocatorStrategy">Optional allocator strategy.</param>
    /// <returns>Buffer containing color data.</returns>
    public unsafe static DataBuffer<Color> FromBitmap(Bitmap data, MemoryAllocatorStrategy allocatorStrategy = MemoryAllocatorStrategy.DefaultPooled)
    {
      if (data is null)
        return null;

      int width = data.Width;
      int height = data.Height;
      BitmapData lockedData = data.LockBits(new System.Drawing.Rectangle(0, 0, width, height), ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);

      try
      {
        DataBuffer<Color> db = DataBuffer.Create<Color>(width * height, allocatorStrategy);
        using (MemoryHandle dbPtr = db.Pin())
        {
          int srcPitch = lockedData.Stride;
          int dstPitch = width * Color.SizeInBytes;

          IntPtr srcPtr = lockedData.Scan0;
          IntPtr dstPtr = dbPtr.AsIntPointer();

          //Iterate through each scanline and copy the line from BGRA to RGBA
          for (int y = 0; y < height; y++)
          {
            TeximpNet.ImageHelper.CopyRGBALine(dstPtr, srcPtr, width, true);

            srcPtr += srcPitch;
            dstPtr += dstPitch;
          }
        }

        return db;
      }
      finally
      {
        data.UnlockBits(lockedData);
      }
    }

    /// <summary>
    /// Generates mipmaps using <see cref="System.Drawing.Graphics"/>.
    /// </summary>
    /// <param name="mip0">Color data for the first mip level.</param>
    /// <param name="mip0Width">Width of the first mip.</param>
    /// <param name="mip0Height">Height of the first mip.</param>
    /// <param name="allocatorStrategy">Optional allocator strategy.</param>
    /// <returns>Array of mipmaps, including the first mip level.</returns>
    public static DataBuffer<Color>[] GenerateMipChain(DataBuffer<Color> mip0, int mip0Width, int mip0Height, MemoryAllocatorStrategy allocatorStrategy = MemoryAllocatorStrategy.DefaultPooled)
    {
      if (mip0 is null || mip0Width <= 0 || mip0Height <= 0)
        return null;

      int numMips = Texture.CalculateMipMapCount(mip0Width, mip0Height);
      DataBuffer<Color>[] mipChain = new DataBuffer<Color>[numMips];
      mipChain[0] = mip0;

      Bitmap mip0Image = ToBitmap(mip0, mip0Width, mip0Height);

      for (int i = 1; i < numMips; i++)
      {
        int newWidth = mip0Width;
        int newHeight = mip0Height;

        Texture.CalculateMipLevelDimensions(i, ref newWidth, ref newHeight);
        using (Bitmap image = new Bitmap(newWidth, newHeight))
        {
          using (System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(image))
          {
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
            g.DrawImage(mip0Image, new System.Drawing.Rectangle(0, 0, newWidth, newHeight));

            mipChain[i] = FromBitmap(image, allocatorStrategy);
          }
        }
      }

      return mipChain;
    }

    /// <summary>
    /// Generates mipmaps for the specified image by using <see cref="SpriteBatch"/> to draw successfully smaller images.
    /// </summary>
    /// <param name="renderContext">Render context to execution drawing on the GPU.</param>
    /// <param name="mip0">Color data for the first mip level.</param>
    /// <param name="mip0Width">Width of the first mip.</param>
    /// <param name="mip0Height">Height of the first mip.</param>
    /// <param name="sampler">Optional sampler state, if null this will be <see cref="SamplerState.LinearWrap"/>.</param>
    /// <param name="allocatorStrategy">Optional allocator strategy.</param>
    /// <returns>Array of mipmaps, including the first mip level.</returns>
    public static DataBuffer<Color>[] GenerateMipChain(IRenderContext renderContext, DataBuffer<Color> mip0, int mip0Width, int mip0Height, SamplerState sampler = null, MemoryAllocatorStrategy allocatorStrategy = MemoryAllocatorStrategy.DefaultPooled)
    {
      if (renderContext is null || mip0Width <= 0 || mip0Height <= 0 || mip0 == null)
        return null;

      int numMips = Texture.CalculateMipMapCount(mip0Width, mip0Height);
      DataBuffer<Color>[] mipChain = new DataBuffer<Color>[numMips];
      mipChain[0] = mip0;

      //Create a GPU texture for the image
      using (Texture2D texture = new Texture2D(renderContext.RenderSystem, mip0Width, mip0Height, TextureOptions.Init(mip0, SurfaceFormat.Color, ResourceUsage.Immutable)))
      {
        //Create a single render target, the size of the original image
        using (RenderTarget2D target = new RenderTarget2D(renderContext.RenderSystem, mip0Width, mip0Height, SurfaceFormat.Color, DepthFormat.None, MSAADescription.Default))
        {
          Viewport oldVp = renderContext.Camera.Viewport;
          Viewport newVp = oldVp;

          //To be safe, rely on getting predefined from the render system, this might be used if the engine isn't initialized
          BlendState bs = renderContext.RenderSystem.PredefinedBlendStates.Opaque;
          RasterizerState rs = renderContext.RenderSystem.PredefinedRasterizerStates.CullNone;
          DepthStencilState dss = renderContext.RenderSystem.PredefinedDepthStencilStates.None;

          if (sampler is null)
            sampler = renderContext.RenderSystem.PredefinedSamplerStates.LinearWrap;

          //Create a spritebatch that can hold a single sprite, then for each miplevel draw the original image to the render target and resolve it
          using (SpriteBatch spriteBatch = new SpriteBatch(renderContext.RenderSystem, 1))
          {
            for (int i = 1; i < numMips; i++)
            {
              int newWidth = mip0Width;
              int newHeight = mip0Height;
              Texture.CalculateMipLevelDimensions(i, ref newWidth, ref newHeight);

              newVp.Width = newWidth;
              newVp.Height = newHeight;

              renderContext.Camera.Viewport = newVp;
              renderContext.SetRenderTarget(target);

              spriteBatch.Begin(renderContext, SpriteSortMode.Immediate, bs, rs, sampler, dss);
              spriteBatch.Draw(texture, new Rectangle(0, 0, newWidth, newHeight), Color.White);
              spriteBatch.End();

              renderContext.SetRenderTarget(null);

              DataBuffer<Color> mipData = DataBuffer.Create<Color>(newWidth * newHeight, allocatorStrategy);
              target.GetData<Color>(mipData.Span, new ResourceRegion2D(0, newWidth, 0, newHeight));

              mipChain[i] = mipData;
            }
          }

          renderContext.Camera.Viewport = oldVp;
        }
      }

      return mipChain;
    }

    /// <summary>
    /// Gets the specified mip surface of the texture as color data (first mip if no sub-level is specified).
    /// </summary>
    /// <param name="renderContext">Render context to execution drawing on the GPU.</param>
    /// <param name="tex">Texture to convert data from.</param>
    /// <param name="sampler">Optional sampler state, if null this will be <see cref="SamplerState.LinearWrap"/>.</param>
    /// <param name="allocatorStrategy">Optional allocator strategy.</param>
    /// <param name="subResource">Mip level, if not specified the first mip.</param>
    /// <returns>Color data.</returns>
    public static DataBuffer<Color> ConvertToColor(IRenderContext renderContext, Texture2D tex, SamplerState sampler = null, MemoryAllocatorStrategy allocatorStrategy = MemoryAllocatorStrategy.DefaultPooled, SubResourceAt subResource = default)
    {
      int width = tex.Width;
      int height = tex.Height;
      subResource.CalculateMipLevelDimensions(ref width, ref height);
      DataBuffer<Color> colorData = DataBuffer.Create<Color>(width * height, allocatorStrategy);

      if (tex.Format == SurfaceFormat.Color)
      {
        tex.GetData<Color>(colorData, subResource);
        return colorData;
      }

      using (RenderTarget2D target = new RenderTarget2D(renderContext.RenderSystem, width, height, SurfaceFormat.Color, DepthFormat.None, MSAADescription.Default))
      {
        Viewport oldVp = renderContext.Camera.Viewport;
        Viewport newVp = oldVp;

        //To be safe, rely on getting predefined from the render system, this might be used if the engine isn't initialized
        BlendState bs = renderContext.RenderSystem.PredefinedBlendStates.Opaque;
        RasterizerState rs = renderContext.RenderSystem.PredefinedRasterizerStates.CullNone;
        DepthStencilState dss = renderContext.RenderSystem.PredefinedDepthStencilStates.None;

        if (sampler is null)
          sampler = renderContext.RenderSystem.PredefinedSamplerStates.LinearWrap;

        //Create a spritebatch that can hold a single sprite, then for each miplevel draw the original image to the render target and resolve it
        using (SpriteBatch spriteBatch = new SpriteBatch(renderContext.RenderSystem, 1))
        {
          newVp.Width = width;
          newVp.Height = height;

          renderContext.Camera.Viewport = newVp;
          renderContext.SetRenderTarget(target);

          spriteBatch.Begin(renderContext, SpriteSortMode.Immediate, bs, rs, sampler, dss);
          spriteBatch.Draw(tex, new Rectangle(0, 0, width, height), Color.White);
          spriteBatch.End();

          renderContext.SetRenderTarget(null);

          target.GetData<Color>(colorData.Span, new ResourceRegion2D(0, width, 0, height));
        }

        renderContext.Camera.Viewport = oldVp;
      }

      return colorData;
    }

    /// <summary>
    /// Calls <see cref="TextureData.Validate"/> and optionally, if not valid, ensures all data buffers are disposed of.
    /// </summary>
    /// <param name="texData">Texture data object.</param>
    /// <param name="disposeIfInvalid">True if data should be disposed if invalid.</param>
    /// <returns>True if the texture data is valid.</returns>
    public static bool ValidateTextureData(ref TextureData texData, bool disposeIfInvalid = true)
    {
      bool isValid = texData.Validate();

      //If requested, dispose of any resources if not valid (may be in a half-way state)
      if (!isValid && disposeIfInvalid)
      {
        if (texData.MipChains != null)
        {
          foreach (TextureData.MipChain mipChain in texData.MipChains)
          {
            if (mipChain == null)
              continue;

            foreach (TextureData.MipSurface mipSurface in mipChain)
            {
              if (mipSurface.Data != null)
                mipSurface.Data.Dispose();
            }

            mipChain.Clear();
          }

          texData.MipChains.Clear();
        }

        texData = new TextureData();
      }

      return isValid;
    }

    /// <summary>
    /// Safely extract image parameters from an unknown <see cref="ImporterParameters"/> object. If null or not an <see cref="TextureImporterParameters"/>,
    /// then default values are set.
    /// </summary>
    /// <param name="parameters">Parameter object to extract from.</param>
    /// <param name="flipImage">True to flip the image vertically, default is false.</param>
    /// <param name="genMipMaps">True to generate mipmaps, default is false.</param>
    /// <param name="resizePowerOfTwo">If image is not power-of-two, resizes the image to the nearest power-of-two size. Default is false.</param>
    /// <param name="convFormat">Specifies which format the texture should be converted to. Default is <see cref="TextureConversionFormat.NoChange"/>.</param>
    /// <param name="colorKey">Specifies a color key, default is <see cref="Color.Magenta"/>.</param>
    /// <param name="colorKeyEnabled">Specifies if color key swapping is enabled or not. Default is false.</param>
    /// <param name="preMultiplyAlpha">Specifies if pixel colors should be pre-multiplified with alpha values. Default is false.</param>
    /// <param name="isNormalMap">True if the texture should be treated as a normal map, false if otherwise.</param>
    public static void ExtractParameters(ImporterParameters parameters, out bool flipImage, out bool genMipMaps, out bool resizePowerOfTwo,
        out TextureConversionFormat convFormat, out Color colorKey, out bool colorKeyEnabled, out bool preMultiplyAlpha, out bool isNormalMap)
    {
      flipImage = false;
      genMipMaps = false;
      resizePowerOfTwo = false;
      convFormat = TextureConversionFormat.NoChange;
      colorKey = Color.Magenta;
      colorKeyEnabled = false;
      preMultiplyAlpha = false;
      isNormalMap = false;

      TextureImporterParameters importParams = parameters as TextureImporterParameters;
      if (importParams == null)
        return;

      flipImage = importParams.FlipImage;
      genMipMaps = importParams.GenerateMipMaps;
      resizePowerOfTwo = importParams.ResizePowerOfTwo;
      convFormat = importParams.TextureFormat;
      colorKey = importParams.ColorKey;
      colorKeyEnabled = importParams.ColorKeyEnabled;
      preMultiplyAlpha = importParams.PreMultiplyAlpha;
      isNormalMap = importParams.IsNormalMap;
    }

    /// <summary>
    /// Determine the block compression format for the surface. If no alpha/1-bit alpha, then <see cref="SurfaceFormat.DXT1"/> if any other alpha value,
    /// then <see cref="SurfaceFormat.DXT5"/>. The surface is expected to be in a RGBA/BGRA format if it has any transparency.
    /// </summary>
    /// <param name="surface">Image to guess format for.</param>
    /// <returns>Compression format to use for the image.</returns>
    public static unsafe CompressionFormat GuessCompressionFormat(Surface surface)
    {
      //If image has no transparency, use the DXT1 format that ignores alpha
      if (surface == null || !surface.IsTransparent)
        return CompressionFormat.DXT1;

      //Sanity check...
      if (surface.ImageType != ImageType.Bitmap || surface.ColorType != ImageColorType.RGBA || surface.BitsPerPixel != 32)
      {
        System.Diagnostics.Debug.Assert(false, "Expected a 32-bit BGRA bitmap.");
        return CompressionFormat.DXT1;
      }

      //Scan the pixel data to determine if has explicit alpha (255 or 0) or fractional alpha (any other value). Fortunately color order doesn't matter for alpha.
      int width = surface.Width;
      int height = surface.Height;

      for (int i = 0; i < height; i++)
      {
        IntPtr scanline = surface.GetScanLine(i);
        RGBAQuad* scanlineRaw = (RGBAQuad*) scanline.ToPointer(); //Reinterpret to something nice

        for (int x = 0; x < width; x++)
        {
          byte alpha = scanlineRaw[width].A;

          //If any other value...we know we need to use DXT5 and can early out
          if (alpha != 255 && alpha != 0)
            return CompressionFormat.DXT5;
        }
      }

      //Otherwise we know there is 1-bit alpha...
      return CompressionFormat.DXT1a;
    }

    /// <summary>
    /// Takes FreeImage <see cref="Surface"/> data and creates a <see cref="TextureData"/> object. Data is always copied (FreeImage almost always is BGRA order).
    /// </summary>
    /// <param name="freeImageSurfaces">List of all mip surfaces for a single 2D texture.</param>
    /// <param name="disposeSurfaces">True to dispose of all input surfaces, false if not to dispose.</param>
    /// <returns>Texture data object containing image data.</returns>
    public static TextureData ConvertToTextureData(IList<Surface> freeImageSurfaces, bool disposeSurfaces = true)
    {
      return ConvertToTextureData(freeImageSurfaces, (freeImageSurfaces == null) ? 0 : freeImageSurfaces.Count, false, disposeSurfaces);
    }

    /// <summary>
    /// Takes FreeImage <see cref="Surface"/> data and creates a <see cref="TextureData"/> object. Data is always copied (FreeImage almost always is BGRA order). This method
    /// allows the creation of array or cube texture data by using the specified mip count to split up the array data.
    /// </summary>
    /// <param name="surfaces">List of all mip/array surfaces. E.g. [Face0_Mip0, Face0_Mip1, Face0_Mip2....Face1_Mip0...]</param>
    /// <param name="mipCount">Number of mipmaps per face.</param>
    /// <param name="isTexCube">Hint for if this is a cube map and not standard texture array.</param>
    /// <param name="disposeSurfaces">True to dispose of all input surfaces, false if not to dispose.</param>
    /// <param name="allocatorStrategy">Optional allocator strategy.</param>
    /// <returns>Texture data object containing image data.</returns>
    public static TextureData ConvertToTextureData(IList<Surface> surfaces, int mipCount, bool isTexCube = false, bool disposeSurfaces = true, MemoryAllocatorStrategy allocatorStrategy = MemoryAllocatorStrategy.DefaultPooled)
    {
      int arrayCount;
      Tesla.Graphics.TextureType texType;

      if (!DetermineTextureInfo(surfaces, mipCount, isTexCube, out arrayCount, out texType))
        return new TextureData(); //Invalid texture data

      SurfaceFormat format = SurfaceFormat.Color;
      ImageConversion? convFormat = null;

      Surface firstSurface = surfaces[0];

      //Might have to do conversions or expansion of formats
      switch (firstSurface.ImageType)
      {
        case ImageType.Bitmap:
          if (firstSurface.BitsPerPixel != 32)
            convFormat = ImageConversion.To32Bits;
          break;
        case ImageType.Float:
          format = SurfaceFormat.Single;
          break;
        case ImageType.RGBAF:
          format = SurfaceFormat.Vector4;
          break;
        case ImageType.RGBF:
          format = SurfaceFormat.Vector3;
          break;
        case ImageType.RGBA16:
          format = SurfaceFormat.RGBA64;
          break;
        case ImageType.RGB16:
          format = SurfaceFormat.RGBA64;
          convFormat = ImageConversion.ToRGBA16; //Expand to 4 components, if we finish our half type, we can just make this a Half3
          break;
        default:
          convFormat = ImageConversion.To32Bits;
          break;
      }

      List<TextureData.MipChain> mipChains = new List<TextureData.MipChain>(arrayCount);
      TextureData.MipChain mips = new TextureData.MipChain(mipCount);

      //Iterate through each image, split up into 1 or more mipchains
      for (int i = 0, currMipCount = 0; i < surfaces.Count; i++, currMipCount++)
      {
        Surface imageData = surfaces[i];

        //Start a new mipchain if necessary
        if (currMipCount == mipCount)
        {
          mipChains.Add(mips);
          mips = new TextureData.MipChain(mipCount);
        }

        TextureData.MipSurface mip;
        mip.Width = (uint) imageData.Width;
        mip.Height = (uint) imageData.Height;
        mip.Depth = 1;
        mip.Data = null;

        Surface imageToCopyFrom = imageData;
        if (convFormat.HasValue)
        {
          if (!disposeSurfaces)
            imageToCopyFrom = imageData.Clone();

          imageToCopyFrom.ConvertTo(convFormat.Value);
        }

        //We configured the format earlier, so one of these cases should be valid
        switch (format)
        {
          case SurfaceFormat.Color:
            mip.Data = DataBuffer.Create<Color>(imageData.Width * imageData.Height, allocatorStrategy);
            CopyColor(mip.Data, imageToCopyFrom);
            break;
          case SurfaceFormat.Single:
            mip.Data = DataBuffer.Create<float>(imageData.Width * imageData.Height, allocatorStrategy);
            CopyFloat(mip.Data, imageToCopyFrom);
            break;
          case SurfaceFormat.Vector4:
            mip.Data = DataBuffer.Create<Vector4>(imageData.Width * imageData.Height, allocatorStrategy);
            CopyVector4(mip.Data, imageToCopyFrom);
            break;
          case SurfaceFormat.Vector3:
            mip.Data = DataBuffer.Create<Vector3>(imageData.Width * imageData.Height, allocatorStrategy);
            CopyVector3(mip.Data, imageToCopyFrom);
            break;
          case SurfaceFormat.RGBA64:
            mip.Data = DataBuffer.Create<byte>(imageData.Width * imageData.Height * format.SizeInBytes(), allocatorStrategy);
            CopyHalfVector4(mip.Data, imageToCopyFrom);
            break;
        }

        //Add to mipchain
        System.Diagnostics.Debug.Assert(mip.Data != null);
        mips.Add(mip);

        //Copy data, free image surfaces may be padded...we also need to account for BGRA format
        if (convFormat.HasValue)
          imageToCopyFrom.Dispose();
      }

      //Make sure to add the last mipchain
      mipChains.Add(mips);

      //Dispose if necessary
      if (disposeSurfaces)
      {
        BufferHelper.DisposeCollection<Surface>(surfaces);
        surfaces.Clear();
      }

      //Populate a texture data with the mipmaps
      TextureData texData;
      texData.Width = mips[0].Width;
      texData.Height = mips[0].Height;
      texData.Depth = mips[0].Depth;
      texData.Format = format;
      texData.TextureType = texType;
      texData.MipChains = mipChains;

      return texData;
    }

    /// <summary>
    /// Takes output from the NVTT <see cref="Compressor"/> and creates a <see cref="TextureData"/> object, either copying or wrapping the data. If wrapping
    /// the data, make sure the DDS mip data is non-padded.
    /// </summary>
    /// <param name="ddsContainer">Container of images that were outputted from a compressor.</param>
    /// <param name="wrapImageData">True to wrap the image data, false to copy.</param>
    /// <param name="disposeSurfaces">True to dispose of all input surfaces (unless if wrapped), false if not to dispose.</param>
    /// <param name="allocatorStrategy">Optional allocator strategy.</param>
    /// <returns>Texture data object containing image data.</returns>
    public static TextureData ConvertToTextureData(DDSContainer ddsContainer, bool wrapImageData = true, bool disposeSurfaces = true, MemoryAllocatorStrategy allocatorStrategy = MemoryAllocatorStrategy.DefaultPooled)
    {
      //Image data from NVTT compressor can be 2D, 3D, Cube, or array 2D, so need to determine this based on the contents of the surfaces. For each arra index (or face),
      //all the mip levels are expected to be grouped together. E.g. [Face0_Mip0, Face0_Mip1, Face0_Mip2....Face1_Mip0...]
      int arrayCount, mipCount;
      Tesla.Graphics.TextureType texType;

      if (!DetermineTextureInfo(ddsContainer, out arrayCount, out mipCount, out texType))
        return new TextureData(); //Invalid texture data

      //When creating the data buffers, specify to strongly type with the color struct
      bool stronglyTypeColor = false;
      bool stronglyTypeBgraColor = false;
      SurfaceFormat format = SurfaceFormat.Color;

      switch (ddsContainer.Format)
      {
        case DXGIFormat.R8G8B8A8_UNorm:
          stronglyTypeColor = true;
          format = SurfaceFormat.Color;
          break;
        case DXGIFormat.B8G8R8A8_UNorm:
          stronglyTypeBgraColor = true;
          format = SurfaceFormat.BGRColor;
          break;
        case DXGIFormat.BC1_UNorm:
          format = SurfaceFormat.DXT1;
          break;
        case DXGIFormat.BC2_UNorm:
          format = SurfaceFormat.DXT3;
          break;
        case DXGIFormat.BC3_UNorm:
          format = SurfaceFormat.DXT5;
          break;
        default:
          System.Diagnostics.Debug.Assert(false, "Unsupported compressed format");
          return new TextureData(); //Invalid texture data
      }

      List<TextureData.MipChain> mipChains = new List<TextureData.MipChain>(arrayCount);

      List<MipChain> ddsMipChains = ddsContainer.MipChains;

      //Iterate through each mipchain
      for (int arrayIndex = 0; arrayIndex < ddsMipChains.Count; arrayIndex++)
      {
        MipChain ddsMipChain = ddsMipChains[arrayIndex];
        TextureData.MipChain mips = new TextureData.MipChain(mipCount);
        mipChains.Add(mips);

        //Iterate through each image
        for (int mipIndex = 0; mipIndex < ddsMipChain.Count; mipIndex++)
        {
          MipData imageData = ddsMipChain[mipIndex];

          TextureData.MipSurface mip;
          mip.Width = (uint) imageData.Width;
          mip.Height = (uint) imageData.Height;
          mip.Depth = (texType == Tesla.Graphics.TextureType.Texture3D) ? 1 : (uint) imageData.Depth;

          //Either copy or wrap the incoming image data, image data is expected to not have padding
          if (!wrapImageData)
          {
            if (stronglyTypeColor)
              mip.Data = DataBuffer.Create<Color>((int) (mip.Width * mip.Height * mip.Depth), allocatorStrategy);
            else if (stronglyTypeBgraColor)
              mip.Data = DataBuffer.Create<ColorBGRA>((int) (mip.Width * mip.Height * mip.Depth), allocatorStrategy);
            else
              mip.Data = DataBuffer.Create<byte>(imageData.SizeInBytes, allocatorStrategy);

            mip.Data.Bytes.CopyFrom(imageData.Data, imageData.SizeInBytes);
          }
          else
          {
            if (stronglyTypeColor)
              mip.Data = DataBuffer.Wrap<Color>(imageData.Data, imageData.SizeInBytes);
            else if (stronglyTypeBgraColor)
              mip.Data = DataBuffer.Wrap<ColorBGRA>(imageData.Data, imageData.SizeInBytes);
            else
              mip.Data = DataBuffer.Wrap<byte>(imageData.Data, imageData.SizeInBytes);
          }

          mips.Add(mip);
        }
      }

      //Dispose if necessary
      if (!wrapImageData && disposeSurfaces)
        ddsContainer.Dispose();

      TextureData.MipSurface mip0 = mipChains[0][0];

      //Populate a texture data with the mipmaps
      TextureData texData;
      texData.Width = mip0.Width;
      texData.Height = mip0.Height;
      texData.Depth = mip0.Depth;
      texData.Format = format;
      texData.TextureType = texType;
      texData.MipChains = mipChains;

      return texData;
    }

    /// <summary>
    /// Determines if the images have padding.
    /// </summary>
    /// <param name="ddsContainer">DDS container</param>
    /// <returns>True if the images have padding, false if otherwise.</returns>
    public static bool HasPadding(DDSContainer ddsContainer)
    {
      if (ddsContainer == null || ddsContainer.MipChains.Count == 0 || ddsContainer.MipChains[0].Count == 0)
        return false;

      MipData mip0 = ddsContainer.MipChains[0][0];

      int rowPitch, slicePitch, widthCount, heightCount;
      TeximpNet.ImageHelper.ComputePitch(ddsContainer.Format, mip0.Width, mip0.Height, out rowPitch, out slicePitch, out widthCount, out heightCount);

      if (mip0.RowPitch == rowPitch && mip0.SlicePitch == slicePitch)
        return false;

      return true;
    }

    private static bool DetermineTextureInfo(IList<Surface> surfaces, int mipCount, bool isCube, out int arrayCount, out Tesla.Graphics.TextureType texType)
    {
      arrayCount = 1; //Always at least one 
      texType = Tesla.Graphics.TextureType.Texture2D;

      if (surfaces == null || surfaces.Count == 0 || mipCount == 0)
      {
        System.Diagnostics.Debug.Assert(false, "No surfaces to copy.");
        return false;
      }

      //Iterate through each surface, increment array count after we look at the specified # of mip maps
      for (int i = 0, currMipCount = 0; i < surfaces.Count; i++, currMipCount++)
      {
        Surface imageData = surfaces[i];

        //Crossed a face boundary...
        if (currMipCount == mipCount)
        {
          arrayCount++;
          currMipCount = 0;
        }
      }

      if ((mipCount * arrayCount) != surfaces.Count)
      {
        System.Diagnostics.Debug.Assert(false, "Expecting # of surfaces = mipCount * arrayCount");
        return false;
      }

      if (isCube && arrayCount != 6)
      {
        System.Diagnostics.Debug.Assert(false, "Expecting Six faces for cubemaps.");
        return false;
      }

      if (arrayCount > 1)
        texType = (isCube) ? Tesla.Graphics.TextureType.TextureCube : Tesla.Graphics.TextureType.Texture2DArray;

      return true;
    }

    private static bool DetermineTextureInfo(DDSContainer ddsImage, out int arrayCount, out int mipCount, out Tesla.Graphics.TextureType texType)
    {
      arrayCount = 1; //Always at least one 
      mipCount = 0;
      texType = Tesla.Graphics.TextureType.Texture2D;

      if (ddsImage == null || ddsImage.MipChains.Count == 0 || ddsImage.MipChains[0].Count == 0)
      {
        System.Diagnostics.Debug.Assert(false, "No surfaces to copy.");
        return false;
      }

      if (!ddsImage.Validate())
        return false;

      arrayCount = ddsImage.MipChains.Count;
      mipCount = ddsImage.MipChains[0].Count;

      switch (ddsImage.Dimension)
      {
        case TeximpNet.DDS.TextureDimension.Cube:
          texType = Graphics.TextureType.TextureCube;
          break;
        case TeximpNet.DDS.TextureDimension.Three:
          texType = Graphics.TextureType.Texture3D;
          break;
        case TeximpNet.DDS.TextureDimension.Two:
          if (arrayCount > 1)
            texType = Graphics.TextureType.Texture2DArray;
          else
            texType = Graphics.TextureType.Texture2D;
          break;
        case TeximpNet.DDS.TextureDimension.One:
          if (arrayCount > 1)
            texType = Graphics.TextureType.Texture1DArray;
          else
            texType = Graphics.TextureType.Texture1D;
          break;
      }

      return true;
    }

    #region FreeImage Copy methods

    //TODO: We don't have a 16-bit float fully fleshed out (Half), when we do, we can just use that!
    [StructLayout(LayoutKind.Sequential)]
    private struct RGBA16
    {
      private static int s_sizeInBytes = BufferHelper.SizeOf<RGBA16>();

      public ushort R;
      public ushort G;
      public ushort B;
      public ushort A;

      public static int SizeInBytes
      {
        get
        {
          return s_sizeInBytes;
        }
      }
    }

    //Copy from BGRA or RGBA order to our Color type
    private static void CopyColor(IDataBuffer dest, Surface src)
    {
      int texelSize = Color.SizeInBytes;

      int width = src.Width;
      int height = src.Height;
      int dstPitch = width * texelSize;
      bool swizzle = Surface.IsBGRAOrder;

      using (MemoryHandle dbPtr = dest.Pin())
      {
        IntPtr dstPtr = dbPtr.AsIntPointer();

        int pitch = Math.Min(src.Pitch, dstPitch);

        unsafe
        {
          if (swizzle)
          {
            //For each scanline...
            for (int row = 0; row < height; row++)
            {
              Color* dPtr = (Color*) dstPtr.ToPointer();
              Color* sPtr = (Color*) src.GetScanLine(row).ToPointer();

              //Copy each pixel, swizzle components...
              for (int count = 0; count < pitch; count += texelSize)
              {
                Color v = *(sPtr++);
                byte temp = v.R;
                v.R = v.B;
                v.B = temp;

                *(dPtr++) = v;
              }

              //Advance to next scanline...
              dstPtr += dstPitch;
            }
          }
          else
          {
            //For each scanline...
            for (int row = 0; row < height; row++)
            {
              IntPtr sPtr = src.GetScanLine(row);

              //Copy entirely...
              BufferHelper.CopyMemory(dstPtr, sPtr, pitch);

              //Advance to next scanline...
              dstPtr += dstPitch;
            }
          }
        }
      }
    }

    //Copy float
    private static void CopyFloat(IDataBuffer dest, Surface src)
    {
      int texelSize = sizeof(float);

      int width = src.Width;
      int height = src.Height;
      int dstPitch = width * texelSize;

      using (MemoryHandle dbPtr = dest.Pin())
      {
        IntPtr dstPtr = dbPtr.AsIntPointer();

        int pitch = Math.Min(src.Pitch, dstPitch);

        unsafe
        {
          //For each scanline...
          for (int row = 0; row < height; row++)
          {
            IntPtr sPtr = src.GetScanLine(row);

            //Copy entirely...
            BufferHelper.CopyMemory(dstPtr, sPtr, pitch);

            //Advance to next scanline...
            dstPtr += dstPitch;
          }
        }
      }
    }

    //Copy 3-component to our Vector3 type, flip R and B if necessary
    private static void CopyVector3(IDataBuffer dest, Surface src)
    {
      int texelSize = Vector4.SizeInBytes;

      int width = src.Width;
      int height = src.Height;
      int dstPitch = width * texelSize;
      bool swizzle = !Surface.IsBGRAOrder; //Seems like float channels are in XYZ order??

      using (MemoryHandle dbPtr = dest.Pin())
      {
        IntPtr dstPtr = dbPtr.AsIntPointer();

        int pitch = Math.Min(src.Pitch, dstPitch);

        unsafe
        {
          if (swizzle)
          {
            //For each scanline...
            for (int row = 0; row < height; row++)
            {
              Vector3* dPtr = (Vector3*) dstPtr.ToPointer();
              Vector3* sPtr = (Vector3*) src.GetScanLine(row).ToPointer();

              //Copy each pixel, swizzle components...
              for (int count = 0; count < pitch; count += texelSize)
              {
                Vector3 v = *(sPtr++);
                float temp = v.X;
                v.X = v.Z;
                v.Z = temp;

                *(dPtr++) = v;
              }

              //Advance to next scanline...
              dstPtr += dstPitch;
            }
          }
          else
          {
            //For each scanline...
            for (int row = 0; row < height; row++)
            {
              IntPtr sPtr = src.GetScanLine(row);

              //Copy entirely...
              BufferHelper.CopyMemory(dstPtr, sPtr, pitch);

              //Advance to next scanline...
              dstPtr += dstPitch;
            }
          }
        }
      }
    }

    //Copy 4-component to our Vector4 type, flip R and B if necessary
    private static void CopyVector4(IDataBuffer dest, Surface src)
    {
      int texelSize = Vector4.SizeInBytes;

      int width = src.Width;
      int height = src.Height;
      int dstPitch = width * texelSize;
      bool swizzle = !Surface.IsBGRAOrder; //Seems like float channels are in XYZ order??

      using (MemoryHandle dbPtr = dest.Pin())
      {
        IntPtr dstPtr = dbPtr.AsIntPointer();

        int pitch = Math.Min(src.Pitch, dstPitch);

        unsafe
        {
          if (swizzle)
          {
            //For each scanline...
            for (int row = 0; row < height; row++)
            {
              Vector4* dPtr = (Vector4*) dstPtr.ToPointer();
              Vector4* sPtr = (Vector4*) src.GetScanLine(row).ToPointer();

              //Copy each pixel, swizzle components...
              for (int count = 0; count < pitch; count += texelSize)
              {
                Vector4 v = *(sPtr++);
                float temp = v.X;
                v.X = v.Z;
                v.Z = temp;

                *(dPtr++) = v;
              }

              //Advance to next scanline...
              dstPtr += dstPitch;
            }
          }
          else
          {
            //For each scanline...
            for (int row = 0; row < height; row++)
            {
              IntPtr sPtr = src.GetScanLine(row);

              //Copy entirely...
              BufferHelper.CopyMemory(dstPtr, sPtr, pitch);

              //Advance to next scanline...
              dstPtr += dstPitch;
            }
          }
        }
      }
    }

    //Copy half vector4, flip R and B if necessary
    private static void CopyHalfVector4(IDataBuffer dest, Surface src)
    {
      int texelSize = RGBA16.SizeInBytes;

      int width = src.Width;
      int height = src.Height;
      int dstPitch = width * texelSize;
      bool swizzle = !Surface.IsBGRAOrder; //Seems like float channels are in XYZ order??

      using (MemoryHandle dbPtr = dest.Pin())
      {
        IntPtr dstPtr = dbPtr.AsIntPointer();

        int pitch = Math.Min(src.Pitch, dstPitch);

        unsafe
        {
          if (swizzle)
          {
            //For each scanline...
            for (int row = 0; row < height; row++)
            {
              RGBA16* dPtr = (RGBA16*) dstPtr.ToPointer();
              RGBA16* sPtr = (RGBA16*) src.GetScanLine(row).ToPointer();

              //Copy each pixel, swizzle components...
              for (int count = 0; count < pitch; count += texelSize)
              {
                RGBA16 v = *(sPtr++);
                ushort temp = v.R;
                v.R = v.B;
                v.B = temp;

                *(dPtr++) = v;
              }

              //Advance to next scanline...
              dstPtr += dstPitch;
            }
          }
          else
          {
            //For each scanline...
            for (int row = 0; row < height; row++)
            {
              IntPtr sPtr = src.GetScanLine(row);

              //Copy entirely...
              BufferHelper.CopyMemory(dstPtr, sPtr, pitch);

              //Advance to next scanline...
              dstPtr += dstPitch;
            }
          }
        }
      }
    }

    #endregion
  }
}
