﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Buffers;
using System.IO;
using Tesla.Graphics;
using TeximpNet;

namespace Tesla.Content
{
  /// <summary>
  /// Writes out a <see cref="Texture2D"/> resource to various image formats supported by Teximp.
  /// </summary>
  public class TeximpTexture2DExternalWriter : IExternalReferenceWriter
  {
    protected TeximpTexture2DExporter m_exporter;

    /// <inheritdoc />
    public Type TargetType { get { return typeof(Texture2D); } }

    /// <inheritdoc />
    public string ResourceExtension { get { return m_exporter.Extension; } }

    /// <summary>
    /// Constructs a new <see cref="TeximpTexture2DExternalWriter"/>.
    /// </summary>
    /// <param name="exporter">Underlying exporter that saves the image.</param>
    public TeximpTexture2DExternalWriter(TeximpTexture2DExporter exporter)
    {
      m_exporter = exporter;
    }

    /// <summary>
    /// Creates a PNG external file writer.
    /// </summary>
    /// <returns>External writer.</returns>
    public static TeximpTexture2DExternalWriter CreatePngWriter()
    {
      return new TeximpTexture2DExternalWriter(new PngTextureExporter());
    }

    /// <summary>
    /// Creates a JPG external file writer.
    /// </summary>
    /// <returns>External writer.</returns>
    public static TeximpTexture2DExternalWriter CreateJpgWriter()
    {
      return new TeximpTexture2DExternalWriter(new JpgTextureExporter());
    }

    /// <inheritdoc />
    /// <exception cref="ArgumentNullException">Thrown if the output resource file, external handler or savable is null.</exception>
    /// <exception cref="ArgumentException">Thrown if the output resource file is not the same as what the writer expects.</exception>
    public void WriteSavable<T>(IResourceFile outputResourceFile, IExternalReferenceHandler externalHandler, T value) where T : class, ISavable
    {
      ArgumentNullException.ThrowIfNull(outputResourceFile, nameof(outputResourceFile));
      ArgumentNullException.ThrowIfNull(externalHandler, nameof(externalHandler));
      ArgumentNullException.ThrowIfNull(value, nameof(value));

      Texture2D tex = value as Texture2D;
      if (tex is null)
        throw new InvalidCastException(StringLocalizer.Instance.GetLocalizedString("TypeMismatch", TargetType.Name, value.GetType().Name));

      if (outputResourceFile.Extension != ResourceExtension)
        throw new ArgumentException(StringLocalizer.Instance.GetLocalizedString("InvalidResourceExtension", ResourceExtension, outputResourceFile.Extension));

      using (Stream output = outputResourceFile.OpenWrite())
        m_exporter.Save(output, tex);
    }
  }
}
