﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System.Buffers;
using System.IO;
using Tesla.Graphics;
using TeximpNet;

namespace Tesla.Content
{
  /// <summary>
  /// Base class for writing Texture2D to image formats supported by Teximp.
  /// </summary>
  public class TeximpTexture2DExporter : ResourceExporter<Texture2D>
  {
    protected ImageFormat m_imageFormat;
    protected ImageSaveFlags m_saveFlags;

    /// <summary>
    /// Constructs a <see cref="TeximpTexture2DExporter"/>.
    /// </summary>
    /// <param name="extension">File extension.</param>
    /// <param name="format">File format.</param>
    /// <param name="saveFlags">Save flags, if any.</param>
    protected TeximpTexture2DExporter(string extension, ImageFormat format, ImageSaveFlags saveFlags = ImageSaveFlags.Default) 
      : base(extension)
    {
      m_imageFormat = format;
      m_saveFlags = saveFlags;
    }

    /// <inheritdoc />
    public override void Save(IResourceFile resourceFile, Texture2D content)
    {
      using (Stream stream = resourceFile.OpenWrite())
        Save(stream, content);
    }

    /// <inheritdoc />
    public override void Save(Stream output, Texture2D content)
    {
      using (Surface surface = ToSurface(content))
        surface.SaveToStream(m_imageFormat, output, m_saveFlags);
    }

    /// <summary>
    /// Converts the first mip level of the texture to a surface object.
    /// </summary>
    /// <param name="tex">2D Texture</param>
    /// <returns>Surface object</returns>
    protected Surface ToSurface(Texture2D tex)
    {
      if (tex.Format == SurfaceFormat.Color || tex.Format == SurfaceFormat.BGRColor)
      {
        SubResourceAt mip0Index = SubResourceAt.Mip(0);
        using (DataBuffer<byte> mip0Data = DataBuffer.Create<byte>(mip0Index.CalculateMipLevelSizeInBytes(tex.Width, tex.Height, tex.Format), MemoryAllocatorStrategy.DefaultPooled))
        {
          tex.GetData<byte>(mip0Data.Span);

          using (MemoryHandle dbPtr = mip0Data.Pin())
            return Surface.LoadFromRawData(dbPtr.AsIntPointer(), tex.Width, tex.Height, tex.Width * 4, tex.Format == SurfaceFormat.BGRColor, true);
        }
      }
      else
      {
        DataBuffer<Color> mip0Data = ImageHelper.ConvertToColor(tex.RenderSystem.ImmediateContext, tex);

        using (MemoryHandle dbPtr = mip0Data.Pin())
          return Surface.LoadFromRawData(dbPtr.AsIntPointer(), tex.Width, tex.Height, tex.Width * 4, true);
      }
    }
  }
}
