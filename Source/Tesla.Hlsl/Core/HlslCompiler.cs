﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Tesla.Content;
using Tesla.Graphics;

namespace Tesla.Hlsl
{
  /// <summary>
  /// Compiles HLSL shaders from one or more HLSL files as determined by a manifest file, outputting
  /// the compiled by bytecode and relevant metadata for using the shaders at runtime.
  /// </summary>
  public class HlslCompiler : IShaderCompiler
  {
    /// <inheritdoc />
    public ShaderCompileResult CompileFromFile(IResourceFile manifestFile, ShaderCompileFlags compileFlags, IShaderSourceHandler? shaderSrcHandler = null)
    {
      ShaderManifest manifest;

      using (Stream manifestStream = manifestFile.OpenRead())
        manifest = ShaderManifest.FromJSON(manifestStream);

      return Compile(manifest, compileFlags, shaderSrcHandler ?? new ShaderSourceHandler(manifestFile));
    }

    /// <inheritdoc />
    public ShaderCompileResult Compile(ShaderManifest input, ShaderCompileFlags compileFlags, IShaderSourceHandler shaderSrcHandler)
    {
      Stopwatch timer = Stopwatch.StartNew();

      HlslCompileContext ctx = new HlslCompileContext(shaderSrcHandler);
      List<ShaderManifest.ResolvedShaderGroup> shaderGroupsToCompile = input.ResolveShaderGroups(ctx.Errors);

      if (ctx.HasErrors)
        return ShaderCompileResult.FromError(ctx.Errors);

      if (!ctx.LoadHlslSourceCode(input))
        return ShaderCompileResult.FromError(ctx.Errors);

      foreach (ShaderManifest.ResolvedShaderGroup shaderGrp in shaderGroupsToCompile)
      {
        if (!ctx.CompileShaderGroup(shaderGrp, compileFlags))
        {
          ctx.WaitOnTasks();
          return ShaderCompileResult.FromError(ctx.Errors);
        }
      }

      ShaderData? data = ctx.CreateShaderData();

      timer.Stop();
      Console.WriteLine($"{timer.ElapsedMilliseconds} ms time to compile '{input.Name}'.");

      if (data is null || ctx.HasErrors)
        return ShaderCompileResult.FromError(ctx.Errors);

      return ShaderCompileResult.FromResult(data);
    }

    /// <inheritdoc />
    public void Dispose() { }
  }
}
