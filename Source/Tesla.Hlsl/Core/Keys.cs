﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using Tesla.Graphics;

namespace Tesla.Hlsl
{
  internal readonly struct MacrosKey : IEquatable<MacrosKey>
  {
    public readonly int Hash;
    public readonly IReadOnlyDictionary<string, ShaderMacro> Macros;

    private MacrosKey(IReadOnlyDictionary<string, ShaderMacro> macros, int hash)
    {
      Hash = hash;
      Macros = macros;
    }

    public override int GetHashCode()
    {
      return Hash;
    }

    public override string ToString()
    {
      StringBuilder builder = new StringBuilder();

      int index = 0;
      foreach (string name in Macros.Keys)
      {
        if (index > 0)
          builder.Append(", ");

        builder.Append($"'{name}'");
        index++;
      }

      return builder.ToString();
    }

    public override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is MacrosKey key)
        return Equals(key);

      return false;
    }

    public bool Equals(MacrosKey other)
    {
      if (Macros.Count != other.Macros.Count)
        return false;

      foreach (ShaderMacro otherMacro in other.Macros.Values)
      {
        if (!Macros.TryGetValue(otherMacro.Name, out ShaderMacro macro) || !macro.Equals(otherMacro))
          return false;
      }

      return true;
    }

    public static MacrosKey Create(IReadOnlyDictionary<string, ShaderMacro> macros)
    {
      List<ShaderMacro> sortedMacros = macros.Values.ToList();
      sortedMacros.Sort((ShaderMacro a, ShaderMacro b) =>
      {
        return String.Compare(a.Name, b.Name, StringComparison.InvariantCulture);
      });

      HashCode hash = new HashCode();
      foreach (ShaderMacro macro in sortedMacros)
        hash.Add(macro);

      return new MacrosKey(macros, hash.ToHashCode());
    }
  }

  internal readonly struct ShaderKey : IEquatable<ShaderKey>
  {
    public readonly int Hash;
    public readonly ShaderStage Stage;
    public readonly string EntryFunctionName;
    public readonly MacrosKey Macros;

    public ShaderKey(ShaderStage stage, string entryFuncName, MacrosKey macros)
    {
      Hash = HashCode.Combine(macros, stage, entryFuncName);
      Stage = stage;
      EntryFunctionName = entryFuncName;
      Macros = macros;
    }

    public override int GetHashCode()
    {
      return Hash;
    }

    public override string ToString()
    {
      return $"'{EntryFunctionName}', {Stage}, [{Macros}]";
    }

    public override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is ShaderKey key)
        return Equals(key);

      return false;
    }

    public bool Equals(ShaderKey other)
    {
      if (Stage != other.Stage)
        return false;

      if (!String.Equals(EntryFunctionName, other.EntryFunctionName, StringComparison.InvariantCulture))
        return false;

      return Macros.Equals(other.Macros);
    }

    public static ShaderKey Create(ShaderStage stage, string entryFunctionName, IReadOnlyDictionary<string, ShaderMacro> macros)
    {
      return new ShaderKey(stage, entryFunctionName, MacrosKey.Create(macros));
    }
  }
}
