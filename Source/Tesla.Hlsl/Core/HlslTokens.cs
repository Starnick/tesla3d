﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using Tesla.Text;

namespace Tesla.Hlsl
{
  internal enum TokenType : int
  {
    Unknown = -1,

    OpenBracket,
    CloseBracket,
    OpenCurlyBrace,
    CloseCurlyBrace,
    OpenParentheses,
    CloseParentheses,
    OpenAngleBracket,
    CloseAngleBracket,
    QuotationMark,
    Equals,
    Semicolon,
    Colon,
    Coma,

    PreProcess_Define,
    PreProcess_UnDef,
    PreProcess_If,
    PreProcess_IfDef,
    PreProcess_IfNDef,
    PreProcess_Elif,
    PreProcess_Else,
    PreProcess_EndIf,
    PreProcess_Defined,
    PreProcess_NotDefined,

    Identifier,
    IncludeDirective,

    CBuffer,
    PerFrameAttribute,
    ComputedParameterAttribute,
    FeatureAttribute,

    // Type modifiers
    Const,
    RowMajor,
    ColumnMajor,

    // Storage class
    Extern,
    NoInterpolation,
    Precise,
    Shared,
    GroupShared,
    Static,
    Uniform,
    Volatile,

    // Object Types

    Texture1D,
    Texture1DArray,
    Texture2D,
    Texture2DArray,
    Texture2DMS,
    Texture2DMSArray,
    TextureCube,
    TextureCubeArray,
    Texture3D,
    Buffer,
    StructuredBuffer,
    AppendStructuredBuffer,
    ConsumeStructuredBuffer,
    ByteAddressBuffer,
    RWBuffer,
    RWByteAddressBuffer,
    RWStructuredBuffer,
    RWTexture1D,
    RWTexture1DArray,
    RWTexture2D,
    RWTexture2DArray,
    RWTexture3D,

    // Sampler state properties
    SamplerState,
    AddressU,
    AddressV,
    AddressW,
    BorderColor,
    Filter,
    MaxAnisotropy,
    MaxLOD,
    MinLOD,
    MipLODBias,
    ComparisonFunc,

    // Address mode values
    Wrap,
    Border,
    Clamp,
    Mirror,
    Mirror_Once,

    // Comparison function values
    NEVER,
    LESS,
    EQUAL,
    LESS_EQUAL,
    GREATER,
    NOT_EQUAL,
    GREATER_EQUAL,
    ALWAYS,

    // Filter values
    MIN_MAG_MIP_POINT,
    MIN_MAG_POINT_MIP_LINEAR,
    MIN_POINT_MAG_LINEAR_MIP_POINT,
    MIN_POINT_MAG_MIP_LINEAR,
    MIN_LINEAR_MAG_MIP_POINT,
    MIN_LINEAR_MAG_POINT_MIP_LINEAR,
    MIN_MAG_LINEAR_MIP_POINT,
    MIN_MAG_MIP_LINEAR,
    ANISOTROPIC,
    COMPARISON_MIN_MAG_MIP_POINT,
    COMPARISON_MIN_MAG_POINT_MIP_LINEAR,
    COMPARISON_MIN_POINT_MAG_LINEAR_MIP_POINT,
    COMPARISON_MIN_POINT_MAG_MIP_LINEAR,
    COMPARISON_MIN_LINEAR_MAG_MIP_POINT,
    COMPARISON_MIN_LINEAR_MAG_POINT_MIP_LINEAR,
    COMPARISON_MIN_MAG_LINEAR_MIP_POINT,
    COMPARISON_MIN_MAG_MIP_LINEAR,
    COMPARISON_ANISOTROPIC,

    // Vector types

    Number,
    Float4
  }

  internal static class HlslTokens 
  {
    private static TokenPattern[] s_patterns;

    static HlslTokens()
    {
      s_patterns = new TokenPattern[Enum.GetValues<TokenType>().Length - 1];

      AddSimple(TokenType.OpenBracket, "[");
      AddSimple(TokenType.CloseBracket, "]");
      AddSimple(TokenType.OpenCurlyBrace, "{");
      AddSimple(TokenType.CloseCurlyBrace, "}");
      AddSimple(TokenType.OpenParentheses, "(");
      AddSimple(TokenType.CloseParentheses, ")");
      AddSimple(TokenType.OpenAngleBracket, "<");
      AddSimple(TokenType.CloseAngleBracket, ">");
      AddSimple(TokenType.QuotationMark, "\"");
      AddSimple(TokenType.Equals, "=");
      AddSimple(TokenType.Semicolon, ";");
      AddSimple(TokenType.Colon, ":");
      AddSimple(TokenType.Coma, ",");

      AddSimpleExact(TokenType.PreProcess_Define, "define");
      AddSimpleExact(TokenType.PreProcess_UnDef, "#undef");
      AddSimpleExact(TokenType.PreProcess_If, "#if");
      AddSimpleExact(TokenType.PreProcess_IfDef, "#ifdef");
      AddSimpleExact(TokenType.PreProcess_IfNDef, "#ifndef");
      AddSimpleExact(TokenType.PreProcess_Elif, "#elif");
      AddSimpleExact(TokenType.PreProcess_Else, "#else");
      AddSimpleExact(TokenType.PreProcess_EndIf, "#endif");
      AddSimpleExact(TokenType.PreProcess_Defined, "defined");
      AddSimpleExact(TokenType.PreProcess_NotDefined, "!defined");

      AddRegex(TokenType.Identifier, "[A-Za-z_][A-Za-z0-9_]*");
      AddSimpleExact(TokenType.IncludeDirective, "#include");
      AddSimple(TokenType.CBuffer, "cbuffer");
      AddSimple(TokenType.PerFrameAttribute, "PerFrame");
      AddSimple(TokenType.ComputedParameterAttribute, "ComputedParameter");
      AddSimple(TokenType.FeatureAttribute, "Feature");

      AddSimple(TokenType.Const, "const");
      AddSimple(TokenType.RowMajor, "row_major");
      AddSimple(TokenType.ColumnMajor, "column_major");

      AddSimple(TokenType.Extern, "extern");
      AddSimple(TokenType.NoInterpolation, "nointerpolation");
      AddSimple(TokenType.Precise, "precise");
      AddSimple(TokenType.Shared, "shared");
      AddSimple(TokenType.GroupShared, "groupshared");
      AddSimple(TokenType.Static, "static");
      AddSimple(TokenType.Uniform, "uniform");
      AddSimple(TokenType.Volatile, "volatile");

      AddSimple(TokenType.Texture1D, "Texture1D");
      AddSimple(TokenType.Texture1DArray, "Texture1DArray");
      AddSimple(TokenType.Texture2D, "Texture2D");
      AddSimple(TokenType.Texture2DArray, "Texture2DArray");
      AddSimple(TokenType.Texture2DMS, "Texture2DMS");
      AddSimple(TokenType.Texture2DMSArray, "Texture2DMSArray");
      AddSimple(TokenType.TextureCube, "TextureCube");
      AddSimple(TokenType.TextureCubeArray, "TextureCubeArray");
      AddSimple(TokenType.Texture3D, "Texture3D");
      AddSimple(TokenType.Buffer, "Buffer");
      AddSimple(TokenType.StructuredBuffer, "StructuredBuffer");
      AddSimple(TokenType.AppendStructuredBuffer, "AppendStructuredBuffer");
      AddSimple(TokenType.ConsumeStructuredBuffer, "ConsumeStructuredBuffer");
      AddSimple(TokenType.ByteAddressBuffer, "ByteAddressBuffer");
      AddSimple(TokenType.RWBuffer, "RWBuffer");
      AddSimple(TokenType.RWByteAddressBuffer, "RWByteAddressBuffer");
      AddSimple(TokenType.RWStructuredBuffer, "RWStructuredBuffer");
      AddSimple(TokenType.RWTexture1D, "RWTexture1D");
      AddSimple(TokenType.RWTexture1DArray, "RWTexture1DArray");
      AddSimple(TokenType.RWTexture2D, "RWTexture2D");
      AddSimple(TokenType.RWTexture2DArray, "RWTexture2DArray");
      AddSimple(TokenType.RWTexture3D, "RWTexture3D");

      AddRegex(TokenType.SamplerState, "(?i)(SamplerState|sampler_state|SamplerComparisonState|sampler|sampler1D|sampler2D|sampler3D|samplerCUBE)");
      AddSimple(TokenType.AddressU, "AddressU");
      AddSimple(TokenType.AddressV, "AddressV");
      AddSimple(TokenType.AddressW, "AddressW");
      AddSimple(TokenType.BorderColor, "BorderColor");
      AddSimple(TokenType.Filter, "Filter");
      AddSimple(TokenType.MaxAnisotropy, "MaxAnisotropy");
      AddSimple(TokenType.MaxLOD, "MaxLOD");
      AddSimple(TokenType.MinLOD, "MinLOD");
      AddSimple(TokenType.MipLODBias, "MipLODBias");
      AddSimple(TokenType.ComparisonFunc, "ComparisonFunc");

      AddSimple(TokenType.Wrap, "Wrap");
      AddSimple(TokenType.Border, "Border");
      AddSimple(TokenType.Clamp, "Clamp");
      AddSimple(TokenType.Mirror, "Mirror");
      AddSimple(TokenType.Mirror_Once, "Mirror_Once");

      AddSimple(TokenType.NEVER, "NEVER");
      AddSimple(TokenType.LESS, "LESS");
      AddSimple(TokenType.EQUAL, "EQUAL");
      AddSimple(TokenType.LESS_EQUAL, "LESS_EQUAL");
      AddSimple(TokenType.GREATER, "GREATER");
      AddSimple(TokenType.NOT_EQUAL, "NOT_EQUAL");
      AddSimple(TokenType.GREATER_EQUAL, "GREATER_EQUAL");
      AddSimple(TokenType.ALWAYS, "ALWAYS");

      AddSimple(TokenType.MIN_MAG_MIP_POINT, "MIN_MAG_MIP_POINT");
      AddSimple(TokenType.MIN_MAG_POINT_MIP_LINEAR, "MIN_MAG_POINT_MIP_LINEAR");
      AddSimple(TokenType.MIN_POINT_MAG_LINEAR_MIP_POINT, "MIN_POINT_MAG_LINEAR_MIP_POINT");
      AddSimple(TokenType.MIN_POINT_MAG_MIP_LINEAR, "MIN_POINT_MAG_MIP_LINEAR");
      AddSimple(TokenType.MIN_LINEAR_MAG_MIP_POINT, "MIN_LINEAR_MAG_MIP_POINT");
      AddSimple(TokenType.MIN_LINEAR_MAG_POINT_MIP_LINEAR, "MIN_LINEAR_MAG_POINT_MIP_LINEAR");
      AddSimple(TokenType.MIN_MAG_LINEAR_MIP_POINT, "MIN_MAG_LINEAR_MIP_POINT");
      AddSimple(TokenType.MIN_MAG_MIP_LINEAR, "MIN_MAG_MIP_LINEAR");
      AddSimple(TokenType.ANISOTROPIC, "ANISOTROPIC");
      AddSimple(TokenType.COMPARISON_MIN_MAG_MIP_POINT, "COMPARISON_MIN_MAG_MIP_POINT");
      AddSimple(TokenType.COMPARISON_MIN_MAG_POINT_MIP_LINEAR, "COMPARISON_MIN_MAG_POINT_MIP_LINEAR");
      AddSimple(TokenType.COMPARISON_MIN_POINT_MAG_LINEAR_MIP_POINT, "COMPARISON_MIN_POINT_MAG_LINEAR_MIP_POINT");
      AddSimple(TokenType.COMPARISON_MIN_POINT_MAG_MIP_LINEAR, "COMPARISON_MIN_POINT_MAG_MIP_LINEAR");
      AddSimple(TokenType.COMPARISON_MIN_LINEAR_MAG_MIP_POINT, "COMPARISON_MIN_LINEAR_MAG_MIP_POINT");
      AddSimple(TokenType.COMPARISON_MIN_LINEAR_MAG_POINT_MIP_LINEAR, "COMPARISON_MIN_LINEAR_MAG_POINT_MIP_LINEAR");
      AddSimple(TokenType.COMPARISON_MIN_MAG_LINEAR_MIP_POINT, "COMPARISON_MIN_MAG_LINEAR_MIP_POINT");
      AddSimple(TokenType.COMPARISON_MIN_MAG_MIP_LINEAR, "COMPARISON_MIN_MAG_MIP_LINEAR");
      AddSimple(TokenType.COMPARISON_ANISOTROPIC, "COMPARISON_ANISOTROPIC");

      AddRegex(TokenType.Number, "[0-9.]");
      AddSimple(TokenType.Float4, "Float4");
    }

    private static void AddSimple(TokenType type, string text)
    {
      s_patterns[(int)type] = CreateSimple(type, text);
    }

    private static void AddSimpleExact(TokenType type, string text)
    {
      s_patterns[(int) type] = new SimpleTokenPattern((int) type, text, StringComparison.InvariantCulture);
    }

    private static void AddRegex(TokenType type, string pattern)
    {
      s_patterns[(int) type] = CreateRegex(type, pattern);
    }

    private static TokenPattern CreateSimple(TokenType type, string text)
    {
      return new SimpleTokenPattern((int) type, text);
    }

    private static TokenPattern CreateRegex(TokenType type, string pattern)
    {
      return new RegexTokenPattern((int) type, pattern);
    }

    public static StringTokenizer CreateScanner()
    {
      StringTokenizer scanner = new StringTokenizer();
      scanner.SkipComments = false; // Custom attributes are inside the double slash comments
      scanner.AddNonSkipDelimiters(new[] { '[', ']', '{', '}', '(', ')', '<', '>', '\"', '=', ';' });
      return scanner;
    }

    public static TokenPattern GetPattern(TokenType tokenType)
    {
      uint index = (uint) tokenType;
      if (index >= (uint) s_patterns.Length)
        throw new ArgumentOutOfRangeException(nameof(tokenType));

      return s_patterns[index];
    }

    public static IReadOnlyList<TokenPattern> GetPatterns(params TokenType[] tokenTypes)
    {
      List<TokenPattern> patterns = new List<TokenPattern>();
      foreach (TokenType tokType in tokenTypes)
      {
        TokenPattern? pattern = GetPattern(tokType);
        if (pattern is not null)
          patterns.Add(pattern);
      }

      return patterns;
    }
  }
}
