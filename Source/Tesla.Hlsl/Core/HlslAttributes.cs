﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System.Collections.Generic;
using Tesla.Graphics;

namespace Tesla.Hlsl
{
  public sealed class HlslSamplerState
  {
    public string Name;
    public TextureAddressMode AddressU;
    public TextureAddressMode AddressV;
    public TextureAddressMode AddressW;
    public ComparisonFunction ComparisonFunction;
    public TextureFilter Filter;
    public int MaxAnisotropy;
    public float MipMapLevelOfDetailBias;
    public int MinMipMapLevel; //Min = 0
    public int MaxMipMapLevel; //Max = int.MaxValue
    public Color BorderColor;

    public HlslSamplerState(string name)
    {
      Name = name;
      AddressU = TextureAddressMode.Clamp;
      AddressV = TextureAddressMode.Clamp;
      AddressW = TextureAddressMode.Clamp;
      Filter = TextureFilter.Linear;
      MaxAnisotropy = 1;
      MipMapLevelOfDetailBias = 0.0f;
      MinMipMapLevel = 0;
      MaxMipMapLevel = int.MaxValue;
      BorderColor = Color.White;
      ComparisonFunction = ComparisonFunction.Never;
    }

    public ShaderData.SamplerStateData CreateSamplerStateData()
    {
      ShaderData.SamplerStateData s;
      s.AddressU = AddressU;
      s.AddressV = AddressV;
      s.AddressW = AddressW;
      s.ComparisonFunction = ComparisonFunction;
      s.Filter = Filter;
      s.MaxAnisotropy = MaxAnisotropy;
      s.MipMapLevelOfDetailBias = MipMapLevelOfDetailBias;
      s.MinMipMapLevel = MinMipMapLevel;
      s.MaxMipMapLevel = MaxMipMapLevel;
      s.BorderColor = BorderColor;

      return s;
    }
  }

  /// <summary>
  /// Manages data pulled from one or more HLSL files that is not parto f the normal D3D Compilation.
  /// </summary>
  public sealed class HlslAttributes
  {
    public Dictionary<string, HlslSamplerState> Samplers;
    public HashSet<string> PerFrameCbuffers;
    public Dictionary<string, string> CBufferNameToFeature;
    public Dictionary<string, string> VariableNameToFeature;
    public Dictionary<string, string> VariableNameToComputedParameter;
  
    public HlslAttributes()
    {
      Samplers = new Dictionary<string, HlslSamplerState>();
      PerFrameCbuffers = new HashSet<string>();
      CBufferNameToFeature = new Dictionary<string, string>();
      VariableNameToComputedParameter = new Dictionary<string, string>();
      VariableNameToFeature = new Dictionary<string, string>();
    }

    public string? ResolveFeatureName(string variableName, string? cbufferName = null)
    {
      if (VariableNameToFeature.TryGetValue(variableName, out string? featureName))
        return featureName;

      if (cbufferName is not null)
      {
        if (CBufferNameToFeature.TryGetValue(cbufferName, out featureName))
          return featureName;
      }

      return null;
    }
  }
}
