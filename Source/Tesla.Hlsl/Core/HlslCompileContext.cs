﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Tesla.Content;
using Tesla.Graphics;
using Tesla.Collections;
using D3D = SharpDX.Direct3D;
using D3DC = SharpDX.D3DCompiler;

namespace Tesla.Hlsl
{
  internal sealed class HlslCompileContext
  {
    private List<string> m_errors;
    private SynchronizedShaderSourceHandler m_handler;

    private Dictionary<MacrosKey, PendingShaderSource> m_processedShaderSources;
    private IndexedList<ShaderKey, PendingCompiledShader> m_compiledShaders;
    private IndexedList<string, PendingConstantBuffer> m_constantBuffers;
    private IndexedList<string, PendingResourceVariable> m_resourceVariables;
    private List<PendingShaderGroup> m_shaderGroups;
    private FeatureStringIndex m_familyNames;
    private FeatureStringIndex m_featureNames;
    private FeatureStringIndex m_parameterNamespaces;
    private FeatureStringIndex m_computedParameterNames;
    private string[] m_shaderProfiles;

    private string? m_sourceCode;
    private string? m_sourceFileName;
    private string? m_shaderLibraryName;

    private List<Task> m_queuedTasks;
    private CancellationTokenSource m_cancellationSrc;

    public static int NumShaderStages { get { return 6; } }

    public List<string> Errors { get { return m_errors; } }

    public bool HasErrors { get { return m_errors.Count > 0; } }

    public HlslCompileContext(IShaderSourceHandler handler)
    {
      m_errors = new List<string>();
      m_handler = new SynchronizedShaderSourceHandler(handler);
      m_shaderProfiles = new string[NumShaderStages];

      m_processedShaderSources = new Dictionary<MacrosKey, PendingShaderSource>();
      m_compiledShaders = new IndexedList<ShaderKey, PendingCompiledShader>();
      m_constantBuffers = new IndexedList<string, PendingConstantBuffer>();
      m_resourceVariables = new IndexedList<string, PendingResourceVariable>();
      m_shaderGroups = new List<PendingShaderGroup>();
      m_familyNames = new FeatureStringIndex(StringComparison.InvariantCulture);
      m_parameterNamespaces = new FeatureStringIndex(StringComparison.InvariantCulture);
      m_featureNames = new FeatureStringIndex(StringComparison.InvariantCultureIgnoreCase); // Ignore case for features
      m_computedParameterNames = new FeatureStringIndex(StringComparison.InvariantCulture);
      m_queuedTasks = new List<Task>();
      m_cancellationSrc = new CancellationTokenSource();
    }

    public uint AddFamilyName(string familyName)
    {
      return (uint) m_familyNames.Add(familyName);
    }

    public uint AddFeatureName(string featureName)
    {
      return (uint) m_featureNames.Add(featureName);
    }

    public uint AddComputedParameterName(string computedParamName)
    {
      return (uint) m_computedParameterNames.Add(computedParamName);
    }

    public uint AddParameterNamespace(string paramNamespace)
    {
      return (uint) m_parameterNamespaces.Add(paramNamespace);
    }

    public void AddError(string error)
    {
      lock (m_errors)
      {
        m_errors.Add(error);
        m_cancellationSrc.Cancel();
      }
    }

    public void AddError(Exception e)
    {
      lock (m_errors)
      {
        EngineLog.LogException(LogLevel.Error, e);
        m_errors.Add(e.Message);
        m_cancellationSrc.Cancel();
      }
    }

    private void AddError(PendingShaderGroup grp, string msg)
    {
      AddError($"Error in ShaderGroup '{grp.Name}': {msg}");
    }

    public void WaitOnTasks()
    {
      if (m_queuedTasks.Count == 0)
        return;

      Task[] tasks = m_queuedTasks.ToArray();
      m_queuedTasks.Clear();

      try
      {
        Task.WaitAll(tasks);
      } 
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        if (!HasErrors)
          AddError(e);
      }
    }

    public bool LoadHlslSourceCode(ShaderManifest input)
    {
      try
      {
        m_sourceCode = m_handler.GetSourceCode(input.File);
        m_sourceFileName = input.File;
        m_shaderLibraryName = input.Name;

        for (int i = 0; i < m_shaderProfiles.Length; i++)
          m_shaderProfiles[i] = GetShaderProfile((ShaderStage) i, input.ShaderProfile);
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
      }

      if (String.IsNullOrEmpty(m_sourceCode))
      {
        AddError($"'{input.File}' not found. No shader source code could be loaded.");
        return false;
      }

      return true;
    }

    public bool CompileShaderGroup(ShaderManifest.ResolvedShaderGroup grp, ShaderCompileFlags compileFlags)
    {
      PendingShaderGroup pendingGrp = PendingShaderGroup.Create(this, grp);
      m_shaderGroups.Add(pendingGrp);

      for (int i = 0; i < HlslCompileContext.NumShaderStages; i++)
      {
        ShaderStage stage = (ShaderStage) i;
        string? entryFuncName = grp.GetShaderEntryFunction(stage);
        if (entryFuncName is null)
          continue;

        int index = CompileShader(stage, entryFuncName, grp.Macros, compileFlags);
        if (index < 0 || HasErrors)
          return false;

        pendingGrp.ShaderIndices.Add((uint) index);
      }

      return true;
    }

    private int CompileShader(ShaderStage shaderType, string entryFunctionName, IReadOnlyDictionary<string, ShaderMacro> macros, ShaderCompileFlags compileFlags)
    {
      if (m_sourceCode is null)
        return -1;

      ShaderKey key = ShaderKey.Create(shaderType, entryFunctionName, macros);

      if (!m_processedShaderSources.TryGetValue(key.Macros, out PendingShaderSource? srcCode))
      {
        srcCode = PendingShaderSource.StartPreProcessing(this, m_sourceCode, m_sourceFileName!, new IncludeAdapter(m_handler), key.Macros, m_cancellationSrc.Token);
        m_processedShaderSources.Add(key.Macros, srcCode);
        m_queuedTasks.Add(srcCode.PendingTask);
      }

      if (!m_compiledShaders.TryGetValue(key, out PendingCompiledShader? pendingCompileShader))
      {
        pendingCompileShader = new PendingCompiledShader(key);
        pendingCompileShader.ShaderIndex = m_compiledShaders.Add(key, pendingCompileShader);
        m_queuedTasks.Add(pendingCompileShader.StartCompiling(this, srcCode.PendingTask, m_sourceFileName!, m_shaderProfiles[(int) shaderType], compileFlags, m_cancellationSrc.Token));
      }

      return pendingCompileShader.ShaderIndex;
    }

    private CompiledShader GetCompiledShader(uint index)
    {
      return m_compiledShaders[(int) index].PendingTask.Result;
    }

    private bool PopulateShaderGroup(PendingShaderGroup grp)
    {
      // This ensures resources used by each shader is created, and validates the consistency of those resources across shaders.
      // For example, if two shaders reference a cbuffer or resource variable by name, they should be structurally the same (same type, layout, etc).
      // Where they bind may differ however.

      // Attribute usage is also accumulated into the shader group, based on variable usage
      if (grp.ShaderIndices.Count == 0)
      {
        AddError($"Expected at least one shader for ShaderGroup {grp.Name}.");
        return false;
      }

      foreach (uint shaderIndex in grp.ShaderIndices)
      {
        CompiledShader shader = GetCompiledShader(shaderIndex);

        // Examine each resource binding for the shader
        foreach (D3DResourceBinding binding in shader.Reflection.BoundResources)
        {
          if (binding.IsConstantBuffer)
          {
            // Should find this cbuffer reflected
            D3DConstantBuffer? reflected = shader.Reflection.GetConstantBufferByName(binding.Name);
            if (reflected is null)
            {
              AddError(grp, $"CBuffer '{binding.Name}' is expected in Shader #{shader.ShaderIndex} ({shader.Key}).");
              return false;
            }

            // Create if not there, if it is then validate if it matches. There should be consistency among same-named resources
            if (!m_constantBuffers.TryGetValue(binding.Name, out PendingConstantBuffer? cb))
            {
              cb = PendingConstantBuffer.Create(this, reflected, shader.Source.CustomAttributes, grp);
              if (cb is null)
                return false;

              m_constantBuffers.Add(binding.Name, cb);
            }
            else
            {
              if (!cb.Validate(this, reflected, shader.Source.CustomAttributes, grp))
                return false;
            }
          }
          else
          {
            // Create if not found, if found validate. Again with resource variables, if same-name it should be consistent
            if (!m_resourceVariables.TryGetValue(binding.Name, out PendingResourceVariable? rv))
            {
              rv = PendingResourceVariable.Create(this, binding, shader, grp);
              if (rv is null)
                return false;

              m_resourceVariables.Add(binding.Name, rv);
            } 
            else
            {
              if (!rv.Validate(this, binding, shader, grp))
                return false;
            }
          }
        }
      }

      // Index builders will now be complete
      if (grp.UsedFeatureIndices.Count > 0)
      {
        grp.Final.FeatureIndices = grp.UsedFeatureIndices.ToArray();
        Array.Sort(grp.Final.FeatureIndices);
      }

      if (grp.UsedComputedParameterIndices.Count > 0)
      {
        grp.Final.ComputedParameterIndices = grp.UsedComputedParameterIndices.ToArray();
        Array.Sort(grp.Final.ComputedParameterIndices);
      }

      grp.Final.ShaderIndices = grp.ShaderIndices.ToArray();

      return true;
    }

    private int GetResourceBindingIndex(D3DResourceBinding binding)
    {
      if (binding.IsConstantBuffer)
      {
        return m_constantBuffers.IndexOfKey(binding.Name);
      }
      else
      {
        return m_resourceVariables.IndexOfKey(binding.Name);
      }
    }

    private bool PopulateShader(CompiledShader compiledShader, ShaderData.Shader sh)
    {
      D3DShaderReflection reflect = compiledShader.Reflection;
      sh.Name = (compiledShader.Key.Macros.Macros.Count > 0) ? $"{compiledShader.Key.EntryFunctionName}[{compiledShader.Key.Macros}]" : compiledShader.Key.EntryFunctionName;
      sh.ShaderType = compiledShader.Key.Stage;
      sh.ShaderProfile = m_shaderProfiles[(int) compiledShader.Key.Stage];
      sh.ShaderFlags = reflect.ShaderFlags;
      sh.ShaderByteCode = compiledShader.CompiledByteCode;
      sh.GeometryShaderInputPrimitive = reflect.GeometryShaderInputPrimitive;
      sh.GeometryShaderOutputTopology = reflect.GeometryShaderOutputTopology;
      sh.GeometryShaderInstanceCount = reflect.GeometryShaderInstanceCount;
      sh.MaxGeometryShaderOutputVertexCount = reflect.MaxGeometryShaderOutputVertexCount;
      sh.GeometryOrHullShaderInputPrimitive = reflect.GeometryOrHullShaderInputPrimitive;
      sh.HullShaderOutputPrimitive = reflect.HullShaderOutputPrimitive;
      sh.HullShaderPartitioning = reflect.HullShaderPartitioning;
      sh.TessellatorDomain = reflect.TessellatorDomain;
      sh.IsSampleFrequencyShader = reflect.IsSampleFrequencyShader;

      // Bound resources
      if (reflect.BoundResources.Length > 0)
      {
        sh.BoundResources = new ShaderData.BoundResource[reflect.BoundResources.Length];
        for (int i = 0; i < reflect.BoundResources.Length; i++)
        {
          D3DResourceBinding binding = reflect.BoundResources[i];
          int index = GetResourceBindingIndex(binding);
          if (index < 0)
          {
            AddError($"Bound Resource '{binding.Name}' was not processed correctly.");
            return false;
          }

          ShaderData.BoundResource b = new ShaderData.BoundResource();
          b.ResourceIndex = (uint) index;
          b.ResourceType = binding.ResourceType;
          b.BindPoint = (uint) binding.BindPoint;
          b.BindCount = (uint) binding.BindCount;
          sh.BoundResources[i] = b;
        }
      }

      // Signatures
      sh.InputSignature = reflect.CreateShaderDataInputSignature();
      sh.OutputSignature = reflect.CreateShaderDataOutputSignature();

      return true;
    }

    public ShaderData? CreateShaderData()
    {
      WaitOnTasks();

      if (HasErrors)
        return null;

      if (m_shaderGroups.Count == 0 || m_compiledShaders.Count == 0)
      {
        AddError("Expected at least one shader or shader grouping.");
        return null;
      }

      HashSet<string> uniqueNames = new HashSet<string>();

      // Process shader groups, this validates shared resources and determines feature / computed parameter usage
      ShaderData.ShaderGroup[] groups = new ShaderData.ShaderGroup[m_shaderGroups.Count];
      for (int i = 0; i < m_shaderGroups.Count; i++)
      {
        PendingShaderGroup grp = m_shaderGroups[i];
        if (!PopulateShaderGroup(grp))
          return null;

        if (uniqueNames.Contains(grp.Final.Name))
        {
          AddError($"Duplicate ShaderGroup '{grp.Final.Name}'. All groups must be uniquely named within the same shader library.");
          return null;
        }

        uniqueNames.Add(grp.Final.Name);
        groups[i] = grp.Final;
      }


      ShaderData data = new ShaderData();

      // Finalize shaders (populates used resources)
      data.ShaderGroups = groups;
      data.Shaders = new ShaderData.Shader[m_compiledShaders.Count];
      for (uint i = 0; i < m_compiledShaders.Count; i++)
      {
        ShaderData.Shader shader = new ShaderData.Shader();
        if (!PopulateShader(GetCompiledShader(i), shader))
          return null;

        data.Shaders[i] = shader;
      }

      // Make sure all variables are unique, constant buffer names and resource variables already will have been checked. But ensure
      // variables within constant buffers are checked against other constant buffers and resource variables.
      if (!ValidateUniqueVariableNaming(uniqueNames))
        return null;

      // Finalize constant buffers
      if (m_constantBuffers.Count > 0)
      {
        data.ConstantBuffers = new ShaderData.ConstantBuffer[m_constantBuffers.Count];
        for (int i = 0; i < m_constantBuffers.Count; i++)
          data.ConstantBuffers[i] = m_constantBuffers[i].Final;
      }

      // Finalize resource variables
      if (m_resourceVariables.Count > 0)
      {
        data.ResourceVariables = new ShaderData.ResourceVariable[m_resourceVariables.Count];
        for (int i = 0; i < m_resourceVariables.Count; i++)
          data.ResourceVariables[i] = m_resourceVariables[i].Final;
      }

      // Finalize the rest
      data.Name = m_shaderLibraryName ?? String.Empty;
      data.Format = ShaderData.ShaderByteCodeFormat.DXBC;
      data.ParameterNamespaces = m_parameterNamespaces.ToArray();
      data.FeatureNames = m_featureNames.ToArray();
      data.ComputedParameterNames = m_computedParameterNames.ToArray();
      data.FamilyNames = m_familyNames.ToArray();

      return data;
    }

    private bool ValidateUniqueVariableNaming(HashSet<string> uniques, string value)
    {
      if (uniques.Contains(value))
      {
        AddError($"Found multiple declarations of the variable '{value}'. Variables must be uniquely named (possible #ifdef redefinition?).");
        return false;
      }

      uniques.Add(value);
      return true;
    }

    private bool ValidateUniqueVariableNaming(HashSet<string> uniqueVariableNames)
    {
      if (m_constantBuffers.Count == 0 && m_resourceVariables.Count == 0)
        return true;

      uniqueVariableNames.Clear();
      foreach (PendingConstantBuffer cb in m_constantBuffers)
      {
        foreach (D3DShaderVariable v in cb.Initial.Variables)
        {
          if (!ValidateUniqueVariableNaming(uniqueVariableNames, v.Name))
            return false;
        }
      }

      foreach (PendingResourceVariable rv in m_resourceVariables)
      {
        if (!ValidateUniqueVariableNaming(uniqueVariableNames, rv.Initial.Name))
          return false;
      }

      return true;
    }

    private sealed class PendingShaderSource
    {
      public MacrosKey Key;
      public readonly Task<ShaderSource> PendingTask;

      private PendingShaderSource(MacrosKey key, Task<ShaderSource> pendingTask)
      {
        Key = key;
        PendingTask = pendingTask;
      }

      public override string ToString()
      {
        if (PendingTask.IsCompletedSuccessfully)
          return PendingTask.Result.ToString();

        return Key.ToString();
      }

      public static PendingShaderSource StartPreProcessing(HlslCompileContext ctx, string sourceCode, string sourceFileName, IncludeAdapter include, MacrosKey macros, CancellationToken token)
      {
        Task<ShaderSource> task = Task<ShaderSource>.Run(() =>
        {
          D3D.ShaderMacro[]? d3dMacros = ConvertShaderMacros(macros.Macros);
          try
          {
            string processedSrcCode = D3DC.ShaderBytecode.Preprocess(sourceCode, d3dMacros, include, out string compileErrors, sourceFileName);

            if (String.IsNullOrEmpty(processedSrcCode) || !String.IsNullOrEmpty(compileErrors))
              throw new TeslaContentException(compileErrors ?? $"Failed to preprocess '{sourceFileName}'.");

            if (token.IsCancellationRequested)
              throw new TeslaContentException($"Aborted preprocessing '{sourceFileName}' due to user cancellation.");

            // Parse the processed shader source for custom attributes, this will need to do its own pre-processing as the above is stripped
            // of comments
            HlslAttributeParser.ReadIncludeFileHandler handleInclude = (string incFileName, IncludeType incType) =>
            {
              return include.ShaderSourceHandler.GetSourceCode(incFileName, incType);
            };

            HlslAttributeParser parser = new HlslAttributeParser();
            HlslAttributes customAttrutes = parser.Parse(sourceCode, handleInclude, macros.Macros.Keys);

            return new ShaderSource(macros, processedSrcCode, customAttrutes);
          }
          catch (Exception e)
          {
            ctx.AddError(e);
            throw e;
          }
        }, token);

        return new PendingShaderSource(macros, task);
      }
    }

    private sealed class ShaderSource
    {
      public readonly MacrosKey Key;
      public readonly string SourceCode;
      public readonly HlslAttributes CustomAttributes;

      public ShaderSource(MacrosKey key, string sourceCode, HlslAttributes customAttributes)
      {
        Key = key;
        SourceCode = sourceCode;
        CustomAttributes = customAttributes;
      }

      public override string ToString()
      {
        return SourceCode;
      }
    }

    private sealed class PendingCompiledShader
    {
      public int ShaderIndex;
      public readonly ShaderKey Key;

      private Task<CompiledShader>? m_pendingTask;

      public Task<CompiledShader> PendingTask
      {
        get
        {
          ArgumentNullException.ThrowIfNull(m_pendingTask);
          return m_pendingTask;
        }
      }

      public PendingCompiledShader(ShaderKey key)
      {
        Key = key;
      }

      public override string ToString()
      {
        return Key.ToString();
      }

      public Task<CompiledShader> StartCompiling(HlslCompileContext ctx, Task<ShaderSource> srcCodeTask, string sourceFileName, string profile, ShaderCompileFlags compileFlags, CancellationToken token)
      {
        if (m_pendingTask is not null)
          return m_pendingTask;

        m_pendingTask = Task.Run<CompiledShader>(async () =>
        {
          ShaderSource srcCode = await srcCodeTask;

          using D3DC.CompilationResult result = D3DC.ShaderBytecode.Compile(srcCode.SourceCode, Key.EntryFunctionName, profile, (D3DC.ShaderFlags) compileFlags, D3DC.EffectFlags.None, sourceFileName);
          if (result.HasErrors)
          {
            ctx.AddError(result.Message);
            throw new TeslaContentException(result.Message);
          }

          return new CompiledShader(ShaderIndex, Key, srcCode, result.Bytecode);
        }, token);

        return m_pendingTask;
      }
    }

    private sealed class CompiledShader
    {
      public readonly int ShaderIndex;
      public readonly ShaderKey Key;
      public readonly ShaderSource Source;
      public readonly ReadOnlyMemory<byte> CompiledByteCode;
      public readonly D3DShaderReflection Reflection;

      public CompiledShader(int shIndex, ShaderKey key, ShaderSource source, D3DC.ShaderBytecode byteCode)
      {
        ShaderIndex = shIndex;
        Key = key;
        Source = source;
        CompiledByteCode = byteCode.Data;
        Reflection = new D3DShaderReflection(byteCode);
      }

      public override string ToString()
      {
        return $"{ShaderIndex} - {Key}";
      }

      public ShaderData.SamplerStateData? GetSamplerStateData(string variableName)
      {
        if (!Source.CustomAttributes.Samplers.TryGetValue(variableName, out HlslSamplerState? ss))
          return null;

        return ss.CreateSamplerStateData();
      }
    }

    private sealed class PendingShaderGroup
    {
      public readonly ShaderData.ShaderGroup Final;
      public readonly List<uint> ShaderIndices;
      public readonly HashSet<uint> UsedFeatureIndices;
      public readonly HashSet<uint> UsedComputedParameterIndices;
      
      public string Name { get { return Final.Name; } }
      
      private PendingShaderGroup()
      {
        Final = new ShaderData.ShaderGroup();
        ShaderIndices = new List<uint>();
        UsedFeatureIndices = new HashSet<uint>();
        UsedComputedParameterIndices = new HashSet<uint>();
      }

      public static PendingShaderGroup Create(HlslCompileContext ctx, ShaderManifest.ResolvedShaderGroup grp)
      {
        PendingShaderGroup pendingGrp = new PendingShaderGroup();
        pendingGrp.Final.Name = grp.Name;
        pendingGrp.Final.FamilyNameIndex = ctx.AddFamilyName(grp.Family);
        pendingGrp.Final.ParameterNamespaceIndex = ctx.AddParameterNamespace(grp.ParameterNamespace);

        // Shader group may have features it declared
        foreach (string feature in grp.Features)
        {
          uint index = ctx.AddFeatureName(feature);
          pendingGrp.UsedFeatureIndices.Add(index);
        }

        return pendingGrp;
      }
    }

    private sealed class PendingResourceVariable
    {
      public readonly ShaderData.ResourceVariable Final;
      public readonly D3DResourceBinding Initial;

      private D3DConstantBuffer? m_structuredBufferLayout;

      private PendingResourceVariable(D3DResourceBinding initial)
      {
        Final = new ShaderData.ResourceVariable();
        Initial = initial;
      }

      public bool Validate(HlslCompileContext ctx, D3DResourceBinding resourceVariable, CompiledShader shader, PendingShaderGroup grp)
      {
        if (!resourceVariable.AreSame(Initial))
        {
          ctx.AddError(grp, $"Resource Variable '{resourceVariable.Name} is inconsistent with a previously processed variable (possible #ifdef redefinition?).");
          return false;
        }

        //Check element count, noticed that the BindCount will be up to the maximum index used by the shader. So a Texture2D[3], and only first index is used
        //bind count will be 1 - probably because the HLSL compiler unrolls this into three Texture2Ds. So be sure to take the maximum bindcount as the elementcount
        Final.ElementCount = Math.Max(Final.ElementCount, (resourceVariable.BindCount > 1) ? resourceVariable.BindCount : 0);

        // Validate sampler is consistent, if has it. Valid not to have one
        if (resourceVariable.IsSampler)
        {
          ShaderData.SamplerStateData? ss = shader.GetSamplerStateData(resourceVariable.Name);

          // If one declared sampler state data, both should have it. Otherwise we have an inconsistent declaration
          if (!AreSameSamplers(Final.SamplerData, ss))
          {
            AddSamplerError(ctx, resourceVariable, grp);
            return false;
          }
        }

        // Validate structured buffer layout is consistent
        if (resourceVariable.IsConstantBuffer)
        {
          D3DConstantBuffer? cb = shader.Reflection.GetConstantBufferByName(resourceVariable.Name);
          string? reasonWhyNotSame = null;
          if (m_structuredBufferLayout is null 
            || (cb is null || cb.Variables.Length == 0) 
            || !m_structuredBufferLayout.Variables[0].AreSame(cb.Variables[0], true, out reasonWhyNotSame))
          {
            AddStructuredBufferError(ctx, resourceVariable, grp, shader, reasonWhyNotSame);
            return false;
          }
        }

        if (!AddAttributeUsage(ctx, shader.Source.CustomAttributes, grp))
          return false;

        return true;
      }

      private bool AddAttributeUsage(HlslCompileContext ctx, HlslAttributes attributes, PendingShaderGroup grp)
      {
        // Resolve feature name
        if (attributes.VariableNameToFeature.TryGetValue(Final.Name, out string? resolvedFeature))
        {
          uint featureIndex = ctx.AddFeatureName(resolvedFeature);

          // Validate if it's the same index if the variable already had one
          if (Final.FeatureNameIndex.HasValue && Final.FeatureNameIndex.Value != featureIndex)
          {
            string expectedFeatureName = ctx.m_featureNames[(int) Final.FeatureNameIndex.Value];
            ctx.AddError(grp, $"Resource Variable '{Final.Name}' has inconsistent feature (possible #ifdef redefinition?). Has '{resolvedFeature}' but expected '{expectedFeatureName}'.");
            return false;
          }

          Final.FeatureNameIndex = featureIndex;
          grp.UsedFeatureIndices.Add(featureIndex);
        }
        else if (Final.FeatureNameIndex.HasValue)
        {
          // Expected a feature name, because we assigned one before. Something is wrong
          string expectedFeatureName = ctx.m_featureNames[(int) Final.FeatureNameIndex.Value];
          ctx.AddError(grp, $"Resource Variable '{Final.Name}' has inconsistent feature (possible #ifdef redefinition?). Expected '{expectedFeatureName}'.");
          return false;
        }

        // Resolve computed parameter name
        if (attributes.VariableNameToComputedParameter.TryGetValue(Final.Name, out string? resolvedComputedParam))
        {
          uint computedParamIndex = ctx.AddComputedParameterName(resolvedComputedParam);

          // Validate if its the same index if the variable already had one
          if (Final.ComputedParameterNameIndex.HasValue && Final.ComputedParameterNameIndex.Value != computedParamIndex)
          {
            // Expected computed param name, because we assigned one before. Something is wrong
            string expectedParamName = ctx.m_computedParameterNames[(int) Final.ComputedParameterNameIndex.Value];
            ctx.AddError(grp, $"Resource Variable '{Final.Name}' has inconsistent computed parameter mapping (possible #ifdef redefinition?). Has '{resolvedComputedParam}' but expected {expectedParamName}.");
            return false;
          }

          Final.ComputedParameterNameIndex = computedParamIndex;
          grp.UsedComputedParameterIndices.Add(computedParamIndex);
        }
        else if (Final.ComputedParameterNameIndex.HasValue)
        {
          string expectedParamName = ctx.m_computedParameterNames[(int) Final.ComputedParameterNameIndex.Value];
          ctx.AddError(grp, $"Resource Variable '{Final.Name}' has inconsistent computed parameter mapping (possible #ifdef redefinition?). Expected '{expectedParamName}'.");
          return false;
        }

        return true;
      }

      public static PendingResourceVariable? Create(HlslCompileContext ctx, D3DResourceBinding resourceVariable, CompiledShader shader, PendingShaderGroup grp)
      {
        PendingResourceVariable v = new PendingResourceVariable(resourceVariable);
        v.Final.InputFlags = resourceVariable.InputFlags;
        v.Final.Name = resourceVariable.Name;
        v.Final.ResourceDimension = resourceVariable.ResourceDimension;
        v.Final.ResourceType = resourceVariable.ResourceType;
        v.Final.ReturnType = resourceVariable.ReturnType;
        v.Final.SampleCount = resourceVariable.SampleCount;
        v.Final.ElementCount = (resourceVariable.BindCount > 1) ? resourceVariable.BindCount : 0;

        // Sampler arrays not supported. Not sure how much used they are
        if (resourceVariable.IsSampler && v.Final.ElementCount == 0)
        {
          // Valid to be null, we can declare a sampler state without any values (will be the default state)
          v.Final.SamplerData = shader.GetSamplerStateData(resourceVariable.Name);
        }

        if (resourceVariable.IsStructuredBuffer)
        {
          // There will be a constant buffer with the same name as the resource variable, which will
          // contain the layout
          D3DConstantBuffer? cb = shader.Reflection.GetConstantBufferByName(resourceVariable.Name);
          if (cb is null || cb.Variables.Length == 0)
          {
            AddStructuredBufferError(ctx, resourceVariable, grp, shader);
            return null;
          }

          v.m_structuredBufferLayout = cb;
          v.Final.StructuredBufferLayout = cb.Variables[0].CreateValueVariable(false);
        }

        if (!v.AddAttributeUsage(ctx, shader.Source.CustomAttributes, grp))
          return null;

        return v;
      }

      private static void AddSamplerError(HlslCompileContext ctx, D3DResourceBinding resourceVariable, PendingShaderGroup grp)
      {
        ctx.AddError(grp, $"Resource Variable '{resourceVariable.Name} is inconsistent with previously processed sampler state (possible #ifdef redefinition?).");
      }

      private static void AddStructuredBufferError(HlslCompileContext ctx, D3DResourceBinding resourceVariable, PendingShaderGroup grp, CompiledShader shader, string? reasonWhyNotSame = null)
      {
        if (reasonWhyNotSame is null)
          ctx.AddError(grp, $"Resource Variable '{resourceVariable.Name} is a structured buffer, but there was no structure buffer layout in Shader #{shader.ShaderIndex} ({shader.Key}).");
        else
          ctx.AddError(grp, $"Resource Variable '{resourceVariable.Name} is inconsistent with previously processed structured buffer (possible #ifdef redefinition?). {reasonWhyNotSame}");
      }

      private static bool AreSameSamplers(ShaderData.SamplerStateData? a, ShaderData.SamplerStateData? b)
      {
        if (a.HasValue && b.HasValue)
        {
          ShaderData.SamplerStateData aa = a.Value;
          ShaderData.SamplerStateData bb = b.Value;

          if (aa.AddressU != bb.AddressU
            || aa.AddressV != bb.AddressV
            || aa.AddressW != bb.AddressW
            || aa.ComparisonFunction != bb.ComparisonFunction
            || aa.Filter != bb.Filter
            || aa.MaxAnisotropy != bb.MaxAnisotropy
            || !MathHelper.IsEqual(aa.MipMapLevelOfDetailBias, bb.MipMapLevelOfDetailBias)
            || aa.MinMipMapLevel != bb.MinMipMapLevel
            || aa.MaxMipMapLevel != bb.MaxMipMapLevel
            || !aa.BorderColor.Equals(bb.BorderColor))
          {
            return false;
          }

          return true;
        }

        if (!a.HasValue && !b.HasValue)
          return true;

        return false;
      }
    }

    private sealed class PendingConstantBuffer
    {
      public readonly ShaderData.ConstantBuffer Final;
      public readonly D3DConstantBuffer Initial;

      private PendingConstantBuffer(D3DConstantBuffer initial)
      {
        Initial = initial;
        Final = initial.CreateShaderDataConstantBuffer();
      }

      public bool Validate(HlslCompileContext ctx, D3DConstantBuffer reflected, HlslAttributes customAttributes, PendingShaderGroup grp)
      {
        if (!Initial.AreSame(reflected, true, out string whyNotSame))
        {
          ctx.AddError(grp, $"Two cbuffers called '{Initial.Name}' do not match in layout. Reason: {whyNotSame}. Ensure layouts of shared cbuffers do not change between permutations (e.g. #ifdef usage within the cbuffer), in order to maximize reusability and prevent incompatible configurations at runtime.");
          return false;
        }

        // Update usage of variables
        for (int i = 0; i < reflected.Variables.Length; i++)
        {
          if (reflected.Variables[i].IsUsed)
          {
            ShaderData.ValueVariable v = Final.Variables[i];
            v.Flags |= ShaderData.ShaderVariableFlags.Used;
          }
        }

        // Make sure per-frame attribute is consistent
        bool thisPerFrame = Final.IsUpdatedPerFrame;
        bool otherPerFrame = customAttributes.PerFrameCbuffers.Contains(Final.Name);
        if (thisPerFrame != otherPerFrame)
        {
          ctx.AddError(grp, $"Two cbuffers called '{Initial.Name}' have inconsistenct PerFrame attributes (possible #ifdef redefinition?).");
          return false;
        }

        return AddAttributeUsage(ctx, customAttributes, grp);
      }

      private bool AddAttributeUsage(HlslCompileContext ctx, HlslAttributes attributes, PendingShaderGroup grp)
      {
        // Is this PerFrame?
        if (attributes.PerFrameCbuffers.Contains(Final.Name))
          Final.IsUpdatedPerFrame = true;

        // May have a cb-wide feature name
        attributes.CBufferNameToFeature.TryGetValue(Final.Name, out string? cbFeatureName);

        foreach (ShaderData.ValueVariable v in Final.Variables)
        {
          // Resolve feature name
          attributes.VariableNameToFeature.TryGetValue(v.Name, out string? vfeatureName);

          string? resolvedFeature = vfeatureName ?? cbFeatureName;
          if (resolvedFeature is not null)
          {
            uint featureIndex = ctx.AddFeatureName(resolvedFeature);

            // Validate if it's the same index if the variable already had one
            if (v.FeatureNameIndex.HasValue && v.FeatureNameIndex.Value != featureIndex)
            {
              string expectedFeatureName = ctx.m_featureNames[(int) v.FeatureNameIndex.Value];
              ctx.AddError(grp, $"Variable '{v.Name}' in cbuffer '{Final.Name}' has inconsistent feature (possibly #ifdef usage within the cbuffer?). Has '{resolvedFeature}' but expected '{expectedFeatureName}'.");
              return false;
            }

            v.FeatureNameIndex = featureIndex;
            grp.UsedFeatureIndices.Add(featureIndex);
          } 
          else if (v.FeatureNameIndex.HasValue)
          {
            // Expected a feature name, because we assigned one before. Something is wrong
            string expectedFeatureName = ctx.m_featureNames[(int) v.FeatureNameIndex.Value];
            ctx.AddError(grp, $"Variable '{v.Name}' in cbuffer '{Final.Name}' has inconsistent feature (possibly #ifdef usage within the cbuffer?). Expected '{expectedFeatureName}'.");
            return false;
          }

          // Resolve computed parameter name
          if (attributes.VariableNameToComputedParameter.TryGetValue(v.Name, out string? resolvedComputedParam))
          {
            uint computedParamIndex = ctx.AddComputedParameterName(resolvedComputedParam);

            // Validate if its the same index if the variable already had one
            if (v.ComputedParameterNameIndex.HasValue && v.ComputedParameterNameIndex.Value != computedParamIndex)
            {
              // Expected computed param name, because we assigned one before. Something is wrong
              string expectedParamName = ctx.m_computedParameterNames[(int) v.ComputedParameterNameIndex.Value];
              ctx.AddError(grp, $"Variable '{v.Name}' in cbuffer '{Final.Name}' has inconsistent computed parameter mapping (possibly #ifdef usage within the cbuffer?). Has '{resolvedComputedParam}' but expected {expectedParamName}.");
              return false;
            }

            v.ComputedParameterNameIndex = computedParamIndex;
            grp.UsedComputedParameterIndices.Add(computedParamIndex);
          }
          else if (v.ComputedParameterNameIndex.HasValue)
          {
            string expectedParamName = ctx.m_computedParameterNames[(int) v.ComputedParameterNameIndex.Value];
            ctx.AddError(grp, $"Variable '{v.Name}' in cbuffer '{Final.Name}' has inconsistent computed parameter mapping (possibly #ifdef usage within the cbuffer?). Expected '{expectedParamName}'.");
            return false;
          }
        }

        return true;
      }

      public static PendingConstantBuffer? Create(HlslCompileContext ctx, D3DConstantBuffer reflected, HlslAttributes attributes, PendingShaderGroup grp)
      {
        PendingConstantBuffer cb = new PendingConstantBuffer(reflected);
        if (!cb.AddAttributeUsage(ctx, attributes, grp))
          return null;

        return cb;
      }
    }

    private static D3D.ShaderMacro[]? ConvertShaderMacros(IReadOnlyDictionary<string, ShaderMacro>? macros)
    {
      if (macros is null || macros.Count == 0)
        return null;

      D3D.ShaderMacro[] d3dMacros = new D3D.ShaderMacro[macros.Count];

      int index = 0;
      foreach (ShaderMacro macro in macros.Values)
      {
        d3dMacros[index] = new D3D.ShaderMacro(macro.Name, macro.Definition);
        index++;
      }

      return d3dMacros;
    }

    private static string GetShaderProfile(ShaderStage stage, string profile)
    {
      string prefix;
      switch (stage)
      {
        case ShaderStage.PixelShader:
          prefix = "ps_";
          break;
        case ShaderStage.GeometryShader:
          prefix = "gs_";
          break;
        case ShaderStage.DomainShader:
          prefix = "ds_";
          break;
        case ShaderStage.HullShader:
          prefix = "hs_";
          break;
        case ShaderStage.ComputeShader:
          prefix = "cs_";
          break;
        case ShaderStage.VertexShader:
        default:
          prefix = "vs_";
          break;
      }

      return $"{prefix}{profile}";
    }
  }
}
