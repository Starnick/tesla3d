﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System.IO;
using Tesla.Graphics;
using D3DC = SharpDX.D3DCompiler;
using SDX = SharpDX;

namespace Tesla.Hlsl
{
  // Include adapter, cannot be shared across threads
  internal sealed class IncludeAdapter : SDX.CallbackBase, D3DC.Include
  {
    private IShaderSourceHandler m_handler;

    public IShaderSourceHandler ShaderSourceHandler { get { return m_handler; } }

    public IncludeAdapter(IShaderSourceHandler handler)
    {
      m_handler = handler;
    }

    public void Close(Stream stream)
    {
      stream.Dispose();
    }

    public Stream Open(D3DC.IncludeType type, string fileName, Stream parentStream)
    {
      return m_handler.OpenInclude(fileName, (type == D3DC.IncludeType.System) ? IncludeType.System : IncludeType.Local);
    }
  }

  // Ensures synchronized access to the shader source handler
  internal sealed class SynchronizedShaderSourceHandler : IShaderSourceHandler
  {
    private IShaderSourceHandler m_handler;
    private object m_sync = new object();

    public SynchronizedShaderSourceHandler(IShaderSourceHandler handler)
    {
      m_handler = handler;
    }

    public void Dispose()
    {
      m_handler.Dispose();
    }

    public void CloseInclude(Stream stream)
    {
      lock (m_sync)
        m_handler.CloseInclude(stream);
    }

    public Stream OpenInclude(string fileName, IncludeType includeType)
    {
      lock (m_sync)
        return m_handler.OpenInclude(fileName, includeType);
    }

    public string GetSourceCode(string fileName, IncludeType? includeType = null)
    {
      lock (m_sync)
        return m_handler.GetSourceCode(fileName, includeType);
    }
  }
}
