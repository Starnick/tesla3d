﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using Tesla.Graphics;
using Tesla.Text;

namespace Tesla.Hlsl
{
  /// <summary>
  /// Parses HLSL looking for specific attributes or other data that are not compiled normally by the D3D Compiler.
  /// </summary>
  public sealed class HlslAttributeParser
  {
    /// <summary>
    /// Handles loading include directives.
    /// </summary>
    /// <param name="filePath">Path to the file.</param>
    /// <param name="includeType">Include type.</param>
    /// <returns>Loaded HLSL source code.</returns>
    public delegate string? ReadIncludeFileHandler(string filePath, IncludeType includeType);

    private IReadOnlyList<TokenPattern> m_attributeTokens;
    private IReadOnlyList<TokenPattern> m_keyTokens;
    private IReadOnlyList<TokenPattern> m_quoteOrAngle;
    private IReadOnlyList<TokenPattern> m_endOfAttribute;
    private IReadOnlyList<TokenPattern> m_comaOrEndBracket;
    private IReadOnlyList<TokenPattern> m_possibleEndOfStatement;
    private IReadOnlyList<TokenPattern> m_samplerProperties;
    private IReadOnlyList<TokenPattern> m_addressTokens;
    private IReadOnlyList<TokenPattern> m_compareFuncTokens;
    private IReadOnlyList<TokenPattern> m_filterTokens;
    private IReadOnlyList<TokenPattern> m_float4Tokens;
    private IReadOnlyList<TokenPattern> m_variableTypeDeclTokens;
    private IReadOnlyList<TokenPattern> m_angleBracketOrIdentifier;
    private IReadOnlyList<TokenPattern> m_preProcessBeginTokens;
    private IReadOnlyList<TokenPattern> m_preProcessSkipTokens;
    private IReadOnlyList<TokenPattern> m_definedOrNotDefinedTokens;

    private bool m_preprocessDefines;
    private Stack<string> m_includeStack;

    /// <summary>
    /// Gets or sets if the parser should honor preprocessor defines (e.g. skip portions of the code that are or aren't marked as defined).
    /// </summary>
    public bool PreProcessDefines
    {
      get
      {
        return m_preprocessDefines;
      }
      set
      {
        m_preprocessDefines = value;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="HlslAttributeParser"/> class.
    /// </summary>
    public HlslAttributeParser()
    {
      m_preprocessDefines = true;
      m_includeStack = new Stack<string>();

      m_attributeTokens = HlslTokens.GetPatterns(TokenType.PerFrameAttribute, TokenType.ComputedParameterAttribute, TokenType.FeatureAttribute);
      m_keyTokens = HlslTokens.GetPatterns(TokenType.OpenBracket, TokenType.SamplerState, TokenType.IncludeDirective);
      m_quoteOrAngle = HlslTokens.GetPatterns(TokenType.QuotationMark, TokenType.OpenAngleBracket);
      m_endOfAttribute = HlslTokens.GetPatterns(TokenType.OpenParentheses, TokenType.Coma, TokenType.CloseBracket);
      m_comaOrEndBracket = HlslTokens.GetPatterns(TokenType.Coma, TokenType.CloseBracket);
      m_possibleEndOfStatement = HlslTokens.GetPatterns(TokenType.Semicolon, TokenType.OpenCurlyBrace, TokenType.Colon);
      m_samplerProperties = HlslTokens.GetPatterns(TokenType.AddressU, TokenType.AddressV, TokenType.AddressW, TokenType.BorderColor, TokenType.Filter, TokenType.MaxAnisotropy, TokenType.MaxLOD, TokenType.MinLOD, TokenType.MipLODBias, TokenType.ComparisonFunc, TokenType.CloseCurlyBrace);
      m_addressTokens = HlslTokens.GetPatterns(TokenType.Wrap, TokenType.Border, TokenType.Clamp, TokenType.Mirror, TokenType.Mirror_Once);
      m_compareFuncTokens = HlslTokens.GetPatterns(TokenType.NEVER, TokenType.LESS, TokenType.EQUAL, TokenType.LESS_EQUAL, TokenType.GREATER, TokenType.NOT_EQUAL, TokenType.GREATER_EQUAL, TokenType.ALWAYS);
      m_filterTokens = HlslTokens.GetPatterns(TokenType.MIN_MAG_MIP_POINT, TokenType.MIN_MAG_POINT_MIP_LINEAR, TokenType.MIN_POINT_MAG_LINEAR_MIP_POINT, TokenType.MIN_POINT_MAG_MIP_LINEAR,
        TokenType.MIN_LINEAR_MAG_MIP_POINT, TokenType.MIN_LINEAR_MAG_POINT_MIP_LINEAR, TokenType.MIN_MAG_LINEAR_MIP_POINT, TokenType.MIN_MAG_MIP_LINEAR, TokenType.ANISOTROPIC,
        TokenType.COMPARISON_MIN_MAG_MIP_POINT, TokenType.COMPARISON_MIN_MAG_POINT_MIP_LINEAR, TokenType.COMPARISON_MIN_POINT_MAG_LINEAR_MIP_POINT, TokenType.COMPARISON_MIN_POINT_MAG_MIP_LINEAR,
        TokenType.COMPARISON_MIN_LINEAR_MAG_MIP_POINT, TokenType.COMPARISON_MIN_LINEAR_MAG_POINT_MIP_LINEAR, TokenType.COMPARISON_MIN_MAG_LINEAR_MIP_POINT, TokenType.COMPARISON_MIN_MAG_MIP_LINEAR, TokenType.COMPARISON_ANISOTROPIC);
      m_float4Tokens = HlslTokens.GetPatterns(TokenType.Float4, TokenType.OpenParentheses, TokenType.Number, TokenType.Coma, TokenType.CloseParentheses, TokenType.Semicolon);
      m_variableTypeDeclTokens = HlslTokens.GetPatterns(TokenType.Const, TokenType.RowMajor, TokenType.ColumnMajor, TokenType.Extern, TokenType.NoInterpolation, TokenType.Precise, TokenType.Shared, TokenType.GroupShared, TokenType.Static, TokenType.Uniform, TokenType.Volatile, TokenType.Identifier);
      m_angleBracketOrIdentifier = HlslTokens.GetPatterns(TokenType.OpenAngleBracket, TokenType.Identifier);
      m_preProcessBeginTokens = HlslTokens.GetPatterns(TokenType.PreProcess_If, TokenType.PreProcess_IfDef, TokenType.PreProcess_IfNDef, TokenType.PreProcess_Define, TokenType.PreProcess_UnDef);
      m_preProcessSkipTokens = HlslTokens.GetPatterns(TokenType.PreProcess_If, TokenType.PreProcess_IfDef, TokenType.PreProcess_IfNDef, TokenType.PreProcess_EndIf, TokenType.PreProcess_Elif, TokenType.PreProcess_Else);
      m_definedOrNotDefinedTokens = HlslTokens.GetPatterns(TokenType.PreProcess_Defined, TokenType.PreProcess_NotDefined);
    }

    /// <summary>
    /// Parses HLSL text for custom attributes and sampler data.
    /// </summary>
    /// <param name="hlslText">HSL source code.</param>
    /// <param name="includeHandler">Handler for finding and loading HLSL source code for include directives.</param>
    /// <returns>HLSL attributes.</returns>
    public HlslAttributes Parse(string hlslText, ReadIncludeFileHandler? includeHandler = null, IEnumerable<string>? defines = null)
    {
      m_includeStack.Clear();

      HlslAttributes attributes = new HlslAttributes();
      ParseHlsl(hlslText, attributes, includeHandler, defines);

      return attributes;
    }

    private void ParseHlsl(string hlslText, HlslAttributes attributes, ReadIncludeFileHandler? includeHandler, IEnumerable<string>? defines)
    {
      StringTokenizer scanner = HlslTokens.CreateScanner();
      scanner.Initialize(hlslText);

      HashSet<string> defineSet = (defines is not null) ? new HashSet<string>(defines) : new HashSet<string>();

      while (scanner.MoveToNextToken())
        ProcessCurrentToken(scanner, attributes, includeHandler, defineSet);
    }

    private bool ProcessCurrentToken(StringTokenizer scanner, HlslAttributes attributes, ReadIncludeFileHandler? includeHandler, HashSet<string> defines)
    {
      Token currToken = scanner.Token.Classify(m_keyTokens);
      switch ((TokenType) currToken.TokenType)
      {
        case TokenType.IncludeDirective:
          if (includeHandler is not null)
            ParseIncludeDirective(scanner, attributes, includeHandler, defines);
          return true;
        case TokenType.SamplerState:
          ParseSampler(scanner, attributes);
          return true;
        case TokenType.OpenBracket:
          ParseAttribute(scanner, attributes);
          return true;
        default:
          return TryParsePreProcessDefines(scanner, attributes, includeHandler, defines);
      }
    }

    private bool TryParsePreProcessDefines(StringTokenizer scanner, HlslAttributes attributes, ReadIncludeFileHandler? includeHandler, HashSet<string> defines)
    {
      if (!m_preprocessDefines)
        return false;

      Token possiblePP = scanner.Token.Classify(m_preProcessBeginTokens);
      switch ((TokenType) possiblePP.TokenType)
      {
        case TokenType.PreProcess_Define:
          ParsePPDefine(scanner, defines);
          return true;
        case TokenType.PreProcess_UnDef:
          ParsePPUndefine(scanner, defines);
          return true;
        // For now, no nesting supported. When we encounter an if block, we scan or skip to the endif
        case TokenType.PreProcess_If:
          ParsePPIf(scanner, attributes, includeHandler, defines);
          return true;
        case TokenType.PreProcess_IfDef:
          ParsePPIfDef(scanner, attributes, includeHandler, defines);
          return true;
        case TokenType.PreProcess_IfNDef:
          ParsePPIfNDef(scanner, attributes, includeHandler, defines);
          return true;
      }

      return false;
    }

    private void ParsePPDefine(StringTokenizer scanner, HashSet<string> defines)
    {
      Token identifier = scanner.ScanNextToken(HlslTokens.GetPattern(TokenType.Identifier));
      ValidateToken(identifier, TokenType.Identifier);

      defines.Add(identifier.Text.ToString());
    }

    private void ParsePPUndefine(StringTokenizer scanner, HashSet<string> defines)
    {
      Token identifier = scanner.ScanNextToken(HlslTokens.GetPattern(TokenType.Identifier));
      ValidateToken(identifier, TokenType.Identifier);

      defines.Remove(identifier.Text.ToString());
    }

    private void ParsePPIf(StringTokenizer scanner, HlslAttributes attributes, ReadIncludeFileHandler? includeHandler, HashSet<string> defines)
    {
      Token defined = scanner.ScanNextToken(m_definedOrNotDefinedTokens);
      switch ((TokenType) defined.TokenType)
      {
        case TokenType.PreProcess_NotDefined:
        case TokenType.PreProcess_Defined:
          ScanAndValidateNextToken(scanner, TokenType.OpenParentheses);
          Token identifier = scanner.ScanNextToken(HlslTokens.GetPattern(TokenType.Identifier));
          ValidateToken(identifier, TokenType.Identifier);
          ScanAndValidateNextToken(scanner, TokenType.CloseParentheses);

          ParsePPIfBody(scanner, attributes, includeHandler, defines, identifier.Text.ToString(), defined.TokenType == (int) TokenType.PreProcess_Defined);
          break;
      }
    }

    private void ParsePPIfDef(StringTokenizer scanner, HlslAttributes attributes, ReadIncludeFileHandler? includeHandler, HashSet<string> defines)
    {
      Token identifier = scanner.ScanNextToken(HlslTokens.GetPattern(TokenType.Identifier));
      ValidateToken(identifier, TokenType.Identifier);

      ParsePPIfBody(scanner, attributes, includeHandler, defines, identifier.Text.ToString(), true);
    }

    private void ParsePPIfNDef(StringTokenizer scanner, HlslAttributes attributes, ReadIncludeFileHandler? includeHandler, HashSet<string> defines)
    {
      Token identifier = scanner.ScanNextToken(HlslTokens.GetPattern(TokenType.Identifier));
      ValidateToken(identifier, TokenType.Identifier);

      ParsePPIfBody(scanner, attributes, includeHandler, defines, identifier.Text.ToString(), false);
    }

    private void ParsePPIfBody(StringTokenizer scanner, HlslAttributes attributes, ReadIncludeFileHandler? includeHandler, HashSet<string> defines, string define, bool checkIsDefined)
    {
      bool consumeBody = (checkIsDefined) ? defines.Contains(define) : !defines.Contains(define);

      if (consumeBody)
      {
        ConsumeIfBody(scanner, attributes, includeHandler, defines);
      }
      else
      {
        // Skip to endif, elif, or else
        while (scanner.MoveToNextToken())
        {
          Token next = scanner.Token.Classify(m_preProcessSkipTokens);
          switch ((TokenType) next.TokenType)
          {
            case TokenType.PreProcess_If:
            case TokenType.PreProcess_IfDef:
            case TokenType.PreProcess_IfNDef:
              ThrowException(next, "Nested #ifdef is not supported.");
              break;
            case TokenType.PreProcess_Else:
              ConsumeIfBody(scanner, attributes, includeHandler, defines);
              return;
            case TokenType.PreProcess_Elif:
              ParsePPIf(scanner, attributes, includeHandler, defines);
              return;
            case TokenType.PreProcess_EndIf:
              return;            
          }
        }
      }
    }

    private void ConsumeIfBody(StringTokenizer scanner, HlslAttributes attributes, ReadIncludeFileHandler? includeHandler, HashSet<string> defines)
    {
      while (scanner.MoveToNextToken())
      {
        Token next = scanner.Token.Classify(m_preProcessSkipTokens);
        switch ((TokenType) next.TokenType)
        {
          case TokenType.PreProcess_If:
          case TokenType.PreProcess_IfDef:
          case TokenType.PreProcess_IfNDef:
            ThrowException(next, "Nested #ifdef is not supported.");
            break;
          case TokenType.PreProcess_Else:
          case TokenType.PreProcess_Elif:
            SkipToEndIf(scanner);
            return;
          case TokenType.PreProcess_EndIf:
            return;
          default:
            ProcessCurrentToken(scanner, attributes, includeHandler, defines);
            break;
        }
      }
    }

    private void SkipToEndIf(StringTokenizer scanner)
    {
      TokenPattern endIf = HlslTokens.GetPattern(TokenType.PreProcess_EndIf);
      while (scanner.MoveToNextToken())
      {
        Token next = scanner.Token.Classify(endIf);
        if (next.TokenType == (int) TokenType.PreProcess_EndIf)
          return;
      }

      if (scanner.IsEOF)
        ThrowException(scanner.Token, "No matching #endif found, unexpected EOF.");
    }

    private void ParseAttribute(StringTokenizer scanner, HlslAttributes attributes)
    {
      Token attrName = scanner.ScanNextToken(m_attributeTokens);

      switch ((TokenType) attrName.TokenType)
      {
        case TokenType.PerFrameAttribute:
          // [PerFrame]
          ParsePerFrameAttribute(scanner, attributes);
          break;
        case TokenType.ComputedParameterAttribute:
          // [ComputedParameter("ParamName")]
          ParseComputedParameterAttribute(scanner, attributes);
          break;
        case TokenType.FeatureAttribute:
          // [Feature] or [Feature("FeatureName")]
          ParseFeatureAttribute(scanner, attributes);
          break;
        }
    }

    private void ParsePerFrameAttribute(StringTokenizer scanner, HlslAttributes attributes)
    {
      ScanAndValidateNextToken(scanner, TokenType.CloseBracket);
      ScanAndValidateNextToken(scanner, TokenType.CBuffer);

      Token cbufferName = ScanNextToken(scanner, TokenType.Identifier);
      ValidateToken(cbufferName, TokenType.Identifier);

      attributes.PerFrameCbuffers.Add(cbufferName.Text.ToString());
    }

    private string? ParseFeatureName(StringTokenizer scanner, bool scanAttributeName, out Token next)
    {
      if (scanAttributeName)
        ScanAndValidateNextToken(scanner, TokenType.FeatureAttribute);

      // Feature can have a name, if not there then only a coma or close bracket is valid
      next = scanner.ScanNextToken(m_endOfAttribute);
      switch ((TokenType) next.TokenType)
      {
        case TokenType.Coma:
        case TokenType.CloseBracket:
          return null;
        case TokenType.OpenParentheses:
          // Extract the name
          ScanAndValidateNextToken(scanner, TokenType.QuotationMark);
          Token featureNameToken = scanner.ScanNextToken(HlslTokens.GetPattern(TokenType.Identifier));
          ValidateToken(featureNameToken, TokenType.Identifier);
          ScanAndValidateNextToken(scanner, TokenType.QuotationMark);
          ScanAndValidateNextToken(scanner, TokenType.CloseParentheses);

          // Validate the next token
          next = scanner.ScanNextToken(m_comaOrEndBracket);
          if (next.TokenType == -1)
            ThrowException(next, $"Unexpected Feature attribute syntax.");

          return featureNameToken.Text.ToString();
        default:
          ThrowException(next, $"Unexpected Feature attribute syntax.");
          return null;
      }
    }

    private void ParseFeatureAttribute(StringTokenizer scanner, HlslAttributes attributes)
    {
      string? featureName = ParseFeatureName(scanner, false, out Token next);
      string? computedParamName = null;

      // Have another attribute, only computed parameter is allowed
      if (next.TokenType == (int) TokenType.Coma)
        computedParamName = ParseComputedParameterName(scanner, true, out next);

      // Validate end of attributes
      ValidateToken(next, TokenType.CloseBracket);

      string variableName = SkipVariableTypeScanNameOrCBuffer(scanner, out bool isCBuffer);

      // If no feature name, then it's the variable name
      if (featureName is null)
        featureName = variableName;

      if (isCBuffer)
        attributes.CBufferNameToFeature.Add(variableName, featureName);
      else
        attributes.VariableNameToFeature.Add(variableName, featureName);

      // If have a computed parameter inline attribute, validate and assign
      if (computedParamName is not null)
      {
        // Computed parameters on a cbuffer doesnt make sense
        if (isCBuffer)
          ThrowException(next, $"'{variableName}' is a CBuffer and therefore not permitted to have a ComputedParameter '{computedParamName}' attribute.");
        
        attributes.VariableNameToComputedParameter.Add(variableName, computedParamName);
      }
    }

    private string ParseComputedParameterName(StringTokenizer scanner, bool scanAttributeName, out Token next)
    {
      if (scanAttributeName)
        ScanAndValidateNextToken(scanner, TokenType.ComputedParameterAttribute);

      ScanAndValidateNextToken(scanner, TokenType.OpenParentheses);
      ScanAndValidateNextToken(scanner, TokenType.QuotationMark);

      Token computedParamName = ScanNextToken(scanner, TokenType.Identifier);
      ValidateToken(computedParamName, TokenType.Identifier);

      ScanAndValidateNextToken(scanner, TokenType.QuotationMark);
      ScanAndValidateNextToken(scanner, TokenType.CloseParentheses);

      // Validate the next token
      next = scanner.ScanNextToken(m_comaOrEndBracket);
      if (next.TokenType == -1)
        ThrowException(next, $"Unexpected ComputedParameter attribute syntax.");

      return computedParamName.Text.ToString();
    }

    private void ParseComputedParameterAttribute(StringTokenizer scanner, HlslAttributes attributes)
    {
      string computedParamName = ParseComputedParameterName(scanner, false, out Token next);
      string? featureName = null;
      bool hasFeature = false;

      // If next token is a coma, may have a feature attribute
      if (next.TokenType == (int) TokenType.Coma)
      {
        featureName = ParseFeatureName(scanner, true, out next);
        hasFeature = true;
      }

      // Validate end of attributes
      ValidateToken(next, TokenType.CloseBracket);

      // Get and assign the variable name
      string variableName = SkipVariableTypeScanName(scanner);
      attributes.VariableNameToComputedParameter.Add(variableName, computedParamName);

      // If has a feature, assign that
      if (hasFeature)
      {
        // Variable name may be the feature name
        if (featureName is null)
          featureName = variableName;

        attributes.VariableNameToFeature.Add(variableName, featureName);
      }
    }

    private bool ParseIfNextAttribute(StringTokenizer scanner)
    {
      Token next = scanner.ScanNextToken(m_comaOrEndBracket);

      switch ((TokenType) next.TokenType)
      {
        case TokenType.Coma:
          return true;
        case TokenType.CloseBracket:
          return false;
        default:
          ThrowException(next, $"Expected end of attribute.");
          return false;
      }
    }

    private string SkipVariableTypeScanNameOrCBuffer(StringTokenizer scanner, out bool isCbuffer)
    {
      isCbuffer = false;
      Token next = scanner.ScanNextToken(m_variableTypeDeclTokens);

      // Check if actually a cbuffer
      if (next.Classify(HlslTokens.GetPattern(TokenType.CBuffer)).TokenType == (int) TokenType.CBuffer)
      {
        isCbuffer = true;

        Token cbufferName = ScanNextToken(scanner, TokenType.Identifier);
        ValidateToken(cbufferName, TokenType.Identifier);

        return cbufferName.Text.ToString();
      }

      return SkipVariableTypeInternal(scanner, next);
    }

    private string SkipVariableTypeScanName(StringTokenizer scanner)
    {
      Token next = scanner.ScanNextToken(m_variableTypeDeclTokens);
      return SkipVariableTypeInternal(scanner, next);
    }

    private string SkipVariableTypeInternal(StringTokenizer scanner, Token next)
    {
      switch ((TokenType) next.TokenType)
      {
        case TokenType.RowMajor:
        case TokenType.ColumnMajor:
        case TokenType.Extern:
        case TokenType.NoInterpolation:
        case TokenType.Precise:
        case TokenType.Shared:
        case TokenType.GroupShared:
        case TokenType.Uniform:
        case TokenType.Volatile:
          // Consume
          return SkipVariableTypeInternal(scanner, next);
        case TokenType.Const:
        case TokenType.Static:
          ThrowException(next, $"Cannot mark a static/const variable with an attribute.");
          break;
        case TokenType.Identifier:
          // Should be the type name, it may have samples or an underlying type, so check if next token is an angle bracket
          next = scanner.ScanNextToken(m_angleBracketOrIdentifier);
          if (next.TokenType == (int) TokenType.OpenAngleBracket)
          {
            // Move until we hit the closing angle bracket, then the next token should be the variable name
            while (scanner.MoveToNextToken())
            {
              next = scanner.Token.Classify(HlslTokens.GetPattern(TokenType.CloseAngleBracket));
              if (next.TokenType == (int) TokenType.CloseAngleBracket)
                next = scanner.ScanNextToken(HlslTokens.GetPattern(TokenType.Identifier));
            }

            if (scanner.IsEOF)
              ThrowException(scanner.Token, $"Unexpected end of file.");
          }
          ValidateToken(next, TokenType.Identifier);
          break;
        default:
          ThrowException(next, $"Unexpected variable declaration.");
          break;
      }

      return next.Text.ToString();
    }

    private void ParseSampler(StringTokenizer scanner, HlslAttributes attributes)
    {
      Token identifier = scanner.ScanNextToken(HlslTokens.GetPattern(TokenType.Identifier));

      // Fault tolerant...sampler may appear
      if (identifier.TokenType != (int) TokenType.Identifier)
        return;

      HlslSamplerState sampler = new HlslSamplerState(identifier.Text.ToString());
      attributes.Samplers.Add(sampler.Name, sampler);

      Token next = scanner.ScanNextToken(m_possibleEndOfStatement);
      switch ((TokenType) next.TokenType)
      {
        // Sometimes the sampler is set to a register, skip over it
        case TokenType.Colon:
          if (!scanner.MoveToNextToken())
            return;

          // Next should be a curly brace if it defines any data
          next = scanner.ScanNextToken(m_possibleEndOfStatement);
          if (next.TokenType == (int) TokenType.Semicolon)
            return;
          break;
        case TokenType.Semicolon:
          return;
      }

      ValidateToken(next, TokenType.OpenCurlyBrace);

      while (scanner.MoveToNextToken())
      {
        next = scanner.Token.Classify(m_samplerProperties);
        switch ((TokenType) next.TokenType)
        {
          case TokenType.CloseCurlyBrace:
            return;
          case TokenType.AddressU:
            sampler.AddressU = ParseAddresMode(scanner);
            break;
          case TokenType.AddressV:
            sampler.AddressV = ParseAddresMode(scanner);
            break;
          case TokenType.AddressW:
            sampler.AddressW = ParseAddresMode(scanner);
            break;
          case TokenType.BorderColor:
            sampler.BorderColor = ParseBorderColor(scanner);
            break;
          case TokenType.Filter:
            sampler.Filter = ParseFilter(scanner);
            break;
          case TokenType.MaxAnisotropy:
            sampler.MaxAnisotropy = ParseInt(scanner);
            break;
          case TokenType.MaxLOD:
            float val = ParseFloat(scanner);
            if (val == float.MaxValue)
              sampler.MaxMipMapLevel = int.MaxValue;
            else
              sampler.MaxMipMapLevel = (int) val;
            break;
          case TokenType.MinLOD:
            sampler.MinMipMapLevel = (int) ParseFloat(scanner);
            break;
          case TokenType.MipLODBias:
            sampler.MipMapLevelOfDetailBias = ParseFloat(scanner);
            break;
          case TokenType.ComparisonFunc:
            sampler.ComparisonFunction = ParseComparisonFunction(scanner);
            break;
        }
      }
    }

    private TextureAddressMode ParseAddresMode(StringTokenizer scanner)
    {
      ScanAndValidateNextToken(scanner, TokenType.Equals);

      Token addressMode = scanner.ScanNextToken(m_addressTokens);
      switch ((TokenType) addressMode.TokenType)
      {
        case TokenType.Wrap:
          return TextureAddressMode.Wrap;
        case TokenType.Clamp:
          return TextureAddressMode.Clamp;
        case TokenType.Mirror:
          return TextureAddressMode.Mirror;
        case TokenType.Mirror_Once:
          return TextureAddressMode.MirrorOnce;
        default:
          ThrowException(addressMode, $"Unexpected address mode.");
          break;
      }

      return TextureAddressMode.Clamp;
    }

    private Color ParseBorderColor(StringTokenizer scanner)
    {
      ScanAndValidateNextToken(scanner, TokenType.Equals);

      Span<float> values = stackalloc float[4];
      int index = 0;

      while (scanner.MoveToNextToken())
      {
        Token next = scanner.Token.Classify(m_float4Tokens);
        switch ((TokenType) next.TokenType)
        {
          case TokenType.Float4:
          case TokenType.OpenParentheses:
          case TokenType.CloseParentheses:
          case TokenType.Coma:
            break;
          case TokenType.Number:
            if (index > 3)
              ThrowException(next, $"Unexpected BorderColor token.");

            values[index] = float.Parse(next.Text);
            index++;
            break;
          case TokenType.Semicolon:
            return new Color(values[0], values[1], values[2], values[3]);
          default:
            ThrowException(next, $"Unexpected BorderColor token.");
            break;

        }
      }

      return Color.White;
    }

    private TextureFilter ParseFilter(StringTokenizer scanner)
    {
      ScanAndValidateNextToken(scanner, TokenType.Equals);

      Token filter = scanner.ScanNextToken(m_filterTokens);
      switch ((TokenType) filter.TokenType)
      {
        case TokenType.MIN_MAG_MIP_POINT:
          return TextureFilter.Point;
        case TokenType.MIN_MAG_POINT_MIP_LINEAR:
          return TextureFilter.PointMipLinear;
        case TokenType.MIN_POINT_MAG_LINEAR_MIP_POINT:
          return TextureFilter.MinPointMagLinearMipPoint;
        case TokenType.MIN_POINT_MAG_MIP_LINEAR:
          return TextureFilter.MinPointMagLinearMipLinear;
        case TokenType.MIN_LINEAR_MAG_MIP_POINT:
          return TextureFilter.MinLinearMagPointMipPoint;
        case TokenType.MIN_LINEAR_MAG_POINT_MIP_LINEAR:
          return TextureFilter.MinLinearMagPointMipLinear;
        case TokenType.MIN_MAG_LINEAR_MIP_POINT:
          return TextureFilter.LinearMipPoint;
        case TokenType.MIN_MAG_MIP_LINEAR:
          return TextureFilter.Linear;
        case TokenType.ANISOTROPIC:
          return TextureFilter.Anisotropic;
        case TokenType.COMPARISON_MIN_MAG_MIP_POINT:
          return TextureFilter.Comparison_Point;
        case TokenType.COMPARISON_MIN_MAG_POINT_MIP_LINEAR:
          return TextureFilter.Comparison_PointMipLinear;
        case TokenType.COMPARISON_MIN_POINT_MAG_LINEAR_MIP_POINT:
          return TextureFilter.Comparison_MinPointMagLinearMipPoint;
        case TokenType.COMPARISON_MIN_POINT_MAG_MIP_LINEAR:
          return TextureFilter.Comparison_MinPointMagLinearMipLinear;
        case TokenType.COMPARISON_MIN_LINEAR_MAG_MIP_POINT:
          return TextureFilter.Comparison_MinLinearMagPointMipPoint;
        case TokenType.COMPARISON_MIN_LINEAR_MAG_POINT_MIP_LINEAR:
          return TextureFilter.Comparison_MinLinearMagPointMipLinear;
        case TokenType.COMPARISON_MIN_MAG_LINEAR_MIP_POINT:
          return TextureFilter.Comparison_LinearMipPoint;
        case TokenType.COMPARISON_MIN_MAG_MIP_LINEAR:
          return TextureFilter.Comparison_Linear;
        case TokenType.COMPARISON_ANISOTROPIC:
          return TextureFilter.Comparison_Anisotropic;
        default:
          ThrowException(filter, $"Unexpected filter value.");
          break;
      }

      return TextureFilter.Linear;
    }

    private ComparisonFunction ParseComparisonFunction(StringTokenizer scanner)
    {
      ScanAndValidateNextToken(scanner, TokenType.Equals);

      Token compareFunc = scanner.ScanNextToken(m_compareFuncTokens);
      switch ((TokenType) compareFunc.TokenType)
      {
        case TokenType.NEVER:
          return ComparisonFunction.Never;
        case TokenType.LESS:
          return ComparisonFunction.Less;
        case TokenType.EQUAL:
          return ComparisonFunction.Equal;
        case TokenType.LESS_EQUAL:
          return ComparisonFunction.LessEqual;
        case TokenType.GREATER:
          return ComparisonFunction.Greater;
        case TokenType.NOT_EQUAL:
          return ComparisonFunction.NotEqual;
        case TokenType.GREATER_EQUAL:
          return ComparisonFunction.GreaterEqual;
        case TokenType.ALWAYS:
          return ComparisonFunction.Always;
        default:
          ThrowException(compareFunc, $"Unexpected comparison function value.");
          break;
      }

      return ComparisonFunction.Never;
    }

    private float ParseFloat(StringTokenizer scanner)
    {
      ScanAndValidateNextToken(scanner, TokenType.Equals);

      Token value = scanner.ReadNextToken();
      if (!value.IsValid)
        ThrowException(value, $"Expected a number.");

      return float.Parse(value.Text, CultureInfo.InvariantCulture);
    }

    private int ParseInt(StringTokenizer scanner)
    {
      ScanAndValidateNextToken(scanner, TokenType.Equals);

      Token value = scanner.ReadNextToken();
      if (!value.IsValid)
        ThrowException(value, $"Expected a number.");

      if (value.StartsWith("0x", StringComparison.InvariantCulture))
        return int.Parse(value.Text.Slice(2), System.Globalization.NumberStyles.HexNumber);

      return int.Parse(value.Text);
    }

    private void ParseIncludeDirective(StringTokenizer scanner, HlslAttributes attributes, ReadIncludeFileHandler includeHandler, HashSet<string> defines)
    {
      Token nextToken = scanner.ScanNextToken(m_quoteOrAngle);
      string? filePath = null;
      IncludeType includeType = IncludeType.Local;
      switch ((TokenType) nextToken.TokenType)
      {
        case TokenType.QuotationMark:
          {
            Token pathToken = scanner.ReadNextToken(true);
            ScanAndValidateNextToken(scanner, TokenType.QuotationMark);

            includeType = IncludeType.Local;
            filePath = pathToken.Text.ToString();
          }
          break;
        case TokenType.OpenAngleBracket:
          {
            Token pathToken = scanner.ReadNextToken(true);
            ScanAndValidateNextToken(scanner, TokenType.CloseAngleBracket);

            includeType = IncludeType.System;
            filePath = pathToken.Text.ToString();
          }
          break;
        default:
          ThrowException(nextToken, $"Expected include statement, instead was {nextToken.Text}.");
          break;
      }

      if (filePath is not null)
      {
        string? hlslText = includeHandler(filePath, includeType);
        if (hlslText is null)
          ThrowException(nextToken, $"Could not locate include file '{filePath}'.");

        m_includeStack.Push(filePath);
        try
        {
          ParseHlsl(hlslText, attributes, includeHandler, defines);
        }
        finally
        {
          m_includeStack.Pop();
        }
      }
    }

    private void ScanAndValidateNextToken(StringTokenizer scanner, TokenType expectedType)
    {
      ValidateToken(scanner.ScanNextToken(HlslTokens.GetPattern(expectedType)), expectedType);
    }

    private Token ScanNextToken(StringTokenizer scanner, TokenType tokenType)
    {
      return scanner.ScanNextToken(HlslTokens.GetPattern(tokenType));
    }

    private void ValidateToken(Token token, TokenType expectedType)
    {
      if (token.TokenType != (int) expectedType)
        ThrowException(token, $"Expected {expectedType}, instead was {token.Text}.");
    }

    [DoesNotReturn]
    private void ThrowException(Token token, string msg)
    {
      if (m_includeStack.Count > 0)
        throw new Exception($"[{m_includeStack.Peek()}] Error at Line {token.LineNumber}, Column {token.ColumnNumber}:\n{msg}.");
      else
        throw new Exception($"Error at Line {token.LineNumber}, Column {token.ColumnNumber}:\n{msg}.");
    }
  }
}
