﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Graphics;
using D3D = SharpDX.D3DCompiler;

namespace Tesla.Hlsl
{
  public sealed class D3DShaderVariableMember
  {
    //From ShaderTypeDescription
    private string m_name;
    private ShaderData.ShaderVariableClass m_class;
    private uint m_numColumns;
    private uint m_numRows;
    private uint m_offsetFromParent; //From parent structure (if member)
    private uint m_sizeInBytes; //Computed based on parent
    private uint m_alignedElementSizeInBytes; //Computed, actual 4-byte aligned size of individual array element
    private ShaderData.ShaderVariableType m_type;
    private uint m_elementCount;

    //From ShaderReflectionType
    private D3DShaderVariableMember[] m_members;

    public string Name { get { return m_name; } }

    public ShaderData.ShaderVariableType VariableType { get { return m_type; } }

    public ShaderData.ShaderVariableClass VariableClass { get { return m_class; } }

    public uint ColumnCount { get { return m_numColumns; } }

    public uint RowCount { get { return m_numRows; } }

    public uint OffsetFromParent { get { return m_offsetFromParent; } }

    public uint SizeInBytes { get { return m_sizeInBytes; } }

    public uint AlignedElementSizeInBytes { get { return m_alignedElementSizeInBytes; } }

    public uint ElementCount { get { return m_elementCount; } }

    public D3DShaderVariableMember[] Members { get { return m_members; } }

    public D3DShaderVariableMember(String typeName, D3D.ShaderReflectionType memberType)
    {
      D3D.ShaderTypeDescription desc = memberType.Description;

      m_name = typeName;
      m_class = (ShaderData.ShaderVariableClass) desc.Class;
      m_numColumns = (uint) desc.ColumnCount;
      m_numRows = (uint) desc.RowCount;
      m_offsetFromParent = (uint)desc.Offset;
      m_type = (ShaderData.ShaderVariableType) desc.Type;
      m_elementCount = (uint) desc.ElementCount;

      ComputeSizeInBytes();

      m_members = new D3DShaderVariableMember[desc.MemberCount];

      for (int i = 0; i < m_members.Length; i++)
      {
        using D3D.ShaderReflectionType memberMemberType = memberType.GetMemberType(i);
        string memberMemberName = memberType.GetMemberTypeName(i);
        m_members[i] = new D3DShaderVariableMember(memberMemberName, memberMemberType);
      }
    }

    public bool AreSame(D3DShaderVariableMember member, out String reasonWhyNotSame)
    {
      if (!m_name.Equals(member.m_name, StringComparison.InvariantCulture))
      {
        reasonWhyNotSame = "Type name not the same";
        return false;
      }

      if (m_class != member.VariableClass
          || m_numColumns != member.ColumnCount
          || m_numRows != member.RowCount
          || m_offsetFromParent != member.OffsetFromParent
          || m_sizeInBytes != member.SizeInBytes
          || m_type != member.VariableType
          || m_elementCount != member.ElementCount)
      {
        reasonWhyNotSame = "Type not the same";
        return false;
      }

      D3DShaderVariableMember[] members = member.Members;

      if (m_members.Length != members.Length)
      {
        reasonWhyNotSame = "Member count not the same";
        return false;
      }

      for (int i = 0; i < m_members.Length; i++)
      {
        if (!m_members[i].AreSame(members[i], out reasonWhyNotSame))
        {
          reasonWhyNotSame = $"Member[{i}] not the same:\n{reasonWhyNotSame}";
          return false;
        }
      }

      reasonWhyNotSame = "";
      return true;
    }

    public ShaderData.ValueVariableMember CreateValueVariableMember()
    {
      ShaderData.ValueVariableMember m = new ShaderData.ValueVariableMember();
      m.Name = Name;
      m.VariableType = VariableType;
      m.VariableClass = VariableClass;
      m.SizeInBytes = SizeInBytes;
      m.AlignedElementSizeInBytes = AlignedElementSizeInBytes;
      m.RowCount = RowCount;
      m.ColumnCount = ColumnCount;
      m.ElementCount = ElementCount;
      m.OffsetFromParentStructure = OffsetFromParent;

      if (Members.Length > 0)
      {
        m.Members = new ShaderData.ValueVariableMember[Members.Length];
        for (int i = 0; i < Members.Length; i++)
          m.Members[i] = Members[i].CreateValueVariableMember();
      }

      return m;
    }

    private void ComputeSizeInBytes()
    {
      m_sizeInBytes = (m_numColumns * m_numRows) * GetSizeOfType();
      if (m_elementCount > 0)
        m_sizeInBytes *= m_elementCount;

      m_alignedElementSizeInBytes = D3DShaderVariable.CalculateAlignedElementSize(m_sizeInBytes, m_elementCount);
    }

    private uint GetSizeOfType()
    {
      if (m_class == ShaderData.ShaderVariableClass.Object || m_class == ShaderData.ShaderVariableClass.InterfaceClass || m_class == ShaderData.ShaderVariableClass.InterfacePointer)
        return 0;

      if (m_class == ShaderData.ShaderVariableClass.Struct)
        return 4;

      switch (this.m_type)
      {
        case ShaderData.ShaderVariableType.Bool:
        case ShaderData.ShaderVariableType.Int:
        case ShaderData.ShaderVariableType.Float:
        case ShaderData.ShaderVariableType.UInt:
          return 4;

        case ShaderData.ShaderVariableType.UInt8:
          return 2;

        case ShaderData.ShaderVariableType.Double:
          return 8;
      }

      return 0;
    }
  }
}