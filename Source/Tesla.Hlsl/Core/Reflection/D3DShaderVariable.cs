﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Graphics;
using D3D = SharpDX.D3DCompiler;

namespace Tesla.Hlsl
{
  public class D3DShaderVariable
  {
    //From ShaderVariableDescription
    private string m_name;
    private uint m_sizeInBytes;
    private uint m_startOffset; //From Constant Buffer
    private ShaderData.ShaderVariableFlags m_flags;
    private bool m_isUsed;
    private uint m_startSampler;
    private uint m_samplerSize;
    private uint m_startTexture;
    private uint m_textureSize;
    private ReadOnlyMemory<byte> m_defaultValue;

    //From ShaderTypeDescription
    private ShaderData.ShaderVariableClass m_class;
    private uint m_numColumns;
    private uint m_numRows;
    private ShaderData.ShaderVariableType m_type;
    private uint m_elementCount;

    private uint m_alignedElementSizeInBytes; //Computed, actual 4-byte aligned size of individual array element

    //From ShaderReflectionType
    private D3DShaderVariableMember[] m_members;

    public string Name { get { return m_name; } }

    public uint SizeInBytes { get { return m_sizeInBytes; } }

    public uint AlignedElementSizeInBytes { get { return m_alignedElementSizeInBytes; } }

    public uint StartOffset { get { return m_startOffset; } }

    public ShaderData.ShaderVariableFlags Flags { get { return m_flags; } }

    public bool IsUsed { get { return m_isUsed; } }

    public uint StartSampler { get { return m_startSampler; } }

    public uint SamplerSize { get { return m_samplerSize; } }

    public uint StartTexture { get { return m_startTexture; } }

    public uint TextureSize { get { return m_textureSize; } }

    public ReadOnlyMemory<byte> DefaultValue { get { return m_defaultValue; } }

    public ShaderData.ShaderVariableClass VariableClass { get { return m_class; } }

    public uint ColumnCount { get { return m_numColumns; } }

    public uint RowCount { get { return m_numRows; } }

    public ShaderData.ShaderVariableType VariableType { get { return m_type; } }

    public uint ElementCount { get { return m_elementCount; } }

    public D3DShaderVariableMember[] Members { get { return m_members; } }

    public D3DShaderVariable(D3D.ShaderReflectionVariable variable)
    {
      D3D.ShaderVariableDescription desc = variable.Description;
      using D3D.ShaderReflectionType type = variable.GetVariableType();
      D3D.ShaderTypeDescription typeDesc = type.Description;

      m_name = desc.Name;
      m_sizeInBytes = (uint)desc.Size;
      m_startOffset = (uint) desc.StartOffset;
      m_flags = (ShaderData.ShaderVariableFlags) desc.Flags;

      // Track usage separately, so it's easier to compare variables between shaders (some may be unused, where we want to compare
      // if structurally the same
      m_isUsed = m_flags.HasFlag(ShaderData.ShaderVariableFlags.Used);
      m_flags &= ~ShaderData.ShaderVariableFlags.Used;

      m_startSampler = (uint)desc.StartSampler;
      m_samplerSize = (uint)desc.SamplerSize;
      m_startTexture = (uint)desc.StartTexture;
      m_textureSize = (uint)desc.TextureSize;
      m_defaultValue = ReadOnlyMemory<byte>.Empty;

      if (desc.DefaultValue != IntPtr.Zero)
      {
        byte[] defValue = new byte[m_sizeInBytes];
        defValue.AsSpan().CopyFrom(desc.DefaultValue, m_defaultValue.Length);
        m_defaultValue = defValue;
      }
      
      m_class = (ShaderData.ShaderVariableClass) typeDesc.Class;
      m_numColumns = (uint) typeDesc.ColumnCount;
      m_numRows = (uint) typeDesc.RowCount;
      m_type = (ShaderData.ShaderVariableType) typeDesc.Type;
      m_elementCount = (uint) typeDesc.ElementCount;

      m_alignedElementSizeInBytes = CalculateAlignedElementSize(m_sizeInBytes, m_elementCount);

      m_members = new D3DShaderVariableMember[typeDesc.MemberCount];

      for (int i = 0; i < m_members.Length; i++)
      {
        using (D3D.ShaderReflectionType memberType = type.GetMemberType(i))
        {
          string memberName = type.GetMemberTypeName(i);
          m_members[i] = new D3DShaderVariableMember(memberName, memberType);
        }
      }

    }

    public bool AreSame(D3DShaderVariable variable, bool structuralOnly, out String reasonWhyNotSame)
    {
      // Ignore IsUsed for comparing if structurally the same, since for two separate shaders, the layout may be
      // the same but the usage may differ.
      if (!structuralOnly)
      {
        if (IsUsed != variable.IsUsed)
        {
          reasonWhyNotSame = "Usage is not the same.";
          return false;
        }
      }

      if (!m_name.Equals(variable.Name, StringComparison.InvariantCulture)
          || m_sizeInBytes != variable.SizeInBytes
          || m_startOffset != variable.StartOffset
          || m_flags != variable.Flags
          || m_startSampler != variable.StartSampler
          || m_samplerSize != variable.SamplerSize
          || m_startTexture != variable.StartTexture
          || m_textureSize != variable.TextureSize)
      {
        reasonWhyNotSame = "Description not the same";
        return false;
      }

      if (m_class != variable.VariableClass
          || m_numColumns != variable.ColumnCount
          || m_numRows != variable.RowCount
          || m_elementCount != variable.ElementCount)
      {
        reasonWhyNotSame = "Type not the same";
        return false;
      }

      D3DShaderVariableMember[] members = variable.Members;

      if (m_members.Length != members.Length)
      {
        reasonWhyNotSame = "Member count not the same";
        return false;
      }

      for (int i = 0; i < m_members.Length; i++)
      {
        if (!m_members[i].AreSame(members[i], out reasonWhyNotSame))
        {
          reasonWhyNotSame = $"Member[{i}] not the same:\n{reasonWhyNotSame}";
          return false;
        }
      }

      reasonWhyNotSame = "";
      return true;
    }

    public ShaderData.ValueVariable CreateValueVariable(bool assignDefaultValue = true)
    {
      ShaderData.ValueVariable v = new ShaderData.ValueVariable();
      v.Name = Name;
      v.SizeInBytes = SizeInBytes;
      v.AlignedElementSizeInBytes = AlignedElementSizeInBytes;
      v.StartOffset = StartOffset;
      v.VariableClass = VariableClass;
      v.VariableType = VariableType;
      v.ColumnCount = ColumnCount;
      v.RowCount = RowCount;
      v.Flags = Flags;

      if (IsUsed)
        v.Flags |= ShaderData.ShaderVariableFlags.Used;

      v.StartSampler = StartSampler;
      v.SamplerSize = SamplerSize;
      v.StartTexture = StartTexture;
      v.TextureSize = TextureSize;
      v.DefaultValue = (assignDefaultValue) ? DefaultValue : ReadOnlyMemory<byte>.Empty;
      v.ElementCount = ElementCount;

      if (Members.Length > 0)
      {
        v.Members = new ShaderData.ValueVariableMember[Members.Length];
        for (int i = 0; i < Members.Length; i++)
          v.Members[i] = Members[i].CreateValueVariableMember();
      }

      return v;
    }

    internal static uint CalculateAlignedElementSize(uint sizeInBytes, uint elementCount)
    {
      uint sizePerElement = (elementCount > 0) ? sizeInBytes / elementCount : sizeInBytes;

      bool needsAlignment = (sizePerElement & 0xF) != 0;
      if (needsAlignment)
        sizePerElement = ((sizePerElement >> 4) + 1) << 4;

      return sizePerElement;
    }
  }
}
