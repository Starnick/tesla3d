﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Graphics;
using D3D = SharpDX.D3DCompiler;

namespace Tesla.Hlsl
{
  /// <summary>
  /// Represents a parameter within a shader signature.
  /// </summary>
  public sealed class D3DShaderParameter
  {
    private string m_semanticName;
    private uint m_semanticIndex;
    private uint m_streamIndex;
    private uint m_register;

    private RegisterComponentUsage m_usageMask;
    private RegisterComponentUsage m_readWriteMask;
    private RegisterComponentType m_componentType;
    private ShaderData.SystemValueType m_systemType;
    private ShaderData.MinPrecision m_minPrecision;

    /// <summary>
    /// Gets the per-parameter string that identifies how the data will be used.
    /// </summary>
    public string SemanticName { get { return m_semanticName; } }

    /// <summary>
    /// Gets the semantic index the modifies the semantic name, used to differentiate different parameters that
    /// use the same semantic.
    /// </summary>
    public uint SemanticIndex { get { return m_semanticIndex; } }
    /// <summary>
    /// Gets the index of which stream the geometry shader is using for the signature parameter.
    /// </summary>
    public uint StreamIndex { get { return m_streamIndex; } }

    /// <summary>
    /// Gets the register that will contain the variable's data.
    /// </summary>
    public uint Register { get { return m_register; } }

    /// <summary>
    /// Gets the mask that indicates which components of a register are used.
    /// </summary>
    public RegisterComponentUsage UsageMask { get { return m_usageMask;} }

    /// <summary>
    /// Gets the mask that indicates whether a given component is never written (if the signature is an output)
    /// or always read (if the signature is an input).
    /// </summary>
    public RegisterComponentUsage ReadWriteMask { get { return m_readWriteMask;} }

    /// <summary>
    /// Gets the per-component data type that is stored in the register. Each register can store up to four
    /// components of data.
    /// </summary>
    public RegisterComponentType ComponentType { get { return m_componentType; } }

    /// <summary>
    /// Gets the predefined string that determines the functionality of certain pipeline stages.
    /// </summary>
    public ShaderData.SystemValueType SystemType { get { return m_systemType; } }

    /// <summary>
    /// Gets the minimum desired interpolation precision.
    /// </summary>
    public ShaderData.MinPrecision MinPrecision { get { return m_minPrecision; } }

    public D3DShaderParameter(D3D.ShaderParameterDescription shaderParameter)
    {
      m_semanticName = shaderParameter.SemanticName;
      m_semanticIndex = (uint) shaderParameter.SemanticIndex;
      m_streamIndex = (uint) shaderParameter.Stream;
      m_register = (uint) shaderParameter.Register;

      //For some reason there's some gobbly gook in the usage mask value
      m_usageMask = (RegisterComponentUsage) (shaderParameter.UsageMask & D3D.RegisterComponentMaskFlags.All);
      m_readWriteMask = (RegisterComponentUsage) shaderParameter.ReadWriteMask;
      m_componentType = (RegisterComponentType) shaderParameter.ComponentType;
      m_systemType = (ShaderData.SystemValueType) shaderParameter.SystemValueType;
      m_minPrecision = (ShaderData.MinPrecision) shaderParameter.MinPrecision;
    }

    public bool AreSame(D3DShaderParameter parameter)
    {
      if (!m_semanticName.Equals(parameter.SemanticName, StringComparison.InvariantCulture)
          || m_semanticIndex != parameter.SemanticIndex
          || m_streamIndex != parameter.StreamIndex
          || m_register != parameter.Register
          || m_usageMask != parameter.UsageMask
          || m_readWriteMask != parameter.ReadWriteMask
          || m_componentType != parameter.ComponentType
          || m_systemType != parameter.SystemType
          || m_minPrecision != parameter.MinPrecision)
      {
        return false;
      }

      return true;
    }
  }
}
