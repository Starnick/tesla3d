﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;
using SharpDX.Direct3D;
using Tesla.Graphics;
using D3DC = SharpDX.D3DCompiler;

namespace Tesla.Hlsl
{
  public sealed class D3DShaderReflection
  {
    private D3DConstantBuffer[] m_constantBuffers;
    private D3DResourceBinding[] m_resourceBindings;
    private D3DShaderParameter[] m_inputSignature;
    private D3DShaderParameter[] m_outputSignature;
    private D3DShaderParameter[] m_patchConstantSignature;

    private uint m_bitwiseInstructionCount;
    private uint m_conditionalMoveInstructionCount;
    private uint m_conversionInstructionCount;
    private ShaderData.InputPrimitive m_geometryShaderInputPrimitive;
    private bool m_isSampleFrequencyShader;
    private uint m_moveInstructionCount;

    private uint m_arrayInstructionCount;
    private uint m_barrierInstructionCount;
    private uint m_controlPointCount;
    private string m_creator;
    private uint m_cutInstructionCount;
    private uint m_declarationCount;
    private uint m_defineCount;
    private uint m_dynamicFlowControlInstructionCount;
    private uint m_emitInstructionCount;
    private ShaderCompileFlags m_shaderflags;
    private uint m_floatInstructionCount;
    private uint m_geometryShaderInstanceCount;
    private ShaderData.PrimitiveTopology m_geometryShaderOutputTopology;
    private ShaderData.TessellatorOutputPrimitive m_hullShaderOutputPrimitive;
    private ShaderData.TessellatorPartitioning m_hullShaderPartitioning;
    private ShaderData.InputPrimitive m_geometryOrHullShaderInputPrimitive;
    private uint m_instructionCount;
    private uint m_interlockedInstructionCount;
    private uint m_integerInstructionCount;
    private uint m_macroInstructionCount;
    private uint m_maxGeometryShaderOutputVertexCount;
    private uint m_staticFlowControlInstructionCount;
    private uint m_tempArrayCount;
    private uint m_tempRegisterCount;
    private ShaderData.TessellatorDomain m_tessellatorDomain;
    private uint m_textureBiasInstructionCount;
    private uint m_textureCompareInstructionCount;
    private uint m_textureGradientInstructionCount;
    private uint m_textureLoadInstructionCount;
    private uint m_textureNormalInstructionCount;
    private uint m_textureStoreInstructionCount;
    private uint m_unsignedIntegerInstructionCount;
    private uint m_shaderVersion; 
    private FeatureLevel m_minFeatureLevel;
    private ShaderData.RequiresFlag m_requiresFlag;
    private Int3 m_threadGroupSize;

    private int m_hashCode;
    private int m_inputSigHash;
    private int m_outputSigHash;
    private ReadOnlyMemory<byte> m_byteCode;
    private ReadOnlyMemory<byte> m_inputSigByteCode;
    private ReadOnlyMemory<byte> m_outputSigByteCode;

    public D3DConstantBuffer[] ConstantBuffers { get {  return m_constantBuffers; } }

    public D3DResourceBinding[] BoundResources { get { return m_resourceBindings; } }

    public D3DShaderParameter[] InputSignature { get { return m_inputSignature; } }

    public D3DShaderParameter[] OutputSignature { get { return m_outputSignature; } }

    public D3DShaderParameter[] PatchConstantSignature { get { return m_patchConstantSignature; } }

    public uint BitwiseInstructionCount { get { return m_bitwiseInstructionCount; } }

    public uint ConditionalMoveInstructionCount { get { return m_conditionalMoveInstructionCount; } }

    public uint ConversionInstructionCount { get { return m_conversionInstructionCount; } }

    public ShaderData.InputPrimitive GeometryShaderInputPrimitive { get { return m_geometryShaderInputPrimitive; } }

    public bool IsSampleFrequencyShader { get { return m_isSampleFrequencyShader; } }

    public uint MoveInstructionCount { get { return m_moveInstructionCount; } }

    public uint ShaderVersion { get { return m_shaderVersion; } }

    public uint ArrayInstructionCount { get { return m_arrayInstructionCount; } }

    public uint BarrierInstructionCount { get { return m_barrierInstructionCount; } }

    public uint ControlPointCount { get { return m_controlPointCount; } }

    public string Creator { get { return m_creator; } }

    public uint CutInstructionCount { get { return m_cutInstructionCount; } }

    public uint DeclarationCount { get { return m_declarationCount; } }

    public uint DefineCount { get { return m_defineCount; } }

    public uint DynamicFlowControlInstructionCount { get { return m_dynamicFlowControlInstructionCount; } }

    public uint EmitInstructionCount { get { return m_emitInstructionCount; } }

    public ShaderCompileFlags ShaderFlags { get { return m_shaderflags; } }

    public uint FloatInstructionCount { get { return m_floatInstructionCount; } }

    public uint GeometryShaderInstanceCount { get { return m_geometryShaderInstanceCount; } }

    public ShaderData.PrimitiveTopology GeometryShaderOutputTopology { get { return m_geometryShaderOutputTopology; } }

    public ShaderData.TessellatorOutputPrimitive HullShaderOutputPrimitive { get { return m_hullShaderOutputPrimitive; } }

    public ShaderData.TessellatorPartitioning HullShaderPartitioning { get { return m_hullShaderPartitioning; } }

    public ShaderData.InputPrimitive GeometryOrHullShaderInputPrimitive { get { return m_geometryOrHullShaderInputPrimitive; } }

    public uint InstructionCount { get { return m_instructionCount; } }

    public uint InterlockedInstructionCount { get { return m_interlockedInstructionCount; } }

    public uint IntegerInstructionCount { get { return m_integerInstructionCount; } }

    public uint MacroInstructionCount { get { return m_macroInstructionCount; } }

    public uint MaxGeometryShaderOutputVertexCount { get { return m_maxGeometryShaderOutputVertexCount; } }

    public uint StaticFlowControlInstructionCount { get { return m_staticFlowControlInstructionCount; } }

    public uint TempArrayCount { get { return m_tempArrayCount; } }

    public uint TempRegisterCount { get { return m_tempRegisterCount; } }

    public ShaderData.TessellatorDomain TessellatorDomain { get { return m_tessellatorDomain; } }

    public uint TextureBiasInstructionCount { get { return m_textureBiasInstructionCount; } }

    public uint TextureCompareInstructionCount { get { return m_textureCompareInstructionCount; } }

    public uint TextureGradientInstructionCount { get { return m_textureGradientInstructionCount; } }

    public uint TextureLoadInstructionCount { get { return m_textureLoadInstructionCount; } }

    public uint TextureNormalInstructionCount { get { return m_textureNormalInstructionCount; } }

    public uint TextureStoreInstructionCount { get { return m_textureStoreInstructionCount; } }

    public uint UnsignedIntegerInstructionCount { get { return m_unsignedIntegerInstructionCount; } }

    public FeatureLevel MinFeatureLevel { get { return m_minFeatureLevel; } }

    public ShaderData.RequiresFlag RequiresFlag { get { return m_requiresFlag; } }

    public Int3 ThreadGroupSize { get { return m_threadGroupSize; } }

    public ReadOnlyMemory<byte> CompiledByteCode { get { return m_byteCode; } }

    public ReadOnlyMemory<byte> InputSignatureByteCode { get { return m_inputSigByteCode; } }

    public ReadOnlyMemory<byte> OutputSignatureByteCode { get { return m_outputSigByteCode; } }

    public int CompiledByteCodeHash { get { return m_hashCode; } }

    public int InputSignatureByteCodeHash { get { return m_inputSigHash; } }

    public int OutputSignatureByteCodeHash { get { return  m_outputSigHash; } }

    public D3DShaderReflection(D3DC.ShaderBytecode byteCode)
    {
      using D3DC.ShaderReflection reflection = new D3DC.ShaderReflection(byteCode);

      Initialize(reflection);

      using D3DC.ShaderBytecode inputSigBytes = byteCode.GetPart(D3DC.ShaderBytecodePart.InputSignatureBlob);
      using D3DC.ShaderBytecode outputSigBytes = byteCode.GetPart(D3DC.ShaderBytecodePart.OutputSignatureBlob);

      m_byteCode = byteCode.Data;
      m_inputSigByteCode = inputSigBytes.Data;
      m_outputSigByteCode = outputSigBytes.Data;

      m_hashCode = BufferHelper.ComputeFNVModifiedHashCode(m_byteCode.Span);
      m_inputSigHash = BufferHelper.ComputeFNVModifiedHashCode(m_inputSigByteCode.Span);
      m_outputSigHash = BufferHelper.ComputeFNVModifiedHashCode(m_outputSigByteCode.Span);
    }

    public D3DShaderReflection(byte[] byteCode)
    {
      using D3DC.ShaderBytecode shaderByteCode = new D3DC.ShaderBytecode(byteCode);
      using D3DC.ShaderReflection reflection = new D3DC.ShaderReflection(shaderByteCode);

      Initialize(reflection);

      using D3DC.ShaderBytecode inputSigBytes = shaderByteCode.GetPart(D3DC.ShaderBytecodePart.InputSignatureBlob);
      using D3DC.ShaderBytecode outputSigBytes = shaderByteCode.GetPart(D3DC.ShaderBytecodePart.OutputSignatureBlob);

      m_byteCode = byteCode;
      m_inputSigByteCode = inputSigBytes.Data;
      m_outputSigByteCode = outputSigBytes.Data;

      m_hashCode = BufferHelper.ComputeFNVModifiedHashCode(m_byteCode.Span);
      m_inputSigHash = BufferHelper.ComputeFNVModifiedHashCode(m_inputSigByteCode.Span);
      m_outputSigHash = BufferHelper.ComputeFNVModifiedHashCode(m_outputSigByteCode.Span);
    }

    public D3DConstantBuffer? GetConstantBufferByName(string name)
    {
      foreach (D3DConstantBuffer cb in m_constantBuffers)
      {
        if (cb.Name.Equals(name, StringComparison.InvariantCulture))
          return cb;
      }

      return null;
    }

    public override int GetHashCode()
    {
      return m_hashCode;
    }

    public ShaderData.Signature CreateShaderDataInputSignature()
    {
      return CreateSignature(m_inputSigByteCode, m_inputSignature);
    }

    public ShaderData.Signature CreateShaderDataOutputSignature()
    {
      return CreateSignature(m_outputSigByteCode, m_outputSignature);
    }

    public bool AreSame(D3DShaderReflection shader, bool structuralOnly, out string reasonWhyNotSame)
    {
      if (m_hashCode != shader.m_hashCode)
      {
        reasonWhyNotSame = "Bytecode hash not the same";
        return false;
      }
      if (m_shaderVersion != shader.ShaderVersion)
      {
        reasonWhyNotSame = "Shader version is not the same";
        return false;
      }

      if (m_bitwiseInstructionCount != shader.BitwiseInstructionCount
          || m_conditionalMoveInstructionCount != shader.ConditionalMoveInstructionCount
          || m_conversionInstructionCount != shader.ConversionInstructionCount
          || m_geometryShaderInputPrimitive != shader.GeometryShaderInputPrimitive
          || m_isSampleFrequencyShader != shader.IsSampleFrequencyShader
          || m_moveInstructionCount != shader.MoveInstructionCount
          || m_arrayInstructionCount != shader.ArrayInstructionCount
          || m_barrierInstructionCount != shader.BarrierInstructionCount
          || m_declarationCount != shader.DeclarationCount
          || m_defineCount != shader.DefineCount
          || m_dynamicFlowControlInstructionCount != shader.DynamicFlowControlInstructionCount
          || m_emitInstructionCount != shader.EmitInstructionCount
          || m_shaderflags != shader.ShaderFlags
          || m_floatInstructionCount != shader.FloatInstructionCount
          || m_geometryShaderInstanceCount != shader.GeometryShaderInstanceCount
          || m_geometryShaderOutputTopology != shader.GeometryShaderOutputTopology
          || m_hullShaderOutputPrimitive != shader.HullShaderOutputPrimitive
          || m_hullShaderPartitioning != shader.HullShaderPartitioning
          || m_geometryOrHullShaderInputPrimitive != shader.GeometryOrHullShaderInputPrimitive
          || m_instructionCount != shader.InstructionCount
          || m_interlockedInstructionCount != shader.InterlockedInstructionCount
          || m_integerInstructionCount != shader.IntegerInstructionCount
          || m_macroInstructionCount != shader.MacroInstructionCount
          || m_maxGeometryShaderOutputVertexCount != shader.MaxGeometryShaderOutputVertexCount
          || m_staticFlowControlInstructionCount != shader.StaticFlowControlInstructionCount
          || m_tempArrayCount != shader.TempArrayCount
          || m_tempRegisterCount != shader.TempRegisterCount
          || m_tessellatorDomain != shader.TessellatorDomain
          || m_textureBiasInstructionCount != shader.TextureBiasInstructionCount
          || m_textureCompareInstructionCount != shader.TextureCompareInstructionCount
          || m_textureGradientInstructionCount != shader.TextureGradientInstructionCount
          || m_textureLoadInstructionCount != shader.TextureLoadInstructionCount
          || m_textureNormalInstructionCount != shader.TextureNormalInstructionCount
          || m_textureStoreInstructionCount != shader.TextureStoreInstructionCount
          || m_unsignedIntegerInstructionCount != shader.UnsignedIntegerInstructionCount
          || m_minFeatureLevel != shader.MinFeatureLevel
          || m_requiresFlag != shader.RequiresFlag
          || m_threadGroupSize != shader.ThreadGroupSize
          || m_controlPointCount != shader.ControlPointCount
          || m_cutInstructionCount != shader.CutInstructionCount
          || !m_creator.Equals(shader.Creator, StringComparison.InvariantCulture))
      {
        reasonWhyNotSame = "Description not the same";
        return false;
      }

      if (!AreInputSignatureSame(shader, out reasonWhyNotSame)
          || !AreOutputSignatureSame(shader, out reasonWhyNotSame)
          || !AreBoundResourcesSame(shader, structuralOnly, out reasonWhyNotSame)
          || !AreConstantBuffersSame(shader, structuralOnly, out reasonWhyNotSame))
      {
        return false;
      }

      reasonWhyNotSame = String.Empty;
      return true;
    }

    private bool AreInputSignatureSame(D3DShaderReflection shader, out string reasonWhyNotSame)
    {
      D3DShaderParameter[] inputSig = shader.InputSignature;

      if (m_inputSignature.Length != inputSig.Length)
      {
        reasonWhyNotSame = "Input signature not the same count";
        return false;
      }

      for (int i = 0; i < m_inputSignature.Length; i++)
      {
        if (!m_inputSignature[i].AreSame(inputSig[i]))
        {
          reasonWhyNotSame = $"Input Parameter {i} not the same.";
          return false;
        }
      }

      reasonWhyNotSame = String.Empty;
      return true;
    }

    private bool AreOutputSignatureSame(D3DShaderReflection shader, out string reasonWhyNotSame)
    {
      D3DShaderParameter[] outputSig = shader.OutputSignature;

      if (m_outputSignature.Length != outputSig.Length)
      {
        reasonWhyNotSame = "Output signature not the same count";
        return false;
      }

      for (int i = 0; i < m_outputSignature.Length; i++)
      {
        if (!m_outputSignature[i].AreSame(outputSig[i]))
        {
          reasonWhyNotSame = $"Output Parameter {i} not the same.";
          return false;
        }
      }

      reasonWhyNotSame = String.Empty;
      return true;
    }

    private bool ArePatchSignatureSame(D3DShaderReflection shader, out string reasonWhyNotSame)
    {
      D3DShaderParameter[] patchSig = shader.PatchConstantSignature;

      if (m_patchConstantSignature.Length != patchSig.Length)
      {
        reasonWhyNotSame = "Patch Constant signature not the same count";
        return false;
      }

      for (int i = 0; i < m_patchConstantSignature.Length; i++)
      {
        if (!m_patchConstantSignature[i].AreSame(patchSig[i]))
        {
          reasonWhyNotSame = $"Patch Constant Parameter {0} not the same.";
          return false;
        }
      }

      reasonWhyNotSame = String.Empty;
      return true;
    }

    private bool AreBoundResourcesSame(D3DShaderReflection shader, bool structuralOnly, out string reasonWhyNotSame)
    {
      D3DResourceBinding[] resources = shader.BoundResources;

      if (m_resourceBindings.Length != resources.Length)
      {
        reasonWhyNotSame = "Bound resource count not the same";
        return false;
      }

      for (int i = 0; i < m_resourceBindings.Length; i++)
      {
        if (!m_resourceBindings[i].AreSame(resources[i], structuralOnly))
        {
          reasonWhyNotSame = $"Bound Resource {i} not the same.";
          return false;
        }
      }

      reasonWhyNotSame = String.Empty;
      return true;
    }

    private bool AreConstantBuffersSame(D3DShaderReflection shader, bool structuralOnly, out string reasonWhyNotSame)
    {
      D3DConstantBuffer[] buffers = shader.ConstantBuffers;

      if (m_constantBuffers.Length != buffers.Length)
      {
        reasonWhyNotSame = "Constant buffer count not the same";
        return false;
      }

      for (int i = 0; i < m_constantBuffers.Length; i++)
      {
        if (!m_constantBuffers[i].AreSame(buffers[i], structuralOnly, out reasonWhyNotSame))
        {
          reasonWhyNotSame = $"Constant Buffer {i} not the same:\n{reasonWhyNotSame}";
          return false;
        }
      }

      reasonWhyNotSame = String.Empty;
      return true;
    }

    [MemberNotNull(nameof(m_constantBuffers), nameof(m_resourceBindings), nameof(m_inputSignature), nameof(m_outputSignature), nameof(m_patchConstantSignature), nameof(m_creator))]
    private void Initialize(D3DC.ShaderReflection reflection)
    {
      D3DC.ShaderDescription desc = reflection.Description;

      m_bitwiseInstructionCount = (uint) reflection.BitwiseInstructionCount;
      m_conditionalMoveInstructionCount = (uint) reflection.ConditionalMoveInstructionCount;
      m_conversionInstructionCount = (uint) reflection.ConversionInstructionCount;
      m_geometryShaderInputPrimitive = (ShaderData.InputPrimitive) reflection.GeometryShaderSInputPrimitive;
      m_isSampleFrequencyShader = reflection.IsSampleFrequencyShader;
      m_moveInstructionCount = (uint) reflection.MoveInstructionCount;
      m_requiresFlag = (ShaderData.RequiresFlag) reflection.RequiresFlags;
      reflection.GetThreadGroupSize(out m_threadGroupSize.X, out m_threadGroupSize.Y, out m_threadGroupSize.Z);

      m_arrayInstructionCount = (uint) desc.ArrayInstructionCount;
      m_barrierInstructionCount = (uint) desc.BarrierInstructions;
      m_controlPointCount = (uint) desc.ControlPoints;
      m_creator = desc.Creator;
      m_cutInstructionCount = (uint) desc.CutInstructionCount;
      m_declarationCount = (uint) desc.DeclarationCount;
      m_defineCount = (uint) desc.DefineCount;
      m_dynamicFlowControlInstructionCount = (uint) desc.DynamicFlowControlCount;
      m_emitInstructionCount = (uint) desc.EmitInstructionCount;
      m_shaderflags = (ShaderCompileFlags) desc.Flags;
      m_floatInstructionCount = (uint) desc.FloatInstructionCount;
      m_geometryShaderInstanceCount = (uint) desc.GeometryShaderInstanceCount;
      m_geometryShaderOutputTopology = (ShaderData.PrimitiveTopology) desc.GeometryShaderOutputTopology;
      m_hullShaderOutputPrimitive = (ShaderData.TessellatorOutputPrimitive) desc.HullShaderOutputPrimitive;
      m_hullShaderPartitioning = (ShaderData.TessellatorPartitioning) desc.HullShaderPartitioning;
      m_geometryOrHullShaderInputPrimitive = (ShaderData.InputPrimitive) desc.InputPrimitive;
      m_instructionCount = (uint) desc.InstructionCount;
      m_interlockedInstructionCount = (uint) desc.InterlockedInstructions;
      m_integerInstructionCount = (uint) desc.IntInstructionCount;
      m_macroInstructionCount = (uint) desc.MacroInstructionCount;
      m_maxGeometryShaderOutputVertexCount = (uint) desc.GeometryShaderMaxOutputVertexCount;
      m_staticFlowControlInstructionCount = (uint) desc.StaticFlowControlCount;
      m_tempArrayCount = (uint) desc.TempArrayCount;
      m_tempRegisterCount = (uint) desc.TempRegisterCount;
      m_tessellatorDomain = (ShaderData.TessellatorDomain) desc.TessellatorDomain;
      m_textureBiasInstructionCount = (uint) desc.TextureBiasInstructions;
      m_textureCompareInstructionCount = (uint) desc.TextureCompInstructions;
      m_textureGradientInstructionCount = (uint) desc.TextureGradientInstructions;
      m_textureLoadInstructionCount = (uint) desc.TextureLoadInstructions;
      m_textureNormalInstructionCount = (uint) desc.TextureNormalInstructions;
      m_textureStoreInstructionCount = (uint) desc.TextureStoreInstructions;
      m_unsignedIntegerInstructionCount = (uint) desc.UintInstructionCount;
      m_shaderVersion = (uint) desc.Version;

      m_minFeatureLevel = reflection.MinFeatureLevel;

      m_constantBuffers = new D3DConstantBuffer[desc.ConstantBuffers];

      for (int i = 0; i < m_constantBuffers.Length; i++)
      {
        using (D3DC.ConstantBuffer cb = reflection.GetConstantBuffer(i))
          m_constantBuffers[i] = new D3DConstantBuffer(cb);
      }

      m_resourceBindings = new D3DResourceBinding[desc.BoundResources];

      for (int i = 0; i < m_resourceBindings.Length; i++)
      {
        D3DC.InputBindingDescription resource = reflection.GetResourceBindingDescription(i);
        m_resourceBindings[i] = new D3DResourceBinding(resource);
      }

      m_inputSignature = new D3DShaderParameter[desc.InputParameters];

      for (int i = 0; i < m_inputSignature.Length; i++)
      {
        D3DC.ShaderParameterDescription param = reflection.GetInputParameterDescription(i);
        m_inputSignature[i] = new D3DShaderParameter(param);
      }

      m_outputSignature = new D3DShaderParameter[desc.OutputParameters];

      for (int i = 0; i < m_outputSignature.Length; i++)
      {
        D3DC.ShaderParameterDescription param = reflection.GetOutputParameterDescription(i);
        m_outputSignature[i] = new D3DShaderParameter(param);
      }

      m_patchConstantSignature = new D3DShaderParameter[desc.PatchConstantParameters];

      for (int i = 0; i < m_patchConstantSignature.Length; i++)
      {
        D3DC.ShaderParameterDescription param = reflection.GetPatchConstantParameterDescription(i);
        m_patchConstantSignature[i] = new D3DShaderParameter(param);
      }
    }

    public static ShaderData.Signature CreateSignature(ReadOnlyMemory<byte> signatureByteCode, ReadOnlySpan<D3DShaderParameter> parameters)
    {
      ShaderData.Signature sig = new ShaderData.Signature();
      sig.ByteCode = signatureByteCode;
      
      if (parameters.Length > 0)
      {
        sig.Parameters = new ShaderData.SignatureParameter[parameters.Length];
        for (int i = 0; i < parameters.Length; i++)
        {
          D3DShaderParameter d3dParam = parameters[i];
          ShaderData.SignatureParameter sigParam = new ShaderData.SignatureParameter();
          sigParam.ComponentType = d3dParam.ComponentType;
          sigParam.ReadWriteMask = d3dParam.ReadWriteMask;
          sigParam.Register = d3dParam.Register;
          sigParam.SemanticIndex = d3dParam.SemanticIndex;
          sigParam.SemanticName = d3dParam.SemanticName;
          sigParam.StreamIndex = d3dParam.StreamIndex;
          sigParam.SystemType = d3dParam.SystemType;
          sigParam.UsageMask = d3dParam.UsageMask;

          sig.Parameters[i] = sigParam;
        }
      }

      return sig;
    }
  }
}
