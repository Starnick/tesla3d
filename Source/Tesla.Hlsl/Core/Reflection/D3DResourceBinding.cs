﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics;
using Tesla.Graphics;
using D3D = SharpDX.D3DCompiler;

namespace Tesla.Hlsl
{
  [DebuggerDisplay("Name = {Name}, ResourceType = {ResourceType}, ResourceDimension = {ResourceDimension}")]
  public class D3DResourceBinding
  {
    private string m_name;
    private ShaderData.ShaderInputType m_resourceType;
    private ShaderData.ResourceDimension m_resourceDimension;
    private ShaderData.ResourceReturnType m_returnType;
    private ShaderData.ShaderInputFlags m_inputFlags;
    private uint m_sampleCount;
    private uint m_bindPoint;
    private uint m_bindCount;

    public string Name { get { return m_name; } }

    public ShaderData.ShaderInputType ResourceType { get { return m_resourceType; } }

    public ShaderData.ResourceDimension ResourceDimension { get { return m_resourceDimension; } }

    public ShaderData.ResourceReturnType ReturnType { get { return m_returnType; } }

    public ShaderData.ShaderInputFlags InputFlags { get { return m_inputFlags; } }

    public uint SampleCount { get { return m_sampleCount; } }

    public uint BindPoint { get { return m_bindPoint; } }

    public uint BindCount { get { return m_bindCount; } }

    public bool IsConstantBuffer { get { return m_resourceType == ShaderData.ShaderInputType.ConstantBuffer || m_resourceType == ShaderData.ShaderInputType.TextureBuffer; } }

    public bool IsSampler { get { return ResourceType == ShaderData.ShaderInputType.Sampler; } }

    public bool IsStructuredBuffer
    {
      get
      {
        return ResourceType == ShaderData.ShaderInputType.Structured || ResourceType == ShaderData.ShaderInputType.RWStructured ||
          ResourceType == ShaderData.ShaderInputType.AppendStructured || ResourceType == ShaderData.ShaderInputType.ConsumeStructured ||
          ResourceType == ShaderData.ShaderInputType.RWStructuredWithCounter;
      }
    }

    public D3DResourceBinding(D3D.InputBindingDescription resourceBinding)
    {
      m_name = resourceBinding.Name;
      m_inputFlags = (ShaderData.ShaderInputFlags) resourceBinding.Flags;
      m_returnType = (ShaderData.ResourceReturnType) resourceBinding.ReturnType;
      m_resourceType = (ShaderData.ShaderInputType) resourceBinding.Type;
      m_resourceDimension = (ShaderData.ResourceDimension) resourceBinding.Dimension;
      m_sampleCount = (uint) resourceBinding.NumSamples;
      m_bindPoint = (uint) resourceBinding.BindPoint;
      m_bindCount = (uint) resourceBinding.BindCount;
    }

    public bool AreSame(D3DResourceBinding resourceBinding, bool structuralOnly = true)
    {
      if (!m_name.Equals(resourceBinding.Name, StringComparison.InvariantCulture)
          || m_inputFlags != resourceBinding.InputFlags
          || m_returnType != resourceBinding.ReturnType
          || m_resourceType != resourceBinding.ResourceType
          || m_resourceDimension != resourceBinding.ResourceDimension
          || m_sampleCount != resourceBinding.SampleCount)
      {
        return false;
      }

      // Between two different shaders, where the resource binds to may differ, but the layout/type may be the same.
      // BindCount also seems correct to ignore if structurally checking, since it should be legal for an array resource to
      // be used differently (one shader only ever uses [0], another uses [0, 1, 2]) but both refer to the same named uniform
      // of a certain type.
      if (!structuralOnly)
      {
        if (m_bindPoint != resourceBinding.BindPoint || m_bindCount != resourceBinding.BindCount)
          return false;
      }

      return true;
    }
  }
}
