﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics;
using Tesla.Graphics;

namespace Tesla.Hlsl
{
  [DebuggerDisplay("Name = {Name}, ResourceType = {ResourceType}, ResourceDimension = {ResourceDimension}")]
  public sealed class D3DBoundResource
  {
    private string m_name;
    private ShaderData.ResourceReturnType m_returnType;
    private ShaderData.ShaderInputType m_resourceType;
    private ShaderData.ResourceDimension m_resourceDimension;
    private ShaderData.ShaderInputFlags m_inputFlags;
    private uint m_sampleCount;

    public string Name { get { return m_name; } }

    public ShaderData.ResourceReturnType ReturnType { get { return m_returnType; } }

    public ShaderData.ShaderInputType ResourceType { get { return m_resourceType; } }

    public ShaderData.ResourceDimension ResourceDimension { get { return m_resourceDimension; } }

    public ShaderData.ShaderInputFlags InputFlags { get { return m_inputFlags; } }

    public uint SampleCount { get { return m_sampleCount; } }

    public bool IsConstantBuffer { get { return m_resourceType == ShaderData.ShaderInputType.ConstantBuffer || m_resourceType == ShaderData.ShaderInputType.TextureBuffer; } }

    public D3DBoundResource(D3DResourceBinding binding)
    {
      m_name = binding.Name;
      m_inputFlags = binding.InputFlags;
      m_returnType = binding.ReturnType;
      m_resourceType = binding.ResourceType;
      m_resourceDimension = binding.ResourceDimension;
      m_sampleCount = (uint) binding.SampleCount;
    }

    public bool AreSame(D3DBoundResource resourceVariable)
    {
      if (!m_name.Equals(resourceVariable.Name, StringComparison.InvariantCulture)
          || m_inputFlags != resourceVariable.InputFlags
          || m_returnType != resourceVariable.ReturnType
          || m_resourceType != resourceVariable.ResourceType
          || m_resourceDimension != resourceVariable.ResourceDimension
          || m_sampleCount != resourceVariable.SampleCount)
      {
        return false;
      }

      return true;
    }
  }
}
