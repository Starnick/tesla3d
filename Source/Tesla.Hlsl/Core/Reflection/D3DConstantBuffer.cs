﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics;
using Tesla.Graphics;
using D3D = SharpDX.D3DCompiler;

namespace Tesla.Hlsl
{
  [DebuggerDisplay("Name = {Name}, SizeInBytes = {SizeInBytes}, VariableCount = {Variables.Length}")]
  public class D3DConstantBuffer
  {
    private string m_name;
    private uint m_sizeInBytes;
    private ShaderData.ShaderVariableFlags m_flags;
    private ShaderData.ConstantBufferType m_type;

    private D3DShaderVariable[] m_variables;

    public string Name { get { return m_name; } }

    public uint SizeInBytes { get { return m_sizeInBytes; } }

    public ShaderData.ShaderVariableFlags Flags { get { return m_flags; } }

    public ShaderData.ConstantBufferType BufferType { get { return m_type; } }

    public D3DShaderVariable[] Variables { get { return m_variables; } }

    public D3DConstantBuffer(D3D.ConstantBuffer constantBuffer)
    {
      D3D.ConstantBufferDescription desc = constantBuffer.Description;

      m_name = desc.Name;
      m_type = (ShaderData.ConstantBufferType) desc.Type;
      m_flags = (ShaderData.ShaderVariableFlags) desc.Flags;
      m_sizeInBytes = (uint) desc.Size;

      // Ensure flags do not have the IsUsed option, only useful for variables within the CB since if a CB is referenced
      // by a shader, then by definition it is used, so we will never see unused CBs, but we -may- see unused variables within
      // a CB
      m_flags &= ~ShaderData.ShaderVariableFlags.Used;

      m_variables = new D3DShaderVariable[desc.VariableCount];

      for (int i = 0; i < m_variables.Length; i++)
      {
        using (D3D.ShaderReflectionVariable variable = constantBuffer.GetVariable(i))
        {
          D3DShaderVariable myVariable = new D3DShaderVariable(variable);
          m_variables[i] = myVariable;
        }
      }
    }

    public bool AreSame(D3DConstantBuffer constantBuffer, bool structuralOnly, out String reasonWhyNotSame)
    {
      if (!m_name.Equals(constantBuffer.Name, StringComparison.InvariantCulture)
          || m_type != constantBuffer.BufferType
          || m_flags != constantBuffer.Flags
          || m_sizeInBytes != constantBuffer.SizeInBytes)
      {
        reasonWhyNotSame = "Description not same";
        return false;
      }

      D3DShaderVariable[] variables = constantBuffer.Variables;

      if (m_variables.Length != variables.Length)
      {
        reasonWhyNotSame = "Variable count not the same";
        return false;
      }

      for (int i = 0; i < m_variables.Length; i++)
      {
        if (!m_variables[i].AreSame(variables[i], structuralOnly, out reasonWhyNotSame))
        {
          reasonWhyNotSame = $"Variable[{i}] not the same:\n{reasonWhyNotSame}";
          return false;
        }
      }

      reasonWhyNotSame = String.Empty;
      return true;
    }

    public ShaderData.ConstantBuffer CreateShaderDataConstantBuffer()
    {
      ShaderData.ConstantBuffer cb = new ShaderData.ConstantBuffer();
      cb.Name = Name;
      cb.BufferType = BufferType;
      cb.Flags = Flags;
      cb.SizeInBytes = SizeInBytes;

      if (Variables.Length > 0)
      {
        cb.Variables = new ShaderData.ValueVariable[Variables.Length];
        for (int i = 0; i < Variables.Length; i++)
          cb.Variables[i] = Variables[i].CreateValueVariable();
      }

      return cb;
    }
  }
}
