﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using Tesla.Direct3D11.Graphics;
using Tesla.Graphics;

namespace Tesla.Content.Tasks
{
  public class BuildContent : Microsoft.Build.Utilities.Task
  {
    public override bool Execute()
    {
      return true;
    }

    public String MyProperty
    {
      get;
      set;
    }
  }

  public class EffectCompilerTask : Microsoft.Build.Utilities.Task
  {
    [Required]
    public ITaskItem ProjectDirectory { get; set; }
    [Required]
    public ITaskItem OutputDirectory { get; set; }
    [Required]
    public ITaskItem[] Files { get; set; }

    [Output]
    public ITaskItem[] ContentFiles { get; set; }

    public override bool Execute()
    {
      String inputDir = ProjectDirectory.ItemSpec;
      String outputDir = Path.Combine(inputDir, OutputDirectory.ItemSpec);

      FileResourceRepository repo = new FileResourceRepository(outputDir);
      repo.OpenConnection();

      List<ITaskItem> contentFiles = new List<ITaskItem>();

      try
      {
        using (IRenderSystem renderSystem = new Tesla.Direct3D11.Graphics.D3D11RenderSystem())
        {
          foreach (ITaskItem file in Files)
          {
            String inputFilePath = Path.Combine(inputDir, file.ItemSpec);
            String outputFilePath = Path.ChangeExtension(Path.Combine(outputDir, file.ItemSpec), ".tebo");

            Log.LogMessage(MessageImportance.High, "Building {0} -> {1}", file.ItemSpec, outputFilePath);

            String finalOutputDir = Path.GetDirectoryName(outputFilePath);
            if (!Directory.Exists(finalOutputDir))
              Directory.CreateDirectory(finalOutputDir);

            EffectCompiler compiler = new EffectCompiler();
            compiler.SetIncludeHandler(new DefaultIncludeHandler(Path.GetDirectoryName(inputFilePath)));

            EffectCompilerResult result = compiler.CompileFromFile(inputFilePath, CompileFlags.None);
            if (!result.HasCompileErrors)
            {
              Effect effect = new Effect(renderSystem, EffectData.Write(result.EffectData, DataCompressionMode.Gzip));

              BinaryResourceExporter exporter = new BinaryResourceExporter();
              exporter.Save(repo.GetResourceFile(outputFilePath), effect);

              contentFiles.Add(new TaskItem(outputFilePath));
            }
            else
            {
              foreach (String err in result.CompileErrors)
                Log.LogError(null, null, null, file.ItemSpec, 0, 0, 0, 0, err, null);

              return false;
            }
          }
        }
      }
      catch (Exception e)
      {
        Log.LogErrorFromException(e);
        return false;
      }
      finally
      {
        ContentFiles = contentFiles.ToArray();
        repo.CloseConnection();
      }

      return true;
    }
  }
}
