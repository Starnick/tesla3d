﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using ImGuiNET;
using SkiaSharp;
using SkiaSharp.HarfBuzz;

namespace Tesla.Gui
{
  public class BitmapData : IDisposable
  {
    public readonly DataBuffer<Color> Pixels;
    public readonly int Width;
    public readonly int Height;

    public BitmapData(int width, int height)
    {
      Pixels = new DataBuffer<Color>(width * height, MemoryAllocatorStrategy.Managed);
      Width = width;
      Height = height;
    }

    public void BlitTo(Span<Color> dst, int dstX, int dstY, int dstImageWidth, Rectangle? srcSubimage = null)
    {
      Rectangle srcRect = (srcSubimage.HasValue) ? srcSubimage.Value : new Rectangle(0, 0, Width, Height);
      ReadOnlySpan<Color> src = Pixels.Span;

      for (int srcY = srcRect.Y; srcY < srcRect.Bottom; srcY++, dstY++)
      {
        int dstStartIndex = dstX + dstImageWidth * dstY;
        int srcStartIndex = srcRect.X + Width * srcY;
        Span<Color> dstRow = dst.Slice(dstStartIndex, srcRect.Width);
        ReadOnlySpan<Color> srcRow = src.Slice(srcStartIndex, srcRect.Width);
        srcRow.CopyTo(dstRow);
      }
    }

    public void BlitFrom(ReadOnlySpan<Color> src, int srcX, int srcY, int srcImageWidth, Rectangle? dstSubimage = null)
    {
      Rectangle dstRect = (dstSubimage.HasValue) ? dstSubimage.Value : new Rectangle(0, 0, Width, Height);
      Span<Color> dst = Pixels.Span;

      for (int dstY = dstRect.Y; dstY < dstRect.Bottom; dstY++, srcY++)
      {
        int dstStartIndex = dstRect.X + Width * dstY;
        int srcStartIndex = srcX + srcImageWidth * srcY;
        Span<Color> dstRow = dst.Slice(dstStartIndex, dstRect.Width);
        ReadOnlySpan<Color> srcRow = src.Slice(srcStartIndex, dstRect.Width);
        srcRow.CopyTo(dstRow);
      }
    }

    public void BlitAlphaFrom(ReadOnlySpan<byte> src, int srcX, int srcY, int srcImageWidth, Rectangle? dstSubimage = null)
    {
      Rectangle dstRect = (dstSubimage.HasValue) ? dstSubimage.Value : new Rectangle(0, 0, Width, Height);
      Span<Color> dst = Pixels.Span;

      for (int dstY = dstRect.Y; dstY < dstRect.Bottom; dstY++, srcY++)
      {
        int dstStartIndex = dstRect.X + Width * dstY;
        int srcStartIndex = srcX + srcImageWidth * srcY;
        Span<Color> dstRow = dst.Slice(dstStartIndex, dstRect.Width);
        ReadOnlySpan<byte> srcRow = src.Slice(srcStartIndex, dstRect.Width);

        for (int i = 0; i < dstRect.Width; i++)
        {
          ref Color c = ref dstRow[i];
          c.A = srcRow[i];
        }
      }
    }

    public void BlitAlphaFrom(ReadOnlySpan<Color> src, int srcX, int srcY, int srcImageWidth, Rectangle? dstSubimage = null)
    {
      Rectangle dstRect = (dstSubimage.HasValue) ? dstSubimage.Value : new Rectangle(0, 0, Width, Height);
      Span<Color> dst = Pixels.Span;

      for (int dstY = dstRect.Y; dstY < dstRect.Bottom; dstY++, srcY++)
      {
        int dstStartIndex = dstRect.X + dstRect.Width * dstY;
        int srcStartIndex = srcX + srcImageWidth * srcY;
        Span<Color> dstRow = dst.Slice(dstStartIndex, dstRect.Width);
        ReadOnlySpan<Color> srcRow = src.Slice(srcStartIndex, dstRect.Width);

        for (int i = 0; i < dstRect.Width; i++)
        {
          ref Color c = ref dstRow[i];
          c.A = srcRow[i].A;
        }
      }
    }

    public void Dispose()
    {
      Pixels.Dispose();
      GC.SuppressFinalize(this);
    }
  }

  public class Glyph
  {
    public BitmapData? Bitmap;
    public uint CodePoint;
    public char Character;
    public float X0;
    public float Y0;
    public float X1;
    public float Y1;
    public float Padding;
    public bool IsColored;
    public bool IsVisible;
    public float AdvanceX;
  }

  public class ImFontBitmaps : IDisposable, IEnumerable<Glyph>
  {
    private static RawBuffer<ushort> s_noGlyphRange;
    private static RawBuffer<ushort> s_defaultGlyphRange;

    private Dictionary<uint, Glyph> m_glyphs;
    private Dictionary<uint, int> m_customRectIndices;
    private GuiFont m_fontDescr;
    private int m_totalSurfaceArea;

    // Save these to populate the ImFont later
    private float m_ascent;
    private float m_descent;
    private Vector2 m_glyphOffset;
    private bool m_pixelSnapH;
    private int m_overSampleH;
    private int m_overSampleV;

    public int GlyphCount { get { return m_glyphs.Count; } }

    public GuiFont Description { get { return m_fontDescr; } }

    public int TotalSurfaceArea { get { return m_totalSurfaceArea; } }

    static ImFontBitmaps()
    {
      // Imgui holds onto the given glyph range and doesn't copy it...
      s_noGlyphRange = new RawBuffer<ushort>(2);
      s_noGlyphRange[0] = (ushort) ' ';
      s_noGlyphRange[1] = (ushort) ' ';

      s_defaultGlyphRange = new RawBuffer<ushort>(2);
      s_defaultGlyphRange[0] = 0x0020;
      s_defaultGlyphRange[1] = 0x00FF;

    }

    private ImFontBitmaps(GuiFont descr, ImFontPtr font, ImFontConfigPtr config)
    {
      m_glyphs = new Dictionary<uint, Glyph>();
      m_customRectIndices = new Dictionary<uint, int>();

      m_fontDescr = descr;
      m_ascent = font.Ascent;
      m_descent = font.Descent;
      m_glyphOffset = config.GlyphOffset;
      m_pixelSnapH = config.PixelSnapH;
      m_overSampleH = config.OversampleH;
      m_overSampleV = config.OversampleV;
      m_totalSurfaceArea = 0;
    }

    private ImFontBitmaps(GuiFont descr, SKFont font)
    {
      m_glyphs = new Dictionary<uint, Glyph>();
      m_customRectIndices = new Dictionary<uint, int>();

      m_fontDescr = descr;

      SKFontMetrics metrics = font.Metrics;
      float ascent = Math.Abs(metrics.Ascent);
      float descent = Math.Abs(metrics.Descent);
      m_ascent = MathF.Floor(ascent);
      m_descent = -MathF.Ceiling(descent);
      m_glyphOffset = new Vector2(0, 0);
      m_pixelSnapH = true;
      m_overSampleH = 2;
      m_overSampleV = 1;
      m_totalSurfaceArea = 0;
    }

    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    public void AddGlyph(Glyph glyph)
    {
      if (m_glyphs.TryGetValue(glyph.CodePoint, out Glyph? oldGlyph))
      {
        if (oldGlyph.Bitmap is not null)
        {
          m_totalSurfaceArea -= oldGlyph.Bitmap.Width * oldGlyph.Bitmap.Height;
          oldGlyph.Bitmap.Dispose();
        }

        m_glyphs.Remove(glyph.CodePoint);
      }

      if (glyph.Bitmap is not null)
        m_totalSurfaceArea += glyph.Bitmap.Width * glyph.Bitmap.Height;

      m_glyphs.Add(glyph.CodePoint, glyph);
    }

    public bool TryGetGlyph(uint codePoint, [NotNullWhen(true)] out Glyph? glyph)
    {
      return m_glyphs.TryGetValue(codePoint, out glyph);
    }

    public bool TryGetGlyph(char character, [NotNullWhen(true)] out Glyph? glyph)
    {
      return m_glyphs.TryGetValue((uint) character, out glyph);
    }

    public IEnumerator<Glyph> GetEnumerator()
    {
      return m_glyphs.Values.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return m_glyphs.Values.GetEnumerator();
    }

    private void Dispose(bool disposing)
    {
      if (disposing)
      {
        foreach (KeyValuePair<uint, Glyph> kv in m_glyphs)
          kv.Value.Bitmap?.Dispose();

        m_glyphs.Clear();
      }
    }

    public unsafe ImFontPtr AddFontToAtlas(ImFontAtlasPtr fontAtlas)
    {
      m_customRectIndices.Clear();

      ImFontConfigPtr config = ImGuiNative.ImFontConfig_ImFontConfig();
      config.OversampleH = m_overSampleH;
      config.OversampleV = m_overSampleV;
      config.PixelSnapH = m_pixelSnapH;
      config.SizePixels = m_fontDescr.Size;
      config.GlyphRanges = s_noGlyphRange.Pointer;
      config.GlyphOffset = m_glyphOffset;

      // Create an empty default font for us to piggyback onto
      ImFontPtr font = fontAtlas.AddFontDefault(config);
      font.FontSize = m_fontDescr.Size; // Comes back without a size?
      font.Ascent = m_ascent;
      font.Descent = m_descent;
      config.Destroy();

      // Grab the config that ImGui created...not sure why they don't just set this to the font ptr
      config = fontAtlas.ConfigData[fontAtlas.ConfigData.Size - 1];
      config.SizePixels = m_fontDescr.Size;
      config.GlyphOffset = m_glyphOffset;
      config.SetFontName(m_fontDescr.ToString());

      // Add all the glyphs as custom glyph rectangles
      foreach (KeyValuePair<uint, Glyph> kv in m_glyphs)
      {
        Glyph glyph = kv.Value;
        int rectIndex = fontAtlas.AddCustomRectFontGlyph(font, (ushort) glyph.CodePoint, glyph.Bitmap?.Width ?? 0, glyph.Bitmap?.Height ?? 0, glyph.AdvanceX);
        m_customRectIndices.Add(kv.Key, rectIndex);
      }

      return font;
    }

    public unsafe void OnFontAtlasBuild(ImFontAtlasPtr atlas, ImFontPtr font, IntPtr textureData, int width, int height)
    {
      // Iterate over our glyphs as the codepoint on ImFontGlyph/ImFontGlyphEx is broken atm, but we can look it up just fine
      ParallelOptions opts = new ParallelOptions();
      Parallel.ForEach(m_glyphs, opts, (KeyValuePair<uint, Glyph> kv) =>
      {
        Span<Color> dstSpan = textureData.AsSpan<Color>(width * height);

        Glyph glyph = kv.Value;
        ImFontGlyphExPtr glyphPtr = font.FindGlyphNoFallbackEx((uint) glyph.CodePoint);
        if (glyphPtr.NativePtr == null)
          return;

        if (glyph.Bitmap is null)
          return;

        ImFontAtlasCustomRectPtr customRectPtr = atlas.GetCustomRectByIndex(m_customRectIndices[glyph.CodePoint]);
        if (customRectPtr.NativePtr == null)
          return;

        Rectangle dstRect = new Rectangle(customRectPtr.X, customRectPtr.Y, customRectPtr.Width, customRectPtr.Height);
        glyph.Bitmap.BlitTo(dstSpan, dstRect.X, dstRect.Y, width);

        PostBuildGlyphFixup(glyphPtr, glyph, width, height);
      });
    }

    private unsafe void PostBuildGlyphFixup(ImFontGlyphExPtr glyphPtr, in Glyph glyph, int texWidth, int texHeight)
    {
      glyphPtr.X0 = glyph.X0;
      glyphPtr.Y0 = glyph.Y0;
      glyphPtr.X1 = glyph.X1;
      glyphPtr.Y1 = glyph.Y1;

      if (glyph.Padding > 0)
      {
        float uPad = (float) glyph.Padding / (float) texWidth;
        float vPad = (float) glyph.Padding / (float) texHeight;
        glyphPtr.U0 += uPad;
        glyphPtr.V0 += vPad;
        glyphPtr.U1 -= uPad;
        glyphPtr.V1 -= vPad;
      }

      glyphPtr.AdvanceX = glyph.AdvanceX;
      glyphPtr.Colored = (uint) (glyph.IsColored ? 1 : 0);
      glyphPtr.Visible = (uint) (glyph.IsVisible ? 1 : 0);
    }

    private static Rectangle CalculateGlyphBitmapRect(ImFontGlyphExPtr glyphPtr, int texWidth, int texHeight)
    {
      Int2 uv0 = new Int2((int) (glyphPtr.U0 * texWidth), (int) (glyphPtr.V0 * texHeight));
      Int2 uv1 = new Int2((int) (glyphPtr.U1 * texWidth), (int) (glyphPtr.V1 * texHeight));

      Rectangle rect = new Rectangle(uv0.X, uv0.Y, uv1.X - uv0.X, uv1.Y - uv0.Y);
      return rect;
    }

    // Src and dest bitmaps same size
    private static void AddFillAlphaToBitmap(BitmapData data, IntPtr fillAlpha)
    {
      for (int y = 0; y < data.Height; y++)
      {
        int startIndex = data.Width * y;
        ReadOnlySpan<byte> alphaRow = fillAlpha.AsBytes(data.Width * data.Height).Slice(startIndex, data.Width);
        Span<Color> colorRow = data.Pixels.Span.Slice(startIndex, data.Width);

        for (int x = 0; x < data.Width; x++)
        {
          ref Color c = ref colorRow[x];
          c.A = alphaRow[x];
        }
      }
    }

    // Src and dest bitmaps same size
    private static void AddOutlineAlphaToBitmap(BitmapData data, IntPtr outlineAlpha)
    {
      for (int y = 0; y < data.Height; y++)
      {
        int startIndex = data.Width * y;
        ReadOnlySpan<byte> alphaRow = outlineAlpha.AsBytes(data.Width * data.Height).Slice(startIndex, data.Width);
        Span<Color> colorRow = data.Pixels.Span.Slice(startIndex, data.Width);

        for (int x = 0; x < data.Width; x++)
        {
          ref Color c = ref colorRow[x];
          c.B = alphaRow[x];
        }
      };
    }

    public static unsafe ImFontBitmaps GenerateDefault(int size)
    {
      return GenerateImBitmaps(ImGuiFontManager.EmbeddedDefaultFont.AsSize(size), null);
    }

    public static ImFontBitmaps Generate(GuiFont descr, RawBuffer<byte> fontData, int fontNumber = 0)
    {
      ArgumentNullException.ThrowIfNull(fontData, nameof(fontData));

      return GenerateImBitmaps(descr, fontData, fontNumber);
    }

    private static unsafe ImFontBitmaps GenerateImBitmaps(GuiFont descr, RawBuffer<byte>? fontData, int fontNumber = 0)
    {
      // Create a dummy font atlas used to generate the bitmaps
      ImFontAtlasPtr atlas = ImGuiNative.ImFontAtlas_ImFontAtlas();
      ImFontBitmaps renderedFont;

      try
      {
        bool withDropShadow = descr.HasThickOutline;

        // Embedded font may still be loaded using our font data. There's a crash when trying to load their embedded font data
        // in multiple threads, so we load it up first and pass it along
        bool isEmbedded = fontData is null || descr.Family.Equals(ImGuiFontManager.EmbeddedDefaultFont.Family);
        bool useEmbeddedFontData = fontData is null;

        ImFontConfigPtr config = ImGuiNative.ImFontConfig_ImFontConfig();
        ImFontPtr font;

        // Default glyph ranges from imgui, Basic Latin + Latin Supplement
        ushort startGlyph = s_defaultGlyphRange[0];
        ushort endGlyph = s_defaultGlyphRange[1];

        try
        {
          config.SizePixels = descr.Size;
          config.PixelSnapH = true;
          config.OversampleV = 1;
          config.OversampleH = (withDropShadow) ? 1 : 2;
          config.GlyphRanges = s_defaultGlyphRange.Pointer;
          config.GlyphOffset = Vector2.Zero;
          config.EllipsisChar = (ushort) 0x0085;

          if (!useEmbeddedFontData)
          {
            config.FontData = fontData!.Pointer;
            config.FontDataSize = fontData.SizeInBytes;
            config.FontDataOwnedByAtlas = false;
          }
          else
          {
            config.FontNo = fontNumber;
          }

          if (isEmbedded)
          {
            config.OversampleH = 1;
            config.GlyphOffset = new Vector2(0, 1 * MathF.Floor(descr.Size / 13f)); // Add +1 offset per 13 units
          }

          font = (useEmbeddedFontData) ? atlas.AddFontDefault(config) : atlas.AddFont(config);
        }
        finally
        {
          config.Destroy();
        }

        // Build the dummy font
        atlas.GetTexDataAsAlpha8(out IntPtr textureData, out int width, out int height, out _);

        renderedFont = new ImFontBitmaps(descr, font, atlas.ConfigData[0]);

        // Pull out glyph data - iterate over range as the codepoint on ImFontGlyph/ImFontGlyphEx is broken atm - FIXME
        for (ushort g = startGlyph; g <= endGlyph; g++)
        {
          ImFontGlyphExPtr glyphPtr = font.FindGlyphNoFallbackEx(g);
          if (glyphPtr.NativePtr == null)
            continue;

          Rectangle srcRect = CalculateGlyphBitmapRect(glyphPtr, width, height);

          Glyph glyph = new Glyph();
          glyph.X0 = glyphPtr.X0;
          glyph.Y0 = glyphPtr.Y0;
          glyph.X1 = glyphPtr.X1;
          glyph.Y1 = glyphPtr.Y1;
          glyph.AdvanceX = glyphPtr.AdvanceX;
          glyph.IsColored = glyphPtr.Colored > 0;
          glyph.IsVisible = glyphPtr.Visible > 0;
          glyph.Padding = atlas.TexGlyphPadding;
          glyph.CodePoint = glyphPtr.Codepoint;
          glyph.Character = (char) glyphPtr.Codepoint;
          glyph.Bitmap = null;

          if (srcRect.Width > 0 && srcRect.Height > 0)
          {
            // Increase the padding of the bitmap based on the padding used in imgui, notice we get a little bleed-thru if we don't during rendering
            int padding = atlas.TexGlyphPadding;
            glyph.Bitmap = new BitmapData(srcRect.Width + (padding * 2), srcRect.Height + (padding * 2));

            ReadOnlySpan<byte> srcSpan = textureData.AsSpan<byte>(width * height);
            glyph.Bitmap.BlitAlphaFrom(srcSpan, srcRect.X, srcRect.Y, width, new Rectangle(padding, padding, srcRect.Width, srcRect.Height));
          }

          renderedFont.AddGlyph(glyph);
        }

      }
      finally
      {
        atlas.Clear();
        atlas.Destroy();
      }

      return renderedFont;
    }

    public static ImFontBitmaps Generate(GuiFont descr, SKFont font)
    {
      // Default glyph ranges from imgui, Basic Latin + Latin Supplement
      ushort startGlyph = s_defaultGlyphRange[0];
      ushort endGlyph = s_defaultGlyphRange[1];

      // Not sure why, but the skia font seems to be two sizes too big compared to the dear imgui counterparts
      float realfontSize = descr.Size - 2;
      font.Size = realfontSize;

      ImFontBitmaps renderedFont = new ImFontBitmaps(descr, font);
      using SKShaper shaper = new SKShaper(font.Typeface);

      float strokeWidth = 0;

      // thin outline
      if (descr.HasOutline)
        strokeWidth = MathF.Min(5f, realfontSize / 6f);

      // heavy outline
      if (descr.HasThickOutline)
        strokeWidth = MathF.Max(4f, realfontSize / 6f);

      using SKPaint textOpts = new SKPaint(font);
      textOpts.SubpixelText = true;
      textOpts.IsAntialias = true;
      textOpts.LcdRenderText = true;
      textOpts.HintingLevel = SKPaintHinting.Full;
      textOpts.Color = new SKColor(255, 255, 255, 255);
      textOpts.Style = SKPaintStyle.Fill;
      textOpts.StrokeWidth = 0;
      textOpts.StrokeMiter = 1.5f; // Seems to work nice for a variety of large and small size
      textOpts.TextSize = realfontSize;

      // Skia gives us glyph metrics where the baseline is zero and the top-left is negative, dear imgui places text with an origin in top-left,
      // so we get the effect of the text appearing on the previous text line. Offset to correct
      float offsetY = renderedFont.m_ascent;

      for (ushort g = startGlyph; g <= endGlyph; g++)
      {
        if (!font.ContainsGlyph((int) g))
          continue;

        string charStr = ((char) g).ToString();

        textOpts.Style = SKPaintStyle.StrokeAndFill;

        if (!GetSkiaGlyph(textOpts, charStr, out SKRect glyphBounds, out float xAdvance))
          continue;

        // Seems to be fine without any more padding...
        int padding = 0;
        int scaleX = 2;
        int scaleY = 1;

        int width = (int) (MathF.Ceiling(glyphBounds.Width) * scaleX) + (padding * 2);
        int height = (int) (MathF.Ceiling(glyphBounds.Height) * scaleY) + (padding * 2);

        SKPoint translation = new SKPoint(-glyphBounds.Left + (padding * (1f / scaleX)), -glyphBounds.Top + (padding * (1f / scaleY)));

        using SKTextBlob textBlob = ShapeText(shaper, font, charStr, textOpts);
        using SKBitmap bitmap = new SKBitmap(width, height, SKColorType.Alpha8, SKAlphaType.Premul);
        using SKCanvas canvas = new SKCanvas(bitmap);
        BitmapData glyphData = new BitmapData(width, height);

        canvas.ResetMatrix();
        canvas.Scale(scaleX, scaleY);
        canvas.Translate(translation);

        if (strokeWidth > 0)
        {
          textOpts.Style = SKPaintStyle.StrokeAndFill;
          textOpts.StrokeWidth = strokeWidth;
          canvas.DrawText(textBlob, 0, 0, textOpts);
          canvas.Flush();

          AddOutlineAlphaToBitmap(glyphData, bitmap.GetPixels());

          canvas.Clear();
        }

        textOpts.Style = SKPaintStyle.Fill;
        canvas.DrawText(textBlob, 0, 0, textOpts);
        canvas.Flush();

        AddFillAlphaToBitmap(glyphData, bitmap.GetPixels());


        /*
        if (charStr == "A")
        {
          Console.WriteLine("A");
          try
          {
            Image<Rgba32> image = new Image<Rgba32>(width, height);
            image.DangerousTryGetSinglePixelMemory(out Memory<Rgba32> pixels);
            glyphData.Pixels.Span.CopyTo(pixels.Span.AsBytes());
            image.SaveAsPng($@"C:\Users\nicho\Documents\Visual Studio Projects\Tesla3D\build\bin\Samples\SampleBrowser\Debug\net7.0-windows\dump\pulled\a-{descr.Size}-skia.png");
          }
          catch (Exception e) { }
        }*/

        Glyph glyph = new Glyph();
        glyph.AdvanceX = MathF.Round(xAdvance + (textOpts.StrokeWidth * ((descr.HasOutline) ? .5f : .25f)));
        glyph.X0 = glyphBounds.Left;
        glyph.Y0 = glyphBounds.Top + offsetY;
        glyph.X1 = glyphBounds.Right;
        glyph.Y1 = glyphBounds.Bottom + offsetY;
        glyph.Bitmap = glyphData;
        glyph.CodePoint = (uint)g;
        glyph.Character = (char) g;
        glyph.Padding = padding;
        glyph.IsVisible = true;
        glyph.IsColored = false;

        renderedFont.AddGlyph(glyph);
      };

      return renderedFont;
    }

    private static bool GetSkiaGlyph(SKPaint textOpts, string charStr, out SKRect bounds, out float xAdvance)
    {
      xAdvance = 0;
      bounds = new SKRect();

      float[] advance = textOpts.GetGlyphWidths(charStr, out SKRect[] rects);
      if (advance is null || rects is null || advance.Length == 0 || rects.Length == 0)
        return false;

      xAdvance = advance[0];
      bounds = rects[0];

      return bounds.Width > 0 && bounds.Height > 0;
    }

    private static SKTextBlob ShapeText(SKShaper shaper, SKFont font, string text, SKPaint textOpts)
    {
      SKShaper.Result result = shaper.Shape(text, 0, 0, textOpts);
      using SKTextBlobBuilder sKTextBlobBuilder = new SKTextBlobBuilder();
      SKPositionedRunBuffer sKPositionedRunBuffer = sKTextBlobBuilder.AllocatePositionedRun(font, result.Codepoints.Length);
      Span<ushort> glyphSpan = sKPositionedRunBuffer.GetGlyphSpan();
      Span<SKPoint> positionSpan = sKPositionedRunBuffer.GetPositionSpan();
      for (int i = 0; i < result.Codepoints.Length; i++)
      {
        glyphSpan[i] = (ushort) result.Codepoints[i];
        positionSpan[i] = result.Points[i];
      }

      return sKTextBlobBuilder.Build();
    }
  }
}
