﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Runtime.InteropServices;
using Tesla.Content;
using Tesla.Graphics;

namespace Tesla.Gui
{
  /// <summary>
  /// Represents the vertex data type used by ImGui. 2D Position, Texture Coordinate, VertexColor
  /// </summary>
  [Serializable, StructLayout(LayoutKind.Sequential)]
  public struct ImGuiVertex : IEquatable<ImGuiVertex>, IRefEquatable<ImGuiVertex>, IVertexType, IPrimitiveValue
  {
    /// <summary>
    /// Vertex Position
    /// </summary>
    public Vector2 Position;

    /// <summary>
    /// Vertex Texture Coordinate
    /// </summary>
    public Vector2 TextureCoordinate;

    /// <summary>
    /// Vertex color.
    /// </summary>
    public Color VertexColor;

    /// <summary>
    /// Companion vertex layout to use when dealing with this vertex type.
    /// </summary>
    public static readonly VertexLayout VertexLayout;

    /// <summary>
    /// Size of the structure in bytes.
    /// </summary>
    public static readonly int SizeInBytes;

    static ImGuiVertex()
    {
      SizeInBytes = BufferHelper.SizeOf<ImGuiVertex>();
      VertexLayout = new VertexLayout(new VertexElement[] {
                new VertexElement(VertexSemantic.Position, 0, VertexFormat.Float2, 0),
                new VertexElement(VertexSemantic.TextureCoordinate, 0, VertexFormat.Float2, 8),
                new VertexElement(VertexSemantic.Color, 0, VertexFormat.Color, 16)
            });
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="ImGuiVertex"/> struct.
    /// </summary>
    /// <param name="position">The vertex position.</param>
    /// <param name="textureCoordinate">The vertex texture coordinate.</param>
    /// <param name="vertexColor">The vertex color.</param>
    public ImGuiVertex(in Vector2 position, in Vector2 textureCoordinate, Color vertexColor)
    {
      Position = position;
      TextureCoordinate = textureCoordinate;
      VertexColor = vertexColor;
    }

    /// <summary>
    /// Tests equality between two vertices.
    /// </summary>
    /// <param name="a">First vertex</param>
    /// <param name="b">Second vertex</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    public static bool operator ==(in ImGuiVertex a, in ImGuiVertex b)
    {
      return a.Position.Equals(b.Position) && a.TextureCoordinate.Equals(b.TextureCoordinate) && a.VertexColor.Equals(b.VertexColor);
    }

    /// <summary>
    /// Tests inequality between two vertices.
    /// </summary>
    /// <param name="a">First vertex</param>
    /// <param name="b">Second vertex</param>
    /// <returns>True if both are not equal, false otherwise.</returns>
    public static bool operator !=(in ImGuiVertex a, in ImGuiVertex b)
    {
      return !a.Position.Equals(b.Position) || !a.TextureCoordinate.Equals(b.TextureCoordinate) || !a.VertexColor.Equals(b.VertexColor);
    }

    /// <summary>
    /// Indicates whether this instance and a specified object are equal.
    /// </summary>
    /// <param name="obj">Another object to compare to.</param>
    /// <returns>True if <paramref name="obj" /> and this instance are the same type and represent the same value; otherwise, false.</returns>
    public readonly override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is ImGuiVertex)
      {
        ImGuiVertex other = (ImGuiVertex) obj;
        return Position.Equals(other.Position) && TextureCoordinate.Equals(other.TextureCoordinate) && VertexColor.Equals(other.VertexColor);
      }

      return false;
    }

    /// <summary>
    /// Tests equality between two vertices.
    /// </summary>
    /// <param name="other">Other vertex to compare to.</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    readonly bool IEquatable<ImGuiVertex>.Equals(ImGuiVertex other)
    {
      return Position.Equals(other.Position) && TextureCoordinate.Equals(other.TextureCoordinate) && VertexColor.Equals(other.VertexColor);
    }

    /// <summary>
    /// Tests equality between two vertices.
    /// </summary>
    /// <param name="other">Other vertex to compare to.</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    public readonly bool Equals(in ImGuiVertex other)
    {
      return Position.Equals(other.Position) && TextureCoordinate.Equals(other.TextureCoordinate) && VertexColor.Equals(other.VertexColor);
    }

    /// <summary>
    /// Tests equality between two vertices within tolerance.
    /// </summary>
    /// <param name="other">Other vertex to compare to.</param>
    /// <param name="tolerance">Tolerance</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    public readonly bool Equals(in ImGuiVertex other, float tolerance)
    {
      return Position.Equals(other.Position, tolerance) && TextureCoordinate.Equals(other.TextureCoordinate, tolerance) && VertexColor.Equals(other.VertexColor);
    }

    /// <summary>
    /// Gets the layout of the vertex.
    /// </summary>
    /// <returns>Vertex layout</returns>
    public readonly VertexLayout GetVertexLayout()
    {
      return VertexLayout;
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
    public readonly override int GetHashCode()
    {
      unchecked
      {
        return Position.GetHashCode() + TextureCoordinate.GetHashCode() + VertexColor.GetHashCode();
      }
    }

    /// <summary>
    /// Returns a <see cref="System.String"/> that represents this instance.
    /// </summary>
    /// <returns>
    /// A <see cref="System.String"/> that represents this instance.
    /// </returns>
    public readonly override string ToString()
    {
      return String.Format(CultureInfo.CurrentCulture, "Position:{0}, TextureCoordinate:{1}, VertexColor:{2}, ", new object[] { Position.ToString(), TextureCoordinate.ToString(), VertexColor.ToString() });
    }

    /// <summary>
    /// Writes the primitive data to the output.
    /// </summary>
    /// <param name="output">Primitive writer.</param>
    public readonly void Write(IPrimitiveWriter output)
    {
      output.Write<Vector2>("Position", Position);
      output.Write<Vector2>("TextureCoordinate", TextureCoordinate);
      output.Write<Color>("VertexColor", VertexColor);
    }

    /// <summary>
    /// Reads the primitive data from the input.
    /// </summary>
    /// <param name="input">Primitive reader.</param>
    public void Read(IPrimitiveReader input)
    {
      input.Read<Vector2>("Position", out Position);
      input.Read<Vector2>("TextureCoordinate", out TextureCoordinate);
      input.Read<Color>("VertexColor", out VertexColor);
    }
  }
}
