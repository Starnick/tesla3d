﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ImGuiNET;
using Tesla.Graphics;

namespace Tesla.Gui
{
  public sealed class ImFontBuilder : IDisposable
  {
    private ImFontAtlasPtr m_atlas;
    private Dictionary<ImFontPtr, ImFontBitmaps> m_inputData;
    private int m_totalSurfaceArea;
    private int m_maxSurfaceArea;
    private int m_maxTextureWidth;

    public ImFontAtlasPtr Atlas { get { return m_atlas; } }

    public int TotalSurfaceArea { get { return m_totalSurfaceArea; } }

    private ImFontBuilder (ImFontAtlasPtr atlas, int maxTextureDims)
    {
      m_atlas = atlas;
      m_inputData = new Dictionary<ImFontPtr, ImFontBitmaps>();
      m_totalSurfaceArea = 0;
      m_maxTextureWidth = maxTextureDims;
      m_maxSurfaceArea = maxTextureDims * maxTextureDims;

      if (atlas.PackIdMouseCursors >= 0)
      {
        ImFontAtlasCustomRectPtr packedCursors = atlas.CustomRects[atlas.PackIdMouseCursors];
        m_totalSurfaceArea += packedCursors.Width * packedCursors.Height;
      }

      if (atlas.PackIdLines >= 0)
      {
        ImFontAtlasCustomRectPtr packedLines = atlas.CustomRects[atlas.PackIdLines];
        m_totalSurfaceArea += packedLines.Width * packedLines.Height;
      }
    }

    public bool HasRoomFor(ImFontBitmaps font)
    {
      if ((m_totalSurfaceArea + font.TotalSurfaceArea) >= (m_maxSurfaceArea * .95f))
        return false;

      return true;
    }

    public ImFontPtr AddFont(ImFontBitmaps font)
    {
      ImFontPtr fontPtr = font.AddFontToAtlas(m_atlas);
      m_totalSurfaceArea += font.TotalSurfaceArea;
      m_inputData.Add(fontPtr, font);

      return fontPtr;
    }

    public Texture2D Build(IRenderSystem renderSystem)
    {
      SetDesiredTextureWidth();

      m_atlas.GetTexDataAsRGBA32(out IntPtr textureData, out int width, out int height, out _);

      ProcessPackedLinesAndCursors(textureData, width, height);

      // None of the bitmaps overlap, so is safe to blit in parallel
      ParallelOptions opts = new ParallelOptions();
      Parallel.ForEach(m_inputData, opts, (KeyValuePair<ImFontPtr, ImFontBitmaps> kv) =>
      {
        kv.Value.OnFontAtlasBuild(m_atlas, kv.Key, textureData, width, height);
      });

      using DataBuffer<Color> data = DataBuffer.Wrap<Color>(textureData, width * height * Color.SizeInBytes);
      Texture2D fontTexture = new Texture2D(renderSystem, width, height, TextureOptions.Init(data, SurfaceFormat.Color, ResourceUsage.Immutable));
      GuiTextureID id = fontTexture;

      m_atlas.SetTexID(id);
      m_atlas.ClearTexData();

      return fontTexture;
    }

    public static unsafe ImFontBuilder Create(int? maxTextureDims = null)
    {
      return new ImFontBuilder(ImGuiNative.ImFontAtlas_ImFontAtlas(), maxTextureDims ?? 4096);
    }

    public void Dispose()
    {
      foreach (KeyValuePair<ImFontPtr, ImFontBitmaps> kv in m_inputData)
        kv.Value.Dispose();

      m_inputData.Clear();
      m_atlas.ClearInputData();
      m_atlas.ClearTexData();

      GC.SuppressFinalize(this);
    } 

    private void SetDesiredTextureWidth()
    {
      int surface_sqrt = (int) MathF.Sqrt(m_totalSurfaceArea) + 1;
      int imguiDefaults = (surface_sqrt >= 4096 * 0.7f) ? 4096 : (surface_sqrt >= 2048 * 0.7f) ? 2048 : (surface_sqrt >= 1024 * 0.7f) ? 1024 : 512;
      m_atlas.TexDesiredWidth = Math.Min(imguiDefaults, m_maxTextureWidth);
    }

    private unsafe void ProcessPackedLinesAndCursors(IntPtr textureData, int width, int height)
    {
      // So we don't have to convert all the pixels in the texture (which can be very large), just look at the rectangles that contain the
      // default items and process those pixels
      Span<int> rectIds = stackalloc int[2];
      rectIds[0] = m_atlas.PackIdLines;
      rectIds[1] = m_atlas.PackIdMouseCursors;

      Span<Color> texDataSpan = textureData.AsSpan<Color>(width * height);

      for (int i = 0; i < rectIds.Length; i++)
      {
        int id = rectIds[i];
        if (id < 0)
          continue;

        ImFontAtlasCustomRectPtr rectPtr = m_atlas.GetCustomRectByIndex(id);
        if (rectPtr.NativePtr == null)
          continue;

        Rectangle rect = new Rectangle(rectPtr.X, rectPtr.Y, rectPtr.Width, rectPtr.Height);
        for (int y = rect.Y; y < rect.Bottom; y++)
        {
          for (int x = rect.X; x < rect.Right; x++)
          {
            ref Color c = ref texDataSpan[x + width * y];
            c = new Color(0, 0, 0, c.A);
          }
        }
      }
    }
  }
}
