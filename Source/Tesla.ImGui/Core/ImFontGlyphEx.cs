﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using ImGuiNET;

namespace Tesla.Gui
{
  [StructLayout(LayoutKind.Sequential)]
  public unsafe struct ImFontGlyphEx
  {
    public uint BitField;
    public float AdvanceX;
    public float X0;
    public float Y0;
    public float X1;
    public float Y1;
    public float U0;
    public float V0;
    public float U1;
    public float V1;
  }

  [StructLayout(LayoutKind.Sequential)]
  public unsafe struct ImFontGlyphExPtr
  {
    public ImFontGlyphEx* NativePtr { get; }

    public ImFontGlyphExPtr(ImFontGlyphEx* nativePtr) => NativePtr = nativePtr;

    public ImFontGlyphExPtr(ImFontGlyph* buggyNativePtr) => NativePtr = (ImFontGlyphEx*) ((void*) buggyNativePtr);

    public ImFontGlyphExPtr(IntPtr nativePtr) => NativePtr = (ImFontGlyphEx*) nativePtr;

    public static implicit operator ImFontGlyphExPtr(ImFontGlyphEx* nativePtr) => new ImFontGlyphExPtr(nativePtr);

    public static implicit operator ImFontGlyphEx*(ImFontGlyphExPtr wrappedPtr) => wrappedPtr.NativePtr;

    public static implicit operator ImFontGlyphExPtr(IntPtr nativePtr) => new ImFontGlyphExPtr(nativePtr);

    public uint Colored
    {
      get => (uint) BitFieldHelper.GetBits(NativePtr->BitField, 0, 1);
      set => NativePtr->BitField = BitFieldHelper.SetBits(NativePtr->BitField, 0, 1, (uint) value);
    }

    public uint Visible
    {
      get => (uint) BitFieldHelper.GetBits(NativePtr->BitField, 1, 1);
      set => NativePtr->BitField = BitFieldHelper.SetBits(NativePtr->BitField, 1, 1, (uint) value);
    }

    // NOTE: still doesn't work ? Getting back gibberish
    public uint Codepoint
    {
      get => (uint) BitFieldHelper.GetBits(NativePtr->BitField, 2, 30);
      set => NativePtr->BitField = BitFieldHelper.SetBits(NativePtr->BitField, 2, 30, (uint) value);
    }

    public ref float AdvanceX => ref Unsafe.AsRef<float>(&NativePtr->AdvanceX);

    public ref float X0 => ref Unsafe.AsRef<float>(&NativePtr->X0);

    public ref float Y0 => ref Unsafe.AsRef<float>(&NativePtr->Y0);

    public ref float X1 => ref Unsafe.AsRef<float>(&NativePtr->X1);

    public ref float Y1 => ref Unsafe.AsRef<float>(&NativePtr->Y1);

    public ref float U0 => ref Unsafe.AsRef<float>(&NativePtr->U0);

    public ref float V0 => ref Unsafe.AsRef<float>(&NativePtr->V0);

    public ref float U1 => ref Unsafe.AsRef<float>(&NativePtr->U1);

    public ref float V1 => ref Unsafe.AsRef<float>(&NativePtr->V1);
  }

  internal static class BitFieldHelper
  {
    public static byte SetBits(byte oldValue, int offset, int bitCount, byte newBits)
    {
      var mask = (byte) ((1 << bitCount) - 1 << offset);
      return (byte) ((oldValue & ~mask) | (newBits << offset & mask));
    }

    public static ushort SetBits(ushort oldValue, int offset, int bitCount, ushort newBits)
    {
      var mask = (ushort) ((1 << bitCount) - 1 << offset);
      return (ushort) ((oldValue & ~mask) | (newBits << offset & mask));
    }

    public static uint SetBits(uint oldValue, int offset, int bitCount, uint newBits)
    {
      var mask = (uint) ((1 << bitCount) - 1 << offset);
      return (uint) ((oldValue & ~mask) | (newBits << offset & mask));
    }

    public static ulong SetBits(ulong oldValue, int offset, int bitCount, ulong newBits)
    {
      var mask = (ulong) ((1 << bitCount) - 1 << offset);
      return (ulong) ((oldValue & ~mask) | (newBits << offset & mask));
    }

    public static ulong GetBits(ulong value, int offset, int bitCount)
    {
      return (value >> offset) & (1UL << bitCount) - 1;
    }
  }
}
