﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System.Text;
using ImGuiNET;

namespace Tesla.Gui
{
  /// <summary>
  /// Additionally ImGui commands.
  /// </summary>
  public static class ImGuiEx
  {
    /// <summary>
    /// Sets the outline color in the current window's draw list.
    /// </summary>
    /// <param name="color">Outline color.</param>
    public static void SetOutlineColor(Color color)
    {
      ImGui.GetWindowDrawList().SetOutlineColor(color);
    }

    /// <summary>
    /// Sets the outline color to black in the current window's draw list.
    /// </summary>
    public static void ResetOutlineColor()
    {
      ImGui.GetWindowDrawList().ResetOutlineColor();
    }

    /// <summary>
    /// Used to pop a font used to style a window's header text. This fixes a bug where the window draw list no longer has a texture ID and
    /// fails to draw properly.
    /// </summary>
    public static void PopFontWindowHeader()
    {
      ImGui.PopFont();

      ImFontPtr font = ImGui.GetFont(); // May be the default font if we popped the stack
      ImDrawListPtr windowDrawList = ImGui.GetWindowDrawList();
      if (windowDrawList._TextureIdStack.Size == 0)
        windowDrawList.PushTextureID(font.ContainerAtlas.TexID);
    }
  }
}
