﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ImGuiNET;
using SkiaSharp;
using Tesla.Content;

namespace Tesla.Gui
{
  public readonly record struct SKFontMatchResult(SKTypeface Typeface, bool IsFallback, GuiFont Description);

  public sealed class SKFontLoader : IDisposable
  {
    private static string[] s_formats = new string[] { ".ttf", ".ttc", ".otf" };

    private readonly record struct FontPath(IResourceRepository Repository, bool IsManaged, ResourceFileScanner Scanner);
    private readonly record struct ImFontData(RawBuffer<byte> Data, int FontNumber);

    private List<FontPath> m_fontPaths;
    private List<IResourceFile> m_filesToLoad;
    private List<string> m_loadedFamilies;
    private Dictionary<string, List<SKTypeface>> m_customLoadedTypefaces;
    private Dictionary<string, Dictionary<FontSetKey, ImFontData>> m_fontDataForImGui;
    private Dictionary<IntPtr, SKTypeface> m_typefacesToDispose;
    private SKTypeface m_defaultTypeFace;

    public SKTypeface EmbeddedTypeFace { get { return m_defaultTypeFace; } }

    public SKFontLoader()
    {
      m_fontPaths = new List<FontPath>();
      m_filesToLoad = new List<IResourceFile>();
      m_fontDataForImGui = new Dictionary<string, Dictionary<FontSetKey, ImFontData>>();
      m_typefacesToDispose = new Dictionary<IntPtr, SKTypeface>();
      m_loadedFamilies = new List<string>();
      m_defaultTypeFace = LoadEmbeddedFont();

      m_customLoadedTypefaces = new Dictionary<string, List<SKTypeface>>();
    }

    public void AddFontPath(IResourceRepository repo)
    {
      ArgumentNullException.ThrowIfNull(repo, nameof(repo));

      bool isManaged = false;
      if (!repo.IsOpen)
      {
        isManaged = true;
        repo.OpenConnection();
      }

      m_fontPaths.Add(new FontPath(repo, isManaged, new ResourceFileScanner(repo)));
      DiscoverAndLoadFonts();
    }

    public IEnumerable<IResourceRepository> GetFontPaths()
    {
      foreach (FontPath entry in m_fontPaths)
        yield return entry.Repository;
    }

    public bool RemoveFontPath(IResourceRepository repo)
    {
      ArgumentNullException.ThrowIfNull(repo, nameof(repo));

      for (int i = 0; i < m_fontPaths.Count; i++)
      {
        FontPath entry = m_fontPaths[i];
        if (Object.ReferenceEquals(entry.Repository, repo))
        {
          m_fontPaths.RemoveAt(i);
          return true;
        }
      }

      return false;
    }

    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    private void Dispose(bool isDisposing)
    {
      if (!isDisposing)
        return;

      foreach (FontPath entry in m_fontPaths)
      {
        try
        {
          if (entry.IsManaged)
            entry.Repository.CloseConnection();
        }
        catch (Exception e)
        {
          EngineLog.LogException(LogLevel.Error, e);
        }
      }

      foreach (Dictionary<FontSetKey, ImFontData> styleset in m_fontDataForImGui.Values)
      {
        foreach (ImFontData entry in styleset.Values)
          entry.Data.Dispose();
      }

      foreach (SKTypeface typeface in m_typefacesToDispose.Values)
        typeface.Dispose();

      m_customLoadedTypefaces.Clear();
      m_fontPaths.Clear();
      m_fontDataForImGui.Clear();
      m_typefacesToDispose.Clear();
    }

    public void DiscoverAndLoadFonts()
    {
      m_filesToLoad.Clear();
      foreach (FontPath entry in m_fontPaths)
      {
        if (!entry.Scanner.TryDiscoverNewFiles(m_filesToLoad))
          continue;

        foreach (IResourceFile file in m_filesToLoad)
        {
          try
          {
            using Stream str = file.OpenRead();
            SKTypeface tf = SKFontManager.Default.CreateTypeface(str);
            if (!m_customLoadedTypefaces.TryGetValue(tf.FamilyName, out List<SKTypeface>? familyfaces))
            {
              familyfaces = new List<SKTypeface>();
              m_customLoadedTypefaces.Add(tf.FamilyName, familyfaces);
            }

            familyfaces.Add(tf);
            TrackDisposable(tf);
          }
          catch (Exception e)
          {
            EngineLog.LogException(LogLevel.Error, e);
            entry.Scanner.MarkFileFailed(file);
          }
        }

        m_filesToLoad.Clear();
      }
    }

    public void SetImFontData(ImFontConfigPtr config, SKTypeface typeface)
    {
      RawBuffer<byte> data = GetImFontData(typeface, out int fontNo);

      config.FontDataOwnedByAtlas = false;
      config.FontData = data.Pointer;
      config.FontDataSize = data.SizeInBytes;
      config.FontNo = fontNo;
    }

    public void SetImFontData(CachedFontStyleSet styleSet, SKTypeface typeface)
    {
      RawBuffer<byte> data = GetImFontData(typeface, out int fontNo);
      styleSet.SetFontData(data, fontNo);
    }

    public RawBuffer<byte> GetImFontData(SKTypeface typeface, out int fontNumber)
    {
      if (!m_fontDataForImGui.TryGetValue(typeface.FamilyName, out Dictionary<FontSetKey, ImFontData>? styleset))
      {
        styleset = new Dictionary<FontSetKey, ImFontData>();
        m_fontDataForImGui.Add(typeface.FamilyName, styleset);
      }

      FontSetKey key = CreateFontSetKey(typeface);
      if (!styleset.TryGetValue(key, out ImFontData entry))
      {
        using SKStreamAsset str = typeface.OpenStream(out fontNumber);
        entry = new ImFontData(new RawBuffer<byte>(str.GetMemoryBase(), str.Length, true), fontNumber);
        styleset.Add(key, entry);
      }

      fontNumber = entry.FontNumber;
      return entry.Data;
    }

    public SKFontMatchResult FindBestMatching(GuiFont descr)
    {
      using SKFontStyle skStyle = CreateSKFontStyle(descr);

      // If it's the embedded font...ignore all styles but respect the size
      if (ImGuiFontManager.EmbeddedDefaultFont.Family.Equals(descr.Family))
      {
        GuiFont embeddedAtSize = ImGuiFontManager.EmbeddedDefaultFont.AsSize(descr.Size);
        return new SKFontMatchResult(m_defaultTypeFace, !embeddedAtSize.Equals(descr), embeddedAtSize);
      }

      // Check the fonts we loaded ourself first...
      SKTypeface? matchedTypeFace = MatchCustomLoadedFamily(descr, skStyle);

      // Look at system installed fonts...
      if (matchedTypeFace is null)
        matchedTypeFace = MatchInstalledFamily(descr, skStyle);

      // Couldn't find it...fallback to embedded font
      if (matchedTypeFace is null)
        return new SKFontMatchResult(m_defaultTypeFace, true, ImGuiFontManager.EmbeddedDefaultFont);

      // Create a description for the found font as it may not match exactly
      GuiFont actualDescr = CreateGuiFont(matchedTypeFace, descr);
      return new SKFontMatchResult(matchedTypeFace, !actualDescr.Equals(descr), actualDescr);
    }

    private SKTypeface? MatchFamily(string family, SKFontStyle skStyle)
    {
      SKTypeface? typeface = SKFontManager.Default.MatchFamily(family, skStyle);
      if (typeface is null || typeface.Handle == SKTypeface.Default.Handle)
        return null;

      TrackDisposable(typeface);
      return typeface;
    }

    private SKTypeface MatchTypeface(SKTypeface typeface, SKFontStyle skStyle)
    {
      SKTypeface? matchedTypeface = SKFontManager.Default.MatchTypeface(typeface, skStyle);
      if (matchedTypeface is null || matchedTypeface.Handle == SKTypeface.Default.Handle)
        return typeface;

      TrackDisposable(matchedTypeface);
      return matchedTypeface;
    }

    private SKTypeface? MatchInstalledFamily(GuiFont descr, SKFontStyle skStyle)
    {
      // Match family only looks at installed fonts...
      SKTypeface? matchedTypeFace = MatchFamily(descr.Family, skStyle);

      // Couldn't find it...check for a close matching family name, e.g. specified "Comic Sans" and we have "Comic Sans MS"
      if (matchedTypeFace is null)
      {
        string? familyName = FindSimilarInstalledFamily(descr.Family);
        if (familyName is not null)
          matchedTypeFace = MatchFamily(familyName, skStyle);
      }

      return matchedTypeFace;
    }

    private SKTypeface? MatchCustomLoadedFamily(GuiFont descr, SKFontStyle skStyle)
    {
      List<SKTypeface>? familyFaces;

      if (!m_customLoadedTypefaces.TryGetValue(descr.Family, out familyFaces))
      {
        // Couldn't find the family, check a close matching family name
        foreach (KeyValuePair<string, List<SKTypeface>> kv in m_customLoadedTypefaces)
        {
          if (kv.Key.Contains(descr.Family))
          {
            familyFaces = kv.Value;
            break;
          }
        }
      }

      if (familyFaces is null)
        return null;

      // Look and score the first typeface
      SKTypeface matchedTypeFace = MatchTypeface(familyFaces[0], skStyle);
      int score = ScoreStyleMatch(matchedTypeFace, skStyle);

      if (score == 0)
        return matchedTypeFace;

      // Look at the rest, if any
      for (int i = 1; i< familyFaces.Count; i++)
      {
        SKTypeface currFace = MatchTypeface(familyFaces[i], skStyle);
        int currScore = ScoreStyleMatch(currFace, skStyle);

        // If current has a score closer to zero, the previous gets bumped. Exactly zero wins automatically
        if (currScore < score)
        {
          score = currScore;
          matchedTypeFace = currFace;

          if (currScore == 0)
            break;
        }
      }

      return matchedTypeFace;
    }

    private string? FindSimilarInstalledFamily(string familyName)
    {
      if (m_loadedFamilies.Count != SKFontManager.Default.FontFamilyCount)
      {
        m_loadedFamilies.Clear();
        m_loadedFamilies.AddRange(SKFontManager.Default.FontFamilies);
      }

      foreach (string loadedFamily in m_loadedFamilies)
      {
        if (loadedFamily.Contains(familyName))
          return loadedFamily;
      }

      return null;
    }

    private void TrackDisposable(SKTypeface? typeface)
    {
      if (typeface is null || typeface.Handle == IntPtr.Zero || m_typefacesToDispose.ContainsKey(typeface.Handle))
        return;

      m_typefacesToDispose.Add(typeface.Handle, typeface);
    }

    private int ScoreStyleMatch(SKTypeface typeFace, SKFontStyle requested)
    {
      int weightDiff = Math.Abs((typeFace.FontWeight - requested.Weight)) / 100;
      int widthDiff = Math.Abs(typeFace.FontWidth - requested.Width);

      // Most likely multiple font files for the same font will be due to italic in one and normal in the other, so if
      // they differ weight it greatly
      int slantdiff = Math.Abs((int) typeFace.FontSlant - (int) requested.Slant) * 1000;

      return weightDiff + widthDiff + slantdiff;
    }

    private FontSetKey CreateFontSetKey(SKTypeface typeface)
    {
      FontWeight weight = FontWeight.Normal;
      int skWeight = typeface.FontWeight;

      if (skWeight <= 199)
        weight = FontWeight.Thin;
      else if (skWeight >= 200 && skWeight <= 299)
        weight = FontWeight.ExtraLight;
      else if (skWeight >= 300 && skWeight <= 349)
        weight = FontWeight.Light;
      else if (skWeight >= 350 && skWeight <= 379)
        weight = FontWeight.SemiLight;
      else if (skWeight >= 380 && skWeight <= 399)
        weight = FontWeight.Book;
      else if (skWeight >= 400 && skWeight <= 499)
        weight = FontWeight.Normal;
      else if (skWeight >= 500 && skWeight <= 599)
        weight = FontWeight.Medium;
      else if (skWeight >= 600 && skWeight <= 699)
        weight = FontWeight.SemiBold;
      else if (skWeight >= 700 && skWeight <= 799)
        weight = FontWeight.Bold;
      else if (skWeight >= 800 && skWeight <= 899)
        weight = FontWeight.ExtraBold;
      else if (skWeight >= 900 && skWeight <= 999)
        weight = FontWeight.Black;
      else if (skWeight >= 1000)
        weight = FontWeight.ExtraBlack;

      FontStyle style = FontStyle.Normal;
      switch (typeface.FontSlant)
      {
        case SKFontStyleSlant.Italic:
          style = FontStyle.Italic;
          break;
        case SKFontStyleSlant.Oblique:
          style = FontStyle.Oblique;
          break;
      }

      // Seems pretty consistent so just do a cast
      FontWidth width = (FontWidth) typeface.FontWidth;
      return new FontSetKey(weight, style, width);
    }

    private GuiFont CreateGuiFont(SKTypeface typeface, GuiFont origDescr)
    {
      FontSetKey key = CreateFontSetKey(typeface);

      FontWeight weight = key.Weight;
      FontStyle style = key.Style;
      FontWidth width = key.Width;

      // Carry over these styles as they're ours, not skia's
      if ((origDescr.Style & FontStyle.ThickOutline) == FontStyle.ThickOutline)
        style |= FontStyle.ThickOutline;

      if ((origDescr.Style & FontStyle.Outline) == FontStyle.Outline)
        style |= FontStyle.Outline;

      if ((origDescr.Style & FontStyle.Embedded) == FontStyle.Embedded)
        style |= FontStyle.Embedded;

      return new GuiFont(origDescr.Family, origDescr.Size, weight, style, width);
    }

    private SKFontStyle CreateSKFontStyle(GuiFont descr)
    {
      SKFontStyleSlant slant = SKFontStyleSlant.Upright;
      if ((descr.Style & FontStyle.Oblique) == FontStyle.Oblique)
        slant = SKFontStyleSlant.Oblique;

      // If have both...override oblique with italic always
      if ((descr.Style & FontStyle.Italic) == FontStyle.Italic)
        slant = SKFontStyleSlant.Italic;

      return new SKFontStyle((int) descr.Weight, (int) descr.Width, slant);
    }

    private unsafe SKTypeface LoadEmbeddedFont()
    {
      // Get imgui to load up the embedded font data then copy it over
      ImFontAtlasPtr atlas = ImGuiNative.ImFontAtlas_ImFontAtlas();
      try
      {
        ImFontPtr font = atlas.AddFontDefault();
        ImFontConfigPtr config = atlas.ConfigData[atlas.ConfigData.Size - 1];
        using UnmanagedMemoryStream ms = new UnmanagedMemoryStream((byte*)config.FontData.ToPointer(), config.FontDataSize);

        SKTypeface embeddedFace = SKFontManager.Default.CreateTypeface(ms);
        TrackDisposable(embeddedFace);
        return embeddedFace;
      }
      finally
      {
        atlas.Destroy();
      }
    }

    private class ResourceFileScanner
    {
      private DateTime? m_lastWritten;
      private HashSet<string> m_scannedPaths;
      private IResourceRepository m_repo;

      public ResourceFileScanner(IResourceRepository repo)
      {
        m_repo = repo;
        m_scannedPaths = new HashSet<string>();
        m_lastWritten = null;
      }

      public void MarkFileFailed(IResourceFile file)
      {
        m_scannedPaths.Remove(file.RelativeName);
      }

      public bool TryDiscoverNewFiles(List<IResourceFile> newDiscoveredFiles)
      {
        DateTime currWritten = m_repo.LastWriteTime;
        if (currWritten == m_lastWritten)
          return false;

        m_lastWritten = currWritten;
        int numAdded = 0;
        foreach (IResourceFile file in m_repo.EnumerateResourceFiles(true))
        {
          if (!s_formats.Contains(file.Extension))
            continue;

          if (m_scannedPaths.Contains(file.RelativeName))
            continue;

          numAdded++;
          newDiscoveredFiles.Add(file);
        }

        return numAdded > 0;
      }
    }
  }
}
