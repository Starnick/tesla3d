﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System.Collections.Generic;
using ImGuiNET;
using Tesla.Input;

namespace Tesla.Gui
{
  internal static class ImGuiInputHelper
  {
    private static List<KeyValuePair<ImGuiKey, Keys>> s_keyMap;
    private static Dictionary<Keys, ImGuiKey> s_keyLookup;

    static ImGuiInputHelper()
    {
      s_keyMap = new List<KeyValuePair<ImGuiKey, Keys>>();
      s_keyLookup = new Dictionary<Keys, ImGuiKey>();

      AddKeyMap(ImGuiKey.Tab, Keys.Tab);
      AddKeyMap(ImGuiKey.LeftArrow, Keys.Left);
      AddKeyMap(ImGuiKey.RightArrow, Keys.Right);
      AddKeyMap(ImGuiKey.UpArrow, Keys.Up);
      AddKeyMap(ImGuiKey.DownArrow, Keys.Down);
      AddKeyMap(ImGuiKey.PageUp, Keys.PageUp);
      AddKeyMap(ImGuiKey.PageDown, Keys.PageDown);
      AddKeyMap(ImGuiKey.Home, Keys.Home);
      AddKeyMap(ImGuiKey.End, Keys.End);
      AddKeyMap(ImGuiKey.Insert, Keys.Insert);
      AddKeyMap(ImGuiKey.Delete, Keys.Delete);
      AddKeyMap(ImGuiKey.Backspace, Keys.Back);
      AddKeyMap(ImGuiKey.Space, Keys.Space);
      AddKeyMap(ImGuiKey.Enter, Keys.Enter);
      AddKeyMap(ImGuiKey.Escape, Keys.Escape);
      AddKeyMap(ImGuiKey.LeftCtrl, Keys.LeftControl);
      AddKeyMap(ImGuiKey.LeftShift, Keys.LeftShift);
      AddKeyMap(ImGuiKey.LeftAlt, Keys.LeftAlt);
      AddKeyMap(ImGuiKey.LeftSuper, Keys.LeftWindows);
      AddKeyMap(ImGuiKey.RightCtrl, Keys.RightControl);
      AddKeyMap(ImGuiKey.RightShift, Keys.RightShift);
      AddKeyMap(ImGuiKey.RightAlt, Keys.RightAlt);
      AddKeyMap(ImGuiKey.RightSuper, Keys.RightWindows);
      AddKeyMap(ImGuiKey._0, Keys.D0);
      AddKeyMap(ImGuiKey._1, Keys.D1);
      AddKeyMap(ImGuiKey._2, Keys.D2);
      AddKeyMap(ImGuiKey._3, Keys.D3);
      AddKeyMap(ImGuiKey._4, Keys.D4);
      AddKeyMap(ImGuiKey._5, Keys.D5);
      AddKeyMap(ImGuiKey._6, Keys.D6);
      AddKeyMap(ImGuiKey._7, Keys.D7);
      AddKeyMap(ImGuiKey._8, Keys.D8);
      AddKeyMap(ImGuiKey._9, Keys.D9);
      AddKeyMap(ImGuiKey.A, Keys.A);
      AddKeyMap(ImGuiKey.B, Keys.B);
      AddKeyMap(ImGuiKey.C, Keys.C);
      AddKeyMap(ImGuiKey.D, Keys.D);
      AddKeyMap(ImGuiKey.E, Keys.E);
      AddKeyMap(ImGuiKey.F, Keys.F);
      AddKeyMap(ImGuiKey.G, Keys.G);
      AddKeyMap(ImGuiKey.H, Keys.H);
      AddKeyMap(ImGuiKey.I, Keys.I);
      AddKeyMap(ImGuiKey.J, Keys.J);
      AddKeyMap(ImGuiKey.K, Keys.K);
      AddKeyMap(ImGuiKey.L, Keys.L);
      AddKeyMap(ImGuiKey.M, Keys.M);
      AddKeyMap(ImGuiKey.N, Keys.N);
      AddKeyMap(ImGuiKey.O, Keys.O);
      AddKeyMap(ImGuiKey.P, Keys.P);
      AddKeyMap(ImGuiKey.Q, Keys.Q);
      AddKeyMap(ImGuiKey.R, Keys.R);
      AddKeyMap(ImGuiKey.S, Keys.S);
      AddKeyMap(ImGuiKey.T, Keys.T);
      AddKeyMap(ImGuiKey.U, Keys.U);
      AddKeyMap(ImGuiKey.V, Keys.V);
      AddKeyMap(ImGuiKey.W, Keys.W);
      AddKeyMap(ImGuiKey.X, Keys.X);
      AddKeyMap(ImGuiKey.Y, Keys.Y);
      AddKeyMap(ImGuiKey.Z, Keys.Z);
      AddKeyMap(ImGuiKey.F1, Keys.F1);
      AddKeyMap(ImGuiKey.F2, Keys.F2);
      AddKeyMap(ImGuiKey.F3, Keys.F3);
      AddKeyMap(ImGuiKey.F4, Keys.F4);
      AddKeyMap(ImGuiKey.F5, Keys.F5);
      AddKeyMap(ImGuiKey.F6, Keys.F6);
      AddKeyMap(ImGuiKey.F7, Keys.F7);
      AddKeyMap(ImGuiKey.F8, Keys.F8);
      AddKeyMap(ImGuiKey.F9, Keys.F9);
      AddKeyMap(ImGuiKey.F10, Keys.F10);
      AddKeyMap(ImGuiKey.F11, Keys.F11);
      AddKeyMap(ImGuiKey.F12, Keys.F12);
      // Missing Apostrophe
      AddKeyMap(ImGuiKey.Comma, Keys.OemComma);
      AddKeyMap(ImGuiKey.Minus, Keys.OemMinus);
      AddKeyMap(ImGuiKey.Period, Keys.OemPeriod);
      // Missing forward slash
      AddKeyMap(ImGuiKey.Semicolon, Keys.OemSemicolon);
      // Missing Equal
      AddKeyMap(ImGuiKey.LeftBracket, Keys.OemOpenBrackets);
      AddKeyMap(ImGuiKey.Backslash, Keys.OemBackslash);
      AddKeyMap(ImGuiKey.RightBracket, Keys.OemCloseBrackets);
      // Not sure what a GraveAccent is
      AddKeyMap(ImGuiKey.CapsLock, Keys.CapsLock);
      AddKeyMap(ImGuiKey.ScrollLock, Keys.Scroll); // Not sure if correct
      AddKeyMap(ImGuiKey.NumLock, Keys.NumLock);
      AddKeyMap(ImGuiKey.PrintScreen, Keys.PrintScreen);
      AddKeyMap(ImGuiKey.Pause, Keys.Pause);
      AddKeyMap(ImGuiKey.Keypad0, Keys.NumPad0);
      AddKeyMap(ImGuiKey.Keypad1, Keys.NumPad1);
      AddKeyMap(ImGuiKey.Keypad2, Keys.NumPad2);
      AddKeyMap(ImGuiKey.Keypad3, Keys.NumPad3);
      AddKeyMap(ImGuiKey.Keypad4, Keys.NumPad4);
      AddKeyMap(ImGuiKey.Keypad5, Keys.NumPad5);
      AddKeyMap(ImGuiKey.Keypad6, Keys.NumPad6);
      AddKeyMap(ImGuiKey.Keypad7, Keys.NumPad7);
      AddKeyMap(ImGuiKey.Keypad8, Keys.NumPad8);
      AddKeyMap(ImGuiKey.Keypad9, Keys.NumPad9);
      AddKeyMap(ImGuiKey.KeypadDecimal, Keys.Decimal);
      AddKeyMap(ImGuiKey.KeypadDivide, Keys.Divide);
      AddKeyMap(ImGuiKey.KeypadMultiply, Keys.Multiply);
      AddKeyMap(ImGuiKey.KeypadSubtract, Keys.Subtract);
      AddKeyMap(ImGuiKey.KeypadAdd, Keys.Add);
      // Missing keypad enter
      // Missing keypad equal
    }

    public static ImGuiKey? ToImGuiKey(Keys key)
    {
      if (s_keyLookup.TryGetValue(key, out ImGuiKey imKey))
        return imKey;

      return null;
    }

    public static void ProcessKeyboardState(ImGuiIOPtr io, KeyboardState ks)
    {
      for (int i = 0; i < s_keyMap.Count; i++)
      {
        KeyValuePair<ImGuiKey, Keys> kv = s_keyMap[i];
        io.AddKeyEvent(kv.Key, ks.IsKeyDown(kv.Value));
      }
    }

    public static void ProcessMouseState(ImGuiIOPtr io, MouseState ms)
    {
      io.AddMouseButtonEvent((int) ImGuiMouseButton.Left, ms.IsButtonPressed(MouseButton.Left));
      io.AddMouseButtonEvent((int) ImGuiMouseButton.Right, ms.IsButtonPressed(MouseButton.Right));
      io.AddMouseButtonEvent((int) ImGuiMouseButton.Middle, ms.IsButtonPressed(MouseButton.Middle));

      io.AddMousePosEvent(ms.X, ms.Y);
      io.AddMouseWheelEvent(0, ms.ScrollWheelDeltaNormalized);
    }

    private static void AddKeyMap(ImGuiKey imKey, Keys key)
    {
      s_keyMap.Add(new KeyValuePair<ImGuiKey, Keys>(imKey, key));
      s_keyLookup.Add(key, imKey);
    }
  }
}
