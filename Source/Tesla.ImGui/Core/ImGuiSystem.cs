﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ImGuiNET;
using Tesla.Application;
using Tesla.Graphics;
using Tesla.Input;

namespace Tesla.Gui
{
  [RequiredDependency(typeof(IRenderSystem))]
  public sealed class ImGuiSystem : IGuiSystem, IEngineServiceDependencyChanged
  {
    private bool m_isDisposed;
    private float m_elapsedTimeSeconds;
    private Dictionary<IWindow, WindowContext> m_windowContexts;
    private ImGuiFontManager m_fontManager;

    // Gpu resources
    private Dictionary<GuiTextureID, TextureEntry> m_textures;
    private IRenderSystem? m_renderSystem;
    private RasterizerState? m_rasterizerState;
    private Effect? m_effect;
    private IEffectParameter? m_projMatrixParam;
    private IEffectParameter? m_textureParam;
    private IEffectParameter? m_outlineColorParam;
    private IEffectShaderGroup? m_fontShader;
    private IEffectShaderGroup? m_colorShader;
    private IEffectShaderGroup? m_colorPremultipliedShader;

    private IndexedBatchVertexBuffer<ImGuiVertex>? m_buffers;

    public bool IsDisposed { get { return m_isDisposed; } }

    public string Name { get { return "DearImGui_GuiSystem"; } }

    public IGuiFontManager FontManager { get { return m_fontManager; } }

    public ImGuiSystem()
    {
      m_isDisposed = false;
      m_windowContexts = new Dictionary<IWindow, WindowContext>();
      m_textures = new Dictionary<GuiTextureID, TextureEntry>();
      m_fontManager = new ImGuiFontManager(this);
    }

    /// <inheritdoc />
    public unsafe void Initialize(Engine engine)
    {
      m_renderSystem = engine.Services.GetService<IRenderSystem>();
      m_fontManager.Initialize(m_renderSystem);

      LoadGpuResources(m_renderSystem);
    }

    /// <inheritdoc />
    public void OnDependencyChanged(EngineServiceChangedEventArgs args)
    {
      if (args.IsService<IRenderSystem>())
      {
        DisposeGpuResources();
        m_renderSystem = args.ServiceAs<IRenderSystem>();
        LoadGpuResources(m_renderSystem);
      }
    }

    public unsafe void SetDefaultFontForAllContexts(GuiFont descr, ImFontPtr imFont)
    {
      IntPtr currCtx = ImGui.GetCurrentContext();

      foreach (KeyValuePair<IWindow, WindowContext> kv in m_windowContexts)
        kv.Value.SetDefaultFont(descr, imFont);

      ImGui.SetCurrentContext(currCtx);
    }

    public unsafe bool TrySetDefaultForContext(IWindow window, GuiFont descr, ImFontPtr imFont)
    {
      if (window is null || !m_windowContexts.TryGetValue(window, out WindowContext? ctx))
        return false;

      IntPtr currCtx = ImGui.GetCurrentContext();

      ctx.SetDefaultFont(descr, imFont);

      ImGui.SetCurrentContext(currCtx);

      return true;
    }

    public GuiFont? TryGetContextDefaultFont(IWindow window)
    {
      if (window is null || !m_windowContexts.TryGetValue(window, out WindowContext? ctx))
        return null;

      return ctx.DefaultFont;
    }

    /// <inheritdoc />
    public InputDeviceFocus GetInputDeviceFocus(IWindow? window)
    {
      if (window is not null && m_windowContexts.TryGetValue(window, out WindowContext? ctx))
      {
        IntPtr currCtx = ImGui.GetCurrentContext();
        bool setCtx = ctx.ImGuiContext != currCtx;
        try
        {
          return GetWindowInputDeviceFocus(ctx, setCtx);
        }
        finally
        {
          if (setCtx)
            ImGui.SetCurrentContext(currCtx);
        }
      }
      else
      {
        IntPtr currCtx = ImGui.GetCurrentContext();
        IntPtr lastCtx = currCtx;
        InputDeviceFocus flags = InputDeviceFocus.None;
        try
        {
          foreach (WindowContext winCtx in m_windowContexts.Values)
          {
            InputDeviceFocus winFlags = GetWindowInputDeviceFocus(winCtx, lastCtx == winCtx.ImGuiContext);
            lastCtx = winCtx.ImGuiContext;
            flags |= winFlags;

            // If already both, no need to keep checking contexts
            if (flags == (InputDeviceFocus.Keyboard | InputDeviceFocus.Mouse))
              return flags;
          }
        } 
        finally
        {
          if (lastCtx != currCtx)
            ImGui.SetCurrentContext(currCtx);
        }

        return flags;
      }
    }

    private InputDeviceFocus GetWindowInputDeviceFocus(WindowContext ctx, bool setCtx)
    {
      if (setCtx)
        ImGui.SetCurrentContext(ctx.ImGuiContext);

      InputDeviceFocus flags = InputDeviceFocus.None;
      ImGuiIOPtr io = ImGui.GetIO();
      if (io.WantCaptureKeyboard)
        flags |= InputDeviceFocus.Keyboard;

      if (io.WantCaptureMouse || io.WantCaptureMouseUnlessPopupClose)
        flags |= InputDeviceFocus.Mouse;

      return flags;
    }

    /// <inheritdoc />
    public GuiTextureID AddTexture(Texture2D texture, bool takeOwnership = true, bool isPremultipliedAlpha = false)
    {
      ArgumentNullException.ThrowIfNull(texture, nameof(texture));

      GuiTextureID id = texture;
      if (!m_textures.ContainsKey(id))
        m_textures.Add(id, new TextureEntry(texture, false, takeOwnership, isPremultipliedAlpha));

      return id;
    }

    public GuiTextureID AddFontTexture(Texture2D texture)
    {
      ArgumentNullException.ThrowIfNull(texture, nameof(texture));

      GuiTextureID id = texture;
      if (!m_textures.ContainsKey(id))
        m_textures.Add(id, new TextureEntry(texture, true, true, true));

      return id;
    }

    /// <inheritdoc />
    public Texture2D? GetTexture(GuiTextureID texId)
    {
      if (texId == GuiTextureID.Invalid)
        return null;

      if (m_textures.TryGetValue(texId, out TextureEntry tex))
        return tex.Texture;

      return null;
    }

    /// <inheritdoc />
    public Texture2D? RemoveTexture(GuiTextureID texId, bool dispose = true)
    {
      if (texId == GuiTextureID.Invalid)
        return null;

      if (!m_textures.TryGetValue(texId, out TextureEntry tex))
        return null;

      m_textures.Remove(texId);

      if (dispose)
        tex.Texture.Dispose();

      return tex.Texture;
    }

    /// <inheritdoc />
    public void Update(IGameTime time)
    {
      m_elapsedTimeSeconds = time.ElapsedTimeInSeconds;

      // Ensure all contexts are no longer in frame
      foreach (KeyValuePair<IWindow, WindowContext> kv in m_windowContexts)
      {
        WindowContext ctx = kv.Value;
        if (ctx.IsInFrame)
        {
          ImGui.SetCurrentContext(ctx.ImGuiContext);
          ImGui.EndFrame();
        }
      }

      // Make updates to any queued font changes
      m_fontManager.BuildPendingFonts(m_renderSystem!);

      // Setup the new frame for each window context all at the same time, so we can setup the same input and communicate
      // with the rest of the engine if the GUI wants to capture the mouse/keyboard
      foreach (KeyValuePair<IWindow, WindowContext> kv in m_windowContexts)
      {
        UpdateContext(kv.Key, kv.Value);

        ImGui.NewFrame();

        kv.Value.IsInFrame = true;
      }
    }

    /// <inheritdoc />
    public void StartOrResumeFrame(IWindow window)
    {
      // Only update input / start frame if haven't seen before
      CreateOrApplyWindowContext(window, true);
    }

    /// <inheritdoc />
    public void Render(IRenderContext renderContext, IWindow window)
    {
      WindowContext ctx = CreateOrApplyWindowContext(window, true);

      ImGui.Render();
      ctx.IsInFrame = false;

      DrawImGui(renderContext, window, ImGui.GetDrawData());
    }

    /// <inheritdoc />
    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    /// <summary>
    /// Gets a task that completes for all pending task loads. Exposed for unit tests
    /// </summary>
    /// <returns>Task that completes when pending fonts have loaded.</returns>
    public Task WhenPendingFontsLoad()
    {
      return m_fontManager.WhenPendingFontsLoad(m_renderSystem!);
    }

    private void DrawImGui(IRenderContext renderContext, IWindow window, ImDrawDataPtr drawData)
    {
      if (m_buffers is null)
        return;

      Rectangle lastScissorRect = renderContext.ScissorRectangle;
      Viewport lastViewport = renderContext.Camera.Viewport;

      // Make sure the swap chain is currently set
      SwapChain? oldSwapChain = null;
      if (window.SwapChain != renderContext.BackBuffer)
        window.SwapChain.SetActive(renderContext);

      renderContext.Camera.Viewport = new Viewport(window.ClientBounds);
      renderContext.BlendFactor = Color.White;
      renderContext.RasterizerState = m_rasterizerState;
      renderContext.BlendState = BlendState.AlphaBlendPremultiplied;
      renderContext.DepthStencilState = DepthStencilState.None;

      // Handle cases when screen coordinates != backbuffer coordinates (e.g. retina)
      drawData.ScaleClipRects(ImGui.GetIO().DisplayFramebufferScale);

      ImGuiIOPtr io = ImGui.GetIO();

      Matrix m = Matrix.CreateOrthographicMatrix(0, io.DisplaySize.X, io.DisplaySize.Y, 0, -1f, 1f);
      m_projMatrixParam!.SetValue(m);

      GuiTextureID prevTexId = GuiTextureID.Invalid;
      TextureEntry? prevTex = null;

      for (int i = 0; i < drawData.CmdListsCount; i++)
      {
        ImDrawListPtr cmdList = drawData.CmdListsRange[i];
        if (cmdList.CmdBuffer.Size == 0)
          continue;

        int vbOffset = 0;
        int ibOffset = 0;

        // Valid for a draw list not to have any data if it's commands only change state
        if (cmdList.VtxBuffer.Size > 0 && cmdList.IdxBuffer.Size > 0)
        {
          ReadOnlySpan<ImGuiVertex> vbData = cmdList.VtxBuffer.Data.AsReadOnlySpan<ImGuiVertex>(cmdList.VtxBuffer.Size);
          ReadOnlySpan<ushort> ibData = cmdList.IdxBuffer.Data.AsReadOnlySpan<ushort>(cmdList.IdxBuffer.Size);

          IndexedBatch<IndexBuffer, VertexBuffer> batch = m_buffers.AppendBatch(renderContext, vbData, ibData);
          renderContext.SetVertexBuffer(batch.VertexBuffer);
          renderContext.SetIndexBuffer(batch.IndexBuffer);

          vbOffset = batch.BaseVertexOffset;
          ibOffset = batch.StartIndex;
        }

        for (int j = 0; j < cmdList.CmdBuffer.Size; j++)
        {
          ImDrawCmdPtr cmd = cmdList.CmdBuffer[j];
          bool applyShaders = false;

          // If command has a callback, pick up a render state change, otherwise the command is not a regular draw command
          if (cmd.UserCallback != IntPtr.Zero)
          {
            switch ((GuiRenderState) cmd.UserCallback)
            {
              case GuiRenderState.OutlineColor:
                {
                  Color c = new Color((uint) cmd.UserCallbackData.ToInt64());
                  m_outlineColorParam!.SetValue(c.ToVector4());
                  prevTex = null; // Next draw command, force shaders to apply
                  break;
                }
            }
          }

          if (cmd.ElemCount == 0)
            continue;

          // Otherwise regular draw command, detect texture change
          if (!prevTex.HasValue || cmd.TextureId != prevTexId || cmd.TextureId == GuiTextureID.Invalid)
          {
            prevTex = new TextureEntry(Texture.Default2D, false, false);
            prevTexId = GuiTextureID.Invalid;

            if (m_textures.TryGetValue(cmd.TextureId, out TextureEntry currTex))
            {
              // Check if texture is still alive
              if (!currTex.Texture.IsDisposed)
                prevTex = currTex;
              else
                m_textures.Remove(cmd.TextureId);
            }

            m_textureParam!.SetResource<Texture2D>(currTex.Texture);
            applyShaders = true;
          }

          if (applyShaders)
          {
            TextureEntry texEntry = prevTex.Value;
            if (texEntry.IsFont)
              m_fontShader!.Apply(renderContext);
            else
            {
              if (texEntry.IsPremultiplyAlpha)
                m_colorPremultipliedShader!.Apply(renderContext);
              else
                m_colorShader!.Apply(renderContext);
            }
          }

          ApplyScissorRectangle(renderContext, cmd);
          renderContext.DrawIndexed(PrimitiveType.TriangleList, (int) cmd.ElemCount, (int) cmd.IdxOffset + ibOffset, (int) cmd.VtxOffset + vbOffset);
        }

        vbOffset += cmdList.VtxBuffer.Size;
        ibOffset += cmdList.IdxBuffer.Size;
      }


      renderContext.ScissorRectangle = lastScissorRect;
      renderContext.Camera.Viewport = lastViewport;

      if (oldSwapChain is not null)
        renderContext.BackBuffer = oldSwapChain;
    }

    private unsafe void Dispose(bool disposing)
    {
      if (!m_isDisposed)
      {
        if (disposing)
        {
          foreach (KeyValuePair<IWindow, WindowContext> kv in m_windowContexts)
            DestroyWindowContext(kv.Key, kv.Value);

          m_windowContexts.Clear();
          m_fontManager.Dispose();

          DisposeGpuResources();
        }

        m_isDisposed = true;
      }
    }

    private void UpdateContext(IWindow window, WindowContext context)
    {
      ImGui.SetCurrentContext(context.ImGuiContext);

      ImGuiIOPtr io = ImGui.GetIO();
      io.DeltaTime = m_elapsedTimeSeconds;

      Rectangle windowRect = window.ClientBounds;
      io.DisplaySize = new System.Numerics.Vector2(windowRect.Width, windowRect.Height);
      io.DisplayFramebufferScale = new System.Numerics.Vector2(1, 1);

      if (Keyboard.IsAvailable && window.Focused)
      {
        ImGuiInputHelper.ProcessKeyboardState(io, Keyboard.GetKeyboardState());

        foreach (char textChar in context.InputCharacters)
          io.AddInputCharacter(textChar);

        // Determine if we need to tell the keyboard system that the gui wants to capture it
        if (Keyboard.FocusState == InputFocusState.App && io.WantCaptureKeyboard)
          Keyboard.FocusState = InputFocusState.Gui;
      }

      if (Mouse.IsAvailable && window.Focused)
      {
        ImGuiInputHelper.ProcessMouseState(io, Mouse.GetMouseState(window.Handle));

        // Determine if we need to tell the mouse system that the gui wants to capture it
        if (Mouse.FocusState == InputFocusState.App && (io.WantCaptureMouse || io.WantCaptureMouseUnlessPopupClose))
          Mouse.FocusState = InputFocusState.Gui;
      }

      bool isFocused = window.Focused;
      if (isFocused != context.HasFocus)
        io.AddFocusEvent(isFocused);

      context.InputCharacters.Clear();
      context.HasFocus = isFocused;
    }

    private unsafe WindowContext CreateOrApplyWindowContext(IWindow window, bool setupForNewFrame = false)
    {
      if (!m_windowContexts.TryGetValue(window, out WindowContext? ctx))
      {
        // Creates and applies the window context
        ctx = new WindowContext(ImGui.CreateContext(m_fontManager.DefaultFontAtlas));
        ctx.SetDefaultFont(m_fontManager.DefaultFont, m_fontManager.GetSystemDefaultImFont());
        ctx.HasFocus = window.Focused;

        // Set some default flags
        ImGuiIOPtr io = ImGui.GetIO();
        io.BackendFlags |= ImGuiBackendFlags.RendererHasVtxOffset | ImGuiBackendFlags.HasMouseCursors;
        io.WantSaveIniSettings = false;

        m_windowContexts.Add(window, ctx);
        window.Disposed += HandleWindowDisposed;
        window.TextInput += HandleTextInput;

        if (setupForNewFrame)
        {
          UpdateContext(window, ctx);
          ImGui.NewFrame();

          ctx.IsInFrame = true;
        }
      }
      else
      {
        ImGui.SetCurrentContext(ctx.ImGuiContext);
      }

      return ctx;
    }

    private void HandleWindowDisposed(IWindow window, EventArgs args)
    {
      if (window is null || !m_windowContexts.TryGetValue(window, out WindowContext? ctx))
        return;

      DestroyWindowContext(window, ctx);
      m_windowContexts.Remove(window);
    }

    private void DestroyWindowContext(IWindow window, WindowContext ctx)
    {
      window.Disposed -= HandleWindowDisposed;
      window.TextInput -= HandleTextInput;
      ImGui.DestroyContext(ctx.ImGuiContext);
    }

    private void HandleTextInput(IWindow sender, TextInputKey textKey)
    {
      if (sender is null || !m_windowContexts.TryGetValue(sender, out WindowContext? ctx))
        return;

      ctx.InputCharacters.Add(textKey.Character);
    }

    private void ApplyScissorRectangle(IRenderContext renderContext, ImDrawCmdPtr cmd)
    {
      ref System.Numerics.Vector4 clipRect = ref cmd.ClipRect;
      renderContext.ScissorRectangle = new Rectangle((int) clipRect.X, (int) clipRect.Y, (int) (clipRect.Z - clipRect.X), (int) (clipRect.W - clipRect.Y));
    }

    private void LoadGpuResources(IRenderSystem renderSystem)
    {
      LoadEffects(renderSystem);

      m_rasterizerState = new RasterizerState(renderSystem);
      m_rasterizerState.Cull = CullMode.None;
      m_rasterizerState.Fill = FillMode.Solid;
      m_rasterizerState.SlopeScaledDepthBias = 0;
      m_rasterizerState.DepthBias = 0;
      m_rasterizerState.MultiSampleEnable = false;

      m_rasterizerState.ScissorTestEnable = true;
      m_rasterizerState.DebugName = "ImGui_RasterizerState";

      m_buffers = new IndexedBatchVertexBuffer<ImGuiVertex>(renderSystem, 10_000, IndexFormat.SixteenBits, 10_000);
      m_buffers.DebugName = "ImGui";
    }

    private void LoadEffects(IRenderSystem renderSystem)
    {
      renderSystem.StandardEffects.LoadProvider(new ImGuiEffectByteCodeProvider());
      m_effect = renderSystem.StandardEffects.CreateEffect("Gui/ImGui")!;
      m_effect.DebugName = "ImGui";
      m_projMatrixParam = m_effect.Parameters["ProjectionMatrix"];
      m_textureParam = m_effect.Parameters["ImGuiTexture"];
      m_outlineColorParam = m_effect.Parameters["OutlineColor"];
      m_fontShader = m_effect.ShaderGroups["ImGuiFont"];
      m_colorShader = m_effect.ShaderGroups["ImGuiColor"];
      m_colorPremultipliedShader = m_effect.ShaderGroups["ImGuiColorPremultiplied"];

      m_outlineColorParam.SetValue<Vector4>(new Vector4(0, 0, 0, 1));
    }

    private void DisposeGpuResources()
    {
      if (m_buffers is not null)
      {
        m_buffers.Dispose();
        m_buffers = null;
      }

      if (m_effect is not null)
      {
        m_effect.Dispose();
        m_effect = null;
        m_projMatrixParam = null;
        m_textureParam = null;
      }

      foreach (KeyValuePair<GuiTextureID, TextureEntry> kv in m_textures)
      {
        if (kv.Value.IsOwned)
          kv.Value.Texture.Dispose();
      }

      m_textures.Clear();
    }

    private class WindowContext
    {
      public IntPtr ImGuiContext;
      public bool HasFocus;
      public bool IsInFrame;
      public GuiFont DefaultFont;
      public List<char> InputCharacters;

      public WindowContext(IntPtr context)
      {
        ImGuiContext = context;
        InputCharacters = new List<char>();
        HasFocus = false;
        IsInFrame = false;
      }

      public unsafe void SetDefaultFont(GuiFont descr, ImFontPtr font)
      {
        ImGui.SetCurrentContext(ImGuiContext);
        ImGui.GetIO().NativePtr->FontDefault = font.NativePtr;
        DefaultFont = descr;
      }
    }

    private struct TextureEntry
    {
      public Texture2D Texture;
      public bool IsFont;
      public bool IsOwned;
      public bool IsPremultiplyAlpha;

      public TextureEntry(Texture2D tex, bool isFont, bool isOwned, bool isPremultiplyAlpha = true)
      {
        Texture = tex;
        IsFont = isFont;
        IsOwned = isOwned;
        IsPremultiplyAlpha = isPremultiplyAlpha || isFont;
      }
    }
  }
}
