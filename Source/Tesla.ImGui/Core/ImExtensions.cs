﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Runtime.InteropServices;
using System.Text;
using ImGuiNET;

namespace Tesla.Gui
{
  public static class ImExtensions
  {
    public static unsafe void SetRenderState(this ImDrawListPtr drawList, GuiRenderState type, int value)
    {
      drawList.AddCallback(new IntPtr((int) type), new IntPtr(value));
    }

    public static unsafe void SetRenderState(this ImDrawListPtr drawList, GuiRenderState type, Color value)
    {
      drawList.AddCallback(new IntPtr((int) type), new IntPtr(value.PackedValue));
    }

    public static unsafe void SetOutlineColor(this ImDrawListPtr drawList, Color value)
    {
      drawList.SetRenderState(GuiRenderState.OutlineColor, value);
    }

    public static unsafe void ResetOutlineColor(this ImDrawListPtr drawList)
    {
      drawList.SetRenderState(GuiRenderState.OutlineColor, Color.Black);
    }

    public static unsafe void AddPolyline(this ImDrawListPtr drawList, ReadOnlySpan<Vector2> points, Color color, float thickness, ImDrawFlags drawFlags = ImDrawFlags.None)
    {
      if (points.IsEmpty)
        return;

      fixed (Vector2* pts = &points[0])
        ImGuiNative.ImDrawList_AddPolyline(drawList.NativePtr, (System.Numerics.Vector2*) pts, points.Length, color.PackedValue, drawFlags, thickness);
    }

    public static unsafe void AddConvexPolyFilled(this ImDrawListPtr drawList, ReadOnlySpan<Vector2> points, Color color)
    {
      if (points.IsEmpty)
        return;

      fixed (Vector2* pts = &points[0])
        ImGuiNative.ImDrawList_AddConvexPolyFilled(drawList.NativePtr, (System.Numerics.Vector2*) pts, points.Length, color.PackedValue);
    }

    public static unsafe ImFontGlyphExPtr GetGlyphEx(this ImFontPtr font, int index)
    {
      return new ImFontGlyphExPtr(font.Glyphs[index].NativePtr);
    }

    public static unsafe ImFontGlyphExPtr FindGlyphEx(this ImFontPtr font, uint codePoint)
    {
      return new ImFontGlyphExPtr(font.FindGlyph((ushort) codePoint));
    }

    public static unsafe ImFontGlyphExPtr FindGlyphNoFallbackEx(this ImFontPtr font, uint codePoint)
    {
      return new ImFontGlyphExPtr(font.FindGlyphNoFallback((ushort) codePoint));
    }

    public static unsafe void SetFontName(this ImFontConfigPtr config, string name)
    {
      byte[] utfName = Encoding.UTF8.GetBytes(name);
      NativeMemory.Clear(config.Name.Data, (nuint) config.Name.Count);
      int length = Math.Min(config.Name.Count, utfName.Length);
      for (int i = 0; i < length; i++)
        config.Name[i] = utfName[i];
    }

    public static unsafe bool Equals(this NullTerminatedString str, byte[] utf8)
    {
      for (int i = 0; i < utf8.Length; i++)
      {
        if (str.Data[i] != utf8[i])
          return false;
      }

      return true;
    }
  }
}
