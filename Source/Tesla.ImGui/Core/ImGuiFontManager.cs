﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ImGuiNET;
using Tesla.Application;
using Tesla.Content;
using Tesla.Graphics;

namespace Tesla.Gui
{
  public sealed class ImGuiFontManager : IGuiFontManager, IDisposable
  {
    public static readonly GuiFont EmbeddedDefaultFont = new GuiFont("ProggyClean", 13);

    private ImGuiSystem m_guiSystem;
    private ImFontBuilder? m_currentFontBuilder;
    private List<ImFontAtlasPtr> m_fontAtlasses;
    private Dictionary<IntPtr, GuiFont> m_imFontToDescr;

    private ImFontsCache m_fontsCache;
    private SKFontLoader m_fontLoader;
    private List<GuiFontFamily> m_loadedFontFamilyDescrs;
    private bool m_loadedFontFamilyDescrDirty;

    private List<Task<FontLoadResult>> m_pendingLoadTasks;
    private List<Task<FontLoadResult>> m_pendingLoadTasksProcessing;
    private bool m_rebuildingAtlasses;

    private GuiFont m_defaultFontDescr;
    private int m_maxTextureDims;

    /// <inheritdoc />
    public GuiFont DefaultFont { get { return m_defaultFontDescr; } }

    /// <inheritdoc />
    public IReadOnlyList<GuiFontFamily> LoadedFonts
    {
      get
      {
        if (m_loadedFontFamilyDescrDirty)
        {
          m_loadedFontFamilyDescrs.Clear();
          m_fontsCache.GatherGuiFontFamilies(m_loadedFontFamilyDescrs);
          m_loadedFontFamilyDescrDirty = false;
        }

        return m_loadedFontFamilyDescrs;
      }
    }

    /// <inheritdoc />
    public unsafe GuiFont ActiveFont
    {
      get
      {
        if (ImGui.GetCurrentContext() == IntPtr.Zero)
          return m_defaultFontDescr;

        IntPtr activeImFont = new IntPtr(ImGui.GetFont().NativePtr);
        if (m_imFontToDescr.TryGetValue(activeImFont, out GuiFont descr))
          return descr;

        return m_defaultFontDescr;
      }
    }

    /// <summary>
    /// Gets the default font atlas that window contexts can be created with.
    /// </summary>
    public ImFontAtlasPtr DefaultFontAtlas { get { return m_fontAtlasses[0]; } }

    public ImGuiFontManager(ImGuiSystem guiSystem)
    {
      m_guiSystem = guiSystem;
      m_fontAtlasses = new List<ImFontAtlasPtr>();
      m_imFontToDescr = new Dictionary<IntPtr, GuiFont>();
      m_fontsCache = new ImFontsCache();
      m_loadedFontFamilyDescrs = new List<GuiFontFamily>();
      m_pendingLoadTasks = new List<Task<FontLoadResult>>();
      m_pendingLoadTasksProcessing = new List<Task<FontLoadResult>>();
      m_loadedFontFamilyDescrDirty = false;
      m_rebuildingAtlasses = false;
      m_maxTextureDims = 4096;

      m_defaultFontDescr = EmbeddedDefaultFont;
      m_fontLoader = new SKFontLoader();
    }

    /// <inheritdoc />
    public void AddFontPath(IResourceRepository repo)
    {
      m_fontLoader.AddFontPath(repo);
    }

    /// <inheritdoc />
    public IEnumerable<IResourceRepository> GetFontPaths()
    {
      return m_fontLoader.GetFontPaths();
    }

    /// <inheritdoc />
    public void RemoveFontPath(IResourceRepository repo)
    {
      m_fontLoader.RemoveFontPath(repo);
    }

    /// <summary>
    /// Gets the system-wide default ImGui font.
    /// </summary>
    /// <returns>ImGui default font.</returns>
    public ImFontPtr GetSystemDefaultImFont()
    {
      CachedFont font = FindCachedFont(m_defaultFontDescr);
      Debug.Assert(font.Status == FontLoadStatus.Loaded);

      return font.ImFont;
    }

    /// <inheritdoc />
    public FontLoadStatus GetFontLoadStatus(GuiFont font)
    {
      return m_fontsCache.GetFontLoadStatus(font);
    }

    /// <inheritdoc />
    public FontLoadStatus LoadFont(GuiFont font)
    {
      font = font.RemoveInvalidStyles();
      CachedFont cachedFont = FindCachedFont(font, false);

      switch (cachedFont.Status)
      {
        // If previously failed, schedule to try again
        case FontLoadStatus.Missing:
          {
            RetryLoadingFonts(cachedFont);
            return FontLoadStatus.Scheduled;
          }
      }

      return cachedFont.Status;
    }

    /// <inheritdoc />
    public bool TrySetDefaultFont(GuiFont font, IWindow? window = null)
    {
      font = font.RemoveInvalidStyles();

      if (window is null && m_defaultFontDescr.Equals(font))
        return true;

      CachedFont cachedFont = FindCachedFont(font, false);
      if (cachedFont.Status != FontLoadStatus.Loaded)
        return false;

      if (window is not null)
        return m_guiSystem.TrySetDefaultForContext(window, cachedFont.Description, cachedFont.ImFont);

      m_guiSystem.SetDefaultFontForAllContexts(cachedFont.Description, cachedFont.ImFont);
      return true;
    }

    /// <inheritdoc />
    public GuiFont GetDefaultFont(IWindow window)
    {
      GuiFont? font = m_guiSystem.TryGetContextDefaultFont(window);
      if (font.HasValue)
        return font.Value;

      return m_defaultFontDescr;
    }

    /// <inheritdoc />
    public void PushFont(GuiFont font)
    {
      if (ImGui.GetCurrentContext() == IntPtr.Zero)
        return;

      if (!font.IsValid)
        font = m_defaultFontDescr;
      else
        font = font.RemoveInvalidStyles();

      CachedFont cachedFont = FindCachedFont(font);
      ImGui.PushFont(cachedFont.ImFont);
    }

    /// <inheritdoc />
    public void PopFont()
    {
      if (ImGui.GetCurrentContext() == IntPtr.Zero)
        return;

      ImGui.PopFont();
    }

    /// <summary>
    /// Initializes the font manager and adds the default ImGui embedded font to the
    /// font atlas.
    /// </summary>
    /// <param name="renderSystem">Render system to build textures.</param>
    public void Initialize(IRenderSystem renderSystem)
    {
      m_maxTextureDims = Math.Min(m_maxTextureDims, renderSystem.Adapter.MaximumTexture2DSize);

      // Load the first font and build the atlas texture, do not wait
      CachedFont defaultFont = GetDefaultCachedFont(false);
      m_fontLoader.SetImFontData(defaultFont.FontSet, m_fontLoader.EmbeddedTypeFace); // Let imgui use their font generation

      ImFontBitmaps? bitmaps = defaultFont.GenerateFontBitmaps();
      if (bitmaps is null)
        throw new InvalidOperationException();

      AppendFontToCurrentAtlas(renderSystem, defaultFont, bitmaps);
      RebuildCurrentFontAtlasTexture(renderSystem);
    }

    /// <summary>
    /// Gets a task that completes for all pending task loads. Exposed for unit tests
    /// </summary>
    /// <param name="renderSystem">Render system to build textures.</param>
    /// <returns>Task that completes when pending fonts have loaded.</returns>
    public Task WhenPendingFontsLoad(IRenderSystem renderSystem)
    {
      if (m_pendingLoadTasks.Count == 0)
        return Task.CompletedTask;

      Task completion = Task.WhenAll(m_pendingLoadTasks);
      return completion.ContinueWith((Task t) =>
      {
        BuildPendingFonts(renderSystem);
      });
    }

    /// <summary>
    /// Processes any scheduled font loading and rebuilds the font atlass(es). This -must-
    /// be called when all window contexts have ended their frames.
    /// </summary>
    /// <param name="renderSystem">Render system to build textures.</param>
    public void BuildPendingFonts(IRenderSystem renderSystem)
    {
      if (m_pendingLoadTasks.Count == 0 || m_rebuildingAtlasses)
        return;

      m_rebuildingAtlasses = true;

      // Swap the pending tasks. Any tasks we're waiting on or new fonts to load discovered during this processing will be taken care of next frame.
      m_pendingLoadTasksProcessing.Clear();
      m_pendingLoadTasksProcessing = Interlocked.Exchange(ref m_pendingLoadTasks, m_pendingLoadTasksProcessing);

      try
      {
        int numAppended = 0;
        foreach (Task<FontLoadResult> task in m_pendingLoadTasksProcessing)
        {
          if (!task.IsCompletedSuccessfully)
          {
            m_pendingLoadTasks.Add(task);
            continue;
          }

          FontLoadResult result = task.Result;
          if (result.Bitmaps is null)
          {
            // Failed to load...get the default font as fallback...
            result.Font.MarkMissing(GetDefaultCachedFont());
            continue;
          }

          AppendFontToCurrentAtlas(renderSystem, result.Font, result.Bitmaps);
          numAppended++;
        }

        if (numAppended > 0)
        {
          RebuildCurrentFontAtlasTexture(renderSystem);
          m_loadedFontFamilyDescrDirty = true;
        }
      }
      finally
      {
        m_pendingLoadTasksProcessing.Clear();
        m_rebuildingAtlasses = false;
      }
    }

    /// <inheritdoc />
    public void Dispose()
    {
      m_currentFontBuilder?.Dispose();
      m_currentFontBuilder = null;

      foreach (ImFontAtlasPtr atlas in m_fontAtlasses)
      {
        m_guiSystem.RemoveTexture(atlas.TexID, true);
        atlas.Destroy();
      }

      m_fontAtlasses.Clear();
      m_imFontToDescr.Clear();
      m_fontsCache.Dispose();
      m_fontLoader.Dispose();

      GC.SuppressFinalize(this);
    }

    private CachedFont FindCachedFont(GuiFont descr, bool fallbackToDefault = false)
    {
      CachedFont cachedFont = m_fontsCache.FindOrCreateFont(descr);

      switch (cachedFont.Status)
      {
        case FontLoadStatus.Loaded:
          return cachedFont;
        case FontLoadStatus.NotLoaded:
          StartLoadingFont(cachedFont);
          break;
      }

      // Fallback to default if it's still loading or missing. During a frame we cannot update the font atlas,
      // so when queried we need something to display
      if (fallbackToDefault)
        cachedFont = GetDefaultCachedFont();

      return cachedFont;
    }

    private CachedFont GetDefaultCachedFont(bool validateIsLoaded = true)
    {
      CachedFont cachedFont = m_fontsCache.FindOrCreateFont(m_defaultFontDescr);

      if (validateIsLoaded)
        Debug.Assert(cachedFont.Status == FontLoadStatus.Loaded);

      return cachedFont;
    }

    [MemberNotNull(nameof(m_currentFontBuilder))]
    private ImFontBuilder CreateNextBuilder()
    {
      ImFontBuilder? prev = m_currentFontBuilder;

      m_currentFontBuilder = ImFontBuilder.Create(m_maxTextureDims);
      m_fontAtlasses.Add(m_currentFontBuilder.Atlas);

      // Dispose of input data, this does not dispose the atlas. That is tracked separately.
      if (prev is not null)
        prev.Dispose();

      return m_currentFontBuilder;
    }

    [MemberNotNull(nameof(m_currentFontBuilder))]
    private unsafe void AppendFontToCurrentAtlas(IRenderSystem renderSystem, CachedFont font, ImFontBitmaps bitmaps)
    {
      if (m_currentFontBuilder is null || !m_currentFontBuilder.HasRoomFor(bitmaps))
      {
        RebuildCurrentFontAtlasTexture(renderSystem);
        m_currentFontBuilder = CreateNextBuilder();
      }

      ImFontPtr imFont = m_currentFontBuilder.AddFont(bitmaps);
      font.MarkLoaded(imFont);
      m_imFontToDescr.Add(new IntPtr(imFont.NativePtr), font.Description);
    }

    private void RebuildCurrentFontAtlasTexture(IRenderSystem renderSystem)
    {
      if (m_currentFontBuilder is null)
        return;

      GuiTextureID id = m_currentFontBuilder.Atlas.TexID;
      if (id != GuiTextureID.Invalid)
        m_guiSystem.RemoveTexture(id, true);

      Texture2D atlasTexture = m_currentFontBuilder.Build(renderSystem);

      // Set a debug name to easily identify, the current font builder is always the last atlas in the list
      int index = m_fontAtlasses.Count - 1;
      atlasTexture.DebugName = $"ImGuiFontTexture{index}";

      m_guiSystem.AddFontTexture(atlasTexture);
    }

    private void StartLoadingFont(CachedFont font)
    {
      if (font.Status != FontLoadStatus.NotLoaded)
        return;

      // If the the fontset does not have something loaded, either first time load or it's missing and we need to resolve a fallback
      if (!font.FontSet.IsFontDataLoaded)
      {
        // Always get the best match even if the fontset's data is missing so we can get the best possible fallback
        SKFontMatchResult loadResult = m_fontLoader.FindBestMatching(font.Description);
        if (!loadResult.IsFallback)
        {
          if (font.IsEmbeddedFont || font.Description.Style.HasFlag(FontStyle.Embedded))
            m_fontLoader.SetImFontData(font.FontSet, loadResult.Typeface);
          else
            font.FontSet.SetFontData(loadResult.Typeface);
        }
        else
        {
          if (!font.FontSet.IsFontDataMissing)
            font.FontSet.MarkFontDataMissing();

          // Look up the fallback...
          CachedFont fallback = FindCachedFont(loadResult.Description, false);
          font.MarkMissing(fallback);
          return;
        }
      }

      // Start font generation
      font.MarkScheduled();

      Task<FontLoadResult> task = Task<FontLoadResult>.Run(() =>
      {
        try
        {
          return new FontLoadResult() { Font = font, Bitmaps = font.GenerateFontBitmaps() };
        }
        catch (Exception e)
        {
          EngineLog.LogException(LogLevel.Error, e);
        }

        return new FontLoadResult() { Font = font };
      });

      m_pendingLoadTasks.Add(task);
    }

    private void RetryLoadingFonts(CachedFont font)
    {
      if (font.Status == FontLoadStatus.Loaded || font.Status == FontLoadStatus.Scheduled)
        return;

      // Most likely the failure is the font data couldn't be resolved. Not sure if we need to check, but lets be safe
      if (!font.FontSet.IsFontDataLoaded)
      {
        font.FontSet.Reset();

        foreach (CachedFont ff in font.FontSet)
          StartLoadingFont(ff);
      }
      else
      {
        StartLoadingFont(font);
      }
    }

    private struct FontLoadResult
    {
      public CachedFont Font;
      public ImFontBitmaps? Bitmaps;
    }
  }
}
