﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using ImGuiNET;
using SkiaSharp;

namespace Tesla.Gui
{
  public sealed class ImFontsCache : IEnumerable<CachedFontFamily>, IDisposable
  {
    private Dictionary<string, CachedFontFamily> m_fontFamilies;

    public int Count { get { return m_fontFamilies.Count; } }

    public CachedFontFamily? this[string name]
    {
      get
      {
        if (m_fontFamilies.TryGetValue(name, out CachedFontFamily? family))
          return family;

        return null;
      }
    }

    public ImFontsCache()
    {
      m_fontFamilies = new Dictionary<string, CachedFontFamily>();
    }

    public CachedFontFamily GetOrCreate(string name)
    {
      ArgumentNullException.ThrowIfNullOrEmpty(name, nameof(name));

      if (m_fontFamilies.TryGetValue(name, out CachedFontFamily? family))
        return family;

      family = new CachedFontFamily(name);
      m_fontFamilies.Add(name, family);

      return family;
    }

    public CachedFont FindOrCreateFont(GuiFont descr)
    {
      CachedFontFamily family = GetOrCreate(descr.Family);
      return family.FindOrCreateFont(new FontSetKey(descr), descr.Size);
    }

    public bool TryFindFont(GuiFont descr, [NotNullWhen(true)] out CachedFont? font)
    {
      font = null;

      CachedFontFamily? family = this[descr.Family];
      if (family is null)
        return false;

      return family.TryFindFont(new FontSetKey(descr), descr.Size, out font);
    }

    public FontLoadStatus GetFontLoadStatus(GuiFont descr)
    {
      CachedFontFamily? family = this[descr.Family];
      if (family is null)
        return FontLoadStatus.NotLoaded;

      return family.GetFontLoadStatus(new FontSetKey(descr), descr.Size);
    }

    public void GatherGuiFontFamilies(List<GuiFontFamily> loadedFamilies)
    {
      foreach (CachedFontFamily family in this)
      {
        GuiFontFamily? descr = family.AsGuiFontFamily();
        if (descr is not null)
          loadedFamilies.Add(descr);
      }
    }
    public Dictionary<string, CachedFontFamily>.ValueCollection.Enumerator GetEnumerator()
    {
      return m_fontFamilies.Values.GetEnumerator();
    }

    IEnumerator<CachedFontFamily> IEnumerable<CachedFontFamily>.GetEnumerator()
    {
      return m_fontFamilies.Values.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return m_fontFamilies.Values.GetEnumerator();
    }

    public void Dispose()
    {
      foreach (CachedFontFamily family in this)
        family.Dispose();

      m_fontFamilies.Clear();

      GC.SuppressFinalize(this);
    }
  }

  public sealed class CachedFontFamily : IEnumerable<CachedFontStyleSet>, IDisposable
  {
    public readonly string FamilyName;

    private Dictionary<FontSetKey, CachedFontStyleSet> m_fontsByStyle;
    private bool m_isEmbedded;

    public int Count { get { return m_fontsByStyle.Count; } }

    public bool IsEmbeddedFont { get { return m_isEmbedded; } }

    public CachedFontStyleSet? this[FontSetKey styleKey]
    {
      get
      {
        if (m_fontsByStyle.TryGetValue(styleKey, out CachedFontStyleSet? styleSet))
          return styleSet;

        return null;
      }
    }

    internal CachedFontFamily(string familyName)
    {
      FamilyName = familyName;
      m_fontsByStyle = new Dictionary<FontSetKey, CachedFontStyleSet>();
      m_isEmbedded = ImGuiFontManager.EmbeddedDefaultFont.Family.Equals(familyName);
    }

    public CachedFontStyleSet GetOrCreate(FontSetKey styleKey)
    {
      if (m_fontsByStyle.TryGetValue(styleKey, out CachedFontStyleSet? styleSet))
        return styleSet;

      styleSet = new CachedFontStyleSet(this, styleKey);
      m_fontsByStyle.Add(styleKey, styleSet);
      return styleSet;
    }

    public CachedFont FindOrCreateFont(FontSetKey styleKey, int size)
    {
      CachedFontStyleSet styleSet = GetOrCreate(styleKey);
      return styleSet.GetOrCreate(size);
    }

    public bool TryFindFont(FontSetKey styleKey, int size, [NotNullWhen(true)] out CachedFont? font)
    {
      font = null;

      CachedFontStyleSet? styleSet = this[styleKey];
      if (styleSet is null)
        return false;

      font = styleSet[size];
      return font is not null;
    }

    public FontLoadStatus GetFontLoadStatus(FontSetKey styleKey, int size)
    {
      CachedFontStyleSet? styleSet = this[styleKey];
      if (styleSet is null)
        return FontLoadStatus.NotLoaded;

      return styleSet.GetFontLoadStatus(size);
    }

    public GuiFontFamily? AsGuiFontFamily()
    {;
      Dictionary<FontSetKey, IReadOnlyList<int>>? loadedFonts = null;
      foreach (CachedFontStyleSet styleSet in this)
      {
        List<int>? sizes = null;

        foreach (CachedFont font in styleSet)
        {
          if (font.Status != FontLoadStatus.Loaded)
            continue;

          if (sizes is null)
            sizes = new List<int>();

          sizes.Add(font.Description.Size);
        }

        if (sizes is null)
          continue;

        if (loadedFonts is null)
          loadedFonts = new Dictionary<FontSetKey, IReadOnlyList<int>>();

        loadedFonts.Add(styleSet.StyleKey, sizes);;
      }

      return (loadedFonts is null) ? null : new GuiFontFamily(FamilyName, loadedFonts);
    }

    public Dictionary<FontSetKey, CachedFontStyleSet>.ValueCollection.Enumerator GetEnumerator()
    {
      return m_fontsByStyle.Values.GetEnumerator();
    }

    IEnumerator<CachedFontStyleSet> IEnumerable<CachedFontStyleSet>.GetEnumerator()
    {
      return m_fontsByStyle.Values.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return m_fontsByStyle.Values.GetEnumerator();
    }

    public void Dispose()
    {
      foreach (CachedFontStyleSet styleSet in this)
        styleSet.Dispose();

      m_fontsByStyle.Clear();

      GC.SuppressFinalize(this);
    }
  }

  public sealed class CachedFontStyleSet : IEnumerable<CachedFont>, IDisposable
  {
    public readonly CachedFontFamily Family;
    public readonly FontSetKey StyleKey;

    private Dictionary<int, CachedFont> m_fontsBySize;

    private RawBuffer<byte>? m_imData;
    private int m_imFontNumber;
    private SKTypeface? m_typeface;
    private bool m_fontDataMissing;

    public bool IsFontDataMissing { get { return m_fontDataMissing; } }

    public bool IsFontDataLoaded { get { return m_typeface is not null || m_imData is not null; } }

    public bool IsEmbeddedFont { get { return Family.IsEmbeddedFont; } }

    public int Count { get { return m_fontsBySize.Count; } }

    public CachedFont? this[int size]
    {
      get
      {
        if (m_fontsBySize.TryGetValue(size, out CachedFont? font))
          return font;

        return null;
      }
    }

    internal CachedFontStyleSet(CachedFontFamily family, FontSetKey styleKey)
    {
      Family = family;
      StyleKey = styleKey;

      m_fontsBySize = new Dictionary<int, CachedFont>();
    }

    public void MarkFontDataMissing()
    {
      Reset();

      m_fontDataMissing = true;
    }

    public void SetFontData(SKTypeface typeFace)
    {
      if (typeFace is null)
      {
        MarkFontDataMissing();
        return;
      }

      Reset();

      m_typeface = typeFace;
      m_fontDataMissing = false;
    }

    public void SetFontData(RawBuffer<byte> data, int fontNumber = 0)
    {
      if (data is null)
      {
        MarkFontDataMissing();
        return;
      }

      Reset();

      m_imData = data;
      m_imFontNumber = fontNumber;
      m_fontDataMissing = false;
    }

    public void Reset()
    {
      m_typeface = null; // Held onto by the SKFontLoader
      m_imData = null; // Held onto by the SKFontLoader
      m_imFontNumber = 0;
      m_fontDataMissing = false;

      foreach (CachedFont font in this)
        font.Reset();
    }

    public CachedFont GetOrCreate(int size)
    {
      if (m_fontsBySize.TryGetValue(size, out CachedFont? font))
        return font;

      font = new CachedFont(new GuiFont(Family.FamilyName, size, StyleKey.Weight, StyleKey.Style, StyleKey.Width), this);
      m_fontsBySize.Add(size, font);

      return font;
    }

    public FontLoadStatus GetFontLoadStatus(int size)
    {
      CachedFont? font = this[size];
      if (font is null)
        return FontLoadStatus.NotLoaded;

      return font.Status;
    }

    public ImFontBitmaps? GenerateFontBitmaps(int size)
    {
      if (!IsFontDataLoaded)
        return null;

      GuiFont descr = new GuiFont(Family.FamilyName, size, StyleKey.Weight, StyleKey.Style, StyleKey.Width);

      if (m_imData is not null)
        return ImFontBitmaps.Generate(descr, m_imData, m_imFontNumber);

      if (m_typeface is not null)
      {
        using SKFont fontInstance = m_typeface.ToFont(size);
        return ImFontBitmaps.Generate(descr, fontInstance);
      }

      return null;
    }

    public Dictionary<int, CachedFont>.ValueCollection.Enumerator GetEnumerator()
    {
      return m_fontsBySize.Values.GetEnumerator();
    }

    IEnumerator<CachedFont> IEnumerable<CachedFont>.GetEnumerator()
    {
      return m_fontsBySize.Values.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return m_fontsBySize.Values.GetEnumerator();
    }

    public void Dispose()
    {
      Reset();

      m_fontsBySize.Clear();

      GC.SuppressFinalize(this);
    }
  }

  public sealed class CachedFont
  {
    public readonly GuiFont Description;
    public readonly CachedFontStyleSet FontSet;

    private FontLoadStatus m_status;
    private CachedFont? m_fallbackFont;
    private ImFontPtr m_imFont;

    public FontLoadStatus Status { get { return m_status; } }

    public ImFontPtr ImFont
    {
      get
      {
        if (m_fallbackFont is not null)
          return m_fallbackFont.ImFont;

        return m_imFont;
      }
    }

    public bool IsFontReady
    {
      get
      {
        if (m_fallbackFont is not null)
          return m_fallbackFont.Status == FontLoadStatus.Loaded;

        return m_status == FontLoadStatus.Loaded;
      }
    }

    public bool IsEmbeddedFont { get { return FontSet.IsEmbeddedFont; } }

    internal CachedFont(GuiFont descr, CachedFontStyleSet fontSet)
    {
      Description = descr;
      FontSet = fontSet;

      Reset();
    }

    public void Reset()
    {
      m_status = FontLoadStatus.NotLoaded;
      m_imFont = IntPtr.Zero;
      m_fallbackFont = null;
    }

    public void MarkScheduled()
    {
      if (m_status == FontLoadStatus.Loaded)
        return;

      Reset();

      m_status = FontLoadStatus.Scheduled;
    }

    public void MarkLoaded(ImFontPtr font)
    {
      m_status = FontLoadStatus.Loaded;
      m_imFont = font;
      m_fallbackFont = null;

      EngineLog.Log(LogLevel.Info, $"Loaded Font {Description.ToString()}.");
    }

    public void MarkMissing(CachedFont fallback)
    {
      ArgumentNullException.ThrowIfNull(fallback);

      m_status = FontLoadStatus.Missing;
      m_imFont = IntPtr.Zero;
      m_fallbackFont = fallback;

      EngineLog.Log(LogLevel.Warn, $"Failed loading Font {Description.ToString()}, falling back to {fallback.Description.ToString()}.");
    }

    public ImFontBitmaps? GenerateFontBitmaps()
    {
      return FontSet.GenerateFontBitmaps(Description.Size);
    }
  }
}
