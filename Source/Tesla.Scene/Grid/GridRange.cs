﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;
using Tesla.Content;

#nullable enable

namespace Tesla.Scene
{
  /// <summary>
  /// Represents a rectangular (min-max) range of tile IDs in a <see cref="SpatialGrid"/>.
  /// </summary>
  public struct GridRange : IPrimitiveValue, IEquatable<GridRange>, IRefEquatable<GridRange>
  {
    /// <summary>
    /// Min ID of tile.
    /// </summary>
    public Int2 Min;

    /// <summary>
    /// Max ID of tile.
    /// </summary>
    public Int2 Max;

    /// <summary>
    /// Gets if the range covers a single tile.
    /// </summary>
    public readonly bool IsSingleTile
    {
      get
      {
        return Min.X == Max.X && Min.Y == Max.Y;
      }
    }

    /// <summary>
    /// Gets if the range is null, that is both min / max are zero.
    /// </summary>
    public readonly bool IsNull
    {
      get
      {
        return Min == Int2.Zero && Max == Int2.Zero;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="GridRange"/> struct.
    /// </summary>
    /// <param name="min">Minimum tile ID.</param>
    /// <param name="max">Maximum tile ID.</param>
    public GridRange(in Int2 min, in Int2 max)
    {
      Min = min;
      Max = max;
    }

    /// <summary>
    /// Queries if the range contains the specified ID.
    /// </summary>
    /// <param name="tileId">Tile ID.</param>
    /// <returns>True if the tile is covered by the range, false if otherwise.</returns>
    public readonly bool Contains(in Int2 tileId)
    {
      if (tileId.X > Max.X || tileId.X < Min.X || tileId.Y > Max.Y || tileId.Y < Min.Y)
        return false;

      return true;
    }

    /// <summary>
    /// Queries if the range intersects with another range.
    /// </summary>
    /// <param name="other">Other range to test against.</param>
    /// <returns>True if the ranges intersect, false if they are completely disjoint.</returns>
    public readonly bool Intersects(in GridRange other)
    {
      if (Min.X > other.Max.X || other.Min.X > Max.X ||
          Min.Y > other.Max.Y || other.Min.Y > Max.Y)
        return false;

      return true;
    }

    /// <summary>
    /// Queries if the range intersects with another range.
    /// </summary>
    /// <param name="other">Other range to test against.</param>
    /// <param name="intersection">The range representing the area that overlaps between both ranges.</param>
    /// <returns>True if the ranges intersect, false if they are completely disjoint.</returns>
    public readonly bool Intersects(in GridRange other, out GridRange intersection)
    {
      if (Min.X > other.Max.X || other.Min.X > Max.X ||
          Min.Y > other.Max.Y || other.Min.Y > Max.Y)
      {
        intersection = new GridRange();
        return false;
      }

      Int2.Max(other.Min, Min, out intersection.Min);
      Int2.Min(other.Max, Max, out intersection.Max);

      return true;
    }

    /// <summary>
    /// Queries if the range intersects with a row extent.
    /// </summary>
    /// <param name="row">Row extent to test against.</param>
    /// <returns>True if the row intersects with the range.</returns>
    public readonly bool Intersects(in TileRowExtent row)
    {
      if (row.Y < Min.Y || row.Y > Max.Y ||
          Min.X > row.XMax || row.XMin > Max.X)
        return false;

      return true;
    }

    /// <summary>
    /// Merges two ranges together.
    /// </summary>
    /// <param name="x">First range.</param>
    /// <param name="y">Second range.</param>
    /// <param name="result">Union of both ranges.</param>
    public static void Merge(in GridRange x, in GridRange y, out GridRange result)
    {
      Int2.Min(x.Min, y.Min, out result.Min);
      Int2.Max(x.Max, y.Max, out result.Max);
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.</returns>
    public readonly override int GetHashCode()
    {
      unchecked
      {
        return Min.GetHashCode() + Max.GetHashCode();
      }
    }

    /// <summary>
    /// Determines whether the specified <see cref="System.Object" /> is equal to this instance.
    /// </summary>
    /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
    /// <returns>True if the specified <see cref="System.Object" /> is equal to this instance; otherwise, False.</returns>
    public readonly override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is GridRange)
        return Equals((GridRange) obj);

      return false;
    }

    /// <summary>
    /// Checks equality between the range and another range
    /// </summary>
    /// <param name="other">Other range</param>
    /// <returns>True if the ranges are equal, false otherwise.</returns>
    readonly bool IEquatable<GridRange>.Equals(GridRange other)
    {
      return other.Min.Equals(Min) && other.Max.Equals(Max);
    }

    /// <summary>
    /// Checks equality between the range and another range
    /// </summary>
    /// <param name="other">Other range</param>
    /// <returns>True if the ranges are equal, false otherwise.</returns>
    public readonly bool Equals(in GridRange other)
    {
      return other.Min.Equals(Min) && other.Max.Equals(Max);
    }

    /// <summary>
    /// Returns the fully qualified type name of this instance.
    /// </summary>
    /// <returns>A <see cref="T:System.String" /> containing a fully qualified type name.</returns>
    public readonly override String ToString()
    {
      return (IsNull) ? "Invalid" : String.Format("Min: [{0}], Max: [{1}]", Min.ToString(), Max.ToString());
    }

    /// <summary>
    /// Writes the primitive data to the output.
    /// </summary>
    /// <param name="output">Primitive writer.</param>
    public readonly void Write(IPrimitiveWriter output)
    {
      output.Write<Int2>("Min", Min);
      output.Write<Int2>("Max", Max);
    }

    /// <summary>
    /// Reads the primitive data from the input.
    /// </summary>
    /// <param name="input">Primitive reader.</param>
    public void Read(IPrimitiveReader input)
    {
      input.Read<Int2>("Min", out Min);
      input.Read<Int2>("Max", out Max);
    }
  }
}
