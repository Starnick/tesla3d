﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;
using Tesla.Content;

#nullable enable

namespace Tesla.Scene
{
  /// <summary>
  /// Represents a single tile row in a grid, indexed by its Y coordinate and a min-max range along the X axis. 
  /// </summary>
  public struct TileRowExtent : IPrimitiveValue, IEquatable<TileRowExtent>, IRefEquatable<TileRowExtent>
  {
    /// <summary>
    /// Y coordinate of the row.
    /// </summary>
    public int Y;

    /// <summary>
    /// Minimum X coordinate of the row.
    /// </summary>
    public int XMin;

    /// <summary>
    /// Maximum X coordinate of the row.
    /// </summary>
    public int XMax;

    /// <summary>
    /// Constructs a new instance of the <see cref="TileRowExtent"/> struct.
    /// </summary>
    /// <param name="y">Y coordinate of the row.</param>
    /// <param name="xMin">Minimum X coordinate of the row.</param>
    /// <param name="xMax">Maximum X coordinate of the row.</param>
    public TileRowExtent(int y, int xMin, int xMax)
    {
      Y = y;
      XMin = xMin;
      XMax = xMax;
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.</returns>
    public readonly override int GetHashCode()
    {
      unchecked
      {
        return Y + XMin + XMax;
      }
    }

    /// <summary>
    /// Determines whether the specified <see cref="System.Object" /> is equal to this instance.
    /// </summary>
    /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
    /// <returns>True if the specified <see cref="System.Object" /> is equal to this instance; otherwise, False.</returns>
    public readonly override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is TileRowExtent)
        return Equals((TileRowExtent) obj);

      return false;
    }

    /// <summary>
    /// Checks equality between the row extent and another.
    /// </summary>
    /// <param name="other">Other row extent</param>
    /// <returns>True if the row extents are equal, false otherwise.</returns>
    readonly bool IEquatable<TileRowExtent>.Equals(TileRowExtent other)
    {
      return other.Y == Y && other.XMin == XMin && other.XMax == XMax;
    }

    /// <summary>
    /// Checks equality between the row extent and another.
    /// </summary>
    /// <param name="other">Other row extent</param>
    /// <returns>True if the row extents are equal, false otherwise.</returns>
    public readonly bool Equals(in TileRowExtent other)
    {
      return other.Y == Y && other.XMin == XMin && other.XMax == XMax;
    }

    /// <summary>
    /// Returns the fully qualified type name of this instance.
    /// </summary>
    /// <returns>A <see cref="T:System.String" /> containing a fully qualified type name.</returns>
    public readonly override String ToString()
    {
      return String.Format("Y: {0}, XMin: {1}, XMax: {2}", Y.ToString(), XMin.ToString(), XMax.ToString());
    }

    /// <summary>
    /// Writes the primitive data to the output.
    /// </summary>
    /// <param name="output">Primitive writer.</param>
    public readonly void Write(IPrimitiveWriter output)
    {
      output.Write("Y", Y);
      output.Write("XMin", XMin);
      output.Write("XMax", XMax);
    }

    /// <summary>
    /// Reads the primitive data from the input.
    /// </summary>
    /// <param name="input">Primitive reader.</param>
    public void Read(IPrimitiveReader input)
    {
      Y = input.ReadInt32("Y");
      XMin = input.ReadInt32("XMin");
      XMax = input.ReadInt32("XMax");
    }

  }
}
