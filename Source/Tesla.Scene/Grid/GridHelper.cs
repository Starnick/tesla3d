﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;

namespace Tesla.Scene
{
  public static class GridHelper
  {
    public static Int2 CalculateGridId(float tileSize, in Vector3 physicalCoordinates)
    {
      //Real X coordinate maps to grid +Y coordinate, Real Z maps to grid +X coordinate
      int xSign = (physicalCoordinates.X < 0) ? -1 : 1;
      int zSign = (physicalCoordinates.Z < 0) ? -1 : 1;

      float invTileSize = 1.0f / tileSize;
      int x = (int) Math.Floor(Math.Abs(physicalCoordinates.X) * invTileSize);
      int z = (int) Math.Floor(Math.Abs(physicalCoordinates.Z) * invTileSize);

      return new Int2((z + 1) * zSign, (x + 1) * xSign);
    }

    public static void CalculateGridId(float tileSize, in Vector3 physicalCoordinates, out Int2 gridId)
    {
      //Real X coordinate maps to grid +Y coordinate, Real Z maps to grid +X coordinate
      int xSign = (physicalCoordinates.X < 0) ? -1 : 1;
      int zSign = (physicalCoordinates.Z < 0) ? -1 : 1;

      float invTileSize = 1.0f / tileSize;
      int x = (int) Math.Floor(Math.Abs(physicalCoordinates.X) * invTileSize);
      int z = (int) Math.Floor(Math.Abs(physicalCoordinates.Z) * invTileSize);

      gridId = new Int2((z + 1) * zSign, (x + 1) * xSign);
    }

    public static BoundingBox.Data CalculatePhysicalCoordinates(float tileSize, in Int2 gridId)
    {
      //Real X coordinate maps to grid +Y coordinate, Real Z maps to grid +X coordinate
      int xFactor = gridId.X < 0 ? 1 : 0;
      int yFactor = gridId.Y < 0 ? 1 : 0;
      float left = (gridId.X + xFactor) * tileSize;
      float top = (gridId.Y + yFactor) * tileSize;
      float halfTileSize = tileSize * 0.5f;

      Vector3 center = new Vector3(top - halfTileSize, 0.0f, left - halfTileSize);
      BoundingBox.Data aabb = new BoundingBox.Data(center, new Vector3(halfTileSize, 0.0f, halfTileSize));

      return aabb;
    }

    public static void CalculatePhysicalCoordinates(float tileSize, in Int2 gridId, out BoundingBox.Data physicalCoord)
    {
      //Real X coordinate maps to grid +Y coordinate, Real Z maps to grid +X coordinate
      int xFactor = gridId.X < 0 ? 1 : 0;
      int yFactor = gridId.Y < 0 ? 1 : 0;
      float left = (gridId.X + xFactor) * tileSize;
      float top = (gridId.Y + yFactor) * tileSize;
      float halfTileSize = tileSize * 0.5f;

      Vector3 center = new Vector3(top - halfTileSize, 0.0f, left - halfTileSize);
      physicalCoord = new BoundingBox.Data(center, new Vector3(halfTileSize, 0.0f, halfTileSize));
    }

    public static void CalculateRegionBounds(float regionSize, float tileSize, in Int2 minGlobalTileId, in Int2 maxGlobalTileId, out BoundingBox.Data bounds)
    {
      BoundingBox.Data minAABB, maxAABB;
      CalculatePhysicalCoordinates(tileSize, minGlobalTileId, out minAABB);
      CalculatePhysicalCoordinates(tileSize, maxGlobalTileId, out maxAABB);

      Vector3 realMin, realMax;
      realMin = new Vector3(minAABB.Center.X - minAABB.Extents.X, 0.0f, minAABB.Center.Z - minAABB.Extents.Z);
      realMax = new Vector3(maxAABB.Center.X + maxAABB.Extents.X, 0.0f, maxAABB.Center.Z + maxAABB.Extents.Z);

      Vector3 center;
      Vector3.Add(realMin, realMax, out center);
      Vector3.Multiply(center, 0.5f, out center);

      Vector3 extents;
      Vector3.Subtract(realMax, realMin, out extents);
      Vector3.Multiply(extents, 0.5f, out extents);

      bounds.Center = center;
      bounds.Extents = extents;
    }

    public static bool CalculateArrayIndex(int tileExtentInRegion, in Int2 globalTileId, in Int2 regionId, out Int2? arrayIndex)
    {
      arrayIndex = null;

      int x = globalTileId.X;
      int y = globalTileId.Y;

      int xOffset = (x > 0) ? 1 : 0;
      int yOffset = (y > 0) ? 1 : 0;

      x = (x - xOffset) - ((regionId.X - xOffset) * tileExtentInRegion);
      y = (tileExtentInRegion - 1) - ((y - yOffset) - ((regionId.Y - yOffset) * tileExtentInRegion));

      //Zero-based array indices, should be positive but if larger than extent or negative then we are out of range
      if (x >= tileExtentInRegion || y >= tileExtentInRegion || x < 0 || y < 0)
        return false;

      arrayIndex = new Int2(x, y);
      return true;
    }

    public static void CalculateMinMaxTileId(in Int2 regionId, int tileExtentInRegion, out Int2 min, out Int2 max)
    {
      int signX = (regionId.X > 0) ? 1 : -1;
      int signY = (regionId.Y > 0) ? 1 : -1;

      int x1 = (regionId.X * tileExtentInRegion);
      int x2 = ((regionId.X - signX) * tileExtentInRegion) + signX;
      int y1 = (regionId.Y * tileExtentInRegion);
      int y2 = ((regionId.Y - signY) * tileExtentInRegion) + signY;

      int minX, maxX, minY, maxY;
      MathHelper.MinMax(x1, x2, out minX, out maxX);
      MathHelper.MinMax(y1, y2, out minY, out maxY);

      min = new Int2(minX, minY);
      max = new Int2(maxX, maxY);
    }

    public static int ToCantorPair(int x, int y)
    {
      //Original function for non-negative: //((x + y) * (x + y + 1)) / 2 + y;

      int A = (x >= 0) ? 2 * x : -2 * x - 1;
      int B = (y >= 0) ? 2 * y : -2 * y - 1;

      return (A + B) * (A + B + 1) / 2 + A;
    }

    public static int ToSzudzikPair(int x, int y)
    {
      int A = (x >= 0) ? 2 * x : -2 * x - 1;
      int B = (y >= 0) ? 2 * y : -2 * y - 1;

      return (A >= B) ? A * A + A + B : A + B * B;
    }

    public static void GetMinMaxHeight(BoundingVolume volume, out float minHeight, out float maxHeight)
    {
      System.Diagnostics.Debug.Assert(volume != null);

      switch (volume.BoundingType)
      {
        case BoundingType.AxisAlignedBoundingBox:
          {
            Vector3 center = volume.Center;
            float yExtent = (volume as BoundingBox).Extents.Y;
            minHeight = center.Y - yExtent;
            maxHeight = center.Y + yExtent;
          }
          break;
        case BoundingType.Sphere:
          {
            Vector3 center = volume.Center;
            float radius = (volume as BoundingSphere).Radius;
            minHeight = center.Y - radius;
            maxHeight = center.Y + radius;
          }
          break;
        default:
          {
            minHeight = float.MaxValue;
            maxHeight = float.MinValue;

            ReadOnlySpan<Vector3> corners = volume.Corners;
            for (int i = 0; i < corners.Length; i++)
            {
              Vector3 c = corners[i];

              minHeight = Math.Min(minHeight, c.Y);
              maxHeight = Math.Max(maxHeight, c.Y);
            }
          }
          break;
      }
    }

    public static void RecycleGridExtent(ref GridExtent extent)
    {
      TileRowExtentList list = extent.CoveredTiles;

      if (list != null)
        list.Recycle();

      extent = new GridExtent();
    }

    public static void RecycleGridExtents(ref GridExtent regionExtent, ref GridExtent tileExtent)
    {
      TileRowExtentList regionList = regionExtent.CoveredTiles;
      TileRowExtentList tileList = tileExtent.CoveredTiles;

      regionExtent = new GridExtent();
      tileExtent = new GridExtent();

      if (regionList != null && Object.ReferenceEquals(regionList, tileList))
      {
        regionList.Recycle();
        return;
      }

      if (regionList != null)
        regionList.Recycle();

      if (tileList != null)
        tileList.Recycle();
    }
  }
}
