﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using Tesla.Graphics;

namespace Tesla.Scene
{
  public class Region : IEnumerable<Tile>, IPickable
  {
    private Tile[] m_grid;
    private Int2 m_regionId;
    private GridRange m_globalTileRange;
    private BoundingBox m_regionBounds;
    private float m_minHeight;
    private float m_maxHeight;
    private float m_regionSize;
    private float m_tileSize;
    private int m_tileExtent;
    private int m_objectCount;
    private SpatialGrid m_parent;
    private Region m_left, m_top, m_right, m_bottom;
    private List<Tile> m_tilesToBeUpdated;
    private HashSet<Tile> m_dirtyTiles;

    /// <summary>
    /// Gets the tiles the region manages.
    /// </summary>
    public IReadOnlyList<Tile> Tiles
    {
      get
      {
        return m_grid;
      }
    }

    /// <summary>
    /// Gets the region tile ID.
    /// </summary>
    public ref readonly Int2 RegionId
    {
      get
      {
        return ref m_regionId;
      }
    }

    /// <summary>
    /// Gets the rectangle of global tile IDs that the region encompasses.
    /// </summary>
    public ref readonly GridRange GlobalTileRange
    {
      get
      {
        return ref m_globalTileRange;
      }
    }

    /// <summary>
    /// Gets the bounds of the region in world space.
    /// </summary>
    public BoundingBox WorldBounds
    {
      get
      {
        return m_regionBounds;
      }
    }

    /// <summary>
    /// Gets the minimum Y-value of objects in the region.
    /// </summary>
    public float MinHeight
    {
      get
      {
        return m_minHeight;
      }
    }

    /// <summary>F
    /// Gets the maximum Y-value of objects in the region.
    /// </summary>
    public float MaxHeight
    {
      get
      {
        return m_maxHeight;
      }
    }

    /// <summary>
    /// Gets the size of the region in world space.
    /// </summary>
    public float RegionSize
    {
      get
      {
        return m_regionSize;
      }
    }

    /// <summary>
    /// Gets the size of a tile in the region.
    /// </summary>
    public float TileSize
    {
      get
      {
        return m_tileSize;
      }
    }

    /// <summary>
    /// Gets the region extent in terms of number of tiles (region is always square).
    /// </summary>
    public int TileExtent
    {
      get
      {
        return m_tileExtent;
      }
    }

    /// <summary>
    /// Gets the total number of objects contained in the region.
    /// </summary>
    public int ObjectCount
    {
      get
      {
        return m_objectCount;
      }
    }

    /// <summary>
    /// Gets the grid that the region is contained in.
    /// </summary>
    public SpatialGrid Parent
    {
      get
      {
        return m_parent;
      }
    }

    /// <summary>
    /// Gets the absolute world bounding of the object.
    /// </summary>
    BoundingVolume IPickable.WorldBounding
    {
      get
      {
        return m_regionBounds;
      }
    }

    public Region(in Int2 regionId, float regionSize, int tileExtent, SpatialGrid parent)
    {
      if (tileExtent < 1)
        throw new ArgumentOutOfRangeException("tileExtent");

      m_regionId = regionId;
      m_regionSize = regionSize;
      m_tileSize = regionSize / tileExtent;
      m_tileExtent = tileExtent;
      m_parent = parent;
      m_minHeight = 0.0f;
      m_maxHeight = 0.0f;

      m_grid = new Tile[tileExtent * tileExtent];
      GridHelper.CalculateMinMaxTileId(regionId, tileExtent, out m_globalTileRange.Min, out m_globalTileRange.Max);

      BoundingBox.Data regionAABB;
      GridHelper.CalculateRegionBounds(m_regionSize, m_tileSize, m_globalTileRange.Min, m_globalTileRange.Max, out regionAABB);
      m_regionBounds = new BoundingBox(regionAABB);

      m_left = m_right = m_top = m_bottom = null;
      m_tilesToBeUpdated = new List<Tile>();
      m_dirtyTiles = new HashSet<Tile>();
    }

    public TileEntry GetEntry(ISpatial obj, in Int2 globalTileId)
    {
      if (!m_globalTileRange.Contains(globalTileId))
        return null;

      Int2? arrayIndex;
      GetArrayIndex(globalTileId, out arrayIndex);

      if (arrayIndex != null)
      {
        Tile tile = m_grid[arrayIndex.Value.Y * m_tileExtent + arrayIndex.Value.X];

        if (tile != null)
          return tile.GetEntry(obj);
      }

      return null;
    }

    public Tile GetTile(in Int2 globalTileId)
    {
      if (!m_globalTileRange.Contains(globalTileId))
        return null;

      Int2? arrayIndex;
      GetArrayIndex(globalTileId, out arrayIndex);

      if (arrayIndex != null)
        return m_grid[arrayIndex.Value.Y * m_tileExtent + arrayIndex.Value.X];

      return null;
    }

    public bool GetTiles(in GridExtent extent, List<Tile> tiles)
    {
      TileIdEnumerator idEnumerator;
      if (tiles == null || TileIdEnumerator.TryCreate(m_globalTileRange, extent, out idEnumerator))
        return false;

      bool foundOne = false;
      while (idEnumerator.MoveNext())
      {
        Int2 tileId = idEnumerator.Current;
        Int2? arrayIndex;
        GetArrayIndex(tileId, out arrayIndex);

        if (arrayIndex != null)
        {
          Tile tile = m_grid[arrayIndex.Value.Y * m_tileExtent + arrayIndex.Value.X];
          if (tile != null)
          {
            tiles.Add(tile);
            foundOne = true;
          }
        }
      }

      return foundOne;
    }

    public bool FindPicks(PickQuery query)
    {
      System.Diagnostics.Debug.Assert(query != null);

      if (!m_regionBounds.Intersects(query.PickRay))
        return false;

      bool success = false;
      for (int i = 0; i < m_grid.Length; i++)
      {
        Tile tile = m_grid[i];
        if (tile != null)
        {
          //Note - calls the public API that doesn't have a visit guard. If need visit guard, then use the query methods in Grid
          success |= tile.FindPicks(query);
        }
      }

      return success;
    }

    internal bool FindPicks(PickQuery query, ref GridExtent extent)
    {
      System.Diagnostics.Debug.Assert(query != null);
      System.Diagnostics.Debug.Assert(!extent.IsNull);

      TileIdEnumerator idEnumerator;
      if (!m_regionBounds.Intersects(query.PickRay) || !TileIdEnumerator.TryCreate(m_globalTileRange, extent, out idEnumerator))
        return false;

      int visitSequence = m_parent.VisitSequence;
      bool foundOne = false;

      while (idEnumerator.MoveNext())
      {
        Int2 tileId = idEnumerator.Current;
        Int2? arrayIndex;
        GetArrayIndex(tileId, out arrayIndex);

        if (arrayIndex != null)
        {
          int index = arrayIndex.Value.Y * m_tileExtent + arrayIndex.Value.X;

          Tile tile = m_grid[index];

          if (tile != null)
            foundOne |= tile.FindPicks(query, visitSequence);
        }
      }

      return foundOne;
    }

    internal bool Add(TileEntry entry, in GridExtent extent, bool skipDuplicateCheck = false)
    {
      System.Diagnostics.Debug.Assert(entry != null);
      System.Diagnostics.Debug.Assert(entry.Object != null);

      System.Diagnostics.Debug.Assert(!extent.IsNull);

      TileIdEnumerator idEnumerator;
      if (!TileIdEnumerator.TryCreate(m_globalTileRange, extent, out idEnumerator))
        return false;

      bool addedOne = false;
      while (idEnumerator.MoveNext())
      {
        Int2 tileId = idEnumerator.Current;
        Int2? arrayIndex;
        GetArrayIndex(tileId, out arrayIndex);

        if (arrayIndex != null)
        {
          int index = arrayIndex.Value.Y * m_tileExtent + arrayIndex.Value.X;

          Tile tile = m_grid[index];

          if (tile == null)
          {
            tile = new Tile(tileId, arrayIndex.Value, m_tileSize, this);
            m_grid[index] = tile;
          }

          addedOne |= tile.Add(entry, skipDuplicateCheck);
        }
      }

      if (addedOne)
        m_objectCount++;

      return addedOne;
    }

    public Region GetNeighbor(Neighbor regionNeighbor)
    {
      switch (regionNeighbor)
      {
        case Neighbor.Left:
          return m_left;
        case Neighbor.Top:
          return m_top;
        case Neighbor.Right:
          return m_right;
        case Neighbor.Bottom:
          return m_bottom;
        default:
          return null;
      }
    }

    internal void SetNeighbor(Neighbor neighbor, Region region)
    {
      switch (neighbor)
      {
        case Neighbor.Left:
          m_left = region;
          if (m_left != null)
            m_left.m_right = this;
          break;
        case Neighbor.Right:
          m_right = region;
          if (m_right != null)
            m_right.m_left = this;
          break;
        case Neighbor.Top:
          m_top = region;
          if (m_top != null)
            m_top.m_bottom = this;
          break;
        case Neighbor.Bottom:
          m_bottom = region;
          if (m_bottom != null)
            m_bottom.m_top = this;
          break;
      }
    }

    internal void Clear()
    {
      if (m_objectCount == 0)
        return;

      for (int i = 0; i < m_grid.Length; i++)
      {
        Tile tile = m_grid[i];
        if (tile != null)
          tile.Clear();
      }

      m_objectCount = 0;
    }

    internal void DiscoverNeighbors()
    {
      GridRange range = m_parent.RegionRange;
      SetNeighbor(Neighbor.Left, FindNextLeftNeighbor(m_regionId, range.Min.X));
      SetNeighbor(Neighbor.Right, FindNextRightNeighbor(m_regionId, range.Max.X));
      SetNeighbor(Neighbor.Top, FindNextTopNeighbor(m_regionId, range.Max.Y));
      SetNeighbor(Neighbor.Bottom, FindNextBottomNeighbor(m_regionId, range.Min.Y));
    }

    //Should only be called after region has been removed 
    internal void FixupNeighborsWhenRemoved()
    {
      if (m_left != null)
        m_left.m_right = m_right;

      if (m_right != null)
        m_right.m_left = m_left;

      if (m_top != null)
        m_top.m_bottom = m_bottom;

      if (m_bottom != null)
        m_bottom.m_top = m_top;
    }

    private Region FindNextLeftNeighbor(Int2 startId, int minId)
    {
      while (startId.X >= minId)
      {
        startId.X -= 1;
        if (startId.X == 0)
          startId.X -= 1;

        Region west = m_parent.GetRegion(startId);
        if (west != null)
          return west;
      }

      return null;
    }

    private Region FindNextRightNeighbor(Int2 startId, int maxId)
    {
      while (startId.X <= maxId)
      {
        startId.X += 1;
        if (startId.X == 0)
          startId.X += 1;

        Region east = m_parent.GetRegion(startId);
        if (east != null)
          return east;
      }

      return null;
    }

    private Region FindNextTopNeighbor(Int2 startId, int maxId)
    {
      while (startId.Y <= maxId)
      {
        startId.Y += 1;
        if (startId.Y == 0)
          startId.Y += 1;

        Region north = m_parent.GetRegion(startId);
        if (north != null)
          return north;
      }

      return null;
    }

    private Region FindNextBottomNeighbor(Int2 startId, int minId)
    {
      while (startId.Y >= minId)
      {
        startId.Y -= 1;
        if (startId.Y == 0)
          startId.Y -= 1;

        Region south = m_parent.GetRegion(startId);
        if (south != null)
          return south;
      }

      return null;
    }

    private void GetArrayIndex(in Int2 globalTileId, out Int2? arrayIndex)
    {
      if (m_tileExtent == 1)
      {
        arrayIndex = Int2.Zero;
        return;
      }
      else
      {
        GridHelper.CalculateArrayIndex(m_tileExtent, globalTileId, m_regionId, out arrayIndex);
      }
    }

    internal void NotifyObjectRemoved()
    {
      m_objectCount--;
    }

    internal void MarkTileDirty(Tile tile)
    {
      if (!m_dirtyTiles.Contains(tile))
        m_tilesToBeUpdated.Add(tile);
    }

    internal void UpdateBounds()
    {
      if (m_tilesToBeUpdated.Count == 0)
        return;

      for (int i = 0; i < m_tilesToBeUpdated.Count; i++)
      {
        m_tilesToBeUpdated[i].UpdateMinMaxHeight();
      }

      m_tilesToBeUpdated.Clear();
      m_dirtyTiles.Clear();

      UpdateMinMaxHeight();
    }

    internal void UpdateMinMaxHeight()
    {
      Vector3 extents = m_regionBounds.Extents;
      Vector3 center = m_regionBounds.Center;

      if (m_objectCount == 0)
      {
        extents.Y = 0.0f;
        center.Y = 0.0f;
        m_minHeight = 0.0f;
        m_maxHeight = 0.0f;
        m_regionBounds.Extents = extents;
        m_regionBounds.Center = center;
        return;
      }

      m_minHeight = float.MaxValue;
      m_maxHeight = float.MinValue;

      for (int i = 0; i < m_grid.Length; i++)
      {
        Tile tile = m_grid[i];
        if (tile != null)
        {
          float localMinHeight = tile.MinHeight;
          float localMaxHeight = tile.MaxHeight;

          m_minHeight = Math.Min(localMinHeight, m_minHeight);
          m_maxHeight = Math.Max(localMaxHeight, m_maxHeight);
        }
      }

      float yCenter = (m_minHeight + m_maxHeight) * 0.5f;
      float yExtent = Math.Abs(m_maxHeight - m_minHeight) * 0.5f;
      extents.Y = yExtent;
      center.Y = yCenter;

      m_regionBounds.Extents = extents;
      m_regionBounds.Center = center;
    }

    /// <summary>
    /// Returns a string that represents the current object.
    /// </summary>
    /// <returns>A string that represents the current object.</returns>
    public override String ToString()
    {
      return String.Format("{0}, Object Count: {1}", m_regionId.ToString(), m_objectCount.ToString());
    }

    /// <summary>
    /// Returns an enumerator that iterates through the collection.
    /// </summary>
    /// <returns>An enumerator that can be used to iterate through the collection.</returns>
    public TileEnumerator GetEnumerator()
    {
      return new TileEnumerator(m_grid);
    }

    /// <summary>
    /// Returns an enumerator that iterates through the collection.
    /// </summary>
    /// <returns>An enumerator that can be used to iterate through the collection.</returns>
    IEnumerator<Tile> IEnumerable<Tile>.GetEnumerator()
    {
      return new TileEnumerator(m_grid);
    }

    /// <summary>
    /// Returns an enumerator that iterates through a collection.
    /// </summary>
    /// <returns>An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.</returns>
    IEnumerator IEnumerable.GetEnumerator()
    {
      return new TileEnumerator(m_grid);
    }

    /// <summary>
    /// Performs a ray-mesh intersection test.
    /// </summary>
    /// <param name="ray">Ray to test against.</param>
    /// <param name="results">List of results to add to.</param>
    /// <param name="ignoreBackfaces">True if backfaces (relative to the pick ray) should be ignored, false if they should be considered a result.</param>
    /// <returns>True if an intersection occured and results were added to the output list, false if no intersection occured.</returns>
    bool IPickable.IntersectsMesh(in Ray ray, IRefList<Pair<LineIntersectionResult, Triangle?>> results, bool ignoreBackfaces)
    {
      return false;
    }

    #region Tile Enumerator

    public struct TileEnumerator : IEnumerator<Tile>
    {
      private Tile[] m_tiles;
      private Tile m_current;
      private int m_currentIndex;

      public Tile Current
      {
        get
        {
          return m_current;
        }
      }

      object IEnumerator.Current
      {
        get
        {
          return m_current;
        }
      }

      internal TileEnumerator(Tile[] tiles)
      {
        m_tiles = tiles;
        m_current = null;
        m_currentIndex = 0;
      }

      public void Dispose()
      {
      }

      public bool MoveNext()
      {
        while (m_currentIndex < m_tiles.Length)
        {
          m_current = m_tiles[m_currentIndex];
          m_currentIndex++;

          if (m_current != null)
            return true;
        }

        return false;
      }

      public void Reset()
      {
        m_current = null;
        m_currentIndex = 0;
      }
    }

    #endregion
  }
}
