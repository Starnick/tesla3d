﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System.Collections.Generic;
using Tesla.Graphics;
using Tesla.Utilities;

namespace Tesla.Scene
{
  public class TileEntry
  {
    private VisitLock m_visitLock;
    private ISpatial m_spatial;
    private int m_objId;
    private List<Tile> m_coveredTiles;
    private List<Region> m_coveredRegions;
    private bool m_isCoveredTilesRectangle;
    private bool m_isCoveredRegionsRectangle;

    public ISpatial Object
    {
      get
      {
        return m_spatial;
      }
    }

    public int ObjectId
    {
      get
      {
        return m_objId;
      }
    }

    public IReadOnlyList<Tile> CoveredTiles
    {
      get
      {
        return m_coveredTiles;
      }
    }

    public IReadOnlyList<Region> CoveredRegions
    {
      get
      {
        return m_coveredRegions;
      }
    }

    public TileEntry()
    {
      m_visitLock = new VisitLock();
      m_spatial = null;
      m_objId = -1;
      m_coveredTiles = new List<Tile>();
      m_coveredRegions = new List<Region>();
    }

    public T GetAs<T>() where T : class, ISpatial
    {
      return m_spatial as T;
    }

    public bool CanVisit(int visitSequence)
    {
      return m_visitLock.IsFirstVisit(visitSequence);
    }

    internal void Recycle()
    {
      m_spatial = null;
      m_objId = -1;
      m_coveredTiles.Clear();
      m_coveredRegions.Clear();

      Pool<TileEntry>.Return(this);
    }

    internal static TileEntry FromPool(ISpatial obj, int objId, in GridExtent coveredRegions, in GridExtent coveredTiles)
    {
      if (obj == null || coveredRegions.IsNull || coveredTiles.IsNull)
        return null;

      TileEntry entry = Pool<TileEntry>.Fetch();
      entry.m_spatial = obj;
      entry.m_objId = objId;
      entry.m_isCoveredTilesRectangle = coveredTiles.IsRectangle;
      entry.m_isCoveredRegionsRectangle = coveredRegions.IsRectangle;

      return entry;
    }

    internal bool IsInGrid(SpatialGrid grid)
    {
      if (m_coveredTiles.Count == 0)
        return false;

      Tile tile = m_coveredTiles[0];
      if (tile.Parent.Parent == grid)
        return true;

      return false;
    }

    internal void OnAdded(Tile tile)
    {
      m_coveredTiles.Add(tile);
    }

    internal void RemoveFromTile(Tile tile)
    {
      if (tile == null)
        return;

      m_coveredTiles.Remove(tile);
    }

    internal void RemoveFromTiles()
    {
      //Remove object from each tile

      for (int i = 0; i < m_coveredTiles.Count; i++)
      {
        Tile tile = m_coveredTiles[i];
        tile.Remove(m_spatial, true);
      }

      m_coveredTiles.Clear();

      //After removing from tiles, make sure each region has a correct object count
      for (int i = 0; i < m_coveredRegions.Count; i++)
      {
        Region region = m_coveredRegions[i];
        region.NotifyObjectRemoved();
      }

      m_coveredRegions.Clear();
    }

    internal void UpdateCoveredRegions()
    {
      m_coveredRegions.Clear();

      Region prevRegion = null;

      for (int i = 0; i < m_coveredTiles.Count; i++)
      {
        Tile tile = m_coveredTiles[i];

        Region region = tile.Parent;

        if (prevRegion != region)
        {
          m_coveredRegions.Add(region);
          prevRegion = region;
        }
      }
    }

    public void CalculateRegionExtent(out GridExtent regionExtent)
    {
      if (m_coveredRegions.Count == 0)
      {
        regionExtent = new GridExtent();
        return;
      }

      if (m_isCoveredRegionsRectangle)
      {
        Int2 minRegionId = new Int2(int.MaxValue, int.MaxValue);
        Int2 maxRegionId = new Int2(int.MinValue, int.MinValue);

        for (int i = 0; i < m_coveredRegions.Count; i++)
        {
          Int2 regionId = m_coveredRegions[i].RegionId;

          Int2.Min(minRegionId, regionId, out minRegionId);
          Int2.Max(maxRegionId, regionId, out maxRegionId);
        }

        GridExtent.FromMinMax(minRegionId, maxRegionId, out regionExtent);
      }
      else
      {
        TileRowExtentList list = TileRowExtentList.FromPool();

        for (int i = 0; i < m_coveredRegions.Count; i++)
        {
          Int2 regionId = m_coveredRegions[i].RegionId;
          list.AddOrUpdate(regionId.X, regionId.Y);
        }

        regionExtent = new GridExtent(list);
      }
    }

    public void CalculateGlobalExtent(out GridExtent globalTileExtent)
    {
      if (m_coveredTiles.Count == 0)
      {
        globalTileExtent = new GridExtent();
        return;
      }

      if (m_isCoveredTilesRectangle)
      {
        Int2 minTileId = new Int2(int.MaxValue, int.MaxValue);
        Int2 maxTileId = new Int2(int.MinValue, int.MinValue);

        for (int i = 0; i < m_coveredTiles.Count; i++)
        {
          Int2 tileId = m_coveredTiles[i].GlobalTileId;

          Int2.Min(minTileId, tileId, out minTileId);
          Int2.Max(maxTileId, tileId, out maxTileId);
        }

        GridExtent.FromMinMax(minTileId, maxTileId, out globalTileExtent);
      }
      else
      {
        TileRowExtentList list = TileRowExtentList.FromPool();

        for (int i = 0; i < m_coveredTiles.Count; i++)
        {
          Int2 tileId = m_coveredTiles[i].GlobalTileId;
          list.AddOrUpdate(tileId.X, tileId.Y);
        }

        globalTileExtent = new GridExtent(list);
      }
    }

    public void CalculateTileExtents(out GridExtent regionExtent, out GridExtent globalTileExtent)
    {
      bool sameExtent = DoRegionsHaveOneTile();

      CalculateRegionExtent(out regionExtent);

      if (sameExtent)
      {
        globalTileExtent = regionExtent;
        return;
      }

      CalculateGlobalExtent(out globalTileExtent);
    }

    private bool DoRegionsHaveOneTile()
    {
      if (m_coveredRegions.Count > 0)
        return m_coveredRegions[0].TileExtent == 1;

      return false;
    }
  }
}
