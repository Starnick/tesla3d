﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;
using Tesla.Content;

#nullable enable

namespace Tesla.Scene
{
  /// <summary>
  /// Represents an extent in a <see cref="SpatialGrid"/> used to query regions and tiles by an ID. An extent can either be rectangular or non-rectangular. In
  /// rectangular cases, the extent is generally defined by a lightweight min-max range. In non-rectangular cases, the extent is defined by a list of
  /// tile row extents that is sorted from min-to-max Y values, where at every Y there is a min-max X value (similar to a list of scanlines) which can form
  /// irregular shapes with the condition that they are always filled.
  /// </summary>
  public struct GridExtent : IPrimitiveValue, IEquatable<GridExtent>, IRefEquatable<GridExtent>
  {
    private GridRange m_range;
    private bool m_isRectangle;
    private TileRowExtentList? m_coveredTiles;

    /// <summary>
    /// Gets if this extent is really rectangular. In most cases this means it does not reference a tile list, but
    /// there may be a case where the tile list represents a rectangular area too. To avoid allocations this is typically
    /// avoided.
    /// </summary>
    public readonly bool IsRectangle
    {
      get
      {
        if (m_coveredTiles != null)
          return m_coveredTiles.IsRectangle;

        return m_isRectangle;
      }
    }

    /// <summary>
    /// Gets if the extent covers any tiles or not.
    /// </summary>
    public readonly bool IsNull
    {
      get
      {
        if (m_coveredTiles != null)
          return m_coveredTiles.Count == 0;

        return m_range.IsNull;
      }
    }

    /// <summary>
    /// Gets the rectangular range of this extent.
    /// </summary>
    public readonly GridRange CoveredRange
    {
      get
      {
        if (m_coveredTiles != null)
          return m_coveredTiles.Bounds;

        return m_range;
      }
    }

    /// <summary>
    /// Gets the list of tiles covered by this range, if it is a non-rectangular extent.
    /// </summary>
    public readonly TileRowExtentList? CoveredTiles
    {
      get
      {
        return m_coveredTiles;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="GridExtent"/> struct.
    /// </summary>
    /// <param name="coveredRange">The covered grid range.</param>
    public GridExtent(in GridRange coveredRange)
    {
      m_isRectangle = true;
      m_range = coveredRange;
      m_coveredTiles = null;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="GridExtent"/> struct.
    /// </summary>
    /// <param name="coveredTiles">List of tiles that the extent covers.</param>
    /// <exception cref="ArgumentNullException">Thrown if the list is null.</exception>
    public GridExtent(TileRowExtentList coveredTiles)
    {
      if (coveredTiles == null)
        throw new ArgumentNullException("coveredTiles");

      m_isRectangle = false;
      m_range = new GridRange();
      m_coveredTiles = coveredTiles;
    }

    /// <summary>
    /// Queries rectangular range of the extent.
    /// </summary>
    /// <param name="range">Grid range.</param>
    public readonly void GetRange(out GridRange range)
    {
      if (!m_isRectangle && m_coveredTiles != null)
        range = m_coveredTiles.Bounds;

      range = m_range;
    }

    public readonly override int GetHashCode()
    {
      return CoveredRange.GetHashCode();
    }

    public readonly override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is GridExtent)
        return Equals((GridExtent) obj);

      return false;
    }

    readonly bool IEquatable<GridExtent>.Equals(GridExtent other)
    {
      return Equals(other);
    }

    public readonly bool Equals(in GridExtent other)
    {
      if (other.IsRectangle && IsRectangle)
        return other.CoveredRange.Equals(CoveredRange);

      return Object.Equals(m_coveredTiles, other.m_coveredTiles);
    }

    /// <summary>
    /// Creates a grid extent from a list of covered tiles, which is not necessarily rectangular. If the covered tiles do form
    /// a rectangular area, then the created extent will not reference the list (and if the extent owns the list, it recycles it).
    /// </summary>
    /// <param name="coveredTiles">Covered tiles the extent represents.</param>
    /// <param name="extent">The extent of the covered tiles.</param>
    /// <param name="claimOwnership">True if the extent should "own" the list, if false then it will create a copy.</param>
    public static void FromCoveredTiles(TileRowExtentList coveredTiles, out GridExtent extent, bool claimOwnership = true)
    {
      if (coveredTiles == null)
      {
        extent = new GridExtent();
        return;
      }

      if (coveredTiles.IsRectangle)
      {
        extent.m_isRectangle = true;
        extent.m_range = coveredTiles.Bounds;
        extent.m_coveredTiles = null;

        if (claimOwnership)
          coveredTiles.Recycle();
      }
      else
      {
        extent.m_isRectangle = false;
        extent.m_range = new GridRange();
        extent.m_coveredTiles = (claimOwnership) ? coveredTiles : new TileRowExtentList(coveredTiles);
      }
    }

    /// <summary>
    /// Creates a grid extent from a range.
    /// </summary>
    /// <param name="range">ID range in the grid.</param>
    /// <param name="extent">The extent of the range.</param>
    public static void FromRange(in GridRange range, out GridExtent extent)
    {
      extent.m_isRectangle = true;
      extent.m_coveredTiles = null;
      extent.m_range = range;
    }

    /// <summary>
    /// Creates a grid extent from a min-max ID pair.
    /// </summary>
    /// <param name="min">Minimum ID in the range.</param>
    /// <param name="max">Maximum ID in the range.</param>
    /// <param name="extent">The extent of the range.</param>
    public static void FromMinMax(in Int2 min, in Int2 max, out GridExtent extent)
    {
      extent.m_isRectangle = true;
      extent.m_coveredTiles = null;
      extent.m_range.Min = min;
      extent.m_range.Max = max;
    }

    /// <summary>
    /// Writes the primitive data to the output.
    /// </summary>
    /// <param name="output">Primitive writer.</param>
    public readonly void Write(IPrimitiveWriter output)
    {
      bool isRectangle = IsRectangle;
      GridRange range = CoveredRange;

      output.Write("IsRectangle", isRectangle);
      output.Write<GridRange>("CoveredRange", range);

      //If say we're in a situation where the covered tiles are not null but still a rectangle, then we already wrote out the needed data and can actually
      //skip writing out the individual tile extent rows
      if (!isRectangle && m_coveredTiles != null)
        m_coveredTiles.WriteContents("CoveredTiles", output);
    }

    /// <summary>
    /// Reads the primitive data from the input.
    /// </summary>
    /// <param name="input">Primitive reader.</param>
    public void Read(IPrimitiveReader input)
    {
      m_isRectangle = input.ReadBoolean("IsRectangle");
      input.Read<GridRange>("CoveredRange", out m_range);
      m_coveredTiles = null;

      if (!m_isRectangle)
      {
        m_coveredTiles = TileRowExtentList.FromPool();
        m_coveredTiles.ReadContents("CoveredTiles", false, input);
      }
    }
  }
}
