﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using Tesla.Graphics;
using Tesla.Utilities;

namespace Tesla.Scene
{
  public class GridQuery
  {
    private const float BORDER_TOLERANCE = .01f;

    private RefList<Vector3> m_projectedPoints;
    private RefList<Vector3> m_clipPolygon;
    private RefList<Vector3> m_clippedProjectedPoints;
    private RefList<Vector3> m_tempList;
    private TileRowExtentList m_scanLines;
    private DataBuffer<Vector3> m_tempCorners;

    /// <summary>
    /// Gets the last queried (potentially clipped) polygon points used to compute grid extents by the query. If the query did not
    ///  use a polygon (e.g. BoundingBoxes/Spheres produce well-formed rectangular extents), then this might not have points. 
    ///  Useful primarily for debugging purposes, since this is only cleared on the next successful query.
    /// </summary>
    public IReadOnlyCollection<Vector3> LastQueriedPolygon
    {
      get
      {
        return m_projectedPoints;
      }
    }

    public GridQuery()
    {
      m_projectedPoints = new RefList<Vector3>();
      m_clipPolygon = new RefList<Vector3>(4);
      m_clippedProjectedPoints = new RefList<Vector3>();
      m_tempList = new RefList<Vector3>();
      m_scanLines = new TileRowExtentList();
      m_tempCorners = DataBuffer.Create<Vector3>(8, MemoryAllocatorStrategy.Managed);
    }

    public bool GetSegmentRegionExtent(SpatialGrid grid, in Segment segment, out GridExtent regionExtent)
    {
      GridExtent globalTileExtent;
      return GetSegmentExtents(grid, segment, true, false, out regionExtent, out globalTileExtent);
    }

    public bool GetSegmentGlobalExtent(SpatialGrid grid, in Segment segment, out GridExtent globalTileExtent)
    {
      GridExtent regionExtent;
      return GetSegmentExtents(grid, segment, false, true, out regionExtent, out globalTileExtent);
    }

    public bool GetSegmentRegionAndGlobalExtent(SpatialGrid grid, in Segment segment, out GridExtent regionExtent, out GridExtent globalTileExtent)
    {
      return GetSegmentExtents(grid, segment, true, true, out regionExtent, out globalTileExtent);
    }

    public bool GetRayRegionExtent(SpatialGrid grid, in Ray ray, out GridExtent regionExtent)
    {
      GridExtent globalTileExtent;
      return GetRayExtents(grid, ray, true, false, out regionExtent, out globalTileExtent);
    }

    public bool GetRayGlobalExtent(SpatialGrid grid, in Ray ray, out GridExtent globalTileExtent)
    {
      GridExtent regionExtent;
      return GetRayExtents(grid, ray, false, true, out regionExtent, out globalTileExtent);
    }

    public bool GetRayRegionAndGlobalExtent(SpatialGrid grid, in Ray ray, out GridExtent regionExtent, out GridExtent globalTileExtent)
    {
      return GetRayExtents(grid, ray, true, true, out regionExtent, out globalTileExtent);
    }

    public bool GetRegionExtent(SpatialGrid grid, BoundingVolume volume, out GridExtent regionExtent, bool clipToGrid = false)
    {
      GridExtent globalTileExtent;
      return GetExtents(grid, volume, clipToGrid, true, false, out regionExtent, out globalTileExtent);
    }

    public bool GetGlobalExtent(SpatialGrid grid, BoundingVolume volume, out GridExtent globalTileExtent, bool clipToGrid = false)
    {
      GridExtent regionExtent;
      return GetExtents(grid, volume, clipToGrid, false, true, out regionExtent, out globalTileExtent);
    }

    public bool GetRegionAndGlobalExtent(SpatialGrid grid, BoundingVolume volume, out GridExtent regionExtent, out GridExtent globalTileExtent, bool clipToGrid = false)
    {
      return GetExtents(grid, volume, clipToGrid, true, true, out regionExtent, out globalTileExtent);
    }

    public bool GetRegionExtentInView(SpatialGrid grid, BoundingFrustum frustum, out GridExtent regionExtent)
    {
      GridExtent globalTileExtent;
      return GetExtents(grid, frustum, true, true, false, out regionExtent, out globalTileExtent);
    }

    private bool GetRayExtents(SpatialGrid grid, in Ray ray, bool getRegions, bool getGlobalTiles, out GridExtent regionExtent, out GridExtent globalTileExtent)
    {
      regionExtent = new GridExtent();
      globalTileExtent = new GridExtent();

      if (grid == null || !grid.WorldBounds.Intersects(ray))
        return false;

      Vector3 min = grid.WorldBounds.Min;
      Vector3 max = grid.WorldBounds.Max;

      //We want to use either the top or bottom planes that constrain the world in the grid, find the maximum point of intersection with that and the ray, and clamp that segment
      //to be inside the volume of the world
      Plane gridPlane = new Plane(Vector3.Up, -grid.MinHeight);

      //Dot product between grid plane normal and ray dir
      float dot = Vector3.Dot(gridPlane.Normal, ray.Direction);

      //Looking up, so use the top plane of the grid
      if (dot > 0.0f)
        gridPlane = new Plane(Vector3.Up, -grid.MaxHeight);

      //Figure out a segment from the ray...start point is Y component stripped out of ray origin
      Segment clippedRay;

      //See if the ray intersects with the grid plane, if it does we have our end point - make sure it's clipped to the grid range
      //otherwise no intersection, so we need to project the ray onto the plane and figure out the intersection
      LineIntersectionResult result;
      if (gridPlane.Intersects(ray, out result))
      {
        clippedRay.StartPoint = new Vector3(ray.Origin.X, 0.0f, ray.Origin.Z);
        ClipPoint(ref clippedRay.StartPoint, min, max);

        clippedRay.EndPoint = result.Point;
        ClipPoint(ref clippedRay.EndPoint, min, max);
      }
      else
      {
        //Fallback if the ray-plane intersection fails (should almost never happen). This would mean the ray and the two planes are perfectly parallel...so find an intersection
        //on the grid bounding volume and strip out the y components of the resulting segment

        BoundingIntersectionResult boundResult;
        if (grid.WorldBounds.Intersects(ray, out boundResult))
        {
          //If ray is outside and intersects, there will be two intersections, this will be the segment
          if (boundResult.IntersectionCount > 1)
          {
            clippedRay.StartPoint = boundResult.ClosestIntersection.Value.Point;
            clippedRay.EndPoint = boundResult.FarthestIntersection.Value.Point;
          }
          else
          {
            //Else it is inside and there will be one intersection, so the start point is the 2D ray origin
            clippedRay.StartPoint = ray.Origin;
            clippedRay.EndPoint = boundResult.ClosestIntersection.Value.Point;
          }

          clippedRay.StartPoint.Y = 0.0f;
          clippedRay.EndPoint.Y = 0.0f;
        }
        else
        {
          //Ray completely missed the grid...should never reach this, since we already filter out rays that totally miss the grid world bounds
          return false;
        }
      }

      bool success = false;

      //Rasterize the line to get the covered regions/tiles
      if (getRegions)
      {
        RasterizeSegment(clippedRay, grid.RegionSize);

        success |= GetCurrentExtent(out regionExtent);
        if (grid.RegionTileExtent == 1)
        {
          getGlobalTiles = false;
          globalTileExtent = regionExtent;
        }
      }

      if (getGlobalTiles)
      {
        RasterizeSegment(clippedRay, grid.TileSize);

        success |= GetCurrentExtent(out globalTileExtent);
      }

      return success;
    }

    private bool GetSegmentExtents(SpatialGrid grid, in Segment segment, bool getRegions, bool getGlobalTiles, out GridExtent regionExtent, out GridExtent globalTileExtent)
    {
      regionExtent = new GridExtent();
      globalTileExtent = new GridExtent();

      if (grid == null || !grid.WorldBounds.Intersects(segment))
        return false;

      //Strip out Y component, should be the projected segment on the grid plane (always centered at zero, normal +Y)
      Segment seg2D;
      seg2D.StartPoint = new Vector3(segment.StartPoint.X, 0.0f, segment.StartPoint.Z);
      seg2D.EndPoint = new Vector3(segment.EndPoint.X, 0.0f, segment.EndPoint.Z);

      //Clip the 2D segment against the X,Z dimensions of the grid bounds
      Vector3 min = grid.WorldBounds.Min;
      Vector3 max = grid.WorldBounds.Max;

      ClipPoint(ref seg2D.StartPoint, min, max);
      ClipPoint(ref seg2D.EndPoint, min, max);

      bool success = false;

      //Rasterize the line to get the covered regions/tiles
      if (getRegions)
      {
        RasterizeSegment(seg2D, grid.RegionSize);

        success |= GetCurrentExtent(out regionExtent);
        if (grid.RegionTileExtent == 1)
        {
          getGlobalTiles = false;
          globalTileExtent = regionExtent;
        }
      }

      if (getGlobalTiles)
      {
        RasterizeSegment(seg2D, grid.TileSize);

        success |= GetCurrentExtent(out globalTileExtent);
      }

      return success;
    }

    private bool GetExtents(SpatialGrid grid, BoundingVolume volume, bool clipToGrid, bool getRegions, bool getGlobalTiles, out GridExtent regionExtent, out GridExtent globalTileExtent)
    {
      regionExtent = new GridExtent();
      globalTileExtent = new GridExtent();

      if (grid == null || volume == null)
        return false;

      switch (volume.BoundingType)
      {
        case BoundingType.AxisAlignedBoundingBox:
          {
            //Flatten to XZ plane
            BoundingBox.Data aabb = new BoundingBox.Data(volume as BoundingBox);
            aabb.Center.Y = 0.0f;
            aabb.Extents.Y = 0.0f;
            return GetTilesUnderBox(grid, aabb, clipToGrid, getRegions, getGlobalTiles, out regionExtent, out globalTileExtent);
          }
        case BoundingType.Sphere:
          {
            //Get as AABB, flatten to XZ plane, not using bresenham circle algorithm since tiles can be missed. Better to overshoot than to undershoot
            BoundingSphere sphere = volume as BoundingSphere;
            BoundingBox.Data aabb = new BoundingBox.Data(sphere.Center, new Vector3(sphere.Radius, 0.0f, sphere.Radius));
            aabb.Center.Y = 0.0f;
            return GetTilesUnderBox(grid, aabb, clipToGrid, getRegions, getGlobalTiles, out regionExtent, out globalTileExtent);
          }
        case BoundingType.Frustum:
          {
            if (!GetProjectedFrustumCorners(grid, volume as BoundingFrustum, m_tempCorners))
              return false;

            GetProjectedBoundingVolume(m_tempCorners);
            break;
          }
        default:
          GetProjectedBoundingVolume(volume.Corners);
          break;
      }

      //Clip the resulting polygon to the grid, the polygon may actually be completely outside. If so, then we don't have a result. Normally
      //we only want to clip to query what is there, and don't clip when we want to add
      if (clipToGrid && !ClipPolygon(grid))
        return false;

      bool success = false;

      if (getRegions)
      {
        RasterizePolygon(grid.RegionSize);

        success |= GetCurrentExtent(out regionExtent);
        if (grid.RegionTileExtent == 1)
        {
          getGlobalTiles = false;
          globalTileExtent = regionExtent;
        }
      }

      if (getGlobalTiles)
      {
        RasterizePolygon(grid.TileSize);

        success |= GetCurrentExtent(out globalTileExtent);
      }

      return success;
    }

    private bool GetCurrentExtent(out GridExtent extent)
    {
      if (m_scanLines.Count == 0)
      {
        extent = new GridExtent();
        return false;
      }

      //If scanlines are a perfect rectangle, then just use the bounds
      if (m_scanLines.Count == 1 || m_scanLines.IsRectangle)
      {
        extent = new GridExtent(m_scanLines.Bounds);
        return true;
      }

      TileRowExtentList list = TileRowExtentList.FromPool();
      m_scanLines.CopyTo(list);
      extent = new GridExtent(list);

      return true;
    }

    private bool GetTilesUnderBox(SpatialGrid grid, in BoundingBox.Data box, bool clipToGrid, bool getRegions, bool getGlobalTiles, out GridExtent regionExtent, out GridExtent globalTileExtent)
    {
      regionExtent = new GridExtent();
      globalTileExtent = new GridExtent();

      Vector3 max = new Vector3(box.Center.X + box.Extents.X, 0.0f, box.Center.Z + box.Extents.Z);
      Vector3 min = new Vector3(box.Center.X - box.Extents.X, 0.0f, box.Center.Z - box.Extents.Z);

      float regionSize = grid.RegionSize;
      float tileSize = grid.TileSize;

      //Clip the min-max to the grid if requested
      if (clipToGrid)
      {
        //If not inside grid, then we dont have any tiles...
        BoundingBox.Data gridBounds = new BoundingBox.Data(grid.WorldBounds);

        //Assuming Y's set to zero in box we get
        if (!box.Intersects(gridBounds))
          return false;

        Vector3 gridMax = new Vector3(gridBounds.Center.X + gridBounds.Extents.X, 0.0f, gridBounds.Center.Z + gridBounds.Extents.Z);
        Vector3 gridMin = new Vector3(gridBounds.Center.X - gridBounds.Extents.X, 0.0f, gridBounds.Center.Z - gridBounds.Extents.Z);

        //Constrain min-max to inside the spatial grid world bounds
        Vector3.Max(min, gridMin, out min);
        Vector3.Min(max, gridMax, out max);
      }

      //Apply a fudge factor to make sure if we have an object straddling a tile border, we keep it "inside"
      min.X += BORDER_TOLERANCE;
      min.Z += BORDER_TOLERANCE;
      max.X -= BORDER_TOLERANCE;
      max.Z -= BORDER_TOLERANCE;

      if (getRegions)
      {
        Int2 maxRegionTile, minRegionTile;
        GridHelper.CalculateGridId(regionSize, max, out maxRegionTile);
        GridHelper.CalculateGridId(regionSize, min, out minRegionTile);

        GridExtent.FromMinMax(minRegionTile, maxRegionTile, out regionExtent);

        if (grid.RegionTileExtent == 1)
        {
          globalTileExtent = regionExtent;
          getGlobalTiles = false;
        }
      }

      if (getGlobalTiles)
      {
        Int2 maxTile, minTile;
        GridHelper.CalculateGridId(tileSize, max, out maxTile);
        GridHelper.CalculateGridId(tileSize, min, out minTile);

        GridExtent.FromMinMax(minTile, maxTile, out globalTileExtent);
      }

      return true;
    }

    private void ClipPoint(ref Vector3 pt, in Vector3 min, in Vector3 max)
    {
      if (pt.X <= min.X)
        pt.X = min.X + BORDER_TOLERANCE;
      else if (pt.X >= max.X)
        pt.X = max.X - BORDER_TOLERANCE;

      if (pt.Z <= min.Z)
        pt.Z = min.Z + BORDER_TOLERANCE;
      else if (pt.Z >= max.Z)
        pt.Z = max.Z - BORDER_TOLERANCE;
    }

    private bool ClipPolygon(SpatialGrid grid)
    {
      if (m_projectedPoints.Count == 0)
        return false;

      BoundingBox gridBounds = grid.WorldBounds;

      Vector3 max = new Vector3(gridBounds.Center.X + gridBounds.Extents.X, 0.0f, gridBounds.Center.Z + gridBounds.Extents.Z);
      Vector3 min = new Vector3(gridBounds.Center.X - gridBounds.Extents.X, 0.0f, gridBounds.Center.Z - gridBounds.Extents.Z);

      //Apply a fudge factor to make sure if we have an object straddling a tile border, we keep it "inside"
      min.X += BORDER_TOLERANCE;
      min.Z += BORDER_TOLERANCE;
      max.X -= BORDER_TOLERANCE;
      max.Z -= BORDER_TOLERANCE;

      m_clipPolygon.Clear();
      m_clipPolygon.Add(new Vector3(min.X, 0.0f, min.Z));
      m_clipPolygon.Add(new Vector3(max.X, 0.0f, min.Z));
      m_clipPolygon.Add(new Vector3(max.X, 0.0f, max.Z));
      m_clipPolygon.Add(new Vector3(min.X, 0.0f, max.Z));

      if (PolygonHelper.Clip2DPolygonXZ(m_projectedPoints, m_clipPolygon, m_clippedProjectedPoints, m_tempList))
      {
        m_projectedPoints.Clear();
        RefList<Vector3> temp = m_projectedPoints;
        m_projectedPoints = m_clippedProjectedPoints;
        m_clippedProjectedPoints = temp;

        return true;
      }

      return false;
    }

    private void RasterizePolygon(float tileSize)
    {
      m_scanLines.Clear();

      //Assumed to be a closed shape
      for (int i = 0; i < m_projectedPoints.Count; i++)
      {
        int nextIndex = (i == m_projectedPoints.Count - 1) ? 0 : i + 1;

        ref readonly Vector3 start = ref m_projectedPoints[i];
        ref readonly Vector3 end = ref m_projectedPoints[nextIndex];

        RasterizeLine(tileSize, start, end);
      }
    }

    private void RasterizeSegment(in Segment segment, float tileSize)
    {
      m_scanLines.Clear();

      RasterizeLine(tileSize, segment.StartPoint, segment.EndPoint);
    }

    //Based on "faster voxel traversal algorithm for raytracing"
    private void RasterizeLine(float tileSize, in Vector3 start, in Vector3 end)
    {
      if (start.Equals(end))
      {
        Int2 id;
        GridHelper.CalculateGridId(tileSize, start, out id);

        TileRowExtent row;
        row.Y = id.Y;
        row.XMax = id.X;
        row.XMin = id.X;

        m_scanLines.AddOrUpdate(row);
        return;
      }

      float deltaX = end.X - start.X;
      float deltaY = end.Z - start.Z;
      float dist = Math.Abs(deltaX) + Math.Abs(deltaY);

      float dx = deltaX / dist;
      float dy = deltaY / dist;
      int maxIterations = (int) Math.Ceiling(dist);

      for (int i = 0; i < maxIterations; i++)
      {
        Vector3 xyz = new Vector3(start.X + dx * i, 0, start.Z + dy * i);

        Int2 id;
        GridHelper.CalculateGridId(tileSize, xyz, out id);

        TileRowExtent row;
        row.Y = id.Y;
        row.XMax = id.X;
        row.XMin = id.X;

        m_scanLines.AddOrUpdate(row);
      }
    }

    // Based on the C implementation from: https://en.wikibooks.org/wiki/Algorithm_Implementation/Geometry/Convex_hull/Monotone_chain
    private void GetProjectedBoundingVolume(ReadOnlySpan<Vector3> corners)
    {
      int n = corners.Length;
      int k = 0; //Number of points in convex hull

      //Find the 2D convex hull of the corners pt (ignore Y), always assume points are 8 (corners of the volume)
      Span<Vector2> pts = stackalloc Vector2[n];
      Span<Vector2> D = stackalloc Vector2[2 * n]; //convex hull set

      //Copy, rather than a straight memcpy iterate and drop the Y coordinate to save space on stack
      for (int i = 0; i < corners.Length; i++)
      {
        ref readonly Vector3 p = ref corners[i];
        pts[i] = new Vector2(p.X, p.Z);
      }

      //Sort with increasing X, in case of tie by increasing Y values.
      for (int i = 0; i < corners.Length; i++)
      {
        for (int j = i; j > 0 && ComparePoint(pts[j - 1], pts[j]) > 0; j--)
          BufferHelper.Swap<Vector2>(ref pts[j], ref pts[j - 1]);
      }

      //Build lower hull
      for (int i = 0; i < n; i++)
      {
        while (k >= 2 && IsLeft(D[k - 2], D[k - 1], pts[i]) <= 0)
          k--;

        D[k++] = pts[i];
      }

      //Build upper hull
      for (int i = n - 2, t = k + 1; i >= 0; i--)
      {
        while (k >= t && IsLeft(D[k - 2], D[k - 1], pts[i]) <= 0)
          k--;

        D[k++] = pts[i];
      }

      //Add convex hull points to projected point list (remember Y == Z)
      m_projectedPoints.Clear();

      for (int i = 0; i < k; i++)
      {
        ref readonly Vector2 p = ref D[i];

        m_projectedPoints.Add(new Vector3(p.X, 0, p.Y));
      }
    }

    private static int ComparePoint(in Vector2 a, in Vector2 b)
    {
      return a.X == b.X ? a.Y.CompareTo(b.Y) : (a.X > b.X ? 1 : -1);
    }

    private float IsLeft(in Vector2 p0, in Vector2 p1, in Vector2 p2)
    {
      return (p1.X - p0.X) * (p2.Y - p0.Y) - (p2.X - p0.X) * (p1.Y - p0.Y);
    }

    private bool GetProjectedFrustumCorners(SpatialGrid grid, BoundingFrustum frustum, DataBuffer<Vector3> projectedCorners)
    {
      Plane plane = new Plane(Vector3.Up, -grid.MinHeight);

      Ray viewDir;
      frustum.GetPlaneOriginOnFrustum(FrustumPlane.Near, out viewDir.Origin, out viewDir.Direction);
      bool lookingDown = false;

      //Test if we're looking down/up towards te grid, if we are then we may want to do ray-plane intersections to see
      //where the far plane should be projected (otherwise, just strip all frustum corners of Y and take the convex hull of that)

      //Dot product between grid plane normal and the view dir
      float dot = Vector3.Dot(plane.Normal, viewDir.Direction);

      //Looking down, and intersects so we're above/in the grid
      if (dot < 0.0f)
      {
        lookingDown = true;
      }
      //Looking up, and intersects so we're below/in the grid
      else if (dot > 0.0f)
      {
        lookingDown = false;

        //Use the top plane of the grid
        plane = new Plane(Vector3.Up, -grid.MaxHeight);
      }

      ReadOnlySpan<Vector3> corners = frustum.Corners;
      if (projectedCorners.Length != corners.Length)
        projectedCorners.Resize(corners.Length);

      //f/n (far/near), t/b (top/bottom), l/r (left/right)
      int n0, n1, n2, n3, f0, f1, f2, f3;

      //If reverse depth, the near/far is reversed...probably can do a much better job encoding this in the bounding frustum
      if (DepthStencilState.IsReverseDepth)
      {
        n0 = 4; n1 = 5; n2 = 6; n3 = 7; f0 = 0; f1 = 1; f2 = 2; f3 = 3;
      }
      else
      {
        n0 = 0; n1 = 1; n2 = 2; n3 = 3; f0 = 4; f1 = 5; f2 = 6; f3 = 7;
      }

      ref readonly Vector3 nBL = ref corners[n0];
      ref readonly Vector3 nTL = ref corners[n1];
      ref readonly Vector3 nTR = ref corners[n2];
      ref readonly Vector3 nBR = ref corners[n3];

      ref readonly Vector3 fBR = ref corners[f0];
      ref readonly Vector3 fTR = ref corners[f1];
      ref readonly Vector3 fTL = ref corners[f2];
      ref readonly Vector3 fBL = ref corners[f3];

      projectedCorners[0] = new Vector3(nBL.X, 0.0f, nBL.Z); //near, bottom left
      projectedCorners[1] = new Vector3(nTL.X, 0.0f, nTL.Z); //near, top left
      projectedCorners[2] = new Vector3(nTR.X, 0.0f, nTR.Z); //near, top right
      projectedCorners[3] = new Vector3(nBR.X, 0.0f, nBR.Z); //near, bottom right

      GetProjectedFrustumPoint(nBR, fBR, fTR, !lookingDown, plane, out projectedCorners[4]);
      GetProjectedFrustumPoint(nTR, fTR, fBR, lookingDown, plane, out projectedCorners[5]);
      GetProjectedFrustumPoint(nTL, fTL, fBL, lookingDown, plane, out projectedCorners[6]);
      GetProjectedFrustumPoint(nBL, fBL, fTL, !lookingDown, plane, out projectedCorners[7]);

      return true;
    }

    private void GetProjectedFrustumPoint(in Vector3 nearPt, in Vector3 farPt, in Vector3 farSecondaryPt, bool lookingDown, in Plane plane, out Vector3 pt)
    {
      Ray nearToFar;
      Ray.FromOriginTarget(nearPt, farPt, out nearToFar);

      LineIntersectionResult result;
      if (plane.Intersects(nearToFar, out result))
      {
        pt = result.Point;
      }
      else
      {
        Ray farToOtherFar;

        if (lookingDown)
        {
          Ray.FromOriginTarget(farPt, farSecondaryPt, out farToOtherFar);
        }
        else
        {
          Ray.FromOriginTarget(farSecondaryPt, farPt, out farToOtherFar);
        }

        if (plane.Intersects(farToOtherFar, out result))
        {
          pt = result.Point;
        }
        else
        {
          pt = farPt;
        }
      }

      //Strip out Y from whatever the result is
      pt.Y = 0.0f;
    }

    public void Recycle()
    {
      Pool<GridQuery>.Return(this);
    }

    public static GridQuery FromPool()
    {
      return Pool<GridQuery>.Fetch();
    }
  }
}
