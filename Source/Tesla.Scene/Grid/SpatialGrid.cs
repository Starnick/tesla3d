﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tesla.Graphics;
using Tesla.Utilities;

namespace Tesla.Scene
{
  public class SpatialGrid : ISpatial, IPickable, IEnumerable<Region>
  {
    private const int REGION_COUNT_FOR_PARALLEL = 2;
    private ExtendedPropertiesCollection m_extendedProperties;
    private Dictionary<int, Region> m_regions;
    private IEqualityComparer m_spatialComparer;
    private float m_regionSize;
    private float m_tileSize;
    private int m_regionTileExtent;
    private BoundingBox m_worldBounds;
    private GridRange m_regionRange;
    private GridRange m_globalTileRange;
    private int m_objectCount;
    private int m_visitSequence;
    private int m_currObjId;
    private bool m_boundsNeedUpdate;
    private float m_minHeight;
    private float m_maxHeight;

    public float RegionSize
    {
      get
      {
        return m_regionSize;
      }
    }

    public float TileSize
    {
      get
      {
        return m_tileSize;
      }
    }

    public int RegionTileExtent
    {
      get
      {
        return m_regionTileExtent;
      }
    }

    public BoundingBox WorldBounds
    {
      get
      {
        return m_worldBounds;
      }
    }

    BoundingVolume ISpatial.WorldBounding
    {
      get
      {
        return m_worldBounds;
      }
    }

    public ExtendedPropertiesCollection ExtendedProperties
    {
      get
      {
        return m_extendedProperties;
      }
    }

    /// <summary>
    /// Gets the minimum Y-value of objects in the spatial grid.
    /// </summary>
    public float MinHeight
    {
      get
      {
        return m_minHeight;
      }
    }

    /// <summary>
    /// Gets the maximum Y-value of objects in the spatial grid.
    /// </summary>
    public float MaxHeight
    {
      get
      {
        return m_maxHeight;
      }
    }

    public ref readonly GridRange RegionRange
    {
      get
      {
        return ref m_regionRange;
      }
    }

    public ref readonly GridRange GlobalTileRange
    {
      get
      {
        return ref m_globalTileRange;
      }
    }

    public int RegionCount
    {
      get
      {
        return m_regions.Count;
      }
    }

    public int ObjectCount
    {
      get
      {
        return m_objectCount;
      }
    }

    public IEqualityComparer Comparer
    {
      get
      {
        return m_spatialComparer;
      }
    }

    internal int VisitSequence
    {
      get
      {
        return m_visitSequence;
      }
    }

    BoundingVolume IPickable.WorldBounding
    {
      get
      {
        return m_worldBounds;
      }
    }

    public SpatialGrid(float regionSize, int regionTileExtent) : this(regionSize, regionTileExtent, null) { }

    public SpatialGrid(float regionSize, int regionTileExtent, IEqualityComparer comparer)
    {
      m_extendedProperties = new ExtendedPropertiesCollection();
      m_regions = new Dictionary<int, Region>();
      m_spatialComparer = comparer ?? EqualityComparer<ISpatial>.Default;
      m_tileSize = regionSize / regionTileExtent;
      m_regionSize = regionSize;
      m_regionTileExtent = regionTileExtent;
      m_boundsNeedUpdate = false;
      m_minHeight = 0.0f;
      m_maxHeight = 0.0f;
      m_currObjId = 0;

      m_regionRange = new GridRange();
      m_globalTileRange = new GridRange();
      m_worldBounds = new BoundingBox();
    }

    public TileEntry GetEntry(ISpatial obj)
    {
      if (obj == null || obj.WorldBounding == null)
        return null;

      Vector3 center = obj.WorldBounding.Center;
      Int2 regionId;
      GridHelper.CalculateGridId(m_regionSize, center, out regionId);

      Region region = GetRegion(regionId);
      if (region != null)
      {
        Int2 tileId;
        GridHelper.CalculateGridId(m_tileSize, center, out tileId);

        return region.GetEntry(obj, tileId);
      }

      return null;
    }

    public TileEntry Add(ISpatial obj, GridQuery query = null)
    {
      if (obj == null || obj.WorldBounding == null)
        return null;

      bool queryPooled = false;
      if (query == null)
      {
        queryPooled = true;
        query = GridQuery.FromPool();
      }

      bool success = false;
      TileEntry entry = null;
      GridExtent regionExtent, globalExtent;
      TileIdEnumerator idEnumerator;
      if (query.GetRegionAndGlobalExtent(this, obj.WorldBounding, out regionExtent, out globalExtent) && TileIdEnumerator.TryCreate(regionExtent, out idEnumerator))
      {
        entry = TileEntry.FromPool(obj, m_currObjId, regionExtent, globalExtent);
        m_currObjId++;

        while (idEnumerator.MoveNext())
        {
          Int2 regionId = idEnumerator.Current;
          success |= AddToRegion(regionId, globalExtent, entry);
        }

        if (success)
        {
          entry.UpdateCoveredRegions();
          m_boundsNeedUpdate = true;
          m_objectCount++;
        }
        else
        {
          System.Diagnostics.Debug.Assert(false, "Adding entry to grid failed.");
          entry.Recycle();
          entry = null;
        }
      }

      GridHelper.RecycleGridExtents(ref regionExtent, ref globalExtent);

      if (queryPooled)
        query.Recycle();

      return entry;
    }

    public bool Remove(TileEntry entry)
    {
      if (entry == null || !entry.IsInGrid(this))
        return false;

      entry.RemoveFromTiles();
      m_objectCount--;
      entry.Recycle();
      m_boundsNeedUpdate = true;

      return true;
    }

    public bool Remove(ISpatial obj)
    {
      TileEntry entry = GetEntry(obj);
      if (entry != null)
      {
        entry.RemoveFromTiles();
        m_objectCount--;
        entry.Recycle();
        m_boundsNeedUpdate = true;

        return true;
      }

      return false;
    }

    public void Clear(bool removeRegions = false)
    {
      foreach (KeyValuePair<int, Region> kv in m_regions)
      {
        kv.Value.Clear();
      }

      if (removeRegions)
        m_regions.Clear();

      m_objectCount = 0;
      ResetBounds();
    }

    public void UpdateBounds()
    {
      if (!m_boundsNeedUpdate)
        return;

      bool firstRegion = true;
      foreach (KeyValuePair<int, Region> kv in m_regions)
      {
        kv.Value.UpdateBounds();
        UpdateBounds(kv.Value, firstRegion);
        firstRegion = false;
      }

      m_boundsNeedUpdate = false;
    }

    public void Import(IReadOnlyList<GridEntry> items, bool recycleGridExtents = true)
    {
      if (items == null)
        return;

      for (int i = 0; i < items.Count; i++)
      {
        GridEntry gridItem = items[i];

        if (gridItem.Object == null || gridItem.Object.WorldBounding == null || gridItem.RegionExtent.IsNull || gridItem.TileExtent.IsNull)
        {
          System.Diagnostics.Debug.Assert(false, "Grid Item is not valid");
          continue;
        }

        TileIdEnumerator idEnumerator;
        if (!TileIdEnumerator.TryCreate(gridItem.RegionExtent, out idEnumerator))
        {
          System.Diagnostics.Debug.Assert(false, "Tile ID Enumerator failed");
          continue;
        }

        TileEntry entry = TileEntry.FromPool(gridItem.Object, m_currObjId, gridItem.RegionExtent, gridItem.TileExtent);
        m_currObjId++;

        bool success = false;

        while (idEnumerator.MoveNext())
        {
          Int2 regionId = idEnumerator.Current;
          success |= AddToRegion(regionId, gridItem.TileExtent, entry);
        }

        if (success)
        {
          entry.UpdateCoveredRegions();
          m_boundsNeedUpdate = true;
          m_objectCount++;
        }
        else
        {
          System.Diagnostics.Debug.Assert(false, "Adding entry to grid failed.");
          entry.Recycle();
        }

        if (recycleGridExtents)
          GridHelper.RecycleGridExtents(ref gridItem.RegionExtent, ref gridItem.TileExtent);
      }
    }

    public void Export(IList<GridEntry> items)
    {
      if (items == null)
        return;

      //To avoid allocations, utilize the same mechanism when processing visible tiles
      m_visitSequence++;

      foreach (Region region in this)
      {
        foreach (Tile tile in region)
        {
          foreach (TileEntry entry in tile)
          {
            if (entry.CanVisit(m_visitSequence))
            {
              GridEntry gridEntry;
              gridEntry.Object = entry.Object;
              entry.CalculateTileExtents(out gridEntry.RegionExtent, out gridEntry.TileExtent);

              items.Add(gridEntry);
            }
          }
        }
      }
    }

    public bool PickRegions(PickQuery pickQuery, GridQuery query = null)
    {
      if (pickQuery == null || (pickQuery.Options & PickingOptions.PrimitivePick) == PickingOptions.PrimitivePick)
        return false;

      bool queryFromPool = false;
      if (query == null)
      {
        query = GridQuery.FromPool();
        queryFromPool = true;
      }

      bool success = false;

      GridExtent regionExtent;
      TileIdEnumerator regionIdEnumerator;
      if (query.GetRayRegionExtent(this, pickQuery.PickRay, out regionExtent) && TileIdEnumerator.TryCreate(m_regionRange, regionExtent, out regionIdEnumerator))
      {
        while (regionIdEnumerator.MoveNext())
        {
          Int2 regionId = regionIdEnumerator.Current;
          Region region = GetRegion(regionId);
          if (region != null)
            success |= pickQuery.AddPick(region);
        }
      }

      if (queryFromPool)
        query.Recycle();

      return success;
    }

    public bool PickTiles(PickQuery pickQuery, GridQuery query = null)
    {
      if (pickQuery == null || (pickQuery.Options & PickingOptions.PrimitivePick) == PickingOptions.PrimitivePick)
        return false;

      ref readonly Ray ray = ref pickQuery.PickRay;

      bool queryFromPool = false;
      if (query == null)
      {
        query = GridQuery.FromPool();
        queryFromPool = true;
      }

      bool success = false;

      GridExtent regionExtent, tileExtent;
      TileIdEnumerator regionIdEnumerator;
      if (query.GetRayRegionAndGlobalExtent(this, ray, out regionExtent, out tileExtent) && TileIdEnumerator.TryCreate(m_regionRange, regionExtent, out regionIdEnumerator))
      {
        while (regionIdEnumerator.MoveNext())
        {
          Int2 regionId = regionIdEnumerator.Current;
          Region region = GetRegion(regionId);
          if (region != null)
          {
            TileIdEnumerator tileIdEnumerator;
            GridRange tileRange = region.GlobalTileRange;
            if (region.WorldBounds.Intersects(ray) && TileIdEnumerator.TryCreate(tileRange, tileExtent, out tileIdEnumerator))
            {
              while (tileIdEnumerator.MoveNext())
              {
                Int2 tileId = tileIdEnumerator.Current;
                Tile tile = region.GetTile(tileId);
                if (tile != null)
                  success |= pickQuery.AddPick(tile);
              }
            }
          }
        }
      }

      if (queryFromPool)
        query.Recycle();

      return success;
    }

    public bool FindPicks(PickQuery query)
    {
      return FindPicks(query, null);
    }

    public bool FindPicks(PickQuery query, GridQuery gridQuery)
    {
      if (query == null)
        return false;

      m_visitSequence++;

      bool queryFromPool = false;
      if (gridQuery == null)
      {
        queryFromPool = true;
        gridQuery = GridQuery.FromPool();
      }

      bool success = false;
      GridExtent regionExtent, globalTileExtent;
      TileIdEnumerator idEnumerator;
      if (gridQuery.GetRayRegionAndGlobalExtent(this, query.PickRay, out regionExtent, out globalTileExtent) && TileIdEnumerator.TryCreate(m_regionRange, regionExtent, out idEnumerator))
      {
        while (idEnumerator.MoveNext())
        {
          Int2 regionId = idEnumerator.Current;

          Region region = GetRegion(regionId);
          if (region != null)
            success |= region.FindPicks(query, ref globalTileExtent);
        }
      }

      if (queryFromPool)
        gridQuery.Recycle();

      GridHelper.RecycleGridExtents(ref regionExtent, ref globalTileExtent);

      return success;
    }

    public void ProcessVisibleSet(IRenderer renderer)
    {
      ProcessVisibleSet(renderer, false);
    }

    public void ProcessVisibleSet(IRenderer renderer, bool skipCullCheck)
    {
      if (renderer == null || m_objectCount == 0)
        return;

      m_visitSequence++;

      List<Region> regionsToQuery = Pool<List<Region>>.Fetch();

      //If skipping cull check, copy all regions to list rather than doing a query
      if (skipCullCheck)
      {
        regionsToQuery.Capacity = Math.Min(regionsToQuery.Capacity, m_regions.Count);

        foreach (KeyValuePair<int, Region> kv in m_regions)
          regionsToQuery.Add(kv.Value);
      }
      else
      {
        ContainmentType gridContainment = QueryRegions(renderer.RenderContext.Camera.Frustum, regionsToQuery);

        //If entire grid contained in the frustum, then can skip cull checks of regions/tiles/objects...
        skipCullCheck = gridContainment == ContainmentType.Inside;
      }

      if (regionsToQuery.Count > 0)
      {
        FrustumCullState state = FrustumCullState.FromPool(regionsToQuery, renderer, m_visitSequence, skipCullCheck);
        state.Process();
        state.Recycle();
      }

      regionsToQuery.Clear();
      Pool<List<Region>>.Return(regionsToQuery);
    }

    public ContainmentType QueryRegions(BoundingVolume queryVolume, List<Region> queryResult, GridQuery query = null)
    {
      if (queryVolume == null || queryResult == null)
        return ContainmentType.Outside;

      ContainmentType containment = queryVolume.Contains(m_worldBounds);

      if (containment == ContainmentType.Outside)
        return ContainmentType.Outside;

      //If entire grid is contained by the frustum, then we iterate overall available regions
      if (containment == ContainmentType.Inside)
      {
        queryResult.Capacity = Math.Max(queryResult.Capacity, m_regions.Count);

        foreach (KeyValuePair<int, Region> kv in m_regions)
          queryResult.Add(kv.Value);

        //The entire grid is in view, so no further inspection of objects in the regions is necessary for visibility
        return ContainmentType.Inside;
      }
      else
      {
        //Otherwise need to do a grid query for the regions, since we're only interested in regions that exist, clip the query polygon to the size of the
        //grid
        bool addedOne = false;
        bool recycleQuery = false;
        if (query == null)
        {
          recycleQuery = true;
          query = GridQuery.FromPool();
        }

        GridExtent regionExtent;
        TileIdEnumerator idEnumerator;
        if (query.GetRegionExtent(this, queryVolume, out regionExtent, true) && TileIdEnumerator.TryCreate(regionExtent, out idEnumerator))
        {
          while (idEnumerator.MoveNext())
          {
            Int2 regionId = idEnumerator.Current;
            Region region = GetRegion(regionId);
            if (region != null)
            {
              queryResult.Add(region);
              addedOne = true;
            }
          }
        }

        if (recycleQuery)
          query.Recycle();

        if (regionExtent.CoveredTiles != null)
          regionExtent.CoveredTiles.Recycle();

        //If added at least one region, then the volume intersects with the grid, but further intersection tests may be needed for objects in the regions
        return (addedOne) ? ContainmentType.Intersects : ContainmentType.Outside;
      }
    }

    public ContainmentType QueryTiles(BoundingVolume queryVolume, List<Tile> queryResult, GridQuery query = null)
    {
      if (queryVolume == null || queryResult == null)
        return ContainmentType.Outside;

      ContainmentType containment = queryVolume.Contains(m_worldBounds);

      if (containment == ContainmentType.Outside)
        return ContainmentType.Outside;

      if (containment == ContainmentType.Inside)
      {
        queryResult.Capacity = Math.Max(queryResult.Capacity, m_regions.Count * m_regionTileExtent * m_regionTileExtent);

        foreach (KeyValuePair<int, Region> kv in m_regions)
        {
          //AddRange actually creates garbage...
          IReadOnlyList<Tile> tiles = kv.Value.Tiles;
          for (int i = 0; i < tiles.Count; i++)
            queryResult.Add(tiles[i]);
        }

        //The entire grid is in view, so no further inspection of objects in the regions is necessary for visibility
        return ContainmentType.Inside;
      }
      else
      {
        //Otherwise need to do a grid query for the regions, since we're only interested in regions that exist, clip the query polygon to the size of the
        //grid
        bool addedOne = false;
        bool recycleQuery = false;
        if (query == null)
        {
          recycleQuery = true;
          query = GridQuery.FromPool();
        }

        GridExtent regionExtent, tileExtent;
        TileIdEnumerator idEnumerator;
        if (query.GetRegionAndGlobalExtent(this, queryVolume, out regionExtent, out tileExtent, true) && TileIdEnumerator.TryCreate(m_regionRange, tileExtent, out idEnumerator))
        {
          while (idEnumerator.MoveNext())
          {
            Int2 regionId = idEnumerator.Current;
            Region region = GetRegion(regionId);
            if (region != null)
              addedOne |= region.GetTiles(tileExtent, queryResult);
          }
        }

        GridHelper.RecycleGridExtents(ref regionExtent, ref tileExtent);

        if (recycleQuery)
          query.Recycle();

        //If added at least one tile, then the volume intersects with the grid, but further intersection tests may be needed for objects in the tiles
        return (addedOne) ? ContainmentType.Intersects : ContainmentType.Outside;
      }
    }

    public ContainmentType QueryObjects<T>(BoundingVolume queryVolume, List<T> queryResult, GridQuery query = null) where T : ISpatial
    {
      return ContainmentType.Outside;
    }

    public Region GetRegion(in Int2 regionId)
    {
      int key = GridHelper.ToCantorPair(regionId.X, regionId.Y);
      Region region;
      m_regions.TryGetValue(key, out region);

      return region;
    }

    public bool RemoveEmptyRegions()
    {
      List<Region> temp = Pool<List<Region>>.Fetch();
      bool firstRegion = true;
      foreach (KeyValuePair<int, Region> kv in m_regions)
      {
        if (kv.Value.ObjectCount == 0)
        {
          temp.Add(kv.Value);
        }
        else
        {
          UpdateBounds(kv.Value, firstRegion);
          firstRegion = false;
        }
      }

      bool culledRegions = temp.Count > 0;

      for (int i = 0; i < temp.Count; i++)
      {
        Region region = temp[i];
        Int2 id = region.RegionId;
        int key = GridHelper.ToCantorPair(id.X, id.Y);
        m_regions.Remove(key);
        region.FixupNeighborsWhenRemoved();
      }

      temp.Clear();
      Pool<List<Region>>.Return(temp);

      return culledRegions;
    }

    public bool CreateRegion(in Int2 regionId)
    {
      if (regionId == Int2.Zero)
        return false;

      int key = GridHelper.ToCantorPair(regionId.X, regionId.Y);
      if (m_regions.ContainsKey(key))
        return true;

      Region region = new Region(regionId, m_regionSize, m_regionTileExtent, this);
      m_regions.Add(key, region);
      region.DiscoverNeighbors();
      UpdateBounds(region);

      return true;
    }

    public bool CreateRegions(in GridExtent extent)
    {
      bool created = false;
      TileIdEnumerator idEnumerator;
      if (!TileIdEnumerator.TryCreate(extent, out idEnumerator))
        return false;

      while (idEnumerator.MoveNext())
      {
        Int2 regionId = idEnumerator.Current;
        int key = GridHelper.ToCantorPair(regionId.X, regionId.Y);
        if (m_regions.ContainsKey(key))
          continue;

        Region region = new Region(regionId, m_regionSize, m_regionTileExtent, this);
        m_regions.Add(key, region);
        region.DiscoverNeighbors();
        UpdateBounds(region);
        created = true;
      }

      return created;
    }

    public bool RemoveRegion(in Int2 regionId)
    {
      GridExtent extent;
      GridExtent.FromMinMax(regionId, regionId, out extent);

      return RemoveRegions(extent);
    }

    public bool RemoveRegions(in GridExtent extent)
    {
      if (extent.IsNull)
        return false;

      bool removed = false;
      TileIdEnumerator idEnumerator;
      if (!TileIdEnumerator.TryCreate(m_regionRange, extent, out idEnumerator))
        return false;

      HashSet<TileEntry> tileEntriesToCleanup = Pool<HashSet<TileEntry>>.Fetch();

      while (idEnumerator.MoveNext())
      {
        Int2 regionId = idEnumerator.Current;
        int key = GridHelper.ToCantorPair(regionId.X, regionId.Y);
        Region region;
        if (m_regions.TryGetValue(key, out region))
        {
          //Ensure all entries are removed from tiles
          IReadOnlyList<Tile> tiles = region.Tiles;
          for (int i = 0; i < tiles.Count; i++)
          {
            IReadOnlyList<TileEntry> entries = tiles[i].Entries;
            for (int j = 0; j < entries.Count; j++)
            {
              TileEntry entry = entries[j];

              if (!tileEntriesToCleanup.Contains(entry))
                tileEntriesToCleanup.Add(entry);
            }
          }

          region.Clear();
          m_regions.Remove(key);
          region.FixupNeighborsWhenRemoved();
          removed = true;
        }
      }

      //Fixup tile entries
      foreach (TileEntry entry in tileEntriesToCleanup)
      {
        entry.UpdateCoveredRegions();

        if (entry.CoveredTiles.Count == 0)
        {
          m_objectCount--;
          entry.Recycle();
        }
      }

      tileEntriesToCleanup.Clear();
      Pool<HashSet<TileEntry>>.Return(tileEntriesToCleanup);

      return removed;
    }

    public void ForEachRegion(Action<Region> action)
    {
      if (action == null)
        return;

      foreach (KeyValuePair<int, Region> kv in m_regions)
        action(kv.Value);
    }

    public void ForEachTile(Action<Tile> action)
    {
      if (action == null)
        return;

      foreach (KeyValuePair<int, Region> kv in m_regions)
      {
        foreach (Tile tile in kv.Value)
          action(tile);
      }
    }

    public void ForEachObject(Action<TileEntry> action)
    {
      if (action == null)
        return;

      m_visitSequence++;

      foreach (KeyValuePair<int, Region> kv in m_regions)
      {
        foreach (Tile tile in kv.Value)
        {
          foreach (TileEntry entry in tile)
          {
            if (entry.CanVisit(m_visitSequence))
              action(entry);
          }
        }
      }
    }

    private bool AddToRegion(in Int2 regionId, in GridExtent extent, TileEntry entry)
    {
      int key = GridHelper.ToCantorPair(regionId.X, regionId.Y);
      Region region;
      if (!m_regions.TryGetValue(key, out region))
      {
        region = new Region(regionId, m_regionSize, m_regionTileExtent, this);
        region.DiscoverNeighbors();
        m_regions.Add(key, region);
        UpdateBounds(region);
      }

      return region.Add(entry, extent);
    }

    private void ResetBounds()
    {
      m_worldBounds.Set(new BoundingBox.Data());
      m_globalTileRange = new GridRange();
      m_regionRange = new GridRange();
      m_minHeight = 0.0f;
      m_maxHeight = 0.0f;
    }

    private void UpdateBounds(Region region, bool forceReset = false)
    {
      bool firstRegion = (m_regions.Count <= 1) || forceReset;

      GridRange tileRange = region.GlobalTileRange;
      GridRange regionRange = new GridRange(region.RegionId, region.RegionId);

      if (firstRegion)
      {
        m_worldBounds.Set(region.WorldBounds);
        m_globalTileRange = tileRange;
        m_regionRange = regionRange;
        m_minHeight = region.MinHeight;
        m_maxHeight = region.MaxHeight;
      }
      else
      {
        m_worldBounds.Merge(region.WorldBounds);
        GridRange.Merge(tileRange, m_globalTileRange, out m_globalTileRange);
        GridRange.Merge(regionRange, m_regionRange, out m_regionRange);

        m_minHeight = Math.Min(m_minHeight, region.MinHeight);
        m_maxHeight = Math.Max(m_maxHeight, region.MaxHeight);
      }
    }

    public RegionEnumerator GetEnumerator()
    {
      return new RegionEnumerator(m_regions);
    }

    IEnumerator<Region> IEnumerable<Region>.GetEnumerator()
    {
      return new RegionEnumerator(m_regions);
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return new RegionEnumerator(m_regions);
    }

    bool IPickable.IntersectsMesh(in Ray ray, IRefList<Pair<LineIntersectionResult, Triangle?>> results, bool ignoreBackfaces)
    {
      return false;
    }

    #region Region Enumerator

    public struct RegionEnumerator : IEnumerator<Region>
    {
      private Dictionary<int, Region> m_regions;
      private Dictionary<int, Region>.ValueCollection.Enumerator m_valueEnumerator;
      private Region m_current;

      public Region Current
      {
        get
        {
          return m_current;
        }
      }

      object IEnumerator.Current
      {
        get
        {
          return m_current;
        }
      }

      internal RegionEnumerator(Dictionary<int, Region> regions)
      {
        m_regions = regions;
        m_valueEnumerator = regions.Values.GetEnumerator();
        m_current = null;
      }

      public void Dispose()
      {
      }

      public bool MoveNext()
      {
        if (m_valueEnumerator.MoveNext())
        {
          m_current = m_valueEnumerator.Current;
          return true;
        }
        else
        {
          m_current = null;
          return false;
        }
      }

      public void Reset()
      {
        m_valueEnumerator = m_regions.Values.GetEnumerator();
        m_current = null;
      }
    }

    #endregion

    #region Dummy Renderer

    private class DummyRenderer : IRenderer
    {
      private IRenderContext m_context;
      private RenderQueue m_renderQueue;
      private RenderStageCollection m_renderStages;

      public IRenderContext RenderContext
      {
        get
        {
          return m_context;
        }
        set
        {
          m_context = value;
        }
      }

      public RenderQueue RenderQueue
      {
        get
        {
          return m_renderQueue;
        }
      }

      public RenderStageCollection RenderStages
      {
        get
        {
          return m_renderStages;
        }
      }

      public DummyRenderer()
      {
        m_renderStages = new RenderStageCollection();
        m_renderQueue = new RenderQueue();
      }

      public void SetDummy(IRenderer renderer)
      {
        m_context = renderer.RenderContext;

        if (AllBucketsMatch(renderer.RenderQueue))
          return;

        m_renderQueue.RemoveAllBuckets();

        foreach (RenderBucket bucket in renderer.RenderQueue)
        {
          m_renderQueue.AddBucket(new RenderBucket(bucket.BucketID, bucket.BucketComparer, 32));
        }
      }

      private bool AllBucketsMatch(RenderQueue other)
      {
        if (m_renderQueue.Count != other.Count)
          return false;

        for (int i = 0; i < other.Count; i++)
        {
          if (m_renderQueue[i].BucketID != other[i].BucketID)
            return false;
        }

        return true;
      }

      public bool Process(IRenderable renderable)
      {
        if (renderable == null || !renderable.IsValidForDraw)
          return false;

        return m_renderQueue.Enqueue(renderable);
      }

      public void Render(bool sortBuckets, bool clearBuckets)
      {
      }

      public void Recycle()
      {
        m_context = null;
        m_renderQueue.ClearBuckets(false);
        Pool<DummyRenderer>.Return(this);
      }

      public static DummyRenderer FromPool(IRenderer template)
      {
        DummyRenderer dummy = Pool<DummyRenderer>.Fetch();
        dummy.SetDummy(template);

        return dummy;
      }
    }


    #endregion

    #region Query cull state

    private class FrustumCullState
    {
      private List<Region> m_regionsToQuery;
      private IRenderer m_renderer;
      private BoundingFrustum m_frustum;
      private Action<int> m_parallelLoopBody;
      private int m_visitSequence;
      private bool m_skipAllCullChecks;
      private bool m_synchronous;

      public FrustumCullState()
      {
        m_parallelLoopBody = LoopBody;
      }

      public void Process()
      {
        if (m_regionsToQuery.Count <= REGION_COUNT_FOR_PARALLEL)
        {
          m_synchronous = true;

          for (int i = 0; i < m_regionsToQuery.Count; i++)
            LoopBody(i);
        }
        else
        {
          m_synchronous = false;
          Parallel.For(0, m_regionsToQuery.Count, m_parallelLoopBody);
        }
      }

      private void LoopBody(int index)
      {
        Region region = m_regionsToQuery[index];

        ContainmentType regionContainment = ContainmentType.Inside;

        //Only check region bounds if the grid is intersecting with the frustum (usually the case...but rarely everything may be in view)
        if (!m_skipAllCullChecks)
        {
          regionContainment = m_frustum.Contains(region.WorldBounds);

          //If region is outside, no more work to be done
          if (regionContainment == ContainmentType.Outside)
            return;
        }

        IRenderer dummy = (m_synchronous) ? m_renderer : null;
        IReadOnlyList<Tile> tiles = region.Tiles;

        for (int i = 0; i < tiles.Count; i++)
        {
          Tile tile = tiles[i];

          //Non-populated tiles may be null
          if (tile == null || tile.Entries.Count == 0)
            continue;

          ContainmentType tileContainment = ContainmentType.Inside;

          //Only check tile bounds if region is intersecting with frustum
          if ((regionContainment == ContainmentType.Intersects))
          {
            tileContainment = m_frustum.Contains(tile.WorldBounds);

            //If tile is outside, go to the next tile
            if (tileContainment == ContainmentType.Outside)
              continue;
          }

          //If the tile is completely inside the frustum, we know then all the objects in that tile are visible
          bool skipCullChecks = tileContainment == ContainmentType.Inside;

          //Lazy get the dummy renderer only if we actually have a tile that intersects with the frustum
          if (dummy == null)
            dummy = DummyRenderer.FromPool(m_renderer);

          IReadOnlyList<TileEntry> entries = tile.Entries;
          for (int j = 0; j < entries.Count; j++)
          {
            TileEntry entry = entries[j];
            if (entry.CanVisit(m_visitSequence))
            {
              entry.Object.ProcessVisibleSet(dummy, skipCullChecks);
            }
          }
        }

        //If have dummy renderer and actually async...make sure to copy render queue and recycle
        if (!m_synchronous && dummy != null)
        {
          if (HasObjects(dummy.RenderQueue))
          {
            lock (m_renderer)
              dummy.RenderQueue.CopyTo(m_renderer.RenderQueue);
          }

            (dummy as DummyRenderer).Recycle();
        }
      }

      private static bool HasObjects(RenderQueue queue)
      {
        for (int i = 0; i < queue.Count; i++)
        {
          if (queue[i].Count > 0)
            return true;
        }

        return false;
      }

      public void Recycle()
      {
        m_regionsToQuery = null;
        m_renderer = null;
        m_frustum = null;

        Pool<FrustumCullState>.Return(this);
      }

      public static FrustumCullState FromPool(List<Region> regions, IRenderer renderer, int visitSequence, bool skipAllCullChecks)
      {
        FrustumCullState state = Pool<FrustumCullState>.Fetch();
        state.m_regionsToQuery = regions;
        state.m_renderer = renderer;
        state.m_frustum = renderer.RenderContext.Camera.Frustum;
        state.m_visitSequence = visitSequence;
        state.m_skipAllCullChecks = skipAllCullChecks;

        return state;
      }
    }

    #endregion
  }
}
