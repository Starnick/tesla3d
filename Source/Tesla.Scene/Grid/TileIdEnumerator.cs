﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections;
using System.Collections.Generic;

namespace Tesla.Scene
{
  /// <summary>
  /// Enumerates all the Tile IDs that are covered by a specified <see cref="GridExtent"/>. The enumerator can optionally clip the range of tile IDs
  /// to a specified <see cref="GridRange"/>.
  /// </summary>
  public struct TileIdEnumerator : IEnumerator<Int2>
  {
    private static GridRange s_infiniteRange = new GridRange(new Int2(int.MinValue, int.MinValue), new Int2(int.MaxValue, int.MaxValue));

    private GridRange m_tileRange;
    private Int2 m_minMaxYIndices;
    private Int2 m_minMaxX;
    private int m_currYIndex;
    private int m_currYValue;
    private int m_currXValue;
    private TileRowExtentList m_tileRows;
    private Int2 m_current;
    private bool m_end;
    private bool m_inOuterLoop;

    /// <summary>
    /// Gets the current Tile ID.
    /// </summary>
    public readonly Int2 Current
    {
      get
      {
        return m_current;
      }
    }

    /// <summary>
    /// Gets the current Tile ID.
    /// </summary>
    readonly object IEnumerator.Current
    {
      get
      {
        return m_current;
      }
    }

    private void Initialize(in GridRange tileRange, in GridRange coveredRange)
    {
      m_tileRange = tileRange;
      m_tileRows = null;
      m_current = Int2.Zero;

      m_minMaxYIndices = new Int2(coveredRange.Min.Y, coveredRange.Max.Y);
      m_minMaxX = new Int2(coveredRange.Min.X, coveredRange.Max.X);

      m_currYIndex = m_minMaxYIndices.X;
      m_currYValue = 0;
      m_currXValue = 0;
      m_end = false;
      m_inOuterLoop = true;
    }

    private void Initialize(in GridRange tileRange, TileRowExtentList tileRows)
    {
      m_tileRange = tileRange;
      m_tileRows = tileRows;
      m_current = Int2.Zero;

      //Find the range of indices to iterate over (should always return true because we only call the ctor if we intersect)
      Int2 minMaxY = new Int2(tileRange.Min.Y, tileRange.Max.Y);

      bool foundYIndices = m_tileRows.FindIndexRange(ref minMaxY, out m_minMaxYIndices);

      System.Diagnostics.Debug.Assert(foundYIndices); //Sanity check

      m_currYIndex = m_minMaxYIndices.X;

      m_currYValue = 0;
      m_currXValue = 0;
      m_minMaxX = Int2.Zero;

      m_end = false;

      m_inOuterLoop = true;
    }

    public static bool TryCreate(in GridExtent extent, out TileIdEnumerator enumerator)
    {
      enumerator = new TileIdEnumerator();

      if (extent.IsNull)
        return false;

      if (extent.IsRectangle)
      {
        GridRange range;
        extent.GetRange(out range);

        enumerator.Initialize(s_infiniteRange, range);
        return true;
      }
      else
      {
        enumerator.Initialize(s_infiniteRange, extent.CoveredTiles);
        return true;
      }
    }

    public static bool TryCreate(in GridRange tileRange, in GridExtent extent, out TileIdEnumerator enumerator)
    {
      enumerator = new TileIdEnumerator();

      if (extent.IsNull)
        return false;

      if (extent.IsRectangle)
      {
        GridRange range;
        extent.GetRange(out range);

        //Make sure the range actually intersects
        GridRange covered;
        if (!range.Intersects(tileRange, out covered))
          return false;

        enumerator.Initialize(tileRange, covered);
        return true;
      }
      else
      {
        TileRowExtentList tileRows = extent.CoveredTiles;

        //Make sure the extent list actually intersects the region
        if (!tileRows.Bounds.Intersects(tileRange))
          return false;

        enumerator.Initialize(tileRange, tileRows);
        return true;
      }
    }

    public bool MoveNext()
    {
      if (m_end)
        return false;

      OuterLoop:

      if (m_inOuterLoop)
      {
        //Outer loop stop condition
        if (m_currYIndex > m_minMaxYIndices.Y)
        {
          m_end = true;
          return false;
        }

        //If the extent is rectangular, then the Y indices correspond to actual Y values, otherwise they correspond to row indices in the tile row list
        if (m_tileRows == null)
        {
          m_currYValue = m_currYIndex;
          m_currXValue = m_minMaxX.X;

          m_currYIndex++;

          //Skip over zero values
          if (m_currYIndex == 0)
            m_currYIndex++;

          m_inOuterLoop = false;
        }
        else
        {
          TileRowExtent row = m_tileRows[m_currYIndex];
          m_currYValue = row.Y;

          //Constraint x min, max to region's box
          m_minMaxX.X = Math.Max(row.XMin, m_tileRange.Min.X);
          m_minMaxX.Y = Math.Min(row.XMax, m_tileRange.Max.X);
          m_currXValue = m_minMaxX.X;

          //Corresponds to actual zero or more index so don't skip zero values
          m_currYIndex++;

          m_inOuterLoop = false;
        }
      }

      for (; m_currXValue <= m_minMaxX.Y; m_currXValue++)
      {
        if (m_currXValue == 0)
          continue;

        //Set the tile ID
        m_current = new Int2(m_currXValue, m_currYValue);

        //Increment the X value and check if we need to go into the outer loop on the next MoveNext()
        m_currXValue++;
        if (m_currXValue > m_minMaxX.Y)
          m_inOuterLoop = true;

        return true;
      }

      m_inOuterLoop = true;
      goto OuterLoop;
    }

    void IEnumerator.Reset() { }

    void IDisposable.Dispose() { }
  }
}
