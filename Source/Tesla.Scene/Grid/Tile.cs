﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using Tesla.Graphics;

namespace Tesla.Scene
{
  public class Tile : IEnumerable<TileEntry>, IPickable
  {
    private List<TileEntry> m_entries;
    private float m_tileSize;
    private BoundingBox m_tileBounds;
    private float m_minHeight, m_maxHeight;
    private Int2 m_globalTileId;
    private Int2 m_arrayIndex;
    private Region m_parent;

    public IReadOnlyList<TileEntry> Entries
    {
      get
      {
        return m_entries;
      }
    }

    public float TileSize
    {
      get
      {
        return m_tileSize;
      }
    }

    public BoundingBox WorldBounds
    {
      get
      {
        return m_tileBounds;
      }
    }

    public float MinHeight
    {
      get
      {
        return m_minHeight;
      }
    }

    public float MaxHeight
    {
      get
      {
        return m_maxHeight;
      }
    }

    public ref readonly Int2 ArrayIndex
    {
      get
      {
        return ref m_arrayIndex;
      }
    }

    public ref readonly Int2 GlobalTileId
    {
      get
      {
        return ref m_globalTileId;
      }
    }

    public Region Parent
    {
      get
      {
        return m_parent;
      }
    }

    BoundingVolume IPickable.WorldBounding
    {
      get
      {
        return m_tileBounds;
      }
    }

    public Tile(in Int2 globalTileId, in Int2 arrayIndex, float tileSize, Region parent)
    {
      m_parent = parent;
      m_tileSize = tileSize;
      m_arrayIndex = arrayIndex;
      m_globalTileId = globalTileId;
      m_entries = new List<TileEntry>();
      m_minHeight = 0.0f;
      m_maxHeight = 0.0f;

      BoundingBox.Data tileAABB;
      GridHelper.CalculatePhysicalCoordinates(tileSize, globalTileId, out tileAABB);

      m_tileBounds = new BoundingBox(tileAABB);
    }

    public TileEntry GetEntry(ISpatial obj)
    {
      if (obj == null)
        return null;

      IEqualityComparer comparer = m_parent.Parent.Comparer;

      for (int i = 0; i < m_entries.Count; i++)
      {
        TileEntry tileEntry = m_entries[i];
        if (comparer.Equals(tileEntry.Object, obj))
          return tileEntry;
      }

      return null;
    }

    public bool FindPicks(PickQuery query)
    {
      if (query == null)
        return false;

      if (!m_tileBounds.Intersects(query.PickRay))
        return false;

      bool success = false;
      for (int i = 0; i < m_entries.Count; i++)
      {
        TileEntry entry = m_entries[i];

        //Note - no visit guard, this is a public API if the caller wants to pick all the objects in this tile, let them
        success |= entry.Object.FindPicks(query);
      }

      return success;
    }

    internal bool FindPicks(PickQuery query, int visitSequence)
    {
      System.Diagnostics.Debug.Assert(query != null);

      bool success = false;
      for (int i = 0; i < m_entries.Count; i++)
      {
        TileEntry entry = m_entries[i];
        if (entry.CanVisit(visitSequence))
        {
          success |= entry.Object.FindPicks(query);
        }
      }

      return success;
    }

    internal bool Add(TileEntry entry, bool skipDuplicateCheck = false)
    {
      IEqualityComparer comparer = m_parent.Parent.Comparer;

      if (!skipDuplicateCheck)
      {
        for (int i = 0; i < m_entries.Count; i++)
        {
          TileEntry tileEntry = m_entries[i];
          if (comparer.Equals(tileEntry.Object, entry.Object))
            return false;
        }
      }

      m_entries.Add(entry);
      entry.OnAdded(this);
      m_parent.MarkTileDirty(this);

      return true;
    }

    internal TileEntry Remove(ISpatial obj, bool calledFromEntry = false)
    {
      IEqualityComparer comparer = m_parent.Parent.Comparer;

      int index = -1;
      TileEntry entryRemoved = null;
      for (int i = 0; i < m_entries.Count; i++)
      {
        TileEntry entry = m_entries[i];
        if (comparer.Equals(entry.Object, obj))
        {
          index = i;
          entryRemoved = entry;
          break;
        }
      }

      if (index == -1)
        return null;

      m_entries.RemoveAt(index);

      if (!calledFromEntry)
        entryRemoved.RemoveFromTile(this);

      m_parent.MarkTileDirty(this);

      return entryRemoved;
    }

    internal void Clear()
    {
      //Remove self from each entry, if no tiles refer to the entry anymore we fix it up later
      for (int i = 0; i < m_entries.Count; i++)
      {
        m_entries[i].RemoveFromTile(this);
      }

      m_entries.Clear();
    }

    internal void UpdateMinMaxHeight()
    {
      Vector3 extents = m_tileBounds.Extents;
      Vector3 center = m_tileBounds.Center;

      if (m_entries.Count == 0)
      {
        extents.Y = 0.0f;
        center.Y = 0.0f;
        m_minHeight = 0.0f;
        m_maxHeight = 0.0f;
        m_tileBounds.Extents = extents;
        m_tileBounds.Center = center;
        return;
      }

      m_minHeight = float.MaxValue;
      m_maxHeight = float.MinValue;

      for (int i = 0; i < m_entries.Count; i++)
      {
        TileEntry entry = m_entries[i];

        float localMinHeight, localMaxHeight;
        GridHelper.GetMinMaxHeight(entry.Object.WorldBounding, out localMinHeight, out localMaxHeight);

        m_minHeight = Math.Min(localMinHeight, m_minHeight);
        m_maxHeight = Math.Max(localMaxHeight, m_maxHeight);
      }

      float yCenter = (m_minHeight + m_maxHeight) * 0.5f;
      float yExtent = Math.Abs(m_maxHeight - m_minHeight) * 0.5f;
      extents.Y = yExtent;
      center.Y = yCenter;

      m_tileBounds.Extents = extents;
      m_tileBounds.Center = center;
    }

    public override String ToString()
    {
      return String.Format("{0}, Object Count: {1}", m_globalTileId.ToString(), m_entries.Count.ToString());
    }

    public EntryEnumerator GetEnumerator()
    {
      return new EntryEnumerator(m_entries);
    }

    IEnumerator<TileEntry> IEnumerable<TileEntry>.GetEnumerator()
    {
      return new EntryEnumerator(m_entries);
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return new EntryEnumerator(m_entries);
    }

    bool IPickable.IntersectsMesh(in Ray ray, IRefList<Pair<LineIntersectionResult, Triangle?>> results, bool ignoreBackfaces)
    {
      return false;
    }

    #region Entry Enumerator

    public struct EntryEnumerator : IEnumerator<TileEntry>
    {
      private List<TileEntry> m_entries;
      private TileEntry m_current;
      private int m_currentIndex;

      public TileEntry Current
      {
        get
        {
          return m_current;
        }
      }

      object IEnumerator.Current
      {
        get
        {
          return m_current;
        }
      }

      internal EntryEnumerator(List<TileEntry> entries)
      {
        m_entries = entries;
        m_current = null;
        m_currentIndex = 0;
      }

      public void Dispose()
      {
      }

      public bool MoveNext()
      {
        while (m_currentIndex < m_entries.Count)
        {
          m_current = m_entries[m_currentIndex];
          m_currentIndex++;

          if (m_current != null)
            return true;
        }

        return false;
      }

      public void Reset()
      {
        m_current = null;
        m_currentIndex = 0;
      }
    }

    #endregion
  }
}
