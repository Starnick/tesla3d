﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using Tesla.Content;
using Tesla.Utilities;
using System.Diagnostics;

namespace Tesla.Scene
{
  //TODO - Revisit to make into IReadOnlyRefList
  [DebuggerDisplay("Count = {Count}, IsRectangle = {IsRectangle}, Bounds = {Bounds}")]
  public sealed class TileRowExtentList : IReadOnlyList<TileRowExtent>, IEquatable<TileRowExtentList>
  {
    private static TileRowExtent[] s_emptyArray = new TileRowExtent[0];

    private TileRowExtent[] m_items;
    private GridRange m_range;
    private int m_count;
    private int m_version;
    private bool m_isRectangle;
    private bool m_hasChanged;

    public int Count
    {
      get
      {
        return m_count;
      }
    }

    public ref readonly GridRange Bounds
    {
      get
      {
        return ref m_range;
      }
    }

    public bool IsRectangle
    {
      get
      {
        if (m_hasChanged)
        {
          m_isRectangle = QueryIsRectangle();
          m_hasChanged = false;
        }

        return m_isRectangle;
      }
    }

    public TileRowExtent this[int index]
    {
      get
      {
        if (index < 0 || index >= m_count)
          throw new ArgumentOutOfRangeException("index");

        return m_items[index];
      }
    }

    public TileRowExtentList()
    {
      m_items = s_emptyArray;
      m_count = 0;
      m_version = 0;
      m_range = new GridRange();
      m_isRectangle = true;
      m_hasChanged = false;
    }

    public TileRowExtentList(int capacity)
    {
      m_items = new TileRowExtent[capacity];
      m_count = 0;
      m_version = 0;
      m_range = new GridRange();
      m_isRectangle = true;
    }

    public TileRowExtentList(TileRowExtentList list)
    {
      m_items = new TileRowExtent[list.Count];
      m_count = list.Count;
      m_version = 0;
      m_isRectangle = list.IsRectangle;
      m_range = list.m_range;
      m_hasChanged = list.m_hasChanged;

      Array.Copy(list.m_items, m_items, m_count);
    }

    public bool AddOrUpdate(int x, int y)
    {
      if (x == 0 || y == 0)
        return false;

      TileRowExtent row;
      row.Y = y;
      row.XMax = x;
      row.XMin = x;

      return AddOrUpdate(row);
    }

    public bool AddOrUpdate(in TileRowExtent row)
    {
      if (row.Y == 0)
        return false;

      int index = BinarySearch(m_items, 0, m_count, row.Y);
      if (index < 0)
      {
        int insertIndex = ~index;

        //Expand array if necessary
        if (m_count == m_items.Length)
          EnsureCapacity(m_count + 1);

        //Shift contents
        if (insertIndex < m_count)
          Array.Copy(m_items, insertIndex, m_items, insertIndex + 1, m_count - insertIndex);

        //Update X bounds...only grows, but we never remove tiles either
        if (m_count == 0)
        {
          //First addition, make sure the range isn't zero
          m_range.Min.X = row.XMin;
          m_range.Max.X = row.XMax;
        }
        else
        {
          m_range.Min.X = Math.Min(row.XMin, m_range.Min.X);
          m_range.Max.X = Math.Max(row.XMax, m_range.Max.X);
        }

        //Set the value
        m_items[insertIndex] = row;
        m_count++;
        m_version++;
      }
      else
      {
        TileRowExtent entry = m_items[index];

        //No need for update
        if (entry.XMin == row.XMin && entry.XMax == row.XMax)
          return true;

        entry.XMin = Math.Min(entry.XMin, row.XMin);
        entry.XMax = Math.Max(entry.XMax, row.XMax);

        //Update X bounds...only grows, but we never remove tiles either
        m_range.Min.X = Math.Min(entry.XMin, m_range.Min.X);
        m_range.Max.X = Math.Max(entry.XMax, m_range.Max.X);

        m_items[index] = entry;
        m_version++;
      }

      //Update Y bounds
      m_range.Min.Y = m_items[0].Y;
      m_range.Max.Y = m_items[m_count - 1].Y;
      m_hasChanged = true;
      return true;
    }

    public bool FindIndexRange(ref Int2 minMaxY, out Int2 minMaxIndices)
    {
      if (m_count == 0)
      {
        minMaxIndices = Int2.Zero;
        return false;
      }

      //Quick check to avoid binary searches - if the tile row min and max fits within the testing range already, we can return the entire list range
      if (WithinYRange(minMaxY.X, minMaxY.Y))
      {
        minMaxIndices = new Int2(0, m_count - 1);
        return true;
      }

      int startIndex = BinarySearch(m_items, 0, m_count, minMaxY.X);
      int endIndex = BinarySearch(m_items, 0, m_count, minMaxY.Y);

      if (startIndex < 0)
        startIndex = ~startIndex;

      if (endIndex < 0)
        endIndex = Math.Max(0, ~endIndex - 1); //Want the previous index

      minMaxIndices.X = startIndex;
      minMaxIndices.Y = endIndex;

      return true;
    }

    public void Clear()
    {
      if (m_count == 0)
        return;

      Array.Clear(m_items, 0, m_count);
      m_count = 0;
      m_isRectangle = true;
      m_version++;
      m_range = new GridRange();
    }

    //Purposefully not sorting...just want to do a straight memcpy to the other list
    public void CopyTo(TileRowExtentList otherList)
    {
      if (otherList == null)
        return;

      otherList.EnsureCapacity(m_count + otherList.m_count);

      Array.Copy(m_items, 0, otherList.m_items, otherList.m_count, m_count);
      otherList.m_count += m_count;

      //Even if other list has entries that make it rectangular, if what we're copying isn't rectangular, then the other list won't be by definition
      if (!m_isRectangle)
        otherList.m_isRectangle = false;

      otherList.CalculateRange();
      otherList.m_version++;
    }

    internal void ReadContents(String name, bool isKnownRectangular, IPrimitiveReader input)
    {
      name = (String.IsNullOrEmpty(name)) ? "TileRowExtentList" : name;

      Clear();

      //Begin reading
      int count = input.BeginReadGroup(name);

      //Resize the array if necessary
      EnsureCapacity(count);

      for (int i = 0; i < count; i++)
        input.Read<TileRowExtent>("Item", out m_items[i]);

      input.EndReadGroup();

      //Set last remaining bits and recompute bounds
      m_isRectangle = isKnownRectangular;
      m_count = count;
      CalculateRange();
    }

    internal void WriteContents(String name, IPrimitiveWriter output)
    {
      name = (String.IsNullOrEmpty(name)) ? "TileRowExtentList" : name;

      output.BeginWriteGroup(name, m_count);

      for (int i = 0; i < m_count; i++)
        output.Write<TileRowExtent>("Item", m_items[i]);

      output.EndWriteGroup();
    }

    private void EnsureCapacity(int min, bool forceToMin = false)
    {
      if (m_items.Length < min)
      {
        int newCapacity = Math.Max((m_items.Length == 0) ? 4 : m_items.Length * 2, min);

        TileRowExtent[] newArray = new TileRowExtent[newCapacity];

        if (m_count > 0)
          Array.Copy(m_items, 0, newArray, 0, m_count);

        m_items = newArray;
      }
    }

    private bool WithinYRange(int minY, int maxY)
    {
      if (m_range.Min.Y >= minY && m_range.Max.Y <= maxY)
        return true;

      return false;
    }

    private void CalculateRange()
    {
      m_range = new GridRange();

      if (m_count == 1)
      {
        TileRowExtent first = m_items[0];

        m_range.Min = new Int2(first.XMin, first.Y);
        m_range.Max = new Int2(first.XMax, first.Y);
      }
      else if (m_count > 0)
      {
        TileRowExtent first = m_items[0];
        TileRowExtent last = m_items[m_count - 1];

        int minY = first.Y;
        int maxY = last.Y;
        int minX = first.XMin;
        int maxX = last.XMax;

        //If not rectangular, need to go through each row, otherwise the min/max is known from the first and last rows
        if (!m_isRectangle)
        {
          for (int i = 0; i < m_count; i++)
          {
            minX = Math.Min(minX, m_items[i].XMin);
            maxX = Math.Max(maxX, m_items[i].XMax);
          }
        }

        m_range.Min = new Int2(minX, minY);
        m_range.Max = new Int2(maxX, maxY);
      }
    }

    public bool QueryIsRectangle()
    {
      //Each row will fit inside the bounds, if any row has a XMin, XMax that is smaller then there is void space
      for (int i = 0; i < m_count; i++)
      {
        TileRowExtent row = m_items[i];
        if (row.XMax < m_range.Max.X || row.XMin > m_range.Min.X)
          return false;
      }

      return true;
    }

    public override int GetHashCode()
    {
      return m_range.GetHashCode();
    }

    public override bool Equals(object obj)
    {
      if (obj is TileRowExtentList)
        return Equals(obj as TileRowExtentList);

      return false;
    }

    public bool Equals(TileRowExtentList other)
    {
      if (other == this)
        return true;

      if (other == null || other.Count != m_count || !other.m_range.Equals(m_range))
        return false;

      for (int i = 0; i < m_count; i++)
      {
        if (!m_items[i].Equals(other.m_items[i]))
          return false;
      }

      return true;
    }

    public void Recycle()
    {
      Clear();
      Pool<TileRowExtentList>.Return(this);
    }

    public static TileRowExtentList FromPool()
    {
      return Pool<TileRowExtentList>.Fetch();
    }

    public Enumerator GetEnumerator()
    {
      return new Enumerator(this);
    }

    IEnumerator<TileRowExtent> IEnumerable<TileRowExtent>.GetEnumerator()
    {
      return new Enumerator(this);
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return new Enumerator(this);
    }

    private static int BinarySearch(TileRowExtent[] rows, int startIndex, int count, int y)
    {
      int max = count - 1;
      int min = startIndex;

      while (min <= max)
      {
        int guessIndex = min + ((max - min) >> 1);
        int value = rows[guessIndex].Y;

        if (value < y)
        {
          min = guessIndex + 1;
        }
        else if (value > y)
        {
          max = guessIndex - 1;
        }
        else
        {
          return guessIndex;
        }
      }

      return ~min;
    }

    #region Enumerator

    public struct Enumerator : IEnumerator<TileRowExtent>
    {
      private TileRowExtentList m_list;
      private int m_version;
      private int m_index;
      private TileRowExtent m_current;

      public TileRowExtent Current
      {
        get
        {
          return m_current;
        }
      }

      object IEnumerator.Current
      {
        get
        {
          return m_current;
        }
      }

      internal Enumerator(TileRowExtentList list)
      {
        m_list = list;
        m_version = list.m_version;
        m_index = 0;
        m_current = new TileRowExtent();
      }

      public void Dispose()
      {
      }

      public bool MoveNext()
      {
        if (m_version != m_list.m_version)
          throw new InvalidOperationException();

        if (m_index < m_list.m_count)
        {
          m_current = m_list.m_items[m_index];
          m_index++;
          return true;
        }

        m_index = m_list.m_count + 1;
        m_current = new TileRowExtent();
        return false;
      }

      public void Reset()
      {
        m_index = 0;
        m_current = new TileRowExtent();
      }
    }

    #endregion
  }
}
