﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Content;

namespace Tesla.Scene
{
  /// <summary>
  /// Represents a collection of hints that are used during rendering of an object in the scenegraph.
  /// </summary>
  /// </summary>
  [SavableVersion(1)]
  public sealed class SceneHints : ISavable
  {
    private IHintable m_source;
    private CullHint m_cullHint;
    private PickingHint m_pickHint;
    private LightCombineHint m_lightHint;
    private BoundingCombineHint m_boundingHint;

    /// <summary>
    /// Gets or sets the cull hint. If the local hint is set to inherit, then this is influenced by the parent hintable.
    /// </summary>
    public CullHint CullHint
    {
      get
      {
        //If not inherit, return local
        if (m_cullHint != CullHint.Inherit)
          return m_cullHint;

        //If inherit, return parent's
        IHintable parent = m_source.ParentHintable;
        if (parent != null)
          return parent.SceneHints.CullHint;

        //If inherit with no parent, return default
        return Scene.CullHint.Dynamic;
      }
      set
      {
        m_cullHint = value;
      }
    }

    /// <summary>
    /// Gets or sets the light hint. If the local hint is set to inherit, then this is influenced by the parent hintable.
    /// </summary>
    public LightCombineHint LightCombineHint
    {
      get
      {
        //If not inherit, return local
        if (m_lightHint != LightCombineHint.Inherit)
          return m_lightHint;

        //If inherit, return parent's
        IHintable parent = m_source.ParentHintable;
        if (parent != null)
          return parent.SceneHints.LightCombineHint;

        //If inherit with no parent, return default
        return LightCombineHint.CombineClosest;
      }
      set
      {
        m_lightHint = value;
      }
    }

    /// <summary>
    /// Gets or sets the bounding hint. If the local hint is set to inherit, then this is influenced by the parent hintable.
    /// </summary>
    public BoundingCombineHint BoundingCombineHint
    {
      get
      {
        if (m_boundingHint != Scene.BoundingCombineHint.Inherit)
          return m_boundingHint;

        //If inherit, return parent's
        IHintable parent = m_source.ParentHintable;
        if (parent != null)
          return parent.SceneHints.BoundingCombineHint;

        //If inherit with no parent, return default
        return BoundingCombineHint.AxisAlignedBoundingBox;
      }
      set
      {
        m_boundingHint = value;
      }
    }

    /// <summary>
    /// Gets or sets the picking hint. If the local is set to inherit, then this is influenced by the parent hintable.
    /// </summary>
    public PickingHint PickingHint
    {
      get
      {
        //If not inherit, return local
        if (m_pickHint != PickingHint.Inherit)
          return m_pickHint;

        //If inherit, return parent's
        IHintable parent = m_source.ParentHintable;
        if (parent != null)
          return parent.SceneHints.PickingHint;

        //If inherit with no parent, return default
        return PickingHint.PickableAndCollidable;
      }
      set
      {
        m_pickHint = value;
      }
    }

    /// <summary>
    /// Gets if the object can be used in picking queries.
    /// </summary>
    public bool IsPickable
    {
      get
      {
        PickingHint hint = PickingHint;
        return hint == PickingHint.Pickable || hint == PickingHint.PickableAndCollidable;
      }
    }

    /// <summary>
    /// Gets if the object can be used in collision queries.
    /// </summary>
    public bool IsCollidable
    {
      get
      {
        PickingHint hint = PickingHint;
        return hint == PickingHint.Collidable || hint == PickingHint.PickableAndCollidable;
      }
    }

    /// <summary>
    /// For ISavable.
    /// </summary>
    private SceneHints() { }

    /// <summary>
    /// Constructs a new instance of the <see cref="SceneHints"/> class.
    /// </summary>
    /// <param name="source">Source hintable</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the source hintable is null.</exception>
    public SceneHints(IHintable source)
    {
      if (source == null)
        throw new ArgumentNullException("source");

      m_source = source;
      m_cullHint = CullHint.Inherit;
      m_lightHint = LightCombineHint.Inherit;
      m_boundingHint = BoundingCombineHint.Inherit;
      m_pickHint = PickingHint.Inherit;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="SceneHints"/> class.
    /// </summary>
    /// <param name="source">Source hintable</param>
    /// <param name="toCopyFrom">Scene hints to copy data from.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the source hintable or template scene hints are null.</exception>
    public SceneHints(IHintable source, SceneHints toCopyFrom)
    {
      if (source == null)
        throw new ArgumentNullException("source");

      m_source = source;
      Set(toCopyFrom);
    }

    /// <summary>
    /// Sets the hintable source.
    /// </summary>
    /// <param name="src">Source hintable.</param>
    public void SetSource(IHintable src)
    {
      m_source = src;
    }

    /// <summary>
    /// Copies the scene hints from another instance.
    /// </summary>
    /// <param name="toCopyFrom">Scene hints to copy data from.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the template scene hints is null.</exception>
    public void Set(SceneHints toCopyFrom)
    {
      if (toCopyFrom == null)
        throw new ArgumentNullException("toCopyFrom");

      m_cullHint = toCopyFrom.m_cullHint;
      m_lightHint = toCopyFrom.m_lightHint;
      m_boundingHint = toCopyFrom.m_boundingHint;
      m_pickHint = toCopyFrom.m_pickHint;
    }

    /// <summary>
    /// Reads the object data from the input.
    /// </summary>
    /// <param name="input">Savable reader</param>
    public void Read(ISavableReader input)
    {
      m_cullHint = input.ReadEnum<CullHint>("CullHint");
      m_pickHint = input.ReadEnum<PickingHint>("PickingHint");
      m_lightHint = input.ReadEnum<LightCombineHint>("LightCombineHint");
      m_boundingHint = input.ReadEnum<BoundingCombineHint>("BoundingCombineHint");
    }

    /// <summary>
    /// Writes the object data to the output.
    /// </summary>
    /// <param name="output">Savable writer</param>
    public void Write(ISavableWriter output)
    {
      output.WriteEnum<CullHint>("CullHint", m_cullHint);
      output.WriteEnum<PickingHint>("PickingHint", m_pickHint);
      output.WriteEnum<LightCombineHint>("LightCombineHint", m_lightHint);
      output.WriteEnum<BoundingCombineHint>("BoundingCombineHint", m_boundingHint);
    }
  }
}
