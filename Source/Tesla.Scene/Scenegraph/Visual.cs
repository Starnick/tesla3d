﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Graphics;
using Tesla.Content;

namespace Tesla.Scene
{
  [SavableVersion(1)]
  public abstract class Visual : Spatial, IRenderable
  {
    private MaterialDefinition m_matDef;
    private BoundingVolume m_modelBounding;
    private LightCollection m_worldLights;
    private RenderPropertyCollection m_renderProperties;
    private LightComparer m_lightComparer;

    public MaterialDefinition MaterialDefinition
    {
      get
      {
        return m_matDef;
      }
      set
      {
        m_matDef = value;

        if (value != null)
          m_worldLights.UpdateShader = true;
      }
    }

    public bool IsMaterialValid
    {
      get
      {
        return m_matDef != null && m_matDef.AreMaterialsValid();
      }
    }

    public BoundingVolume ModelBounding
    {
      get
      {
        return m_modelBounding;
      }
      set
      {
        SetModelBounding(value);
      }
    }

    public LightCollection WorldLights
    {
      get
      {
        return m_worldLights;
      }
    }

    public RenderPropertyCollection RenderProperties
    {
      get
      {
        return m_renderProperties;
      }
    }

    public bool IsPickable
    {
      get
      {
        return SceneHints.IsPickable;
      }
      set
      {

        SceneHints hints = SceneHints;
        PickingHint hint = hints.PickingHint;

        if (value)
        {
          switch (hint)
          {
            case PickingHint.Collidable:
              hints.PickingHint = PickingHint.PickableAndCollidable;
              break;
            case PickingHint.None:
            case PickingHint.Inherit:
              hints.PickingHint = PickingHint.Pickable;
              break;
          }
        }
        else
        {
          switch (hint)
          {
            case PickingHint.PickableAndCollidable:
              hints.PickingHint = PickingHint.Collidable;
              break;
            case PickingHint.Pickable:
            case PickingHint.Inherit:
              hints.PickingHint = PickingHint.None;
              break;
          }
        }
      }
    }

    public abstract bool IsValidForDraw
    {
      get;
    }

    //For ISavable
    protected Visual() { }

    protected Visual(String name)
        : base(name)
    {
      //Monitor light changes, because while the visual may not need to be updated, we will still want to update the shader with new light info
      //when we render the visual
      m_worldLights = new LightCollection();
      m_worldLights.MonitorLightChanges = true;
      m_renderProperties = new RenderPropertyCollection();
      SetDefaultRenderProperties(false);
    }

    public abstract void UpdateModelBounding();
    public abstract void SetupDrawCall(IRenderContext renderContext, RenderBucketID currentBucketID, MaterialPass currentPass);

    public override void SetMaterialDefinition(MaterialDefinition newMatDef, bool copyMatDef)
    {
      m_matDef = (copyMatDef) ? newMatDef.Clone() : newMatDef;
      m_worldLights.UpdateShader = true;
    }

    public override void SetModelBounding(BoundingVolume volume, bool calculateBounds)
    {
      m_modelBounding = volume;

      if (volume != null)
      {
        SetWorldBounding(volume.Clone());
      }
      else
      {
        RemoveWorldBounding();
      }

      //Decide if we use the bounding volume as is, or are using it as a template but
      //still want to calculate based on the mesh
      if (calculateBounds)
      {
        UpdateModelBounding();
      }
      else
      {
        //Even without calculation, the model bounding has changed
        PropagateDirtyUp(DirtyMark.Bounding);
      }
    }

    public override void UpdateWorldBound(bool recurse)
    {
      if (m_modelBounding != null)
      {
        //Set and transform from local bound - guaranted to be the same type when set model bound.
        WorldBounding.Set(m_modelBounding);
        WorldBounding.Transform(WorldTransform);
      }
      else
      {
        RemoveWorldBounding();
      }

      ClearDirty(DirtyMark.Bounding);
    }

    public override void UpdateWorldLights(bool recurse)
    {
      m_worldLights.Clear();

      LightCollection localLights = Lights;
      m_worldLights.GlobalAmbient = localLights.GlobalAmbient;
      m_worldLights.IsEnabled = localLights.IsEnabled;

      if (!m_worldLights.IsEnabled)
        return;

      switch (SceneHints.LightCombineHint)
      {
        case LightCombineHint.Local:
          m_worldLights.AddRange(localLights);
          SortLights();
          break;
        case LightCombineHint.CombineClosest:
          CollectLights(m_worldLights);
          SortLights();
          break;
      }

      ClearDirty(DirtyMark.Lighting);
    }

    public override void SortLights()
    {
      if (m_lightComparer == null)
        m_lightComparer = new LightComparer(this);

      m_worldLights.Sort(m_lightComparer);
    }

    public override void Read(ISavableReader input)
    {
      base.Read(input);

      m_matDef = input.ReadExternalSavable<MaterialDefinition>("MaterialDefinition");
      m_modelBounding = input.ReadSavable<BoundingVolume>("ModelBounding");

      //If have a model bounding, need to copy type as world bounding
      if (m_modelBounding != null)
        SetWorldBounding(m_modelBounding.Clone());

      m_renderProperties = input.ReadSavable<RenderPropertyCollection>("RenderProperties");

      //The default render properties are property accessors, and as such are not serialized
      m_worldLights = new LightCollection();
      m_worldLights.MonitorLightChanges = true;
      SetDefaultRenderProperties(true);
    }

    public override void Write(ISavableWriter output)
    {
      base.Write(output);

      output.WriteExternalSavable<MaterialDefinition>("MaterialDefinition", m_matDef);
      output.WriteSavable<BoundingVolume>("ModelBounding", m_modelBounding);
      output.WriteSavable<RenderPropertyCollection>("RenderProperties", m_renderProperties);
    }

    protected virtual void SetDefaultRenderProperties(bool fromSavable)
    {
      m_renderProperties.Add(new WorldTransformProperty(WorldTransform));
      m_renderProperties.Add(new LightCollectionProperty(WorldLights));

      m_renderProperties.Add(new WorldBoundingVolumeProperty(delegate ()
      {
        return WorldBounding;
      }));

      //Ortho order will be serialized
      if (!fromSavable)
      {
        m_renderProperties.Add(new OrthoOrderProperty(0));
      }
    }

    protected override void PopulateClone(Spatial clone, CopyContext copyContext)
    {
      base.PopulateClone(clone, copyContext);

      Visual vs = clone as Visual;
      if (vs == null)
        return;

      vs.m_worldLights = new LightCollection(m_worldLights);
      vs.m_worldLights.MonitorLightChanges = true;
      vs.m_renderProperties = m_renderProperties.Clone();

      if (m_lightComparer != null)
        vs.m_lightComparer = new LightComparer(vs);

      if (copyContext[CopyContext.CopyMaterialKey])
      {
        if (m_matDef != null)
          vs.m_matDef = m_matDef.Clone();
      }
      else
      {
        vs.m_matDef = m_matDef;
      }

      if (m_modelBounding != null)
      {
        vs.m_modelBounding = m_modelBounding.Clone();
        vs.SetWorldBounding(WorldBounding.Clone());
      }

      vs.SetDefaultRenderProperties(true);
    }
  }
}
