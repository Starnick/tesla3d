﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;

namespace Tesla.Scene
{
  /// <summary>
  /// Dirty flags for use in updating the scene graph.
  /// </summary>
  [Flags]
  public enum DirtyMark
  {
    /// <summary>
    /// No update necessary.
    /// </summary>
    None = 0,

    /// <summary>
    /// Transform values require updating.
    /// </summary>
    Transform = 1,

    /// <summary>
    /// Bounding values require updating.
    /// </summary>
    Bounding = 2,

    /// <summary>
    /// Lighting state requires updating.
    /// </summary>
    Lighting = 4,

    /// <summary>
    /// All states require updating.
    /// </summary>
    All = Transform | Bounding | Lighting
  }

  /// <summary>
  /// Hint for how the engine combines lights when updating the world light list for each mesh.
  /// </summary>
  public enum LightCombineHint
  {
    /// <summary>
    /// Do what the parent does.
    /// </summary>
    Inherit = 0,

    /// <summary>
    /// Combine lights starting from the spatial working towards the root node. The resulting lights are sorted and only the closest are used up to the max number
    /// of lights.
    /// </summary>
    CombineClosest = 1,

    /// <summary>
    /// Use only the spatial's local lights and do not combine.
    /// </summary>
    Local = 2,

    /// <summary>
    /// Do not update lights.
    /// </summary>
    Off = 3
  }

  /// <summary>
  /// Hint for how the engine will treat scene graph Spatials during the culling pass.
  /// </summary>
  public enum CullHint
  {
    /// <summary>
    /// Do what the parent does.
    /// </summary>
    Inherit = 0,

    /// <summary>
    /// Cull based on visibility, if the spatial is outside the frustum it is culled. Otherwise it is not.
    /// </summary>
    Dynamic = 1,

    /// <summary>
    /// Always cull the spatial, regardless of visibility.
    /// </summary>
    Always = 2,

    /// <summary>
    /// Never cull the spatial, regardless of visibility.
    /// </summary>
    Never = 3
  }

  /// <summary>
  /// Picking hints for how the engine will treat the scene graph Spatials during picking/collision queries.
  /// </summary>
  public enum PickingHint
  {
    /// <summary>
    /// Spatial will not be included in either picking or collision tests.
    /// </summary>
    None = 0,

    /// <summary>
    /// Do what the parent does.
    /// </summary>
    Inherit = 1,

    /// <summary>
    /// Spatial can be included in picking tests.
    /// </summary>
    Pickable = 2,

    /// <summary>
    /// Spatial can be included in collision tests.
    /// </summary>
    Collidable = 3,

    /// <summary>
    /// Spatial can be included in both picking and collision tests.
    /// </summary>
    PickableAndCollidable = 4
  }

  /// <summary>
  /// Bounding hints for how nodes in the scenegraph will construc their world boundings based on their children.
  /// </summary>
  public enum BoundingCombineHint
  {
    /// <summary>
    /// Do what the parent does.
    /// </summary>
    Inherit = 0,

    /// <summary>
    /// Clone the first valid child's world bounding and use that type as the world bounding of the node.
    /// </summary>
    CloneFromChildren = 1,

    /// <summary>
    /// Always use a bounding sphere.
    /// </summary>
    Sphere = 2,

    /// <summary>
    /// Always use an axis aligned bounding box.
    /// </summary>
    AxisAlignedBoundingBox = 3,

    /// <summary>
    /// Always use a bounding capsule.
    /// </summary>
    Capsule = 4,

    /// <summary>
    /// Always use an oriented bounding box.
    /// </summary>
    OrientedBoundingBox = 5
  }
}