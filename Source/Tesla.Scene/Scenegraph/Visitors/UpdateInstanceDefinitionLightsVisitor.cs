﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesla;
using Tesla.Graphics;

namespace Tesla.Scene
{
  public sealed class UpdateInstanceDefinitionLightsVisitor : ISpatialVisitor
  {
    private LightCollection m_lights;
    private bool m_clearAndAdd;

    public LightCollection Lights
    {
      get
      {
        return m_lights;
      }
      set
      {
        m_lights = value;
      }
    }

    public bool ClearAndAdd
    {
      get
      {
        return m_clearAndAdd;
      }
      set
      {
        m_clearAndAdd = value;
      }
    }

    public UpdateInstanceDefinitionLightsVisitor() : this(null, false) { }

    public UpdateInstanceDefinitionLightsVisitor(LightCollection lights) : this(lights, false) { }

    public UpdateInstanceDefinitionLightsVisitor(LightCollection lights, bool clearAndAdd)
    {
      m_clearAndAdd = clearAndAdd;
      m_lights = lights;

      if (m_lights == null)
      {
        m_lights = new LightCollection();
        m_lights.MonitorLightChanges = false;
      }
    }

    public bool Visit(Spatial spatial)
    {
      if (m_lights == null)
        return false;

      Mesh msh = spatial as Mesh;
      if (msh != null && msh.IsInstanced)
      {
        InstanceDefinition instanceDef = msh.InstanceDefinition;
        instanceDef.WorldLights.GlobalAmbient = m_lights.GlobalAmbient;
        instanceDef.WorldLights.IsEnabled = m_lights.IsEnabled;

        if (!instanceDef.WorldLights.IsEnabled)
          return true;

        if (m_clearAndAdd)
          instanceDef.WorldLights.Clear();

        instanceDef.WorldLights.AddRange(m_lights);
      }

      return true;
    }
  }
}
