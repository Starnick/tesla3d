﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesla.Graphics;
using Tesla.Utilities;
using Tesla;

namespace Tesla.Scene
{
  public sealed class GeometryDebuggerVisitor : ISpatialVisitor
  {
    private GeometryDebugger m_geomDebugger;
    private IRenderContext m_context;
    private bool m_drawTransparent;
    private DebugView m_flags;
    private bool m_drawOnlyLeaf;
    private byte m_savedAlpha;

    public bool DrawTransparent
    {
      get
      {
        return m_drawTransparent;
      }
      set
      {
        m_drawTransparent = value;
      }
    }

    public bool DrawOnlyLeafObjects
    {
      get
      {
        return m_drawOnlyLeaf;
      }
      set
      {
        m_drawOnlyLeaf = value;
      }
    }

    public DebugView ViewFlags
    {
      get
      {
        return m_flags;
      }
      set
      {
        m_flags = value;
      }
    }

    public GeometryDebugger GeometryDebugger
    {
      get
      {
        return m_geomDebugger;
      }
    }

    public GeometryDebuggerVisitor()
    {
      m_geomDebugger = new GeometryDebugger();
      m_flags = DebugView.BoundingVolumes;
    }

    public GeometryDebuggerVisitor(IRenderSystem renderSystem)
    {
      m_geomDebugger = new GeometryDebugger(renderSystem);
      m_flags = DebugView.BoundingVolumes;
    }

    public void Draw(IRenderContext renderContext, Spatial sceneGraph)
    {
      Draw(renderContext, sceneGraph, true);
    }

    public void Draw(IRenderContext renderContext, Spatial sceneGraph, bool visitChildren)
    {
      if (sceneGraph == null || !SetupBegin(renderContext))
        return;

      m_context = renderContext;

      if (visitChildren)
      {
        sceneGraph.AcceptVisitor(this, true);
      }
      else
      {
        (this as ISpatialVisitor).Visit(sceneGraph);
      }

      m_context = null;
      m_geomDebugger.End();

      //Restore alpha
      Color bColor = m_geomDebugger.BoundingColor;
      bColor.A = m_savedAlpha;
      m_geomDebugger.BoundingColor = bColor;
    }

    private bool SetupBegin(IRenderContext renderContext)
    {
      if (renderContext == null || m_flags == DebugView.None)
        return false;

      BlendState bs = null;
      RasterizerState rs = null;
      DepthStencilState ds = null;
      Color bColor = m_geomDebugger.BoundingColor;
      m_savedAlpha = bColor.A;

      if (m_drawTransparent)
      {
        bs = BlendState.AlphaBlendNonPremultiplied;
        rs = RasterizerState.CullBackClockwiseFront;
        ds = DepthStencilState.None;

        bColor.A = 64;
        m_geomDebugger.BoundingColor = bColor;
      }
      else
      {
        bColor.A = m_savedAlpha;
        m_geomDebugger.BoundingColor = bColor;
      }

      m_context = null;
      m_geomDebugger.Begin(renderContext, bs, ds, rs);
      return true;
    }

    bool ISpatialVisitor.Visit(Spatial spatial)
    {
      //Do visibility checking
      BoundingVolume bv = spatial.WorldBounding;
      if (bv == null)
        return false;

      bool isNode = spatial is Node;
      bool wantBounds = true;
      if (isNode && m_drawOnlyLeaf)
        wantBounds = false;

      if (m_context.Camera.Frustum.Contains(bv) == ContainmentType.Outside)
        return false;

      if (wantBounds && (m_flags & DebugView.BoundingVolumes) == DebugView.BoundingVolumes)
      {
        m_geomDebugger.DrawBoundingVolume(spatial.WorldBounding);
      }

      if (spatial is IMeshDataContainer)
      {
        MeshData md = (spatial as IMeshDataContainer).MeshData;

        if ((m_flags & DebugView.Normals) == DebugView.Normals)
        {
          m_geomDebugger.DrawNormals(md, spatial.WorldMatrix, bv);
        }
        else if ((m_flags & DebugView.TangentBasis) == DebugView.TangentBasis)
        {
          m_geomDebugger.DrawTangentBasis(md, spatial.WorldMatrix, bv);
        }
      }

      return true;
    }
  }
}
