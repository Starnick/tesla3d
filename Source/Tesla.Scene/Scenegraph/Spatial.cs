﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using Tesla.Content;
using Tesla.Graphics;
using Tesla.Utilities;

namespace Tesla.Scene
{
  [SavableVersion(1)]
  public abstract class Spatial : ISpatial, ISavable, IHintable
  {
    private static CopyContext s_nullCopyContext = new CopyContext();

    private String m_name;
    private Node m_parent;
    private DirtyMark m_dirtyMark;
    private Transform m_localTransform;
    private Transform m_worldTransform;
    private BoundingVolume m_worldBounding;
    private SceneHints m_sceneHints;
    private LightCollection m_localLights;
    private ExtendedPropertiesCollection m_extendedProperties;

    public String Name
    {
      get
      {
        return m_name;
      }
      set
      {
        m_name = value;
      }
    }

    public Node Parent
    {
      get
      {
        return m_parent;
      }
    }

    public Transform Transform
    {
      get
      {
        return m_localTransform;
      }
      set
      {
        if (value == null)
          m_localTransform.SetIdentity();

        m_localTransform = value;
        PropagateDirtyDown(DirtyMark.All);
        PropagateDirtyUp(DirtyMark.Bounding);
      }
    }

    public Vector3 Scale
    {
      get
      {
        return m_localTransform.Scale;
      }
      set
      {
        m_localTransform.SetScale(value);
        PropagateDirtyDown(DirtyMark.All);
        PropagateDirtyUp(DirtyMark.Bounding);
      }
    }

    public Quaternion Rotation
    {
      get
      {
        return m_localTransform.Rotation;
      }
      set
      {
        m_localTransform.SetRotation(value);
        PropagateDirtyDown(DirtyMark.All);
        PropagateDirtyUp(DirtyMark.Bounding);
      }
    }

    public Vector3 Translation
    {
      get
      {
        return m_localTransform.Translation;
      }
      set
      {
        m_localTransform.SetTranslation(value);
        PropagateDirtyDown(DirtyMark.All);
        PropagateDirtyUp(DirtyMark.Bounding);
      }
    }

    public Transform WorldTransform
    {
      get
      {
        return m_worldTransform;
      }
    }

    public Vector3 WorldScale
    {
      get
      {
        return m_worldTransform.Scale;
      }
    }

    public Quaternion WorldRotation
    {
      get
      {
        return m_worldTransform.Rotation;
      }
    }

    public Vector3 WorldTranslation
    {
      get
      {
        return m_worldTransform.Translation;
      }
    }

    public Matrix WorldMatrix
    {
      get
      {
        return m_worldTransform.Matrix;
      }
    }

    public BoundingVolume WorldBounding
    {
      get
      {
        return m_worldBounding;
      }
    }

    public IHintable ParentHintable
    {
      get
      {
        return m_parent;
      }
    }

    public SceneHints SceneHints
    {
      get
      {
        return m_sceneHints;
      }
    }

    public LightCollection Lights
    {
      get
      {
        return m_localLights;
      }
    }

    public ExtendedPropertiesCollection ExtendedProperties
    {
      get
      {
        return m_extendedProperties;
      }
    }

    protected Spatial() { }

    public Spatial(String name)
    {
      m_name = name;
      m_parent = null;
      m_localTransform = new Transform();
      m_worldTransform = new Transform();
      m_dirtyMark = DirtyMark.All;
      m_sceneHints = new SceneHints(this);
      m_localLights = new LightCollection();
      m_localLights.CollectionModified += LocalLights_CollectionModified;
      m_extendedProperties = new ExtendedPropertiesCollection();
    }

    #region Visitor

    public virtual bool AcceptVisitor(ISpatialVisitor visitor, bool preexecute = true)
    {
      if (visitor != null)
        return visitor.Visit(this);

      return true;
    }

    public virtual bool AcceptVisitor(Func<Spatial, bool> visitor, bool preexecute = true)
    {
      if (visitor != null)
        return visitor(this);

      return true;
    }

    #endregion

    #region Transform

    public void SetScale(float scale)
    {
      m_localTransform.SetScale(scale);
      PropagateDirtyDown(DirtyMark.All);
      PropagateDirtyUp(DirtyMark.Bounding);
    }

    public void SetScale(in Vector3 scale)
    {
      m_localTransform.SetScale(scale);
      PropagateDirtyDown(DirtyMark.All);
      PropagateDirtyUp(DirtyMark.Bounding);
    }

    public void SetTranslation(float x, float y, float z)
    {
      m_localTransform.SetTranslation(x, y, z);
      PropagateDirtyDown(DirtyMark.All);
      PropagateDirtyUp(DirtyMark.Bounding);
    }

    public void SetTranslation(in Vector3 translation)
    {
      m_localTransform.SetTranslation(translation);
      PropagateDirtyDown(DirtyMark.All);
      PropagateDirtyUp(DirtyMark.Bounding);
    }

    public void SetRotation(in Matrix matrix)
    {
      m_localTransform.SetRotation(matrix);
      PropagateDirtyDown(DirtyMark.All);
      PropagateDirtyUp(DirtyMark.Bounding);
    }

    public void SetRotation(in Quaternion rotation)
    {
      m_localTransform.SetRotation(rotation);
      PropagateDirtyDown(DirtyMark.All);
      PropagateDirtyUp(DirtyMark.Bounding);
    }

    #endregion

    #region Dirty Notification

    public void MarkDirty(DirtyMark flag)
    {
      m_dirtyMark |= flag;
    }

    public void ClearDirty(DirtyMark flag)
    {
      m_dirtyMark &= ~flag;
    }

    public bool IsDirty(DirtyMark flag)
    {
      return (m_dirtyMark & flag) == flag;
    }

    public virtual void PropagateDirtyUp(DirtyMark flag)
    {
      m_dirtyMark |= flag;
      if (m_parent != null)
        m_parent.PropagateDirtyUp(flag);
    }

    public virtual void PropagateDirtyDown(DirtyMark flag)
    {
      m_dirtyMark |= flag;
    }

    #endregion

    #region Update

    public void Update(IGameTime time)
    {
      Update(time, true);
    }

    public virtual void Update(IGameTime time, bool initiator)
    {
      if (IsDirty(DirtyMark.Transform))
        UpdateWorldTransform(false);

      if (IsDirty(DirtyMark.Lighting))
        UpdateWorldLights(false);

      if (IsDirty(DirtyMark.Bounding))
      {
        UpdateWorldBound(false);
        if (initiator)
          PropagateBoundToRoot();
      }
    }

    public virtual void UpdateWorldTransform(bool recurse)
    {
      m_worldTransform.Set(m_localTransform);

      if (m_parent != null)
        m_worldTransform.CombineWithParent(m_parent.WorldTransform);

      ClearDirty(DirtyMark.Transform);
    }

    public abstract void UpdateWorldBound(bool recurse);
    public abstract void UpdateWorldLights(bool recurse);

    public void PropagateBoundToRoot()
    {
      if (m_parent != null)
      {
        m_parent.UpdateWorldBound(false);
        m_parent.PropagateBoundToRoot();
      }
    }

    #endregion

    #region Process visible set

    public void ProcessVisibleSet(IRenderer renderer)
    {
      ProcessVisibleSet(renderer, false);
    }

    public virtual void ProcessVisibleSet(IRenderer renderer, bool skipCullCheck)
    {
      //Will be either Always, Never, or Dynamic
      CullHint hint = m_sceneHints.CullHint;

      //If always call, immediately return
      if (hint == CullHint.Always)
        return;

      ContainmentType frustumIntersect = ContainmentType.Inside;

      //Do frustum check - unless if no world bounding, or if we're skipping the check for any reason
      if (!skipCullCheck && hint != CullHint.Never && m_worldBounding != null)
      {
        Camera cam = renderer.RenderContext.Camera;
        frustumIntersect = cam.Frustum.Contains(m_worldBounding);
      }

      //If we're not outside, then continue drawing
      if (frustumIntersect != ContainmentType.Outside)
        OnProcessVisibleSet(renderer);
    }

    protected virtual void OnProcessVisibleSet(IRenderer renderer) { }

    #endregion

    #region Misc

    public virtual bool FindPicks(PickQuery query)
    {
      if (query == null)
        return false;

      //If pickable, the query will do a bounding intersection test first so no need to do it here
      IPickable pickable = this as IPickable;
      if (pickable != null)
        return query.AddPick(pickable);

      return false;
    }

    public Spatial GetAncestorNamed(String name)
    {
      return GetAncestorNamed<Spatial>(name);
    }

    public T GetAncestorNamed<T>(String name) where T : Spatial
    {
      if (String.IsNullOrEmpty(name))
        return null;

      if (m_parent != null)
      {
        if (m_parent.Name.Equals(name) && m_parent is T)
          return m_parent as T;

        return m_parent.GetAncestorNamed<T>(name);
      }

      return null;
    }

    public Spatial GetDescendentNamed(String name)
    {
      return GetDescendentNamed<Spatial>(name);
    }

    public virtual T GetDescendentNamed<T>(String name) where T : Spatial
    {
      return null; //No children
    }

    public bool RemoveFromParent()
    {
      if (m_parent != null)
      {
        m_parent.Children.Remove(this);
        m_parent = null;
        return true;
      }

      return false;
    }

    internal void AttachToParent(Node newParent)
    {
      RemoveFromParent();
      m_parent = newParent;
    }

    internal void SetParent(Node newParent)
    {
      m_parent = newParent;
    }

    public Spatial Clone(CopyContext copyContext = null)
    {
      Spatial clone = SmartActivator.CreateInstance(GetType()) as Spatial;
      if (clone == null)
        return null;

      PopulateClone(clone, (copyContext == null) ? s_nullCopyContext : copyContext);

      return clone;
    }

    public void MakeInstanced(MaterialDefinition instanceMatDef, params IInstanceDataProvider[] instanceDataProviders)
    {
      MakeInstanced(instanceMatDef, true, instanceDataProviders);
    }

    public void MakeNonInstanced(MaterialDefinition newMatDef)
    {
      MakeNonInstanced(newMatDef, true);
    }

    public void SetMaterialDefinition(MaterialDefinition newMatDef)
    {
      SetMaterialDefinition(newMatDef, true);
    }

    public void SetModelBounding(BoundingVolume volume)
    {
      SetModelBounding(volume, true);
    }

    public abstract void MakeInstanced(MaterialDefinition instanceMatDef, bool copyMatDef, params IInstanceDataProvider[] instanceDataProviders);
    public abstract void MakeNonInstanced(MaterialDefinition newMatDef, bool copyMatDef);
    public abstract void SetMaterialDefinition(MaterialDefinition newMatDef, bool copyMatDef);
    public abstract void SetModelBounding(BoundingVolume volume, bool calculateBounds);
    public abstract void SortLights();

    protected void CollectLights(LightCollection lights)
    {
      lights.AddRange(m_localLights);

      if (m_parent != null)
        m_parent.CollectLights(lights);
    }

    protected void RemoveWorldBounding()
    {
      m_worldBounding = null;
    }

    protected void SetWorldBounding(BoundingVolume volume)
    {
      m_worldBounding = volume;
    }

    protected virtual void PopulateClone(Spatial clone, CopyContext copyContext)
    {
      clone.m_name = m_name;
      clone.m_parent = null;
      clone.m_localTransform = new Transform(m_localTransform);
      clone.m_worldTransform = new Transform(m_worldTransform);
      clone.m_dirtyMark = DirtyMark.None; //Copying EVERYTHING, so there shouldn't be a need to update
      clone.m_sceneHints = new SceneHints(clone, m_sceneHints);
      clone.m_localLights = new LightCollection(m_localLights);
      clone.m_localLights.CollectionModified += clone.LocalLights_CollectionModified;
    }

    private void LocalLights_CollectionModified(LightCollection sender, EventArgs args)
    {
      PropagateDirtyDown(DirtyMark.Lighting);
    }

    #endregion

    public virtual void Read(ISavableReader input)
    {
      m_name = input.ReadString("Name");
      m_localTransform = input.ReadSavable<Transform>("LocalTransform");
      m_sceneHints = input.ReadSavable<SceneHints>("SceneHints");
      m_sceneHints.SetSource(this);
      m_localLights = input.ReadSavable<LightCollection>("LocalLights");
      m_localLights.CollectionModified += LocalLights_CollectionModified;

      m_worldTransform = new Transform(m_localTransform);
      m_parent = null;
      m_dirtyMark = DirtyMark.All;

      ReadExtendedProperties(input);
    }

    public virtual void Write(ISavableWriter output)
    {
      output.Write("Name", m_name);
      output.WriteSavable<Transform>("LocalTransform", m_localTransform);
      output.WriteSavable<SceneHints>("SceneHints", m_sceneHints);
      output.WriteSavable<LightCollection>("LocalLights", m_localLights);

      WriteExtendedProperties(output);
    }

    public override String ToString()
    {
      if (!String.IsNullOrEmpty(m_name))
        return String.Format("{0}: {1}", GetType().Name, m_name);

      return base.ToString();
    }

    private void ReadExtendedProperties(ISavableReader input)
    {
      int extendedPropCount = input.BeginReadGroup("ExtendedProperties");
      m_extendedProperties = new ExtendedPropertiesCollection(extendedPropCount);

      for (int i = 0; i < extendedPropCount; i++)
      {
        input.BeginReadGroup("Property");

        int key = input.ReadInt32("Key");
        ISavable prop = input.ReadSavable<ISavable>("Value");
        m_extendedProperties.Add(key, prop);

        input.EndReadGroup();
      }

      input.EndReadGroup();
    }

    private void WriteExtendedProperties(ISavableWriter output)
    {
      int extendedSavablePropCount = m_extendedProperties.GetCountOf<ISavable>();

      output.BeginWriteGroup("ExtendedProperties", extendedSavablePropCount);

      foreach (KeyValuePair<int, Object> kv in m_extendedProperties)
      {
        if (kv.Value is ISavable)
        {
          output.BeginWriteGroup("Property");

          output.Write("Key", kv.Key);
          output.WriteSavable<ISavable>("Value", kv.Value as ISavable);

          output.EndWriteGroup();
        }
      }

      output.EndWriteGroup();
    }
  }
}
