﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using Tesla.Graphics;
using Tesla.Content;

namespace Tesla.Scene
{
  [SavableVersion(1)]
  public class Mesh : Visual, IInstancedRenderable, IMeshDataContainer, IPickable
  {
    private MeshData m_meshData;
    private SubMeshRange? m_meshRange;
    private InstanceDefinition m_instanceDefinition;

    public MeshData MeshData
    {
      get
      {
        return m_meshData;
      }
      set
      {
        if (m_instanceDefinition == null)
          m_meshData = value;
      }
    }

    public SubMeshRange? MeshRange
    {
      get
      {
        return m_meshRange;
      }
      set
      {
        if (m_instanceDefinition == null)
          m_meshRange = value;
      }
    }

    public InstanceDefinition InstanceDefinition
    {
      get
      {
        return m_instanceDefinition;
      }
    }

    public bool IsInstanced
    {
      get
      {
        return m_instanceDefinition != null;
      }
    }

    public override bool IsValidForDraw
    {
      get
      {
        if (m_instanceDefinition != null && m_instanceDefinition.IsValidForDraw)
          return true;

        bool invalid = !IsMaterialValid || m_meshData == null || m_meshData.VertexBuffer == null || m_meshData.VertexBuffer.IsDisposed
            || (m_meshData.UseIndexedPrimitives && (m_meshData.IndexBuffer == null || m_meshData.IndexBuffer.IsDisposed));

        return !invalid;
      }
    }

    protected Mesh() { }

    public Mesh(String name) : this(name, null) { }

    public Mesh(String name, MeshData meshData)
        : base(name)
    {
      m_meshData = (meshData == null) ? new MeshData() : meshData;
    }

    public void MakeInstanced(InstanceDefinition instanceDef)
    {
      if (instanceDef == null)
        return;

      if (m_instanceDefinition != null)
        m_instanceDefinition.RemoveInstance(this);

      //SetInstancedDefinition will be called
      instanceDef.AddInstance(this);
    }

    public override void MakeInstanced(MaterialDefinition instanceMatDef, bool copyMatDef, params IInstanceDataProvider[] instanceDataProviders)
    {
      if (copyMatDef && instanceMatDef != null)
        instanceMatDef = instanceMatDef.Clone();

      InstanceDefinition instanceDef = new InstanceDefinition(instanceMatDef, m_meshData);

      if (instanceDataProviders != null)
      {
        for (int i = 0; i < instanceDataProviders.Length; i++)
          instanceDef.AddInstanceData(instanceDataProviders[i]);
      }

      //SetInstancedDefinition will be called
      instanceDef.AddInstance(this);
    }

    public override void MakeNonInstanced(MaterialDefinition newMatDef, bool copyMatDef)
    {
      if (m_instanceDefinition != null)
        m_instanceDefinition.RemoveInstance(this);

      SetMaterialDefinition(newMatDef, copyMatDef);
    }

    protected override void OnProcessVisibleSet(IRenderer renderer)
    {
      if (IsInstanced)
      {
        m_instanceDefinition.NotifyToDraw(renderer, this);
      }
      else
      {
        //If no material, should not be drawn at all
        if (!IsMaterialValid)
          return;

        renderer.Process(this);
      }
    }

    public override void SetupDrawCall(IRenderContext renderContext, RenderBucketID currentBucketID, MaterialPass currentPass)
    {
      renderContext.SetVertexBuffer(m_meshData.VertexBuffer);

      int offset;
      int count;
      int baseVertexOffset;
      GraphicsHelper.GetMeshDrawParameters(m_meshData, m_meshRange, out offset, out count, out baseVertexOffset);

      if (m_meshData.UseIndexedPrimitives)
      {
        renderContext.SetIndexBuffer(m_meshData.IndexBuffer);
        renderContext.DrawIndexed(m_meshData.PrimitiveType, count, offset, baseVertexOffset);
      }
      else
      {
        renderContext.Draw(m_meshData.PrimitiveType, count, offset);
      }
    }

    public override void UpdateModelBounding()
    {
      BoundingVolume modelBounding = ModelBounding;

      if (m_meshData is null || modelBounding is null)
        return;

      if (m_meshData.UseIndexedPrimitives)
      {
        DataBuffer<Vector3> pos = m_meshData.Positions;
        IndexData? indices = m_meshData.Indices;

        if (pos != null && indices.HasValue)
          modelBounding.ComputeFromIndexedPoints(pos, indices.Value, m_meshRange);
      }
      else
      {
        DataBuffer<Vector3> pos = m_meshData.Positions;
        if (pos is not null)
          modelBounding.ComputeFromPoints(pos, m_meshRange);
      }

      PropagateDirtyUp(DirtyMark.Bounding);
    }

    public override void Read(ISavableReader input)
    {
      base.Read(input);

      m_meshData = input.ReadSharedSavable<MeshData>("MeshData");
      input.ReadNullable<SubMeshRange>("MeshRange", out m_meshRange);
      m_instanceDefinition = input.ReadSharedSavable<InstanceDefinition>("InstanceDefinition");
    }

    public override void Write(ISavableWriter output)
    {
      base.Write(output);

      output.WriteSharedSavable<MeshData>("MeshData", m_meshData);
      output.Write<SubMeshRange>("MeshRange", m_meshRange);
      output.WriteSharedSavable<InstanceDefinition>("InstanceDefinition", m_instanceDefinition);
    }

    protected override void PopulateClone(Spatial clone, CopyContext copyContext)
    {
      base.PopulateClone(clone, copyContext);

      Mesh msh = clone as Mesh;
      if (msh == null)
        return;

      //If instanced...add the new instance
      if (m_instanceDefinition != null)
      {
        m_instanceDefinition.AddInstance(msh);
      }
      else
      {
        //Copy mesh data if want to, else just share it
        if (copyContext[CopyContext.CopyMeshDataKey])
        {
          msh.m_meshData = m_meshData.Clone();
        }
        else
        {
          msh.m_meshData = m_meshData;
        }

        msh.m_meshRange = m_meshRange;
      }
    }

    #region IInstancedRenderable

    bool IInstancedRenderable.SetInstanceDefinition(InstanceDefinition instanceDef)
    {
      m_instanceDefinition = instanceDef;
      m_meshData = instanceDef.MeshData;
      m_meshRange = instanceDef.MeshRange;
      MaterialDefinition = null; //If being instanced now, set the material to null

      return true;
    }

    void IInstancedRenderable.RemoveInstanceDefinition()
    {
      m_instanceDefinition = null;
      m_meshData = null;
      m_meshRange = null;
    }

    public bool IntersectsMesh(in Ray ray, IRefList<Pair<LineIntersectionResult, Triangle?>> results, bool ignoreBackfaces)
    {
      if (m_meshData == null)
        return false;

      return m_meshData.Intersects(ray, WorldTransform.Matrix, m_meshRange, results, ignoreBackfaces);
    }

    #endregion
  }
}
