﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;

namespace Tesla.Scene
{
  /// <summary>
  /// Context for copying scenegraph information. It contains a collection of parameters, each represented by
  /// a key, instructing the Spatials of how to copy data.
  /// </summary>
  public class CopyContext
  {
    /// <summary>
    /// Do a deep copy of materials or share them.
    /// </summary>
    public static readonly String CopyMaterialKey = "CopyMaterial";

    /// <summary>
    /// Do a deep copy of mesh data or shares them.
    /// </summary>
    public static readonly String CopyMeshDataKey = "CopyMeshData";

    private Dictionary<String, bool> m_properties;

    /// <summary>
    /// Gets a context that instructs Visuals to clone material instances. Default is to share.
    /// </summary>
    public static CopyContext CopyMaterialContext
    {
      get
      {
        CopyContext c = new CopyContext();
        c[CopyMaterialKey] = true;

        return c;
      }
    }

    /// <summary>
    /// Gets a context that instructs Meshes to clone mesh data instances. Default is to share.
    /// </summary>
    public static CopyContext CopyMeshDataContext
    {
      get
      {
        CopyContext c = new CopyContext();
        c[CopyMeshDataKey] = true;

        return c;
      }
    }

    /// <summary>
    /// Gets a context that instructs Visuals and Meshes to clone materials and mesh data instances. Default is to share.
    /// </summary>
    public static CopyContext CopyMaterialMeshDataContext
    {
      get
      {
        CopyContext c = new CopyContext();
        c[CopyMaterialKey] = true;
        c[CopyMeshDataKey] = true;

        return c;
      }
    }

    /// <summary>
    /// Gets the copy parameter by its key.
    /// </summary>
    /// <param name="key">Key of the copy parameter.</param>
    /// <returns>Copy parameter, or false if the key was not found.</returns>
    public bool this[String key]
    {
      get
      {
        if (String.IsNullOrEmpty(key))
          return false;

        bool value;
        if (m_properties.TryGetValue(key, out value))
          return value;

        return false;
      }
      set
      {
        if (String.IsNullOrEmpty(key))
          return;

        m_properties[key] = value;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="CopyContext"/> class.
    /// </summary>
    public CopyContext()
    {
      m_properties = new Dictionary<string, bool>();
    }

    /// <summary>
    /// Tries to get a copy parameter by its key.
    /// </summary>
    /// <param name="key">Key of the copy parameter.</param>
    /// <param name="value">Copy parameter, or false if it was not found.</param>
    /// <returns>True if the copy parameter was found, false otherwise.</returns>
    public bool TryGetValue(String key, out bool value)
    {
      if (String.IsNullOrEmpty(key))
      {
        value = false;
        return false;
      }

      if (m_properties.TryGetValue(key, out value))
        return true;

      return false;
    }
  }
}
