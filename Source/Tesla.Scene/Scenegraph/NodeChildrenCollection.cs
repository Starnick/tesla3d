﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Runtime.InteropServices;
using System.Collections;
using System.Collections.Generic;

namespace Tesla.Scene
{
  public class NodeChildrenCollection : IList<Spatial>, IReadOnlyList<Spatial>
  {
    private Node m_parent;
    private List<Spatial> m_children;
    private int m_version;
    private bool m_suspendPropogateDirty = false;

    public Node Parent
    {
      get
      {
        return m_parent;
      }
    }

    public int Capacity
    {
      get
      {
        return m_children.Capacity;
      }
      set
      {
        m_children.Capacity = value;
      }
    }

    public int Count
    {
      get
      {
        return m_children.Count;
      }
    }

    public Spatial this[int index]
    {
      get
      {
        if (index < 0 || index >= m_children.Count)
          return null;

        return m_children[index];
      }
      set
      {
        if (index < 0 || index >= m_children.Count)
          return;

        if (value == null)
        {
          RemoveAt(index);
          return;
        }

        Spatial old = m_children[index];
        old.SetParent(null);
        value.AttachToParent(m_parent);
        m_children[index] = value;

        m_parent.PropagateDirtyDown(DirtyMark.All);
        m_parent.PropagateDirtyUp(DirtyMark.Bounding);
        m_version++;
      }
    }

    public Spatial this[String name]
    {
      get
      {
        if (String.IsNullOrEmpty(name))
          return null;

        for (int i = 0; i < m_children.Count; i++)
        {
          Spatial child = m_children[i];
          if (name.Equals(child.Name))
            return child;
        }

        return null;
      }
    }

    public bool IsReadOnly
    {
      get
      {
        return false;
      }
    }

    internal bool SuspendPropogateDirty
    {
      get
      {
        return m_suspendPropogateDirty;
      }
      set
      {
        m_suspendPropogateDirty = value;
      }
    }

    public NodeChildrenCollection(Node parent)
    {
      if (parent == null)
        throw new ArgumentNullException("parent");

      m_parent = parent;
      m_children = new List<Spatial>();
    }

    public NodeChildrenCollection(Node parent, int initialCapacity)
    {
      if (parent == null)
        throw new ArgumentNullException("parent");

      m_parent = parent;
      m_children = new List<Spatial>(initialCapacity);
    }

    public void Add(Spatial spatial)
    {
      if (spatial == null || spatial.Parent == m_parent)
        return;

      spatial.AttachToParent(m_parent);
      m_children.Add(spatial);

      if (!m_suspendPropogateDirty)
      {
        m_parent.PropagateDirtyDown(DirtyMark.All);
        m_parent.PropagateDirtyUp(DirtyMark.Bounding);
      }

      m_version++;
    }

    public void AddRange(IEnumerable<Spatial> spatials)
    {
      if (spatials == null)
        return;

      bool addedOne = false;

      foreach (Spatial s in spatials)
      {
        if (s != null && s.Parent != m_parent)
        {
          s.AttachToParent(m_parent);
          m_children.Add(s);
          addedOne = true;
        }
      }

      if (addedOne)
      {
        if (!m_suspendPropogateDirty)
        {
          m_parent.PropagateDirtyDown(DirtyMark.All);
          m_parent.PropagateDirtyUp(DirtyMark.Bounding);
        }

        m_version++;
      }
    }

    public int IndexOf(Spatial spatial)
    {
      if (spatial == null || spatial.Parent != m_parent)
        return -1;

      for (int i = 0; i < m_children.Count; i++)
      {
        if (m_children[i] == spatial)
          return i;
      }

      return -1;
    }

    public bool Remove(Spatial spatial)
    {
      if (spatial == null || spatial.Parent != m_parent)
        return false;

      for (int i = 0; i < m_children.Count; i++)
      {
        Spatial child = m_children[i];
        if (child == spatial)
        {
          child.SetParent(null);
          m_children.RemoveAt(i);

          if (!m_suspendPropogateDirty)
          {
            m_parent.PropagateDirtyUp(DirtyMark.Bounding);
          }

          m_version++;
          return true;
        }
      }

      return false;
    }

    public bool Remove(String name)
    {
      if (String.IsNullOrEmpty(name))
        return false;

      for (int i = 0; i < m_children.Count; i++)
      {
        Spatial child = m_children[i];
        if (name.Equals(child.Name))
        {
          child.SetParent(null);
          m_children.RemoveAt(i);

          if (!m_suspendPropogateDirty)
          {
            m_parent.PropagateDirtyUp(DirtyMark.Bounding);
          }

          m_version++;
          return true;
        }
      }

      return false;
    }

    public void RemoveAt(int index)
    {
      if (index < 0 || index >= m_children.Count)
        return;

      Spatial child = m_children[index];
      child.SetParent(null);
      m_children.RemoveAt(index);

      if (!m_suspendPropogateDirty)
      {
        m_parent.PropagateDirtyUp(DirtyMark.Bounding);
      }

      m_version++;
    }

    public void Clear()
    {
      for (int i = 0; i < m_children.Count; i++)
        m_children[i].SetParent(null);

      m_children.Clear();
      m_version++;
    }

    public void Insert(int index, Spatial item)
    {
      if (index < 0 || item == null || item.Parent == m_parent)
        return;

      item.AttachToParent(m_parent);
      m_children.Insert(index, item);

      if (!m_suspendPropogateDirty)
      {
        m_parent.PropagateDirtyDown(DirtyMark.All);
        m_parent.PropagateDirtyUp(DirtyMark.Bounding);
      }

      m_version++;
    }

    public bool Contains(Spatial item)
    {
      if (item == null)
        return false;

      if (item.Parent == m_parent)
        return true;

      for (int i = 0; i < m_children.Count; i++)
      {
        if (m_children[i] == item)
          return true;
      }

      return false;
    }

    public Spatial[] ToArray()
    {
      return m_children.ToArray();
    }

    public void CopyTo(Spatial[] array)
    {
      m_children.CopyTo(array);
    }

    public void CopyTo(Spatial[] array, int arrayIndex)
    {
      m_children.CopyTo(array, arrayIndex);
    }

    public void Sort(IComparer<Spatial> comparer)
    {
      m_children.Sort(comparer);
    }

    public NodeChildrenEnumerator GetEnumerator()
    {
      return new NodeChildrenEnumerator(this);
    }

    IEnumerator<Spatial> IEnumerable<Spatial>.GetEnumerator()
    {
      return new NodeChildrenEnumerator(this);
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return new NodeChildrenEnumerator(this);
    }

    #region Enumerator

    [Serializable, StructLayout(LayoutKind.Sequential)]
    public struct NodeChildrenEnumerator : IEnumerator<Spatial>
    {
      private NodeChildrenCollection m_collection;
      private List<Spatial> m_children;
      private int m_index;
      private int m_version;
      private Spatial m_current;

      public Spatial Current
      {
        get
        {
          return m_current;
        }
      }

      Object IEnumerator.Current
      {
        get
        {
          return m_current;
        }
      }

      internal NodeChildrenEnumerator(NodeChildrenCollection children)
      {
        m_collection = children;
        m_children = children.m_children;
        m_index = 0;
        m_version = children.m_version;
        m_current = null;
      }

      public bool MoveNext()
      {
        ThrowIfChanged();

        if (m_index < m_children.Count)
        {
          m_current = m_children[m_index];
          m_index++;
          return true;
        }
        else
        {
          m_index++;
          m_current = null;
          return false;
        }
      }

      public void Reset()
      {
        ThrowIfChanged();
        m_index = 0;
        m_current = null;
      }

      public void Dispose() { }

      private void ThrowIfChanged()
      {
        if (m_version != m_collection.m_version)
          throw new InvalidOperationException(StringLocalizer.Instance.GetLocalizedString("CollectionModifiedDuringEnumeration"));
      }
    }

    #endregion
  }
}
