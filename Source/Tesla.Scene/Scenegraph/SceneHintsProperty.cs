﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Graphics;

namespace Tesla.Scene
{
  /// <summary>
  /// Render property for a <see cref="SceneHints"/> that represents various hints used when an object is rendered.
  /// </summary>
  public sealed class SceneHintsProperty : RenderProperty<SceneHints>
  {
    /// <summary>
    /// Unique ID for this render property.
    /// </summary>
    public static RenderPropertyID PropertyID = GetPropertyID<SceneHintsProperty>();

    /// <summary>
    /// Constructs a new instance of the <see cref="SceneHintsProperty"/> class.
    /// </summary>
    /// <param name="hints">Scene hints.</param>
    public SceneHintsProperty(SceneHints hints) : base(PropertyID, hints)
    {
      if (hints == null)
        throw new ArgumentNullException("hints");
    }
  }
}
