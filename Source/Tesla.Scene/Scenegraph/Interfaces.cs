﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using Tesla.Graphics;

namespace Tesla.Scene
{
  /// <summary>
  /// Defines an object that visists each spatial in the scene graph.
  /// </summary>
  public interface ISpatialVisitor
  {
    /// <summary>
    /// Visists the spatial.
    /// </summary>
    /// <param name="spatial">Spatial to visit.</param>
    /// <returns>True if to keep visiting the subsequent spatials, false to stop processing.</returns>
    bool Visit(Spatial spatial);
  }

  /// <summary>
  /// Represents an object that provides hints, local and absolute, that provide context to how to update/render a scene. Each hint
  /// can be set to inherit, deferring to whatever its parent does (or the default).
  /// </summary>
  public interface IHintable
  {
    /// <summary>
    /// Gets the parent of this hintable.
    /// </summary>
    IHintable ParentHintable { get; }

    /// <summary>
    /// Gets the scene hints bound to this hintable.
    /// </summary>
    SceneHints SceneHints { get; }
  }
}
