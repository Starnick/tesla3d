﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesla.Graphics;

namespace Tesla.Scene
{
  public class LightComparer : IComparer<Light>
  {
    private Spatial m_spatial;

    public LightComparer(Spatial spatial)
    {
      m_spatial = spatial;
    }

    public int Compare(Light x, Light y)
    {
      float first = GetLightValue(x);
      float second = GetLightValue(y);

      if (first < second)
      {
        return -1;
      }
      else if (first > second)
      {
        return 1;
      }

      return 0;
    }

    private float GetLightValue(Light l)
    {
      if (!l.IsEnabled)
        return 0;

      switch (l.LightType)
      {
        case LightType.Directional:
          return GetColorStrength(l);
        case LightType.Point:
          return GetPointValue(l as PointLight);
        case LightType.Spot:
          return GetSpotValue(l as SpotLight);
        default:
          return 0;
      }
    }

    private float GetPointValue(PointLight pl)
    {
      if (!pl.Attenuate)
        return GetColorStrength(pl);

      Vector3 pos = pl.Position;
      float strength = GetColorStrength(pl);

      Vector3 posOfObject;
      BoundingVolume bv = m_spatial.WorldBounding;
      if (bv == null)
      {
        posOfObject = m_spatial.WorldTransform.Translation;
      }
      else
      {
        posOfObject = bv.Center;
      }

      float attenuation = Light.ComputeAttenuationFactor(posOfObject, pos, pl.Range);
      if (MathHelper.IsNearlyZero(attenuation))
        return 0;

      return strength / attenuation;
    }

    private float GetSpotValue(SpotLight sl)
    {
      BoundingVolume bv = m_spatial.WorldBounding;
      if (bv == null)
        return 0;

      Vector3 dir = sl.Direction;
      Vector3 pos = sl.Position;
      Plane plane = new Plane(dir, pos);

      if (bv.Intersects(plane) == PlaneIntersectionType.Back)
      {
        if (!sl.Attenuate)
          return GetColorStrength(sl);

        float strength = GetColorStrength(sl);
        Vector3 center = bv.Center;

        float attenuation = Light.ComputeAttenuationFactor(center, pos, sl.Range);
        if (MathHelper.IsNearlyZero(attenuation))
          return 0;

        return strength / attenuation;
      }

      return 0;
    }

    private float GetColorStrength(Light l)
    {
      return Strength(l.Ambient) + Strength(l.Diffuse);
    }

    private float Strength(Color c)
    {
      float r = c.R;
      float g = c.G;
      float b = c.B;

      return MathF.Sqrt(r * r + g * g + b * b);
    }
  }
}
