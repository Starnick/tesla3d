﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using SDXMI = SharpDX.Mathematics.Interop;
using X2 = SharpDX.XAudio2;
using SHM = SharpDX.Multimedia;

namespace Tesla.XAudio2.Audio
{
  public static class AudioHelper
  {
    public static void ConvertVector3(ref Vector3 v, out SDXMI.RawVector3 result)
    {
      result.X = v.X;
      result.Y = v.Y;
      result.Z = v.Z;
    }

    public static TimeSpan GetTimeDuration(X2.AudioBuffer audioBuffer, uint[] decodedPacketsInfo, SHM.WaveFormat format)
    {
      if (format == null)
        return TimeSpan.Zero;

      long samplesDuration = 0;

      if (audioBuffer != null)
      {
        switch (format.Encoding)
        {
          case SHM.WaveFormatEncoding.Pcm:
            samplesDuration = audioBuffer.AudioBytes * 8L / ((long) (format.BitsPerSample * format.Channels));
            break;
          case SHM.WaveFormatEncoding.Adpcm:
            SHM.WaveFormatAdpcm adpcm = format as SHM.WaveFormatAdpcm;
            samplesDuration = (audioBuffer.AudioBytes / adpcm.BlockAlign) * adpcm.SamplesPerBlock;
            long part = audioBuffer.AudioBytes % adpcm.BlockAlign;
            if (part > 0)
            {
              if (part >= (7 * adpcm.Channels))
                samplesDuration += (part * 2) / (adpcm.Channels - 12);
            }
            break;
          case SHM.WaveFormatEncoding.Wmaudio2:
          case SHM.WaveFormatEncoding.Wmaudio3:
            if (decodedPacketsInfo != null)
              samplesDuration = decodedPacketsInfo[decodedPacketsInfo.Length - 1] / format.Channels;
            break;
        }
      }

      return (format.SampleRate > 0) ? TimeSpan.FromMilliseconds((samplesDuration * 1000) / format.SampleRate) : TimeSpan.Zero;
    }

    public static void SetPanMatrix(X2.SourceVoice voice, SHM.WaveFormat voiceFormat, X2.MasteringVoice masteringVoice, SpeakerConfiguration speakers, float panValue, ref float[] panMatrix)
    {
      if (voice == null || voiceFormat == null || masteringVoice == null)
        return;

      panValue = MathHelper.Clamp(panValue, -1.0f, 1.0f);

      int srcChannelCount = voiceFormat.Channels;
      int dstChannelCount = masteringVoice.VoiceDetails.InputChannelCount;

      int length = srcChannelCount * dstChannelCount;
      if (panMatrix == null || panMatrix.Length != length)
        panMatrix = new float[length];

      //Default to full volume for all channels
      for (int i = 0; i < panMatrix.Length; i++)
        panMatrix[i] = 1.0f;

      if (panValue != 0.0f)
      {
        float panLeft = 1.0f - panValue;
        float panRight = 1.0f + panValue;

        //(srcChannelCount x dstChannel) + currSrcChannel
        for (int i = 0; i < srcChannelCount; i++)
        {
          switch (speakers)
          {
            case SpeakerConfiguration.Stereo:
            case SpeakerConfiguration.Surround:
              panMatrix[(srcChannelCount * 0) + i] = panLeft;
              panMatrix[(srcChannelCount * 1) + i] = panRight;
              break;
            case SpeakerConfiguration.Quad:
              panMatrix[(srcChannelCount * 0) + i] = panLeft;
              panMatrix[(srcChannelCount * 2) + i] = panLeft;
              panMatrix[(srcChannelCount * 1) + i] = panRight;
              panMatrix[(srcChannelCount * 3) + i] = panRight;
              break;
            case SpeakerConfiguration.FivePointOne:
            case SpeakerConfiguration.FivePointOneSurround:
            case SpeakerConfiguration.SevenPointOne:
              panMatrix[(srcChannelCount * 0) + i] = panLeft;
              panMatrix[(srcChannelCount * 4) + i] = panLeft;
              panMatrix[(srcChannelCount * 1) + i] = panRight;
              panMatrix[(srcChannelCount * 5) + i] = panRight;
              break;
            case SpeakerConfiguration.SevenPointOneSurround:
              panMatrix[(srcChannelCount * 0) + i] = panLeft;
              panMatrix[(srcChannelCount * 4) + i] = panLeft;
              panMatrix[(srcChannelCount * 6) + i] = panLeft;
              panMatrix[(srcChannelCount * 1) + i] = panRight;
              panMatrix[(srcChannelCount * 5) + i] = panRight;
              panMatrix[(srcChannelCount * 7) + i] = panRight;
              break;
            case SpeakerConfiguration.Mono:
            case SpeakerConfiguration.None:
            default:
              //Dont do any panning
              break;
          }
        }
      }

      voice.SetOutputMatrix(srcChannelCount, dstChannelCount, panMatrix);
    }
  }
}
