﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesla.Audio;
using Tesla.Audio.Implementation;
using X2 = SharpDX.XAudio2;
using X3D = SharpDX.X3DAudio;
using SHM = SharpDX.Multimedia;
using XAPO = SharpDX.XAPO;
using Tesla;
using Tesla.XAudio2.Audio.Implementation;
using Tesla.Utilities;

namespace Tesla.XAudio2.Audio
{
  public sealed class X2AudioSystem : IAudioSystem
  {
    private bool m_isDisposed;
    private X2.XAudio2 m_device;
    private X2.MasteringVoice m_masteringVoice;
    private XAPO.Fx.MasteringLimiter m_masteringLimiter;
    private SpeakerConfiguration m_speakers;

    private float m_masterVolume;
    private float m_distanceScale;
    private float m_dopplerScale;
    private float m_speedOfSound;

    private bool m_isReverbEnabled;
    private bool m_isSpatialAudioEnabled;

    private X3D.X3DAudio m_3dDevice;
    private X2.Fx.Reverb m_reverb;
    private X2.SubmixVoice m_reverbVoice;
    private X2.Fx.ReverbParameters m_reverbParams;

    private int m_currentResourceID;
    private SourceVoicePools m_voicePools;
    private ImplementationFactoryCollection m_implFactories;

    public bool IsDisposed
    {
      get
      {
        return m_isDisposed;
      }
    }

    public string Name
    {
      get
      {
        return "XAudio2_AudioSystem";
      }
    }

    public String Platform
    {
      get
      {
        return "XAudio2";
      }
    }

    public float MasterVolume
    {
      get
      {
        return m_masterVolume;
      }
      set
      {
        if (!MathHelper.IsEqual(value, m_masterVolume))
        {
          m_masterVolume = MathHelper.Clamp(value, 0.0f, 1.0f);
          if (m_masteringVoice != null)
            m_masteringVoice.SetVolume(m_masterVolume);
        }
      }
    }

    public float SpeedOfSound
    {
      get
      {
        return m_speedOfSound;
      }
      set
      {
        if (value <= 0.0f)
          value = 1.0f;

        m_speedOfSound = value;

        if (m_masteringVoice != null)
        {
          m_3dDevice = new X3D.X3DAudio((SHM.Speakers) m_speakers, m_speedOfSound);
        }
      }
    }

    public float DistanceScale
    {
      get
      {
        return m_distanceScale;
      }
      set
      {
        m_distanceScale = value;
      }
    }

    public float DopplerScale
    {
      get
      {
        return m_dopplerScale;
      }
      set
      {
        m_dopplerScale = value;
      }
    }

    public bool IsReverberationEffectEnabled
    {
      get
      {
        return m_isReverbEnabled;
      }
      set
      {
        m_isReverbEnabled = value;

        if (m_reverbVoice != null)
        {
          if (value)
          {
            m_reverbVoice.EnableEffect(0);
          }
          else
          {
            m_reverbVoice.DisableEffect(0);
          }
        }
      }
    }

    public bool IsSpatialAudioEnabled
    {
      get
      {
        return m_isSpatialAudioEnabled;
      }
      set
      {
        m_isSpatialAudioEnabled = value;
      }
    }

    public X2.XAudio2 X2Device
    {
      get
      {
        return m_device;
      }
    }

    public X3D.X3DAudio X3DDevice
    {
      get
      {
        return m_3dDevice;
      }
    }

    public X2.MasteringVoice X2MasteringVoice
    {
      get
      {
        return m_masteringVoice;
      }
    }

    public X2.SubmixVoice X2ReverbVoice
    {
      get
      {
        return m_reverbVoice;
      }
    }

    public SpeakerConfiguration Speakers
    {
      get
      {
        return m_speakers;
      }
    }

    public SourceVoicePools VoicePools
    {
      get
      {
        return m_voicePools;
      }
    }

    public X2AudioSystem()
    {
      m_isDisposed = false;
      m_masterVolume = 1.0f;
      m_distanceScale = 1.0f;
      m_dopplerScale = 1.0f;
      m_speedOfSound = 343.5f;

      try
      {
        m_device = new X2.XAudio2(X2.XAudio2Flags.None, X2.ProcessorSpecifier.AnyProcessor, X2.XAudio2Version.Version27);
        m_device.StartEngine();

      }
      catch (Exception e)
      {
        Dispose();

        EngineLog.LogException(LogLevel.Error, e);
        throw new TeslaAudioException(StringLocalizer.Instance.GetLocalizedString("ErrorCreatingDevice"), e);
      }

      if (m_device.DeviceCount == 0)
      {
        Dispose(true);

        throw new TeslaAudioException(StringLocalizer.Instance.GetLocalizedString("NoAudioDevices"));
      }

      m_masteringVoice = new X2.MasteringVoice(m_device, X2.XAudio2.DefaultChannels, X2.XAudio2.DefaultSampleRate, 0);
      m_masteringVoice.SetVolume(m_masterVolume);

      m_speakers = (SpeakerConfiguration) m_device.GetDeviceDetails(0).OutputFormat.ChannelMask;
      m_3dDevice = new X3D.X3DAudio((SHM.Speakers) m_speakers, m_speedOfSound);

      m_isSpatialAudioEnabled = true;
      m_isReverbEnabled = true;
      m_reverbParams = (X2.Fx.ReverbParameters) X2.Fx.ReverbI3DL2Parameters.Presets.Default;
      CreateReverbSubmixVoice();
      CreateMasteringLimiter();

      m_voicePools = new SourceVoicePools(m_device);

      m_implFactories = new ImplementationFactoryCollection();
      InitializeFactories();
    }

    public void SetReverberationEffect(ReverberationParameters effectParams)
    {
      X2.Fx.ReverbI3DL2Parameters revParams;
      revParams.WetDryMix = effectParams.WetDryMix;
      revParams.Room = effectParams.Room;
      revParams.RoomHF = effectParams.RoomHF;
      revParams.RoomRolloffFactor = effectParams.RoomRollOffFactor;
      revParams.DecayTime = effectParams.DecayTime;
      revParams.DecayHFRatio = effectParams.DecayHFRatio;
      revParams.Reflections = effectParams.Reflections;
      revParams.ReflectionsDelay = effectParams.ReflectionsDelay;
      revParams.Reverb = effectParams.Reverberation;
      revParams.ReverbDelay = effectParams.ReverberationDelay;
      revParams.Diffusion = effectParams.Diffusion;
      revParams.Density = effectParams.Density;
      revParams.HFReference = effectParams.HFReference;

      //Explicit conversion operator to reverb params used by XAudio2
      m_reverbParams = (X2.Fx.ReverbParameters) revParams;

      if (m_reverbVoice != null && m_reverb != null)
        m_reverbVoice.SetEffectParameters<X2.Fx.ReverbParameters>(0, m_reverbParams);
    }

    public T GetImplementationFactory<T>() where T : IAudioResourceImplFactory
    {
      return m_implFactories.GetImplementationFactory<T>();
    }

    public bool TryGetImplementationFactory<T>(out T implementationFactory) where T : IAudioResourceImplFactory
    {
      return m_implFactories.TryGetImplementationFactory<T>(out implementationFactory);
    }

    public bool IsSupported<T>() where T : AudioResource
    {
      return m_implFactories.IsSupported<T>();
    }

    internal bool AddImplementationFactory<T>(T implFactory) where T : IAudioResourceImplFactory
    {
      return m_implFactories.AddImplementationFactory<T>(implFactory);
    }

    internal bool RemoveImplementationFactory<T>(T implFactory) where T : IAudioResourceImplFactory
    {
      return m_implFactories.RemoveImplementationFactory<T>(implFactory);
    }

    internal int GetNextUniqueResourceID()
    {
      return Interlocked.Increment(ref m_currentResourceID);
    }

    public void Initialize(Engine engine) { }

    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    private void Dispose(bool isDisposing)
    {
      if (!m_isDisposed)
      {
        if (isDisposing)
        {
          if (m_device != null)
          {
            m_device.Dispose();
            m_device = null;
          }

          if (m_masteringVoice != null)
          {
            m_masteringVoice.Dispose();
            m_masteringVoice = null;
          }
        }

        m_isDisposed = true;
      }
    }

    private void InitializeFactories()
    {
      new X2SoundEffectImplFactory().Initialize(this);
    }

    private void CreateReverbSubmixVoice()
    {
      X2.VoiceDetails voiceDetals = m_masteringVoice.VoiceDetails;
      X2.SubmixVoiceFlags flags = X2.SubmixVoiceFlags.UseFilter; //IsReverbFilterEnabled
      m_reverbVoice = new X2.SubmixVoice(m_device, 1, voiceDetals.InputSampleRate, flags, 0);

      m_reverb = new X2.Fx.Reverb(m_device);
      m_reverbVoice.SetEffectChain(new X2.EffectDescriptor[] { new X2.EffectDescriptor(m_reverb, 1) });
      m_reverbVoice.SetEffectParameters<X2.Fx.ReverbParameters>(0, m_reverbParams);
      m_reverbVoice.EnableEffect(0);
    }

    private void CreateMasteringLimiter()
    {
      XAPO.Fx.MasteringLimiterParameters limitParams = new XAPO.Fx.MasteringLimiterParameters();
      limitParams.Loudness = 1000;
      limitParams.Release = 6;

      m_masteringLimiter = new XAPO.Fx.MasteringLimiter(m_device);
      m_masteringLimiter.Parameter = limitParams;
      m_masteringVoice.SetEffectChain(new X2.EffectDescriptor[] { new X2.EffectDescriptor(m_masteringLimiter) });
      m_masteringVoice.EnableEffect(0);
    }

    #region ImplementationFactoryCollection

    private sealed class ImplementationFactoryCollection : IEnumerable<IAudioResourceImplFactory>
    {
      private Dictionary<Type, IAudioResourceImplFactory> m_audioResourceTypeToFactory;
      private Dictionary<Type, IAudioResourceImplFactory> m_factoryTypeToFactory;

      public ImplementationFactoryCollection()
      {
        m_audioResourceTypeToFactory = new Dictionary<Type, IAudioResourceImplFactory>();
        m_factoryTypeToFactory = new Dictionary<Type, IAudioResourceImplFactory>();
      }

      public bool AddImplementationFactory<T>(T implFactory) where T : IAudioResourceImplFactory
      {
        if (implFactory == null || m_audioResourceTypeToFactory.ContainsKey(implFactory.AudioResourceType))
          return false;

        m_audioResourceTypeToFactory.Add(implFactory.AudioResourceType, implFactory);
        m_factoryTypeToFactory.Add(typeof(T), implFactory);

        return true;
      }

      public bool RemoveImplementationFactory<T>(T implFactory) where T : IAudioResourceImplFactory
      {
        if (implFactory == null || !m_audioResourceTypeToFactory.ContainsKey(implFactory.AudioResourceType))
          return false;

        return m_audioResourceTypeToFactory.Remove(implFactory.AudioResourceType) && m_factoryTypeToFactory.Remove(typeof(T));
      }

      public T GetImplementationFactory<T>() where T : IAudioResourceImplFactory
      {
        IAudioResourceImplFactory factory;
        if (m_factoryTypeToFactory.TryGetValue(typeof(T), out factory) && factory is T)
          return (T) factory;

        return default(T);
      }

      public bool TryGetImplementationFactory<T>(out T implementationFactory) where T : IAudioResourceImplFactory
      {
        implementationFactory = default(T);

        IAudioResourceImplFactory factory;
        if (m_factoryTypeToFactory.TryGetValue(typeof(T), out factory) && factory is T)
        {
          implementationFactory = (T) factory;
          return true;
        }

        return false;
      }

      public bool IsSupported<T>() where T : AudioResource
      {
        return m_audioResourceTypeToFactory.ContainsKey(typeof(T));
      }

      public IEnumerator<IAudioResourceImplFactory> GetEnumerator()
      {
        return m_factoryTypeToFactory.Values.GetEnumerator();
      }

      IEnumerator IEnumerable.GetEnumerator()
      {
        return m_factoryTypeToFactory.Values.GetEnumerator();
      }
    }

    #endregion
  }
}
