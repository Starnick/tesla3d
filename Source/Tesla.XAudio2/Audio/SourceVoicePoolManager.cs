﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesla.Utilities;
using X2 = SharpDX.XAudio2;
using SHM = SharpDX.Multimedia;

namespace Tesla.XAudio2.Audio
{
  public class SourceVoicePoolManager : IPoolManager<SourceVoiceRef>
  {
    private X2.XAudio2 m_device;
    private SHM.WaveFormat m_waveFormat;

    public X2.XAudio2 X2Device
    {
      get
      {
        return m_device;
      }
    }

    public SHM.WaveFormat Format
    {
      get
      {
        return m_waveFormat;
      }
    }

    public SourceVoicePoolManager(X2.XAudio2 device, SHM.WaveFormat format)
    {
      m_device = device;
      m_waveFormat = format;
    }

    public bool Allocate(out SourceVoiceRef item)
    {
      item = new SourceVoiceRef(new X2.SourceVoice(m_device, m_waveFormat, X2.VoiceFlags.None, 1024.0f, false), m_waveFormat);
      return true;
    }

    public bool IsActive(SourceVoiceRef item)
    {
      //If the voice is managed by an instance, or a fire and forget instance is not yet playing...it still is active
      return item.IsOwned || item.Voice.State.BuffersQueued > 0;
    }

    public void Free(SourceVoiceRef item)
    {
      X2.SourceVoice srcVoice = item.Voice;

      //Just doubly be sure that the voice isnt active. This shouldn't happen ever.
      if (srcVoice.State.BuffersQueued > 0)
      {
        System.Diagnostics.Debug.Assert(true);
        srcVoice.Stop();
        srcVoice.FlushSourceBuffers();
      }

      srcVoice.DestroyVoice();
      srcVoice.Dispose();
    }
  }
}
