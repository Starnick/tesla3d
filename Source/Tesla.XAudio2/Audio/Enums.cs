﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using SHM = SharpDX.Multimedia;

namespace Tesla.XAudio2.Audio
{
  /// <summary>
  /// Speaker configuration based on the individual speaker flags from SharpDX.Multimedia
  /// </summary>
  public enum SpeakerConfiguration
  {
    None = 0,
    Mono = SHM.Speakers.FrontCenter,
    Stereo = SHM.Speakers.FrontLeft | SHM.Speakers.FrontRight,
    Quad = SHM.Speakers.FrontLeft | SHM.Speakers.FrontRight | SHM.Speakers.BackLeft | SHM.Speakers.BackRight,
    Surround = SHM.Speakers.FrontLeft | SHM.Speakers.FrontRight | SHM.Speakers.FrontCenter | SHM.Speakers.BackCenter,
    FivePointOne = SHM.Speakers.FrontLeft | SHM.Speakers.FrontRight | SHM.Speakers.FrontCenter | SHM.Speakers.LowFrequency | SHM.Speakers.BackLeft | SHM.Speakers.BackRight,
    FivePointOneSurround = SHM.Speakers.FrontLeft | SHM.Speakers.FrontRight | SHM.Speakers.FrontCenter | SHM.Speakers.LowFrequency | SHM.Speakers.SideLeft | SHM.Speakers.SideRight,
    SevenPointOne = SHM.Speakers.FrontLeft | SHM.Speakers.FrontRight | SHM.Speakers.FrontCenter | SHM.Speakers.LowFrequency | SHM.Speakers.BackLeft | SHM.Speakers.BackRight | SHM.Speakers.FrontLeftOfCenter | SHM.Speakers.FrontRightOfCenter,
    SevenPointOneSurround = SHM.Speakers.FrontLeft | SHM.Speakers.FrontRight | SHM.Speakers.FrontCenter | SHM.Speakers.LowFrequency | SHM.Speakers.BackLeft | SHM.Speakers.BackRight | SHM.Speakers.SideLeft | SHM.Speakers.SideRight
  }
}
