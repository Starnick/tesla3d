﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using X2 = SharpDX.XAudio2;
using SHM = SharpDX.Multimedia;

namespace Tesla.XAudio2.Audio
{
  public sealed class SourceVoiceRef
  {
    private X2.SourceVoice m_voice;
    private SHM.WaveFormat m_format;
    private bool m_isOwned;

    public X2.SourceVoice Voice
    {
      get
      {
        return m_voice;
      }
    }

    public SHM.WaveFormat Format
    {
      get
      {
        return m_format;
      }
    }

    public bool IsOwned
    {
      get
      {
        return m_isOwned;
      }
      set
      {
        m_isOwned = value;
      }
    }

    public SourceVoiceRef(X2.SourceVoice voice, SHM.WaveFormat format) : this(voice, format, false) { }

    public SourceVoiceRef(X2.SourceVoice voice, SHM.WaveFormat format, bool isOwned)
    {
      m_voice = voice;
      m_format = format;
      m_isOwned = isOwned;
    }
  }
}
