﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesla;
using Tesla.Audio;
using Tesla.Audio.Implementation;
using SDX = SharpDX;
using X2 = SharpDX.XAudio2;
using X3D = SharpDX.X3DAudio;
using SHM = SharpDX.Multimedia;

namespace Tesla.XAudio2.Audio.Implementation
{
  public sealed class X2SoundEffectImplFactory : X2AudioResourceImplFactory, ISoundEffectImplFactory
  {
    /// <summary>
    /// Constructs a new instance of the <see cref="X2SoundEffectImplFactory"/> class.
    /// </summary>
    public X2SoundEffectImplFactory() : base(typeof(SoundEffect)) { }

    public ISoundEffectImpl CreateImplementation(IReadOnlyDataBuffer audioData)
    {
      return new X2SoundEffectImpl(X2AudioSystem, GetNextUniqueResourceID(), audioData);
    }

    /// <summary>
    /// Called when the factory is initialized.
    /// </summary>
    /// <param name="audioSystem">The XAudio2 audio system.</param>
    protected override void OnInitialize(X2AudioSystem audioSystem)
    {
      RegisterFactory<ISoundEffectImplFactory>(this);
    }
  }

  public sealed class X2SoundEffectImpl : AudioResourceImpl, ISoundEffectImpl
  {
    private X2AudioSystem m_audioSystem;
    private IReadOnlyDataBuffer m_audioData;
    private TimeSpan m_duration;
    private SHM.WaveFormat m_format;
    private float[] m_panMatrix;
    private uint[] m_decodedPacketsInfo;
    private List<WeakReference<SoundEffectInstance>> m_children;

    private X2.AudioBuffer m_audioBuffer;
    private X2.AudioBuffer m_loopedAudioBuffer;

    public TimeSpan Duration
    {
      get
      {
        return m_duration;
      }
    }

    public IReadOnlyDataBuffer AudioData
    {
      get
      {
        return m_audioData;
      }
    }

    public SHM.WaveFormat Format
    {
      get
      {
        return m_format;
      }
    }

    public X2.AudioBuffer AudioBuffer
    {
      get
      {
        return m_audioBuffer;
      }
    }

    public X2.AudioBuffer LoopedAudioBuffer
    {
      get
      {
        return m_loopedAudioBuffer;
      }
    }

    public uint[] DecodedPacketsInfo
    {
      get
      {
        return m_decodedPacketsInfo;
      }
    }

    internal X2SoundEffectImpl(X2AudioSystem audioSystem, int resourceID, IReadOnlyDataBuffer audioData)
        : base(audioSystem, resourceID)
    {
      m_audioSystem = audioSystem;
      m_audioData = audioData;

      //TODO - Come up with a sound effect storage format similar to effect data
      using (SHM.SoundStream stream = new SHM.SoundStream(new DataBufferStream(audioData)))
      {
        SDX.DataStream ds = stream.ToDataStream();
        m_format = stream.Format;
        m_decodedPacketsInfo = stream.DecodedPacketsInfo;
        m_audioBuffer = new X2.AudioBuffer(ds);
        m_loopedAudioBuffer = new X2.AudioBuffer(ds);
        m_loopedAudioBuffer.LoopCount = X2.AudioBuffer.LoopInfinite;
      }

      m_duration = AudioHelper.GetTimeDuration(m_audioBuffer, m_decodedPacketsInfo, m_format);
      m_children = new List<WeakReference<SoundEffectInstance>>();
    }

    public bool Play(float volume, float pitch, float pan)
    {
      SourceVoiceRef voiceRef;
      if (!m_audioSystem.VoicePools.TryFetch(m_format, out voiceRef))
        return false;

      voiceRef.IsOwned = false;

      X2.SourceVoice voice = voiceRef.Voice;
      voice.SetVolume(MathHelper.Clamp(volume, 0.0f, 1.0f));
      voice.SetFrequencyRatio(X2.XAudio2.SemitonesToFrequencyRatio(MathHelper.Clamp(pitch, -1.0f, 1.0f)));
      AudioHelper.SetPanMatrix(voice, m_format, m_audioSystem.X2MasteringVoice, m_audioSystem.Speakers, pan, ref m_panMatrix);

      voice.SubmitSourceBuffer(m_audioBuffer, m_decodedPacketsInfo);
      voice.Start();

      return true;
    }

    public ISoundEffectInstance CreateInstance()
    {
      SourceVoiceRef voiceRef;
      if (!m_audioSystem.VoicePools.TryFetch(m_format, out voiceRef))
        throw new InvalidOperationException(StringLocalizer.Instance.GetLocalizedString("CreateSoundEffectInstanceFailed"));

      SoundEffectInstance instance = new SoundEffectInstance(this, voiceRef);

      lock (m_children)
      {
        m_children.Add(new WeakReference<SoundEffectInstance>(instance));
      }

      return instance;
    }

    protected override void Dispose(bool disposing)
    {
      if (!IsDisposed)
      {
        if (disposing)
        {
          lock (m_children)
          {
            for (int i = 0; i < m_children.Count; i++)
            {
              WeakReference<SoundEffectInstance> wkRef = m_children[i];
              SoundEffectInstance sfxInstance;
              if (wkRef.TryGetTarget(out sfxInstance))
                sfxInstance.OnParentDisposed();
            }

            m_children.Clear();
          }

          if (m_audioBuffer != null)
          {
            m_audioBuffer.Stream.Dispose();
            m_audioBuffer = null;
            m_loopedAudioBuffer = null; //Shares same stream as other audio buffer
          }
        }
      }

      base.Dispose(disposing);
    }

    private void OnChildDisposed(SoundEffectInstance child)
    {
      //When a child has been disposed, sweep the child list
      lock (m_children)
      {
        for (int i = 0; i < m_children.Count; i++)
        {
          WeakReference<SoundEffectInstance> wkRef = m_children[i];
          SoundEffectInstance sfxInstance;
          if (!wkRef.TryGetTarget(out sfxInstance) || sfxInstance == child)
          {
            m_children.RemoveAt(i);
            i--;
          }
        }
      }
    }

    private sealed class SoundEffectInstance : ISoundEffectInstance
    {
      private X2SoundEffectImpl m_soundEffect;
      private SourceVoiceRef m_voice;
      private float[] m_panMatrix;
      private float m_volume;
      private float m_pan;
      private float m_pitch;
      private bool m_isPaused;
      private bool m_isLooped;
      private bool m_isDisposed;

      private X3D.Listener m_listener;
      private X3D.Emitter m_emitter;
      private X3D.DspSettings m_dspSettings;
      private bool m_isReverbSubmixSet = false;
      private float[] m_reverbLevels;

      public float Volume
      {
        get
        {
          return m_volume;
        }
        set
        {
          CheckDisposed();

          if (!MathHelper.IsEqual(value, m_volume))
          {
            m_volume = value;
            m_voice.Voice.SetVolume(MathHelper.Clamp(m_volume, 0.0f, 1.0f));
          }
        }
      }

      public float Pan
      {
        get
        {
          return m_pan;
        }
        set
        {
          CheckDisposed();

          if (!MathHelper.IsEqual(value, m_pan))
          {
            m_pan = value;
            X2AudioSystem audioSystem = m_soundEffect.m_audioSystem;
            AudioHelper.SetPanMatrix(m_voice.Voice, m_voice.Format, audioSystem.X2MasteringVoice, audioSystem.Speakers, m_pan, ref m_panMatrix);
          }
        }
      }

      public float Pitch
      {
        get
        {
          return m_pitch;
        }
        set
        {
          CheckDisposed();

          if (!MathHelper.IsEqual(value, m_pitch))
          {
            m_pitch = value;
            m_voice.Voice.SetFrequencyRatio(X2.XAudio2.SemitonesToFrequencyRatio(MathHelper.Clamp(m_pitch, -1.0f, 1.0f)));
          }
        }
      }

      public SoundState State
      {
        get
        {
          if (m_voice == null || m_voice.Voice.State.BuffersQueued == 0)
            return SoundState.Stopped;

          if (m_isPaused)
            return SoundState.Paused;

          return SoundState.Playing;
        }
      }

      public SoundEffect Parent
      {
        get
        {
          if (m_soundEffect == null)
            return null;

          return m_soundEffect.Parent as SoundEffect;
        }
      }

      public IAudioSystem AudioSystem
      {
        get
        {
          if (m_soundEffect == null)
            return null;

          return m_soundEffect.m_audioSystem;
        }
      }

      public bool IsLooped
      {
        get
        {
          return m_isLooped;
        }
        set
        {
          m_isLooped = value;
        }
      }

      public bool IsDisposed
      {
        get
        {
          return m_isDisposed;
        }
      }

      private X2.AudioBuffer AudioBufferToUse
      {
        get
        {
          return (m_isLooped) ? m_soundEffect.LoopedAudioBuffer : m_soundEffect.AudioBuffer;
        }
      }

      public SoundEffectInstance(X2SoundEffectImpl soundEffect, SourceVoiceRef voice)
      {
        m_soundEffect = soundEffect;
        m_voice = voice;
        m_voice.IsOwned = true;

        m_isDisposed = false;
        m_isPaused = false;
        m_isLooped = false;
        m_panMatrix = null;

        //Reset the voice because it may have been used before
        Volume = 1.0f;
        Pan = 0.0f;
        Pitch = 0.0f;
      }

      public void Apply3D(AudioListener listener, AudioEmitter emitter)
      {
        CheckDisposed();

        X2AudioSystem audioSystem = m_soundEffect.m_audioSystem;

        X3D.Emitter x3dEmitter = PopulateEmitter(ref emitter);
        X3D.Listener x3dListener = PopulateListener(ref listener);

        if (m_dspSettings == null)
          m_dspSettings = new X3D.DspSettings(m_voice.Format.Channels, audioSystem.X2MasteringVoice.VoiceDetails.InputChannelCount);

        X3D.CalculateFlags flags = X3D.CalculateFlags.Matrix | X3D.CalculateFlags.Doppler | X3D.CalculateFlags.LpfDirect;
        SHM.Speakers speakers = (SHM.Speakers) audioSystem.Speakers;

        if ((speakers & SHM.Speakers.LowFrequency) > SHM.Speakers.None)
        {
          //On devices with a LFE channel allow mono source data to be routed to the LFE destination channel
          flags |= X3D.CalculateFlags.RedirectToLfe;
        }

        X2.SourceVoice srcVoice = m_voice.Voice;

        bool isReverbEnabled = audioSystem.IsReverberationEffectEnabled;

        if (isReverbEnabled)
        {
          flags |= X3D.CalculateFlags.Reverb | X3D.CalculateFlags.LpfReverb;

          if (!m_isReverbSubmixSet)
          {
            X2.VoiceSendFlags sendFlags = X2.VoiceSendFlags.UseFilter;
            X2.VoiceSendDescriptor[] outputVoices = new X2.VoiceSendDescriptor[]
        {
                        new X2.VoiceSendDescriptor(sendFlags, audioSystem.X2MasteringVoice),
                        new X2.VoiceSendDescriptor(sendFlags, audioSystem.X2ReverbVoice)
        };

            srcVoice.SetOutputVoices(outputVoices);
            m_isReverbSubmixSet = true;
          }
        }

        audioSystem.X3DDevice.Calculate(x3dListener, x3dEmitter, flags, m_dspSettings);

        srcVoice.SetFrequencyRatio(m_dspSettings.DopplerFactor);
        srcVoice.SetOutputMatrix(audioSystem.X2MasteringVoice, m_dspSettings.SourceChannelCount, m_dspSettings.DestinationChannelCount, m_dspSettings.MatrixCoefficients);

        //Why does this not work!?!?
        /*
        if(isReverbEnabled)
        {
            if(m_reverbLevels == null || m_reverbLevels.Length != m_voice.Format.Channels)
                m_reverbLevels = new float[m_voice.Format.Channels];

            for(int i = 0; i < m_reverbLevels.Length; i++)
                m_reverbLevels[i] = m_dspSettings.ReverbLevel;

            srcVoice.SetOutputMatrix(audioSystem.X2ReverbVoice, 1, 1, m_reverbLevels);

            //If ReverbFilterEnabled
            X2.FilterParameters filterDirect = new X2.FilterParameters();
            filterDirect.Type = X2.FilterType.LowPassFilter;

            //From XAudio2CutOffFrequencyToRadians() in XAudio2.h
            filterDirect.Frequency = 2.0f * (float) Math.Sin((X3D.X3DAudio.PI / 6.0f) * m_dspSettings.LpfDirectCoefficient);
            filterDirect.OneOverQ = 1.0f;

            srcVoice.SetOutputFilterParameters(audioSystem.X2MasteringVoice, filterDirect);

            //Nested - If ReverbEffectEnabled
            X2.FilterParameters filterReverb = new X2.FilterParameters();
            filterReverb.Type = X2.FilterType.LowPassFilter;

            //From XAudio2CutOffFrequencyToRadians() in XAudio2.h
            filterDirect.Frequency = 2.0f * (float) Math.Sin((X3D.X3DAudio.PI / 6.0f) * m_dspSettings.LpfReverbCoefficient);
            filterDirect.OneOverQ = 1.0f;

            srcVoice.SetOutputFilterParameters(audioSystem.X2ReverbVoice, filterReverb);
        }*/
      }

      public void Play()
      {
        CheckDisposed();

        //Only play if not already playing
        if (State == SoundState.Playing)
          return;

        X2.SourceVoice srcVoice = m_voice.Voice;
        if (srcVoice.State.BuffersQueued > 0)
        {
          srcVoice.Stop();
          srcVoice.FlushSourceBuffers();
        }

        srcVoice.SubmitSourceBuffer(AudioBufferToUse, m_soundEffect.m_decodedPacketsInfo);
        srcVoice.Start();

        //Make sure we denote that we are no longer in a paused state
        m_isPaused = false;
      }

      public void Pause()
      {
        CheckDisposed();

        if (m_isPaused)
          return;

        //Even if state is stopped, make sure we're "paused" so resume will work
        m_voice.Voice.Stop();
        m_isPaused = true;
      }

      public void Resume()
      {
        CheckDisposed();

        X2.SourceVoice srcVoice = m_voice.Voice;

        //If not looped and we stopped, then we need to essentially do a "Play"
        if (!m_isLooped && srcVoice.State.BuffersQueued == 0)
        {
          srcVoice.Stop();
          srcVoice.FlushSourceBuffers();
          srcVoice.SubmitSourceBuffer(AudioBufferToUse, m_soundEffect.m_decodedPacketsInfo);
        }

        //Otherwise, just start again
        srcVoice.Start();
        m_isPaused = false;
      }

      public void Stop(bool immediate)
      {
        CheckDisposed();

        if (immediate)
        {
          m_voice.Voice.Stop();
          m_voice.Voice.FlushSourceBuffers();
        }
        else if (m_isLooped)
        {
          m_voice.Voice.ExitLoop();
        }
        else
        {
          m_voice.Voice.Stop((int) X2.PlayFlags.Tails);
        }

        //If in paused state, make sure we denote that we truly stopped
        m_isPaused = false;
      }

      public void Dispose()
      {
        Dispose(true);
        GC.SuppressFinalize(this);
      }

      private void Dispose(bool isDisposing)
      {
        if (!m_isDisposed)
        {
          if (isDisposing)
          {
            ReleaseSourceVoice();

            if (m_soundEffect != null)
            {
              m_soundEffect.OnChildDisposed(this);
              m_soundEffect = null;
            }
          }

          m_isDisposed = true;
        }
      }

      internal void OnParentDisposed()
      {
        if (!m_isDisposed)
        {
          ReleaseSourceVoice();
          m_soundEffect = null;
          m_isDisposed = true;
        }
      }

      private void ReleaseSourceVoice()
      {
        if (m_voice != null && m_voice.Voice != null)
        {
          m_voice.IsOwned = false;

          X2.SourceVoice srcVoice = m_voice.Voice;

          srcVoice.Stop();
          srcVoice.FlushSourceBuffers();

          if (m_isReverbSubmixSet)
          {
            srcVoice.SetOutputVoices((X2.VoiceSendDescriptor[]) null);
            m_isReverbSubmixSet = false;
          }

          //Return the voice back to the pool
          if (m_soundEffect != null)
          {
            m_soundEffect.m_audioSystem.VoicePools.Return(m_voice);
          }
          else
          {
            //This should NEVER happen, but just in case make sure we cleanup
            System.Diagnostics.Debug.Assert(true);
            m_voice.Voice.DestroyVoice();
            m_voice.Voice.Dispose();
          }

          m_voice = null;
        }
      }

      private void CheckDisposed()
      {
        if (m_isDisposed)
          throw new ObjectDisposedException(GetType().FullName);
      }

      private X3D.Emitter PopulateEmitter(ref AudioEmitter emitter)
      {
        if (m_emitter == null)
          m_emitter = new X3D.Emitter();

        //X3DAudio uses a left handed coordinate system with values on x-axis increasing from left to right, y-axis from bottom to top,
        //and z-axis from near to far
        //Azimuths are measured clockwise from a given reference direction
        //
        //Since we're on a right handed coordinate system, we need to invert the z-axis (forward vector) and negate Z values for any pos/velocity values

        Vector3 forward;
        Vector3.Negate(emitter.Forward, out forward);

        Vector3 pos = emitter.Position;
        pos.Z *= -1.0f;

        Vector3 velocity = emitter.Velocity;
        velocity.Z *= -1.0f;

        AudioHelper.ConvertVector3(ref forward, out m_emitter.OrientFront);
        AudioHelper.ConvertVector3(ref emitter.Up, out m_emitter.OrientTop);
        AudioHelper.ConvertVector3(ref pos, out m_emitter.Position);
        AudioHelper.ConvertVector3(ref velocity, out m_emitter.Velocity);

        X2AudioSystem audioSystem = m_soundEffect.m_audioSystem;
        m_emitter.DopplerScaler = audioSystem.DopplerScale;
        m_emitter.CurveDistanceScaler = audioSystem.DistanceScale;
        m_emitter.ChannelCount = m_voice.Format.Channels;

        if (m_emitter.ChannelCount > 1 || (m_emitter.ChannelAzimuths != null && m_emitter.ChannelAzimuths.Length != m_emitter.ChannelCount))
          m_emitter.ChannelAzimuths = new float[m_emitter.ChannelCount];

        return m_emitter;
      }

      private X3D.Listener PopulateListener(ref AudioListener listener)
      {
        if (m_listener == null)
          m_listener = new X3D.Listener();

        //X3DAudio uses a left handed coordinate system with values on x-axis increasing from left to right, y-axis from bottom to top,
        //and z-axis from near to far
        //Azimuths are measured clockwise from a given reference direction
        //
        //Since we're on a right handed coordinate system, we need to invert the z-axis (forward vector) and negate Z values for any pos/velocity values

        Vector3 forward;
        Vector3.Negate(listener.Forward, out forward);

        Vector3 pos = listener.Position;
        pos.Z *= -1.0f;

        Vector3 velocity = listener.Velocity;
        velocity.Z *= -1.0f;

        AudioHelper.ConvertVector3(ref forward, out m_listener.OrientFront);
        AudioHelper.ConvertVector3(ref listener.Up, out m_listener.OrientTop);
        AudioHelper.ConvertVector3(ref pos, out m_listener.Position);
        AudioHelper.ConvertVector3(ref velocity, out m_listener.Velocity);

        return m_listener;
      }
    }
  }
}
