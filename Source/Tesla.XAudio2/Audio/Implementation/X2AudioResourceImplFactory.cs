﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Audio;
using Tesla.Audio.Implementation;

namespace Tesla.XAudio2.Audio.Implementation
{
  /// <summary>
  /// Common base class for all XAudio2 graphic resource implementation factories.
  /// </summary>
  public abstract class X2AudioResourceImplFactory : IAudioResourceImplFactory
  {
    private X2AudioSystem m_audioSystem;
    private Type m_resourceType;

    /// <summary>
    /// Gets the audio system this factory binds resources to.
    /// </summary>
    public IAudioSystem AudioSystem
    {
      get
      {
        return m_audioSystem;
      }
    }

    /// <summary>
    /// Gets the type of audio resource this implementation factory creates implementations for.
    /// </summary>
    public Type AudioResourceType
    {
      get
      {
        return m_resourceType;
      }
    }

    /// <summary>
    /// Gets the audio system as an <see cref="X2AudioSystem"/>.
    /// </summary>
    protected X2AudioSystem X2AudioSystem
    {
      get
      {
        return m_audioSystem;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="X2AudioResourceImplFactory"/> class.
    /// </summary>
    /// <param name="resourceType">Type of the audio resource the factory creates implementations for.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the type is null.</exception>
    protected X2AudioResourceImplFactory(Type resourceType)
    {
      if (resourceType == null)
        throw new ArgumentNullException("resourceType");

      m_resourceType = resourceType;
    }

    /// <summary>
    /// Initializes for the specified audio system, and registers the factory to the render system.
    /// </summary>
    /// <param name="audioSystem">The Xaudio2 audio system to initialize the factory with.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the audio system is null.</exception>
    public void Initialize(X2AudioSystem audioSystem)
    {
      if (audioSystem == null)
        throw new ArgumentNullException("audioSystem");

      m_audioSystem = audioSystem;
      OnInitialize(m_audioSystem);
    }

    /// <summary>
    /// Called when the factory is initialized.
    /// </summary>
    /// <param name="renderSystem">The XAudio2 audio system.</param>
    protected abstract void OnInitialize(X2AudioSystem audioSystem);

    /// <summary>
    /// Gets the next available resource ID from the audio system.
    /// </summary>
    /// <returns>Next unique resource ID.</returns>
    /// <exception cref="TeslaAudioException">Thrown if the factory is not initialized.</exception>
    protected int GetNextUniqueResourceID()
    {
      if (m_audioSystem == null)
        throw AudioExceptionHelper.NewImplementationFactoryNotInitialized();

      return m_audioSystem.GetNextUniqueResourceID();
    }

    /// <summary>
    /// Registers the factory of the specified type to the audio system.
    /// </summary>
    /// <typeparam name="T">Type of implementation factory (usually an interface).</typeparam>
    /// <param name="implFactory">Implementation factory to register.</param>
    /// <returns>True if successful, false otherwise.</returns>
    /// <exception cref="TeslaAudioException">Thrown if the factory is not initialized.</exception>
    protected bool RegisterFactory<T>(T implFactory) where T : IAudioResourceImplFactory
    {
      if (m_audioSystem == null)
        throw AudioExceptionHelper.NewImplementationFactoryNotInitialized();

      return m_audioSystem.AddImplementationFactory<T>(implFactory);
    }

    /// <summary>
    /// ReRegisters the factory of the specified type to the audio system. If a factory of the same type was previously register, it is
    /// removed from the audio system.
    /// </summary>
    /// <typeparam name="T">Type of implementation factory (usually an interface).</typeparam>
    /// <param name="implFactory">Implementation factory to register.</param>
    /// <returns>True if successful, false otherwise.</returns>
    /// <exception cref="TeslaGraphicsException">Thrown if the factory is not initialized.</exception>
    protected bool ReRegisterFactory<T>(T implFactory) where T : IAudioResourceImplFactory
    {
      if (m_audioSystem == null)
        throw AudioExceptionHelper.NewImplementationFactoryNotInitialized();

      m_audioSystem.RemoveImplementationFactory<T>(implFactory);
      return m_audioSystem.AddImplementationFactory<T>(implFactory);
    }
  }
}
