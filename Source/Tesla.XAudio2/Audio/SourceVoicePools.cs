﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesla.Utilities;
using X2 = SharpDX.XAudio2;
using SHM = SharpDX.Multimedia;

namespace Tesla.XAudio2.Audio
{
  public sealed class SourceVoicePools
  {
    private Dictionary<SHM.WaveFormatEncoding, Dictionary<uint, ObjectPool<SourceVoiceRef>>> m_pools;
    private X2.XAudio2 m_device;

    public SourceVoicePools(X2.XAudio2 device)
    {
      m_pools = new Dictionary<SHM.WaveFormatEncoding, Dictionary<uint, ObjectPool<SourceVoiceRef>>>();
      m_device = device;
    }

    public bool TryFetch(SHM.WaveFormat format, out SourceVoiceRef item)
    {
      if (format == null)
      {
        item = null;
        return false;
      }

      lock (m_pools)
      {
        Dictionary<uint, ObjectPool<SourceVoiceRef>> pools = GetObjectPoolDictionary(format.Encoding);
        uint formatKey = GetFormatKey(format);

        ObjectPool<SourceVoiceRef> pool;
        if (!pools.TryGetValue(formatKey, out pool))
        {
          pool = new ObjectPool<SourceVoiceRef>(new SourceVoicePoolManager(m_device, format));
          pool.SweepActiveList = true;
          pools.Add(formatKey, pool);
        }

        System.Diagnostics.Debug.Assert((pool.Manager as SourceVoicePoolManager).Format.Equals(format));

        return pool.TryFetch(out item);
      }
    }

    public void Return(SourceVoiceRef item)
    {
      if (item == null)
        return;

      lock (m_pools)
      {
        SHM.WaveFormat format = item.Format;
        Dictionary<uint, ObjectPool<SourceVoiceRef>> pools = GetObjectPoolDictionary(format.Encoding);
        uint formatKey = GetFormatKey(format);

        //If a pool is not here, then it doesn't own the item because pools keep track of that
        ObjectPool<SourceVoiceRef> pool;
        if (pools.TryGetValue(formatKey, out pool))
          pool.Return(item);
      }
    }

    private uint GetFormatKey(SHM.WaveFormat format)
    {
      return (uint) format.GetHashCode();
    }

    private Dictionary<uint, ObjectPool<SourceVoiceRef>> GetObjectPoolDictionary(SHM.WaveFormatEncoding encoding)
    {
      Dictionary<uint, ObjectPool<SourceVoiceRef>> poolsForEncoding;
      if (!m_pools.TryGetValue(encoding, out poolsForEncoding))
      {
        poolsForEncoding = new Dictionary<uint, ObjectPool<SourceVoiceRef>>();
        m_pools.Add(encoding, poolsForEncoding);
      }

      return poolsForEncoding;
    }
  }
}
