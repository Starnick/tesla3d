﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Graphics;
using Tesla.Graphics.Implementation;
using D3D = SharpDX.Direct3D;
using D3D11 = SharpDX.Direct3D11;

#nullable enable

namespace Tesla.Direct3D11.Graphics.Implementation
{
  /// <summary>
  /// A factory that creates Direct3D11 implementations of type <see cref="IRasterizerStateImpl"/>.
  /// </summary>
  public sealed class D3D11RasterizerStateImplFactory : D3D11GraphicsResourceImplFactory, IRasterizerStateImplFactory
  {
    /// <summary>
    /// Constructs a new instance of the <see cref="D3D11RasterizerStateImplFactory"/> class.
    /// </summary>
    public D3D11RasterizerStateImplFactory() : base(typeof(RasterizerState)) { }

    /// <summary>
    /// Creates a new implementation object instance.
    /// </summary>
    /// <returns>The render state implementation.</returns>
    public IRasterizerStateImpl CreateImplementation()
    {
      return new D3D11RasterizerStateImpl(D3D11RenderSystem, GetNextUniqueResourceID());
    }

    /// <summary>
    /// Called when the factory is initialized.
    /// </summary>
    /// <param name="renderSystem">The D3D11 render system.</param>
    protected override void OnInitialize(D3D11RenderSystem renderSystem)
    {
      RegisterFactory<IRasterizerStateImplFactory>(this);
    }
  }

  /// <summary>
  /// A Direct3D11 implementation for <see cref="RasterizerState"/>.
  /// </summary>
  public sealed class D3D11RasterizerStateImpl : RasterizerStateImpl, ID3D11RasterizerState
  {
    private D3D11.Device m_device;
    private D3D11.RasterizerState? m_nativeRasterizerState;
    private bool m_antialiasedLineOptionSupported;
    private bool m_depthClipOptionSupported;

    /// <summary>
    /// Gets if the <see cref="AntialiasedLineEnable" /> property is supported. This can vary by implementation.
    /// </summary>
    public override bool IsAntialiasedLineOptionSupported
    {
      get
      {
        return m_antialiasedLineOptionSupported;
      }
    }

    /// <summary>
    /// Gets if the <see cref="DepthClipEnable" /> property is supported. This can vary by implementation.
    /// </summary>
    public override bool IsDepthClipOptionSupported
    {
      get
      {
        return m_depthClipOptionSupported;
      }
    }

    /// <summary>
    /// Gets the native D3D11 device.
    /// </summary>
    public D3D11.Device D3DDevice
    {
      get
      {
        return m_device;
      }
    }

    /// <summary>
    /// Gets the native D3D11 rasterizer state.
    /// </summary>
    public D3D11.RasterizerState D3DRasterizerState
    {
      get
      {
        return m_nativeRasterizerState!;
      }
    }

    internal D3D11RasterizerStateImpl(D3D11RenderSystem renderSystem, int resourceID)
        : base(renderSystem, resourceID)
    {

      m_device = renderSystem.D3DDevice;
      CheckSupport(m_device, out m_antialiasedLineOptionSupported, out m_depthClipOptionSupported);

      SetDefaultDebugName("RasterizerState");
    }

    /// <summary>
    /// Called when the state is bound, signaling the implementation to create and bind the native state.
    /// </summary>
    protected override void CreateNativeState()
    {
      D3D11.RasterizerStateDescription desc = new D3D11.RasterizerStateDescription();
      desc.IsAntialiasedLineEnabled = (IsAntialiasedLineOptionSupported) ? AntialiasedLineEnable : false;
      desc.IsDepthClipEnabled = (IsDepthClipOptionSupported) ? DepthClipEnable : false;
      desc.IsMultisampleEnabled = MultiSampleEnable;
      desc.IsScissorEnabled = ScissorTestEnable;
      desc.IsFrontCounterClockwise = (VertexWinding == Tesla.Graphics.VertexWinding.CounterClockwise) ? true : false;

      desc.DepthBias = DepthBias;
      desc.DepthBiasClamp = DepthBiasClamp;
      desc.SlopeScaledDepthBias = SlopeScaledDepthBias;
      desc.CullMode = Direct3DHelper.ToD3DCullMode(Cull);
      desc.FillMode = Direct3DHelper.ToD3DFillMode(Fill);

      m_nativeRasterizerState = new D3D11.RasterizerState(m_device, desc);
      m_nativeRasterizerState.DebugName = DebugName;
    }

    /// <summary>
    /// Called when the name of the graphics resource is changed, useful if the implementation wants to set the name to
    /// be used as a debug name.
    /// </summary>
    /// <param name="name">New name of the resource</param>
    protected override void OnDebugNameChange(string name)
    {
      if (m_nativeRasterizerState is not null)
        m_nativeRasterizerState.DebugName = name;
    }

    /// <summary>
    /// Releases unmanaged and - optionally - managed resources.
    /// </summary>
    /// <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
    protected override void Dispose(bool disposing)
    {
      if (!IsDisposed)
      {
        if (disposing)
        {
          if (m_nativeRasterizerState is not null)
          {
            m_nativeRasterizerState.Dispose();
            m_nativeRasterizerState = null;
          }
        }

        base.Dispose(disposing);
      }
    }

    private void CheckSupport(D3D11.Device device, out bool antialiasedLineOptionSupport, out bool depthClipOptionSupport)
    {
      antialiasedLineOptionSupport = false;
      depthClipOptionSupport = false;

      switch (device.FeatureLevel)
      {
        case D3D.FeatureLevel.Level_11_1:
        case D3D.FeatureLevel.Level_11_0:
        case D3D.FeatureLevel.Level_10_1:
        case D3D.FeatureLevel.Level_10_0:
          antialiasedLineOptionSupport = true;
          depthClipOptionSupport = true;
          break;
      }
    }
  }
}
