﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Buffers;
using System.Diagnostics.CodeAnalysis;
using Tesla.Graphics;
using Tesla.Graphics.Implementation;
using D3D11 = SharpDX.Direct3D11;
using DXGI = SharpDX.DXGI;

#nullable enable

namespace Tesla.Direct3D11.Graphics.Implementation
{
  /// <summary>
  /// A factory that creates Direct3D11 implementation of type <see cref="IVertexBufferImpl"/>.
  /// </summary>
  public sealed class D3D11VertexBufferImplFactory : D3D11GraphicsResourceImplFactory, IVertexBufferImplFactory
  {
    /// <summary>
    /// Constructs a new instance of the <see cref="D3D11VertexBufferImplFactory"/> class.
    /// </summary>
    public D3D11VertexBufferImplFactory() : base(typeof(VertexBuffer)) { }

    /// <inheritdoc />
    public IVertexBufferImpl CreateImplementation(VertexLayout vertexLayout, int vertexCount, VertexBufferOptions options)
    {
      return new D3D11VertexBufferImpl(D3D11RenderSystem, GetNextUniqueResourceID(), vertexLayout, vertexCount, options);
    }

    /// <inheritdoc />
    protected override void OnInitialize(D3D11RenderSystem renderSystem)
    {
      RegisterFactory<IVertexBufferImplFactory>(this);
    }
  }

  /// <summary>
  /// A factory that creates Direct3D11 implementation of type <see cref="IStreamOutputBufferImpl"/>.
  /// </summary>
  public sealed class D3D11StreamOutputImplFactory : D3D11GraphicsResourceImplFactory, IStreamOutputBufferImplFactory
  {
    /// <summary>
    /// Constructs a new instance of the <see cref="D3D11StreamOutputImplFactory"/> class.
    /// </summary>
    public D3D11StreamOutputImplFactory() : base(typeof(StreamOutputBuffer)) { }

    /// <inheritdoc />
    public IStreamOutputBufferImpl CreateImplementation(VertexLayout vertexLayout, int vertexCount, VertexBufferOptions options)
    {
      return new D3D11VertexBufferImpl(D3D11RenderSystem, GetNextUniqueResourceID(), vertexLayout, vertexCount, options, D3D11.BindFlags.StreamOutput, "StreamOutputBuffer");
    }

    /// <inheritdoc />
    protected override void OnInitialize(D3D11RenderSystem renderSystem)
    {
      RegisterFactory<IStreamOutputBufferImplFactory>(this);
    }
  }

  /// <summary>
  /// A Direct3D11 implementation for <see cref="VertexBuffer"/>.
  /// </summary>
  public sealed class D3D11VertexBufferImpl : GraphicsResourceImpl, IVertexBufferImpl, IStreamOutputBufferImpl, ID3D11Buffer, ID3D11ShaderResourceView
  {
    private D3D11.Device m_device;
    private D3D11.Buffer m_nativeBuffer;
    private D3D11.ShaderResourceView? m_shaderResourceView;
    private VertexLayout m_vertexLayout;
    private int m_vertexCount;
    private ResourceUsage m_resourceUsage;
    private ResourceBindFlags m_resourceBindFlags;
    private ShaderStageBinding m_boundStages;

    /// <inheritdoc />
    public VertexLayout VertexLayout
    {
      get
      {
        return m_vertexLayout;
      }
    }

    /// <inheritdoc />
    public int ElementCount
    {
      get
      {
        return m_vertexCount;
      }
    }

    /// <inheritdoc />
    public int ElementStride
    {
      get
      {
        return m_vertexLayout.VertexStride;
      }
    }

    /// <inheritdoc />
    public ResourceUsage ResourceUsage
    {
      get
      {
        return m_resourceUsage;
      }
    }

    /// <inheritdoc />
    public ResourceBindFlags ResourceBindFlags
    {
      get
      {
        return m_resourceBindFlags;
      }
    }

    /// <summary>
    /// Gets the native D3D11 device.
    /// </summary>
    public D3D11.Device D3DDevice
    {
      get
      {
        return m_device;
      }
    }

    /// <inheritdoc />
    public D3D11.Buffer D3DBuffer
    {
      get
      {
        return m_nativeBuffer;
      }
    }

    /// <inheritdoc />
    public D3D11.ShaderResourceView D3DShaderResourceView
    {
      get
      {
        if (m_shaderResourceView is null)
          throw new InvalidOperationException(StringLocalizer.Instance.GetLocalizedString("ResourceNotBindableToShader"));

        return m_shaderResourceView;
      }
    }

    /// <inheritdoc />
    public ShaderStageBinding BoundStages
    {
      get
      {
        return m_boundStages;
      }
      set
      {
        m_boundStages = value;
      }
    }

    internal D3D11VertexBufferImpl(D3D11RenderSystem renderSystem, int resourceID, VertexLayout vertexLayout, int vertexCount, VertexBufferOptions options, D3D11.BindFlags additionalBindFlags = D3D11.BindFlags.None, string debugName = "VertexBuffer")
        : base(renderSystem, resourceID)
    {
      m_device = renderSystem.D3DDevice;
      m_vertexLayout = vertexLayout;
      m_vertexCount = vertexCount;
      m_resourceUsage = options.ResourceUsage;
      m_resourceBindFlags = ResourceBindFlags.Default;

      D3D11.BufferDescription desc = new D3D11.BufferDescription();
      desc.BindFlags = D3D11.BindFlags.VertexBuffer | additionalBindFlags;
      desc.Usage = Direct3DHelper.ToD3DResourceUsage(m_resourceUsage);
      desc.CpuAccessFlags = (m_resourceUsage == ResourceUsage.Dynamic) ? D3D11.CpuAccessFlags.Write : D3D11.CpuAccessFlags.None;
      desc.OptionFlags = D3D11.ResourceOptionFlags.None;
      desc.SizeInBytes = vertexLayout.VertexStride * m_vertexCount;
      desc.StructureByteStride = 0;

      bool createSRV = false;
      if (options.ResourceBindFlags.HasFlag(ResourceBindFlags.ShaderResource))
      {
        m_resourceBindFlags |= ResourceBindFlags.ShaderResource;
        desc.BindFlags |= D3D11.BindFlags.ShaderResource;
        desc.OptionFlags |= D3D11.ResourceOptionFlags.BufferAllowRawViews;
        createSRV = true;
      }

      // If we have data...it may be either one interleaved or a number of individual vertex attributes
      IReadOnlyDataBuffer? interleavedData = null;
      if (options.Data.Length >= 1)
        interleavedData = (options.Data.Length > 1) ? ResourceHelper.CreatedInterleavedVertexBuffer(vertexLayout, options.Data, MemoryAllocatorStrategy.DefaultPooled) : options.Data[0];

      try
      {
        using (MemoryHandle handle = interleavedData?.Pin() ?? default)
          m_nativeBuffer = new D3D11.Buffer(m_device, handle.AsIntPointer(), desc);
      }
      finally
      {
        // Dispose of the interleaved data if we created it
        if (options.Data.Length > 1 && interleavedData is not null)
          interleavedData.Dispose();
      }

      if (createSRV)
      {
        D3D11.ShaderResourceViewDescription srvDesc = new D3D11.ShaderResourceViewDescription();
        srvDesc.BufferEx.ElementCount = ((m_vertexCount * m_vertexLayout.VertexStride)) / sizeof(uint);
        srvDesc.BufferEx.Flags = D3D11.ShaderResourceViewExtendedBufferFlags.Raw;
        srvDesc.Format = DXGI.Format.R32_Typeless;
        srvDesc.Dimension = SharpDX.Direct3D.ShaderResourceViewDimension.ExtendedBuffer;

        m_shaderResourceView = new D3D11.ShaderResourceView(m_device, m_nativeBuffer, srvDesc);
      }

      SetDefaultDebugName(debugName);
    }

    /// <summary>
    /// Reads interleaved vertex data from the vertexbuffer and stores it in a span of data buffers. Each data buffer represents
    /// a single vertex element (in the order declared by the vertex declaration) and must all have the same number of elements and match
    /// the vertex declaration in format size. The number of elements needs to match the number of vertex elements in the vertex buffer.
    /// </summary>
    /// <param name="data">Span of buffers representing each vertex attribute that will contain data read from the vertex buffer.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if data is null.</exception>
    /// <exception cref="System.ArgumentOutOfRangeException">Thrown if the number of buffers do not match the number
    /// of vertex elements, or if the number of vertices in each buffer does not match the vertex count,
    /// or if there is a byte size mismatch of any kind.</exception>
    public void GetInterleavedData(ReadOnlySpan<IReadOnlyDataBuffer> data)
    {
      D3D11.DeviceContext d3dContext = m_device.ImmediateContext;

      ResourceHelper.ReadInterleavedVertexData(m_nativeBuffer, d3dContext, m_vertexCount, m_vertexLayout, m_resourceUsage, data, false);
    }

    /// <summary>
    /// Writes interleaved vertex data from an array of data buffers into the vertex buffer. Each data buffer represents
    /// a single vertex element (in the order declared by the vertex declaration) and must all have the same number of elements and match
    /// the vertex declaration in format size. The number of elements needs to match the number of vertex elements in the vertex buffer. For dynamic
    /// vertex buffers, this always uses the <see cref="DataWriteOptions.Discard"/> flag.
    /// </summary>
    /// <param name="renderContext">The current render context.</param> 
    /// <param name="data">Span of buffers representing each vertex attribute whose data will be written to the vertex buffer.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if data is null.</exception>
    /// <exception cref="System.ArgumentOutOfRangeException">Thrown if the number of buffers do not match the number
    /// of vertex elements, or if the number of vertices in each buffer does not match the vertex count,
    /// or if there is a byte size mismatch of any kind.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if attempting to write to an immutable resource.</exception>
    public void SetInterleavedData(IRenderContext renderContext, ReadOnlySpan<IReadOnlyDataBuffer> data)
    {
      D3D11.DeviceContext d3dContext = Direct3DHelper.GetD3DDeviceContext(renderContext);

      ResourceHelper.WriteInterleavedVertexData(m_nativeBuffer, d3dContext, m_vertexCount, m_vertexLayout, m_resourceUsage, data, false);
    }

    /// <summary>
    /// Reads data from the vertex buffer into the specified data buffer.
    /// </summary>
    /// <typeparam name="T">Type of data to read from the vertex buffer.</typeparam>
    /// <param name="data">Buffer to hold data copied from the vertex buffer.</param>
    /// <param name="offsetInBytes">Offset in bytes from the beginning of the vertex buffer to the data.</param>
    /// <param name="vertexStride">Vertex step size used to advance within the GPU buffer when iterating over individual vertex attributes. Specify 0 or the size specified in the vertex layout for most copy operations.</param>
    /// <exception cref="ArgumentNullException">Thrown if the buffer is empty.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if any of the count or offsets are out of range, or if the vertex stride is too small, or would cause an overflow in the copy operation.</exception>
    public void GetData<T>(Span<T> data, int offsetInBytes, int vertexStride) where T : unmanaged
    {
      D3D11.DeviceContext d3dContext = m_device.ImmediateContext;

      ResourceHelper.ReadVertexData<T>(m_nativeBuffer, d3dContext, m_vertexCount, m_vertexLayout, m_resourceUsage, data, offsetInBytes, vertexStride);
    }

    /// <inheritdoc />
    void IBufferImpl.GetData<T>(Span<T> data, int offsetInBytes)
    {
      GetData<T>(data, offsetInBytes, 0);
    }

    /// <summary>
    /// Writes data to the vertex buffer from the specified data buffer.
    /// </summary>
    /// <typeparam name="T">Type of data to write to the vertex buffer.</typeparam>
    /// <param name="renderContext">The current render context.</param>
    /// <param name="data">Buffer with the data to be copied to the vertex buffer.</param>
    /// <param name="offsetInBytes">Offset in bytes from the beginning of the vertex buffer to the data.</param>
    /// <param name="vertexStride">Vertex step size used to advance within the GPU buffer when iterating over individual vertex attributes. Specify 0 or the size specified in the vertex layout for most copy operations.</param>
    /// <param name="writeOptions">Writing options, valid only for dynamic vertex buffers.</param>
    /// <exception cref="ArgumentNullException">Thrown if the buffer is empty.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if any of the count or offsets are out of range, or if the vertex stride is too small, or would cause an overflow in the copy operation.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if attempting to write to an immutable resource.</exception>
    public void SetData<T>(IRenderContext renderContext, ReadOnlySpan<T> data, int offsetInBytes, int vertexStride, DataWriteOptions writeOptions) where T : unmanaged
    {
      D3D11.DeviceContext d3dContext = Direct3DHelper.GetD3DDeviceContext(renderContext);

      ResourceHelper.WriteVertexData<T>(m_nativeBuffer, d3dContext, m_vertexCount, m_vertexLayout, m_resourceUsage, data, offsetInBytes, vertexStride, writeOptions);
    }

    /// <inheritdoc />
    void IBufferImpl.SetData<T>(IRenderContext renderContext, ReadOnlySpan<T> data, int offsetInBytes, DataWriteOptions writeOptions)
    {
      SetData<T>(renderContext, data, offsetInBytes, 0, writeOptions);
    }

    /// <summary>
    /// Called when the name of the graphics resource is changed, useful if the implementation wants to set the name to
    /// be used as a debug name.
    /// </summary>
    /// <param name="name">New name of the resource</param>
    protected override void OnDebugNameChange(String name)
    {
      if (m_nativeBuffer is not null)
        m_nativeBuffer.DebugName = name;

      if (m_shaderResourceView is not null)
        m_shaderResourceView.DebugName = (String.IsNullOrEmpty(name)) ? name : name + "_SRV";
    }

    /// <summary>
    /// Releases unmanaged and - optionally - managed resources.
    /// </summary>
    /// <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
    protected override void Dispose(bool disposing)
    {
      if (!IsDisposed)
      {
        if (disposing)
        {
          if (m_shaderResourceView is not null)
          {
            m_shaderResourceView.Dispose();
            m_shaderResourceView = null;
          }

          if (m_nativeBuffer is not null)
          {
            m_nativeBuffer.Dispose();
            m_nativeBuffer = null!;
          }
        }
      }

      base.Dispose(disposing);
    }
  }
}
