﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;
using Tesla.Graphics;
using Tesla.Graphics.Implementation;
using D3D11 = SharpDX.Direct3D11;
using DXGI = SharpDX.DXGI;
using SDXMI = SharpDX.Mathematics.Interop;

#nullable enable

namespace Tesla.Direct3D11.Graphics.Implementation
{
  /// <summary>
  /// A factory that creates Direct3D11 implementations of type <see cref="ISwapChainImpl"/>.
  /// </summary>
  public sealed class D3D11SwapChainImplFactory : D3D11GraphicsResourceImplFactory, ISwapChainImplFactory
  {

    /// <summary>
    /// Constructs a new instance of the <see cref="D3D11SwapChainImplFactory"/> class.
    /// </summary>
    public D3D11SwapChainImplFactory() : base(typeof(SwapChain)) { }

    /// <summary>
    /// Creates a new implementation object instance.
    /// </summary>
    /// <param name="windowHandle">The handle to the window the swap chain is bounded to and which the swap chain presents to.</param>
    /// <param name="presentParams">Presentation parameters defining how the swap chain should be setup.</param>
    /// <returns>The swap chain implementation.</returns>
    public ISwapChainImpl CreateImplementation(IntPtr windowHandle, PresentationParameters presentParams)
    {
      return new D3D11SwapChainImpl(D3D11RenderSystem, GetNextUniqueResourceID(), windowHandle, presentParams);
    }

    /// <inheritdoc />
    protected override void OnInitialize(D3D11RenderSystem renderSystem)
    {
      RegisterFactory<ISwapChainImplFactory>(this);
    }
  }

  /// <summary>
  /// A Direct3D11 implementation for <see cref="SwapChain"/>.
  /// </summary>
  public sealed class D3D11SwapChainImpl : GraphicsResourceImpl, ISwapChainImpl, ID3D11SwapChain
  {
    private D3D11.Device m_device;
    private DXGI.SwapChain m_nativeSwapChain;
    private D3D11.RenderTargetView m_renderTargetView;
    private D3D11.DepthStencilView? m_depthStencilView;
    private D3D11.Texture2D m_backBuffer;
    private IntPtr m_windowHandle;
    private PresentationParameters m_presentParams;
    private bool m_resetting;

    /// <inheritdoc />
    public event TypedEventHandler<ID3D11Backbuffer, EventArgs>? OnResetResize;

    /// <inheritdoc />
    public PresentationParameters PresentationParameters
    {
      get
      {
        return m_presentParams;
      }
    }

    /// <inheritdoc />
    public DisplayMode CurrentDisplayMode
    {
      get
      {
        DXGI.ModeDescription modeDesc = m_nativeSwapChain.Description.ModeDescription;
        return new DisplayMode(modeDesc.Width, modeDesc.Height, modeDesc.RefreshRate.Numerator, Direct3DHelper.FromD3DSurfaceFormat(modeDesc.Format));
      }
    }

    /// <inheritdoc />
    public IntPtr WindowHandle
    {
      get
      {
        return m_windowHandle;
      }
    }

    /// <inheritdoc />
    public IntPtr MonitorHandle
    {
      get
      {
        using (DXGI.Output output = m_nativeSwapChain.ContainingOutput)
          return output.Description.MonitorHandle;
      }
    }

    /// <inheritdoc />
    public bool IsFullScreen
    {
      get
      {
        return m_presentParams.IsFullScreen;
      }
    }

    /// <inheritdoc />
    public bool IsWideScreen
    {
      get
      {
        return CurrentDisplayMode.AspectRatio > 1.6f;
      }
    }

    /// <summary>
    /// Gets the native D3D11 device.
    /// </summary>
    public D3D11.Device D3DDevice
    {
      get
      {
        return m_device;
      }
    }

    /// <summary>
    /// Gets the native DXGI device.
    /// </summary>
    public DXGI.SwapChain DXGISwapChain
    {
      get
      {
        return m_nativeSwapChain;
      }
    }

    /// <summary>
    /// Gets the native D3D11 texture that represents the backbuffer.
    /// </summary>
    public D3D11.Texture2D D3DTexture2D
    {
      get
      {
        return m_backBuffer;
      }
    }

    /// <inheritdoc />
    public D3D11.RenderTargetView D3DRenderTargetView
    {
      get
      {
        return m_renderTargetView;
      }
    }

    /// <inheritdoc />
    public D3D11.DepthStencilView? D3DDepthStencilView
    {
      get
      {
        return m_depthStencilView;
      }
    }

    /// <inheritdoc />
    D3D11.DepthStencilView? ID3D11DepthStencilView.D3DReadOnlyDepthStencilView
    {
      get
      {
        return null;
      }
    }

    internal D3D11SwapChainImpl(D3D11RenderSystem renderSystem, int resourceID, IntPtr windowHandle, PresentationParameters presentParams)
        : base(renderSystem, resourceID)
    {

      m_device = renderSystem.D3DDevice;

      m_windowHandle = windowHandle;
      m_presentParams = presentParams;
      m_resetting = false;

      CreateSwapChain(m_windowHandle, m_presentParams);
      CreateViews();

      SetDefaultDebugName("SwapChain");
    }

    public void Clear(IRenderContext renderContext, ClearOptions clearOptions, Color color, float depth, int stencil)
    {
      D3D11.DeviceContext nativeContext = Direct3DHelper.GetD3DDeviceContext(renderContext);

      Clear(nativeContext, clearOptions, color, depth, stencil);
    }

    public void GetBackBufferData<T>(Span<T> data, ResourceRegion2D? subimage) where T : unmanaged
    {
      D3D11.DeviceContext nativeContext = m_device.ImmediateContext;
      D3D11.Texture2D resource = m_backBuffer;
      bool createdResolve = false;

      //If MSAA, need to first resolve it into a temporary texture before copying the data.
      if (m_presentParams.MultiSampleCount > 1)
      {
        D3D11.Texture2DDescription desc = m_backBuffer.Description;
        desc.SampleDescription.Count = 1;
        desc.SampleDescription.Quality = 0;
        desc.BindFlags = D3D11.BindFlags.None;
        desc.CpuAccessFlags = D3D11.CpuAccessFlags.None;
        desc.Usage = D3D11.ResourceUsage.Default;

        resource = new D3D11.Texture2D(m_device, desc);
        createdResolve = true;

        nativeContext.ResolveSubresource(m_backBuffer, 0, resource, 0, desc.Format);
      }

      ResourceRegion3D? region;
      if (subimage.HasValue)
      {
        region = new ResourceRegion3D(subimage.Value);
      }
      else
      {
        region = new ResourceRegion3D(0, m_presentParams.BackBufferWidth, 0, m_presentParams.BackBufferHeight, 0, 1);
      }

      ResourceHelper.ReadTextureData<T>(resource, nativeContext, m_presentParams.BackBufferWidth, m_presentParams.BackBufferHeight, 1, 1, 1, m_presentParams.BackBufferFormat, ResourceUsage.Static, false,
          data, SubResourceAt.Zero, region);

      if (createdResolve)
        resource.Dispose();
    }

    public void Present()
    {
      if (m_resetting)
        return;

      m_nativeSwapChain.Present((int) m_presentParams.PresentInterval, DXGI.PresentFlags.None);
    }

    public void Reset(IntPtr windowHandle, PresentationParameters presentParams)
    {
      if (m_resetting)
        return;

      m_resetting = true;

      DestroyViews();
      DestroySwapChain();
      m_windowHandle = windowHandle;
      m_presentParams = presentParams;
      CreateSwapChain(m_windowHandle, m_presentParams);
      CreateViews();

      //Be sure to set the name to the new objects
      if (!String.IsNullOrEmpty(DebugName))
        OnDebugNameChange(DebugName);

      TypedEventHandler<ID3D11Backbuffer, EventArgs>? evt = OnResetResize;
      if (evt is not null)
        evt(this, EventArgs.Empty);

      m_resetting = false;
    }

    public void Resize(int width, int height)
    {
      if (m_resetting || m_presentParams.BackBufferWidth == width && m_presentParams.BackBufferHeight == height)
        return;

      EngineLog.Log(LogLevel.Debug, "{0}***IN SWAPCHAIN RESIZE, W:{1}, H:{2}***", DateTime.Now.ToString(), width.ToString(), height.ToString());

      m_presentParams.BackBufferWidth = width;
      m_presentParams.BackBufferHeight = height;

      DXGI.SwapChainFlags flags = DXGI.SwapChainFlags.None;
      if (m_presentParams.IsFullScreen)
        flags = DXGI.SwapChainFlags.AllowModeSwitch;

      DestroyViews();
      m_nativeSwapChain.ResizeBuffers(1, width, height, Direct3DHelper.ToD3DSurfaceFormat(m_presentParams.BackBufferFormat), flags);
      CreateViews();

      //Be sure to set the name to the new objects
      if (!String.IsNullOrEmpty(DebugName))
        OnDebugNameChange(DebugName);

      TypedEventHandler<ID3D11Backbuffer, EventArgs>? evt = OnResetResize;
      if (evt is not null)
        evt(this, EventArgs.Empty);
    }

    public bool ToggleFullScreen()
    {
      m_presentParams.IsFullScreen = !m_presentParams.IsFullScreen;

      DXGI.ModeDescription modeDesc = new DXGI.ModeDescription();
      modeDesc.Format = Direct3DHelper.ToD3DSurfaceFormat(m_presentParams.BackBufferFormat);
      modeDesc.Width = m_presentParams.BackBufferWidth;
      modeDesc.Height = m_presentParams.BackBufferHeight;
      modeDesc.RefreshRate = new DXGI.Rational(60, 1);

      //For resizing
      //int width = m_presentParams.BackBufferWidth;
      //int height = m_presentParams.BackBufferHeight;

      //Set full screen mode
      if (m_presentParams.IsFullScreen)
      {
        m_nativeSwapChain.ResizeTarget(ref modeDesc);

        using (DXGI.Output output = m_nativeSwapChain.ContainingOutput)
        {
          m_nativeSwapChain.SetFullscreenState(true, output);
        }

        //This would be an EXTRA resize I think, we should respond to the window's WM_SIZE message to resize, which gets sent because of ResizeTarget
        //Resize(width, height);
      }
      //Set to windowed
      else
      {
        modeDesc.RefreshRate = new DXGI.Rational(0, 0);
        m_nativeSwapChain.IsFullScreen = false;

        //This would be an EXTRA resize I think, we should respond to the window's WM_SIZE message to resize, which gets sent because of ResizeTarget
        //Resize(width, height);

        m_nativeSwapChain.ResizeTarget(ref modeDesc);
      }

      return m_presentParams.IsFullScreen;
    }

    public void Clear(D3D11.DeviceContext deviceContext, ClearOptions options, float depth, int stencil)
    {
      Clear(deviceContext, options, Color.Black, depth, stencil);
    }

    public void Clear(D3D11.DeviceContext deviceContext, ClearOptions options, Color color, float depth, int stencil)
    {
      if (m_renderTargetView is not null && ((options & ClearOptions.Target) == ClearOptions.Target))
      {
        SDXMI.RawColor4 sdxColor;
        Direct3DHelper.ConvertColor(ref color, out sdxColor);
        deviceContext.ClearRenderTargetView(m_renderTargetView, sdxColor);
      }

      bool clearDepth = (options & ClearOptions.Depth) == ClearOptions.Depth;
      bool clearStencil = (options & ClearOptions.Stencil) == ClearOptions.Stencil;

      if (m_depthStencilView is not null && (clearDepth || clearStencil))
      {
        D3D11.DepthStencilClearFlags clearFlags;

        if (clearDepth && clearStencil)
        {
          clearFlags = D3D11.DepthStencilClearFlags.Depth | D3D11.DepthStencilClearFlags.Stencil;
        }
        else if (clearDepth)
        {
          clearFlags = D3D11.DepthStencilClearFlags.Depth;
        }
        else
        {
          clearFlags = D3D11.DepthStencilClearFlags.Stencil;
        }

        deviceContext.ClearDepthStencilView(m_depthStencilView, clearFlags, depth, (byte) stencil);
      }
    }

    public void NotifyOnFirstBind()
    {
      //No-op
    }

    public void ResolveResource(D3D11.DeviceContext deviceContext)
    {
      //No-op
    }

    protected override void OnDebugNameChange(String name)
    {
      if (m_backBuffer is not null)
        m_backBuffer.DebugName = name;

      if (m_renderTargetView is not null)
        m_renderTargetView.DebugName = (String.IsNullOrEmpty(name)) ? name : name + "_RTV";

      if (m_depthStencilView is not null)
        m_depthStencilView.DebugName = (String.IsNullOrEmpty(name)) ? name : name + "_DSV";

      if (m_nativeSwapChain is not null)
        m_nativeSwapChain.DebugName = (String.IsNullOrEmpty(name)) ? name : name + "_Backbuffer";
    }

    protected override void Dispose(bool disposing)
    {
      if (!IsDisposed)
      {
        if (disposing)
        {
          DestroyViews();
          DestroySwapChain();
        }

        base.Dispose(disposing);
      }
    }

    [MemberNotNull(nameof(m_nativeSwapChain))]
    private void CreateSwapChain(IntPtr windowHandle, PresentationParameters presentParams)
    {

      DXGI.SwapEffect swapEffect;
      DXGI.Format format = Direct3DHelper.ToD3DSurfaceFormat(presentParams.BackBufferFormat);
      DXGI.SampleDescription sampleDesc = new DXGI.SampleDescription(presentParams.MultiSampleCount, presentParams.MultiSampleQuality);
      bool multiSampleEnabled = sampleDesc.Count > 1;

      switch (presentParams.RenderTargetUsage)
      {
        case RenderTargetUsage.PreserveContents:
          if (multiSampleEnabled)
          {
            swapEffect = DXGI.SwapEffect.Discard;
          }
          else
          {
            swapEffect = DXGI.SwapEffect.Sequential;
          }
          break;
        default:
          swapEffect = DXGI.SwapEffect.Discard;
          break;
      }

      DXGI.ModeDescription modeDesc = new DXGI.ModeDescription(presentParams.BackBufferWidth, presentParams.BackBufferHeight, new DXGI.Rational(60, 1), format);

      DXGI.SwapChainDescription swapDesc = new DXGI.SwapChainDescription();
      swapDesc.BufferCount = 1;
      swapDesc.Flags = DXGI.SwapChainFlags.AllowModeSwitch;
      swapDesc.IsWindowed = true;
      swapDesc.ModeDescription = modeDesc;
      swapDesc.OutputHandle = windowHandle;
      swapDesc.SampleDescription = sampleDesc;
      swapDesc.SwapEffect = swapEffect;
      swapDesc.Usage = DXGI.Usage.RenderTargetOutput;

      DXGI.Factory factory = (RenderSystem as D3D11RenderSystem)!.DXGIFactory;
      factory.MakeWindowAssociation(windowHandle, DXGI.WindowAssociationFlags.IgnoreAll | DXGI.WindowAssociationFlags.IgnoreAltEnter);
      m_nativeSwapChain = new DXGI.SwapChain(factory, m_device, swapDesc);

      if (m_presentParams.IsFullScreen)
      {
        m_nativeSwapChain.ResizeTarget(ref modeDesc);

        using (DXGI.Output output = m_nativeSwapChain.ContainingOutput)
        {
          m_nativeSwapChain.SetFullscreenState(true, output);
        }

        m_nativeSwapChain.ResizeBuffers(1, modeDesc.Width, modeDesc.Height, modeDesc.Format, DXGI.SwapChainFlags.AllowModeSwitch);
      }
    }

    [MemberNotNull(nameof(m_backBuffer), nameof(m_renderTargetView))]
    private void CreateViews()
    {
      m_backBuffer = D3D11.Resource.FromSwapChain<D3D11.Texture2D>(m_nativeSwapChain, 0);

      D3D11.RenderTargetViewDescription rtvDesc = new D3D11.RenderTargetViewDescription();
      rtvDesc.Dimension = (m_presentParams.MultiSampleCount > 1) ? D3D11.RenderTargetViewDimension.Texture2DMultisampled : D3D11.RenderTargetViewDimension.Texture2D;
      rtvDesc.Format = Direct3DHelper.ToD3DSurfaceFormat(m_presentParams.BackBufferFormat);
      rtvDesc.Texture2D.MipSlice = 0;

      m_renderTargetView = new D3D11.RenderTargetView(m_device, m_backBuffer, rtvDesc);

      if (m_presentParams.DepthStencilFormat != DepthFormat.None)
      {
        D3D11.Texture2DDescription depthBufferDesc = new D3D11.Texture2DDescription();
        depthBufferDesc.ArraySize = 1;
        depthBufferDesc.BindFlags = D3D11.BindFlags.DepthStencil;
        depthBufferDesc.CpuAccessFlags = D3D11.CpuAccessFlags.None;
        depthBufferDesc.Format = Direct3DHelper.ToD3DDepthFormat(m_presentParams.DepthStencilFormat);
        depthBufferDesc.Width = m_presentParams.BackBufferWidth;
        depthBufferDesc.Height = m_presentParams.BackBufferHeight;
        depthBufferDesc.MipLevels = 1;
        depthBufferDesc.OptionFlags = D3D11.ResourceOptionFlags.None;
        depthBufferDesc.SampleDescription = m_backBuffer.Description.SampleDescription;
        depthBufferDesc.Usage = D3D11.ResourceUsage.Default;

        using (D3D11.Texture2D depthBuffer = new D3D11.Texture2D(m_device, depthBufferDesc))
        {
          m_depthStencilView = new D3D11.DepthStencilView(m_device, depthBuffer);
        }
      }
    }

    private void DestroySwapChain()
    {
      if (m_nativeSwapChain is not null)
      {
        if (m_presentParams.IsFullScreen)
          m_nativeSwapChain.SetFullscreenState(false, null);

        m_nativeSwapChain.Dispose();
        m_nativeSwapChain = null!;
      }
    }

    private void DestroyViews()
    {
      if (m_renderTargetView is not null)
      {
        m_renderTargetView.Dispose();
        m_renderTargetView = null!;
      }

      if (m_depthStencilView is not null)
      {
        m_depthStencilView.Dispose();
        m_depthStencilView = null!;
      }

      if (m_backBuffer is not null)
      {
        m_backBuffer.Dispose();
        m_backBuffer = null!;
      }
    }
  }
}
