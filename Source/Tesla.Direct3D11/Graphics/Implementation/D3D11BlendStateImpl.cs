﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Graphics;
using Tesla.Graphics.Implementation;
using D3D = SharpDX.Direct3D;
using D3D11 = SharpDX.Direct3D11;

#nullable enable

namespace Tesla.Direct3D11.Graphics.Implementation
{
  /// <summary>
  /// A factory that creates Direct3D11 implementations of type <see cref="IBlendStateImpl"/>.
  /// </summary>
  public sealed class D3D11BlendStateImplFactory : D3D11GraphicsResourceImplFactory, IBlendStateImplFactory
  {
    /// <summary>
    /// Constructs a new instance of the <see cref="D3D11BlendStateImplFactory"/> class.
    /// </summary>
    public D3D11BlendStateImplFactory() : base(typeof(BlendState)) { }

    /// <summary>
    /// Creates a new implementation object instance.
    /// </summary>
    /// <returns>The render state implementation.</returns>
    public IBlendStateImpl CreateImplementation()
    {
      return new D3D11BlendStateImpl(D3D11RenderSystem, GetNextUniqueResourceID());
    }

    /// <summary>
    /// Called when the factory is initialized.
    /// </summary>
    /// <param name="renderSystem">The D3D11 render system.</param>
    protected override void OnInitialize(D3D11RenderSystem renderSystem)
    {
      RegisterFactory<IBlendStateImplFactory>(this);
    }
  }

  /// <summary>
  /// A Direct3D11 implementation for <see cref="BlendState"/>.
  /// </summary>
  public sealed class D3D11BlendStateImpl : BlendStateImpl, ID3D11BlendState
  {
    private D3D11.Device m_device;
    private D3D11.BlendState? m_nativeBlendState;
    private bool m_alphaToCoverageSupported;
    private bool m_independentBlendSupported;

    /// <summary>
    /// Checks if alpha-to-coverage is supported. This can vary by implementation.
    /// </summary>
    public override bool IsAlphaToCoverageSupported
    {
      get
      {
        return m_alphaToCoverageSupported;
      }
    }

    /// <summary>
    /// Checks if independent blending of multiple render targets (MRT) is supported. This can vary by implementation. If not supported, then the blending options
    /// specified for the first render target index are used for all other bound render targets, if those targets blend are enabled.
    /// </summary>
    public override bool IsIndependentBlendSupported
    {
      get
      {
        return m_independentBlendSupported;
      }
    }

    /// <summary>
    /// Gets the native D3D11 device.
    /// </summary>
    public D3D11.Device D3DDevice
    {
      get
      {
        return m_device;
      }
    }

    /// <summary>
    /// Gets the native D3D11 blend state.
    /// </summary>
    public D3D11.BlendState D3DBlendState
    {
      get
      {
        return m_nativeBlendState!;
      }
    }

    internal D3D11BlendStateImpl(D3D11RenderSystem renderSystem, int resourceID)
        : base(renderSystem, resourceID)
    {

      m_device = renderSystem.D3DDevice;
      CheckSupport(m_device, out m_alphaToCoverageSupported, out m_independentBlendSupported);

      SetDefaultDebugName("BlendState");
    }

    /// <summary>
    /// Called when the state is bound, signaling the implementation to create and bind the native state.
    /// </summary>
    protected override void CreateNativeState()
    {
      D3D11.BlendStateDescription desc = new D3D11.BlendStateDescription();
      desc.AlphaToCoverageEnable = (IsAlphaToCoverageSupported) ? AlphaToCoverageEnable : false;
      desc.IndependentBlendEnable = (IsIndependentBlendSupported) ? IndependentBlendEnable : false;

      D3D11.RenderTargetBlendDescription[] rtDescs = desc.RenderTarget;
      for (int i = 0; i < rtDescs.Length; i++)
      {
        RenderTargetBlendDescription blendDesc;
        GetRenderTargetBlendDescription(i, out blendDesc);

        D3D11.RenderTargetBlendDescription d3dBlendDesc;
        Direct3DHelper.ConvertRenderTargetBlendDescription(ref blendDesc, out d3dBlendDesc);

        rtDescs[i] = d3dBlendDesc;
      }

      m_nativeBlendState = new D3D11.BlendState(m_device, desc);
      m_nativeBlendState.DebugName = DebugName;
    }

    /// <summary>
    /// Called when the name of the graphics resource is changed, useful if the implementation wants to set the name to
    /// be used as a debug name.
    /// </summary>
    /// <param name="name">New name of the resource</param>
    protected override void OnDebugNameChange(string name)
    {
      if (m_nativeBlendState is not null)
        m_nativeBlendState.DebugName = name;
    }

    /// <summary>
    /// Releases unmanaged and - optionally - managed resources.
    /// </summary>
    /// <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
    protected override void Dispose(bool disposing)
    {
      if (!IsDisposed)
      {
        if (disposing)
        {
          if (m_nativeBlendState is not null)
          {
            m_nativeBlendState.Dispose();
            m_nativeBlendState = null;
          }
        }

        base.Dispose(disposing);
      }
    }

    private void CheckSupport(D3D11.Device device, out bool alphaToCoverageSupport, out bool independentBlendSupport)
    {
      alphaToCoverageSupport = false;
      independentBlendSupport = false;

      switch (device.FeatureLevel)
      {
        case D3D.FeatureLevel.Level_11_1:
        case D3D.FeatureLevel.Level_11_0:
        case D3D.FeatureLevel.Level_10_1:
        case D3D.FeatureLevel.Level_10_0:
          alphaToCoverageSupport = true;
          independentBlendSupport = true;
          break;
      }
    }
  }
}
