﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Graphics;
using Tesla.Graphics.Implementation;
using D3D11 = SharpDX.Direct3D11;

#nullable enable

namespace Tesla.Direct3D11.Graphics.Implementation
{
  /// <summary>
  /// A factory that creates Direct3D11 implementations of type <see cref="IOcclusionQueryImpl"/>.
  /// </summary>
  public sealed class D3D11OcclusionQueryImplFactory : D3D11GraphicsResourceImplFactory, IOcclusionQueryImplFactory
  {
    /// <summary>
    /// Constructs a new instance of the <see cref="D3D11OcclusionQueryImplFactory"/> class.
    /// </summary>
    public D3D11OcclusionQueryImplFactory() : base(typeof(OcclusionQuery)) { }

    /// <summary>
    /// Creates a new implementation object instance.
    /// </summary>
    /// <returns>The occlusion query implementation.</returns>
    public IOcclusionQueryImpl CreateImplementation()
    {
      return new D3D11OcclusionQueryImpl(D3D11RenderSystem, GetNextUniqueResourceID());
    }

    /// <summary>
    /// Called when the factory is initialized.
    /// </summary>
    /// <param name="renderSystem">The D3D11 render system.</param>
    protected override void OnInitialize(D3D11RenderSystem renderSystem)
    {
      RegisterFactory<IOcclusionQueryImplFactory>(this);
    }
  }

  /// <summary>
  /// A Direct3D11 implementation for <see cref="OcclusionQuery"/>.
  /// </summary>
  public sealed class D3D11OcclusionQueryImpl : GraphicsResourceImpl, IOcclusionQueryImpl
  {
    private D3D11.Device m_device;
    private D3D11.Query m_nativeQuery;
    private D3D11.DeviceContext? m_context;
    private bool m_inQuery;
    private bool m_queryComplete;
    private long m_pixelCount;

    /// <summary>
    /// Gets if the query has been completed or not.
    /// </summary>
    public bool IsComplete
    {
      get
      {
        if (m_queryComplete)
          return true;

        if (m_context is null)
          return false;

        return m_context.IsDataAvailable(m_nativeQuery);
      }
    }

    /// <summary>
    /// Gets the number of visible pixels, after the query has been completed.
    /// </summary>
    public long PixelCount
    {
      get
      {
        if (!m_queryComplete)
          return 0;

        return m_pixelCount;
      }
    }

    /// <summary>
    /// Gets the native D3D11 device.
    /// </summary>
    public D3D11.Device D3DDevice
    {
      get
      {
        return m_device;
      }
    }

    /// <summary>
    /// Gets the native D3D11 query.
    /// </summary>
    public D3D11.Query D3DQuery
    {
      get
      {
        return m_nativeQuery;
      }
    }

    internal D3D11OcclusionQueryImpl(D3D11RenderSystem renderSystem, int resourceID)
        : base(renderSystem, resourceID)
    {

      D3D11.QueryDescription desc = new D3D11.QueryDescription();
      desc.Flags = D3D11.QueryFlags.None;
      desc.Type = D3D11.QueryType.Occlusion;

      m_device = renderSystem.D3DDevice;
      m_nativeQuery = new D3D11.Query(m_device, desc);
      m_inQuery = false;
      m_queryComplete = false;
      m_pixelCount = 0;
      m_context = null;

      SetDefaultDebugName("OcclusionQuery");
    }

    /// <summary>
    /// Begins the query.
    /// </summary>
    /// <exception cref="TeslaGraphicsException">Thrown if Begin() was already previously called.</exception>
    public void Begin()
    {
      if (m_inQuery)
        throw new TeslaGraphicsException(StringLocalizer.Instance.GetLocalizedString("AlreadyCalledBeginOnQuery"));

      m_queryComplete = false;
      m_pixelCount = 0;
      m_context = m_device.ImmediateContext;
      m_context.Begin(m_nativeQuery);

      m_inQuery = true;
    }

    /// <summary>
    /// Ends the query.
    /// </summary>
    /// <exception cref="TeslaGraphicsException">Thrown if Begin() was not previously called.</exception>
    public void End()
    {
      if (!m_inQuery || m_context is null)
        throw new TeslaGraphicsException(StringLocalizer.Instance.GetLocalizedString("BeginNotCalledYet"));

      m_context.End(m_nativeQuery);

      m_inQuery = false;
      m_queryComplete = true;
      m_pixelCount = m_context.GetData<long>(m_nativeQuery);
      m_context = null;
    }

    /// <summary>
    /// Called when the name of the graphics resource is changed, useful if the implementation wants to set the name to
    /// be used as a debug name.
    /// </summary>
    /// <param name="name">New name of the resource</param>
    protected override void OnDebugNameChange(string name)
    {
      if (m_nativeQuery is not null)
        m_nativeQuery.DebugName = name;
    }

    /// <summary>
    /// Releases unmanaged and - optionally - managed resources.
    /// </summary>
    /// <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
    protected override void Dispose(bool disposing)
    {
      if (!IsDisposed)
      {
        if (disposing)
        {
          if (m_nativeQuery is not null)
          {
            m_nativeQuery.Dispose();
            m_nativeQuery = null!;
          }
        }

        base.Dispose(disposing);
      }
    }
  }
}
