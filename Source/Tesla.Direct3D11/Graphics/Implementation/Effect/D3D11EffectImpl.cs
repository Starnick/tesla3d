﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System.Collections.Generic;
using Tesla.Graphics;
using Tesla.Graphics.Implementation;
using D3D11 = SharpDX.Direct3D11;

namespace Tesla.Direct3D11.Graphics.Implementation
{
  /// <summary>
  /// A factory that creates Direct3D11 implementations of type <see cref="IEffectImpl"/>.
  /// </summary>
  public sealed class D3D11EffectImplFactory : D3D11GraphicsResourceImplFactory, IEffectImplFactory
  {
    public D3D11EffectImplFactory() : base(typeof(Effect)) { }

    /// <summary>
    /// Creates a new implementation object instance.
    /// </summary>
    /// <param name="shaderByteCode">Compiled shader byte code.</param>
    /// <returns>The effect implementation.</returns>
    public IEffectImpl CreateImplementation(byte[] shaderByteCode)
    {
      return new D3D11EffectImpl(D3D11RenderSystem, GetNextUniqueResourceID(), shaderByteCode);
    }

    /// <summary>
    /// Called when the factory is initialized.
    /// </summary>
    /// <param name="renderSystem">The D3D11 render system.</param>
    protected override void OnInitialize(D3D11RenderSystem renderSystem)
    {
      RegisterFactory<IEffectImplFactory>(this);
    }
  }

  /// <summary>
  /// A Direct3D11 implementation for <see cref="Effect"/>.
  /// </summary>
  public sealed class D3D11EffectImpl : GraphicsResourceImpl, IEffectImpl
  {
    private D3D11.Device m_device;
    private int m_sortKey;
    private EffectConstantBufferCollection m_constantBuffers;
    private EffectParameterCollection m_parameters;
    private EffectShaderGroupCollection m_shaderGroups;
    private D3D11EffectShaderGroup m_currentShaderGroup;
    private EffectData m_effectData;
    private D3D11EffectResourceManager m_resourceManager;
    private byte[] m_byteCode;

    /// <summary>
    /// Occurs when an effect shader group that is contained by this effect is about to be applied to a render context.
    /// </summary>
    public event OnApplyDelegate OnShaderGroupApply;

    /// <summary>
    /// Gets the effect sort key, used to compare effects as a first step in sorting objects to render. The sort key is the same
    /// for cloned effects. Further sorting can then be done using the indices of the contained techniques, and the indices of their passes.
    /// </summary>
    public int SortKey
    {
      get
      {
        return m_sortKey;
      }
    }

    /// <summary>
    /// Gets or sets the currently active shader group.
    /// </summary>
    public IEffectShaderGroup CurrentShaderGroup
    {
      get
      {
        return m_currentShaderGroup;
      }
      set
      {
        m_currentShaderGroup = value as D3D11EffectShaderGroup;
      }
    }

    /// <summary>
    /// Gets the shader groups contained in this effect.
    /// </summary>
    public EffectShaderGroupCollection ShaderGroups
    {
      get
      {
        return m_shaderGroups;
      }
    }

    /// <summary>
    /// Gets all effect parameters used by all passes.
    /// </summary>
    public EffectParameterCollection Parameters
    {
      get
      {
        return m_parameters;
      }
    }

    /// <summary>
    /// Gets all constant buffers that contain all value type parameters used by all passes.
    /// </summary>
    public EffectConstantBufferCollection ConstantBuffers
    {
      get
      {
        return m_constantBuffers;
      }
    }

    /// <summary>
    /// Gets the compiled effect byte code that represents this effect.
    /// </summary>
    public byte[] EffectByteCode
    {
      get
      {
        return m_byteCode;
      }
    }

    /// <summary>
    /// Gets the native D3D11 device.
    /// </summary>
    public D3D11.Device D3DDevice
    {
      get
      {
        return m_device;
      }
    }

    internal D3D11EffectResourceManager Resourcemanager
    {
      get
      {
        return m_resourceManager;
      }
    }

    internal EffectData EffectData
    {
      get
      {
        return m_effectData;
      }
    }

    internal D3D11EffectImpl(D3D11RenderSystem renderSystem, int resourceID, D3D11EffectImpl implToCloneFrom)
        : base(renderSystem, resourceID)
    {
      m_device = renderSystem.D3DDevice;
      m_byteCode = implToCloneFrom.m_byteCode;
      m_effectData = implToCloneFrom.m_effectData;
      m_resourceManager = implToCloneFrom.Resourcemanager.Clone();
      m_sortKey = implToCloneFrom.m_sortKey;

      Initialize(implToCloneFrom);
    }

    internal D3D11EffectImpl(D3D11RenderSystem renderSystem, int resourceID, byte[] byteCode)
        : base(renderSystem, resourceID)
    {
      m_device = renderSystem.D3DDevice;
      m_byteCode = byteCode;
      m_effectData = EffectData.Read(byteCode);
      m_resourceManager = new D3D11EffectResourceManager(m_effectData);
      m_sortKey = renderSystem.GetNextUniqueEffectSortKey();

      Initialize();
    }

    /// <summary>
    /// Clones the effect, and possibly sharing relevant underlying resources. Cloned instances are guaranteed to be
    /// completely separate from the source effect in terms of parameters, changing one will not change the other. But unlike
    /// creating a new effect from the same compiled byte code, native resources can still be shared more effectively.
    /// </summary>
    /// <returns>Cloned effect implementation.</returns>
    public IEffectImpl Clone()
    {
      D3D11RenderSystem renderSystem = RenderSystem as D3D11RenderSystem;
      D3D11EffectImpl clonedImpl = new D3D11EffectImpl(renderSystem, renderSystem.GetNextUniqueResourceID(), this);

      return clonedImpl;
    }

    /// <summary>
    /// Releases unmanaged and - optionally - managed resources.
    /// </summary>
    /// <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
    protected override void Dispose(bool disposing)
    {
      if (!IsDisposed)
      {
        if (disposing)
        {
          //Call release on the shader manager, last one turns off the lights and dispose of shared shader objects
          m_resourceManager.ShaderManager.Release();

          //Release constant buffers
          for (int i = 0; i < m_constantBuffers.Count; i++)
            (m_constantBuffers[i] as D3D11EffectConstantBuffer).Dispose();
        }
      }

      base.Dispose(disposing);
    }

    internal void OnPreApply(IRenderContext renderContext, IEffectShaderGroup shaderGroup)
    {
      OnApplyDelegate temp = OnShaderGroupApply;
      if (temp is not null)
        temp(renderContext, shaderGroup);
    }

    private void Initialize(D3D11EffectImpl implToCloneFrom = null)
    {
      //Setup constant buffers
      if (m_effectData.ConstantBuffers is not null && m_effectData.ConstantBuffers.Length > 0)
      {
        D3D11EffectConstantBuffer[] cbs = new D3D11EffectConstantBuffer[m_effectData.ConstantBuffers.Length];

        for (int i = 0; i < cbs.Length; i++)
        {
          D3D11EffectConstantBuffer cb = (implToCloneFrom is null) ? new D3D11EffectConstantBuffer(m_device, this, m_effectData.ConstantBuffers[i])
              : new D3D11EffectConstantBuffer(m_device, this, m_effectData.ConstantBuffers[i], implToCloneFrom.m_constantBuffers[i] as D3D11EffectConstantBuffer);

          cb.DebugName = $"{ResourceID}:{cb.Name}";
          cbs[i] = cb;
        }

        m_constantBuffers = new EffectConstantBufferCollection(cbs);
        m_resourceManager.SetConstantBuffers(m_constantBuffers);
      }
      else
      {
        m_constantBuffers = EffectConstantBufferCollection.EmptyCollection;
      }

      //Setup shader resources
      if (m_effectData.ResourceVariables is not null && m_effectData.ResourceVariables.Length > 0)
      {
        List<D3D11EffectParameter> variables = new List<D3D11EffectParameter>(m_effectData.ResourceVariables.Length);
        for (int i = 0, startResourceIndex = 0; i < m_effectData.ResourceVariables.Length; i++)
        {
          EffectData.ResourceVariable rvar = m_effectData.ResourceVariables[i];

          if (rvar.ResourceType != D3DShaderInputType.ConstantBuffer && rvar.ResourceType != D3DShaderInputType.TextureBuffer)
          {
            D3D11EffectParameter param = D3D11EffectParameter.CreateResourceVariable(this, m_resourceManager, startResourceIndex, rvar);

            //ONLY set the default sampler state if we are NOT being cloned, otherwise cloning the resource manager already took care of this!
            if (implToCloneFrom is null && param.ParameterType == EffectParameterType.SamplerState && rvar.SamplerData.HasValue)
              SetDefaultSamplerState(param, rvar);

            variables.Add(param);
          }

          startResourceIndex += (rvar.ElementCount == 0) ? 1 : rvar.ElementCount;
        }

        m_parameters = new EffectParameterCollection(new CompositeEffectParameterCollection(m_constantBuffers, variables));
      }
      else
      {
        m_parameters = new EffectParameterCollection(new CompositeEffectParameterCollection(m_constantBuffers, EffectParameterCollection.EmptyCollection));
      }

      //Setup shader groups
      if (m_effectData.ShaderGroups is not null && m_effectData.ShaderGroups.Length > 0)
      {
        D3D11EffectShaderGroup[] groups = new D3D11EffectShaderGroup[m_effectData.ShaderGroups.Length];

        if (implToCloneFrom is null)
        {
          for (int i = 0; i < groups.Length; i++)
            groups[i] = new D3D11EffectShaderGroup(this, m_effectData.ShaderGroups[i], i);
        }
        else
        {
          for (int i = 0; i < groups.Length; i++)
            groups[i] = new D3D11EffectShaderGroup(this, m_effectData.ShaderGroups[i], i, implToCloneFrom.m_shaderGroups[i] as D3D11EffectShaderGroup);
        }

        m_shaderGroups = new EffectShaderGroupCollection(groups);
        m_currentShaderGroup = m_shaderGroups[0] as D3D11EffectShaderGroup;
      }
      else
      {
        m_shaderGroups = EffectShaderGroupCollection.EmptyCollection;
        m_currentShaderGroup = null;
      }
    }

    private void SetDefaultSamplerState(D3D11EffectParameter param, EffectData.ResourceVariable data)
    {
      //Sampler arrays are not supported for the default value setting...maybe one day
      if (param.IsArray || !data.SamplerData.HasValue)
        return;

      EffectData.SamplerStateData ssData = data.SamplerData.Value;
      SamplerState ss = new SamplerState(RenderSystem);
      ss.AddressU = ssData.AddressU;
      ss.AddressV = ssData.AddressV;
      ss.AddressW = ssData.AddressW;
      ss.Filter = ssData.Filter;
      ss.MaxAnisotropy = ssData.MaxAnisotropy;
      ss.MinMipMapLevel = ssData.MinMipMapLevel;
      ss.MaxMipMapLevel = ssData.MaxMipMapLevel;
      ss.MipMapLevelOfDetailBias = ssData.MipMapLevelOfDetailBias;
      ss.BorderColor = ssData.BorderColor;
      ss.CompareFunction = ssData.ComparisonFunction;

      ss = RenderSystem.RenderStateCache.GetOrCache(ss);
      param.SetResource<SamplerState>(ss);
    }
  }
}
