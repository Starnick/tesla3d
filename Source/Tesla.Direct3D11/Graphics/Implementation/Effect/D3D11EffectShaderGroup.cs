﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics;
using Tesla.Graphics;

namespace Tesla.Direct3D11.Graphics.Implementation
{
  [DebuggerDisplay("Name = {Name}, Name = {Name}, ShaderGroupIndex = {ShaderGroupIndex}")]
  public sealed class D3D11EffectShaderGroup : IEffectShaderGroup
  {
    private static readonly int NumberOfStages = Enum.GetValues(typeof(ShaderStage)).Length;

    private String m_name;
    private int m_groupIndex;
    private D3D11EffectImpl m_impl;

    private EffectData.ShaderGroup m_groupData;
    private ShaderBlock[] m_shaderBlocks;
    private InputLayoutCache m_localLayoutCache;

    /// <summary>
    /// Gets the name of the effect part.
    /// </summary>
    public String Name
    {
      get
      {
        return m_name;
      }
    }

    /// <summary>
    /// Gets the index of this shader group in the collection it is contained in.
    /// </summary>
    public int ShaderGroupIndex
    {
      get
      {
        return m_groupIndex;
      }
    }

    internal D3D11EffectShaderGroup(D3D11EffectImpl impl, EffectData.ShaderGroup groupData, int groupIndex) : this(impl, groupData, groupIndex, null) { }

    internal D3D11EffectShaderGroup(D3D11EffectImpl impl, EffectData.ShaderGroup groupData, int groupIndex, D3D11EffectShaderGroup groupToCloneFrom)
    {
      m_impl = impl;
      m_name = groupData.Name;
      m_groupIndex = groupIndex;
      m_groupData = groupData;
      m_shaderBlocks = new ShaderBlock[NumberOfStages];

      if (groupData.ShaderIndices is not null && groupData.ShaderIndices.Length > 0)
      {
        EffectData effectData = impl.EffectData;
        D3D11EffectResourceManager resourceManager = impl.Resourcemanager;
        int[] shaderIndices = groupData.ShaderIndices;

        for (int i = 0; i < shaderIndices.Length; i++)
        {
          int shaderIndex = shaderIndices[i];
          EffectData.Shader shader = effectData.Shaders[shaderIndex];
          m_shaderBlocks[(int) shader.ShaderType] = resourceManager.GetShaderBlock(impl.D3DDevice, shader);

          if (shader.ShaderType == ShaderStage.VertexShader)
          {
            if (groupToCloneFrom is null)
            {
              HashedShaderSignature inputSig = new HashedShaderSignature(shader.InputSignature.ByteCode, shader.InputSignature.HashCode);
              m_localLayoutCache = new InputLayoutCache(impl.D3DDevice, inputSig);
            }
            else
            {
              m_localLayoutCache = groupToCloneFrom.m_localLayoutCache;
            }
          }

          m_shaderBlocks[(int) shader.ShaderType] = resourceManager.GetShaderBlock(impl.D3DDevice, effectData.Shaders[shaderIndex]);
        }
      }
    }

    public void Apply(IRenderContext renderContext)
    {
      if (renderContext is null)
        return;

      D3D11RenderContext d3dRenderContext = renderContext as D3D11RenderContext;

      //Setting of render states is done at a higher level (e.g. material, or possibly
      //by hooking into the OnApply event)

      m_impl.OnPreApply(d3dRenderContext, this);

      //Announce input signature
      d3dRenderContext.SetCurrentInputLayoutCache(m_localLayoutCache);

      //Apply shader blocks
      for (int i = 0; i < m_shaderBlocks.Length; i++)
      {
        ShaderBlock shaderBlock = m_shaderBlocks[i];
        if (shaderBlock is not null)
        {
          shaderBlock.Apply(d3dRenderContext);
        }
        else
        {
          //Apply NULL shader
          ShaderStage stage = (ShaderStage) i;
          D3D11ShaderStage shaderStage = d3dRenderContext.GetShaderStage(stage) as D3D11ShaderStage;
          shaderStage.SetShader(HashedShader.NullShader(stage));
        }
      }
    }

    /// <summary>
    /// Queries the shader group if it contains a shader used by the specified shader stage.
    /// </summary>
    /// <param name="shaderStage">Shader stage to query.</param>
    /// <returns>True if the group contains a shader that will be bound to the shader stage, false otherwise.</returns>
    public bool ContainsShader(ShaderStage shaderStage)
    {
      for (int i = 0; i < m_shaderBlocks.Length; i++)
      {
        if (m_shaderBlocks[i].Shader.ShaderType == shaderStage)
          return true;
      }

      return false;
    }

    /// <summary>
    /// Checks if this part belongs to the given effect.
    /// </summary>
    /// <param name="effect">Effect to check against</param>
    /// <returns>True if the effect is the parent of this part, false otherwise.</returns>
    public bool IsPartOf(Effect effect)
    {
      if (effect is null)
        return false;

      return Object.ReferenceEquals(effect.Implementation, m_impl);
    }
  }
}
