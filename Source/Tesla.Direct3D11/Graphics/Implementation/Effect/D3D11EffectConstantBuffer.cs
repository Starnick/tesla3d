﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Buffers;
using System.Diagnostics;
using SharpDX.Direct3D11;
using Tesla.Graphics;
using D3D = SharpDX.Direct3D;
using D3D11 = SharpDX.Direct3D11;
using SDX = SharpDX;

namespace Tesla.Direct3D11.Graphics.Implementation
{
  /// <summary>
  /// A Direct3D11 implementation of <see cref="IEffectConstantBuffer"/>.
  /// </summary>
  [DebuggerDisplay("Name = {Name}, SizeInBytes = {SizeInBytes}, Count = {Parameters.Count}")]
  public sealed class D3D11EffectConstantBuffer : IDisposable, IEffectConstantBuffer, ID3D11ShaderResourceView, ID3D11Buffer, IShaderResource
  {
    private DataBuffer<byte> m_buffer;
    private D3D11.Buffer m_nativeBuffer;
    private D3D11.ShaderResourceView m_textureBufferView;
    private string m_name;
    private int m_sizeInBytes;
    private bool m_isDirty;
    private bool m_isTextureBuffer;
    private bool m_isDisposed;
    private ShaderStageBinding m_boundStages;
    private D3D11EffectImpl m_impl;
    private EffectParameterCollection m_parameters;
    private EffectData.ConstantBuffer m_constantBufferData;

    /// <inheritdoc />
    public string Name
    {
      get
      {
        return m_name;
      }
      set
      {
        m_name = value;
      }
    }

    /// <inheritdoc />
    public string DebugName
    {
      get
      {
        return m_nativeBuffer.DebugName;
      }
      set
      {
        m_nativeBuffer.DebugName = value;

        if (m_textureBufferView is not null)
          m_textureBufferView.DebugName = (string.IsNullOrEmpty(value)) ? value : value + "_SRV";
      }
    }
    /// <inheritdoc />
    public int SizeInBytes
    {
      get
      {
        return m_sizeInBytes;
      }
    }

    /// <inheritdoc />
    public EffectParameterCollection Parameters
    {
      get
      {
        return m_parameters;
      }
    }

    /// <inheritdoc />
    public bool IsTextureBuffer
    {
      get
      {
        return m_isTextureBuffer;
      }
    }

    /// <inheritdoc />
    public bool IsDisposed
    {
      get
      {
        return m_isDisposed;
      }
    }

    public D3D11.Buffer D3DBuffer
    {
      get
      {
        return m_nativeBuffer;
      }
    }

    public D3D11.ShaderResourceView D3DShaderResourceView
    {
      get
      {
        return m_textureBufferView;
      }
    }

    /// <inheritdoc />
    public ShaderResourceType ResourceType
    {
      get
      {
        return ShaderResourceType.Buffer;
      }
    }

    /// <inheritdoc />
    public ShaderStageBinding BoundStages
    {
      get
      {
        return m_boundStages;
      }
      set
      {
        m_boundStages = value;
      }
    }

    internal Span<byte> BufferBytes
    {
      get
      {
        return m_buffer.Span;
      }
    }

    internal bool IsDirty
    {
      get
      {
        return m_isDirty;
      }
      set
      {
        m_isDirty = value;
      }
    }

    internal D3D11EffectConstantBuffer(D3D11.Device device, D3D11EffectImpl impl, EffectData.ConstantBuffer effectData) : this(device, impl, effectData, null) { }

    internal unsafe D3D11EffectConstantBuffer(D3D11.Device device, D3D11EffectImpl impl, EffectData.ConstantBuffer effectData, D3D11EffectConstantBuffer bufferToCloneFrom)
    {
      m_impl = impl;
      m_constantBufferData = effectData;
      m_name = effectData.Name;
      m_sizeInBytes = effectData.SizeInBytes;
      m_isTextureBuffer = false;
      m_isDisposed = false;

      if (effectData.BufferType == D3DConstantBufferType.TextureBuffer)
        m_isTextureBuffer = true;

      m_buffer = DataBuffer.Create<byte>(m_sizeInBytes);

      //If cloning, copy buffer memory, otherwise we'll set the default value (potentially) when we create the parameters
      if (bufferToCloneFrom is not null)
        bufferToCloneFrom.BufferBytes.CopyTo(m_buffer.Bytes);

      if (effectData.Variables is not null && effectData.Variables.Length > 0)
      {
        D3D11EffectParameter[] variables = new D3D11EffectParameter[effectData.Variables.Length];
        bool isCloning = bufferToCloneFrom is not null;
        for (int i = 0; i < variables.Length; i++)
          variables[i] = D3D11EffectParameter.CreateValueVariable(impl, this, effectData.Variables[i], isCloning);

        m_parameters = new EffectParameterCollection(variables);
      }
      else
      {
        m_parameters = EffectParameterCollection.EmptyCollection;
      }

      //Set is dirty false here since we may have set it true when creating the parameters
      m_isDirty = false;

      D3D11.BufferDescription desc = new D3D11.BufferDescription();
      desc.CpuAccessFlags = D3D11.CpuAccessFlags.Write;
      desc.OptionFlags = D3D11.ResourceOptionFlags.None;
      desc.SizeInBytes = m_sizeInBytes;
      desc.StructureByteStride = 0;
      desc.Usage = D3D11.ResourceUsage.Dynamic;
      desc.BindFlags = (m_isTextureBuffer) ? D3D11.BindFlags.ShaderResource : D3D11.BindFlags.ConstantBuffer;

      using (MemoryHandle bufferHandle = m_buffer.Pin())
        m_nativeBuffer = new D3D11.Buffer(device, bufferHandle.AsIntPointer(), desc);

      if (m_isTextureBuffer)
      {
        D3D11.ShaderResourceViewDescription srvDesc = new D3D11.ShaderResourceViewDescription();
        srvDesc.Format = SDX.DXGI.Format.R32G32B32A32_Float;
        srvDesc.Dimension = D3D.ShaderResourceViewDimension.Buffer;
        srvDesc.Buffer.ElementOffset = 0;
        srvDesc.Buffer.ElementWidth = m_sizeInBytes / (4 * sizeof(float)); //Divided by register size

        m_textureBufferView = new D3D11.ShaderResourceView(device, m_nativeBuffer, srvDesc);
      }
    }

    public bool IsPartOf(Effect effect)
    {
      if (effect is null)
        return false;

      return Object.ReferenceEquals(effect.Implementation, m_impl);
    }

    public T Get<T>() where T : unmanaged
    {
      return m_buffer.Span.Read<T>();
    }

    public T Get<T>(int offsetInBytes) where T : unmanaged
    {
      return m_buffer.Span.Slice(offsetInBytes).Read<T>();
    }

    public T[] GetRange<T>(int count) where T : unmanaged
    {
      T[] data = new T[count];
      m_buffer.Span.CopyTo<T>(data);
      return data;
    }

    public T[] GetRange<T>(int offsetInBytes, int count) where T : unmanaged
    {
      T[] data = new T[count];
      m_buffer.Span.Slice(offsetInBytes).CopyTo<T>(data);
      return data;
    }

    public void Set<T>(in T value) where T : unmanaged
    {
      m_buffer.Span.Write<T>(value);
      m_isDirty = true;
    }

    public void Set<T>(int offsetInBytes, in T value) where T : unmanaged
    {
      m_buffer.Span.Slice(offsetInBytes).Write<T>(value);
      m_isDirty = true;
    }

    public void Set<T>(params T[] values) where T : unmanaged
    {
      m_buffer.Span.Write<T>(values);
      m_isDirty = true;
    }

    public void Set<T>(int offsetInBytes, params T[] values) where T : unmanaged
    {
      m_buffer.Span.Slice(offsetInBytes).Write<T>(values);
      m_isDirty = true;
    }

    public void Update(D3D11.DeviceContext context)
    {
      if (m_isDirty)
      {
        SDX.DataBox dataBox = context.MapSubresource(m_nativeBuffer, 0, D3D11.MapMode.WriteDiscard, D3D11.MapFlags.None);
        BufferBytes.CopyTo(dataBox.DataPointer, SizeInBytes);
        context.UnmapSubresource(m_nativeBuffer, 0);

        m_isDirty = false;
      }
    }

    /// <summary>
    /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
    /// </summary>
    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    private void Dispose(bool disposing)
    {
      if (!IsDisposed)
      {
        if (disposing)
        {
          if (m_buffer is not null)
          {
            m_buffer.Dispose();
            m_buffer = null;
          }

          if (m_textureBufferView is not null)
          {
            m_textureBufferView.Dispose();
            m_textureBufferView = null;
          }
        }

        m_isDisposed = true;
      }
    }
  }
}
