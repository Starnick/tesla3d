﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Tesla.Graphics;

namespace Tesla.Direct3D11.Graphics.Implementation
{
  [DebuggerDisplay("Name = {Name}, ParameterClass = {ParameterClass}, ParameterType = {ParameterType}, SizeInBytes = {SizeInBytes}, StartOffsetInBytes = {StartOffsetInBytes}, ElementIndex = {ElementIndex}, StructureMemberIndex = {StructureMemberIndex}")]
  public sealed class D3D11EffectParameter : IEffectParameter
  {
    private delegate void SetMatrixDelegate(int offset, in Matrix matrix);
    private delegate void GetMatrixDelegate(int offset, out Matrix matrix);

    private D3D11EffectConstantBuffer m_constantBuffer;
    private EffectParameterCollection m_elements;
    private EffectParameterCollection m_structureMembers;
    private String m_name;
    private int m_columnCount;
    private int m_rowCount;
    private EffectParameterClass m_paramClass;
    private EffectParameterType m_paramType;
    private int m_sizeInBytes;
    private int m_matrixSizeInBytes; //Only valid for matrices, basically our IsAlignedToFloat4 for matrices - cache it upfront
    private int m_startOffsetInBytes; //Includes element index
    private int m_structureMemberIndex;
    private int m_elementIndex;
    private int m_totalElementCount;
    private bool m_isArray;
    private bool m_isValueType;
    private bool m_isStructureMember;
    private Type m_type;

    private D3D11EffectImpl m_impl;
    private EffectData.ValueVariable m_valueVariable;
    private EffectData.ValueVariableMember m_memberVariable;
    private EffectData.ResourceVariable m_resourceVariable;
    private D3D11EffectResourceManager m_resourceManager;
    private int m_startResourceIndex; //Includes element index

    private SetMatrixDelegate m_setMatrixDelegate;
    private GetMatrixDelegate m_getMatrixDelegate;

    public String Name
    {
      get
      {
        return m_name;
      }
    }

    public IEffectConstantBuffer ContainingBuffer
    {
      get
      {
        return m_constantBuffer;
      }
    }

    public EffectParameterCollection Elements
    {
      get
      {
        return m_elements;
      }
    }

    public EffectParameterCollection StructureMembers
    {
      get
      {
        return m_structureMembers;
      }
    }

    public EffectParameterClass ParameterClass
    {
      get
      {
        return m_paramClass;
      }
    }

    public EffectParameterType ParameterType
    {
      get
      {
        return m_paramType;
      }
    }

    public Type DefaultNetType
    {
      get
      {
        return m_type;
      }
    }

    public int ColumnCount
    {
      get
      {
        return m_columnCount;
      }
    }

    public int RowCount
    {
      get
      {
        return m_rowCount;
      }
    }

    public int SizeInBytes
    {
      get
      {
        return m_sizeInBytes;
      }
    }

    public int StartOffsetInBytes
    {
      get
      {
        return m_startOffsetInBytes;
      }
    }

    public int ElementIndex
    {
      get
      {
        return m_elementIndex;
      }
    }

    public int StructureMemberIndex
    {
      get
      {
        return m_structureMemberIndex;
      }
    }

    public bool IsArray
    {
      get
      {
        return m_isArray;
      }
    }

    public bool IsElementInArray
    {
      get
      {
        return !m_isArray && m_totalElementCount > 0;
      }
    }

    public bool IsStructureMember
    {
      get
      {
        return m_isStructureMember;
      }
    }

    public bool IsValueType
    {
      get
      {
        return m_isValueType;
      }
    }

    private D3D11EffectParameter() { }

    internal static D3D11EffectParameter CreateResourceVariable(D3D11EffectImpl impl, D3D11EffectResourceManager resourceManager, int startResourceIndex, EffectData.ResourceVariable variable)
    {
      return CreateResourceVariable(impl, resourceManager, startResourceIndex, 0, variable, variable.ElementCount > 0);
    }

    private static D3D11EffectParameter CreateResourceVariable(D3D11EffectImpl impl, D3D11EffectResourceManager resourceManager, int startResourceIndex, int elementIndex, EffectData.ResourceVariable variable, bool isArrayRoot)
    {
      D3D11EffectParameter p = new D3D11EffectParameter();
      p.m_impl = impl;
      p.m_resourceManager = resourceManager;
      p.m_resourceVariable = variable;

      //Set properties
      p.m_name = variable.Name;
      p.m_columnCount = 0;
      p.m_rowCount = 0;
      p.m_paramClass = EffectParameterClass.Object;
      p.m_paramType = Direct3DHelper.FromD3DResourceDimensionAndInputType(variable.ResourceDimension, variable.ResourceType);
      p.m_sizeInBytes = 0;
      p.m_structureMemberIndex = 0;
      p.m_elementIndex = elementIndex;
      p.m_totalElementCount = variable.ElementCount; //Keep track of array max range for validation
      p.m_isArray = isArrayRoot;
      p.m_isValueType = false;
      p.m_startOffsetInBytes = 0;
      p.m_startResourceIndex = startResourceIndex + elementIndex;
      p.m_type = GraphicsHelper.GetEffectParameterDefaultDataType(p.m_paramClass, p.m_paramType);

      //Set unused properties
      p.m_memberVariable = null;
      p.m_valueVariable = null;
      p.m_constantBuffer = null;
      p.m_isStructureMember = false;

      bool collectElements = isArrayRoot;

      if (collectElements)
      {
        D3D11EffectParameter[] elements = new D3D11EffectParameter[p.m_totalElementCount];
        for (int i = 0; i < p.m_totalElementCount; i++)
          elements[i] = CreateResourceVariable(impl, resourceManager, startResourceIndex, i, variable, false);

        p.m_elements = new EffectParameterCollection(elements);
      }
      else
      {
        p.m_elements = EffectParameterCollection.EmptyCollection;
      }

      p.m_structureMembers = EffectParameterCollection.EmptyCollection;

      return p;
    }

    //For creating a complete value variable. May be a scalar, vector, struct and/or may be an array of elements.
    internal static D3D11EffectParameter CreateValueVariable(D3D11EffectImpl impl, D3D11EffectConstantBuffer constantBuffer, EffectData.ValueVariable variable, bool isCloning)
    {
      return CreateValueVariable(impl, constantBuffer, 0, variable, isCloning, variable.ElementCount > 0);
    }

    private static D3D11EffectParameter CreateValueVariable(D3D11EffectImpl impl, D3D11EffectConstantBuffer constantBuffer, int elemIndex, EffectData.ValueVariable variable, bool isCloning, bool isArrayRoot)
    {
      D3D11EffectParameter p = new D3D11EffectParameter();
      p.m_impl = impl;
      p.m_constantBuffer = constantBuffer;
      p.m_valueVariable = variable;

      //Set properties
      p.m_name = variable.Name;
      p.m_columnCount = variable.ColumnCount;
      p.m_rowCount = variable.RowCount;
      p.m_paramClass = Direct3DHelper.FromD3DShaderVariableClass(variable.VariableClass);
      p.m_paramType = Direct3DHelper.FromD3DShaderVariableType(variable.VariableType);
      p.m_sizeInBytes = variable.SizeInBytes;
      p.m_structureMemberIndex = 0;
      p.m_elementIndex = elemIndex;
      p.m_totalElementCount = variable.ElementCount; //Keep track of array max range for validation
      p.m_isArray = isArrayRoot;
      p.m_isValueType = true;
      p.m_isStructureMember = false;
      p.m_type = GraphicsHelper.GetEffectParameterDefaultDataType(p.m_paramClass, p.m_paramType, p.m_columnCount);

      p.m_startOffsetInBytes = variable.StartOffset;

      if (elemIndex > 0)
        p.m_startOffsetInBytes += (elemIndex * variable.AlignedElementSizeInBytes);

      p.FixupSizeForElementInArray(variable.AlignedElementSizeInBytes);

      p.SetMatrixImplementation();

      //Set unused properties
      p.m_memberVariable = null;
      p.m_resourceVariable = null;
      p.m_resourceManager = null;
      p.m_startResourceIndex = -1;

      p.PopulateSubParameters(isCloning, isArrayRoot, variable);

      if (!isCloning && !p.IsElementInArray)
        p.SetDefaultValue(variable.DefaultValue);

      return p;
    }

    //For creating a complete structure member. A member is contained in a parent structure, but may be an array of elements or have other structure members too.
    private static D3D11EffectParameter CreateStructureMember(D3D11EffectImpl impl, D3D11EffectConstantBuffer constantBuffer, int elemIndex, int memberIndex, int startOffsetOfParentInBytes, EffectData.ValueVariableMember variableMember, bool isCloning)
    {
      return CreateStructureMember(impl, constantBuffer, elemIndex, memberIndex, startOffsetOfParentInBytes, variableMember, isCloning, variableMember.ElementCount > 0);
    }

    private static D3D11EffectParameter CreateStructureMember(D3D11EffectImpl impl, D3D11EffectConstantBuffer constantBuffer, int elemIndex, int memberIndex, int startOffsetOfParentInBytes, EffectData.ValueVariableMember variableMember, bool isCloning, bool isArrayRoot, bool isSubArrayElement = false)
    {
      D3D11EffectParameter p = new D3D11EffectParameter();
      p.m_impl = impl;
      p.m_constantBuffer = constantBuffer;
      p.m_memberVariable = variableMember;

      //Set properties
      p.m_name = variableMember.Name;
      p.m_columnCount = variableMember.ColumnCount;
      p.m_rowCount = variableMember.RowCount;
      p.m_paramClass = Direct3DHelper.FromD3DShaderVariableClass(variableMember.VariableClass);
      p.m_paramType = Direct3DHelper.FromD3DShaderVariableType(variableMember.VariableType);
      p.m_sizeInBytes = variableMember.SizeInBytes;
      p.m_structureMemberIndex = memberIndex;
      p.m_elementIndex = elemIndex;
      p.m_totalElementCount = variableMember.ElementCount; //Keep track of array max range for validation
      p.m_isArray = isArrayRoot;
      p.m_isValueType = true;
      p.m_isStructureMember = true;
      p.m_type = GraphicsHelper.GetEffectParameterDefaultDataType(p.m_paramClass, p.m_paramType, p.m_columnCount);

      //If the start of an array that is embedded inside a struct member, ignore the offset from the parent structure as it'll be repeated
      //from the previous call, the startOffset supplied should already be at the correct location
      if (isSubArrayElement)
      {
        p.m_startOffsetInBytes = startOffsetOfParentInBytes;

        if (elemIndex > 0)
          p.m_startOffsetInBytes += (elemIndex * variableMember.AlignedElementSizeInBytes);
      }
      else
      {
        p.m_startOffsetInBytes = startOffsetOfParentInBytes + variableMember.OffsetFromParentStructure;
      }

      p.FixupSizeForElementInArray(variableMember.AlignedElementSizeInBytes);

      p.SetMatrixImplementation();

      //Set unused properties
      p.m_valueVariable = null;
      p.m_resourceVariable = null;
      p.m_resourceManager = null;
      p.m_startResourceIndex = -1;

      p.PopulateSubParameters(isCloning, isArrayRoot, variableMember);

      return p;
    }

    private void FixupSizeForElementInArray(int alignedElementSizeInBytes)
    {
      //If an element of an array, fix up the size to match a single element. Array elements are padded (basically using float4s), except
      //for the last element which is not padded!
      if (IsElementInArray)
      {
        if (m_elementIndex == m_totalElementCount - 1)
          m_sizeInBytes = m_sizeInBytes - (alignedElementSizeInBytes * (m_totalElementCount - 1));
        else
          m_sizeInBytes = alignedElementSizeInBytes;
      }
    }
    private void PopulateSubParameters(bool isCloning, bool isArrayRoot, object variable)
    {
      //Passing in EffectData.ValueVariable or ValueVariableMember to reduce code duplication
      EffectData.ValueVariableMember[] structMembers = GetStructMembers(variable);

      bool isValueVariable = variable is EffectData.ValueVariable;

      //Collect elements if the parameter is an array type
      bool collectElements = isArrayRoot && m_totalElementCount > 0;

      //Do not collect structure members for an array type, only do it for the parameter that represents each array index (the parent parameter of each element is considered the
      //array object, not the first index. However the array object can be set directly with raw data much like a memcpy)
      bool collectMembers = !collectElements && (m_paramClass == EffectParameterClass.Struct) && (structMembers is not null) && (structMembers.Length > 0);

      //Collect elements
      if (collectElements)
      {
        D3D11EffectParameter[] elements = new D3D11EffectParameter[m_totalElementCount];
        for (int i = 0; i < m_totalElementCount; i++)
        {
          if (isValueVariable)
            elements[i] = CreateValueVariable(m_impl, m_constantBuffer, i, variable as EffectData.ValueVariable, isCloning, false);
          else
            elements[i] = CreateStructureMember(m_impl, m_constantBuffer, i, 0, m_startOffsetInBytes, variable as EffectData.ValueVariableMember, isCloning, false, true);
        }
        m_elements = new EffectParameterCollection(elements);
      }
      else
      {
        m_elements = EffectParameterCollection.EmptyCollection;
      }

      //Collect structure members
      if (collectMembers)
      {
        D3D11EffectParameter[] members = new D3D11EffectParameter[structMembers.Length];
        for (int i = 0; i < structMembers.Length; i++)
        {
          members[i] = CreateStructureMember(m_impl, m_constantBuffer, m_elementIndex, i, m_startOffsetInBytes, structMembers[i], isCloning);
        }
        m_structureMembers = new EffectParameterCollection(members);
      }
      else
      {
        m_structureMembers = EffectParameterCollection.EmptyCollection;
      }
    }

    private EffectData.ValueVariableMember[] GetStructMembers(object variable)
    {
      EffectData.ValueVariableMember vvarm = variable as EffectData.ValueVariableMember;
      if (vvarm is not null)
        return vvarm.Members;

      EffectData.ValueVariable vvar = variable as EffectData.ValueVariable;
      if (vvar is not null)
        return vvar.Members;

      return null;
    }

    public T GetValue<T>() where T : unmanaged
    {
#if DEBUG
      ValidateValue(BufferHelper.SizeOf<T>());
#endif

      return m_constantBuffer.BufferBytes.Slice(m_startOffsetInBytes).Read<T>();
    }

    public T[] GetValueArray<T>(int count) where T : unmanaged
    {
#if DEBUG
      ValidateValueArray(BufferHelper.SizeOf<T>(), count);
#endif

      T[] values = new T[count];
      Span<byte> src = m_constantBuffer.BufferBytes.Slice(m_startOffsetInBytes);
      // If the value is aligned to float4, have to copy it one by one due to padding between elements.
      if (IsAlignedToFloat4<T>(out int sizeInBytes, out int padding))
      {
        for (int i = 0; i < values.Length; i++)
          values[i] = src.ReadAndStep<T>(padding);
      }
      else
      {
        src.Read<T>(values);
      }

      return values;
    }

    public Matrix GetMatrixValue()
    {
#if DEBUG
      ValidateValue(m_matrixSizeInBytes);
#endif

      Matrix value;
      m_getMatrixDelegate(m_startOffsetInBytes, out value);

      return value;
    }

    public Matrix GetMatrixValueTranspose()
    {
#if DEBUG
      ValidateValue(m_matrixSizeInBytes);
#endif
      Matrix value;
      m_getMatrixDelegate(m_startOffsetInBytes, out value);

      value.Transpose();
      return value;
    }

    public Matrix[] GetMatrixValueArray(int count)
    {
#if DEBUG
      ValidateValueArray(m_matrixSizeInBytes, count);
#endif

      Matrix[] values = new Matrix[count];
      int offset = m_startOffsetInBytes;
      for (int i = 0; i < count; i++)
      {
        Matrix value;
        m_getMatrixDelegate(offset, out value);
        values[i] = value;
        offset += m_matrixSizeInBytes;
      }

      return values;
    }

    public Matrix[] GetMatrixValueArrayTranspose(int count)
    {
#if DEBUG
      ValidateValueArray(m_matrixSizeInBytes, count);
#endif

      Matrix[] values = new Matrix[count];
      int offset = m_startOffsetInBytes;
      for (int i = 0; i < count; i++)
      {
        Matrix value;
        m_getMatrixDelegate(offset, out value);
        value.Transpose();
        values[i] = value;
        offset += m_matrixSizeInBytes;
      }

      return values;
    }

    public T GetResource<T>() where T : IShaderResource
    {
#if DEBUG
      ValidateResource();
#endif

      return (T) m_resourceManager.Resources[m_startResourceIndex];
    }

    public T[] GetResourceArray<T>(int count) where T : IShaderResource
    {
#if DEBUG
      ValidateResourceArray(count);
#endif

      T[] values = new T[count];
      int startIndex = m_startResourceIndex;
      for (int i = 0; i < count; i++)
      {
        values[i] = (T) m_resourceManager.Resources[startIndex + i];
      }

      return values;
    }

    public void SetValue<T>(in T value) where T : unmanaged
    {
#if DEBUG
      ValidateValue(BufferHelper.SizeOf<T>());
#endif

      m_constantBuffer.BufferBytes.Slice(m_startOffsetInBytes).Write<T>(value);
      m_constantBuffer.IsDirty = true;
    }

    public void SetValue<T>(params T[] values) where T : unmanaged
    {
#if DEBUG
      ValidateValueArray(BufferHelper.SizeOf<T>(), (values is null) ? 0 : values.Length);
#endif

      Span<byte> dst = m_constantBuffer.BufferBytes.Slice(m_startOffsetInBytes);

      //If a 16-byte vector already, can just write directly. Otherwise, we need to calculate the correct stride to step by
      //for each element.
      if (IsAlignedToFloat4<T>(out int sizeInBytes, out int padding))
      {
        for (int i = 0; i < values.Length; i++)
          dst.WriteAndStep<T>(values[i], padding);
      }
      else
      {
        dst.Write<T>(values);
      }

      m_constantBuffer.IsDirty = true;
    }

    public void SetValue(in Matrix value)
    {
#if DEBUG
      ValidateValue(m_matrixSizeInBytes);
#endif

      m_setMatrixDelegate(m_startOffsetInBytes, in value);
      m_constantBuffer.IsDirty = true;
    }

    public void SetValue(params Matrix[] values)
    {
#if DEBUG
      ValidateValueArray(m_matrixSizeInBytes, (values is null) ? 0 : values.Length);
#endif

      int offset = m_startOffsetInBytes;
      for (int i = 0; i < values.Length; i++)
      {
        m_setMatrixDelegate(offset, in values[i]);
        offset += m_matrixSizeInBytes;
      }

      m_constantBuffer.IsDirty = true;
    }

    public void SetValueTranspose(in Matrix value)
    {
#if DEBUG
      ValidateValue(m_matrixSizeInBytes);
#endif

      Matrix transposed;
      Matrix.Transpose(in value, out transposed);
      m_setMatrixDelegate(m_startOffsetInBytes, in transposed);
      m_constantBuffer.IsDirty = true;
    }

    public void SetValueTranspose(params Matrix[] values)
    {
#if DEBUG
      ValidateValueArray(m_matrixSizeInBytes, (values is null) ? 0 : values.Length);
#endif

      int offset = m_startOffsetInBytes;
      for (int i = 0; i < values.Length; i++)
      {
        Matrix transposed;
        Matrix.Transpose(in values[i], out transposed);

        m_setMatrixDelegate(offset, in transposed);
        offset += m_matrixSizeInBytes;
      }

      m_constantBuffer.IsDirty = true;
    }

    public void SetResource<T>(T resource) where T : IShaderResource
    {
#if DEBUG
      ValidateResource();
#endif

      m_resourceManager.Resources[m_startResourceIndex] = resource;
    }

    public void SetResource<T>(params T[] resources) where T : IShaderResource
    {
#if DEBUG
      ValidateResourceArray((resources is null) ? 0 : resources.Length);
#endif

      Array.Copy(resources, 0, m_resourceManager.Resources, m_startResourceIndex, resources.Length);
    }

    public bool IsPartOf(Effect effect)
    {
      if (effect is null)
        return false;

      return Object.ReferenceEquals(effect.Implementation, m_impl);
    }

    private void ValidateResource()
    {
      bool isResource;
      switch (m_paramType)
      {
        case EffectParameterType.Texture1D:
        case EffectParameterType.Texture1DArray:
        case EffectParameterType.Texture2D:
        case EffectParameterType.Texture2DArray:
        case EffectParameterType.Texture2DMS:
        case EffectParameterType.Texture2DMSArray:
        case EffectParameterType.Texture3D:
        case EffectParameterType.TextureCube:
        case EffectParameterType.TextureCubeArray:
        case EffectParameterType.SamplerState:
        case EffectParameterType.Buffer:
        case EffectParameterType.ByteAddressBuffer:
        case EffectParameterType.StructuredBuffer:
          isResource = true;
          break;
        default:
          isResource = false;
          break;
      }

      if (m_paramClass != EffectParameterClass.Object || !isResource)
        throw new InvalidCastException(StringLocalizer.Instance.GetLocalizedString("NotResourceParameter"));
    }

    private void ValidateResourceArray(int count)
    {
      ValidateResource();

      if (!IsArray || count <= m_elementIndex || count > (m_totalElementCount - m_elementIndex))
        throw new ArgumentOutOfRangeException("value", StringLocalizer.Instance.GetLocalizedString("ElementCountTooMany"));
    }

    private void ValidateValue(int sizeOfValue)
    {
      bool isValue;
      switch (m_paramType)
      {
        case EffectParameterType.Bool:
        case EffectParameterType.Int32:
        case EffectParameterType.Single:
          isValue = true;
          break;
        default:
          isValue = false;
          break;
      }

      bool isVectorOrMatrix;
      switch (m_paramClass)
      {
        case EffectParameterClass.MatrixColumns:
        case EffectParameterClass.MatrixRows:
        case EffectParameterClass.Scalar:
        case EffectParameterClass.Vector:
          isVectorOrMatrix = true;
          break;
        default:
          isVectorOrMatrix = false;
          break;
      }

      if (!isVectorOrMatrix || !isValue)
        throw new InvalidCastException(StringLocalizer.Instance.GetLocalizedString("NotValueParameter"));

      if (sizeOfValue > m_sizeInBytes)
        throw new ArgumentOutOfRangeException("value", StringLocalizer.Instance.GetLocalizedString("ValueTooLarge", sizeOfValue.ToString(), m_sizeInBytes.ToString()));
    }

    private void ValidateValueArray(int sizeOfValue, int count)
    {
      ValidateValue(sizeOfValue * count);

      if (!IsArray || count <= m_elementIndex || count > (m_totalElementCount - m_elementIndex))
        throw new ArgumentOutOfRangeException("value", StringLocalizer.Instance.GetLocalizedString("ElementCountTooMany"));
    }

    // TODO - should be able to clean up these set/get matrix impls
    private unsafe void SetMatrixColumMajor(int offset, in Matrix matrix)
    {
      Span<float> buffPtr = m_constantBuffer.BufferBytes.Slice(offset).Reinterpret<byte, float>();
      fixed (Matrix* pMatrix = &matrix)
      {
        float* matrixPtr = (float*) pMatrix;

        //Transpose
        for (int col = 0; col < m_columnCount; col++)
        {
          for (int row = 0; row < m_rowCount; row++)
          {
            buffPtr[row] = matrixPtr[row * 4];
          }

          matrixPtr++;
          buffPtr = buffPtr.Slice(4);
        }
      }

      m_constantBuffer.IsDirty = true;
    }

    private unsafe void GetMatrixColumnMajor(int offset, out Matrix matrix)
    {
      matrix = new Matrix();
      Span<float> buffPtr = m_constantBuffer.BufferBytes.Slice(offset).Reinterpret<byte, float>();
      fixed (Matrix* pMatrix = &matrix)
      {
        float* matrixPtr = (float*) pMatrix;

        //Transpose
        for (int col = 0; col < m_columnCount; col++)
        {
          for (int row = 0; row < m_rowCount; row++)
          {
            matrixPtr[row * 4] = buffPtr[row];
          }

          matrixPtr++;
          buffPtr = buffPtr.Slice(4);
        }
      }
    }

    private unsafe void SetMatrixRowMajor(int offset, in Matrix matrix)
    {
      Span<float> buffPtr = m_constantBuffer.BufferBytes.Slice(offset).Reinterpret<byte, float>();
      fixed (Matrix* pMatrix = &matrix)
      {
        float* matrixPtr = (float*) pMatrix;

        //Only if matrix is expecting less columns/rows than our 4x4 row-major matrix
        for (int row = 0; row < m_rowCount; row++)
        {
          for (int col = 0; col < m_columnCount; col++)
          {
            buffPtr[col] = matrixPtr[col * 4];
          }

          matrixPtr++;
          buffPtr = buffPtr.Slice(4);
        }
      }

      m_constantBuffer.IsDirty = true;
    }

    private unsafe void GetMatrixRowMajor(int offset, out Matrix matrix)
    {
      matrix = new Matrix();
      Span<float> buffPtr = m_constantBuffer.BufferBytes.Slice(offset).Reinterpret<byte, float>();
      fixed (Matrix* pMatrix = &matrix)
      {
        float* matrixPtr = (float*) pMatrix;

        //Only if matrix is expecting less columns/rows than our 4x4 row-major matrix
        for (int row = 0; row < m_rowCount; row++)
        {
          for (int col = 0; col < m_columnCount; col++)
          {
            matrixPtr[col * 4] = buffPtr[col];
          }

          matrixPtr++;
          buffPtr = buffPtr.Slice(4);
        }
      }
    }

    private unsafe void SetMatrixDirect(int offset, in Matrix matrix)
    {
      m_constantBuffer.Set<Matrix>(offset, in matrix);
    }

    private unsafe void GetMatrixDirect(int offset, out Matrix matrix)
    {
      matrix = m_constantBuffer.Get<Matrix>(offset);
    }

    private static bool IsAlignedToFloat4<T>(out int sizeInBytes, out int padding) where T : unmanaged
    {
      sizeInBytes = BufferHelper.SizeOf<T>();
      padding = 0;
      int offset = sizeInBytes % 16;
      bool notAligned = offset != 0;
      if (notAligned)
        padding = 16 - offset;

      return notAligned;
    }

    private void SetMatrixImplementation()
    {
      bool isColumnMatrix = m_paramClass == EffectParameterClass.MatrixColumns;
      bool isRowMatrix = m_paramClass == EffectParameterClass.MatrixRows;

      if (isColumnMatrix || isRowMatrix)
      {
        m_matrixSizeInBytes = ((isColumnMatrix) ? m_columnCount : m_rowCount) * 4 * sizeof(float);
        bool directCopy = m_columnCount == 4 && m_rowCount == 4 && isRowMatrix;

        if (directCopy)
        {
          m_setMatrixDelegate = new SetMatrixDelegate(SetMatrixDirect);
          m_getMatrixDelegate = new GetMatrixDelegate(GetMatrixDirect);
        }
        else
        {
          m_setMatrixDelegate = (isColumnMatrix) ? new SetMatrixDelegate(SetMatrixColumMajor) : new SetMatrixDelegate(SetMatrixRowMajor);
          m_getMatrixDelegate = (isColumnMatrix) ? new GetMatrixDelegate(GetMatrixColumnMajor) : new GetMatrixDelegate(GetMatrixRowMajor);
        }
      }
    }

    private void SetDefaultValue(ReadOnlySpan<byte> defaultValue)
    {
      if (defaultValue.IsEmpty)
        return;

      defaultValue.CopyTo(m_constantBuffer.BufferBytes.Slice(m_startOffsetInBytes, defaultValue.Length));
      m_constantBuffer.IsDirty = true;
    }
  }
}
