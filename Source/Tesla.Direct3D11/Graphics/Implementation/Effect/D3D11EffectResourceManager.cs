﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Threading;
using Tesla.Graphics;

using D3D11 = SharpDX.Direct3D11;

namespace Tesla.Direct3D11.Graphics.Implementation
{
  internal sealed class ShaderManager
  {
    private int m_referenceCount;
    private Dictionary<int, HashedShader> m_shaderMap;

    public ShaderManager()
    {
      m_shaderMap = new Dictionary<int, HashedShader>();
      AddRef();
    }

    public HashedShader GetOrCreateShader(D3D11.Device device, EffectData.Shader shaderdata)
    {
      //No need to lock, because all the shaders will be created when the effect is fully initialized. After that, the shader map
      //effectively becomes read only. Release, on the other hand, may still happen in whatever last effect instance is out there.
      HashedShader shader;

      ShaderStage type = shaderdata.ShaderType;
      int hashCode = shaderdata.HashCode;
      byte[] byteCode = shaderdata.ShaderByteCode;

      if (m_shaderMap.TryGetValue(hashCode, out shader))
        return shader;

      switch (type)
      {
        case ShaderStage.VertexShader:
          shader = new HashedShader(new D3D11.VertexShader(device, byteCode), hashCode, type);
          break;
        case ShaderStage.PixelShader:
          shader = new HashedShader(new D3D11.PixelShader(device, byteCode), hashCode, type);
          break;
        case ShaderStage.GeometryShader:
          shader = new HashedShader(new D3D11.GeometryShader(device, byteCode), hashCode, type);
          GSWithSOCache soCache = new GSWithSOCache(device);
          soCache.SetShader(shaderdata);
          shader.Shader.Tag = soCache;
          break;
        case ShaderStage.DomainShader:
          shader = new HashedShader(new D3D11.DomainShader(device, byteCode), hashCode, type);
          break;
        case ShaderStage.HullShader:
          shader = new HashedShader(new D3D11.HullShader(device, byteCode), hashCode, type);
          break;
        case ShaderStage.ComputeShader:
          shader = new HashedShader(new D3D11.ComputeShader(device, byteCode), hashCode, type);
          break;
      }

      m_shaderMap.Add(hashCode, shader);

      return shader;
    }

    public void AddRef()
    {
      Interlocked.Increment(ref m_referenceCount);
    }

    public void Release()
    {
      Interlocked.Decrement(ref m_referenceCount);

      //Whoever hits zero first is the one to dispose.
      if (m_referenceCount == 0)
      {
        foreach (KeyValuePair<int, HashedShader> kv in m_shaderMap)
        {
          D3D11.DeviceChild shader = kv.Value.Shader;
          if (shader is not null)
            shader.Dispose();
        }

        m_shaderMap.Clear();
      }
    }
  }

  internal sealed class GSWithSOCache
  {
    private D3D11.Device m_device;
    private EffectData.Shader m_shader;
    private HashedShader m_gsWithSOShader;
    private Object m_sync;
    private int[] m_stride;

    public GSWithSOCache(D3D11.Device device)
    {
      m_device = device;
      m_sync = new Object();
      m_stride = new int[1];
    }

    public void SetShader(EffectData.Shader shader)
    {
      m_shader = shader;
    }

    public HashedShader? GetGeometryShaderWithSO(StreamOutputBufferBinding[] soTargets, int numSOTargets)
    {
      //TODO - Only support a single Stream output target for now...
      if (soTargets is null || numSOTargets != 1)
        return null;

      lock (m_sync)
      {
        if (m_gsWithSOShader.Shader is null && m_shader is not null)
        {
          EffectData.Signature sig = m_shader.OutputSignature;
          D3D11.StreamOutputElement[] soEntries = new D3D11.StreamOutputElement[sig.Parameters.Length];

          int hash = 17;
          hash = (hash * 31) + m_shader.HashCode;

          for (int i = 0; i < sig.Parameters.Length; i++)
          {
            EffectData.SignatureParameter param = sig.Parameters[i];
            byte numComp = 0;

            if (param.UsageMask == D3DComponentMaskFlags.All)
            {
              numComp = 4;
            }
            else
            {
              if ((param.UsageMask & D3DComponentMaskFlags.ComponentX) == D3DComponentMaskFlags.ComponentX)
                numComp++;

              if ((param.UsageMask & D3DComponentMaskFlags.ComponentY) == D3DComponentMaskFlags.ComponentY)
                numComp++;

              if ((param.UsageMask & D3DComponentMaskFlags.ComponentZ) == D3DComponentMaskFlags.ComponentZ)
                numComp++;

              if ((param.UsageMask & D3DComponentMaskFlags.ComponentW) == D3DComponentMaskFlags.ComponentW)
                numComp++;
            }

            D3D11.StreamOutputElement entry = new D3D11.StreamOutputElement(param.StreamIndex, param.SemanticName, param.SemanticIndex, 0, numComp, 0);
            soEntries[i] = entry;

            hash = (hash * 31) + param.StreamIndex.GetHashCode();
            hash = (hash * 31) + param.SemanticName.GetHashCode();
            hash = (hash * 31) + param.SemanticIndex.GetHashCode();
            hash = (hash * 31) + numComp;
          }

          m_stride[0] = soTargets[0].StreamOutputBuffer.VertexLayout.VertexStride;
          D3D11.GeometryShader gs = new D3D11.GeometryShader(m_device, m_shader.ShaderByteCode, soEntries, m_stride, -1);
          m_gsWithSOShader = new HashedShader(gs, hash, ShaderStage.GeometryShader);
        }
      }

      return m_gsWithSOShader;
    }
  }

  internal sealed class D3D11EffectResourceManager
  {
    private static readonly IShaderResource[] EmptyResourceArray = new IShaderResource[0];

    public IShaderResource[] Resources;
    public EffectConstantBufferCollection ConstantBuffers;

    private Dictionary<int, ShaderBlock> m_shaderBlocks;
    private ShaderManager m_shaderManager;
    private EffectData m_effectData;

    public ShaderManager ShaderManager
    {
      get
      {
        return m_shaderManager;
      }
    }

    private D3D11EffectResourceManager(D3D11EffectResourceManager toCloneFrom)
    {
      m_effectData = toCloneFrom.m_effectData;
      m_shaderManager = toCloneFrom.ShaderManager;
      m_shaderManager.AddRef();

      m_shaderBlocks = new Dictionary<int, ShaderBlock>();

      //Clone shader blocks
      foreach (KeyValuePair<int, ShaderBlock> kv in toCloneFrom.m_shaderBlocks)
        m_shaderBlocks.Add(kv.Key, kv.Value.Clone(this));

      if (m_effectData.ResourceVariables is not null && m_effectData.ResourceVariables.Length > 0)
      {
        //Manager we're cloning from already knows the total resource count, and we want to preserve attached resources,
        //so just clone their array
        Resources = toCloneFrom.Resources.Clone() as IShaderResource[];
      }
      else
      {
        Resources = EmptyResourceArray;
      }

      ConstantBuffers = EffectConstantBufferCollection.EmptyCollection;
    }

    public D3D11EffectResourceManager(EffectData effectData)
    {
      m_shaderBlocks = new Dictionary<int, ShaderBlock>();
      m_effectData = effectData;
      m_shaderManager = new ShaderManager();

      if (m_effectData.ResourceVariables is not null && m_effectData.ResourceVariables.Length > 0)
      {
        int totalResourceCount = 0;
        for (int i = 0; i < m_effectData.ResourceVariables.Length; i++)
        {
          EffectData.ResourceVariable rvar = m_effectData.ResourceVariables[i];
          totalResourceCount += (rvar.ElementCount == 0) ? 1 : rvar.ElementCount;
        }

        Resources = new IShaderResource[totalResourceCount];
      }
      else
      {
        Resources = EmptyResourceArray;
      }

      ConstantBuffers = EffectConstantBufferCollection.EmptyCollection;
    }

    public void SetConstantBuffers(EffectConstantBufferCollection constantBuffers)
    {
      if (constantBuffers is null || constantBuffers.Count == 0)
        return;

      ConstantBuffers = constantBuffers;
    }

    public ShaderBlock GetShaderBlock(D3D11.Device device, EffectData.Shader shader)
    {
      if (shader is null)
        return null;

      ShaderBlock shaderBlock;
      if (m_shaderBlocks.TryGetValue(shader.HashCode, out shaderBlock))
        return shaderBlock;

      shaderBlock = new ShaderBlock(device, shader, this);
      m_shaderBlocks.Add(shader.HashCode, shaderBlock);

      return shaderBlock;
    }

    public D3D11EffectResourceManager Clone()
    {
      return new D3D11EffectResourceManager(this);
    }
  }

  internal sealed class ShaderBlock
  {
    private static readonly ShaderResourceDependency[] EmptyDependencyArray = new ShaderResourceDependency[0];

    public HashedShader Shader;
    public ShaderResourceDependency[] Dependencies;

    private GSWithSOCache m_gsHelper;

    private ShaderBlock(ShaderBlock toCloneFrom, D3D11EffectResourceManager resourceManager)
    {
      Shader = toCloneFrom.Shader;
      Dependencies = EmptyDependencyArray;
      m_gsHelper = toCloneFrom.m_gsHelper;

      if (toCloneFrom.Dependencies.Length > 0)
      {
        Dependencies = new ShaderResourceDependency[toCloneFrom.Dependencies.Length];
        for (int i = 0; i < Dependencies.Length; i++)
        {
          Dependencies[i] = toCloneFrom.Dependencies[i].Clone(resourceManager);
        }
      }
    }

    public ShaderBlock(D3D11.Device device, EffectData.Shader shader, D3D11EffectResourceManager resourceManager)
    {
      Shader = resourceManager.ShaderManager.GetOrCreateShader(device, shader);
      Dependencies = EmptyDependencyArray;
      m_gsHelper = Shader.Shader.Tag as GSWithSOCache;

      if (shader.BoundResources is not null && shader.BoundResources.Length > 0)
      {
        EffectData.BoundResource[] boundResources = shader.BoundResources;

        if (boundResources.Length == 1)
        {
          EffectData.BoundResource firstResource = boundResources[0];
          Range slotRange = new Range(firstResource.BindPoint, firstResource.BindPoint + firstResource.BindCount);
          D3DShaderInputType inputType = firstResource.ResourceType;
          Dependencies = new ShaderResourceDependency[] { CreateDependency(resourceManager, inputType, slotRange,
                        new ResourceLink[] { CreateResourceLink(ref firstResource)}) };
        }
        else
        {
          List<ShaderResourceDependency> dependencies = new List<ShaderResourceDependency>();
          List<ResourceLink> tempLinks = new List<ResourceLink>();

          EffectData.BoundResource firstResource = boundResources[0];
          Range slotRange = new Range(firstResource.BindPoint, firstResource.BindPoint + firstResource.BindCount);
          D3DShaderInputType inputType = firstResource.ResourceType;
          tempLinks.Add(CreateResourceLink(ref firstResource));

          for (int i = 1; i < boundResources.Length; i++)
          {
            EffectData.BoundResource nextResource = boundResources[i];

            if (slotRange.End == nextResource.BindPoint && IsCompatibleType(inputType, nextResource.ResourceType))
            {
              slotRange.Expand(nextResource.BindCount);
              tempLinks.Add(CreateResourceLink(ref nextResource));
            }
            else
            {
              dependencies.Add(CreateDependency(resourceManager, inputType, slotRange, tempLinks.ToArray()));
              tempLinks.Clear();

              slotRange = new Range(nextResource.BindPoint, nextResource.BindPoint + nextResource.BindCount);
              inputType = nextResource.ResourceType;
              tempLinks.Add(CreateResourceLink(ref nextResource));
            }
          }

          if (tempLinks.Count > 0)
            dependencies.Add(CreateDependency(resourceManager, inputType, slotRange, tempLinks.ToArray()));

          Dependencies = dependencies.ToArray();
        }
      }
    }

    public void Apply(D3D11RenderContext renderContext)
    {
      D3D11ShaderStage stage = renderContext.GetShaderStage(Shader.ShaderType) as D3D11ShaderStage;

      for (int i = 0; i < Dependencies.Length; i++)
      {
        Dependencies[i].Apply(renderContext, stage);
      }

      if (Shader.ShaderType == ShaderStage.GeometryShader && m_gsHelper is not null)
      {
        //If a geometry shader, determine if we have stream outputs...if so, then we need to create (or get a cached) geometry shader
        //that was created with Stream Out.
        int numSO;
        StreamOutputBufferBinding[] soTargets = renderContext.GetBoundSOWithoutCopy(out numSO);
        HashedShader? shaderWithSO = m_gsHelper.GetGeometryShaderWithSO(soTargets, numSO);

        stage.SetShader((shaderWithSO.HasValue) ? shaderWithSO.Value : Shader);
      }
      else
      {
        stage.SetShader(Shader);
      }
    }

    public ShaderBlock Clone(D3D11EffectResourceManager manager)
    {
      return new ShaderBlock(this, manager);
    }

    private ResourceLink CreateResourceLink(ref EffectData.BoundResource boundResource)
    {
      Range range = new Range(boundResource.ResourceIndex, boundResource.ResourceIndex + boundResource.BindCount);
      bool isConstantBuffer = boundResource.ResourceType == D3DShaderInputType.ConstantBuffer || boundResource.ResourceType == D3DShaderInputType.TextureBuffer;

      return new ResourceLink(range, isConstantBuffer);
    }

    private ShaderResourceDependency CreateDependency(D3D11EffectResourceManager manager, D3DShaderInputType type, Range slotRange, ResourceLink[] resourceLinks)
    {
      switch (type)
      {
        case D3DShaderInputType.Sampler:
          return new SamplerDependency(manager, slotRange, resourceLinks);
        case D3DShaderInputType.ConstantBuffer:
          return new ConstantBufferDependency(manager, slotRange, resourceLinks);
        default:
          return new ResourceDependency(manager, slotRange, resourceLinks);
      }
    }

    private bool IsCompatibleType(D3DShaderInputType prevType, D3DShaderInputType nextType)
    {
      if (prevType == nextType)
        return true;

      switch (prevType)
      {
        case D3DShaderInputType.AppendStructured:
        case D3DShaderInputType.ByteAddress:
        case D3DShaderInputType.ConsumeStructured:
        case D3DShaderInputType.RWByteAddress:
        case D3DShaderInputType.RWStructured:
        case D3DShaderInputType.RWStructuredWithCounter:
        case D3DShaderInputType.RWTyped:
        case D3DShaderInputType.Structured:
        case D3DShaderInputType.Texture:
        case D3DShaderInputType.TextureBuffer:
          switch (nextType)
          {
            case D3DShaderInputType.AppendStructured:
            case D3DShaderInputType.ByteAddress:
            case D3DShaderInputType.ConsumeStructured:
            case D3DShaderInputType.RWByteAddress:
            case D3DShaderInputType.RWStructured:
            case D3DShaderInputType.RWStructuredWithCounter:
            case D3DShaderInputType.RWTyped:
            case D3DShaderInputType.Structured:
            case D3DShaderInputType.Texture:
            case D3DShaderInputType.TextureBuffer:
              return true;
            default:
              return false;
          }
        case D3DShaderInputType.ConstantBuffer:
          switch (nextType)
          {
            case D3DShaderInputType.ConstantBuffer:
              return true;
            default:
              return false;
          }
        case D3DShaderInputType.Sampler:
          switch (nextType)
          {
            case D3DShaderInputType.Sampler:
              return true;
            default:
              return false;
          }
        default:
          return false;
      }
    }
  }

  internal abstract class ShaderResourceDependency
  {
    public D3D11EffectResourceManager ResourceManager;
    public Range SlotRange; //Contigious range over the shader stage slots (Start is the first slot index, and length is the bind count)
    public ResourceLink[] ResourceLinks; //Indices to either the Resources or ConstantBuffers collections in the manager

    public ShaderResourceDependency(D3D11EffectResourceManager manager, Range slotRange, ResourceLink[] resourceLinks)
    {
      ResourceManager = manager;
      SlotRange = slotRange;
      ResourceLinks = resourceLinks;
    }

    public abstract void Apply(D3D11RenderContext renderContext, D3D11ShaderStage stage);
    public abstract ShaderResourceDependency Clone(D3D11EffectResourceManager manager);
  }

  internal sealed class SamplerDependency : ShaderResourceDependency
  {
    private SamplerState[] m_samplers;

    public SamplerDependency(D3D11EffectResourceManager manager, Range slotRange, ResourceLink[] resourceLinks)
        : base(manager, slotRange, resourceLinks)
    {
      m_samplers = new SamplerState[slotRange.Length];
    }

    public override void Apply(D3D11RenderContext renderContext, D3D11ShaderStage stage)
    {
      //Gather from manager, because these are set by the user
      for (int i = 0, index = 0; i < ResourceLinks.Length; i++)
      {
        ResourceLink link = ResourceLinks[i];
        for (int j = link.Range.Start; j < link.Range.End; j++, index++)
        {
          m_samplers[index] = ResourceManager.Resources[j] as SamplerState;
        }
      }

      stage.SetSamplers(SlotRange.Start, m_samplers);

      //Clear so we don't hold onto them for GC
      Array.Clear(m_samplers, 0, m_samplers.Length);
    }

    public override ShaderResourceDependency Clone(D3D11EffectResourceManager manager)
    {
      return new SamplerDependency(manager, SlotRange, ResourceLinks);
    }
  }

  internal sealed class ResourceDependency : ShaderResourceDependency
  {
    private IShaderResource[] m_resources;

    public ResourceDependency(D3D11EffectResourceManager manager, Range slotRange, ResourceLink[] resourceLinks)
        : base(manager, slotRange, resourceLinks)
    {
      m_resources = new IShaderResource[slotRange.Length];
    }

    public override void Apply(D3D11RenderContext renderContext, D3D11ShaderStage stage)
    {
      //Gather from manager, because these are set by the user (except for tbuffers)
      for (int i = 0, index = 0; i < ResourceLinks.Length; i++)
      {
        ResourceLink link = ResourceLinks[i];

        //Since tbuffers are constant buffers with SRVs, we have to determine which resource we need to get, from which array.
        for (int j = link.Range.Start; j < link.Range.End; j++, index++)
        {
          m_resources[index] = (link.IsConstantBuffer) ? ResourceManager.ConstantBuffers[j] as D3D11EffectConstantBuffer : ResourceManager.Resources[j];
        }
      }

      stage.SetShaderResources(SlotRange.Start, m_resources);

      //Clear so we don't hold onto them for GC
      Array.Clear(m_resources, 0, m_resources.Length);
    }

    public override ShaderResourceDependency Clone(D3D11EffectResourceManager manager)
    {
      return new ResourceDependency(manager, SlotRange, ResourceLinks);
    }
  }

  internal sealed class ConstantBufferDependency : ShaderResourceDependency
  {
    private D3D11.Buffer[] m_buffers;

    public ConstantBufferDependency(D3D11EffectResourceManager manager, Range slotRange, ResourceLink[] resourceLinks)
        : base(manager, slotRange, resourceLinks)
    {
      m_buffers = new D3D11.Buffer[SlotRange.Length];
    }

    public override void Apply(D3D11RenderContext renderContext, D3D11ShaderStage stage)
    {
      D3D11.DeviceContext deviceContext = renderContext.D3DDeviceContext;

      //Need to iterate over the buffers anyways to update them before setting
      for (int i = 0, index = 0; i < ResourceLinks.Length; i++)
      {
        ResourceLink link = ResourceLinks[i];

        for (int j = link.Range.Start; j < link.Range.End; j++, index++)
        {
          D3D11EffectConstantBuffer cb = ResourceManager.ConstantBuffers[j] as D3D11EffectConstantBuffer;
          cb.Update(deviceContext);

          m_buffers[index] = cb.D3DBuffer;
        }
      }

      stage.SetConstantBuffers(SlotRange.Start, m_buffers);

      Array.Clear(m_buffers, 0, m_buffers.Length);
    }

    public override ShaderResourceDependency Clone(D3D11EffectResourceManager manager)
    {
      return new ConstantBufferDependency(manager, SlotRange, ResourceLinks);
    }
  }

  internal struct Range
  {
    public int Start;
    public int End;

    public int Length
    {
      get
      {
        return End - Start;
      }
    }

    public Range(int start, int end)
    {
      Start = start;
      End = end;
    }

    public void Expand(int count)
    {
      End += count;
    }
  }

  internal struct ResourceLink
  {
    public Range Range;
    public bool IsConstantBuffer;

    public ResourceLink(Range range, bool isConstantBuffer)
    {
      Range = range;
      IsConstantBuffer = isConstantBuffer;
    }
  }
}
