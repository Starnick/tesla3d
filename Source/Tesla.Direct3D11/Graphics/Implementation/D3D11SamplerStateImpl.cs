﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Graphics;
using Tesla.Graphics.Implementation;
using D3D = SharpDX.Direct3D;
using D3D11 = SharpDX.Direct3D11;

#nullable enable

namespace Tesla.Direct3D11.Graphics.Implementation
{
  /// <summary>
  /// A factory that creates Direct3D11 implementations of type <see cref="ISamplerStateImpl"/>.
  /// </summary>
  public sealed class D3D11SamplerStateImplFactory : D3D11GraphicsResourceImplFactory, ISamplerStateImplFactory
  {
    /// <summary>
    /// Constructs a new instance of the <see cref="D3D11SamplerStateImplFactory"/> class.
    /// </summary>
    public D3D11SamplerStateImplFactory() : base(typeof(SamplerState)) { }

    /// <summary>
    /// Creates a new implementation object instance.
    /// </summary>
    /// <returns>The render state implementation.</returns>
    public ISamplerStateImpl CreateImplementation()
    {
      return new D3D11SamplerStateImpl(D3D11RenderSystem, GetNextUniqueResourceID());
    }

    /// <summary>
    /// Called when the factory is initialized.
    /// </summary>
    /// <param name="renderSystem">The D3D11 render system.</param>
    protected override void OnInitialize(D3D11RenderSystem renderSystem)
    {
      RegisterFactory<ISamplerStateImplFactory>(this);
    }
  }

  /// <summary>
  /// A Direct3D11 implementation for <see cref="SamplerState"/>.
  /// </summary>
  public sealed class D3D11SamplerStateImpl : SamplerStateImpl, ID3D11SamplerState
  {
    private D3D11.Device m_device;
    private D3D11.SamplerState? m_nativeSamplerState;
    private int m_supportedAnisotropyLevels;

    /// <summary>
    /// Gets the number of anisotropy levels supported. This can vary by implementation.
    /// </summary>
    public override int SupportedAnisotropyLevels
    {
      get
      {
        return m_supportedAnisotropyLevels;
      }
    }

    /// <summary>
    /// Gets the native D3D11 device.
    /// </summary>
    public D3D11.Device D3DDevice
    {
      get
      {
        return m_device;
      }
    }

    /// <summary>
    /// Gets the native D3D11 sampler state.
    /// </summary>
    public D3D11.SamplerState D3DSamplerState
    {
      get
      {
        return m_nativeSamplerState!;
      }
    }

    internal D3D11SamplerStateImpl(D3D11RenderSystem renderSystem, int resourceID)
        : base(renderSystem, resourceID)
    {

      m_device = renderSystem.D3DDevice;
      m_supportedAnisotropyLevels = CheckSupport(m_device);

      SetDefaultDebugName("SamplerState");
    }

    /// <summary>
    /// Checks if the specified texture addressing mode is supported by the graphics platform.
    /// </summary>
    /// <param name="mode">Texture addressing mode</param>
    /// <returns>True if supported, false otherwise.</returns>
    public override bool IsAddressingModeSupported(TextureAddressMode mode)
    {
      switch (mode)
      {
        case TextureAddressMode.Border:
        case TextureAddressMode.Clamp:
        case TextureAddressMode.Mirror:
        case TextureAddressMode.MirrorOnce:
        case TextureAddressMode.Wrap:
          return true;
        default:
          return false;
      }
    }

    /// <summary>
    /// Called when the state is bound, signaling the implementation to create and bind the native state.
    /// </summary>
    protected override void CreateNativeState()
    {
      D3D11.SamplerStateDescription desc = new D3D11.SamplerStateDescription();
      desc.AddressU = Direct3DHelper.ToD3DTextureAddressMode(AddressU);
      desc.AddressV = Direct3DHelper.ToD3DTextureAddressMode(AddressV);
      desc.AddressW = Direct3DHelper.ToD3DTextureAddressMode(AddressW);
      desc.Filter = Direct3DHelper.ToD3DFilter(Filter);

      desc.MaximumAnisotropy = MaxAnisotropy;
      desc.MipLodBias = MipMapLevelOfDetailBias;
      desc.MinimumLod = (float) MinMipMapLevel;
      desc.MaximumLod = (MaxMipMapLevel == int.MaxValue) ? float.MaxValue : (float) MaxMipMapLevel;

      Color borderColor = BorderColor;
      Direct3DHelper.ConvertColor(ref borderColor, out desc.BorderColor);
      desc.ComparisonFunction = Direct3DHelper.ToD3DComparison(CompareFunction);

      m_nativeSamplerState = new D3D11.SamplerState(m_device, desc);
      m_nativeSamplerState.DebugName = DebugName;
    }

    /// <summary>
    /// Called when the name of the graphics resource is changed, useful if the implementation wants to set the name to
    /// be used as a debug name.
    /// </summary>
    /// <param name="name">New name of the resource</param>
    protected override void OnDebugNameChange(string name)
    {
      if (m_nativeSamplerState is not null)
        m_nativeSamplerState.DebugName = name;
    }

    /// <summary>
    /// Releases unmanaged and - optionally - managed resources.
    /// </summary>
    /// <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
    protected override void Dispose(bool disposing)
    {
      if (!IsDisposed)
      {
        if (disposing)
        {
          if (m_nativeSamplerState is not null)
          {
            m_nativeSamplerState.Dispose();
            m_nativeSamplerState = null;
          }
        }

        base.Dispose(disposing);
      }
    }

    private int CheckSupport(D3D11.Device device)
    {
      switch (device.FeatureLevel)
      {
        case D3D.FeatureLevel.Level_11_1:
        case D3D.FeatureLevel.Level_11_0:
        case D3D.FeatureLevel.Level_10_1:
        case D3D.FeatureLevel.Level_10_0:
          return 16;
        default:
          return 4;
      }
    }
  }
}
