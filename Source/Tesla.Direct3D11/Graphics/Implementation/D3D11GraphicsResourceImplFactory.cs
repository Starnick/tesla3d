﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Graphics;
using Tesla.Graphics.Implementation;

#nullable enable

namespace Tesla.Direct3D11.Graphics.Implementation
{
  /// <summary>
  /// Common base class for all Direct3D11 graphic resource implementation factories.
  /// </summary>
  public abstract class D3D11GraphicsResourceImplFactory : IGraphicsResourceImplFactory
  {
    private D3D11RenderSystem? m_renderSystem;
    private Type m_resourceType;

    /// <inheritdoc />
    public IRenderSystem RenderSystem
    {
      get
      {
        return D3D11RenderSystem;
      }
    }

    /// <inheritdoc />
    public Type GraphicsResourceType
    {
      get
      {
        return m_resourceType;
      }
    }

    /// <summary>
    /// Gets the render system as an <see cref="D3D11RenderSystem"/>.
    /// </summary>
    protected D3D11RenderSystem D3D11RenderSystem
    {
      get
      {
        if (m_renderSystem is null)
          throw GraphicsExceptionHelper.NewImplementationFactoryNotInitialized();

        return m_renderSystem;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="D3D11GraphicsResourceImplFactory"/> class.
    /// </summary>
    /// <param name="resourceType">Type of the graphic resource the factory creates implementations for.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the type is null.</exception>
    protected D3D11GraphicsResourceImplFactory(Type resourceType)
    {
      ArgumentNullException.ThrowIfNull(resourceType, nameof(resourceType));

      m_resourceType = resourceType;
    }

    /// <summary>
    /// Initializes for the specified render system, and registers the factory to the render system.
    /// </summary>
    /// <param name="renderSystem">The D3D11 render system to initialize the factory with.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the render system is null.</exception>
    public void Initialize(D3D11RenderSystem renderSystem)
    {
      GraphicsHelper.ThrowIfRenderSystemNull(renderSystem);

      m_renderSystem = renderSystem;
      OnInitialize(m_renderSystem);
    }

    /// <summary>
    /// Called when the factory is initialized.
    /// </summary>
    /// <param name="renderSystem">The D3D11 render system.</param>
    protected abstract void OnInitialize(D3D11RenderSystem renderSystem);

    /// <summary>
    /// Gets the next available resource ID from the render system.
    /// </summary>
    /// <returns>Next unique resource ID.</returns>
    /// <exception cref="TeslaGraphicsException">Thrown if the factory is not initialized.</exception>
    protected int GetNextUniqueResourceID()
    {
      if (m_renderSystem is null)
        throw GraphicsExceptionHelper.NewImplementationFactoryNotInitialized();

      return m_renderSystem.GetNextUniqueResourceID();
    }

    /// <summary>
    /// Registers the factory of the specified type to the render system.
    /// </summary>
    /// <typeparam name="T">Type of implementation factory (usually an interface).</typeparam>
    /// <param name="implFactory">Implementation factory to register.</param>
    /// <returns>True if successful, false otherwise.</returns>
    /// <exception cref="TeslaGraphicsException">Thrown if the factory is not initialized.</exception>
    protected bool RegisterFactory<T>(T implFactory) where T : IGraphicsResourceImplFactory
    {
      if (m_renderSystem is null)
        throw GraphicsExceptionHelper.NewImplementationFactoryNotInitialized();

      return m_renderSystem.AddImplementationFactory<T>(implFactory);
    }

    /// <summary>
    /// ReRegisters the factory of the specified type to the render system. If a factory of the same type was previously register, it is
    /// removed from the render system.
    /// </summary>
    /// <typeparam name="T">Type of implementation factory (usually an interface).</typeparam>
    /// <param name="implFactory">Implementation factory to register.</param>
    /// <returns>True if successful, false otherwise.</returns>
    /// <exception cref="TeslaGraphicsException">Thrown if the factory is not initialized.</exception>
    protected bool ReRegisterFactory<T>(T implFactory) where T : IGraphicsResourceImplFactory
    {
      if (m_renderSystem is null)
        throw GraphicsExceptionHelper.NewImplementationFactoryNotInitialized();

      m_renderSystem.RemoveImplementationFactory<T>(implFactory);
      return m_renderSystem.AddImplementationFactory<T>(implFactory);
    }
  }
}
