﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Graphics;
using Tesla.Graphics.Implementation;
using D3D11 = SharpDX.Direct3D11;

#nullable enable

namespace Tesla.Direct3D11.Graphics.Implementation
{
  /// <summary>
  /// A factory that creates Direct3D11 implementation of type <see cref="ITexture3DImpl"/>.
  /// </summary>
  public sealed class D3D11Texture3DImplFactory : D3D11GraphicsResourceImplFactory, ITexture3DImplFactory
  {

    /// <summary>
    /// Constructs a new instance of the <see cref="D3D11Texture3DImplFactory"/> class.
    /// </summary>
    public D3D11Texture3DImplFactory() : base(typeof(Texture3D)) { }

    /// <summary>
    /// Creates a new implementation object instance.
    /// </summary>
    /// <param name="width">Width of the texture, in texels.</param>
    /// <param name="height">Height of the texture, in texels.</param>
    /// <param name="depth">Depth of the texture, in texels.</param>
    /// <param name="mipMapCount">Number of mip map levels, must be greater than zero.</param>
    /// <param name="options">Common <see cref="TextureOptions"/> that may also contain initial data.</param>
    /// <returns>The texture implementation.</returns>
    public ITexture3DImpl CreateImplementation(int width, int height, int depth, int mipMapCount, TextureOptions options)
    {
      return new D3D11Texture3DImpl(D3D11RenderSystem, GetNextUniqueResourceID(), width, height, depth, mipMapCount, options);
    }

    /// <inheritdoc />
    protected override void OnInitialize(D3D11RenderSystem renderSystem)
    {
      RegisterFactory<ITexture3DImplFactory>(this);
    }
  }

  /// <summary>
  /// A Direct3D11 implementation for <see cref="Texture3D"/>.
  /// </summary>
  public sealed class D3D11Texture3DImpl : GraphicsResourceImpl, ITexture3DImpl, ID3D11ShaderResourceView
  {
    private D3D11.Device m_device;
    private D3D11.Texture3D m_nativeTexture;
    private D3D11.ShaderResourceView m_shaderResourceView;
    private SurfaceFormat m_format;
    private int m_mipCount;
    private int m_width;
    private int m_height;
    private int m_depth;
    private ResourceUsage m_resourceUsage;
    private ResourceBindFlags m_resourceBindFlags;
    private ShaderStageBinding m_boundStages;

    /// <inheritdoc />
    public SurfaceFormat Format
    {
      get
      {
        return m_format;
      }
    }

    /// <inheritdoc />
    public int MipCount
    {
      get
      {
        return m_mipCount;
      }
    }

    /// <inheritdoc />
    public int Width
    {
      get
      {
        return m_width;
      }
    }

    /// <inheritdoc />
    public int Height
    {
      get
      {
        return m_height;
      }
    }

    /// <inheritdoc />
    public int Depth
    {
      get
      {
        return m_depth;
      }
    }

    /// <inheritdoc />
    public ResourceUsage ResourceUsage
    {
      get
      {
        return m_resourceUsage;
      }
    }

    /// <inheritdoc />
    public ResourceBindFlags ResourceBindFlags
    {
      get
      {
        return m_resourceBindFlags;
      }
    }

    /// <summary>
    /// Gets the native D3D11 device.
    /// </summary>
    public D3D11.Device D3DDevice
    {
      get
      {
        return m_device;
      }
    }

    /// <summary>
    /// Gets the native D3D11 texture.
    /// </summary>
    public D3D11.Texture3D D3DTexture3D
    {
      get
      {
        return m_nativeTexture;
      }
    }

    /// <inheritdoc />
    public D3D11.ShaderResourceView D3DShaderResourceView
    {
      get
      {
        return m_shaderResourceView;
      }
    }

    /// <inheritdoc />
    public ShaderStageBinding BoundStages
    {
      get
      {
        return m_boundStages;
      }
      set
      {
        m_boundStages = value;
      }
    }

    internal D3D11Texture3DImpl(D3D11RenderSystem renderSystem, int resourceID, int width, int height, int depth, int mipMapCount, TextureOptions options)
        : base(renderSystem, resourceID)
    {

      m_device = renderSystem.D3DDevice;
      m_width = width;
      m_height = height;
      m_depth = depth;
      m_mipCount = mipMapCount;
      m_format = options.Format;
      m_resourceUsage = options.ResourceUsage;
      m_resourceBindFlags = ResourceBindFlags.Default | ResourceBindFlags.ShaderResource; // Always set shader

      D3D11.Texture3DDescription desc = new D3D11.Texture3DDescription();
      desc.MipLevels = m_mipCount;
      desc.Width = m_width;
      desc.Height = m_height;
      desc.Depth = m_depth;
      desc.Usage = Direct3DHelper.ToD3DResourceUsage(m_resourceUsage);
      desc.Format = Direct3DHelper.ToD3DSurfaceFormat(m_format);
      desc.BindFlags = D3D11.BindFlags.ShaderResource;
      desc.CpuAccessFlags = (m_resourceUsage == ResourceUsage.Dynamic) ? D3D11.CpuAccessFlags.Write : D3D11.CpuAccessFlags.None;
      desc.OptionFlags = D3D11.ResourceOptionFlags.None;

      // TODO - Check this, shouldn't it use the depth?
      using (PooledArray<MappedBuffer> handles = BufferHelper.MapPooled(m_format, 1, m_mipCount, m_width, m_height, options.Data))
      {
        m_nativeTexture = new D3D11.Texture3D(m_device, desc, ResourceHelper.AsDataBoxArray(handles));
        m_shaderResourceView = new D3D11.ShaderResourceView(m_device, m_nativeTexture);

        BufferHelper.Unmap(handles);
      }

      SetDefaultDebugName("Texture3D");
    }

    /// <summary>
    /// Reads data from the texture into the specified data buffer.
    /// </summary>
    /// <typeparam name="T">Type of data to read from the texture.</typeparam>
    /// <param name="data">Buffer to hold data copied from the texture.</param>
    /// <param name="subIndex">Index to identify which subresource (e.g. mipmap) to access.</param>
    /// <param name="subimage">The subimage region, in texels, of the 3D texture to read from, if null the whole image is read from.</param>
    /// <exception cref="ArgumentNullException">Thrown if the buffer is null or length zero.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the mip level is out of bounds, or if the start index and element count are out of bounds, or if the data type to and texture format msimatch in size,
    /// or if the total number of bytes to copy from the sub resource does not match the actual data size of the region to copy.</exception>
    public void GetData<T>(Span<T> data, SubResourceAt subIndex, ResourceRegion3D? subimage) where T : unmanaged
    {
      D3D11.DeviceContext d3dContext = m_device.ImmediateContext;

      ResourceHelper.ReadTextureData<T>(m_nativeTexture, d3dContext, m_width, m_height, m_depth, 1, m_mipCount, m_format, m_resourceUsage, false,
          data, subIndex, subimage);
    }

    /// <summary>
    /// Writes data to the texture from the specified data buffer.
    /// </summary>
    /// <typeparam name="T">Type of data to write to the texture.</typeparam>
    /// <param name="renderContext">The current render context.</param>
    /// <param name="data">Buffer with the data to be copied to the texture.</param>
    /// <param name="subIndex">Index to identify which subresource (e.g. mipmap) to access.</param>
    /// <param name="subimage">The subimage region, in texels, of the 3D texture to write to, if null the whole image is written to.</param>
    /// <param name="writeOptions">Writing options, valid only for dynamic textures.</param>
    /// <exception cref="ArgumentNullException">Thrown if the buffer is null or length zero.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the mip level is out of bounds, or if the start index and element count are out of bounds, or if the data type to and texture format msimatch in size,
    /// or if the total number of bytes to copy from the sub resource does not match the actual data size of the region to copy.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if attempting to write to an immutable resource.</exception>
    public void SetData<T>(IRenderContext renderContext, ReadOnlySpan<T> data, SubResourceAt subIndex, ResourceRegion3D? subimage, DataWriteOptions writeOptions) where T : unmanaged
    {
      D3D11.DeviceContext d3dContext = Direct3DHelper.GetD3DDeviceContext(renderContext);

      ResourceHelper.WriteTextureData<T>(m_nativeTexture, d3dContext, m_width, m_height, m_depth, 1, m_mipCount, m_format, m_resourceUsage, false,
          data, subIndex, subimage, writeOptions);
    }

    /// <summary>
    /// Called when the name of the graphics resource is changed, useful if the implementation wants to set the name to
    /// be used as a debug name.
    /// </summary>
    /// <param name="name">New name of the resource</param>
    protected override void OnDebugNameChange(String name)
    {
      if (m_nativeTexture is not null)
        m_nativeTexture.DebugName = name;

      if (m_shaderResourceView is not null)
        m_shaderResourceView.DebugName = (String.IsNullOrEmpty(name)) ? name : name + "_SRV";
    }

    /// <summary>
    /// Releases unmanaged and - optionally - managed resources.
    /// </summary>
    /// <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
    protected override void Dispose(bool disposing)
    {
      if (!IsDisposed)
      {
        if (disposing)
        {
          if (m_nativeTexture is not null)
          {
            m_nativeTexture.Dispose();
            m_nativeTexture = null!;
          }

          if (m_shaderResourceView is not null)
          {
            m_shaderResourceView.Dispose();
            m_shaderResourceView = null!;
          }
        }

        base.Dispose(disposing);
      }
    }
  }
}
