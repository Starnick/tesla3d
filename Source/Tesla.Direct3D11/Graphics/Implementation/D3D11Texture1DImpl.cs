﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Buffers;
using Tesla.Graphics;
using Tesla.Graphics.Implementation;
using D3D = SharpDX.Direct3D;
using D3D11 = SharpDX.Direct3D11;

#nullable enable

namespace Tesla.Direct3D11.Graphics.Implementation
{
  /// <summary>
  /// A factory that creates Direct3D11 implementations of type <see cref="ITexture1DImpl"/>.
  /// </summary>
  public sealed class D3D11Texture1DImplFactory : D3D11GraphicsResourceImplFactory, ITexture1DImplFactory
  {

    /// <summary>
    /// Constructs a new instance of the <see cref="D3D11Texture1DImplFactory"/> class.
    /// </summary>
    public D3D11Texture1DImplFactory() : base(typeof(Texture1D)) { }

    /// <summary>
    /// Creates a new implementation object instance.
    /// </summary>
    /// <param name="width">Width of the texture, in texels.</param>
    /// <param name="mipMapCount">Number of mip map levels, must be greater than zero.</param>
    /// <param name="options">Common <see cref="TextureOptions"/> that may also contain initial data.</param>
    /// <returns>The texture implementation.</returns>
    public ITexture1DImpl CreateImplementation(int width, int mipMapCount, TextureOptions options)
    {
      return new D3D11Texture1DImpl(D3D11RenderSystem, GetNextUniqueResourceID(), width, 1, mipMapCount, options);
    }

    /// <inheritdoc />
    protected override void OnInitialize(D3D11RenderSystem renderSystem)
    {
      RegisterFactory<ITexture1DImplFactory>(this);
    }
  }

  /// <summary>
  /// A factory that creates Direct3D11 implementations of type <see cref="ITexture1DArrayImpl"/>.
  /// </summary>
  public sealed class D3D11Texture1DArrayImplFactory : D3D11GraphicsResourceImplFactory, ITexture1DArrayImplFactory
  {

    /// <summary>
    /// Constructs a new instance of the <see cref="D3D11Texture1DArrayImplFactory"/> class.
    /// </summary>
    public D3D11Texture1DArrayImplFactory() : base(typeof(Texture1DArray)) { }

    /// <summary>
    /// Creates a new implementation object instance.
    /// </summary>
    /// <param name="width">Width of the texture, in texels.</param>
    /// <param name="arrayCount">Number of array slices, must be greater than zero.</param>
    /// <param name="mipMapCount">Number of mip map levels, must be greater than zero.</param>
    /// <param name="options">Common <see cref="TextureOptions"/> that may also contain initial data.</param>
    /// <returns>The texture implementation.</returns>
    public ITexture1DArrayImpl CreateImplementation(int width, int arrayCount, int mipMapCount, TextureOptions options)
    {
      return new D3D11Texture1DImpl(D3D11RenderSystem, GetNextUniqueResourceID(), width, arrayCount, mipMapCount, options);
    }

    /// <inheritdoc />
    protected override void OnInitialize(D3D11RenderSystem renderSystem)
    {
      RegisterFactory<ITexture1DArrayImplFactory>(this);
    }
  }

  /// <summary>
  /// A Direct3D11 implementation for <see cref="Texture1D" /> and <see cref="Texture1DArray" />.
  /// </summary>
  public sealed class D3D11Texture1DImpl : GraphicsResourceImpl, ITexture1DImpl, ITexture1DArrayImpl, ID3D11ShaderResourceView
  {
    private D3D11.Device m_device;
    private D3D11.Texture1D m_nativeTexture;
    private D3D11.ShaderResourceView m_shaderResourceView;
    private D3D11ShaderResourceWrapper[]? m_subTextures;
    private SurfaceFormat m_format;
    private int m_arrayCount;
    private int m_mipCount;
    private int m_width;
    private ResourceUsage m_resourceUsage;
    private ResourceBindFlags m_resourceBindFlags;
    private ShaderStageBinding m_boundStages;

    /// <inheritdoc />
    public SurfaceFormat Format
    {
      get
      {
        return m_format;
      }
    }

    /// <inheritdoc />
    public int ArrayCount
    {
      get
      {
        return m_arrayCount;
      }
    }

    /// <inheritdoc />
    public int MipCount
    {
      get
      {
        return m_mipCount;
      }
    }

    /// <inheritdoc />
    public int Width
    {
      get
      {
        return m_width;
      }
    }

    /// <inheritdoc />
    public ResourceUsage ResourceUsage
    {
      get
      {
        return m_resourceUsage;
      }
    }

    /// <inheritdoc />
    public ResourceBindFlags ResourceBindFlags
    {
      get
      {
        return m_resourceBindFlags;
      }
    }

    /// <summary>
    /// Gets the native D3D11 device.
    /// </summary>
    public D3D11.Device D3DDevice
    {
      get
      {
        return m_device;
      }
    }

    /// <summary>
    /// Gets the native D3D11 texture.
    /// </summary>
    public D3D11.Texture1D D3DTexture1D
    {
      get
      {
        return m_nativeTexture;
      }
    }

    /// <inheritdoc />
    public D3D11.ShaderResourceView D3DShaderResourceView
    {
      get
      {
        return m_shaderResourceView;
      }
    }

    /// <inheritdoc />
    public ShaderStageBinding BoundStages
    {
      get
      {
        return m_boundStages;
      }
      set
      {
        m_boundStages = value;
      }
    }

    internal D3D11Texture1DImpl(D3D11RenderSystem renderSystem, int resourceID, int width, int arrayCount, int mipMapCount, TextureOptions options)
        : base(renderSystem, resourceID)
    {

      m_device = renderSystem.D3DDevice;
      m_width = width;
      m_arrayCount = arrayCount;
      m_mipCount = mipMapCount;
      m_format = options.Format;
      m_resourceUsage = options.ResourceUsage;
      m_resourceBindFlags = ResourceBindFlags.Default | ResourceBindFlags.ShaderResource; // Always set shader

      D3D11.Texture1DDescription desc = new D3D11.Texture1DDescription();
      desc.ArraySize = m_arrayCount;
      desc.MipLevels = m_mipCount;
      desc.Width = m_width;
      desc.Usage = Direct3DHelper.ToD3DResourceUsage(m_resourceUsage);
      desc.Format = Direct3DHelper.ToD3DSurfaceFormat(m_format);
      desc.BindFlags = D3D11.BindFlags.ShaderResource;
      desc.CpuAccessFlags = (m_resourceUsage == ResourceUsage.Dynamic) ? D3D11.CpuAccessFlags.Write : D3D11.CpuAccessFlags.None;
      desc.OptionFlags = D3D11.ResourceOptionFlags.None;

      using (PooledArray<MemoryHandle> handles = BufferHelper.PinPooled(options.Data))
      {
        m_nativeTexture = new D3D11.Texture1D(m_device, desc, ResourceHelper.AsDataBoxArray(handles));
        m_shaderResourceView = new D3D11.ShaderResourceView(m_device, m_nativeTexture);

        BufferHelper.Unpin(handles);
      }

      SetDefaultDebugName((m_arrayCount > 1) ? "Texture1DArray" : "Texture1D");
    }

    /// <summary>
    /// Reads data from the texture into the specified data buffer.
    /// </summary>
    /// <typeparam name="T">Type of data to read from the texture.</typeparam>
    /// <param name="data">Buffer to hold data copied from the texture.</param>
    /// <param name="subIndex">Index to identify which subresource (e.g. mipmap, array slice) to access.</param>
    /// <param name="subimage">The subimage region, in texels, of the 1D texture to read from, if null the whole image is read from.</param>
    /// <exception cref="ArgumentNullException">Thrown if the buffer is null or length zero.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the mip level/array slice are out of bounds, or if the start index and element count are out of bounds, or if the data type to and texture format msimatch in size,
    /// or if the total number of bytes to copy from the sub resource does not match the actual data size of the region to copy.</exception>
    public void GetData<T>(Span<T> data, SubResourceAt subIndex, ResourceRegion1D? subimage) where T : unmanaged
    {
      D3D11.DeviceContext d3dContext = m_device.ImmediateContext;

      ResourceRegion3D? region = null;
      if (subimage.HasValue)
        region = new ResourceRegion3D(subimage.Value);

      ResourceHelper.ReadTextureData<T>(m_nativeTexture, d3dContext, m_width, 1, 1, m_arrayCount, m_mipCount, m_format, m_resourceUsage, false,
          data, subIndex, region);
    }

    /// <summary>
    /// Writes data to the texture from the specified data buffer.
    /// </summary>
    /// <typeparam name="T">Type of data to write to the texture.</typeparam>
    /// <param name="renderContext">The current render context.</param>
    /// <param name="data">Buffer with the data to be copied to the texture.</param>
    /// <param name="subIndex">Index to identify which subresource (e.g. mipmap, array slice) to access.</param>
    /// <param name="subimage">The subimage region, in texels, of the 1D texture to write to, if null the whole image is written to.</param>
    /// <param name="writeOptions">Writing options, valid only for dynamic textures.</param>
    /// <exception cref="ArgumentNullException">Thrown if the buffer is null or length zero.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the mip level/array slice are out of bounds, or if the start index and element count are out of bounds, or if the data type to and texture format msimatch in size,
    /// or if the total number of bytes to copy from the sub resource does not match the actual data size of the region to copy.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if attempting to write to an immutable resource.</exception>
    public void SetData<T>(IRenderContext renderContext, ReadOnlySpan<T> data, SubResourceAt subIndex, ResourceRegion1D? subimage, DataWriteOptions writeOptions) where T : unmanaged
    {
      D3D11.DeviceContext d3dContext = Direct3DHelper.GetD3DDeviceContext(renderContext);

      ResourceRegion3D? region = null;
      if (subimage.HasValue)
        region = new ResourceRegion3D(subimage.Value);

      ResourceHelper.WriteTextureData<T>(m_nativeTexture, d3dContext, m_width, 1, 1, m_arrayCount, m_mipCount, m_format, m_resourceUsage, false,
          data, subIndex, region, writeOptions);
    }

    /// <summary>
    /// Gets a sub texture at the specified array index.
    /// </summary>
    /// <param name="arrayIndex">Zero-based index of the sub texture.</param>
    /// <returns>The sub texture.</returns>
    public IShaderResource? GetSubTexture(int arrayIndex)
    {
      //Always return null if not an array resource
      if (m_arrayCount == 1)
        return null;

      //Return null if out of bounds
      if (arrayIndex < 0 || arrayIndex >= m_arrayCount)
        return null;

      if (m_subTextures is null)
        m_subTextures = new D3D11ShaderResourceWrapper[m_arrayCount];

      D3D11ShaderResourceWrapper subTexture = m_subTextures[arrayIndex];

      //Create the SRV as needed
      if (subTexture is null)
      {
        D3D11.ShaderResourceViewDescription desc = new D3D11.ShaderResourceViewDescription();
        desc.Texture1DArray.FirstArraySlice = arrayIndex;
        desc.Texture1DArray.ArraySize = 1;
        desc.Texture1DArray.MipLevels = MipCount;
        desc.Texture1DArray.MostDetailedMip = 0;
        desc.Format = Direct3DHelper.ToD3DSurfaceFormat(m_format);
        desc.Dimension = D3D.ShaderResourceViewDimension.Texture1DArray;

        subTexture = new D3D11ShaderResourceWrapper(new D3D11.ShaderResourceView(m_device, m_nativeTexture, desc), true);
        m_subTextures[arrayIndex] = subTexture;
      }

      return subTexture;
    }

    /// <summary>
    /// Called when the name of the graphics resource is changed, useful if the implementation wants to set the name to
    /// be used as a debug name.
    /// </summary>
    /// <param name="name">New name of the resource</param>
    protected override void OnDebugNameChange(String name)
    {
      if (m_nativeTexture is not null)
        m_nativeTexture.DebugName = name;

      if (m_shaderResourceView is not null)
        m_shaderResourceView.DebugName = (String.IsNullOrEmpty(name)) ? name : name + "_SRV";
    }

    /// <summary>
    /// Releases unmanaged and - optionally - managed resources.
    /// </summary>
    /// <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
    protected override void Dispose(bool disposing)
    {
      if (!IsDisposed)
      {
        if (disposing)
        {
          if (m_nativeTexture is not null)
          {
            m_nativeTexture.Dispose();
            m_nativeTexture = null!;
          }

          if (m_shaderResourceView is not null)
          {
            m_shaderResourceView.Dispose();
            m_shaderResourceView = null!;
          }

          if (m_subTextures is not null)
          {
            for (int i = 0; i < m_subTextures.Length; i++)
            {
              D3D11ShaderResourceWrapper subTexture = m_subTextures[i];
              if (subTexture is not null)
                subTexture.Dispose();
            }
          }

          m_subTextures = null!;
        }

        base.Dispose(disposing);
      }
    }
  }
}
