﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Graphics;
using Tesla.Graphics.Implementation;
using D3D11 = SharpDX.Direct3D11;

#nullable enable

namespace Tesla.Direct3D11.Graphics.Implementation
{
  /// <summary>
  /// A factory that creates Direct3D11 implementations of type <see cref="IDepthStencilStateImpl"/>.
  /// </summary>
  public sealed class D3D11DepthStencilStateImplFactory : D3D11GraphicsResourceImplFactory, IDepthStencilStateImplFactory
  {
    /// <summary>
    /// Constructs a new instance of the <see cref="D3D11DepthStencilStateImplFactory"/> class.
    /// </summary>
    public D3D11DepthStencilStateImplFactory() : base(typeof(DepthStencilState)) { }

    /// <summary>
    /// Creates a new implementation object instance.
    /// </summary>
    /// <returns>The render state implementation.</returns>
    public IDepthStencilStateImpl CreateImplementation()
    {
      return new D3D11DepthStencilStateImpl(D3D11RenderSystem, GetNextUniqueResourceID());
    }

    /// <summary>
    /// Called when the factory is initialized.
    /// </summary>
    /// <param name="renderSystem">The D3D11 render system.</param>
    protected override void OnInitialize(D3D11RenderSystem renderSystem)
    {
      RegisterFactory<IDepthStencilStateImplFactory>(this);
    }
  }

  /// <summary>
  /// A Direct3D11 implementation for <see cref="DepthStencilState"/>.
  /// </summary>
  public sealed class D3D11DepthStencilStateImpl : DepthStencilStateImpl, ID3D11DepthStencilState
  {
    private D3D11.Device m_device;
    private D3D11.DepthStencilState? m_nativeDepthStencilState;

    /// <summary>
    /// Gets the native D3D11 device.
    /// </summary>
    public D3D11.Device D3DDevice
    {
      get
      {
        return m_device;
      }
    }

    /// <summary>
    /// Gets the native D3D11 depth stencil state.
    /// </summary>
    public D3D11.DepthStencilState D3DDepthStencilState
    {
      get
      {
        return m_nativeDepthStencilState!;
      }
    }

    internal D3D11DepthStencilStateImpl(D3D11RenderSystem renderSystem, int resourceID)
        : base(renderSystem, resourceID)
    {

      m_device = renderSystem.D3DDevice;

      SetDefaultDebugName("DepthStencilState");
    }

    /// <summary>
    /// Called when the state is bound, signaling the implementation to create and bind the native state.
    /// </summary>
    protected override void CreateNativeState()
    {
      D3D11.DepthStencilStateDescription desc = new D3D11.DepthStencilStateDescription();
      desc.IsDepthEnabled = DepthEnable;
      desc.IsStencilEnabled = StencilEnable;
      desc.DepthWriteMask = (DepthWriteEnable) ? D3D11.DepthWriteMask.All : D3D11.DepthWriteMask.Zero;
      desc.DepthComparison = Direct3DHelper.ToD3DComparison(DepthFunction);
      desc.StencilWriteMask = (byte) StencilWriteMask;
      desc.StencilReadMask = (byte) StencilReadMask;

      //Set front facing stencil op
      D3D11.DepthStencilOperationDescription frontOpDesc = new D3D11.DepthStencilOperationDescription();
      frontOpDesc.Comparison = Direct3DHelper.ToD3DComparison(StencilFunction);
      frontOpDesc.DepthFailOperation = Direct3DHelper.ToD3DStencilOperation(StencilDepthFail);
      frontOpDesc.FailOperation = Direct3DHelper.ToD3DStencilOperation(StencilFail);
      frontOpDesc.PassOperation = Direct3DHelper.ToD3DStencilOperation(StencilPass);

      //Setup back facing stencil op
      D3D11.DepthStencilOperationDescription backOpDesc = new D3D11.DepthStencilOperationDescription();

      if (TwoSidedStencilEnable)
      {
        backOpDesc.Comparison = Direct3DHelper.ToD3DComparison(BackFaceStencilFunction);
        backOpDesc.DepthFailOperation = Direct3DHelper.ToD3DStencilOperation(BackFaceStencilDepthFail);
        backOpDesc.FailOperation = Direct3DHelper.ToD3DStencilOperation(BackFaceStencilFail);
        backOpDesc.PassOperation = Direct3DHelper.ToD3DStencilOperation(BackFaceStencilPass);
      }
      else
      {
        //Set defaults
        backOpDesc.Comparison = D3D11.Comparison.Always;
        backOpDesc.DepthFailOperation = D3D11.StencilOperation.Keep;
        backOpDesc.FailOperation = D3D11.StencilOperation.Keep;
        backOpDesc.PassOperation = D3D11.StencilOperation.Keep;
      }

      desc.FrontFace = frontOpDesc;
      desc.BackFace = backOpDesc;

      m_nativeDepthStencilState = new D3D11.DepthStencilState(m_device, desc);
      m_nativeDepthStencilState.DebugName = DebugName;
    }

    /// <summary>
    /// Called when the name of the graphics resource is changed, useful if the implementation wants to set the name to
    /// be used as a debug name.
    /// </summary>
    /// <param name="name">New name of the resource</param>
    protected override void OnDebugNameChange(string name)
    {
      if (m_nativeDepthStencilState is not null)
        m_nativeDepthStencilState.DebugName = name;
    }

    /// <summary>
    /// Releases unmanaged and - optionally - managed resources.
    /// </summary>
    /// <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
    protected override void Dispose(bool disposing)
    {
      if (!IsDisposed)
      {
        if (disposing)
        {
          if (m_nativeDepthStencilState is not null)
          {
            m_nativeDepthStencilState.Dispose();
            m_nativeDepthStencilState = null;
          }
        }

        base.Dispose(disposing);
      }
    }
  }
}
