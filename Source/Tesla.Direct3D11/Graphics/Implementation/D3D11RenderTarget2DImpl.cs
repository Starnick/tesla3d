﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Graphics;
using Tesla.Graphics.Implementation;
using D3D11 = SharpDX.Direct3D11;

#nullable enable

namespace Tesla.Direct3D11.Graphics.Implementation
{
  /// <summary>
  /// A factory that creates Direct3D11 implementations of type <see cref="IRenderTarget2DImpl"/>.
  /// </summary>
  public sealed class D3D11RenderTarget2DImplFactory : D3D11GraphicsResourceImplFactory, IRenderTarget2DImplFactory
  {

    /// <summary>
    /// Gets if a render target can be created with a shared depth stencil buffer.
    /// </summary>
    public bool SupportsDepthBufferSharing
    {
      get
      {
        return true;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="D3D11RenderTarget2DImplFactory"/> class.
    /// </summary>
    public D3D11RenderTarget2DImplFactory() : base(typeof(RenderTarget2D)) { }

    /// <summary>
    /// Creates a new implementation object instance.
    /// </summary>
    /// <param name="width">Width of the target, in texels.</param>
    /// <param name="height">Height of the target, in texels.</param>
    /// <param name="mipMapCount">Number of mip map levels, must be greater than zero. If the target has MSAA it only will have one mip map level.</param>
    /// <param name="format">Surface format of the render target.</param>
    /// <param name="preferredMSAA">Preferred MSAA settings, if not supported the next best possible valid setting will be used.</param>
    /// <param name="depthFormat">Depth format of the depth-stencil buffer, if any. If this is set to none, no depth buffer is created.</param>
    /// <param name="preferReadableDepth">True if it is preferred that the depth-stencil buffer is readable, that is if it can be bound as a shader resource, false otherwise. This may or may not be supported.</param>
    /// <param name="targetUsage">Target usage, specifying how the render target should be handled when it is bound to the pipeline.</param>
    /// <returns>The render target implementation</returns>
    public IRenderTarget2DImpl CreateImplementation(int width, int height, int mipMapCount, SurfaceFormat format, MSAADescription preferredMSAA, DepthFormat depthFormat, bool preferReadableDepth, RenderTargetUsage targetUsage)
    {
      return new D3D11RenderTarget2DImpl(D3D11RenderSystem, GetNextUniqueResourceID(), width, height, 1, mipMapCount, format, preferredMSAA, depthFormat, preferReadableDepth, targetUsage);
    }

    /// <summary>
    /// Creates a new implementation object instance. If the depth stencil buffer is not shareable, this will fail.
    /// </summary>
    /// <param name="format">Surface format of the render target.</param>
    /// <param name="mipMapCount">Number of mip map levels, must be greater than zero. If the target has MSAA it only will have one mip map level.</param>
    /// <param name="depthBuffer">Depth stencil buffer that is to be shared with this render target and dictate dimension and MSAA settings. It cannot be null.</param>
    /// <returns>The render target implementation</returns>
    /// <exception cref="Tesla.Graphics.TeslaGraphicsException">Thrown if the depth buffer is not valid.</exception>
    public IRenderTarget2DImpl CreateImplementation(SurfaceFormat format, int mipMapCount, IDepthStencilBuffer depthBuffer)
    {
      D3D11DepthStencilBufferWrapper? d3d11DepthBuffer = depthBuffer as D3D11DepthStencilBufferWrapper;

      if (d3d11DepthBuffer is null || d3d11DepthBuffer.D3DDevice != D3D11RenderSystem.D3DDevice)
        throw new TeslaGraphicsException(StringLocalizer.Instance.GetLocalizedString("DepthBufferInvalid"));

      return new D3D11RenderTarget2DImpl(D3D11RenderSystem, GetNextUniqueResourceID(), format, mipMapCount, d3d11DepthBuffer);
    }

    /// <summary>
    /// Called when the factory is initialized.
    /// </summary>
    /// <param name="renderSystem">The D3D11 render system.</param>
    protected override void OnInitialize(D3D11RenderSystem renderSystem)
    {
      RegisterFactory<IRenderTarget2DImplFactory>(this);
    }
  }

  /// <summary>
  /// A factory that creates Direct3D11 implementations of type <see cref="IRenderTarget2DArrayImpl"/>.
  /// </summary>
  public sealed class D3D11RenderTarget2DArrayImplFactory : D3D11GraphicsResourceImplFactory, IRendertarget2DArrayImplFactory
  {

    /// <summary>
    /// Gets if a render target can be created with a shared depth stencil buffer.
    /// </summary>
    public bool SupportsDepthBufferSharing
    {
      get
      {
        return true;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="D3D11RenderTarget2DArrayImplFactory"/> class.
    /// </summary>
    public D3D11RenderTarget2DArrayImplFactory() : base(typeof(RenderTarget2DArray)) { }

    /// <summary>
    /// Creates a new implementation object instance.
    /// </summary>
    /// <param name="width">Width of the target, in texels.</param>
    /// <param name="height">Height of the target, in texels.</param>
    /// <param name="arrayCount">Number of array slices, must be greater than zero.</param>
    /// <param name="mipMapCount">Number of mip map levels, must be greater than zero. If the target has MSAA it only will have one mip map level.</param>
    /// <param name="format">Surface format of the render target.</param>
    /// <param name="preferredMSAA">Preferred MSAA settings, if not supported the next best possible valid setting will be used.</param>
    /// <param name="depthFormat">Depth fo++rmat of the depth-stencil buffer, if any. If this is set to none, no depth buffer is created.</param>
    /// <param name="preferReadableDepth">True if it is preferred that the depth-stencil buffer is readable, that is if it can be bound as a shader resource, false otherwise. This may or may not be supported.</param>
    /// <param name="targetUsage">Target usage, specifying how the render target should be handled when it is bound to the pipeline.</param>
    /// <returns>The render target implementation</returns>
    public IRenderTarget2DArrayImpl CreateImplementation(int width, int height, int arrayCount, int mipMapCount, SurfaceFormat format, MSAADescription preferredMSAA, DepthFormat depthFormat, bool preferReadableDepth, RenderTargetUsage targetUsage)
    {
      return new D3D11RenderTarget2DImpl(D3D11RenderSystem, GetNextUniqueResourceID(), width, height, arrayCount, mipMapCount, format, preferredMSAA, depthFormat, preferReadableDepth, targetUsage);
    }

    /// <summary>
    /// Creates a new implementation object instance. If the depth stencil buffer is not shareable, this will fail.
    /// </summary>
    /// <param name="format">Surface format of the render target.</param>
    /// <param name="mipMapCount">Number of mip map levels, must be greater than zero. If the target has MSAA it only will have one mip map level.</param>
    /// <param name="depthBuffer">Depth stencil buffer that is to be shared with this render target and dictate dimension and MSAA settings. It cannot be null.</param>
    /// <returns>The render target implementation</returns>
    /// <exception cref="Tesla.Graphics.TeslaGraphicsException">Thrown if the depth buffer is not valid.</exception>
    public IRenderTarget2DArrayImpl CreateImplementation(SurfaceFormat format, int mipMapCount, IDepthStencilBuffer depthBuffer)
    {
      D3D11DepthStencilBufferWrapper? d3d11DepthBuffer = depthBuffer as D3D11DepthStencilBufferWrapper;

      if (d3d11DepthBuffer is null || d3d11DepthBuffer.D3DDevice != D3D11RenderSystem.D3DDevice)
        throw new TeslaGraphicsException(StringLocalizer.Instance.GetLocalizedString("DepthBufferInvalid"));

      return new D3D11RenderTarget2DImpl(D3D11RenderSystem, GetNextUniqueResourceID(), format, mipMapCount, d3d11DepthBuffer);
    }

    /// <summary>
    /// Called when the factory is initialized.
    /// </summary>
    /// <param name="renderSystem">The D3D11 render system.</param>
    protected override void OnInitialize(D3D11RenderSystem renderSystem)
    {
      RegisterFactory<IRendertarget2DArrayImplFactory>(this);
    }
  }

  /// <summary>
  /// A Direct3D11 implementation for <see cref="RenderTarget2D"/> and <see cref="RenderTarget2DArray"/>.
  /// </summary>
  public sealed class D3D11RenderTarget2DImpl : GraphicsResourceImpl, IRenderTarget2DImpl, IRenderTarget2DArrayImpl, ID3D11RenderTargetView, ID3D11ShaderResourceView
  {
    private D3D11RenderTargetWrapper m_targetWrapper;

    /// <summary>
    /// Gets the depth stencil buffer associated with the render target, if no buffer is associated then this will be null.
    /// </summary>
    public IDepthStencilBuffer DepthStencilBuffer
    {
      get
      {
        return m_targetWrapper.DepthStencilBuffer;
      }
    }

    /// <summary>
    /// Gets the multisample settings for the resource. The MSAA count, quality, and if the resource should be resolved to
    /// a non-MSAA resource for shader input. MSAA targets that do not resolve to a non-MSAA resource will only ever have one mip map per array slice.
    /// </summary>
    public MSAADescription MultisampleDescription
    {
      get
      {
        return m_targetWrapper.MultisampleDescription;
      }
    }

    /// <summary>
    /// Gets the target usage, specifying how the target should be handled when it is bound to the pipeline. Generally this is
    /// set to discard by default.
    /// </summary>
    public RenderTargetUsage TargetUsage
    {
      get
      {
        return m_targetWrapper.TargetUsage;
      }
    }

    /// <summary>
    /// Gets the format of the texture resource.
    /// </summary>
    public SurfaceFormat Format
    {
      get
      {
        return m_targetWrapper.Format;
      }
    }

    /// <summary>
    /// Gets the number of array slices in the texture. Slices may be indexed in the range [0, ArrayCount).
    /// </summary>
    public int ArrayCount
    {
      get
      {
        return m_targetWrapper.ArrayCount;
      }
    }

    /// <summary>
    /// Gets the number of mip map levels in the texture resource. Mip levels may be indexed in the range of [0, MipCount).
    /// </summary>
    public int MipCount
    {
      get
      {
        return m_targetWrapper.MipCount;
      }
    }

    /// <summary>
    /// Gets the texture width, in texels.
    /// </summary>
    public int Width
    {
      get
      {
        return m_targetWrapper.Width;
      }
    }

    /// <summary>
    /// Gets the texture height, in texels.
    /// </summary>
    public int Height
    {
      get
      {
        return m_targetWrapper.Height;
      }
    }

    /// <inheritdoc />
    public ResourceUsage ResourceUsage
    {
      get
      {
        return ResourceUsage.Static;
      }
    }

    /// <inheritdoc />
    public ResourceBindFlags ResourceBindFlags
    {
      get
      {
        return ResourceBindFlags.Default | ResourceBindFlags.ShaderResource;
      }
    }

    /// <summary>
    /// Gets the native D3D11 device.
    /// </summary>
    public D3D11.Device D3DDevice
    {
      get
      {
        return m_targetWrapper.D3DDevice;
      }
    }

    /// <summary>
    /// Gets the native D3D render target view.
    /// </summary>
    public D3D11.RenderTargetView D3DRenderTargetView
    {
      get
      {
        return m_targetWrapper.D3DRenderTargetView;
      }
    }

    /// <summary>
    /// Gets the native D3D11 shader resource view. If multisampled and set to resolve,
    /// this is a view of the resolve texture, otherwise its a view of the non-resolve texture.
    /// </summary>
    public D3D11.ShaderResourceView D3DShaderResourceView
    {
      get
      {
        return m_targetWrapper.D3DShaderResourceView;
      }
    }

    /// <summary>
    /// Gets the native D3D11 texture (may or may not be multisampled).
    /// </summary>
    public D3D11.Resource D3DTexture
    {
      get
      {
        return m_targetWrapper.D3DTexture;
      }
    }

    /// <summary>
    /// Gets the native D3D11 resolve texture (never multisampled), if it exists.
    /// </summary>
    public D3D11.Resource D3DResolveTexture
    {
      get
      {
        return m_targetWrapper.D3DResolveTexture;
      }
    }

    /// <summary>
    /// Gets or sets the shader stages that this resource is currently bound to.
    /// </summary>
    public ShaderStageBinding BoundStages
    {
      get
      {
        return m_targetWrapper.BoundStages;
      }
      set
      {
        m_targetWrapper.BoundStages = value;
      }
    }

    internal D3D11RenderTarget2DImpl(D3D11RenderSystem renderSystem, int resourceID, int width, int height, int arrayCount, int mipMapCount, SurfaceFormat format, MSAADescription preferredMSAA, DepthFormat depthFormat, bool preferReadableDepth, RenderTargetUsage targetUsage)
        : base(renderSystem, resourceID)
    {

      m_targetWrapper = new D3D11RenderTargetWrapper(renderSystem.D3DDevice, width, height, false, arrayCount, mipMapCount, format, preferredMSAA, depthFormat, preferReadableDepth, arrayCount > 1, targetUsage);

      SetDefaultDebugName("RenderTarget2D");
    }

    internal D3D11RenderTarget2DImpl(D3D11RenderSystem renderSystem, int resourceID, SurfaceFormat format, int mipMapCount, D3D11DepthStencilBufferWrapper depthBuffer)
        : base(renderSystem, resourceID)
    {

      m_targetWrapper = new D3D11RenderTargetWrapper(format, mipMapCount, depthBuffer);

      SetDefaultDebugName("RenderTarget2D");
    }

    /// <summary>
    /// Gets the generic render target implementation.
    /// </summary>
    /// <returns></returns>
    public D3D11RenderTargetWrapper GetD3D11RenderTargetWrapper()
    {
      return m_targetWrapper;
    }

    /// <summary>
    /// Gets a sub render target at the specified array index.
    /// </summary>
    /// <param name="arrayIndex">Zero-based index of the sub render target.</param>
    /// <returns>The sub render target.</returns>
    public IRenderTarget GetSubRenderTarget(int arrayIndex)
    {
      return m_targetWrapper.GetSubRenderTarget(arrayIndex);
    }

    /// <summary>
    /// Gets a sub texture at the specified array index.
    /// </summary>
    /// <param name="arraySlice">Zero-based index of the sub texture.</param>
    /// <returns>The sub texture.</returns>
    public IShaderResource GetSubTexture(int arraySlice)
    {
      return m_targetWrapper.GetSubRenderTarget(arraySlice);
    }

    /// <summary>
    /// Called on the first render target in the group before the group is bound to the context.
    /// </summary>
    public void NotifyOnFirstBind()
    {
      m_targetWrapper.NotifyOnFirstBind();
    }

    /// <summary>
    /// Resolves the resource if its multisampled and does any mip map generation.
    /// </summary>
    /// <param name="deviceContext">Device context</param>
    public void ResolveResource(D3D11.DeviceContext deviceContext)
    {
      m_targetWrapper.ResolveResource(deviceContext);
    }

    /// <summary>
    /// Clears the render target.
    /// </summary>
    /// <param name="deviceContext">Device context</param>
    /// <param name="options">Clear options</param>
    /// <param name="color">Color to clear to.</param>
    /// <param name="depth">Depth to clear to.</param>
    /// <param name="stencil">Stencil to clear to</param>
    public void Clear(D3D11.DeviceContext deviceContext, ClearOptions options, Color color, float depth, int stencil)
    {
      m_targetWrapper.Clear(deviceContext, options, color, depth, stencil);
    }

    /// <summary>
    /// Reads data from the texture into the specified data buffer.
    /// </summary>
    /// <typeparam name="T">Type of data to read from the texture.</typeparam>
    /// <param name="data">Buffer to hold data copied from the texture.</param>
    /// <param name="subIndex">Index to identify which subresource (e.g. mipmap, array slice) to access.</param>
    /// <param name="subimage">The subimage region, in texels, of the 2D texture to read from, if null the whole image is read from.</param>
    /// <exception cref="ArgumentNullException">Thrown if the buffer is null or length zero.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the mip level/array slice are out of bounds, or if the start index and element count are out of bounds, or if the data type to and texture format msimatch in size,
    /// or if the total number of bytes to copy from the sub resource does not match the actual data size of the region to copy.</exception>
    public void GetData<T>(Span<T> data, SubResourceAt subIndex, ResourceRegion2D? subimage) where T : unmanaged
    {
      D3D11.DeviceContext d3dContext = m_targetWrapper.D3DDevice.ImmediateContext;

      ResourceRegion3D? region;
      if (subimage.HasValue)
      {
        region = new ResourceRegion3D(subimage.Value);
      }
      else
      {
        region = new ResourceRegion3D(0, m_targetWrapper.Width, 0, m_targetWrapper.Height, 0, 1);
      }

      D3D11.Resource resource = m_targetWrapper.D3DResolveTexture ?? m_targetWrapper.D3DTexture;

      ResourceHelper.ReadTextureData<T>(resource, d3dContext, m_targetWrapper.Width, m_targetWrapper.Height, 1, m_targetWrapper.ArrayCount, m_targetWrapper.MipCount, m_targetWrapper.Format, ResourceUsage.Static, false,
          data, subIndex, region);
    }


    /// <summary>
    /// Writes data to the texture from the specified data buffer.
    /// </summary>
    /// <typeparam name="T">Type of data to write to the texture.</typeparam>
    /// <param name="renderContext">The current render context.</param>
    /// <param name="data">Buffer with the data to be copied to thetexture.</param>
    /// <param name="subIndex">Index to identify which subresource (e.g. mipmap, array slice) to access.</param>
    /// <param name="subimage">The subimage region, in texels, of the 2D texture to write to, if null the whole image is written to.</param>
    /// <param name="writeOptions">Writing options, valid only for dynamic textures.</param>
    /// <exception cref="ArgumentNullException">Thrown if the data buffer is null or length zero.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the mip level/array slice are out of bounds, or if the start index and element count are out of bounds, or if the data type to and texture format msimatch in size,
    /// or if the total number of bytes to copy from the sub resource does not match the actual data size of the region to copy.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if attempting to write to an immutable resource.</exception>
    public void SetData<T>(IRenderContext renderContext, ReadOnlySpan<T> data, SubResourceAt subIndex, ResourceRegion2D? subimage, DataWriteOptions writeOptions) where T : unmanaged
    {
      D3D11.DeviceContext d3dContext = Direct3DHelper.GetD3DDeviceContext(renderContext);

      ResourceRegion3D? region;
      if (subimage.HasValue)
      {
        region = new ResourceRegion3D(subimage.Value);
      }
      else
      {
        region = new ResourceRegion3D(0, m_targetWrapper.Width, 0, m_targetWrapper.Height, 0, 1);
      }

      D3D11.Resource resource = m_targetWrapper.D3DResolveTexture ?? m_targetWrapper.D3DTexture;

      ResourceHelper.WriteTextureData<T>(resource, d3dContext, m_targetWrapper.Width, m_targetWrapper.Height, 1, m_targetWrapper.ArrayCount, m_targetWrapper.MipCount, m_targetWrapper.Format, ResourceUsage.Static, false,
          data, subIndex, region, writeOptions);
    }

    /// <summary>
    /// Called when the name of the graphics resource is changed, useful if the implementation wants to set the name to
    /// be used as a debug name.
    /// </summary>
    /// <param name="name">New name of the resource</param>
    protected override void OnDebugNameChange(string name)
    {
      m_targetWrapper.Name = name;
    }

    /// <summary>
    /// Releases unmanaged and - optionally - managed resources.
    /// </summary>
    /// <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
    protected override void Dispose(bool disposing)
    {
      if (!IsDisposed)
      {
        if (disposing)
          m_targetWrapper.Dispose();

        base.Dispose(disposing);
      }
    }

  }
}
