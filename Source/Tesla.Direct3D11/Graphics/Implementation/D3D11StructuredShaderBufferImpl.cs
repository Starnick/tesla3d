﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Buffers;
using Tesla.Graphics;
using Tesla.Graphics.Implementation;
using D3D = SharpDX.Direct3D;
using D3D11 = SharpDX.Direct3D11;
using DXGI = SharpDX.DXGI;

#nullable enable

namespace Tesla.Direct3D11.Graphics.Implementation
{
  public sealed class D3D11StructuredShaderBufferImplFactory : D3D11GraphicsResourceImplFactory, IStructuredShaderBufferImplFactory
  {
    public D3D11StructuredShaderBufferImplFactory() : base(typeof(StructuredShaderBuffer)) { }

    /// <inheritdoc />
    public IStructuredShaderBufferImpl CreateImplementation(int elementStride, int elementCount, BufferOptions options)
    {
      return new D3D11StructuredShaderBufferImpl(D3D11RenderSystem, GetNextUniqueResourceID(), elementStride, elementCount, options);
    }

    /// <inheritdoc />
    protected override void OnInitialize(D3D11RenderSystem renderSystem)
    {
      RegisterFactory<IStructuredShaderBufferImplFactory>(this);
    }
  }

  public sealed class D3D11StructuredShaderBufferImpl : GraphicsResourceImpl, IStructuredShaderBufferImpl, ID3D11Buffer, ID3D11ShaderResourceView
  {
    private D3D11.Device m_device;
    private D3D11.Buffer m_nativeBuffer;
    private D3D11.ShaderResourceView m_shaderResourceView;
    private int m_elementCount;
    private int m_elementStride;
    private ResourceUsage m_resourceUsage;
    private ResourceBindFlags m_resourceBindFlags;
    private ShaderStageBinding m_boundStages;

    /// <inheritdoc />
    public int ElementCount
    {
      get
      {
        return m_elementCount;
      }
    }

    /// <inheritdoc />
    public int ElementStride
    {
      get
      {
        return m_elementStride;
      }
    }

    /// <inheritdoc />
    public ResourceUsage ResourceUsage
    {
      get
      {
        return m_resourceUsage;
      }
    }

    /// <inheritdoc />
    public ResourceBindFlags ResourceBindFlags
    {
      get
      {
        return m_resourceBindFlags;
      }
    }

    /// <summary>
    /// Gets the native D3D11 device.
    /// </summary>
    public D3D11.Device D3DDevice
    {
      get
      {
        return m_device;
      }
    }

    /// <inheritdoc />
    public D3D11.Buffer D3DBuffer
    {
      get
      {
        return m_nativeBuffer;
      }
    }

    /// <inheritdoc />
    public D3D11.ShaderResourceView D3DShaderResourceView
    {
      get
      {
        return m_shaderResourceView;
      }
    }

    /// <inheritdoc />
    public ShaderStageBinding BoundStages
    {
      get
      {
        return m_boundStages;
      }
      set
      {
        m_boundStages = value;
      }
    }

    internal D3D11StructuredShaderBufferImpl(D3D11RenderSystem renderSystem, int resourceID, int elementStride, int elementCount, BufferOptions options)
      : base(renderSystem, resourceID)
    {
      m_device = renderSystem.D3DDevice;
      m_elementStride = elementStride;
      m_elementCount = elementCount;
      m_resourceUsage = options.ResourceUsage;
      m_resourceBindFlags = ResourceBindFlags.Default | ResourceBindFlags.ShaderResource; // Always set shader

      D3D11.BufferDescription desc = new D3D11.BufferDescription();
      desc.BindFlags = D3D11.BindFlags.ShaderResource;
      desc.Usage = Direct3DHelper.ToD3DResourceUsage(m_resourceUsage);
      desc.CpuAccessFlags = (m_resourceUsage == ResourceUsage.Dynamic) ? D3D11.CpuAccessFlags.Write : D3D11.CpuAccessFlags.None;
      desc.OptionFlags = D3D11.ResourceOptionFlags.BufferStructured;
      desc.SizeInBytes = m_elementCount * m_elementStride;
      desc.StructureByteStride = elementStride;

      using (MemoryHandle handle = (options.Data is not null) ? options.Data.Pin() : default)
        m_nativeBuffer = new D3D11.Buffer(m_device, handle.AsIntPointer(), desc);

      m_shaderResourceView = new D3D11.ShaderResourceView(m_device, m_nativeBuffer);

      SetDefaultDebugName("StructuredShaderBuffer");
    }

    /// <inheritdoc />
    /// <param name="offsetInBytes">Offset from the start of the index buffer at which to start copying from</param>
    /// <exception cref="ArgumentNullException">Thrown if the buffer is empty.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if any of the count or offsets are out of range, or would cause an overflow in the copy operation.</exception>
    public void GetData<T>(Span<T> data, int offsetInBytes) where T : unmanaged
    {
      D3D11.DeviceContext d3dContext = m_device.ImmediateContext;
      int bufferSizeInBytes = m_elementCount * m_elementStride;

      ResourceHelper.ReadBufferData<T>(m_nativeBuffer, d3dContext, bufferSizeInBytes, m_resourceUsage, data, offsetInBytes);
    }

    /// <inheritdoc />
    /// <exception cref="ArgumentNullException">Thrown if the buffer is empty.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if any of the count or offsets are out of range, or would cause an overflow in the copy operation.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if attempting to write to an immutable resource.</exception>
    public void SetData<T>(IRenderContext renderContext, ReadOnlySpan<T> data, int offsetInBytes, DataWriteOptions writeOptions) where T : unmanaged
    {
      D3D11.DeviceContext d3dContext = Direct3DHelper.GetD3DDeviceContext(renderContext);
      int bufferSizeInBytes = m_elementCount * m_elementStride;

      ResourceHelper.WriteBufferData<T>(m_nativeBuffer, d3dContext, bufferSizeInBytes, m_resourceUsage, data, offsetInBytes, writeOptions);
    }

    /// <inheritdoc />
    protected override void OnDebugNameChange(string name)
    {
      if (m_nativeBuffer is not null)
        m_nativeBuffer.DebugName = name;

      if (m_shaderResourceView is not null)
        m_shaderResourceView.DebugName = (String.IsNullOrEmpty(name)) ? name : name + "_SRV";
    }

    /// <inheritdoc />
    protected override void Dispose(bool disposing)
    {
      if (!IsDisposed)
      {
        if (disposing)
        {
          if (m_shaderResourceView is not null)
          {
            m_shaderResourceView.Dispose();
            m_shaderResourceView = null!;
          }

          if (m_nativeBuffer is not null)
          {
            m_nativeBuffer.Dispose();
            m_nativeBuffer = null!;
          }
        }
      }

      base.Dispose(disposing);
    }
  }
}
