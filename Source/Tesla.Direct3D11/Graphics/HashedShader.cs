﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using Tesla.Graphics;
using D3D11 = SharpDX.Direct3D11;
using SDX = SharpDX;
using DXGI = SharpDX.DXGI;

namespace Tesla.Direct3D11.Graphics
{
  public struct HashedShaderSignature : IEquatable<HashedShaderSignature>
  {
    public byte[] ByteCode;
    public int HashCode;

    public HashedShaderSignature(byte[] byteCode, int hashCode)
    {
      ByteCode = byteCode;
      HashCode = hashCode;
    }

    public static bool operator ==(HashedShaderSignature a, HashedShaderSignature b)
    {
      return (a.HashCode == b.HashCode) && BufferHelper.Compare(a.ByteCode, b.ByteCode);
    }

    public static bool operator !=(HashedShaderSignature a, HashedShaderSignature b)
    {
      return (a.HashCode != b.HashCode) || !BufferHelper.Compare(a.ByteCode, b.ByteCode);
    }

    public override bool Equals(object obj)
    {
      if (obj is HashedShaderSignature)
      {
        return Equals((HashedShaderSignature) obj);
      }

      return false;
    }

    public bool Equals(HashedShaderSignature other)
    {
      return (HashCode == other.HashCode) && BufferHelper.Compare(ByteCode, other.ByteCode);
    }

    public override int GetHashCode()
    {
      return HashCode;
    }
  }

  public struct HashedShader : IEquatable<HashedShader>
  {
    public D3D11.DeviceChild Shader;
    public int HashCode;
    public ShaderStage ShaderType;

    public HashedShader(D3D11.DeviceChild shader, int hashCode, ShaderStage shaderType)
    {
      Shader = shader;
      HashCode = hashCode;
      ShaderType = shaderType;
    }

    public static implicit operator HashedShader(D3D11.VertexShader shader)
    {
      return new HashedShader(shader, (shader is not null) ? shader.GetHashCode() : 0, ShaderStage.VertexShader);
    }

    public static implicit operator HashedShader(D3D11.PixelShader shader)
    {
      return new HashedShader(shader, (shader is not null) ? shader.GetHashCode() : 0, ShaderStage.PixelShader);
    }

    public static implicit operator HashedShader(D3D11.GeometryShader shader)
    {
      return new HashedShader(shader, (shader is not null) ? shader.GetHashCode() : 0, ShaderStage.GeometryShader);
    }

    public static implicit operator HashedShader(D3D11.HullShader shader)
    {
      return new HashedShader(shader, (shader is not null) ? shader.GetHashCode() : 0, ShaderStage.HullShader);
    }

    public static implicit operator HashedShader(D3D11.DomainShader shader)
    {
      return new HashedShader(shader, (shader is not null) ? shader.GetHashCode() : 0, ShaderStage.DomainShader);
    }

    public static implicit operator HashedShader(D3D11.ComputeShader shader)
    {
      return new HashedShader(shader, (shader is not null) ? shader.GetHashCode() : 0, ShaderStage.ComputeShader);
    }

    public static bool operator ==(HashedShader a, HashedShader b)
    {
      return (a.ShaderType == b.ShaderType) && (a.HashCode == b.HashCode) && Object.ReferenceEquals(a.Shader, b.Shader);
    }

    public static bool operator !=(HashedShader a, HashedShader b)
    {
      return (a.ShaderType != b.ShaderType) || (a.HashCode != b.HashCode) || !Object.ReferenceEquals(a.Shader, b.Shader);
    }

    public static HashedShader NullShader(ShaderStage stageType)
    {
      return new HashedShader(null, 0, stageType);
    }

    public override bool Equals(object obj)
    {
      if (obj is HashedShader)
        return Equals((HashedShader) obj);

      return false;
    }

    public bool Equals(HashedShader other)
    {
      return (ShaderType == other.ShaderType) && (HashCode == other.HashCode) && Object.ReferenceEquals(Shader, other.Shader);
    }

    public override int GetHashCode()
    {
      return HashCode;
    }
  }
}
