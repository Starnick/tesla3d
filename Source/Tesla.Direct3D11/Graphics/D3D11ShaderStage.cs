﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Graphics;
using D3D11 = SharpDX.Direct3D11;

namespace Tesla.Direct3D11.Graphics
{
  public sealed class D3D11ShaderStage : IShaderStage
  {
    private static readonly SamplerState[] EmptySamplersArray = new SamplerState[0];
    private static readonly IShaderResource[] EmptyResourceArray = new IShaderResource[0];
    private static readonly D3D11.Buffer[] EmptyConstantBufferArray = new D3D11.Buffer[0];

    private int m_maxSamplerSlots;
    private int m_maxResourceSlots;
    private int m_maxConstantBufferSlots;
    private int m_lastResourceSlotIndex;

    private SamplerState[] m_samplers;
    private IShaderResource[] m_resources;
    private D3D11.Buffer[] m_nativeConstantBuffers;
    private ShaderStage m_stageType;
    private ShaderStageBinding m_stageBinding;
    private D3D11.CommonShaderStage m_nativeShaderStage;
    private HashedShader m_hashedShader;

    //Temp arrays for passing data since SharpDX always reads the input array from index zero.
    private D3D11.SamplerState[] m_tempSamplers;
    private D3D11.ShaderResourceView[] m_tempResources;

    public int MaximumSamplerSlots
    {
      get
      {
        return m_samplers.Length;
      }
    }

    public int MaximumResourceSlots
    {
      get
      {
        return m_maxResourceSlots;
      }
    }

    public int MaximumConstantBufferSlots
    {
      get
      {
        return m_maxConstantBufferSlots;
      }
    }

    public ShaderStage StageType
    {
      get
      {
        return m_stageType;
      }
    }

    public ShaderStageBinding StageBinding
    {
      get
      {
        return m_stageBinding;
      }
    }

    public D3D11ShaderStage(D3D11.CommonShaderStage d3dShaderStage, ShaderStage stageType, int maxSamplerSlots, int maxResourceSlots, int maxConstantBufferSlots)
    {
      m_maxSamplerSlots = maxSamplerSlots;
      m_maxResourceSlots = maxResourceSlots;
      m_maxConstantBufferSlots = maxConstantBufferSlots;
      m_lastResourceSlotIndex = -1;

      m_samplers = new SamplerState[maxSamplerSlots];
      m_resources = new IShaderResource[maxResourceSlots];
      m_nativeConstantBuffers = new D3D11.Buffer[maxConstantBufferSlots];
      m_stageType = stageType;
      m_nativeShaderStage = d3dShaderStage;
      m_hashedShader = new HashedShader(null, 0, m_stageType);

      m_tempSamplers = new D3D11.SamplerState[maxSamplerSlots];
      m_tempResources = new D3D11.ShaderResourceView[maxResourceSlots];

      switch (m_stageType)
      {
        case ShaderStage.VertexShader:
          m_stageBinding = ShaderStageBinding.VertexShader;
          break;
        case ShaderStage.GeometryShader:
          m_stageBinding = ShaderStageBinding.GeometryShader;
          break;
        case ShaderStage.HullShader:
          m_stageBinding = ShaderStageBinding.HullShader;
          break;
        case ShaderStage.DomainShader:
          m_stageBinding = ShaderStageBinding.DomainShader;
          break;
        case ShaderStage.PixelShader:
          m_stageBinding = ShaderStageBinding.PixelShader;
          break;
        case ShaderStage.ComputeShader:
          m_stageBinding = ShaderStageBinding.ComputeShader;
          break;
        default:
          m_stageBinding = ShaderStageBinding.None;
          break;
      }
    }

    #region Samplers

    public void SetSampler(SamplerState sampler)
    {
      if (sampler is null)
        sampler = SamplerState.PointClamp;

      if (!sampler.IsSameState(m_samplers[0]))
      {
        m_samplers[0] = sampler;
        m_nativeShaderStage.SetSampler(0, (sampler.Implementation as ID3D11SamplerState).D3DSamplerState);
      }
    }

    public void SetSampler(int slotIndex, SamplerState sampler)
    {
      if (slotIndex < 0 || slotIndex >= m_maxSamplerSlots)
        return;

      if (sampler is null)
        sampler = SamplerState.PointClamp;

      if (!sampler.IsSameState(m_samplers[slotIndex]))
      {
        m_samplers[slotIndex] = sampler;
        m_nativeShaderStage.SetSampler(slotIndex, (sampler.Implementation as ID3D11SamplerState).D3DSamplerState);
      }
    }

    public void SetSamplers(params SamplerState[] samplers)
    {
      SetSamplers(0, samplers);
    }

    public void SetSamplers(int startSlotIndex, params SamplerState[] samplers)
    {
      int count = (samplers is not null) ? samplers.Length : 0;
      if (samplers is null || count == 0 || count > m_maxSamplerSlots)
        return;

      int applyCount = 0;
      for (int i = 0; i < count; i++)
      {
        int slotIndex = startSlotIndex + i;
        SamplerState sampler = samplers[i];
        if (sampler is null)
          sampler = SamplerState.PointClamp;

        //Set to temporary array
        m_tempSamplers[i] = (sampler.Implementation as ID3D11SamplerState).D3DSamplerState;

        //Check if the sampler is already set at the index, if so then we may -not- have to apply. But we need to keep checking.
        //If at least one sampler isn't set, we need to reset all. If all are already there, we can ignore setting (but still need to
        //clear the temp array)
        if (!sampler.IsSameState(m_samplers[slotIndex]))
        {
          m_samplers[slotIndex] = sampler;
          applyCount++;
        }
      }

      if (applyCount > 0)
        m_nativeShaderStage.SetSamplers(startSlotIndex, count, m_tempSamplers);

      Array.Clear(m_tempSamplers, 0, count);
    }

    public SamplerState[] GetSamplers(int startSlotIndex, int count)
    {
      if (startSlotIndex < 0 || count <= 0 || (startSlotIndex + count) > m_maxSamplerSlots)
        return EmptySamplersArray;

      SamplerState[] samplers = new SamplerState[count];
      Array.Copy(m_samplers, startSlotIndex, samplers, 0, count);

      return samplers;
    }

    #endregion

    #region Resources

    public void SetShaderResource(IShaderResource resource)
    {
      if (!Object.ReferenceEquals(m_resources[0], resource))
      {
        //Clear the previous resource's bound flag (if it exists), and set the incoming resource's bound flag
        RemoveBoundFlag(m_resources[0], m_stageBinding);
        SetBoundFlag(resource, m_stageBinding);

        m_resources[0] = resource;
        m_nativeShaderStage.SetShaderResource(0, Direct3DHelper.GetD3DShaderResourceView(resource));

        UpdateLastResourceIndex(0);
      }
    }

    public void SetShaderResource(int slotIndex, IShaderResource resource)
    {
      if (slotIndex < 0 || slotIndex >= m_maxResourceSlots)
        return;

      if (!Object.ReferenceEquals(m_resources[slotIndex], resource))
      {
        //Clear the previous resource's bound flag (if it exists), and set the incoming resource's bound flag
        RemoveBoundFlag(m_resources[slotIndex], m_stageBinding);
        SetBoundFlag(resource, m_stageBinding);

        m_resources[slotIndex] = resource;
        m_nativeShaderStage.SetShaderResource(slotIndex, Direct3DHelper.GetD3DShaderResourceView(resource));

        UpdateLastResourceIndex(slotIndex);
      }
    }

    public void SetShaderResources(params IShaderResource[] resources)
    {
      SetShaderResources(0, resources);
    }

    public void SetShaderResources(int startSlotIndex, params IShaderResource[] resources)
    {
      int count = (resources is not null) ? resources.Length : 0;
      if (resources is null || count == 0 || startSlotIndex < 0 || (startSlotIndex + count) > m_maxResourceSlots)
        return;

      int applyCount = 0;
      for (int i = 0; i < count; i++)
      {
        int slotIndex = startSlotIndex + i;
        IShaderResource resource = resources[i];

        //Set to temporary array, starting at the beginning rather than the slot index
        m_tempResources[i] = Direct3DHelper.GetD3DShaderResourceView(resource);

        //Check if the resource is already set at the index, if so then we may -not- have to apply. But we need to keep checking.
        //If at least one sampler isn't set, we need to reset all. If all are already there, we can ignore setting (but still need to
        //clear the temp array)
        if (!Object.ReferenceEquals(m_resources[slotIndex], resource))
        {
          //Clear the previous resource's bound flag (if it exists), and set the incoming resource's bound flag
          RemoveBoundFlag(m_resources[slotIndex], m_stageBinding);
          SetBoundFlag(resource, m_stageBinding);

          m_resources[slotIndex] = resource;
          applyCount++;
        }
      }

      if (applyCount > 0)
      {
        m_nativeShaderStage.SetShaderResources(startSlotIndex, count, m_tempResources);

        UpdateLastResourceIndex(startSlotIndex + (count - 1));
      }

      Array.Clear(m_tempResources, 0, count);
    }

    public IShaderResource[] GetShaderResources(int startSlotIndex, int count)
    {
      if (startSlotIndex < 0 || count <= 0 || (startSlotIndex + count) > m_maxResourceSlots)
        return EmptyResourceArray;

      IShaderResource[] resources = new IShaderResource[count];
      Array.Copy(m_resources, startSlotIndex, resources, 0, count);

      return resources;
    }

    #endregion

    #region Constant Buffers

    public void SetConstantBuffer(int slotIndex, D3D11.Buffer constantBuffer)
    {
      if (slotIndex < 0 || slotIndex >= m_maxConstantBufferSlots)
        return;

      if (!Object.ReferenceEquals(m_nativeConstantBuffers[slotIndex], constantBuffer))
      {
        m_nativeConstantBuffers[slotIndex] = constantBuffer;
        m_nativeShaderStage.SetConstantBuffer(slotIndex, constantBuffer);
      }
    }

    public void SetConstantBuffers(int startSlotIndex, params D3D11.Buffer[] constantBuffers)
    {
      int count = (constantBuffers is not null) ? constantBuffers.Length : 0;
      if (constantBuffers is null || count == 0 || startSlotIndex < 0 || (count + startSlotIndex) > m_maxConstantBufferSlots)
        return;

      int applyCount = 0;
      for (int i = 0; i < count; i++)
      {
        int slotIndex = startSlotIndex + i;
        D3D11.Buffer cb = constantBuffers[i];

        //Check if the CB is already set at the index, if so then we may -not- have to apply. But we need to keep checking.
        //If at least one CB isn't set, we need to reset all. If all are already there, we can ignore setting (but still need to
        //clear the temp array)
        if (!Object.ReferenceEquals(m_nativeConstantBuffers[slotIndex], cb))
        {
          m_nativeConstantBuffers[slotIndex] = cb;
          applyCount++;
        }
      }

      // No need to use a temp array as sharpdx reads from the beginning of the passed array and we're already working with its objects
      if (applyCount > 0)
        m_nativeShaderStage.SetConstantBuffers(startSlotIndex, count, constantBuffers);
    }

    public D3D11.Buffer[] GetConstantBuffers(int startSlotIndex, int count)
    {
      if (startSlotIndex < 0 || count <= 0 || (startSlotIndex + count) > m_maxConstantBufferSlots)
        return EmptyConstantBufferArray;

      D3D11.Buffer[] constantBuffers = new D3D11.Buffer[count];
      Array.Copy(m_nativeConstantBuffers, startSlotIndex, constantBuffers, 0, count);

      return constantBuffers;
    }

    #endregion

    #region Shader Setting

    public void SetShader(HashedShader shader)
    {
      if (m_stageType == shader.ShaderType && m_hashedShader.HashCode != shader.HashCode)
      {
        m_hashedShader = shader;

        switch (m_stageType)
        {
          case ShaderStage.VertexShader:
            (m_nativeShaderStage as D3D11.CommonShaderStage<D3D11.VertexShader>).Set(shader.Shader as D3D11.VertexShader);
            break;
          case ShaderStage.PixelShader:
            (m_nativeShaderStage as D3D11.CommonShaderStage<D3D11.PixelShader>).Set(shader.Shader as D3D11.PixelShader);
            break;
          case ShaderStage.GeometryShader:
            (m_nativeShaderStage as D3D11.CommonShaderStage<D3D11.GeometryShader>).Set(shader.Shader as D3D11.GeometryShader);
            break;
          case ShaderStage.HullShader:
            (m_nativeShaderStage as D3D11.CommonShaderStage<D3D11.HullShader>).Set(shader.Shader as D3D11.HullShader);
            break;
          case ShaderStage.DomainShader:
            (m_nativeShaderStage as D3D11.CommonShaderStage<D3D11.DomainShader>).Set(shader.Shader as D3D11.DomainShader);
            break;
          case ShaderStage.ComputeShader:
            (m_nativeShaderStage as D3D11.CommonShaderStage<D3D11.ComputeShader>).Set(shader.Shader as D3D11.ComputeShader);
            break;
        }
      }
    }

    #endregion

    public void ClearState()
    {
      //Clear bound state
      for (int i = 0; i <= m_lastResourceSlotIndex; i++)
        RemoveBoundFlag(m_resources[i], m_stageBinding);

      Array.Clear(m_samplers, 0, m_samplers.Length);
      Array.Clear(m_resources, 0, m_resources.Length);
      Array.Clear(m_nativeConstantBuffers, 0, m_nativeConstantBuffers.Length);

      Array.Clear(m_tempSamplers, 0, m_tempSamplers.Length);
      Array.Clear(m_tempResources, 0, m_tempResources.Length);

      m_hashedShader = new HashedShader(null, 0, m_stageType);
      m_lastResourceSlotIndex = -1;
    }

    public void ClearBoundSlots(IShaderResource resource)
    {
      if (resource is null)
        return;

      int highestIndex = -1;

      for (int i = 0; i <= m_lastResourceSlotIndex; i++)
      {
        IShaderResource other = m_resources[i];
        if (Object.ReferenceEquals(other, resource))
        {
          highestIndex = i;
          m_resources[i] = null;

          //I think in general the resource will only be bound to one slot, but it may have been bound to multiple slots
          //that are not necessarily contigious
          m_nativeShaderStage.SetShaderResource(i, null);
        }
      }

      //Clear the bound flag for this stage
      RemoveBoundFlag(resource, m_stageBinding);

      //If the last resource was removed, make sure we keep the last index used up to date
      UpdateLastResourceIndex(highestIndex);
    }

    private void UpdateLastResourceIndex(int highestIndexInUpdate)
    {
      if (highestIndexInUpdate < m_lastResourceSlotIndex)
        return;

      //Start at the highest index we set when setting resources, and reverse iterate
      //over the set resources until a null resource is found. If so, set the last resource index
      //to that value and return.
      for (int i = highestIndexInUpdate; i >= 0; i--)
      {
        if (m_resources[i] is not null)
        {
          m_lastResourceSlotIndex = i;
          return;
        }
      }

      m_lastResourceSlotIndex = -1;
    }

    private static void RemoveBoundFlag(IShaderResource resource, ShaderStageBinding flag)
    {
      ID3D11ShaderResourceView srv = Direct3DHelper.GetD3DShaderResourceView_Interface(resource);
      if (srv is not null)
        srv.BoundStages &= ~flag;
    }

    private static void SetBoundFlag(IShaderResource resource, ShaderStageBinding flag)
    {
      ID3D11ShaderResourceView srv = Direct3DHelper.GetD3DShaderResourceView_Interface(resource);
      if (srv is not null)
        srv.BoundStages |= flag;
    }
  }
}
