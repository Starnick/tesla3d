﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Graphics;
using D3D11 = SharpDX.Direct3D11;

namespace Tesla.Direct3D11.Graphics
{
  public sealed class D3D11CommandListWrapper : ICommandList, ID3D11CommandList
  {
    private bool m_isDisposed;
    private String m_name;
    private D3D11.CommandList m_commandList;

    public bool IsDisposed
    {
      get
      {
        return m_isDisposed;
      }
    }

    public String Name
    {
      get
      {
        return m_name;
      }
      set
      {
        m_name = value;

        if (m_commandList is not null)
          m_commandList.DebugName = m_name;
      }
    }

    public D3D11.CommandList D3DCommandList
    {
      get
      {
        return m_commandList;
      }
    }

    internal D3D11CommandListWrapper(D3D11.CommandList commandList)
    {
      m_commandList = commandList;
    }

    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    private void Dispose(bool disposing)
    {
      if (!m_isDisposed)
      {

        if (disposing)
          m_commandList.Dispose();

        m_isDisposed = true;
      }
    }
  }
}
