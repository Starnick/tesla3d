﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using Tesla.Graphics;

#nullable enable

namespace Tesla.Direct3D11.Graphics
{
  /// <summary>
  /// Effect bytecode provider for D3D11 effects.
  /// </summary>
  public sealed class D3D11EffectByteCodeProvider : StandardEffectLibrary.BaseProvider
  {
    /// <summary>
    /// Constructs a new instance of the <see cref="D3D11EffectByteCodeProvider"/> class.
    /// </summary>
    public D3D11EffectByteCodeProvider() : base(String.Empty) { }

    /// <inheritdoc />
    protected override void Preload(Dictionary<String, byte[]> effectByteCodes)
    {
      //Key should always be a string (name of the file) and value should always be a byte[]
      foreach (DictionaryEntry kv in StandardEffects.ResourceManager.GetResourceSet(CultureInfo.InvariantCulture, true, false)!)
      {
        effectByteCodes.Add((string) kv.Key, (byte[]) kv.Value!);
      }
    }
  }
}
