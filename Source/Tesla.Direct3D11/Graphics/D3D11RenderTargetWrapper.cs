﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Graphics;
using D3D = SharpDX.Direct3D;
using D3D11 = SharpDX.Direct3D11;
using DXGI = SharpDX.DXGI;
using SDXMI = SharpDX.Mathematics.Interop;

namespace Tesla.Direct3D11.Graphics
{
  public sealed class D3D11RenderTargetWrapper : IRenderTarget, ID3D11RenderTargetView, ID3D11ShaderResourceView
  {
    private D3D11.Device m_device;
    private D3D11.RenderTargetView m_renderTargetView;
    private D3D11.ShaderResourceView m_shaderResourceView;
    private D3D11.Resource m_nativeTexture;
    private D3D11.Resource m_resolveTexture;
    private string m_name;
    private ShaderResourceType m_resourceType;
    private ShaderStageBinding m_boundStages;
    private int m_width;
    private int m_height;
    private int m_depth;
    private SurfaceFormat m_surfaceFormat;
    private int m_mipCount;
    private int m_arrayCount;
    private bool m_isArrayResource;
    private bool m_isCubeResource;
    private MSAADescription m_msaaDescription;
    private RenderTargetUsage m_targetUsage;
    private bool m_isDisposed;
    private bool m_ownsTexture;
    private D3D11RenderTargetWrapper m_parentTarget;
    private D3D11DepthStencilBufferWrapper m_parentDepthBuffer;
    private int m_subResourceIndex;

    private D3D11RenderTargetWrapper[] m_subTargets;

    /// <inheritdoc />
    public bool IsDisposed
    {
      get
      {
        return m_isDisposed;
      }
    }

    /// <inheritdoc />
    public ShaderResourceType ResourceType
    {
      get
      {
        return m_resourceType;
      }
    }

    /// <inheritdoc />
    public ShaderStageBinding BoundStages
    {
      get
      {
        return m_boundStages;
      }
      set
      {
        m_boundStages = value;
      }
    }

    /// <inheritdoc />
    public string Name
    {
      get
      {
        return m_name;
      }
      set
      {
        m_name = value;
      }
    }

    /// <inheritdoc />
    public string DebugName
    {
      get
      {
        return m_nativeTexture.DebugName;
      }
      set
      {
        if (m_ownsTexture && m_nativeTexture is not null)
          m_nativeTexture.DebugName = value;

        if (m_renderTargetView is not null)
          m_renderTargetView.DebugName = (string.IsNullOrEmpty(value)) ? value : value + "_RTV";

        if (m_shaderResourceView is not null)
          m_shaderResourceView.DebugName = (string.IsNullOrEmpty(value)) ? value : value + "_SRV";

        if (m_ownsTexture && m_resolveTexture is not null)
          m_resolveTexture.DebugName = (string.IsNullOrEmpty(value)) ? value : value + "_RESOLVE";
      }
    }

    /// <inheritdoc />
    public IDepthStencilBuffer DepthStencilBuffer
    {
      get
      {
        return GetDepthBuffer();
      }
    }

    /// <inheritdoc />
    public DepthFormat DepthStencilFormat
    {
      get
      {
        D3D11DepthStencilBufferWrapper depthBuffer = GetDepthBuffer();

        if (depthBuffer is null)
          return DepthFormat.None;

        return depthBuffer.DepthStencilFormat;
      }
    }

    /// <inheritdoc />
    public SurfaceFormat Format
    {
      get
      {
        return m_surfaceFormat;
      }
    }

    /// <inheritdoc />
    public int Width
    {
      get
      {
        return m_width;
      }
    }

    /// <inheritdoc />
    public int Height
    {
      get
      {
        return m_height;
      }
    }

    /// <inheritdoc />
    public int Depth
    {
      get
      {
        return m_depth;
      }
    }

    /// <inheritdoc />
    public int MipCount
    {
      get
      {
        return m_mipCount;
      }
    }

    /// <inheritdoc />
    public int ArrayCount
    {
      get
      {
        return m_arrayCount;
      }
    }

    /// <inheritdoc />
    public bool IsArrayResource
    {
      get
      {
        return m_isArrayResource;
      }
    }

    /// <inheritdoc />
    public bool IsCubeResource
    {
      get
      {
        return m_isCubeResource;
      }
    }
    /// <inheritdoc />
    public bool IsSubResource
    {
      get
      {
        return m_subResourceIndex != -1;
      }
    }

    /// <inheritdoc />
    public int SubResourceIndex
    {
      get
      {
        return m_subResourceIndex;
      }
    }

    /// <inheritdoc />
    public MSAADescription MultisampleDescription
    {
      get
      {
        return m_msaaDescription;
      }
    }

    /// <inheritdoc />
    public RenderTargetUsage TargetUsage
    {
      get
      {
        return m_targetUsage;
      }
    }

    public D3D11.Device D3DDevice
    {
      get
      {
        return m_device;
      }
    }

    public D3D11.RenderTargetView D3DRenderTargetView
    {
      get
      {
        return m_renderTargetView;
      }
    }

    public D3D11.ShaderResourceView D3DShaderResourceView
    {
      get
      {
        return m_shaderResourceView;
      }
    }

    public D3D11.Resource D3DTexture
    {
      get
      {
        return m_nativeTexture;
      }
    }

    public D3D11.Resource D3DResolveTexture
    {
      get
      {
        return m_resolveTexture;
      }
    }

    public D3D11RenderTargetWrapper Parent
    {
      get
      {
        return m_parentTarget;
      }
    }

    private bool ReallyWantResolve
    {
      get
      {
        return m_msaaDescription.Count > 1 && m_msaaDescription.ResolveShaderResource;
      }
    }

    public D3D11RenderTargetWrapper(D3D11.Device device, int width, int arrayCount, int mipCount, SurfaceFormat format, MSAADescription msaaDesc, DepthFormat depthFormat, bool readableDepth, bool optimizeDepthForSingleSurface, RenderTargetUsage targetUsage)
    {
      ArgumentNullException.ThrowIfNull(device, nameof(device));

      m_device = device;
      m_isDisposed = false;
      m_ownsTexture = true;
      m_msaaDescription = msaaDesc;
      m_surfaceFormat = format;
      m_targetUsage = targetUsage;
      m_isCubeResource = false;
      m_parentDepthBuffer = null;
      m_parentTarget = null;
      m_subResourceIndex = -1;
      m_mipCount = mipCount;

      Initialize1DRenderTarget(width, arrayCount);
      InitializeDepthBuffer(depthFormat, readableDepth, optimizeDepthForSingleSurface);

      Name = String.Empty;
    }

    public D3D11RenderTargetWrapper(D3D11.Device device, int width, int height, bool isCube, int arrayCount, int mipCount, SurfaceFormat format, MSAADescription msaaDesc, DepthFormat depthFormat, bool readableDepth, bool optimizeDepthForSingleSurface, RenderTargetUsage targetUsage)
    {
      ArgumentNullException.ThrowIfNull(device, nameof(device));

      m_device = device;
      m_isDisposed = false;
      m_ownsTexture = true;
      m_msaaDescription = msaaDesc;
      m_surfaceFormat = format;
      m_targetUsage = targetUsage;
      m_isCubeResource = isCube;
      m_parentDepthBuffer = null;
      m_parentTarget = null;
      m_subResourceIndex = -1;
      m_mipCount = mipCount;

      Initialize2DRenderTarget(width, height, arrayCount);
      InitializeDepthBuffer(depthFormat, readableDepth, optimizeDepthForSingleSurface);

      Name = String.Empty;
    }

    public D3D11RenderTargetWrapper(D3D11.Device device, int width, int height, int depth, int mipCount, SurfaceFormat format, MSAADescription msaaDesc, DepthFormat depthFormat, bool readableDepth, bool optimizeDepthForSingleSurface, RenderTargetUsage targetUsage)
    {
      ArgumentNullException.ThrowIfNull(device, nameof(device));

      m_device = device;
      m_isDisposed = false;
      m_ownsTexture = true;
      m_msaaDescription = msaaDesc;
      m_surfaceFormat = format;
      m_targetUsage = targetUsage;
      m_isCubeResource = false;
      m_parentDepthBuffer = null;
      m_parentTarget = null;
      m_subResourceIndex = -1;
      m_mipCount = mipCount;

      Initialize3DRenderTarget(width, height, depth);
      InitializeDepthBuffer(depthFormat, readableDepth, optimizeDepthForSingleSurface);

      Name = String.Empty;
    }

    public D3D11RenderTargetWrapper(SurfaceFormat surfaceFormat, int mipCount, D3D11DepthStencilBufferWrapper depthBuffer)
    {
      ArgumentNullException.ThrowIfNull(depthBuffer, nameof(depthBuffer));

      if (depthBuffer.Parent is not null)
        depthBuffer = depthBuffer.Parent;

      m_device = depthBuffer.D3DDevice;
      m_isDisposed = false;
      m_ownsTexture = true;
      m_msaaDescription = depthBuffer.MultisampleDescription;
      m_targetUsage = depthBuffer.TargetUsage;
      m_mipCount = mipCount;
      m_width = depthBuffer.Width;
      m_height = depthBuffer.Height;
      m_surfaceFormat = surfaceFormat;
      m_isArrayResource = depthBuffer.IsArrayResource;
      m_isCubeResource = depthBuffer.IsCubeResource;
      m_resourceType = depthBuffer.ResourceType;
      m_subResourceIndex = -1;
      m_parentDepthBuffer = depthBuffer;
      m_parentTarget = null;

      if (depthBuffer.ResourceType == ShaderResourceType.Texture3D)
      {
        m_arrayCount = 1;
        m_depth = depthBuffer.ArrayCount;
      }
      else
      {
        m_arrayCount = depthBuffer.ArrayCount;
        m_depth = 1;
      }

      depthBuffer.AddRef();

      switch (m_resourceType)
      {
        case ShaderResourceType.Texture3D:
          Initialize3DRenderTarget(m_width, m_height, m_depth);
          break;
        case ShaderResourceType.TextureCubeMSArray:
        case ShaderResourceType.TextureCubeMS:
        case ShaderResourceType.TextureCubeArray:
        case ShaderResourceType.TextureCube:
        case ShaderResourceType.Texture2DMSArray:
        case ShaderResourceType.Texture2DMS:
        case ShaderResourceType.Texture2DArray:
        case ShaderResourceType.Texture2D:
          Initialize2DRenderTarget(m_width, m_height, m_arrayCount);
          break;
        case ShaderResourceType.Texture1DArray:
        case ShaderResourceType.Texture1D:
          Initialize1DRenderTarget(m_width, m_arrayCount);
          break;
      }

      Name = String.Empty;
    }

    private D3D11RenderTargetWrapper(D3D11RenderTargetWrapper parentTarget, int arraySlice)
    {
      m_device = parentTarget.D3DDevice;
      m_isDisposed = false;
      m_ownsTexture = false;
      m_msaaDescription = parentTarget.m_msaaDescription;
      m_targetUsage = parentTarget.m_targetUsage;
      m_arrayCount = parentTarget.m_arrayCount;
      m_mipCount = parentTarget.m_mipCount;
      m_width = parentTarget.m_width;
      m_height = parentTarget.m_height;
      m_depth = parentTarget.m_depth;
      m_surfaceFormat = parentTarget.m_surfaceFormat;
      m_isArrayResource = parentTarget.m_isArrayResource;
      m_isCubeResource = parentTarget.m_isCubeResource;
      m_nativeTexture = parentTarget.m_nativeTexture;
      m_resolveTexture = parentTarget.m_resolveTexture;
      m_resourceType = parentTarget.m_resourceType;
      m_subResourceIndex = arraySlice;
      m_parentTarget = parentTarget;
      m_parentDepthBuffer = null;

      if (m_nativeTexture is D3D11.Texture1D)
      {
        Initialize1DRenderTargetViewsOnly(arraySlice);
      }
      else if (m_nativeTexture is D3D11.Texture2D)
      {
        Initialize2DRenderTargetViewsOnly(arraySlice);
      }
      else if (m_nativeTexture is D3D11.Texture3D)
      {
        Initialize3DRenderTargetViewsOnly(arraySlice);
      }

      Name = parentTarget.m_name + "[" + GetSubName() + "]";
      DebugName = Name;
    }

    public IRenderTarget GetSubRenderTarget(int arrayIndex)
    {
      //Either array or cube and has to not be sub
      if (!m_isArrayResource && m_isCubeResource && IsSubResource)
        return null;

      int totalSubResources = m_arrayCount;
      int actualArraySlice = arrayIndex;

      //Account for cube arrays, and if not an array but a cube map, we still have the 6 faces
      if (m_isCubeResource && m_isArrayResource)
      {
        actualArraySlice *= 6;
      }
      else if (m_isCubeResource)
      {
        totalSubResources *= 6;
      }

      if (m_subTargets is null)
        m_subTargets = new D3D11RenderTargetWrapper[totalSubResources];

      if (arrayIndex < 0 || arrayIndex >= m_subTargets.Length)
        return null;

      D3D11RenderTargetWrapper subTarget = m_subTargets[arrayIndex];
      if (subTarget is null)
      {
        subTarget = new D3D11RenderTargetWrapper(this, actualArraySlice);
        m_subTargets[arrayIndex] = subTarget;
      }

      return subTarget;
    }

    public void NotifyOnFirstBind()
    {
      if (IsSubResource)
        return;

      //For array/cube resources, we may have a depth buffer that has been optimized, where it uses one face that is
      //shared among everyone - but now we're binding the "parent" which may be the full array or cube resource, so we need
      //to "expand" the shared depth buffer. All resources that share this guy will come along, since we validate against
      //the size of the full resource anyways, any sub resources sharing this will eventually have individual views
      //created as necessary when/if they get bound.
      //
      //If you never bind the "full" array/cube resource, which probably will be common, you'll go along in life merrily, 
      //and consume less resource memory to boot,  this will only kick in if you want to do something fancy.
      if (m_parentDepthBuffer is not null && m_parentDepthBuffer.IsOptimizedForSingleSurface)
        m_parentDepthBuffer.MakeOptimizedToFull();
    }

    public void ResolveResource(D3D11.DeviceContext deviceContext)
    {
      if (m_resolveTexture is not null)
      {
        DXGI.Format resolveFormat = Direct3DHelper.ToD3DSurfaceFormat(m_surfaceFormat);
        if (IsSubResource)
        {
          deviceContext.ResolveSubresource(m_nativeTexture, m_subResourceIndex, m_resolveTexture, m_subResourceIndex, resolveFormat);
        }
        else
        {
          for (int i = 0; i < m_arrayCount; i++)
          {
            deviceContext.ResolveSubresource(m_nativeTexture, i, m_resolveTexture, i, resolveFormat);
          }
        }
      }

      //SRV will be for either the resolve texture or original texture, either way it needs mips generated
      //whether we have a resolve texture or not. 
      if (m_mipCount > 1)
        deviceContext.GenerateMips(m_shaderResourceView);
    }

    public void Clear(D3D11.DeviceContext deviceContext, ClearOptions options, Color color, float depth, int stencil)
    {
      if ((options & ClearOptions.Target) == ClearOptions.Target)
      {
        SDXMI.RawColor4 sdxColor;
        Direct3DHelper.ConvertColor(ref color, out sdxColor);
        deviceContext.ClearRenderTargetView(m_renderTargetView, sdxColor);
      }

      if ((options & ClearOptions.Depth) == ClearOptions.Depth || (options & ClearOptions.Stencil) == ClearOptions.Stencil)
      {
        D3D11DepthStencilBufferWrapper depthBuffer = GetDepthBuffer();
        if (depthBuffer is not null)
          depthBuffer.Clear(deviceContext, options, depth, stencil);
      }
    }

    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    #region 1D Render Target Initialization

    private void Initialize1DRenderTarget(int width, int arrayCount)
    {
      //Set fields
      m_arrayCount = arrayCount;
      m_width = width;
      m_height = 1;
      m_depth = 1;
      m_isArrayResource = arrayCount > 1;
      m_resourceType = (m_isArrayResource) ? ShaderResourceType.Texture1DArray : ShaderResourceType.Texture1D;

      //Setup texture resource

      D3D11.Texture1DDescription texDesc = new D3D11.Texture1DDescription();
      texDesc.Format = Direct3DHelper.ToD3DSurfaceFormat(m_surfaceFormat);
      texDesc.Width = width;
      texDesc.MipLevels = m_mipCount;
      texDesc.ArraySize = arrayCount;
      texDesc.CpuAccessFlags = D3D11.CpuAccessFlags.None;
      texDesc.OptionFlags = (m_mipCount > 1) ? D3D11.ResourceOptionFlags.GenerateMipMaps : D3D11.ResourceOptionFlags.None;
      texDesc.Usage = D3D11.ResourceUsage.Default;
      texDesc.BindFlags = D3D11.BindFlags.RenderTarget | D3D11.BindFlags.ShaderResource;

      m_nativeTexture = new D3D11.Texture1D(m_device, texDesc);

      //Can't multisample, so don't bother creating resolve texture

      Initialize1DRenderTargetViewsOnly(-1);
    }

    private void Initialize1DRenderTargetViewsOnly(int arraySlice)
    {
      //Setup render target view
      D3D11.RenderTargetViewDescription rtvDesc = new D3D11.RenderTargetViewDescription();
      rtvDesc.Format = Direct3DHelper.ToD3DSurfaceFormat(m_surfaceFormat);
      rtvDesc.Dimension = (m_isArrayResource) ? D3D11.RenderTargetViewDimension.Texture1DArray : D3D11.RenderTargetViewDimension.Texture1D;

      //For creating views of sub textures
      int numArraySlices = (arraySlice < 0) ? m_arrayCount : 1;
      arraySlice = (arraySlice < 0) ? 0 : arraySlice;

      if (m_isArrayResource)
      {
        rtvDesc.Texture1DArray.FirstArraySlice = arraySlice;
        rtvDesc.Texture1DArray.ArraySize = numArraySlices;
        rtvDesc.Texture1DArray.MipSlice = 0;
      }
      else
      {
        rtvDesc.Texture1D.MipSlice = 0;
      }

      m_renderTargetView = new D3D11.RenderTargetView(m_device, m_nativeTexture, rtvDesc);

      //Setup shader resource view

      D3D11.ShaderResourceViewDescription srvDesc = new D3D11.ShaderResourceViewDescription();
      srvDesc.Format = rtvDesc.Format;

      if (m_isArrayResource)
      {
        srvDesc.Dimension = D3D.ShaderResourceViewDimension.Texture1DArray;
        srvDesc.Texture1DArray.FirstArraySlice = arraySlice;
        srvDesc.Texture1DArray.ArraySize = numArraySlices;
        srvDesc.Texture1DArray.MipLevels = m_mipCount;
        srvDesc.Texture1DArray.MostDetailedMip = 0;
      }
      else
      {
        srvDesc.Dimension = D3D.ShaderResourceViewDimension.Texture1D;
        srvDesc.Texture1D.MipLevels = m_mipCount;
        srvDesc.Texture1D.MostDetailedMip = 0;
      }

      //Can't multisample this texture, so don't bother checking for resolve texture

      m_shaderResourceView = new D3D11.ShaderResourceView(m_device, m_nativeTexture, srvDesc);
    }

    #endregion

    #region 2D Render Target Initialization

    private void Initialize2DRenderTarget(int width, int height, int arrayCount)
    {
      //Set fields
      m_arrayCount = arrayCount;
      m_width = width;
      m_height = height;
      m_depth = 1;
      m_isArrayResource = arrayCount > 1;

      if (m_msaaDescription.IsMultisampled)
      {
        if (m_isCubeResource)
        {
          m_resourceType = (m_isArrayResource) ? ShaderResourceType.TextureCubeMSArray : ShaderResourceType.TextureCubeMS;
        }
        else
        {
          m_resourceType = (m_isArrayResource) ? ShaderResourceType.Texture2DMSArray : ShaderResourceType.Texture2DMS;
        }
      }
      else
      {
        if (m_isCubeResource)
        {
          m_resourceType = (m_isArrayResource) ? ShaderResourceType.TextureCubeArray : ShaderResourceType.TextureCube;
        }
        else
        {
          m_resourceType = (m_isArrayResource) ? ShaderResourceType.Texture2DArray : ShaderResourceType.Texture2D;
        }
      }

      //Setup texture resource

      D3D11.Texture2DDescription texDesc = new D3D11.Texture2DDescription();
      texDesc.Width = width;
      texDesc.Height = height;
      texDesc.MipLevels = (!m_msaaDescription.IsMultisampled) ? m_mipCount : 1;
      texDesc.ArraySize = (m_isCubeResource) ? arrayCount * 6 : arrayCount;
      texDesc.CpuAccessFlags = D3D11.CpuAccessFlags.None;
      texDesc.Usage = D3D11.ResourceUsage.Default;
      texDesc.SampleDescription = new DXGI.SampleDescription(m_msaaDescription.Count, m_msaaDescription.QualityLevel);
      texDesc.OptionFlags = (m_isCubeResource && !m_msaaDescription.IsMultisampled) ? D3D11.ResourceOptionFlags.TextureCube : D3D11.ResourceOptionFlags.None;
      texDesc.OptionFlags |= (m_mipCount > 1 && !ReallyWantResolve) ? D3D11.ResourceOptionFlags.GenerateMipMaps : D3D11.ResourceOptionFlags.None;
      texDesc.BindFlags = D3D11.BindFlags.RenderTarget | D3D11.BindFlags.ShaderResource;
      texDesc.Format = Direct3DHelper.ToD3DSurfaceFormat(m_surfaceFormat);

      m_nativeTexture = new D3D11.Texture2D(m_device, texDesc);

      //Check if want to create a non-msaa resolve texture
      if (ReallyWantResolve)
      {
        //If need to resolve, only have the resolve texture have generate mip maps
        if (m_mipCount > 1)
          texDesc.OptionFlags |= SharpDX.Direct3D11.ResourceOptionFlags.GenerateMipMaps;

        texDesc.SampleDescription = new DXGI.SampleDescription(1, 0);
        texDesc.MipLevels = m_mipCount;
        texDesc.OptionFlags = (m_isCubeResource) ? D3D11.ResourceOptionFlags.TextureCube : D3D11.ResourceOptionFlags.None;
        m_resolveTexture = new D3D11.Texture2D(m_device, texDesc);
      }

      Initialize2DRenderTargetViewsOnly(-1);
    }

    private void Initialize2DRenderTargetViewsOnly(int arraySlice)
    {
      //Setup render target view
      D3D11.RenderTargetViewDescription rtvDesc = new D3D11.RenderTargetViewDescription();
      rtvDesc.Format = Direct3DHelper.ToD3DSurfaceFormat(m_surfaceFormat);

      if (m_msaaDescription.IsMultisampled)
      {
        rtvDesc.Dimension = (m_isArrayResource || m_isCubeResource) ? D3D11.RenderTargetViewDimension.Texture2DMultisampledArray : D3D11.RenderTargetViewDimension.Texture2DMultisampled;
      }
      else
      {
        rtvDesc.Dimension = (m_isArrayResource || m_isCubeResource) ? D3D11.RenderTargetViewDimension.Texture2DArray : D3D11.RenderTargetViewDimension.Texture2D;
      }

      //For creating views of sub textures
      int numArraySlices = (arraySlice < 0) ? (m_isCubeResource) ? m_arrayCount * 6 : m_arrayCount : (m_isCubeResource && m_isArrayResource) ? 6 : 1;
      int cubeCount = (arraySlice < 0) ? m_arrayCount : 1; //Only valid with cube resource array
      arraySlice = (arraySlice < 0) ? 0 : arraySlice;

      if (m_isArrayResource || m_isCubeResource)
      {
        if (m_msaaDescription.IsMultisampled)
        {
          rtvDesc.Dimension = D3D11.RenderTargetViewDimension.Texture2DMultisampledArray;
          rtvDesc.Texture2DMSArray.FirstArraySlice = arraySlice;
          rtvDesc.Texture2DMSArray.ArraySize = numArraySlices;
        }
        else
        {
          rtvDesc.Dimension = D3D11.RenderTargetViewDimension.Texture2DArray;
          rtvDesc.Texture2DArray.FirstArraySlice = arraySlice;
          rtvDesc.Texture2DArray.ArraySize = numArraySlices;
          rtvDesc.Texture2DArray.MipSlice = 0;
        }
      }
      else
      {
        if (m_msaaDescription.IsMultisampled)
        {
          rtvDesc.Dimension = D3D11.RenderTargetViewDimension.Texture2DMultisampled;
        }
        else
        {
          rtvDesc.Dimension = D3D11.RenderTargetViewDimension.Texture2D;
          rtvDesc.Texture2D.MipSlice = 0;
        }
      }

      m_renderTargetView = new D3D11.RenderTargetView(m_device, m_nativeTexture, rtvDesc);

      //Setup shader resource view
      D3D11.ShaderResourceViewDescription srvDesc = new D3D11.ShaderResourceViewDescription();
      srvDesc.Format = rtvDesc.Format;

      if (m_isArrayResource)
      {
        if (m_msaaDescription.IsMultisampled)
        {
          srvDesc.Dimension = D3D.ShaderResourceViewDimension.Texture2DMultisampledArray;
          srvDesc.Texture2DMSArray.FirstArraySlice = arraySlice;
          srvDesc.Texture2DMSArray.ArraySize = numArraySlices;
        }
        else
        {
          if (m_isCubeResource)
          {
            srvDesc.Dimension = D3D.ShaderResourceViewDimension.TextureCubeArray;
            srvDesc.TextureCubeArray.CubeCount = cubeCount;
            srvDesc.TextureCubeArray.First2DArrayFace = arraySlice;
            srvDesc.TextureCubeArray.MipLevels = m_mipCount;
            srvDesc.TextureCubeArray.MostDetailedMip = 0;
          }
          else
          {
            srvDesc.Dimension = D3D.ShaderResourceViewDimension.Texture2DArray;
            srvDesc.Texture2DArray.FirstArraySlice = arraySlice;
            srvDesc.Texture2DArray.ArraySize = numArraySlices;
            srvDesc.Texture2DArray.MipLevels = m_mipCount;
            srvDesc.Texture2DArray.MostDetailedMip = 0;
          }
        }
      }
      else
      {
        if (m_isCubeResource)
        {
          if (m_msaaDescription.IsMultisampled)
          {
            srvDesc.Dimension = D3D.ShaderResourceViewDimension.Texture2DMultisampledArray;
            srvDesc.Texture2DMSArray.FirstArraySlice = arraySlice;
            srvDesc.Texture2DMSArray.ArraySize = numArraySlices;
          }
          else
          {
            srvDesc.Dimension = D3D.ShaderResourceViewDimension.TextureCube;
            srvDesc.TextureCube.MipLevels = m_mipCount;
            srvDesc.TextureCube.MostDetailedMip = 0;
          }
        }
        else
        {
          if (m_msaaDescription.IsMultisampled)
          {
            srvDesc.Dimension = D3D.ShaderResourceViewDimension.Texture2DMultisampled;
          }
          else
          {
            srvDesc.Dimension = D3D.ShaderResourceViewDimension.Texture2D;
            srvDesc.Texture2D.MipLevels = m_mipCount;
            srvDesc.Texture2D.MostDetailedMip = 0;
          }
        }
      }

      if (m_resolveTexture is not null && ReallyWantResolve)
        m_shaderResourceView = new D3D11.ShaderResourceView(m_device, m_resolveTexture, srvDesc);
      else
        m_shaderResourceView = new D3D11.ShaderResourceView(m_device, m_nativeTexture, srvDesc);
    }

    #endregion

    #region 3D Render Target Initialization

    private void Initialize3DRenderTarget(int width, int height, int depth)
    {
      //Set fields
      m_width = width;
      m_height = height;
      m_depth = depth;
      m_resourceType = ShaderResourceType.Texture3D;

      //Setup texture resource

      D3D11.Texture3DDescription texDesc = new D3D11.Texture3DDescription();
      texDesc.Width = width;
      texDesc.Height = height;
      texDesc.Depth = depth;
      texDesc.MipLevels = m_mipCount;
      texDesc.CpuAccessFlags = D3D11.CpuAccessFlags.None;
      texDesc.OptionFlags = (m_mipCount > 1) ? D3D11.ResourceOptionFlags.GenerateMipMaps : D3D11.ResourceOptionFlags.None;
      texDesc.Usage = D3D11.ResourceUsage.Default;
      texDesc.BindFlags = D3D11.BindFlags.RenderTarget | D3D11.BindFlags.ShaderResource;
      texDesc.Format = Direct3DHelper.ToD3DSurfaceFormat(m_surfaceFormat);

      m_nativeTexture = new D3D11.Texture3D(m_device, texDesc);

      //Cant multisample, so don't bother to create resolve texture

      Initialize3DRenderTargetViewsOnly(-1);
    }

    private void Initialize3DRenderTargetViewsOnly(int depthSlice)
    {
      //Setup render target view
      D3D11.RenderTargetViewDescription rtvDesc = new D3D11.RenderTargetViewDescription();
      rtvDesc.Format = Direct3DHelper.ToD3DSurfaceFormat(m_surfaceFormat);
      rtvDesc.Dimension = D3D11.RenderTargetViewDimension.Texture3D;

      //For creating views of sub textures
      int numDepthSlices = (depthSlice < 0) ? m_depth : 1;
      depthSlice = (depthSlice < 0) ? 0 : depthSlice;

      rtvDesc.Texture3D.DepthSliceCount = numDepthSlices;
      rtvDesc.Texture3D.FirstDepthSlice = depthSlice;
      rtvDesc.Texture3D.MipSlice = 0;

      m_renderTargetView = new D3D11.RenderTargetView(m_device, m_nativeTexture, rtvDesc);

      //Setup shader resource view
      D3D11.ShaderResourceViewDescription srvDesc = new D3D11.ShaderResourceViewDescription();
      srvDesc.Format = rtvDesc.Format;
      srvDesc.Dimension = D3D.ShaderResourceViewDimension.Texture3D;
      srvDesc.Texture3D.MipLevels = m_mipCount;
      srvDesc.Texture3D.MostDetailedMip = 0;

      //Can't multisample this texture, so don't bother checking for resolve texture

      m_shaderResourceView = new D3D11.ShaderResourceView(m_device, m_nativeTexture, srvDesc);
    }

    #endregion

    private void InitializeDepthBuffer(DepthFormat depthFormat, bool createShaderView, bool optimizeForSingleSurface)
    {
      if (depthFormat == DepthFormat.None)
        return;

      switch (m_resourceType)
      {
        case ShaderResourceType.Texture1D:
        case ShaderResourceType.Texture1DArray:
          m_parentDepthBuffer = new D3D11DepthStencilBufferWrapper(m_device, m_width, depthFormat, m_arrayCount, createShaderView, optimizeForSingleSurface, m_msaaDescription, m_targetUsage);
          m_parentDepthBuffer.AddRef();
          break;
        case ShaderResourceType.Texture2D:
        case ShaderResourceType.Texture2DArray:
        case ShaderResourceType.Texture2DMS:
        case ShaderResourceType.Texture2DMSArray:
        case ShaderResourceType.TextureCube:
        case ShaderResourceType.TextureCubeMS:
        case ShaderResourceType.TextureCubeArray:
        case ShaderResourceType.TextureCubeMSArray:
          m_parentDepthBuffer = new D3D11DepthStencilBufferWrapper(m_device, m_width, m_height, m_isCubeResource, depthFormat, m_arrayCount, createShaderView, optimizeForSingleSurface, m_msaaDescription, m_targetUsage);
          m_parentDepthBuffer.AddRef();
          break;
        case ShaderResourceType.Texture3D:
          m_parentDepthBuffer = new D3D11DepthStencilBufferWrapper(m_device, m_width, m_height, m_depth, depthFormat, createShaderView, optimizeForSingleSurface, m_msaaDescription, m_targetUsage);
          m_parentDepthBuffer.AddRef();
          break;
      }
    }

    private D3D11DepthStencilBufferWrapper GetDepthBuffer()
    {
      if (IsSubResource)
      {
        D3D11DepthStencilBufferWrapper parentDepthBuffer = m_parentTarget.m_parentDepthBuffer;

        if (parentDepthBuffer is null)
          return null;

        if (parentDepthBuffer.IsOptimizedForSingleSurface)
          return parentDepthBuffer;

        return parentDepthBuffer.GetSubBuffer(m_subResourceIndex);
      }
      else
      {
        return m_parentDepthBuffer;
      }
    }

    private void Dispose(bool disposing)
    {
      if (!m_isDisposed)
      {
        if (disposing)
        {
          if (m_renderTargetView is not null)
          {
            m_renderTargetView.Dispose();
            m_renderTargetView = null;
          }

          if (m_shaderResourceView is not null)
          {
            m_shaderResourceView.Dispose();
            m_shaderResourceView = null;
          }

          if (m_ownsTexture && m_resolveTexture is not null)
          {
            m_resolveTexture.Dispose();
            m_resolveTexture = null;
          }

          if (m_ownsTexture && m_nativeTexture is not null)
          {
            m_nativeTexture.Dispose();
            m_nativeTexture = null;
          }

          if (m_parentDepthBuffer is not null)
          {
            m_parentDepthBuffer.Release();
          }
        }

        m_isDisposed = true;
      }
    }

    private string GetSubName()
    {
      if (m_isCubeResource)
      {
        if (m_isArrayResource)
        {
          //Cube arrays in multiples of 6, so all we can do is get the "array index" of the cube
          int cubeIndex = m_subResourceIndex / 6;
          return cubeIndex.ToString();
        }
        else
        {
          return "Face: " + ((CubeMapFace) m_subResourceIndex).ToString();
        }
      }
      else
      {
        return m_subResourceIndex.ToString();
      }
    }
  }
}
