﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesla.Graphics;
using System.IO;
using System.IO.Compression;

namespace Tesla.Direct3D11.Graphics
{
  /// <summary>
  /// Represents compiled HLSL shader fragments, shader group (somewhat analogous to techniques/passes, but like a flattened list) information, and reflected meta data about the shaders
  /// for our own implementation of something like Effects11.
  /// </summary>
  public sealed class EffectData
  {
    public const byte RuntimeID = 1;
    public const ushort Version = 3;

    private static readonly ConstantBuffer[] EmptyConstantBufferArray = new ConstantBuffer[0];
    private static readonly ValueVariable[] EmptyValueVariableArray = new ValueVariable[0];
    private static readonly ValueVariableMember[] EmptyValueVariableMemberArray = new ValueVariableMember[0];
    private static readonly ResourceVariable[] EmptyResourceVariableArray = new ResourceVariable[0];
    private static readonly ShaderGroup[] EmptyShaderGroupArray = new ShaderGroup[0];
    private static readonly Shader[] EmptyShaderArray = new Shader[0];
    private static readonly BoundResource[] EmptyBoundResourceArray = new BoundResource[0];
    private static readonly SignatureParameter[] EmptySignatureParameterArray = new SignatureParameter[0];
    private static readonly int[] EmptyIntArray = new int[0];
    private static readonly byte[] EmptyByteArray = new byte[0];
    private static readonly Signature EmptySignature = new Signature { ByteCode = EmptyByteArray, HashCode = 0, Parameters = EmptySignatureParameterArray };

    public ConstantBuffer[] ConstantBuffers;
    public ResourceVariable[] ResourceVariables;
    public ShaderGroup[] ShaderGroups;
    public Shader[] Shaders;

    public static EffectData Read(byte[] effectByteCode)
    {
      if (effectByteCode is null || effectByteCode.Length < TEFXHeader.SizeInBytes)
        return null;

      using (MemoryStream stream = new MemoryStream(effectByteCode))
      {
        return Read(stream);
      }
    }

    public static EffectData Read(Stream effectByteCode)
    {
      if (effectByteCode is null || !effectByteCode.CanRead)
        return null;

      TEFXHeader? hasHeader = TEFXHeader.ReadHeader(effectByteCode);

      if (hasHeader is null)
        return null;

      TEFXHeader header = hasHeader.Value;

      if (header.RuntimeID != RuntimeID || header.DataSizeInBytes == 0)
        return null;

      EffectData effectData = null;
      using (BinaryReader input = new BinaryReader(effectByteCode, Encoding.Default, true))
      {
        effectData = new EffectData();
        if (header.CompressionMode == DataCompressionMode.Gzip)
        {
          using (GZipStream decompStream = new GZipStream(new MemoryStream(input.ReadBytes((int) header.DataSizeInBytes)), CompressionMode.Decompress))
          {
            using (MemoryStream byteDataStream = new MemoryStream())
            {
              decompStream.CopyTo(byteDataStream);
              byteDataStream.Position = 0;

              using (BinaryReader decompressedInput = new BinaryReader(byteDataStream))
              {
                effectData.Read(decompressedInput, header.Version);
              }
            }
          }
        }
        else
        {
          effectData.Read(input, header.Version);
        }
      }

      return effectData;
    }

    public static byte[] Write(EffectData effectData, DataCompressionMode compressionMode = DataCompressionMode.None)
    {
      if (effectData is null)
        return null;

      using (MemoryStream memStream = new MemoryStream())
      {
        Write(effectData, memStream);
        return memStream.ToArray();
      }
    }

    public static bool Write(EffectData effectData, Stream output, DataCompressionMode compressionMode = DataCompressionMode.None)
    {
      if (effectData is null || output is null || !output.CanWrite)
        return false;

      using (MemoryStream dataStream = new MemoryStream())
      {
        using (BinaryWriter writer = new BinaryWriter(dataStream, Encoding.Default, true))
        {
          effectData.Write(writer);
        }

        //Write header
        TEFXHeader header = new TEFXHeader(RuntimeID, Version, compressionMode, (uint) dataStream.Length);
        TEFXHeader.WriteHeader(header, output);

        //Write data
        if (compressionMode == DataCompressionMode.Gzip)
        {
          using (MemoryStream compressedData = new MemoryStream())
          {
            using (GZipStream compressionStream = new GZipStream(compressedData, CompressionMode.Compress, true))
            {
              dataStream.WriteTo(compressionStream);
            }

            compressedData.WriteTo(output);
          }
        }
        else
        {
          dataStream.WriteTo(output);
        }
      }

      return true;
    }

    public void Dump(TextWriter output)
    {
      output.WriteLine("TEFX Text Dump");
      output.WriteLine(String.Format("{0}: {1}", "RuntimeID", RuntimeID.ToString()));
      output.WriteLine(String.Format("{0}: {1}", "Version", Version.ToString()));
      output.WriteLine("\n===================================================================");
      output.WriteLine("Variable Section");
      output.WriteLine("\n");

      String nextPrefix = "\t";

      int constantBufferCount = (ConstantBuffers is null) ? 0 : ConstantBuffers.Length;
      output.WriteLine(String.Format("{0}: {1}", "# Constant Buffers", constantBufferCount.ToString()));
      output.WriteLine();
      for (int i = 0; i < constantBufferCount; i++)
      {
        ConstantBuffers[i].Dump(output, nextPrefix);
        output.WriteLine();
      }

      output.WriteLine("\n");

      int resourceVariableCount = (ResourceVariables is null) ? 0 : ResourceVariables.Length;
      output.WriteLine(String.Format("{0}: {1}", "# Resource Variables", resourceVariableCount.ToString()));
      output.WriteLine();
      for (int i = 0; i < resourceVariableCount; i++)
      {
        ResourceVariables[i].Dump(output, nextPrefix);
        output.WriteLine();
      }

      output.WriteLine("\n===================================================================");
      output.WriteLine("Shader Section");
      output.WriteLine("\n");

      int shaderCount = (Shaders is null) ? 0 : Shaders.Length;
      output.WriteLine(String.Format("{0}: {1}", "# Shaders", shaderCount.ToString()));
      output.WriteLine();
      for (int i = 0; i < shaderCount; i++)
      {
        Shaders[i].Dump(output, nextPrefix);
        output.WriteLine();
      }

      output.WriteLine("\n===================================================================");
      output.WriteLine("ShaderGroups Section");
      output.WriteLine("\n");

      int shaderGroupcount = (ShaderGroups is null) ? 0 : ShaderGroups.Length;
      output.WriteLine(String.Format("{0}: {1}", "# ShaderGroups", shaderGroupcount.ToString()));
      output.WriteLine();
      for (int i = 0; i < shaderGroupcount; i++)
      {
        ShaderGroups[i].Dump(output, nextPrefix);
        output.WriteLine();
      }

      output.Flush();
    }

    private void Read(BinaryReader input, int versionNum)
    {
      int cbCount = input.ReadInt32();
      ConstantBuffers = (cbCount > 0) ? new ConstantBuffer[cbCount] : EmptyConstantBufferArray;
      for (int i = 0; i < cbCount; i++)
      {
        ConstantBuffers[i] = ConstantBuffer.Read(input, versionNum);
      }

      int resourceVariableCount = input.ReadInt32();
      ResourceVariables = (resourceVariableCount > 0) ? new ResourceVariable[resourceVariableCount] : EmptyResourceVariableArray;
      for (int i = 0; i < resourceVariableCount; i++)
      {
        ResourceVariables[i] = ResourceVariable.Read(input, versionNum);
      }

      int shaderCount = input.ReadInt32();
      Shaders = (shaderCount > 0) ? new Shader[shaderCount] : EmptyShaderArray;
      for (int i = 0; i < shaderCount; i++)
      {
        Shaders[i] = Shader.Read(input, versionNum);
      }

      int techniqueCount = input.ReadInt32();
      ShaderGroups = (techniqueCount > 0) ? new ShaderGroup[techniqueCount] : EmptyShaderGroupArray;
      for (int i = 0; i < techniqueCount; i++)
      {
        ShaderGroups[i] = ShaderGroup.Read(input, versionNum);
      }
    }

    private void Write(BinaryWriter output)
    {
      if (ConstantBuffers is null)
      {
        output.Write(0);
      }
      else
      {
        output.Write(ConstantBuffers.Length);
        for (int i = 0; i < ConstantBuffers.Length; i++)
        {
          ConstantBuffer.Write(ConstantBuffers[i], output);
        }
      }

      if (ResourceVariables is null)
      {
        output.Write(0);
      }
      else
      {
        output.Write(ResourceVariables.Length);
        for (int i = 0; i < ResourceVariables.Length; i++)
        {
          ResourceVariable.Write(ResourceVariables[i], output);
        }
      }

      if (Shaders is null)
      {
        output.Write(0);
      }
      else
      {
        output.Write(Shaders.Length);
        for (int i = 0; i < Shaders.Length; i++)
        {
          Shader.Write(Shaders[i], output);
        }
      }

      if (ShaderGroups is null)
      {
        output.Write(0);
      }
      else
      {
        output.Write(ShaderGroups.Length);
        for (int i = 0; i < ShaderGroups.Length; i++)
        {
          ShaderGroup.Write(ShaderGroups[i], output);
        }
      }
    }

    #region Nested Classes

    public sealed class ShaderGroup
    {
      public String Name;
      public int[] ShaderIndices;

      public static ShaderGroup Read(BinaryReader input, int versionNum)
      {
        ShaderGroup shaderGroup = new ShaderGroup();

        shaderGroup.Name = input.ReadString();

        int shaderIndicesCount = input.ReadInt32();
        shaderGroup.ShaderIndices = (shaderIndicesCount > 0) ? new int[shaderIndicesCount] : EmptyIntArray;

        for (int i = 0; i < shaderIndicesCount; i++)
        {
          shaderGroup.ShaderIndices[i] = input.ReadInt32();
        }

        return shaderGroup;
      }

      public static void Write(ShaderGroup shaderGroup, BinaryWriter output)
      {
        output.Write(shaderGroup.Name ?? "ShaderGroup");

        if (shaderGroup.ShaderIndices is null)
        {
          output.Write(0);
        }
        else
        {
          output.Write(shaderGroup.ShaderIndices.Length);
          for (int i = 0; i < shaderGroup.ShaderIndices.Length; i++)
          {
            output.Write(shaderGroup.ShaderIndices[i]);
          }
        }
      }

      public void Dump(TextWriter output, String linePrefix)
      {
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "Name", Name));

        String nextPrefix = linePrefix + "\t";

        int shaderCount = (ShaderIndices is null) ? 0 : ShaderIndices.Length;
        output.WriteLine();
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "# Shaders", shaderCount.ToString()));
        output.WriteLine();
        for (int i = 0; i < shaderCount; i++)
        {
          output.WriteLine(String.Format("{0}{1}: {2}", nextPrefix, "Shader Index", ShaderIndices[i].ToString()));
        }
      }
    }

    public sealed class ConstantBuffer
    {
      public String Name;
      public int SizeInBytes;
      public D3DShaderVariableFlags Flags;
      public D3DConstantBufferType BufferType;
      public ValueVariable[] Variables;

      public static ConstantBuffer Read(BinaryReader input, int versionNum)
      {
        ConstantBuffer cb = new ConstantBuffer();

        cb.Name = input.ReadString();
        cb.SizeInBytes = input.ReadInt32();
        cb.Flags = (D3DShaderVariableFlags) input.ReadInt32();
        cb.BufferType = (D3DConstantBufferType) input.ReadInt32();

        int variableCount = input.ReadInt32();
        cb.Variables = (variableCount > 0) ? new ValueVariable[variableCount] : EmptyValueVariableArray;
        for (int i = 0; i < variableCount; i++)
        {
          cb.Variables[i] = ValueVariable.Read(input, versionNum);
        }

        return cb;
      }

      public static void Write(ConstantBuffer buffer, BinaryWriter output)
      {
        output.Write(buffer.Name);
        output.Write(buffer.SizeInBytes);
        output.Write((int) buffer.Flags);
        output.Write((int) buffer.BufferType);

        if (buffer.Variables is null)
        {
          output.Write(0);
        }
        else
        {
          output.Write(buffer.Variables.Length);
          for (int i = 0; i < buffer.Variables.Length; i++)
          {
            ValueVariable.Write(buffer.Variables[i], output);
          }
        }
      }

      public void Dump(TextWriter output, String linePrefix)
      {
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "Name", Name));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "SizeInBytes", SizeInBytes.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "Flags", Flags.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "BufferType", BufferType.ToString()));

        String nextPrefix = linePrefix + "\t";

        int variableCount = (Variables is null) ? 0 : Variables.Length;
        output.WriteLine();
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "# Variables", variableCount.ToString()));
        output.WriteLine();
        for (int i = 0; i < variableCount; i++)
        {
          Variables[i].Dump(output, nextPrefix);
          output.WriteLine();
        }
      }
    }

    public sealed class ResourceVariable
    {
      public String Name;
      public D3DShaderInputType ResourceType;
      public D3DResourceDimension ResourceDimension;
      public D3DResourceReturnType ReturnType;
      public D3DShaderInputFlags InputFlags;
      public int SampleCount;
      public int ElementCount;

      //Serves as "default value" like for value variables. This is why we do not have a separate category
      //in the effect data to reduce instances. When we create a sampler state at runtime we'll do appropiate caching there.
      public SamplerStateData? SamplerData;

      // If a structured buffer, the layout representing each element. Null if not a structured buffer
      public ValueVariable StructuredBufferLayout;

      public bool IsSampler
      {
        get
        {
          return ResourceType == D3DShaderInputType.Sampler;
        }
      }

      public bool IsStructuredBuffer
      {
        get
        {
          return ResourceType == D3DShaderInputType.Structured || ResourceType == D3DShaderInputType.RWStructured ||
            ResourceType == D3DShaderInputType.AppendStructured || ResourceType == D3DShaderInputType.ConsumeStructured ||
            ResourceType == D3DShaderInputType.RWStructuredWithCounter;
        }
      }

      public static ResourceVariable Read(BinaryReader input, int versionNum)
      {
        ResourceVariable variable = new ResourceVariable();

        variable.Name = input.ReadString();
        variable.ResourceType = (D3DShaderInputType) input.ReadInt32();
        variable.ResourceDimension = (D3DResourceDimension) input.ReadInt32();
        variable.ReturnType = (D3DResourceReturnType) input.ReadInt32();
        variable.InputFlags = (D3DShaderInputFlags) input.ReadInt32();
        variable.SampleCount = input.ReadInt32();
        variable.ElementCount = input.ReadInt32();
        variable.SamplerData = null;

        //Starting with Version 2, writing out default sampler so read it in, otherwise it wont be in the input stream
        if (versionNum >= 2 && variable.IsSampler)
        {
          //Check if has sampler data
          if (input.ReadBoolean())
            variable.SamplerData = SamplerStateData.Read(input, versionNum);
        }
        //Starting with Version 3, supporting structured buffers which have an element layout
        else if (versionNum >= 3 && variable.IsStructuredBuffer)
        {
          variable.StructuredBufferLayout = ValueVariable.Read(input, versionNum);
        }

        return variable;
      }

      public static void Write(ResourceVariable variable, BinaryWriter output)
      {
        output.Write(variable.Name);
        output.Write((int) variable.ResourceType);
        output.Write((int) variable.ResourceDimension);
        output.Write((int) variable.ReturnType);
        output.Write((int) variable.InputFlags);
        output.Write(variable.SampleCount);
        output.Write(variable.ElementCount);

        if (variable.IsSampler)
        {
          bool hasData = variable.SamplerData.HasValue;
          output.Write(hasData);

          if (hasData)
            SamplerStateData.Write(variable.SamplerData.Value, output);
        }
        else if (variable.IsStructuredBuffer)
        {
          ValueVariable.Write(variable.StructuredBufferLayout, output);
        }
      }

      public void Dump(TextWriter output, String linePrefix)
      {
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "Name", Name));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "ResourceType", ResourceType.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "ResourceDimension", ResourceDimension.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "ReturnType", ReturnType.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "InputFlags", InputFlags.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "SampleCount", SampleCount.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "ElementCount", ElementCount.ToString()));

        String nextPrefix = linePrefix + "\t";

        if (IsSampler && SamplerData.HasValue)
          SamplerData.Value.Dump(output, nextPrefix);

        if (IsStructuredBuffer)
          StructuredBufferLayout.Dump(output, nextPrefix);
      }
    }

    public sealed class ValueVariableMember
    {
      public String Name;
      public D3DShaderVariableClass VariableClass;
      public D3DShaderVariableType VariableType;
      public int ColumnCount;
      public int RowCount;
      public int ElementCount;
      public int OffsetFromParentStructure;
      public int SizeInBytes;
      public int AlignedElementSizeInBytes;
      public ValueVariableMember[] Members;

      public static ValueVariableMember Read(BinaryReader input, int versionNum)
      {
        ValueVariableMember variable = new ValueVariableMember();

        variable.Name = input.ReadString();
        variable.VariableClass = (D3DShaderVariableClass) input.ReadInt32();
        variable.VariableType = (D3DShaderVariableType) input.ReadInt32();
        variable.ColumnCount = input.ReadInt32();
        variable.RowCount = input.ReadInt32();
        variable.ElementCount = input.ReadInt32();
        variable.OffsetFromParentStructure = input.ReadInt32();
        variable.SizeInBytes = input.ReadInt32();
        variable.AlignedElementSizeInBytes = input.ReadInt32();

        int memberCount = input.ReadInt32();
        variable.Members = (memberCount > 0) ? new ValueVariableMember[memberCount] : EmptyValueVariableMemberArray;
        for (int i = 0; i < memberCount; i++)
        {
          variable.Members[i] = ValueVariableMember.Read(input, versionNum);
        }

        return variable;
      }

      public static void Write(ValueVariableMember variable, BinaryWriter output)
      {
        output.Write(variable.Name);
        output.Write((int) variable.VariableClass);
        output.Write((int) variable.VariableType);
        output.Write(variable.ColumnCount);
        output.Write(variable.RowCount);
        output.Write(variable.ElementCount);
        output.Write(variable.OffsetFromParentStructure);
        output.Write(variable.SizeInBytes);
        output.Write(variable.AlignedElementSizeInBytes);

        if (variable.Members is null)
        {
          output.Write(0);
        }
        else
        {
          output.Write(variable.Members.Length);
          for (int i = 0; i < variable.Members.Length; i++)
          {
            ValueVariableMember.Write(variable.Members[i], output);
          }
        }
      }

      public void Dump(TextWriter output, String linePrefix)
      {
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "Name", Name));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "VariableClass", VariableClass.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "VariableType", VariableType.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "ColumnCount", ColumnCount.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "RowCount", RowCount.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "ElementCount", ElementCount.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "OffsetFromParentStructure", OffsetFromParentStructure.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "SizeInBytes", SizeInBytes.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "AlignedElementSizeInBytes", AlignedElementSizeInBytes.ToString()));

        String nextPrefix = linePrefix + "\t";

        int memberCount = (Members is null) ? 0 : Members.Length;
        output.WriteLine();
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "# Members", memberCount.ToString()));
        output.WriteLine();
        for (int i = 0; i < memberCount; i++)
        {
          Members[i].Dump(output, nextPrefix);
          output.WriteLine();
        }
      }
    }

    public sealed class ValueVariable
    {
      public String Name;
      public int SizeInBytes;
      public int AlignedElementSizeInBytes;
      public int StartOffset;
      public D3DShaderVariableClass VariableClass;
      public D3DShaderVariableType VariableType;
      public int ColumnCount;
      public int RowCount;
      public D3DShaderVariableFlags Flags;
      public int StartSampler;
      public int SamplerSize;
      public int StartTexture;
      public int TextureSize;
      public byte[] DefaultValue;
      public int ElementCount;

      public ValueVariableMember[] Members;

      public static ValueVariable Read(BinaryReader input, int versionNum)
      {
        ValueVariable variable = new ValueVariable();

        variable.Name = input.ReadString();
        variable.SizeInBytes = input.ReadInt32();
        variable.AlignedElementSizeInBytes = input.ReadInt32();
        variable.StartOffset = input.ReadInt32();
        variable.VariableClass = (D3DShaderVariableClass) input.ReadInt32();
        variable.VariableType = (D3DShaderVariableType) input.ReadInt32();
        variable.ColumnCount = input.ReadInt32();
        variable.RowCount = input.ReadInt32();
        variable.Flags = (D3DShaderVariableFlags) input.ReadInt32();
        variable.StartSampler = input.ReadInt32();
        variable.SamplerSize = input.ReadInt32();
        variable.StartTexture = input.ReadInt32();
        variable.TextureSize = input.ReadInt32();

        int defaultValueBytes = input.ReadInt32();
        variable.DefaultValue = (defaultValueBytes > 0) ? input.ReadBytes(defaultValueBytes) : Array.Empty<byte>();
        variable.ElementCount = input.ReadInt32();

        int memberCount = input.ReadInt32();
        variable.Members = (memberCount > 0) ? new ValueVariableMember[memberCount] : EmptyValueVariableMemberArray;
        for (int i = 0; i < memberCount; i++)
        {
          variable.Members[i] = ValueVariableMember.Read(input, versionNum);
        }

        return variable;
      }

      public static void Write(ValueVariable variable, BinaryWriter output)
      {
        output.Write(variable.Name);
        output.Write(variable.SizeInBytes);
        output.Write(variable.AlignedElementSizeInBytes);
        output.Write(variable.StartOffset);
        output.Write((int) variable.VariableClass);
        output.Write((int) variable.VariableType);
        output.Write(variable.ColumnCount);
        output.Write(variable.RowCount);
        output.Write((int) variable.Flags);
        output.Write(variable.StartSampler);
        output.Write(variable.SamplerSize);
        output.Write(variable.StartTexture);
        output.Write(variable.TextureSize);
        output.Write(variable.DefaultValue.Length);
        output.Write(variable.DefaultValue, 0, variable.DefaultValue.Length);
        output.Write(variable.ElementCount);

        if (variable.Members is null)
        {
          output.Write(0);
        }
        else
        {
          output.Write(variable.Members.Length);
          for (int i = 0; i < variable.Members.Length; i++)
          {
            ValueVariableMember.Write(variable.Members[i], output);
          }
        }
      }

      public void Dump(TextWriter output, String linePrefix)
      {
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "Name", Name));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "SizeInBytes", SizeInBytes.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "AlignedElementSizeInBytes", AlignedElementSizeInBytes.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "StartOffset", StartOffset.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "VariableClass", VariableClass.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "VariableType", VariableType.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "ColumnCount", ColumnCount.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "RowCount", RowCount.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "Flags", Flags.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "StartSampler", StartSampler.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "SamplerSize", SamplerSize.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "StartTexture", StartTexture.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "TextureSize", TextureSize.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "Default Value", "Byte[]"));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "ElementCount", ElementCount.ToString()));

        String nextPrefix = linePrefix + "\t";

        int memberCount = (Members is null) ? 0 : Members.Length;
        output.WriteLine();
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "# Members", memberCount.ToString()));
        output.WriteLine();
        for (int i = 0; i < memberCount; i++)
        {
          Members[i].Dump(output, nextPrefix);
          output.WriteLine();
        }
      }
    }

    public sealed class Shader
    {
      public ShaderStage ShaderType;
      public String ShaderProfile;
      public byte[] ShaderByteCode;
      public int HashCode;
      public D3DInputPrimitive GeometryShaderInputPrimitive;
      public D3DPrimitiveTopology GeometryShaderOutputTopology;
      public int GeometryShaderInstanceCount;
      public int MaxGeometryShaderOutputVertexCount;
      public D3DInputPrimitive GeometryOrHullShaderInputPrimitive;
      public D3DTessellatorOutputPrimitive HullShaderOutputPrimitive;
      public D3DTessellatorPartitioning HullShaderPartitioning;
      public D3DTessellatorDomain TessellatorDomain;
      public bool IsSampleFrequencyShader;
      public CompileFlags ShaderFlags;
      public Signature InputSignature;
      public Signature OutputSignature;
      public BoundResource[] BoundResources;

      public static Shader Read(BinaryReader input, int versionNum)
      {
        Shader shader = new Shader();

        shader.ShaderType = (ShaderStage) input.ReadInt32();
        shader.ShaderProfile = input.ReadString();
        shader.ShaderByteCode = input.ReadBytes(input.ReadInt32());
        shader.HashCode = input.ReadInt32();
        shader.GeometryShaderInputPrimitive = (D3DInputPrimitive) input.ReadInt32();
        shader.GeometryShaderOutputTopology = (D3DPrimitiveTopology) input.ReadInt32();
        shader.GeometryShaderInstanceCount = input.ReadInt32();
        shader.MaxGeometryShaderOutputVertexCount = input.ReadInt32();
        shader.GeometryOrHullShaderInputPrimitive = (D3DInputPrimitive) input.ReadInt32();
        shader.HullShaderOutputPrimitive = (D3DTessellatorOutputPrimitive) input.ReadInt32();
        shader.HullShaderPartitioning = (D3DTessellatorPartitioning) input.ReadInt32();
        shader.TessellatorDomain = (D3DTessellatorDomain) input.ReadInt32();
        shader.IsSampleFrequencyShader = input.ReadBoolean();
        shader.ShaderFlags = (CompileFlags) input.ReadInt32();

        shader.InputSignature = Signature.Read(input, versionNum);
        shader.OutputSignature = Signature.Read(input, versionNum);

        int boundResourceCount = input.ReadInt32();
        shader.BoundResources = (boundResourceCount > 0) ? new BoundResource[boundResourceCount] : EmptyBoundResourceArray;
        for (int i = 0; i < boundResourceCount; i++)
        {
          shader.BoundResources[i] = BoundResource.Read(input, versionNum);
        }

        return shader;
      }

      public static void Write(Shader shader, BinaryWriter output)
      {
        output.Write((int) shader.ShaderType);
        output.Write(shader.ShaderProfile);
        output.Write(shader.ShaderByteCode.Length);
        output.Write(shader.ShaderByteCode, 0, shader.ShaderByteCode.Length);
        output.Write(shader.HashCode);
        output.Write((int) shader.GeometryShaderInputPrimitive);
        output.Write((int) shader.GeometryShaderOutputTopology);
        output.Write(shader.GeometryShaderInstanceCount);
        output.Write(shader.MaxGeometryShaderOutputVertexCount);
        output.Write((int) shader.GeometryOrHullShaderInputPrimitive);
        output.Write((int) shader.HullShaderOutputPrimitive);
        output.Write((int) shader.HullShaderPartitioning);
        output.Write((int) shader.TessellatorDomain);
        output.Write(shader.IsSampleFrequencyShader);
        output.Write((int) shader.ShaderFlags);

        Signature.Write((shader.InputSignature is null) ? EmptySignature : shader.InputSignature, output);
        Signature.Write((shader.OutputSignature is null) ? EmptySignature : shader.OutputSignature, output);

        if (shader.BoundResources is null)
        {
          output.Write(0);
        }
        else
        {
          output.Write(shader.BoundResources.Length);
          for (int i = 0; i < shader.BoundResources.Length; i++)
          {
            BoundResource.Write(shader.BoundResources[i], output);
          }
        }
      }

      public void Dump(TextWriter output, String linePrefix)
      {
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "ShaderType", ShaderType.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "ShaderProfile", ShaderProfile.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "Bytecode", "Byte[]"));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "HashCode", HashCode.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "GeometryShaderInputPrimitive", GeometryShaderInputPrimitive.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "GeometryShaderOutputTopology", GeometryShaderOutputTopology.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "HullShaderOutputPrimitive", HullShaderOutputPrimitive.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "HullShaderPartitioning", HullShaderPartitioning.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "TessellatorDomain", TessellatorDomain.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "IsSampleFrequencyShader", IsSampleFrequencyShader.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "ShaderFlags", ShaderFlags.ToString()));

        String nextPrefix = linePrefix + "\t";

        output.WriteLine();
        output.WriteLine(String.Format("{0}{1}", linePrefix, "Input Signature:"));
        output.WriteLine();
        InputSignature.Dump(output, nextPrefix);

        output.WriteLine(String.Format("{0}{1}", linePrefix, "Output Signature:"));
        output.WriteLine();
        OutputSignature.Dump(output, nextPrefix);

        int boundResourceCount = (BoundResources is null) ? 0 : BoundResources.Length;
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "# Bound Resources", boundResourceCount.ToString()));
        output.WriteLine();
        for (int i = 0; i < boundResourceCount; i++)
        {
          BoundResources[i].Dump(output, nextPrefix);
          output.WriteLine();
        }
      }

      public override int GetHashCode()
      {
        return HashCode;
      }
    }

    public class Signature
    {
      public byte[] ByteCode;
      public int HashCode;
      public SignatureParameter[] Parameters;

      public static Signature Read(BinaryReader input, int versionNum)
      {
        Signature signature = new Signature();

        signature.ByteCode = input.ReadBytes(input.ReadInt32());
        signature.HashCode = input.ReadInt32();

        int paramCount = input.ReadInt32();
        signature.Parameters = (paramCount > 0) ? new SignatureParameter[paramCount] : EmptySignatureParameterArray;
        for (int i = 0; i < paramCount; i++)
        {
          signature.Parameters[i] = SignatureParameter.Read(input, versionNum);
        }

        return signature;
      }

      public static void Write(Signature signature, BinaryWriter output)
      {
        output.Write(signature.ByteCode.Length);
        output.Write(signature.ByteCode);
        output.Write(signature.HashCode);

        if (signature.Parameters is null)
        {
          output.Write(0);
        }
        else
        {
          output.Write(signature.Parameters.Length);
          for (int i = 0; i < signature.Parameters.Length; i++)
          {
            SignatureParameter.Write(signature.Parameters[i], output);
          }
        }
      }

      public void Dump(TextWriter output, String linePrefix)
      {
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "Bytecode", "Byte[]"));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "HashCode", HashCode.ToString()));

        String nextPrefix = linePrefix + "\t";

        int paramCount = (Parameters is null) ? 0 : Parameters.Length;
        output.WriteLine();
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "# Signature Parameters", paramCount.ToString()));
        output.WriteLine();
        for (int i = 0; i < paramCount; i++)
        {
          Parameters[i].Dump(output, nextPrefix);
          output.WriteLine();
        }
      }

      public override int GetHashCode()
      {
        return HashCode;
      }
    }

    public struct BoundResource
    {
      public D3DShaderInputType ResourceType;
      public int ResourceIndex;
      public int BindPoint;
      public int BindCount;

      public static BoundResource Read(BinaryReader input, int versionNum)
      {
        BoundResource resource;
        resource.ResourceType = (D3DShaderInputType) input.ReadInt32();
        resource.ResourceIndex = input.ReadInt32();
        resource.BindPoint = input.ReadInt32();
        resource.BindCount = input.ReadInt32();

        return resource;
      }

      public static void Write(BoundResource resource, BinaryWriter output)
      {
        output.Write((int) resource.ResourceType);
        output.Write(resource.ResourceIndex);
        output.Write(resource.BindPoint);
        output.Write(resource.BindCount);
      }

      public void Dump(TextWriter output, String linePrefix)
      {
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "ResourceType", ResourceType.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "ResourceIndex", ResourceIndex.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "BindPoint", BindPoint.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "BindCount", BindCount.ToString()));
      }
    }

    public struct SignatureParameter
    {
      public String SemanticName;
      public int SemanticIndex;
      public int StreamIndex;
      public int Register;
      public D3DComponentMaskFlags UsageMask;
      public D3DComponentMaskFlags ReadWriteMask;
      public D3DComponentType ComponentType;
      public D3DSystemValueType SystemType;

      public static SignatureParameter Read(BinaryReader input, int versionNum)
      {
        SignatureParameter parameter;
        parameter.SemanticName = input.ReadString();
        parameter.SemanticIndex = input.ReadInt32();
        parameter.StreamIndex = input.ReadInt32();
        parameter.Register = input.ReadInt32();
        parameter.UsageMask = (D3DComponentMaskFlags) input.ReadInt32();
        parameter.ReadWriteMask = (D3DComponentMaskFlags) input.ReadInt32();
        parameter.ComponentType = (D3DComponentType) input.ReadInt32();
        parameter.SystemType = (D3DSystemValueType) input.ReadInt32();

        return parameter;
      }

      public static void Write(SignatureParameter parameter, BinaryWriter output)
      {
        output.Write(parameter.SemanticName);
        output.Write(parameter.SemanticIndex);
        output.Write(parameter.StreamIndex);
        output.Write(parameter.Register);
        output.Write((int) parameter.UsageMask);
        output.Write((int) parameter.ReadWriteMask);
        output.Write((int) parameter.ComponentType);
        output.Write((int) parameter.SystemType);
      }

      public void Dump(TextWriter output, String linePrefix)
      {
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "SemanticName", SemanticName));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "SemanticIndex", SemanticIndex.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "StreamIndex", StreamIndex.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "Register", Register.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "UsageMask", UsageMask.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "ReadWriteMask", ReadWriteMask.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "ComponentType", ComponentType.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "SystemType", SystemType.ToString()));
      }
    }

    public struct SamplerStateData
    {
      public TextureAddressMode AddressU;
      public TextureAddressMode AddressV;
      public TextureAddressMode AddressW;
      public ComparisonFunction ComparisonFunction;
      public TextureFilter Filter;
      public int MaxAnisotropy;
      public float MipMapLevelOfDetailBias;
      public int MinMipMapLevel; //Min = 0
      public int MaxMipMapLevel; //Max = int.MaxValue
      public Color BorderColor;

      public static SamplerStateData Read(BinaryReader input, int versionNum)
      {
        SamplerStateData ssData;
        ssData.AddressU = (TextureAddressMode) input.ReadByte();
        ssData.AddressV = (TextureAddressMode) input.ReadByte();
        ssData.AddressW = (TextureAddressMode) input.ReadByte();
        ssData.ComparisonFunction = (ComparisonFunction) input.ReadByte();
        ssData.Filter = (TextureFilter) input.ReadByte();
        ssData.MaxAnisotropy = (int) input.ReadByte(); //Range in 1-16
        ssData.MipMapLevelOfDetailBias = input.ReadSingle();
        ssData.MinMipMapLevel = input.ReadInt32();
        ssData.MaxMipMapLevel = input.ReadInt32();
        ssData.BorderColor = new Color(input.ReadUInt32());

        return ssData;
      }

      public static void Write(SamplerStateData ssData, BinaryWriter output)
      {
        output.Write((byte) ssData.AddressU);
        output.Write((byte) ssData.AddressV);
        output.Write((byte) ssData.AddressW);
        output.Write((byte) ssData.ComparisonFunction);
        output.Write((byte) ssData.Filter);
        output.Write((byte) ssData.MaxAnisotropy); //Range in 1-16
        output.Write(ssData.MipMapLevelOfDetailBias);
        output.Write(ssData.MinMipMapLevel);
        output.Write(ssData.MaxMipMapLevel);
        output.Write(ssData.BorderColor.PackedValue);
      }

      public void Dump(TextWriter output, String linePrefix)
      {
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "AddressU", AddressU.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "AddressV", AddressV.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "AddressW", AddressW.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "Filter", Filter.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "ComparisonFunction", ComparisonFunction.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "MaxAnisotropy", MaxAnisotropy.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "MipMapLevelOfDetailBias", MipMapLevelOfDetailBias.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "MinMipMapLevel", MinMipMapLevel.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "MaxMipMapLevel", MaxMipMapLevel.ToString()));
        output.WriteLine(String.Format("{0}{1}: {2}", linePrefix, "BorderColor", BorderColor.ToString()));
      }
    }

    #endregion
  }
}
