﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.IO;
using Tesla;
using Tesla.Graphics;

using D3D11 = SharpDX.Direct3D11;
using DXGI = SharpDX.DXGI;

namespace Tesla.Direct3D11.Graphics
{
  /// <summary>
  /// Interface that specifies how #include files should be located.
  /// </summary>
  public interface IIncludeHandler
  {
    /// <summary>
    /// User implemented method for closing the #include file stream.
    /// </summary>
    /// <param name="stream">Stream that was opened</param>
    void Close(Stream stream);

    /// <summary>
    /// User implemented method for opening and reading the contents of a shader #include file.
    /// </summary>
    /// <param name="includeType">Determines the location of the #include file</param>
    /// <param name="fileName">Name of the #include file</param>
    /// <param name="parentStream">Stream that contains the shader contents that includes the #include file</param>
    Stream Open(IncludeType includeType, String fileName, Stream parentStream);

    /// <summary>
    /// Adds an include directory to search in.
    /// </summary>
    /// <param name="directory">Directory to search in.</param>
    /// <returns>True if successfully added to the search list, false otherwise.</returns>
    bool AddIncludeDirectory(String directory);

    /// <summary>
    /// Removes an include directory to search in.
    /// </summary>
    /// <param name="directory">Directory to remove.</param>
    /// <returns>True if successfully removed from the list, false otherwise.</returns>
    bool RemoveIncludeDirectory(String directory);
  }

  /// <summary>
  /// Interface for an effect compiler that takes in TEFX source code (.fx or HLSL fragments) and outputs effect data
  /// that can be consumed by the runtime.
  /// </summary>
  public interface IEffectCompiler
  {
    /// <summary>
    /// Compiles an effect (.fx) from a file.
    /// </summary>
    /// <param name="effectFileName">File name of the effect.</param>
    /// <param name="flags">Compile flags.</param>
    /// <param name="macros">Macros to be used during compiling.</param>
    /// <returns>Compile result</returns>
    EffectCompilerResult CompileFromFile(String effectFileName, CompileFlags flags, ShaderMacro[] macros);

    /// <summary>
    /// Compiles an effect (.fx).
    /// </summary>
    /// <param name="effectCode">Effect code to compile.</param>
    /// <param name="flags">Compile flags.</param>
    /// <param name="sourceFileName">Original source name of file.</param>
    /// <param name="macros">Macros to be used during compiling.</param>
    /// <returns>Compile result</returns>
    EffectCompilerResult Compile(String effectCode, CompileFlags flags, String sourceFileName, ShaderMacro[] macros);

    /// <summary>
    /// Compiles an effect.
    /// </summary>
    /// <param name="effectContent">Effect content that defines HLSL source code, entry points, etc that will be assembled into a complete effect.</param>
    /// <param name="flags">Compile flags.</param>
    /// <param name="macros">Macros to be used during compiling.</param>
    /// <returns>Compile result</returns>
    EffectCompilerResult Compile(TEFXContent effectContent, CompileFlags flags, ShaderMacro[] macros);

    /// <summary>
    /// Sets the include handler to locate include files.
    /// </summary>
    /// <param name="includeHandler">Handler to locate and open include files.</param>
    void SetIncludeHandler(IIncludeHandler includeHandler);
  }

  /// <summary>
  /// Defines an object that holds a native D3D11 shader resource view.
  /// </summary>
  public interface ID3D11ShaderResourceView
  {
    /// <summary>
    /// Gets the native D3D11 shader resource view.
    /// </summary>
    D3D11.ShaderResourceView D3DShaderResourceView { get; }

    /// <summary>
    /// Gets or sets the shader stages that this resource is currently bound to.
    /// </summary>
    ShaderStageBinding BoundStages { get; set; }
  }

  /// <summary>
  /// Defines an object that holds a native D3D11 render target view and which can resolve the resource.
  /// </summary>
  public interface ID3D11RenderTargetView
  {
    /// <summary>
    /// Gets the native D3D11 render target view.
    /// </summary>
    D3D11.RenderTargetView D3DRenderTargetView { get; }

    /// <summary>
    /// Called on the first render target in the group before the group is bound to the context.
    /// </summary>
    void NotifyOnFirstBind();

    /// <summary>
    /// Resolves the resource if its multisampled and does any mip map generation.
    /// </summary>
    /// <param name="deviceContext">Device context</param>
    void ResolveResource(D3D11.DeviceContext deviceContext);

    /// <summary>
    /// Clears the render target.
    /// </summary>
    /// <param name="deviceContext">Device context</param>
    /// <param name="options">Clear options</param>
    /// <param name="color">Color to clear to.</param>
    /// <param name="depth">Depth to clear to.</param>
    /// <param name="stencil">Stencil to clear to</param>
    void Clear(D3D11.DeviceContext deviceContext, ClearOptions options, Color color, float depth, int stencil);
  }

  /// <summary>
  /// Defines an object that holds a native D3D11 depth stencil view and which can resolve the resource.
  /// </summary>
  public interface ID3D11DepthStencilView
  {
    /// <summary>
    /// Gets the native D3D11 depth stencil view.
    /// </summary>
    D3D11.DepthStencilView D3DDepthStencilView { get; }

    /// <summary>
    /// Gets the native read-only D3D11 depth stencil view (if not readable, then this should be null).
    /// </summary>
    D3D11.DepthStencilView D3DReadOnlyDepthStencilView { get; }

    /// <summary>
    /// Clears the depth stencil buffer.
    /// </summary>
    /// <param name="deviceContext">Device context</param>
    /// <param name="options">Clear options</param>
    /// <param name="depth">Depth to clear to.</param>
    /// <param name="stencil">Stencil to clear to</param>
    void Clear(D3D11.DeviceContext deviceContext, ClearOptions options, float depth, int stencil);
  }

  /// <summary>
  /// Defines an object that manages a backbuffer that is set to the render context. When it is the active backbuffer and is resized,
  /// the render context automatically knows to re-apply the resource views.
  /// </summary>
  public interface ID3D11Backbuffer : ID3D11RenderTargetView, ID3D11DepthStencilView
  {
    /// <summary>
    /// Occurs when the backbuffer views are destroyed during a resize or reset event.
    /// </summary>
    event TypedEventHandler<ID3D11Backbuffer, EventArgs> OnResetResize;
  }

  /// <summary>
  /// Defines an object that holds a native DXGI Swapchain.
  /// </summary>
  public interface ID3D11SwapChain : ID3D11Backbuffer
  {
    /// <summary>
    /// Gets the native DXGI swap chain.
    /// </summary>
    DXGI.SwapChain DXGISwapChain { get; }
  }

  /// <summary>
  /// Defines an object that holds a native D3D11 buffer.
  /// </summary>
  public interface ID3D11Buffer
  {
    /// <summary>
    /// Gets the native D3D11 buffer.
    /// </summary>
    D3D11.Buffer D3DBuffer { get; }
  }

  /// <summary>
  /// Defines an object that holds a native D3D11 blend state.
  /// </summary>
  public interface ID3D11BlendState
  {
    /// <summary>
    /// Gets the native D3D11 blend state.
    /// </summary>
    D3D11.BlendState D3DBlendState { get; }
  }

  /// <summary>
  /// Defines an object that holds a native D3D11 rasterizer state.
  /// </summary>
  public interface ID3D11RasterizerState
  {
    /// <summary>
    /// Gets the native D3D11 rasterizer state.
    /// </summary>
    D3D11.RasterizerState D3DRasterizerState { get; }
  }

  /// <summary>
  /// Defines an object that holds a native D3D11 sampler state.
  /// </summary>
  public interface ID3D11SamplerState
  {
    /// <summary>
    /// Gets the native D3D11 sampler state.
    /// </summary>
    D3D11.SamplerState D3DSamplerState { get; }
  }

  /// <summary>
  /// Defines an object that holds a native D3D11 depth stencil state.
  /// </summary>
  public interface ID3D11DepthStencilState
  {
    /// <summary>
    /// Gets the native D3D11 depth stencil state.
    /// </summary>
    D3D11.DepthStencilState D3DDepthStencilState { get; }
  }

  /// <summary>
  /// Defines an object that holds a native D3D11 command list.
  /// </summary>
  public interface ID3D11CommandList
  {
    /// <summary>
    /// Gets the native D3D11 command list.
    /// </summary>
    D3D11.CommandList D3DCommandList { get; }
  }
}
