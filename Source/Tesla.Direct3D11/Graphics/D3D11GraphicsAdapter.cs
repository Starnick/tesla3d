﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using Tesla.Graphics;
using D3D = SharpDX.Direct3D;
using D3D11 = SharpDX.Direct3D11;
using DXGI = SharpDX.DXGI;
using SDX = SharpDX;
using SDXMI = SharpDX.Mathematics.Interop;

namespace Tesla.Direct3D11.Graphics
{
  /// <summary>
  /// Implementation for a Direct3D11 graphics adapter.
  /// </summary>
  public class D3D11GraphicsAdapter : IGraphicsAdapter
  {
    private D3D11.Device m_device;
    private D3D.FeatureLevel m_featureLevel;
    private int m_adapterIndex;
    private DisplayModeCollection m_supportedDisplayModes;
    private OutputCollection m_outputs;
    private String m_description;
    private int m_deviceId;
    private int m_revision;
    private int m_subsystemId;
    private int m_vendorId;
    private long m_dedicatedVideoMemory;
    private long m_sharedSystemMemory;
    private long m_dedicatedSystemMemory;

    /// <summary>
    /// Gets the collection of supported display modes.
    /// </summary>
    public DisplayModeCollection SupportedDisplayModes
    {
      get
      {
        return m_supportedDisplayModes;
      }
    }

    /// <summary>
    /// Gets the collection of outputs (e.g. monitors).
    /// </summary>
    public OutputCollection Outputs
    {
      get
      {
        return m_outputs;
      }
    }

    /// <summary>
    /// Gets the description of the device.
    /// </summary>
    public String Description
    {
      get
      {
        return m_description;
      }
    }

    /// <summary>
    /// Gets the device ID which identifies the particular chipset.
    /// </summary>
    public int DeviceId
    {
      get
      {
        return m_deviceId;
      }
    }

    /// <summary>
    /// Gets the adapter's revision number for the particular chipset its associated with.
    /// </summary>
    public int Revision
    {
      get
      {
        return m_revision;
      }
    }

    /// <summary>
    /// Gets the value that identifies the adapter's subsystem.
    /// </summary>
    public int SubSystemId
    {
      get
      {
        return m_subsystemId;
      }
    }

    /// <summary>
    /// Gets the value that identifies that chipset's manufacturer.
    /// </summary>
    public int VendorId
    {
      get
      {
        return m_vendorId;
      }
    }

    /// <summary>
    /// Gets if this is the default adapter, always the first adapter.
    /// </summary>
    public bool IsDefaultAdapter
    {
      get
      {
        return m_adapterIndex == 0;
      }
    }

    /// <summary>
    /// Gets the adapter index.
    /// </summary>
    public int AdapterIndex
    {
      get
      {
        return m_adapterIndex;
      }
    }

    /// <summary>
    /// Gets the maximum (U) size of a Texture1D resource.
    /// </summary>
    public int MaximumTexture1DSize
    {
      get
      {
        return GetMaximumTextureSize(TextureDimension.One);
      }
    }

    /// <summary>
    /// Gets the maximum number of array slices in a Texture1DArray resource, if zero arrays are not supported.
    /// </summary>
    public int MaximumTexture1DArrayCount
    {
      get
      {
        return GetMaximumTextureArrayCount(TextureDimension.One);
      }
    }

    /// <summary>
    /// Gets the maximum size (U,V) of a Texture2D resource.
    /// </summary>
    public int MaximumTexture2DSize
    {
      get
      {
        return GetMaximumTextureSize(TextureDimension.Two);
      }
    }

    /// <summary>
    /// Gets the maximum number of array slices in a Texture2DArray resource, if zero arrays are not supported.
    /// </summary>
    public int MaximumTexture2DArrayCount
    {
      get
      {
        return GetMaximumTextureArrayCount(TextureDimension.Two);
      }
    }

    /// <summary>
    /// Gets the maximum size (U,V,W) of a Texture3D resource.
    /// </summary>
    public int MaximumTexture3DSize
    {
      get
      {
        return GetMaximumTextureSize(TextureDimension.Three);
      }
    }

    /// <summary>
    /// Gets the maximum size of a TextureCube resource.
    /// </summary>
    public int MaximumTextureCubeSize
    {
      get
      {
        return GetMaximumTextureSize(TextureDimension.Cube);
      }
    }

    /// <summary>
    /// Gets the maximum number of array slices in a Texture2DArray resource, if zero arrays are not supported.
    /// </summary>
    public int MaximumTextureCubeArrayCount
    {
      get
      {
        return GetMaximumTextureArrayCount(TextureDimension.Cube);
      }
    }

    /// <summary>
    /// Gets the maximum size of any texture resource in bytes.
    /// </summary>
    public int MaximumTextureResourceSize
    {
      get
      {
        return GetMaximumTextureResouceSize();
      }
    }

    /// <summary>
    /// Gets the maximum number of render targets that can be set to the
    /// device at once (MRT).
    /// </summary>
    public int MaximumMultiRenderTargets
    {
      get
      {
        return GetMaximumMultiRenderTargets();
      }
    }

    /// <summary>
    /// Gets the maximum number of vertex buffers that can be set to the device at once.
    /// </summary>
    public int MaximumVertexStreams
    {
      get
      {
        return GetMaximumVertexStreams();
      }
    }

    /// <summary>
    /// Gets the maximum number of stream output targets that can be set to the device at once.
    /// </summary>
    public int MaximumStreamOutputTargets
    {
      get
      {
        return GetMaximumStreamOutputTargets();
      }
    }

    /// <summary>
    /// Gets the number of bytes of system memory not shared with the CPU.
    /// </summary>
    public long DedicatedSystemMemory
    {
      get
      {
        return m_dedicatedSystemMemory;
      }
    }

    /// <summary>
    /// Gets the number of bytes of video memory not shared with the CPU.
    /// </summary>
    public long DedicatedVideoMemory
    {
      get
      {
        return m_dedicatedVideoMemory;
      }
    }

    /// <summary>
    /// Gets the number of bytes of system memory shared with the CPU.
    /// </summary>
    public long SharedSystemMemory
    {
      get
      {
        return m_sharedSystemMemory;
      }
    }

    /// <summary>
    /// Gets the D3D11 Device. Only valid when the adapter is used to initialize a render system.
    /// </summary>
    public D3D11.Device D3D11Device
    {
      get
      {
        return m_device;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="D3D11GraphicsAdapter"/> class.
    /// </summary>
    /// <param name="adapter">The DXGI adapter.</param>
    /// <param name="index">The adapter index.</param>
    internal D3D11GraphicsAdapter(DXGI.Adapter adapter, int index)
    {
      m_adapterIndex = index;

      DXGI.AdapterDescription desc = adapter.Description;
      m_dedicatedSystemMemory = desc.DedicatedSystemMemory;
      m_dedicatedVideoMemory = desc.DedicatedVideoMemory;
      m_sharedSystemMemory = desc.SharedSystemMemory;
      m_description = desc.Description;
      m_deviceId = desc.DeviceId;
      m_subsystemId = desc.SubsystemId;
      m_vendorId = desc.VendorId;
      m_revision = desc.Revision;
      m_featureLevel = D3D.FeatureLevel.Level_11_0;

      DXGI.Output[] dxgiOutputs = adapter.Outputs;
      Output[] outputs = new Output[dxgiOutputs.Length];
      List<DisplayMode> modes = new List<DisplayMode>();

      for (int i = 0; i < dxgiOutputs.Length; i++)
      {
        DXGI.Output dxgiOutput = dxgiOutputs[i];
        DXGI.OutputDescription dxgiOutputDesc = dxgiOutput.Description;

        Rectangle desktopBounds;
        SDXMI.RawRectangle dxgiDesktopBounds = dxgiOutputDesc.DesktopBounds;
        Direct3DHelper.ConvertRectangle(ref dxgiDesktopBounds, out desktopBounds);

        outputs[i] = new Output(dxgiOutputDesc.MonitorHandle, dxgiOutputDesc.DeviceName, desktopBounds);
        foreach (SurfaceFormat format in Enum.GetValues(typeof(SurfaceFormat)))
        {
          try
          {
            DXGI.ModeDescription[] modeList = dxgiOutput.GetDisplayModeList(Direct3DHelper.ToD3DSurfaceFormat(format), 0);
            if (modeList is null)
              continue;

            foreach (DXGI.ModeDescription modeDesc in modeList)
            {
              DisplayMode displayMode = new DisplayMode(modeDesc.Width, modeDesc.Height,
                  (int) MathF.Floor((float) modeDesc.RefreshRate.Numerator / (float) modeDesc.RefreshRate.Denominator), format);

              if (!modes.Contains(displayMode))
              {
                modes.Add(displayMode);
              }
            }

          }
          catch (Exception e)
          {
            EngineLog.LogException(LogLevel.Error, e);
          }
        }

        dxgiOutput.Dispose();
      }

      m_supportedDisplayModes = new DisplayModeCollection(modes);
      m_outputs = new OutputCollection(outputs);
    }

    /// <summary>
    /// Checks if the specified surface format is valid for texture resources.
    /// </summary>
    /// <param name="surfaceFormat">Surface format</param>
    /// <param name="texType">Type of texture</param>
    /// <returns>True if valid, false otherwise</returns>
    public bool CheckTextureFormat(SurfaceFormat surfaceFormat, TextureDimension texType)
    {
      if (m_device is null || m_device.IsDisposed)
        return false;

      D3D11.FormatSupport support = m_device.CheckFormatSupport(Direct3DHelper.ToD3DSurfaceFormat(surfaceFormat));

      switch (texType)
      {
        case TextureDimension.One:
          return Direct3DHelper.IsFlagSet((int) support, (int) D3D11.FormatSupport.Texture1D);
        case TextureDimension.Two:
        case TextureDimension.Cube:
          return Direct3DHelper.IsFlagSet((int) support, (int) D3D11.FormatSupport.Texture2D);
        case TextureDimension.Three:
          return Direct3DHelper.IsFlagSet((int) support, (int) D3D11.FormatSupport.Texture3D);
        default:
          return false;
      }
    }

    /// <summary>
    /// Checks if the specified formats and sample counts are valid for a render target.
    /// </summary>
    /// <param name="format">Surface format</param>
    /// <param name="depthFormat">Depth format</param>
    /// <param name="multiSampleCount">Sample count</param>
    /// <returns>
    /// True if a valid combination, false otherwise.
    /// </returns>
    public bool CheckRenderTargetFormat(SurfaceFormat format, DepthFormat depthFormat, int multiSampleCount)
    {
      return CheckSurfaceFormat(format, multiSampleCount) && CheckDepthFormat(depthFormat, multiSampleCount);
    }

    /// <summary>
    /// Checks if the specified formats and sample counts are valid for a back buffer.
    /// </summary>
    /// <param name="format">Surface format</param>
    /// <param name="depthFormat">Depth format</param>
    /// <param name="multiSampleCount">Sample count</param>
    /// <returns>True if a valid combination, false otherwise.</returns>
    public bool CheckBackBufferFormat(SurfaceFormat format, DepthFormat depthFormat, int multiSampleCount)
    {
      return CheckSurfaceFormat(format, multiSampleCount) && CheckDepthFormat(depthFormat, multiSampleCount);
    }

    /// <summary>
    /// Checks if the render target format can be resolved if multisampled.
    /// </summary>
    /// <param name="format">Format to test</param>
    /// <returns>True if it is resolvable, false otherwise.</returns>
    public bool IsMultisampleResolvable(SurfaceFormat format)
    {
      if (m_device is null || m_device.IsDisposed)
        return false;

      D3D11.FormatSupport support = m_device.CheckFormatSupport(Direct3DHelper.ToD3DSurfaceFormat(format));
      return Direct3DHelper.IsFlagSet((int) support, (int) D3D11.FormatSupport.MultisampleResolve);
    }

    /// <summary>
    /// Checks if the shader stage is supported or not.
    /// </summary>
    /// <param name="shaderStage">Shader stage</param>
    /// <returns>True if the shader stage is supported, false otherwise.</returns>
    public bool IsShaderStageSupported(ShaderStage shaderStage)
    {
      switch (m_featureLevel)
      {
        case D3D.FeatureLevel.Level_11_1:
        case D3D.FeatureLevel.Level_11_0:
          return true;
        case D3D.FeatureLevel.Level_10_1:
        case D3D.FeatureLevel.Level_10_0:
          if (shaderStage == ShaderStage.ComputeShader)
            return false;
          return true;
        case D3D.FeatureLevel.Level_9_3:
        case D3D.FeatureLevel.Level_9_2:
        case D3D.FeatureLevel.Level_9_1:
          if (shaderStage == ShaderStage.VertexShader || shaderStage == ShaderStage.PixelShader)
            return true;
          return false;
        default:
          return false;
      }
    }

    /// <summary>
    /// Checks for the number of multisample quality levels are supported for the sample count. A value of zero
    /// means the format/multisample count combination is not valid. A non-zero value determines the number of quality levels that can be
    /// set, so a value of one means quality level zero, a value of two means quality levels zero and one, etc. The meanings of each quality level is
    /// vendor-specific.
    /// </summary>
    /// <param name="format">Specified format</param>
    /// <param name="multiSamplecount">Sample count</param>
    /// <returns>Number of supported quality levels.</returns>
    public int CheckMultisampleQualityLevels(SurfaceFormat format, int multiSamplecount)
    {
      if (m_device is null || m_device.IsDisposed)
        return 0;

      return m_device.CheckMultisampleQualityLevels(Direct3DHelper.ToD3DSurfaceFormat(format), multiSamplecount);
    }

    /// <summary>
    /// Returns a string that represents the current object.
    /// </summary>
    /// <returns>A string that represents the current object.</returns>
    public override String ToString()
    {
      return m_description;
    }

    internal void SetDevice(D3D11.Device device)
    {
      m_device = device;

      if (m_device is not null && !m_device.IsDisposed)
        m_featureLevel = m_device.FeatureLevel;
    }

    private bool CheckDepthFormat(DepthFormat depthFormat, int sampleCount)
    {
      if (depthFormat == DepthFormat.None)
        return true;

      if (m_device is null || m_device.IsDisposed)
        return false;

      DXGI.Format format = Direct3DHelper.ToD3DDepthFormat(depthFormat);
      D3D11.FormatSupport support = m_device.CheckFormatSupport(format);

      if (sampleCount > 0)
      {
        bool correctSamples = m_device.CheckMultisampleQualityLevels(format, sampleCount) > 0;

        return correctSamples && Direct3DHelper.IsFlagSet((int) support, (int) D3D11.FormatSupport.DepthStencil) &&
            Direct3DHelper.IsFlagSet((int) support, (int) D3D11.FormatSupport.MultisampleRenderTarget);
      }

      return Direct3DHelper.IsFlagSet((int) support, (int) D3D11.FormatSupport.DepthStencil);
    }

    private bool CheckSurfaceFormat(SurfaceFormat surfaceFormat, int sampleCount)
    {
      if (m_device is null || m_device.IsDisposed)
        return false;

      DXGI.Format format = Direct3DHelper.ToD3DSurfaceFormat(surfaceFormat);
      D3D11.FormatSupport support = m_device.CheckFormatSupport(format);

      if (sampleCount > 0)
      {
        bool correctSamples = m_device.CheckMultisampleQualityLevels(format, sampleCount) > 0;

        return correctSamples && Direct3DHelper.IsFlagSet((int) support, (int) D3D11.FormatSupport.RenderTarget) &&
            Direct3DHelper.IsFlagSet((int) support, (int) D3D11.FormatSupport.MultisampleRenderTarget);
      }

      return Direct3DHelper.IsFlagSet((int) support, (int) D3D11.FormatSupport.RenderTarget);
    }

    private int GetMaximumTextureSize(TextureDimension texDim)
    {
      switch (texDim)
      {
        case TextureDimension.One:
        case TextureDimension.Two:
          switch (m_featureLevel)
          {
            case D3D.FeatureLevel.Level_11_1:
            case D3D.FeatureLevel.Level_11_0:
              return 16384;
            case D3D.FeatureLevel.Level_10_1:
            case D3D.FeatureLevel.Level_10_0:
              return 8192;
            case D3D.FeatureLevel.Level_9_3:
              return 4096;
            case D3D.FeatureLevel.Level_9_2:
            case D3D.FeatureLevel.Level_9_1:
              return 2048;
            default:
              return 0;
          }
        case TextureDimension.Three:
          switch (m_featureLevel)
          {
            case D3D.FeatureLevel.Level_11_1:
            case D3D.FeatureLevel.Level_11_0:
            case D3D.FeatureLevel.Level_10_1:
            case D3D.FeatureLevel.Level_10_0:
              return 2048;
            case D3D.FeatureLevel.Level_9_3:
            case D3D.FeatureLevel.Level_9_2:
            case D3D.FeatureLevel.Level_9_1:
              return 256;
            default:
              return 0;
          }
        case TextureDimension.Cube:
          switch (m_featureLevel)
          {
            case D3D.FeatureLevel.Level_11_1:
            case D3D.FeatureLevel.Level_11_0:
              return 16384;
            case D3D.FeatureLevel.Level_10_1:
            case D3D.FeatureLevel.Level_10_0:
              return 8192;
            case D3D.FeatureLevel.Level_9_3:
              return 4096;
            case D3D.FeatureLevel.Level_9_2:
            case D3D.FeatureLevel.Level_9_1:
              return 512;
            default:
              return 0;
          }
        default:
          return 0;
      }
    }

    private int GetMaximumTextureArrayCount(TextureDimension texDim)
    {
      switch (texDim)
      {
        case TextureDimension.One:
          switch (m_featureLevel)
          {
            case D3D.FeatureLevel.Level_11_1:
            case D3D.FeatureLevel.Level_11_0:
              return 2048;
            case D3D.FeatureLevel.Level_10_1:
            case D3D.FeatureLevel.Level_10_0:
              return 512;
            default:
              return 1;
          }
        case TextureDimension.Two:
          switch (m_featureLevel)
          {
            case D3D.FeatureLevel.Level_11_1:
            case D3D.FeatureLevel.Level_11_0:
              return 2048;
            case D3D.FeatureLevel.Level_10_1:
            case D3D.FeatureLevel.Level_10_0:
              return 512;
            default:
              return 1;
          }
        case TextureDimension.Cube:
          switch (m_featureLevel)
          {
            case D3D.FeatureLevel.Level_11_1:
            case D3D.FeatureLevel.Level_11_0:
              return 2046; // 2D array max / 6, rounded down
            case D3D.FeatureLevel.Level_10_1:
            case D3D.FeatureLevel.Level_10_0:
              return 85;
            default:
              return 1;
          }
        default:
          return 0;
      }
    }

    private int GetMaximumTextureResouceSize()
    {
      switch (m_featureLevel)
      {
        case D3D.FeatureLevel.Level_11_1:
        case D3D.FeatureLevel.Level_11_0:
          return (int) (Math.Min(Math.Max(128f, 0.5f * (m_dedicatedVideoMemory / 1048576f)), 2048f) * 1048576f);
        case D3D.FeatureLevel.Level_10_1:
        case D3D.FeatureLevel.Level_10_0:
        case D3D.FeatureLevel.Level_9_3:
        case D3D.FeatureLevel.Level_9_2:
        case D3D.FeatureLevel.Level_9_1:
          return 128;
        default:
          return 0;
      }
    }

    private int GetMaximumVertexStreams()
    {
      switch (m_featureLevel)
      {
        case D3D.FeatureLevel.Level_11_1:
        case D3D.FeatureLevel.Level_11_0:
        case D3D.FeatureLevel.Level_10_1:
          return 32;
        case D3D.FeatureLevel.Level_10_0:
        case D3D.FeatureLevel.Level_9_3:
        case D3D.FeatureLevel.Level_9_2:
        case D3D.FeatureLevel.Level_9_1:
          return 16;
        default:
          return 1;
      }
    }

    private int GetMaximumMultiRenderTargets()
    {
      switch (m_featureLevel)
      {
        case D3D.FeatureLevel.Level_11_1:
        case D3D.FeatureLevel.Level_11_0:
        case D3D.FeatureLevel.Level_10_1:
        case D3D.FeatureLevel.Level_10_0:
          return 8;
        case D3D.FeatureLevel.Level_9_3:
          return 4;
        case D3D.FeatureLevel.Level_9_2:
        case D3D.FeatureLevel.Level_9_1:
          return 1;
        default:
          return 0;
      }
    }

    private int GetMaximumStreamOutputTargets()
    {
      switch (m_featureLevel)
      {
        case D3D.FeatureLevel.Level_11_1:
        case D3D.FeatureLevel.Level_11_0:
        case D3D.FeatureLevel.Level_10_1:
        case D3D.FeatureLevel.Level_10_0:
          return 4;
        default:
          return 0;
      }
    }
  }
}
