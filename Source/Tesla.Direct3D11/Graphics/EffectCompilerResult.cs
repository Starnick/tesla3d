﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;

namespace Tesla.Direct3D11.Graphics
{
  /// <summary>
  /// Represents the result of compiling an effect source file.
  /// </summary>
  public sealed class EffectCompilerResult
  {
    private EffectData m_effectData;
    private String[] m_compileErrors;

    /// <summary>
    /// Gets the compiled effect (if successful).
    /// </summary>
    public EffectData EffectData
    {
      get
      {
        return m_effectData;
      }
    }

    /// <summary>
    /// Gets if the result was not successful and there are compile errors.
    /// </summary>
    public bool HasCompileErrors
    {
      get
      {
        return m_compileErrors is not null && m_compileErrors.Length > 0;
      }
    }

    /// <summary>
    /// Gets compile errors, if any.
    /// </summary>
    public String[] CompileErrors
    {
      get
      {
        return m_compileErrors;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="EffectCompilerResult"/> class.
    /// </summary>
    /// <param name="effectData">Compiled effect data.</param>
    /// <param name="errorMessages">Compile error messages.</param>
    public EffectCompilerResult(EffectData effectData, String[] errorMessages)
    {
      m_effectData = effectData;
      m_compileErrors = errorMessages;
    }
  }
}
