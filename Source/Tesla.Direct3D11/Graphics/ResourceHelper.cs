﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Buffers;
using SharpDX;
using Tesla.Graphics;
using D3D11 = SharpDX.Direct3D11;
using SDX = SharpDX;

#nullable enable

namespace Tesla.Direct3D11.Graphics
{
  /// <summary>
  /// Helper class for reading/writing to Direct3D11 resources.
  /// </summary>
  public static class ResourceHelper
  {
    private static Object s_sync = new Object();

    /// <summary>
    /// Createds an interleaved vertex buffer containing data from each attribute buffer.
    /// </summary>
    /// <param name="vertexLayout">The vertex layout defining the contents of the buffer.</param>
    /// <param name="data">Span of databuffers representing the individual vertex attributes.</param>
    /// <param name="allocatorStrategy">Optional allocator strategy.</param>
    /// <param name="validateData">True to validate the data, false to just do the copy.</param>
    /// <returns>Interleaved vertex buffer.</returns>
    /// <exception cref="System.ArgumentNullException">Thrown if span is empty or any buffers are null.</exception>
    /// <exception cref="System.ArgumentOutOfRangeException">Thrown if the number of buffers do not match the number
    /// of vertex elements, or if the number of vertices in each buffer does not match the vertex count, or if there is a byte size mismatch of any kind.</exception>
    public static IReadOnlyDataBuffer CreatedInterleavedVertexBuffer(VertexLayout vertexLayout, ReadOnlySpan<IReadOnlyDataBuffer> data, MemoryAllocatorStrategy allocatorStrategy = MemoryAllocatorStrategy.Default, bool validateData = true)
    {
      /** Validate if buffers are individual vertex attributes **/
      if (validateData)
        GraphicsHelper.ValidateInterleavedData(vertexLayout, data);

      int vertexCount = data[0].Length;
      int totalSizeInBytes = BufferHelper.SizeOf(data!);
      int vertexStride = vertexLayout.VertexStride;

      /** Copy Data **/

      DataBuffer<byte> interleavedData = DataBuffer.Create<byte>(totalSizeInBytes, allocatorStrategy);
      using (MemoryHandle interleavedHandle = interleavedData.Pin())
      {
        // For each attribute buffer, write each attribute in it's proper place then restart and write the next, etc
        int vertexOffset = 0;
        for (int vertAttribIndex = 0; vertAttribIndex < data.Length; vertAttribIndex++)
        {
          IReadOnlyDataBuffer db = data[vertAttribIndex];
          int vertAttribSize = db.ElementSizeInBytes;

          // Write each individual vertex attribute for each vertex
          IntPtr dstPtr = interleavedHandle.AsIntPointer(vertexOffset);
          using (MemoryHandle dbHandle = db.Pin())
          {
            IntPtr srcPtr = dbHandle.AsIntPointer();
            for (int i = 0; i < vertexCount; i++)
            {
              BufferHelper.CopyMemory(dstPtr, srcPtr, vertAttribSize);

              srcPtr += vertAttribSize;
              dstPtr += vertexStride;
            }
          }

          vertexOffset += vertAttribSize;
        }
      }

      return interleavedData;
    }

    /// <summary>
    /// Writes interleaved vertex data to a D3D11 Buffer.
    /// </summary>
    /// <param name="nativeBuffer">The native D3D11 Buffer.</param>
    /// <param name="context">The D3D11 device context</param>
    /// <param name="vertexCount">The number of vertices in the buffer.</param>
    /// <param name="vertexLayout">The vertex layout defining the contents of the buffer.</param>
    /// <param name="resourceUsage">The resource usage of the buffer.</param>
    /// <param name="data">Span of buffers representing the vertex data.</param>
    /// <param name="validateData">True to validate the data, false to just do the copy.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if data is null.</exception>
    /// <exception cref="System.ArgumentOutOfRangeException">Thrown if the number of buffers do not match the number
    /// of vertex elements, or if the number of vertices in each buffer does not match the vertex count, or if there is a byte size mismatch of any kind.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if attempting to write to an immutable resource.</exception>
    public static void WriteInterleavedVertexData(D3D11.Buffer nativeBuffer, D3D11.DeviceContext context, int vertexCount, VertexLayout vertexLayout, ResourceUsage resourceUsage, ReadOnlySpan<IReadOnlyDataBuffer> data, bool validateData = true)
    {
      /** Validate **/
      if (validateData)
      {
        Direct3DHelper.CheckIfImmutable(resourceUsage);
        GraphicsHelper.ValidateInterleavedData(vertexLayout, data);
      }

      int totalSizeInBytes = BufferHelper.SizeOf(data!);
      int vertexStride = vertexLayout.VertexStride;

      /** Copy Data **/

      lock (s_sync)
      {
        //If dynamic buffer, just map it, and always doing a write discard
        if (resourceUsage == ResourceUsage.Dynamic)
        {
          try
          {
            SDX.DataBox dataBox = context.MapSubresource(nativeBuffer, 0, D3D11.MapMode.WriteDiscard, D3D11.MapFlags.None);

            // For each attribute buffer, write each attribute in it's proper place then restart and write the next, etc
            int vertexOffset = 0;
            for (int vertAttribIndex = 0; vertAttribIndex < data.Length; vertAttribIndex++)
            {
              IReadOnlyDataBuffer db = data[vertAttribIndex];
              int vertAttribSize = db.ElementSizeInBytes;

              // Write each individual vertex attribute for each vertex
              IntPtr dstPtr = dataBox.DataPointer + vertexOffset;
              using (MemoryHandle dbHandle = db.Pin())
              {
                IntPtr srcPtr = dbHandle.AsIntPointer();
                for (int i = 0; i < vertexCount; i++)
                {
                  BufferHelper.CopyMemory(dstPtr, srcPtr, vertAttribSize);

                  srcPtr += vertAttribSize;
                  dstPtr += vertexStride;
                }
              }

              vertexOffset += vertAttribSize;
            }
          }
          finally
          {
            context.UnmapSubresource(nativeBuffer, 0);
          }
          //Otherwise need to create a staging resource, accumulate each vertex into it then copy to the buffer
        }
        else
        {
          //Create staging resource that is the size of the vertex buffer
          using (D3D11.Resource staging = CreateStaging(context.Device, nativeBuffer))
          {
            try
            {
              SDX.DataBox dataBox = context.MapSubresource(staging, 0, D3D11.MapMode.Write, D3D11.MapFlags.None);

              // For each attribute buffer, write each attribute in it's proper place then restart and write the next, etc
              int vertexOffset = 0;
              for (int vertAttribIndex = 0; vertAttribIndex < data.Length; vertAttribIndex++)
              {
                IReadOnlyDataBuffer db = data[vertAttribIndex];
                int vertAttribSize = db.ElementSizeInBytes;

                // Write each individual vertex attribute for each vertex
                IntPtr dstPtr = dataBox.DataPointer + vertexOffset;
                using (MemoryHandle dbHandle = db.Pin())
                {
                  IntPtr srcPtr = dbHandle.AsIntPointer();
                  for (int i = 0; i < vertexCount; i++)
                  {
                    BufferHelper.CopyMemory(dstPtr, srcPtr, vertAttribSize);

                    srcPtr += vertAttribSize;
                    dstPtr += vertexStride;
                  }
                }

                vertexOffset += vertAttribSize;
              }
            }
            finally
            {
              context.UnmapSubresource(staging, 0);
            }

            //Copy entire staging resource to buffer
            context.CopyResource(staging, nativeBuffer);
          }
        }
      }
    }

    /// <summary>
    /// Reads interleaved vertex data from a D3D11 Buffer.
    /// </summary>
    /// <param name="nativeBuffer">The native D3D11 Buffer.</param>
    /// <param name="context">The D3D11 device context</param>
    /// <param name="vertexCount">The number of vertices in the buffer.</param>
    /// <param name="vertexLayout">The vertex layout defining the contents of the buffer.</param>
    /// <param name="resourceUsage">The resource usage of the buffer.</param>
    /// <param name="data">Span of buffers representing the vertex data.</param>
    /// <param name="validateData">True to validate the data, false to just do the copy.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if data is null.</exception>
    /// <exception cref="System.ArgumentOutOfRangeException">Thrown if the number of buffers do not match the number
    /// of vertex elements, or if the number of vertices in each buffer does not match the vertex count, or if there is a byte size mismatch of any kind.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if attempting to write to an immutable resource.</exception>
    public static void ReadInterleavedVertexData(D3D11.Buffer nativeBuffer, D3D11.DeviceContext context, int vertexCount, VertexLayout vertexLayout, ResourceUsage resourceUsage, ReadOnlySpan<IReadOnlyDataBuffer> data, bool validateData = true)
    {
      /** Validate **/
      if (validateData)
      {
        Direct3DHelper.CheckIfImmutable(resourceUsage);
        GraphicsHelper.ValidateInterleavedData(vertexLayout, data);
      }

      int totalSizeInBytes = BufferHelper.SizeOf(data!);
      int vertexStride = vertexLayout.VertexStride;

      /** Copy Data **/

      lock (s_sync)
      {
        //Reading - always need a staging buffer the size of the vertex buffer
        using (D3D11.Resource staging = CreateStaging(context.Device, nativeBuffer))
        {
          //Copy entire buffer to the staging resource
          context.CopyResource(nativeBuffer, staging);

          try
          {
            SDX.DataBox dataBox = context.MapSubresource(staging, 0, D3D11.MapMode.Read, D3D11.MapFlags.None);

            // For each attribute buffer, copy the part of the vertex into the individual buffer, then restart at the beginning for the next buffer, etc
            int vertexOffset = 0;
            for (int vertAttribIndex = 0; vertAttribIndex < data.Length; vertAttribIndex++)
            {
              IReadOnlyDataBuffer db = data[vertAttribIndex];
              int vertAttribSize = db.ElementSizeInBytes;

              // Read each vertex's specific attribute
              IntPtr srcPtr = dataBox.DataPointer + vertexOffset;
              using (MemoryHandle dbHandle = db.Pin())
              {
                IntPtr dstPtr = dbHandle.AsIntPointer();
                for (int i = 0; i < vertexCount; i++)
                {
                  BufferHelper.CopyMemory(dstPtr, srcPtr, vertAttribSize);

                  dstPtr += vertAttribSize;
                  srcPtr += vertexStride;
                }
              }

              vertexOffset += vertAttribSize;
            }
          }
          finally
          {
            context.UnmapSubresource(staging, 0);
          }
        }
      }
    }

    /// <summary>
    /// Writes vertex data to a D3D11 Buffer.
    /// </summary>
    /// <typeparam name="T">Type of data to copy</typeparam>
    /// <param name="nativeBuffer">The native D3D11 Buffer.</param>
    /// <param name="context">The D3D11 device context</param>
    /// <param name="vertexCount">The number of vertices in the buffer.</param>
    /// <param name="vertexLayout">The vertex layout defining the contents of the buffer.</param>
    /// <param name="resourceUsage">The resource usage of the buffer.</param>
    /// <param name="data">The buffer to copy data to/from.</param>
    /// <param name="offsetInBytes">Offset from the start of the native buffer at which to start copying from</param>
    /// <param name="vertexStride">Vertex step size to advance in the GPU buffer when iterating over individual vertices/vertex attributes, usually zero or the stride specified in the vertex layout.</param>
    /// <param name="writeOptions">Write options for writing.</param>
    /// <exception cref="ArgumentNullException">Thrown if the buffer is null or length zero.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if any of the count or offsets are out of range, or if the vertex stride is too small, or would cause an overflow in the copy operation.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if attempting to write to an immutable resource.</exception>
    public static unsafe void WriteVertexData<T>(D3D11.Buffer nativeBuffer, D3D11.DeviceContext context, int vertexCount, VertexLayout vertexLayout, ResourceUsage resourceUsage, ReadOnlySpan<T> data, int offsetInBytes, int vertexStride, DataWriteOptions writeOptions) where T : unmanaged
    {
      /** Validate **/

      Direct3DHelper.CheckIfImmutable(resourceUsage);
      Direct3DHelper.CheckSpanEmpty<T>(data);

      int bufferSizeInBytes = vertexCount * vertexLayout.VertexStride;
      int elemSizeInBytes = sizeof(T);
      int elementCount = data.Length;
      int dataSizeInBytes = elementCount * elemSizeInBytes;

      // Step to iterate over vertices, if zero, the step is the size of a vertex
      int vertexStep = (vertexStride == 0) ? vertexLayout.VertexStride : vertexStride;

      if (vertexStride != 0)
      {
        int remainingVertexStride = vertexStride - elemSizeInBytes;

        if (remainingVertexStride < 0)
          throw new ArgumentOutOfRangeException(nameof(vertexStride), StringLocalizer.Instance.GetLocalizedString("VertexStrideTooSmall"));

        // Calculate adjustment to the total data size to copy from the VB, since we may be only requesting individual attributes of a vertex,
        // the actual amount of bytes we copy from/to the buffer will be greater
        if (elementCount > 1)
          dataSizeInBytes = ((elementCount - 1) * remainingVertexStride) + dataSizeInBytes;
      }

      //Prevent overflow out of range errors
      if (offsetInBytes < 0 || offsetInBytes > bufferSizeInBytes)
        throw new ArgumentOutOfRangeException(nameof(offsetInBytes), StringLocalizer.Instance.GetLocalizedString("ByteOffsetOutOfRange"));

      if ((offsetInBytes + dataSizeInBytes) > bufferSizeInBytes)
        throw new ArgumentOutOfRangeException(nameof(data), StringLocalizer.Instance.GetLocalizedString("ByteOffsetAndCountOverflow"));

      /** Copy data **/

      D3D11.ResourceRegion region = new D3D11.ResourceRegion(offsetInBytes, 0, 0, offsetInBytes + dataSizeInBytes, 1, 1);

      lock (s_sync)
      {
        //Map directly to dynamic resources
        if (resourceUsage == ResourceUsage.Dynamic)
        {
          try
          {
            SDX.DataBox dataBox = context.MapSubresource(nativeBuffer, 0, Direct3DHelper.ToD3DMapMode(writeOptions), D3D11.MapFlags.None);

            // If vertex step is the size of a vertex, we can copy in bulk
            if (vertexStep == vertexLayout.VertexStride)
            {
              data.CopyTo(dataBox.DataPointer + offsetInBytes, dataSizeInBytes);
            }
            //Otherwise need to go element by element, incrementing by the vertex step
            else
            {
              IntPtr dstPtr = dataBox.DataPointer + offsetInBytes;
              fixed (byte* dataPtr = data.AsBytes())
              {
                IntPtr srcPtr = new IntPtr(dataPtr);
                for (int i = 0; i < elementCount; i++)
                {
                  BufferHelper.CopyMemory(dstPtr, srcPtr, elemSizeInBytes);

                  dstPtr += vertexStep;
                  srcPtr += elemSizeInBytes;
                }
              }
            }
          }
          finally
          {
            context.UnmapSubresource(nativeBuffer, 0);
          }
        }
        else
        {
          // If vertex step is the size of a vertex, we can copy in bulk
          if (vertexStep == vertexLayout.VertexStride)
          {
            fixed (T* dbPtr = data)
            {
              context.UpdateSubresource(new SDX.DataBox(new IntPtr(dbPtr), dataSizeInBytes, 0), nativeBuffer, 0, region);
            }
          }
          //Otherwise need to create a staging resource, copy to it, write each element, then copy back
          else
          {
            //Create a staging resource that is only the length o the buffer that we're modifying
            using (D3D11.Resource staging = CreateStaging(context.Device, nativeBuffer, region))
            {
              //Need to first copy the affected vertices, so we don't have any gaps when we copy back
              context.CopySubresourceRegion(nativeBuffer, 0, region, staging, 0);

              try
              {
                SDX.DataBox dataBox = context.MapSubresource(staging, 0, D3D11.MapMode.Write, D3D11.MapFlags.None);

                // Need to go element by element
                IntPtr dstPtr = dataBox.DataPointer;
                fixed (byte* dataPtr = data.AsBytes())
                {
                  IntPtr srcPtr = new IntPtr(dataPtr);
                  for (int i = 0; i < elementCount; i++)
                  {
                    BufferHelper.CopyMemory(dstPtr, srcPtr, elemSizeInBytes);

                    dstPtr += vertexStep;
                    srcPtr += elemSizeInBytes;
                  }
                }

                //And copy back to the buffer at the end
                context.CopySubresourceRegion(staging, 0, null, nativeBuffer, 0, offsetInBytes, 0, 0);
              }
              finally
              {
                context.UnmapSubresource(staging, 0);
              }
            }
          }
        }
      }
    }

    /// <summary>
    /// Read vertex data from a D3D11 Buffer.
    /// </summary>
    /// <typeparam name="T">Type of data to copy</typeparam>
    /// <param name="nativeBuffer">The native D3D11 Buffer.</param>
    /// <param name="context">The D3D11 device context</param>
    /// <param name="vertexCount">The number of vertices in the buffer.</param>
    /// <param name="vertexLayout">The vertex layout defining the contents of the buffer.</param>
    /// <param name="resourceUsage">The resource usage of the buffer.</param>
    /// <param name="data">The buffer to copy data to/from.</param>
    /// <param name="offsetInBytes">Offset from the start of the native buffer at which to start copying from</param>
    /// <param name="vertexStride">Vertex step size to advance in the GPU buffer when iterating over individual vertices/vertex attributes, usually zero or the stride specified in the vertex layout.</param>
    /// <exception cref="ArgumentNullException">Thrown if the buffer is null or length zero.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if any of the count or offsets are out of range, or if the vertex stride is too small, or would cause an overflow in the copy operation.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if attempting to write to an immutable resource.</exception>
    public static unsafe void ReadVertexData<T>(D3D11.Buffer nativeBuffer, D3D11.DeviceContext context, int vertexCount, VertexLayout vertexLayout, ResourceUsage resourceUsage, Span<T> data, int offsetInBytes, int vertexStride) where T : unmanaged
    {
      /** Validate **/

      Direct3DHelper.CheckSpanEmpty<T>(data);

      int bufferSizeInBytes = vertexCount * vertexLayout.VertexStride;
      int elemSizeInBytes = sizeof(T);
      int elementCount = data.Length;
      int dataSizeInBytes = elementCount * elemSizeInBytes;

      // Step to iterate over vertices, if zero, the step is the size of a vertex
      int vertexStep = (vertexStride == 0) ? vertexLayout.VertexStride : vertexStride;

      if (vertexStride != 0)
      {
        int remainingVertexStride = vertexStride - elemSizeInBytes;

        if (remainingVertexStride < 0)
          throw new ArgumentOutOfRangeException(nameof(vertexStride), StringLocalizer.Instance.GetLocalizedString("VertexStrideTooSmall"));

        // Calculate adjustment to the total data size to copy from the VB, since we may be only requesting individual attributes of a vertex
        // the actual amount of bytes we copy from/to the buffer will be greater
        if (elementCount > 1)
          dataSizeInBytes = ((elementCount - 1) * remainingVertexStride) + dataSizeInBytes;
      }

      //Prevent overflow out of range errors
      if (offsetInBytes < 0 || offsetInBytes > bufferSizeInBytes)
        throw new ArgumentOutOfRangeException(nameof(offsetInBytes), StringLocalizer.Instance.GetLocalizedString("ByteOffsetOutOfRange"));

      if ((offsetInBytes + dataSizeInBytes) > bufferSizeInBytes)
        throw new ArgumentOutOfRangeException(nameof(data), StringLocalizer.Instance.GetLocalizedString("ByteOffsetAndCountOverflow"));

      /** Copy data **/

      D3D11.ResourceRegion region = new D3D11.ResourceRegion(offsetInBytes, 0, 0, offsetInBytes + dataSizeInBytes, 1, 1);

      lock (s_sync)
      {
        //Reading data will always mean creating the staging resource -- create a resource the size of the data that we're reading
        using (D3D11.Resource staging = CreateStaging(context.Device, nativeBuffer, region))
        {
          context.CopySubresourceRegion(nativeBuffer, 0, region, staging, 0);

          try
          {
            SDX.DataBox dataBox = context.MapSubresource(staging, 0, D3D11.MapMode.Read, D3D11.MapFlags.None);

            // If vertex step is the size of a vertex, we can copy in bulk
            if (vertexStep == vertexLayout.VertexStride)
            {
              data.CopyFrom(dataBox.DataPointer, dataSizeInBytes);
            }
            //Otherwise need to go element by element to pull out individual vertex attributes
            else
            {
              IntPtr srcPtr = dataBox.DataPointer;
              fixed (byte* dataPtr = data.AsBytes())
              {
                IntPtr dstPtr = new IntPtr(dataPtr);
                for (int i = 0; i < elementCount; i++)
                {
                  BufferHelper.CopyMemory(dstPtr, srcPtr, elemSizeInBytes);

                  srcPtr += vertexStep;
                  dstPtr += elemSizeInBytes;
                }
              }
            }
          }
          finally
          {
            context.UnmapSubresource(staging, 0);
          }
        }
      }
    }

    /// <summary>
    /// Write data to a D3D11 Buffer.
    /// </summary>
    /// <typeparam name="T">Type of data to copy</typeparam>
    /// <param name="nativeBuffer">The native D3D11 Buffer.</param>
    /// <param name="context">The D3D11 device context</param>
    /// <param name="bufferSizeInBytes">The buffer size in bytes.</param>
    /// <param name="resourceUsage">The resource usage of the buffer.</param>
    /// <param name="data">The buffer to copy data to/from.</param>
    /// <param name="offsetInBytes">Offset from the start of the native buffer at which to start copying from</param>
    /// <param name="writeOptions">Write options for writing.</param>
    /// <exception cref="ArgumentNullException">Thrown if the buffer is null or length zero.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if any of the count or offsets are out of range, or would cause an overflow in the copy operation.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if attempting to write to an immutable resource.</exception>
    public static unsafe void WriteBufferData<T>(D3D11.Buffer nativeBuffer, D3D11.DeviceContext context, int bufferSizeInBytes, ResourceUsage resourceUsage, ReadOnlySpan<T> data, int offsetInBytes, DataWriteOptions writeOptions) where T : unmanaged
    {
      /** Validate **/

      Direct3DHelper.CheckIfImmutable(resourceUsage);
      Direct3DHelper.CheckSpanEmpty<T>(data);

      int elemSizeInBytes = sizeof(T);
      int dataSizeInBytes = data.Length * elemSizeInBytes;

      //Prevent overflow out of range errors
      if (offsetInBytes < 0 || offsetInBytes > bufferSizeInBytes)
        throw new ArgumentOutOfRangeException(nameof(offsetInBytes), StringLocalizer.Instance.GetLocalizedString("ByteOffsetOutOfRange"));

      if ((offsetInBytes + dataSizeInBytes) > bufferSizeInBytes)
        throw new ArgumentOutOfRangeException(nameof(data), StringLocalizer.Instance.GetLocalizedString("ByteOffsetAndCountOverflow"));

      /** Copy data **/

      lock (s_sync)
      {
        //Writing means either mapping the dynamic resource or using update subresource.
        if (resourceUsage == ResourceUsage.Dynamic)
        {
          try
          {
            SDX.DataBox dataBox = context.MapSubresource(nativeBuffer, 0, Direct3DHelper.ToD3DMapMode(writeOptions), D3D11.MapFlags.None);
            data.CopyTo(dataBox.DataPointer + offsetInBytes, dataSizeInBytes);
          }
          finally
          {
            context.UnmapSubresource(nativeBuffer, 0);
          }
        }
        else
        {
          D3D11.ResourceRegion region = new D3D11.ResourceRegion(offsetInBytes, 0, 0, offsetInBytes + dataSizeInBytes, 1, 1);
          fixed (T* dbPtr = data)
            context.UpdateSubresource(new SDX.DataBox(new IntPtr(dbPtr), dataSizeInBytes, 0), nativeBuffer, 0, region);
        }
      }
    }

    /// <summary>
    /// Read data from a D3D11 Buffer.
    /// </summary>
    /// <typeparam name="T">Type of data to copy</typeparam>
    /// <param name="nativeBuffer">The native D3D11 Buffer.</param>
    /// <param name="context">The D3D11 device context</param>
    /// <param name="bufferSizeInBytes">The buffer size in bytes.</param>
    /// <param name="resourceUsage">The resource usage of the buffer.</param>
    /// <param name="data">The buffer to copy data to/from.</param>
    /// <param name="startIndex">The start index to read/write to in the data buffer.</param>
    /// <param name="elementCount">The number of elements to copy.</param>
    /// <param name="offsetInBytes">Offset from the start of the native buffer at which to start copying from</param>
    /// <exception cref="ArgumentNullException">Thrown if the buffer is null or length zero.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if any of the count or offsets are out of range, or would cause an overflow in the copy operation.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if attempting to write to an immutable resource.</exception>
    public static unsafe void ReadBufferData<T>(D3D11.Buffer nativeBuffer, D3D11.DeviceContext context, int bufferSizeInBytes, ResourceUsage resourceUsage, Span<T> data, int offsetInBytes) where T : unmanaged
    {
      /** Validate **/
      Direct3DHelper.CheckSpanEmpty<T>(data);

      int elemSizeInBytes = sizeof(T);
      int dataSizeInBytes = data.Length * elemSizeInBytes;

      //Prevent overflow out of range errors
      if (offsetInBytes < 0 || offsetInBytes > bufferSizeInBytes)
        throw new ArgumentOutOfRangeException(nameof(offsetInBytes), StringLocalizer.Instance.GetLocalizedString("ByteOffsetOutOfRange"));

      if ((offsetInBytes + dataSizeInBytes) > bufferSizeInBytes)
        throw new ArgumentOutOfRangeException(nameof(data), StringLocalizer.Instance.GetLocalizedString("ByteOffsetAndCountOverflow"));

      /** Copy data **/

      lock (s_sync)
      {
        D3D11.ResourceRegion region = new D3D11.ResourceRegion(offsetInBytes, 0, 0, offsetInBytes + dataSizeInBytes, 1, 1);

        //Reading means creating a staging resource, copying to it, then mapping it -- the staging resource will be the size of the data that we're reading.
        using (D3D11.Resource staging = CreateStaging(context.Device, nativeBuffer, region))
        {
          context.CopySubresourceRegion(nativeBuffer, 0, region, staging, 0);

          try
          {
            SDX.DataBox dataBox = context.MapSubresource(staging, 0, D3D11.MapMode.Read, D3D11.MapFlags.None);
            data.CopyFrom(dataBox.DataPointer, dataSizeInBytes);
          }
          finally
          {
            context.UnmapSubresource(staging, 0);
          }
        }
      }
    }

    /// <summary>
    /// Writes data to a D3D11 Texture resource (1D, 2D, 3D, and array variants).
    /// </summary>
    /// <typeparam name="T">Type of data to copy</typeparam>
    /// <param name="nativeTexture">The D3D11 Texture resource</param>
    /// <param name="context">The D3D11 device context</param>
    /// <param name="width">The width of the texture (first mip level).</param>
    /// <param name="height">The height of the texture (first mip level).</param>
    /// <param name="depth">The depth of the texture (first mip level).</param>
    /// <param name="arrayCount">The number of array slices.</param>
    /// <param name="mipCount">The number of mip levels.</param>
    /// <param name="format">The surface format of the texture.</param>
    /// <param name="resourceUsage">The resource usage of the texture.</param>
    /// <param name="isCubeMap">True if the resource is a cubemap or not.</param>
    /// <param name="data">The buffer to copy data to/from.</param>
    /// <param name="subIndex">Index into the subresource that is being written to.</param>
    /// <param name="subimage">The subimage resource region.</param>
    /// <param name="writeOptions">Write options for writing.</param>
    /// <exception cref="ArgumentNullException">Thrown if the buffer is null or length zero.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the mip level/array slice are out of bounds, or if the start index and element count are out of bounds, or if the data type to and texture format msimatch in size,
    /// or if the total number of bytes to copy from the sub resource does not match the actual data size of the region to copy.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if attempting to write to an immutable resource.</exception>
    public static unsafe void WriteTextureData<T>(D3D11.Resource nativeTexture, D3D11.DeviceContext context, int width, int height, int depth, int arrayCount, int mipCount, SurfaceFormat format, ResourceUsage resourceUsage, bool isCubeMap,
        ReadOnlySpan<T> data, SubResourceAt subIndex, ResourceRegion3D? subimage, DataWriteOptions writeOptions) where T : unmanaged
    {
      /** Validate **/

      Direct3DHelper.CheckIfImmutable(resourceUsage);

      if (!subIndex.Validate(mipCount, arrayCount, isCubeMap))
        throw new ArgumentOutOfRangeException(nameof(subIndex), StringLocalizer.Instance.GetLocalizedString("SubResourceIndexOutOfRange"));

      //Calc subresource dimensions - Get the dimensions of the mip level to access
      subIndex.CalculateMipLevelDimensions(ref width, ref height, ref depth);

      //Block compressed textures are minimum 4x4, the last few mip levels will be that...even if they have 2x2 and 1x1 texels
      if (format.IsCompressedFormat())
      {
        width = Math.Max(4, width);

        if (nativeTexture.Dimension == D3D11.ResourceDimension.Texture2D)
          height = Math.Max(4, height);

        if (nativeTexture.Dimension == D3D11.ResourceDimension.Texture3D)
          depth = Math.Max(4, depth);
      }

      //Check subimage - Get the image region
      ResourceRegion3D subimageValue;
      if (subimage.HasValue)
      {
        subimageValue = subimage.Value;
        subimageValue.ValidateRegion(ref width, ref height, ref depth);
      }
      else
      {
        subimageValue = new ResourceRegion3D(0, width, 0, height, 0, depth);
      }

      // Check buffer ranges
      int elementCount = data.Length;
      int formatSizeInBytes = format.SizeInBytes();
      int elemSizeInBytes = sizeof(T);
      int dataSizeInBytes = Texture.CalculateRegionSizeInBytes(format, ref width, ref height, depth, out formatSizeInBytes);

      //Check total size - Make sure the actual total size in bytes meets what we're expecting
      if (dataSizeInBytes != (elementCount * elemSizeInBytes))
        throw new ArgumentOutOfRangeException(nameof(data), StringLocalizer.Instance.GetLocalizedString("InvalidSizeOfDataToCopy"));

      Direct3DHelper.CheckFormatSize(formatSizeInBytes, elemSizeInBytes);

      /** Copy data **/

      int subresourceIndex = subIndex.CalculateSubResourceIndex(mipCount);
      int rowStride = width * formatSizeInBytes;
      int depthStride = rowStride * height;
      D3D11.ResourceRegion region = new D3D11.ResourceRegion(subimageValue.Left, subimageValue.Top, subimageValue.Front, subimageValue.Right, subimageValue.Bottom, subimageValue.Back);

      lock (s_sync)
      {
        //Writing means either mapping the dynamic resource or using update subresource.
        if (resourceUsage == ResourceUsage.Dynamic)
        {
          try
          {
            SDX.DataBox dataBox = context.MapSubresource(nativeTexture, subresourceIndex, Direct3DHelper.ToD3DMapMode(writeOptions), D3D11.MapFlags.None);

            IntPtr dstPtr = dataBox.DataPointer + (region.Left * formatSizeInBytes) + (region.Top * dataBox.RowPitch) + (region.Front * dataBox.SlicePitch);

            //If Texture1D, Texture2D, or TextureCube the depth stride is not needed
            double boxDepthStride = (depth == 1) ? dataBox.SlicePitch : depthStride;

            //If same stride, then there is no padding and we can just copy the amount of data in one fell swoop
            if (dataBox.RowPitch == rowStride && boxDepthStride == depthStride)
            {
              data.CopyTo(dstPtr, dataSizeInBytes);
            }
            else
            {
              // Otherwise, go by slice and scanline by scanline. This is always starts at zero (beginning of the supplied buffer),
              // and advances along the data by the dimensions given by the subimage (or full image). The user buffer is expected not to have padding, but
              // the GPU buffer may.
              fixed (T* dbPtr = data)
              {
                IntPtr srcPtr = new IntPtr(dbPtr);

                //Iterate for each depth
                for (int slice = 0; slice < depth; slice++)
                {
                  //Start with a pointer that points to the start of the slice
                  IntPtr dPtr = dstPtr;

                  //And iterate + copy each line per the height of the image
                  for (int row = 0; row < height; row++)
                  {
                    BufferHelper.CopyMemory(dPtr, srcPtr, rowStride);

                    //Advance the temporary slice pointer and the source pointer
                    srcPtr += rowStride;
                    dPtr += dataBox.RowPitch;
                  }

                  //Adance the destination pointer by the slice pitch to get to the next image
                  dstPtr += dataBox.SlicePitch;
                }
              }
            }
          }
          finally
          {
            context.UnmapSubresource(nativeTexture, subresourceIndex);
          }
        }
        else
        {
          fixed (T* dbPtr = data)
            context.UpdateSubresource(new SDX.DataBox(new IntPtr(dbPtr), rowStride, depthStride), nativeTexture, subresourceIndex, region);
        }
      }
    }

    /// <summary>
    /// Reads data from a D3D11 Texture resource (1D, 2D, 3D, and array variants).
    /// </summary>
    /// <typeparam name="T">Type of data to copy</typeparam>
    /// <param name="nativeTexture">The D3D11 Texture resource</param>
    /// <param name="context">The D3D11 device context</param>
    /// <param name="width">The width of the texture (first mip level).</param>
    /// <param name="height">The height of the texture (first mip level).</param>
    /// <param name="depth">The depth of the texture (first mip level).</param>
    /// <param name="arrayCount">The number of array slices.</param>
    /// <param name="mipCount">The number of mip levels.</param>
    /// <param name="format">The surface format of the texture.</param>
    /// <param name="resourceUsage">The resource usage of the texture.</param>
    /// <param name="isCubeMap">True if the resource is a cubemap or not.</param>
    /// <param name="data">The buffer to copy data to/from.</param>
    /// <param name="subIndex">Index into the subresource that is being written to.</param>
    /// <param name="subimage">The subimage resource region.</param>
    /// <exception cref="ArgumentNullException">Thrown if the buffer is null or length zero.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the mip level/array slice are out of bounds, or if the start index and element count are out of bounds, or if the data type to and texture format msimatch in size,
    /// or if the total number of bytes to copy from the sub resource does not match the actual data size of the region to copy.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if attempting to write to an immutable resource.</exception>
    public static unsafe void ReadTextureData<T>(D3D11.Resource nativeTexture, D3D11.DeviceContext context, int width, int height, int depth, int arrayCount, int mipCount, SurfaceFormat format, ResourceUsage resourceUsage, bool isCubeMap,
        Span<T> data, SubResourceAt subIndex, ResourceRegion3D? subimage) where T : unmanaged
    {
      /** Validate **/


      if (!subIndex.Validate(mipCount, arrayCount, isCubeMap))
        throw new ArgumentOutOfRangeException(nameof(subIndex), StringLocalizer.Instance.GetLocalizedString("SubResourceIndexOutOfRange"));

      //Calc subresource dimensions - Get the dimensions of the mip level to access
      subIndex.CalculateMipLevelDimensions(ref width, ref height, ref depth);

      //Block compressed textures are minimum 4x4, the last few mip levels will be that...even if they have 2x2 and 1x1 texels
      if (format.IsCompressedFormat())
      {
        width = Math.Max(4, width);

        if (nativeTexture.Dimension == D3D11.ResourceDimension.Texture2D)
          height = Math.Max(4, height);

        if (nativeTexture.Dimension == D3D11.ResourceDimension.Texture3D)
          depth = Math.Max(4, depth);
      }

      //Check subimage - Get the image region
      ResourceRegion3D subimageValue;
      if (subimage.HasValue)
      {
        subimageValue = subimage.Value;
        subimageValue.ValidateRegion(ref width, ref height, ref depth);
      }
      else
      {
        subimageValue = new ResourceRegion3D(0, width, 0, height, 0, depth);
      }

      // Check buffer ranges
      int elementCount = data.Length;
      int formatSizeInBytes = format.SizeInBytes();
      int elemSizeInBytes = sizeof(T);
      int dataSizeInBytes = Texture.CalculateRegionSizeInBytes(format, ref width, ref height, depth, out formatSizeInBytes);

      //Check total size - Make sure the actual total size in bytes meets what we're expecting
      if (dataSizeInBytes != (elementCount * elemSizeInBytes))
        throw new ArgumentOutOfRangeException(nameof(data), StringLocalizer.Instance.GetLocalizedString("InvalidSizeOfDataToCopy"));

      Direct3DHelper.CheckFormatSize(formatSizeInBytes, elemSizeInBytes);

      /** Copy data **/

      int subresourceIndex = subIndex.CalculateSubResourceIndex(mipCount);
      int rowStride = width * formatSizeInBytes;
      int depthStride = rowStride * height;
      D3D11.ResourceRegion region = new D3D11.ResourceRegion(subimageValue.Left, subimageValue.Top, subimageValue.Front, subimageValue.Right, subimageValue.Bottom, subimageValue.Back);

      lock (s_sync)
      {
        //Reading means creating a staging resource, copying to it, then mapping it. Only create a minimum single surface that represents the region we're copying from.
        using (D3D11.Resource staging = CreateStaging(context.Device, nativeTexture, region))
        {
          context.CopySubresourceRegion(nativeTexture, subresourceIndex, region, staging, 0);

          try
          {
            SDX.DataBox dataBox = context.MapSubresource(staging, 0, D3D11.MapMode.Read, D3D11.MapFlags.None);

            IntPtr srcPtr = dataBox.DataPointer;

            //If Texture1D, Texture2D, or TextureCube the depth stride is not needed
            double boxDepthStride = (depth == 1) ? dataBox.SlicePitch : depthStride;

            //If same stride, then there is no padding and we can just copy the amount of data in one fell swoop
            if (dataBox.RowPitch == rowStride && boxDepthStride == depthStride)
            {
              data.CopyFrom<T>(srcPtr, dataSizeInBytes);
            }
            else
            {
              // Otherwise, go by slice and scanline by scanline. This is always starts at zero (beginning of the staging buffer / output buffer),
              // and advances along the data by the dimensions given by the subimage (or full image). The user buffer is expected not to have padding, but
              // the GPU buffer may.
              fixed (T* dbPtr = data)
              {
                IntPtr dstPtr = new IntPtr(dbPtr);

                //Iterate for each depth
                for (int slice = 0; slice < depth; slice++)
                {
                  //Start with a pointer that points to the start of the slice
                  IntPtr sPtr = srcPtr;

                  //And iterate + copy each line per the height of the image
                  for (int row = 0; row < height; row++)
                  {
                    BufferHelper.CopyMemory(dstPtr, sPtr, rowStride);

                    //Advance the temporary slice pointer and the destination pointer
                    sPtr += dataBox.RowPitch;
                    dstPtr += rowStride;
                  }

                  //Advance the src pointer by the slice pitch to get to the next image
                  srcPtr += dataBox.SlicePitch;
                }
              }
            }
          }
          finally
          {
            context.UnmapSubresource(staging, subresourceIndex);
          }
        }
      }
    }

    public static D3D11.Resource CreateStaging(D3D11.Device device, D3D11.Resource resource, D3D11.ResourceRegion? subRegion = null)
    {
      switch (resource.Dimension)
      {
        case D3D11.ResourceDimension.Buffer:
          {
            D3D11.Buffer buffer = (resource as D3D11.Buffer)!;
            D3D11.BufferDescription desc = buffer.Description;
            return CreateStaging(device, desc, subRegion);
          }
        case D3D11.ResourceDimension.Texture1D:
          {
            D3D11.Texture1D texture = (resource as D3D11.Texture1D)!;
            D3D11.Texture1DDescription desc = texture.Description;
            return CreateStaging(device, desc, subRegion);
          }
        case D3D11.ResourceDimension.Texture2D:
          {
            D3D11.Texture2D texture = (resource as D3D11.Texture2D)!;
            D3D11.Texture2DDescription desc = texture.Description;
            return CreateStaging(device, desc, subRegion);
          }
        case D3D11.ResourceDimension.Texture3D:
          {
            D3D11.Texture3D texture = (resource as D3D11.Texture3D)!;
            D3D11.Texture3DDescription desc = texture.Description;
            return CreateStaging(device, desc, subRegion);
          }
        default:
          throw new TeslaGraphicsException("Unknown resource for staging resource creation"); //Shouldn't happen
      }
    }

    public static D3D11.Buffer CreateStaging(D3D11.Device device, in D3D11.BufferDescription bufferDesc, D3D11.ResourceRegion? subRegion = null)
    {
      D3D11.BufferDescription desc = new D3D11.BufferDescription();
      desc.BindFlags = D3D11.BindFlags.None;
      desc.CpuAccessFlags = D3D11.CpuAccessFlags.Read | D3D11.CpuAccessFlags.Write;
      desc.OptionFlags = D3D11.ResourceOptionFlags.None;

      if (subRegion is null)
      {
        desc.SizeInBytes = bufferDesc.SizeInBytes;
      }
      else
      {
        D3D11.ResourceRegion region = subRegion.Value;

        desc.SizeInBytes = CalculateWidth(region);
      }

      desc.Usage = D3D11.ResourceUsage.Staging;

      return new D3D11.Buffer(device, desc);
    }

    public static D3D11.Texture1D CreateStaging(D3D11.Device device, in D3D11.Texture1DDescription texDesc, D3D11.ResourceRegion? subRegion = null)
    {
      D3D11.Texture1DDescription desc = new D3D11.Texture1DDescription();
      desc.ArraySize = texDesc.ArraySize;
      desc.MipLevels = texDesc.MipLevels;

      if (subRegion is null)
      {
        desc.Width = texDesc.Width;
      }
      else
      {
        D3D11.ResourceRegion region = subRegion.Value;

        desc.Width = CalculateWidth(region);

        //Set mip and array to 1 if we have a sub region
        desc.ArraySize = 1;
        desc.MipLevels = 1;
      }

      desc.Usage = D3D11.ResourceUsage.Staging;
      desc.Format = texDesc.Format;
      desc.BindFlags = D3D11.BindFlags.None;
      desc.CpuAccessFlags = D3D11.CpuAccessFlags.Read | D3D11.CpuAccessFlags.Write;
      desc.OptionFlags = D3D11.ResourceOptionFlags.None;

      return new D3D11.Texture1D(device, desc);
    }

    public static D3D11.Texture2D CreateStaging(D3D11.Device device, in D3D11.Texture2DDescription texDesc, D3D11.ResourceRegion? subRegion = null)
    {
      D3D11.Texture2DDescription desc = new D3D11.Texture2DDescription();
      desc.ArraySize = texDesc.ArraySize;
      desc.MipLevels = texDesc.MipLevels;
      desc.OptionFlags = texDesc.OptionFlags;

      if (subRegion is null)
      {
        desc.Width = texDesc.Width;
        desc.Height = texDesc.Height;
      }
      else
      {
        D3D11.ResourceRegion region = subRegion.Value;

        desc.Width = CalculateWidth(region);
        desc.Height = CalculateHeight(region);

        //Set mip and array to 1 if we have a sub region
        desc.ArraySize = 1;
        desc.MipLevels = 1;

        //if a subregion, we're not creating the entire resource...so if a cube map, we'll crash since we're just creating one face
        desc.OptionFlags &= ~D3D11.ResourceOptionFlags.TextureCube;
      }

      desc.Format = texDesc.Format;
      desc.Usage = D3D11.ResourceUsage.Staging;
      desc.BindFlags = D3D11.BindFlags.None;
      desc.CpuAccessFlags = D3D11.CpuAccessFlags.Read | D3D11.CpuAccessFlags.Write;
      desc.SampleDescription = new SDX.DXGI.SampleDescription(1, 0);

      return new D3D11.Texture2D(device, desc);
    }

    public static D3D11.Texture3D CreateStaging(D3D11.Device device, in D3D11.Texture3DDescription texDesc, D3D11.ResourceRegion? subRegion = null)
    {
      D3D11.Texture3DDescription desc = new D3D11.Texture3DDescription();
      desc.MipLevels = texDesc.MipLevels;

      if (subRegion is null)
      {
        desc.Width = texDesc.Width;
        desc.Height = texDesc.Height;
        desc.Depth = texDesc.Depth;
      }
      else
      {
        D3D11.ResourceRegion region = subRegion.Value;

        desc.Width = CalculateWidth(region);
        desc.Height = CalculateHeight(region);
        desc.Depth = CalculateDepth(region);

        //Set mip to 1 if we have a sub region
        desc.MipLevels = 1;
      }

      desc.Format = texDesc.Format;
      desc.Usage = D3D11.ResourceUsage.Staging;
      desc.BindFlags = D3D11.BindFlags.None;
      desc.CpuAccessFlags = D3D11.CpuAccessFlags.Read | D3D11.CpuAccessFlags.Write;
      desc.OptionFlags = D3D11.ResourceOptionFlags.None;

      return new D3D11.Texture3D(device, desc);
    }

    private static int CalculateWidth(in D3D11.ResourceRegion subRegion)
    {
      return subRegion.Right - subRegion.Left;
    }

    private static int CalculateHeight(in D3D11.ResourceRegion subRegion)
    {
      return subRegion.Bottom - subRegion.Top;
    }

    private static int CalculateDepth(in D3D11.ResourceRegion subRegion)
    {
      return subRegion.Back - subRegion.Front;
    }

    /// <summary>
    /// Converts handles to <see cref="SDX.DataBox"/>. SharpDX only accepts arrays of exact length and not the new types.
    /// </summary>
    /// <param name="handles">Memory handles.</param>
    /// <returns>Array of databox or null if the length is zero.</returns>
    public static SDX.DataBox[]? AsDataBoxArray(ReadOnlySpan<MemoryHandle> handles)
    {
      if (handles.Length == 0)
        return null;

      SDX.DataBox[] array = new DataBox[handles.Length];
      for (int i = 0; i < handles.Length; i++)
        array[i] = new SDX.DataBox(handles[i].AsIntPointer());

      return array;
    }

    /// <summary>
    /// Converts mapped buffers to <see cref="SDX.DataBox"/>. SharpDX only accepts arrays of exact length and not the new types.
    /// </summary>
    /// <param name="mappedBuffers">Mapped buffers.</param>
    /// <returns>Array of databox or null if the length is zero.</returns>
    public static SDX.DataBox[]? AsDataBoxArray(ReadOnlySpan<MappedBuffer> mappedBuffers)
    {
      if (mappedBuffers.Length == 0)
        return null;

      SDX.DataBox[] array = new DataBox[mappedBuffers.Length];
      for (int i = 0; i < mappedBuffers.Length; i++) 
      {
        MappedBuffer mapped = mappedBuffers[i];
        array[i] = new SDX.DataBox(mapped.Data.AsIntPointer(), mapped.RowPitch, mapped.SlicePitch);
      }

      return array;
    }
  }
}
