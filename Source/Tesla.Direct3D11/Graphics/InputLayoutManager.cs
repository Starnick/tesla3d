﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesla.Graphics;

using D3D11 = SharpDX.Direct3D11;

namespace Tesla.Direct3D11.Graphics
{
  public sealed class InputLayoutManager : IDisposable
  {
    private D3D11.Device m_device;
    private bool m_isDisposed;
    private ConcurrentDictionary<HashedShaderSignature, InputLayoutCache> m_layoutCaches;
    private Func<HashedShaderSignature, InputLayoutCache> m_cacheCreatorFactory;

    public bool IsDisposed
    {
      get
      {
        return m_isDisposed;
      }
    }

    public InputLayoutManager(D3D11.Device device)
    {
      m_device = device;
      m_layoutCaches = new ConcurrentDictionary<HashedShaderSignature, InputLayoutCache>();
      m_cacheCreatorFactory = CacheCreatorFactory;
    }

    public D3D11.InputLayout GetOrCreate(HashedShaderSignature inputSig, int numBuffers, VertexBufferBinding[] vertexBufferSlots)
    {
      if (inputSig.ByteCode is null || numBuffers <= 0 || vertexBufferSlots is null || numBuffers > vertexBufferSlots.Length)
        return null;

      if (m_isDisposed)
        throw new ObjectDisposedException("InputLayoutManager");

      InputLayoutCache cache = m_layoutCaches.GetOrAdd(inputSig, m_cacheCreatorFactory);

      //Take care of trivial case
      if (numBuffers == 1)
      {
        return cache.GetOrCreate(null, vertexBufferSlots[0]);
      }
      else
      {
        return cache.GetOrCreate(null, numBuffers, vertexBufferSlots);
      }
    }

    public D3D11.InputLayout GetOrCreate(HashedShaderSignature inputSig, VertexBufferBinding firstSlotVertexBuffer)
    {
      if (inputSig.ByteCode is null)
        return null;

      if (m_isDisposed)
        throw new ObjectDisposedException("InputLayoutManager");

      InputLayoutCache cache = m_layoutCaches.GetOrAdd(inputSig, m_cacheCreatorFactory);
      return cache.GetOrCreate(null, firstSlotVertexBuffer);
    }

    private InputLayoutCache CacheCreatorFactory(HashedShaderSignature inputSig)
    {
      return new InputLayoutCache(m_device, inputSig);
    }

    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    private void Dispose(bool disposing)
    {
      if (!m_isDisposed)
      {
        if (disposing)
        {
          lock (m_layoutCaches)
          {
            foreach (KeyValuePair<HashedShaderSignature, InputLayoutCache> kv in m_layoutCaches)
            {
              kv.Value.Clear(true);
            }
          }
        }

        m_isDisposed = true;
      }
    }
  }

  public sealed class InputLayoutCache
  {
    private static readonly int SemanticCount = Enum.GetValues(typeof(VertexSemantic)).Length;
    private D3D11.Device m_device;
    private HashedShaderSignature m_shaderSig;
    private ConcurrentDictionary<int, D3D11.InputLayout> m_cachedLayouts;

    public InputLayoutCache(D3D11.Device device, HashedShaderSignature inputSig)
    {
      m_device = device;
      m_shaderSig = inputSig;
      m_cachedLayouts = new ConcurrentDictionary<int, D3D11.InputLayout>();
    }

    /// <summary>
    /// Clears the cache and optionally disposes of contained input layouts. Generally a cache is used as a local cache paired with the <see cref="InputLayoutManager"/>
    /// that serves as a global cache. What's in the local cache will be in the global cache, therefore the global cache "owns" the layouts and should properly dispose of them.
    /// However, since the local cache can be used as stand alone, we need to expose a method to dispose
    /// </summary>
    /// <param name="disposeLayouts"></param>
    public void Clear(bool disposeLayouts)
    {
      if (disposeLayouts)
      {
        lock (m_cachedLayouts)
        {
          foreach (KeyValuePair<int, D3D11.InputLayout> kv in m_cachedLayouts)
          {
            kv.Value.Dispose();
          }

          m_cachedLayouts.Clear();
        }
      }
      else
      {
        m_cachedLayouts.Clear();
      }
    }

    public D3D11.InputLayout GetOrCreate(int numBuffers, params VertexBufferBinding[] vertexBufferSlots)
    {
      return GetOrCreate(null, numBuffers, vertexBufferSlots);
    }

    public D3D11.InputLayout GetOrCreate(VertexBufferBinding vertexBuffer)
    {
      return GetOrCreate(null, vertexBuffer);
    }

    public unsafe D3D11.InputLayout GetOrCreate(InputLayoutManager globalCache, int numBuffers, params VertexBufferBinding[] vertexBufferSlots)
    {
      if (numBuffers <= 0 || vertexBufferSlots is null || vertexBufferSlots.Length == 0 || numBuffers > vertexBufferSlots.Length)
        return null;

      //Take care of trivial case
      if (numBuffers == 1)
        return GetOrCreate(globalCache, vertexBufferSlots[0]);

      int key = GenerateLayoutKey(vertexBufferSlots, numBuffers); //Also ensures [0, numBuffers) are valid
      D3D11.InputLayout layout;

      //Look up if the composite declaration is in the cache
      if (!m_cachedLayouts.TryGetValue(key, out layout))
      {
        bool createdLayout = false;

        //Not found - look up in the global cache if it exists. Or if not, create a new layout to add to the cache.
        if (globalCache is null)
        {
          try
          {
            //For keeping track of current semantic index
            int* semanticIndices = stackalloc int[SemanticCount];

            int numElems = CountVertexElements(vertexBufferSlots, numBuffers);
            if (numElems == 0)
              return null;

            D3D11.InputElement[] elements = new D3D11.InputElement[numElems];
            for (int i = 0, index = 0; i < numBuffers; i++)
            {
              VertexBufferBinding binding = vertexBufferSlots[i];
              VertexLayout decl = binding.VertexBuffer?.VertexLayout;
              int offset = 0; //For every stream, reset offset
              int instanceFrequency = binding.InstanceFrequency;

              if (decl is null)
                throw new ArgumentNullException("vertexBuffer");

              for (int j = 0; j < decl.ElementCount; j++)
              {
                VertexElement vertexElement = decl[j];

                elements[index++] = new D3D11.InputElement
                (
                    Direct3DHelper.ToD3DVertexSemantic(vertexElement.SemanticName),
                    IncrementSemanticIndex(semanticIndices, vertexElement.SemanticName),
                    Direct3DHelper.ToD3DVertexFormat(vertexElement.Format),
                    offset,
                    i, //stream slot
                    (instanceFrequency > 0) ? D3D11.InputClassification.PerInstanceData : D3D11.InputClassification.PerVertexData,
                    instanceFrequency
                );

                offset += vertexElement.Format.SizeInBytes();
              }
            }

            layout = new D3D11.InputLayout(m_device, m_shaderSig.ByteCode, elements);
          }
          catch (Exception e)
          {
            throw new TeslaGraphicsException(StringLocalizer.Instance.GetLocalizedString("InvalidInputLayout"), e);
          }
        }
        else
        {
          layout = globalCache.GetOrCreate(m_shaderSig, numBuffers, vertexBufferSlots);
        }

        if (layout is null)
          return null;

        //Now we have our layout, either from the global cache or we just created it. At this point we attempt to add it, but it may be there from
        //another cache operation, so that will fail and we'll return what's already in the cache (and properly dispose of a layout we just created but didn't need).
        if (!m_cachedLayouts.TryAdd(key, layout))
        {
          if (createdLayout)
            layout.Dispose();

          m_cachedLayouts.TryGetValue(key, out layout);
        }
      }

      return layout;
    }

    public D3D11.InputLayout GetOrCreate(InputLayoutManager globalCache, VertexBufferBinding vertexBuffer)
    {
      if (vertexBuffer.VertexBuffer is null)
        throw new ArgumentNullException(nameof(vertexBuffer));

      D3D11.InputLayout layout;
      VertexLayout vertexDecl = vertexBuffer.VertexBuffer.VertexLayout;
      int instanceFrequency = vertexBuffer.InstanceFrequency;
      int key = vertexDecl.GetHashCode();

      //Look up the declaration in the cache
      if (!m_cachedLayouts.TryGetValue(key, out layout))
      {
        bool createdLayout = false;

        //Not found - look up in the global cache if it exists. Or if not, create a new layout to add to the cache.
        if (globalCache is null)
        {
          try
          {
            D3D11.InputElement[] elements = new D3D11.InputElement[vertexDecl.ElementCount];
            for (int i = 0; i < vertexDecl.ElementCount; i++)
            {
              VertexElement vertexElement = vertexDecl[i];

              elements[i] = new D3D11.InputElement
              (
                  Direct3DHelper.ToD3DVertexSemantic(vertexElement.SemanticName),
                  vertexElement.SemanticIndex,
                  Direct3DHelper.ToD3DVertexFormat(vertexElement.Format),
                  vertexElement.Offset,
                  0,
                  (instanceFrequency > 0) ? D3D11.InputClassification.PerInstanceData : D3D11.InputClassification.PerVertexData,
                  instanceFrequency
              );
            }

            layout = new D3D11.InputLayout(m_device, m_shaderSig.ByteCode, elements);
          }
          catch (Exception e)
          {
            throw new TeslaGraphicsException(StringLocalizer.Instance.GetLocalizedString("InvalidInputLayout"), e);
          }
        }
        else
        {
          layout = globalCache.GetOrCreate(m_shaderSig, vertexBuffer);
        }

        //Now we have our layout, either from the global cache or we just created it. At this point we attempt to add it, but it may be there from
        //another cache operation, so that will fail and we'll return what's already in the cache (and properly dispose of a layout we just created but didn't need).
        if (!m_cachedLayouts.TryAdd(key, layout))
        {
          if (createdLayout)
            layout.Dispose();

          m_cachedLayouts.TryGetValue(key, out layout);
        }
      }

      return layout;
    }

    private static int GenerateLayoutKey(VertexBufferBinding[] vBuffers, int numBuffers)
    {
      unchecked
      {
        uint p = 16777619;
        uint hash = 2166136261;

        for (int i = 0; i < numBuffers; i++)
        {
          VertexBufferBinding binding = vBuffers[i];
          VertexBuffer vb = binding.VertexBuffer;

          if (vb is null)
            throw new ArgumentNullException(StringLocalizer.Instance.GetLocalizedString("InputLayoutMustBeContiguous"));

          hash = (hash ^ (uint) vb.VertexLayout.GetHashCode()) * p;
          hash = (hash ^ (uint) binding.InstanceFrequency) * p; //Need to include step rate since thats part of the layout and may change
        }

        hash += hash << 13;
        hash ^= hash >> 7;
        hash += hash << 3;
        hash ^= hash >> 17;
        hash += hash << 5;

        return (int) hash;
      }
    }

    private unsafe int IncrementSemanticIndex(int* semanticCounts, VertexSemantic semantic)
    {
      int index = (int) semantic;
      int currCount = semanticCounts[index];
      semanticCounts[index] = currCount + 1;

      return currCount;
    }

    private static int CountVertexElements(VertexBufferBinding[] vBuffers, int numBuffers)
    {
      int count = 0;
      for (int i = 0; i < numBuffers; i++)
      {
        VertexBuffer vb = vBuffers[i].VertexBuffer;
        if (vb is null)
          continue;

        count += vb.VertexLayout.ElementCount;
      }

      return count;
    }
  }
}
