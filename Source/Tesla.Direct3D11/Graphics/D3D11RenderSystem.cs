﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using Tesla.Direct3D11.Graphics.Implementation;
using Tesla.Graphics;
using Tesla.Graphics.Implementation;
using D3D = SharpDX.Direct3D;
using D3D11 = SharpDX.Direct3D11;
using DXGI = SharpDX.DXGI;

namespace Tesla.Direct3D11.Graphics
{
  /// <summary>
  /// Direct3D11 render system implementation.
  /// </summary>
  public sealed class D3D11RenderSystem : IRenderSystem
  {
    private D3D11.Device m_device;
    private DXGI.Factory m_factory;
    private D3D11GraphicsAdapter m_adapter;
    private D3D11RenderContext m_immediateContext;
    private ImplementationFactoryCollection m_implFactories;
    private PredefinedRenderStateProvider m_prebuiltRenderStates;
    private StandardEffectLibrary m_standardEffectLibrary;
    private InputLayoutManager m_globalInputLayoutCache;
    private SupportedFeatures m_featureSupport;
    private bool m_isDisposed;

    private int m_currentResourceID;
    private int m_currentEffectSortKey;

    /// <inheritdoc />
    public bool IsDisposed
    {
      get
      {
        return m_isDisposed;
      }
    }

    /// <inheritdoc />
    String IEngineService.Name
    {
      get
      {
        return "Direct3D11_RenderSystem";
      }
    }

    /// <inheritdoc />
    public String Platform
    {
      get
      {
        return "Direct3D11";
      }
    }

    /// <inheritdoc />
    public IRenderContext ImmediateContext
    {
      get
      {
        return m_immediateContext;
      }
    }

    /// <inheritdoc />
    public IGraphicsAdapter Adapter
    {
      get
      {
        return m_adapter;
      }
    }

    /// <inheritdoc />
    public SupportedFeatures FeatureSupport
    {
      get
      {
        return m_featureSupport;
      }
    }

    /// <inheritdoc />
    public IPredefinedBlendStateProvider PredefinedBlendStates
    {
      get
      {
        return m_prebuiltRenderStates;
      }
    }

    /// <inheritdoc />
    public IPredefinedDepthStencilStateProvider PredefinedDepthStencilStates
    {
      get
      {
        return m_prebuiltRenderStates;
      }
    }

    /// <inheritdoc />
    public IPredefinedRasterizerStateProvider PredefinedRasterizerStates
    {
      get
      {
        return m_prebuiltRenderStates;
      }
    }

    /// <inheritdoc />
    public IPredefinedSamplerStateProvider PredefinedSamplerStates
    {
      get
      {
        return m_prebuiltRenderStates;
      }
    }

    public IRenderStateCache RenderStateCache
    {
      get
      {
        return m_prebuiltRenderStates;
      }
    }

    /// <inheritdoc />
    public StandardEffectLibrary StandardEffects
    {
      get
      {
        return m_standardEffectLibrary;
      }
    }

    /// <inheritdoc />
    public InputLayoutManager GlobalInputLayoutManager
    {
      get
      {
        return m_globalInputLayoutCache;
      }
    }

    /// <summary>
    /// Gets the native D3D11 Device.
    /// </summary>
    public D3D11.Device D3DDevice
    {
      get
      {
        return m_device;
      }
    }

    /// <summary>
    /// Gets the native DXGI Factory that created the D3D11 device.
    /// </summary>
    public DXGI.Factory DXGIFactory
    {
      get
      {
        return m_factory;
      }
    }

    /// <summary>
    /// Gets the native D3D11 immediate context.
    /// </summary>
    public D3D11.DeviceContext D3DImmediateContext
    {
      get
      {
        return m_device.ImmediateContext;
      }
    }

    public D3D11RenderSystem() : this(null, D3D11CreationFlags.None, D3D11FeatureLevel.Level_11_1, false) { }

    public D3D11RenderSystem(bool useReverseDepth) : this(null, D3D11CreationFlags.None, D3D11FeatureLevel.Level_11_1, useReverseDepth) { }

    public D3D11RenderSystem(D3D11CreationFlags creationFlags, bool useReverseDepth = false) : this(null, creationFlags, D3D11FeatureLevel.Level_11_1, useReverseDepth) { }

    public D3D11RenderSystem(D3D11CreationFlags creationFlags, D3D11FeatureLevel featureLevel, bool useReverseDepth = false) : this(null, creationFlags, featureLevel, useReverseDepth) { }

    public D3D11RenderSystem(D3D11GraphicsAdapter adapter) : this(adapter, D3D11CreationFlags.None, D3D11FeatureLevel.Level_11_1) { }

    public D3D11RenderSystem(D3D11GraphicsAdapter adapter, D3D11CreationFlags creationFlags, bool useReverseDepth = false) : this(adapter, creationFlags, D3D11FeatureLevel.Level_11_1, useReverseDepth) { }

    public D3D11RenderSystem(D3D11GraphicsAdapter adapter, D3D11CreationFlags creationFlags, D3D11FeatureLevel featureLevel, bool useReverseDepth = false)
    {
      m_factory = new DXGI.Factory1();

      if (adapter is null)
        adapter = new D3D11GraphicsAdapter(m_factory.GetAdapter(0), 0);

      try
      {
        m_device = new D3D11.Device(m_factory.GetAdapter(adapter.AdapterIndex), (D3D11.DeviceCreationFlags) creationFlags, (D3D.FeatureLevel) featureLevel);
        m_adapter = adapter;
        m_adapter.SetDevice(m_device);
      }
      catch (Exception e)
      {
        Dispose(true);

        EngineLog.LogException(LogLevel.Error, e);
        throw new TeslaGraphicsException(StringLocalizer.Instance.GetLocalizedString("ErrorCreatingDevice"), e);
      }

      m_implFactories = new ImplementationFactoryCollection();
      m_featureSupport = CheckFeatures(m_device);

      m_isDisposed = false;
      m_currentResourceID = 0;
      m_currentEffectSortKey = 0;

      InitializeFactories();

      m_globalInputLayoutCache = new InputLayoutManager(m_device);

      m_prebuiltRenderStates = new PredefinedRenderStateProvider(this, useReverseDepth);
      m_standardEffectLibrary = new StandardEffectLibrary(this);
      m_standardEffectLibrary.LoadProvider(new D3D11EffectByteCodeProvider());

      m_immediateContext = new D3D11RenderContext(this, adapter, m_device.ImmediateContext, true);
    }

    /// <inheritdoc />
    public IDeferredRenderContext CreateDeferredRenderContext()
    {
      if (!m_featureSupport.CommandLists)
        throw new TeslaGraphicsException(StringLocalizer.Instance.GetLocalizedString("DeferredRenderContextNotAavailable"));

      return new D3D11RenderContext(this, m_adapter, new D3D11.DeviceContext(m_device), false);
    }

    /// <inheritdoc />
    public T GetImplementationFactory<T>() where T : IGraphicsResourceImplFactory
    {
      return m_implFactories.GetImplementationFactory<T>();
    }

    /// <inheritdoc />
    public bool TryGetImplementationFactory<T>(out T implementationFactory) where T : IGraphicsResourceImplFactory
    {
      return m_implFactories.TryGetImplementationFactory<T>(out implementationFactory);
    }

    /// <inheritdoc />
    public bool IsSupported<T>() where T : GraphicsResource
    {
      return m_implFactories.IsSupported<T>();
    }

    /// <inheritdoc />
    public void Initialize(Engine engine) { }

    /// <inheritdoc />
    public IEnumerator<IGraphicsResourceImplFactory> GetEnumerator()
    {
      return m_implFactories.GetEnumerator();
    }

    /// <inheritdoc />
    IEnumerator IEnumerable.GetEnumerator()
    {
      return m_implFactories.GetEnumerator();
    }

    /// <inheritdoc />
    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    internal bool AddImplementationFactory<T>(T implFactory) where T : IGraphicsResourceImplFactory
    {
      return m_implFactories.AddImplementationFactory<T>(implFactory);
    }

    internal bool RemoveImplementationFactory<T>(T implFactory) where T : IGraphicsResourceImplFactory
    {
      return m_implFactories.RemoveImplementationFactory<T>(implFactory);
    }

    internal int GetNextUniqueResourceID()
    {
      return Interlocked.Increment(ref m_currentResourceID);
    }

    internal int GetNextUniqueEffectSortKey()
    {
      return Interlocked.Increment(ref m_currentEffectSortKey);
    }

    private void Dispose(bool disposing)
    {
      if (!m_isDisposed)
      {
        if (disposing)
        {
          m_prebuiltRenderStates.Dispose();

          if (m_immediateContext is not null)
          {
            m_immediateContext.Dispose();
            m_immediateContext = null;
          }

          if (m_device is not null)
          {
            m_device.Dispose();
            m_device = null;
          }

          if (m_factory is not null)
          {
            m_factory.Dispose();
            m_factory = null;
          }

          if (m_globalInputLayoutCache is not null)
          {
            m_globalInputLayoutCache.Dispose();
            m_globalInputLayoutCache = null;
          }

          m_isDisposed = true;
        }
      }
    }

    private SupportedFeatures CheckFeatures(D3D11.Device device)
    {
      SupportedFeatures features = new SupportedFeatures();
      device.CheckThreadingSupport(out _, out features.CommandLists);
      features.NoOverwriteOnDynamicShaderBuffers = device.CheckD3D11Feature().MapNoOverwriteOnDynamicBufferSRV;

      return features;
    }

    private void InitializeFactories()
    {
      new D3D11BlendStateImplFactory().Initialize(this);
      new D3D11DepthStencilStateImplFactory().Initialize(this);
      new D3D11RasterizerStateImplFactory().Initialize(this);
      new D3D11SamplerStateImplFactory().Initialize(this);

      new D3D11ShaderBufferImplFactory().Initialize(this);
      new D3D11StructuredShaderBufferImplFactory().Initialize(this);
      new D3D11IndexBufferImplFactory().Initialize(this);
      new D3D11VertexBufferImplFactory().Initialize(this);
      new D3D11StreamOutputImplFactory().Initialize(this);

      new D3D11Texture1DImplFactory().Initialize(this);
      new D3D11Texture1DArrayImplFactory().Initialize(this);
      new D3D11Texture2DImplFactory().Initialize(this);
      new D3D11Texture2DArrayImplFactory().Initialize(this);
      new D3D11Texture3DImplFactory().Initialize(this);
      new D3D11TextureCubeImplFactory().Initialize(this);
      new D3D11TextureCubeArrayImplFactory().Initialize(this);

      new D3D11RenderTarget2DImplFactory().Initialize(this);
      new D3D11RenderTarget2DArrayImplFactory().Initialize(this);
      new D3D11RenderTargetCubeImplFactory().Initialize(this);

      new D3D11OcclusionQueryImplFactory().Initialize(this);
      new D3D11SwapChainImplFactory().Initialize(this);
      new D3D11EffectImplFactory().Initialize(this);
    }

    #region ImplementationFactoryCollection

    private sealed class ImplementationFactoryCollection : IEnumerable<IGraphicsResourceImplFactory>
    {
      private Dictionary<Type, IGraphicsResourceImplFactory> m_graphicResourceTypeToFactory;
      private Dictionary<Type, IGraphicsResourceImplFactory> m_factoryTypeToFactory;

      public ImplementationFactoryCollection()
      {
        m_graphicResourceTypeToFactory = new Dictionary<Type, IGraphicsResourceImplFactory>();
        m_factoryTypeToFactory = new Dictionary<Type, IGraphicsResourceImplFactory>();
      }

      public bool AddImplementationFactory<T>(T implFactory) where T : IGraphicsResourceImplFactory
      {
        if (implFactory is null || m_graphicResourceTypeToFactory.ContainsKey(implFactory.GraphicsResourceType))
          return false;

        m_graphicResourceTypeToFactory.Add(implFactory.GraphicsResourceType, implFactory);
        m_factoryTypeToFactory.Add(typeof(T), implFactory);

        return true;
      }

      public bool RemoveImplementationFactory<T>(T implFactory) where T : IGraphicsResourceImplFactory
      {
        if (implFactory is null || !m_graphicResourceTypeToFactory.ContainsKey(implFactory.GraphicsResourceType))
          return false;

        return m_graphicResourceTypeToFactory.Remove(implFactory.GraphicsResourceType) && m_factoryTypeToFactory.Remove(typeof(T));
      }

      public T GetImplementationFactory<T>() where T : IGraphicsResourceImplFactory
      {
        IGraphicsResourceImplFactory factory;
        if (m_factoryTypeToFactory.TryGetValue(typeof(T), out factory) && factory is T)
          return (T) factory;

        return default(T);
      }

      public bool TryGetImplementationFactory<T>(out T implementationFactory) where T : IGraphicsResourceImplFactory
      {
        implementationFactory = default(T);

        IGraphicsResourceImplFactory factory;
        if (m_factoryTypeToFactory.TryGetValue(typeof(T), out factory) && factory is T)
        {
          implementationFactory = (T) factory;
          return true;
        }

        return false;
      }

      public bool IsSupported<T>() where T : GraphicsResource
      {
        return m_graphicResourceTypeToFactory.ContainsKey(typeof(T));
      }

      public IEnumerator<IGraphicsResourceImplFactory> GetEnumerator()
      {
        return m_factoryTypeToFactory.Values.GetEnumerator();
      }

      IEnumerator IEnumerable.GetEnumerator()
      {
        return m_factoryTypeToFactory.Values.GetEnumerator();
      }
    }

    #endregion
  }
}
