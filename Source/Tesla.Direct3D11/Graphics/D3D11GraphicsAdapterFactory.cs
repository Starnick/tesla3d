﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using DXGI = SharpDX.DXGI;

namespace Tesla.Direct3D11.Graphics
{
  /// <summary>
  /// Factory for creating <see cref="D3D11GraphicsAdapter" /> instances.
  /// </summary>
  public class D3D11GraphicsAdapterFactory : IEnumerable<D3D11GraphicsAdapter>
  {
    private List<D3D11GraphicsAdapter> m_adapters;

    /// <summary>
    /// Gets the default adapter.
    /// </summary>
    public D3D11GraphicsAdapter DefaultAdapter
    {
      get
      {
        return m_adapters[0];
      }
    }

    /// <summary>
    /// Gets the number of adapters.
    /// </summary>
    public int Count
    {
      get
      {
        return m_adapters.Count;
      }
    }

    /// <summary>
    /// Gets the adapter at the specified index.
    /// </summary>
    /// <param name="index">Zero-based index of the adapter.</param>
    /// <returns>The adapter.</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the index is out of range.</exception>
    public D3D11GraphicsAdapter this[int index]
    {
      get
      {
        if (index < 0 || index >= m_adapters.Count)
          throw new ArgumentOutOfRangeException("index");

        return m_adapters[index];
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="D3D11GraphicsAdapterFactory"/> class.
    /// </summary>
    public D3D11GraphicsAdapterFactory()
    {
      DXGI.Factory factory = new DXGI.Factory1();
      DXGI.Adapter[] adapters = factory.Adapters;
      m_adapters = new List<D3D11GraphicsAdapter>();
      foreach (DXGI.Adapter adapter in adapters)
      {
        m_adapters.Add(new D3D11GraphicsAdapter(adapter, m_adapters.Count));
      }
    }

    /// <summary>
    /// Returns an enumerator that iterates through the collection.
    /// </summary>
    /// <returns>A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.</returns>
    public IEnumerator<D3D11GraphicsAdapter> GetEnumerator()
    {
      return m_adapters.GetEnumerator();
    }

    /// <summary>
    /// Returns an enumerator that iterates through a collection.
    /// </summary>
    /// <returns>An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.</returns>
    IEnumerator IEnumerable.GetEnumerator()
    {
      return m_adapters.GetEnumerator();
    }
  }
}
