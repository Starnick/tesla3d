﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Graphics;
using D3D11 = SharpDX.Direct3D11;

namespace Tesla.Direct3D11.Graphics
{
  /// <summary>
  /// A D3D11 Shader resource that specifically holds shader resource views.
  /// </summary>
  public sealed class D3D11ShaderResourceWrapper : IShaderResource, ID3D11ShaderResourceView
  {
    private D3D11.Device m_device;
    private D3D11.ShaderResourceView m_shaderResourceView;
    private ShaderResourceType m_resourceType;
    private ShaderStageBinding m_boundStages;
    private bool m_owned;
    private bool m_isDisposed;
    private string m_name;
    private string m_debugName;

    /// <inheritdoc />
    public bool IsDisposed
    {
      get
      {
        return m_isDisposed;
      }
    }

    /// <inheritdoc />
    public string Name
    {
      get
      {
        return m_name;
      }
      set
      {
        m_name = value;
      }
    }

    /// <inheritdoc />
    public string DebugName
    {
      get
      {
        return m_debugName;
      }
      set
      {
        m_debugName = value;

        if (m_shaderResourceView is not null)
          m_shaderResourceView.DebugName = (string.IsNullOrEmpty(value)) ? value : value + "_SRV";
      }
    }

    /// <inheritdoc />
    public ShaderResourceType ResourceType
    {
      get
      {
        return m_resourceType;
      }
    }

    /// <inheritdoc />
    public ShaderStageBinding BoundStages
    {
      get
      {
        return m_boundStages;
      }
      set
      {
        m_boundStages = value;
      }
    }

    /// <summary>
    /// Gets the native D3D11 device.
    /// </summary>
    public D3D11.Device D3DDevice
    {
      get
      {
        return m_device;
      }
    }

    /// <summary>
    /// Gets the native D3D11 shader resource view.
    /// </summary>
    public D3D11.ShaderResourceView D3DShaderResourceView
    {
      get
      {
        return m_shaderResourceView;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="D3D11ShaderResourceWrapper"/> class.
    /// </summary>
    /// <param name="shaderResourceView">The shader resource view to manage.</param>
    public D3D11ShaderResourceWrapper(D3D11.ShaderResourceView shaderResourceView) : this(shaderResourceView, true) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="D3D11ShaderResourceWrapper"/> class.
    /// </summary>
    /// <param name="shaderResourceView">The shader resource view to manage.</param>
    /// <param name="isOwned">True if this holder owns the resource, otherwise false. If true then when dispose is called, it will clean the resource up.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the resource view is null.</exception>
    public D3D11ShaderResourceWrapper(D3D11.ShaderResourceView shaderResourceView, bool isOwned)
    {
      ArgumentNullException.ThrowIfNull(shaderResourceView, nameof(shaderResourceView));

      m_device = shaderResourceView.Device;
      m_resourceType = Direct3DHelper.FromD3DShaderResourceViewDimension(shaderResourceView.Description.Dimension);
      m_owned = isOwned;
      m_isDisposed = false;

      m_name = String.Empty;
      m_debugName = shaderResourceView.DebugName;
    }

    /// <inheritdoc />
    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    private void Dispose(bool disposing)
    {
      if (!m_isDisposed)
      {
        if (disposing)
        {
          if (m_owned)
          {
            m_shaderResourceView.Dispose();
            m_shaderResourceView = null;
          }
        }

        m_isDisposed = true;
      }
    }
  }
}
