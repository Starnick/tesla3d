﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Threading;
using Tesla.Graphics;
using D3D = SharpDX.Direct3D;
using D3D11 = SharpDX.Direct3D11;
using DXGI = SharpDX.DXGI;

namespace Tesla.Direct3D11.Graphics
{
  public sealed class D3D11DepthStencilBufferWrapper : IDepthStencilBuffer, ID3D11DepthStencilView, ID3D11ShaderResourceView
  {
    private D3D11.Device m_device;
    private D3D11.DepthStencilView m_depthStencilView;
    private D3D11.DepthStencilView m_readOnlyDepthStencilView;
    private D3D11.ShaderResourceView m_shaderResourceView;
    private D3D11.Resource m_nativeTexture;
    private string m_name;
    private ShaderResourceType m_resourceType;
    private ShaderStageBinding m_boundStages;
    private DepthFormat m_depthFormat;
    private int m_width;
    private int m_height;
    private int m_arrayCount;
    private bool m_isArrayResource;
    private bool m_isCubeResource;
    private MSAADescription m_msaaDescription;
    private RenderTargetUsage m_targetUsage;
    private bool m_isReadable;
    private bool m_isDisposed;
    private bool m_ownsTexture;
    private D3D11DepthStencilBufferWrapper m_parentBuffer;
    private int m_subResourceIndex;
    private int m_refCount = 0;
    private bool m_optimizeForSingleSurface;

    private D3D11DepthStencilBufferWrapper[] m_subBuffers;

    /// <inheritdoc />
    public bool IsDisposed
    {
      get
      {
        return m_isDisposed;
      }
    }

    /// <inheritdoc />
    public ShaderResourceType ResourceType
    {
      get
      {
        return m_resourceType;
      }
    }

    /// <inheritdoc />
    public ShaderStageBinding BoundStages
    {
      get
      {
        return m_boundStages;
      }
      set
      {
        m_boundStages = value;
      }
    }

    /// <inheritdoc />
    public string Name
    {
      get
      {
        return m_name;
      }
      set
      {
        m_name = value;
      }
    }

    /// <inheritdoc />
    public string DebugName
    {
      get
      {
        return m_nativeTexture.DebugName;
      }
      set
      {
        if (m_ownsTexture && m_nativeTexture is not null)
          m_nativeTexture.DebugName = value;

        if (m_depthStencilView is not null)
          m_depthStencilView.DebugName = value ?? string.Format("{0}_DSV", value);

        if (m_readOnlyDepthStencilView is not null)
          m_readOnlyDepthStencilView.DebugName = value ?? string.Format("{0}_DSV [READONLY]", value);

        if (m_shaderResourceView is not null)
          m_shaderResourceView.DebugName = value ?? string.Format("{0}_SRV", value);
      }
    }

    /// <inheritdoc />
    public bool IsReadable
    {
      get
      {
        return m_isReadable;
      }
    }

    public bool IsShareable
    {
      get
      {
        return true;
      }
    }

    public DepthFormat DepthStencilFormat
    {
      get
      {
        return m_depthFormat;
      }
    }

    public int Width
    {
      get
      {
        return m_width;
      }
    }

    public int Height
    {
      get
      {
        return m_height;
      }
    }

    public int ArrayCount
    {
      get
      {
        return m_arrayCount;
      }
    }

    public bool IsArrayResource
    {
      get
      {
        return m_isArrayResource;
      }
    }

    public bool IsCubeResource
    {
      get
      {
        return m_isCubeResource;
      }
    }

    /// <summary>
    /// Gets if the resource is a sub-resource, representing an individual array slice if the main resource is an array resource or an
    /// individual face if its a cube resource. If this is a sub resource, then its sub resource index indicates the position in the array/cube.
    /// </summary>
    public bool IsSubResource
    {
      get
      {
        return m_subResourceIndex != -1;
      }
    }

    /// <summary>
    /// Gets the array index if the resource is a sub resource in an array. If not, then the index is -1.
    /// </summary>
    public int SubResourceIndex
    {
      get
      {
        return m_subResourceIndex;
      }
    }

    public MSAADescription MultisampleDescription
    {
      get
      {
        return m_msaaDescription;
      }
    }

    public RenderTargetUsage TargetUsage
    {
      get
      {
        return m_targetUsage;
      }
    }

    public D3D11.Device D3DDevice
    {
      get
      {
        return m_device;
      }
    }

    public D3D11.DepthStencilView D3DDepthStencilView
    {
      get
      {
        return m_depthStencilView;
      }
    }

    public D3D11.DepthStencilView D3DReadOnlyDepthStencilView
    {
      get
      {
        return m_readOnlyDepthStencilView;
      }
    }

    public D3D11.ShaderResourceView D3DShaderResourceView
    {
      get
      {
        return m_shaderResourceView;
      }
    }

    public D3D11.Resource D3DTexture
    {
      get
      {
        return m_nativeTexture;
      }
    }

    public D3D11DepthStencilBufferWrapper Parent
    {
      get
      {
        return m_parentBuffer;
      }
    }

    public bool IsOptimizedForSingleSurface
    {
      get
      {
        return m_optimizeForSingleSurface;
      }
    }

    public D3D11DepthStencilBufferWrapper(D3D11.Device device, int width, DepthFormat depthFormat, int arrayCount, bool createShaderView, bool optimizeForSingleSurface, MSAADescription msaaDesc, RenderTargetUsage targetUsage)
    {
      if (device is null)
        throw new ArgumentNullException("device");

      m_device = device;
      m_isDisposed = false;
      m_ownsTexture = true;
      m_optimizeForSingleSurface = optimizeForSingleSurface;
      m_isReadable = createShaderView;
      m_msaaDescription = msaaDesc;
      m_targetUsage = targetUsage;
      m_isCubeResource = false;
      m_depthFormat = depthFormat;
      m_parentBuffer = null;
      m_subResourceIndex = -1;

      Initialize1DDepthBuffer(width, arrayCount);

      Name = String.Empty;
    }

    public D3D11DepthStencilBufferWrapper(D3D11.Device device, int width, int height, bool isCube, DepthFormat depthFormat, int arrayCount, bool createShaderView, bool optimizeForSingleSurface, MSAADescription msaaDesc, RenderTargetUsage targetUsage)
    {
      if (device is null)
        throw new ArgumentNullException("device");

      m_device = device;
      m_isDisposed = false;
      m_ownsTexture = true;
      m_optimizeForSingleSurface = optimizeForSingleSurface;
      m_isReadable = createShaderView;
      m_msaaDescription = msaaDesc;
      m_targetUsage = targetUsage;
      m_isCubeResource = isCube;
      m_depthFormat = depthFormat;
      m_parentBuffer = null;
      m_subResourceIndex = -1;

      Initialize2DDepthBuffer(width, height, arrayCount);

      Name = String.Empty;
    }

    public D3D11DepthStencilBufferWrapper(D3D11.Device device, int width, int height, int depth, DepthFormat depthFormat, bool createShaderView, bool optimizeForSingleSurface, MSAADescription msaaDesc, RenderTargetUsage targetUsage)
    {
      if (device is null)
        throw new ArgumentNullException("device");

      m_device = device;
      m_isDisposed = false;
      m_ownsTexture = true;
      m_optimizeForSingleSurface = optimizeForSingleSurface;
      m_isReadable = createShaderView;
      m_msaaDescription = new MSAADescription(1, 0, msaaDesc.ResolveShaderResource);
      m_targetUsage = targetUsage;
      m_isCubeResource = false;
      m_isArrayResource = true;
      m_depthFormat = depthFormat;
      m_parentBuffer = null;
      m_subResourceIndex = -1;

      Initialize3DDepthBuffer(width, height, depth);

      Name = String.Empty;
    }

    private D3D11DepthStencilBufferWrapper(D3D11DepthStencilBufferWrapper parentBuffer, int arraySlice)
    {
      m_device = parentBuffer.D3DDevice;
      m_isDisposed = false;
      m_ownsTexture = false;
      m_optimizeForSingleSurface = false;
      m_isReadable = parentBuffer.m_isReadable;
      m_msaaDescription = parentBuffer.m_msaaDescription;
      m_targetUsage = parentBuffer.m_targetUsage;
      m_arrayCount = parentBuffer.m_arrayCount;
      m_width = parentBuffer.m_width;
      m_height = parentBuffer.m_height;
      m_isArrayResource = parentBuffer.m_isArrayResource;
      m_isCubeResource = parentBuffer.m_isCubeResource;
      m_nativeTexture = parentBuffer.m_nativeTexture;
      m_resourceType = parentBuffer.m_resourceType;
      m_depthFormat = parentBuffer.m_depthFormat;
      m_parentBuffer = parentBuffer;
      m_subResourceIndex = arraySlice;

      if (m_nativeTexture is D3D11.Texture1D)
      {
        Initialize1DDepthBufferViewsOnly(arraySlice);
      }
      else if (m_nativeTexture is D3D11.Texture2D || m_nativeTexture is D3D11.Texture3D)
      {
        Initialize2DDepthBufferViewsOnly(arraySlice);
      }

      //Adjust for cube arrays
      if (m_isCubeResource && m_isArrayResource)
        m_subResourceIndex /= 6;

      Name = parentBuffer.m_name + "[" + GetSubName() + "]";
      DebugName = Name;
    }

    public D3D11DepthStencilBufferWrapper GetSubBuffer(int subResourceIndex)
    {
      //Either array or cube and has to not be sub -- or if we're optimized, then we'll only ever have one surface
      if ((!m_isArrayResource && !m_isCubeResource && IsSubResource) || m_optimizeForSingleSurface)
        return null;

      int totalSubResources = m_arrayCount;
      int actualArraySlice = subResourceIndex;

      //Account for cube arrays, and if not an array but a cube map, we still have the 6 faces.
      if (m_isCubeResource && m_isArrayResource)
      {
        actualArraySlice *= 6;
      }
      else if (m_isCubeResource)
      {
        totalSubResources *= 6;
      }

      if (m_subBuffers is null)
        m_subBuffers = new D3D11DepthStencilBufferWrapper[totalSubResources];

      if (subResourceIndex < 0 || subResourceIndex >= m_subBuffers.Length)
        return null;

      D3D11DepthStencilBufferWrapper subBuffer = m_subBuffers[subResourceIndex];
      if (subBuffer is null)
      {
        subBuffer = new D3D11DepthStencilBufferWrapper(this, actualArraySlice);
        m_subBuffers[subResourceIndex] = subBuffer;
      }

      return subBuffer;
    }

    public void Clear(D3D11.DeviceContext deviceContext, ClearOptions options, float depth, int stencil)
    {
      if (options == ClearOptions.Target)
        return;

      D3D11.DepthStencilClearFlags clearFlags;
      bool clearDepth = false;
      bool clearStencil = false;

      if ((options & ClearOptions.Depth) == ClearOptions.Depth)
        clearDepth = true;

      if ((options & ClearOptions.Stencil) == ClearOptions.Stencil)
        clearStencil = true;

      if (clearDepth && clearStencil)
      {
        clearFlags = D3D11.DepthStencilClearFlags.Depth | D3D11.DepthStencilClearFlags.Stencil;
      }
      else if (clearDepth)
      {
        clearFlags = D3D11.DepthStencilClearFlags.Depth;
      }
      else
      {
        clearFlags = D3D11.DepthStencilClearFlags.Stencil;
      }

      deviceContext.ClearDepthStencilView(m_depthStencilView, clearFlags, depth, (byte) stencil);
    }

    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    internal void AddRef()
    {
      Interlocked.Increment(ref m_refCount);
    }

    internal void Release()
    {
      Interlocked.Decrement(ref m_refCount);

      if (m_refCount == 0)
        Dispose();
    }

    public void MakeOptimizedToFull()
    {
      if (m_optimizeForSingleSurface)
      {
        m_optimizeForSingleSurface = false;

        if (m_depthStencilView is not null)
        {
          m_depthStencilView.Dispose();
          m_depthStencilView = null;
        }

        if (m_readOnlyDepthStencilView is not null)
        {
          m_readOnlyDepthStencilView.Dispose();
          m_readOnlyDepthStencilView = null;
        }

        if (m_shaderResourceView is not null)
        {
          m_shaderResourceView.Dispose();
          m_shaderResourceView = null;
        }

        if (m_nativeTexture is not null)
        {
          m_nativeTexture.Dispose();
          m_nativeTexture = null;
        }

        switch (m_resourceType)
        {
          case ShaderResourceType.Texture3D:
            Initialize3DDepthBuffer(m_width, m_height, m_arrayCount);
            break;
          case ShaderResourceType.TextureCubeMSArray:
          case ShaderResourceType.TextureCubeMS:
          case ShaderResourceType.TextureCubeArray:
          case ShaderResourceType.TextureCube:
          case ShaderResourceType.Texture2DMSArray:
          case ShaderResourceType.Texture2DArray:
          case ShaderResourceType.Texture2DMS:
          case ShaderResourceType.Texture2D:
            Initialize2DDepthBuffer(m_width, m_height, m_arrayCount);
            break;
          case ShaderResourceType.Texture1DArray:
          case ShaderResourceType.Texture1D:
            Initialize1DDepthBuffer(m_width, m_arrayCount);
            break;
        }
      }
    }

    private string GetSubName()
    {
      if (m_isCubeResource)
      {
        if (m_isArrayResource)
        {
          //Cube arrays in multiples of 6, so all we can do is get the "array index" of the cube
          int cubeIndex = m_subResourceIndex / 6;
          return cubeIndex.ToString();
        }
        else
        {
          return "Face: " + ((CubeMapFace) m_subResourceIndex).ToString();
        }
      }
      else
      {
        return m_subResourceIndex.ToString();
      }
    }

    #region 1D Depth Buffer Initialization

    private void Initialize1DDepthBuffer(int width, int arrayCount)
    {
      //Set fields
      m_arrayCount = arrayCount;
      m_width = width;
      m_height = 1;
      m_isArrayResource = arrayCount > 1;
      m_resourceType = (m_isArrayResource) ? ShaderResourceType.Texture1DArray : ShaderResourceType.Texture1D;

      //Setup texture resource

      D3D11.Texture1DDescription texDesc = new D3D11.Texture1DDescription();
      texDesc.Width = width;
      texDesc.MipLevels = 1;
      texDesc.CpuAccessFlags = D3D11.CpuAccessFlags.None;
      texDesc.OptionFlags = D3D11.ResourceOptionFlags.None;
      texDesc.Usage = D3D11.ResourceUsage.Default;

      if (!m_optimizeForSingleSurface)
      {
        texDesc.ArraySize = arrayCount;
      }
      else
      {
        texDesc.ArraySize = 1;
      }

      if (m_isReadable)
      {
        texDesc.BindFlags = D3D11.BindFlags.DepthStencil | D3D11.BindFlags.ShaderResource;
        texDesc.Format = Direct3DHelper.ToD3DTextureFormatFromDepthFormat(m_depthFormat);
      }
      else
      {
        texDesc.BindFlags = D3D11.BindFlags.DepthStencil;
        texDesc.Format = Direct3DHelper.ToD3DDepthFormat(m_depthFormat);
      }

      m_nativeTexture = new D3D11.Texture1D(m_device, texDesc);

      Initialize1DDepthBufferViewsOnly(-1);
    }

    private void Initialize1DDepthBufferViewsOnly(int arraySlice)
    {
      //Setup depth stencil view
      D3D11.DepthStencilViewDescription dsvDesc = new D3D11.DepthStencilViewDescription();
      dsvDesc.Flags = D3D11.DepthStencilViewFlags.None;
      dsvDesc.Format = Direct3DHelper.ToD3DDepthFormat(m_depthFormat);

      //For creating views of sub textures
      int numArraySlices = (arraySlice < 0) ? m_arrayCount : 1;
      arraySlice = (arraySlice < 0) ? 0 : arraySlice;

      if (!m_optimizeForSingleSurface)
      {
        if (m_isArrayResource)
        {
          dsvDesc.Dimension = D3D11.DepthStencilViewDimension.Texture1DArray;
          dsvDesc.Texture1DArray.FirstArraySlice = arraySlice;
          dsvDesc.Texture1DArray.ArraySize = numArraySlices;
          dsvDesc.Texture1DArray.MipSlice = 0;
        }
        else
        {
          dsvDesc.Dimension = D3D11.DepthStencilViewDimension.Texture1D;
          dsvDesc.Texture1D.MipSlice = 0;
        }
      }
      else
      {
        dsvDesc.Dimension = D3D11.DepthStencilViewDimension.Texture1D;
        dsvDesc.Texture1D.MipSlice = 0;
      }

      m_depthStencilView = new D3D11.DepthStencilView(m_device, m_nativeTexture, dsvDesc);

      //Set up shader resource view

      if (m_isReadable)
      {
        D3D11.ShaderResourceViewDescription srvDesc = new D3D11.ShaderResourceViewDescription();
        srvDesc.Format = Direct3DHelper.ToD3DShaderResourceFormatFromDepthFormat(m_depthFormat);

        if (!m_optimizeForSingleSurface)
        {
          if (m_isArrayResource)
          {
            srvDesc.Dimension = D3D.ShaderResourceViewDimension.Texture1DArray;
            srvDesc.Texture1DArray.FirstArraySlice = arraySlice;
            srvDesc.Texture1DArray.ArraySize = numArraySlices;
            srvDesc.Texture1DArray.MipLevels = 1;
            srvDesc.Texture1DArray.MostDetailedMip = 0;
          }
          else
          {
            srvDesc.Dimension = D3D.ShaderResourceViewDimension.Texture1D;
            srvDesc.Texture1D.MipLevels = 1;
            srvDesc.Texture1D.MostDetailedMip = 0;
          }
        }
        else
        {
          srvDesc.Dimension = D3D.ShaderResourceViewDimension.Texture1D;
          srvDesc.Texture1D.MipLevels = 1;
          srvDesc.Texture1D.MostDetailedMip = 0;
        }

        m_shaderResourceView = new D3D11.ShaderResourceView(m_device, m_nativeTexture, srvDesc);
      }
    }

    #endregion

    #region 2D Depth Buffer Initialization

    private void Initialize2DDepthBuffer(int width, int height, int arrayCount)
    {
      //Set fields
      m_arrayCount = arrayCount;
      m_width = width;
      m_height = height;
      m_isArrayResource = arrayCount > 1;

      if (m_msaaDescription.IsMultisampled)
      {
        if (m_isCubeResource)
        {
          m_resourceType = (m_isArrayResource) ? ShaderResourceType.TextureCubeMSArray : ShaderResourceType.TextureCubeMS;
        }
        else
        {
          m_resourceType = (m_isArrayResource) ? ShaderResourceType.Texture2DMSArray : ShaderResourceType.Texture2DMS;
        }
      }
      else
      {
        if (m_isCubeResource)
        {
          m_resourceType = (m_isArrayResource) ? ShaderResourceType.TextureCubeArray : ShaderResourceType.TextureCube;
        }
        else
        {
          m_resourceType = (m_isArrayResource) ? ShaderResourceType.Texture2DArray : ShaderResourceType.Texture2D;
        }
      }

      //Setup texture resource

      D3D11.Texture2DDescription texDesc = new D3D11.Texture2DDescription();
      texDesc.Width = width;
      texDesc.Height = height;
      texDesc.MipLevels = 1;
      texDesc.CpuAccessFlags = D3D11.CpuAccessFlags.None;
      texDesc.Usage = D3D11.ResourceUsage.Default;
      texDesc.SampleDescription = new DXGI.SampleDescription(m_msaaDescription.Count, m_msaaDescription.QualityLevel);

      if (!m_optimizeForSingleSurface)
      {
        texDesc.ArraySize = (m_isCubeResource) ? arrayCount * 6 : arrayCount;
        texDesc.OptionFlags = (m_isCubeResource && !m_msaaDescription.IsMultisampled) ? D3D11.ResourceOptionFlags.TextureCube : D3D11.ResourceOptionFlags.None;
      }
      else
      {
        texDesc.ArraySize = 1;
        texDesc.OptionFlags = D3D11.ResourceOptionFlags.None;
      }

      if (m_isReadable)
      {
        texDesc.BindFlags = D3D11.BindFlags.DepthStencil | D3D11.BindFlags.ShaderResource;
        texDesc.Format = Direct3DHelper.ToD3DTextureFormatFromDepthFormat(m_depthFormat);
      }
      else
      {
        texDesc.BindFlags = D3D11.BindFlags.DepthStencil;
        texDesc.Format = Direct3DHelper.ToD3DDepthFormat(m_depthFormat);
      }

      m_nativeTexture = new D3D11.Texture2D(m_device, texDesc);

      Initialize2DDepthBufferViewsOnly(-1);
    }

    private void Initialize2DDepthBufferViewsOnly(int arraySlice)
    {
      //Setup depth stencil view
      D3D11.DepthStencilViewDescription dsvDesc = new D3D11.DepthStencilViewDescription();
      dsvDesc.Flags = D3D11.DepthStencilViewFlags.None;
      dsvDesc.Format = Direct3DHelper.ToD3DDepthFormat(m_depthFormat);

      //For creating views of sub textures
      int numArraySlices = (arraySlice < 0) ? (m_isCubeResource) ? m_arrayCount * 6 : m_arrayCount : (m_isCubeResource && m_isArrayResource) ? 6 : 1;
      int cubeCount = (arraySlice < 0) ? m_arrayCount : 1; //Only valid with cube resource array
      arraySlice = (arraySlice < 0) ? 0 : arraySlice;

      if (!m_optimizeForSingleSurface)
      {
        if (m_isArrayResource || m_isCubeResource)
        {
          if (m_msaaDescription.IsMultisampled)
          {
            dsvDesc.Dimension = D3D11.DepthStencilViewDimension.Texture2DMultisampledArray;
            dsvDesc.Texture2DMSArray.FirstArraySlice = arraySlice;
            dsvDesc.Texture2DMSArray.ArraySize = numArraySlices;
          }
          else
          {
            dsvDesc.Dimension = D3D11.DepthStencilViewDimension.Texture2DArray;
            dsvDesc.Texture2DArray.FirstArraySlice = arraySlice;
            dsvDesc.Texture2DArray.ArraySize = numArraySlices;
            dsvDesc.Texture2DArray.MipSlice = 0;
          }
        }
        else
        {
          if (m_msaaDescription.IsMultisampled)
          {
            dsvDesc.Dimension = D3D11.DepthStencilViewDimension.Texture2DMultisampled;
          }
          else
          {
            dsvDesc.Dimension = D3D11.DepthStencilViewDimension.Texture2D;
            dsvDesc.Texture2D.MipSlice = 0;
          }
        }
      }
      else
      {
        if (m_msaaDescription.IsMultisampled)
        {
          dsvDesc.Dimension = D3D11.DepthStencilViewDimension.Texture2DMultisampled;
        }
        else
        {
          dsvDesc.Dimension = D3D11.DepthStencilViewDimension.Texture2D;
          dsvDesc.Texture2D.MipSlice = 0;
        }
      }

      m_depthStencilView = new D3D11.DepthStencilView(m_device, m_nativeTexture, dsvDesc);

      //Set up shader resource view

      if (m_isReadable)
      {
        dsvDesc.Flags = SharpDX.Direct3D11.DepthStencilViewFlags.ReadOnlyDepth;

        // If format has no stencil, specifying the RO-stencil flag will cause an error
        switch (m_depthFormat)
        {
          case DepthFormat.Depth32Stencil8:
          case DepthFormat.Depth24Stencil8:
            dsvDesc.Flags |= D3D11.DepthStencilViewFlags.ReadOnlyStencil;
            break;
        }

        m_readOnlyDepthStencilView = new SharpDX.Direct3D11.DepthStencilView(m_device, m_nativeTexture, dsvDesc);

        D3D11.ShaderResourceViewDescription srvDesc = new D3D11.ShaderResourceViewDescription();
        srvDesc.Format = Direct3DHelper.ToD3DShaderResourceFormatFromDepthFormat(m_depthFormat);

        if (!m_optimizeForSingleSurface)
        {
          if (m_isArrayResource)
          {
            if (m_msaaDescription.IsMultisampled)
            {
              srvDesc.Dimension = D3D.ShaderResourceViewDimension.Texture2DMultisampledArray;
              srvDesc.Texture2DMSArray.FirstArraySlice = arraySlice;
              srvDesc.Texture2DMSArray.ArraySize = numArraySlices;
            }
            else
            {
              if (m_isCubeResource)
              {
                srvDesc.Dimension = D3D.ShaderResourceViewDimension.TextureCubeArray;
                srvDesc.TextureCubeArray.CubeCount = cubeCount;
                srvDesc.TextureCubeArray.First2DArrayFace = arraySlice;
                srvDesc.TextureCubeArray.MipLevels = 1;
                srvDesc.TextureCubeArray.MostDetailedMip = 0;
              }
              else
              {
                srvDesc.Dimension = D3D.ShaderResourceViewDimension.Texture2DArray;
                srvDesc.Texture2DArray.FirstArraySlice = arraySlice;
                srvDesc.Texture2DArray.ArraySize = numArraySlices;
                srvDesc.Texture2DArray.MipLevels = 1;
                srvDesc.Texture2DArray.MostDetailedMip = 0;
              }
            }
          }
          else
          {
            if (m_isCubeResource)
            {
              if (m_msaaDescription.IsMultisampled)
              {
                srvDesc.Dimension = D3D.ShaderResourceViewDimension.Texture2DMultisampledArray;
                srvDesc.Texture2DMSArray.FirstArraySlice = arraySlice;
                srvDesc.Texture2DMSArray.ArraySize = numArraySlices;
              }
              else
              {
                srvDesc.Dimension = D3D.ShaderResourceViewDimension.TextureCube;
                srvDesc.TextureCube.MipLevels = 1;
                srvDesc.TextureCube.MostDetailedMip = 0;
              }
            }
            else
            {
              if (m_msaaDescription.IsMultisampled)
              {
                srvDesc.Dimension = D3D.ShaderResourceViewDimension.Texture2DMultisampled;
              }
              else
              {
                srvDesc.Dimension = D3D.ShaderResourceViewDimension.Texture2D;
                srvDesc.Texture2D.MipLevels = 1;
                srvDesc.Texture2D.MostDetailedMip = 0;
              }
            }
          }
        }
        else
        {
          if (m_msaaDescription.IsMultisampled)
          {
            srvDesc.Dimension = D3D.ShaderResourceViewDimension.Texture2DMultisampled;
          }
          else
          {
            srvDesc.Dimension = D3D.ShaderResourceViewDimension.Texture2D;
            srvDesc.Texture2D.MipLevels = 1;
            srvDesc.Texture2D.MostDetailedMip = 0;
          }
        }

        m_shaderResourceView = new D3D11.ShaderResourceView(m_device, m_nativeTexture, srvDesc);
      }
    }

    #endregion

    #region 3D Depth Buffer Initialization

    private void Initialize3DDepthBuffer(int width, int height, int depth)
    {
      //Set fields
      m_arrayCount = depth;
      m_width = width;
      m_height = height;
      m_resourceType = ShaderResourceType.Texture3D;

      //Setup texture resource

      D3D11.Texture2DDescription texDesc = new D3D11.Texture2DDescription();
      texDesc.Width = width;
      texDesc.Height = height;
      texDesc.MipLevels = 1;
      texDesc.CpuAccessFlags = D3D11.CpuAccessFlags.None;
      texDesc.OptionFlags = D3D11.ResourceOptionFlags.None;
      texDesc.Usage = D3D11.ResourceUsage.Default;
      texDesc.SampleDescription = new DXGI.SampleDescription(1, 0);

      if (!m_optimizeForSingleSurface)
      {
        texDesc.ArraySize = depth;
      }
      else
      {
        texDesc.ArraySize = 1;
      }

      if (m_isReadable)
      {
        texDesc.BindFlags = D3D11.BindFlags.DepthStencil | D3D11.BindFlags.ShaderResource;
        texDesc.Format = Direct3DHelper.ToD3DTextureFormatFromDepthFormat(m_depthFormat);
      }
      else
      {
        texDesc.BindFlags = D3D11.BindFlags.DepthStencil;
        texDesc.Format = Direct3DHelper.ToD3DDepthFormat(m_depthFormat);
      }

      m_nativeTexture = new D3D11.Texture2D(m_device, texDesc);

      //Basically created a non-multisampled 2D texture array where array size is the depth of the 3D texture, from here on
      //its the same setup as a 2D texture array
      Initialize2DDepthBufferViewsOnly(-1);
    }

    #endregion

    private void Dispose(bool disposing)
    {
      if (!m_isDisposed)
      {
        if (disposing)
        {
          if (m_depthStencilView is not null)
          {
            m_depthStencilView.Dispose();
            m_depthStencilView = null;
          }

          if (m_readOnlyDepthStencilView is not null)
          {
            m_readOnlyDepthStencilView.Dispose();
            m_readOnlyDepthStencilView = null;
          }

          if (m_shaderResourceView is not null)
          {
            m_shaderResourceView.Dispose();
            m_shaderResourceView = null;
          }

          if (m_ownsTexture && m_nativeTexture is not null)
          {
            m_nativeTexture.Dispose();
            m_nativeTexture = null;
          }

          if (m_subBuffers is not null)
          {
            for (int i = 0; i < m_subBuffers.Length; i++)
            {
              D3D11DepthStencilBufferWrapper subBuffer = m_subBuffers[i];
              if (subBuffer is not null)
                subBuffer.Dispose();
            }

            m_subBuffers = null;
          }
        }

        m_isDisposed = true;
      }
    }
  }
}
