﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using Tesla.Graphics;
using D3D11 = SharpDX.Direct3D11;
using SDXMI = SharpDX.Mathematics.Interop;
using DXGI = SharpDX.DXGI;

namespace Tesla.Direct3D11.Graphics
{
  public class D3D11RenderContext : IRenderContext, IDeferredRenderContext
  {
    private D3D11.DeviceContext m_deviceContext;
    private IRenderSystem m_renderSystem;
    private Dictionary<Type, IRenderContextExtension> m_extensions;
    private D3D11ShaderStage[] m_shaderStages;
    private bool m_isImmediateContext;
    private bool m_isDisposed;

    private EnforcedRenderState m_enforcedRenderState;
    private BlendState m_blendState;
    private RasterizerState m_rasterizerState;
    private DepthStencilState m_depthStencilState;
    private Rectangle m_scissorRectangle;
    private Color m_blendFactor;
    private int m_referenceStencil;
    private int m_blendSampleMask;
    private Camera m_camera;

    private IndexBuffer m_indexBuffer;
    private BufferHelper m_bufferHelper;
    private RenderTargetHelper m_renderTargetHelper;
    private VertexBufferBinding[] m_singleVB;
    private StreamOutputBufferBinding[] m_singleSO;
    private IRenderTarget[] m_singleRT;
    private InputLayoutManager m_inputLayoutManager;
    private InputLayoutCache m_localInputLayoutCache;
    private D3D11.InputLayout m_previousInputLayout;

    public event TypedEventHandler<IRenderContext, EventArgs> Disposing;

    public bool IsDisposed
    {
      get
      {
        return m_isDisposed;
      }
    }

    public IRenderSystem RenderSystem
    {
      get
      {
        return m_renderSystem;
      }
    }

    public bool IsImmediateContext
    {
      get
      {
        return m_isImmediateContext;
      }
    }

    public BlendState BlendState
    {
      get
      {
        return m_blendState;
      }
      set
      {
        //If enforced, filter out
        if ((m_enforcedRenderState & EnforcedRenderState.BlendState) == EnforcedRenderState.BlendState)
          return;

        //If null, set default state
        if (value is null)
          value = m_renderSystem.PredefinedBlendStates.Opaque;

        //If a new state, apply it
        if (!value.IsSameState(m_blendState))
        {
          if (!value.IsBound)
            value.BindRenderState();

          m_blendState = value;
          m_blendFactor = value.BlendFactor;
          m_blendSampleMask = value.MultiSampleMask;
          SDXMI.RawColor4 sdxBf;
          Direct3DHelper.ConvertColor(ref m_blendFactor, out sdxBf);

          ID3D11BlendState nativeState = value.Implementation as ID3D11BlendState;
          m_deviceContext.OutputMerger.SetBlendState(nativeState.D3DBlendState, sdxBf, m_blendSampleMask);
        }
      }
    }

    public RasterizerState RasterizerState
    {
      get
      {
        return m_rasterizerState;
      }
      set
      {
        //If enforced, filter out
        if ((m_enforcedRenderState & EnforcedRenderState.RasterizerState) == EnforcedRenderState.RasterizerState)
          return;

        //If null, set default state
        if (value is null)
          value = m_renderSystem.PredefinedRasterizerStates.CullBackClockwiseFront;

        //If a new state, apply it
        if (!value.IsSameState(m_rasterizerState))
        {
          if (!value.IsBound)
            value.BindRenderState();

          m_rasterizerState = value;

          ID3D11RasterizerState nativeState = value.Implementation as ID3D11RasterizerState;
          m_deviceContext.Rasterizer.State = nativeState.D3DRasterizerState;
        }
      }
    }

    public DepthStencilState DepthStencilState
    {
      get
      {
        return m_depthStencilState;
      }
      set
      {
        //If enforced, filter out
        if ((m_enforcedRenderState & EnforcedRenderState.DepthStencilState) == EnforcedRenderState.DepthStencilState)
          return;

        //If null, set default state
        if (value is null)
          value = m_renderSystem.PredefinedDepthStencilStates.Default;

        //If a new state, apply it
        if (!value.IsSameState(m_depthStencilState))
        {
          if (!value.IsBound)
            value.BindRenderState();

          m_depthStencilState = value;
          m_referenceStencil = m_depthStencilState.ReferenceStencil;

          ID3D11DepthStencilState nativeState = value.Implementation as ID3D11DepthStencilState;
          m_deviceContext.OutputMerger.SetDepthStencilState(nativeState.D3DDepthStencilState, m_referenceStencil);
        }
      }
    }

    public EnforcedRenderState EnforcedRenderState
    {
      get
      {
        return m_enforcedRenderState;
      }
      set
      {
        m_enforcedRenderState = value;
      }
    }

    public Rectangle ScissorRectangle
    {
      get
      {
        return m_scissorRectangle;
      }
      set
      {
        if (value.Equals(m_scissorRectangle))
          return;

        m_scissorRectangle = value;
        m_deviceContext.Rasterizer.SetScissorRectangle(m_scissorRectangle.Left, m_scissorRectangle.Top,
            m_scissorRectangle.Right, m_scissorRectangle.Bottom);
      }
    }

    public Color BlendFactor
    {
      get
      {
        return m_blendFactor;
      }
      set
      {
        if (value.Equals(m_blendFactor))
          return;

        m_blendFactor = value;

        //Applying blend factor means re-applying the entire blend state
        SDXMI.RawColor4 sdxBf;
        Direct3DHelper.ConvertColor(ref m_blendFactor, out sdxBf);
        ID3D11BlendState nativeState = m_blendState.Implementation as ID3D11BlendState;
        m_deviceContext.OutputMerger.SetBlendState(nativeState.D3DBlendState, sdxBf, m_blendSampleMask);
      }
    }

    public int BlendSampleMask
    {
      get
      {
        return m_blendSampleMask;
      }
      set
      {
        if (value == m_blendSampleMask)
          return;

        m_blendSampleMask = value;

        //Applying the sample mask means re-applying the entire blend state
        SDXMI.RawColor4 sdxBf;
        Direct3DHelper.ConvertColor(ref m_blendFactor, out sdxBf);
        ID3D11BlendState nativeState = m_blendState.Implementation as ID3D11BlendState;
        m_deviceContext.OutputMerger.SetBlendState(nativeState.D3DBlendState, sdxBf, m_blendSampleMask);
      }
    }

    public int ReferenceStencil
    {
      get
      {
        return m_referenceStencil;
      }
      set
      {
        if (value == m_referenceStencil)
          return;

        m_referenceStencil = value;

        //Applying the reference stencil means re-applying the entire depth stencil state
        ID3D11DepthStencilState nativeState = m_depthStencilState.Implementation as ID3D11DepthStencilState;
        m_deviceContext.OutputMerger.SetDepthStencilState(nativeState.D3DDepthStencilState, m_referenceStencil);
      }
    }

    public Camera Camera
    {
      get
      {
        return m_camera;
      }
      set
      {
        if (m_camera == value)
          return;

        StopCameraEvents();

        m_camera = value;

        StartCameraEvents();
      }
    }

    public SwapChain BackBuffer
    {
      get
      {
        return m_renderTargetHelper.BackBuffer;
      }
      set
      {
        m_renderTargetHelper.BackBuffer = value;
      }
    }

    public D3D11.Device D3DDevice
    {
      get
      {
        return m_deviceContext.Device;
      }
    }

    public D3D11.DeviceContext D3DDeviceContext
    {
      get
      {
        return m_deviceContext;
      }
    }

    public InputLayoutManager InputLayoutManager
    {
      get
      {
        return m_inputLayoutManager;
      }
    }

    internal D3D11RenderContext(D3D11RenderSystem renderSystem, D3D11GraphicsAdapter adapter, D3D11.DeviceContext deviceContext, bool isImmediate)
    {
      m_renderSystem = renderSystem;
      m_deviceContext = deviceContext;
      m_isImmediateContext = isImmediate;
      Camera = new Camera(); //Create an uninitialized camera
      m_isDisposed = false;

      m_extensions = new Dictionary<Type, IRenderContextExtension>();

      //Initialize shader stage wrappers
      int maxSamplerSlots = D3D11.CommonShaderStage.SamplerSlotCount;
      int maxResourceSlots = D3D11.CommonShaderStage.InputResourceSlotCount;
      int maxConstantBufferSlots = D3D11.CommonShaderStage.ConstantBufferApiSlotCount;

      m_shaderStages = new D3D11ShaderStage[]
      {
                new D3D11ShaderStage(deviceContext.VertexShader, ShaderStage.VertexShader, maxSamplerSlots, maxResourceSlots, maxConstantBufferSlots),
                new D3D11ShaderStage(deviceContext.HullShader, ShaderStage.HullShader, maxSamplerSlots, maxResourceSlots, maxConstantBufferSlots),
                new D3D11ShaderStage(deviceContext.DomainShader, ShaderStage.DomainShader, maxSamplerSlots, maxResourceSlots, maxConstantBufferSlots),
                new D3D11ShaderStage(deviceContext.GeometryShader, ShaderStage.GeometryShader, maxSamplerSlots, maxResourceSlots, maxConstantBufferSlots),
                new D3D11ShaderStage(deviceContext.PixelShader, ShaderStage.PixelShader, maxSamplerSlots, maxResourceSlots, maxConstantBufferSlots),
                new D3D11ShaderStage(deviceContext.ComputeShader, ShaderStage.ComputeShader, maxSamplerSlots, maxResourceSlots, maxConstantBufferSlots)
      };

      m_bufferHelper = new BufferHelper(m_deviceContext, adapter);
      m_renderTargetHelper = new RenderTargetHelper(this, adapter);
      m_singleVB = new VertexBufferBinding[1];
      m_singleSO = new StreamOutputBufferBinding[1];
      m_singleRT = new IRenderTarget[1];
      m_inputLayoutManager = renderSystem.GlobalInputLayoutManager;

      SetDefaultRenderStates();
    }

    public void SetCurrentInputLayoutCache(InputLayoutCache localCache)
    {
      m_localInputLayoutCache = localCache;
    }

    public IShaderStage GetShaderStage(ShaderStage shaderStage)
    {
      return m_shaderStages[(int) shaderStage];
    }

    public IEnumerable<IShaderStage> GetShaderStages()
    {
      return m_shaderStages.Clone() as IShaderStage[];
    }

    public bool IsShaderStageSupported(ShaderStage shaderStage)
    {
      return true;
    }

    public T GetExtension<T>() where T : IRenderContextExtension
    {
      IRenderContextExtension ext;
      if (m_extensions.TryGetValue(typeof(T), out ext))
        return (T) ext;

      return default(T);
    }

    public IEnumerable<IRenderContextExtension> GetExtensions()
    {
      return m_extensions.Values;
    }

    public bool IsExtensionSupported<T>() where T : IRenderContextExtension
    {
      return m_extensions.ContainsKey(typeof(T));
    }

    public ICommandList FinishCommandList(bool restoreDeferredContextState)
    {
      if (m_isImmediateContext)
        throw new TeslaGraphicsException(StringLocalizer.Instance.GetLocalizedString("FinishCommandListOnDeferredContext"));

      D3D11.CommandList cmdList = m_deviceContext.FinishCommandList(restoreDeferredContextState);

      //If not restoring state, then our cached state will be dirty
      if (!restoreDeferredContextState)
      {
        ClearOurState(true);
      }

      return new D3D11CommandListWrapper(cmdList);
    }

    public void ExecuteCommandList(ICommandList commandList, bool preserveImmediateContextState)
    {
      if (!m_isImmediateContext)
        throw new TeslaGraphicsException(StringLocalizer.Instance.GetLocalizedString("ExecuteCommandListOnImmediateContext"));

      ID3D11CommandList cmdList = commandList as ID3D11CommandList;

      if (cmdList is null)
        throw new ArgumentNullException("commandList");

      //If not restoring state, then our cached state will be dirty
      if (!preserveImmediateContextState)
      {
        ClearOurState(true);
      }

      m_deviceContext.ExecuteCommandList(cmdList.D3DCommandList, preserveImmediateContextState);
    }

    public void SetIndexBuffer(IndexBuffer indexBuffer)
    {
      if (Object.ReferenceEquals(m_indexBuffer, indexBuffer))
        return;

      m_indexBuffer = indexBuffer;

      D3D11.Buffer nativeBuffer = null;
      DXGI.Format format = DXGI.Format.R16_UInt;

      if (indexBuffer is not null)
      {
        nativeBuffer = (indexBuffer.Implementation as ID3D11Buffer).D3DBuffer;
        format = Direct3DHelper.ToD3DIndexFormat(indexBuffer.IndexFormat);
      }

      m_deviceContext.InputAssembler.SetIndexBuffer(nativeBuffer, format, 0);
    }

    public void SetVertexBuffer(VertexBufferBinding vertexBuffer)
    {
      if (vertexBuffer.VertexBuffer is not null)
      {
        m_singleVB[0] = vertexBuffer;
        m_bufferHelper.SetVertexBuffers(m_singleVB, 1);
        m_singleVB[0] = new VertexBufferBinding(); //Don't hold onto the object
      }
      else
      {
        m_bufferHelper.SetVertexBuffers(null, 0);
      }
    }

    public void SetVertexBuffers(params VertexBufferBinding[] vertexBuffers)
    {
      int count = (vertexBuffers is not null) ? vertexBuffers.Length : 0;
      m_bufferHelper.SetVertexBuffers(vertexBuffers, count);
    }

    public void SetStreamOutputTarget(StreamOutputBufferBinding streamOutputBuffer)
    {
      if (streamOutputBuffer.StreamOutputBuffer is not null)
      {
        m_singleSO[0] = streamOutputBuffer;
        m_bufferHelper.SetStreamOutputTargets(m_singleSO, 1);
        m_singleSO[0] = new StreamOutputBufferBinding(); //Don't hold onto the object
      }
      else
      {
        m_bufferHelper.SetStreamOutputTargets(null, 0);
      }
    }

    public void SetStreamOutputTargets(params StreamOutputBufferBinding[] streamOutputBuffers)
    {
      int count = (streamOutputBuffers is not null) ? streamOutputBuffers.Length : 0;
      m_bufferHelper.SetStreamOutputTargets(streamOutputBuffers, count);
    }

    public void SetRenderTarget(SetTargetOptions options, IRenderTarget renderTarget)
    {
      if (renderTarget is not null)
      {
        m_singleRT[0] = renderTarget;
        m_renderTargetHelper.SetRenderTargets(options, m_singleRT, 1);
        m_singleRT[0] = null; //Don't hold onto the object
      }
      else
      {
        m_renderTargetHelper.SetRenderTargets(options, null, 0);
      }
    }

    public void SetRenderTargets(SetTargetOptions options, params IRenderTarget[] renderTargets)
    {
      int count = (renderTargets is not null) ? renderTargets.Length : 0;
      m_renderTargetHelper.SetRenderTargets(options, renderTargets, count);
    }

    public IndexBuffer GetIndexBuffer()
    {
      return m_indexBuffer;
    }

    public VertexBufferBinding[] GetVertexBuffers()
    {
      return m_bufferHelper.GetVertexBuffers();
    }

    public StreamOutputBufferBinding[] GetStreamOutputTargets()
    {
      return m_bufferHelper.GetStreamOutputTargets();
    }

    public IRenderTarget[] GetRenderTargets()
    {
      return m_renderTargetHelper.GetRenderTargets();
    }

    public void ClearState()
    {
      m_deviceContext.ClearState();
      ClearOurState(true);
    }

    public void Clear(Color color)
    {
      m_renderTargetHelper.Clear(ClearOptions.All, color, 1.0f, 0);
    }

    public void Clear(ClearOptions options, Color color, float depth, int stencil)
    {
      m_renderTargetHelper.Clear(options, color, depth, stencil);
    }

    public void Draw(PrimitiveType primitiveType, int vertexCount, int startVertexIndex)
    {
#if DEBUG
      if (vertexCount <= 0)
        throw new ArgumentOutOfRangeException("vertexCount", StringLocalizer.Instance.GetLocalizedString("VertexCountMustBeGreaterThanZero"));

      if (startVertexIndex < 0)
        throw new ArgumentOutOfRangeException("startVertexIndex", StringLocalizer.Instance.GetLocalizedString("BufferOffsetsMustBeZeroGreater"));

      if (m_localInputLayoutCache is null)
        throw new TeslaGraphicsException(StringLocalizer.Instance.GetLocalizedString("MustApplyPass"));
#endif

      SetCurrentInputLayout();
      m_deviceContext.InputAssembler.PrimitiveTopology = Direct3DHelper.ToD3DPrimitiveTopology(primitiveType);
      m_deviceContext.Draw(vertexCount, startVertexIndex);
    }

    public void DrawIndexed(PrimitiveType primitiveType, int indexCount, int startIndex, int baseVertexOffset)
    {
#if DEBUG
      if (indexCount <= 0)
        throw new ArgumentOutOfRangeException("indexCount", StringLocalizer.Instance.GetLocalizedString("IndexCountMustBeGreaterThanZero"));

      if (m_indexBuffer is null)
        throw new ArgumentNullException("indexBuffer", StringLocalizer.Instance.GetLocalizedString("IndexBufferRequired"));

      if (baseVertexOffset < 0)
        throw new ArgumentOutOfRangeException("baseVertexOffset", StringLocalizer.Instance.GetLocalizedString("BufferOffsetsMustBeZeroGreater"));

      if (m_localInputLayoutCache is null)
        throw new TeslaGraphicsException(StringLocalizer.Instance.GetLocalizedString("MustApplyPass"));
#endif

      SetCurrentInputLayout();
      m_deviceContext.InputAssembler.PrimitiveTopology = Direct3DHelper.ToD3DPrimitiveTopology(primitiveType);
      m_deviceContext.DrawIndexed(indexCount, startIndex, baseVertexOffset);
    }

    public void DrawIndexedInstanced(PrimitiveType primitiveType, int indexCountPerInstance, int instanceCount, int startIndex, int baseVertexOffset, int startInstanceOffset)
    {
#if DEBUG
      if (indexCountPerInstance <= 0)
        throw new ArgumentOutOfRangeException("indexCountPerInstance", StringLocalizer.Instance.GetLocalizedString("IndexCountMustBeGreaterThanZero"));

      if (instanceCount <= 0)
        throw new ArgumentOutOfRangeException("instanceCount", StringLocalizer.Instance.GetLocalizedString("InstanceCountMustBeGreaterThanZero"));

      if (m_indexBuffer is null)
        throw new ArgumentNullException("indexBuffer", StringLocalizer.Instance.GetLocalizedString("IndexBufferRequired"));

      if (baseVertexOffset < 0 || startIndex < 0 || startInstanceOffset < 0)
        throw new ArgumentOutOfRangeException("offsets", StringLocalizer.Instance.GetLocalizedString("BufferOffsetsMustBeZeroGreater"));

      if (m_localInputLayoutCache is null)
        throw new TeslaGraphicsException(StringLocalizer.Instance.GetLocalizedString("MustApplyPass"));
#endif

      SetCurrentInputLayout();
      m_deviceContext.InputAssembler.PrimitiveTopology = Direct3DHelper.ToD3DPrimitiveTopology(primitiveType);
      m_deviceContext.DrawIndexedInstanced(indexCountPerInstance, instanceCount, startIndex, baseVertexOffset, startInstanceOffset);
    }

    public void DrawInstanced(PrimitiveType primitiveType, int vertexCountPerInstance, int instanceCount, int startVertexIndex, int startInstanceOffset)
    {
#if DEBUG
      if (vertexCountPerInstance <= 0)
        throw new ArgumentOutOfRangeException("vertexCountPerInstance", StringLocalizer.Instance.GetLocalizedString("VertexCountMustBeGreaterThanZero"));

      if (instanceCount <= 0)
        throw new ArgumentOutOfRangeException("instanceCount", StringLocalizer.Instance.GetLocalizedString("InstanceCountMustBeGreaterThanZero"));

      if (startVertexIndex < 0 || startInstanceOffset < 0)
        throw new ArgumentOutOfRangeException("offsets", StringLocalizer.Instance.GetLocalizedString("BufferOffsetsMustBeZeroGreater"));

      if (m_localInputLayoutCache is null)
        throw new TeslaGraphicsException(StringLocalizer.Instance.GetLocalizedString("MustApplyPass"));
#endif

      SetCurrentInputLayout();
      m_deviceContext.InputAssembler.PrimitiveTopology = Direct3DHelper.ToD3DPrimitiveTopology(primitiveType);
      m_deviceContext.DrawInstanced(vertexCountPerInstance, instanceCount, startVertexIndex, startInstanceOffset);
    }

    public void DrawAuto(PrimitiveType primitiveType)
    {
#if DEBUG
      if (m_localInputLayoutCache is null)
        throw new TeslaGraphicsException(StringLocalizer.Instance.GetLocalizedString("MustApplyPass"));
#endif

      SetCurrentInputLayout();
      m_deviceContext.InputAssembler.PrimitiveTopology = Direct3DHelper.ToD3DPrimitiveTopology(primitiveType);
      m_deviceContext.DrawAuto();
    }

    public void Flush()
    {
      m_deviceContext.Flush();
    }

    public StreamOutputBufferBinding[] GetBoundSOWithoutCopy(out int numActive)
    {
      numActive = m_bufferHelper.CurrentStreamOutputCount;
      return m_bufferHelper.BoundStreamOutputTargets;
    }

    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    private void Dispose(bool disposing)
    {
      if (!m_isDisposed)
      {

        TypedEventHandler<IRenderContext, EventArgs> handler = Disposing;
        if (handler is not null)
          handler(this, EventArgs.Empty);

        if (disposing)
        {
          //Null out references
          ClearOurState(false);

          StopCameraEvents();

          if (!m_isImmediateContext && m_deviceContext is not null)
          {
            m_deviceContext.Dispose();
            m_deviceContext = null;
          }
        }

        m_isDisposed = true;
      }
    }

    private void ClearOurState(bool setDefaultRenderStates)
    {
      m_bufferHelper.ClearState();
      m_renderTargetHelper.ClearState();

      for (int i = 0; i < m_shaderStages.Length; i++)
        m_shaderStages[i].ClearState();

      m_indexBuffer = null;
      m_previousInputLayout = null;
      m_camera = null;

      if (setDefaultRenderStates)
        SetDefaultRenderStates();
    }

    private void SetDefaultRenderStates()
    {
      m_enforcedRenderState = EnforcedRenderState.None;
      m_blendState = null;
      m_rasterizerState = null;
      m_depthStencilState = null;

      BlendState = m_renderSystem.PredefinedBlendStates.Opaque;
      RasterizerState = m_renderSystem.PredefinedRasterizerStates.CullBackClockwiseFront;
      DepthStencilState = m_renderSystem.PredefinedDepthStencilStates.Default;

      m_scissorRectangle = new Rectangle(0, 0, 0, 0);
    }

    private void SetCurrentInputLayout()
    {
      D3D11.InputLayout inputLayout = null;

      if (m_localInputLayoutCache is not null)
        inputLayout = m_localInputLayoutCache.GetOrCreate(m_inputLayoutManager, m_bufferHelper.CurrentVertexBufferCount, m_bufferHelper.BoundVertexBuffers);

      if (!Object.ReferenceEquals(m_previousInputLayout, inputLayout))
      {
        m_deviceContext.InputAssembler.InputLayout = inputLayout;
        m_previousInputLayout = inputLayout;
      }
    }

    private void StartCameraEvents()
    {
      if (m_camera is not null)
      {
        //Apply the viewport
        Viewport vp = m_camera.Viewport;
        m_deviceContext.Rasterizer.SetViewport(vp.X, vp.Y, vp.Width, vp.Height, vp.MinDepth, vp.MaxDepth);

        m_camera.ViewportChanged += Camera_ViewportChanged;
      }
    }

    private void StopCameraEvents()
    {
      if (m_camera is not null)
        m_camera.ViewportChanged -= Camera_ViewportChanged;
    }

    private void Camera_ViewportChanged(Camera sender, EventArgs args)
    {
      Viewport vp = sender.Viewport;
      m_deviceContext.Rasterizer.SetViewport(vp.X, vp.Y, vp.Width, vp.Height, vp.MinDepth, vp.MaxDepth);
    }

    #region Buffer Helper

    internal sealed class BufferHelper
    {
      private static readonly VertexBufferBinding[] EmptyVertexBufferArray = new VertexBufferBinding[0];
      private static readonly StreamOutputBufferBinding[] EmptyStreamOutputArray = new StreamOutputBufferBinding[0];

      private int m_currVbCount;
      private int m_currSOCount;
      private int m_maxVbCount;
      private int m_maxSOCount;

      private VertexBufferBinding[] m_boundVertexBuffers;
      private StreamOutputBufferBinding[] m_boundStreamOutputTargets;

      private D3D11.VertexBufferBinding[] m_tempVBuffers;
      private D3D11.StreamOutputBufferBinding[] m_tempSOBuffers;

      private D3D11.InputAssemblerStage m_inputAssembler;
      private D3D11.StreamOutputStage m_streamOutput;

      public VertexBufferBinding[] BoundVertexBuffers
      {
        get
        {
          return m_boundVertexBuffers;
        }
      }

      public int CurrentVertexBufferCount
      {
        get
        {
          return m_currVbCount;
        }
      }

      public StreamOutputBufferBinding[] BoundStreamOutputTargets
      {
        get
        {
          return m_boundStreamOutputTargets;
        }
      }

      public int CurrentStreamOutputCount
      {
        get
        {
          return m_currSOCount;
        }
      }

      public BufferHelper(D3D11.DeviceContext context, D3D11GraphicsAdapter adapter)
      {
        m_currVbCount = 0;
        m_currSOCount = 0;

        m_maxVbCount = adapter.MaximumVertexStreams;
        m_maxSOCount = adapter.MaximumStreamOutputTargets;

        m_boundVertexBuffers = new VertexBufferBinding[m_maxVbCount];
        m_tempVBuffers = new D3D11.VertexBufferBinding[m_maxVbCount];

        m_boundStreamOutputTargets = new StreamOutputBufferBinding[m_maxSOCount];
        m_tempSOBuffers = new D3D11.StreamOutputBufferBinding[m_maxSOCount];

        m_inputAssembler = context.InputAssembler;
        m_streamOutput = context.StreamOutput;
      }

      #region Vertex Buffers

      public void SetVertexBuffers(VertexBufferBinding[] vertexBuffers, int vbCount)
      {
        if (vbCount > m_maxVbCount)
          throw new TeslaGraphicsException("vertexBuffers", StringLocalizer.Instance.GetLocalizedString("TooManyVertexBuffers", vbCount.ToString(), m_boundVertexBuffers.Length.ToString()));

        int prevCount = m_currVbCount;
        m_currVbCount = vbCount;

        //If zero, setting null, want to clear all previously active buffers, set all to null, and return early
        if (vbCount == 0)
        {
          if (prevCount != 0)
          {
            Array.Clear(m_boundVertexBuffers, 0, prevCount);
            m_inputAssembler.SetVertexBuffers(0, prevCount, m_tempVBuffers);

            return;
          }
        }
        //Else go through each buffer we want to set, if all are present in the correct slots then we don't update, but if
        //at least one needs updating we need to set them all
        else
        {
          int applyCount = 0;

          //Iterate through buffers, apply if not the same
          for (int i = 0; i < vbCount; i++)
          {
            VertexBufferBinding binding = vertexBuffers[i];

            //Set to temporary array
            VertexBuffer vb = binding.VertexBuffer;
            D3D11.VertexBufferBinding nativeBinding = new D3D11.VertexBufferBinding();

            if (vb is not null)
            {
              nativeBinding.Buffer = (vb.Implementation as ID3D11Buffer).D3DBuffer;
              nativeBinding.Stride = vb.VertexLayout.VertexStride;
              nativeBinding.Offset = binding.VertexOffset;
            }
            else
            {
              nativeBinding.Buffer = null;
              nativeBinding.Stride = 0;
              nativeBinding.Offset = 0;
            }

            m_tempVBuffers[i] = nativeBinding;

            //Check if the VB is already set at the index, if so then we may -not- have to apply. But we need to keep checking.
            //If at least one VB isnt set, we need to reset all. If all are already there, we can ignore setting (but still need to clear
            //the temp array).
            if (!binding.Equals(m_boundVertexBuffers[i]))
            {
              m_boundVertexBuffers[i] = binding;
              applyCount++;
            }
          }

          //Clear remaining slots to ensure we don't have any leftover cruft because we had a lot of buffers bound previously
          if (vbCount < prevCount)
            Array.Clear(m_boundVertexBuffers, vbCount, m_maxVbCount - vbCount);

          //If at least one needs to be set, apply all the buffers in the temp array
          if (applyCount > 0)
            m_inputAssembler.SetVertexBuffers(0, vbCount, m_tempVBuffers);

          //Always clear the temp array before returning
          Array.Clear(m_tempVBuffers, 0, vbCount);
        }
      }

      public VertexBufferBinding[] GetVertexBuffers()
      {
        if (m_currVbCount == 0)
          return EmptyVertexBufferArray;

        VertexBufferBinding[] buffers = new VertexBufferBinding[m_currVbCount];
        Array.Copy(m_boundVertexBuffers, 0, buffers, 0, m_currVbCount);

        return buffers;
      }

      #endregion

      #region Stream Output

      public void SetStreamOutputTargets(StreamOutputBufferBinding[] streamOutputBuffers, int soCount)
      {
        if (soCount > m_boundStreamOutputTargets.Length)
          throw new TeslaGraphicsException("streamOutputBuffers", StringLocalizer.Instance.GetLocalizedString("TooManySOTargets", soCount.ToString(), m_boundStreamOutputTargets.Length.ToString()));

        int prevCount = m_currSOCount;
        m_currSOCount = soCount;

        //If zero, setting null, want to clear all previously active buffers, set all to null, and return early
        if (soCount == 0)
        {
          if (prevCount != 0)
          {
            Array.Clear(m_boundStreamOutputTargets, 0, prevCount);
            m_streamOutput.SetTargets(m_tempSOBuffers); //ALL or nothing

            return;
          }
        }
        //Else go through each buffer we want to set, if all are present in the correct slots then we don't update,
        //but but if at least one needs updating we need to set them all
        else
        {

          int applyCount = 0;

          //Iterate through buffers, apply if not the same
          for (int i = 0; i < soCount; i++)
          {
            StreamOutputBufferBinding binding = streamOutputBuffers[i];

            //Set to temporary array
            StreamOutputBuffer so = binding.StreamOutputBuffer;
            D3D11.StreamOutputBufferBinding nativeBinding = new D3D11.StreamOutputBufferBinding();

            if (so is not null)
            {
              nativeBinding.Buffer = (so.Implementation as ID3D11Buffer).D3DBuffer;
              nativeBinding.Offset = binding.VertexOffset;
            }
            else
            {
              nativeBinding.Buffer = null;
              nativeBinding.Offset = 0;
            }

            m_tempSOBuffers[i] = nativeBinding;

            //Check if the SO is already set at the index, if so then we may -not- have to apply. But we need to keep checking.
            //If at least one SO isn't set, we need to reset all. If all are already there we can ignore setting (but still need to clear
            //the temp array).

            if (!binding.Equals(m_boundStreamOutputTargets[i]))
            {
              m_boundStreamOutputTargets[i] = binding;

              applyCount++;
            }
          }

          //Clear remaining slots to ensure we don't have any leftover cruft because we had a lot of buffers bound previously
          if (soCount < prevCount)
            Array.Clear(m_boundStreamOutputTargets, soCount, m_maxSOCount - soCount);

          //If at least one needs to be set, apply all the buffers in the temp array
          if (applyCount > 0)
          {
            //Before setting, unbind any SO buffers that are currently bound as a vertex buffer
            ScanAndUnbindSOBuffersFromIA();

            m_streamOutput.SetTargets(m_tempSOBuffers); //ALL or nothing
          }

          //Always clear the temp array before returning
          Array.Clear(m_tempSOBuffers, 0, soCount);
        }
      }

      public StreamOutputBufferBinding[] GetStreamOutputTargets()
      {
        if (m_currSOCount == 0)
          return EmptyStreamOutputArray;

        StreamOutputBufferBinding[] buffers = new StreamOutputBufferBinding[m_currSOCount];
        Array.Copy(m_boundStreamOutputTargets, 0, buffers, 0, m_currSOCount);

        return buffers;
      }

      #endregion

      public void ClearState()
      {
        Array.Clear(m_boundVertexBuffers, 0, m_currVbCount);
        Array.Clear(m_boundStreamOutputTargets, 0, m_currSOCount);

        m_currVbCount = 0;
        m_currSOCount = 0;
      }

      private void ScanAndUnbindSOBuffersFromIA()
      {
        //For every bound vertex buffer, determine if it's a SO that we're in the process of binding, if it is,
        //then we must set the vertex buffer at that IA slot to null and set the state to the IA. Most cases we probably won't have
        //more than one SO, so there might not be a lot of SetVertexBuffers(..) calls in practice.
        for (int i = 0; i < m_currVbCount; i++)
        {
          VertexBuffer vb = m_boundVertexBuffers[i].VertexBuffer;

          if (vb is not null && vb is StreamOutputBuffer)
          {
            for (int j = 0; j < m_currSOCount; j++)
            {
              StreamOutputBuffer so = m_boundStreamOutputTargets[j].StreamOutputBuffer;

              //If the SO buffer == vertex buffer, then unbind it from the input assembler
              //but do not decrement the current VB count
              if (Object.ReferenceEquals(so, vb))
              {
                m_boundVertexBuffers[i] = null;
                m_inputAssembler.SetVertexBuffers(i, new D3D11.VertexBufferBinding());
              }
            }
          }
        }
      }
    }

    #endregion

    #region RenderTarget Helper

    internal class RenderTargetHelper
    {
      private static readonly IRenderTarget[] EmptyRenderTargetArray = new IRenderTarget[0];

      private SwapChain m_backBuffer;
      private D3D11RenderContext m_renderContext;
      private D3D11.DeviceContext m_deviceContext;
      private D3D11.OutputMergerStage m_outputMerger;

      private bool m_noUserRenderTargetsSet;
      private int m_currRenderTargetCount;
      private int m_maxRenderTargetCount;
      private IRenderTarget[] m_boundRenderTargets;
      private D3D11.RenderTargetView[] m_tempRenderTargets;

      public SwapChain BackBuffer
      {
        get
        {
          return m_backBuffer;
        }
        set
        {
          if (!Object.ReferenceEquals(m_backBuffer, value))
          {
            //Setup events to handle when the swapchain is reset or resized.
            StopSwapChainEvents();

            m_backBuffer = value;

            StartSwapChainEvents();

            //If the old swapchain was active OR no render targets set, set the new one's targets. 
            //So if user targets are set, then we don't unseat them. When we set user targets to null, then the
            //current active backbuffer will be set back to active.
            if (m_noUserRenderTargetsSet)
              SetActiveBackBufferTargets();
          }
        }
      }

      public bool NoUserRenderTargetsSet
      {
        get
        {
          return m_noUserRenderTargetsSet;
        }
      }

      public int CurrentRenderTargetCount
      {
        get
        {
          return m_currRenderTargetCount;
        }
      }

      public IRenderTarget[] BoundRenderTargets
      {
        get
        {
          return m_boundRenderTargets;
        }
      }

      public RenderTargetHelper(D3D11RenderContext renderContext, D3D11GraphicsAdapter adapter)
      {
        m_backBuffer = null;
        m_renderContext = renderContext;
        m_deviceContext = renderContext.D3DDeviceContext;
        m_outputMerger = m_deviceContext.OutputMerger;

        m_noUserRenderTargetsSet = true;
        m_currRenderTargetCount = 0;
        m_maxRenderTargetCount = adapter.MaximumMultiRenderTargets;

        m_boundRenderTargets = new IRenderTarget[m_maxRenderTargetCount];
        m_tempRenderTargets = new D3D11.RenderTargetView[m_maxRenderTargetCount];
      }

      public void Clear(ClearOptions options, Color color, float depth, int stencil)
      {
        if (m_noUserRenderTargetsSet)
        {
          m_backBuffer.Clear(m_renderContext, options, color, depth, stencil);
        }
        else
        {
          ClearOptions justTarget = ClearOptions.Target;
          bool clearDepth = (options & ClearOptions.Depth) == ClearOptions.Depth || (options & ClearOptions.Stencil) == ClearOptions.Stencil;

          //Only clear the depth/stencil of the very first render target, otherwise just clear the target and not any depth buffer of the rest (they may also share depth buffers)
          for (int i = 0; i < m_currRenderTargetCount; i++)
          {
            IRenderTarget renderTarget = m_boundRenderTargets[i];
            ID3D11RenderTargetView rtv = GetRenderTargetView(renderTarget);
            if (rtv is not null)
              rtv.Clear(m_deviceContext, justTarget, color, depth, stencil);

            if (i == 0 && clearDepth)
            {
              ID3D11DepthStencilView dsv = renderTarget.DepthStencilBuffer as ID3D11DepthStencilView;
              if (dsv is not null)
                dsv.Clear(m_deviceContext, options, depth, stencil);
            }
          }
        }
      }

      public void SetRenderTargets(SetTargetOptions options, IRenderTarget[] renderTargets, int numTargets)
      {
        //If not active and setting to null, set back to the active swap chain and resolve user targets
        if (renderTargets is null || numTargets == 0)
        {
          SetActiveBackBufferTargets();

          return;
        }

        //Otherwise back buffer is not active, and we want to set user targets
        m_noUserRenderTargetsSet = false;

        if (numTargets > m_maxRenderTargetCount)
          throw new TeslaGraphicsException(StringLocalizer.Instance.GetLocalizedString("TooManyRenderTargets"));

        //Resolve any outstanding targets
        ResolveTargets();

        int width = 0;
        int height = 0;
        int depth = 0;
        int sampleCount = 0;
        int arrayCount = 0;
        bool isCube = false;
        bool isArray = false;
        D3D11.DepthStencilView dsv = null;

        //Compare all targets to set with the first one, also extract the DSV from the first one
        for (int i = 0; i < numTargets; i++)
        {
          IRenderTarget rt = renderTargets[i];
          if (rt is null)
            throw new ArgumentNullException(StringLocalizer.Instance.GetLocalizedString("RenderTargetNullAt", i.ToString()));

          if (i == 0)
          {
            width = rt.Width;
            height = rt.Height;
            depth = rt.Depth;
            sampleCount = rt.MultisampleDescription.Count;
            arrayCount = rt.ArrayCount;
            isCube = rt.IsCubeResource;
            isArray = rt.IsArrayResource;

            if (options != SetTargetOptions.NoDepthBuffer)
            {
              ID3D11DepthStencilView depthBuffer = rt.DepthStencilBuffer as ID3D11DepthStencilView;
              if (depthBuffer is not null)
              {
                switch (options)
                {
                  case SetTargetOptions.ReadOnlyDepthBuffer:
                    dsv = depthBuffer.D3DReadOnlyDepthStencilView;

#if DEBUG
                    if (dsv is null)
                      EngineLog.Log(LogLevel.Warn, "Trying to set a depth buffer as read-only when it was not created as readable!");
#endif
                    break;
                  case SetTargetOptions.None:
                    dsv = depthBuffer.D3DDepthStencilView;
                    break;
                }
              }
            }
          }
          else
          {
            if (rt.Width != width || rt.Height != height || rt.Depth != depth || rt.MultisampleDescription.Count != sampleCount || rt.ArrayCount != arrayCount ||
                rt.IsCubeResource != isCube || rt.IsArrayResource != isArray)
              throw new TeslaGraphicsException(StringLocalizer.Instance.GetLocalizedString("RenderTargetDimensionsMustMatch"));
          }

          UnbindShaderInputs(rt, m_renderContext.m_shaderStages);

          m_boundRenderTargets[i] = rt;
          m_tempRenderTargets[i] = GetRenderTargetView(rt).D3DRenderTargetView;
          m_currRenderTargetCount++;
        }

        //Clear remaining slots
        if (numTargets < m_maxRenderTargetCount)
        {
          int remainingCount = m_maxRenderTargetCount - numTargets;
          Array.Clear(m_boundRenderTargets, numTargets, remainingCount);
          Array.Clear(m_tempRenderTargets, numTargets, remainingCount);
        }

        m_outputMerger.SetTargets(dsv, m_currRenderTargetCount, m_tempRenderTargets);
        Array.Clear(m_tempRenderTargets, 0, m_currRenderTargetCount);
      }

      public IRenderTarget[] GetRenderTargets()
      {
        if (m_currRenderTargetCount == 0)
          return EmptyRenderTargetArray;

        IRenderTarget[] renderTargets = new IRenderTarget[m_currRenderTargetCount];
        Array.Copy(m_boundRenderTargets, 0, renderTargets, 0, m_currRenderTargetCount);

        return renderTargets;
      }

      public void ClearState()
      {
        Array.Clear(m_boundRenderTargets, 0, m_currRenderTargetCount);

        m_currRenderTargetCount = 0;
        m_noUserRenderTargetsSet = false;
        m_backBuffer = null;
        StopSwapChainEvents();
      }

      private void StartSwapChainEvents()
      {
        if (m_backBuffer is not null)
        {
          ID3D11Backbuffer swp = m_backBuffer.Implementation as ID3D11Backbuffer;
          if (swp is not null)
            swp.OnResetResize += ReApplyActiveBackBufferTargets;
        }
      }

      private void StopSwapChainEvents()
      {
        if (m_backBuffer is not null)
        {
          ID3D11Backbuffer swp = m_backBuffer.Implementation as ID3D11Backbuffer;
          if (swp is not null)
            swp.OnResetResize -= ReApplyActiveBackBufferTargets;
        }
      }

      private void ResolveTargets()
      {
        if (m_currRenderTargetCount == 0)
          return;

        for (int i = 0; i < m_currRenderTargetCount; i++)
        {
          ID3D11RenderTargetView rt = GetRenderTargetView(m_boundRenderTargets[i]);
          if (rt is not null)
            rt.ResolveResource(m_deviceContext);
        }
      }

      private void SetActiveBackBufferTargets()
      {
        //Resolve any outstanding targets
        ResolveTargets();

        //Ensure we unseat any targets beyond the first slot!
        if (m_currRenderTargetCount > 0)
        {
          Array.Clear(m_boundRenderTargets, 0, m_currRenderTargetCount);
          m_currRenderTargetCount = 0;
        }

        if (m_backBuffer is not null)
        {
          ID3D11RenderTargetView rtv = m_backBuffer.Implementation as ID3D11RenderTargetView;
          ID3D11DepthStencilView dsv = m_backBuffer.Implementation as ID3D11DepthStencilView;

          D3D11.DepthStencilView depthStencilView = (dsv is not null) ? dsv.D3DDepthStencilView : null;

          if (rtv is not null)
            m_tempRenderTargets[0] = rtv.D3DRenderTargetView;

          m_outputMerger.SetTargets(depthStencilView, 1, m_tempRenderTargets);
          m_tempRenderTargets[0] = null;
        }
        //No active buffer, but also need to set state to null. Avoid setting for repeated null calls
        else if (!m_noUserRenderTargetsSet)
        {
          //No backbuffer to set to...
          m_outputMerger.SetTargets((D3D11.RenderTargetView) null);
        }

        m_noUserRenderTargetsSet = true;
      }

      private static ID3D11RenderTargetView GetRenderTargetView(IRenderTarget rt)
      {
        if (rt is GraphicsResource)
        {
          return (rt as GraphicsResource).Implementation as ID3D11RenderTargetView;
        }
        else
        {
          return rt as ID3D11RenderTargetView;
        }
      }

      private static void UnbindShaderInputs(IRenderTarget rt, D3D11ShaderStage[] stages)
      {
        ID3D11ShaderResourceView srv = Direct3DHelper.GetD3DShaderResourceView_Interface(rt);
        if (srv is null)
          return;

        //Look ta every shader stage, if the render target is bound as input, unset it -- else D3D11 reports a warning
        //(or some drivers can crash?) and the SRV gets unset anyways, but our wrapper doesn't know that, so it'll still
        //think the shader resource has been set unless if explicitly unset
        ShaderStageBinding boundStages = srv.BoundStages;
        for (int i = 0; i < stages.Length; i++)
        {
          D3D11ShaderStage stage = stages[i];
          ShaderStageBinding currStageFlag = stage.StageBinding;

          if ((boundStages & currStageFlag) == currStageFlag)
            stage.ClearBoundSlots(rt);
        }
      }

      private void ReApplyActiveBackBufferTargets(ID3D11Backbuffer swapChain, EventArgs e)
      {
        if (m_noUserRenderTargetsSet)
          SetActiveBackBufferTargets();
      }
    }

    #endregion
  }

  #region Extension methods

  internal static class DeviceContextExtensionMethods
  {
    public unsafe static void SetVertexBuffers(this D3D11.InputAssemblerStage inputAssembler, int startSlot, int numBuffers, params D3D11.VertexBufferBinding[] vertexBufferBindings)
    {
      IntPtr* vertexBuffers = stackalloc IntPtr[numBuffers];
      int* stridesPtr = stackalloc int[numBuffers];
      int* offsetsPtr = stackalloc int[numBuffers];

      for (int i = 0; i < numBuffers; i++)
      {
        D3D11.VertexBufferBinding binding = vertexBufferBindings[i];

        vertexBuffers[i] = (binding.Buffer is null) ? IntPtr.Zero : binding.Buffer.NativePointer;
        stridesPtr[i] = binding.Stride;
        offsetsPtr[i] = binding.Offset;
      }

      inputAssembler.SetVertexBuffers(startSlot, numBuffers, new IntPtr(vertexBuffers), new IntPtr(stridesPtr), new IntPtr(offsetsPtr));
    }
  }

  #endregion
}
