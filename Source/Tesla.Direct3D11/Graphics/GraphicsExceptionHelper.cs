﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using SharpDX;
using Tesla.Graphics;

namespace Tesla.Direct3D11.Graphics
{
  internal static class GraphicsExceptionHelper
  {
    public static TeslaGraphicsException NewInvalidEnumException(Enum value)
    {
      return new TeslaGraphicsException(value.GetType().Name + "." + value.ToString(), StringLocalizer.Instance.GetLocalizedString("InvalidEnum"));
    }

    public static TeslaGraphicsException NewSharpDXError(Result result)
    {
      return new TeslaGraphicsException(StringLocalizer.Instance.GetLocalizedString("Direct3D11Error", result.ToString()));
    }

    public static TeslaGraphicsException NewImplementationFactoryNotInitialized()
    {
      return new TeslaGraphicsException(StringLocalizer.Instance.GetLocalizedString("ImplementationFactoryNotInitialized"));
    }

    public static TeslaGraphicsException NewCannotWriteToImmutableResource()
    {
      return new TeslaGraphicsException(StringLocalizer.Instance.GetLocalizedString("CannotWriteToImmutableResource"));
    }
  }
}
