﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using Tesla.Content;
using Tesla.Graphics;

namespace Tesla.SquidUI
{
  /// <summary>
  /// Renders Squid UI elements.
  /// </summary>
  public class SquidUIRenderer : Squid.ISquidRenderer
  {
    private bool m_isDisposed;
    private ContentManager m_content;
    private SpriteBatch m_batch;
    private IRenderContext m_context;
    private RasterizerState m_scissorTest;

    private Dictionary<int, SpriteFont> m_idToFont;
    private Dictionary<int, Texture2D> m_idToTexture;
    private Dictionary<String, int> m_nameToFontId;
    private Dictionary<String, int> m_nameToTextureId;
    private UISkin m_defaultSkin;
    private Texture2D m_blankTex;
    private Rectangle m_blankTexRect;

    /// <summary>
    /// Gets the sprite batch used to render UI elements.
    /// </summary>
    public SpriteBatch Batch
    {
      get
      {
        return m_batch;
      }
    }

    /// <summary>
    /// Gets the default skin.
    /// </summary>
    public UISkin DefaultSkin
    {
      get
      {
        return m_defaultSkin;
      }
    }

    /// <summary>
    /// Gets the default font.
    /// </summary>
    public SpriteFont DefaultFont
    {
      get
      {
        return m_idToFont[0];
      }
    }

    /// <summary>
    /// Gets the texture used for drawing solid boxes.
    /// </summary>
    public Texture2D BlankTexture
    {
      get
      {
        return m_blankTex;
      }
    }

    /// <summary>
    /// Gets the subimage rectangle in the blank texture that represents a solid box.
    /// </summary>
    public Rectangle BlankTextureRectangle
    {
      get
      {
        return m_blankTexRect;
      }
    }

    /// <summary>
    /// Gets the content manager used to load UI assets.
    /// </summary>
    public ContentManager ContentManager
    {
      get
      {
        return m_content;
      }
    }

    /// <summary>
    /// Gets the render context used for drawing UI elements.
    /// </summary>
    public IRenderContext Context
    {
      get
      {
        return m_context;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="SquidUIRenderer"/> class.
    /// </summary>
    /// <param name="content">Content manager used to load UI assets.</param>
    /// <param name="context">Render context used to draw UI elements.</param>
    public SquidUIRenderer(ContentManager content, IRenderContext context)
    {
      m_content = content;
      m_context = context;
      m_batch = new SpriteBatch(context.RenderSystem);

      m_scissorTest = new RasterizerState(context.RenderSystem);
      m_scissorTest.ScissorTestEnable = true;
      m_scissorTest.Cull = CullMode.None;
      m_scissorTest.BindRenderState();

      m_idToFont = new Dictionary<int, SpriteFont>();
      m_idToTexture = new Dictionary<int, Texture2D>();
      m_nameToFontId = new Dictionary<String, int>();
      m_nameToTextureId = new Dictionary<String, int>();

      LoadDefaults(content);
    }

    /// <summary>
    /// Sets the blank texture used for drawing solid boxes.
    /// </summary>
    /// <param name="texture">The texture used.</param>
    /// <param name="subImage">Optional subimage into the texture, if null then the whole texture dimensions are set.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the texture is null.</exception>
    public void SetBlankTexture(Texture2D texture, Rectangle? subImage)
    {
      if (texture == null)
        throw new ArgumentNullException("texture");

      m_blankTex = texture;

      if (subImage.HasValue)
      {
        m_blankTexRect = subImage.Value;
      }
      else
      {
        m_blankTexRect = new Rectangle(0, 0, texture.Width, texture.Height);
      }
    }

    /// <summary>
    /// Sets the default UI skin. When the renderer is created, a default skin is loaded up, but this allows it to be overridden.
    /// </summary>
    /// <param name="skin">New default skin.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the object is null.</exception>
    public void SetDefaultSkin(UISkin skin)
    {
      if (skin == null)
        throw new ArgumentNullException("skin");

      m_defaultSkin = skin;
    }

    /// <summary>
    /// Sets the default UI font. When the renderer is created, a default font is loaded up, but this allows it to be overridden.
    /// </summary>
    /// <param name="font">New default font.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the object is null.</exception>
    public void SetDefaultFont(SpriteFont font)
    {
      if (font == null)
        throw new ArgumentNullException("font");

      m_idToFont[0] = font;
    }

    private void LoadDefaults(ContentManager content)
    {
      BinaryResourceImporter importer = new BinaryResourceImporter();

      SpriteFont defaultFont = LoadEmbeddedResource<SpriteFont>(importer, content, "Arial_10");
      m_nameToFontId.Add("default", 0);
      m_idToFont.Add(0, defaultFont);

      TextureAtlas defaultTexture = LoadEmbeddedResource<TextureAtlas>(importer, content, "DefaultSkin_TextureAtlas");
      m_nameToTextureId.Add("DefaultSkin_TextureAtlas.tebo", 0);
      m_idToTexture.Add(0, defaultTexture.Texture);

      m_defaultSkin = LoadEmbeddedResource<UISkin>(importer, content, "DefaultSkin");

      //The default texture atlas has a "blank" portion 
      m_blankTex = defaultTexture.Texture;
      if (!defaultTexture.TryGetValue("blank", out m_blankTexRect))
        System.Diagnostics.Debug.Assert(false);
    }

    private T LoadEmbeddedResource<T>(BinaryResourceImporter importer, ContentManager content, String name) where T : ISavable
    {
      DataBuffer<byte> db = DataBuffer.Wrap<byte>((byte[]) DefaultUI.ResourceManager.GetObject(name, DefaultUI.Culture));
      using (DataBufferStream str = new DataBufferStream(db, false, true))
      {
        ISavable savable = importer.Load(str, content, null);
        return (T) savable;
      }
    }

    /// <summary>
    /// Starts the batch.
    /// </summary>
    public void StartBatch()
    {
      m_batch.Begin(m_context, SpriteSortMode.Deferred, BlendState.AlphaBlendNonPremultiplied, m_scissorTest, SamplerState.LinearWrap, DepthStencilState.None);
    }

    /// <summary>
    /// Draws a box.
    /// </summary>
    /// <param name="x">The x.</param>
    /// <param name="y">The y.</param>
    /// <param name="width">The width.</param>
    /// <param name="height">The height.</param>
    /// <param name="color">The color.</param>
    public void DrawBox(int x, int y, int width, int height, int color)
    {
      Rectangle dest = new Rectangle(x, y, width, height);
      ColorBGRA bgra = new ColorBGRA((uint) color);

      m_batch.Draw(m_blankTex, dest, m_blankTexRect, new Color(bgra.R, bgra.G, bgra.B, bgra.A));
    }

    /// <summary>
    /// Draws the text.
    /// </summary>
    /// <param name="text">The text.</param>
    /// <param name="x">The x.</param>
    /// <param name="y">The y.</param>
    /// <param name="font">The font.</param>
    /// <param name="color">The color.</param>
    public void DrawText(string text, int x, int y, int font, int color)
    {
      SpriteFont f = m_idToFont[font];
      ColorBGRA bgra = new ColorBGRA((uint) color);

      m_batch.DrawString(f, text, new Vector2(x, y), new Color(bgra.R, bgra.G, bgra.B, bgra.A));
    }

    /// <summary>
    /// Draws the texture.
    /// </summary>
    /// <param name="texture">The texture.</param>
    /// <param name="x">The x.</param>
    /// <param name="y">The y.</param>
    /// <param name="width">The width.</param>
    /// <param name="height">The height.</param>
    /// <param name="source">The source.</param>
    /// <param name="color">The color.</param>
    public void DrawTexture(int texture, int x, int y, int width, int height, Squid.Rectangle source, int color)
    {
      Texture2D tex = m_idToTexture[texture];
      ColorBGRA bgra = new ColorBGRA((uint) color);

      Rectangle dest = new Rectangle(x, y, width, height);
      Rectangle src = new Rectangle(source.Left, source.Top, source.Width, source.Height);

      m_batch.Draw(tex, dest, src, new Color(bgra.R, bgra.G, bgra.B, bgra.A));
    }

    /// <summary>
    /// Ends the batch.
    /// </summary>
    /// <param name="final">if set to <c>true</c> [final].</param>
    public void EndBatch(bool final)
    {
      m_batch.End();
    }

    /// <summary>
    /// Returns a unique integer for the given font name
    /// </summary>
    /// <param name="name">The name.</param>
    /// <returns>System.Int32.</returns>
    public int GetFont(string name)
    {
      int id;

      if (!m_nameToFontId.TryGetValue(name, out id))
      {
        SpriteFont font = m_content.Load<SpriteFont>(name);
        if (font == null)
          return -1;

        id = m_idToFont.Count;
        m_idToFont.Add(id, font);
        m_nameToFontId.Add(name, id);
      }

      return id;
    }

    /// <summary>
    /// Gets the size of the text.
    /// </summary>
    /// <param name="text">The text.</param>
    /// <param name="font">The font.</param>
    /// <returns>Point.</returns>
    public Squid.Point GetTextSize(string text, int font)
    {
      if (String.IsNullOrEmpty(text))
        return new Squid.Point();

      SpriteFont f = m_idToFont[font];
      Vector2 size = f.MeasureString(text);

      return new Squid.Point((int) size.X, (int) size.Y);
    }

    /// <summary>
    /// Returns a unique integer for the given texture name
    /// </summary>
    /// <param name="name">The name.</param>
    /// <returns>System.Int32.</returns>
    public int GetTexture(string name)
    {
      int id;

      if (!m_nameToTextureId.TryGetValue(name, out id))
      {
        Texture2D tex = m_content.Load<Texture2D>(name);
        if (tex == null)
          return -1;

        id = m_idToTexture.Count;

        m_idToTexture.Add(id, tex);
        m_nameToTextureId.Add(name, id);
      }

      return id;
    }

    /// <summary>
    /// Gets the size of the texture.
    /// </summary>
    /// <param name="texture">The texture.</param>
    /// <returns>Point.</returns>
    public Squid.Point GetTextureSize(int texture)
    {
      Texture2D tex = m_idToTexture[texture];

      return new Squid.Point(tex.Width, tex.Height);
    }

    /// <summary>
    /// Set the scissor rectangle
    /// </summary>
    /// <param name="x">The x.</param>
    /// <param name="y">The y.</param>
    /// <param name="width">The width.</param>
    /// <param name="height">The height.</param>
    public void Scissor(int x, int y, int width, int height)
    {
      if (x < 0)
        x = 0;
      if (y < 0)
        y = 0;

      m_context.ScissorRectangle = new Rectangle(x, y, width, height);
    }

    /// <summary>
    /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
    /// </summary>
    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    /// <summary>
    /// Releases unmanaged and - optionally - managed resources.
    /// </summary>
    /// <param name="disposing"><c>True</c> to release both managed and unmanaged resources; <c>False</c> to release only unmanaged resources.</param>
    protected virtual void Dispose(bool disposing)
    {
      if (m_isDisposed)
      {
        if (disposing)
        {
          m_batch.Dispose();
          m_scissorTest.Dispose();

          m_idToTexture.Clear();
          m_idToFont.Clear();
        }

        m_isDisposed = true;
      }
    }
  }
}
