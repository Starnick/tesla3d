﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using Tesla.Content;

namespace Tesla.SquidUI
{
  /// <summary>
  /// Adapter for <see cref="Squid.Margin"/> for serialization purposes.
  /// </summary>
  public struct UIMargin : IPrimitiveValue
  {
    /// <summary>
    /// X units from the leftmost of the border;
    /// </summary>
    public int Left;

    /// <summary>
    /// Y units from the topmost of the border;
    /// </summary>
    public int Top;

    /// <summary>
    /// X units from the rightmost of the border.
    /// </summary>
    public int Right;

    /// <summary>
    /// Y units from the bottomost of the border.
    /// </summary>
    public int Bottom;

    /// <summary>
    /// Copies data from <see cref="Squid.Margin"/>.
    /// </summary>
    /// <param name="margin">Margin to copy from.</param>
    public readonly void ToSquidMargin(out Squid.Margin margin)
    {
      margin = new Squid.Margin(Left, Top, Right, Bottom);
    }

    /// <summary>
    /// Creates a <see cref="Squid.Margin"/> copy.
    /// </summary>
    /// <param name="margin">Margin to write to.</param>
    public void FromSquidMargin(ref Squid.Margin margin)
    {
      Left = margin.Left;
      Top = margin.Top;
      Right = margin.Right;
      Bottom = margin.Bottom;
    }

    /// <summary>
    /// Writes the primitive data to the output.
    /// </summary>
    /// <param name="output">Primitive writer.</param>
    public readonly void Write(IPrimitiveWriter output)
    {
      output.Write("Left", Left);
      output.Write("Top", Top);
      output.Write("Right", Right);
      output.Write("Bottom", Bottom);
    }

    /// <summary>
    /// Reads the primitive data from the input.
    /// </summary>
    /// <param name="input">Primitive reader.</param>
    public void Read(IPrimitiveReader input)
    {
      Left = input.ReadInt32("Left");
      Top = input.ReadInt32("Top");
      Right = input.ReadInt32("Right");
      Bottom = input.ReadInt32("Bottom");
    }
  }

  /// <summary>
  /// Wrapper around <see cref="Squid.Style"/> for serialization purposes.
  /// </summary>
  public struct UIStyle : IPrimitiveValue
  {
    private Squid.Style m_nativeStyle;

    /// <summary>
    /// Gets the native style.
    /// </summary>
    public readonly Squid.Style NativeStyle
    {
      get
      {
        return m_nativeStyle;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="UIStyle"/> struct.
    /// </summary>
    /// <param name="nativeStyle">The native style.</param>
    public UIStyle(Squid.Style nativeStyle)
    {
      m_nativeStyle = nativeStyle;
    }

    /// <summary>
    /// Writes the primitive data to the output.
    /// </summary>
    /// <param name="output">Primitive writer.</param>
    public readonly void Write(IPrimitiveWriter output)
    {
      Color textColor, tintColor, backColor;
      ToColor(m_nativeStyle.TextColor, out textColor);
      ToColor(m_nativeStyle.Tint, out tintColor);
      ToColor(m_nativeStyle.BackColor, out backColor);

      Squid.Margin marginTextPadding = m_nativeStyle.TextPadding;
      Squid.Margin marginGrid = m_nativeStyle.Grid;

      UIMargin textPadding = new UIMargin();
      UIMargin grid = new UIMargin();
      textPadding.FromSquidMargin(ref marginTextPadding);
      grid.FromSquidMargin(ref marginGrid);

      Squid.Rectangle squidTexRect = m_nativeStyle.TextureRect;
      Rectangle texRect = new Rectangle(squidTexRect.Left, squidTexRect.Top, squidTexRect.Width, squidTexRect.Height);

      output.Write<Color>("Tint", tintColor);
      output.Write<Color>("BackColor", backColor);
      output.Write<Color>("TextColor", textColor);
      output.Write<UIMargin>("TextPadding", textPadding);
      output.WriteEnum<Squid.Alignment>("TextAlign", m_nativeStyle.TextAlign);
      output.Write("Opacity", m_nativeStyle.Opacity);
      output.Write("Font", m_nativeStyle.Font);
      output.Write("Texture", m_nativeStyle.Texture);
      output.Write<Rectangle>("TextureRect", texRect);
      output.Write<UIMargin>("Grid", grid);
      output.WriteEnum<Squid.TextureMode>("Tiling", m_nativeStyle.Tiling);
    }

    /// <summary>
    /// Reads the primitive data from the input.
    /// </summary>
    /// <param name="input">Primitive reader.</param>
    public void Read(IPrimitiveReader input)
    {
      Color textColor, tintColor, backColor;
      UIMargin textPadding, grid;
      Rectangle texRect;

      m_nativeStyle = new Squid.Style();

      input.Read<Color>("Tint", out tintColor);
      input.Read<Color>("BackColor", out backColor);
      input.Read<Color>("TextColor", out textColor);
      input.Read<UIMargin>("TextPadding", out textPadding);

      m_nativeStyle.TextAlign = input.ReadEnum<Squid.Alignment>("TextAlign");
      m_nativeStyle.Opacity = input.ReadSingle("Opacity");
      m_nativeStyle.Font = input.ReadString("Font");
      m_nativeStyle.Texture = input.ReadString("Texture");

      input.Read<Rectangle>("TextureRect", out texRect);
      input.Read<UIMargin>("Grid", out grid);

      m_nativeStyle.Tiling = input.ReadEnum<Squid.TextureMode>("Tiling");
      m_nativeStyle.TextColor = FromColor(textColor);
      m_nativeStyle.Tint = FromColor(tintColor);
      m_nativeStyle.BackColor = FromColor(backColor);
      m_nativeStyle.TextPadding = new Squid.Margin(textPadding.Left, textPadding.Top, textPadding.Right, textPadding.Bottom);
      m_nativeStyle.Grid = new Squid.Margin(grid.Left, grid.Top, grid.Right, grid.Bottom);
      m_nativeStyle.TextureRect = new Squid.Rectangle(texRect.Left, texRect.Top, texRect.Width, texRect.Height);
    }

    private static void ToColor(int argb, out Color color)
    {
      int a, r, g, b;

      Squid.ColorInt.FromArgb(argb, out a, out r, out g, out b);

      color = new Color(r, g, b, a);
    }

    private static int FromColor(Color color)
    {
      return Squid.ColorInt.ARGB(color.A, color.R, color.G, color.B);
    }
  }

  /// <summary>
  /// Wrapper around <see cref="Squid.ControlStyle"/> for serialization purposes.
  /// </summary>
  public struct UIControlStyle : IPrimitiveValue
  {
    private Squid.ControlStyle m_nativeControlStyle;

    /// <summary>
    /// Gets the native control style.
    /// </summary>
    public readonly Squid.ControlStyle NativeControlStyle
    {
      get
      {
        return m_nativeControlStyle;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="UIControlStyle"/> struct.
    /// </summary>
    /// <param name="nativeControlStyle">The native control style.</param>
    public UIControlStyle(Squid.ControlStyle nativeControlStyle)
    {
      m_nativeControlStyle = nativeControlStyle;
    }

    /// <summary>
    /// Writes the primitive data to the output.
    /// </summary>
    /// <param name="output">Primitive writer.</param>
    public readonly void Write(IPrimitiveWriter output)
    {
      Squid.StyleCollection styles = m_nativeControlStyle.Styles;

      output.BeginWriteGroup("UIStyles", styles.Count);

      foreach (KeyValuePair<Squid.ControlState, Squid.Style> kv in styles)
      {
        output.BeginWriteGroup("Entry");

        UIStyle style = new UIStyle(kv.Value);

        output.WriteEnum<Squid.ControlState>("ControlState", kv.Key);
        output.Write<UIStyle>("UIStyle", style);

        output.EndWriteGroup();
      }

      output.EndWriteGroup();
    }

    /// <summary>
    /// Reads the primitive data from the input.
    /// </summary>
    /// <param name="input">Primitive reader.</param>
    public void Read(IPrimitiveReader input)
    {
      m_nativeControlStyle = new Squid.ControlStyle();

      int count = input.BeginReadGroup("UIStyles");

      for (int i = 0; i < count; i++)
      {
        input.BeginReadGroup("Entry");

        UIStyle style;

        Squid.ControlState state = input.ReadEnum<Squid.ControlState>("ControlState");
        input.Read<UIStyle>("UIStyle", out style);

        m_nativeControlStyle.Styles[state] = style.NativeStyle;

        input.EndReadGroup();
      }

      input.EndReadGroup();
    }
  }

  /// <summary>
  /// Savable version of <see cref="Squid.Skin"/>.
  /// </summary>
  [SavableVersion(1)]
  public class UISkin : Squid.Skin, ISavable, INamable
  {
    private String m_name;

    /// <summary>
    /// Gets or sets the name of the object.
    /// </summary>
    public String Name
    {
      get
      {
        return m_name;
      }
      set
      {
        m_name = value;
      }
    }

    /// <summary>
    /// constructs a new instance of the <see cref="UISkin"/> class.
    /// </summary>
    public UISkin()
    {
      m_name = String.Empty;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="UISkin"/> class.
    /// </summary>
    /// <param name="skin">Skin to copy from.</param>
    public UISkin(Squid.Skin skin)
    {
      if (skin != null)
      {
        foreach (KeyValuePair<String, Squid.ControlStyle> kv in skin)
          Add(kv.Key, kv.Value.Copy());
      }

      m_name = String.Empty;
    }

    /// <summary>
    /// Reads the object data from the input.
    /// </summary>
    /// <param name="input">Savable reader</param>
    public void Read(ISavableReader input)
    {
      m_name = input.ReadString("Name");

      int count = input.BeginReadGroup("ControlStyles");

      for (int i = 0; i < count; i++)
      {
        input.BeginReadGroup("Entry");

        String name = input.ReadString("Name");

        UIControlStyle controlStyle;
        input.Read<UIControlStyle>("ControlStyle", out controlStyle);

        Add(name, controlStyle.NativeControlStyle);

        input.EndReadGroup();
      }

      input.EndReadGroup();
    }

    /// <summary>
    /// Writes the object data to the output.
    /// </summary>
    /// <param name="output">Savable writer</param>
    public void Write(ISavableWriter output)
    {
      output.Write("Name", m_name);

      output.BeginWriteGroup("ControlStyles", Count);

      foreach (KeyValuePair<String, Squid.ControlStyle> kv in this)
      {
        output.BeginWriteGroup("Entry");

        UIControlStyle controlStyle = new UIControlStyle(kv.Value);

        output.Write("Name", kv.Key);
        output.Write<UIControlStyle>("ControlStyle", controlStyle);

        output.EndWriteGroup();
      }

      output.EndWriteGroup();
    }
  }
}
