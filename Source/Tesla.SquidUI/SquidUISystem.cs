﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Content;
using Tesla.Graphics;
using Tesla.Input;

namespace Tesla.SquidUI
{
  /// <summary>
  /// Manager for a Squid User Interface.
  /// </summary>
  public class SquidUISystem : IUpdatableEngineService
  {
    private SquidUIRenderer m_renderer;
    private bool[] m_pressedMouseButtons = new bool[5];
    private int m_lastScroll = 0;

    /// <summary>
    /// Gets the name of the service.
    /// </summary>
    public String Name
    {
      get
      {
        return "SquidUI";
      }
    }

    /// <summary>
    /// Gets the UI renderer.
    /// </summary>
    public SquidUIRenderer Renderer
    {
      get
      {
        if (m_renderer == null)
          InitializeRenderer();

        return m_renderer;
      }
    }

    /// <summary>
    /// Initializes the service. This is called by the engine when a service is newly registered.
    /// </summary>
    /// <param name="engine">Engine instance</param>
    void IEngineService.Initialize(Engine engine)
    {
    }

    /// <summary>
    /// Initializes the service with a renderer. If none is set, this is automatically called with default parameters.
    /// </summary>
    public void InitializeRenderer()
    {
      if (m_renderer != null)
        return;

      IRenderSystem renderSystem = IRenderSystem.Current;
      m_renderer = new SquidUIRenderer(new ContentManager(), renderSystem.ImmediateContext);
      Squid.Gui.Renderer = m_renderer;
    }

    /// <summary>
    /// Initializes the service with a renderer.
    /// </summary>
    /// <param name="uiContentManager">Content manager that the renderer will use to load UI assets.</param>
    public void InitializeRenderer(ContentManager uiContentManager)
    {
      if (m_renderer != null)
        return;

      IRenderSystem renderSystem = IRenderSystem.Current;
      m_renderer = new SquidUIRenderer(uiContentManager, renderSystem.ImmediateContext);
      Squid.Gui.Renderer = m_renderer;
    }

    /// <summary>
    /// Initializes the service with a renderer.
    /// </summary>
    /// <param name="uiContentManager">Content manager that the renderer will use to load UI assets.</param>
    /// <param name="context">Render context used to draw the UI.</param>
    public void InitializeRenderer(ContentManager uiContentManager, IRenderContext context)
    {
      if (m_renderer != null)
        return;

      m_renderer = new SquidUIRenderer(uiContentManager, context);
      Squid.Gui.Renderer = m_renderer;
    }

    /// <summary>
    /// Notifies the service to update its internal state.
    /// </summary>
    /// <param name="time">Elapsed time since the last update.</param>
    public void Update(IGameTime time)
    {
      if (m_renderer != null)
        InitializeRenderer();

      //Needs to happen after input services update...
      MouseState ms = Mouse.GetMouseState();

      m_pressedMouseButtons[0] = ms.LeftButton == ButtonState.Pressed ? true : false;
      m_pressedMouseButtons[1] = ms.RightButton == ButtonState.Pressed ? true : false;
      m_pressedMouseButtons[2] = ms.MiddleButton == ButtonState.Pressed ? true : false;
      m_pressedMouseButtons[3] = ms.XButton1 == ButtonState.Pressed ? true : false;
      m_pressedMouseButtons[4] = ms.XButton2 == ButtonState.Pressed ? true : false;

      int wheel = ms.ScrollWheelValue > m_lastScroll ? -1 : (ms.ScrollWheelValue < m_lastScroll ? 1 : 0);
      m_lastScroll = ms.ScrollWheelValue;

      Squid.Gui.SetMouse(ms.X, ms.Y, wheel);
      Squid.Gui.SetButtons(m_pressedMouseButtons);
      Squid.Gui.TimeElapsed = (float) time.ElapsedGameTime.Milliseconds;
    }
  }
}
