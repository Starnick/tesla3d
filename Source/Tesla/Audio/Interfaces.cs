﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Audio.Implementation;

#nullable enable

namespace Tesla.Audio
{
  public interface IAudioSystem : IDisposableEngineService
  {
    /// <summary>
    /// Gets the current <see cref="IAudioSystem"/> registered to the <see cref="Engine.Instance"/>.
    /// </summary>
    /// <exception cref="ArgumentNullException">Thrown if <see cref="Engine.Initialize"/> has yet to be called -or- the current instance is null.</exception>
    public static IAudioSystem Current { get { return CoreServices.AudioSystem.ServiceOrThrow; } }

    String Platform { get; }

    float MasterVolume { get; set; }
    float SpeedOfSound { get; set; }
    float DistanceScale { get; set; }
    float DopplerScale { get; set; }

    bool IsReverberationEffectEnabled { get; set; }
    bool IsSpatialAudioEnabled { get; set; }

    void SetReverberationEffect(ReverberationParameters effectParams);
    T GetImplementationFactory<T>() where T : IAudioResourceImplFactory;
    bool TryGetImplementationFactory<T>(out T implementationFactory) where T : IAudioResourceImplFactory;
    bool IsSupported<T>() where T : AudioResource;
  }

  public interface ISoundEffectInstance : IDisposable
  {
    float Volume { get; set; }
    float Pan { get; set; }
    float Pitch { get; set; }

    SoundState State { get; }
    SoundEffect Parent { get; }
    IAudioSystem AudioSystem { get; }

    bool IsLooped { get; set; }
    bool IsDisposed { get; }

    void Apply3D(AudioListener listener, AudioEmitter emitter);

    void Play();
    void Pause();
    void Resume();
    void Stop(bool immediate = true);
  }
}
