﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System.Runtime.InteropServices;

#nullable enable

namespace Tesla.Audio
{
  /// <summary>
  /// Parameters for defining reverberation audio effects. Based on the Interactive 3D Audio Rendering Guidelines Level 2.0 (I3DL2).
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 4)]
  public struct ReverberationParameters
  {
    /// <summary>
    /// Percentage of the output that will be reverberation. Permitted range is between 0 and 100.
    /// </summary>
    public float WetDryMix;

    /// <summary>
    /// Attenuation of the room effect. Permitted range is between -10000 and 0 mB (millibels).
    /// </summary>
    public int Room;

    /// <summary>
    /// Attenuation of the room high-frequency effect. Permitted range is between -10000 and 0 mB (millibels). 
    /// </summary>
    public int RoomHF;

    /// <summary>
    /// Roll off factor for the reflected signals. Permitted range is from 0.0 and 10.0.
    /// </summary>
    public float RoomRollOffFactor;

    /// <summary>
    /// Reverberation decay time at low frequencies. Permitted range is between 0.1 and 20.0 seconds.
    /// </summary>
    public float DecayTime;

    /// <summary>
    /// Ratio of the decay time at high frequencies to the decay time at low frequencies.Permitted range is between
    /// 0.1 and 2.0.
    /// </summary>
    public float DecayHFRatio;

    /// <summary>
    /// Attenuation of early reflections relative to <see cref="Room"/>. Permitted range is between -10000 and 1000 mB (millibels).
    /// </summary>
    public int Reflections;

    /// <summary>
    /// Delay time of the first reflection relative to the direct path. Permitted range is between 0.0 and 0.3 seconds.
    /// </summary>
    public float ReflectionsDelay;

    /// <summary>
    /// Attenuation of late reverberation relative to <see cref="Room"/>. Permitted range is between -10000 and 2000 mB (millibels).
    /// </summary>
    public int Reverberation;

    /// <summary>
    /// Time limit between the early reflections and the late reverberation relative to the time of the first
    /// reflection. Permitted range is between 0.0 and 0.1 seconds.
    /// </summary>
    public float ReverberationDelay;

    /// <summary>
    /// Echo density in the late reverberation decay. Permitted range is a percentage between 0 and 100.
    /// </summary>
    public float Diffusion;

    /// <summary>
    /// Modal density in the late reverberation decay. Permitted range is a percentage between 0 and 100.
    /// </summary>
    public float Density;

    /// <summary>
    /// Reference high frequency. Permitted range is between 20.0 and 20000.0 Hz.
    /// </summary>
    public float HFReference;

    /// <summary>
    /// Constructs a new instance of the <see cref="ReverberationParameters"/> struct.
    /// </summary>
    /// <param name="wetDryMix">Percentage of the output that will be reverberation. Permitted range is between 0 and 100.</param>
    /// <param name="room">Attenuation of the room effect. Permitted range is between -10000 and 0 mB (millibels).</param>
    /// <param name="roomHF">Attenuation of the room high-frequency effect. Permitted range is between -10000 and 0 mB (millibels). </param>
    /// <param name="roomRolloffFactor">Roll off factor for the reflected signals. Permitted range is from 0.0 and 10.0.</param>
    /// <param name="decayTime">Reverberation decay time at low frequencies. Permitted range is between 0.1 and 20.0 seconds.</param>
    /// <param name="decayHFRatio">Ratio of the decay time at high frequencies to the decay time at low frequencies.Permitted range is between 0.1 and 2.0.</param>
    /// <param name="reflections">Attenuation of early reflections relative to <see cref="Room"/>. Permitted range is between -10000 and 1000 mB (millibels).</param>
    /// <param name="reflectionsDelay">Delay time of the first reflection relative to the direct path. Permitted range is between 0.0 and 0.3 seconds.</param>
    /// <param name="reverberation">Attenuation of late reverberation relative to <see cref="Room"/>. Permitted range is between -10000 and 2000 mB (millibels).</param>
    /// <param name="reverberationDelay">Time limit between the early reflections and the late reverberation relative to the time of the first reflection. Permitted range is between 0.0 and 0.1 seconds.</param>
    /// <param name="diffusion">Echo density in the late reverberation decay. Permitted range is a percentage between 0 and 100.</param>
    /// <param name="density">Modal density in the late reverberation decay. Permitted range is a percentage between 0 and 100.</param>
    /// <param name="hfReference">Reference high frequency. Permitted range is between 20.0 and 20000.0 Hz.</param>
    public ReverberationParameters(float wetDryMix, int room, int roomHF, float roomRolloffFactor, float decayTime,
        float decayHFRatio, int reflections, float reflectionsDelay, int reverberation, float reverberationDelay,
        float diffusion, float density, float hfReference)
    {
      WetDryMix = wetDryMix;
      Room = room;
      RoomHF = roomHF;
      RoomRollOffFactor = roomRolloffFactor;
      DecayTime = decayTime;
      DecayHFRatio = decayHFRatio;
      Reflections = reflections;
      ReflectionsDelay = reflectionsDelay;
      Reverberation = reverberation;
      ReverberationDelay = reverberationDelay;
      Diffusion = diffusion;
      Density = density;
      HFReference = hfReference;
    }

    #region Predefined Parameters

    /// <summary>
    /// Default reverberation parameters.
    /// </summary>
    public static ReverberationParameters Default
    {
      get
      {
        return new ReverberationParameters(100, -10000, 0, 0.0f, 1.00f, 0.50f, -10000, 0.020f, -10000, 0.040f, 100.0f, 100.0f, 5000.0f);
      }
    }

    /// <summary>
    /// Generic referberation parameters.
    /// </summary>
    public static ReverberationParameters Generic
    {
      get
      {
        return new ReverberationParameters(100, -1000, -100, 0.0f, 1.49f, 0.83f, -2602, 0.007f, 200, 0.011f, 100.0f, 100.0f, 5000.0f);
      }
    }

    /// <summary>
    /// Reverberation parameters for a Padded Cell.
    /// </summary>
    public static ReverberationParameters PaddedCell
    {
      get
      {
        return new ReverberationParameters(100, -1000, -6000, 0.0f, 0.17f, 0.10f, -1204, 0.001f, 207, 0.002f, 100.0f, 100.0f, 5000.0f);
      }
    }

    /// <summary>
    /// Reverberation parameters for a (Standard) room.
    /// </summary>
    public static ReverberationParameters StandardRoom
    {
      get
      {
        return new ReverberationParameters(100, -1000, -454, 0.0f, 0.40f, 0.83f, -1646, 0.002f, 53, 0.003f, 100.0f, 100.0f, 5000.0f);
      }
    }

    /// <summary>
    /// Reverberation parameters for a Bath room.
    /// </summary>
    public static ReverberationParameters BathRoom
    {
      get
      {
        return new ReverberationParameters(100, -1000, -1200, 0.0f, 1.49f, 0.54f, -370, 0.007f, 1030, 0.011f, 100.0f, 60.0f, 5000.0f);
      }
    }

    /// <summary>
    /// Reverberation parameters for a Living room.
    /// </summary>
    public static ReverberationParameters LivingRoom
    {
      get
      {
        return new ReverberationParameters(100, -1000, -6000, 0.0f, 0.50f, 0.10f, -1376, 0.003f, -1104, 0.004f, 100.0f, 100.0f, 5000.0f);
      }
    }

    /// <summary>
    /// Reverberation parameters for a Stone room.
    /// </summary>
    public static ReverberationParameters StoneRoom
    {
      get
      {
        return new ReverberationParameters(100, -1000, -300, 0.0f, 2.31f, 0.64f, -711, 0.012f, 83, 0.017f, 100.0f, 100.0f, 5000.0f);
      }
    }

    /// <summary>
    /// Reverberation parameters for an auditorium.
    /// </summary>
    public static ReverberationParameters Auditorium
    {
      get
      {
        return new ReverberationParameters(100, -1000, -476, 0.0f, 4.32f, 0.59f, -789, 0.020f, -289, 0.030f, 100.0f, 100.0f, 5000.0f);
      }
    }

    /// <summary>
    /// Reverberation parameters for a concert hall.
    /// </summary>
    public static ReverberationParameters ConcertHall
    {
      get
      {
        return new ReverberationParameters(100, -1000, -500, 0.0f, 3.92f, 0.70f, -1230, 0.020f, -2, 0.029f, 100.0f, 100.0f, 5000.0f);
      }
    }

    /// <summary>
    /// Reverberation parameters for a cave.
    /// </summary>
    public static ReverberationParameters Cave
    {
      get
      {
        return new ReverberationParameters(100, -1000, 0, 0.0f, 2.91f, 1.30f, -602, 0.015f, -302, 0.022f, 100.0f, 100.0f, 5000.0f);
      }
    }

    /// <summary>
    /// Reverberation parameters for an arena.
    /// </summary>
    public static ReverberationParameters Arena
    {
      get
      {
        return new ReverberationParameters(100, -1000, -698, 0.0f, 7.24f, 0.33f, -1166, 0.020f, 16, 0.030f, 100.0f, 100.0f, 5000.0f);
      }
    }

    /// <summary>
    /// Reverberation parameters for a hanger.
    /// </summary>
    public static ReverberationParameters Hanger
    {
      get
      {
        return new ReverberationParameters(100, -1000, -1000, 0.0f, 10.05f, 0.23f, -602, 0.020f, 198, 0.030f, 100.0f, 100.0f, 5000.0f);
      }
    }

    /// <summary>
    /// Reverberation parameters for a carpeted hallway.
    /// </summary>
    public static ReverberationParameters CarpetedHallway
    {
      get
      {
        return new ReverberationParameters(100, -1000, -4000, 0.0f, 0.30f, 0.10f, -1831, 0.002f, -1630, 0.030f, 100.0f, 100.0f, 5000.0f);
      }
    }

    /// <summary>
    /// Reverberation parameters for a (standard) hallway.
    /// </summary>
    public static ReverberationParameters Hallway
    {
      get
      {
        return new ReverberationParameters(100, -1000, -300, 0.0f, 1.49f, 0.59f, -1219, 0.007f, 441, 0.011f, 100.0f, 100.0f, 5000.0f);
      }
    }

    /// <summary>
    /// Reverberation parameters for a stone corridor.
    /// </summary>
    public static ReverberationParameters StoneCorridor
    {
      get
      {
        return new ReverberationParameters(100, -1000, -237, 0.0f, 2.70f, 0.79f, -1214, 0.013f, 395, 0.020f, 100.0f, 100.0f, 5000.0f);
      }
    }

    /// <summary>
    /// Reverberation parameters for an alley way.
    /// </summary>
    public static ReverberationParameters Alley
    {
      get
      {
        return new ReverberationParameters(100, -1000, -270, 0.0f, 1.49f, 0.86f, -1204, 0.007f, -4, 0.011f, 100.0f, 100.0f, 5000.0f);
      }
    }

    /// <summary>
    /// Reverberation parameters for a forest.
    /// </summary>
    public static ReverberationParameters Forest
    {
      get
      {
        return new ReverberationParameters(100, -1000, -3300, 0.0f, 1.49f, 0.54f, -2560, 0.162f, -613, 0.088f, 79.0f, 100.0f, 5000.0f);
      }
    }

    /// <summary>
    /// Reverberation parameters for a city.
    /// </summary>
    public static ReverberationParameters City
    {
      get
      {
        return new ReverberationParameters(100, -1000, -800, 0.0f, 1.49f, 0.67f, -2273, 0.007f, -2217, 0.011f, 50.0f, 100.0f, 5000.0f);
      }
    }

    /// <summary>
    /// Reverberation parameters for a mountain.
    /// </summary>
    public static ReverberationParameters Mountains
    {
      get
      {
        return new ReverberationParameters(100, -1000, -2500, 0.0f, 1.49f, 0.21f, -2780, 0.300f, -2014, 0.100f, 27.0f, 100.0f, 5000.0f);
      }
    }

    /// <summary>
    /// Reverberation parameters for a quarry.
    /// </summary>
    public static ReverberationParameters Quarry
    {
      get
      {
        return new ReverberationParameters(100, -1000, -1000, 0.0f, 1.49f, 0.83f, -10000, 0.061f, 500, 0.025f, 100.0f, 100.0f, 5000.0f);
      }
    }

    /// <summary>
    /// Reverberation parameters for a plain.
    /// </summary>
    public static ReverberationParameters Plain
    {
      get
      {
        return new ReverberationParameters(100, -1000, -2000, 0.0f, 1.49f, 0.50f, -2466, 0.179f, -2514, 0.100f, 21.0f, 100.0f, 5000.0f);
      }
    }

    /// <summary>
    /// Reverberation parameters for a parking lot.
    /// </summary>
    public static ReverberationParameters ParkingLot
    {
      get
      {
        return new ReverberationParameters(100, -1000, 0, 0.0f, 1.65f, 1.50f, -1363, 0.008f, -1153, 0.012f, 100.0f, 100.0f, 5000.0f);
      }
    }

    /// <summary>
    /// Reverberation parameters for a sewer pipe.
    /// </summary>
    public static ReverberationParameters SewerPipe
    {
      get
      {
        return new ReverberationParameters(100, -1000, -1000, 0.0f, 2.81f, 0.14f, 429, 0.014f, 648, 0.021f, 80.0f, 60.0f, 5000.0f);
      }
    }

    /// <summary>
    /// Reverberation parameters for under water.
    /// </summary>
    public static ReverberationParameters UnderWater
    {
      get
      {
        return new ReverberationParameters(100, -1000, -4000, 0.0f, 1.49f, 0.10f, -449, 0.007f, 1700, 0.011f, 100.0f, 100.0f, 5000.0f);
      }
    }

    /// <summary>
    /// Reverberation parameters for a small room.
    /// </summary>
    public static ReverberationParameters SmallRoom
    {
      get
      {
        return new ReverberationParameters(100, -1000, -600, 0.0f, 1.10f, 0.83f, -400, 0.005f, 500, 0.010f, 100.0f, 100.0f, 5000.0f);
      }
    }

    /// <summary>
    /// Reverberation parameters for a medium sized room.
    /// </summary>
    public static ReverberationParameters MediumRoom
    {
      get
      {
        return new ReverberationParameters(100, -1000, -600, 0.0f, 1.30f, 0.83f, -1000, 0.010f, -200, 0.020f, 100.0f, 100.0f, 5000.0f);
      }
    }

    /// <summary>
    /// Reverberation parameters for a large sized room.
    /// </summary>
    public static ReverberationParameters LargeRoom
    {
      get
      {
        return new ReverberationParameters(100, -1000, -600, 0.0f, 1.50f, 0.83f, -1600, 0.020f, -1000, 0.040f, 100.0f, 100.0f, 5000.0f);
      }
    }

    /// <summary>
    /// Reverberation parameters for a medium sized hall.
    /// </summary>
    public static ReverberationParameters MediumHall
    {
      get
      {
        return new ReverberationParameters(100, -1000, -600, 0.0f, 1.80f, 0.70f, -1300, 0.015f, -800, 0.030f, 100.0f, 100.0f, 5000.0f);
      }
    }

    /// <summary>
    /// Reverberation parameters for a large sized hall.
    /// </summary>
    public static ReverberationParameters LargeHall
    {
      get
      {
        return new ReverberationParameters(100, -1000, -600, 0.0f, 1.80f, 0.70f, -2000, 0.030f, -1400, 0.060f, 100.0f, 100.0f, 5000.0f);
      }
    }

    /// <summary>
    /// Reverberation parameters for a plate.
    /// </summary>
    public static ReverberationParameters Plate
    {
      get
      {
        return new ReverberationParameters(100, -1000, -200, 0.0f, 1.30f, 0.90f, 0, 0.002f, 0, 0.010f, 100.0f, 75.0f, 5000.0f);
      }
    }

    #endregion
  }
}
