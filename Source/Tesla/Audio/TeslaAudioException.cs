﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Runtime.Serialization;

#nullable enable

namespace Tesla.Audio
{
  /// <summary>
  /// Engine exception for an error related to the audio system.
  /// </summary>
  public class TeslaAudioException : TeslaException
  {
    /// <summary>
    /// Constructs a new instance of the <see cref="TeslaAudioException"/> class.
    /// </summary>
    public TeslaAudioException() : base() { }

    /// <summary>
    /// Constructs a new instance of the <see cref="TeslaAudioException"/> class.
    /// </summary>
    /// <param name="msg">Error message that explains the reason for the exception</param>
    public TeslaAudioException(string? msg) : base(msg) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="TeslaAudioException"/> class.
    /// </summary>
    /// <param name="paramName">Parameter name that caused the exception.</param>
    /// <param name="msg">Error message that explains the reason for the exception.</param>
    public TeslaAudioException(string paramName, string? msg) : base(paramName, msg) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="TeslaAudioException"/> class.
    /// </summary>
    /// <param name="msg">Error message that explains the reason for the exception.</param>
    /// <param name="innerException">Exception that caused this exception to be thrown, generally a more specific exception.</param>
    public TeslaAudioException(string? msg, Exception? innerException) : base(msg, innerException) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="TeslaAudioException"/> class.
    /// </summary>
    /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that holds the serialized object data about the exception being thrown.</param>
    /// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains contextual information about the source or destination.</param>
    public TeslaAudioException(SerializationInfo info, StreamingContext context) : base(info, context) { }
  }
}
