﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;
using Tesla.Audio.Implementation;
using Tesla.Graphics.Implementation;

#nullable enable

namespace Tesla.Audio
{
  /// <summary>
  /// Base class for all audio resources that are created and managed by an audio system.
  /// </summary>
  public abstract class AudioResource : IDisposable, INamable
  {
    private IAudioResourceImpl? m_impl;
    private object? m_tag;

    /// <summary>
    /// Occurs when the audio resource is disposed.
    /// </summary>
    public event TypedEventHandler<AudioResource>? Disposing;

    /// <summary>
    /// Gets or sets the name of the object.
    /// </summary>
    [AllowNull]
    public String Name
    {
      get
      {
        if (m_impl is not null)
          return m_impl.Name;

        return String.Empty;
      }
      set
      {
        if (m_impl is not null)
          m_impl.Name = value ?? String.Empty;
      }
    }

    /// <summary>
    /// Gets or sets custom data.
    /// </summary>
    public object? Tag
    {
      get
      {
        return m_tag;
      }
      set
      {
        m_tag = value;
      }
    }

    /// <summary>
    /// Gets if this resource has been disposed or not.
    /// </summary>
    public bool IsDisposed
    {
      get
      {
        if (m_impl is null)
          return true;

        return m_impl.IsDisposed;
      }
    }

    /// <summary>
    /// Gets the ID of this resource.
    /// </summary>
    public int ResourceID
    {
      get
      {
        if (m_impl is null)
          return -1;

        return m_impl.ResourceID;
      }
    }

    /// <summary>
    /// Gets the audio system that created and managed this resource.
    /// </summary>
    public IAudioSystem AudioSystem
    {
      get
      {
        if (m_impl is null)
          return null!;

        return m_impl.AudioSystem;
      }
    }

    /// <summary>
    /// Gets the underlying platform-specific implementation of this resource.
    /// </summary>
    public IAudioResourceImpl Implementation
    {
      get
      {
        return m_impl!;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="AudioResource"/> class.
    /// </summary>
    protected AudioResource() { }

    /// <summary>
    /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
    /// </summary>
    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
    public override int GetHashCode()
    {
      return m_impl?.ResourceID ?? 0;
    }

    /// <summary>
    /// Releases unmanaged and - optionally - managed resources.
    /// </summary>
    /// <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
    protected virtual void Dispose(bool disposing)
    {
      if (m_impl is not null && !m_impl.IsDisposed)
      {
        if (disposing)
        {
          OnDispose();

          m_impl.Dispose();
        }
      }
    }

    /// <summary>
    /// Binds an implementation to the current audio resource. An implementation can only be bound to a single audio resource.
    /// </summary>
    /// <param name="implementor">Audio resource implementation</param>
    /// <exception cref="ArgumentNullException">Thrown if the implementation is null.</exception>
    protected void BindImplementation(IAudioResourceImpl implementor)
    {
      if (implementor is null)
        throw new ArgumentNullException(nameof(implementor), StringLocalizer.Instance.GetLocalizedString("CannotBindNullImplementation"));

      // Dispose of the old implementation
      string? oldName = null;
      if (m_impl is not null)
      {
        oldName = m_impl.Name;
        m_impl.Dispose();
      }

      m_impl = implementor;
      m_impl.BindImplementation(this);

      if (oldName is not null)
        m_impl.Name = oldName;
    }

    /// <summary>
    /// Checks if the resource was disposed and if so, throws an ObjectDisposedException.
    /// </summary>
    /// <exception cref="ObjectDisposedException">If resource has been disposed already.</exception>
    protected void CheckDisposed()
    {
      if (IsDisposed)
        throw new ObjectDisposedException(GetType().FullName);
    }

    /// <summary>
    /// Called right before the implementation is disposed.
    /// </summary>
    protected virtual void OnDispose()
    {
      TypedEventHandler<AudioResource>? handler = Disposing;
      if (handler is not null)
        handler(this, EventArgs.Empty);
    }

    /// <summary>
    /// Dangerously casts the current implementation as the target type. This may be null if not the appropiate type.
    /// </summary>
    /// <typeparam name="T">Concrete implementation type.</typeparam>
    /// <returns>Concrete implementation as the type.</returns>
    protected T GetImplAs<T>() where T : class, IAudioResourceImpl
    {
      return (m_impl as T)!;
    }
  }
}
