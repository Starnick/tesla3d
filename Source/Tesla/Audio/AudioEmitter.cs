﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Tesla.Content;

#nullable enable

namespace Tesla.Audio
{
  /// <summary>
  /// Represents a 3D audio emitter to simulate 3D audio effects.
  /// </summary>
  [Serializable]
  [StructLayout(LayoutKind.Sequential, Pack = 4)]
  public struct AudioEmitter : IEquatable<AudioEmitter>, IRefEquatable<AudioEmitter>, IFormattable, IPrimitiveValue
  {
    /// <summary>
    /// Scalar applied to the level of Doppler Effect calculated between the emitter and any audio listener.
    /// </summary>
    public float DopplerScale;

    /// <summary>
    /// Forward orientation vector for the emitter.
    /// </summary>
    public Vector3 Forward;

    /// <summary>
    /// Up orientation vector for the emitter.
    /// </summary>
    public Vector3 Up;

    /// <summary>
    /// Velocity vector of the emitter.
    /// </summary>
    public Vector3 Velocity;

    /// <summary>
    /// Position of the emitter.
    /// </summary>
    public Vector3 Position;

    private static readonly AudioEmitter s_zero = new AudioEmitter(1.0f, Vector3.Forward, Vector3.Up, Vector3.Zero, Vector3.Zero);
    private static readonly int s_sizeInBytes = BufferHelper.SizeOf<AudioEmitter>();

    /// <summary>
    /// Gets the default audio emitter centered at the world origin with a Forward axis of {0, 0, -1} and Up axis of {0, 0, 0}
    /// with a doppler scale of 1.0f.
    /// </summary>
    public static ref readonly AudioEmitter Zero
    {
      get
      {
        return ref s_zero;
      }
    }

    /// <summary>
    /// Gets the size of the <see cref="AudioEmitter"/> type in bytes.
    /// </summary>
    public static int SizeInBytes
    {
      get
      {
        return s_sizeInBytes;
      }
    }

    /// <summary>
    /// Gets whether the axes are normalized.
    /// </summary>
    public readonly bool IsNormalized
    {
      get
      {
        return Forward.IsNormalized && Up.IsNormalized;
      }
    }

    /// <summary>
    /// Gets whether any of the axes are degenerate (equal to zero).
    /// </summary>
    public readonly bool IsDegenerate
    {
      get
      {
        return Forward.IsAlmostZero() || Up.IsAlmostZero();
      }
    }

    /// <summary>
    /// Gets whether any of the components of the axes are NaN (Not A Number).
    /// </summary>
    public readonly bool IsNaN
    {
      get
      {
        return float.IsNaN(DopplerScale) || Forward.IsNaN || Up.IsNaN || Velocity.IsNaN || Position.IsNaN;
      }
    }

    /// <summary>
    /// Gets whether any of the components of the axes are positive or negative infinity.
    /// </summary>
    public readonly bool IsInfinity
    {
      get
      {
        return float.IsInfinity(DopplerScale) || Forward.IsInfinity || Up.IsInfinity || Velocity.IsInfinity || Position.IsInfinity;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="AudioEmitter"/> struct.
    /// </summary>
    /// <param name="dopplerScale">Doppler scalar.</param>
    /// <param name="forward">Forward orientation vector.</param>
    /// <param name="up">Up orientation vector.</param>
    /// <param name="velocity">Velocity vector.</param>
    /// <param name="position">Position in the world.</param>
    public AudioEmitter(float dopplerScale, in Vector3 forward, in Vector3 up, in Vector3 velocity, in Vector3 position)
    {
      DopplerScale = dopplerScale;
      Forward = forward;
      Up = up;
      Velocity = velocity;
      Position = position;
    }

    /// <summary>
    /// Determines whether the specified <see cref="System.Object" /> is equal to this instance.
    /// </summary>
    /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
    /// <returns>True if the specified <see cref="System.Object" /> is equal to this instance; otherwise, false.</returns>
    public readonly override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is AudioEmitter)
        return Equals((AudioEmitter) obj);

      return false;
    }

    /// <summary>
    /// Tests equality between this emitter and another.
    /// </summary>
    /// <param name="other">Emitter</param>
    /// <returns>True if equal, false otherwise.</returns>
    readonly bool IEquatable<AudioEmitter>.Equals(AudioEmitter other)
    {
      return (MathF.Abs(other.DopplerScale - DopplerScale) <= MathHelper.ZeroTolerance) && other.Forward.Equals(Forward) && other.Up.Equals(Up)
          && other.Velocity.Equals(Velocity) && other.Position.Equals(Position);
    }

    /// <summary>
    /// Tests equality between this emitter and another.
    /// </summary>
    /// <param name="other">Emitter</param>
    /// <returns>True if equal, false otherwise.</returns>
    public readonly bool Equals(in AudioEmitter other)
    {
      return (MathF.Abs(other.DopplerScale - DopplerScale) <= MathHelper.ZeroTolerance) && other.Forward.Equals(Forward) && other.Up.Equals(Up)
          && other.Velocity.Equals(Velocity) && other.Position.Equals(Position);
    }

    /// <summary>
    /// Tests equality between this emitter and another.
    /// </summary>
    /// <param name="other">Emitter</param>
    /// <param name="tolerance">Tolerance</param>
    /// <returns>True if equal, false otherwise.</returns>
    public readonly bool Equals(in AudioEmitter other, float tolerance)
    {
      return (MathF.Abs(other.DopplerScale - DopplerScale) <= tolerance) && other.Forward.Equals(Forward, tolerance) && other.Up.Equals(Up, tolerance)
          && other.Velocity.Equals(Velocity, tolerance) && other.Position.Equals(Position, tolerance);
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
    public readonly override int GetHashCode()
    {
      unchecked
      {
        return DopplerScale.GetHashCode() + Forward.GetHashCode() + Up.GetHashCode() + Velocity.GetHashCode() + Position.GetHashCode();
      }
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly override String ToString()
    {
      CultureInfo info = CultureInfo.CurrentCulture;
      return String.Format(info, "DopplerScale: {0}, Forward: [{1}], Up: [{2}], Velocity: [{3}], Position: [{4}]",
          new Object[] { DopplerScale.ToString(info), Forward.ToString(info), Up.ToString(info), Velocity.ToString(info), Position.ToString(info) });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="format">The format</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly String ToString(String? format)
    {
      if (format == null)
        return ToString();

      CultureInfo info = CultureInfo.CurrentCulture;
      return String.Format(info, "DopplerScale: {0}, Forward: [{1}], Up: [{2}], Velocity: [{3}], Position: [{4}]",
          new Object[] { DopplerScale.ToString(format, info), Forward.ToString(format, info), Up.ToString(format, info), Velocity.ToString(format, info), Position.ToString(format, info) });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="formatProvider">The format provider.</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly String ToString(IFormatProvider? formatProvider)
    {
      if (formatProvider == null)
        return ToString();

      return String.Format(formatProvider, "DopplerScale: {0}, Forward: [{1}], Up: [{2}], Velocity: [{3}], Position: [{4}]",
          new Object[] { DopplerScale.ToString(formatProvider), Forward.ToString(formatProvider), Up.ToString(formatProvider), Velocity.ToString(formatProvider), Position.ToString(formatProvider) });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="format">The format.</param>
    /// <param name="formatProvider">The format provider.</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly String ToString(String? format, IFormatProvider? formatProvider)
    {
      if (formatProvider == null)
        return ToString();

      if (format == null)
        return ToString(formatProvider);

      return String.Format(formatProvider, "DopplerScale: {0}, Forward: [{1}], Up: [{2}], Velocity: [{3}], Position: [{4}]",
          new Object[] { DopplerScale.ToString(format, formatProvider), Forward.ToString(format, formatProvider), Up.ToString(format, formatProvider), Velocity.ToString(format, formatProvider), Position.ToString(format, formatProvider) });
    }

    /// <summary>
    /// Writes the primitive data to the output.
    /// </summary>
    /// <param name="output">Primitive writer.</param>
    readonly void IPrimitiveValue.Write(IPrimitiveWriter output)
    {
      output.Write("DopplerScale", DopplerScale);
      output.Write<Vector3>("Forward", Forward);
      output.Write<Vector3>("Up", Up);
      output.Write<Vector3>("Velocity", Velocity);
      output.Write<Vector3>("Position", Position);
    }

    /// <summary>
    /// Reads the primitive data from the input.
    /// </summary>
    /// <param name="input">Primitive reader.</param>
    void IPrimitiveValue.Read(IPrimitiveReader input)
    {
      DopplerScale = input.ReadSingle("DopplerScale");
      input.Read<Vector3>("Forward", out Forward);
      input.Read<Vector3>("Up", out Up);
      input.Read<Vector3>("Velocity", out Velocity);
      input.Read<Vector3>("Position", out Position);
    }
  }
}
