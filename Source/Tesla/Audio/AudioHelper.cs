﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#nullable enable

namespace Tesla.Audio
{
  /// <summary>
  /// General audio helper.
  /// </summary>
  public static class AudioHelper
  {
    /// <summary>
    /// Gets an audio system from the service provider.
    /// </summary>
    /// <param name="serviceProvider">Service provider.</param>
    /// <returns>Audio system that the service provider owns.</returns>
    /// <exception cref="ArgumentNullException">Thrown if no audio system was found in the service provider.</exception>
    public static IAudioSystem GetAudioSystem(IServiceProvider serviceProvider)
    {
      IAudioSystem? audioSystem = serviceProvider?.GetService(typeof(IAudioSystem)) as IAudioSystem;
      ThrowIfAudioSystemNull(audioSystem);

      return audioSystem;
    }

    /// <summary>
    /// Throws if the audio system argument is null.
    /// </summary>
    /// <param name="audioSystem">Audio system.</param>
    /// <exception cref="ArgumentNullException">Thrown if the argument is null.</exception>
    public static void ThrowIfAudioSystemNull([NotNull] IAudioSystem? audioSystem)
    {
      if (audioSystem is null)
        throw new ArgumentNullException(nameof(audioSystem), StringLocalizer.Instance.GetLocalizedString("AudioSystemNull"));
    }
  }
}
