﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Audio.Implementation;

#nullable enable

namespace Tesla.Audio
{
  /// <summary>
  /// Represents a loaded sound resource. Sound effects allow simple "fire-and-forget" playing of audio or to create
  /// instances of the sound effect that share audio resources and allow for advanced playback.
  /// </summary>
  public class SoundEffect : AudioResource
  {
    #region Public Properties

    /// <summary>
    /// Gets the duration of the sound effect.
    /// </summary>
    public TimeSpan Duration
    {
      get
      {
        return SoundEffectImpl.Duration;
      }
    }

    /// <summary>
    /// Gets the audio data of the sound effect.
    /// </summary>
    public IReadOnlyDataBuffer AudioData
    {
      get
      {
        return SoundEffectImpl.AudioData;
      }
    }

    #endregion

    //For casting purposes
    private ISoundEffectImpl SoundEffectImpl
    {
      get
      {
        return GetImplAs<ISoundEffectImpl>();
      }
      set
      {
        BindImplementation(value);
      }
    }

    /// <summary>
    /// For ISavable only.
    /// </summary>
    protected SoundEffect() { }

    public SoundEffect(IAudioSystem audioSystem, IReadOnlyDataBuffer audioData)
    {
      AudioHelper.ThrowIfAudioSystemNull(audioSystem);
      ArgumentNullException.ThrowIfNull(audioData, nameof(audioData));

      ISoundEffectImplFactory factory;
      if (!audioSystem.TryGetImplementationFactory<ISoundEffectImplFactory>(out factory))
        throw AudioExceptionHelper.NewUnsupportedFeatureException(audioSystem, typeof(SoundEffect));

      try
      {
        SoundEffectImpl = factory.CreateImplementation(audioData);
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw AudioExceptionHelper.NewErrorCreatingImplementation(typeof(SoundEffect), e);
      }
    }

    /// <summary>
    /// Plays a "fire-and-forget" sound.
    /// </summary>
    /// <returns>True if the sound effect successfully began to play, false if not.</returns>
    /// <exception cref="ObjectDisposedException">Thrown if the resource has already been disposed.</exception>
    /// <exception cref="TeslaAudioException">Thrown if an error occurs while reading from the resource.</exception>
    /// <remarks>See implementors for details on specific exceptions that can be thrown.</remarks>
    public bool Play()
    {
      CheckDisposed();

      try
      {
        return SoundEffectImpl.Play(1.0f, 0.0f, 0.0f);
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw new TeslaAudioException(StringLocalizer.Instance.GetLocalizedString("ErrorPlayingSound"), e);
      }
    }

    /// <summary>
    /// Plays a "fire-and-forget" sound.
    /// </summary>
    /// <param name="volume">Volume ranging from 0.0f (silence) to 1.0f (full volume), relative to the sound system's master volume parameter.</param>
    /// <param name="pitch">Pitch adjustment ranging from -1.0f (down one octave) to 1.0f (up one octave). 0.0f is unity (normal) pitch.</param>
    /// <param name="pan">Panning, ranging from -1.0f (full left) to 1.0f (full right) where 0.0f is centered.</param>
    /// <returns>True if the sound effect successfully began to play, false if not.</returns>
    /// <exception cref="ObjectDisposedException">Thrown if the resource has already been disposed.</exception>
    /// <exception cref="TeslaAudioException">Thrown if an error occurs while reading from the resource.</exception>
    /// <remarks>See implementors for details on specific exceptions that can be thrown.</remarks>
    public bool Play(float volume, float pitch, float pan)
    {
      CheckDisposed();

      try
      {
        return SoundEffectImpl.Play(volume, pitch, pan);
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw new TeslaAudioException(StringLocalizer.Instance.GetLocalizedString("ErrorPlayingSound"), e);
      }
    }

    /// <summary>
    /// Creates a new instance for this sound effect, allowing access to more advanced playback features, such as 3D audio.
    /// </summary>
    /// <returns>Sound effect instance.</returns>
    /// <exception cref="ObjectDisposedException">Thrown if the resource has already been disposed.</exception>
    /// <exception cref="TeslaAudioException">Thrown if an error occurs in creating the instance.</exception>
    /// <remarks>See implementors for details on specific exceptions that can be thrown.</remarks>
    public ISoundEffectInstance CreateInstance()
    {
      CheckDisposed();
      try
      {
        return SoundEffectImpl.CreateInstance();
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw new TeslaAudioException(StringLocalizer.Instance.GetLocalizedString("ErrorCreatingSoundEffectInstance"), e);
      }
    }
  }
}
