﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Collections.Specialized;
using System.Diagnostics.CodeAnalysis;

#nullable enable

namespace Tesla.Collections
{
  /// <summary>
  /// Represents a collection of features that maps an index to each feature, so it can be used in operations such as bitflags. Similar to a set,
  /// the object itself is used as a key to ensure there is only one instance of it. The order of features are in the same order that they are added.
  /// </summary>
  /// <typeparam name="T">Type of feature, must be hashable or provide a custom equality comparer.</typeparam>
  public class FeatureIndex<T> : IReadOnlyList<FeatureIndex<T>.Entry> where T : notnull
  {
    /// <summary>
    /// Entry inside <see cref="FeatureIndex{T}"/>.
    /// </summary>
    /// <param name="Feature">Feature object.</param>
    /// <param name="Index">Index of feature.</param>
    public readonly record struct Entry(T Feature, int Index);

    private IndexedList<T, Entry> m_features;

    /// <summary>
    /// Gets the number of features represented.
    /// </summary>
    public int Count { get { return m_features.Count; } }

    /// <summary>
    /// Gets the feature by index.
    /// </summary>
    /// <param name="index">Index of the feature.</param>
    /// <returns>Feature object.</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the index is out of range.</exception>
    public T this[int index] { get { return m_features[index].Feature; } }

    /// <inheritdoc />
    FeatureIndex<T>.Entry IReadOnlyList<FeatureIndex<T>.Entry>.this[int index] { get { return m_features[index]; } }

    /// <summary>
    /// Constructs a new instance of the <see cref="FeatureIndex{T}"/> class.
    /// </summary>
    /// <param name="comparer">Optional comparer for equality / hashing.</param>
    public FeatureIndex(IEqualityComparer<T>? comparer = null)
    {
      m_features = new IndexedList<T, Entry>(comparer);
    }


    /// <summary>
    /// Constructs a new instance of the <see cref="FeatureIndex{T}"/> class.
    /// </summary>
    /// <param name="capacity">Initial capacity of the list.</param>
    /// <param name="comparer">Optional comparer for equality / hashing.</param>
    public FeatureIndex(int capacity, IEqualityComparer<T>? comparer = null)
    {
      m_features = new IndexedList<T, Entry>(capacity, comparer);
    }

    /// <summary>
    /// Copies the features to a new array.
    /// </summary>
    /// <returns>Array of features.</returns>
    public T[] ToArray()
    {
      T[] values = BufferHelper.NewArray<T>(m_features.Count);
      for (int i = 0; i < m_features.Count; i++)
        values[i] = m_features[i].Feature;

      return values;
    }

    /// <summary>
    /// Creates a <see cref="ImmutableArray{T}"/> from the contents of the <see cref="FeatureIndex{T}"/>.
    /// </summary>
    /// <returns>Immutable array of values.</returns>
    public ImmutableArray<T> ToImmutableArray()
    {
      ImmutableArray<T>.Builder builder = ImmutableArray.CreateBuilder<T>(m_features.Count);
      for (int i = 0; i < m_features.Count; i++)
        builder.Add(m_features[i].Feature);

      return builder.ToImmutable();
    }

    /// <summary>
    /// Adds the feature to the index map and returns the index. If the feature is already present, returns the index to the existing feature.
    /// </summary>
    /// <param name="feature">Feature</param>
    /// <returns>Index of the feature.</returns>
    public int Add(T feature)
    {
      if (m_features.TryGetValue(feature, out Entry entry))
        return entry.Index;

      int index = m_features.Count;
      m_features.Add(feature, new Entry(feature, index));

      return index;
    }

    /// <summary>
    /// Adds multiple features to the index map.
    /// </summary>
    /// <param name="features">Features.</param>
    public void AddRange(IEnumerable<T> features)
    {
      foreach (T feature in features)
        Add(feature);
    }

    /// <summary>
    /// Queries to see if the feature is contained in the index map.
    /// </summary>
    /// <param name="feature">Feature</param>
    /// <returns></returns>
    public bool Contains(T feature)
    {
      return m_features.ContainsKey(feature);
    }

    /// <summary>
    /// Gets the index of the feature. If not known, then returns a value of -1.
    /// </summary>
    /// <param name="feature">Feature to get the index of.</param>
    /// <returns>Index of feature, or -1 if not present.</returns>
    public int IndexOf(T feature)
    {
      return m_features.IndexOfKey(feature);
    }

    /// <summary>
    /// Tries to get the feature corresponding to the specified index.
    /// </summary>
    /// <param name="index">Index of the feature.</param>
    /// <param name="feature">Feature object, if it exists.</param>
    /// <returns>True if the index was valid, false if there is no feature at that index.</returns>
    public bool TryGetFeature(int index, [NotNullWhen(true)] out T? feature)
    {
      if (!BufferHelper.IsInRange(index, m_features.Count))
      {
        feature = default;
        return false;
      }

      feature = m_features[index].Feature;
      return true;
    }

    /// <summary>
    /// Creates a bit array from the specified list of features, and filters out features not present.
    /// </summary>
    /// <param name="features">Features, if any are not present in the index then that feature is skipped.</param>
    /// <returns>BitArray where each bit associated with the feature is enabled.</returns>
    public BitArray CreateBitArray(IEnumerable<T>? features = null)
    {
      BitArray bits = new BitArray(m_features.Count);
      if (features is null)
        return bits;

      foreach (T feature in features)
      {
        int indx = IndexOf(feature);
        if (indx == -1)
          continue;

        bits.Set(indx, true);
      }

      return bits;
    }

    /// <summary>
    /// Creates a bit array from the specified list of feature indices, and filters out features not present.
    /// </summary>
    /// <param name="indices">Indices of features, if any are not found, that feature is skipped.</param>
    /// <returns>BitArray where each bit associated with the feature is enabled.</returns>
    public BitArray CreateBitArray(IReadOnlyList<int> indices)
    {
      uint count = (uint)Count;
      BitArray bits = new BitArray(m_features.Count);
      for (int i = 0; i < indices.Count; i++)
      {
        int indx = indices[i];
        if ((uint)indx >= count)
          continue;

        bits.Set(indx, true);
      }

      return bits;
    }

    /// <summary>
    /// Creates a bit array from the specified list of feature indices, and filters out features not present.
    /// </summary>
    /// <param name="indices">Indices of features, if any are not found, that feature is skipped.</param>
    /// <returns>BitArray where each bit associated with the feature is enabled.</returns>
    public BitArray CreateBitArray(ReadOnlySpan<int> indices)
    {
      uint count = (uint)Count;
      BitArray bits = new BitArray(m_features.Count);
      for (int i = 0; i < indices.Length; i++)
      {
        int indx = indices[i];
        if ((uint)indx >= count)
          continue;

        bits.Set(indx, true);
      }

      return bits;
    }

    /// <summary>
    /// Creates a <see cref="BitVector32"/> from the specified list of feature indices. Only valid if there are 32 values or less.
    /// </summary>
    /// <param name="indices">Indices of features, if any are not found, that feature is skipped.</param>
    /// <returns><see cref="BitVector32"/> containing which features are turned on.</returns>
    /// <exception cref="ArgumentException">Thrown if the <see cref="FeatureIndex{T}"/> has more than 32 values.</exception>
    public BitVector32 CreateBitVector32(IEnumerable<T>? features = null)
    {
      CheckIfCanFitBitVector32();

      BitVector32 bits = new BitVector32();
      if (features is null)
        return bits;

      foreach (T feature in features)
      {
        int indx = IndexOf(feature);
        if (indx == -1)
          continue;

        bits[1 << indx] = true;
      }

      return bits;
    }

    /// <summary>
    /// Creates a <see cref="BitVector32"/> from the specified list of feature indices. Only valid if there are 32 values or less.
    /// </summary>
    /// <param name="indices">Indices of features, if any are not found, that feature is skipped.</param>
    /// <returns><see cref="BitVector32"/> containing which features are turned on.</returns>
    /// <exception cref="ArgumentException">Thrown if the <see cref="FeatureIndex{T}"/> has more than 32 values.</exception>
    public BitVector32 CreateBitVector32(IReadOnlyList<int> indices)
    {
      CheckIfCanFitBitVector32();

      uint count = (uint)Count;
      BitVector32 bits = new BitVector32();
      for (int i = 0; i < indices.Count; i++)
      {
        int indx = indices[i];
        if ((uint)indx >= count)
          continue;

        bits[1 << indx] = true;
      }

      return bits;
    }

    /// <summary>
    /// Creates a <see cref="BitVector32"/> from the specified list of feature indices. Only valid if there are 32 values or less.
    /// </summary>
    /// <param name="indices">Indices of features, if any are not found, that feature is skipped.</param>
    /// <returns><see cref="BitVector32"/> containing which features are turned on.</returns>
    /// <exception cref="ArgumentException">Thrown if the <see cref="FeatureIndex{T}"/> has more than 32 values.</exception>
    public BitVector32 CreateBitVector32(ReadOnlySpan<int> indices)
    {
      CheckIfCanFitBitVector32();

      uint count = (uint)Count;
      BitVector32 bits = new BitVector32();
      for (int i = 0; i < indices.Length; i++)
      {
        int indx = indices[i];
        if ((uint)indx >= count)
          continue;

        bits[1 << indx] = true;
      }

      return bits;
    }

    /// <summary>
    /// Returns an enumerator to iterate through the feature indices.
    /// </summary>
    /// <returns>Enumerator.</returns>
    public ListEnumerator<Entry> GetEnumerator()
    {
      return m_features.GetEnumerator();
    }

    /// <inheritdoc />
    IEnumerator<Entry> IEnumerable<Entry>.GetEnumerator()
    {
      return m_features.GetEnumerator();
    }

    /// <inheritdoc />
    IEnumerator IEnumerable.GetEnumerator()
    {
      return m_features.GetEnumerator();
    }

    private void CheckIfCanFitBitVector32()
    {
      if (Count > 32)
        throw new ArgumentException("Must have 32 values or less to create a BitVector32.");
    }
  }

  /// <summary>
  /// Represents a collection of feature strings that are mapped to indices.
  /// </summary>
  public class FeatureStringIndex : FeatureIndex<string>
  {
    /// <summary>
    /// Constructs a new instance of the <see cref="FeatureStringIndex"/> class.
    /// </summary>
    /// <param name="comparison">Type of string comparison, by default ignores culture and casing.</param>
    public FeatureStringIndex(StringComparison comparison = StringComparison.InvariantCultureIgnoreCase)
      : base(new StringEqualityComparer(comparison)) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="FeatureStringIndex"/> class.
    /// </summary>
    /// <param name="capacity">Initial capacity of the list.</param>
    /// <param name="comparison">Type of string comparison, by default ignores culture and casing.</param>
    public FeatureStringIndex(int capacity, StringComparison comparison = StringComparison.InvariantCultureIgnoreCase)
      : base(capacity, new StringEqualityComparer(comparison)) { }

    private sealed class StringEqualityComparer : IEqualityComparer<string>
    {
      private StringComparison m_compareType;

      public StringEqualityComparer(StringComparison compareType)
      {
        m_compareType = compareType;
      }

      public bool Equals(string? x, string? y)
      {
        return string.Equals(x, y, m_compareType);
      }

      public int GetHashCode([DisallowNull] string obj)
      {
        return obj.GetHashCode(m_compareType);
      }
    }
  }
}
