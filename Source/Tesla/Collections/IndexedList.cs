﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.CompilerServices;

#nullable enable

namespace Tesla.Collections
{
  /// <summary>
  /// List-like data structure that tracks each object's index via a key for fast lookup. Element removal is slower than a typical list, as the indexing needs
  /// to be recomputed if elements are removed from the middle of the list.
  /// </summary>
  /// <typeparam name="TKey">Key type associated with the value.</typeparam>
  /// <typeparam name="TValue">Value type.</typeparam>
  [DebuggerDisplay("Count = {Count}")]
  public sealed class IndexedList<TKey, TValue> : IReadOnlyDictionary<TKey, TValue>, IReadOnlyList<TValue> where TKey : notnull
  {
    private record struct IndexedValue(TValue Value, int Index);

    private Dictionary<TKey, IndexedValue> m_lookup;
    private List<KeyValuePair<TKey, TValue>> m_list;

    /// <summary>
    /// Gets or sets the capacity of the list. If the capacity is smaller than the size, it will lose the excess elements.
    /// </summary>
    public int Capacity
    {
      get
      {
        return m_list.Capacity;
      }
      set
      {
        // If new capacity is smaller than the count, then the list will lose elements after the index, need to remove them from the lookup
        if (value < m_list.Count)
        {
          for (int i = value; i < m_list.Count; i++)
          {
            KeyValuePair<TKey, TValue> kv = m_list[i];
            m_lookup.Remove(kv.Key);
          }
        }

        m_list.Capacity = value;
      }
    }

    /// <summary>
    /// Gets the key comparer used for fast lookup.
    /// </summary>
    public IEqualityComparer<TKey> KeyComparer { get { return m_lookup.Comparer; } }

    /// <inheritdoc />
    public int Count { get { return m_list.Count; } }

    /// <summary>
    /// Gets or set a value in the list based on the key. If the key-value was not present, it is added. If it existed, it is overwritten.
    /// </summary>
    /// <param name="key">Key associated with the value.</param>
    public TValue this[TKey key] { get { return m_lookup[key].Value; } set { AddInternal(key, value, true); } }

    /// <summary>
    /// Gets or sets a value in the list at the index. This reuses the key already associate with the index.
    /// </summary>
    /// <param name="index">Index of the value.</param>
    public TValue this[int index] { get { return GetValueAt(index); } set { SetValueAt(index, value); } }

    /// <inheritdoc />
    public IEnumerable<TKey> Keys { get { return m_lookup.Keys; } }

    /// <inheritdoc />
    IEnumerable<TValue> IReadOnlyDictionary<TKey, TValue>.Values { get { return ToArray(); } }

    /// <summary>
    /// Constructs a new instance of the <see cref="IndexedList{TKey, TValue}"/> class.
    /// </summary>
    /// <param name="keyComparer">Optional key comparer.</param>
    public IndexedList(IEqualityComparer<TKey>? keyComparer = null)
    {
      m_lookup = new Dictionary<TKey, IndexedValue>(keyComparer);
      m_list = new List<KeyValuePair<TKey, TValue>>();
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="IndexedList{TKey, TValue}"/> class.
    /// </summary>
    /// <param name="values">Key-value pairs to initialize the list to.</param>
    /// <param name="keyComparer">Optional key comparer.</param>
    public IndexedList(IEnumerable<KeyValuePair<TKey, TValue>> values, IEqualityComparer<TKey>? keyComparer = null)
    {
      int count = (values is IReadOnlyCollection<KeyValuePair<TKey, TValue>> rc) ? rc.Count : 0; 
      m_lookup = new Dictionary<TKey, IndexedValue>(count, keyComparer);
      m_list = new List<KeyValuePair<TKey, TValue>>(count);

      AddRange(values);
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="IndexedList{TKey, TValue}"/> class.
    /// </summary>
    /// <param name="capacity">Initial capacity of the list.</param>
    /// <param name="keyComparer">Optional key comparer.</param>
    public IndexedList(int capacity, IEqualityComparer<TKey>? keyComparer = null)
    {
      m_lookup = new Dictionary<TKey, IndexedValue>(capacity, keyComparer);
      m_list = new List<KeyValuePair<TKey, TValue>>(capacity);
    }

    /// <summary>
    /// Copies contents of <see cref="IndexedList{TKey, TValue}"/> to a new array.
    /// </summary>
    /// <returns>Array of values.</returns>
    public TValue[] ToArray()
    {
      TValue[] values = BufferHelper.NewArray<TValue>(m_list.Count);
      for (int i = 0; i < m_list.Count; i++)
        values[i] = m_list[i].Value;
 
      return values;
    }

    /// <summary>
    /// Copies contents of <see cref="IndexedList{TKey, TValue}"/> to a new array.
    /// </summary>
    /// <returns>Array of keys-values.</returns>
    public KeyValuePair<TKey, TValue>[] ToKeyValueArray()
    {
      return m_list.ToArray();
    }

    /// <summary>
    /// Creates a <see cref="List{T}"/> from the contents of the <see cref="IndexedList{TKey, TValue}"/>.
    /// </summary>
    /// <returns>List of the values.</returns>
    public List<TValue> ToList()
    {
      List<TValue> values = new List<TValue>(m_list.Count);
      for (int i = 0; i < m_list.Count; i++)
        values.Add(m_list[i].Value);

      return values;
    }

    /// <summary>
    /// Creates a <see cref="List{T}"/> from the contents of the <see cref="IndexedList{TKey, TValue}"/>.
    /// </summary>
    /// <returns>List of the keys-values.</returns>
    public List<KeyValuePair<TKey, TValue>> ToKeyValueList()
    {
      return new List<KeyValuePair<TKey, TValue>>(m_list);
    }

    /// <summary>
    /// Creates a <see cref="Dictionary{TKey, TValue}"/> from the contents of the <see cref="IndexedList{TKey, TValue}"/>.
    /// </summary>
    /// <returns>Dictionary of key-value pairs.</returns>
    public Dictionary<TKey, TValue> ToDictionary()
    {
      return new Dictionary<TKey, TValue>(m_list, m_lookup.Comparer);
    }

    /// <summary>
    /// Creates a <see cref="ImmutableArray{T}"/> from the contents of the <see cref="IndexedList{TKey, TValue}"/>.
    /// </summary>
    /// <returns>Immutable array of values.</returns>
    public ImmutableArray<TValue> ToImmutableArray()
    {
      ImmutableArray<TValue>.Builder builder = ImmutableArray.CreateBuilder<TValue>(m_list.Count);
      for (int i = 0; i < m_list.Count; i++)
        builder.Add(m_list[i].Value);

      return builder.ToImmutable();
    }

    /// <summary>
    /// Creates a <see cref="ImmutableList{T}"/> from the contents of the <see cref="IndexedList{TKey, TValue}"/>.
    /// </summary>
    /// <returns>Immutable list of values.</returns>
    public ImmutableList<TValue> ToImmutableList()
    {
      ImmutableList<TValue>.Builder builder = ImmutableList.CreateBuilder<TValue>();
      for (int i = 0; i < m_list.Count; i++)
        builder.Add(m_list[i].Value);

      return builder.ToImmutable();
    }

    /// <summary>
    /// Creates a <see cref="ImmutableDictionary{TKey, TValue}"/> from the contents of the <see cref="IndexedList{TKey, TValue}"/>.
    /// </summary>
    /// <param name="valueComparer">Optional value comparer.</param>
    /// <returns>Immutable dictionary of key-value pairs.</returns>
    public ImmutableDictionary<TKey, TValue> ToImmutableDictionary(IEqualityComparer<TValue>? valueComparer = null)
    {
      return ImmutableDictionary.CreateRange<TKey, TValue>(m_lookup.Comparer, valueComparer, m_list);
    }

    /// <summary>
    /// Creates a <see cref="ImmutableSortedDictionary{TKey, TValue}"/> from the contents of the <see cref="IndexedList{TKey, TValue}"/>.
    /// </summary>
    /// <param name="valueComparer">Optional value comparer.</param>
    /// <returns>Immutable sorted dictionary of key-value pairs.</returns>
    public ImmutableSortedDictionary<TKey, TValue> ToImmutableSortedDictionary(IComparer<TKey>? keyComparer = null, IEqualityComparer<TValue>? valueComparer = null)
    {
      return ImmutableSortedDictionary.CreateRange<TKey, TValue>(keyComparer, valueComparer, m_list);
    }

    /// <summary>
    /// Gets the key-value pair at the specified index.
    /// </summary>
    /// <param name="index">Index.</param>
    /// <returns>Key and value associated with the index.</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the index is out of range.</exception>
    public KeyValuePair<TKey, TValue> GetKeyValueAt(int index)
    {
      return m_list[index];
    }

    /// <summary>
    /// Gets the key at the specified index.
    /// </summary>
    /// <param name="index">Index.</param>
    /// <returns>Key associated with the index.</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the index is out of range.</exception>
    public TKey GetKeyAt(int index)
    {
      return m_list[index].Key;
    }

    /// <summary>
    /// Gets the value at the specified index.
    /// </summary>
    /// <param name="index">Index.</param>
    /// <returns>Value associated with the index.</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the index is out of range.</exception>
    public TValue GetValueAt(int index)
    {
      return m_list[index].Value;
    }

    /// <summary>
    /// Sets the value at the specified index.
    /// </summary>
    /// <param name="index">Index.</param>
    /// <param name="value">Value to set.</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the index is out of range.</exception>
    public void SetValueAt(int index, TValue value)
    {
      // Lookup the key first
      KeyValuePair<TKey, TValue> kv = m_list[index];
      m_lookup[kv.Key] = new IndexedValue(value, index);
      m_list[index] = new KeyValuePair<TKey, TValue>(kv.Key, value);
    }

    /// <summary>
    /// Gets the index of the key-value pair. This first looks to see if the key is in the lookup, and checks for equality with the value.
    /// </summary>
    /// <param name="keyValue">Key-value pair to find which index it exists in the list, if it does.</param>
    /// <returns>The index of the value, or -1 if not found.</returns>
    public int IndexOf(KeyValuePair<TKey, TValue> keyValue)
    {
      if (m_lookup.TryGetValue(keyValue.Key, out IndexedValue indexedValue))
      {
        if (EqualityComparer<TValue>.Default.Equals(keyValue.Value))
          return indexedValue.Index;
      }

      return -1;
    }

    /// <summary>
    /// Gets the index of the key. If not present, then returns -1.
    /// </summary>
    /// <param name="key">Key to search.</param>
    /// <returns>The index of the value, or -1 if not found.</returns>
    public int IndexOfKey(TKey key)
    {
      if (m_lookup.TryGetValue(key, out IndexedValue indexedValue))
        return indexedValue.Index;

      return -1;
    }

    /// <summary>
    /// Gets the index of the value.
    /// </summary>
    /// <param name="value">Value to search if it exists in the list.</param>
    /// <returns>The index of the value, or -1 if not found.</returns>
    public int IndexOfValue(TValue value)
    {
      return IndexOfValue(value, 0, m_list.Count);
    }

    /// <summary>
    /// Gets the index of the value.
    /// </summary>
    /// <param name="value">Value to search if it exists in the list.</param>
    /// <param name="index">Starting index to search at.</param>
    /// <returns>The index of the value, or -1 if not found.</returns>
    public int IndexOfValue(TValue value, int index)
    {
      return IndexOfValue(value, index, m_list.Count - index);
    }

    /// <summary>
    /// Gets the index of the value.
    /// </summary>
    /// <param name="value">Value to search if it exists in the list.</param>
    /// <param name="index">Starting index to search at.</param>
    /// <param name="count">Number of elements to search.</param>
    /// <returns>The index of the value, or -1 if not found.</returns>
    public int IndexOfValue(TValue value, int index, int count)
    {
      if (!BufferHelper.IsInRange(index, count, m_list.Count))
        return -1;

      IEqualityComparer<TValue> comparer = EqualityComparer<TValue>.Default;

      for (int i = index; i < count; i++)
      {
        if (comparer.Equals(m_list[i].Value, value))
          return i;
      }

      return -1;
    }

    /// <summary>
    /// Finds the index of the value matching the specified predicate.
    /// </summary>
    /// <param name="predicate">Matching function.</param>
    /// <returns>The index of the value, or -1 if not found.</returns>
    public int FindIndex(Predicate<TValue> predicate)
    {
      return FindIndex(0, m_list.Count, predicate);
    }

    /// <summary>
    /// Finds the index of the value matching the specified predicate.
    /// </summary>
    /// <param name="index">Index to start searching at.</param>
    /// <param name="predicate">Matching function.</param>
    /// <returns>The index of the value, or -1 if not found.</returns>
    public int FindIndex(int index, Predicate<TValue> predicate)
    {
      return FindIndex(index, m_list.Count - index, predicate);
    }

    /// <summary>
    /// Finds the index of the value matching the specified predicate.
    /// </summary>
    /// <param name="index">Index to start searching at.</param>
    /// <param name="count">Number of elements to search.</param>
    /// <param name="predicate">Matching function.</param>
    /// <returns>The index of the value, or -1 if not found.</returns>
    public int FindIndex(int index, int count, Predicate<TValue> predicate)
    {
      if (!BufferHelper.IsInRange(index, count, m_list.Count))
        return -1;

      for (int i = index; i < count; i++)
      {
        if (predicate(m_list[i].Value))
          return i;
      }

      return -1;
    }

    /// <summary>
    /// Queries if the key is present.
    /// </summary>
    /// <param name="key">Key to search.</param>
    /// <returns>True if an object associated with the key is present, false if otherwise.</returns>
    public bool ContainsKey(TKey key)
    {
      return IndexOfKey(key) != -1;
    }

    /// <summary>
    /// Queries if the value is present.
    /// </summary>
    /// <param name="value">Value to search.</param>
    /// <returns>True if the object is present, false if otherwise.</returns>
    public bool ContainsValue(TValue value)
    {
      return IndexOfValue(value) != -1;
    }

    /// <summary>
    /// Queries if the key-value is present. This first looks to see if the key is in the lookup, and checks for equality with the value.
    /// </summary>
    /// <param name="keyValue">Key-value to search.</param>
    /// <returns>True if the key-value is present, false if otherwise.</returns>
    public bool Contains(KeyValuePair<TKey, TValue> keyValue)
    {
      return IndexOf(keyValue) != -1;
    }

    /// <summary>
    /// Adds the object to the list.
    /// </summary>
    /// <param name="key">Key to search.</param>
    /// <param name="value">Value to add.</param>
    /// <returns>Index of the added value.</returns>
    /// <exception cref="ArgumentException">Thrown if the key is already present.</exception>
    public int Add(TKey key, TValue value)
    {
      return AddInternal(key, value, false);
    }

    /// <summary>
    /// Adds the range of values to the list.
    /// </summary>
    /// <param name="values"></param>
    public void AddRange(IEnumerable<KeyValuePair<TKey, TValue>> values)
    {
      foreach (KeyValuePair<TKey, TValue> kv in values)
        AddInternal(kv.Key, kv.Value, false);
    }

    /// <summary>
    /// Removes a value associated with the specified key.
    /// </summary>
    /// <param name="key">Key.</param>
    /// <returns>True if the value was removed, false if the index is out of range.</returns>
    public bool Remove(TKey key)
    {
      if (!m_lookup.Remove(key, out IndexedValue indexedValue))
        return false;

      m_list.RemoveAt(indexedValue.Index);
      UpdateRemovedAt(indexedValue.Index);

      return true;
    }

    /// <summary>
    /// Removes a key-value at the specified index.
    /// </summary>
    /// <param name="index">Index in list.</param>
    /// <returns>True if the value was removed, false if the index is out of range.</returns>
    public bool RemoveAt(int index)
    {
      if (!BufferHelper.IsInRange(index, m_list.Count))
        return false;

      KeyValuePair<TKey, TValue> kv = m_list[index];
      m_lookup.Remove(kv.Key);
      m_list.RemoveAt(index);
      UpdateRemovedAt(index);

      return true;
    }

    /// <summary>
    /// Removes a range of elements starting at the specified index.
    /// </summary>
    /// <param name="index">Index in list.</param>
    /// <param name="count">Number of elements to remove.</param>
    /// <returns>True if elements were removed, false if the index is out of range or nothing to remove.</returns>
    public bool RemoveRange(int index, int count)
    {
      if (!BufferHelper.IsInRange(index, count, m_list.Count) || count == 0)
        return false;

      for (int i = index; i < count; i++)
      {
        KeyValuePair<TKey, TValue> kv = m_list[i];
        m_lookup.Remove(kv.Key);
      }

      m_list.RemoveRange(index, count);
      UpdateRemovedAt(index);
      return true;
    }

    /// <summary>
    /// Copies the values of the list to the span, starting at the first index.
    /// </summary>
    /// <param name="values">Span to hold the copies, also specifies the number of elements to copy.</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the number of elements is out of range of the list.</exception>
    public void CopyTo(Span<TValue> values)
    {
      CopyTo(0, values);
    }

    /// <summary>
    /// Copies the values of the list to the span, starting at the specified index.
    /// </summary>
    /// <param name="index">Starting index to start copying from.</param>
    /// <param name="values">Span to hold the copies, also specifies the number of elements to copy.</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the index or number of elements is out of range of the list.</exception>
    public void CopyTo(int index, Span<TValue> values)
    {
      if (!BufferHelper.IsInRange(index, values.Length, m_list.Count))
        throw new ArgumentOutOfRangeException(nameof(values));

      for (int i = index; i < values.Length; i++)
        values[i] = m_list[i].Value;
    }

    /// <summary>
    /// Reverses the list and re-indexes vlaues.
    /// </summary>
    public void Reverse()
    {
      m_list.Reverse();
      Reindex();
    }

    /// <summary>
    /// Sorts the list and re-indexes values.
    /// </summary>
    /// <param name="compare">Comparer to use.</param>
    public void Sort(Comparison<KeyValuePair<TKey, TValue>> compare)
    {
      m_list.Sort(compare);
      Reindex();
    }

    /// <summary>
    /// Sorts the list and re-indexes values.
    /// </summary>
    /// <param name="comparer">Comparer to use.</param>
    public void sort(IComparer<KeyValuePair<TKey, TValue>> comparer)
    {
      m_list.Sort(comparer);
      Reindex();
    }

    /// <summary>
    /// Tries to get an object from the list that matches the key.
    /// </summary>
    /// <param name="key">Key to search.</param>
    /// <param name="value">Value in the list, if found. If not found, then the default value.</param>
    /// <returns>True if an object matching the key was found, false if not.</returns>
    public bool TryGetValue(TKey key, [MaybeNullWhen(false)] out TValue value)
    {
      if (m_lookup.TryGetValue(key, out IndexedValue indexedValue))
      {
        value = indexedValue.Value;
        return true;
      }

      value = default;
      return false;
    }

    /// <summary>
    /// Tries to add an object to the list. If the key is already present, returns the already added object's index.
    /// </summary>
    /// <param name="key">Key to search.</param>
    /// <param name="value">Value to try to add.</param>
    /// <returns>Index of the added value -or- the index of the previously added value matching the key.</returns>
    public int TryAdd(TKey key, TValue value)
    {
      int index = IndexOfKey(key);
      if (index != -1)
        return index;

      IndexedValue indexedValue = new IndexedValue(value, m_list.Count);
      m_lookup.Add(key, indexedValue);
      m_list.Add(new KeyValuePair<TKey, TValue>(key, value));

      return indexedValue.Index;
    }

    /// <summary>
    /// Clears the list.
    /// </summary>
    public void Clear()
    {
      m_list.Clear();
      m_lookup.Clear();
    }

    /// <summary>
    /// Trims the list to it's used capacity to reduce overall memory footprint, especially if no more values are to be added.
    /// </summary>
    public void TrimExcess()
    {
      m_list.TrimExcess();
      m_lookup.TrimExcess();
    }

    /// <summary>
    /// Gets a lightweight enumerator for iterating over the <see cref="TKey"/> keys in the <see cref="IndexedList{TKey, TValue}"/>.
    /// </summary>
    /// <returns>Key enumerator.</returns>
    public KeyEnumerator GetKeyEnumerator()
    {
      return new KeyEnumerator(m_list);
    }

    /// <inheritdoc cref="IEnumerable{T}.GetEnumerator"/>
    public ListEnumerator<TValue> GetEnumerator()
    {
      return new ListEnumerator<TValue>(this);
    }

    /// <inheritdoc />
    IEnumerator<TValue> IEnumerable<TValue>.GetEnumerator()
    {
      return new ListEnumerator<TValue>(this);
    }

    /// <inheritdoc />
    IEnumerator IEnumerable.GetEnumerator()
    {
      return new ListEnumerator<TValue>(this);
    }

    /// <inheritdoc />
    IEnumerator<KeyValuePair<TKey, TValue>> IEnumerable<KeyValuePair<TKey, TValue>>.GetEnumerator()
    {
      return m_list.GetEnumerator();
    }

    private int AddInternal(TKey key, TValue value, bool overwrite)
    {
      int index = IndexOfKey(key);
      if (index == -1)
      {
        IndexedValue indexedValue = new IndexedValue(value, m_list.Count);
        m_lookup.Add(key, indexedValue);
        m_list.Add(new KeyValuePair<TKey, TValue>(key, value));
        return indexedValue.Index;
      }

      if (!overwrite)
        throw new ArgumentException(StringLocalizer.Instance.GetLocalizedString("KeyAlreadyPresent", key.ToString()));

      m_lookup[key] = new IndexedValue(value, index);
      m_list[index] = new KeyValuePair<TKey, TValue>(key, value);
      return index;
    }

    private void UpdateRemovedAt(int index)
    {
      // Is the indexed value at the end of the list? No other indices are affected
      // Was the index the last element? If so, no other indices are affected
      if (index == m_list.Count)
        return;

      // Otherwise list is shifted, recompute the lookup
      Reindex(index);
    }

    private void Reindex()
    {
      Reindex(0, m_list.Count);
    }

    private void Reindex(int start)
    {
      Reindex(start, m_list.Count - start);
    }

    private void Reindex(int start, int count)
    {
      for (int i = start; i < count; i++)
      {
        KeyValuePair<TKey, TValue> kv = m_list[i];
        m_lookup[kv.Key] = new IndexedValue(kv.Value, i);
      }
    }

    #region Key Enumerator

    /// <summary>
    /// Enumerates indexed values.
    /// </summary>
    public struct KeyEnumerator : IEnumerator<TKey>
    {
      private List<KeyValuePair<TKey, TValue>> m_list;
      private int m_index;
      private int m_length;

      /// <inheritdoc />
      public TKey Current { [MethodImpl(MethodImplOptions.AggressiveInlining)] get { return m_list[m_index].Key; } }

      /// <inheritdoc />
      object? IEnumerator.Current { get { return m_list[m_index].Value; } }

      internal KeyEnumerator(List<KeyValuePair<TKey, TValue>> list)
      {
        m_list = list;
        m_index = -1;
        m_length = list.Count;
      }

      /// <inheritdoc />
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public bool MoveNext()
      {
        if (m_index >= m_length)
          return false;

        m_index++;
        return m_index < m_length;
      }

      /// <inheritdoc />
      public void Reset()
      {
        m_index = -1;
      }

      /// <inheritdoc />
      public void Dispose() { }
    }

    #endregion
  }
}
