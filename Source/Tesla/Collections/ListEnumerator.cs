﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System.Collections;
using System.Collections.Generic;

#nullable enable

namespace Tesla.Collections
{
  /// <summary>
  /// Simple enumerator for <see cref="IReadOnlyList{T}"/>.
  /// </summary>
  public struct ListEnumerator<T> : IEnumerator<T>
  {
    private IReadOnlyList<T> m_list;
    private int m_index;
    private int m_length;

    /// <inheritdoc />
    public T Current { get { return m_list[m_index]; } }

    /// <inheritdoc />
    object? IEnumerator.Current { get { return m_list[m_index]; } }

    public ListEnumerator(IReadOnlyList<T> list)
    {
      m_list = list;
      m_index = -1;
      m_length = list.Count;
    }

    /// <inheritdoc />
    public bool MoveNext()
    {
      if (m_index >= m_length)
        return false;

      m_index++;
      return m_index < m_length;
    }

    /// <inheritdoc />
    public void Reset()
    {
      m_index = -1;
    }

    /// <inheritdoc />
    public void Dispose() { }
  }
}
