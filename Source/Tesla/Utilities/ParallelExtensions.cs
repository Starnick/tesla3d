﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Threading.Tasks;

namespace Tesla.Utilities
{
    /// <summary>
    /// Extension methods for <see cref="Task"/>.
    /// </summary>
    public static class ParallelExtensions
    {

        /// <summary>
        /// Up casts a task to the specified type.
        /// </summary>
        /// <typeparam name="T">Type of the task</typeparam>
        /// <typeparam name="U">Type to upcast to</typeparam>
        /// <param name="task">The task</param>
        /// <returns>The continuation</returns>
        public static Task<U> Upcast<T, U>(this Task<T> task) where T : U
        {
            if(task == null)
                throw new ArgumentNullException("task");

            TaskCompletionSource<U> tcs = new TaskCompletionSource<U>();

            task.ContinueWith(t =>
            {
                if(t.IsFaulted)
                    tcs.SetException(t.Exception.Flatten());
                else if(t.IsCanceled)
                    tcs.SetCanceled();
                else
                    tcs.SetResult((U) t.Result);
            }, TaskContinuationOptions.ExecuteSynchronously);

            return tcs.Task;
        }

        /// <summary>
        /// Down casts a task to the specified type.
        /// </summary>
        /// <typeparam name="T">Type of the task</typeparam>
        /// <typeparam name="D">Type to downcast to</typeparam>
        /// <param name="task">The task</param>
        /// <returns>The continuation</returns>
        public static Task<D> Downcast<T, D>(this Task<T> task) where D : T
        {
            if(task == null)
                throw new ArgumentNullException("task");

            TaskCompletionSource<D> tcs = new TaskCompletionSource<D>();

            task.ContinueWith(t =>
            {
                if(t.IsFaulted)
                    tcs.SetException(t.Exception.Flatten());
                else if(t.IsCanceled)
                    tcs.SetCanceled();
                else
                    tcs.SetResult((D) t.Result);

            }, TaskContinuationOptions.ExecuteSynchronously);

            return tcs.Task;
        }

        /// <summary>
        /// Casts a task to the specified type.
        /// </summary>
        /// <typeparam name="T">Type of the task</typeparam>
        /// <typeparam name="K">Type to cast to</typeparam>
        /// <param name="task">The task</param>
        /// <returns>The continuation</returns>
        public static Task<K> Cast<T, K>(this Task<T> task)
        {
            if(task == null)
                throw new ArgumentNullException("task");

            TaskCompletionSource<K> tcs = new TaskCompletionSource<K>();

            task.ContinueWith(t =>
            {
                if(t.IsFaulted)
                {
                    tcs.SetException(t.Exception.Flatten());
                }
                else if(t.IsCanceled)
                {
                    tcs.SetCanceled();
                }
                else
                {
                    Object obj = t.Result;
                    if(obj is K)
                        tcs.SetResult((K) obj);
                    else
                        tcs.SetException(new InvalidCastException());
                }
            }, TaskContinuationOptions.ExecuteSynchronously);

            return tcs.Task;
        }

        /// <summary>
        /// "Then" task combinator where when the first task completes, the next function is invoked to
        /// produce a task and is provided with output from the previous task if the first task has not been
        /// canceled or faulted.
        /// </summary>
        /// <typeparam name="T1">Return type of the first task and input type of the second task</typeparam>
        /// <typeparam name="T2">Return type of the second task</typeparam>
        /// <param name="first">The first task</param>
        /// <param name="next">The function to execute after the first task finishes</param>
        /// <returns>The continuation</returns>
        public static Task<T2> Then<T1, T2>(this Task<T1> first, Func<T1, Task<T2>> next)
        {
            if(first == null)
                throw new ArgumentNullException("first");

            if(next == null)
                throw new ArgumentNullException("next");

            TaskCompletionSource<T2> tcs = new TaskCompletionSource<T2>();

            first.ContinueWith(delegate
            {
                if(first.IsFaulted)
                    tcs.TrySetException(first.Exception.Flatten());
                else if(first.IsCanceled)
                    tcs.TrySetCanceled();
                else
                {
                    try
                    {
                        Task<T2> t = next(first.Result);

                        if(t == null)
                        {
                            tcs.TrySetCanceled();
                        }
                        else
                        {
                            t.ContinueWith(delegate
                            {
                                if(t.IsFaulted)
                                    tcs.TrySetException(t.Exception.Flatten());
                                else if(t.IsCanceled)
                                    tcs.TrySetCanceled();
                                else
                                    tcs.TrySetResult(t.Result);
                            }, TaskContinuationOptions.ExecuteSynchronously);
                        }
                    }
                    catch(Exception e)
                    {
                        tcs.TrySetException(e);
                    }
                }
            }, TaskContinuationOptions.ExecuteSynchronously);

            return tcs.Task;
        }
    }
}
