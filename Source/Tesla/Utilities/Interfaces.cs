﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System.Diagnostics.CodeAnalysis;

#nullable enable

namespace Tesla.Utilities
{
  /// <summary>
  /// A manager for an object pool that provides specific logic to be executed on creation, management, and disposal of pooled items. Basically
  /// a "Pool Boy".
  /// </summary>
  /// <typeparam name="T">Type of pooled object.</typeparam>
  public interface IPoolManager<T> where T : class
  {
    /// <summary>
    /// Allocates a new object, with the possibility of failure.
    /// </summary>
    /// <param name="item">Newly created object.</param>
    /// <returns>True if the object was created, false if otherwise.</returns>
    bool Allocate([MaybeNullWhen(false)] out T item);

    /// <summary>
    /// Determines if the item is active or not active. Inactive items will automatically be recycled and returned to the pool. This is to support
    /// "fire and forget" mechanisms.
    /// </summary>
    /// <param name="item">Object to determine if active or not.</param>
    /// <returns>True if the object is still active, false if otherwise.</returns>
    bool IsActive(T item);

    /// <summary>
    /// Does the necessary cleanup and disposal of an object.
    /// </summary>
    /// <param name="item">Object to cleanup.</param>
    void Free(T item);
  }
}
