﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System.Threading;

namespace Tesla.Utilities
{
  /// <summary>
  /// A lock that is useful to guarantee an object is processed only once when being visited by multiple threads (e.g. first one wins, the rest see the lock as already marked and so ignore the object). 
  /// This requires each "round" of processing to have a globally unique sequence number (e.g. a render frame # or update # in a game). The sequence number that was used to take the lock is saved, so it is important to avoid copying-by-value of the lock.
  /// </summary>
  public struct VisitLock
  {
    private int m_beingVisited;
    private int m_lastVisitSequence;

    /// <summary>
    /// Determines if the lock has been visited during the current sequence. 
    /// </summary>
    /// <param name="currentVisitSequence">The current globally unique visit number.</param>
    /// <returns>True if this is the first time the lock has been visited, false if otherwise.</returns>
    public bool IsFirstVisit(int currentVisitSequence)
    {
      //Take the "lock", if it's already been taken it's being visited so we don't need to do anything
      if (Interlocked.Exchange(ref m_beingVisited, 1) == 0)
      {
        //If this has been visited during this visitation run, then we don't do anything
        bool isFirstVisit = false;
        if (m_lastVisitSequence != currentVisitSequence)
        {
          //If not, then set the last visit run to the current
          isFirstVisit = true;
          m_lastVisitSequence = currentVisitSequence;
        }

        //Release the "lock"
        Interlocked.Exchange(ref m_beingVisited, 0);
        return isFirstVisit;
      }

      return false;
    }
  }
}
