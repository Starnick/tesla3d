﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#nullable enable

namespace Tesla.Utilities
{
  /// <summary>
  /// Represents a mutable string of characters where the backing store is a buffer that can be represented as a <see cref="ReadOnlySpan{T}"/>.
  /// </summary>
  [DebuggerDisplay("{ToString(), raw}")]
  public sealed class SpanCharBuilder : IDisposable
  {
    private DataBufferBuilder<char> m_buffer;
    private bool m_isDisposed;

    /// <summary>
    /// Gets if this has been disposed or not.
    /// </summary>
    public bool IsDisposed { get { return m_isDisposed; } }

    /// <summary>
    /// Gets a span of characters representing the builder contents.
    /// </summary>
    public ReadOnlySpan<char> Span
    {
      get
      {
        return m_buffer.Span;
      }
    }

    /// <summary>
    /// Gets a span of bytes representing the builder contents.
    /// </summary>
    public ReadOnlySpan<byte> Bytes
    {
      get
      {
        return m_buffer.Bytes;
      }
    }

    /// <summary>
    /// Gets the length of the string represented by the builder.
    /// </summary>
    public int Length
    {
      get
      {
        return m_buffer.Count;
      }
    }

    /// <summary>
    /// Gets or sets the capacity of the builder.
    /// </summary>
    public int Capacity
    {
      get
      {
        return m_buffer.Capacity;
      }
      set
      {
        m_buffer.Capacity = value;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="SpanCharBuilder"/> class.
    /// </summary>
    /// <param name="initialCapacity">Initial capacity, default is 16.</param>
    public SpanCharBuilder(int initialCapacity = 16)
    {
      m_buffer = new DataBufferBuilder<char>(initialCapacity, MemoryAllocator<char>.DefaultPooled);
    }

    /// <summary>
    /// Clears the contents of the builder.
    /// </summary>
    public void Clear()
    {
      ObjectDisposedException.ThrowIf(m_isDisposed, this);

      m_buffer.Count = 0;
    }

    /// <summary>
    /// Append a single character to the builder.
    /// </summary>
    /// <param name="c">Character value.</param>
    /// <returns>The builder that was appended to.</returns>
    public SpanCharBuilder Append(char c)
    {
      ObjectDisposedException.ThrowIf(m_isDisposed, this);

      m_buffer.Set(c);

      return this;
    }

    /// <summary>
    /// Append the bool value ("true" or "false") to the builder.
    /// </summary>
    /// <param name="value">Bool value to append.</param>
    /// <returns>The builder that was appended to.</returns>
    public SpanCharBuilder Append(bool value)
    {
      return Append(value.ToString());
    }

    /// <summary>
    /// If the string is not null, append it's contents to the builder.
    /// </summary>
    /// <param name="value">String value to append.</param>
    /// <returns>The builder that was appended to.</returns>
    public SpanCharBuilder Append(string? value)
    {
      if (String.IsNullOrEmpty(value))
        return this;

      return Append(value.AsSpan());
    }

    /// <summary>
    /// If the object is not null, call ToString() on the object and append that value to the builder.
    /// </summary>
    /// <param name="value">Object to append.</param>
    /// <returns>The builder that was appended to.</returns>
    public SpanCharBuilder Append(object? value)
    {
      if (value is null)
        return this;

      return Append(value.ToString().AsSpan());
    }

    /// <summary>
    /// Appends the span of characters to the builder.
    /// </summary>
    /// <param name="chars">Span of characters.</param>
    /// <returns>The builder that was appended to.</returns>
    public SpanCharBuilder Append(ReadOnlySpan<char> chars)
    {
      ObjectDisposedException.ThrowIf(m_isDisposed, this);

      m_buffer.SetRange(chars);

      return this;
    }

    /// <summary>
    /// Appends the <see cref="ISpanFormattable"/> value to the builder.
    /// </summary>
    /// <typeparam name="T">Span formattable type.</typeparam>
    /// <param name="value">Value to format.</param>
    /// <returns>The builder that was appended to.</returns>
    public SpanCharBuilder Append<T>(T value) where T : ISpanFormattable
    {
      ObjectDisposedException.ThrowIf(m_isDisposed, this);

      if (value.TryFormat(m_buffer.RemainingSpan, out int charsWritten, default, null))
      {
        m_buffer.Count += charsWritten;
        return this;
      }

      return Append(value.ToString().AsSpan());
    }

    /// <summary>
    /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
    /// </summary>
    public void Dispose()
    {
      if (!m_isDisposed)
      {
        m_buffer.Clear();
        m_isDisposed = true;
      }

      GC.SuppressFinalize(this);
    }

    /// <summary>
    /// Returns the builder's contents as string.
    /// </summary>
    /// <returns>String value.</returns>
    public override string ToString()
    {
      if (Length == 0)
        return String.Empty;

      return new String(Span);
    }
  }
}
