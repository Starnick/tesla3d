/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Tesla.Graphics;

// TODO NULLABLE

namespace Tesla.Utilities
{
  public sealed class GeometryDebugger
  {
    private IRenderContext m_currContext;
    private DebugShape<BoxGenerator> m_boxShape;
    private DebugShape<SphereGenerator> m_sphereShape;
    private DebugShape<CapsuleGenerator> m_capsuleShape;
    private DebugFrustumShape m_frustumShape;
    private LineBatch m_lineBatch;
    private PrimitiveBatch<VertexPositionColor> m_primBatch;
    private BoundingBox m_measureBox;

    private Color m_boundingColor;
    private Color m_normalStartColor;
    private Color m_normalEndColor;
    private Color m_tangentStartColor;
    private Color m_tangentEndColor;
    private Color m_bitangentStartColor;
    private Color m_bitangentEndColor;
    private float m_lineScaleAmount;

    public Color BoundingColor
    {
      get
      {
        return m_boundingColor;
      }
      set
      {
        m_boundingColor = value;
        m_boxShape.ShapeColor = value;
        m_sphereShape.ShapeColor = value;
        m_capsuleShape.ShapeColor = value;
        m_frustumShape.ShapeColor = value;
      }
    }

    public Color NormalStartColor
    {
      get
      {
        return m_normalStartColor;
      }
      set
      {
        m_normalStartColor = value;
        m_frustumShape.NormalStartColor = value;
      }
    }

    public Color NormalEndColor
    {
      get
      {
        return m_normalEndColor;
      }
      set
      {
        m_normalEndColor = value;
        m_frustumShape.NormalEndColor = value;
      }
    }

    public Color TangentStartColor
    {
      get
      {
        return m_tangentStartColor;
      }
      set
      {
        m_tangentEndColor = value;
      }
    }

    public Color TangentEndColor
    {
      get
      {
        return m_tangentEndColor;
      }
      set
      {
        m_tangentEndColor = value;
      }
    }

    public Color BitangentStartColor
    {
      get
      {
        return m_bitangentStartColor;
      }
      set
      {
        m_bitangentEndColor = value;
      }
    }

    public Color BitangentEndColor
    {
      get
      {
        return m_bitangentEndColor;
      }
      set
      {
        m_bitangentEndColor = value;
      }
    }

    public float LineScaleAmount
    {
      get
      {
        return m_lineScaleAmount;
      }
      set
      {
        m_lineScaleAmount = Math.Max(0.01f, value);
        m_frustumShape.LineScaleAmount = m_lineScaleAmount;
      }
    }

    public GeometryDebugger() : this(IRenderSystem.Current) { }

    public GeometryDebugger(IRenderSystem renderSystem)
    {
      m_lineScaleAmount = 0.5f;
      m_boundingColor = Color.White;
      m_normalStartColor = Color.Red;
      m_normalEndColor = Color.Pink;
      m_tangentStartColor = Color.Green;
      m_tangentEndColor = Color.YellowGreen;
      m_bitangentStartColor = Color.Blue;
      m_bitangentEndColor = Color.BlueViolet;

      m_boxShape = new DebugShape<BoxGenerator>(new BoxGenerator(), m_boundingColor);
      m_boxShape.Generator.Use32BitIndices = false;
      m_boxShape.Generator.InsideOut = false;

      m_sphereShape = new DebugShape<SphereGenerator>(new SphereGenerator(), m_boundingColor);
      m_sphereShape.Generator.Use32BitIndices = false;
      m_sphereShape.Generator.InsideOut = false;
      m_sphereShape.Generator.Tessellation = 6;

      m_capsuleShape = new DebugShape<CapsuleGenerator>(new CapsuleGenerator(), m_boundingColor);
      m_capsuleShape.Generator.Use32BitIndices = false;
      m_capsuleShape.Generator.InsideOut = false;
      m_capsuleShape.Generator.VerticalSegmentCount = 4;
      m_capsuleShape.Generator.SphereTessellation = 3;
      m_capsuleShape.Generator.HorizontalSegmentCount = 12;

      m_frustumShape = new DebugFrustumShape(m_boundingColor, m_normalStartColor, m_normalEndColor, m_lineScaleAmount);

      m_lineBatch = new LineBatch(renderSystem);
      m_primBatch = m_lineBatch.BatchNoTexture; //Bit of a hack...
      m_currContext = null;
      m_measureBox = new BoundingBox();
    }

    #region Simple Draw API

    public void Draw(IRenderContext renderContext, BoundingVolume bv)
    {
      if (renderContext == null || bv == null)
        return;

      Begin(renderContext);

      DrawBoundingVolume(bv);

      End();
    }

    public void Draw(IRenderContext renderContext, MeshData md, bool drawTangentBasis, in Matrix worldMatrix, BoundingVolume measureBv = null)
    {
      if (renderContext == null || md == null)
        return;

      Begin(renderContext);

      if (drawTangentBasis)
      {
        DrawTangentBasis(md, worldMatrix, measureBv);
      }
      else
      {
        DrawNormals(md, worldMatrix, measureBv);
      }

      End();
    }

    public void Draw(IRenderContext renderContext, DebugView flags, RenderQueue renderQueue)
    {
      if (renderQueue == null || flags == DebugView.None || renderContext == null)
        return;

      Begin(renderContext);

      for (int i = 0; i < renderQueue.Count; i++)
      {
        RenderBucket bucket = renderQueue[i];

        for (int j = 0; j < bucket.Count; j++)
        {
          DrawSingleRenderable(flags, bucket[j].Renderable);
        }
      }

      End();
    }

    public void Draw(IRenderContext renderContext, DebugView flags, IRenderable renderable)
    {
      if (renderable == null || flags == DebugView.None || renderContext == null)
        return;

      Begin(renderContext);

      DrawSingleRenderable(flags, renderable);

      End();
    }

    public void Draw(IRenderContext renderContext, DebugView flags, params IRenderable[] renderables)
    {
      if (renderables == null || renderables.Length == 0 || flags == DebugView.None || renderContext == null)
        return;

      Begin(renderContext);

      for (int i = 0; i < renderables.Length; i++)
        DrawSingleRenderable(flags, renderables[i]);

      End();
    }

    private void DrawSingleRenderable(DebugView flags, IRenderable renderable)
    {
      if (renderable == null)
        return;

      if (renderable is InstanceDefinition)
      {
        InstanceDefinition instanceDef = renderable as InstanceDefinition;
        IReadOnlyList<IRenderable> instancesToDraw = instanceDef.InstancesToDraw;

        for (int i = 0; i < instancesToDraw.Count; i++)
          DrawSingleRenderable(flags, instancesToDraw[i]);

        return;
      }

      IMeshDataContainer mdContainer = renderable as IMeshDataContainer;
      WorldBoundingVolumeProperty bvProp;
      BoundingVolume bv = null;
      if (renderable.RenderProperties.TryGet<WorldBoundingVolumeProperty>(out bvProp))
        bv = bvProp.Value;

      Transform worldTransform = renderable.WorldTransform;

      if (mdContainer != null && worldTransform != null)
      {
        if ((flags & DebugView.Normals) == DebugView.Normals)
        {
          DrawNormals(mdContainer.MeshData, worldTransform.Matrix, bv);
        }
        else if ((flags & DebugView.TangentBasis) == DebugView.TangentBasis)
        {
          DrawTangentBasis(mdContainer.MeshData, worldTransform.Matrix, bv);
        }
      }

      if (bv != null && (flags & DebugView.BoundingVolumes) == DebugView.BoundingVolumes)
      {
        DrawBoundingVolume(bv);
      }
    }

    #endregion

    #region Complex Draw API

    public void Begin(IRenderContext renderContext, BlendState bs = null, DepthStencilState dss = null, RasterizerState rs = null)
    {
      if (m_currContext != null)
      {
        Debug.Assert(false, "GeometryDebugger::Begin already called. You may have missed a call to End().");
        return;
      }

      m_currContext = renderContext;

      if (bs == null)
        bs = BlendState.Opaque;

      if (dss == null)
        dss = DepthStencilState.Default;

      if (rs == null)
        rs = RasterizerState.CullNoneWireframe;

      m_lineBatch.Begin(renderContext, bs, dss, rs, null, null, Matrix.Identity);
    }

    public void DrawNormals(MeshData md, in Matrix worldMatrix, BoundingVolume measureBv = null)
    {

      if (m_currContext == null)
      {
        Debug.Assert(false, "GeometryDebugger:Begin was not previously called.");
        return;
      }

      if (md == null)
        return;

      DataBuffer<Vector3> positions = md.Positions;
      DataBuffer<Vector3> normals = md.Normals;
      IndexData? indices = md.Indices;

      if (positions == null || normals == null || (md.UseIndexedPrimitives && !indices.HasValue))
        return;

      float scale = m_lineScaleAmount;
      if (measureBv != null)
      {
        m_measureBox.ComputeFromPoints(measureBv.Corners, null);
        scale *= (m_measureBox.Extents.Length() / 6.0f);
      }

      if (indices.HasValue)
      {
        IndexData indData = indices.Value;
        for (int i = 0; i < indData.Length; i++)
        {
          int index = indData[i];
          Vector3 pt = positions[index];
          Vector3.Transform(pt, worldMatrix, out pt);

          Vector3 n = normals[index];
          Vector3.TransformNormal(n, worldMatrix, out n);

          Vector3 endPt = pt;
          Vector3.Multiply(n, scale, out n);
          Vector3.Add(endPt, n, out endPt);

          m_lineBatch.DrawLine(pt, endPt, m_normalStartColor, m_normalEndColor, 0.0f);
        }
      }
      else
      {
        for (int i = 0; i < positions.Length; i++)
        {
          Vector3 pt = positions[i];
          Vector3.Transform(pt, worldMatrix, out pt);

          Vector3 n = normals[i];
          Vector3.TransformNormal(n, worldMatrix, out n);

          Vector3 endPt = pt;
          Vector3.Multiply(n, scale, out n);
          Vector3.Add(endPt, n, out endPt);

          m_lineBatch.DrawLine(pt, endPt, m_normalStartColor, m_normalEndColor, 0.0f);
        }
      }
    }

    public void DrawTangentBasis(MeshData md, in Matrix worldMatrix, BoundingVolume measureBv = null)
    {
      if (m_currContext == null)
      {
        Debug.Assert(false, "GeometryDebugger:Begin was not previously called.");
        return;
      }

      if (md == null)
        return;

      DataBuffer<Vector3> positions = md.Positions;
      DataBuffer<Vector3> normals = md.Normals;
      DataBuffer<Vector3> tangents = md.Tangents;
      DataBuffer<Vector3> bitangents = md.Bitangents;
      IndexData? indices = md.Indices;

      if (positions == null || normals == null || (md.UseIndexedPrimitives && !indices.HasValue))
        return;

      //If so far so good, but missing tangents/binormals, at the very least let's draw some normals
      if (tangents == null || bitangents == null)
        DrawNormals(md, worldMatrix, measureBv);

      float scale = m_lineScaleAmount;
      if (measureBv != null)
      {
        m_measureBox.ComputeFromPoints(measureBv.Corners, null);
        scale *= (m_measureBox.Extents.Length() / 6.0f);
      }

      if (indices.HasValue)
      {
        IndexData indData = indices.Value;
        for (int i = 0; i < indData.Length; i++)
        {
          int index = indData[i];
          Vector3 pt = positions[index];
          Vector3.Transform(pt, worldMatrix, out pt);

          //Normal
          Vector3 n = normals[index];
          Vector3.TransformNormal(n, worldMatrix, out n);

          Vector3 endPt = pt;
          Vector3.Multiply(n, scale, out n);
          Vector3.Add(endPt, n, out endPt);

          m_lineBatch.DrawLine(pt, endPt, m_normalStartColor, m_normalEndColor, 0.0f);

          //Tangent
          if (tangents != null)
          {
            Vector3 tn = tangents[index];
            Vector3.TransformNormal(tn, worldMatrix, out tn);

            endPt = pt;
            Vector3.Multiply(tn, scale, out tn);
            Vector3.Add(endPt, tn, out endPt);

            m_lineBatch.DrawLine(pt, endPt, m_tangentStartColor, m_tangentEndColor, 0.0f);
          }

          //Binormal
          if (bitangents != null)
          {
            Vector3 bn = bitangents[index];
            Vector3.TransformNormal(bn, worldMatrix, out bn);

            endPt = pt;
            Vector3.Multiply(bn, scale, out bn);
            Vector3.Add(endPt, bn, out endPt);

            m_lineBatch.DrawLine(pt, endPt, m_bitangentStartColor, m_bitangentEndColor, 0.0f);
          }

        }
      }
      else
      {
        for (int i = 0; i < positions.Length; i++)
        {
          Vector3 pt = positions[i];
          Vector3.Transform(pt, worldMatrix, out pt);

          //Normal
          Vector3 n = normals[i];
          Vector3.TransformNormal(n, worldMatrix, out n);

          Vector3 endPt = pt;
          Vector3.Multiply(n, scale, out n);
          Vector3.Add(endPt, n, out endPt);

          m_lineBatch.DrawLine(pt, endPt, m_normalStartColor, m_normalEndColor, 0.0f);

          //Tangent
          if (tangents != null)
          {
            Vector3 tn = tangents[i];
            Vector3.TransformNormal(tn, worldMatrix, out tn);

            endPt = pt;
            Vector3.Multiply(tn, scale, out tn);
            Vector3.Add(endPt, tn, out endPt);

            m_lineBatch.DrawLine(pt, endPt, m_tangentStartColor, m_tangentEndColor, 0.0f);
          }

          //Binormal
          if (bitangents != null)
          {
            Vector3 bn = bitangents[i];
            Vector3.TransformNormal(bn, worldMatrix, out bn);

            endPt = pt;
            Vector3.Multiply(bn, scale, out bn);
            Vector3.Add(endPt, bn, out endPt);

            m_lineBatch.DrawLine(pt, endPt, m_bitangentStartColor, m_bitangentEndColor, 0.0f);
          }
        }
      }
    }

    public void DrawBoundingVolume(BoundingVolume bv)
    {
      if (m_currContext == null)
      {
        Debug.Assert(false, "GeometryDebugger:Begin was not previously called.");
        return;
      }

      if (bv == null)
        return;

      switch (bv.BoundingType)
      {
        case BoundingType.AxisAlignedBoundingBox:
          DrawBoundingBox(m_currContext, bv as BoundingBox);
          break;
        case BoundingType.Capsule:
          DrawBoundingCapsule(m_currContext, bv as BoundingCapsule);
          break;
        case BoundingType.Frustum:
          DrawBoundingFrustum(m_currContext, bv as BoundingFrustum);
          break;
        case BoundingType.Mesh:
          DrawBoundingMesh(m_currContext, bv as BoundingMesh);
          break;
        case BoundingType.OrientedBoundingBox:
          DrawOrientedBoundingBox(m_currContext, bv as OrientedBoundingBox);
          break;
        case BoundingType.Sphere:
          DrawBoundingSphere(m_currContext, bv as BoundingSphere);
          break;
      }
    }

    public void End()
    {
      if (m_currContext == null)
      {
        Debug.Assert(false, "GeometryDebugger:Begin was not previously called.");
        return;
      }

      m_currContext = null;
      m_lineBatch.End();
    }

    #endregion

    #region Draw Bounding Volumes

    private void DrawBoundingBox(IRenderContext renderContext, BoundingBox box)
    {
      m_boxShape.Generator.Center = box.Center;
      m_boxShape.Generator.Extents = box.Extents;
      m_boxShape.Generator.Axes = Triad.UnitAxes;

      m_boxShape.Draw(m_primBatch);
    }

    private void DrawBoundingSphere(IRenderContext renderContext, BoundingSphere sphere)
    {
      m_sphereShape.Generator.Center = sphere.Center;
      m_sphereShape.Generator.Radius = sphere.Radius;

      m_sphereShape.Draw(m_primBatch);
    }

    private void DrawOrientedBoundingBox(IRenderContext renderContext, OrientedBoundingBox obb)
    {
      m_boxShape.Generator.Center = obb.Center;
      m_boxShape.Generator.Extents = obb.Extents;
      m_boxShape.Generator.Axes = obb.Axes;

      m_boxShape.Draw(m_primBatch);
    }

    private void DrawOrientedBoundingBox(IRenderContext renderContext, ref OrientedBoundingBox.Data obb)
    {
      m_boxShape.Generator.Center = obb.Center;
      m_boxShape.Generator.Extents = obb.Extents;
      m_boxShape.Generator.Axes = obb.Axes;

      m_boxShape.Draw(m_primBatch);
    }

    private void DrawBoundingCapsule(IRenderContext renderContext, BoundingCapsule capsule)
    {
      if (capsule.CenterLine.IsDegenerate)
      {
        m_sphereShape.Generator.Center = capsule.Center;
        m_sphereShape.Generator.Radius = capsule.Radius;

        m_sphereShape.Draw(m_primBatch);
      }
      else
      {
        m_capsuleShape.Generator.CenterLine = capsule.CenterLine;
        m_capsuleShape.Generator.Radius = capsule.Radius;

        m_capsuleShape.Draw(m_primBatch);
      }
    }

    private void DrawBoundingFrustum(IRenderContext renderContext, BoundingFrustum frustum)
    {
      m_frustumShape.Draw(frustum, m_primBatch);
    }

    private void DrawBoundingMesh(IRenderContext renderContext, BoundingMesh mesh)
    {

    }

    #endregion

    #region Debug Shape

    private class DebugShape<T> where T : GeometryGenerator
    {
      private T m_generator;
      private DataBuffer<Vector3> m_positions;
      private IndexData m_indices;
      private DataBuffer<VertexPositionColor> m_verts;
      private Color m_shapeColor;

      public T Generator
      {
        get
        {
          return m_generator;
        }
      }

      public Color ShapeColor
      {
        get
        {
          return m_shapeColor;
        }
        set
        {
          m_shapeColor = value;
        }
      }

      public DebugShape(T generator, Color shapeColor)
      {
        m_generator = generator;
        m_shapeColor = shapeColor;
      }

      public void Draw(PrimitiveBatch<VertexPositionColor> primBatch)
      {
        m_generator.Build(ref m_positions, ref m_indices);

        if (m_verts == null || m_verts.Length != m_positions.Length)
          m_verts = DataBuffer.Create<VertexPositionColor>(m_positions.Length);

        for (int i = 0; i < m_verts.Length; i++)
        {
          VertexPositionColor v = new VertexPositionColor(m_positions[i], m_shapeColor);
          m_verts[i] = v;
        }

        primBatch.DrawIndexed(PrimitiveBatchTopology.TriangleList, m_verts, m_indices);
      }
    }

    private class DebugFrustumShape
    {
      private IndexData m_indices;
      private DataBuffer<VertexPositionColor> m_verts;
      private Color m_shapeColor;
      private Color m_normalStartColor;
      private Color m_normalEndColor;
      private float m_lineScaleAmount;

      public Color ShapeColor
      {
        get
        {
          return m_shapeColor;
        }
        set
        {
          m_shapeColor = value;
        }
      }

      public Color NormalStartColor
      {
        get
        {
          return m_normalStartColor;
        }
        set
        {
          m_normalStartColor = value;
        }
      }

      public Color NormalEndColor
      {
        get
        {
          return m_normalEndColor;
        }
        set
        {
          m_normalEndColor = value;
        }
      }

      public float LineScaleAmount
      {
        get
        {
          return m_lineScaleAmount;
        }
        set
        {
          m_lineScaleAmount = value;
        }
      }

      public DebugFrustumShape(Color shapeColor, Color normalStartColor, Color normalEndColor, float lineScaleAmt)
      {
        m_shapeColor = shapeColor;
        m_normalStartColor = normalStartColor;
        m_normalEndColor = normalEndColor;
        m_lineScaleAmount = lineScaleAmt;

        m_verts = DataBuffer.Create<VertexPositionColor>(8);

        //Indices will never changed, based on the internal layout of the Corners of a frustum
        m_indices = DataBuffer.Create<ushort>(new ushort[]
        {
                    //near plane
                    0, 1, 2,
                    0, 2, 3,

                    //far plane
                    4, 5, 6,
                    4, 6, 7,

                    //left plane
                    7, 6, 1,
                    7, 1, 0,

                    //right plane
                    3, 2, 5,
                    3, 5, 4,

                    //top plane
                    1, 6, 5,
                    1, 5, 2,

                    //bottom plane
                    4, 7, 0,
                    4, 0, 3
        });
      }

      public void Draw(BoundingFrustum frustum, PrimitiveBatch<VertexPositionColor> primBatch)
      {
        //Draw the frustum first
        ReadOnlySpan<Vector3> corners = frustum.Corners;
        System.Diagnostics.Debug.Assert(corners.Length == m_verts.Length);

        for (int i = 0; i < m_verts.Length; i++)
        {
          VertexPositionColor v = new VertexPositionColor(corners[i], m_shapeColor);
          m_verts[i] = v;
        }

        primBatch.DrawIndexed(PrimitiveBatchTopology.TriangleList, m_verts, m_indices);

        //Then draw the plane normals
        for (int i = 0; i < frustum.PlaneCount; i++)
        {
          Vector3 origin, normal;

          frustum.GetPlaneOriginOnFrustum((FrustumPlane) i, out origin, out normal);

          Vector3.Multiply(normal, m_lineScaleAmount, out normal);
          Vector3.Add(origin, normal, out normal);

          VertexPositionColor start = new VertexPositionColor(origin, m_normalStartColor);
          VertexPositionColor end = new VertexPositionColor(normal, m_normalEndColor);

          primBatch.DrawLine(start, end);
        }
      }
    }

    #endregion
  }
}
