﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;

namespace Tesla.Utilities
{
    /// <summary>
    /// Flags for which components of the geometry to draw during geometry debugging.
    /// </summary>
    [Flags]
    public enum DebugView
    {
        /// <summary>
        /// Draw nothing.
        /// </summary>
        None = 0,

        /// <summary>
        /// Draw normals.
        /// </summary>
        Normals = 1,

        /// <summary>
        /// Draw tangent basis (tangents or both tangents/bitangents).
        /// </summary>
        TangentBasis = 2,

        /// <summary>
        /// Draw the bounding volume of the object.
        /// </summary>
        BoundingVolumes = 4,

        /// <summary>
        /// Draw everything.
        /// </summary>
        All = Normals | TangentBasis | BoundingVolumes
    }
}
