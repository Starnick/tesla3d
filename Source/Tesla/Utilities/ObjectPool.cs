﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;

// TODO NULLABLE

namespace Tesla.Utilities
{
  /// <summary>
  /// Thread-safe object pool with several options. Each pool has a manager that takes care of allocating and disposing of objects. The pool can handle
  /// "fire and forget" mechanisms and automatically clean up tracked objects that are lent out but no longer active (as governed by the pool manager). It
  /// can also support a maximum capacity where the pool tries to maintain N number of objects managed at any given time and will not create new objects if
  /// the capacity is met or exceeded.
  /// </summary>
  /// <typeparam name="T">Type of pooled object.</typeparam>
  public class ObjectPool<T> where T : class
  {
    private HashSet<T> m_trackedObjects;
    private HashSet<T> m_tempTrackedObjects;
    private Queue<T> m_freeItems;
    private IPoolManager<T> m_poolBoy;
    private bool m_sweepActiveList;
    private bool m_trackActiveObjects;
    private int m_poolCapacity;
    private Object m_sync = new Object();

    /// <summary>
    /// Gets the manager for this pool.
    /// </summary>
    public IPoolManager<T> Manager
    {
      get
      {
        return m_poolBoy;
      }
    }

    /// <summary>
    /// Gets the number of free objects in the pool that can be fetched.
    /// </summary>
    public int FreeCount
    {
      get
      {
        return m_freeItems.Count;
      }
    }

    /// <summary>
    /// Gets the number of active objects that are currently lent out and not available to be fetched.
    /// </summary>
    public int ActiveCount
    {
      get
      {
        return m_trackedObjects.Count;
      }
    }

    /// <summary>
    /// Gets the number of objects this pool manages.
    /// </summary>
    public int TotalCount
    {
      get
      {
        return m_trackedObjects.Count + m_freeItems.Count;
      }
    }

    /// <summary>
    /// Gets or sets the maximum capacity of the pool. This is optional, a value of zero or less denotes no capacity on the number of
    /// objects the pool can manage. If this capacity is met, then no new objects will be created if all objects are currently being lent out. This
    /// means <see cref="TryFetch"/> will fail.
    /// </summary>
    public int PoolCapacity
    {
      get
      {
        return m_poolCapacity;
      }
      set
      {
        if (value != m_poolCapacity)
        {
          m_poolCapacity = value;

          //No trimming if turning off max capacity...
          if (value > 0)
            TrimPool();
        }
      }
    }

    /// <summary>
    /// Gets or sets if the pool should sweep the active list during a <see cref="TryFetch"/> to try and automatically reclaim unreturned objects. This
    /// supports a "fire and forget" mechanism. Only can be set to true if the pool actually tracks active objects.
    /// </summary>
    public bool SweepActiveList
    {
      get
      {
        return m_sweepActiveList;
      }
      set
      {
        //If only tracking objects...set the value
        if (m_trackActiveObjects)
          m_sweepActiveList = value;
      }
    }

    /// <summary>
    /// Gets if the pool is tracking objects.
    /// </summary>
    public bool IsTrackingObjects
    {
      get
      {
        return m_trackActiveObjects;
      }
    }

    private bool IsAtCapacity
    {
      get
      {
        return m_poolCapacity > 0 && TotalCount >= m_poolCapacity;
      }
    }

    private bool IsAboveCapacity
    {
      get
      {
        return m_poolCapacity > 0 && TotalCount > m_poolCapacity;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="ObjectPool{T}"/> class.
    /// </summary>
    /// <param name="poolManager">Pool manager that handles creation and cleanup of pooled objects.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the pool manager is null.</exception>
    public ObjectPool(IPoolManager<T> poolManager) : this(poolManager, 0, true, null) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="ObjectPool{T}"/> class.
    /// </summary>
    /// <param name="poolManager">Pool manager that handles creation and cleanup of pooled objects.</param>
    /// <param name="poolCapacity">Capacity of the pool, optional.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the pool manager is null.</exception>
    public ObjectPool(IPoolManager<T> poolManager, int poolCapacity) : this(poolManager, poolCapacity, true, null) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="ObjectPool{T}"/> class.
    /// </summary>
    /// <param name="poolManager">Pool manager that handles creation and cleanup of pooled objects.</param>
    /// <param name="poolCapacity">Capacity of the pool, optional.</param>
    /// <param name="trackObjects">True if active objects should be tracked, false otherwise.</param>
    /// <param name="comparer">Equality comparer for pooled objects.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the pool manager is null.</exception>
    public ObjectPool(IPoolManager<T> poolManager, int poolCapacity, bool trackObjects, IEqualityComparer<T> comparer)
    {
      if (poolManager == null)
        throw new ArgumentNullException("poolManager");

      if (!trackObjects)
      {
        m_sweepActiveList = false;
        m_trackActiveObjects = false;
      }
      else
      {
        m_trackActiveObjects = true;
        m_sweepActiveList = true;
      }

      if (comparer == null)
        comparer = EqualityComparer<T>.Default;

      m_trackedObjects = new HashSet<T>(comparer);
      m_freeItems = new Queue<T>(poolCapacity);
      m_poolCapacity = poolCapacity;
      m_poolBoy = poolManager;
    }

    /// <summary>
    /// Clears the pool of all managed objects, including objects currently being lent out. This will also do the necessary cleanup of each object.
    /// </summary>
    public void Clear()
    {
      lock (m_sync)
      {
        while (m_freeItems.Count > 0)
          m_poolBoy.Free(m_freeItems.Dequeue());

        if (m_trackActiveObjects)
        {
          foreach (T activeItem in m_trackedObjects)
            m_poolBoy.Free(activeItem);

          m_trackedObjects.Clear();
        }
      }
    }

    /// <summary>
    /// Forces a sweep of the list of the objects currently being lent out to see if there are any that are no longer active. If the object is not active,
    /// it is automatically recycled and returned to the pool. 
    /// </summary>
    public void ReturnInActives()
    {
      if (!m_trackActiveObjects || !m_sweepActiveList)
        return;

      lock (m_sync)
      {
        SweepActiveListAndReturn();
      }
    }

    /// <summary>
    /// Returns the object back to the pool.
    /// </summary>
    /// <param name="item">Item to return to the pool.</param>
    public void Return(T item)
    {
      if (item == null)
        return;

      lock (m_sync)
      {
        //Try and remove from tracking collection, if it wasn't there then this isn't owned by us
        if (m_trackActiveObjects && !m_trackedObjects.Remove(item))
          return;

        //Make sure if the pool capacity was changed, and we're at or above capacity, we start freeing objects.
        if (IsAtCapacity)
        {
          m_poolBoy.Free(item);
        }
        else
        {
          m_freeItems.Enqueue(item);
        }
      }
    }

    /// <summary>
    /// Returns a collection of objects back to the pool. Use this to avoid creating garbage with GetEnumerator.
    /// </summary>
    /// <param name="items">Items to return to the pool.</param>
    public void Return(IList<T> items)
    {
      if (items == null)
        return;

      lock (m_sync)
      {
        for (int i = 0; i < items.Count; i++)
        {
          T item = items[i];

          if (item != null)
          {
            //Try and remove from tracking collection, if it wasn't there then this isn't owned by us
            if (m_trackActiveObjects && !m_trackedObjects.Remove(item))
              continue;

            //Make sure if the pool capacity was changed, and we're at or above capacity, we start freeing objects.
            if (IsAtCapacity)
            {
              m_poolBoy.Free(item);
            }
            else
            {
              m_freeItems.Enqueue(item);
            }
          }
        }
      }
    }

    /// <summary>
    /// Returns a collection of objects back to the pool.
    /// </summary>
    /// <param name="items">Items to return to the pool.</param>
    public void Return(IEnumerable<T> items)
    {
      if (items == null)
        return;

      lock (m_sync)
      {
        foreach (T item in items)
        {
          if (item != null)
          {
            //Try and remove from tracking collection, if it wasn't there then this isn't owned by us
            if (m_trackActiveObjects && !m_trackedObjects.Remove(item))
              continue;

            //Make sure if the pool capacity was changed, and we're at or above capacity, we start freeing objects.
            if (IsAtCapacity)
            {
              m_poolBoy.Free(item);
            }
            else
            {
              m_freeItems.Enqueue(item);
            }
          }
        }
      }
    }

    /// <summary>
    /// Tries to fetch an object from the pool. If there are free items, one of those is returned. Depending on pooling options, if there
    /// are no free items, then the pool attempts to recycle or create objects only if tracking is enabled. If creation fails or if there is a maximum pool capacity that is met,
    /// then this will fail, again only if tracking is enabled.
    /// </summary>
    /// <param name="item">Pooled object.</param>
    /// <returns>True if the object was successfully fetched from the pool, false otherwise.</returns>
    public bool TryFetch(out T item)
    {
      lock (m_sync)
      {
        //If allowed, sweep active list and recycle any
        if (m_sweepActiveList && m_freeItems.Count == 0)
          SweepActiveListAndReturn();

        //Swept but still no free objects...
        if (m_freeItems.Count == 0)
        {
          //We're at capacity, so shouldn't create new objects, we must return a failure
          if (m_trackActiveObjects && IsAtCapacity)
          {
            item = null;
            return false;
          }

          if (!m_poolBoy.Allocate(out item))
            return false;
        }
        else
        {
          //Get from the free queue...
          item = m_freeItems.Dequeue();
        }

        //Sanity check
        System.Diagnostics.Debug.Assert(item != null, "Item should exist");

        //Track the object if not null and can track...
        if (m_trackActiveObjects)
          m_trackedObjects.Add(item);

        return true;
      }
    }

    /// <summary>
    /// Tries to fetch multiple objects from the pool. If there are free items, as many as possible of those is returned. Depending on pooling options, if there
    /// are no free items, then the pool attempts to recycle or create objects only if tracking is enabled. If creation fails or if there is a maximum pool capacity that is met,
    /// then this will fail, again only if tracking is enabled.
    /// </summary>
    /// <param name="item">Pooled object.</param>
    /// <returns>Returns the number of items fetched, if any.</returns>
    public int TryFetch(IList<T> items, int requestedCount)
    {
      lock (m_sync)
      {
        int actualCount = 0;

        //If allowed, sweep active list and recycle any
        if (m_sweepActiveList && m_freeItems.Count == 0)
          SweepActiveListAndReturn();

        for (int i = 0; i < requestedCount; i++)
        {
          T item = null;

          //Swept but still no free objects...
          if (m_freeItems.Count == 0)
          {
            //We're at capacity, so shouldn't create new objects, we must return a failure
            if (m_trackActiveObjects && IsAtCapacity)
              return actualCount;

            if (!m_poolBoy.Allocate(out item))
              return actualCount;
          }
          else
          {
            //Get from the free queue...
            item = m_freeItems.Dequeue();
          }

          //Sanity check
          System.Diagnostics.Debug.Assert(item != null, "Item should exist");

          //Track the object if not null and can track...
          if (m_trackActiveObjects)
            m_trackedObjects.Add(item);

          actualCount++;
          items.Add(item);
        }

        return actualCount;
      }
    }

    private void SweepActiveListAndReturn()
    {
      //If not tracking objects...can't sweep..
      if (!m_trackActiveObjects)
        return;

      if (m_tempTrackedObjects == null)
        m_tempTrackedObjects = new HashSet<T>(m_trackedObjects.Comparer);

      int totalCount = m_trackedObjects.Count + m_freeItems.Count;

      foreach (T item in m_trackedObjects)
      {
        if (m_poolBoy.IsActive(item))
        {
          m_tempTrackedObjects.Add(item);
        }
        else
        {
          //Is above capacity
          if (m_poolCapacity > 0 && totalCount > m_poolCapacity)
          {
            totalCount--;
            m_poolBoy.Free(item);
          }
          else
          {
            m_freeItems.Enqueue(item);
          }
        }
      }

      //Swap the tracking sets
      HashSet<T> swap = m_trackedObjects;
      m_trackedObjects = m_tempTrackedObjects;
      m_tempTrackedObjects = swap;

      m_tempTrackedObjects.Clear();
    }

    private void TrimPool()
    {
      lock (m_sync)
      {
        //Sweep if necessary...
        if (m_sweepActiveList)
          SweepActiveListAndReturn();

        //Keep dequeing and freeing until we get at capacity
        while (!IsAboveCapacity)
        {
          m_poolBoy.Free(m_freeItems.Dequeue());
        }
      }
    }
  }
}
