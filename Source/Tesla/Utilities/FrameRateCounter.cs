﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;

namespace Tesla.Utilities
{
    /// <summary>
    /// Simple utility for keeping track of FPS.
    /// </summary>
    public sealed class FrameRateCounter
    {
        private TimeSpan m_elapsedTime, m_updateInterval;
        private int m_frameCounter, m_currentFrameRate;

        /// <summary>
        /// Gets or sets the update interval of the counter.
        /// </summary>
        public TimeSpan UpdateInterval
        {
            get
            {
                return m_updateInterval;
            }
            set
            {
                m_updateInterval = value;
            }
        }

        /// <summary>
        /// Gets the current frame rate per second.
        /// </summary>
        public int CurrentFrameRate
        {
            get
            {
                return m_currentFrameRate;
            }
        }

        /// <summary>
        /// Constructs a new <see cref="FrameRateCounter"/>.
        /// </summary>
        public FrameRateCounter() : this(TimeSpan.FromSeconds(1)) { }

        /// <summary>
        /// Constructs a new <see cref="FrameRateCounter"/>.
        /// </summary>
        /// <param name="updateInterval">Interval at which the counter updates the current frame rate.</param>
        public FrameRateCounter(TimeSpan updateInterval)
        {
            if (updateInterval <= TimeSpan.Zero)
                updateInterval = TimeSpan.FromSeconds(1);
             
            m_updateInterval = updateInterval;
            m_elapsedTime = TimeSpan.Zero;
            m_frameCounter = 0;
            m_currentFrameRate = 0;
        }

        /// <summary>
        /// Updates the counter.
        /// </summary>
        /// <param name="time">Current update time.</param>
        public void Update(IGameTime time)
        {
            m_elapsedTime += time.ElapsedGameTime;

            if(m_elapsedTime > m_updateInterval)
            {
                m_elapsedTime -= m_updateInterval;
                m_currentFrameRate = m_frameCounter;
                m_frameCounter = 0;
            }
        }

        /// <summary>
        /// Notifies the counter that one frame has been drawn.
        /// </summary>
        public void IncrementOneFrame()
        {
            m_frameCounter++;
        }

        /// <summary>
        /// Resets the counter.
        /// </summary>
        public void Reset()
        {
            m_frameCounter = 0;
            m_currentFrameRate = 0;
            m_elapsedTime = TimeSpan.Zero;
        }
    }
}
