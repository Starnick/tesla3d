﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;

namespace Tesla.Utilities
{
    /// <summary>
    /// Simple cyclic reference detector to be used when processing parent-child or nested relationships in order to
    /// detect cycles in the relationship. The object is added as a reference, processed, then removed as a reference. 
    /// If the object is added as a reference again, an exception is thrown, or it can be queried if it has already been announced.
    /// </summary>
    public sealed class CyclicReferenceDetector
    {
        private Dictionary<Object, bool> m_detector;

        /// <summary>
        /// Constructs a new instance of the <see cref="CyclicReferenceDetector"/> class.
        /// </summary>
        public CyclicReferenceDetector()
        {
            m_detector = new Dictionary<object, bool>(new ReferenceEqualityComparer<Object>());
        }

        /// <summary>
        /// Adds a reference to the detector.
        /// </summary>
        /// <param name="value">The object reference.</param>
        /// <exception cref="TeslaException">Thrown if the object has already been announced, meaning a a circular loop between references has been found.</exception>
        public void AddReference(Object value)
        {
            if(m_detector.ContainsKey(value))
                throw new TeslaException(StringLocalizer.Instance.GetLocalizedString("CyclicReferenceDetected"));

            m_detector.Add(value, true);
        }

        /// <summary>
        /// Removes a reference from the detector.
        /// </summary>
        /// <param name="value">The object reference.</param>
        public void RemoveReference(Object value)
        {
            m_detector.Remove(value);
        }

        /// <summary>
        /// Checks if the reference has already been added to the detector.
        /// </summary>
        /// <param name="value">The object reference.</param>
        /// <returns>True if the reference has been seen by the detector, false otherwise.</returns>
        public bool IsCyclicReference(Object value)
        {
            return m_detector.ContainsKey(value);
        }
    }
}
