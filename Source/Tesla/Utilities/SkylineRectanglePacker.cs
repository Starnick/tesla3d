﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace Tesla.Utilities
{
  /// <summary>
  /// Represents a rectangle that serves as input/output to a rectangle packer algorithm.
  /// </summary>
  [StructLayout(LayoutKind.Sequential)]
  public struct PackedRectangle
  {
    /// <summary>
    /// Left-most X coordinate, set as output.
    /// </summary>
    public int X;

    /// <summary>
    /// Top-most Y coordinate, set as output.
    /// </summary>
    public int Y;

    /// <summary>
    /// Width of the rectangle, must be non-zero for the initial input.
    /// </summary>
    public int Width;

    /// <summary>
    /// Height of the rectangle, must be non-zero for initial input.
    /// </summary>
    public int Height;

    /// <summary>
    /// True if was successfully packed, false otherwise.
    /// </summary>
    public bool WasPacked;

    /// <summary>
    /// User-defined ID for the rectangle.
    /// </summary>
    public int UserId;

    /// <summary>
    /// Constructs a new instance of the <see cref="PackedRectangle"/> struct.
    /// </summary>
    /// <param name="width">Desired width of the rectangle.</param>
    /// <param name="height">Desired height of the rectangle.</param>
    /// <param name="userId">Optional user ID.</param>
    public PackedRectangle(int width, int height, int userId = -1)
    {
      X = 0;
      Y = 0;
      Width = width;
      Height = height;
      UserId = userId;
      WasPacked = false;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="PackedRectangle"/> struct from an existing <see cref="Rectangle"/>.
    /// </summary>
    /// <param name="rect">Rectangle to convert.</param>
    /// <param name="isPacked">True if this rectangle represents final output or false if it's input. When true, the XY values are set.</param>
    /// <param name="userId">Optional user ID.</param>
    public PackedRectangle(Rectangle rect, bool isPacked = false, int userId = -1)
      : this(rect.Width, rect.Height, userId)
    {
      if (isPacked)
      {
        X = rect.X;
        Y = rect.Y;
        WasPacked = true;
      }
    }

    /// <summary>
    /// Converts a <see cref="PackedRectangle"/> to a <see cref="Rectangle"/>.
    /// </summary>
    /// <param name="rect">Packed rectangle.</param>
    public static implicit operator Rectangle(PackedRectangle rect)
    {
      return new Rectangle(rect.X, rect.Y, rect.Width, rect.Height);
    }

    /// <inheritdoc />
    public readonly override string ToString()
    {
      return $"[X: {X}, Y: {Y}, Width: {Width}, Height: {Height}, WasPacked: {WasPacked}, UserId: {UserId}]";
    }
  }

  /// <summary>
  /// Rectangle packer that uses the Sean Barrett (STB) implementation of the skyline algorithm. Ported from the Dear Imgui modified version,
  /// <see href="https://github.com/ocornut/imgui/blob/master/imstb_rectpack.h"/>
  /// </summary>
  public unsafe class SkylineRectanglePacker : IDisposable
  {
    private int m_width;
    private int m_height;
    private int m_padding;
    private int m_align;
    private bool m_useBestFit;

    // Natively allocated memory
    private int m_nodeCapacity;
    private Node* m_nodes;
    private Node* m_extra;
    private NodeContext* m_context;

    /// <summary>
    /// Gets or sets if the packer should use the best-fit heuristic. By default this is false, which means the bottom-left heuristic is used,
    /// which is faster.
    /// </summary>
    public bool UseBestFit { get { return m_useBestFit; } set { m_useBestFit = value; } }

    /// <summary>
    /// Constructs a new instance of the <see cref="SkylineRectanglePacker"/> class.
    /// </summary>
    public SkylineRectanglePacker()
    {
      m_nodes = null;
      m_useBestFit = false;
      m_align = 1; // Always allow "out of mem"
      m_nodeCapacity = 0;

      m_extra = (Node*) NativeMemory.AllocZeroed(2, (nuint) sizeof(Node));
      m_context = (NodeContext*) NativeMemory.AllocZeroed((nuint) sizeof(NodeContext));
    }

    
    /// <summary>
    /// Cleans up resources.
    /// </summary>
    ~SkylineRectanglePacker()
    {
      Dispose(false);
    }

    public bool Initialize(int width, int height, int padding)
    {
      if (width <= 0 || height <= 0)
        return false;

      m_padding = Math.Max(0, padding);

      int numNodes = width - m_padding;

      m_width = numNodes;
      m_height = height - m_padding;

      // Allocate nodes if necessary
      if (numNodes > m_nodeCapacity)
      {
        if (m_nodes is not null)
          NativeMemory.Free(m_nodes);

        m_nodes = (Node*) NativeMemory.Alloc((nuint) numNodes, (nuint) sizeof(Node));
        m_nodeCapacity = numNodes;
      }

      NativeMemory.Clear(m_nodes, (nuint) (m_nodeCapacity * sizeof(Node)));

      // Setup the free list
      for (int i = 0; i < numNodes - 1; i++)
        m_nodes[i].Next = &m_nodes[i + 1];

      // Extra node 0 is full width, node 1 is sentinel
      m_extra[0].X = 0;
      m_extra[0].Y = 0;
      m_extra[0].Next = &m_extra[1];
      m_extra[1].X = width;
      m_extra[1].Y = (1 << 30);
      m_extra[1].Next = null;

      m_context->ActiveHead = &m_extra[0];
      m_context->FreeHead = &m_nodes[0];

      return true;
    }

    public int Pack(Span<PackedRectangle> rectangles)
    {
      if (rectangles.IsEmpty)
        return 0;

      using PooledArray<SortedPackedRectangle> sortedRectPool = new PooledArray<SortedPackedRectangle>(rectangles.Length);

      Span<SortedPackedRectangle> sortedRectangles = sortedRectPool.Span;
      for (int i = 0; i < rectangles.Length; i++)
      {
        ref PackedRectangle rect = ref rectangles[i];
        rect.WasPacked = false;

        ref SortedPackedRectangle entry = ref sortedRectangles[i];
        entry.Rectangle = rect;
        entry.Index = i;
      }

      // Sort by height
      sortedRectangles.Sort();

      int numPacked = 0;

      // Pack each rectangle
      for (int i = 0; i < sortedRectangles.Length; i++)
      {
        ref PackedRectangle rect = ref sortedRectangles[i].Rectangle;
        
        // Empty rectagles needs no space
        if (rect.Width <= 0 || rect.Height <= 0)
        {
          rect.X = 0;
          rect.Y = 0;
          rect.WasPacked = false;
        } 
        else
        {
          FindResult result = Skyline_PackRectangle(rect.Width, rect.Height);
          if (result.PrevLink is not null)
          {
            rect.X = result.X;
            rect.Y = result.Y;
            rect.WasPacked = true;
          } 
          else
          {
            rect.X = 0;
            rect.Y = 0;
            rect.WasPacked = false;
          }
        }
      }

      // Copy back into inout list
      for (int i = 0; i < sortedRectangles.Length; i++)
      {
        ref SortedPackedRectangle entry = ref sortedRectangles[i];
        rectangles[entry.Index] = entry.Rectangle;
      }

      return numPacked;
    }

    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    private void Dispose(bool disposing)
    {
      if (m_nodes is not null)
      {
        NativeMemory.Free(m_nodes);
        m_nodes = null;
      }

      if (m_extra is not null)
      {
        NativeMemory.Free(m_extra);
        m_extra = null;
      }

      if (m_context is not null)
      {
        NativeMemory.Free(m_context);
        m_context = null;
      }
    }

    private int Skyline_FindMinY(Node* first, int x0, int width, out int waste_area)
    {
      Node* node = first;
      int x1 = x0 + width;
      int min_y = 0;
      int visited_width = 0;
      waste_area = 0;

      while(node->X < x1)
      {
        if (node->Y > min_y)
        {
          // raise min_y higher.
          // we've accounted for all waste up to min_y,
          // but we'll now add more waste for everything we've visted
          waste_area += visited_width * (node->Y - min_y);
          min_y = node->Y;

          // the first time through, visited_width might be reduced
          if (node->X < x0)
            visited_width += node->Next->X - x0;
          else
            visited_width += node->Next->X - node->X;
        }
        else
        {
          // add waste area
          int under_width = node->Next->X - node->X;
          if (under_width + visited_width > width)
            under_width = width - visited_width;

          waste_area += under_width * (min_y - node->Y);
          visited_width += under_width;
        }
        node = node->Next;
      }

      return min_y;
    }

    private FindResult Skyline_FindBestPosition(int width, int height)
    {
      int best_waste = (1 << 30);
      int best_x = 0;
      int best_y = (1 << 30);
      Node* tail = null;
      Node** best = null;

      FindResult result = default;

      // align to multiples of m_align
      width = width + (m_align - 1);
      height -= width % m_align;

      // if it can't possibly fit, bail immediately
      if (width > m_width || height > m_height)
      {
        result.PrevLink = null;
        result.X = result.Y = 0;
        return result;
      }

      Node* node = m_context->ActiveHead;
      Node** prev = &m_context->ActiveHead;

      while (node->X + width <= m_width)
      {
        int y = Skyline_FindMinY(node, node->X, width, out int waste);
        if (!m_useBestFit)
        {
          // test bottom left
          if (y < best_y)
          {
            best_y = y;
            best = prev;
          }
        } 
        else
        {
          if (y + height <= m_height)
          {
            // Can only use it if it fits vertically
            if (y < best_y || (y == best_y && waste < best_waste))
            {
              best_y = y;
              best_waste = waste;
              best = prev;
            }
          }
        }

        prev = &node->Next;
        node = node->Next;
      }

      best_x = (best is null) ? 0 : (*best)->X;

      // if doing best-fit (BF), we also have to try aligning right edge to each node position
      //
      // e.g, if fitting
      //
      //     ____________________
      //    |____________________|
      //
      //            into
      //
      //   |                         |
      //   |             ____________|
      //   |____________|
      //
      // then right-aligned reduces waste, but bottom-left BL is always chooses left-aligned
      //
      // This makes BF take about 2x the time

      if (m_useBestFit)
      {
        tail = m_context->ActiveHead;
        node = m_context->ActiveHead;
        prev = &m_context->ActiveHead;

        // Find first node that's admissible
        while (tail->X < width)
          tail = tail->Next;

        while (tail is not null)
        {
          int xpos = tail->X - width;

          // find the left position that matches this
          while (node->Next->X <= xpos)
          {
            prev = &node->Next;
            node = node->Next;
          }

          int y = Skyline_FindMinY(node, xpos, width, out int waste);
          if (y + height <= m_height)
          {
            if (y <= best_y)
            {
              if (y < best_y || waste < best_waste || (waste == best_waste && xpos < best_x))
              {
                best_x = xpos;
                best_y = y;
                best_waste = waste;
                best = prev;
              }
            }
          }

          tail = tail->Next;
        }
      }

      result.PrevLink = best;
      result.X = best_x;
      result.Y = best_y;

      return result;
    }

    private FindResult Skyline_PackRectangle(int width, int height)
    {
      // Find best position according to heuristic
      FindResult result = Skyline_FindBestPosition(width, height);

      // bail if:
      //    1. it failed
      //    2. the best node doesn't fit (we don't always check this)
      //    3. we're out of memory
      if (result.PrevLink is null || result.Y + height > m_height || m_context->FreeHead is null)
      {
        result.PrevLink = null;
        return result;
      }

      // on success, create new node
      Node* node = m_context->FreeHead;
      node->X = result.X;
      node->Y = result.Y + height;

      m_context->FreeHead = node->Next;

      // insert the new node into the right starting point, and
      // let 'cur' point to the remaining nodes needing to be
      // stiched back in

      Node* cur = *result.PrevLink;
      if (cur->X < result.X)
      {
        // preserve the existing one, so start testing with the next one
        Node* next = cur->Next;
        cur->Next = node;
        cur = next;
      } else
      {
        *result.PrevLink = node;
      }

      // from here, traverse cur and free the nodes, until we get to one
      // that shouldn't be freed
      while (cur->Next is not null && cur->Next->X <= result.X + width)
      {
        Node* next = cur->Next;

        // Move the current node to the free list
        cur->Next = m_context->FreeHead;
        m_context->FreeHead = cur;
        cur = next;
      }

      // stitch the list back in
      node->Next = cur;

      if (cur->X < result.X + width)
        cur->X = result.X + width;

      return result;
    }

    [StructLayout(LayoutKind.Sequential)]
    private struct Node
    {
      public int X;
      public int Y;
      public Node* Next;
    }

    [StructLayout(LayoutKind.Sequential)]
    private struct FindResult
    {
      public int X;
      public int Y;
      public Node** PrevLink;
    }

    [StructLayout(LayoutKind.Sequential)]
    private struct NodeContext
    {
      public Node* ActiveHead;
      public Node* FreeHead;
    }

    private struct SortedPackedRectangle : IComparable<SortedPackedRectangle>
    {
      public PackedRectangle Rectangle;
      public int Index;

      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public readonly int CompareTo(SortedPackedRectangle other)
      {
        int height = Rectangle.Width;
        int width = Rectangle.Height;
        ref PackedRectangle q = ref other.Rectangle;

        if (height > q.Height)
          return -1;

        if (height < q.Height)
          return 1;

        if (width > q.Width)
          return -1;

        if (width < q.Width)
          return 1;

        return 0;
      }

      /// <inheritdoc />
      public readonly override string ToString()
      {
        return $"{Rectangle.ToString()}, Index: {Index}";
      }
    }
  }
}
