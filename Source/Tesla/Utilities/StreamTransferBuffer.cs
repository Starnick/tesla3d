﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.IO;

#nullable enable

namespace Tesla.Utilities
{
  /// <summary>
  /// A buffer that facilitates transfer between managed/unmanaged memory and <see cref="Stream"/> objects. An intermediate byte buffer is created and pinned,
  /// allow for copying between it and unmanaged memory.
  /// </summary>
  public class StreamTransferBuffer : IDisposable
  {
    private const int MaxStackAllocSize = 128;
    private bool m_isDisposed;
    private int m_minBufferSize;
    private IMemoryOwnerEx<byte>? m_buffer;
    private int m_lastReadByteCount;

    /// <summary>
    /// Gets the byte array used by the buffer.
    /// </summary>
    public Span<byte> Bytes
    {
      get
      {
        if (m_buffer == null)
          m_buffer = MemoryAllocator<byte>.PooledManaged.Allocate(m_minBufferSize);

        return m_buffer.Span;
      }
    }

    /// <summary>
    /// Gets the size of the buffer in bytes.
    /// </summary>
    public int Length
    {
      get
      {
        return Bytes.Length;
      }
    }

    /// <summary>
    /// Gets the number of bytes last read.
    /// </summary>
    public int LastReadByteCount
    {
      get
      {
        return m_lastReadByteCount;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="StreamTransferBuffer"/> class. The buffer size will be 81,920 bytes.
    /// </summary>
    public StreamTransferBuffer() : this(81920) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="StreamTransferBuffer"/> class.
    /// </summary>
    /// <param name="numBytes">Size of the buffer, but cannot exceed LOH allocation of 85,000 bytes.</param>
    public StreamTransferBuffer(int numBytes)
    {
      if (numBytes <= 0)
        numBytes = 81920;

      //Restrict buffer size to be less than LOH min size < 85,000 bytes
      m_minBufferSize = Math.Min(numBytes, BufferHelper.LOHThreshold - 1);
      m_isDisposed = false;
      m_lastReadByteCount = 0;
    }

    /// <summary>
    /// Finalizes an instance of the <see cref="StreamTransferBuffer"/> class.
    /// </summary>
    ~StreamTransferBuffer()
    {
      Dispose(false);
    }

    /// <summary>
    /// Reads a number of bytes from the stream into the specified span. The number of bytes is determined by the span's length.
    /// </summary>
    /// <param name="input">Stream to read from.</param>
    /// <param name="bytes">Span to hold the read data.</param>
    /// <returns>Actual read number of bytes (<see cref="LastReadByteCount"/> is also set to this).</returns>
    public int Read(Stream input, Span<byte> bytes)
    {
      int numRead = 0;
      int numRequested = bytes.Length;
      while (numRead < numRequested)
      {
        int bytesRead = input.Read(bytes.Slice(numRead));
        if (bytesRead == 0)
          break;

        numRead += bytesRead;
      }

      m_lastReadByteCount = numRead;
      return m_lastReadByteCount;
    }

    /// <summary>
    /// Reads the value from the stream. First the buffer is filled from the stream by the data's size in bytes, then the data is copied.
    /// </summary>
    /// <typeparam name="T">Type of data to read.</typeparam>
    /// <param name="input">Stream to read from.</param>
    /// <param name="value">The value to read.</param>
    /// <returns>True if the value was read from the stream, false if there was not enough bytes to read.</returns>
    public unsafe bool Read<T>(Stream input, out T value) where T : unmanaged
    {
      int size = BufferHelper.SizeOf<T>();
      Span<byte> bytes = (size > MaxStackAllocSize) ? Bytes.Slice(0, size) : stackalloc byte[size];

      //Fill buffer...validate we read the expected # of bytes
      if (Read(input, bytes) != size)
      {
        value = default;
        return false;
      }

      //Copy data
      value = bytes.Read<T>();
      return true;
    }

    /// <summary>
    /// Reads the value from the stream. First the buffer is filled from the stream by the data's size in bytes, then the data is copied.
    /// </summary>
    /// <typeparam name="T">Type of data to read.</typeparam>
    /// <param name="input">Stream to read from.</param>
    /// <returns>The read value.</returns>
    public unsafe T Read<T>(Stream input) where T : unmanaged
    {
      int size = BufferHelper.SizeOf<T>();
      Span<byte> bytes = (size > MaxStackAllocSize) ? Bytes.Slice(0, size) : stackalloc byte[size];

      //Fill buffer...validate we read the expected # of bytes
      if (Read(input, bytes) != size)
        throw new EndOfStreamException(StringLocalizer.Instance.GetLocalizedString("EOFStream"));

      //Copy data
      return bytes.Read<T>();
    }

    /// <summary>
    /// Writes the span of bytes into the stream.
    /// </summary>
    /// <param name="output">Stream to write to.</param>
    /// <param name="bytes">Span of bytes to write.</param>
    public void Write(Stream output, ReadOnlySpan<byte> bytes)
    {
      m_lastReadByteCount = 0;
      output.Write(bytes);
    }

    /// <summary>
    /// Writes the value to the stream.
    /// </summary>
    /// <typeparam name="T">Type of data to write.</typeparam>
    /// <param name="output">Stream to write to.</param>
    /// <param name="value">Value to write.</param>
    public unsafe void Write<T>(Stream output, in T value) where T : unmanaged
    {
      int size = BufferHelper.SizeOf<T>();
      fixed (T* ptr = &value)
      {
        ReadOnlySpan<byte> bytes = new ReadOnlySpan<byte>(ptr, size);
        Write(output, bytes);
      }
    }

    /// <summary>
    /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
    /// </summary>
    public void Dispose()
    {
      Dispose(true);

      GC.SuppressFinalize(this);
    }

    /// <summary>
    /// Releases unmanaged and - optionally - managed resources.
    /// </summary>
    /// <param name="isDisposing"><c>True</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
    protected void Dispose(bool isDisposing)
    {
      if (!m_isDisposed)
      {
        if (isDisposing && m_buffer != null)
          m_buffer.Dispose();

        m_buffer = null;
        m_lastReadByteCount = 0;

        m_isDisposed = true;
      }
    }
  }
}
