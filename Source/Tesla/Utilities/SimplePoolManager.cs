﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;

#nullable enable

namespace Tesla.Utilities
{
  /// <summary>
  /// A simple delegate-based implementation of a pool manager.
  /// </summary>
  /// <typeparam name="T">Type of pooled object.</typeparam>
  public sealed class SimplePoolManager<T> : IPoolManager<T> where T : class
  {
    private Func<T> m_allocate;
    private Func<T, bool>? m_isActive;
    private Action<T>? m_free;

    /// <summary>
    /// Constructs a new instance of the <see cref="SimplePoolManager{T}"/> class.
    /// </summary>
    /// <param name="allocateFunc">The allocate function.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the allocate function is null.</exception>
    public SimplePoolManager(Func<T> allocateFunc) : this(allocateFunc, null, null) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="SimplePoolManager{T}"/> class.
    /// </summary>
    /// <param name="allocateFunc">The allocate function.</param>
    /// <param name="freeFunc">The free function, optional.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the allocate function is null.</exception>
    public SimplePoolManager(Func<T> allocateFunc, Action<T> freeFunc) : this(allocateFunc, null, freeFunc) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="SimplePoolManager{T}"/> class.
    /// </summary>
    /// <param name="allocateFunc">The allocate function.</param>
    /// <param name="isActiveFunc">The IsActive function, optional.</param>
    /// <param name="freeFunc">The free function, optional.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the allocate function is null.</exception>
    public SimplePoolManager(Func<T> allocateFunc, Func<T, bool>? isActiveFunc, Action<T>? freeFunc)
    {
      ArgumentNullException.ThrowIfNull(allocateFunc, nameof(allocateFunc));

      m_allocate = allocateFunc;
      m_isActive = isActiveFunc;
      m_free = freeFunc;
    }

    /// <summary>
    /// Allocates a new object, with the possibility of failure.
    /// </summary>
    /// <param name="item">Newly created object.</param>
    /// <returns>True if the object was created, false if otherwise.</returns>
    public bool Allocate([MaybeNullWhen(false)] out T item)
    {
      item = m_allocate();
      return item is not null;
    }

    /// <summary>
    /// Determines if the item is active or not active. Inactive items will automatically be recycled and returned to the pool. This is to support
    /// "fire and forget" mechanisms.
    /// </summary>
    /// <param name="item">Object to determine if active or not.</param>
    /// <returns>True if the object is still active, false if otherwise.</returns>
    public bool IsActive(T item)
    {
      if (item is null || m_isActive is null)
        return true;

      return m_isActive(item);
    }

    /// <summary>
    /// Does the necessary cleanup and disposal of an object.
    /// </summary>
    /// <param name="item">Object to cleanup.</param>
    public void Free(T item)
    {
      if (item is null)
        return;

      if (m_free is null)
      {
        if (item is IDisposable disp)
          disp.Dispose();

        return;
      }

      m_free(item);
    }
  }
}
