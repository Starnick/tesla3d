﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;

namespace Tesla.Utilities
{
    /// <summary>
    /// Wrapper around <see cref="ObjectPool{T}"/>.
    /// <remarks>
    /// Use for objects that have a parameterless constructor, there is no management for inactive objects or tracking for active objects
    /// so every object needs to be explicitly returned back to the pool. The pool allows for unlimited number of objects and is thread-safe. If you need
    /// a more specialized pool to manage active objects, especially heavy resources, you need to create your own pool/pool manager as this wrapper is intended for fast 
    /// "I need a temporary object" operations.
    /// </remarks>
    /// </summary>
    /// <typeparam name="T">Type of object in the pool.</typeparam>
    public static class Pool<T> where T : class, new()
    {
        private static readonly ObjectPool<T> s_pool;

        /// <summary>
        /// Gets or sets the the maximum count of objects that can be in the pool at any given time. If an object is returned to the pool and the available objects in the pool
        /// are at this capacity, then the returned object is disposed of. If more objects than the pool capacity are requested, those objects are created (meaning <see cref="Fetch"/> will
        /// always succeed). To disable pool capacity, set this value to zero.
        /// </summary>
        public static int MaxObjectsInPool
        {
            get
            {
                return s_pool.PoolCapacity;
            }
            set
            {
                s_pool.PoolCapacity = Math.Max(value, 0);
            }
        }

        static Pool()
        {
            Type type = typeof(T);
            IEqualityComparer<T> comparer = null;
            if(type.IsValueType)
                comparer = EqualityComparer<T>.Default;
            else
                comparer = new ReferenceEqualityComparer<T>();

            s_pool = new ObjectPool<T>(new DefaultPoolManager(), 0, false, comparer);
            s_pool.SweepActiveList = false;
        }

        /// <summary>
        /// Fetches an object from the pool.
        /// </summary>
        /// <returns>Pooled object.</returns>
        public static T Fetch()
        {
            T obj;
            s_pool.TryFetch(out obj);

            return obj;
        }

        /// <summary>
        /// Fetches a list of objects from the pool.
        /// </summary>
        /// <param name="items">List to store the items.</param>
        /// <param name="requestedCount">Number of items to request.</param>
        /// <returns>Actual number of objects fetched from the pool.</returns>
        public static int Fetch(IList<T> items, int requestedCount)
        {
            return s_pool.TryFetch(items, requestedCount);
        }

        /// <summary>
        /// Returns an object to the pool.
        /// </summary>
        /// <param name="obj">Pooled object.</param>
        public static void Return(T obj)
        {
            s_pool.Return(obj);
        }

        /// <summary>
        /// Returns a collection of objects to the pool.
        /// </summary>
        /// <param name="objs">Pooled objects.</param>
        public static void Return(IEnumerable<T> objs)
        {
            s_pool.Return(objs);
        }

        /// <summary>
        /// Returns a list of objects to the pool.
        /// </summary>
        /// <param name="objs">Pooled objects.</param>
        public static void Return(IList<T> objs)
        {
            s_pool.Return(objs);
        }

        private sealed class DefaultPoolManager : IPoolManager<T>
        {
            public bool Allocate(out T item)
            {
                item = new T();
                return true;
            }

            public bool IsActive(T item)
            {
                return true;
            }

            public void Free(T item)
            {
            }
        }
    }
}
