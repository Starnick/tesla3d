﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;

namespace Tesla.Application
{
  /// <summary>
  /// A read-only collection of windows.
  /// </summary>
  public sealed class WindowCollection : ReadOnlyList<IWindow>
  {
    /// <summary>
    /// Gets an <see cref="IWindow"/> contained in this collection that corresponds to the specified window handle.
    /// </summary>
    /// <param name="handle">Window handle.</param>
    /// <returns>Corresponding window, or null if none match.</returns>
    public IWindow this[IntPtr handle]
    {
      get
      {
        foreach (IWindow window in this)
        {
          if (window is not null && window.Handle == handle)
            return window;
        }

        return null;
      }
    }

    /// <summary>
    /// Gets if any window in the collection currently has focus.
    /// </summary>
    public bool FocusedAnyWindow
    {
      get
      {
        foreach (IWindow window in this)
        {
          if (window.Focused)
            return true;
        }

        return false;
      }
    }

    /// <summary>
    /// Gets if the mouse is inside any window in the collection.
    /// </summary>
    public bool IsMouseInsideAnyWindow
    {
      get
      {
        foreach (IWindow window in this)
        {
          if (window.IsMouseInsideWindow)
            return true;
        }

        return false;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="WindowCollection"/> class.
    /// </summary>
    /// <param name="windowList">Collection of windows</param>
    public WindowCollection(IList<IWindow> windowList) : base(windowList) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="WindowCollection"/> class.
    /// </summary>
    /// <param name="windows">Collection of windows</param>
    public WindowCollection(IEnumerable<IWindow> windows) : base(windows) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="WindowCollection"/> class.
    /// </summary>
    /// <param name="windows">Collection of windows</param>
    public WindowCollection(params IWindow[] windows) : base(windows) { }
  }
}
