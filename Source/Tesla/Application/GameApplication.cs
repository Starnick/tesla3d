﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Threading;
using Tesla.Content;
using Tesla.Graphics;
using Tesla.Gui;
using Tesla.Input;

namespace Tesla.Application
{
  public abstract class GameApplication
  {
    private Engine m_engine;
    private EngineServiceRegistry m_services;
    private GameTimer m_timer;
    private IWindow m_gameWindow;
    private ContentManager m_content;
    private SwapChain m_swapChain;
    private IApplicationHost m_host;
    private IRenderSystem m_renderSystem;
    private IRenderContext m_renderContext;
    private Color m_clearColor;
    private float m_clearDepth;
    private int m_clearStencil;

    private GameTime m_gameTime;
    private TimeSpan m_totalGameTime;
    private TimeSpan m_targetElapsedTime;
    private TimeSpan m_inactiveSleepTime;
    private TimeSpan m_maximumElapsedTime;
    private TimeSpan m_lastFrameElapsedTime;
    private TimeSpan m_accumulatedElapsedGameTime;
    private int[] m_lastUpdateCount;
    private int m_nextLastUpdateCountIndex;
    private float m_updateCountAvgSlowLimit;
    private bool m_isFixedTimeStep;
    private bool m_forceElapsedTimeToZero;
    private bool m_isExiting;
    private bool m_isActive;
    private bool m_isDrawRunningSlowly;
    private bool m_suppressDraw;
    private bool m_isHeadless;
    private bool m_isRunning;

    //Initialize variables
    private PresentationParameters m_initPP;
    private IResourceRepository m_initRepo;
    private IPlatformInitializer m_initPlatform;

    public Engine Engine
    {
      get
      {
        return m_engine;
      }
    }

    public EngineServiceRegistry Services
    {
      get
      {
        return m_services;
      }
    }

    public IWindow GameWindow
    {
      get
      {
        return m_gameWindow;
      }
    }

    public IApplicationHost GameHost
    {
      get
      {
        return m_host;
      }
    }

    public ContentManager Content
    {
      get
      {
        return m_content;
      }
    }

    public IRenderSystem RenderSystem
    {
      get
      {
        return m_renderSystem;
      }
    }

    /// <summary>
    /// Gets or sets the value that the backbuffer is cleared to.
    /// </summary>
    public Color ClearColor
    {
      get
      {
        return m_clearColor;
      }
      set
      {
        m_clearColor = value;
      }
    }

    /// <summary>
    /// Gets or sets the value that the depth buffer is cleared to.
    /// </summary>
    public float ClearDepth
    {
      get
      {
        return m_clearDepth;
      }
      set
      {
        m_clearDepth = value;
      }
    }

    /// <summary>
    /// Gets or sets the value that the stencil buffer is cleared to.
    /// </summary>
    public int ClearStencil
    {
      get
      {
        return m_clearStencil;
      }
      set
      {
        m_clearStencil = value;
      }
    }

    public TimeSpan TargetElapsedTime
    {
      get
      {
        return m_targetElapsedTime;
      }
      set
      {
        if (value <= TimeSpan.Zero)
          throw new ArgumentOutOfRangeException("value", StringLocalizer.Instance.GetLocalizedString("TimeSpanCannotBeZero"));

        m_targetElapsedTime = value;
      }
    }

    public TimeSpan InactiveSleepTime
    {
      get
      {
        return m_inactiveSleepTime;
      }
      set
      {
        if (value < TimeSpan.Zero)
          throw new ArgumentOutOfRangeException("value", StringLocalizer.Instance.GetLocalizedString("TimeSpanCannotBeZero"));

        m_inactiveSleepTime = value;
      }
    }

    public bool IsFixedTimeStep
    {
      get
      {
        return m_isFixedTimeStep;
      }
      set
      {
        m_isFixedTimeStep = value;
      }
    }

    public bool IsActive
    {
      get
      {
        return m_isActive;
      }
    }

    public bool IsHeadless
    {
      get
      {
        return m_isHeadless;
      }
    }

    public bool IsRunning
    {
      get
      {
        return m_isRunning;
      }
    }

    public bool SuppressDrawOneFrame
    {
      get
      {
        return m_suppressDraw;
      }
      set
      {
        m_suppressDraw = value;
      }
    }

    public GameApplication(IPlatformInitializer platformInitializer)
    {
      PresentationParameters pp = new PresentationParameters();
      pp.BackBufferFormat = SurfaceFormat.Color;
      pp.BackBufferHeight = 600;
      pp.BackBufferWidth = 800;
      pp.DepthStencilFormat = DepthFormat.Depth32Stencil8;
      pp.DisplayOrientation = DisplayOrientation.Default;
      pp.IsFullScreen = false;
      pp.MultiSampleCount = 0;
      pp.MultiSampleQuality = 0;
      pp.PresentInterval = PresentInterval.Immediate;
      pp.RenderTargetUsage = RenderTargetUsage.DiscardContents;

      m_initPP = pp;
      m_initRepo = null;
      m_initPlatform = platformInitializer;
    }

    public GameApplication(PresentationParameters presentParams, IPlatformInitializer platformInitializer)
    {
      m_initPP = presentParams;
      m_initRepo = null;
      m_initPlatform = platformInitializer;
    }

    public GameApplication(PresentationParameters presentParams, IResourceRepository resourceRepo, IPlatformInitializer platformInitializer)
    {
      m_initPP = presentParams;
      m_initRepo = resourceRepo;
      m_initPlatform = platformInitializer;
    }

    public void Run(ApplicationMode mode = ApplicationMode.Normal)
    {
      Initialize(m_initPP, m_initRepo, m_initPlatform, mode);

      m_timer.Start();

      m_host.Run(GameLoopTick, m_gameWindow);
    }

    public void Exit()
    {
      m_isExiting = true;
      m_host.Shutdown();
    }

    public void ResetElapsedTime()
    {
      m_forceElapsedTimeToZero = true;
      m_isDrawRunningSlowly = false;

      Array.Clear(m_lastUpdateCount, 0, m_lastUpdateCount.Length);
      m_nextLastUpdateCountIndex = 0;
    }

    protected virtual void OnInitialize(Engine engine) { }
    protected virtual void OnExiting() { }
    protected virtual void ConfigureContentManager(ContentManager content) { }
    protected virtual void LoadContent(ContentManager content) { }
    protected virtual void UnloadContent(ContentManager content) { }
    protected virtual void OnViewportResized(IWindow gameWindow) { }

    protected abstract void Update(IGameTime time);
    protected abstract void Render(IRenderContext context, IGameTime time);

    private void Initialize(PresentationParameters presentParams, IResourceRepository resourceRepo, IPlatformInitializer platformInitializer, ApplicationMode mode)
    {
      m_engine = Engine.Initialize(platformInitializer);
      m_services = m_engine.Services;

      m_renderSystem = m_services.GetService<IRenderSystem>();
      if (m_renderSystem is not null)
        m_renderContext = m_renderSystem.ImmediateContext;

      m_host = m_services.GetService<IApplicationHost>();
      m_host.ShuttingDown += HandleHostShutDown;
      m_isHeadless = (mode == ApplicationMode.Headless) || m_renderSystem is null;
      m_gameWindow = null;

      if (!m_isHeadless)
      {
        m_gameWindow = m_host.CreateWindow(presentParams, mode == ApplicationMode.Normal); //true for toplevel, false for child window
        m_gameWindow.EnableInputEvents = false;
        m_gameWindow.EnablePaintEvents = false;
        m_gameWindow.EnableUserResizing = false;
        m_gameWindow.CenterToScreen();
        m_gameWindow.ResumeRendering += ResumeRendering;
        m_gameWindow.SuspendRendering += SuspendRendering;
        m_gameWindow.ClientSizeChanged += ClientSizeChanged;
        m_swapChain = m_gameWindow.SwapChain;

        // Setup the mouse for the game window in case it's not set elsewhere
        Mouse.WindowHandle = m_gameWindow.Handle;
        Rectangle bounds = m_gameWindow.ClientBounds;
        Mouse.SetPosition(bounds.Width / 2, bounds.Height / 2);
      }

      m_clearColor = Color.CornflowerBlue;
      m_clearDepth = SwapChain.DefaultClearDepth;
      m_clearStencil = 0;

      InitializeClocks();

      if (resourceRepo is null)
        m_content = new ContentManager(m_services);
      else
        m_content = new ContentManager(m_services, resourceRepo);

      //Null out init
      m_initPP = new PresentationParameters();
      m_initRepo = null;
      m_initPlatform = null;

      ConfigureContentManager(m_content);
      OnInitialize(m_engine);

      LoadContent(m_content);

      m_isRunning = true;
    }

    private void InitializeClocks()
    {
      m_gameTime = new GameTime(TimeSpan.Zero, TimeSpan.Zero);
      m_timer = new GameTimer();
      m_maximumElapsedTime = TimeSpan.FromMilliseconds(500.0f);
      m_isFixedTimeStep = true;
      m_isDrawRunningSlowly = false;
      m_suppressDraw = false;
      m_isActive = true;
      m_isExiting = false;
      m_inactiveSleepTime = TimeSpan.FromMilliseconds(20.0f);
      m_targetElapsedTime = TimeSpan.FromTicks(166667L);
      m_totalGameTime = TimeSpan.Zero;
      m_lastFrameElapsedTime = TimeSpan.Zero;
      m_accumulatedElapsedGameTime = TimeSpan.Zero;

      m_lastUpdateCount = new int[4];
      m_nextLastUpdateCountIndex = 0;

      //Calculate the update count avg slow limit (assuming moving average is >=3)
      //E.g., moving average of 4:
      //Avg slow limit = (2 * 2 + (4 - 2)) / 4 = 1.5f
      const int badUpdateCountTime = 2; //Number of bad frames, which is a frame that has at  least two updates
      int maxLastCount = 2 * Math.Min(badUpdateCountTime, m_lastUpdateCount.Length);
      m_updateCountAvgSlowLimit = (float) ((maxLastCount + (m_lastUpdateCount.Length - maxLastCount)) / m_lastUpdateCount.Length);
    }

    private void GameLoopTick()
    {
      if (m_isExiting)
        return;

      if (!m_isActive)
        Thread.Sleep(m_inactiveSleepTime);

      m_timer.Update();

      TimeSpan elapsedTime = m_timer.GameTime.ElapsedGameTime;

      if (m_forceElapsedTimeToZero)
      {
        elapsedTime = TimeSpan.Zero;
        m_forceElapsedTimeToZero = false;
      }

      if (elapsedTime > m_maximumElapsedTime)
        elapsedTime = m_maximumElapsedTime;

      bool suppressNextDraw = true;
      int updateCount = 1;
      TimeSpan singleFrameElapsedTime = elapsedTime;

      if (m_isFixedTimeStep)
      {
        if (Math.Abs(elapsedTime.Ticks - m_targetElapsedTime.Ticks) < (m_targetElapsedTime.Ticks >> 6))
          elapsedTime = m_targetElapsedTime;

        m_accumulatedElapsedGameTime += elapsedTime;

        //Calculate number of updates to do
        updateCount = (int) (m_accumulatedElapsedGameTime.Ticks / m_targetElapsedTime.Ticks);

        //If there is no need for update, then exit
        if (updateCount == 0)
        {
          //Check if we can sleep the thread to free CPU resources
          TimeSpan sleepTime = m_targetElapsedTime - m_accumulatedElapsedGameTime;
          if (sleepTime > TimeSpan.Zero)
            Thread.Sleep(sleepTime);

          return;
        }

        //Calculate a moving average on updatecount
        m_lastUpdateCount[m_nextLastUpdateCountIndex] = updateCount;
        float updateCountMean = 0;
        for (int i = 0; i < m_lastUpdateCount.Length; i++)
          updateCountMean += m_lastUpdateCount[i];

        updateCountMean /= m_lastUpdateCount.Length;
        m_nextLastUpdateCountIndex = (m_nextLastUpdateCountIndex + 1) % m_lastUpdateCount.Length;

        //Test when we are running slowly
        m_isDrawRunningSlowly = updateCountMean > m_updateCountAvgSlowLimit;

        //We are going to call Update "updateCount" times so we can subtract this from accumulated elapsed game time
        m_accumulatedElapsedGameTime = new TimeSpan(m_accumulatedElapsedGameTime.Ticks - (updateCount * m_targetElapsedTime.Ticks));
        singleFrameElapsedTime = m_targetElapsedTime;
      }
      else
      {
        Array.Clear(m_lastUpdateCount, 0, m_lastUpdateCount.Length);
        m_nextLastUpdateCountIndex = 0;
        m_isDrawRunningSlowly = false;
      }

      //Reset the time of the next frame
      for (m_lastFrameElapsedTime = TimeSpan.Zero; updateCount > 0 && !m_isExiting; updateCount--)
      {
        m_gameTime.Set(singleFrameElapsedTime, m_totalGameTime, m_isDrawRunningSlowly);

        try
        {
          DoUpdate(m_gameTime);

          //If no exception, then we can draw the frame
          suppressNextDraw &= m_suppressDraw;
          m_suppressDraw = false;
        }
        finally
        {
          m_lastFrameElapsedTime += singleFrameElapsedTime;
          m_totalGameTime += singleFrameElapsedTime;
        }
      }

      bool isMinimized = (m_gameWindow is not null) ? m_gameWindow.IsMinimized : false;

      if (!suppressNextDraw && !m_isExiting && !isMinimized)
      {
        DoRender(m_gameTime);
      }
    }

    private void DoUpdate(IGameTime gameTime)
    {
      //Setup time for computed parameters
      ComputedParameter.GameTime = gameTime;

      m_engine.Update(gameTime);

      // If got a Gui, start a new frame. No-op if not.
      AppGui.StartOrResumeFrame(GameWindow);

      Update(gameTime);
    }

    private void DoRender(IGameTime gameTime)
    {
      //Setup time for computed parameters
      ComputedParameter.GameTime = gameTime;

      //Do normal single window rendering if not in headless mode
      if (!m_isHeadless)
      {
        m_swapChain.SetActiveAndClear(m_renderContext, ClearOptions.All, m_clearColor, m_clearDepth, m_clearStencil);

        Render(m_renderContext, gameTime);

        // If not a Gui, end the frame. No-op if not
        AppGui.Render(m_renderContext, GameWindow);

        m_swapChain.Present();
      }
      //If in headless, and rendersystem is not null, then just call the render method for whatever the client wants to do
      else if (m_renderSystem is not null)
      {
        Render(m_renderContext, gameTime);
      }
    }

    private void SuspendRendering(IWindow sender, EventArgs args)
    {
      m_timer.Pause();
      m_isActive = false;
    }

    private void ResumeRendering(IWindow sender, EventArgs args)
    {
      m_timer.Resume();
      m_isActive = true;
    }

    private void HandleHostShutDown(IApplicationHost sender, EventArgs args)
    {
      OnExiting();

      m_content.Unload();
      UnloadContent(m_content);

      Engine.Destroy();
    }

    private void ClientSizeChanged(IWindow sender, EventArgs args)
    {
      if (!m_isRunning)
        return;

      OnViewportResized(m_gameWindow);
    }
  }
}
