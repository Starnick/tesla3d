﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using Tesla.Input;

#nullable enable

namespace Tesla.Application
{
  /// <summary>
  /// Represents text input, a character and the key that was pressed.
  /// </summary>
  public struct TextInputKey
  {
    /// <summary>
    /// Character code for lookup.
    /// </summary>
    public readonly char Character;

    /// <summary>
    /// Corresponding input key that generated the character.
    /// </summary>
    public readonly Keys Key;

    /// <summary>
    /// Constructs a new <see cref="TextInputKey"/> instance.
    /// </summary>
    /// <param name="character">Character code.</param>
    /// <param name="key">Corresponding input key.</param>
    public TextInputKey(char character, Keys key)
    {
      Character = character;
      Key = key;
    }
  }
}
