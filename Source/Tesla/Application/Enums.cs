﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#nullable enable

namespace Tesla.Application
{
  /// <summary>
  /// Defines different modes a <see cref="GameApplication"/> can be started in.
  /// </summary>
  public enum ApplicationMode
  {
    /// <summary>
    /// Default mode. The game window is a top-level window that has a message pump.
    /// </summary>
    Normal = 0,

    /// <summary>
    /// The game window is not a top-level window and can be a child of another form or embedded in an application.
    /// </summary>
    ChildWindow = 1,

    /// <summary>
    /// The game application does not manage any game window, only a render loop.
    /// </summary>
    Headless = 2
  }

  /// <summary>
  /// Enumerates different full screen modes a <see cref="IWindow"/> can be in.
  /// </summary>
  public enum WindowMode
  {
    /// <summary>
    /// Not in full screen and the window has a border (usually meaning it can be moved around and resized).
    /// </summary>
    Normal = 0,
    
    /// <summary>
    /// The window is in full screen exclusive mode, which may be faster as the the app has ownership of the display and GPU, but
    /// it takes longer to switch between applications. The window may also have different display resolutions other than the native resolution.
    /// </summary>
    FullScreen = 1,

    /// <summary>
    /// The window mimicks being in full screen, it still acts as a windowed application without a border but is maximized on the screen. This
    /// may be slower than full screen exclusive, but it's faster to switch between applications. The window's display resolution is tied to the native resolution of the display.
    /// </summary>
    FullScreenBorderless = 2
  }
}
