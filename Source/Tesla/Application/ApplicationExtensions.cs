﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Gui;
using Tesla.Input;

#nullable enable

namespace Tesla.Application
{
  /// <summary>
  /// Extension methods for <see cref="IApplicationHost"/>.
  /// </summary>
  public static class HostExtensions
  {
    /// <summary>
    /// Determines keyboard focus state for the window. If the window is null, then all windows are looked at in the host.
    /// </summary>
    /// <param name="host">Application host.</param>
    /// <param name="window">Window to determine input focus. If null, all windows are looked at.</param>
    /// <returns>Focus state.</returns>
    public static InputFocusState DetermineKeyboardFocusState(this IApplicationHost host, IWindow? window = null)
    {
      if (window is null)
        return (host.Windows.FocusedAnyWindow) ? (AppGui.HasKeyboardFocus ? InputFocusState.Gui : InputFocusState.App) : InputFocusState.None;

      if (window.Focused)
        return GuiWantsKeyboard(window) ? InputFocusState.Gui : InputFocusState.App;

      return InputFocusState.None;
    }

    /// <summary>
    /// Determines mouse focus state for the window. If the window is null, then all windows are looked at in the host.
    /// </summary>
    /// <param name="host">Application host.</param>
    /// <param name="window">Window to determine input focus. If null, all windows are looked at.</param>
    /// <returns>Focus state.</returns>
    public static InputFocusState DetermineMouseFocusState(this IApplicationHost host, IWindow? window = null)
    {
      if (window is null)
        return (host.Windows.FocusedAnyWindow && host.Windows.IsMouseInsideAnyWindow) ? (AppGui.HasMouseFocus ? InputFocusState.Gui : InputFocusState.App) : InputFocusState.None;

      if (window.Focused && window.IsMouseInsideWindow)
        return GuiWantsMouse(window) ? InputFocusState.Gui : InputFocusState.App;

      return InputFocusState.None;
    }

    /// <summary>
    /// Determines mouse focus state given a window handle. If the window handle is <see cref="IntPtr.Zero"/> or the window not found,
    /// then all windows are looked at in the host.
    /// </summary>
    /// <param name="host">Application host.</param>
    /// <param name="windowHandle">Window handle. If <see cref="IntPtr.Zero"/> or window was not found, all windows are looked at.</param>
    /// <returns>Focus state.</returns>
    public static InputFocusState DetermineMouseFocusState(this IApplicationHost host, IntPtr windowHandle)
    {
      return host.DetermineMouseFocusState(host.Windows[windowHandle]);
    }

    private static bool GuiWantsKeyboard(IWindow? window)
    {
      return (AppGui.GetInputDeviceFocus(window) & InputDeviceFocus.Keyboard) == InputDeviceFocus.Keyboard;
    }

    private static bool GuiWantsMouse(IWindow? window)
    {
      return (AppGui.GetInputDeviceFocus(window) & InputDeviceFocus.Mouse) == InputDeviceFocus.Mouse;
    }
  }
}
