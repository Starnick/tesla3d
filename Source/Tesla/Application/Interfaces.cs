﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Threading;
using System.Threading.Tasks;
using Tesla.Graphics;
using Tesla.Gui;
using Tesla.Input;

#nullable enable

namespace Tesla.Application
{
  /// <summary>
  /// Abstraction for running an event loop for simulation / rendering and creating native windows from the OS.
  /// </summary>
  public interface IApplicationHost : IDisposableEngineService
  {
    /// <summary>
    /// Gets the current <see cref="IApplicationHost"/> registered to the <see cref="Engine.Instance"/>.
    /// </summary>
    /// <exception cref="ArgumentNullException">Thrown if <see cref="Engine.Initialize"/> has yet to be called -or- the current instance is null.</exception>
    public static IApplicationHost Current { get { return CoreServices.ApplicationHost.ServiceOrThrow; } }

    /// <summary>
    /// Occurs when the message loop ceases to run.
    /// </summary>
    public event TypedEventHandler<IApplicationHost> ShuttingDown;

    /// <summary>
    /// Gets a string identifying the application platform.
    /// </summary>
    string Platform { get; }

    /// <summary>
    /// Gets the thread the host is running on.
    /// </summary>
    Thread Thread { get; }

    /// <summary>
    /// Gets the main window associated with the host. If this exists, this window will automatically call <see cref="Shutdown"/> when
    /// closed. This will be null if the host is running in "headless" mode (e.g. no windows created) or in another configuration,
    /// such as an editor app where the windows are embedded inside another type of application.
    /// </summary>
    IWindow? MainWindow { get; }

    /// <summary>
    /// Gets the collection of UI windows the host has created or adapted.
    /// </summary>
    WindowCollection Windows { get; }

    /// <summary>
    /// Gets whether the host's message loop is running or not.
    /// </summary>
    bool IsRunning { get; }

    /// <summary>
    /// Starts the application host's message loop on the current thread. On idle (after message processing) the callback will be invoked to run
    /// simulation updates / rendering. Optionally, a main window can be specified that will automatically shut down the host when
    /// it is closed, even if there were other windows created. If no window is specified, the application must call <see cref="Shutdown"/> explicitly.
    /// This is a blocking call until the message loop is shut down.
    /// </summary>
    /// <param name="onIdleCallback">Logic to run when the app is idle.</param>
    /// <param name="mainWindow">Optional main window that will shutdown the host when closed.</param>
    void Run(Action onIdleCallback, IWindow? mainWindow = null);

    /// <summary>
    /// Stops the message loop and closes/disposes of all owned windows, putting the host back into a ready state. 
    /// Usually called on application exit. This does not dispose the host, calling <see cref="Engine.Destroy"/> will dispose of the host service.
    /// </summary>
    void Shutdown();

    /// <summary>
    /// Invokes the action on the thread that the application host runs its message loop on (aka run on UI thread). This
    /// returns immediately with a <see cref="Task"/> that represents the operation. If the current thread is already the message loop thread,
    /// this executes immediately.
    /// </summary>
    /// <param name="action">Action to invoke</param>
    /// <returns>Task representing the asynchronous operation.</returns>
    ValueTask Invoke(Action action);

    /// <summary>
    /// Queues the action to be run before the OnIdle callback on the next update tick.
    /// </summary>
    /// <param name="action">Action to invoke</param>
    /// <returns>Task representing the asynchronous operation.</returns>
    Task Queue(Action action);

    /// <summary>
    /// Invokes the function on the thread that the application host runs its message loop on (aka run on UI thread). This
    /// returns immediately with a <see cref="Task"/> that represents the operation. If the current thread is already the message loop thread,
    /// this executes immediately.
    /// </summary>
    /// <typeparam name="TResult">Type of result to return</typeparam>
    /// <param name="action">Action that returns a result</param>
    /// <returns>Task representing the asynchronous operation.</returns>
    ValueTask<TResult> Invoke<TResult>(Func<TResult> action);

    /// <summary>
    /// Queues the action to be run before the OnIdle callback on the next update tick.
    /// </summary>
    /// <typeparam name="TResult">Type of result to return</typeparam>
    /// <param name="action">Action that returns a result</param>
    /// <returns>Task representing the asynchronous operation.</returns>
    Task<TResult> Queue<TResult>(Func<TResult> action);

    /// <summary>
    /// Creates a new UI window owned by the application host.
    /// </summary>
    /// <param name="presentParams">Presentation options for the window.</param>
    /// <param name="isTopLevelWindow">Notifies whether the window should be a top level container (e.g. a Form).</param>
    /// <returns>The newly created window.</returns>
    IWindow CreateWindow(PresentationParameters presentParams, bool isTopLevelWindow);

    /// <summary>
    /// Creates a UI window adapter to an existing native window.
    /// </summary>
    /// <param name="nativeWindow">Native window to adapt.</param>
    /// <param name="templatePresentationParameters">Optional template presentation parameters. Some properties may be ignored.</param>
    /// <param name="resizeNativeWindow">True if the native window should be resized based on template presentation parameters, false if those
    /// properties should be ignored.</param>
    /// <returns>The UI window adapter</returns>
    /// <remarks>See implementors for details on specific exceptions that can be thrown.</remarks>
    IWindow FromNativeWindow(Object nativeWindow, PresentationParameters? templatePresentationParameters, bool resizeNativeWindow);
  }

  /// <summary>
  /// Abstraction around a native OS window. This may be a top-level window (e.g. a Form in Win32) or a child control embedded in
  /// a larger UI application.
  /// </summary>
  public interface IWindow : IDisposable
  {
    event TypedEventHandler<IWindow> ClientSizeChanged;
    event TypedEventHandler<IWindow> OrientationChanged;
    event TypedEventHandler<IWindow> GotFocus;
    event TypedEventHandler<IWindow> LostFocus;
    event TypedEventHandler<IWindow> ResumeRendering;
    event TypedEventHandler<IWindow> SuspendRendering;
    event TypedEventHandler<IWindow> Paint;
    event TypedEventHandler<IWindow> Closed;
    event TypedEventHandler<IWindow> Disposed;

    event TypedEventHandler<IWindow, MouseState> MouseClick;
    event TypedEventHandler<IWindow, MouseState> MouseDoubleClick;
    event TypedEventHandler<IWindow, MouseState> MouseUp;
    event TypedEventHandler<IWindow, MouseState> MouseDown;
    event TypedEventHandler<IWindow, MouseState> MouseMove;
    event TypedEventHandler<IWindow, MouseState> MouseWheel;

    event TypedEventHandler<IWindow, KeyboardState> KeyDown;
    event TypedEventHandler<IWindow, KeyboardState> KeyPress;
    event TypedEventHandler<IWindow, KeyboardState> KeyUp;

    event TypedEventHandler<IWindow, TextInputKey> TextInput;

    event TypedEventHandler<IWindow> MouseEnter;
    event TypedEventHandler<IWindow> MouseLeave;
    event TypedEventHandler<IWindow> MouseHover;

    /// <summary>
    /// Enables or disables user resizing, if the window is in full screen, this has no effect.
    /// </summary>
    bool EnableUserResizing { get; set; }

    /// <summary>
    /// Enables or disables input events.
    /// </summary>
    bool EnableInputEvents { get; set; }

    /// <summary>
    /// Enables or disables system UI paint events. If the application is meant to not render continuously this should be turned on in order to draw.
    /// </summary>
    bool EnablePaintEvents { get; set; }

    /// <summary>
    /// Enables or disables redraw on resizing the window. If false, the window only redraws until the resizing stops.
    /// </summary>
    bool EnableResizeRedraw { get; set; }

    /// <summary>
    /// Gets if the window is minimized.
    /// </summary>
    bool IsMinimized { get; }

    /// <summary>
    /// Gets or sets if the mouse cursor should be visible within the window.
    /// </summary>
    bool IsMouseVisible { get; set; }

    /// <summary>
    /// Gets if the mouse is currently inside the window.
    /// </summary>
    bool IsMouseInsideWindow { get; }
    
    /// <summary>
    /// Gets if the window is in full screen mode (either exclusive or borderless).
    /// </summary>
    bool IsFullScreen { get; }

    /// <summary>
    /// Gets or sets the window mode - e.g. windowed vs full screen.
    /// </summary>
    WindowMode WindowMode { get; set; }

    /// <summary>
    /// Gets or sets if the window is visible or not.
    /// </summary>
    bool IsVisible { get; set; }

    /// <summary>
    /// Gets if the window is a top-level window (e.g. a Form) or a child control within a larger application.
    /// </summary>
    bool IsTopLevelWindow { get; }

    /// <summary>
    /// Gets if the window has been disposed.
    /// </summary>
    bool IsDisposed { get; }

    /// <summary>
    /// Gets if the window currently has focus.
    /// </summary>
    bool Focused { get; }

    /// <summary>
    /// Gets or sets the screen location (upper-left corner) of the window.
    /// </summary>
    Int2 Location { get; set; }

    /// <summary>
    /// Gets the client size (inside the window border) of the control.
    /// </summary>
    Rectangle ClientBounds { get; }

    /// <summary>
    /// Gets the screen bounds of the control.
    /// </summary>
    Rectangle ScreenBounds { get; }

    /// <summary>
    /// Gets the underlying native window handle.
    /// </summary>
    IntPtr Handle { get; }

    /// <summary>
    /// Gets the swapchain that manages the backbuffer for the window.
    /// </summary>
    SwapChain SwapChain { get; }

    /// <summary>
    /// Gets or sets the window title.
    /// </summary>
    string Title { get; set; }

    /// <summary>
    /// Gets the underlying native window.
    /// </summary>
    object NativeWindow { get; }

    /// <summary>
    /// Gets or sets an object tag to the window. This can be any user data associated with the window.
    /// </summary>
    object? Tag { get; set; }

    /// <summary>
    /// Gets the application host that created and manages the window.
    /// </summary>
    IApplicationHost Host { get; }

    /// <summary>
    /// Focuses the window.
    /// </summary>
    void Focus();

    /// <summary>
    /// Centers the window to the middle of the screen. If this is not a top-level window, an attempt is made to center the containing window.
    /// </summary>
    void CenterToScreen();

    /// <summary>
    /// Closes the window and disposes of it.
    /// </summary>
    void Close();

    /// <summary>
    /// Notifies the window to repaint. Used if the window does not render continuously, this will schedule a paint event.
    /// </summary>
    void Repaint();

    /// <summary>
    /// Resets the swapchain to the specified parameters.
    /// </summary>
    /// <param name="pp">New presentation parameters.</param>
    void Reset(PresentationParameters pp);

    /// <summary>
    /// Resizes the window, and the swapchain backbuffer to the specified size.
    /// </summary>
    /// <param name="width">Width of the backbuffer.</param>
    /// <param name="height">Height of the backbuffer.</param>
    void Resize(int width, int height);
  }
}
