﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#nullable enable

namespace Tesla.Text
{
  /// <summary>
  /// Enumerates different type of token matching.
  /// </summary>
  public enum TokenMatchType
  {
    /// <summary>
    /// No match.
    /// </summary>
    None = 0,

    /// <summary>
    /// Partial match.
    /// </summary>
    Partial = 1,

    /// <summary>
    /// Full match.
    /// </summary>
    Full = 2
  }

  /// <summary>
  /// Represents the result of a token match operation.
  /// </summary>
  public readonly struct TokenMatch
  {
    /// <summary>
    /// Gets a result that is no match.
    /// </summary>
    public readonly static TokenMatch NoMatch = new TokenMatch(TokenMatchType.None, 0, 0);

    /// <summary>
    /// Gets the type of match.
    /// </summary>
    public readonly TokenMatchType MatchType;

    /// <summary>
    /// Gets the start index within the string at which the match starts (if there is one).
    /// </summary>
    public readonly int Index;

    /// <summary>
    /// Gets the length of the match (if there is one).
    /// </summary>
    public readonly int Length;

    /// <summary>
    /// Gets if the result has any match.
    /// </summary>
    public readonly bool HasMatch { get { return MatchType != TokenMatchType.None; } }

    /// <summary>
    /// Gets if the result matches the string fully.
    /// </summary>
    public readonly bool IsFullMatch { get { return MatchType == TokenMatchType.Full; } }

    /// <summary>
    /// Creates a new instance of <see cref="TokenMatch"/> struct.
    /// </summary>
    /// <param name="type"></param>
    /// <param name="index"></param>
    /// <param name="length"></param>
    public TokenMatch(TokenMatchType type, int index, int length)
    {
      MatchType = type;
      Index = index;
      Length = length;
    }

    /// <inheritdoc />
    public readonly override string ToString()
    {
      return $"{MatchType}, {Index}, {Length}";
    }
  }
}
