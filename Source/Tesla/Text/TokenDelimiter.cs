﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Text.RegularExpressions;

#nullable enable

namespace Tesla.Text
{
  /// <summary>
  /// Represents a character that signifies a separator between tokens in a character stream.
  /// </summary>
  public readonly struct TokenDelimiter
  {
    /// <summary>
    /// The characer that serves as a delimiter.
    /// </summary>
    public readonly char Delimiter;

    /// <summary>
    /// Whether or not the character should be skipped (e.g. not appear in tokens). If false, the character is treated as
    /// a token itself (e.g. a curly brace in code text).
    /// </summary>
    public readonly bool IsSkip;

    /// <summary>
    /// Constructs a new instance of <see cref="TokenDelimiter"/> class.
    /// </summary>
    /// <param name="delimiter">Character that separates tokens in a character stream.</param>
    /// <param name="isSkip">True if the character should be skipped and not appear in tokens, false if the character is a token itself.</param>
    public TokenDelimiter(char delimiter, bool isSkip = false)
    {
      Delimiter = delimiter;
      IsSkip = isSkip;
    }

    /// <inheritdoc />
    public readonly override string ToString()
    {
      return $"{Delimiter}, IsSkip={IsSkip}";
    }
  }
}
