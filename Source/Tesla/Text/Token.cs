﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;

#nullable enable

namespace Tesla.Text
{
  /// <summary>
  /// Represents a lightweight token from a character stream. It doesn't allocate a string to hold the
  /// text data, instead relies on an external <see cref="ReadOnlyMemory{T}"/> to construct a span of characters.
  /// </summary>
  public readonly struct Token
  {
    /// <summary>
    /// Represents an invalid token with an empty text value.
    /// </summary>
    public readonly static Token InvalidToken = new Token(ReadOnlyMemory<char>.Empty, 0, 0);

    /// <summary>
    /// The line number at which the token appears in the string data.
    /// </summary>
    public readonly int LineNumber;

    /// <summary>
    /// The column number at which the first character in the token appears in the string data.
    /// </summary>
    public readonly int ColumnNumber;

    /// <summary>
    /// User-defined token type ID. By default -1.
    /// </summary>
    public readonly int TokenType;

    private readonly ReadOnlyMemory<char> m_data;
    private readonly int m_pos;
    private readonly int m_count;

    /// <summary>
    /// Gets the token's text value.
    /// </summary>
    public readonly ReadOnlySpan<char> Text
    {
      get
      {
        if (m_count == 0)
          return ReadOnlySpan<char>.Empty;

        return m_data.Span.Slice(m_pos, m_count);
      }
    }

    /// <summary>
    /// Gets if the token is valid or not. Invalid tokens are empty text spans.
    /// </summary>
    public readonly bool IsValid { get { return m_count > 0; } }

    /// <summary>
    /// Constructs a new instance of <see cref="Token"/> struct.
    /// </summary>
    /// <param name="data">String data.</param>
    /// <param name="pos">Starting index within the data.</param>
    /// <param name="count">Number of characters representing the token.</param>
    /// <param name="lineNumber">Optional line number of the token.</param>
    /// <param name="columnNumber">Optional column number of the token (character at the start).</param>
    /// <param name="tokenType">Optional user-defined token type ID.</param>
    public Token(ReadOnlyMemory<char> data, int pos, int count, int lineNumber = 0, int columnNumber = 0, int tokenType = -1)
    {
      LineNumber = lineNumber;
      ColumnNumber = columnNumber;
      TokenType = tokenType;

      m_data = data;
      m_pos = pos;
      m_count = count;
    }

    /// <summary>
    /// Returns a copy of the token with the specified token type.
    /// </summary>
    /// <param name="tokenType">Token type.</param>
    /// <returns>The token with a new type classification.</returns>
    public readonly Token AsTokenType(int tokenType)
    {
      return new Token(m_data, m_pos, m_count, LineNumber, ColumnNumber, tokenType);
    }

    /// <summary>
    /// Classifies the token and returns a copy with the token type, or the specified default type.
    /// </summary>
    /// <param name="pattern">Pattern to match against the token string.</param>
    /// <param name="exactMatch">Only allow exact matching, by default true. If false, the longest partial match is the classification.</param>
    /// <param name="defaultTokenType">Optionally specify a default token type, if the token is not any of these. By default -1.</param>
    /// <returns>The classified token.</returns>
    public readonly Token Classify(TokenPattern pattern, bool exactMatch = true, int defaultTokenType = -1)
    {
      if (!IsValid)
        return this;

      int tokenType = defaultTokenType;
      if (pattern is not null)
      {
        TokenMatch match = pattern.Match(Text);
        if (match.IsFullMatch || (!exactMatch && match.HasMatch && match.Index == 0))
          tokenType = pattern.TokenType;
      }

      return AsTokenType(tokenType);
    }

    /// <summary>
    /// Classifies the token and returns a copy with the token type, or the specified default type.
    /// </summary>
    /// <param name="patterns">Patterns to match against the token string.</param>
    /// <param name="exactMatch">Only allow exact matching, by default true. If false, the longest partial match is the classification.</param>
    /// <param name="defaultTokenType">Optionally specify a default token type, if the token is not any of these. By default -1.</param>
    /// <returns>The classified token.</returns>
    public readonly Token Classify(IReadOnlyList<TokenPattern> patterns, bool exactMatch = true, int defaultTokenType = -1)
    {
      if (!IsValid)
        return this;

      int tokenType = defaultTokenType;
      TokenMatch? lastMatch = null;

      if (patterns is not null)
      {
        for (int i = 0; i < patterns.Count; i++)
        {
          TokenPattern pattern = patterns[i];
          if (pattern is null)
            continue;

          TokenMatch match = pattern.Match(Text);

          // Keep going if no match or if not matching from the start
          if (!match.HasMatch || match.Index != 0)
            continue;

          // Full match is an early out
          if (match.IsFullMatch)
          {
            tokenType = pattern.TokenType;
            break;
          }

          // If allow partial match, take the longest one
          if (!exactMatch && (!lastMatch.HasValue || match.Length > lastMatch.Value.Length))
          {
            tokenType = pattern.TokenType;
            lastMatch = match;
          }
        }
      }

      return AsTokenType(tokenType);
    }

    /// <summary>
    /// Checks if the token text is equal to the specified text.
    /// </summary>
    /// <param name="str">Text</param>
    /// <param name="compareType">Optional comparison type, by default <see cref="StringComparison.InvariantCultureIgnoreCase"/>.</param>
    /// <returns>True if the token text is the text value, false if otherwise.</returns>
    public readonly bool Is(ReadOnlySpan<char> str, StringComparison compareType = StringComparison.InvariantCultureIgnoreCase)
    {
      return Text.Equals(str, compareType);
    }

    /// <summary>
    /// Checks if the token text begins with the specified prefix.
    /// </summary>
    /// <param name="str">Text prefix</param>
    /// <param name="compareType">Optional comparison type, by default <see cref="StringComparison.InvariantCultureIgnoreCase"/>.</param>
    /// <returns>True if the token text begins with the prefix, false if otherwise.</returns>
    public readonly bool StartsWith(ReadOnlySpan<char> str, StringComparison compareType = StringComparison.InvariantCultureIgnoreCase)
    {
      return Text.StartsWith(str, compareType);
    }

    /// <inheritdoc />
    public readonly override string ToString()
    {
      return $"\"{Text}\" Line: {LineNumber}, Column: {ColumnNumber}, Type: {TokenType}";
    }
  }
}
