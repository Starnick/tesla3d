﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Text.RegularExpressions;

#nullable enable

namespace Tesla.Text
{
  /// <summary>
  /// Represents a pattern to classify a token string.
  /// </summary>
  public abstract class TokenPattern
  {
    /// <summary>
    /// Gets a token type that is associated with the pattern.
    /// </summary>
    public readonly int TokenType;

    /// <summary>
    /// Constructs a new instance of <see cref="TokenPattern"/> class.
    /// </summary>
    /// <param name="tokenType">Token type associated with the pattern.</param>
    public TokenPattern(int tokenType)
    {
      TokenType = tokenType;
    }

    /// <summary>
    /// Performs a match on the token string with a pattern.
    /// </summary>
    /// <param name="token">Token string.</param>
    /// <returns>Match result, may be a partial match.</returns>
    public abstract TokenMatch Match(ReadOnlySpan<char> token);
  }

  /// <summary>
  /// Represents a simple string pattern, which uses string equality to match.
  /// </summary>
  public sealed class SimpleTokenPattern : TokenPattern
  {
    /// <summary>
    /// Gets the string pattern.
    /// </summary>
    public readonly ReadOnlyMemory<char> Text;

    /// <summary>
    /// Gets the string comparison type.
    /// </summary>
    public readonly StringComparison ComparisonType;

    /// <summary>
    /// Constructs a new instance of <see cref="SimpleTokenPattern"/> class.
    /// </summary>
    /// <param name="tokenType">Token type associated with the pattern.</param>
    /// <param name="text">String pattern.</param>
    /// <param name="compareType">Optional comparison type.</param>
    public SimpleTokenPattern(int tokenType, ReadOnlyMemory<char> text, StringComparison compareType = StringComparison.InvariantCultureIgnoreCase)
      : base(tokenType)
    {
      Text = text;
      ComparisonType = compareType;
    }


    /// <summary>
    /// Constructs a new instance of <see cref="SimpleTokenPattern"/> class.
    /// </summary>
    /// <param name="tokenType">Token type associated with the pattern.</param>
    /// <param name="text">String pattern.</param>
    /// <param name="compareType">Optional comparison type.</param>
    public SimpleTokenPattern(int tokenType, string text, StringComparison compareType = StringComparison.InvariantCultureIgnoreCase) 
      : this(tokenType, text?.AsMemory() ?? ReadOnlyMemory<char>.Empty, compareType) { }

    /// <summary>
    /// Performs a match on the token string with a pattern.
    /// </summary>
    /// <param name="token">Token string.</param>
    /// <returns>Returns either a full match or no match result.</returns>
    public override TokenMatch Match(ReadOnlySpan<char> token)
    {
      if (token.Equals(Text.Span, ComparisonType))
        return new TokenMatch(TokenMatchType.Full, 0, token.Length);

      return TokenMatch.NoMatch;
    }
  }

  /// <summary>
  /// Represents a regular expression pattern to classify a token string.
  /// </summary>
  public class RegexTokenPattern : TokenPattern
  {
    /// <summary>
    /// Gets the regular expression pattern.
    /// </summary>
    public readonly Regex Regex;

    /// <summary>
    /// Constructs a new instance of <see cref="RegexTokenPattern"/> class.
    /// </summary>
    /// <param name="tokenType">Token type associated with the pattern.</param>
    /// <param name="regex">Regular expression.</param>
    public RegexTokenPattern(int tokenType, Regex regex)
      : base(tokenType)
    {
      Regex = regex;
    }

    /// <summary>
    /// Constructs a new instance of <see cref="RegexTokenPattern"/> class.
    /// </summary>
    /// <param name="tokenType">Token type associated with the pattern.</param>
    /// <param name="pattern">String pattern.</param>
    /// <param name="options">Regular expression options.</param>
    public RegexTokenPattern(int tokenType, string pattern, RegexOptions options = RegexOptions.Compiled)
      : base(tokenType)
    {
      Regex = new Regex(pattern, options);
    }

    /// <inheritdoc />
    public override TokenMatch Match(ReadOnlySpan<char> token)
    {
      foreach (ValueMatch m in Regex.EnumerateMatches(token))
        return new TokenMatch(m.Length == token.Length ? TokenMatchType.Full : TokenMatchType.Partial, m.Index, m.Length);

      return TokenMatch.NoMatch;
    }
  }
}
