﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;

#nullable enable

namespace Tesla.Text
{
  /// <summary>
  /// Lightweight string tokenizer that can be configured to split string input into a series of tokens. By default splits for white space
  /// and carriage returns/new lines. Tokens are views into the string data to avoid allocation and can be classified by using <see cref="TokenPattern"/>.
  /// </summary>
  public class StringTokenizer
  {
    private const char EOF = (char) 0;

    private Dictionary<char, TokenDelimiter> m_delimiters;
    private ReadOnlyMemory<char> m_data;
    private Token m_lastReadToken;
    private bool m_skipComments;
    private bool m_skipBlockComments;
    private int m_pos;
    private int m_line;
    private int m_column;

    /// <summary>
    /// Gets the delimiters the tokenizer is configure with.
    /// </summary>
    public IEnumerable<TokenDelimiter> Delimiters { get { return m_delimiters.Values; } }

    /// <summary>
    /// Gets the last read token, if it exists. If it does not exist, the token returned will be invalid.
    /// </summary>
    public Token Token { get { return m_lastReadToken; } }

    /// <summary>
    /// Gets if the tokenizer is at the end of the data buffer.
    /// </summary>
    public bool IsEOF { get { return m_pos >= m_data.Length; } }

    /// <summary>
    /// Gets or sets if double slash comments should be skipped.
    /// </summary>
    public bool SkipComments { get { return m_skipComments; } set { m_skipComments = value; } }

    /// <summary>
    /// Gets or sets if block comments should be skipped.
    /// </summary>
    public bool SkipBlockComments { get { return m_skipBlockComments; } set { m_skipBlockComments = value; } }

    /// <summary>
    /// Constructs a new instance of <see cref="StringTokenizer"/>.
    /// </summary>
    public StringTokenizer()
    {
      m_delimiters = new Dictionary<char, TokenDelimiter>();
      AddSkipDelimiters(new char[] { ' ', '\t', '\r', '\n' });

      m_skipBlockComments = true;
      m_skipComments = true;

      Initialize(String.Empty);
    }

    /// <summary>
    /// Adds a delimiter to the scanner. These are special characters that when encountered will signify the end of a token,
    /// some can be skipped (e.g. white space) others can be tokens (e.g. curly braces when parsing code text).
    /// </summary>
    /// <param name="delimiter">Delimiter to add.</param>
    public void AddDelimiter(TokenDelimiter delimiter)
    {
      m_delimiters[delimiter.Delimiter] = delimiter;
    }

    /// <summary>
    /// Adds one or more delimiters whose characters will be skipped and not appear in tokens.
    /// </summary>
    /// <param name="chars">One or more characters representing the delimiters.</param>
    public void AddSkipDelimiters(ReadOnlySpan<char> chars)
    {
      foreach (char c in chars)
        m_delimiters[c] = new TokenDelimiter(c, true);
    }

    /// <summary>
    /// Adds one or more delimiters whose characters will themselves be tokens (e.g. will not "skip").
    /// </summary>
    /// <param name="chars">One or more characters representing the delimiters.</param>
    public void AddNonSkipDelimiters(ReadOnlySpan<char> chars)
    {
      foreach (char c in chars)
        m_delimiters[c] = new TokenDelimiter(c, false);
    }

    /// <summary>
    /// Initializes the tokenizer to read the specified string.
    /// </summary>
    /// <param name="str">String value.</param>
    public void Initialize(string? str)
    {
      if (String.IsNullOrEmpty(str))
      {
        Initialize(ReadOnlyMemory<char>.Empty);
        return;
      }

      Initialize(str.AsMemory());
    }

    /// <summary>
    /// Initializes the tokenizer to read the specified string.
    /// </summary>
    /// <param name="str"></param>
    public void Initialize(ReadOnlyMemory<char> str)
    {
      m_data = str;
      Restart();
    }

    /// <summary>
    /// Restarts the tokenizer back to the beginning of the string data.
    /// </summary>
    public void Restart()
    {
      m_pos = 0;
      m_column = 0;
      m_line = 1;
      m_lastReadToken = Token.InvalidToken;
    }

    /// <summary>
    /// Moves to the next token and classifies it. The token may or may not be valid and may not match any of the patterns given.
    /// The last read token is always accessible as a property on the tokenizer.
    /// </summary>
    /// <param name="pattern">Pattern to classify the token, based on an exact match.</param>
    /// <param name="defaultTokenType">Optionally define a default token type if the token does not match any of the patterns.</param>
    /// <param name="preserveWhitespace">Optionally, do NOT skip any white space, e.g. ensuring a filepath is completely captured as a token.</param>
    /// <returns>The read token, which may be invalid if the tokenizer is at the end of the data.</returns>
    public Token ScanNextToken(TokenPattern pattern, int defaultTokenType = -1, bool preserveWhitespace = false)
    {
      if (MoveToNextToken(preserveWhitespace))
      {
        m_lastReadToken = m_lastReadToken.Classify(pattern, true, defaultTokenType);
        return m_lastReadToken;
      }

      return Token.InvalidToken;
    }

    /// <summary>
    /// Moves to the next token and classifies it. The token may or may not be valid and may not match any of the patterns given.
    /// The last read token is always accessible as a property on the tokenizer.
    /// </summary>
    /// <param name="patterns">One or more patterns to classify the token, based on an exact match.</param>
    /// <param name="defaultTokenType">Optionally define a default token type if the token does not match any of the patterns.</param>
    /// <param name="preserveWhitespace">Optionally, do NOT skip any white space, e.g. ensuring a filepath is completely captured as a token.</param>
    /// <returns>The read token, which may be invalid if the tokenizer is at the end of the data.</returns>
    public Token ScanNextToken(IReadOnlyList<TokenPattern> patterns, int defaultTokenType = -1, bool preserveWhitespace = false)
    {
      if (MoveToNextToken(preserveWhitespace))
      {
        m_lastReadToken = m_lastReadToken.Classify(patterns, true, defaultTokenType);
        return m_lastReadToken;
      }

      return Token.InvalidToken;
    }

    /// <summary>
    /// Moves to the next token and returns it. The token may or may not be valid. The last read token is always accessible as a property on the tokenizer.
    /// </summary>
    /// <param name="preserveWhitespace">Optionally, do NOT skip any white space, e.g. ensuring a filepath is completely captured as a token.</param>
    /// <returns>The read token, which may be invalid if the tokenizer is at the end of the data.</returns>
    public Token ReadNextToken(bool preserveWhitespace = false)
    {
      if (MoveToNextToken(preserveWhitespace))
        return m_lastReadToken;

      return Token.InvalidToken;
    }

    /// <summary>
    /// Moves to the next token and returns if it is valid or not. The last read token is always accessible as a property on the tokenizer.
    /// </summary>
    /// <param name="preserveWhitespace">Optionally, do NOT skip any white space, e.g. ensuring a filepath is completely captured as a token.</param>
    /// <returns>True if a token was read, false if it is not valid.</returns>
    public bool MoveToNextToken(bool preserveWhitespace = false)
    {
      // If previous move ended on EOF, early out. Nothing left to do
      if (IsEOF)
      {
        m_lastReadToken = Token.InvalidToken;
        return false;
      }

      // Try to consume a new token
      bool tokenStarted = false;
      int startPos = m_pos;
      int startColumn = m_column;
      int startLine = m_line;

      while (true)
      {
        char c = PeekChar();

        // If EOF, pending token is finished, if started
        if (c == EOF)
          break;

        // Handle incrementing lines
        HandleLineIncrement(c);

        // Handle comment skipping
        if (IsStartOfComment())
        {
          if (tokenStarted)
            break;

          SkipComment();
          continue;
        }

        // If delimiter, pending token is finished. If no pending token, we skip ahead or consume it as a token.
        if (m_delimiters.TryGetValue(c, out TokenDelimiter tokDelim))
        {
          if (!tokenStarted)
          {
            if (tokDelim.IsSkip)
            {
              SkipChar(1);
              continue;
            }
            else
            {
              // The token is the single character so can return early
              ConsumeChar(false, ref startPos, ref startLine, ref startColumn);
              return SetToken(startPos, m_pos, startLine, startColumn);
            }
          }
          else
          {
            // Special case for path handling, allow whitespace to be consumed. Otherwise, we halt on the delimiter.
            bool wantChar = (tokDelim.Delimiter == ' ') ? preserveWhitespace : false;
            if (!wantChar)
              break;
          }
        }

        // Consume the character into the pending token
        ConsumeChar(tokenStarted, ref startPos, ref startLine, ref startColumn);
        tokenStarted = true;
      }

      if (!tokenStarted)
        return false;

      return SetToken(startPos, m_pos, startLine, startColumn);
    }

    private bool SetToken(int startPos, int endPos, int startLine, int startColumn, int tokenType = -1)
    {
      int count = endPos - startPos;
      if (count <= 0)
      {
        m_lastReadToken = Token.InvalidToken;
        return false;
      }

      m_lastReadToken = new Token(m_data, startPos, count, startLine, startColumn, tokenType);
      return true;
    }

    private char PeekChar(int lookAheadCount = 0)
    {
      if (m_pos + lookAheadCount >= m_data.Length)
        return EOF;

      return m_data.Span[m_pos + lookAheadCount];
    }

    private void ConsumeChar(bool tokenStarted, ref int startPos, ref int startLine, ref int startColumn)
    {
      if (!tokenStarted)
      {
        startPos = m_pos;
        startLine = m_line;
        startColumn = m_column;
      }

      m_pos++;
      m_column++;
    }

    private void SkipChar(int count = 1)
    {
      m_pos += count;
      m_column += count;
    }

    private void HandleLineIncrement(char c)
    {
      if (c != '\n')
        return;

      m_line++;
      m_column = 0;
    }

    private bool IsStartOfComment()
    {
      if (PeekChar(0) != '/')
        return false;

      char nextChar = PeekChar(1);

      if (m_skipComments && nextChar == '/')
        return true;

      if (m_skipBlockComments && nextChar == '*')
        return true;

      return false;
    }

    private void SkipComment()
    {
      if (PeekChar(0) != '/')
        return;

      char nextChar = PeekChar(1);

      // Skip until a new line
      if (m_skipComments && nextChar == '/')
      {
        SkipChar(2);
        while (true)
        {
          char c = PeekChar(0);
          if (c == EOF || c == '\r' || c == '\n')
            break;

          SkipChar(1);
        }

        return;
      }

      // Skip until find the end of the block comment
      if (m_skipBlockComments && nextChar == '*')
      {
        SkipChar(2);
        while (true)
        {
          char c = PeekChar(0);
          if (c == EOF)
            break;

          if (c == '*' && PeekChar(1) == '/')
          {
            SkipChar(2);
            break;
          }

          SkipChar(1);
          HandleLineIncrement(c);
        }
      }
    }
  }
}
