﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;

#nullable enable

namespace Tesla.Graphics.Implementation
{
  /// <summary>
  /// Common base class for a <see cref="BlendState"/> implementation.
  /// </summary>
  public abstract class BlendStateImpl : GraphicsResourceImpl, IBlendStateImpl
  {
    private bool m_isBound;
    private bool m_alphaToCoverageEnable;
    private bool m_independentBlendEnable;
    private Color m_blendFactor;
    private int m_multiSampleMask;

    private RenderTargetBlendDescription[] m_renderTargetBlends;

    /// <summary>
    /// Gets if the render state has been bound to the pipeline, once bound the state becomes immutable.
    /// </summary>
    public bool IsBound
    {
      get
      {
        return m_isBound;
      }
    }

    /// <summary>
    /// Gets the number of render targets that allow for independent blending. This can vary by implementation, at least one is always guaranteed.
    /// </summary>
    public virtual int RenderTargetBlendCount { get { return RenderSystem.Adapter.MaximumMultiRenderTargets; } }

    /// <summary>
    /// Checks if alpha-to-coverage is supported. This can vary by implementation.
    /// </summary>
    public abstract bool IsAlphaToCoverageSupported { get; }

    /// <summary>
    /// Checks if independent blending of multiple render targets (MRT) is supported. This can vary by implementation. If not supported, then the blending options
    /// specified for the first render target index are used for all other bound render targets, if those targets blend are enabled.
    /// </summary>
    public abstract bool IsIndependentBlendSupported { get; }

    /// <summary>
    /// Gets or sets whether alpha-to-coverage should be used as a multisampling technique when writing a pixel to a render target. Support for this may vary by implementation. By
    /// default, this value is false.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public bool AlphaToCoverageEnable
    {
      get
      {
        return m_alphaToCoverageEnable;
      }
      set
      {
        ThrowIfBound();
        m_alphaToCoverageEnable = value;
      }
    }

    /// <summary>
    /// Gets or sets whether independent blending is enabled for multiple render targets (MRT). If this is false, the blending options specified for the first render target index
    /// is used for all render targets currently bound. Support for this may vary by implementation. By default, this value is false.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public bool IndependentBlendEnable
    {
      get
      {
        return m_independentBlendEnable;
      }
      set
      {
        ThrowIfBound();
        m_independentBlendEnable = value;
      }
    }

    /// <summary>
    /// Gets or sets the blend factor color. By default, this value is <see cref="Color.White" />.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public Color BlendFactor
    {
      get
      {
        return m_blendFactor;
      }
      set
      {
        ThrowIfBound();
        m_blendFactor = value;
      }
    }

    /// <summary>
    /// Gets or sets the multisample mask. By default, this value is 0xffffffff.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public int MultiSampleMask
    {
      get
      {
        return m_multiSampleMask;
      }
      set
      {
        ThrowIfBound();
        m_multiSampleMask = value;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="BlendStateImpl"/> class.
    /// </summary>
    /// <param name="renderSystem">The render system that manages this graphics implementation</param>
    /// <param name="resourceID">ID of the resource, supplied by the render system</param>
    protected BlendStateImpl(IRenderSystem renderSystem, int resourceID)
        : base(renderSystem, resourceID)
    {
      m_renderTargetBlends = new RenderTargetBlendDescription[RenderTargetBlendCount];
      m_isBound = false;
    }

    /// <summary>
    /// Gets the complete blend description for a render target bound at the specified index.
    /// </summary>
    /// <param name="renderTargetIndex">Zero-based index of the render target that is bound to the pipeline.</param>
    /// <param name="blendDesc">The blend description that holds the blending options for the render target.</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the render target index is out of range.</exception>
    public void GetRenderTargetBlendDescription(int renderTargetIndex, out RenderTargetBlendDescription blendDesc)
    {
      ThrowIfIndexOutOfRange(renderTargetIndex);
      blendDesc = m_renderTargetBlends[renderTargetIndex];
    }

    /// <summary>
    /// Sets the complete blend description for a render target bound at the specified index.
    /// </summary>
    /// <param name="renderTargetIndex">Zero-based index of the render target that is bound to the pipeline.</param>
    /// <param name="blendDesc">The blend description that holds the blending options for the render target.</param>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the render target index is out of range.</exception>
    public void SetRenderTargetBlendDescription(int renderTargetIndex, ref RenderTargetBlendDescription blendDesc)
    {
      ThrowIfBound();
      ThrowIfIndexOutOfRange(renderTargetIndex);
      m_renderTargetBlends[renderTargetIndex] = blendDesc;
    }

    /// <summary>
    /// Binds the implementation, creating the underlying state. Once bound the state is read-only. If unbound, this will happen
    /// automatically when the state is first used during rendering. It is best practice to do this ahead of time.
    /// </summary>
    public void BindBlendState()
    {
      if (!m_isBound)
      {
        CreateNativeState();
        m_isBound = true;
      }
    }

    /// <summary>
    /// Called when the state is bound, signaling the implementation to create and bind the native state.
    /// </summary>
    protected abstract void CreateNativeState();

    private void ThrowIfBound()
    {
      if (m_isBound)
        throw new InvalidOperationException(StringLocalizer.Instance.GetLocalizedString("RenderStateBound"));
    }

    private void ThrowIfIndexOutOfRange(int renderTargetIndex)
    {
      if (renderTargetIndex < 0 || renderTargetIndex >= m_renderTargetBlends.Length)
        throw new ArgumentOutOfRangeException(nameof(renderTargetIndex), StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));
    }
  }
}
