﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;

#nullable enable

namespace Tesla.Graphics.Implementation
{
  /// <summary>
  /// Base class for all platform-specific graphics resource implementations that are created by a render system and bound to a graphics resource.
  /// </summary>
  public abstract class GraphicsResourceImpl : IGraphicsResourceImpl
  {
    private string m_debugName;
    private bool m_isDisposed;
    private int m_resourceID;
    private IRenderSystem m_renderSystem;
    private GraphicsResource? m_parent;

    /// <summary>
    /// Gets or sets the name of the object.
    /// </summary>
    public string DebugName
    {
      get
      {
        return m_debugName;
      }
      set
      {
        m_debugName = value;
        OnDebugNameChange(value);
      }
    }

    /// <summary>
    /// Gets if this resource has been disposed or not.
    /// </summary>
    public bool IsDisposed
    {
      get
      {
        return m_isDisposed;
      }
    }

    /// <summary>
    /// Gets the ID of this resource.
    /// </summary>
    public int ResourceID
    {
      get
      {
        return m_resourceID;
      }
    }

    /// <summary>
    /// Gets the render system that created and manages this resource.
    /// </summary>
    public IRenderSystem RenderSystem
    {
      get
      {
        return m_renderSystem;
      }
    }

    /// <summary>
    /// Gets the graphics resource parent that this implementation is bound to.
    /// </summary>
    public GraphicsResource Parent
    {
      get
      {
        return m_parent!;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="GraphicsResourceImpl"/> class.
    /// </summary>
    /// <param name="renderSystem">The render system that manages this graphics implementation</param>
    /// <param name="resourceID">ID of the resource, supplied by the render system</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the render system is null.</exception>
    protected GraphicsResourceImpl(IRenderSystem renderSystem, int resourceID)
    {
      GraphicsHelper.ThrowIfRenderSystemNull(renderSystem);

      m_isDisposed = false;
      m_resourceID = resourceID;
      m_renderSystem = renderSystem;
      m_debugName = String.Empty;
    }

    /// <summary>
    /// Called by the parent graphics resource when the implementation is first created. This only should be called once, during the creation of
    /// a graphics resource.
    /// </summary>
    /// <param name="resource">Graphics resource that is to be bound to this implementation</param>
    /// <exception cref="InvalidOperationException">Thrown if the implementation has already been bound to a resource.</exception>
    /// <exception cref="ArgumentNullException">Thrown if the resource is null.</exception>
    public void BindImplementation(GraphicsResource resource)
    {
      if (m_parent is not null)
        throw new InvalidOperationException(StringLocalizer.Instance.GetLocalizedString("ImplementationAlreadyBound"));

      if (resource is null)
        throw new ArgumentNullException(nameof(resource));

      m_parent = resource;
    }

    /// <summary>
    /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
    /// </summary>
    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    /// <summary>
    /// Releases unmanaged and - optionally - managed resources.
    /// </summary>
    /// <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
    protected virtual void Dispose(bool disposing)
    {
      m_isDisposed = true;
    }

    /// <summary>
    /// Called when the name of the graphics resource is changed, useful if the implementation wants to set the name to
    /// be used as a debug name.
    /// </summary>
    /// <param name="name">New name of the resource</param>
    protected virtual void OnDebugNameChange(string name) { }

    /// <summary>
    /// Checks if the implementation was disposed and if so, throws an ObjectDisposedException.
    /// </summary>
    protected void CheckDisposed()
    {
      ObjectDisposedException.ThrowIf(m_isDisposed, this);
    }

    /// <summary>
    /// Sets a default debug name.
    /// </summary>
    /// <param name="resourceType">Resource type name, if null defaults to "Resource".</param>
    protected void SetDefaultDebugName(string? resourceType)
    {
      DebugName = String.IsNullOrEmpty(resourceType) ? $"Resource - {ResourceID}" : $"{resourceType} - {ResourceID}";
    }
  }
}
