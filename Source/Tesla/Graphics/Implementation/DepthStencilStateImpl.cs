﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;

#nullable enable

namespace Tesla.Graphics.Implementation
{
  /// <summary>
  /// Common base class for a <see cref="DepthStencilState"/> implementation.
  /// </summary>
  public abstract class DepthStencilStateImpl : GraphicsResourceImpl, IDepthStencilStateImpl
  {
    private bool m_isBound;
    private bool m_depthEnable;
    private bool m_depthWriteEnable;
    private ComparisonFunction m_depthFunction;
    private bool m_stencilEnable;
    private int m_referenceStencil;
    private int m_stencilReadMask;
    private int m_stencilWriteMask;
    private bool m_twoSidedStencilEnable;
    private ComparisonFunction m_stencilFunction;
    private StencilOperation m_stencilDepthFail;
    private StencilOperation m_stencilFail;
    private StencilOperation m_stencilPass;
    private ComparisonFunction m_backFaceStencilFunction;
    private StencilOperation m_backFaceStencilDepthFail;
    private StencilOperation m_backFaceStencilFail;
    private StencilOperation m_backFaceStencilPass;

    /// <summary>
    /// Gets if the render state has been bound to the pipeline, once bound the state becomes read-only.
    /// </summary>
    public bool IsBound
    {
      get
      {
        return m_isBound;
      }
    }

    /// <summary>
    /// Gets or sets if the depth buffer should be enabled. By default, this value is true.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public bool DepthEnable
    {
      get
      {
        return m_depthEnable;
      }
      set
      {
        ThrowIfBound();
        m_depthEnable = value;
      }
    }

    /// <summary>
    /// Gets or sets if the depth buffer should be writable. By default, this value is true.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception> 
    public bool DepthWriteEnable
    {
      get
      {
        return m_depthWriteEnable;
      }
      set
      {
        ThrowIfBound();
        m_depthWriteEnable = value;
      }
    }

    /// <summary>
    /// Gets or sets the depth comparison function for the depth test. By default, this value is <see cref="ComparisonFunction.LessEqual" />.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public ComparisonFunction DepthFunction
    {
      get
      {
        return m_depthFunction;
      }
      set
      {
        ThrowIfBound();
        m_depthFunction = value;
      }
    }

    /// <summary>
    /// Gets or sets if the stencil buffer should be enabled. By default, this value is false.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public bool StencilEnable
    {
      get
      {
        return m_stencilEnable;
      }
      set
      {
        ThrowIfBound();
        m_stencilEnable = value;
      }
    }

    /// <summary>
    /// Gets or sets the reference stencil value used for stencil testing. By default, this value is zero.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception> 
    public int ReferenceStencil
    {
      get
      {
        return m_referenceStencil;
      }
      set
      {
        ThrowIfBound();
        m_referenceStencil = value;
      }
    }

    /// <summary>
    /// Gets or sets the value that identifies a portion of the depth-stencil buffer for reading stencil data. By default, this value is <see cref="int.MaxValue" />.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public int StencilReadMask
    {
      get
      {
        return m_stencilReadMask;
      }
      set
      {
        ThrowIfBound();
        m_stencilReadMask = value;
      }
    }

    /// <summary>
    /// Gets or sets the value that identifies a portion of the depth-stencil buffer for writing stencil data. By default, this value is <see cref="int.MaxValue" />.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public int StencilWriteMask
    {
      get
      {
        return m_stencilWriteMask;
      }
      set
      {
        ThrowIfBound();
        m_stencilWriteMask = value;
      }
    }

    /// <summary>
    /// Gets or sets if two sided stenciling is enabled, where if back face stencil testing/operations should be conducted in addition to the front face (as dictated by the winding order
    /// of the primitive). By default, this value is false.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public bool TwoSidedStencilEnable
    {
      get
      {
        return m_twoSidedStencilEnable;
      }
      set
      {
        ThrowIfBound();
        m_twoSidedStencilEnable = value;
      }
    }

    /// <summary>
    /// Gets or sets the comparison function used for testing a front facing triangle. By default, this value is <see cref="ComparisonFunction.Always" />.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public ComparisonFunction StencilFunction
    {
      get
      {
        return m_stencilFunction;
      }
      set
      {
        ThrowIfBound();
        m_stencilFunction = value;
      }
    }

    /// <summary>
    /// Gets or sets the stencil operation done when the stencil test passes, but the depth test fails for a front facing triangle. By default, this value is
    /// <see cref="StencilOperation.Keep" />.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public StencilOperation StencilDepthFail
    {
      get
      {
        return m_stencilDepthFail;
      }
      set
      {
        ThrowIfBound();
        m_stencilDepthFail = value;
      }
    }

    /// <summary>
    /// Gets or sets the stencil operation done when the stencil test fails for a front facing triangle. By default, this value is <see cref="StencilOperation.Keep" />.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public StencilOperation StencilFail
    {
      get
      {
        return m_stencilFail;
      }
      set
      {
        ThrowIfBound();
        m_stencilFail = value;
      }
    }

    /// <summary>
    /// Gets or sets the stencil operation done when the stencil test passes for a front facing triangle. By default, this value is <see cref="StencilOperation.Keep" />.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public StencilOperation StencilPass
    {
      get
      {
        return m_stencilPass;
      }
      set
      {
        ThrowIfBound();
        m_stencilPass = value;
      }
    }

    /// <summary>
    /// Gets or sets the comparison function used for testing a back facing triangle. By default, this value is <see cref="ComparisonFunction.Always" />.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception> 
    public ComparisonFunction BackFaceStencilFunction
    {
      get
      {
        return m_backFaceStencilFunction;
      }
      set
      {
        ThrowIfBound();
        m_backFaceStencilFunction = value;
      }
    }

    /// <summary>
    /// Gets or sets the stencil operation done when the stencil test passes, but the depth test fails for a back facing triangle. By default, this value is
    /// <see cref="StencilOperation.Keep" />.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public StencilOperation BackFaceStencilDepthFail
    {
      get
      {
        return m_backFaceStencilDepthFail;
      }
      set
      {
        ThrowIfBound();
        m_backFaceStencilDepthFail = value;
      }
    }

    /// <summary>
    /// Gets or sets the stencil operation done when the stencil test fails for a back facing triangle. By default, this value is <see cref="StencilOperation.Keep" />.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public StencilOperation BackFaceStencilFail
    {
      get
      {
        return m_backFaceStencilFail;
      }
      set
      {
        ThrowIfBound();
        m_backFaceStencilFail = value;
      }
    }

    /// <summary>
    /// Gets or sets the stencil operation done when the stencil test passes for a back facing triangle. By default, this value is <see cref="StencilOperation.Keep" />.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public StencilOperation BackFaceStencilPass
    {
      get
      {
        return m_backFaceStencilPass;
      }
      set
      {
        ThrowIfBound();
        m_backFaceStencilPass = value;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="DepthStencilStateImpl"/> class.
    /// </summary>
    /// <param name="renderSystem">The render system that manages this graphics implementation</param>
    /// <param name="resourceID">ID of the resource, supplied by the render system</param>
    protected DepthStencilStateImpl(IRenderSystem renderSystem, int resourceID)
        : base(renderSystem, resourceID)
    {
      m_isBound = false;
    }

    /// <summary>
    /// Binds the implementation, creating the underlying state. Once bound the state is read-only. If unbound, this will happen
    /// automatically when the state is first used during rendering. It is best practice to do this ahead of time.
    /// </summary>
    public void BindDepthStencilState()
    {
      if (!m_isBound)
      {
        CreateNativeState();
        m_isBound = true;
      }
    }

    /// <summary>
    /// Called when the state is bound, signaling the implementation to create and bind the native state.
    /// </summary>
    protected abstract void CreateNativeState();

    private void ThrowIfBound()
    {
      if (m_isBound)
        throw new InvalidOperationException(StringLocalizer.Instance.GetLocalizedString("RenderStateBound"));
    }
  }
}
