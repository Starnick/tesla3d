﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;

#nullable enable

namespace Tesla.Graphics.Implementation
{
  /// <summary>
  /// Common base class for a <see cref="SamplerState"/> implementation.
  /// </summary>
  public abstract class SamplerStateImpl : GraphicsResourceImpl, ISamplerStateImpl
  {
    private bool m_isBound;
    private TextureAddressMode m_addressU;
    private TextureAddressMode m_addressV;
    private TextureAddressMode m_addressW;
    private TextureFilter m_filter;
    private int m_maxAnisotropy;
    private float m_mipMapLevelOfDetailBias;
    private int m_minMipMapLevel;
    private int m_maxMipMapLevel;
    private Color m_borderColor;
    private ComparisonFunction m_compareFunction;

    /// <summary>
    /// Gets if the render state has been bound to the pipeline, once bound the state becomes read-only.
    /// </summary>
    public bool IsBound
    {
      get
      {
        return m_isBound;
      }
    }

    /// <summary>
    /// Gets the number of anisotropy levels supported. This can vary by implementation.
    /// </summary>
    public abstract int SupportedAnisotropyLevels { get; }

    /// <summary>
    /// Gets or sets the addressing mode for the U coordinate. By default, this value is <see cref="TextureAddressMode.Clamp" />.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public TextureAddressMode AddressU
    {
      get
      {
        return m_addressU;
      }
      set
      {
        ThrowIfBound();
        m_addressU = value;
      }
    }

    /// <summary>
    /// Gets or sets the addressing mode for the V coordinate. By default, this value is <see cref="TextureAddressMode.Clamp" />.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public TextureAddressMode AddressV
    {
      get
      {
        return m_addressV;
      }
      set
      {
        ThrowIfBound();
        m_addressV = value;
      }
    }

    /// <summary>
    /// Gets or sets the addressing mode for the W coordinate. By default, this value is <see cref="TextureAddressMode.Clamp" />.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public TextureAddressMode AddressW
    {
      get
      {
        return m_addressW;
      }
      set
      {
        ThrowIfBound();
        m_addressW = value;
      }
    }

    /// <summary>
    /// Gets or sets the filtering used during texture sampling. By default, this value is <see cref="TextureFilter.Linear" />.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public TextureFilter Filter
    {
      get
      {
        return m_filter;
      }
      set
      {
        ThrowIfBound();
        m_filter = value;
      }
    }

    /// <summary>
    /// Gets or sets the maximum anisotropy. This is used to clamp values when the filter is set to anisotropic. By default, this value is
    /// <see cref="SupportedAnisotropyLevels" /> as it can vary by implementation. The minimum value is always one. If a value is higher or lower than
    /// these limits, it is clamped. 
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public int MaxAnisotropy
    {
      get
      {
        return m_maxAnisotropy;
      }
      set
      {
        ThrowIfBound();
        m_maxAnisotropy = MathHelper.Clamp(value, 1, SupportedAnisotropyLevels);
      }
    }

    /// <summary>
    /// Gets or sets the mipmap LOD bias. This is the offset from the calculated mipmap level that is actually used (e.g. sampled at mipmap level 3 with offset 2, then the
    /// mipmap at level 5 is sampled). By default, this value is zero.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public float MipMapLevelOfDetailBias
    {
      get
      {
        return m_mipMapLevelOfDetailBias;
      }
      set
      {
        ThrowIfBound();
        m_mipMapLevelOfDetailBias = value;
      }
    }

    /// <summary>
    /// Gets or sets the lower bound of the mipmap range [0, n-1] to clamp access to, where zero is the largest and most detailed mipmap level. The level n-1 is the least detailed mipmap level.
    /// By default, this value is zero.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public int MinMipMapLevel
    {
      get
      {
        return m_minMipMapLevel;
      }
      set
      {
        ThrowIfBound();
        m_minMipMapLevel = value;
      }
    }

    /// <summary>
    /// Gets or sets the upper bound of the mipmap range [0, n-1] to clamp access to, where zero is the largest and most detailed mipmap level. The level n-1 is the least detailed mipmap level.
    /// By default, this value is <see cref="int.MaxValue" />.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public int MaxMipMapLevel
    {
      get
      {
        return m_maxMipMapLevel;
      }
      set
      {
        ThrowIfBound();
        m_maxMipMapLevel = value;
      }
    }

    /// <summary>
    /// Gets or sets the border color if the texture addressing is set to border. By default, this value is <see cref="Color.TransparentBlack" />.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public Color BorderColor
    {
      get
      {
        return m_borderColor;
      }
      set
      {
        ThrowIfBound();
        m_borderColor = value;
      }
    }

    /// <summary>
    /// Gets or sets function that compares sampled data against existing sampled data. By default, this value is <see cref="ComparisonFunction.Never"/>.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public ComparisonFunction CompareFunction
    {
      get
      {
        return m_compareFunction;
      }
      set
      {
        ThrowIfBound();
        m_compareFunction = value;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="SamplerStateImpl"/> class.
    /// </summary>
    /// <param name="renderSystem">The render system that manages this graphics implementation</param>
    /// <param name="resourceID">ID of the resource, supplied by the render system</param>
    protected SamplerStateImpl(IRenderSystem renderSystem, int resourceID)
        : base(renderSystem, resourceID)
    {
      m_isBound = false;
    }

    /// <summary>
    /// Checks if the specified texture addressing mode is supported by the graphics platform.
    /// </summary>
    /// <param name="mode">Texture addressing mode</param>
    /// <returns>True if supported, false otherwise.</returns>
    public abstract bool IsAddressingModeSupported(TextureAddressMode mode);

    /// <summary>
    /// Binds the implementation, creating the underlying state. Once bound the state is read-only. If unbound, this will happen
    /// automatically when the state is first used during rendering. It is best practice to do this ahead of time.
    /// </summary>
    public void BindSamplerState()
    {
      if (!m_isBound)
      {
        CreateNativeState();
        m_isBound = true;
      }
    }

    /// <summary>
    /// Called when the state is bound, signaling the implementation to create and bind the native state.
    /// </summary>
    protected abstract void CreateNativeState();

    private void ThrowIfBound()
    {
      if (m_isBound)
        throw new InvalidOperationException(StringLocalizer.Instance.GetLocalizedString("RenderStateBound"));
    }
  }
}
