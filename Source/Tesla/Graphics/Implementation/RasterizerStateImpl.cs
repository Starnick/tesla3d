﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;

#nullable enable

namespace Tesla.Graphics.Implementation
{
  /// <summary>
  /// Common base class for a <see cref="RasterizerState"/> implementation.
  /// </summary>
  public abstract class RasterizerStateImpl : GraphicsResourceImpl, IRasterizerStateImpl
  {
    private bool m_isBound;
    private CullMode m_cull;
    private VertexWinding m_vertexWinding;
    private FillMode m_fill;
    private int m_depthBias;
    private float m_depthBiasClamp;
    private float m_slopeScaledDepthBias;
    private bool m_depthClipEnable;
    private bool m_multiSampleEnable;
    private bool m_antialiasedLineEnable;
    private bool m_scissorTestEnable;

    /// <summary>
    /// Gets if the render state has been bound to the pipeline, once bound the state becomes read-only.
    /// </summary>
    public bool IsBound
    {
      get
      {
        return m_isBound;
      }
    }

    /// <summary>
    /// Gets if the <see cref="AntialiasedLineEnable" /> property is supported. This can vary by implementation.
    /// </summary>
    public abstract bool IsAntialiasedLineOptionSupported { get; }

    /// <summary>
    /// Gets if the <see cref="DepthClipEnable" /> property is supported. This can vary by implementation.
    /// </summary>
    public abstract bool IsDepthClipOptionSupported { get; }

    /// <summary>
    /// Gets or sets how primitives are to be culled. By default, this value is <see cref="CullMode.Back" />.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public CullMode Cull
    {
      get
      {
        return m_cull;
      }
      set
      {
        ThrowIfBound();
        m_cull = value;
      }
    }

    /// <summary>
    /// Gets or sets the vertex winding of a primitive, specifying the front face of the triangle. By default, this value is <see cref="Graphics.VertexWinding.CounterClockwise" />.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public VertexWinding VertexWinding
    {
      get
      {
        return m_vertexWinding;
      }
      set
      {
        ThrowIfBound();
        m_vertexWinding = value;
      }
    }

    /// <summary>
    /// Gets or sets the fill mode of a primitive. By default, this value is <see cref="FillMode.Solid" />.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public FillMode Fill
    {
      get
      {
        return m_fill;
      }
      set
      {
        ThrowIfBound();
        m_fill = value;
      }
    }

    /// <summary>
    /// Gets or sets the depth bias, which is a value added to the depth value at a given pixel. By default, this value is zero.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception> 
    public int DepthBias
    {
      get
      {
        return m_depthBias;
      }
      set
      {
        ThrowIfBound();
        m_depthBias = value;
      }
    }

    /// <summary>
    /// Gets or sets the depth bias clamp (maximum value) of a pixel. By default, this value is zero.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public float DepthBiasClamp
    {
      get
      {
        return m_depthBiasClamp;
      }
      set
      {
        ThrowIfBound();
        m_depthBiasClamp = value;
      }
    }

    /// <summary>
    /// Gets or sets the slope scaled depth bias, a scalar on a given pixel's slope. By default, this value is zero.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public float SlopeScaledDepthBias
    {
      get
      {
        return m_slopeScaledDepthBias;
      }
      set
      {
        ThrowIfBound();
        m_slopeScaledDepthBias = value;
      }
    }

    /// <summary>
    /// Gets or sets if depth clipping is enabled. If false, the hardware skips z-clipping. By default, this value is true.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public bool DepthClipEnable
    {
      get
      {
        return m_depthClipEnable;
      }
      set
      {
        ThrowIfBound();
        m_depthClipEnable = value;
      }
    }

    /// <summary>
    /// Gets or sets whether to use the quadrilateral or alpha line anti-aliasing algorithm on MSAA render targets. If set to true, the quadrilaterla line anti-aliasing algorithm is used.
    /// Otherwise the alpha line-anti-aliasing algorithm is used, if <see cref="MultiSampleEnable"/> is set to false and is supported. By default, this value is true.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public bool MultiSampleEnable
    {
      get
      {
        return m_multiSampleEnable;
      }
      set
      {
        ThrowIfBound();
        m_multiSampleEnable = value;
      }
    }

    /// <summary>
    /// Gets or sets whether to enable line antialising. This only applies if doing line drawing and <see cref="MultiSampleEnable" /> is set to false.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public bool AntialiasedLineEnable
    {
      get
      {
        return m_antialiasedLineEnable;
      }
      set
      {
        ThrowIfBound();
        m_antialiasedLineEnable = value;
      }
    }

    /// <summary>
    /// Gets or sets if scissor rectangle culling should be enabled or not. All pixels outside an active scissor rectangle are culled. By default, this value is set to false.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception> 
    public bool ScissorTestEnable
    {
      get
      {
        return m_scissorTestEnable;
      }
      set
      {
        ThrowIfBound();
        m_scissorTestEnable = value;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="RasterizerStateImpl"/> class.
    /// </summary>
    /// <param name="renderSystem">The render system that manages this graphics implementation</param>
    /// <param name="resourceID">ID of the resource, supplied by the render system</param>
    protected RasterizerStateImpl(IRenderSystem renderSystem, int resourceID)
        : base(renderSystem, resourceID)
    {
      m_isBound = false;
    }

    /// <summary>
    /// Binds the implementation, creating the underlying state. Once bound the state is read-only. If unbound, this will happen
    /// automatically when the state is first used during rendering. It is best practice to do this ahead of time.
    /// </summary>
    public void BindRasterizerState()
    {
      if (!m_isBound)
      {
        CreateNativeState();
        m_isBound = true;
      }
    }

    /// <summary>
    /// Called when the state is bound, signaling the implementation to create and bind the native state.
    /// </summary>
    protected abstract void CreateNativeState();

    private void ThrowIfBound()
    {
      if (m_isBound)
        throw new InvalidOperationException(StringLocalizer.Instance.GetLocalizedString("RenderStateBound"));
    }
  }
}
