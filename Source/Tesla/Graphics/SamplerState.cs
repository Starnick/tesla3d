﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Content;
using Tesla.Graphics.Implementation;

namespace Tesla.Graphics
{
  /// <summary>
  /// Represents a render state that controls how a texture is sampled.
  /// </summary>
  [SavableVersion(1)]
  public class SamplerState : RenderState, IShaderResource
  {
    private RenderStateKey m_key;

    #region Predefined SamplerStates

    /// <summary>
    /// Gets the predefined state object where point filtering is used and UVW coordinates wrap.
    /// </summary>
    public static SamplerState PointWrap
    {
      get
      {
        return IRenderSystem.Current.PredefinedSamplerStates.PointWrap;
      }
    }

    /// <summary>
    /// Gets the predefined state object where point filtering is used and UVW coordinates are clamped in the range of [0, 1]. This
    /// is the default state.
    /// </summary>
    public static SamplerState PointClamp
    {
      get
      {
        return IRenderSystem.Current.PredefinedSamplerStates.PointClamp;
      }
    }

    /// <summary>
    /// Gets the predefined state object where linear filtering is used and UVW coordinates wrap.
    /// </summary>
    public static SamplerState LinearWrap
    {
      get
      {
        return IRenderSystem.Current.PredefinedSamplerStates.LinearWrap;
      }
    }

    /// <summary>
    /// Gets the predefined state object where linear filtering is used and UVW coordinates are clamped in the range of [0, 1].
    /// </summary>
    public static SamplerState LinearClamp
    {
      get
      {
        return IRenderSystem.Current.PredefinedSamplerStates.LinearClamp;
      }
    }

    /// <summary>
    /// Gets the predefined state object where anisotropic filtering is used and UVW coordinates wrap.
    /// </summary>
    public static SamplerState AnisotropicWrap
    {
      get
      {
        return IRenderSystem.Current.PredefinedSamplerStates.AnisotropicWrap;
      }
    }

    /// <summary>
    /// Gets the predefined state object where anisotropic filtering is used and UVW coordinates are
    /// clamped in the range of [0, 1].
    /// </summary>
    public static SamplerState AnisotropicClamp
    {
      get
      {
        return IRenderSystem.Current.PredefinedSamplerStates.AnisotropicClamp;
      }
    }

    #endregion

    #region RenderState Properties

    /// <summary>
    /// Gets if the render state has been bound to the pipeline, once bound the state becomes read-only.
    /// </summary>
    public override bool IsBound
    {
      get
      {
        return SamplerStateImpl.IsBound;
      }
    }

    /// <summary>
    /// Gets the render state type.
    /// </summary>
    public override RenderStateType StateType
    {
      get
      {
        return RenderStateType.SamplerState;
      }
    }

    /// <summary>
    /// Gets the key that identifies this render state type and configuration for comparing states.
    /// </summary>
    public override RenderStateKey RenderStateKey
    {
      get
      {
        if (!SamplerStateImpl.IsBound)
          return ComputeRenderStateKey();

        return m_key;
      }
    }

    #endregion

    #region Public SamplerState Properties

    /// <summary>
    /// Gets the number of anisotropy levels supported. This can vary by implementation.
    /// </summary>
    public int SupportedAnisotropyLevels
    {
      get
      {
        return SamplerStateImpl.SupportedAnisotropyLevels;
      }
    }

    /// <summary>
    /// Gets or sets the addressing mode for the U coordinate. By default, this value is <see cref="TextureAddressMode.Clamp" />.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public TextureAddressMode AddressU
    {
      get
      {
        return SamplerStateImpl.AddressU;
      }
      set
      {
        SamplerStateImpl.AddressU = value;
      }
    }

    /// <summary>
    /// Gets or sets the addressing mode for the V coordinate. By default, this value is <see cref="TextureAddressMode.Clamp" />.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public TextureAddressMode AddressV
    {
      get
      {
        return SamplerStateImpl.AddressV;
      }
      set
      {
        SamplerStateImpl.AddressV = value;
      }
    }

    /// <summary>
    /// Gets or sets the addressing mode for the W coordinate. By default, this value is <see cref="TextureAddressMode.Clamp" />.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public TextureAddressMode AddressW
    {
      get
      {
        return SamplerStateImpl.AddressW;
      }
      set
      {
        SamplerStateImpl.AddressW = value;
      }
    }

    /// <summary>
    /// Gets or sets the filtering used during texture sampling. By default, this value is <see cref="TextureFilter.Linear" />.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public TextureFilter Filter
    {
      get
      {
        return SamplerStateImpl.Filter;
      }
      set
      {
        SamplerStateImpl.Filter = value;
      }
    }

    /// <summary>
    /// Gets or sets the maximum anisotropy. This is used to clamp values when the filter is set to anisotropic. By default, this value is set to 1 and can be set up
    /// to <see cref="SupportedAnisotropyLevels" />, which can vary by implementation. If a higher or lower value is set than supported, it is clamped.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public int MaxAnisotropy
    {
      get
      {
        return SamplerStateImpl.MaxAnisotropy;
      }
      set
      {
        SamplerStateImpl.MaxAnisotropy = value;
      }
    }

    /// <summary>
    /// Gets or sets the mipmap LOD bias. This is the offset from the calculated mipmap level that is actually used (e.g. sampled at mipmap level 3 with offset 2, then the
    /// mipmap at level 5 is sampled). By default, this value is zero.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public float MipMapLevelOfDetailBias
    {
      get
      {
        return SamplerStateImpl.MipMapLevelOfDetailBias;
      }
      set
      {
        SamplerStateImpl.MipMapLevelOfDetailBias = value;
      }
    }

    /// <summary>
    /// Gets or sets the lower bound of the mipmap range [0, n-1] to clamp access to, where zero is the largest and most detailed mipmap level. The level n-1 is the least detailed mipmap level.
    /// By default, this value is zero.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public int MinMipMapLevel
    {
      get
      {
        return SamplerStateImpl.MinMipMapLevel;
      }
      set
      {
        SamplerStateImpl.MinMipMapLevel = value;
      }
    }

    /// <summary>
    /// Gets or sets the upper bound of the mipmap range [0, n-1] to clamp access to, where zero is the largest and most detailed mipmap level. The level n-1 is the least detailed mipmap level.
    /// By default, this value is <see cref="int.MaxValue" />.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public int MaxMipMapLevel
    {
      get
      {
        return SamplerStateImpl.MaxMipMapLevel;
      }
      set
      {
        SamplerStateImpl.MaxMipMapLevel = value;
      }
    }

    /// <summary>
    /// Gets or sets the border color if the texture addressing is set to border. By default, this value is <see cref="Color.TransparentBlack" />.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public Color BorderColor
    {
      get
      {
        return SamplerStateImpl.BorderColor;
      }
      set
      {
        SamplerStateImpl.BorderColor = value;
      }
    }

    /// <summary>
    /// Gets or sets function that compares sampled data against existing sampled data. By default, this value is <see cref="ComparisonFunction.Never"/>.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public ComparisonFunction CompareFunction
    {
      get
      {
        return SamplerStateImpl.CompareFunction;
      }
      set
      {
        SamplerStateImpl.CompareFunction = value;
      }
    }

    /// <summary>
    /// Gets the shader resource type.
    /// </summary>
    public ShaderResourceType ResourceType
    {
      get
      {
        return ShaderResourceType.SamplerState;
      }
    }

    #endregion

    //Private property to cast the implementation
    private ISamplerStateImpl SamplerStateImpl
    {
      get
      {
        return GetImplAs<ISamplerStateImpl>();
      }
      set
      {
        BindImplementation(value);
      }
    }

    /// <summary>
    /// For ISavable only.
    /// </summary>
    protected SamplerState() { }

    /// <summary>
    /// Constructs a new instance of the <see cref="SamplerState"/> class.
    /// </summary>
    /// <param name="renderSystem">Render system used to create the underlying implementation.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the render system is null.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if creating the underlying implementation fails or is unsupported, see inner exception for a more detailed error.</exception>
    public SamplerState(IRenderSystem renderSystem)
    {
      CreateImplementation(renderSystem);
      SetDefaults();
    }

    /// <summary>
    /// Binds the render state to the graphics pipeline. If not called after the state is created, it is automatically done the first time the render state
    /// is applied. Once bound, the render state becomes immutable.
    /// </summary>
    public override void BindRenderState()
    {
      if (!SamplerStateImpl.IsBound)
      {
        SamplerStateImpl.BindSamplerState();
        m_key = ComputeRenderStateKey();
      }
    }

    #region ISavable Methods

    /// <summary>
    /// Reads the object data from the input.
    /// </summary>
    /// <param name="input">Savable reader</param>
    public override void Read(ISavableReader input)
    {
      IRenderSystem renderSystem = GraphicsHelper.GetRenderSystem(input.ServiceProvider);
      ISamplerStateImpl impl = CreateImplementation(renderSystem);

      Name = input.ReadString("Name");
      impl.AddressU = input.ReadEnum<TextureAddressMode>("AddressU");
      impl.AddressV = input.ReadEnum<TextureAddressMode>("AddressV");
      impl.AddressW = input.ReadEnum<TextureAddressMode>("AddressW");
      impl.Filter = input.ReadEnum<TextureFilter>("Filter");
      impl.MipMapLevelOfDetailBias = input.ReadSingle("MipMapLevelOfDetailBias");
      impl.MinMipMapLevel = input.ReadInt32("MinMipMapLevel");
      impl.MaxMipMapLevel = input.ReadInt32("MaxMipMapLevel");
      impl.MaxAnisotropy = input.ReadInt32("MaxAnisotropy");
      impl.BorderColor = input.Read<Color>("BorderColor");
      impl.CompareFunction = input.ReadEnum<ComparisonFunction>("CompareFunction");

      impl.BindSamplerState();
    }

    /// <summary>
    /// Writes the object data to the output.
    /// </summary>
    /// <param name="output">Savable writer</param>
    public override void Write(ISavableWriter output)
    {
      ISamplerStateImpl impl = SamplerStateImpl;

      output.Write("Name", Name);
      output.WriteEnum<TextureAddressMode>("AddressU", impl.AddressU);
      output.WriteEnum<TextureAddressMode>("AddressV", impl.AddressV);
      output.WriteEnum<TextureAddressMode>("AddressW", impl.AddressW);
      output.WriteEnum<TextureFilter>("Filter", impl.Filter);
      output.Write("MipMapLevelOfDetailBias", impl.MipMapLevelOfDetailBias);
      output.Write("MinMipMapLevel", impl.MinMipMapLevel);
      output.Write("MaxMipMapLevel", impl.MaxMipMapLevel);
      output.Write("MaxAnisotropy", impl.MaxAnisotropy);
      output.Write("BorderColor", impl.BorderColor);
      output.WriteEnum<ComparisonFunction>("CompareFunction", impl.CompareFunction);
    }

    #endregion

    #region Implementation Creation and State Key Methods

    private ISamplerStateImpl CreateImplementation(IRenderSystem renderSystem)
    {
      if (renderSystem == null)
        throw new ArgumentNullException("renderSystem", StringLocalizer.Instance.GetLocalizedString("RenderSystemNull"));

      ISamplerStateImplFactory factory;
      if (!renderSystem.TryGetImplementationFactory<ISamplerStateImplFactory>(out factory))
        throw GraphicsExceptionHelper.NewUnsupportedFeatureException(renderSystem, typeof(SamplerState));

      try
      {
        SamplerStateImpl = factory.CreateImplementation();
      }
      catch (Exception e)
      {
        throw GraphicsExceptionHelper.NewErrorCreatingImplementation(typeof(SamplerState), e);
      }

      return SamplerStateImpl;
    }

    private void SetDefaults()
    {
      ISamplerStateImpl impl = SamplerStateImpl;

      impl.AddressU = TextureAddressMode.Clamp;
      impl.AddressV = TextureAddressMode.Clamp;
      impl.AddressW = TextureAddressMode.Clamp;
      impl.Filter = TextureFilter.Linear;
      impl.MaxAnisotropy = 1;
      impl.MipMapLevelOfDetailBias = 0.0f;
      impl.MinMipMapLevel = 0;
      impl.MaxMipMapLevel = int.MaxValue;
      impl.BorderColor = Color.White;
      impl.CompareFunction = ComparisonFunction.Never;
    }

    private RenderStateKey ComputeRenderStateKey()
    {
      unchecked
      {
        ISamplerStateImpl impl = SamplerStateImpl;

        //Casting enums to ints to avoid boxing

        int hash = 17;

        hash = (hash * 31) + (int) StateType;
        hash = (hash * 31) + (int) impl.AddressU;
        hash = (hash * 31) + (int) impl.AddressV;
        hash = (hash * 31) + (int) impl.AddressW;
        hash = (hash * 31) + (int) impl.Filter;
        hash = (hash * 31) + impl.MaxAnisotropy;
        hash = (hash * 31) + impl.MinMipMapLevel;
        hash = (hash * 31) + (impl.MaxMipMapLevel % int.MaxValue);
        hash = (hash * 31) + impl.MipMapLevelOfDetailBias.GetHashCode();
        hash = (hash * 31) + impl.BorderColor.GetHashCode();
        hash = (hash * 31) + (int) impl.CompareFunction;

        return new RenderStateKey(StateType, hash);
      }
    }

    #endregion
  }
}
