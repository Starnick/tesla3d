﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// Render property for a <see cref="BoundingVolume"/> that represents an object's world bounding volume.
  /// </summary>
  public sealed class WorldBoundingVolumeProperty : RenderPropertyAccessor<BoundingVolume>
  {
    private BoundingVolume? m_value;

    /// <summary>
    /// Unique ID for this render property.
    /// </summary>
    public static RenderPropertyID PropertyID = GetPropertyID<WorldBoundingVolumeProperty>();

    /// <summary>
    /// Constructs a new instance of the <see cref="WorldBoundingVolumeProperty"/> class.
    /// </summary>
    /// <param name="accessor">Accessor function.</param>
    public WorldBoundingVolumeProperty(GetPropertyValue<BoundingVolume> accessor) : base(PropertyID, accessor) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="WorldBoundingVolumeProperty"/> class.
    /// </summary>
    public WorldBoundingVolumeProperty() : this(new BoundingSphere()) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="WorldTransformProperty"/> class.
    /// </summary>
    /// <param name="volume">World transform.</param>
    public WorldBoundingVolumeProperty(BoundingVolume volume)
        : base(PropertyID)
    {
      m_value = volume ?? new BoundingSphere();
      Accessor = GetValue;
    }

    private BoundingVolume GetValue()
    {
      return m_value!;
    }
  }
}
