﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// Defines common texture creation options.
  /// </summary>
  public ref struct TextureOptions
  {
    /// <summary>
    /// True to create subresources for the entire mip map chain of the texture, false otherwise (only the first mip level of each slice is created). Default is false.
    /// </summary>
    public bool WantMipMaps;

    /// <summary>
    /// Surface format of the texture. Default is <see cref="SurfaceFormat.Color"/>.
    /// </summary>
    public SurfaceFormat Format;

    /// <summary>
    /// Resource usage specifying the type of memory the texture should use. If <see cref="ResourceUsage.Immutable"/> then initial data
    /// is required as the texture cannot be modified once created. Default is <see cref="ResourceUsage.Static"/>.
    /// </summary>
    public ResourceUsage ResourceUsage;

    /// <summary>
    /// Resource bind flags specifying in what other ways the texture can be bound to the graphics pipeline. Default is <see cref="ResourceBindFlags.Default"/>.
    /// </summary>
    public ResourceBindFlags ResourceBindFlags;

    /// <summary>
    /// Initial data to populate the texture. It may be empty, but if non-empty the length must be (mipmap count * slice count) of the texture but entries are permitted to be null. 
    /// Default is an empty span.
    /// </summary>
    /// <remarks>
    /// <para>
    /// The initial data is in order of each mipmap chain for each slice (if array or 3D), so the first mipmap chain of the first array slice
    /// comes first, then the next mipmapchain for the next slice, etc. Each buffer most not exceed the size of it's mipmap level. 
    /// See <see cref="SubResourceAt"/> for index and buffer size calculations.
    /// for a given texture size/format.
    /// </para>
    /// <para>
    /// For cubemaps the slice order is defined by <see cref="CubeMapFace"/> which effectively means cubemaps are 2D arrays in multiples of 6.
    /// </para>
    /// </remarks>
    public ReadOnlySpan<IReadOnlyDataBuffer?> Data;

    /// <summary>
    /// Constructs a new instance of the <see cref="TextureOptions"/> struct.
    /// </summary>
    /// <param name="wantMipMaps">True to create subresources for the entire mip map chain of the texture, false otherwise (only the first mip level of each slice is created).</param>
    /// <param name="format">Surface format of the texture.</param>
    /// <param name="usage">Resource usage specifying the type of memory the texture should use. If <see cref="ResourceUsage.Immutable"/> then initial data
    /// is required as the texture cannot be modified once created.</param>
    /// <param name="bindFlags">Resource binding flags specifying in what other ways the texture can be bound to the graphics pipeline.</param>
    /// <param name="data">Data to initialize the texture with. If the texture is immutable, this is required, but not for other usage types.</param>
    public TextureOptions(bool wantMipMaps, SurfaceFormat format, ResourceUsage usage, ResourceBindFlags bindFlags, ReadOnlySpan<IReadOnlyDataBuffer?> data)
    {
      WantMipMaps = wantMipMaps;
      Format = format;
      ResourceUsage = usage;
      ResourceBindFlags = bindFlags;
      Data = data;
    }

    /// <summary>
    /// Creates default texture options for an empty texture.
    /// </summary>
    /// <param name="format">Optional surface format of the texture.</param>
    /// <param name="usage">Optional resource usage specifying the type of memory the texture should use. If <see cref="ResourceUsage.Immutable"/> then initial data
    /// is required as the texture cannot be modified once created.</param>
    /// <param name="bindFlags">Optional binding flags.</param>
    /// <returns>Texture options.</returns>
    public static TextureOptions Init(SurfaceFormat format = SurfaceFormat.Color, ResourceUsage usage = ResourceUsage.Static, ResourceBindFlags bindFlags = ResourceBindFlags.Default)
    {
      return new TextureOptions(false, format, usage, bindFlags, ReadOnlySpan<IReadOnlyDataBuffer?>.Empty);
    }

    /// <summary>
    /// Creates default texture options for an empty texture.
    /// </summary>
    /// <param name="wantMipMaps">True to create subresources for the entire mip map chain of the texture, false otherwise (only the first mip level of each slice is created).</param>
    /// <param name="format">Optional surface format of the texture.</param>
    /// <param name="usage">Optional resource usage specifying the type of memory the texture should use. If <see cref="ResourceUsage.Immutable"/> then initial data
    /// is required as the texture cannot be modified once created.</param>
    /// <param name="bindFlags">Optional binding flags.</param>
    /// <returns>Texture options.</returns>
    public static TextureOptions Init(bool wantMipMaps, SurfaceFormat format = SurfaceFormat.Color, ResourceUsage usage = ResourceUsage.Static, ResourceBindFlags bindFlags = ResourceBindFlags.Default)
    {
      return new TextureOptions(wantMipMaps, format, usage, bindFlags, ReadOnlySpan<IReadOnlyDataBuffer?>.Empty);
    }

    /// <summary>
    /// Creates default texture options with the specified data for the first miplevel of a 1D or 2D or 3D texture.
    /// </summary>
    /// <param name="data">First mip-level initial data.</param>
    /// <param name="format">Optional surface format of the texture.</param>
    /// <param name="usage">Optional resource usage specifying the type of memory the texture should use. If <see cref="ResourceUsage.Immutable"/> then initial data
    /// is required as the texture cannot be modified once created.</param>
    /// <param name="bindFlags">Optional binding flags.</param>
    /// <returns>Texture options.</returns>
    public static TextureOptions Init(in IReadOnlyDataBuffer data, SurfaceFormat format = SurfaceFormat.Color, ResourceUsage usage = ResourceUsage.Static, ResourceBindFlags bindFlags = ResourceBindFlags.Default)
    {
      return new TextureOptions(false, format, usage, bindFlags, new ReadOnlySpan<IReadOnlyDataBuffer?>(data));
    }

    /// <summary>
    /// Creates default texture options with the specified data for multiple mip levels and/or multiple slices.
    /// </summary>
    /// <param name="data">Data to initialize the texture with. It may be empty, but if non-empty the length must be (mipmap count * slice count) of the texture.</param>
    /// <param name="wantMipMaps">True to create subresources for the entire mip map chain of the texture, false otherwise (only the first mip level of each slice is created).</param>
    /// <param name="format">Optional surface format of the texture.</param>
    /// <param name="usage">Optional resource usage specifying the type of memory the texture should use. If <see cref="ResourceUsage.Immutable"/> then initial data
    /// is required as the texture cannot be modified once created.</param>
    /// <param name="bindFlags">Optional binding flags.</param>
    /// <returns>Texture options.</returns>
    /// <remarks>
    /// <para>
    /// The initial data is in order of each mipmap chain for each slice (if array or Cube), so the first mipmap chain of the first array slice
    /// comes first, then the next mipmapchain for the next slice, etc. Each buffer most not exceed the size of it's mipmap level. 
    /// See <see cref="SubResourceAt"/> for index and buffer size calculations for a given texture size/format.
    /// </para>
    /// <para>
    /// For cubemaps the slice order is defined by <see cref="CubeMapFace"/> which effectively means cubemaps are 2D arrays in multiples of 6.
    /// </para>
    /// </remarks>
    public static TextureOptions Init(ReadOnlySpan<IReadOnlyDataBuffer?> data, bool wantMipMaps, SurfaceFormat format = SurfaceFormat.Color, ResourceUsage usage = ResourceUsage.Static, ResourceBindFlags bindFlags = ResourceBindFlags.Default)
    {
      return new TextureOptions(wantMipMaps, format, usage, bindFlags, data);
    }

    /// <summary>
    /// Creates default texture options with the specified data for multiple slices for an array or cube texture. Each slice will only have one mip level.
    /// </summary>
    /// <param name="data">Data to initialize the texture with. It may be empty, but if non-empty the length must be (mipmap count * slice count) of the texture.</param>
    /// <param name="format">Optional surface format of the texture.</param>
    /// <param name="usage">Optional resource usage specifying the type of memory the texture should use. If <see cref="ResourceUsage.Immutable"/> then initial data
    /// is required as the texture cannot be modified once created.</param>
    /// <param name="bindFlags">Optional binding flags.</param>
    /// <returns>Texture options.</returns>
    /// <remarks>
    /// <para>
    /// The initial data is in order of each mipmap chain for each slice (if array or or Cube), so the first mipmap chain of the first array slice
    /// comes first, then the next mipmapchain for the next slice, etc. Each buffer most not exceed the size of it's mipmap level. 
    /// See <see cref="SubResourceAt"/> for index and buffer size calculations for a given texture size/format.
    /// </para>
    /// <para>
    /// For cubemaps the slice order is defined by <see cref="CubeMapFace"/> which effectively means cubemaps are 2D arrays in multiples of 6.
    /// </para>
    /// </remarks>
    public static TextureOptions Init(ReadOnlySpan<IReadOnlyDataBuffer?> data, SurfaceFormat format = SurfaceFormat.Color, ResourceUsage usage = ResourceUsage.Static, ResourceBindFlags bindFlags = ResourceBindFlags.Default)
    {
      return new TextureOptions(false, format, usage, bindFlags, data);
    }

    /// <summary>
    /// Convience conversion operator to create a default texture options that specifies for mipmaps.
    /// </summary>
    /// <param name="wantMipMaps">True to create subresources for the entire mip map chain of the texture, false otherwise (only the first mip level of each slice is created).</param>
    public static implicit operator TextureOptions(bool wantMipMaps)
    {
      return Init(wantMipMaps);
    }

    /// <summary>
    /// Convience conversion operator to create a default texture options with the specified format.
    /// </summary>
    /// <param name="format">Surface format of the texture.</param>
    public static implicit operator TextureOptions(SurfaceFormat format)
    {
      return Init(format);
    }

    /// <summary>
    /// Convience conversion operator to create a default texture options with the specified usage.
    /// </summary>
    /// <param name="usage">Resource usage.</param>
    public static implicit operator TextureOptions(ResourceUsage usage)
    {
      return Init(SurfaceFormat.Color, usage);
    }

    /// <summary>
    /// Convience conversion operator to create a default texture options with the specified binding flags.
    /// </summary>
    /// <param name="bindFlags">Resource binding flags specifying in what other ways the buffer can be bound to the graphics pipeline.</param>
    public static implicit operator TextureOptions(ResourceBindFlags bindFlags)
    {
      return Init(SurfaceFormat.Color, ResourceUsage.Static, bindFlags);
    }
  }
}
