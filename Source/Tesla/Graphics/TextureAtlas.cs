﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/


using System;
using System.Collections.Generic;
using Tesla.Content;

namespace Tesla.Graphics
{
  /// <summary>
  /// A texture atlas is a single texture that contains a number of sub-textures, all referenced by a name and a rectangle
  /// (in texel coordinates). It is commonly used to batch small textures together to reduce state changes in the render pipeline.
  /// </summary>
  [SavableVersion(1)]
  public class TextureAtlas : Dictionary<string, Rectangle>, ISavable, INamable, IContentCastable
  {
    private Texture2D m_texture;
    private string m_name;

    /// <summary>
    /// Gets or sets the texture.
    /// </summary>
    public Texture2D Texture
    {
      get
      {
        return m_texture;
      }
      set
      {
        ArgumentNullException.ThrowIfNull(value, nameof(Texture));
        m_texture = value;
      }
    }

    /// <summary>
    /// Gets or sets the name of the object.
    /// </summary>
    public string Name
    {
      get
      {
        return m_name;
      }

      set
      {
        if (m_texture is not null)
          m_texture.Name = value;

        m_name = value;
      }
    }

    /// <summary>
    /// For ISavable.
    /// </summary>
    protected TextureAtlas() { }

    /// <summary>
    /// Constructs a new instance of the <see cref="TextureAtlas"/> class.
    /// </summary>
    /// <param name="texture">The texture.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the texture is null.</exception>
    public TextureAtlas(Texture2D texture)
    {
      ArgumentNullException.ThrowIfNull(texture, nameof(texture));

      m_texture = texture;
      m_name = String.Empty;
    }

    /// <summary>
    /// Reads the object data from the input.
    /// </summary>
    /// <param name="input">Savable reader</param>
    public void Read(ISavableReader input)
    {
      m_name = input.ReadString("Name");
      m_texture = input.ReadSharedSavable<Texture2D>("Texture");

      int numSubImages = input.BeginReadGroup("SubImages");

      for (int i = 0; i < numSubImages; i++)
      {
        input.BeginReadGroup("SubImage");

        String name = input.ReadString("Name");
        Rectangle rect;
        input.Read<Rectangle>("Rectangle", out rect);

        Add(name, rect);

        input.EndReadGroup();
      }

      input.EndReadGroup();
    }

    /// <summary>
    /// Writes the object data to the output.
    /// </summary>
    /// <param name="output">Savable writer</param>
    public void Write(ISavableWriter output)
    {
      output.Write("Name", m_name);

      output.WriteSharedSavable<Texture2D>("Texture", m_texture);

      output.BeginWriteGroup("SubImages", Count);

      foreach (KeyValuePair<String, Rectangle> kv in this)
      {
        output.BeginWriteGroup("SubImage");

        output.Write("Name", kv.Key);
        output.Write<Rectangle>("Rectangle", kv.Value);

        output.EndWriteGroup();
      }

      output.EndWriteGroup();
    }

    /// <summary>
    /// Attempts to cast the content item to another type.
    /// </summary>
    /// <param name="targetType">Type to cast to.</param>
    /// <param name="subresourceName">Optional subresource name.</param>
    /// <returns>Casted type or null if the type could not be converted.</returns>
    object IContentCastable.CastTo(Type targetType, String subresourceName)
    {
      if (targetType.IsAssignableFrom(typeof(Texture2D)))
        return m_texture;

      return this;
    }
  }
}
