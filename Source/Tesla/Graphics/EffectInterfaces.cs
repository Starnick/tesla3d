﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;

// TODO Nullable

namespace Tesla.Graphics
{
  /// <summary>
  /// Represents a part of an <see cref="Effect"/>, all the individual components that comprise a complete effect implement this interface.
  /// </summary>
  public interface IEffectPart : INamed
  {
    /// <summary>
    /// Checks if this part belongs to the given effect.
    /// </summary>
    /// <param name="effect">Effect to check against</param>
    /// <returns>True if the effect is the parent of this part, false otherwise.</returns>
    bool IsPartOf(Effect effect);
  }

  /// <summary>
  /// Represents a parameter used by a shader in the effect. Parameters may be uniforms (constant values) or resources (e.g. textures). A parameter
  /// may be a single value or an array of parameters. 
  /// </summary>
  public interface IEffectParameter : IEffectPart
  {
    /// <summary>
    /// Gets the containing constant buffer, if it exists. This will never exist for resource parameters.
    /// </summary>
    IEffectConstantBuffer ContainingBuffer { get; }

    /// <summary>
    /// Gets the elements of the array, if the parameter is one.
    /// </summary>
    EffectParameterCollection Elements { get; }

    /// <summary>
    /// Gets the struct members, if the parameter is a struct type.
    /// </summary>
    EffectParameterCollection StructureMembers { get; }

    /// <summary>
    /// Gets the class of the parameter (e.g. scalar, vector, matrix, object and so on).
    /// </summary>
    EffectParameterClass ParameterClass { get; }

    /// <summary>
    /// Gets the data type of the parameter (e.g. int, float, texture2d and so on).
    /// </summary>
    EffectParameterType ParameterType { get; }

    /// <summary>
    /// Gets the .NET data type of the parameter, may be null (e.g. parameter is a structure).
    /// </summary>
    Type DefaultNetType { get; }

    /// <summary>
    /// Gets the number of columns if the parameter class is not an object. E.g. a Matrix4x4 would have 4 rows and 4 columns.
    /// </summary>
    int ColumnCount { get; }

    /// <summary>
    /// Gets the number of rows if the parameter class is not an object. E.g. a Matrix4x4 would have 4 rows and 4 columns.
    /// </summary>
    int RowCount { get; }

    /// <summary>
    /// Gets the parameter's size in bytes.
    /// </summary>
    int SizeInBytes { get; }

    /// <summary>
    /// Gets the parameter's starting offset in its containing buffer, in bytes.
    /// </summary>
    int StartOffsetInBytes { get; }

    /// <summary>
    /// Gets the index of the element in its parent's array element collection.
    /// </summary>
    int ElementIndex { get; }

    /// <summary>
    /// Gets the index of the member in its parent's struct member collection.
    /// </summary>
    int StructureMemberIndex { get; }

    /// <summary>
    /// Gets if the parameter is an array.
    /// </summary>
    bool IsArray { get; }

    /// <summary>
    /// Gets if the parameter is an element in an array.
    /// </summary>
    bool IsElementInArray { get; }

    /// <summary>
    /// Gets if the parameter is a structure member.
    /// </summary>
    bool IsStructureMember { get; }

    /// <summary>
    /// Gets if the parameter is a uniform constant value and not a resource value.
    /// </summary>
    bool IsValueType { get; }

    /// <summary>
    /// Gets the value of the parameter, if it is a value type.
    /// </summary>
    /// <typeparam name="T">Value type.</typeparam>
    /// <returns>Parameter value.</returns>
    T GetValue<T>() where T : unmanaged;

    /// <summary>
    /// Gets the array of values of the parameter, if it is an array value type.
    /// </summary>
    /// <typeparam name="T">Value type.</typeparam>
    /// <param name="count">Number of values of the array to return.</param>
    /// <returns>Array of parameter values.</returns>
    T[] GetValueArray<T>(int count) where T : unmanaged;

    /// <summary>
    /// Sets the value to the parameter, if it is a value type.
    /// </summary>
    /// <typeparam name="T">Value type.</typeparam>
    /// <param name="value">Value to set.</param>
    void SetValue<T>(in T value) where T : unmanaged;

    /// <summary>
    /// Sets the array of values to the parameter, if it is an array value type. This is a more efficient operation than setting individual value
    /// to each array element individually.
    /// </summary>
    /// <typeparam name="T">Value type.</typeparam>
    /// <param name="values">Values to set.</param>
    void SetValue<T>(params T[] values) where T : unmanaged;

    /// <summary>
    /// Gets the matrix of the parameter, if it is a matrix type.
    /// </summary>
    /// <returns>Matrix value.</returns>
    Matrix GetMatrixValue();

    /// <summary>
    /// Gets the transposed matrix of the parameter, if it is a matrix type.
    /// </summary>
    /// <returns>Transposed matrix value.</returns>
    Matrix GetMatrixValueTranspose();

    /// <summary>
    /// Gets the array of matrices of the parameter, if it is a matrix type.
    /// </summary>
    /// <param name="count">Number of matrices of the array to return.</param>
    /// <returns>Array of matrices.</returns>
    Matrix[] GetMatrixValueArray(int count);

    /// <summary>
    /// Gets the array of transposed matrices of the parameter, if it is an array matrix type.
    /// </summary>
    /// <param name="count">Number of matrices of the array to return.</param>
    /// <returns>Array of transposed matrices.</returns>
    Matrix[] GetMatrixValueArrayTranspose(int count);

    /// <summary>
    /// Sets the matrix to the parameter, if it is a matrix type.
    /// </summary>
    /// <param name="value">Matrix to set.</param>
    void SetValue(in Matrix value);

    /// <summary>
    /// Sets the array of matrices to the parameter, if it is an array matrix type. This is a more efficient operation than setting individual matrices
    /// to each array element individually.
    /// </summary>
    /// <param name="values">Array of matrices to set.</param>
    void SetValue(params Matrix[] values);

    /// <summary>
    /// Transposes the matrix then sets it to the parameter, if it is a matrix type.
    /// </summary>
    /// <param name="value">Matrix to set.</param>
    void SetValueTranspose(in Matrix value);

    /// <summary>
    /// Transposes the array of matrices then sets them to the parameter, if it is an array matrix type. This is a more efficient operation than setting individual matrices
    /// to each array element individually.
    /// </summary>
    /// <param name="values">Array of matrices to set.</param>
    void SetValueTranspose(params Matrix[] values);

    /// <summary>
    /// Gets a shader resource set to the parameter, if it is a resource type.
    /// </summary>
    /// <typeparam name="T">Shader resource type.</typeparam>
    /// <returns>Shader resource set to the parameter. Null is returned if none is set.</returns>
    T GetResource<T>() where T : IShaderResource;

    /// <summary>
    /// Gets an array of shader resources set to the parameter, if it is an array resource type.
    /// </summary>
    /// <typeparam name="T">Shader resource type.</typeparam>
    /// <param name="count">Number of shader resources of the array to return.</param>
    /// <returns>Array of shader resources set to the parameter. The array may have null elements if no resource is set at those indices.</returns>
    T[] GetResourceArray<T>(int count) where T : IShaderResource;

    /// <summary>
    /// Sets a shader resource to the parameter, if it is a resource type.
    /// </summary>
    /// <typeparam name="T">Shader resource type.</typeparam>
    /// <param name="resource">Shader resource to set. Null is an acceptable value.</param>
    void SetResource<T>(T resource) where T : IShaderResource;

    /// <summary>
    /// Sets an array of shader resources to the parameter, if it is an array resource type. This is a more efficient operation than setting individual shader resources 
    /// to each array element individually.
    /// </summary>
    /// <typeparam name="T">Shader resource type.</typeparam>
    /// <param name="resources">Array of shader resources to set. The array must exist, but null values in the array are acceptable.</param>
    void SetResource<T>(params T[] resources) where T : IShaderResource;
  }

  /// <summary>
  /// Represents an effect constant buffer. A constant buffer contains only value type effect parameters
  /// </summary>
  public interface IEffectConstantBuffer : IEffectPart
  {
    /// <summary>
    /// Gets the buffer's total size in bytes.
    /// </summary>
    int SizeInBytes { get; }

    /// <summary>
    /// Gets a collection of parameters that is contained by this buffer.
    /// </summary>
    EffectParameterCollection Parameters { get; }

    /// <summary>
    /// Gets the contents of the buffer as a struct, typically one that represents the entire buffer. The struct type should be padded and aligned correctly.
    /// </summary>
    /// <typeparam name="T">Type of the struct.</typeparam>
    /// <returns>Contents of the buffer returned as a struct</returns>
    T Get<T>() where T : unmanaged;

    /// <summary>
    /// Gets the contents of the buffer as a struct starting at the specified offset, typically one that represents an element in the buffer. The struct type should be 
    /// padded and aligned correctly.
    /// </summary>
    /// <typeparam name="T">Type of the struct.</typeparam>
    /// <param name="offsetInBytes">Start offset to read from in the buffer.</param>
    /// <returns>Contents of the buffer returned as a struct</returns>
    T Get<T>(int offsetInBytes) where T : unmanaged;

    /// <summary>
    /// Gets the contents of the buffer as an array of structs. The struct type should be padded and aligned correctly.
    /// </summary>
    /// <typeparam name="T">Type of the struct.</typeparam>
    /// <param name="count">Number of values to get.</param>
    /// <returns>Array of values from the buffer.</returns>
    T[] GetRange<T>(int count) where T : unmanaged;

    /// <summary>
    /// Gets the contents of the buffer as an array of structs, starting at the specified offset. The struct type should be padded and aligned correctly.
    /// </summary>
    /// <typeparam name="T">Type of the struct.</typeparam>
    /// <param name="offsetInBytes">Start offset to read from in the buffer.</param>
    /// <param name="count">Number of values to read.</param>
    /// <returns>Array of values from the buffer.</returns>
    T[] GetRange<T>(int offsetInBytes, int count) where T : unmanaged;

    /// <summary>
    /// Sets the value to the buffer, typically one that represents the entire buffer. The struct type should be padded and aligned correctly.
    /// </summary>
    /// <typeparam name="T">Type of the struct.</typeparam>
    /// <param name="value">Value to set.</param>
    void Set<T>(in T value) where T : unmanaged;

    /// <summary>
    /// Sets the value to the buffer starting at the specified offset, typically one that represents an element in the buffer. The struct type should be padded and aligned 
    /// correctly.
    /// </summary>
    /// <typeparam name="T">Type of the struct.</typeparam>
    /// <param name="offsetInBytes">Start offset to write to the buffer at.</param>
    /// <param name="value">Value to set.</param>
    void Set<T>(int offsetInBytes, in T value) where T : unmanaged;

    /// <summary>
    /// Sets the array of values to the buffer. The struct type should be padded and aligned 
    /// correctly.
    /// </summary>
    /// <typeparam name="T">Type of the struct.</typeparam>
    /// <param name="values">Array of values to write to the buffer.</param>
    void Set<T>(params T[] values) where T : unmanaged;

    /// <summary>
    /// Sets the array of values to the buffer starting at the specified offset. The struct type should be padded and aligned 
    /// correctly.
    /// </summary>
    /// <typeparam name="T">Type of the struct.</typeparam>
    /// <param name="offsetInBytes">Start offset to write to the buffer at.</param>
    /// <param name="values">Array of values to write to the buffer.</param>
    void Set<T>(int offsetInBytes, params T[] values) where T : unmanaged;
  }

  /// <summary>
  /// Represents a group of shaders. 
  /// </summary>
  public interface IEffectShaderGroup : IEffectPart
  {
    /// <summary>
    /// Gets the index of this shader group in the collection it is contained in.
    /// </summary>
    int ShaderGroupIndex { get; }

    /// <summary>
    /// Binds the set of shaders defined in the group and the resources used by them to the context.
    /// </summary>
    /// <param name="renderContext">Render context to apply to.</param>
    void Apply(IRenderContext renderContext);

    /// <summary>
    /// Queries the shader group if it contains a shader used by the specified shader stage.
    /// </summary>
    /// <param name="shaderStage">Shader stage to query.</param>
    /// <returns>True if the group contains a shader that will be bound to the shader stage, false otherwise.</returns>
    bool ContainsShader(ShaderStage shaderStage);
  }
}
