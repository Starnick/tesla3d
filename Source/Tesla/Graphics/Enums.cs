﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.CompilerServices;

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// Defines the different options for setting render targets/depth buffers.
  /// </summary>
  public enum SetTargetOptions
  {
    /// <summary>
    /// Default behavior, set targets and a depth buffer of the first target (if any).
    /// </summary>
    None = 0,

    /// <summary>
    /// Set no depth buffer when setting render targets.
    /// </summary>
    NoDepthBuffer = 1,

    /// <summary>
    /// Set the depth buffer as read-only so it can be simultaneously set as a shader resource.
    /// </summary>
    ReadOnlyDepthBuffer = 2
  }

  /// <summary>
  /// Defines the different options for shadow mapping.
  /// </summary>
  public enum ShadowMode
  {
    /// <summary>
    /// The object will not be included in casting shadows nor receive shadow maps.
    /// </summary>
    None = 0,

    /// <summary>
    /// The object will be included in casting shadows.
    /// </summary>
    Cast = 1,

    /// <summary>
    /// The object will only receive shadow maps.
    /// </summary>
    Receive = 2,

    /// <summary>
    /// The object will be included in casting shadows and will also receive shadow maps.
    /// </summary>
    CastAndReceive = 3
  }

  /// <summary>
  /// Defines the different transparency options when rendering translucent geometry.
  /// </summary>
  public enum TransparencyMode
  {
    /// <summary>
    /// One sided - where the renderable is drawn normally.
    /// </summary>
    OneSided = 0,

    /// <summary>
    /// Two sided - where the renderable is drawn using a two-pass scheme that will enforce certain render states in order to properly
    /// draw front and back parts of a transparent geometry.
    /// </summary>
    TwoSided = 1
  }

  /// <summary>
  /// Defines the projection mode for a Camera.
  /// </summary>
  public enum ProjectionMode
  {
    /// <summary>
    /// Indiciates the camera should use a perspective projection.
    /// </summary>
    Perspective = 0,

    /// <summary>
    /// Indicates the camera should use an orthographic projection.
    /// </summary>
    Orthographic = 1
  }

  /// <summary>
  /// Enumerates supported types of lights.
  /// </summary>
  public enum LightType
  {
    /// <summary>
    /// A light that represents a source that has a position in space and is omni-directional.
    /// </summary>
    Point = 0,

    /// <summary>
    /// A light that represents a source that has a position in space that emits light in a cone.
    /// </summary>
    Spot = 1,

    /// <summary>
    /// A light that represents a source infinitely away and is purely directional based.
    /// </summary>
    Directional = 2,

    /// <summary>
    /// A custom light defined by user.
    /// </summary>
    Custom = 3
  }

  /// <summary>
  /// Defines general shader compile flags. Some options may not be available on all platforms.
  /// </summary>
  [Flags]
  public enum ShaderCompileFlags
  {
    /// <summary>
    /// No flags set. Compile with moderate optimization.
    /// </summary>
    None = 0,

    /// <summary>
    /// Output debug information with shader code.
    /// </summary>
    Debug = 1,

    /// <summary>
    /// Skip validation of shader code.
    /// </summary>
    SkipValidation = 2,

    /// <summary>
    /// Skip optimization of shader code.
    /// </summary>
    SkipOptimization = 4,

    /// <summary>
    /// Pack matrices in row major order.
    /// </summary>
    PackMatrixRowMajor = 8,

    /// <summary>
    /// Pack matrices in column major order.
    /// </summary>
    PackMatrixColumnMajor = 16,

    /// <summary>
    /// Forces all computations in the shader to occur at partial precision.
    /// </summary>
    PartialPrecision = 32,

    /// <summary>
    /// Disables preshaders. The compiler will not pull out static expressions that are evaluated on the host CPU.
    /// </summary>
    NoPreShader = 256,

    /// <summary>
    /// Hint to compiler to avoid flow control instructions.
    /// </summary>
    AvoidFlowControl = 512,

    /// <summary>
    /// Hint to compiler to not avoid flow control instructions.
    /// </summary>
    PreferFlowControl = 1024,

    /// <summary>
    /// Forces strict compile, which might not allow for legacy syntax.
    /// </summary>
    EnableStrictness = 2048,

    /// <summary>
    /// Enables older shaders to compile to certain targets.
    /// </summary>
    EnableBackwardsCompatibility = 4096,

    /// <summary>
    /// Lowest optimization level, produces slower code but quicker.
    /// </summary>
    OptimizationLevel0 = 16384,

    /// <summary>
    /// Second highest optimization level.
    /// </summary>
    OptimizationLevel3 = 32768,

    /// <summary>
    /// Highest optimization level, produces faster code but slower.
    /// </summary>
    OptimizationLevel2 = 49152,

    /// <summary>
    /// Treat all warnings as errors.
    /// </summary>
    WarningsAreErrors = 262144
  }

  /// <summary>
  /// Defines different types of resources that can be bound to a shader stage.
  /// </summary>
  public enum ShaderResourceType
  {
    /// <summary>
    /// Unknown type.
    /// </summary>
    Unknown = 0,

    /// <summary>
    /// A buffer resource, which is a 1D resource.
    /// </summary>
    Buffer = 1,

    /// <summary>
    /// A Texture1D resource.
    /// </summary>
    Texture1D = 2,

    /// <summary>
    /// A Texture1DArray resource.
    /// </summary>
    Texture1DArray = 3,

    /// <summary>
    /// A Texture2D resource.
    /// </summary>
    Texture2D = 4,

    /// <summary>
    /// A Texture2DArray resource.
    /// </summary>
    Texture2DArray = 5,

    /// <summary>
    /// A MultiSampled Texture2D resource.
    /// </summary>
    Texture2DMS = 6,

    /// <summary>
    /// A MultiSampled Texture2DArray resource.
    /// </summary>
    Texture2DMSArray = 7,

    /// <summary>
    /// A Texture3D resource.
    /// </summary>
    Texture3D = 8,

    /// <summary>
    /// A TextureCube resource.
    /// </summary>
    TextureCube = 9,

    /// <summary>
    /// A TextureCubeArray resource.
    /// </summary>
    TextureCubeArray = 10,

    /// <summary>
    /// A MultiSampled TextureCube resource.
    /// </summary>
    TextureCubeMS = 11,

    /// <summary>
    /// A MultiSampled TextureCubeArray resource.
    /// </summary>
    TextureCubeMSArray = 12,

    /// <summary>
    /// A SamplerState resource.
    /// </summary>
    SamplerState = 13
  }

  /// <summary>
  /// Enumerates different shader stage types in the programmable shader pipeline.
  /// </summary>
  public enum ShaderStage
  {
    /// <summary>
    /// The vertex shader stage, which processes individual vertices.
    /// </summary>
    VertexShader = 0,

    /// <summary>
    /// The hull shader stage, which produces a geometry patch for tessellation.
    /// </summary>
    HullShader = 1,

    /// <summary>
    /// The  domain shader stage, which calculates vertex positions of subdivided points in the output patch for tessellation.
    /// </summary>
    DomainShader = 2,

    /// <summary>
    /// The geometry shader stage, which processes entire primitives.
    /// </summary>
    GeometryShader = 3,

    /// <summary>
    /// The pixel shader stage, which processes fragments of rasterized primitives.
    /// </summary>
    PixelShader = 4,

    /// <summary>
    /// The compute shader stage, a specialized shader that can do general computations on the GPU.
    /// </summary>
    ComputeShader = 5
  }

  /// <summary>
  /// Flags for for enforcing render states on the render context. When a state is enforced, state-setting for that type
  /// is ignored.
  /// </summary>
  [Flags]
  public enum EnforcedRenderState
  {
    /// <summary>
    /// No states enforced.
    /// </summary>
    None = 0,

    /// <summary>
    /// BlendState is enforced.
    /// </summary>
    BlendState = 1,

    /// <summary>
    /// RasterizerState is enforced.
    /// </summary>
    RasterizerState = 2,

    /// <summary>
    /// DepthStencilState is enforced.
    /// </summary>
    DepthStencilState = 4,

    /// <summary>
    /// All render states are enforced.
    /// </summary>
    All = BlendState | RasterizerState | DepthStencilState
  }

  /// <summary>
  /// Stencil buffer operation enumeration.
  /// </summary>
  public enum StencilOperation
  {
    /// <summary>
    /// Do not update the stencil-buffer entry. This is the default value.
    /// </summary>
    Keep = 0,

    /// <summary>
    /// Sets the stencil-buffer entry to zero.
    /// </summary>
    Zero = 1,

    /// <summary>
    /// Replaces the stencil-buffer entry with the reference value.
    /// </summary>
    Replace = 2,

    /// <summary>
    /// Increments the stencil-buffer entry, wrapping to zero if the new value
    /// exceeds the maximum value.
    /// </summary>
    Increment = 3,

    /// <summary>
    /// Decrements the stencil-buffer entry, wrapping to the maximum value if the new
    /// value is less than zero.
    /// </summary>
    Decrement = 4,

    /// <summary>
    /// Increments the stencil-buffer entry, clamping to the maximum value.
    /// </summary>
    IncrementAndClamp = 5,

    /// <summary>
    /// Decrements the stencil-buffer entry, clamping to zero.
    /// </summary>
    DecrementAndClamp = 6,

    /// <summary>
    /// Inverts the bits in the stencil-buffer entry.
    /// </summary>
    Invert = 7
  }

  /// <summary>
  /// Comparison function enumeration for alpha, stencil, sampling or depth-buffer testing.
  /// </summary>
  public enum ComparisonFunction
  {
    /// <summary>
    /// Always pass the test.
    /// </summary>
    Always = 0,

    /// <summary>
    /// Always fail the test.
    /// </summary>
    Never = 1,

    /// <summary>
    /// Accept new pixel if its value is less than the value of the current pixel.
    /// </summary>
    Less = 2,

    /// <summary>
    /// Accept new pixel if its value is less than or equal to the value of the current pixel.
    /// </summary>
    LessEqual = 3,

    /// <summary>
    /// Accept the new pixel if its value is equal to the value of the current pixel.
    /// </summary>
    Equal = 4,

    /// <summary>
    /// Accept the new pixel if its value is greater than or equal to the value of the current pixel.
    /// </summary>
    GreaterEqual = 5,

    /// <summary>
    /// Accept the new pixel if its value is greater than the value of the current pixel.
    /// </summary>
    Greater = 6,

    /// <summary>
    /// Accept the new pixel if its value is not equal to the current pixel.
    /// </summary>
    NotEqual = 7
  }

  /// <summary>
  /// Defines the channels that can be written to
  /// a render target's color buffer.
  /// </summary>
  [Flags]
  public enum ColorWriteChannels
  {
    /// <summary>
    /// Write to no channels.
    /// </summary>
    None = 0,

    /// <summary>
    /// Only write to the red (R) channel.
    /// </summary>
    Red = 1,

    /// <summary>
    /// Only write to the green (G) channel.
    /// </summary>
    Green = 2,

    /// <summary>
    /// Only write to the blue (B) channel.
    /// </summary>
    Blue = 4,

    /// <summary>
    /// Only write to the alpha (A) channel.
    /// </summary>
    Alpha = 8,

    /// <summary>
    /// Write to all (RGBA) channels.
    /// </summary>
    All = 15
  }

  /// <summary>
  /// Defines the render state type.
  /// </summary>
  public enum RenderStateType
  {
    /// <summary>
    /// State is a BlendState.
    /// </summary>
    BlendState = 0,

    /// <summary>
    /// State is a DepthStencilState.
    /// </summary>
    DepthStencilState = 1,

    /// <summary>
    /// State is a RasterizerState.
    /// </summary>
    RasterizerState = 2,

    /// <summary>
    /// State is a SamplerState.
    /// </summary>
    SamplerState = 3
  }

  /// <summary>
  /// Defines how to combine source and destination colors for
  /// blending operations.
  /// </summary>
  public enum BlendFunction
  {
    /// <summary>
    /// Destination is added to the source. Result = (SourceColor * SourceBlend)
    /// + (DestinationColor * DestinationBlend).
    /// </summary>
    Add = 0,

    /// <summary>
    /// Destination is subtracted from the source. Result = (SourceColor * SourceBlend)
    /// - (DestinationColor * DestinationBlend).
    /// </summary>
    Subtract = 1,

    /// <summary>
    /// The source is subtracted from the destination. Result = (DestinationColor * DestinationBlend)
    /// - (SourceColor * SourceBlend).
    /// </summary>
    ReverseSubtract = 2,

    /// <summary>
    /// The result is the minimum of source and destination. Result = min(SourceColor * SourceBlend),
    /// (DestinationColor * DestinationBlend)).
    /// </summary>
    Minimum = 3,

    /// <summary>
    /// The result is the maximum of source and destination. Result = max(SourceColor * SourceBlend),
    /// (DestinationColor * DestinationBlend)).
    /// </summary>
    Maximum = 4
  }

  /// <summary>
  /// Defines blending factors for source and destination colors.
  /// </summary>
  public enum Blend
  {
    /// <summary>
    /// Each color component is multiplied by (0, 0, 0, 0).
    /// </summary>
    Zero = 0,

    /// <summary>
    /// Each color component is multiplied by (1, 1, 1, 1).
    /// </summary>
    One = 1,

    /// <summary>
    /// Each color component is multiplied by the source color: (Rs, Gs, Bs, As).
    /// </summary>
    SourceColor = 2,

    /// <summary>
    /// Each color component is multiplied by the inverse source color: (1 - Rs, 1 - Gs, 1 - Bs, 1 - As).
    /// </summary>
    InverseSourceColor = 3,

    /// <summary>
    /// Each color component is multiplied by the alpha of the source color: (As, As, As, As).
    /// </summary>
    SourceAlpha = 4,

    /// <summary>
    /// Each color component is multiplied by the inverse alpha of the source color: (1 - As, 1 - As, 1 - As, 1 - As).
    /// </summary>
    InverseSourceAlpha = 5,

    /// <summary>
    /// Each color component is multiplied by the destination color: (Rd, Gd, Bd, Ad).
    /// </summary>
    DestinationColor = 6,

    /// <summary>
    /// Each color component is multiplied by the inverse destination color: (1 - Rd,1 - Gd,1 - Bd,1 - Ad).
    /// </summary>
    InverseDestinationColor = 7,

    /// <summary>
    /// Each color component is multiplied by the alpha of the destination color: (Ad, Ad, Ad, Ad).
    /// </summary>
    DestinationAlpha = 8,

    /// <summary>
    /// Each color component is multiplied by the inverse alpha of the destination color: (1 - Ad,1 - Ad,1 - Ad,1 - Ad).
    /// </summary>
    InverseDestinationAlpha = 9,

    /// <summary>
    /// Each color component is multiplied by a constant blend factor.
    /// </summary>
    BlendFactor = 10,

    /// <summary>
    /// Each color component is muliplied by the inverse of a constant blend factor.
    /// </summary>
    InverseBlendFactor = 11,

    /// <summary>
    /// Each color component is multiplied by either the alpha of the source color
    /// or the inverse of the alpha of the source color, whichever is greater: (f, f, f, 1) where
    /// f = min(A, 1 - Ad)
    /// </summary>
    SourceAlphaSaturation = 12
  }

  /// <summary>
  /// Determines how data is used when a render target is activated.
  /// </summary>
  public enum RenderTargetUsage
  {
    /// <summary>
    /// Always clear the contents of the target.
    /// </summary>
    DiscardContents = 0,

    /// <summary>
    /// Always keep the contents of the target
    /// </summary>
    PreserveContents = 1,

    /// <summary>
    /// Use the platform default, which is either discard or preserve.
    /// </summary>
    PlatformDefault = 2
  }

  /// <summary>
  /// Describes if presenting should enable or disable VSync.
  /// </summary>
  public enum PresentInterval
  {
    /// <summary>
    /// Present to the screen without waiting for vertical retrace.
    /// </summary>
    Immediate = 0,

    /// <summary>
    /// Wait for one vertical retrace period.
    /// </summary>
    One = 1,

    /// <summary>
    /// Wait for two vertical retrace periods.
    /// </summary>
    Two = 2
  }

  /// <summary>
  /// Defines the display orientation.
  /// </summary>
  [Flags]
  public enum DisplayOrientation
  {
    /// <summary>
    /// Default orientation.
    /// </summary>
    Default = 0,

    /// <summary>
    /// Display is rotated CCW 90 degrees into a landscape orientation (width > height).
    /// </summary>
    LandscapeLeft = 1,

    /// <summary>
    /// Display is rotated CW 90 degrees into a landscape orientation (width > height).
    /// </summary>
    LandscapeRight = 2,

    /// <summary>
    /// Display is oriented where height is greater than width.
    /// </summary>
    Portrait = 4
  }

  /// <summary>
  /// Defines the type of an effect parameter.
  /// </summary>
  public enum EffectParameterType
  {
    /// <summary>
    /// Parameter is unknown.
    /// </summary>
    Unknown = 0,

    /// <summary>
    /// Parameter is a void pointer.
    /// </summary>
    Void = 1,

    /// <summary>
    /// Parameter is a boolean.
    /// </summary>
    Bool = 2,

    /// <summary>
    /// Parameter is a 32-bit integer.
    /// </summary>
    Int32 = 3,

    /// <summary>
    /// Parameter is a 32-bit floating point.
    /// </summary>
    Single = 4,

    /// <summary>
    /// Parameter is a string.
    /// </summary>
    String = 5,

    /// <summary>
    /// Parameter is a texture.
    /// </summary>
    Texture = 6,

    /// <summary>
    /// Parameter is a 1D texture.
    /// </summary>
    Texture1D = 7,

    /// <summary>
    /// Parameter is a 1D array texture.
    /// </summary>
    Texture1DArray = 8,

    /// <summary>
    /// Parameter is a 2D texture.
    /// </summary>
    Texture2D = 9,

    /// <summary>
    /// Parameter is a 2D array texture.
    /// </summary>
    Texture2DArray = 10,

    /// <summary>
    /// Parameter is a multisampled 2D texture.
    /// </summary>
    Texture2DMS = 11,

    /// <summary>
    /// Parameter is a multisampled 2D texture.
    /// </summary>
    Texture2DMSArray = 12,

    /// <summary>
    /// Parameter is a 3D texture.
    /// </summary>
    Texture3D = 13,

    /// <summary>
    /// Parameter is a cube texture.
    /// </summary>
    TextureCube = 14,

    /// <summary>
    /// Parameter is a cube array texture.
    /// </summary>
    TextureCubeArray = 15,

    /// <summary>
    /// Parameter is a sampler state.
    /// </summary>
    SamplerState = 16,

    /// <summary>
    /// Parameter is a buffer, similar to a 1D Texture in terms of formats, e.g. each element is a 1-4 component vector data type.
    /// </summary>
    Buffer = 17,

    /// <summary>
    /// Parameter is a ByteAddress Buffer, where the data is raw and loaded as multiples of size(uint).
    /// </summary>
    ByteAddressBuffer = 18,

    /// <summary>
    /// Parameter is a Structured buffer, where the data is tightly packed and defined by the shader. The parameter will
    /// have a layout structure of a single element.
    /// </summary>
    StructuredBuffer = 19,
  }

  /// <summary>
  /// Defines the class of an effect parameter.
  /// </summary>
  public enum EffectParameterClass
  {
    /// <summary>
    /// Parameter is a scalar value.
    /// </summary>
    Scalar = 0,

    /// <summary>
    /// Parameter is a vector value.
    /// </summary>
    Vector = 1,

    /// <summary>
    /// Parameter is a row-major matrix value.
    /// </summary>
    MatrixRows = 2,

    /// <summary>
    /// Parameter is a column-major matrix value.
    /// </summary>
    MatrixColumns = 3,

    /// <summary>
    /// Parameter is either a texture, string, or other resource type.
    /// </summary>
    Object = 4,

    /// <summary>
    /// Parameter is a struct.
    /// </summary>
    Struct = 5
  }

  /// <summary>
  /// Defines how primitives should be filled.
  /// </summary>
  public enum FillMode
  {
    /// <summary>
    /// The entire primitive should be drawn and filled.
    /// </summary>
    Solid = 0,

    /// <summary>
    /// Only the edges of primitives should be drawn.
    /// </summary>
    WireFrame = 1
  }

  /// <summary>
  /// Defines the how vertices are wound. This defines the criteria
  /// to define what are the back and front of a primitive.
  /// </summary>
  public enum VertexWinding
  {
    /// <summary>
    /// Clockwise vertices are defined as Front facing. 
    /// </summary>
    Clockwise = 0,

    /// <summary>
    /// CounterClockwise vertices are defined as Front facing. 
    /// </summary>
    CounterClockwise = 1
  }

  /// <summary>
  /// Defines how a primitive should be culled.
  /// </summary>
  public enum CullMode
  {
    /// <summary>
    /// No primitive is culled.
    /// </summary>
    None = 0,

    /// <summary>
    /// Front facing primitives are culled.
    /// </summary>
    Front = 1,

    /// <summary>
    /// Back facing primitives are culled.
    /// </summary>
    Back = 2
  }

  /// <summary>
  /// Options to clear rendertargets and the back buffer.
  /// </summary>
  [Flags]
  public enum ClearOptions
  {
    /// <summary>
    /// Clear depth buffer.
    /// </summary>
    Depth = 1,

    /// <summary>
    /// Clear stencil buffer.
    /// </summary>
    Stencil = 2,

    /// <summary>
    /// Clear the render target/color buffer.
    /// </summary>
    Target = 4,

    /// <summary>
    /// Clears the render target, depth, and stencil buffers.
    /// </summary>
    All = Target | Depth | Stencil
  }

  /// <summary>
  /// Format for a vertex element in the vertex buffer.
  /// </summary>
  public enum VertexFormat
  {
    /// <summary>
    /// A vector containing four 8-bit unsigned bytes in normalized format (interpreted as normalized floating-point value in range of [0, 1]). Commonly used for RGBA color. 
    /// </summary>
    Color = 0,

    /// <summary>
    /// A 16-bit unsigned integer.
    /// </summary>
    UShort = 1,

    /// <summary>
    /// A vector containing two 16-bit unsigned integers for a total size of 32 bits.
    /// </summary>
    UShort2 = 2,

    /// <summary>
    /// A vector containing four 16-bit unsigned integers for a total size of 64 bits.
    /// </summary>
    UShort4 = 3,

    /// <summary>
    /// A 16-bit unsigned integer in normalized format (interpreted as normalized floating-point value in range of [0, 1]).
    /// </summary>
    NormalizedUShort = 4,

    /// <summary>
    /// A vector containing two 16-bit unsigned integers in normalized format (interpreted as normalized floating-point value in range of [0, 1]) for a total size of 32 bits.
    /// </summary>
    NormalizedUShort2 = 5,

    /// <summary>
    /// A vector containing four 16-bit unsigned integers in normalized format (interpreted as normalized floating-point value in range of [0, 1]) for a total size of 64 bits.
    /// </summary>
    NormalizedUShort4 = 6,

    /// <summary>
    /// A 16-bit integer.
    /// </summary>
    Short = 7,

    /// <summary>
    /// A vector containing two 16-bit integers for a total size of 32 bits.
    /// </summary>
    Short2 = 8,

    /// <summary>
    /// A vector containing four 16-bit integers for a total size of 64 bits.
    /// </summary>
    Short4 = 9,

    /// <summary>
    /// A 16-bit integer in normalized format (interpreted as normalized floating-point value in range of [-1, 1]).
    /// </summary>
    NormalizedShort = 10,

    /// <summary>
    /// A vector containing two 16-bit integers in normalized format (interpreted as normalized floating-point value in range of [-1, 1]) for a total size of 32 bits.
    /// </summary>
    NormalizedShort2 = 11,

    /// <summary>
    /// A vector containing four 16-bit integers in normalized format (interpreted as normalized floating-point value in range of [-1, 1]) for a total size of 64 bits.
    /// </summary>
    NormalizedShort4 = 12,

    /// <summary>
    /// A 32-bit unsigned integer.
    /// </summary>
    UInt = 13,

    /// <summary>
    /// A vector containing two 32-bit unsigned integers for a total size of 64 bits.
    /// </summary>
    UInt2 = 14,

    /// <summary>
    /// A vector containing three 32-bit unsigned integers for a total size of 96 bits.
    /// </summary>
    UInt3 = 15,

    /// <summary>
    /// A vector containing four 32-bit unsigned integers for a total size of 128 bits.
    /// </summary>
    UInt4 = 16,

    /// <summary>
    /// A 32-bit integer.
    /// </summary>
    Int = 17,

    /// <summary>
    /// A vector containing two 32-bit integers for a total size of 64 bits.
    /// </summary>
    Int2 = 18,

    /// <summary>
    /// A vector containing three 32-bit integers for a total size of 96 bits.
    /// </summary>
    Int3 = 19,

    /// <summary>
    /// A vector containing four 32-bit integers for a total size of 128 bits.
    /// </summary>
    Int4 = 20,

    /// <summary>
    /// A 16-bit float.
    /// </summary>
    Half = 21,

    /// <summary>
    /// A vector containing two 16-bit floats for a total size of 32 bits.
    /// </summary>
    Half2 = 22,

    /// <summary>
    /// A vector containing four 16-bit floats for a total size of 64 bits.
    /// </summary>
    Half4 = 23,

    /// <summary>
    /// A 32-bit float.
    /// </summary>
    Float = 24,

    /// <summary>
    /// A vector containing two 32-bit floats for a total size of 64 bits.
    /// </summary>
    Float2 = 25,

    /// <summary>
    /// A vector containing three 32-bit floats for a total size of 96 bits.
    /// </summary>
    Float3 = 26,

    /// <summary>
    /// A vector containing four 32-bit floats for a total size of 128 bits.
    /// </summary>
    Float4 = 27
  }

  /// <summary>
  /// Enumeration for vertex element usage, these correspond
  /// directly to the semantics of vertex-shader inputs. There can be a certain
  /// level of disconnect of these semantics and actual intent, since texture coordinates can
  /// be used for custom data types.
  /// </summary>
  public enum VertexSemantic
  {
    /// <summary>
    /// Vertex position info in object space.
    /// </summary>
    Position = 0,

    /// <summary>
    /// Vertex color.
    /// </summary>
    Color = 1,

    /// <summary>
    /// Vertex texture coordinate. Often used for user-defined data also.
    /// </summary>
    TextureCoordinate = 2,

    /// <summary>
    /// Vertex normal.
    /// </summary>
    Normal = 3,

    /// <summary>
    /// Vertex tangent.
    /// </summary>
    Tangent = 4,

    /// <summary>
    /// Vertex bitangent.
    /// </summary>
    Bitangent = 5,

    /// <summary>
    /// Blending indices for skinning.
    /// </summary>
    BlendIndices = 6,

    /// <summary>
    /// Blending weights for skinning.
    /// </summary>
    BlendWeight = 7,

    /// <summary>
    /// Point size for point sprites.
    /// </summary>
    PointSize = 8,

    /// <summary>
    /// User defined. For text semantics appears as "USERDEFINED".
    /// </summary>
    UserDefined = 9
  }

  /// <summary>
  /// Options for writing data to a hardware resource.
  /// </summary>
  public enum DataWriteOptions
  {
    /// <summary>
    /// Default writing mode. Buffer may be overwritten using this.
    /// </summary>
    None = 0,

    /// <summary>
    /// This writing mode will discard all contents of the buffer. Only applicable for Dynamic 
    /// buffers.
    /// </summary>
    Discard = 1,

    /// <summary>
    /// This writing mode will not overwrite existing contents of the buffer. Only applicable
    /// for dynamic buffers.
    /// </summary>
    NoOverwrite = 2
  }

  /// <summary>
  /// Resource usage enumeration, defines how the resource can be accessed by the GPU/CPU.
  /// </summary>
  public enum ResourceUsage
  {
    /// <summary>
    /// Resource is GPU-accessible (read/write) only, the default usage.
    /// </summary>
    Static = 0,

    /// <summary>
    /// Resource is write-accessible by CPU and read-accessible by GPU. Typically for
    /// resources that update every frame.
    /// </summary>
    Dynamic = 1,

    /// <summary>
    /// Resource is GPU-read only, meaning once created it can never change.
    /// </summary>
    Immutable = 2
  }

  /// <summary>
  /// Defines additional ways a resource can be bound to the graphics pipeline.
  /// </summary>
  [Flags]
  public enum ResourceBindFlags
  {
    /// <summary>
    /// No additional binding specified.
    /// </summary>
    Default = 0,

    /// <summary>
    /// Allow the resource to be bound as a shader input, e.g. <see cref="IShaderResource"/>. Some objects will
    /// always be bound as a shader resource so this flag may be ignored. Other objects may optionally be bound
    /// as a shader resource, if possible, but do not by default to allow the driver to optimize the resource.
    /// </summary>
    ShaderResource = 1,

    /// <summary>
    /// Allow the resource to bound as an unordered access, allowing it to be read/written to from multiple threads simultaneously
    /// using atomic functions.
    /// </summary>
    UnorderedAccess = 2
  }

  /// <summary>
  /// Addressing mode for textures.
  /// </summary>
  public enum TextureAddressMode
  {
    /// <summary>
    /// Tile the texture at every integer junction. E.g. For u values
    /// between 0 and 2, the texture is repeated twice. No mirroring is performed.
    /// </summary>
    Wrap = 0,

    /// <summary>
    /// Texture coordinates are clamped to the range [0.0, 1.0]
    /// </summary>
    Clamp = 1,

    /// <summary>
    /// Texture coordinates outside the range [0.0, 1.0] are set to the border color specified by a currently bound sampler state.
    /// </summary>
    Border = 2,

    /// <summary>
    /// Similar to wrap except the texture is flipped at every integer junction. E.g. for
    /// u values between 0 and 1, the texture is addressed normally but between 1 and 2,
    /// it is flipped and so on.
    /// </summary>
    Mirror = 3,

    /// <summary>
    /// Similar to mirror and clamp. Takes the absolute value of the texture coordinate (thus mirroring around 0) 
    /// and clamps to the maximum value
    /// </summary>
    MirrorOnce = 4
  }

  /// <summary>
  /// Enumeration for texture dimensions.
  /// </summary>
  public enum TextureDimension
  {
    /// <summary>
    /// One dimensional texture.
    /// </summary>
    One = 0,

    /// <summary>
    /// Two dimensional texture.
    /// </summary>
    Two = 1,

    /// <summary>
    /// Three dimensional texture.
    /// </summary>
    Three = 2,

    /// <summary>
    /// An array of 6 two dimensional textures where each texture is a face of a cube.
    /// </summary>
    Cube = 3
  }

  /// <summary>
  /// Enumeration for valid filtering schemes involving shrinking (minify), expanding (magnify),
  /// or filtering between mipmap levels.
  /// </summary>
  public enum TextureFilter
  {
    /// <summary>
    /// Use point filtering.
    /// </summary>
    Point = 0,

    /// <summary>
    /// Use point filtering for min/mag and linear filtering for mip.
    /// </summary>
    PointMipLinear = 1,

    /// <summary>
    /// Use linear filtering.
    /// </summary>
    Linear = 2,

    /// <summary>
    /// Use linear filtering for min/mag and point filtering for mip.
    /// </summary>
    LinearMipPoint = 3,

    /// <summary>
    /// Use linear filtering for min and mip, but point for mag.
    /// </summary>
    MinLinearMagPointMipLinear = 4,

    /// <summary>
    /// Use linear filtering for min, and point filtering for both mag and mip.
    /// </summary>
    MinLinearMagPointMipPoint = 5,

    /// <summary>
    /// Use point filtering for min, and linear filtering for both mag and mip.
    /// </summary>
    MinPointMagLinearMipLinear = 6,

    /// <summary>
    /// Use point filtering for min and mip, but point for mip.
    /// </summary>
    MinPointMagLinearMipPoint = 7,

    /// <summary>
    /// Use anisotropic filtering.
    /// </summary>
    Anisotropic = 8,

    /// <summary>
    /// Use point filtering. Compare the result to the comparison value.
    /// </summary>
    Comparison_Point = 9,

    /// <summary>
    /// Use point filtering for min/mag and linear filtering for mip. Compare the result to the comparison value.
    /// </summary>
    Comparison_PointMipLinear = 10,

    /// <summary>
    /// Use linear filtering. Compare the result to the comparison value.
    /// </summary>
    Comparison_Linear = 11,

    /// <summary>
    /// Use linear filtering for min/mag and point filtering for mip. Compare the result to the comparison value.
    /// </summary>
    Comparison_LinearMipPoint = 12,

    /// <summary>
    /// Use linear filtering for min and mip, but point for mag. Compare the result to the comparison value.
    /// </summary>
    Comparison_MinLinearMagPointMipLinear = 13,

    /// <summary>
    /// Use linear filtering for min, and point filtering for both mag and mip. Compare the result to the comparison value.
    /// </summary>
    Comparison_MinLinearMagPointMipPoint = 14,

    /// <summary>
    /// Use point filtering for min, and linear filtering for both mag and mip. Compare the result to the comparison value.
    /// </summary>
    Comparison_MinPointMagLinearMipLinear = 15,

    /// <summary>
    /// Use point filtering for min and mip, but point for mip. Compare the result to the comparison value.
    /// </summary>
    Comparison_MinPointMagLinearMipPoint = 16,

    /// <summary>
    /// Use anisotropic filtering. Compare the result to the comparison value.
    /// </summary>
    Comparison_Anisotropic = 17
  }

  /// <summary>
  /// Enumeration of the faces of a CubeMap.
  /// </summary>
  public enum CubeMapFace
  {
    /// <summary>
    /// Face on the +X axis
    /// </summary>
    PositiveX = 0,

    /// <summary>
    /// Face on the -X axis
    /// </summary>
    NegativeX = 1,

    /// <summary>
    /// Face on the +Y axis
    /// </summary>
    PositiveY = 2,

    /// <summary>
    /// Face on the -Y axis
    /// </summary>
    NegativeY = 3,

    /// <summary>
    /// Face on the +Z axis
    /// </summary>
    PositiveZ = 4,

    /// <summary>
    /// Face on the -Z axis
    /// </summary>
    NegativeZ = 5
  }

  /// <summary>
  /// Defines a surface's encoding format.
  /// </summary>
  public enum SurfaceFormat
  {
    /// <summary>
    /// Unsigned 32-Bit RGB pixel format with alpha, 8 bits per channel.
    /// </summary>
    Color = 0,

    /// <summary>
    /// Unsigned 32-bit BGR pixel format with alpha, 8 bits per channel. This differs from the default color format where 
    /// the R and B components are swapped.
    /// </summary>
    BGRColor = 1,

    /// <summary>
    /// Unsigned 16-bit BGR pixel format with 5 bits for blue, 6 bits for green, and 5 bits for red.
    /// </summary>
    BGR565 = 2,

    /// <summary>
    /// Unsigned 16-bit BGRA pixel format with 5 bits for r,g,b channels and 1 bit for alpha.
    /// </summary>
    BGRA5551 = 3,

    /// <summary>
    /// DXT1 compression texture format. Surface dimensions must be in multiples of 4.
    /// </summary>
    DXT1 = 4,

    /// <summary>
    /// DXT3 compression texture format. Surface dimensions must be in multiples of 4.
    /// </summary>
    DXT3 = 5,

    /// <summary>
    /// DXT5 compression texture format. Surface dimensions must be in multiples of 4.
    /// </summary>
    DXT5 = 6,

    /// <summary>
    /// Unsigned 32-bit RGBA pixel format with 10 bits for r,g,b, channels and 2 bits for alpha.
    /// </summary>
    RGBA1010102 = 7,

    /// <summary>
    /// Unsigned 32-bit RG pixel format using 16 bits for red and green.
    /// </summary>
    RG32 = 8,

    /// <summary>
    /// Unsigned 64-bit RGBA pixel format using 16 bit for r,g,b,a channels.
    /// </summary>
    RGBA64 = 9,

    /// <summary>
    /// Unsigned 8-bit alpha.
    /// </summary>
    Alpha8 = 10,

    /// <summary>
    /// IEEE 32-bit float format using 32 bits for the red channel.
    /// </summary>
    Single = 11,

    /// <summary>
    /// IEEE 64-bit float format using 32 bits for red and green.
    /// </summary>
    Vector2 = 12,

    /// <summary>
    /// IEEE 96-bit float format using 32 bits for red, green, blue.
    /// </summary>
    Vector3 = 13,

    /// <summary>
    /// IEEE 128-bit float format using 32 bits for r,g,b,a channels.
    /// </summary>
    Vector4 = 14
  }

  /// <summary>
  /// Defines the format of a RenderTarget's depth buffer.
  /// </summary>
  public enum DepthFormat
  {
    /// <summary>
    /// No depth buffer to be created.
    /// </summary>
    None = 0,

    /// <summary>
    /// A depth buffer that holds 16-bit depth data.
    /// </summary>
    Depth16 = 1,

    /// <summary>
    /// A depth buffer that holds 32-bit data, where 24 bits are for depth and 8 are for stencil.
    /// </summary>
    Depth24Stencil8 = 2,

    /// <summary>
    /// A depth buffer that holds 32-bit floating point depth data.
    /// </summary>
    Depth32 = 3,

    /// <summary>
    /// A depth buffer that holds 32-bit floating point depth data with an additional 8 bits for stencil.
    /// </summary>
    Depth32Stencil8 = 4
  }

  /// <summary>
  /// Defines the topology of mesh data.
  /// </summary>
  public enum PrimitiveType
  {
    /// <summary>
    /// Vertex data is defined as a list of triangles, where every three new vertices make up a triangle.
    /// </summary>
    TriangleList = 0,

    /// <summary>
    /// Vertex data is defined as a list of triangles where every two new vertices, and a vertex from the 
    /// previous triangle make up a triangle.
    /// </summary>
    TriangleStrip = 1,

    /// <summary>
    /// Vertex data is defined as a list of lines where every two new vertices make up a line.
    /// </summary>
    LineList = 2,

    /// <summary>
    /// Vertex data is defined as a list of lines where every one new vertex and a vertex from the previous line
    /// make up a new line.
    /// </summary>
    LineStrip = 3,

    /// <summary>
    /// Vertex data is defined as a list of points.
    /// </summary>
    PointList = 4
  }

  /// <summary>
  /// Specifies which components are used in a GPU register.
  /// </summary>
  [Flags]
  public enum RegisterComponentUsage
  {
    None = 0,
    X = 1,
    Y = 2,
    Z = 4,
    W = 8,
    All = X | Y | Z | W
  }

  /// <summary>
  /// Specifies the type of data in a GPU register.
  /// </summary>
  public enum RegisterComponentType
  {
    Unknown = 0,
    UInt = 1,
    SInt = 2,
    Float = 3
  }

  /// <summary>
  /// Extension methods for graphic enumerations.
  /// </summary>
  public static class GraphicEnumExtensions
  {
    /// <summary>
    /// Classifies the type of GPU register that would hold the <see cref="VertexFormat"/>.
    /// </summary>
    /// <param name="format">Incoming vertex format.</param>
    /// <param name="componentType">Type of data (after expansion/conversion).</param>
    /// <param name="componentUsage">Flags for which components are used.</param>
    public static void ClassifyRegister(this VertexFormat format, out RegisterComponentType componentType, out RegisterComponentUsage componentUsage)
    {
      switch (format)
      {
        case VertexFormat.Int4:
        case VertexFormat.Short4:
          componentUsage = RegisterComponentUsage.All;
          componentType = RegisterComponentType.SInt;
          break;
        case VertexFormat.Int3:
          componentUsage = RegisterComponentUsage.X | RegisterComponentUsage.Y | RegisterComponentUsage.Z;
          componentType = RegisterComponentType.SInt;
          break;
        case VertexFormat.Int2:
        case VertexFormat.Short2:
          componentUsage = RegisterComponentUsage.X | RegisterComponentUsage.Y;
          componentType = RegisterComponentType.SInt;
          break;
        case VertexFormat.Int:
        case VertexFormat.Short:
          componentUsage = RegisterComponentUsage.X;
          componentType = RegisterComponentType.SInt;
          break;
        case VertexFormat.UInt4:
        case VertexFormat.UShort4:
          componentUsage = RegisterComponentUsage.All;
          componentType = RegisterComponentType.UInt;
          break;
        case VertexFormat.UInt3:
          componentUsage = RegisterComponentUsage.X | RegisterComponentUsage.Y | RegisterComponentUsage.Z;
          componentType = RegisterComponentType.UInt;
          break;
        case VertexFormat.UInt2:
        case VertexFormat.UShort2:
          componentUsage = RegisterComponentUsage.X | RegisterComponentUsage.Y;
          componentType = RegisterComponentType.UInt;
          break;
        case VertexFormat.UInt:
        case VertexFormat.UShort:
          componentUsage = RegisterComponentUsage.X;
          componentType = RegisterComponentType.UInt;
          break;
        case VertexFormat.Color:
        case VertexFormat.Float4:
        case VertexFormat.Half4:
        case VertexFormat.NormalizedShort4:
        case VertexFormat.NormalizedUShort4:
          componentUsage = RegisterComponentUsage.All;
          componentType = RegisterComponentType.Float;
          break;
        case VertexFormat.Float3:
          componentUsage = RegisterComponentUsage.X | RegisterComponentUsage.Y | RegisterComponentUsage.Z;
          componentType = RegisterComponentType.Float;
          break;
        case VertexFormat.Float2:
        case VertexFormat.Half2:
        case VertexFormat.NormalizedShort2:
        case VertexFormat.NormalizedUShort2:
          componentUsage = RegisterComponentUsage.X | RegisterComponentUsage.Y;
          componentType = RegisterComponentType.Float;
          break;
        case VertexFormat.Float:
        case VertexFormat.Half:
        case VertexFormat.NormalizedShort:
        case VertexFormat.NormalizedUShort:
        default:
          componentUsage = RegisterComponentUsage.X;
          componentType = RegisterComponentType.Float;
          break;
      }
    }

    /// <summary>
    /// Gets the size of the surface format in bytes.
    /// </summary>
    /// <param name="format">Surface format</param>
    /// <returns>Size of the format, in bytes.</returns>
    public static int SizeInBytes(this SurfaceFormat format)
    {
      switch (format)
      {
        case SurfaceFormat.Vector4:
          return 16;
        case SurfaceFormat.Vector3:
          return 12;
        case SurfaceFormat.Vector2:
        case SurfaceFormat.RGBA64:
          return 8;
        case SurfaceFormat.Single:
        case SurfaceFormat.Color:
        case SurfaceFormat.BGRColor:
        case SurfaceFormat.RGBA1010102:
        case SurfaceFormat.RG32:
          return 4;
        case SurfaceFormat.BGR565:
        case SurfaceFormat.BGRA5551:
        case SurfaceFormat.DXT3:
        case SurfaceFormat.DXT5:
          return 2;
        case SurfaceFormat.DXT1:
        case SurfaceFormat.Alpha8:
          return 1;
        default:
          return 0;
      }
    }

    /// <summary>
    /// Gets if the format is compressed or not.
    /// </summary>
    /// <param name="format">Surface format</param>
    /// <returns>True if the format is compressed, false otherwise.</returns>
    public static bool IsCompressedFormat(this SurfaceFormat format)
    {
      switch (format)
      {
        case SurfaceFormat.DXT1:
        case SurfaceFormat.DXT3:
        case SurfaceFormat.DXT5:
          return true;
        default:
          return false;
      }
    }

    /// <summary>
    /// Gets the size of the vertex format in bytes.
    /// </summary>
    /// <param name="format">Vertex format</param>
    /// <returns>Size of the format, in bytes.</returns>
    public static int SizeInBytes(this VertexFormat format)
    {
      switch (format)
      {
        case VertexFormat.Color:
          return Color.SizeInBytes;
        case VertexFormat.UShort:
        case VertexFormat.NormalizedUShort:
        case VertexFormat.Short:
        case VertexFormat.NormalizedShort:
          return sizeof(short);
        case VertexFormat.UShort2:
        case VertexFormat.NormalizedUShort2:
        case VertexFormat.Short2:
        case VertexFormat.NormalizedShort2:
          return sizeof(short) * 2;
        case VertexFormat.UShort4:
        case VertexFormat.NormalizedUShort4:
        case VertexFormat.Short4:
        case VertexFormat.NormalizedShort4:
          return sizeof(short) * 4;
        case VertexFormat.UInt:
        case VertexFormat.Int:
          return sizeof(int);
        case VertexFormat.UInt2:
        case VertexFormat.Int2:
          return Int2.SizeInBytes;
        case VertexFormat.UInt3:
        case VertexFormat.Int3:
          return Int3.SizeInBytes;
        case VertexFormat.UInt4:
        case VertexFormat.Int4:
          return Int4.SizeInBytes;
        case VertexFormat.Half:
          return BufferHelper.SizeOf<Half>();
        case VertexFormat.Half2:
          return BufferHelper.SizeOf<Half>() * 2;
        case VertexFormat.Half4:
          return BufferHelper.SizeOf<Half>() * 4;
        case VertexFormat.Float:
          return sizeof(float);
        case VertexFormat.Float2:
          return Vector2.SizeInBytes;
        case VertexFormat.Float3:
          return Vector3.SizeInBytes;
        case VertexFormat.Float4:
          return Vector4.SizeInBytes;
        default:
          return 0;
      }
    }

    /// <summary>
    /// Gets the corresponding runtime type this format represents.
    /// </summary>
    /// <param name="format">Vertex format</param>
    /// <returns>Format runtime type.</returns>
    public static Type GetFormatType(this VertexFormat format)
    {
      switch (format)
      {
        case VertexFormat.Color:
          return typeof(Color);
        case VertexFormat.UShort:
        case VertexFormat.NormalizedUShort:
        case VertexFormat.Short:
        case VertexFormat.NormalizedShort:
          return typeof(short);
        case VertexFormat.UShort2:
        case VertexFormat.NormalizedUShort2:
        case VertexFormat.Short2:
        case VertexFormat.NormalizedShort2:
          return typeof(Int2);
        case VertexFormat.UShort4:
        case VertexFormat.NormalizedUShort4:
        case VertexFormat.Short4:
        case VertexFormat.NormalizedShort4:
          return typeof(Int4);
        case VertexFormat.UInt:
        case VertexFormat.Int:
          return typeof(int);
        case VertexFormat.UInt2:
        case VertexFormat.Int2:
          return typeof(Int2);
        case VertexFormat.UInt3:
        case VertexFormat.Int3:
          return typeof(Int3);
        case VertexFormat.UInt4:
        case VertexFormat.Int4:
          return typeof(Int4);
        case VertexFormat.Half:
          return typeof(Half);
        case VertexFormat.Half2:
          // TODO
          return typeof(byte); //return typeof(Half2);
        case VertexFormat.Half4:
          // TODO
          return typeof(byte); //return typeof(Half4);
        case VertexFormat.Float:
          return typeof(float);
        case VertexFormat.Float2:
          return typeof(Vector2);
        case VertexFormat.Float3:
          return typeof(Vector3);
        case VertexFormat.Float4:
          return typeof(Vector4);
        default:
          return typeof(byte); // By default, treat as bytes...
      }
    }

    /// <summary>
    /// Gets the size of the index format in bytes.
    /// </summary>
    /// <param name="format">Index format.</param>
    /// <returns>Size of the format, in bytes.</returns>
    public static int SizeInBytes(this IndexFormat format)
    {
      switch (format)
      {
        case IndexFormat.SixteenBits:
          return sizeof(ushort);
        case IndexFormat.ThirtyTwoBits:
        default:
          return sizeof(int);
      }
    }

    /// <summary>
    /// Gets the corresponding runtime type this format represents.
    /// </summary>
    /// <param name="format">Vertex format</param>
    /// <returns>Format runtime type.</returns>
    public static Type GetFormatType(this IndexFormat format)
    {
      switch (format)
      {
        case IndexFormat.SixteenBits:
          return typeof(ushort);
        case IndexFormat.ThirtyTwoBits:
        default:
          return typeof(int);
      }
    }

    /// <summary>
    /// Binds the specified render target to the first slot and the remaining slots are set to null. A value of null will unbind all currently bound
    /// render targets.
    /// </summary>
    /// <param name="context">Render context.</param>
    /// <param name="renderTarget">Render target to bind.</param>
    public static void SetRenderTarget(this IRenderContext context, [AllowNull] IRenderTarget renderTarget)
    {
      context.SetRenderTarget(SetTargetOptions.None, renderTarget);
    }

    /// <summary>
    /// Binds the specified number of render targets, starting at the first slot. Any remaining slots are set to null. A render target cannot be bound
    /// as both input and output at the same time. A value of null will unbind all currently
    /// bound render targets.
    /// </summary>
    /// <param name="context">Render context.</param>
    /// <param name="renderTargets">Render targets to bind.</param>
    public static void SetRenderTargets(this IRenderContext context, params IRenderTarget[] renderTargets)
    {
      context.SetRenderTargets(SetTargetOptions.None, renderTargets);
    }
  }
}
