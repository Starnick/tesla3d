﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.IO;
using Tesla.Content;

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// The standard effect library contains effect byte code that do not require a content repository and can be used to create new effect instances. 
  /// The standard library can be extended to include effect byte code from other sources.
  /// </summary>
  public sealed class StandardEffectLibrary : IDisposable
  {
    private bool m_isDisposed;

    private Dictionary<string, Entry> m_effects;
    private Dictionary<string, Entry>? m_tempEffects;
    private Dictionary<string, IEffectByteCodeProvider> m_providers;
    private object m_sync;
    private IRenderSystem m_renderSystem;

    /// <summary>
    /// Gets the rendersystem associated with the effect library.
    /// </summary>
    public IRenderSystem RenderSystem
    {
      get
      {
        return m_renderSystem;
      }
    }

    /// <summary>
    /// Gets if the effect library has been disposed or not.
    /// </summary>
    public bool IsDisposed
    {
      get
      {
        return m_isDisposed;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="StandardEffectLibrary"/> class.
    /// </summary>
    /// <param name="renderSystem">The render system used to create effect instances.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the render system is null.</exception>
    public StandardEffectLibrary(IRenderSystem renderSystem)
    {
      GraphicsHelper.ThrowIfRenderSystemNull(renderSystem);

      m_effects = new Dictionary<string, Entry>();
      m_providers = new Dictionary<string, IEffectByteCodeProvider>();
      m_sync = new object();
      m_renderSystem = renderSystem;

      m_isDisposed = false;
    }

    /// <summary>
    /// Loads the effect byte code provider. The provider merely loads the byte code while
    /// the effect library manages the created effects.
    /// </summary>
    /// <param name="provider">The provider to be loaded.</param>
    /// <returns>True if the provider was added, false if it was already present.</returns>
    public bool LoadProvider(IEffectByteCodeProvider provider)
    {
      if (provider is null)
        return false;

      string folderPath = ContentHelper.NormalizePath(provider.FolderPath);

      lock (m_sync)
      {
        if (folderPath is null || m_providers.ContainsKey(folderPath))
          return false;

        m_providers.Add(folderPath, provider);
        return true;
      }
    }

    /// <summary>
    /// Queries if the provider is already loaded in the standard effect library.
    /// </summary>
    /// <param name="provider">Provider that may be loaded or not.</param>
    /// <returns>True if the provider was loaded, false if it is not present.</returns>
    public bool IsProviderLoaded(IEffectByteCodeProvider provider)
    {
      if (provider is null)
        return false;

      lock (m_sync)
      {
        foreach (KeyValuePair<string, IEffectByteCodeProvider> kv in m_providers)
        {
          if (kv.Value == provider)
            return true;
        }

        return false;
      }
    }

    /// <summary>
    /// Queries if the folder path is registered with a provider in the standard effect library.
    /// </summary>
    /// <param name="folderPath">Folder path</param>
    /// <returns>True if the folder path is registered with a provider, false if a provider is not present.</returns>
    public bool IsProviderLoaded(string folderPath)
    {
      if (folderPath is null)
        return false;

      string normalizedFolderPath = ContentHelper.NormalizePath(folderPath);

      lock (m_sync)
      {
        if (!m_providers.ContainsKey(normalizedFolderPath))
          return false;

        return true;
      }
    }

    /// <summary>
    /// Unloads the effect byte code provider using the specified folder path.
    /// </summary>
    /// <param name="folderPath">The folder path of the provider to be unloaded.</param>
    /// <returns>True if the provider was removed, false if it wasn't present.</returns>
    public bool UnloadProvider(string folderPath)
    {
      if (folderPath is null)
        return false;

      folderPath = ContentHelper.NormalizePath(folderPath);

      lock (m_sync)
      {
        IEffectByteCodeProvider? provider;
        if (!m_providers.TryGetValue(folderPath, out provider))
          return false;

        m_providers.Remove(folderPath);

        if (m_tempEffects is null)
          m_tempEffects = new Dictionary<String, Entry>(m_effects.Count);

        //Go through cache and dipose of effects and add ones from other providers
        //to the temp dictionary
        foreach (KeyValuePair<String, Entry> kv in m_effects)
        {
          if (kv.Value.Provider == provider)
          {
            kv.Value.FirstEffectInstance.Dispose();
          }
          else
          {
            m_tempEffects.Add(kv.Key, kv.Value);
          }
        }

        m_effects.Clear();
        SwapDictionaries();

        //If provider is disposable, make sure we call that too
        if (provider is IDisposable disp)
          disp.Dispose();

        return true;
      }
    }

    /// <summary>
    /// Creates a new standard effect corresponding to the full name (without extension). The folder path in the name corresponds
    /// to the provider the effect byte code resides in.
    /// </summary>
    /// <param name="fullName">The full name, folder path + name of the effect instance to return.</param>
    /// <returns>Effect instance, or null if not found.</returns>
    public Effect? CreateEffect(string fullName)
    {
      if (String.IsNullOrEmpty(fullName))
        return null;

      //If there is an extension, this will strip it out
      fullName = Path.ChangeExtension(fullName, null);

      lock (m_sync)
      {
        //Get the effect, if not there query for its byte code and create the first instance,
        //otherwise just clone
        Entry entry;
        if (!m_effects.TryGetValue(fullName, out entry))
        {
          string? folderPath = Path.GetDirectoryName(fullName);
          string? name = Path.GetFileName(fullName); //Will be without extension always

          // Allow folderpath to be empty string
          if (folderPath == null || String.IsNullOrEmpty(name))
            return null;

          IEffectByteCodeProvider? provider;
          if (!m_providers.TryGetValue(folderPath, out provider))
            return null;

          byte[] byteCode = provider.GetEffectByteCode(name);

          //Could not find the byte code
          if (byteCode is null)
            return null;

          entry.ByteCode = byteCode;
          entry.Provider = provider;
          entry.FirstEffectInstance = new Effect(m_renderSystem, entry.ByteCode, fullName);
          entry.FirstEffectInstance.Name = fullName;

          m_effects.Add(fullName, entry);

          return entry.FirstEffectInstance; //Always return first instance
        }

        return entry.FirstEffectInstance.Clone(); //Always clone subsequent instances
      }
    }

    /// <summary>
    /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
    /// </summary>
    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    private void Dispose(bool isDisposing)
    {
      if (!m_isDisposed)
      {
        if (isDisposing)
        {
          //Dispose of all effects
          foreach (KeyValuePair<string, Entry> kv in m_effects)
            kv.Value.FirstEffectInstance.Dispose();

          m_effects.Clear();

          //Remove and dispose of all providers
          foreach (KeyValuePair<string, IEffectByteCodeProvider> kv in m_providers)
          {
            if (kv.Value is IDisposable disp)
              disp.Dispose();
          }

          m_providers.Clear();
        }

        m_isDisposed = true;
      }
    }

    private void SwapDictionaries()
    {
      Dictionary<String, Entry> swap = m_effects;
      m_effects = m_tempEffects!;
      m_tempEffects = swap;
    }

    //Entry for our effect cache
    private struct Entry
    {
      public Effect FirstEffectInstance;
      public byte[] ByteCode;
      public IEffectByteCodeProvider Provider;
    }

    /// <summary>
    /// Base implementation for a <see cref="IEffectByteCodeProvider"/>. Effect byte code buffers get preloaded so no locking is necessary.
    /// </summary>
    public abstract class BaseProvider : IEffectByteCodeProvider
    {
      private string m_folderPath;
      private Dictionary<string, byte[]> m_effectByteCodes;

      /// <summary>
      /// Gets the folder path where the effect shader files of this provider reside in. The Engine's default effect shader files are in the topmost folder (e.g.
      /// <see cref="String.Empty" /> would be the value).
      /// </summary>
      public string FolderPath
      {
        get
        {
          return m_folderPath;
        }
      }

      /// <summary>
      /// Constructs a new instance of the <see cref="BaseProvider"/> class.
      /// </summary>
      /// <param name="folderPath">The folder path that identifies the location of the effect shader files.</param>
      /// <exception cref="ArgumentNullException">Thrown if the folder path is null.</exception>
      protected BaseProvider(string folderPath)
      {
        ArgumentNullException.ThrowIfNull(folderPath, nameof(folderPath));

        m_folderPath = folderPath;
        m_effectByteCodes = new Dictionary<string, byte[]>();

        Preload(m_effectByteCodes);
      }

      /// <summary>
      /// Gets the effect byte code specified by the name.
      /// </summary>
      /// <param name="name">The name of the effect file to get byte code for. Should not include the .tefx extension.</param>
      /// <returns>Effect byte code, or null if the name does not correspond to an effect file.</returns>
      public byte[]? GetEffectByteCode(string name)
      {
        if (String.IsNullOrEmpty(name))
          return null;

        byte[]? byteCode;
        if (!m_effectByteCodes.TryGetValue(name, out byteCode))
          byteCode = null;

        return byteCode;
      }

      /// <summary>
      /// Called to preload all the effect byte code buffers.
      /// </summary>
      /// <param name="effectByteCodes">Cache that will hold the effect byte code buffers.</param>
      protected abstract void Preload(Dictionary<string, byte[]> effectByteCodes);
    }
  }
}
