﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Diagnostics;
using Tesla.Content;

// TODO Nullable

namespace Tesla.Graphics
{
  /// <summary>
  /// <para>
  /// At a high level, a material defines how an object is rendered. At a low level, it tracks shader uniforms (aka parameters) and manages state to setup the
  /// render pipeline. 
  /// </para>
  /// <para>
  /// Parameters of an effect can be tracked where the data assigned to that parameter can be read/written via a script file. The other
  /// type of parameter binding is "computed" and thus data driven, where the name of a provider in code is associated with a shader uniform, this is useful for automatically setting data
  /// every frame without explicitly knowing the value of that data (e.g. time, screen information, lighting, World-View-Projection transforms, and other properties from an objects or any data
  /// that needs to be "assembled" from various sources at run-time and can't be precomputed and saved).
  /// </para>
  /// <para>
  /// Passes setup state for individual draw calls. Each pass contains a group of shaders (vertex/pixel/etc) and render states, and when the pass is applied parameter data is flushed to the GPU.
  /// </para>
  /// </summary>
  [SavableVersion(3)]
  [DebuggerDisplay("Name = {Name}, ParentScript = {ParentScriptName}, ScriptFile = {ScriptFileName}, PassCount = {Passes.Count}")]
  public class Material : ISavable, IComparable<Material>, INamable, IDisposable
  {
    private static RenderPropertyCollection s_nullProps = new RenderPropertyCollection();

    private String m_name;
    private String m_parentScriptName;
    private String m_scripFileName;
    private bool m_isAnonymous;
    private Effect m_effect;
    private MaterialPassCollection m_passes;
    private Dictionary<String, ComputedParameterBinding> m_computedParameterBindings;
    private Dictionary<String, ScriptParameterBinding> m_scriptParameterBindings;

    //Properties
    private InheritedValue<ShadowMode> m_shadowMode;
    private InheritedValue<TransparencyMode> m_transparencyMode;

    /// <summary>
    /// Gets or sets the name of the material.
    /// </summary>
    public String Name
    {
      get
      {
        return m_name;
      }
      set
      {
        m_name = value;
        if (String.IsNullOrEmpty(value))
          m_name = String.Empty;
      }
    }

    /// <summary>
    /// Gets or sets the parent script name. This can be a content path to another script file (including ".tem" extension and with or without a subresource name)
    /// or the name of a "standard" script. If the name is prepended with "::" then the parent material exists in the same script file as this material.
    /// </summary>
    public String ParentScriptName
    {
      get
      {
        return m_parentScriptName;
      }
      set
      {
        m_parentScriptName = value;
        if (String.IsNullOrEmpty(value))
          m_parentScriptName = String.Empty;
      }
    }

    /// <summary>
    /// Gets or sets the name of the script file. Multiple materials can reside in a single script file.
    /// </summary>
    public String ScriptFileName
    {
      get
      {
        return m_scripFileName;
      }
      set
      {
        m_scripFileName = value;
        if (String.IsNullOrEmpty(value))
          m_scripFileName = String.Empty;
      }
    }

    /// <summary>
    /// Gets or sets if the material is an "anonymous" material in a material definition script (.temd). Material definitions
    /// can reference materials defined in separate material scripts (.tem), or declare the material inline in the definition script. Anonymous materials
    /// are not declared with a name, instead the material name is generated based on the material definition name and render bucket name.
    /// </summary>
    public bool IsAnonymous
    {
      get
      {
        return m_isAnonymous;
      }
      set
      {
        m_isAnonymous = value;
      }
    }

    /// <summary>
    /// Gets if the material is derivative of a standard material (meaning the script's parent is a standard material). Materials that inherit
    /// from standard materials usually will define a unique texture or color, so a material instance will never be a standard material itself, but will
    /// inherit from one.
    /// </summary>
    public bool IsStandardContent
    {
      get
      {
        return !String.IsNullOrEmpty(StandardMaterialScriptLibrary.GetMaterialScript(m_parentScriptName));
      }
    }

    /// <summary>
    /// Gets if the material is derivative of a parent material.
    /// </summary>
    public bool HasParent
    {
      get
      {
        return !String.IsNullOrEmpty(m_parentScriptName);
      }
    }

    /// <summary>
    /// Gets the effect that manages the GPU shaders and associated resources.
    /// </summary>
    public Effect Effect
    {
      get
      {
        return m_effect;
      }
    }

    /// <summary>
    /// Gets the computed parameter bindings that the material is tracking.
    /// </summary>
    public IEnumerable<ComputedParameterBinding> ComputedParameterBindings
    {
      get
      {
        return m_computedParameterBindings.Values;
      }
    }

    /// <summary>
    /// Gets the number of computed parameter bindings that the material is tracking.
    /// </summary>
    public int ComputedParameterBindingCount
    {
      get
      {
        return m_computedParameterBindings.Count;
      }
    }

    /// <summary>
    /// Gets the parameter bindings that the material is tracking.
    /// </summary>
    public IEnumerable<ScriptParameterBinding> ScriptParameterBindings
    {
      get
      {
        return m_scriptParameterBindings.Values;
      }
    }

    /// <summary>
    /// Gets the number of parameter bindings that the material is tracking.
    /// </summary>
    public int ScriptParameterBindingCount
    {
      get
      {
        return m_scriptParameterBindings.Count;
      }
    }

    /// <summary>
    /// Gets the pass collection.
    /// </summary>
    public MaterialPassCollection Passes
    {
      get
      {
        return m_passes;
      }
    }

    /// <summary>
    /// Gets or sets the shadow mode. If inherited or the default value, then it won't be written out during script serialization.
    /// </summary>
    public InheritedValue<ShadowMode> ShadowMode
    {
      get
      {
        return m_shadowMode;
      }
      set
      {
        m_shadowMode = value;
      }
    }

    /// <summary>
    /// Gets or sets the transparency mode. If inherited or the default value, then it won't be written out during script serialization.
    /// </summary>
    public InheritedValue<TransparencyMode> TransparencyMode
    {
      get
      {
        return m_transparencyMode;
      }
      set
      {
        m_transparencyMode = value;
      }
    }

    /// <summary>
    /// Gets if the material is valid for rendering. The effect must exist/not be disposed and the material must have valid passes.
    /// </summary>
    public bool IsValid
    {
      get
      {
        return m_passes.Count > 0 && m_effect != null && !m_effect.IsDisposed;
      }
    }

    /// <summary>
    /// Gets if the material has been disposed or not.
    /// </summary>
    public bool IsDisposed
    {
      get
      {
        if (m_effect == null)
          return true;

        return m_effect.IsDisposed;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Material"/> class.
    /// </summary>
    protected Material() { }

    /// <summary>
    /// Constructs a new instance of the <see cref="Material"/> class.
    /// </summary>
    /// <param name="effect">Effect to bind to this material.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the effect is null.</exception>
    public Material(Effect effect) : this("Material", effect) { }


    /// <summary>
    /// Constructs a new instance of the <see cref="Material"/> class.
    /// </summary>
    /// <param name="name">Optional name for the material.</param>
    /// <param name="effect">Effect to bind to this material.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the effect is null.</exception>
    public Material(String name, Effect effect)
    {
      m_name = (String.IsNullOrEmpty(name)) ? "Material" : name;
      m_scripFileName = String.Empty;
      m_parentScriptName = String.Empty;
      m_isAnonymous = false;
      m_effect = effect;

      if (effect == null)
        throw new ArgumentNullException("effect");

      m_passes = new MaterialPassCollection(this);
      m_computedParameterBindings = new Dictionary<String, ComputedParameterBinding>();
      m_scriptParameterBindings = new Dictionary<String, ScriptParameterBinding>();

      m_shadowMode = new InheritedValue<ShadowMode>(Graphics.ShadowMode.None, false);
      m_transparencyMode = new InheritedValue<TransparencyMode>(Graphics.TransparencyMode.OneSided, false);
    }

    /// <summary>
    /// Clones this instance.
    /// </summary>
    /// <returns>A new material instance that is a deep copy.</returns>
    public Material Clone()
    {
      Material clone = new Material(m_name, m_effect.Clone());

      clone.m_parentScriptName = m_parentScriptName;
      clone.m_scripFileName = m_scripFileName;
      clone.m_isAnonymous = m_isAnonymous;
      clone.m_shadowMode = m_shadowMode;
      clone.m_transparencyMode = m_transparencyMode;

      //Copy over passes

      m_passes.CopyPasses(clone.m_passes);

      //Just copy contents of each collection to the clone, everything should already be valid

      foreach (KeyValuePair<String, ComputedParameterBinding> kv in m_computedParameterBindings)
      {
        ComputedParameterBinding oldBinding = kv.Value;
        ComputedParameterBinding newBinding = new ComputedParameterBinding(oldBinding.Name, oldBinding.Provider, clone.m_effect.Parameters[oldBinding.Name], oldBinding.IsInherited);
        clone.m_computedParameterBindings.Add(newBinding.Name, newBinding);
      }

      foreach (KeyValuePair<String, ScriptParameterBinding> kv in m_scriptParameterBindings)
      {
        ScriptParameterBinding oldBinding = kv.Value;
        ScriptParameterBinding newBinding = new ScriptParameterBinding(oldBinding.Name, clone.m_effect.Parameters[oldBinding.Name], oldBinding.IsInherited);
        clone.m_scriptParameterBindings.Add(newBinding.Name, newBinding);
      }

      return clone;
    }

    /// <summary>
    /// Called before using the material passes to setup draw calls. Computed parameters are calculated and effect state is updated.
    /// </summary>
    /// <param name="renderContext">Current render context.</param>
    /// <param name="properties">Render properties of the object to be rendered.</param>
    public void ApplyMaterial(IRenderContext renderContext, RenderPropertyCollection properties = null)
    {
      if (renderContext == null)
        return;

      if (properties == null)
        properties = s_nullProps;

      foreach (KeyValuePair<String, ComputedParameterBinding> kv in m_computedParameterBindings)
      {
        ComputedParameterBinding binding = kv.Value;
        if (binding.IsValid)
        {
          Object localState = binding.LocalState;
          binding.Provider.UpdateParameter(renderContext, properties, this, binding.Parameter, ref localState);
          binding.LocalState = localState;
        }
      }
    }

    /// <summary>
    /// Sets up a computed parameter binding, which will track a parameter in the material's effect and feed it data from a provider.
    /// </summary>
    /// <param name="parameterName">Name of the parameter.</param>
    /// <param name="provider">Provider that will compute the parameter data.</param>
    /// <param name="binding">The computed parameter binding that was added or that already existed.</param>
    /// <returns>True if the computed parameter binding was created or already existed, false if the parameter doesn't exist or the provider is null.</returns>
    public bool SetComputedParameterBinding(String parameterName, IComputedParameterProvider provider, out ComputedParameterBinding binding)
    {
      binding = null;

      if (String.IsNullOrEmpty(parameterName) || provider == null)
        return false;

      //If we already have the parameter name, then it was successfully added at one point. So don't do anything but report that we have it.
      if (m_computedParameterBindings.TryGetValue(parameterName, out binding))
        return true;

      IEffectParameter effectParam = m_effect.Parameters[parameterName];
      if (effectParam == null)
        return false;

      binding = new ComputedParameterBinding(parameterName, provider, effectParam);
      bool isValid = binding.IsValid;

      if (isValid)
      {
        m_computedParameterBindings.Add(parameterName, binding);
      }
      else
      {
        binding = null;
      }

      return isValid;
    }

    /// <summary>
    /// Sets up a computed parameter binding, which will track a parameter in the material's effect and feed it data from a provider.
    /// </summary>
    /// <param name="parameterName">Name of the parameter.</param>
    /// <param name="provider">Provider that will compute the parameter data.</param>
    /// <returns>True if the computed parameter binding was created or already existed, false if the parameter doesn't exist or the provider is null.</returns>
    public bool SetComputedParameterBinding(String parameterName, IComputedParameterProvider provider)
    {
      ComputedParameterBinding binding;
      return SetComputedParameterBinding(parameterName, provider, out binding);
    }

    /// <summary>
    /// Retrieves a computed parameter binding by name.
    /// </summary>
    /// <param name="parameterName">Name of the parameter.</param>
    /// <returns>The computed parameter binding, or null if it doesn't exist.</returns>
    public ComputedParameterBinding GetComputedParameterBinding(String parameterName)
    {
      if (String.IsNullOrEmpty(parameterName))
        return null;

      ComputedParameterBinding binding;
      if (m_computedParameterBindings.TryGetValue(parameterName, out binding))
        return binding;

      return null;
    }

    /// <summary>
    /// Removes a computed parameter binding by name.
    /// </summary>
    /// <param name="parameterName">Name of the parameter.</param>
    /// <returns>True if the binding was removed, false otherwise.</returns>
    public bool RemoveComputedParameterBinding(String parameterName)
    {
      if (String.IsNullOrEmpty(parameterName))
        return false;

      return m_computedParameterBindings.Remove(parameterName);
    }

    /// <summary>
    /// Sets up a binding with a parameter in the material's effect.
    /// </summary>
    /// <param name="parameterName">Name of the parameter.</param>
    /// <param name="dataType">Optional data type (if null, a default type is used). Each parameter (non-structures) have a default .NET type, but some parameters may be associated with
    /// a different type. E.g. a parameter that is a 32-bit integer but is really a packed <see cref="Color"/>.</param>
    /// <param name="binding">The script binding that was added or that already existed.</param>
    /// <returns>True if the binding is valid, false if not.</returns>
    public bool SetParameterBinding(String parameterName, Type dataType, out ScriptParameterBinding binding)
    {
      binding = null;

      if (String.IsNullOrEmpty(parameterName))
        return false;

      //If we already have the parameter name, then it was successfully added at one point. So don't do anything but report that we have it.
      if (m_scriptParameterBindings.TryGetValue(parameterName, out binding))
        return true;

      IEffectParameter effectParam = m_effect.Parameters[parameterName];
      if (effectParam == null)
        return false;

      binding = new ScriptParameterBinding(parameterName, effectParam);
      bool isValid = binding.IsValid;

      if (isValid)
      {
        m_scriptParameterBindings.Add(parameterName, binding);
        if (dataType != null)
          binding.DataType = dataType;
      }
      else
      {
        binding = null;
      }

      return isValid;
    }

    /// <summary>
    /// Sets up a binding with a parameter in the material's effect.
    /// </summary>
    /// <param name="parameterName">Name of the parameter.</param>
    /// <param name="dataType">Optional data type. Each parameter (non-structures) have a default .NET type, but some parameters may be associated with
    /// a different type. E.g. a parameter that is a 32-bit integer but is really a packed <see cref="Color"/>.</param>
    /// <returns>True if the binding is valid, false if not.</returns>
    public bool SetParameterBinding(String parameterName, Type dataType = null)
    {
      ScriptParameterBinding binding;
      return SetParameterBinding(parameterName, dataType, out binding);
    }

    /// <summary>
    /// Retrieves a parameter binding by name.
    /// </summary>
    /// <param name="parameterName">Name of the parameter.</param>
    /// <returns>The parameter binding, or null if it doesn't exist.</returns>
    public ScriptParameterBinding GetParameterBinding(String parameterName)
    {
      if (String.IsNullOrEmpty(parameterName))
        return null;

      ScriptParameterBinding binding;
      if (m_scriptParameterBindings.TryGetValue(parameterName, out binding))
        return binding;

      return null;
    }

    /// <summary>
    /// Removes a parameter binding by name.
    /// </summary>
    /// <param name="parameterName">Name of the parameter.</param>
    /// <returns>True if the parameter was removed, false otherwise.</returns>
    public bool RemoveParameterBinding(String parameterName)
    {
      if (String.IsNullOrEmpty(parameterName))
        return false;

      return m_scriptParameterBindings.Remove(parameterName);
    }

    /// <summary>
    /// Ensures a script binding exists for the parameter (if not already) and sets the value to the effect parameter. If the parameter
    /// does not exist, this safely avoids setting the value.
    /// </summary>
    /// <typeparam name="T">Type of value to set.</typeparam>
    /// <param name="parameterName">Name of the parameter.</param>
    /// <param name="value">Value to set.</param>
    /// <returns>True if the parameter exists, which means there will exist a script binding for it and the value was set.</returns>
    public bool SetParameterValue<T>(String parameterName, in T value) where T : unmanaged
    {
      ScriptParameterBinding binding;
      bool success = SetParameterBinding(parameterName, typeof(T), out binding);
      if (success)
      {
        binding.Parameter.SetValue<T>(in value);
        binding.IsInherited = false; //Setting a value...no longer inherited
      }

      return success;
    }

    /// <summary>
    /// Ensures a script binding exists for the parameter (if not already) and sets the matrix value to the effect parameter. If the parameter
    /// does not exist, this safely avoids setting the value.
    /// </summary>
    /// <param name="parameterName">Name of the parameter.</param>
    /// <param name="value">Matrix to set.</param>
    /// <param name="transpose">True if the matrix should be transposed before setting, false otherwise.</param>
    /// <returns>True if the parameter exists, which means there will exist a binding for it and the value was set.</returns>
    public bool SetParameterValue(String parameterName, in Matrix value, bool transpose = false)
    {
      ScriptParameterBinding binding;
      bool success = SetParameterBinding(parameterName, typeof(Matrix), out binding);
      if (success)
      {
        if (transpose)
          binding.Parameter.SetValueTranspose(in value);
        else
          binding.Parameter.SetValue(in value);

        binding.IsInherited = false; //Setting a value...no longer inherited
      }

      return success;
    }

    /// <summary>
    /// Ensures a script binding exists for the parameter (if not already) and sets the resource to the effect parameter. If the parameter
    /// does not exist, this safely avoids setting the resource.
    /// </summary>
    /// <typeparam name="T">Type of resource to set.</typeparam>
    /// <param name="parameterName">Name of the parameter.</param>
    /// <param name="resource">Resource to set (e.g. SamplerState, Texture2D), nulls is valid.</param>
    /// <returns>True if the parameter exists, which means there will exist a binding for it and the resource was set.</returns>
    public bool SetParameterResource<T>(String parameterName, T resource) where T : IShaderResource
    {
      ScriptParameterBinding binding;
      bool success = SetParameterBinding(parameterName, (resource != null) ? resource.GetType() : null, out binding);
      if (success)
      {
        binding.Parameter.SetResource<T>(resource);
        binding.IsInherited = false; //Setting a value...no longer inherited
      }

      return success;
    }

    /// <summary>
    /// Reads the object data from the input.
    /// </summary>
    /// <param name="input">Savable reader</param>
    public void Read(ISavableReader input)
    {
      int version = input.GetVersion(typeof(Material));

      m_name = input.ReadString("Name");
      m_scripFileName = input.ReadString("ScriptFileName");
      m_parentScriptName = String.Empty;
      m_isAnonymous = false;

      //Added more metadata in version 2
      if (version >= 2)
      {
        m_parentScriptName = input.ReadString("ParentScriptName");
        m_isAnonymous = input.ReadBoolean("IsAnonymous");
      }

      //Added inheritance metadata for these properties in version 3
      if (version >= 3)
      {
        m_shadowMode = ReadInheritedValueEnum<ShadowMode>("ShadowMode", input);
        m_transparencyMode = ReadInheritedValueEnum<TransparencyMode>("TransparencyMode", input);
      }
      else
      {
        m_shadowMode = input.ReadEnum<ShadowMode>("ShadowMode");
        m_transparencyMode = input.ReadEnum<TransparencyMode>("TransparencyMode");
      }

      bool isStandardEffect = input.ReadBoolean("IsStandardEffect");

      //If material was valid before and reading into an existing instance, ensure old effect is destroyed
      if (m_effect != null)
        m_effect.Dispose();

      if (isStandardEffect)
      {
        String standardEffectName = input.ReadString("Effect");
        m_effect = GraphicsHelper.GetRenderSystem(input.ServiceProvider).StandardEffects.CreateEffect(standardEffectName);
      }
      else
      {
        m_effect = input.ReadExternalSavable<Effect>("Effect");

        //Ensure we're cloning the effect so two materials using it don't use the same instance. Maybe come up with something better...
        if (m_effect != null)
          m_effect = m_effect.Clone();
      }

      int parameterBindingCount = input.BeginReadGroup("ParameterBindings");

      //Initialize or clear parameter binding collection
      if (m_scriptParameterBindings == null)
        m_scriptParameterBindings = new Dictionary<String, ScriptParameterBinding>(parameterBindingCount);
      else
        m_scriptParameterBindings.Clear();

      //Read parameter bindings
      for (int i = 0; i < parameterBindingCount; i++)
        ReadParameterBinding(input, version);

      input.EndReadGroup();

      int computedParameterBindingCount = input.BeginReadGroup("ComputedParameterBindings");

      //Initialize or clear computed parameter binding collection
      if (m_computedParameterBindings == null)
        m_computedParameterBindings = new Dictionary<String, ComputedParameterBinding>(computedParameterBindingCount);
      else
        m_computedParameterBindings.Clear();

      //Read computed parameters
      for (int i = 0; i < computedParameterBindingCount; i++)
        ReadComputedParameterBinding(input, version);

      input.EndReadGroup();

      int passCount = input.BeginReadGroup("Passes");

      //Initialize or clear pass collection
      if (m_passes == null)
        m_passes = new MaterialPassCollection(this, passCount);
      else
        m_passes.Clear();

      //Read passes
      for (int i = 0; i < passCount; i++)
        ReadPass(input, version);

      input.EndReadGroup();

      //Set the current shader group to the first pass, more of a convention than anything.
      if (m_effect != null && passCount > 0)
        m_effect.CurrentShaderGroup = m_passes[0].ShaderGroup;
    }

    /// <summary>
    /// Writes the object data to the output.
    /// </summary>
    /// <param name="output">Savable writer</param>
    public void Write(ISavableWriter output)
    {
      output.Write("Name", m_name);
      output.Write("ScriptFileName", m_scripFileName);
      output.Write("ParentScriptName", m_parentScriptName);
      output.Write("IsAnonymous", m_isAnonymous);

      WriteInheritedValueEnum<ShadowMode>("ShadowMode", m_shadowMode, output);
      WriteInheritedValueEnum<TransparencyMode>("TransparencyMode", m_transparencyMode, output);

      output.Write("IsStandardEffect", m_effect.IsStandardContent);

      if (m_effect.IsStandardContent)
      {
        output.Write("Effect", m_effect.StandardContentName);
      }
      else
      {
        output.WriteExternalSavable<Effect>("Effect", m_effect);
      }

      output.BeginWriteGroup("ParameterBindings", m_scriptParameterBindings.Count);

      foreach (KeyValuePair<String, ScriptParameterBinding> kv in m_scriptParameterBindings)
        WriteParameterBinding(kv.Value, output);

      output.EndWriteGroup();

      output.BeginWriteGroup("ComputedParameterBindings", m_computedParameterBindings.Count);

      foreach (KeyValuePair<String, ComputedParameterBinding> kv in m_computedParameterBindings)
        WriteComputedParameterBinding(kv.Value, output);

      output.EndWriteGroup();

      output.BeginWriteGroup("Passes", m_passes.Count);

      for (int i = 0; i < m_passes.Count; i++)
        WritePass(m_passes[i], output);

      output.EndWriteGroup();
    }

    /// <summary>
    /// Compares the current instance with another object of the same type and returns an integer that indicates whether the current instance precedes, follows, or occurs in the same position in the sort order as the other object.
    /// </summary>
    /// <param name="other">An object to compare with this instance.</param>
    /// <returns>A value that indicates the relative order of the objects being compared. The return value has these meanings: Value Meaning Less than zero This instance precedes <paramref name="other" /> in the sort order.  Zero This instance occurs in the same position in the sort order as <paramref name="other" />. Greater than zero This instance follows <paramref name="other" /> in the sort order.</returns>
    public int CompareTo(Material other)
    {
      if (other == null || !other.IsValid)
        return -1;

      if (other == this)
        return 0;

      int otherSortKey = other.m_effect.SortKey;
      int thisSortKey = m_effect.SortKey;

      if (thisSortKey < otherSortKey)
      {
        return -1;
      }
      else if (thisSortKey > otherSortKey)
      {
        return 1;
      }
      else
      {
        //If same effects, look at what shader groups are used for each pass
        MaterialPassCollection otherPasses = other.m_passes;
        for (int i = 0; i < m_passes.Count; i++)
        {
          if (i < otherPasses.Count)
          {
            int thisShaderIndex = m_passes[i].ShaderGroup.ShaderGroupIndex;
            int otherShaderIndex = otherPasses[i].ShaderGroup.ShaderGroupIndex;

            //Compare the shaders used by each pass
            if (thisShaderIndex < otherShaderIndex)
            {
              return -1;
            }
            else if (thisShaderIndex > otherShaderIndex)
            {
              return 1;
            }
          }
          else
          {
            //If each pass has been equal so far, but "this" material has more passes (and therefore more state changes), then sort "other" material as less than
            return 1;
          }
        }
      }

      //If got to this point, then materials are equal in what shaders they use
      return 0;
    }

    /// <summary>
    /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
    /// </summary>
    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    /// <summary>
    /// Releases unmanaged and - optionally - managed resources.
    /// </summary>
    /// <param name="isDisposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
    protected virtual void Dispose(bool isDisposing)
    {
      if (!IsDisposed)
      {
        if (isDisposing)
        {
          if (m_effect != null)
            m_effect.Dispose();
        }
      }
    }

    #region Serialization Helpers

    private InheritedValue<T> ReadInheritedValueEnum<T>(String name, ISavableReader input) where T : struct, Enum
    {
      InheritedValue<T> val;

      input.BeginReadGroup(name);

      val.Value = input.ReadEnum<T>("Value");
      val.IsInherited = input.ReadBoolean("IsInherited");

      input.EndReadGroup();

      return val;
    }

    private void WriteInheritedValueEnum<T>(String name, in InheritedValue<T> val, ISavableWriter output) where T : struct, Enum
    {
      output.BeginWriteGroup(name);

      output.WriteEnum<T>("Value", val.Value);
      output.Write("IsInherited", val.IsInherited);

      output.EndWriteGroup();
    }

    private void ReadPass(ISavableReader input, int version)
    {
      input.BeginReadGroup("Pass");

      String name = input.ReadString("Name");
      bool isInherited = (version >= 2) ? input.ReadBoolean("IsInherited") : false;
      bool isOverride = (version >= 3) ? input.ReadBoolean("IsOverride") : false;
      String shaderGroup = input.ReadString("ShaderGroup");

      MaterialPass pass = m_passes.Add(name, shaderGroup);
      pass.RenderStatesToApply = input.ReadEnum<EnforcedRenderState>("RenderStatesToApply");

      if ((pass.RenderStatesToApply & EnforcedRenderState.BlendState) == EnforcedRenderState.BlendState)
        pass.BlendState = ReadRenderState<BlendState>("BlendState", input, RenderStateType.BlendState);

      if ((pass.RenderStatesToApply & EnforcedRenderState.DepthStencilState) == EnforcedRenderState.DepthStencilState)
        pass.DepthStencilState = ReadRenderState<DepthStencilState>("DepthStencilState", input, RenderStateType.DepthStencilState);

      if ((pass.RenderStatesToApply & EnforcedRenderState.RasterizerState) == EnforcedRenderState.RasterizerState)
        pass.RasterizerState = ReadRenderState<RasterizerState>("RasterizerState", input, RenderStateType.RasterizerState);

      //Set these last to ensure override/inheritance metadata is untouched by setting data
      pass.IsInherited = isInherited;
      pass.IsOverride = isOverride;

      input.EndReadGroup();
    }

    private void WritePass(MaterialPass pass, ISavableWriter output)
    {
      output.BeginWriteGroup("Pass");

      output.Write("Name", pass.Name);
      output.Write("IsInherited", pass.IsInherited);
      output.Write("IsOverride", pass.IsOverride);
      output.Write("ShaderGroup", pass.ShaderGroup.Name);
      output.WriteEnum<EnforcedRenderState>("RenderStatesToApply", pass.RenderStatesToApply);

      if ((pass.RenderStatesToApply & EnforcedRenderState.BlendState) == EnforcedRenderState.BlendState)
        WriteRenderState<BlendState>("BlendState", pass.BlendState, output);

      if ((pass.RenderStatesToApply & EnforcedRenderState.DepthStencilState) == EnforcedRenderState.DepthStencilState)
        WriteRenderState<DepthStencilState>("DepthStencilState", pass.DepthStencilState, output);

      if ((pass.RenderStatesToApply & EnforcedRenderState.RasterizerState) == EnforcedRenderState.RasterizerState)
        WriteRenderState<RasterizerState>("RasterizerState", pass.RasterizerState, output);

      output.EndWriteGroup();
    }

    private T ReadRenderState<T>(String propName, ISavableReader input, RenderStateType type) where T : RenderState
    {
      input.BeginReadGroup(propName);

      bool isPredefined = input.ReadBoolean("IsPredefined");

      try
      {
        if (isPredefined)
        {
          String name = input.ReadString("State");
          switch (type)
          {
            case RenderStateType.BlendState:
              return m_effect.RenderSystem.PredefinedBlendStates.GetBlendStateByName(name) as T;
            case RenderStateType.DepthStencilState:
              return m_effect.RenderSystem.PredefinedDepthStencilStates.GetDepthStencilStateByName(name) as T;
            case RenderStateType.RasterizerState:
              return m_effect.RenderSystem.PredefinedRasterizerStates.GetRasterizerStateByName(name) as T;
            default:
              return null;
          }
        }
        else
        {
          return m_effect.RenderSystem.RenderStateCache.GetOrCache(input.ReadSharedSavable<T>("State"));
        }
      }
      finally
      {
        input.EndReadGroup();
      }
    }

    private void WriteRenderState<T>(String name, T rs, ISavableWriter output) where T : RenderState
    {
      output.BeginWriteGroup(name);

      bool isPredefined = (rs != null) ? rs.IsStandardContent : false;
      output.Write("IsPredefined", isPredefined);

      if (isPredefined)
        output.Write("State", rs.StandardContentName);
      else
        output.WriteSharedSavable<T>("State", rs);

      output.EndWriteGroup();
    }

    private void ReadComputedParameterBinding(ISavableReader input, int version)
    {
      input.BeginReadGroup("ComputedParameterBinding");

      String compParamName = input.ReadString("ComputedParameterName");
      String bindingName = input.ReadString("BindingName");
      bool isInherited = (version >= 2) ? input.ReadBoolean("IsInherited") : false;

      //Should be present, unexpected behaviour if the provider is not there...
      IComputedParameterProvider provider = ComputedParameter.GetProvider(compParamName);
      if (provider == null)
        throw new TeslaContentException(StringLocalizer.Instance.GetLocalizedString("ComputedParameterProviderDoesNotExist", compParamName));

      if (SetComputedParameterBinding(bindingName, provider))
        m_computedParameterBindings[bindingName].IsInherited = isInherited;

      input.EndReadGroup();
    }

    private void WriteComputedParameterBinding(ComputedParameterBinding binding, ISavableWriter output)
    {
      output.BeginWriteGroup("ComputedParameterBinding");

      output.Write("ComputedParameterName", binding.Provider.ComputedParameterName);
      output.Write("BindingName", binding.Name);
      output.Write("IsInherited", binding.IsInherited);

      output.EndWriteGroup();
    }

    private void ReadParameterBinding(ISavableReader input, int version)
    {
      input.BeginReadGroup("ParameterBinding");

      String bindingName = input.ReadString("BindingName");
      bool isInherited = (version >= 2) ? input.ReadBoolean("IsInherited") : false;

      if (SetParameterBinding(bindingName))
      {
        ScriptParameterBinding binding = m_scriptParameterBindings[bindingName];
        binding.IsInherited = isInherited;

        ReadEffectParameterData(binding.Parameter, input);
      }

      input.EndReadGroup();
    }

    private void WriteParameterBinding(ScriptParameterBinding binding, ISavableWriter output)
    {
      output.BeginWriteGroup("ParameterBinding");

      output.Write("BindingName", binding.Name);
      output.Write("IsInherited", binding.IsInherited);
      WriteEffectParameterData(binding.Parameter, output);

      output.EndWriteGroup();
    }

    private void ReadEffectParameterData(IEffectParameter p, ISavableReader input, String overrideName = null)
    {
      if (p.IsArray)
      {
        int elemCount = input.BeginReadGroup(p.Name);

        EffectParameterCollection elems = p.Elements;
        System.Diagnostics.Debug.Assert(elemCount != elems.Count, "Too many array elements for effect parameter.");

        for (int i = 0; i < elemCount; i++)
          ReadEffectParameterData(elems[i], input, "Element"); //Override the name of each element

        input.EndReadGroup();
      }
      else
      {
        String name = (String.IsNullOrEmpty(overrideName)) ? p.Name : overrideName;

        if (p.StructureMembers.Count > 0)
        {
          int structMemberCount = input.BeginReadGroup(name);

          EffectParameterCollection structMembers = p.StructureMembers;
          System.Diagnostics.Debug.Assert(structMemberCount != structMembers.Count, "Too many structure members for effect parameter.");

          for (int i = 0; i < structMemberCount; i++)
            ReadEffectParameterData(structMembers[i], input); //Do not override the name of any structure members

          input.EndReadGroup();
        }
        else
        {
          switch (p.ParameterClass)
          {
            case EffectParameterClass.Object:
              switch (p.ParameterType)
              {
                case EffectParameterType.SamplerState:
                  {
                    SamplerState ss = input.ReadSharedSavable<SamplerState>(name);
                    p.SetResource<SamplerState>(ss);
                  }
                  break;
                case EffectParameterType.Texture:
                case EffectParameterType.Texture1D:
                case EffectParameterType.Texture1DArray:
                case EffectParameterType.Texture2D:
                case EffectParameterType.Texture2DArray:
                case EffectParameterType.Texture2DMS:
                case EffectParameterType.Texture2DMSArray:
                case EffectParameterType.Texture3D:
                case EffectParameterType.TextureCube:
                case EffectParameterType.TextureCubeArray:
                  {
                    Texture tex = input.ReadExternalSavable<Texture>(name);
                    p.SetResource<Texture>(tex);
                  }
                  break;
              }
              break;
            case EffectParameterClass.MatrixColumns:
            case EffectParameterClass.MatrixRows:
              if (p.ColumnCount == 4 && p.RowCount == 4)
              {
                switch (p.ParameterType)
                {
                  case EffectParameterType.Single:
                    {
                      Matrix val;
                      input.Read<Matrix>(name, out val);
                      p.SetValue(val);
                    }
                    break;
                }
              }
              break;
            case EffectParameterClass.Scalar:
            case EffectParameterClass.Vector:
              if (p.RowCount == 1)
              {
                switch (p.ParameterType)
                {
                  case EffectParameterType.Bool:
                    switch (p.ColumnCount)
                    {
                      case 1:
                        {
                          p.SetValue<Bool>(input.Read<Bool>(name));
                        }
                        break;
                      case 2:
                        {
                          Bool2 val;
                          input.Read<Bool2>(name, out val);
                          p.SetValue<Bool2>(val);
                        }
                        break;
                      case 3:
                        {
                          Bool3 val;
                          input.Read<Bool3>(name, out val);
                          p.SetValue<Bool3>(val);
                        }
                        break;
                      case 4:
                        {
                          Bool4 val;
                          input.Read<Bool4>(name, out val);
                          p.SetValue<Bool4>(val);
                        }
                        break;
                    }
                    break;
                  case EffectParameterType.Int32:
                    switch (p.ColumnCount)
                    {
                      case 1:
                        {
                          p.SetValue<int>(input.ReadInt32(name));
                        }
                        break;
                      case 2:
                        {
                          Int2 val;
                          input.Read<Int2>(name, out val);
                          p.SetValue<Int2>(val);
                        }
                        break;
                      case 3:
                        {
                          Int3 val;
                          input.Read<Int3>(name, out val);
                          p.SetValue<Int3>(val);
                        }
                        break;
                      case 4:
                        {
                          Int4 val;
                          input.Read<Int4>(name, out val);
                          p.SetValue<Int4>(val);
                        }
                        break;
                    }
                    break;
                  case EffectParameterType.Single:
                    switch (p.ColumnCount)
                    {
                      case 1:
                        {
                          p.SetValue<float>(input.ReadSingle(name));
                        }
                        break;
                      case 2:
                        {
                          Vector2 val;
                          input.Read<Vector2>(name, out val);
                          p.SetValue<Vector2>(val);
                        }
                        break;
                      case 3:
                        {
                          Vector3 val;
                          input.Read<Vector3>(name, out val);
                          p.SetValue<Vector3>(val);
                        }
                        break;
                      case 4:
                        {
                          Vector4 val;
                          input.Read<Vector4>(name, out val);
                          p.SetValue<Vector4>(val);
                        }
                        break;
                    }
                    break;
                }
              }
              break;
          }
        }
      }
    }

    private void WriteEffectParameterData(IEffectParameter p, ISavableWriter output, String overrideName = null)
    {
      //Maybe have a single helper class do this...this will require changing if we add new parameter types
      //Also, this will error if the effect parameter changed...we don't do any validation
      if (p.IsArray)
      {
        EffectParameterCollection elems = p.Elements;
        output.BeginWriteGroup(p.Name, elems.Count);

        for (int i = 0; i < elems.Count; i++)
          WriteEffectParameterData(elems[i], output, "Element"); //Override the name of each element

        output.EndWriteGroup();
      }
      else
      {
        String name = (String.IsNullOrEmpty(overrideName)) ? p.Name : overrideName;

        if (p.StructureMembers.Count > 0)
        {
          EffectParameterCollection structMembers = p.StructureMembers;

          output.BeginWriteGroup(name, structMembers.Count);

          for (int i = 0; i < structMembers.Count; i++)
            WriteEffectParameterData(structMembers[i], output); //Do not override the name of any structure members

          output.EndWriteGroup();
        }
        else
        {
          switch (p.ParameterClass)
          {
            case EffectParameterClass.Object:
              switch (p.ParameterType)
              {
                case EffectParameterType.SamplerState:
                  output.WriteSharedSavable<SamplerState>(name, p.GetResource<SamplerState>());
                  break;
                case EffectParameterType.Texture:
                case EffectParameterType.Texture1D:
                case EffectParameterType.Texture1DArray:
                case EffectParameterType.Texture2D:
                case EffectParameterType.Texture2DArray:
                case EffectParameterType.Texture2DMS:
                case EffectParameterType.Texture2DMSArray:
                case EffectParameterType.Texture3D:
                case EffectParameterType.TextureCube:
                case EffectParameterType.TextureCubeArray:
                  output.WriteExternalSavable<Texture>(name, p.GetResource<Texture>());
                  break;
              }
              break;
            case EffectParameterClass.MatrixColumns:
            case EffectParameterClass.MatrixRows:
              if (p.ColumnCount == 4 && p.RowCount == 4)
              {
                switch (p.ParameterType)
                {
                  case EffectParameterType.Single:
                    output.Write<Matrix>(name, p.GetMatrixValue());
                    break;
                }
              }
              break;
            case EffectParameterClass.Scalar:
            case EffectParameterClass.Vector:
              if (p.RowCount == 1)
              {
                switch (p.ParameterType)
                {
                  case EffectParameterType.Bool:
                    switch (p.ColumnCount)
                    {
                      case 1:
                        output.Write<Bool>(name, p.GetValue<Bool>());
                        break;
                      case 2:
                        output.Write<Bool2>(name, p.GetValue<Bool2>());
                        break;
                      case 3:
                        output.Write<Bool3>(name, p.GetValue<Bool3>());
                        break;
                      case 4:
                        output.Write<Bool4>(name, p.GetValue<Bool4>());
                        break;
                    }
                    break;
                  case EffectParameterType.Int32:
                    switch (p.ColumnCount)
                    {
                      case 1:
                        output.Write(name, p.GetValue<int>());
                        break;
                      case 2:
                        output.Write<Int2>(name, p.GetValue<Int2>());
                        break;
                      case 3:
                        output.Write<Int3>(name, p.GetValue<Int3>());
                        break;
                      case 4:
                        output.Write<Int4>(name, p.GetValue<Int4>());
                        break;
                    }
                    break;
                  case EffectParameterType.Single:
                    switch (p.ColumnCount)
                    {
                      case 1:
                        output.Write(name, p.GetValue<float>());
                        break;
                      case 2:
                        output.Write<Vector2>(name, p.GetValue<Vector2>());
                        break;
                      case 3:
                        output.Write<Vector3>(name, p.GetValue<Vector3>());
                        break;
                      case 4:
                        output.Write<Vector4>(name, p.GetValue<Vector4>());
                        break;
                    }
                    break;
                }
              }
              break;
          }
        }
      }
    }

    #endregion

    #region Binding Objects

    /// <summary>
    /// A binding between a computed parameter provider and an effect parameter instance.
    /// </summary>
    public sealed class ComputedParameterBinding
    {
      private String m_name;
      private IComputedParameterProvider m_provider;
      private IEffectParameter m_parameter;
      private Object m_localState;
      private bool m_isValid;
      private bool m_isInherited;

      /// <summary>
      /// Gets the parameter name that is being bound.
      /// </summary>
      public String Name
      {
        get
        {
          return m_name;
        }
      }

      /// <summary>
      /// Gets the computed parameter provider.
      /// </summary>
      public IComputedParameterProvider Provider
      {
        get
        {
          return m_provider;
        }
      }

      /// <summary>
      /// Gets the effect parameter instance.
      /// </summary>
      public IEffectParameter Parameter
      {
        get
        {
          return m_parameter;
        }
      }

      /// <summary>
      /// Gets if the binding is valid.
      /// </summary>
      public bool IsValid
      {
        get
        {
          return m_isValid;
        }
      }

      /// <summary>
      /// Gets or sets if the binding is inherited from a parent script.  If inherited, then it won't be written out during script serialization.
      /// </summary>
      public bool IsInherited
      {
        get
        {
          return m_isInherited;
        }
        set
        {
          m_isInherited = value;
        }
      }

      /// <summary>
      /// Gets or sets the local state object used to pass information to the provider.
      /// </summary>
      public Object LocalState
      {
        get
        {
          return m_localState;
        }
        set
        {
          m_localState = value;
        }
      }

      /// <summary>
      /// Constructs a new instance of the <see cref="ComputedParameterBinding"/> class.
      /// </summary>
      /// <param name="name">Name of parameter to bind.</param>
      /// <param name="provider">Computed parameter provider.</param>
      /// <param name="parameter">Effect parameter instance.</param>
      /// <param name="isInherited">Optional metadata for if the binding is inherited from a parent material.</param>
      public ComputedParameterBinding(String name, IComputedParameterProvider provider, IEffectParameter parameter, bool isInherited = false)
      {
        m_name = name;
        m_provider = provider;
        m_parameter = parameter;
        m_isInherited = isInherited;

        Validate();
      }

      private void Validate()
      {
        m_isValid = m_parameter != null && m_provider != null && m_provider.ValidateParameter(m_parameter) &&
            !String.IsNullOrEmpty(m_name) && m_parameter.Name.Equals(m_name);
      }
    }

    /// <summary>
    /// A binding between a material script parameter and an effect parameter instance.
    /// </summary>
    public sealed class ScriptParameterBinding
    {
      private String m_name;
      private IEffectParameter m_parameter;
      private Type m_dataType;
      private bool m_isValid;
      private bool m_isInherited;

      /// <summary>
      /// Gets the parameter name that is being bound.
      /// </summary>
      public String Name
      {
        get
        {
          return m_name;
        }
      }

      /// <summary>
      /// Gets the effect parameter instance.
      /// </summary>
      public IEffectParameter Parameter
      {
        get
        {
          return m_parameter;
        }
      }

      /// <summary>
      /// Gets if the binding is valid.
      /// </summary>
      public bool IsValid
      {
        get
        {
          return m_isValid;
        }
      }

      /// <summary>
      /// Gets or sets if the binding is inherited from a parent script. If inherited, then it won't be written out during script serialization.
      /// </summary>
      public bool IsInherited
      {
        get
        {
          return m_isInherited;
        }
        set
        {
          m_isInherited = value;
        }
      }

      /// <summary>
      /// Gets the bound data type. This might be different than the default type of the effect parameter.
      /// </summary>
      public Type DataType
      {
        get
        {
          return m_dataType;
        }
        internal set
        {
          m_dataType = value;
        }
      }

      /// <summary>
      /// Constructs a new instance of the <see cref="ScriptParameterBinding"/> class.
      /// </summary>
      /// <param name="name">Name of parameter to bind.</param>
      /// <param name="parameter">Effect parameter instance.</param>
      /// <param name="isInherited">Optional metadata for if the binding is inherited from a parent material.</param>
      public ScriptParameterBinding(String name, IEffectParameter parameter, bool isInherited = false)
      {
        m_name = name;
        m_parameter = parameter;
        m_dataType = null;
        m_isInherited = isInherited;

        Validate();
      }

      private void Validate()
      {
        m_isValid = m_parameter != null && !String.IsNullOrEmpty(m_name) && m_parameter.Name.Equals(m_name);

        if (m_isValid)
          m_dataType = m_parameter.DefaultNetType;
      }
    }

    #endregion
  }
}
