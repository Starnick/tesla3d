﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Diagnostics;
using Tesla.Content;

namespace Tesla.Graphics
{
    /// <summary>
    /// A collection for material definitions, typically representing a set of materials found in a single TEMD script file.
    /// </summary>
    [SavableVersion(1)]
    [DebuggerDisplay("ScriptName = {ScriptFileName}, Count = {Count}")]
    public sealed class MaterialDefinitionScriptCollection : NamedList<MaterialDefinition>, ISavable, IContentCastable
    {
        private String m_scriptFileName = String.Empty;

        /// <summary>
        /// Gets or sets the material script file name.
        /// </summary>
        public String ScriptFileName
        {
            get
            {
                return m_scriptFileName;
            }
            set
            {
                m_scriptFileName = value;
                if(String.IsNullOrEmpty(value))
                    m_scriptFileName = String.Empty;
            }
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="MaterialDefinitionScriptCollection"/> class.
        /// </summary>
        public MaterialDefinitionScriptCollection() : base() { }


        /// <summary>
        /// Constructs a new instance of the <see cref="MaterialDefinitionScriptCollection"/> class.
        /// </summary>
        /// <param name="capacity">Initial capacity of the collection.</param>
        public MaterialDefinitionScriptCollection(int capacity) : base(capacity) { }

        /// <summary>
        /// Constructs a new instance of the <see cref="MaterialDefinitionScriptCollection"/> class.
        /// </summary>
        /// <param name="matDefs">Collection of materials to add.</param>
        public MaterialDefinitionScriptCollection(IEnumerable<MaterialDefinition> matDefs) : base(matDefs) { }

        /// <summary>
        /// Constructs a new instance of the <see cref="MaterialDefinitionScriptCollection"/> class.
        /// </summary>
        /// <param name="matDefs">Collection of materials to add.</param>
        public MaterialDefinitionScriptCollection(params MaterialDefinition[] matDefs) : base(matDefs) { }

        /// <summary>
        /// Reads the object data from the input.
        /// </summary>
        /// <param name="input">Savable reader</param>
        public void Read(ISavableReader input)
        {
            int count = input.ReadInt32("Count");
            Capacity = count;

            for(int i = 0; i < count; i++)
            {
                MaterialDefinition matDef = input.ReadSavable<MaterialDefinition>("MaterialDefinition");
                if(matDef != null)
                    Add(matDef);
            }
        }

        /// <summary>
        /// Writes the object data to the output.
        /// </summary>
        /// <param name="output">Savable writer</param>
        public void Write(ISavableWriter output)
        {
            output.Write("Count", Count);

            for(int i = 0; i < Count; i++)
                output.WriteSavable<MaterialDefinition>("MaterialDefinition", this[i]);
        }

        /// <summary>
        /// Attempts to cast the content item to another type.
        /// </summary>
        /// <param name="targetType">Type to cast to.</param>
        /// <param name="subresourceName">Optional subresource name.</param>
        /// <returns>Casted type or null if the type could not be converted.</returns>
        Object IContentCastable.CastTo(Type targetType, String subresourceName)
        {
            if(targetType == typeof(MaterialDefinition))
            {
                if(Count > 0)
                {
                    //If has a subresource name, search for that material definition (may be null), otherwise return the first material
                    if(!String.IsNullOrEmpty(subresourceName))
                    {
                        return this[subresourceName];
                    }
                    else
                    {
                        return this[0];
                    }
                }

                return null;
            }

            return this;
        }
    }
}
