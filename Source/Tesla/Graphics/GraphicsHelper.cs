﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// General graphics helper.
  /// </summary>
  public static class GraphicsHelper
  {
    /// <summary>
    /// Gets a render system from the service provider.
    /// </summary>
    /// <param name="serviceProvider">Service provider.</param>
    /// <returns>Render system that the service provider owns.</returns>
    /// <exception cref="ArgumentNullException">Thrown if no render system was found in the service provider.</exception>
    public static IRenderSystem GetRenderSystem(IServiceProvider serviceProvider)
    {
      IRenderSystem? renderSystem = serviceProvider?.GetService(typeof(IRenderSystem)) as IRenderSystem;
      ThrowIfRenderSystemNull(renderSystem);

      return renderSystem;
    }

    /// <summary>
    /// Throws if the render system argument is null.
    /// </summary>
    /// <param name="renderSystem">Render system.</param>
    /// <exception cref="ArgumentNullException">Thrown if the argument is null.</exception>
    public static void ThrowIfRenderSystemNull([NotNull] IRenderSystem? renderSystem)
    {
      if (renderSystem is null)
        throw new ArgumentNullException(nameof(renderSystem), StringLocalizer.Instance.GetLocalizedString("RenderSystemNull"));
    }

    /// <summary>
    /// Throws if the render context argument is null.
    /// </summary>
    /// <param name="renderSystem">Render context.</param>
    /// <exception cref="ArgumentNullException">Thrown if the argument is null.</exception>
    public static void ThrowIfRenderContextNull([NotNull] IRenderContext? renderContext)
    {
      if (renderContext is null)
        throw new ArgumentNullException(nameof(renderContext), StringLocalizer.Instance.GetLocalizedString("NullRenderContext"));
    }

    /// <summary>
    /// Resolves draw parameters for a submesh. This does not validate parameters and is agnostic for indexed and non-indexed drawing (the latter
    /// can ignore the base vertex offset).
    /// </summary>
    /// <param name="meshData">Mesh data.</param>
    /// <param name="range">Range in the mesh geometry buffers.</param>
    /// <param name="offset">Offset in the vertex / index buffer</param>
    /// <param name="count">Number of vertices.</param>
    /// <param name="baseVertexOffset">Offset to add to an index buffer value to read a vertex buffer value.</param>
    public static void GetMeshDrawParameters(MeshData meshData, in SubMeshRange? range, out int offset, out int count, out int baseVertexOffset)
    {
      offset = 0;
      count = 0;
      baseVertexOffset = 0;

      if (range.HasValue)
      {
        SubMeshRange meshRange = range.Value;
        offset = meshRange.Offset;
        count = meshRange.Count;
        baseVertexOffset = meshRange.BaseVertexOffset;
      }
      else if (meshData != null)
      {
        count = (meshData.UseIndexedPrimitives) ? meshData.IndexCount : meshData.VertexCount;
      }
    }

    /// <summary>
    /// Gets a "best guess" default .NET (or engine) type that represents the type of data that can be set an effect parameter that
    /// has the specified class and type.
    /// </summary>
    /// <param name="classType">Parameter type, e.g. bool, int, float</param>
    /// <param name="paramType">Parameter class, e.g. scale, vector, resource.</param>
    /// <param name="columnCount">If a vector/matrix class, the number of values, e.g. float2, float3, int4.</param>
    /// <returns>.NET default type that represents the data (or at least, the size) the effect parameter expects.</returns>
    public static Type GetEffectParameterDefaultDataType(EffectParameterClass classType, EffectParameterType paramType, int columnCount = 1)
    {
      switch (classType)
      {
        case EffectParameterClass.Scalar:
        case EffectParameterClass.Vector:
          switch (paramType)
          {
            case EffectParameterType.Bool:
              switch (columnCount)
              {
                case 1:
                  return typeof(Bool);
                case 2:
                  return typeof(Bool2);
                case 3:
                  return typeof(Bool3);
                case 4:
                  return typeof(Bool4);
              }
              break;
            case EffectParameterType.Int32:
              switch (columnCount)
              {
                case 1:
                  return typeof(int);
                case 2:
                  return typeof(Int2);
                case 3:
                  return typeof(Int3);
                case 4:
                  return typeof(Int4);
              }
              break;
            case EffectParameterType.Single:
              switch (columnCount)
              {
                case 1:
                  return typeof(float);
                case 2:
                  return typeof(Vector2);
                case 3:
                  return typeof(Vector3);
                case 4:
                  return typeof(Vector4);
              }
              break;
          }
          break;
        case EffectParameterClass.MatrixColumns:
        case EffectParameterClass.MatrixRows:
          return typeof(Matrix); //Row/Column count may not be 4x4 though
        case EffectParameterClass.Object:
          switch (paramType)
          {
            case EffectParameterType.SamplerState:
              return typeof(SamplerState);
            case EffectParameterType.Texture1D:
              return typeof(Texture1D);
            case EffectParameterType.Texture1DArray:
              return typeof(Texture1DArray);
            case EffectParameterType.Texture2D:
            case EffectParameterType.Texture2DMS:
              return typeof(Texture2D);
            case EffectParameterType.Texture2DArray:
            case EffectParameterType.Texture2DMSArray:
              return typeof(Texture2DArray);
            case EffectParameterType.Texture3D:
              return typeof(Texture3D);
            case EffectParameterType.TextureCube:
              return typeof(TextureCube);
            case EffectParameterType.TextureCubeArray:
              return typeof(TextureCubeArray);
            case EffectParameterType.Texture:
              return typeof(Texture); //I don't think this will ever happen
            case EffectParameterType.Buffer:
              return typeof(ShaderBuffer);
            case EffectParameterType.StructuredBuffer:
              return typeof(StructuredShaderBuffer);
            case EffectParameterType.ByteAddressBuffer:
              return typeof(VertexBuffer);
          }
          break;
        case EffectParameterClass.Struct:
          switch (paramType)
          {
            case EffectParameterType.Void:
              return typeof(void);
          }
          break;
      }

      // Should not happen unless if we forgot to add types...
      throw new ArgumentException("Missing type for effect parameter.");
    }

    /// <summary>
    /// Validates a span of buffers that represent individual vertex attributes.
    /// </summary>
    /// <param name="vertexLayout">Defines the layout of a vertex.</param>
    /// <param name="data">One or more vertex attributes. These can match the element type or be raw buffers.</param>
    /// <param name="vertexCount">Optional vertex count. If null, the first buffer element count defines the number of vertices.</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the buffers do not match the vertex layout or size.</exception>
    /// <exception cref="ArgumentNullException">Thrown if the vertex layout is null or if any of the buffers are null.</exception>
    public static void ValidateInterleavedData(VertexLayout vertexLayout, ReadOnlySpan<IReadOnlyDataBuffer> data, int? vertexCount = null)
    {
      if (vertexLayout is null)
        throw new ArgumentNullException(nameof(vertexLayout), StringLocalizer.Instance.GetLocalizedString("VertexLayoutNull"));

      if (data.IsEmpty)
        throw new ArgumentNullException(nameof(data), StringLocalizer.Instance.GetLocalizedString("DataBufferNull"));

      int totalSize = 0;
      int vertexStride = 0;
      int numVertices = vertexCount ?? CalculateVertexCount(vertexLayout, data);

      if (data.Length > 1)
      {
        // Verify the incoming vertex attribute databuffers match the layout
        if (data.Length != vertexLayout.ElementCount)
          throw new ArgumentOutOfRangeException(nameof(data), StringLocalizer.Instance.GetLocalizedString("VertexLayoutElementCountMismatch"));

        for (int i = 0; i < data.Length; i++)
        {
          IReadOnlyDataBuffer? db = data[i];
          if (db is null || db.Length == 0)
            throw new ArgumentNullException(nameof(data), StringLocalizer.Instance.GetLocalizedString("DataBufferNull"));

          // Data buffer may be raw bytes or it may be strongly typed
          VertexElement element = vertexLayout[i];
          int vSizeInBytes = element.Format.SizeInBytes();
          int vertAttributeCount = BufferHelper.ComputeElementCount(db.SizeInBytes, vSizeInBytes);
          if (vertAttributeCount != numVertices)
            throw new ArgumentOutOfRangeException(nameof(data), StringLocalizer.Instance.GetLocalizedString("VertexCountMismatch"));

          totalSize += db.SizeInBytes;
          vertexStride += vSizeInBytes;
        }
      } 
      else
      {
        // Verify the incoming interleaved buffer exists
        IReadOnlyDataBuffer? interleaved = data[0];
        if (interleaved is null || interleaved.Length == 0)
          throw new ArgumentNullException(nameof(data), StringLocalizer.Instance.GetLocalizedString("DataBufferNull"));

        totalSize = interleaved.SizeInBytes;
        vertexStride = vertexLayout.VertexStride;
      }

      // Total size and stride must match the layout
      if ((totalSize != (vertexLayout.VertexStride * numVertices)) || (vertexStride != vertexLayout.VertexStride))
        throw new ArgumentOutOfRangeException(nameof(data), StringLocalizer.Instance.GetLocalizedString("VertexBufferSizeMismatch"));

      //Vertex stride should divide evenly into the total buffer size
      if (totalSize % vertexStride != 0 || BufferHelper.ComputeElementCount(totalSize, vertexStride) != numVertices)
        throw new ArgumentOutOfRangeException(nameof(data), StringLocalizer.Instance.GetLocalizedString("VertexLayoutStrideMismatch"));
    }

    /// <summary>
    /// Computes the number of vertices represented by the data - either a single buffer representing the interleaved
    /// vertex data, or multiple buffers for each vertex attribute. If the latter, it must be 1:1 to the vertex layout.
    /// </summary>
    /// <param name="vertexLayout">Layout of a single vertex.</param>
    /// <param name="data">One or more buffers representing the data.</param>
    /// <returns>Number of vertices.</returns>
    public static int CalculateVertexCount(VertexLayout vertexLayout, ReadOnlySpan<IReadOnlyDataBuffer> data)
    {
      if (vertexLayout is null || data.Length == 0)
        return 0;

      IReadOnlyDataBuffer? firstBuffer = data[0];
      if (firstBuffer is null)
        return 0;

      int stride;

      // Single buffer means it's interleaved. Use the total vertex strie
      if (data.Length == 1)
      {
        stride = vertexLayout.VertexStride;
      } 
      else
      {
        // Otherwise use the size of the first vertex attribute
        if (vertexLayout.ElementCount == 0)
          return 0;

        stride = vertexLayout[0].Format.SizeInBytes();
      }

      return BufferHelper.ComputeElementCount(firstBuffer.SizeInBytes, stride);
    }
  }
}
