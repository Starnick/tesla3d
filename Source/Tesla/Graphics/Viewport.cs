﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Tesla.Content;

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// Defines a 2D region that a 3D rendering is projected onto where the positive X axis is right and positive Y axis is down.
  /// </summary>
  [Serializable]
  [StructLayout(LayoutKind.Sequential)]
  public struct Viewport : IEquatable<Viewport>, IRefEquatable<Viewport>, IPrimitiveValue
  {
    /// <summary>
    /// Top left X coordinate of the viewport.
    /// </summary>
    public int X;

    /// <summary>
    /// Top left Y coordinate of the viewport.
    /// </summary>
    public int Y;

    /// <summary>
    /// Width of the viewport.
    /// </summary>
    public int Width;

    /// <summary>
    /// Height of the viewport.
    /// </summary>
    public int Height;

    /// <summary>
    /// Minimum Z depth.
    /// </summary>
    public float MinDepth;

    /// <summary>
    /// Maximum Z depth.
    /// </summary>
    public float MaxDepth;

    private static readonly int s_sizeInBytes = BufferHelper.SizeOf<Viewport>();

    /// <summary>
    /// Gets the size of the viewport type in bytes.
    /// </summary>
    public static int SizeInBytes
    {
      get
      {
        return s_sizeInBytes;
      }
    }

    /// <summary>
    /// Gets or sets the bounds of the viewport as a rectangle.
    /// </summary>
    public Rectangle Bounds
    {
      readonly get
      {
        return new Rectangle(X, Y, Width, Height);
      }
      set
      {
        X = value.X;
        Y = value.Y;
        Width = value.Width;
        Height = value.Height;
      }
    }

    /// <summary>
    /// Gets the aspect ratio of the viewport, which is width divided by height.
    /// </summary>
    public readonly float AspectRatio
    {
      get
      {
        if (Height != 0 && Width != 0)
          return (float) Width / (float) Height;

        return 0.0f;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Viewport"/> struct.
    /// </summary>
    /// <param name="x">Top left X coordinate</param>
    /// <param name="y">Top left Y coordinate</param>
    /// <param name="width">Width of the viewport</param>
    /// <param name="height">Height of the viewport</param>
    public Viewport(int x, int y, int width, int height)
    {
      X = x;
      Y = y;
      Width = width;
      Height = height;
      MinDepth = 0.0f;
      MaxDepth = 1.0f;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Viewport"/> struct.
    /// </summary>
    /// <param name="x">Top left X coordinate</param>
    /// <param name="y">Top left Y coordinate</param>
    /// <param name="width">Width of the viewport</param>
    /// <param name="height">Height of the viewport</param>
    /// <param name="minDepth">Minimum depth</param>
    /// <param name="maxDepth">Maximum depth</param>
    public Viewport(int x, int y, int width, int height, float minDepth, float maxDepth)
    {
      X = x;
      Y = y;
      Width = width;
      Height = height;
      MinDepth = minDepth;
      MaxDepth = maxDepth;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Viewport"/> struct. Min depth is set to 0.0 and Max depth to 1.0.
    /// </summary>
    /// <param name="bounds">The rectangle that represents the bounds of the viewport.</param>
    public Viewport(in Rectangle bounds)
    {
      X = bounds.X;
      Y = bounds.Y;
      Width = bounds.Width;
      Height = bounds.Height;
      MinDepth = 0.0f;
      MaxDepth = 1.0f;
    }

    /// <summary>
    /// Tests equality between two viewports.
    /// </summary>
    /// <param name="a">First viewport</param>
    /// <param name="b">Second viewport</param>
    /// <returns>True if the two viewports are equal, false otherwise.</returns>
    public static bool operator ==(in Viewport a, in Viewport b)
    {
      return (a.X == b.X) && (a.Y == b.Y) && (a.Width == b.Width) && (a.Height == b.Height) &&
          MathHelper.IsEqual(a.MinDepth, b.MinDepth) && MathHelper.IsEqual(a.MaxDepth, b.MaxDepth);
    }

    /// <summary>
    /// Tests inequality between two viewports.
    /// </summary>
    /// <param name="a">First viewport</param>
    /// <param name="b">Second viewport</param>
    /// <returns>True if the two viewports are not equal, false otherwise.</returns>
    public static bool operator !=(in Viewport a, in Viewport b)
    {
      return (a.X != b.X) || (a.Y != b.Y) || (a.Width != b.Width) || (a.Height != b.Height) ||
          !MathHelper.IsEqual(a.MinDepth, b.MinDepth) || !MathHelper.IsEqual(a.MaxDepth, b.MaxDepth);
    }

    /// <summary>
    /// Projects a 3D vector from object space to screen space.
    /// </summary>
    /// <param name="source">Vector to project</param>
    /// <param name="worldMatrix">World matrix</param>
    /// <param name="viewMatrix">View matrix</param>
    /// <param name="projMatrix">Projection matrix</param>
    /// <returns>The projected vector in screen space.</returns>
    public readonly Vector3 Project(in Vector3 source, in Matrix worldMatrix, in Matrix viewMatrix, in Matrix projMatrix)
    {
      Matrix wv;
      Matrix wvp;

      Matrix.Multiply(worldMatrix, viewMatrix, out wv);
      Matrix.Multiply(wv, projMatrix, out wvp);

      Vector3 result;
      Project(source, wvp, out result);

      return result;
    }

    /// <summary>
    /// Projects a 3D vector from object space to screen space.
    /// </summary>
    /// <param name="source">Vector to project</param>
    /// <param name="worldMatrix">World matrix</param>
    /// <param name="viewMatrix">View matrix</param>
    /// <param name="projMatrix">Projection matrix</param>
    /// <param name="result">The projected vector in screen space.</param>
    public readonly void Project(in Vector3 source, in Matrix worldMatrix, in Matrix viewMatrix, in Matrix projMatrix, out Vector3 result)
    {
      Matrix wv;
      Matrix wvp;

      Matrix.Multiply(worldMatrix, viewMatrix, out wv);
      Matrix.Multiply(wv, projMatrix, out wvp);

      Project(source, wvp, out result);
    }

    /// <summary>
    /// Projects a 3D vector from object space to screen space.
    /// </summary>
    /// <param name="source">Vector to project</param>
    /// <param name="worldViewProjectionMatrix">The World-View-Projection matrix</param>
    /// <returns>The projected vector in screen space.</returns>
    public readonly Vector3 Project(in Vector3 source, in Matrix worldViewProjectionMatrix)
    {
      Vector3 result;
      Vector3.Transform(source, worldViewProjectionMatrix, out result);

      float w = (source.X * worldViewProjectionMatrix.M14) + (source.Y * worldViewProjectionMatrix.M24) +
          (source.Z * worldViewProjectionMatrix.M34) + worldViewProjectionMatrix.M44;

      if (!MathHelper.WithinEpsilon(w, 1.0f))
        Vector3.Divide(result, w, out result);

      result.X = (((result.X + 1.0f) * 0.5f) * Width) + X;
      result.Y = (((-result.Y + 1.0f) * 0.5f) * Height) + Y;
      result.Z = (result.Z * (MaxDepth - MinDepth)) + MinDepth;

      return result;
    }

    /// <summary>
    /// Projects a 3D vector from object space to screen space.
    /// </summary>
    /// <param name="source">Vector to project</param>
    /// <param name="worldViewProjectionMatrix">The World-View-Projection matrix</param>
    /// <param name="result">The projected vector in screen space.</param>
    public readonly void Project(in Vector3 source, in Matrix worldViewProjectionMatrix, out Vector3 result)
    {
      Vector3.Transform(source, worldViewProjectionMatrix, out result);

      float w = (source.X * worldViewProjectionMatrix.M14) + (source.Y * worldViewProjectionMatrix.M24) +
          (source.Z * worldViewProjectionMatrix.M34) + worldViewProjectionMatrix.M44;

      if (!MathHelper.WithinEpsilon(w, 1.0f))
        Vector3.Divide(result, w, out result);

      result.X = (((result.X + 1.0f) * 0.5f) * Width) + X;
      result.Y = (((-result.Y + 1.0f) * 0.5f) * Height) + Y;
      result.Z = (result.Z * (MaxDepth - MinDepth)) + MinDepth;
    }

    /// <summary>
    /// Converts a point in screen space to a point in object space.
    /// </summary>
    /// <param name="source">Vector to un-project (in screen coordinates)</param>
    /// <param name="worldMatrix">World matrix</param>
    /// <param name="viewMatrix">View matrix</param>
    /// <param name="projMatrix">Projection matrix</param>
    /// <returns>The un-projected vector in object space.</returns>
    public readonly Vector3 UnProject(in Vector3 source, in Matrix worldMatrix, in Matrix viewMatrix, in Matrix projMatrix)
    {
      Matrix wv;
      Matrix wvp;

      Matrix.Multiply(worldMatrix, viewMatrix, out wv);
      Matrix.Multiply(wv, projMatrix, out wvp);

      Vector3 result;
      UnProject(source, wvp, out result);

      return result;
    }

    /// <summary>
    /// Converts a point in screen space to a point in object space.
    /// </summary>
    /// <param name="source">Vector to un-project (in screen coordinates)</param>
    /// <param name="worldMatrix">World matrix</param>
    /// <param name="viewMatrix">View matrix</param>
    /// <param name="projMatrix">Projection matrix</param>
    /// <param name="result">The un-projected vector in object space.</param>
    public readonly void UnProject(in Vector3 source, in Matrix worldMatrix, in Matrix viewMatrix, in Matrix projMatrix, out Vector3 result)
    {
      Matrix wv;
      Matrix wvp;

      Matrix.Multiply(worldMatrix, viewMatrix, out wv);
      Matrix.Multiply(wv, projMatrix, out wvp);

      UnProject(source, wvp, out result);
    }

    /// <summary>
    /// Converts a point in screen space to a point in object space.
    /// </summary>
    /// <param name="source">Vector to un-project (in screen coordinates)</param>
    /// <param name="worldViewProjectionMatrix">The World-View-Projection matrix</param>
    /// <returns>The un-projected vector in object space.</returns>
    public readonly Vector3 UnProject(in Vector3 source, in Matrix worldViewProjectionMatrix)
    {
      Matrix invWvp;
      Matrix.Invert(worldViewProjectionMatrix, out invWvp);

      Vector3 temp;
      temp.X = (((source.X - X) / ((float) Width)) * 2.0f) - 1.0f;
      temp.Y = -((((source.Y - Y) / ((float) Height)) * 2.0f) - 1.0f);
      temp.Z = (source.Z - MinDepth) / (MaxDepth - MinDepth);

      Vector3 result;
      Vector3.Transform(temp, invWvp, out result);

      float w = (temp.X * invWvp.M14) + (temp.Y * invWvp.M24) + (temp.Z * invWvp.M34) + invWvp.M44;
      if (!MathHelper.WithinEpsilon(w, 1.0f))
        Vector3.Divide(result, w, out result);

      return result;
    }

    /// <summary>
    /// Converts a point in screen space to a point in object space.
    /// </summary>
    /// <param name="source">Vector to un-project (in screen coordinates)</param>
    /// <param name="worldViewProjectionMatrix">The World-View-Projection matrix</param>
    /// <param name="result">The un-projected vector in object space.</param>
    public readonly void UnProject(in Vector3 source, in Matrix worldViewProjectionMatrix, out Vector3 result)
    {
      Matrix invWvp;
      Matrix.Invert(worldViewProjectionMatrix, out invWvp);

      Vector3 temp;
      temp.X = (((source.X - X) / ((float) Width)) * 2.0f) - 1.0f;
      temp.Y = -((((source.Y - Y) / ((float) Height)) * 2.0f) - 1.0f);
      temp.Z = (source.Z - MinDepth) / (MaxDepth - MinDepth);

      Vector3.Transform(temp, invWvp, out result);

      float w = (temp.X * invWvp.M14) + (temp.Y * invWvp.M24) + (temp.Z * invWvp.M34) + invWvp.M44;
      if (!MathHelper.WithinEpsilon(w, 1.0f))
        Vector3.Divide(result, w, out result);
    }

    /// <summary>
    /// Determines whether the specified <see cref="System.Object" /> is equal to this instance.
    /// </summary>
    /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
    /// <returns>True if the specified <see cref="System.Object" /> is equal to this instance; otherwise, false.</returns>
    public readonly override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is Viewport)
        return Equals((Viewport) obj);

      return false;
    }

    /// <summary>
    /// Tests equality between the viewport and another viewport.
    /// </summary>
    /// <param name="other">Viewport to test</param>
    /// <returns>True if equal, false otherwise.</returns>
    readonly bool IEquatable<Viewport>.Equals(Viewport other)
    {
      return (X == other.X) && (Y == other.Y) && (Width == other.Width) && (Height == other.Height) &&
          MathHelper.IsEqual(MinDepth, other.MinDepth) && MathHelper.IsEqual(MaxDepth, other.MaxDepth);
    }

    /// <summary>
    /// Tests equality between the viewport and another viewport.
    /// </summary>
    /// <param name="other">Viewport to test</param>
    /// <returns>True if equal, false otherwise.</returns>
    public readonly bool Equals(in Viewport other)
    {
      return (X == other.X) && (Y == other.Y) && (Width == other.Width) && (Height == other.Height) &&
          MathHelper.IsEqual(MinDepth, other.MinDepth) && MathHelper.IsEqual(MaxDepth, other.MaxDepth);
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.</returns>
    public readonly override int GetHashCode()
    {
      unchecked
      {
        return X.GetHashCode() + Y.GetHashCode() + Width.GetHashCode() + Height.GetHashCode() + MaxDepth.GetHashCode() + MinDepth.GetHashCode();
      }
    }

    /// <summary>
    /// Returns the fully qualified type name of this instance.
    /// </summary>
    /// <returns>A <see cref="T:System.String" /> containing a fully qualified type name.</returns>
    public readonly override string ToString()
    {
      CultureInfo info = CultureInfo.CurrentCulture;
      return String.Format(info, "X:{0} Y:{1} Width:{2} Height:{3} MinDepth:{4} MaxDepth:{5}",
          new object[] { X.ToString(info), Y.ToString(info), Width.ToString(info), Height.ToString(info), MinDepth.ToString(info), MaxDepth.ToString(info) });
    }

    /// <summary>
    /// Writes the primitive data to the output.
    /// </summary>
    /// <param name="output">Primitive writer.</param>
    public readonly void Write(IPrimitiveWriter output)
    {
      output.Write("X", X);
      output.Write("Y", Y);
      output.Write("Width", Width);
      output.Write("Height", Height);
      output.Write("MinDepth", MinDepth);
      output.Write("MaxDepth", MaxDepth);
    }

    /// <summary>
    /// Reads the primitive data from the input.
    /// </summary>
    /// <param name="input">Primitive reader.</param>
    public void Read(IPrimitiveReader input)
    {
      X = input.ReadInt32("X");
      Y = input.ReadInt32("Y");
      Width = input.ReadInt32("Width");
      Height = input.ReadInt32("Height");
      MinDepth = input.ReadSingle("MinDepth");
      MaxDepth = input.ReadSingle("MaxDepth");
    }
  }
}
