﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Content;
using Tesla.Utilities;

namespace Tesla.Graphics
{
    /// <summary>
    /// Base class for all render properties. These properties are holders for bits of information that may be used by interested parties
    /// (such as the Material computed parameters) when an object is rendered. Each property has a unique ID associated and is based on the property type,
    /// allowing for fast look up and organization.
    /// </summary>
    public abstract class RenderProperty
    {
        private RenderPropertyID m_id;

        /// <summary>
        /// Gets the unique ID value for this property.
        /// </summary>
        public RenderPropertyID ID
        {
            get
            {
                return m_id;
            }
            protected set 
            {
                m_id = value;
            }
        }

        /// <summary>
        /// Gets if the property can be cloned.
        /// </summary>
        public virtual bool CanClone
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="RenderProperty"/> class.
        /// </summary>
        /// <param name="id">The unique ID value.</param>
        protected RenderProperty(RenderPropertyID id)
        {
            m_id = id;
        }

        /// <summary>
        /// Queries the unique ID value based on property type.
        /// </summary>
        /// <typeparam name="Property">Type of property.</typeparam>
        /// <returns>Associated render property of the type.</returns>
        public static RenderPropertyID GetPropertyID<Property>()
        {
            return RenderPropertyIDHolder<Property>.ID;
        }

        /// <summary>
        /// Checks if the render property and this render property are the same, that is, have the same ID.
        /// </summary>
        /// <param name="property">Other property to check if it is the same type.</param>
        /// <returns>True if both properties share the same ID, false otherwise.</returns>
        public bool IsSameProperty(RenderProperty property)
        {
            if(property == null)
                return false;

            return m_id == property.m_id;
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
        public override int GetHashCode()
        {
            unchecked
            {
                return m_id.GetHashCode();
            }
        }

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>A string that represents the current object.</returns>
        public override String ToString()
        {
            return ID.ToString();
        }

        /// <summary>
        /// Get a copy of the render property. Some properties may not be cloned.
        /// </summary>
        /// <returns>Copy of the render property.</returns>
        public virtual RenderProperty Clone()
        {
            return null;
        }

        #region IPrimitive property

        /// <summary>
        /// General base class for <see cref="IPrimitiveValue"/> render properties. This property can be serialized.
        /// </summary>
        /// <typeparam name="T">Primitive value type.</typeparam>
        public abstract class Primitive<T> : RenderProperty, ISavable where T : struct, IPrimitiveValue
        {
            private T m_value;

            /// <summary>
            /// Gets or sets the property value.
            /// </summary>
            public T Value
            {
                get
                {
                    return m_value;
                }
                set
                {
                    m_value = value;
                }
            }

            /// <summary>
            /// Gets if the property can be cloned. For IPrimitiveValue this is true because IPrimitiveValue types 
            /// should always be structs.
            /// </summary>
            public override bool CanClone
            {
                get
                {
                    return typeof(T).IsValueType;
                }
            }

            /// <summary>
            /// Constructs a new instance of the <see cref="Primitive{T}"/> class.
            /// </summary>
            /// <param name="id">The unique ID value.</param>
            protected Primitive(RenderPropertyID id) : this(id, default(T)) { }

            /// <summary>
            /// Constructs a new instance of the <see cref="Primitive{T}"/> class.
            /// </summary>
            /// <param name="id">The unique ID value.</param>
            /// <param name="value">The property value.</param>
            protected Primitive(RenderPropertyID id, T value) 
                : base(id)
            {
                m_value = value;
            }

            /// <summary>
            /// Get a copy of the render property. Some properties may not be cloned.
            /// </summary>
            /// <returns>Copy of the render property.</returns>
            public override RenderProperty Clone()
            {
                Primitive<T> prop = SmartActivator.CreateInstance(GetType()) as Primitive<T>;
                prop.m_id = m_id;
                prop.m_value = m_value;

                return prop;
            }

            /// <summary>
            /// Returns a string that represents the current object.
            /// </summary>
            /// <returns>A string that represents the current object.</returns>
            public override String ToString()
            {
                return String.Format("ID: {0}, Type: {1}", ID.ToString(), typeof(T).ToString());
            }

            /// <summary>
            /// Gets the value of the property.
            /// </summary>
            /// <param name="value">Value held by the property.</param>
            public void GetValue(out T value)
            {
                value = m_value;
            }

            /// <summary>
            /// Reads the object data from the input.
            /// </summary>
            /// <param name="input">Savable reader</param>
            public void Read(ISavableReader input)
            {
                input.Read<T>("Value", out m_value);
            }

            /// <summary>
            /// Writes the object data to the output.
            /// </summary>
            /// <param name="output">Savable writer</param>
            public void Write(ISavableWriter output)
            {
                output.Write<T>("Value", m_value);
            }
        }

        #endregion

        #region ISavable property

        /// <summary>
        /// General base class for <see cref="ISavable"/> render properties. This property can be serialized.
        /// </summary>
        /// <typeparam name="T">Savable value type.</typeparam>
        [SavableVersion(1)]
        public abstract class Savable<T> : RenderProperty, ISavable where T : class, ISavable
        {
            private T m_value;

            /// <summary>
            /// Gets or sets the property value.
            /// </summary>
            public T Value
            {
                get
                {
                    return m_value;
                }
                set
                {
                    m_value = value;
                }
            }

            /// <summary>
            /// Gets if the property can be cloned.
            /// </summary>
            public override bool CanClone
            {
                get
                {
                    return typeof(IDeepCloneable).IsAssignableFrom(typeof(T));
                }
            }

            /// <summary>
            /// Constructs a new instance of the <see cref="Primitive{T}"/> class.
            /// </summary>
            /// <param name="id">The unique ID value.</param>
            protected Savable(RenderPropertyID id) : this(id, default(T)) { }

            /// <summary>
            /// Constructs a new instance of the <see cref="Primitive{T}"/> class.
            /// </summary>
            /// <param name="id">The unique ID value.</param>
            /// <param name="value">The property value.</param>
            protected Savable(RenderPropertyID id, T value)
                : base(id)
            {
                m_value = value;
            }

            /// <summary>
            /// Get a copy of the render property. Some properties may not be cloned.
            /// </summary>
            /// <returns>Copy of the render property.</returns>
            public override RenderProperty Clone()
            {
                if(!CanClone)
                    return null;

                Savable<T> prop = SmartActivator.CreateInstance(GetType()) as Savable<T>;
                prop.m_id = m_id;
                prop.m_value = (m_value == null) ? null : (m_value as IDeepCloneable).Clone() as T;

                return prop;
            }

            /// <summary>
            /// Returns a string that represents the current object.
            /// </summary>
            /// <returns>A string that represents the current object.</returns>
            public override String ToString()
            {
                return String.Format("ID: {0}, Type: {1}", ID.ToString(), typeof(T).ToString());
            }

            /// <summary>
            /// Gets the value of the property.
            /// </summary>
            /// <param name="value">Value held by the property.</param>
            public void GetValue(out T value)
            {
                value = m_value;
            }

            /// <summary>
            /// Reads the object data from the input.
            /// </summary>
            /// <param name="input">Savable reader</param>
            public void Read(ISavableReader input)
            {
                m_value = input.ReadSavable<T>("Value");
            }

            /// <summary>
            /// Writes the object data to the output.
            /// </summary>
            /// <param name="output">Savable writer</param>
            public void Write(ISavableWriter output)
            {
                output.WriteSavable<T>("Value", m_value);
            }
        }

        #endregion

        #region .NET primitive properties

        /// <summary>
        /// Base class for <see cref="Int16"/> properties. This property can be serialized.
        /// </summary>
        [SavableVersion(1)]
        public abstract class Short : RenderProperty<short>, ISavable
        {
            /// <summary>
            /// Gets if the property can be cloned.
            /// </summary>
            public override bool CanClone
            {
                get
                {
                    return true;
                }
            }

            /// <summary>
            /// Constructs a new instance of the <see cref="Short"/> class.
            /// </summary>
            /// <param name="id">The unique ID value.</param>
            protected Short(RenderPropertyID id) : base(id, 0) { }

            /// <summary>
            /// Constructs a new instance of the <see cref="Short"/> class.
            /// </summary>
            /// <param name="id">The unique ID value.</param>
            /// <param name="value">The property value.</param>
            protected Short(RenderPropertyID id, short value) : base(id, value) { }

            /// <summary>
            /// Get a copy of the render property. Some properties may not be cloned.
            /// </summary>
            /// <returns>Copy of the render property.</returns>
            public override RenderProperty Clone()
            {
                Short prop = SmartActivator.CreateInstance(GetType()) as Short;
                prop.m_id = m_id;
                prop.Value = Value;

                return prop;
            }

            /// <summary>
            /// Reads the object data from the input.
            /// </summary>
            /// <param name="input">Savable reader</param>
            public void Read(ISavableReader input)
            {
                Value = input.ReadInt16("Value");
            }

            /// <summary>
            /// Writes the object data to the output.
            /// </summary>
            /// <param name="output">Savable writer</param>
            public void Write(ISavableWriter output)
            {
                output.Write("Value", Value);
            }
        }

        /// <summary>
        /// Base class for <see cref="Int32"/> properties. This property can be serialized.
        /// </summary>
        [SavableVersion(1)]
        public abstract class Int : RenderProperty<int>, ISavable
        {
            /// <summary>
            /// Gets if the property can be cloned.
            /// </summary>
            public override bool CanClone
            {
                get
                {
                    return true;
                }
            }

            /// <summary>
            /// Constructs a new instance of the <see cref="Int"/> class.
            /// </summary>
            /// <param name="id">The unique ID value.</param>
            protected Int(RenderPropertyID id) : this(id, 0) { }

            /// <summary>
            /// Constructs a new instance of the <see cref="Int"/> class.
            /// </summary>
            /// <param name="id">The unique ID value.</param>
            /// <param name="value">The property value.</param>
            protected Int(RenderPropertyID id, int value) : base(id, value) { }

            /// <summary>
            /// Get a copy of the render property. Some properties may not be cloned.
            /// </summary>
            /// <returns>Copy of the render property.</returns>
            public override RenderProperty Clone()
            {
                Int prop = SmartActivator.CreateInstance(GetType()) as Int;
                prop.m_id = m_id;
                prop.Value = Value;

                return prop;
            }

            /// <summary>
            /// Reads the object data from the input.
            /// </summary>
            /// <param name="input">Savable reader</param>
            public void Read(ISavableReader input)
            {
                Value = input.ReadInt32("Value");
            }

            /// <summary>
            /// Writes the object data to the output.
            /// </summary>
            /// <param name="output">Savable writer</param>
            public void Write(ISavableWriter output)
            {
                output.Write("Value", Value);
            }
        }

        /// <summary>
        /// Base class for <see cref="Int64"/> properties. This property can be serialized.
        /// </summary>
        [SavableVersion(1)]
        public abstract class Long : RenderProperty<long>, ISavable
        {
            /// <summary>
            /// Gets if the property can be cloned.
            /// </summary>
            public override bool CanClone
            {
                get
                {
                    return true;
                }
            }

            /// <summary>
            /// Constructs a new instance of the <see cref="Long"/> class.
            /// </summary>
            /// <param name="id">The unique ID value.</param>
            protected Long(RenderPropertyID id) : this(id, 0) { }

            /// <summary>
            /// Constructs a new instance of the <see cref="Long"/> class.
            /// </summary>
            /// <param name="id">The unique ID value.</param>
            /// <param name="value">The property value.</param>
            protected Long(RenderPropertyID id, long value) : base(id, value) { }

            /// <summary>
            /// Get a copy of the render property. Some properties may not be cloned.
            /// </summary>
            /// <returns>Copy of the render property.</returns>
            public override RenderProperty Clone()
            {
                Long prop = SmartActivator.CreateInstance(GetType()) as Long;
                prop.m_id = m_id;
                prop.Value = Value;

                return prop;
            }

            /// <summary>
            /// Reads the object data from the input.
            /// </summary>
            /// <param name="input">Savable reader</param>
            public void Read(ISavableReader input)
            {
                Value = input.ReadInt64("Value");
            }

            /// <summary>
            /// Writes the object data to the output.
            /// </summary>
            /// <param name="output">Savable writer</param>
            public void Write(ISavableWriter output)
            {
                output.Write("Value", Value);
            }
        }

        /// <summary>
        /// Base class for <see cref="Float"/> properties. This property can be serialized.
        /// </summary>
        [SavableVersion(1)]
        public abstract class Float : RenderProperty<float>, ISavable
        {
            /// <summary>
            /// Gets if the property can be cloned.
            /// </summary>
            public override bool CanClone
            {
                get
                {
                    return true;
                }
            }

            /// <summary>
            /// Constructs a new instance of the <see cref="Float"/> class.
            /// </summary>
            /// <param name="id">The unique ID value.</param>
            protected Float(RenderPropertyID id) : this(id, 0.0f) { }

            /// <summary>
            /// Constructs a new instance of the <see cref="Float"/> class.
            /// </summary>
            /// <param name="id">The unique ID value.</param>
            /// <param name="value">The property value.</param>
            protected Float(RenderPropertyID id, float value) : base(id, value) { }

            /// <summary>
            /// Get a copy of the render property. Some properties may not be cloned.
            /// </summary>
            /// <returns>Copy of the render property.</returns>
            public override RenderProperty Clone()
            {
                Float prop = SmartActivator.CreateInstance(GetType()) as Float;
                prop.m_id = m_id;
                prop.Value = Value;

                return prop;
            }

            /// <summary>
            /// Reads the object data from the input.
            /// </summary>
            /// <param name="input">Savable reader</param>
            public void Read(ISavableReader input)
            {
                Value = input.ReadSingle("Value");
            }

            /// <summary>
            /// Writes the object data to the output.
            /// </summary>
            /// <param name="output">Savable writer</param>
            public void Write(ISavableWriter output)
            {
                output.Write("Value", Value);
            }
        }

        /// <summary>
        /// Base class for <see cref="Double"/> properties. This property can be serialized.
        /// </summary>
        [SavableVersion(1)]
        public abstract class Double : RenderProperty<double>, ISavable
        {
            /// <summary>
            /// Gets if the property can be cloned.
            /// </summary>
            public override bool CanClone
            {
                get
                {
                    return true;
                }
            }

            /// <summary>
            /// Constructs a new instance of the <see cref="Double"/> class.
            /// </summary>
            /// <param name="id">The unique ID value.</param>
            protected Double(RenderPropertyID id) : this(id, 0.0) { }

            /// <summary>
            /// Constructs a new instance of the <see cref="Double"/> class.
            /// </summary>
            /// <param name="id">The unique ID value.</param>
            /// <param name="value">The property value.</param>
            protected Double(RenderPropertyID id, double value) : base(id, value) { }

            /// <summary>
            /// Get a copy of the render property. Some properties may not be cloned.
            /// </summary>
            /// <returns>Copy of the render property.</returns>
            public override RenderProperty Clone()
            {
                Double prop = SmartActivator.CreateInstance(GetType()) as Double;
                prop.m_id = m_id;
                prop.Value = Value;

                return prop;
            }

            /// <summary>
            /// Reads the object data from the input.
            /// </summary>
            /// <param name="input">Savable reader</param>
            public void Read(ISavableReader input)
            {
                Value = input.ReadDouble("Value");
            }

            /// <summary>
            /// Writes the object data to the output.
            /// </summary>
            /// <param name="output">Savable writer</param>
            public void Write(ISavableWriter output)
            {
                output.Write("Value", Value);
            }
        }

        #endregion
    }

    /// <summary>
    /// Generic base class for render properties. Generally new render properties derive from this type. By default this property
    /// does not implement serialization, but derived classes can.
    /// </summary>
    /// <remarks>
    /// If serialization is required for the property, there are a number of ready to use subclasses for <see cref="IPrimitiveValue"/>,
    /// <see cref="ISavable"/> and .NET primitive types.
    /// </remarks>
    /// <typeparam name="T">Property value type.</typeparam>
    public abstract class RenderProperty<T> : RenderProperty
    {
        private T m_value;

        /// <summary>
        /// Gets or sets the property value.
        /// </summary>
        public T Value
        {
            get
            {
                return m_value;
            }
            set
            {
                m_value = value;
            }
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="RenderProperty{T}"/> class.
        /// </summary>
        /// <param name="id">The unique ID value.</param>
        /// <param name="value">The property value.</param>
        protected RenderProperty(RenderPropertyID id, T value) 
            : base(id)
        {
            m_value = value;
        }

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>A string that represents the current object.</returns>
        public override String ToString()
        {
            return String.Format("ID: {0}, Type: {1}", ID.ToString(), typeof(T).ToString());
        }

        /// <summary>
        /// Gets the value of the property.
        /// </summary>
        /// <param name="value">Value held by the property.</param>
        public void GetValue(out T value)
        {
            value = m_value;
        }
    }

    /// <summary>
    /// Generic base class for render properties whose value comes from a different source. This acts as a facade to query
    /// those values (generally value types).
    /// </summary>
    /// <typeparam name="T">Property value type.</typeparam>
    public abstract class RenderPropertyAccessor<T> : RenderProperty
    {
        private GetPropertyValue<T> m_accessor;

        /// <summary>
        /// Gets the property value.
        /// </summary>
        public T Value
        {
            get
            {
                return m_accessor();
            }
        }

        /// <summary>
        /// Gets or sets the backing store getter.
        /// </summary>
        protected GetPropertyValue<T> Accessor
        {
            get
            {
                return m_accessor;
            }
            set
            {
                m_accessor = value;
            }
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="RenderPropertyAccessor{T}"/> class.
        /// </summary>
        /// <param name="id">The unique ID value.</param>
        protected RenderPropertyAccessor(RenderPropertyID id) : base(id) { }

        /// <summary>
        /// Constructs a new instance of the <see cref="RenderPropertyAccessor{T}"/> class.
        /// </summary>
        /// <param name="id">The unique ID value.</param>
        /// <param name="accessor">Getter delegate for the property value.</param>
        protected RenderPropertyAccessor(RenderPropertyID id, GetPropertyValue<T> accessor)
            : base(id)
        {
            m_accessor = accessor;
        }

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>A string that represents the current object.</returns>
        public override String ToString()
        {
            return String.Format("ID: {0}, Type: {1}", ID.ToString(), typeof(T).ToString());
        }

        /// <summary>
        /// Gets the value of the property.
        /// </summary>
        /// <param name="value">Value held by the property.</param>
        public void GetValue(out T value)
        {
            value = m_accessor();
        }
    }

    /// <summary>
    /// Accessor delegate for getting the underlying value of a render property.
    /// </summary>
    /// <typeparam name="T">Generic value type.</typeparam>
    /// <returns>The value.</returns>
    public delegate T GetPropertyValue<T>();

    internal static class RenderPropertyIDHolder<T>
    {
        public static readonly RenderPropertyID ID = RenderPropertyID.GenerateNewUniqueID();
    }
}
