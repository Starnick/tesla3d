﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;

namespace Tesla.Graphics
{
    /// <summary>
    /// A collection of passes in a material. Each pass contains all the state information for the rendering pipeline that is applied before a draw call is issued.
    /// </summary>
    public sealed class MaterialPassCollection : ReadOnlyNamedListFast<MaterialPass>
    {
        private Material m_material;

        internal MaterialPassCollection(Material material) : this(material, 0) { }

        internal MaterialPassCollection(Material material, int initialCapacity) 
            : base(initialCapacity)
        {
            m_material = material;
        }

        /// <summary>
        /// Adds a new pass to the collection.
        /// </summary>
        /// <param name="passName">Name of the pass.</param>
        /// <param name="shaderGroupName">Name of the shader group from the effect to be used for the pass.</param>
        /// <returns>The created pass. This may be null if the pass was failed to be created.</returns>
        public MaterialPass Add(String passName, String shaderGroupName)
        {
            if(String.IsNullOrEmpty(passName) || m_material.Effect == null)
                return null;

            IEffectShaderGroup shaderGroup = m_material.Effect.ShaderGroups[shaderGroupName];
            if(shaderGroup == null)
                return null;

            MaterialPass pass = new MaterialPass(m_material, shaderGroup, passName);
            pass.PassIndex = Count;
            base.m_list.Add(pass);

            PopulateFastLookupTable();

            return pass;
        }

        /// <summary>
        /// Adds a new pass to the collection.
        /// </summary>
        /// <param name="passName">Name of the pass.</param>
        /// <param name="shaderGroup">Shader group from the effect to be used for the pass.</param>
        /// <returns>The created pass. This may be null if the pass was failed to be created.</returns>
        public MaterialPass Add(String passName, IEffectShaderGroup shaderGroup)
        {
            if(String.IsNullOrEmpty(passName) || shaderGroup == null || !shaderGroup.IsPartOf(m_material.Effect))
                return null;

            MaterialPass pass = new MaterialPass(m_material, shaderGroup, passName);
            pass.PassIndex = Count;
            base.m_list.Add(pass);

            PopulateFastLookupTable();

            return pass;
        }

        /// <summary>
        /// Removes the pass from the collection.
        /// </summary>
        /// <param name="passName">Name of the pass to remove.</param>
        /// <returns>True if successfully removed, false if it could not be found.</returns>
        public bool Remove(String passName)
        {
            if(String.IsNullOrEmpty(passName))
                return false;

            for(int i = 0; i < m_list.Count; i++)
            {
                MaterialPass pass = m_list[i];
                if(passName.Equals(pass.Name))
                {
                    m_list.RemoveAt(i);
                    pass.PassIndex = -1;
                    PopulateFastLookupTable();
                    ReIndexPasses(i);
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Removes the pass from the collection.
        /// </summary>
        /// <param name="pass">Pass to remove.</param>
        /// <returns>True if successfully removed, false if not (e.g. the pass does not belong to this material).</returns>
        public bool Remove(MaterialPass pass)
        {
            if(pass == null || pass.Parent != m_material)
                return false;

            for(int i = 0; i < m_list.Count; i++)
            {
                MaterialPass p = m_list[i];
                if(pass == p)
                {
                    m_list.RemoveAt(i);
                    pass.PassIndex = -1;
                    PopulateFastLookupTable();
                    ReIndexPasses(i);
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Clears the collection of passes.
        /// </summary>
        public void Clear()
        {
            m_list.Clear();

            PopulateFastLookupTable();
        }

        internal void UpdateFastLookup()
        {
            PopulateFastLookupTable();
        }

        internal void CopyPasses(MaterialPassCollection collection)
        {
            for(int i = 0; i < m_list.Count; i++)
            {
                MaterialPass oldPass = m_list[i];
                MaterialPass newPass = new MaterialPass(collection.m_material, collection.m_material.Effect.ShaderGroups[oldPass.ShaderGroup.ShaderGroupIndex], oldPass.Name, oldPass.IsInherited);

                if((oldPass.RenderStatesToApply & EnforcedRenderState.BlendState) == EnforcedRenderState.BlendState)
                    newPass.BlendState = oldPass.BlendState;

                if((oldPass.RenderStatesToApply & EnforcedRenderState.RasterizerState) == EnforcedRenderState.RasterizerState)
                    newPass.RasterizerState = oldPass.RasterizerState;

                if((oldPass.RenderStatesToApply & EnforcedRenderState.DepthStencilState) == EnforcedRenderState.DepthStencilState)
                    newPass.DepthStencilState = oldPass.DepthStencilState;

                collection.m_list.Add(newPass);
            }

            collection.PopulateFastLookupTable();
        }

        private void ReIndexPasses(int startIndex)
        {
            for(int i = startIndex; i < m_list.Count; i++)
            {
                MaterialPass pass = m_list[i];
                pass.PassIndex = i;
            }
        }
    }
}
