﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Runtime.InteropServices;
using Tesla.Content;

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// Describes a blending states for a render target. This is used to group common data together for BlendState only and changing values here
  /// will not affect platform-device states.
  /// </summary>
  [Serializable]
  [StructLayout(LayoutKind.Sequential)]
  public struct RenderTargetBlendDescription : IEquatable<RenderTargetBlendDescription>, IPrimitiveValue
  {
    private static RenderTargetBlendDescription s_default;

    /// <summary>
    /// If blending is enabled for the render target. The default is false.
    /// </summary>
    public bool BlendEnable;

    /// <summary>
    /// The alpha blend function used. The default is Add.
    /// </summary>
    public BlendFunction AlphaBlendFunction;

    /// <summary>
    /// The alpha source blend used. The default is One.
    /// </summary>
    public Blend AlphaSourceBlend;

    /// <summary>
    /// The alpha destination blend used. The default is Zero.
    /// </summary>
    public Blend AlphaDestinationBlend;

    /// <summary>
    /// The color blend function used. The default is Add.
    /// </summary>
    public BlendFunction ColorBlendFunction;

    /// <summary>
    /// The color source blend used. The default is One.
    /// </summary>
    public Blend ColorSourceBlend;

    /// <summary>
    /// The color destination blend used. The default is Zero.
    /// </summary>
    public Blend ColorDestinationBlend;

    /// <summary>
    /// The render target color channels that should be written to during blending.
    /// </summary>
    public ColorWriteChannels WriteChannels;

    /// <summary>
    /// Gets a default render target blend description.
    /// </summary>
    public static RenderTargetBlendDescription Default
    {
      get
      {
        return s_default;
      }
    }

    /// <summary>
    /// Gets if the this description matches the default description settings.
    /// </summary>
    public readonly bool IsDefault
    {
      get
      {
        return BlendEnable == false && AlphaBlendFunction == BlendFunction.Add && AlphaSourceBlend == Blend.One && AlphaDestinationBlend == Blend.Zero
            && ColorBlendFunction == BlendFunction.Add && ColorSourceBlend == Blend.One && ColorDestinationBlend == Blend.Zero && WriteChannels == ColorWriteChannels.All;
      }
    }

    //Static constructor for default description
    static RenderTargetBlendDescription()
    {
      s_default.BlendEnable = false;

      s_default.AlphaBlendFunction = BlendFunction.Add;
      s_default.AlphaSourceBlend = Blend.One;
      s_default.AlphaDestinationBlend = Blend.Zero;

      s_default.ColorBlendFunction = BlendFunction.Add;
      s_default.ColorSourceBlend = Blend.One;
      s_default.ColorDestinationBlend = Blend.Zero;

      s_default.WriteChannels = ColorWriteChannels.All;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="RenderTargetBlendDescription"/> struct.
    /// </summary>
    /// <param name="blendEnable">True if blending is enabled for the render target, false otherwise.</param>
    /// <param name="alphaBlendFunction">The alpha blend function used.</param>
    /// <param name="alphaSourceBlend">The alpha source blend used.</param>
    /// <param name="alphaDestinationBlend">The alpha destination blend used.</param>
    /// <param name="colorBlendFunction">The color blend function used.</param>
    /// <param name="colorSourceBlend">The color source blend used.</param>
    /// <param name="colorDestinationBlend">The color destination blend used.</param>
    /// <param name="writeChannels">The render target color channels that should be written to during blending.</param>
    public RenderTargetBlendDescription(bool blendEnable, BlendFunction alphaBlendFunction, Blend alphaSourceBlend, Blend alphaDestinationBlend, BlendFunction colorBlendFunction, Blend colorSourceBlend, Blend colorDestinationBlend, ColorWriteChannels writeChannels)
    {
      BlendEnable = blendEnable;
      AlphaBlendFunction = alphaBlendFunction;
      AlphaSourceBlend = alphaSourceBlend;
      AlphaDestinationBlend = alphaDestinationBlend;
      ColorBlendFunction = colorBlendFunction;
      ColorSourceBlend = colorSourceBlend;
      ColorDestinationBlend = colorDestinationBlend;
      WriteChannels = writeChannels;
    }

    /// <summary>
    /// Writes the primitive data to the output.
    /// </summary>
    /// <param name="output">Primitive writer.</param>
    public readonly void Write(IPrimitiveWriter output)
    {
      output.Write("BlendEnable", BlendEnable);
      output.WriteEnum<BlendFunction>("AlphaBlendFunction", AlphaBlendFunction);
      output.WriteEnum<Blend>("AlphaSourceBlend", AlphaSourceBlend);
      output.WriteEnum<Blend>("AlphaDestinationBlend", AlphaDestinationBlend);

      output.WriteEnum<BlendFunction>("ColorBlendFunction", ColorBlendFunction);
      output.WriteEnum<Blend>("ColorSourceBlend", ColorSourceBlend);
      output.WriteEnum<Blend>("ColorDestinationBlend", ColorDestinationBlend);

      output.WriteEnum<ColorWriteChannels>("ColorWriteChannels", WriteChannels);
    }

    /// <summary>
    /// Reads the primitive data from the input.
    /// </summary>
    /// <param name="input">Primitive reader.</param>
    public void Read(IPrimitiveReader input)
    {
      BlendEnable = input.ReadBoolean("BlendEnable");
      AlphaBlendFunction = input.ReadEnum<BlendFunction>("AlphaBlendFunction");
      AlphaSourceBlend = input.ReadEnum<Blend>("AlphaSourceBlend");
      AlphaDestinationBlend = input.ReadEnum<Blend>("AlphaDestinationBlend");

      ColorBlendFunction = input.ReadEnum<BlendFunction>("ColorBlendFunction");
      ColorSourceBlend = input.ReadEnum<Blend>("ColorSourceBlend");
      ColorDestinationBlend = input.ReadEnum<Blend>("ColorDestinationBlend");

      WriteChannels = input.ReadEnum<ColorWriteChannels>("ColorWriteChannels");
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
    public readonly override int GetHashCode()
    {
      unchecked
      {
        //Casting enums to ints to avoid boxing

        int hash = 17;

        hash = (hash * 31) + ((BlendEnable) ? 1 : 0);

        hash = (hash * 31) + (int) AlphaBlendFunction;
        hash = (hash * 31) + (int) AlphaSourceBlend;
        hash = (hash * 31) + (int) AlphaDestinationBlend;

        hash = (hash * 31) + (int) ColorBlendFunction;
        hash = (hash * 31) + (int) ColorSourceBlend;
        hash = (hash * 31) + (int) ColorDestinationBlend;

        hash = (hash * 31) + (int) WriteChannels;

        return hash;
      }
    }

    /// <summary>
    /// Determines whether the specified <see cref="System.Object"/> is equal to this instance.
    /// </summary>
    /// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
    /// <returns>
    ///   <c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.
    /// </returns>
    public readonly override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is RenderTargetBlendDescription)
        return Equals((RenderTargetBlendDescription) obj);

      return false;
    }

    /// <summary>
    /// Indicates whether the current object is equal to another object of the same type.
    /// </summary>
    /// <param name="other">An object to compare with this object.</param>
    /// <returns>
    /// true if the current object is equal to the <paramref name="other"/> parameter; otherwise, false.
    /// </returns>
    public readonly bool Equals(RenderTargetBlendDescription other)
    {
      return BlendEnable == other.BlendEnable && AlphaBlendFunction == other.AlphaBlendFunction && AlphaSourceBlend == other.AlphaSourceBlend && AlphaDestinationBlend == other.AlphaDestinationBlend
              && ColorBlendFunction == other.ColorBlendFunction && ColorSourceBlend == other.ColorSourceBlend && ColorDestinationBlend == other.ColorDestinationBlend && WriteChannels == other.WriteChannels;
    }

    /// <summary>
    /// Returns the fully qualified type name of this instance.
    /// </summary>
    /// <returns>A <see cref="T:System.String" /> containing a fully qualified type name.</returns>
    public readonly override String ToString()
    {
      CultureInfo info = CultureInfo.CurrentCulture;
      return String.Format(info, "BlendEnable: {0}, AlphaBlendFunction: {1}, AlphaSourceBlend: {2}, AlphaDestinationBlend: {3}, BlendFunction: {4}, ColorSourceBlend: {5}, ColorDestinationBlend: {6}, ColorWriteChannels: {7}",
          new object[] { BlendEnable.ToString(), AlphaBlendFunction.ToString(), AlphaSourceBlend.ToString(), AlphaDestinationBlend.ToString(), ColorBlendFunction.ToString(), ColorSourceBlend.ToString(), ColorDestinationBlend.ToString(), WriteChannels.ToString() });
    }
  }
}
