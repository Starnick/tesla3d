﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Runtime.InteropServices;

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// Description of an output of a graphics adapter, such as a monitor.
  /// </summary>
  [StructLayout(LayoutKind.Sequential)]
  public struct Output : IEquatable<Output>, IRefEquatable<Output>
  {
    private IntPtr m_monitorHandle;
    private String m_name;
    private Rectangle m_bounds;

    /// <summary>
    /// Gets the handle to the output.
    /// </summary>
    public readonly IntPtr MonitorHandle
    {
      get
      {
        return m_monitorHandle;
      }
    }

    /// <summary>
    /// Gets the name of the output.
    /// </summary>
    public readonly string Name
    {
      get
      {
        return m_name;
      }
    }

    /// <summary>
    /// Gets the output's bounds in desktop coordinate.
    /// </summary>
    public readonly Rectangle DesktopBounds
    {
      get
      {
        return m_bounds;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Output"/> struct.
    /// </summary>
    /// <param name="monitorHandle">Handle of the output</param>
    /// <param name="name">Name of the output</param>
    /// <param name="bounds">Output's bounds in desktop coordinates</param>
    public Output(IntPtr monitorHandle, String name, Rectangle bounds)
    {
      m_name = name ?? String.Empty;
      m_monitorHandle = monitorHandle;
      m_bounds = bounds;
    }

    /// <summary>
    /// Tests equality between two outputs
    /// </summary>
    /// <param name="a">First output</param>
    /// <param name="b">Second output</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    public static bool operator ==(in Output a, in Output b)
    {
      return (a.m_monitorHandle == b.m_monitorHandle) && String.Equals(a.m_name, b.m_name) && (a.m_bounds.Equals(b.m_bounds));
    }

    /// <summary>
    /// Tests inequality between two outputs
    /// </summary>
    /// <param name="a">First output</param>
    /// <param name="b">Second output</param>
    /// <returns>True if both are not equal, false otherwise.</returns>
    public static bool operator !=(in Output a, in Output b)
    {
      return (a.m_monitorHandle != b.m_monitorHandle) || !String.Equals(a.m_name, b.m_name) || !a.m_bounds.Equals(b.m_bounds);
    }

    /// <summary>
    /// Tests equality betwen this output and another.
    /// </summary>
    /// <param name="other">Other output to compare to.</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    readonly bool IEquatable<Output>.Equals(Output other)
    {
      return (m_monitorHandle == other.m_monitorHandle) && String.Equals(m_name, other.m_name) && m_bounds.Equals(other.m_bounds);
    }

    /// <summary>
    /// Tests equality betwen this output and another.
    /// </summary>
    /// <param name="other">Other output to compare to.</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    public readonly bool Equals(in Output other)
    {
      return (m_monitorHandle == other.m_monitorHandle) && String.Equals(m_name, other.m_name) && m_bounds.Equals(other.m_bounds);
    }

    /// <summary>
    /// Indicates whether this instance and a specified object are equal.
    /// </summary>
    /// <param name="obj">Another object to compare to.</param>
    /// <returns>True if <paramref name="obj" /> and this instance are the same type and represent the same value; otherwise, false. </returns>
    public readonly override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is DisplayMode)
      {
        Output other = (Output) obj;
        return (m_monitorHandle == other.m_monitorHandle) && String.Equals(m_name, other.m_name) && m_bounds.Equals(other.m_bounds);
      }

      return false;
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
    public readonly override int GetHashCode()
    {
      unchecked
      {
        return m_monitorHandle.GetHashCode() + m_name.GetHashCode() + m_bounds.GetHashCode();
      }
    }

    /// <summary>
    /// Returns the fully qualified type name of this instance.
    /// </summary>
    /// <returns>
    /// A <see cref="T:System.String" /> containing a fully qualified type name.
    /// </returns>
    public readonly override string ToString()
    {
      CultureInfo info = CultureInfo.CurrentCulture;
      return String.Format(info, "MonitorHandle: {0}, Name: {1}, DesktopBounds: {2}",
          new object[] { m_monitorHandle.ToString(), m_name, m_bounds.ToString() });
    }
  }
}
