﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Tesla.Graphics
{
    /// <summary>
    /// A render bucket is a collection of renderables that are to be drawn, generally sorted in some order to achieve a rendering effect
    /// or to minimize overdraw and state switching.
    /// </summary>
    public sealed class RenderBucket : IReadOnlyList<RenderBucketEntry>
    {
        private RenderBucketID m_bucketID;
        private IRenderBucketEntryComparer m_comparer;

        //Bucket o' renderables
        private RenderBucketEntry[] m_tempList;
        private RenderBucketEntry[] m_renderables;
        private int m_count;
        private int m_version;

        /// <summary>
        /// Gets the ID corresponding to this render bucket.
        /// </summary>
        public RenderBucketID BucketID
        {
            get
            {
                return m_bucketID;
            }
        }

        /// <summary>
        /// Gets the comparer used to sort the renderables.
        /// </summary>
        public IRenderBucketEntryComparer BucketComparer
        {
            get
            {
                return m_comparer;
            }
        }

        /// <summary>
        /// Gets the number of renderables in the collection.
        /// </summary>
        public int Count
        {
            get
            {
                return m_count;
            }
        }

        /// <summary>
        /// Gets the renderable at the specified index in the bucket.
        /// </summary>
        /// <param name="index">The index of the renderable in the bucket.</param>
        /// <exception cref="System.ArgumentNullException">Thrown if the index is out of range.</exception>
        public RenderBucketEntry this[int index]
        {
            get
            {
                if(index < 0 || index > m_count)
                    throw new ArgumentNullException("index");

                return m_renderables[index];
            }
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="RenderBucket"/> class.
        /// </summary>
        /// <param name="bucketID">ID that corresponds to this bucket.</param>
        /// <param name="comparer">Comparer used to sort the bucket.</param>
        /// <param name="initialSize">Initial capacity of the bucket, when exceeded the bucket will double this value.</param>
        /// <exception cref="System.ArgumentNullException">Thrown if the comparer is null.</exception>
        /// <exception cref="System.ArgumentOutOfRangeException">Thrown if the initial size is less than or equal to zero.</exception>
        public RenderBucket(RenderBucketID bucketID, IRenderBucketEntryComparer comparer, int initialSize)
        {
            if(comparer == null)
                throw new ArgumentNullException("comparer");

            if(initialSize <= 0)
                throw new ArgumentOutOfRangeException("initialSize");

            m_bucketID = bucketID;
            m_comparer = comparer;

            m_count = 0;
            m_renderables = new RenderBucketEntry[initialSize];
        }

        /// <summary>
        /// Adds a renderable to the bucket.
        /// </summary>
        /// <param name="renderable">Renderable to add.</param>
        /// <param name="material">Optional material to be used with the renderable.</param>
        /// <returns>True if the renderable was added to the bucket, false if otherwise.</returns>
        public bool Add(IRenderable renderable, Material material)
        {
            if(renderable == null || !renderable.IsValidForDraw)
                return false;

            EnsureCapacity(m_count + 1);

            m_renderables[m_count++] = new RenderBucketEntry(material, renderable);
            m_version++;
            return true;
        }

        /// <summary>
        /// Removes a renderable from the bucket.
        /// </summary>
        /// <param name="renderable">Renderable to remove.</param>
        /// <returns>True if the renderable was removed from the bucket, false if otherwise.</returns>
        public bool Remove(IRenderable renderable)
        {
            if(renderable == null)
                return false;

            for(int i = 0; i < m_count; i++)
            {
                if(m_renderables[i].Renderable == renderable)
                {
                    //Remove and shift all the subsequent renderables down by one
                    m_count--;
                    if(i < m_count)
                        Array.Copy(m_renderables, i + 1, m_renderables, i, m_count - i);

                    m_renderables[m_count] = new RenderBucketEntry();
                    m_version++;

                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Clears the bucket of all renderables.
        /// </summary>
        public void Clear()
        {
            Array.Clear(m_renderables, 0, m_count);

            m_count = 0;
            m_version++;
        }

        /// <summary>
        /// Draws all renderables in the bucket.
        /// </summary>
        /// <param name="renderContext">Render context.</param>
        /// <param name="applyMaterials">True to apply the materials of the renderables, false if just to setup draw calls.</param>
        public void DrawAll(IRenderContext renderContext, bool applyMaterials)
        {
            if(m_count == 0)
                return;

            if(applyMaterials)
            {
                DrawAllWithMaterials(renderContext);
            }
            else
            {
                //Just execute the draw calls for each renderable
                for(int i = 0; i < m_count; i++)
                    m_renderables[i].Renderable.SetupDrawCall(renderContext, m_bucketID, null);
            }
        }

        /// <summary>
        /// Draws each renderable using a single override material.
        /// </summary>
        /// <param name="renderContext">Render context.</param>
        /// <param name="overrideMaterial">Override material.</param>
        public void DrawAll(IRenderContext renderContext, Material overrideMaterial)
        {
            if (m_count == 0 || overrideMaterial == null)
                return;

            //Ignore the material for each renderable and instead use the override material. For each object, we apply the material (setup constant buffers/other state), then draw
            //each pass.

            //Unlike the normal draw all logic, we can't batch up passes because then we'd lose the applied parameter state since we're using a single material instance.
            for (int i = 0; i < m_count; i++)
            {
                RenderBucketEntry currRenderBucketEntry = m_renderables[i];
                IRenderable renderable = currRenderBucketEntry.Renderable;

                overrideMaterial.ApplyMaterial(renderContext, renderable.RenderProperties);
                MaterialPassCollection passes = overrideMaterial.Passes;

                for (int passIndex = 0; passIndex < passes.Count; passIndex++)
                {
                    MaterialPass pass = passes[passIndex];
                    pass.Apply(renderContext);

                    renderable.SetupDrawCall(renderContext, m_bucketID, pass);
                }
            }
        }

        /// <summary>
        /// Sorts the renderables in the bucket.
        /// </summary>
        public void Sort()
        {
            if(m_count > 1)
            {
                //If null or not the same length, then need to resize/create a new array
                if(m_tempList == null || m_tempList.Length < m_renderables.Length)
                {
                    m_tempList = m_renderables.Clone() as RenderBucketEntry[];
                }
                else
                {
                    //Otherwise copy up to the count
                    Array.Copy(m_renderables, m_tempList, m_count);
                }

                //Merge sort
                MergeInsertionSort<RenderBucketEntry>(m_tempList, m_renderables, 0, m_count, m_comparer);

                //Clear temp list
                Array.Clear(m_tempList, 0, m_count);

                m_version++;
            }
        }

        /// <summary>
        /// Copies the content of this render bucket to another.
        /// </summary>
        /// <param name="bucket">Other bucket to copy content to.</param>
        public void CopyTo(RenderBucket bucket)
        {
            if (bucket == null)
                return;

            bucket.EnsureCapacity(bucket.Count + m_count);

            Array.Copy(m_renderables, 0, bucket.m_renderables, bucket.m_count, m_count);
            bucket.m_count += m_count;
            bucket.m_version++;
        }

        private void EnsureCapacity(int min)
        {
            if(m_renderables.Length < min)
            {
                int newCapacity = Math.Max((m_renderables.Length == 0) ? 4 : m_renderables.Length * 2, min);

                RenderBucketEntry[] newArray = new RenderBucketEntry[newCapacity];

                if (m_count > 0)
                    Array.Copy(m_renderables, 0, newArray, 0, m_count);

                m_renderables = newArray;
            }
        }

        /// <summary>
        /// Draws each renderable with their materials.
        /// </summary>
        /// <param name="renderContext">Render context.</param>
        private void DrawAllWithMaterials(IRenderContext renderContext)
        {
            for(int i = 0; i < m_count; i++)
            {
                RenderBucketEntry currRenderBucketEntry = m_renderables[i];
                IRenderable renderable = currRenderBucketEntry.Renderable;
                Material currMat = currRenderBucketEntry.Material;
                if(currMat == null)
                {
                    //If the material is null, and the renderable was valid for draw, most likely
                    //the renderable is doing some custom state setting in its setup draw call. We don't know enough information to do anything else.
                    renderable.SetupDrawCall(renderContext, m_bucketID, null);
                }
                else
                {
                    //The renderable has a material, now let's try to optimize by rendering in "chunks". For this material, let's see if there are other renderables
                    //ahead of us that use the same material, or share the same shaders. If so, then we can group the objects by pass -- set shaders once and
                    //some of the resources for each pass, then render all the objects before moving to the next pass, and so on.
                    int toIndex = GetEndIndex(i, currMat);
                    MaterialPassCollection passes = currMat.Passes;
                    int passCount = passes.Count;

                    //If same index, then no common materials found, so render this object normally (foreach pass, do draw).
                    if(toIndex == i)
                    {
                        currMat.ApplyMaterial(renderContext, renderable.RenderProperties);

                        for(int passIndex = 0; passIndex < passCount; passIndex++)
                        {
                            MaterialPass pass = passes[passIndex];
                            pass.Apply(renderContext);

                            renderable.SetupDrawCall(renderContext, m_bucketID, pass);
                        }
                    }
                    else
                    {
                        //If more than one found, render this chunk by applying one pass, render all objects, apply next pass, render all objects, etc.
                        for(int passIndex = 0; passIndex < passCount; passIndex++)
                        {
                            for(int j = i; j <= toIndex; j++)
                            {
                                currRenderBucketEntry = m_renderables[j];
                                renderable = currRenderBucketEntry.Renderable;
                                currMat = currRenderBucketEntry.Material;

                                //For every object, we want its material to process data before we render the first pass exactly once,
                                //so all the constant buffers get filled and are ready to be bound
                                if(passIndex == 0)
                                    currMat.ApplyMaterial(renderContext, renderable.RenderProperties);

                                //Applying each pass will bind every resource as necessary, and because these materials share quite a lot
                                //in common, redundant resources should be filtered out. GPU resources like constant buffers will be unique
                                //between effect instances, but the pass shaders should all be the same resource, which will reduce state switching.
                                MaterialPass pass = currMat.Passes[passIndex];
                                pass.Apply(renderContext);

                                //Execute the draw calls
                                renderable.SetupDrawCall(renderContext, m_bucketID, pass);
                            }
                        }

                        //Set i to the index of the last renderable we drew, on next loop step it will be the index of the next different materialed
                        //renderable
                        i = toIndex;
                    }
                }
            }
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.</returns>
        public override int GetHashCode()
        {
            unchecked
            {
                return m_bucketID.Value;
            }
        }

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>A string that represents the current object.</returns>
        public override String ToString()
        {
            return m_bucketID.ToString();
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>An enumerator that can be used to iterate through the collection.</returns>
        public RenderBucketEnumerator GetEnumerator()
        {
            return new RenderBucketEnumerator(this);
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.</returns>
        IEnumerator<RenderBucketEntry> IEnumerable<RenderBucketEntry>.GetEnumerator()
        {
            return new RenderBucketEnumerator(this);
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return new RenderBucketEnumerator(this);
        }

        private int GetEndIndex(int startIndex, Material currMat)
        {
            if(currMat == null)
                return startIndex;

            for(int i = startIndex + 1; i < m_count; i++)
            {
                Material nextMat = m_renderables[i].Material;

                //Compare curr material to the next, it may be null or invalid or a different set of shaders, or even just different number of passes. For any
                //of those cases, non-zero will be returned.
                if(currMat.CompareTo(nextMat) != 0)
                    return i - 1;
            }

            //Made it to the end without a switch, so use the last index
            return m_count - 1;
        }

        #region Merge Insertion Sort

        private const int INSERTION_SORT_THRESHOLD = 7;

        //Assumes aux and data start out with the same contents...
        private static void MergeInsertionSort<T>(T[] aux, T[] data, int low, int high, IComparer<T> comp)
        {
            //Use insertion sort on small arrays
            int length = high - low;
            if(length < INSERTION_SORT_THRESHOLD)
            {
                for(int i = low; i < high; i++)
                {
                    for(int j = i; j > low && comp.Compare(data[j - 1], data[j]) > 0; j--)
                    {
                        T temp = data[j];
                        data[j] = data[j - 1];
                        data[j - 1] = temp;
                    }
                }
                return;
            }

            //Merge sort: Recursively sort each half of dest into src
            int destLow = low;
            int destHight = high;

            int mid = (low + high) >> 1;
            MergeInsertionSort<T>(data, aux, low, mid, comp);
            MergeInsertionSort<T>(data, aux, mid, high, comp);

            //If list is already sorted, copy from src to dest
            if(comp.Compare(aux[mid - 1], aux[mid]) <= 0)
                Array.Copy(aux, low, data, destLow, length);

            //Merge sorted halves (now in src) into dest
            for(int i = destLow, p = low, q = mid; i < destHight; i++)
            {
                if(q >= high || p < mid && comp.Compare(aux[p], aux[q]) <= 0)
                    data[i] = aux[p++];
                else
                    data[i] = aux[q++];
            }
        }

        #endregion

        #region RenderBucket Enumerator

        /// <summary>
        /// Enumerates elements of a <see cref="RenderBucket"/>.
        /// </summary>
        [Serializable, StructLayout(LayoutKind.Sequential)]
        public struct RenderBucketEnumerator : IEnumerator<RenderBucketEntry>
        {
            private RenderBucket m_bucket;
            private RenderBucketEntry[] m_list;
            private int m_index;
            private int m_count;
            private int m_version;
            private RenderBucketEntry m_current;

            /// <summary>
            /// Gets the current value.
            /// </summary>
            public RenderBucketEntry Current
            {
                get
                {
                    return m_current;
                }
            }

            /// <summary>
            /// Gets the current value.
            /// </summary>
            object IEnumerator.Current
            {
                get
                {
                    return m_current;
                }
            }

            internal RenderBucketEnumerator(RenderBucket bucket)
            {
                m_bucket = bucket;
                m_list = bucket.m_renderables;
                m_count = bucket.m_count;
                m_index = 0;
                m_version = bucket.m_version;
                m_current = new RenderBucketEntry();
            }

            /// <summary>
            /// Advances the enumerator to the next element of the collection.
            /// </summary>
            /// <returns>True if the enumerator was successfully advanced to the next element; false if the enumerator has passed the end of the collection.</returns>
            public bool MoveNext()
            {
                ThrowIfChanged();

                if(m_index < m_count)
                {
                    m_current = m_list[m_index];
                    m_index++;
                    return true;
                }
                else
                {
                    m_index++;
                    m_current = new RenderBucketEntry();
                    return false;
                }
            }

            /// <summary>
            /// Sets the enumerator to its initial position, which is before the first element in the collection.
            /// </summary>
            public void Reset()
            {
                ThrowIfChanged();
                m_index = 0;
                m_current = new RenderBucketEntry();
            }

            /// <summary>
            /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
            /// </summary>
            public void Dispose() { }

            private void ThrowIfChanged()
            {
                if(m_version != m_bucket.m_version)
                    throw new InvalidOperationException(StringLocalizer.Instance.GetLocalizedString("CollectionModifiedDuringEnumeration"));
            }
        }

        #endregion
    }
}
