﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Tesla.Graphics
{
    /// <summary>
    /// Encapsulates a method to draw the contents of a single render bucket.
    /// </summary>
    /// <param name="renderContext">Render context.</param>
    /// <param name="bucket">Render bucket.</param>
    public delegate void DrawRenderBucket(IRenderContext renderContext, RenderBucket bucket);

    /// <summary>
    /// A render queue that contains a collection of buckets that renderables will be placed and sorted in when a scene is processed for rendering. 
    /// The order of the buckets will be based on the order they were added to the render queue.
    /// </summary>
    ///<remarks>
    /// Each bucket entry consists of a renderable (geometry) paired with a material. Generally renderables are sorted based on material or other criteria, allowing for rendering to be 
    /// batched together to reduce state switching. A renderable may contain multiple materials for different render passes (e.g. shadows, opaque, glow) and thus may have an entry 
    /// in multiple buckets. So you can think of each bucket representing the input for a render pass. The logic for handling one or more render passes is implemented in a 
    /// corresponding <see cref="IRenderStage"/>.
    ///</remarks>
    public sealed class RenderQueue : IReadOnlyList<RenderBucket>
    {
        private List<RenderBucket> m_listofBuckets;
        private Dictionary<RenderBucketID, RenderBucket> m_renderBuckets;
        private Dictionary<MarkID, IRenderable> m_markedRenderables;
        private int m_version;

        /// <summary>
        /// Gets the number of render buckets present in the queue.
        /// </summary>
        public int Count
        {
            get 
            {
                return m_renderBuckets.Count;
            }
        }

        /// <summary>
        /// Gets the <see cref="RenderBucket"/> with the specified bucket identifier.
        /// </summary>
        /// <param name="bucketID">The bucket identifier.</param>
        public RenderBucket this[RenderBucketID bucketID]
        {
            get 
            {
                if(bucketID.IsValid)
                {
                    RenderBucket bucket;
                    if(m_renderBuckets.TryGetValue(bucketID, out bucket))
                        return bucket;
                }

                return null;
            }
        }

        /// <summary>
        /// Gets the <see cref="RenderBucket"/> with the specified zero-based index.
        /// </summary>
        public RenderBucket this[int index]
        {
            get
            {
                if(index < 0 || index >= m_listofBuckets.Count)
                    return null;

                return m_listofBuckets[index];
            }
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="RenderQueue"/> class.
        /// </summary>
        public RenderQueue()
        {
            m_listofBuckets = new List<RenderBucket>();
            m_renderBuckets = new Dictionary<RenderBucketID, RenderBucket>();
            m_markedRenderables = new Dictionary<MarkID, IRenderable>();
        }

        #region Bucket management

        /// <summary>
        /// Adds a render bucket to the collection.
        /// </summary>
        /// <param name="bucket">Bucket to add.</param>
        /// <returns>True if the bucket was added, false otherwise.</returns>
        public bool AddBucket(RenderBucket bucket)
        {
            if(bucket == null || !bucket.BucketID.IsValid)
                return false;

            if(m_renderBuckets.ContainsKey(bucket.BucketID))
                return false;

            m_renderBuckets.Add(bucket.BucketID, bucket);
            m_listofBuckets.Add(bucket);

            m_version++;

            return true;
        }

        /// <summary>
        /// Removes a render bucket from the collection.
        /// </summary>
        /// <param name="bucketID">Bucket to remove.</param>
        /// <returns>True if the bucket was removed, false otherwise.</returns>
        public bool RemoveBucket(RenderBucketID bucketID)
        {
            if(!bucketID.IsValid)
                return false;

            RenderBucket bucket;
            if(m_renderBuckets.TryGetValue(bucketID, out bucket))
            {
                m_renderBuckets.Remove(bucketID);
                m_listofBuckets.Remove(bucket);

                m_version++;

                return true;
            }

            return false;
        }

        /// <summary>
        /// Removes all render buckets from the collection.
        /// </summary>
        public void RemoveAllBuckets()
        {
            m_renderBuckets.Clear();
            m_listofBuckets.Clear();
            m_version++;
        }

        /// <summary>
        /// Tries to get the render bucket corresponding to the specified ID.
        /// </summary>
        /// <param name="bucketID">Bucket ID.</param>
        /// <param name="bucket">The render bucket, or null if it was not contained in the collection.</param>
        /// <returns>True if the bucket was found, false if otherwise.</returns>
        public bool TryGetBucket(RenderBucketID bucketID, out RenderBucket bucket)
        {
            bucket = null;

            if(!bucketID.IsValid)
                return false;

            return m_renderBuckets.TryGetValue(bucketID, out bucket);
        }

        #endregion

        #region Clear buckets

        /// <summary>
        /// Clears all render buckets.
        /// </summary>
        public void ClearBuckets(bool notifyMarked = true)
        {
            ClearBuckets(notifyMarked, null);
        }

        /// <summary>
        /// Clears the specified render buckets.
        /// </summary>
        /// <param name="bucketIDs">IDs of buckets whose contents should be cleared.</param>
        public void ClearBuckets(bool notifyMarked = true, params RenderBucketID[] bucketIDs)
        {
            if(bucketIDs == null)
            {
                for(int i = 0; i < m_listofBuckets.Count; i++)
                    m_listofBuckets[i].Clear();
            }
            else
            {
                for(int i = 0; i < bucketIDs.Length; i++)
                {
                    RenderBucketID id = bucketIDs[i];
                    RenderBucket bucket;
                    if(TryGetBucket(id, out bucket))
                        bucket.Clear();
                }
            }

            ClearMarks(notifyMarked);
        }

        #endregion

        #region Sort buckets

        /// <summary>
        /// Sorts all render buckets.
        /// </summary>
        /// <param name="cam">Camera to use for sorting.</param>
        public void SortBuckets(Camera cam)
        {
            SortBuckets(cam, null);
        }

        /// <summary>
        /// Sorts the specified render buckets.
        /// </summary>
        /// <param name="cam">Camera used for sorting.</param>
        /// <param name="bucketIDs">IDs of buckets whose contents should be sorted.</param>
        public void SortBuckets(Camera cam, params RenderBucketID[] bucketIDs)
        {
            if(bucketIDs == null)
            {
                for(int i = 0; i < m_listofBuckets.Count; i++)
                {
                    RenderBucket bucket = m_listofBuckets[i];
                    IRenderBucketEntryComparer comparer = bucket.BucketComparer;
                    comparer.SetCamera(cam);
                    bucket.Sort();
                    comparer.SetCamera(null);
                }
            }
            else
            {
                for(int i = 0; i < bucketIDs.Length; i++)
                {
                    RenderBucketID id = bucketIDs[i];
                    RenderBucket bucket;
                    if(TryGetBucket(id, out bucket))
                    {
                        IRenderBucketEntryComparer comparer = bucket.BucketComparer;
                        comparer.SetCamera(cam);
                        bucket.Sort();
                        comparer.SetCamera(null);
                    }
                }
            }
        }

        #endregion

        #region Render buckets

        /// <summary>
        /// Draws all renderables in all render buckets.
        /// </summary>
        /// <param name="renderContext">Render context.</param>
        public void RenderBuckets(IRenderContext renderContext)
        {
            RenderBuckets(renderContext, true, null);
        }

        /// <summary>
        /// Draws all renderables in all render buckets.
        /// </summary>
        /// <param name="renderContext">Render context.</param>
        /// <param name="applyMaterials">True if materials should be applied, otherwise no state is set by the buckets during drawing.</param>
        public void RenderBuckets(IRenderContext renderContext, bool applyMaterials)
        {
            RenderBuckets(renderContext, applyMaterials, null);
        }

        /// <summary>
        /// Draws all renderables in the specified render buckets.
        /// </summary>
        /// <param name="renderContext">Render context.</param>
        /// <param name="bucketIDs">IDs of buckets whose contents should be drawn.</param>
        public void RenderBuckets(IRenderContext renderContext, params RenderBucketID[] bucketIDs)
        {
            RenderBuckets(renderContext, true, bucketIDs);
        }

        /// <summary>
        /// Draws all renderables in the specified render buckets.
        /// </summary>
        /// <param name="renderContext">Render context.</param>
        /// <param name="applyMaterials">True if materials should be applied, otherwise no state is set by the buckets during drawing.</param>
        /// <param name="bucketIDs">IDs of buckets whose contents should be drawn.</param>
        public void RenderBuckets(IRenderContext renderContext, bool applyMaterials, params RenderBucketID[] bucketIDs)
        {
            if(bucketIDs == null)
            {
                for(int i = 0; i < m_listofBuckets.Count; i++)
                    m_listofBuckets[i].DrawAll(renderContext, applyMaterials);
            }
            else
            {
                for(int i = 0; i < bucketIDs.Length; i++)
                {
                    RenderBucketID id = bucketIDs[i];
                    RenderBucket bucket;
                    if(TryGetBucket(id, out bucket))
                        bucket.DrawAll(renderContext, applyMaterials);
                }
            }
        }

        /// <summary>
        /// Draws all renderables in all render buckets using the supplied draw function.
        /// </summary>
        /// <param name="renderContext">Render context.</param>
        /// <param name="drawMethod">Method for drawing renderables.</param>
        public void RenderBuckets(IRenderContext renderContext, DrawRenderBucket drawMethod)
        {
            RenderBuckets(renderContext, drawMethod, null);
        }

        /// <summary>
        /// Draws all renderables in the specified render buckets using the supplied draw function.
        /// </summary>
        /// <param name="renderContext">Render context.</param>
        /// <param name="drawMethod">Method for drawing renderables.</param>
        /// <param name="bucketIDs">IDs of buckets whose contents should be drawn.</param>
        public void RenderBuckets(IRenderContext renderContext, DrawRenderBucket drawMethod, params RenderBucketID[] bucketIDs)
        {
            if(bucketIDs == null)
            {
                for(int i = 0; i < m_listofBuckets.Count; i++)
                    drawMethod(renderContext, m_listofBuckets[i]);
            }
            else
            {
                for(int i = 0; i < bucketIDs.Length; i++)
                {
                    RenderBucketID id = bucketIDs[i];
                    RenderBucket bucket;
                    if(TryGetBucket(id, out bucket))
                        drawMethod(renderContext, bucket);
                }
            }
        }

        #endregion

        #region Renderable processing

        /// <summary>
        /// Adds the renderable to the queue.
        /// </summary>
        /// <param name="renderable">Renderable to queue for drawing.</param>
        /// <returns>True if the renderable was successfully added, false otherwise.</returns>
        public bool Enqueue(IRenderable renderable)
        {
            if(renderable == null)
                return false;

            bool addedOne = false;
            foreach(KeyValuePair<RenderBucketID, Material> kv in renderable.MaterialDefinition)
            {
                RenderBucket bucket;
                if(kv.Key.IsValid && m_renderBuckets.TryGetValue(kv.Key, out bucket))
                    addedOne |= bucket.Add(renderable, kv.Value);
            }

            return addedOne;
        }

        /// <summary>
        /// Adds a list of renderables to the queue.
        /// </summary>
        /// <param name="renderables">Renderables to queue for drawing.</param>
        /// <returns>True if one or more renderable were successfully added, false if otherwise.</returns>
        public bool Enqueue(IReadOnlyList<IRenderable> renderables)
        {
            if (renderables == null)
                return false;

            bool addedOne = false;
            for(int i = 0; i < renderables.Count; i++)
            {
                IRenderable renderable = renderables[i];
                if (renderable == null)
                    continue;

                foreach(KeyValuePair<RenderBucketID, Material> kv in renderable.MaterialDefinition)
                {
                    RenderBucket bucket;
                    if(kv.Key.IsValid && m_renderBuckets.TryGetValue(kv.Key, out bucket))
                        addedOne |= bucket.Add(renderable, kv.Value);
                }
            }

            return addedOne;
        }

        /// <summary>
        /// Removes the renderable from the queue.
        /// </summary>
        /// <param name="renderable">Renderable to remove from the queue.</param>
        /// <returns>True if the renderable was successfully removed, false otherwise.</returns>
        public bool Dequeue(IRenderable renderable)
        {
            if(renderable == null)
                return false;

            bool removedOne = false;

            foreach(KeyValuePair<RenderBucketID, Material> kv in renderable.MaterialDefinition)
            {
                RenderBucket bucket;
                if(kv.Key.IsValid && m_renderBuckets.TryGetValue(kv.Key, out bucket))
                    removedOne |= bucket.Remove(renderable);
            }

            return removedOne;
        }

        /// <summary>
        /// Removes the renderables from the queue.
        /// </summary>
        /// <param name="renderables">Renderables to remove from the queue.</param>
        /// <returns>True if the renderable was successfully removed, false otherwise.</returns>
        public bool Dequeue(IReadOnlyList<IRenderable> renderables)
        {
            if (renderables == null)
                return false;

            bool removedOne = false;
            for (int i = 0; i < renderables.Count; i++)
            {
                IRenderable renderable = renderables[i];
                if (renderable == null)
                    continue;

                foreach (KeyValuePair<RenderBucketID, Material> kv in renderable.MaterialDefinition)
                {
                    RenderBucket bucket;
                    if (kv.Key.IsValid && m_renderBuckets.TryGetValue(kv.Key, out bucket))
                        removedOne |= bucket.Remove(renderable);
                }
            }

            return removedOne;
        }

        #endregion

        #region Mark API

        /// <summary>
        /// Marks the renderable. This is useful during processing to track a renderable.
        /// </summary>
        /// <param name="id">Mark ID to identify the renderable.</param>
        /// <param name="renderable">Renderable to mark.</param>
        /// <returns>True if the renderable was added to the marked list, false if it is invalid or already marked.</returns>
        public bool Mark(MarkID id, IRenderable renderable)
        {
            if (!id.IsValid || renderable == null || m_markedRenderables.ContainsKey(id))
                return false;

            m_markedRenderables.Add(id, renderable);
            return true;
        }

        /// <summary>
        /// Unmarks the renderable identified by the ID.
        /// </summary>
        /// <param name="id">Mark ID to identify the renderable.</param>
        /// <returns>True if the renderable was removed from the marked list, false otherwise.</returns>
        public bool Unmark(MarkID id)
        {
            if (!id.IsValid)
                return false;

            IRenderable rnd;
            if(m_markedRenderables.TryGetValue(id, out rnd))
            {
                IMarkedRenderable mkRnd = rnd as IMarkedRenderable;
                if(mkRnd != null)
                    mkRnd.OnMarkCleared(id, this);

                m_markedRenderables.Remove(id);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Queries if the renderable identified by the ID has been marked by this queue.
        /// </summary>
        /// <param name="id">Mark ID to identify the renerable.</param>
        public bool IsMarked(MarkID id)
        {
            if(!id.IsValid)
                return false;

            return m_markedRenderables.ContainsKey(id);
        }

        /// <summary>
        /// Gets the marked renderable as identified by the ID. If the renderable is not present then null is returned.
        /// </summary>
        /// <param name="id">Mark ID to identify the renderable.</param>
        /// <returns>The marked renderable, or null if it was not found.</returns>
        public IRenderable GetMarked(MarkID id)
        {
            if(!id.IsValid)
                return null;

            IRenderable rnd;
            m_markedRenderables.TryGetValue(id, out rnd);

            return rnd;
        }

        /// <summary>
        /// Clears all marked renderables from the queue.
        /// </summary>
        public void ClearMarks(bool notifyMarked = true)
        {
            if (notifyMarked)
            {
                foreach (KeyValuePair<MarkID, IRenderable> kv in m_markedRenderables)
                {
                    IMarkedRenderable mkRend = kv.Value as IMarkedRenderable;
                    if (mkRend != null)
                        mkRend.OnMarkCleared(kv.Key, this);
                }
            }

            m_markedRenderables.Clear();
        }

        #endregion

        /// <summary>
        /// Copies the contents of this render queue into the other render queue.
        /// </summary>
        /// <param name="renderqueue">Render queue to copy contents to.</param>
        public void CopyTo(RenderQueue renderqueue)
        {
            if(renderqueue == null)
                return;

            for(int i = 0; i < m_listofBuckets.Count; i++)
            {
                RenderBucket bucket = m_listofBuckets[i];
                RenderBucket other;
                if(renderqueue.TryGetBucket(bucket.BucketID, out other))
                {
                    bucket.CopyTo(other);
                }
            }

            foreach(KeyValuePair<MarkID, IRenderable> kv in m_markedRenderables)
            {
                if (!renderqueue.m_markedRenderables.ContainsKey(kv.Key))
                    renderqueue.m_markedRenderables.Add(kv.Key, kv.Value);
            }
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>An enumerator that can be used to iterate through the collection.</returns>
        public RenderQueueEnumerator GetEnumerator()
        {
            return new RenderQueueEnumerator(this);
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.</returns>
        IEnumerator<RenderBucket> IEnumerable<RenderBucket>.GetEnumerator()
        {
            return new RenderQueueEnumerator(this);
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return new RenderQueueEnumerator(this);
        }

        #region Enumerator

        /// <summary>
        /// Enumerates elements of a <see cref="RenderQueue"/>.
        /// </summary>
        [Serializable, StructLayout(LayoutKind.Sequential)]
        public struct RenderQueueEnumerator : IEnumerator<RenderBucket>
        {
            private RenderQueue m_queue;
            private List<RenderBucket> m_buckets;
            private int m_index;
            private int m_count;
            private int m_version;
            private RenderBucket m_current;

            /// <summary>
            /// Gets the current value.
            /// </summary>
            public RenderBucket Current
            {
                get
                {
                    return m_current;
                }
            }

            object IEnumerator.Current
            {
                get
                {
                    return m_current;
                }
            }

            internal RenderQueueEnumerator(RenderQueue queue)
            {
                m_queue = queue;
                m_buckets = queue.m_listofBuckets;
                m_count = m_buckets.Count;
                m_index = 0;
                m_version = queue.m_version;
                m_current = null;
            }

            /// <summary>
            /// Advances the enumerator to the next element of the collection.
            /// </summary>
            /// <returns>True if the enumerator was successfully advanced to the next element; false if the enumerator has passed the end of the collection.</returns>
            public bool MoveNext()
            {
                ThrowIfChanged();

                if(m_index < m_count)
                {
                    m_current = m_buckets[m_index];
                    m_index++;
                    return true;
                }
                else
                {
                    m_index++;
                    m_current = null;
                    return false;
                }
            }

            /// <summary>
            /// Sets the enumerator to its initial position, which is before the first element in the collection.
            /// </summary>
            public void Reset()
            {
                ThrowIfChanged();
                m_index = 0;
                m_current = null;
            }

            /// <summary>
            /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
            /// </summary>
            public void Dispose() { }

            private void ThrowIfChanged()
            {
                if(m_version != m_queue.m_version)
                    throw new InvalidOperationException(StringLocalizer.Instance.GetLocalizedString("CollectionModifiedDuringEnumeration"));
            }
        }

        #endregion
    }
}
