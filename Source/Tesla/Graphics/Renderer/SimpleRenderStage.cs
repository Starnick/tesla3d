﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System.Collections.Generic;

namespace Tesla.Graphics
{
    /// <summary>
    /// A simple rendering stage that draws a single bucket with materials.
    /// </summary>
    public sealed class SimpleRenderStage : IRenderStage
    {
        private RenderBucketID m_bucketToDraw;

        /// <summary>
        /// Gets or sets the bucket to draw.
        /// </summary>
        public RenderBucketID BucketToDraw
        {
            get
            {
                return m_bucketToDraw;
            }
            set
            {
                m_bucketToDraw = value;
            }
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="SimpleRenderStage"/> class.
        /// </summary>
        /// <param name="id">The bucket identifier.</param>
        public SimpleRenderStage(RenderBucketID id)
        {
            m_bucketToDraw = id;
        }

        /// <summary>
        /// Executes the draw logic of the stage.
        /// </summary>
        /// <param name="queue">Render queue of objects that are to be drawn by the renderer.</param>
        /// <param name="renderContext">Render context of the renderer.</param>
        public void Execute(IRenderContext renderContext, RenderQueue queue)
        {
            RenderBucket bucket = queue[m_bucketToDraw];
            if(bucket != null)
                bucket.DrawAll(renderContext, true);
        }
    }

    /// <summary>
    /// A simple rendering stage that just draws the specified buckets with their materials.
    /// </summary>
    public sealed class SimpleMultiRenderStage : IRenderStage
    {
        private List<RenderBucketID> m_bucketsToDraw;

        /// <summary>
        /// Gets the list of render buckets that will be drawn, in the supplied order of the list, by the render stage.
        /// </summary>
        public List<RenderBucketID> BucketsToDraw
        {
            get
            {
                return m_bucketsToDraw;
            }
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="SimpleMultiRenderStage"/> class.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public SimpleMultiRenderStage(RenderBucketID id)
        {
            m_bucketsToDraw = new List<RenderBucketID>(1);
            m_bucketsToDraw.Add(id);
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="SimpleMultiRenderStage"/> class.
        /// </summary>
        /// <param name="ids">The ids.</param>
        public SimpleMultiRenderStage(params RenderBucketID[] ids)
        {
            m_bucketsToDraw = new List<RenderBucketID>(ids);
        }

        /// <summary>
        /// Executes the draw logic of the stage.
        /// </summary>
        /// <param name="queue">Render queue of objects that are to be drawn by the renderer.</param>
        /// <param name="renderContext">Render context of the renderer.</param>
        public void Execute(IRenderContext renderContext, RenderQueue queue)
        {
            for(int i = 0; i < m_bucketsToDraw.Count; i++)
            {
                RenderBucketID id = m_bucketsToDraw[i];
                RenderBucket bucket = queue[m_bucketsToDraw[i]];
                if(bucket != null)
                    bucket.DrawAll(renderContext, true);
            }
        }
    }
}