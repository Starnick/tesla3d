﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading;
using Tesla.Content;

namespace Tesla.Graphics
{
    /// <summary>
    /// Identifies a render bucket in the render queue.
    /// </summary>
    [Serializable]
    [StructLayout(LayoutKind.Sequential, Size = 4)]
    public struct RenderBucketID : IEquatable<RenderBucketID>, IComparable<RenderBucketID>
    {
        private String m_name;
        private int m_idValue;

        private static readonly RenderBucketID s_invalid;
        private static readonly RenderBucketID s_shadowMap;
        private static readonly RenderBucketID s_preOpaque;
        private static readonly RenderBucketID s_opaque;
        private static readonly RenderBucketID s_transparent;
        private static readonly RenderBucketID s_ortho;
        private static readonly RenderBucketID s_postOpaque;

        private const String INVALID_NAME = "Invalid";
        private static Dictionary<String, RenderBucketID> s_registeredIDs;
        private static int s_globalCurrentId = -1;

        /// <summary>
        /// Gets the invalid bucket ID.
        /// </summary>
        public static RenderBucketID Invalid
        {
            get
            {
                return s_invalid;
            }
        }

        /// <summary>
        /// Gets a predefined bucket ID: ShadowMap - Any (generally non-transparent) geometry that casts a shadow and should be included in shadow map generation, sorted by material and/or front-to-back.
        /// </summary>
        public static RenderBucketID ShadowMap
        {
            get
            {
                return s_shadowMap;
            }
        }

        /// <summary>
        /// Gets a predefined bucket ID: PreOpaque - Standard non-transparent geometry guaranteed to be rendered first, sorted by material and/or front-to-back.
        /// </summary>
        public static RenderBucketID PreOpaque
        {
            get
            {
                return s_preOpaque;
            }
        }

        /// <summary>
        /// Gets a predefined bucket ID: Opaque - Standard non-transparent geometry rendering, sorted by material and/or front-to-back.
        /// </summary>
        public static RenderBucketID Opaque
        {
            get
            {
                return s_opaque;
            }
        }

        /// <summary>
        /// Gets a predefined bucket ID: Transparent - Transparent geometry rendering, sorted back-to-front.
        /// </summary>
        public static RenderBucketID Transparent
        {
            get
            {
                return s_transparent;
            }
        }

        /// <summary>
        /// Gets a predefined bucket ID: Ortho - Geometry that should be rendered with an orthographic projection (or sprites), sorted by
        /// an ortho order defined by <see cref="OrthoOrderProperty"/>.
        /// </summary>
        public static RenderBucketID Ortho
        {
            get
            {
                return s_ortho;
            }
        }

        /// <summary>
        /// Gets a predefined bucket ID: PostOpaque - Standard non-transparent geometry guaranted to be rendered last, sorted by material and/or front-to-back.
        /// </summary>
        public static RenderBucketID PostOpaque
        {
            get
            {
                return s_postOpaque;
            }
        }

        /// <summary>
        /// Gets the name of the render bucket.
        /// </summary>
        public String Name
        {
            get 
            {
                return m_name;
            }
        }

        /// <summary>
        /// Gets the integer value of the ID.
        /// </summary>
        public int Value
        {
            get
            {
                return m_idValue;
            }
        }

        /// <summary>
        /// Gets if the ID is valid (greater than or equal to zero).
        /// </summary>
        public bool IsValid
        {
            get
            {
                return m_idValue >= 0;
            }
        }

        static RenderBucketID()
        {
            s_registeredIDs = new Dictionary<String, RenderBucketID>();

            s_invalid = new RenderBucketID(INVALID_NAME, -1);
            s_shadowMap = RegisterID("ShadowMap");
            s_preOpaque = RegisterID("PreOpaque");
            s_opaque = RegisterID("Opaque");
            s_transparent = RegisterID("Transparent");
            s_ortho = RegisterID("Ortho");
            s_postOpaque = RegisterID("PostOpaque");
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="RenderBucketID"/> struct.
        /// </summary>
        /// <param name="name">Name of the render bucket.</param>
        /// <param name="idValue">The integer ID value.</param>
        public RenderBucketID(String name, int idValue)
        {
            if(idValue < 0)
            {
                m_name = INVALID_NAME;
                m_idValue = -1;
            }
            else
            {
                if(String.IsNullOrEmpty(name))
                    throw new ArgumentNullException("name");

                m_name = name;
                m_idValue = idValue;
            }
        }

        /// <summary>
        /// Registers a new render bucket. This is not thread safe, all render buckets should be registered up front at the start of the application. Buckets
        /// are referenced by a unique string name.
        /// </summary>
        /// <param name="bucketName">Unique render bucket name.</param>
        /// <returns>New render bucket ID.</returns>
        public static RenderBucketID RegisterID(String bucketName)
        {
            if(String.IsNullOrEmpty(bucketName))
                throw new ArgumentException(StringLocalizer.Instance.GetLocalizedString("RenderBucketID_InvalidName"), "bucketName");

            RenderBucketID oldId;
            if(s_registeredIDs.TryGetValue(bucketName, out oldId))
                return oldId;

            Interlocked.Increment(ref s_globalCurrentId);
            RenderBucketID newId = new RenderBucketID(bucketName, s_globalCurrentId);
            s_registeredIDs.Add(bucketName, newId);

            return newId;
        }

        /// <summary>
        /// Queries a render bucket ID from the global map by its name.
        /// </summary>
        /// <param name="name">Name of the render bucket ID.</param>
        /// <returns>The corrosponding render bucket ID, or the <see cref="Invalid"/> ID if it could not be found.</returns>
        public static RenderBucketID QueryID(String name)
        {
            if(String.IsNullOrEmpty(name))
                return RenderBucketID.Invalid;

            RenderBucketID id;
            if(s_registeredIDs.TryGetValue(name, out id))
                return id;

            return RenderBucketID.Invalid;
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
        public override int GetHashCode()
        {
            unchecked
            {
                return m_idValue;
            }
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object" />, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns><c>True</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.</returns>
        public override bool Equals(object obj)
        {
            if(obj is RenderBucketID)
                return ((RenderBucketID) obj).m_idValue == m_idValue;

            return false;
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>True if the current object is equal to the <paramref name="other" /> parameter; otherwise, false.</returns>
        public bool Equals(RenderBucketID other)
        {
            return other.m_idValue == m_idValue;
        }

        /// <summary>
        /// Compares the current object with another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>A value that indicates the relative order of the objects being compared. The return value has the following meanings: Value Meaning Less than zero This object is less than the <paramref name="other" /> parameter.Zero This object is equal to <paramref name="other" />. Greater than zero This object is greater than <paramref name="other" />.</returns>
        public int CompareTo(RenderBucketID other)
        {
            if(m_idValue < other.m_idValue)
                return -1;

            if(m_idValue > other.m_idValue)
                return 1;

            return 0;
        }

        /// <summary>
        /// Checks inequality between two render bucket IDs.
        /// </summary>
        /// <param name="a">First ID.</param>
        /// <param name="b">Second ID.</param>
        /// <returns>True if the two values are not the same, false otherwise.</returns>
        public static bool operator !=(RenderBucketID a, RenderBucketID b)
        {
            return a.m_idValue != b.m_idValue;
        }

        /// <summary>
        /// Checks equality between two render bucket IDs.
        /// </summary>
        /// <param name="a">First ID.</param>
        /// <param name="b">Second ID.</param>
        /// <returns>True if the two values are the same, false otherwise.</returns>
        public static bool operator ==(RenderBucketID a, RenderBucketID b)
        {
            return a.m_idValue == b.m_idValue;
        }

        /// <summary>
        /// Checks if the first render bucket ID is greater than the second.
        /// </summary>
        /// <param name="a">First ID.</param>
        /// <param name="b">Second ID.</param>
        /// <returns>True if the first value is greater than the second, false otherwise.</returns>
        public static bool operator >(RenderBucketID a, RenderBucketID b)
        {
            return a.m_idValue > b.m_idValue;
        }

        /// <summary>
        /// Checks if the first render bucket ID is less than the second.
        /// </summary>
        /// <param name="a">First ID.</param>
        /// <param name="b">Second ID.</param>
        /// <returns>True if the first value is less than the second, false otherwise.</returns>
        public static bool operator <(RenderBucketID a, RenderBucketID b)
        {
            return a.m_idValue < b.m_idValue;
        }

        /// <summary>
        /// Checks if the first render bucket ID is greater than or equal to the second.
        /// </summary>
        /// <param name="a">First ID.</param>
        /// <param name="b">Second ID.</param>
        /// <returns>True if the first value is greater than or equal to the second, false otherwise.</returns>
        public static bool operator >=(RenderBucketID a, RenderBucketID b)
        {
            return a.m_idValue >= b.m_idValue;
        }

        /// <summary>
        /// Checks if the first render bucket ID is less than or equal to the second.
        /// </summary>
        /// <param name="a">First ID.</param>
        /// <param name="b">Second ID.</param>
        /// <returns>True if the first value is less than or equal to the second, false otherwise.</returns>
        public static bool operator <=(RenderBucketID a, RenderBucketID b)
        {
            return a.m_idValue <= b.m_idValue;
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
        public override String ToString()
        {
            return m_name;
        }
    }
}
