﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

namespace Tesla.Graphics
{
    /// <summary>
    /// Simple forward renderer.
    /// </summary>
    public sealed class ForwardRenderer : BaseRenderer
    {
        /// <summary>
        /// Constructs a new instance of the <see cref="ForwardRenderer"/> class.
        /// </summary>
        public ForwardRenderer(IRenderContext renderContext) : base(renderContext)
        {
            SetDefaultStages();
        }

        //These correspond to the default render bucket IDs.
        private void SetDefaultStages()
        {
            OpaqueRenderBucketComparer opaqueComparer = new OpaqueRenderBucketComparer();
            TransparentRenderBucketComparer transparentComparer = new TransparentRenderBucketComparer();
            OrthoRenderBucketComparer orthoComparer = new OrthoRenderBucketComparer();
            int initialSize = 32;

            RenderQueue.AddBucket(new RenderBucket(RenderBucketID.PreOpaque, opaqueComparer, initialSize));
            RenderQueue.AddBucket(new RenderBucket(RenderBucketID.Opaque, opaqueComparer, initialSize));
            RenderQueue.AddBucket(new RenderBucket(RenderBucketID.Transparent, transparentComparer, initialSize));
            RenderQueue.AddBucket(new RenderBucket(RenderBucketID.Ortho, orthoComparer, initialSize));
            RenderQueue.AddBucket(new RenderBucket(RenderBucketID.PostOpaque, opaqueComparer, initialSize));

            RenderStages.AddStage(new SimpleRenderStage(RenderBucketID.PreOpaque));
            RenderStages.AddStage(new SimpleRenderStage(RenderBucketID.Opaque));
            RenderStages.AddStage(new TransparentRenderStage(RenderContext.RenderSystem, RenderBucketID.Transparent));
            RenderStages.AddStage(new SimpleRenderStage(RenderBucketID.Ortho));
            RenderStages.AddStage(new SimpleRenderStage(RenderBucketID.PostOpaque));
        }
    }
}
