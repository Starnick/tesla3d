﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System.Collections.Generic;

namespace Tesla.Graphics
{
    /// <summary>
    /// Compares renderables based on their <see cref="OrthoOrderProperty"/>.
    /// </summary>
    public sealed class OrthoRenderBucketComparer : IRenderBucketEntryComparer
    {
        /// <summary>
        /// Constructs a new instance of the <see cref="OrthoRenderBucketComparer"/> class.
        /// </summary>
        public OrthoRenderBucketComparer() { }

        /// <summary>
        /// Sets a camera to be used by the comparer during sorting.
        /// </summary>
        /// <param name="cam">Camera to use during sorting.</param>
        public void SetCamera(Camera cam)
        {
            //no-op
        }

        /// <summary>
        /// Compares two objects and returns a value indicating whether one is less than, equal to, or greater than the other.
        /// </summary>
        /// <param name="x">The first object to compare.</param>
        /// <param name="y">The second object to compare.</param>
        /// <returns>A signed integer that indicates the relative values of <paramref name="x" /> and <paramref name="y" />, as shown in the following table.Value Meaning Less than zero<paramref name="x" /> is less than <paramref name="y" />.Zero<paramref name="x" /> equals <paramref name="y" />.Greater than zero<paramref name="x" /> is greater than <paramref name="y" />.</returns>
        public int Compare(RenderBucketEntry x, RenderBucketEntry y)
        {
            OrthoOrderProperty orthoX, orthoY;

            if(x.Renderable.RenderProperties.TryGet<OrthoOrderProperty>(out orthoX) && y.Renderable.RenderProperties.TryGet<OrthoOrderProperty>(out orthoY))
            {
                int oX = orthoX.Value;
                int oY = orthoY.Value;

                if(oX < oY)
                    return -1;

                if(oX > oY)
                    return 1;
            }

            return 0;
        }
    }
}
