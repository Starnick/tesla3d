﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tesla.Graphics
{
    /// <summary>
    /// Compares renderables based on distance to the camera in order to ensure a back-to-front ordering for correct transparent rendering.
    /// </summary>
    public sealed class TransparentRenderBucketComparer : IRenderBucketEntryComparer
    {
        private Camera m_cam;

        /// <summary>
        /// Construct a new instance of the <see cref="TransparentRenderBucketComparer"/> class.
        /// </summary>
        public TransparentRenderBucketComparer()
        {
        }

        /// <summary>
        /// Sets a camera to be used by the comparer during sorting.
        /// </summary>
        /// <param name="cam">Camera to use during sorting.</param>
        public void SetCamera(Camera cam)
        {
            m_cam = cam;
        }

        /// <summary>
        /// Compares two objects and returns a value indicating whether one is less than, equal to, or greater than the other.
        /// </summary>
        /// <param name="x">The first object to compare.</param>
        /// <param name="y">The second object to compare.</param>
        /// <returns>A signed integer that indicates the relative values of <paramref name="x" /> and <paramref name="y" />, as shown in the following table.Value Meaning Less than zero<paramref name="x" /> is less than <paramref name="y" />.Zero<paramref name="x" /> equals <paramref name="y" />.Greater than zero<paramref name="x" /> is greater than <paramref name="y" />.</returns>
        public int Compare(RenderBucketEntry x, RenderBucketEntry y)
        {
            //Some asserts, hould always have a camera during sorting
            System.Diagnostics.Debug.Assert(m_cam != null);

            //Sort back-to-front
            float distX = DistanceToCamera(x.Renderable, m_cam);
            float distY = DistanceToCamera(y.Renderable, m_cam);

            if(distX < distY)
                return 1;

            if(distX > distY)
                return -1;

            return 0;
        }

        private float DistanceToCamera(IRenderable renderable, Camera cam)
        {
            Vector3 camPos = cam.Position;
            Vector3 camDir = cam.Direction;

            Vector3 worldPos = renderable.WorldTransform.Translation;
            Vector3 distVector;

            Vector3.Subtract(worldPos, camPos, out distVector);
            float retVal = Vector3.Dot(distVector, camDir);
            float temp = Vector3.Dot(camDir, camDir);

            retVal = Math.Abs(retVal / temp);
            Vector3.Multiply(camDir, retVal, out distVector);

            return distVector.Length();
        }
    }
}
