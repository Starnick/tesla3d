﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Tesla.Graphics
{
    /// <summary>
    /// Collection of render stages.
    /// </summary>
    public class RenderStageCollection : IReadOnlyList<IRenderStage>
    {
        private List<IRenderStage> m_stages;
        private int m_version;

        /// <summary>
        /// Gets the number of render stages in the collection.
        /// </summary>
        public int Count
        {
            get
            {
                return m_stages.Count;
            }
        }

        /// <summary>
        /// Gets the <see cref="IRenderStage"/> with the specified zero-based index.
        /// </summary>
        public IRenderStage this[int index]
        {
            get
            {
                if(index < 0 || index >= m_stages.Count)
                    return null;

                return m_stages[index];
            }
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="RenderStageCollection"/> class.
        /// </summary>
        public RenderStageCollection()
        {
            m_stages = new List<IRenderStage>();
        }

        /// <summary>
        /// Adds a render stage to the collection.
        /// </summary>
        /// <param name="stage">Render stage to add.</param>
        /// <returns>True if the stage wass added, false otherwise.</returns>
        public bool AddStage(IRenderStage stage)
        {
            if(stage == null)
                return false;

            m_stages.Add(stage);
            m_version++;

            return true;
        }

        /// <summary>
        /// Inserts the render stage at the specified index.
        /// </summary>
        /// <param name="index">Zero-based index at which to inser the stage into the collection.</param>
        /// <param name="stage">Render stage to add.</param>
        /// <returns>True if the stage was inserted, false otherwise.</returns>
        public bool InsertStage(int index, IRenderStage stage)
        {
            if(index < 0 || index >= m_stages.Count || stage == null)
                return false;

            m_stages.Insert(index, stage);
            m_version++;

            return true;
        }

        /// <summary>
        /// Removes a render stage from the collection.
        /// </summary>
        /// <param name="stage">Render stage to remove.</param>
        /// <returns>True if the stage was removed, false otherwise.</returns>
        public bool RemoveStage(IRenderStage stage)
        {
            if(stage == null)
                return false;

            bool removed =  m_stages.Remove(stage);
            if(removed)
                m_version++;

            return removed;
        }

        /// <summary>
        /// Removes a render stage at the specified index in the collection.
        /// </summary>
        /// <param name="index">Zero-based index of the render stage.</param>
        /// <returns>True if the stage was removed, false if otherwise.</returns>
        public bool RemoveStageAt(int index)
        {
            if(index < 0 || index >= m_stages.Count)
                return false;

            m_stages.RemoveAt(index);
            m_version++;

            return true;
        }

        /// <summary>
        /// Removes all render stages from the collection.
        /// </summary>
        public void RemoveAllStages()
        {
            m_stages.Clear();
            m_version++;
        }

        /// <summary>
        /// Executes all the render stages in the collection.
        /// </summary>
        /// <param name="renderContext">Render context.</param>
        /// <param name="queue">Render queue of objects to be processed.</param>
        public void ExecuteStages(IRenderContext renderContext, RenderQueue queue)
        {
            for(int i = 0; i < m_stages.Count; i++)
                m_stages[i].Execute(renderContext, queue);
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>An enumerator that can be used to iterate through the collection.</returns>
        public RenderStageCollectionEnumerator GetEnumerator()
        {
            return new RenderStageCollectionEnumerator(this);
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>An enumerator that can be used to iterate through the collection.</returns>
        IEnumerator<IRenderStage> IEnumerable<IRenderStage>.GetEnumerator()
        {
            return new RenderStageCollectionEnumerator(this);
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return new RenderStageCollectionEnumerator(this);
        }

        #region Enumerator

        /// <summary>
        /// Enumerates elements of a <see cref="RenderStageCollection"/>.
        /// </summary>
        [Serializable, StructLayout(LayoutKind.Sequential)]
        public struct RenderStageCollectionEnumerator : IEnumerator<IRenderStage>
        {
            private RenderStageCollection m_renderStages;
            private List<IRenderStage> m_stages;
            private int m_index;
            private int m_count;
            private int m_version;
            private IRenderStage m_current;

            /// <summary>
            /// Gets the current value.
            /// </summary>
            public IRenderStage Current
            {
                get 
                {
                    return m_current;
                }
            }

            object IEnumerator.Current
            {
                get 
                {
                    return m_current;
                }
            }

            internal RenderStageCollectionEnumerator(RenderStageCollection renderStages)
            {
                m_renderStages = renderStages;
                m_stages = renderStages.m_stages;
                m_index = 0;
                m_count = m_stages.Count;
                m_version = renderStages.m_version;
                m_current = null;
            }

            /// <summary>
            /// Advances the enumerator to the next element of the collection.
            /// </summary>
            /// <returns>True if the enumerator was successfully advanced to the next element; false if the enumerator has passed the end of the collection.</returns>
            public bool MoveNext()
            {
                ThrowIfChanged();

                if(m_index < m_count)
                {
                    m_current = m_stages[m_index];
                    m_index++;
                    return true;
                }
                else
                {
                    m_index++;
                    m_current = null;
                    return false;
                }
            }

            /// <summary>
            /// Sets the enumerator to its initial position, which is before the first element in the collection.
            /// </summary>
            public void Reset()
            {
                ThrowIfChanged();
                m_index = 0;
                m_current = null;
            }

            /// <summary>
            /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
            /// </summary>
            public void Dispose() { }

            private void ThrowIfChanged()
            {
                if(m_version != m_renderStages.m_version)
                    throw new InvalidOperationException(StringLocalizer.Instance.GetLocalizedString("CollectionModifiedDuringEnumeration"));
            }
        }

        #endregion
    }
}
