﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System.Collections.Generic;

namespace Tesla.Graphics
{
    /// <summary>
    /// Defines a coordinator for rendering objects. It defines a queue of objects that serves as input for the renderer and a collection
    /// of stages that serves as the logic that is executed to composite the final rendered image.
    /// </summary>
    public interface IRenderer
    {
        /// <summary>
        /// Gets the render queue that holds the objects pending to be drawn.
        /// </summary>
        RenderQueue RenderQueue { get; }

        /// <summary>
        /// Gets the renderer's render stages that contain the logic in drawing objects and the
        /// order that they are drawn.
        /// </summary>
        RenderStageCollection RenderStages { get; }

        /// <summary>
        /// Gets or sets the render context used to generate draw commands.
        /// </summary>
        IRenderContext RenderContext { get; set; }

        /// <summary>
        /// Process a renderable to be rendered.
        /// </summary>
        /// <param name="renderable">Renderable to process.</param>
        /// <returns>True if the renderable was processed successfully, false otherwise.</returns>
        bool Process(IRenderable renderable);

        /// <summary>
        /// Executes all render stages in the renderer.
        /// </summary>
        /// <param name="sortBuckets">True if the render queue should be sorted before executing render stages, false if not.</param>
        /// <param name="clearBuckets">True if the render queue should be cleared after executing render stages, false if not.</param>
        void Render(bool sortBuckets, bool clearBuckets);
    }

    /// <summary>
    /// Defines an object that can be rendered and thus has a set of properties that describe how it should
    /// be rendered.
    /// </summary>
    public interface IRenderable
    {
        /// <summary>
        /// Gets the material definition that contains the materials used to render the object.
        /// </summary>
        MaterialDefinition MaterialDefinition { get; }

        /// <summary>
        /// Gets the world transform of the renderable. At the bare minimum, this render property should be present in the render properties collection.
        /// </summary>
        Transform WorldTransform { get; }

        /// <summary>
        /// Gets the collection of render properties of the object.
        /// </summary>
        RenderPropertyCollection RenderProperties { get; }

        /// <summary>
        /// Gets if the renderable is valid for drawing.
        /// </summary>
        bool IsValidForDraw { get; }

        /// <summary>
        /// Performs the necessary draw calls to render the object.
        /// </summary>
        /// <param name="renderContext">Render context.</param>
        /// <param name="currentBucketID">The current bucket being drawn, may be invalid.</param>
        /// <param name="currentPass">The current pass that is drawing the renderable, may be null.</param>
        void SetupDrawCall(IRenderContext renderContext, RenderBucketID currentBucketID, MaterialPass currentPass);
    }

    /// <summary>
    /// Defines a renderable that will be notified by a render queue when marks are cleared.
    /// </summary>
    public interface IMarkedRenderable : IRenderable
    {
        /// <summary>
        /// Called when the renderable gets cleared from the queue.
        /// </summary>
        /// <param name="id">ID that corresponded to the renderale in the queue.</param>
        /// <param name="queue">Render queue that the renderable was marked in.</param>
        void OnMarkCleared(MarkID id, RenderQueue queue);
    }

    /// <summary>
    /// Defines a stage in a renderer. Each render stage executes logic to contribute to the final output (e.g. shadow map generation, object rendering,
    /// composition, etc).
    /// </summary>
    public interface IRenderStage
    {
        /// <summary>
        /// Executes the draw logic of the stage.
        /// </summary>
        /// <param name="renderContext">Render context of the renderer.</param>
        /// <param name="queue">Render queue of objects that are to be drawn by the renderer.</param>
        void Execute(IRenderContext renderContext, RenderQueue queue);
    }

    /// <summary>
    /// Defines a comparer for render bucket entries.
    /// </summary>
    public interface IRenderBucketEntryComparer : IComparer<RenderBucketEntry>
    {
        /// <summary>
        /// Sets a camera to be used by the comparer during sorting.
        /// </summary>
        /// <param name="cam">Camera to use during sorting.</param>
        void SetCamera(Camera cam);
    }
}
