﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tesla.Graphics
{
    /// <summary>
    /// Compares renderables primarily by their material to minimize state changes in the graphics pipeline. If two objects
    /// share a similar material, then they are sorted front-to-back to minimize overdraw.
    /// </summary>
    public sealed class OpaqueRenderBucketComparer : IRenderBucketEntryComparer
    {
        private Camera m_cam;

        /// <summary>
        /// Constructs a new instance of the <see cref="OpaqueRenderBucketComparer"/> class.
        /// </summary>
        public OpaqueRenderBucketComparer()
        {
        }

        /// <summary>
        /// Sets a camera to be used by the comparer during sorting.
        /// </summary>
        /// <param name="cam">Camera to use during sorting.</param>
        public void SetCamera(Camera cam)
        {
            m_cam = cam;
        }

        public int Compare(RenderBucketEntry x, RenderBucketEntry y)
        {
            //Compare materials if applicable
            if(x.Material != null && y.Material != null)
            {
                int matCompareResult = x.Material.CompareTo(y.Material);
                if(matCompareResult != 0)
                    return matCompareResult;
            }

            //Some asserts, hould always have a camera during sorting
            System.Diagnostics.Debug.Assert(m_cam != null);

            //Otherwise, do front-back ordering
            float distX = DistanceToCamera(x.Renderable, m_cam);
            float distY = DistanceToCamera(y.Renderable, m_cam);

            if(distX < distY)
                return -1;

            if(distX > distY)
                return 1;

            return 0;
        }

        private float DistanceToCamera(IRenderable renderable, Camera cam)
        {
            Vector3 camPos = cam.Position;
            Vector3 camDir = cam.Direction;

            Vector3 worldPos = renderable.WorldTransform.Translation;
            Vector3 distVector;

            Vector3.Subtract(worldPos, camPos, out distVector);
            float retVal = Vector3.Dot(distVector, camDir);
            float temp = Vector3.Dot(camDir, camDir);

            retVal = Math.Abs(retVal / temp);
            Vector3.Multiply(camDir, retVal, out distVector);

            return distVector.Length();
        }
    }
}
