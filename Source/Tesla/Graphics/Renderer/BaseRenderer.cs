﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;

namespace Tesla.Graphics
{
    /// <summary>
    /// An abstract renderer to serve as the root for other implementations.
    /// </summary>
    public abstract class BaseRenderer : IRenderer
    {
        private RenderQueue m_renderQueue;
        private IRenderContext m_renderContext;
        private RenderStageCollection m_renderStages;

        /// <summary>
        /// Gets or sets the render context used to generate draw commands.
        /// </summary>
        public IRenderContext RenderContext
        {
            get
            {
                return m_renderContext;
            }
            set
            {
                if (value == null)
                {
                    System.Diagnostics.Debug.Assert(true, "RenderContext cannot be null.");
                    return;
                }

                m_renderContext = value;
            }
        }

        /// <summary>
        /// Gets the renderer's render stages that contain the logic in drawing objects and the
        /// order that they are drawn.
        /// </summary>
        public RenderStageCollection RenderStages
        {
            get
            {
                return m_renderStages;
            }
        }

        /// <summary>
        /// Gets the render queue that holds the objects pending to be drawn.
        /// </summary>
        public RenderQueue RenderQueue
        {
            get
            {
                return m_renderQueue;
            }
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="BaseRenderer"/> class.
        /// </summary>
        /// <param name="renderContext">The render context.</param>
        protected BaseRenderer(IRenderContext renderContext)
        {
            if(renderContext == null)
                throw new ArgumentNullException("renderContext");

            m_renderQueue = new RenderQueue();
            m_renderStages = new RenderStageCollection();
            m_renderContext = renderContext;
        }

        /// <summary>
        /// Process a renderable to be rendered.
        /// </summary>
        /// <param name="renderable">Renderable to process.</param>
        /// <returns>True if the renderable was processed successfully, false otherwise.</returns>
        public virtual bool Process(IRenderable renderable)
        {
            if(renderable == null || !renderable.IsValidForDraw)
                return false;

            return RenderQueue.Enqueue(renderable);
        }

        /// <summary>
        /// Executes all render stages in the renderer. The render queue is sorted and cleared.
        /// </summary>
        public void Render()
        {
            Render(true, true);
        }

        /// <summary>
        /// Executes all render stages in the renderer.
        /// </summary>
        /// <param name="sortBuckets">True if the render queue should be sorted before executing render stages, false if not.</param>
        /// <param name="clearBuckets">True if the render queue should be cleared after executing render stages, false if not.</param>
        public virtual void Render(bool sortBuckets, bool clearBuckets)
        {
            if(sortBuckets)
                m_renderQueue.SortBuckets(m_renderContext.Camera);

            for(int i = 0; i < m_renderStages.Count; i++)
                m_renderStages[i].Execute(m_renderContext, m_renderQueue);

            if(clearBuckets)
                m_renderQueue.ClearBuckets();
        }
    }
}
