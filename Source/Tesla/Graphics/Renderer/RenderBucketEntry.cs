﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

namespace Tesla.Graphics
{
    /// <summary>
    /// An entry inside a render bucket. Each entry is a renderable with an (optional) material that will be used
    /// to render the object.
    /// </summary>
    public struct RenderBucketEntry
    {
        /// <summary>
        /// Material used for rendering the object.
        /// </summary>
        public Material Material;

        /// <summary>
        /// Object to be rendered.
        /// </summary>
        public IRenderable Renderable;

        /// <summary>
        /// Constructs a new instance of the <see cref="RenderBucketEntry"/> struct.
        /// </summary>
        /// <param name="material">The (optional) material used for rendering.</param>
        /// <param name="renderable">The object to be rendered.</param>
        public RenderBucketEntry(Material material, IRenderable renderable)
        {
            Material = material;
            Renderable = renderable;
        }
    }
}
