﻿﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

namespace Tesla.Graphics
{
    /// <summary>
    /// Defines logic for rendering transparent objects.
    /// </summary>
    public sealed class TransparentRenderStage : IRenderStage
    {
        private RenderBucketID m_transparentBucketID;
        private RasterizerState m_pass1RS;
        private DepthStencilState m_pass1DSS;
        private RasterizerState m_pass2RS;

        private RasterizerState m_defaultPass1RS;

        /// <summary>
        /// Gets or sets the transparent render bucket ID. By default this is <see cref="RenderBucketID.Transparent"/>.
        /// </summary>
        public RenderBucketID TransparentBucketID
        {
            get
            {
                return m_transparentBucketID;
            }
            set
            {
                m_transparentBucketID = value;
            }
        }

        /// <summary>
        /// Gets or sets the rasterizer state for rendering back-faces of geometry. Default is a state set to <see cref="CullMode.Front"/>
        /// and <see cref="VertexWinding.CounterClockwise"/>. Setting this to null sets the default state.
        /// </summary>
        public RasterizerState BackPassRasterizerState
        {
            get
            {
                return m_pass1RS;
            }
            set
            {
                m_pass1RS = value;
                if(value == null)
                    m_pass1RS = m_defaultPass1RS;
            }
        }

        /// <summary>
        /// Gets or sets the depth stencil state for rendering back-faces of geometry. Default is a state set to <see cref="DepthStencilState.DepthWriteOff"/>.
        /// Setting this to null sets the default state.
        /// </summary>
        public DepthStencilState BackPassDepthStencilState
        {
            get
            {
                return m_pass1DSS;
            }
            set
            {
                m_pass1DSS = value;
                if(value == null)
                    m_pass1DSS = DepthStencilState.DepthWriteOff;
            }
        }

        /// <summary>
        /// Gets or sets the rasterizer state for rendering the front-faces of geometry. Default is a state set to <see cref="RasterizerState.CullBackClockwiseFront"/>.
        /// Setting this to null sets the default state.
        /// </summary>
        public RasterizerState FrontPassRasterizerState
        {
            get
            {
                return m_pass2RS;
            }
            set
            {
                m_pass2RS = value;
                if(value == null)
                    m_pass2RS = RasterizerState.CullBackClockwiseFront;
            }
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="TransparentRenderStage"/> class.
        /// </summary>
        /// <param name="renderSystem">The render system.</param>
        public TransparentRenderStage(IRenderSystem renderSystem) : this(renderSystem, RenderBucketID.Transparent) { }


        /// <summary>
        /// Constructs a new instance of the <see cref="TransparentRenderStage"/> class.
        /// </summary>
        /// <param name="renderSystem">The render system.</param>
        /// <param name="transparentBucketID">The transparent bucket identifier.</param>
        public TransparentRenderStage(IRenderSystem renderSystem, RenderBucketID transparentBucketID)
        {
            m_transparentBucketID = transparentBucketID;

            InitRenderStates(renderSystem);
        }

        /// <summary>
        /// Executes the draw logic of the stage.
        /// </summary>
        /// <param name="queue">Render queue of objects that are to be drawn by the renderer.</param>
        /// <param name="renderContext">Render context of the renderer.</param>
        public void Execute(IRenderContext renderContext, RenderQueue queue)
        {
            RenderBucket bucket = queue[m_transparentBucketID];
            if(bucket == null)
                return;

            //See if we have an enforced RS state already that is wireframe or no culling, since mostly likely these are meant to be enforced
            //for debug purposes. If so, then we do single-pass rendering and don't enforce any other render states.
            bool isDebugRendering = IsDebugRendering(renderContext);
            RasterizerState oldEnRs = null;
            DepthStencilState oldEnDs = null;

            if(!isDebugRendering)
                GetPreviousEnforcedRenderState(renderContext, out oldEnRs, out oldEnDs);

            for(int i = 0; i < bucket.Count; i++)
            {
                RenderBucketEntry RenderBucketEntry = bucket[i];
                IRenderable renderable = RenderBucketEntry.Renderable;
                Material material = RenderBucketEntry.Material;

                TransparencyMode transMode = material.TransparencyMode;

                if(material != null)
                {
                    if(material.Passes.Count != 1)
                        transMode = TransparencyMode.OneSided;

                    if(transMode == TransparencyMode.OneSided || isDebugRendering)
                    {
                        DrawRenderable(renderContext, renderable, material);
                        continue;
                    }

                    //Do two pass rendering

                    //Enforce first pass states (back rendering)
                    renderContext.RasterizerState = m_pass1RS;
                    renderContext.DepthStencilState = m_pass1DSS;
                    renderContext.EnforcedRenderState |= EnforcedRenderState.DepthStencilState | EnforcedRenderState.RasterizerState;

                    DrawRenderable(renderContext, renderable, material);

                    //Enforce second pass states (front rendering)
                    renderContext.EnforcedRenderState &= ~EnforcedRenderState.DepthStencilState;
                    renderContext.RasterizerState = m_pass2RS;

                    DrawRenderable(renderContext, renderable, material);

                    renderContext.EnforcedRenderState &= ~EnforcedRenderState.RasterizerState;
                }
            }

            if(!isDebugRendering)
                HonorPreviousEnforcedRenderState(renderContext, oldEnRs, oldEnDs);
        }

        private void DrawRenderable(IRenderContext renderContext, IRenderable renderable, Material material)
        {
            material.ApplyMaterial(renderContext, renderable.RenderProperties);

            MaterialPassCollection passes = material.Passes;
            for(int passIndex = 0; passIndex < passes.Count; passIndex++)
            {
                MaterialPass pass = passes[passIndex];
                pass.Apply(renderContext);
                renderable.SetupDrawCall(renderContext, m_transparentBucketID, pass);
            }
        }

        private bool IsDebugRendering(IRenderContext renderContext)
        {
            if((renderContext.EnforcedRenderState & EnforcedRenderState.RasterizerState) == EnforcedRenderState.RasterizerState)
            {
                RasterizerState oldEnRS = renderContext.RasterizerState;
                if(oldEnRS.Cull == CullMode.None || oldEnRS.Fill == FillMode.WireFrame)
                    return true;
            }

            return false;
        }

        private void GetPreviousEnforcedRenderState(IRenderContext renderContext, out RasterizerState rsState, out DepthStencilState dsState)
        {
            rsState = null;
            dsState = null;

            if((renderContext.EnforcedRenderState & EnforcedRenderState.RasterizerState) == EnforcedRenderState.RasterizerState)
            {
                rsState = renderContext.RasterizerState;
                renderContext.EnforcedRenderState &= ~EnforcedRenderState.RasterizerState;
            }

            if((renderContext.EnforcedRenderState & EnforcedRenderState.DepthStencilState) == EnforcedRenderState.DepthStencilState)
            {
                dsState = renderContext.DepthStencilState;
                renderContext.EnforcedRenderState &= ~EnforcedRenderState.DepthStencilState;
            }
        }

        private void HonorPreviousEnforcedRenderState(IRenderContext renderContext, RasterizerState rsState, DepthStencilState dsState)
        {
            if(rsState != null)
            {
                renderContext.RasterizerState = rsState;
                renderContext.EnforcedRenderState |= EnforcedRenderState.RasterizerState;
            }

            if(dsState != null)
            {
                renderContext.DepthStencilState = dsState;
                renderContext.EnforcedRenderState |= EnforcedRenderState.DepthStencilState;
            }
        }

        private void InitRenderStates(IRenderSystem renderSystem)
        {
            m_pass1RS = m_defaultPass1RS = new RasterizerState(renderSystem);
            m_pass1RS.Cull = CullMode.Front;
            m_pass1RS.VertexWinding = VertexWinding.CounterClockwise;
            m_pass1RS.BindRenderState();

            m_pass1DSS = DepthStencilState.DepthWriteOff;

            m_pass2RS = RasterizerState.CullBackClockwiseFront;
        }
    }
}