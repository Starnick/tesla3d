﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;

namespace Tesla.Graphics
{
    /// <summary>
    /// A "simple" renderable implementation providing the barebones of rendering an object. Subclass this to provide your custom
    /// drawing logic.
    /// </summary>
    public abstract class SimpleRenderable : IRenderable
    {
        private MaterialDefinition m_matDef;
        private Transform m_worldTransform;
        private RenderPropertyCollection m_renderProperties;

        /// <summary>
        /// Gets if the renderable is valid for drawing.
        /// </summary>
        public virtual bool IsValidForDraw
        {
            get
            {
                return (m_matDef != null) ? m_matDef.AreMaterialsValid() : false;
            }
        }

        /// <summary>
        /// Gets the material definition that contains the materials used to render the object.
        /// </summary>
        public MaterialDefinition MaterialDefinition
        {
            get
            {
                return m_matDef;
            }
            set
            {
                m_matDef = value;
            }
        }

        /// <summary>
        /// Gets the collection of render properties of the object.
        /// </summary>
        public RenderPropertyCollection RenderProperties
        {
            get
            {
                return m_renderProperties;
            }
            set
            {
                m_renderProperties = value;
            }
        }

        /// <summary>
        /// Gets the world transform of the renderable. At the bare minimum, this render property should be present in the render properties collection.
        /// </summary>
        public Transform WorldTransform
        {
            get
            {
                return m_worldTransform;
            }
            set
            {
                m_worldTransform = value;
            }
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="SimpleRenderable"/> class.
        /// </summary>
        public SimpleRenderable()
        {
            m_matDef = new MaterialDefinition();
            m_renderProperties = new RenderPropertyCollection();
            m_worldTransform = new Transform();
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="SimpleRenderable"/> class.
        /// </summary>
        /// <param name="matDef">The material definition.</param>
        /// <param name="renderProps">The render property collection.</param>
        /// <param name="worldTransform">The world transform.</param>
        public SimpleRenderable(MaterialDefinition matDef, RenderPropertyCollection renderProps, Transform worldTransform)
        {
            m_matDef = matDef;
            m_renderProperties = new RenderPropertyCollection();
            m_worldTransform = worldTransform;
        }

        /// <summary>
        /// Performs the necessary draw calls to render the object.
        /// </summary>
        /// <param name="renderContext">Render context.</param>
        /// <param name="currentBucketID">The current bucket being drawn, may be invalid.</param>
        /// <param name="currentPass">The current pass that is drawing the renderable, may be null.</param>
        public abstract void SetupDrawCall(IRenderContext renderContext, RenderBucketID currentBucketID, MaterialPass currentPass);
    }

    /// <summary>
    /// A "simple" renderable that takes in an <see cref="Action{T1, T2, T3}"/> to allow for the draw call to be defined as a delegate.
    /// </summary>
    public sealed class DelegateRenderable : SimpleRenderable
    {
        private Action<IRenderContext, RenderBucketID, MaterialPass> m_setupDrawCall;
        private Func<bool> m_isValidForDraw;

        /// <summary>
        /// Gets if the renderable is valid for drawing.
        /// </summary>
        public override bool IsValidForDraw
        {
            get
            {
                if (m_isValidForDraw != null)
                    return m_isValidForDraw();

                return base.IsValidForDraw;
            }
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="DelegateRenderable"/> class.
        /// </summary>
        /// <param name="drawFunc">The draw function.</param>
        public DelegateRenderable(Action<IRenderContext, RenderBucketID, MaterialPass> drawFunc)
        {
            m_setupDrawCall = drawFunc;
            m_isValidForDraw = null;
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="DelegateRenderable"/> class.
        /// </summary>
        /// <param name="drawFunc">The draw function.</param>
        /// <param name="isValidForDrawFunc">Optional function for getting if the renderable is valid for draw, if not specified then
        /// validity will be pased on the contents of the material definition.</param>
        public DelegateRenderable(Action<IRenderContext, RenderBucketID, MaterialPass> drawFunc, Func<bool> isValidForDrawFunc)
        {
            m_setupDrawCall = drawFunc;
            m_isValidForDraw = isValidForDrawFunc;
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="DelegateRenderable"/> class.
        /// </summary>
        /// <param name="drawFunc">The draw function.</param>
        /// <param name="matDef">The material definition.</param>
        /// <param name="renderProps">The render property collection.</param>
        /// <param name="worldTransform">The world transform.</param>
        public DelegateRenderable(Action<IRenderContext, RenderBucketID, MaterialPass> drawFunc, MaterialDefinition matDef, RenderPropertyCollection renderProps, Transform worldTransform)
            : base(matDef, renderProps, worldTransform)
        {
            m_setupDrawCall = drawFunc;
            m_isValidForDraw = null;
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="DelegateRenderable"/> class.
        /// </summary>
        /// <param name="drawFunc">The draw function.</param>
        /// <param name="matDef">The material definition.</param>
        /// <param name="renderProps">The render property collection.</param>
        /// <param name="worldTransform">The world transform.</param>
        /// <param name="isValidForDrawFunc">Optional function for getting if the renderable is valid for draw, if not specified then
        /// validity will be pased on the contents of the material definition.</param>
        public DelegateRenderable(Action<IRenderContext, RenderBucketID, MaterialPass> drawFunc, MaterialDefinition matDef, RenderPropertyCollection renderProps, Transform worldTransform, Func<bool> isValidForDrawFunc)
            : base(matDef, renderProps, worldTransform)
        {
            m_setupDrawCall = drawFunc;
            m_isValidForDraw = isValidForDrawFunc;
        }

        /// <summary>
        /// Setups the draw call.
        /// </summary>
        /// <param name="renderContext">The render context.</param>
        /// <param name="currentBucketID">The current bucket identifier.</param>
        /// <param name="currentPass">The current pass.</param>
        public override void SetupDrawCall(IRenderContext renderContext, RenderBucketID currentBucketID, MaterialPass currentPass)
        {
            m_setupDrawCall(renderContext, currentBucketID, currentPass);
        }
    }
}
