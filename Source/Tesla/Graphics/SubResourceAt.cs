﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;

namespace Tesla.Graphics
{
  /// <summary>
  /// Specifies how to index a texture subresource. A texture may be one or more array slices, of which
  /// each may have one or more mipmap levels.
  /// </summary>
  public readonly struct SubResourceAt
  {
    /// <summary>
    /// Index of the main resource, the first mip level and the first array slice.
    /// </summary>
    public static readonly SubResourceAt Zero = new SubResourceAt(0, 0);

    /// <summary>
    /// The zero-based mip level of the subresource. 
    /// </summary>
    public readonly int MipLevel;

    /// <summary>
    /// The zero-based array slice of the subresource.
    /// </summary>
    public readonly int ArraySlice;

    /// <summary>
    /// Gets the corresponding face of a cubemap resource.
    /// </summary>
    public readonly CubeMapFace CubeMapFace { get { return (CubeMapFace) (ArraySlice % 6); } }

    /// <summary>
    /// Gets the logical array index of the subresource if it's a cubemap array.
    /// </summary>
    /// <remarks>
    /// Cubemaps are essentially 2D texture arrays with 6 slices, an array of them will have array slice
    /// counts that are in multiples of 6, so the first 6 array slices are cubemap 0, the next 6 are cubemap 1, etc.
    /// </remarks>
    public readonly int CubeMapArrayIndex { get { return (int) Math.Floor(ArraySlice / 6f); } }

    /// <summary>
    /// Constructs a new instance of the <see cref="SubResourceAt"/> struct.
    /// </summary>
    /// <param name="mipLevel">Zero-based mipmap level/</param>
    /// <param name="arraySlice">Zero-based array slice. By default, 0 is the first slice.</param>
    public SubResourceAt(int mipLevel, int arraySlice = 0)
    {
      MipLevel = mipLevel;
      ArraySlice = arraySlice;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="SubResourceAt"/> struct.
    /// </summary>
    /// <param name="mipLevel">Zero-based mipmap level</param>
    /// <param name="face">Corresponding face of the cubemap resource. By default, the +X face is the first face.</param>
    /// <param name="cubeMapArrayIndex">Optional logical cubemap array index. By default zero.</param>
    public SubResourceAt(int mipLevel, CubeMapFace face = CubeMapFace.PositiveX, int cubeMapArrayIndex = 0)
    {
      MipLevel = mipLevel;
      ArraySlice = (cubeMapArrayIndex * 6) + (int) face;
    }

    /// <summary>
    /// Validates if the indices are valid for a given resource dimensions.
    /// </summary>
    /// <param name="numMipMaps">Number of mipmap levels in the resource.</param>
    /// <param name="arrayCount">Resource array slice count. Non-array resources will still have a count of 1.</param>
    /// <param name="isCubeMap">Optionally identifies if the resource is a cubemap. If so, the arrayCount is multiplified by 6.</param>
    /// <returns>True if the indices are valid, false if not.</returns>
    public readonly bool Validate(int numMipMaps, int arrayCount = 1, bool isCubeMap = false)
    {
      if (isCubeMap)
        arrayCount *= 6;

      if (MipLevel < 0 || MipLevel >= numMipMaps)
        return false;

      if (ArraySlice < 0 || ArraySlice >= arrayCount)
        return false;

      return true;
    }

    /// <summary>
    /// Calculates the uncompressed width of the mip map level, given the mip level index and the width of the texture's
    /// first mip level.
    /// </summary>
    /// <param name="width">Width of the texture at the first mip level. This will hold the resulting width at the specified mip level.</param>
    public readonly void CalculateMipLevelDimensions(ref int width)
    {
      Texture.CalculateMipLevelDimensions(MipLevel, ref width);
    }

    /// <summary>
    /// Calculates the uncompressed width/height of the mip map level, given the mip level index and the width/height of the texture's
    /// first mip level.
    /// </summary>
    /// <param name="width">Width of the texture at the first mip level. This will hold the resulting width at the specified mip level.</param>
    /// <param name="height">Height of the texture at the first mip level. This will hold the resulting height at the specified mip level.</param>
    public readonly void CalculateMipLevelDimensions(ref int width, ref int height)
    {
      Texture.CalculateMipLevelDimensions(MipLevel, ref width, ref height);
    }

    /// <summary>
    /// Calculates the uncompressed width/height/depth of the mip map level, given the mip level index and the width/height/depth of the texture's
    /// first mip level.
    /// </summary>
    /// <param name="width">Width of the texture at the first mip level. This will hold the resulting width at the specified mip level.</param>
    /// <param name="height">Height of the texture at the first mip level. This will hold the resulting height at the specified mip level.</param>
    /// <param name="depth">Depth of the texture at the first mip level. This will hold the resulting depth at the specified mip level.</param>
    public readonly void CalculateMipLevelDimensions(ref int width, ref int height, ref int depth)
    {
      Texture.CalculateMipLevelDimensions(MipLevel, ref width, ref height, ref depth);
    }

    /// <summary>
    /// Calculates the size of the specified mip level in bytes.
    /// </summary>
    /// <param name="width">Width of the texture at the first mip level.</param>
    /// <param name="format">Format of the texture. 1D textures cannot be block compressed, so those are invalid formats.</param>
    /// <returns>The size of the mip level in bytes, if the calculation was not valid, zero is returned</returns>
    public readonly int CalculateMipLevelSizeInBytes(int width, SurfaceFormat format)
    {
      return Texture.CalculateMipLevelSizeInBytes(MipLevel, width, format);
    }

    /// <summary>
    /// Calculates the size of the specified mip level in bytes.
    /// </summary>
    /// <param name="width">Width of the texture at the first mip level. </param>
    /// <param name="height">Height of the texture at the first mip level. </param>
    /// <param name="format">Format of the texture.</param>
    /// <returns>The size of the mip level in bytes, if the calculation was not valid, zero is returned</returns>
    public readonly int CalculateMipLevelSizeInBytes(int width, int height, SurfaceFormat format)
    {
      return Texture.CalculateMipLevelSizeInBytes(MipLevel, width, height, format);
    }

    /// <summary>
    /// Calculates the size of the specified mip level in bytes.
    /// </summary>
    /// <param name="width">Width of the texture at the first mip level.</param>
    /// <param name="height">Height of the texture at the first mip level.</param>
    /// <param name="depth">Depth of the texture at the first mip level.</param>
    /// <param name="format">Format of the texture.</param>
    /// <returns>The size of the mip level in bytes, if the calculation was not valid, zero is returned</returns>
    public readonly int CalculateMipLevelSizeInBytes(int width, int height, int depth, SurfaceFormat format)
    {
      return Texture.CalculateMipLevelSizeInBytes(MipLevel, width, height, depth, format);
    }

    /// <summary>
    /// Calculates the sub resource index of a resource at the specified array and mip index, given the number of mip levels
    /// the resource contains.
    /// </summary>
    /// <param name="numMipLevels">Number of mip levels.</param>
    /// <returns>The calculated subresource index.</returns>
    public readonly int CalculateSubResourceIndex(int numMipLevels)
    {
      return Texture.CalculateSubResourceIndex(ArraySlice, MipLevel, numMipLevels);
    }

    /// <summary>
    /// Constructs a <see cref="SubResourceAt"/> at the specified mip level for the first array slice.
    /// </summary>
    /// <param name="mipLevel">Zero-based mip level.</param>
    /// <returns>SubResource indices</returns>
    public static SubResourceAt Mip(int mipLevel)
    {
      return new SubResourceAt(mipLevel, 0);
    }

    /// <summary>
    /// Constructs a <see cref="SubResourceAt"/> at the first mip level for the specified array slice.
    /// </summary>
    /// <param name="arraySlice">Zero-based array slice.</param>
    /// <returns>SubResource indices</returns>
    public static SubResourceAt Slice(int arraySlice)
    {
      return new SubResourceAt(0, arraySlice);
    }

    /// <summary>
    /// Constructs a <see cref="SubResourceAt"/> at the firt mip level for the specified cubemap face.
    /// </summary>
    /// <param name="face">Cubemap face.</param>
    /// <returns>SubResource indices</returns>
    public static SubResourceAt Face(CubeMapFace face)
    {
      return new SubResourceAt(0, face);
    }

    /// <summary>
    /// Constructs a <see cref="SubResourceAt"/> at the specified mip level and array slice.
    /// </summary>
    /// <param name="mipLevel">Zero-based mip level.</param>
    /// <param name="arraySlice">Zero-based array slice.</param>
    /// <returns>SubResource indices</returns>
    public static SubResourceAt MipSlice(int mipLevel = 0, int arraySlice = 0)
    {
      return new SubResourceAt(mipLevel, arraySlice);
    }

    /// <summary>
    /// Constructs a <see cref="SubResourceAt"/> at the specified mip level and cubemap face.
    /// </summary>
    /// <param name="mipLevel">Zero-based mip level.</param>
    /// <param name="face">Corresponding face of the cubemap resource. By default, the +X face is the first face.</param>
    /// <param name="cubeMapArrayIndex">Optional logical cubemap array index. By default zero.</param>
    /// <returns>SubResource indices</returns>
    public static SubResourceAt MipFace(int mipLevel = 0, CubeMapFace face = CubeMapFace.PositiveX, int cubeMapArrayIndex = 0)
    {
      return new SubResourceAt(mipLevel, face, cubeMapArrayIndex);
    }
  }
}
