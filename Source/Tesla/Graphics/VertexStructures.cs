﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Tesla.Content;

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// Represents a vertex that only has position data.
  /// </summary>
  [Serializable, StructLayout(LayoutKind.Sequential)]
  public struct VertexPosition : IEquatable<VertexPosition>, IRefEquatable<VertexPosition>, IVertexType, IPrimitiveValue
  {
    /// <summary>
    /// Vertex Position
    /// </summary>
    public Vector3 Position;

    /// <summary>
    /// Companion vertex layout to use when dealing with this vertex type.
    /// </summary>
    public static readonly VertexLayout VertexLayout;

    /// <summary>
    /// Size of the structure in bytes.
    /// </summary>
    public static readonly int SizeInBytes;

    static VertexPosition()
    {
      SizeInBytes = BufferHelper.SizeOf<VertexPosition>();
      VertexLayout = new VertexLayout(new VertexElement[] {
                new VertexElement(VertexSemantic.Position, 0, VertexFormat.Float3, 0)
            });
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="VertexPosition"/> struct.
    /// </summary>
    /// <param name="position">The vertex position.</param>
    public VertexPosition(in Vector3 position)
    {
      Position = position;
    }

    /// <summary>
    /// Tests equality between two vertices.
    /// </summary>
    /// <param name="a">First vertex</param>
    /// <param name="b">Second vertex</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    public static bool operator ==(in VertexPosition a, in VertexPosition b)
    {
      return a.Position.Equals(b.Position);
    }

    /// <summary>
    /// Tests inequality between two vertices.
    /// </summary>
    /// <param name="a">First vertex</param>
    /// <param name="b">Second vertex</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    public static bool operator !=(in VertexPosition a, in VertexPosition b)
    {
      return !a.Position.Equals(b.Position);
    }

    /// <summary>
    /// Indicates whether this instance and a specified object are equal.
    /// </summary>
    /// <param name="obj">Another object to compare to.</param>
    /// <returns>True if <paramref name="obj" /> and this instance are the same type and represent the same value; otherwise, false.</returns>
    public readonly override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is VertexPositionTexture)
      {
        VertexPosition other = (VertexPosition) obj;
        return Position.Equals(other.Position);
      }

      return false;
    }

    /// <summary>
    /// Tests equality between two vertices.
    /// </summary>
    /// <param name="other">Other vertex to compare to.</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    readonly bool IEquatable<VertexPosition>.Equals(VertexPosition other)
    {
      return Position.Equals(other.Position);
    }

    /// <summary>
    /// Tests equality between two vertices.
    /// </summary>
    /// <param name="other">Other vertex to compare to.</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    public readonly bool Equals(in VertexPosition other)
    {
      return Position.Equals(other.Position);
    }

    /// <summary>
    /// Tests equality between two vertices within tolerance.
    /// </summary>
    /// <param name="other">Other vertex to compare to.</param>
    /// <param name="tolerance">Tolerance</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    public readonly bool Equals(in VertexPosition other, float tolerance)
    {
      return Position.Equals(other.Position, tolerance);
    }

    /// <summary>
    /// Gets the layout of the vertex.
    /// </summary>
    /// <returns>Vertex layout</returns>
    public readonly VertexLayout GetVertexLayout()
    {
      return VertexLayout;
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
    public readonly override int GetHashCode()
    {
      unchecked
      {
        return Position.GetHashCode();
      }
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly override string ToString()
    {
      return String.Format(CultureInfo.CurrentCulture, "Position:{0}", new object[] { Position.ToString() });
    }

    /// <summary>
    /// Writes the primitive data to the output.
    /// </summary>
    /// <param name="output">Primitive writer.</param>
    public readonly void Write(IPrimitiveWriter output)
    {
      output.Write<Vector3>("Position", Position);
    }

    /// <summary>
    /// Reads the primitive data from the input.
    /// </summary>
    /// <param name="input">Primitive reader.</param>
    public void Read(IPrimitiveReader input)
    {
      input.Read<Vector3>("Position", out Position);
    }
  }

  /// <summary>
  /// Represents a vertex that has position and texture coordinate data.
  /// </summary>
  [Serializable, StructLayout(LayoutKind.Sequential)]
  public struct VertexPositionTexture : IEquatable<VertexPositionTexture>, IRefEquatable<VertexPositionTexture>, IVertexType, IPrimitiveValue
  {
    /// <summary>
    /// Vertex Position
    /// </summary>
    public Vector3 Position;

    /// <summary>
    /// Vertex Texture Coordinate
    /// </summary>
    public Vector2 TextureCoordinate;

    /// <summary>
    /// Companion vertex layout to use when dealing with this vertex type.
    /// </summary>
    public static readonly VertexLayout VertexLayout;

    /// <summary>
    /// Size of the structure in bytes.
    /// </summary>
    public static readonly int SizeInBytes;

    static VertexPositionTexture()
    {
      SizeInBytes = BufferHelper.SizeOf<VertexPositionTexture>();
      VertexLayout = new VertexLayout(new VertexElement[] {
                new VertexElement(VertexSemantic.Position, 0, VertexFormat.Float3, 0),
                new VertexElement(VertexSemantic.TextureCoordinate, 0, VertexFormat.Float2, 12)
            });
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="VertexPositionTexture"/> struct.
    /// </summary>
    /// <param name="position">The vertex position.</param>
    /// <param name="textureCoordinate">The vertex texture coordinate.</param>
    public VertexPositionTexture(in Vector3 position, in Vector2 textureCoordinate)
    {
      Position = position;
      TextureCoordinate = textureCoordinate;
    }

    /// <summary>
    /// Tests equality between two vertices.
    /// </summary>
    /// <param name="a">First vertex</param>
    /// <param name="b">Second vertex</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    public static bool operator ==(in VertexPositionTexture a, in VertexPositionTexture b)
    {
      return a.Position.Equals(b.Position) && a.TextureCoordinate.Equals(b.TextureCoordinate);
    }

    /// <summary>
    /// Tests inequality between two vertices.
    /// </summary>
    /// <param name="a">First vertex</param>
    /// <param name="b">Second vertex</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    public static bool operator !=(in VertexPositionTexture a, in VertexPositionTexture b)
    {
      return !a.Position.Equals(b.Position) || !a.TextureCoordinate.Equals(b.TextureCoordinate);
    }

    /// <summary>
    /// Indicates whether this instance and a specified object are equal.
    /// </summary>
    /// <param name="obj">Another object to compare to.</param>
    /// <returns>True if <paramref name="obj" /> and this instance are the same type and represent the same value; otherwise, false.</returns>
    public readonly override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is VertexPositionTexture)
      {
        VertexPositionTexture other = (VertexPositionTexture) obj;
        return Position.Equals(other.Position) && TextureCoordinate.Equals(other.TextureCoordinate);
      }

      return false;
    }

    /// <summary>
    /// Tests equality between two vertices.
    /// </summary>
    /// <param name="other">Other vertex to compare to.</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    readonly bool IEquatable<VertexPositionTexture>.Equals(VertexPositionTexture other)
    {
      return Position.Equals(other.Position) && TextureCoordinate.Equals(other.TextureCoordinate);
    }

    /// <summary>
    /// Tests equality between two vertices.
    /// </summary>
    /// <param name="other">Other vertex to compare to.</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    public readonly bool Equals(in VertexPositionTexture other)
    {
      return Position.Equals(other.Position) && TextureCoordinate.Equals(other.TextureCoordinate);
    }

    /// <summary>
    /// Tests equality between two vertices within tolerance.
    /// </summary>
    /// <param name="other">Other vertex to compare to.</param>
    /// <param name="tolerance">Tolerance</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    public readonly bool Equals(in VertexPositionTexture other, float tolerance)
    {
      return Position.Equals(other.Position, tolerance) && TextureCoordinate.Equals(other.TextureCoordinate, tolerance);
    }

    /// <summary>
    /// Gets the layout of the vertex.
    /// </summary>
    /// <returns>Vertex layout</returns>
    public readonly VertexLayout GetVertexLayout()
    {
      return VertexLayout;
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
    public readonly override int GetHashCode()
    {
      unchecked
      {
        return Position.GetHashCode() + TextureCoordinate.GetHashCode();
      }
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly override string ToString()
    {
      return String.Format(CultureInfo.CurrentCulture, "Position:{0}, TextureCoordinate:{1}", new object[] { Position.ToString(), TextureCoordinate.ToString() });
    }

    /// <summary>
    /// Writes the primitive data to the output.
    /// </summary>
    /// <param name="output">Primitive writer.</param>
    public readonly void Write(IPrimitiveWriter output)
    {
      output.Write<Vector3>("Position", Position);
      output.Write<Vector2>("TextureCoordinate", TextureCoordinate);
    }

    /// <summary>
    /// Reads the primitive data from the input.
    /// </summary>
    /// <param name="input">Primitive reader.</param>
    public void Read(IPrimitiveReader input)
    {
      input.Read<Vector3>("Position", out Position);
      input.Read<Vector2>("TextureCoordinate", out TextureCoordinate);
    }
  }

  /// <summary>
  /// Represents a vertex with position and color data.
  /// </summary>
  [Serializable, StructLayout(LayoutKind.Sequential)]
  public struct VertexPositionColor : IEquatable<VertexPositionColor>, IRefEquatable<VertexPositionColor>, IVertexType, IPrimitiveValue
  {
    /// <summary>
    /// Vertex Position
    /// </summary>
    public Vector3 Position;

    /// <summary>
    /// Vertex color.
    /// </summary>
    public Color VertexColor;

    /// <summary>
    /// Companion vertex layout to use when dealing with this vertex type.
    /// </summary>
    public static readonly VertexLayout VertexLayout;

    /// <summary>
    /// Size of the structure in bytes.
    /// </summary>
    public static readonly int SizeInBytes;

    static VertexPositionColor()
    {
      SizeInBytes = BufferHelper.SizeOf<VertexPositionColor>();
      VertexLayout = new VertexLayout(new VertexElement[] {
                new VertexElement(VertexSemantic.Position, 0, VertexFormat.Float3, 0),
                new VertexElement(VertexSemantic.Color, 0, VertexFormat.Color, 12),
            });
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="VertexPositionColor"/> struct.
    /// </summary>
    /// <param name="position">The vertex position.</param>
    /// <param name="vertexColor">The vertex color</param>
    public VertexPositionColor(in Vector3 position, Color vertexColor)
    {
      Position = position;
      VertexColor = vertexColor;
    }

    /// <summary>
    /// Tests equality between two vertices.
    /// </summary>
    /// <param name="a">First vertex</param>
    /// <param name="b">Second vertex</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    public static bool operator ==(in VertexPositionColor a, in VertexPositionColor b)
    {
      return a.Position.Equals(b.Position) && a.VertexColor.Equals(b.VertexColor);
    }

    /// <summary>
    /// Tests inequality between two vertices.
    /// </summary>
    /// <param name="a">First vertex</param>
    /// <param name="b">Second vertex</param>
    /// <returns>True if both are not equal, false otherwise.</returns>
    public static bool operator !=(in VertexPositionColor a, in VertexPositionColor b)
    {
      return !a.Position.Equals(b.Position) || !a.VertexColor.Equals(b.VertexColor);
    }

    /// <summary>
    /// Determines whether the specified <see cref="System.Object" /> is equal to this instance.
    /// </summary>
    /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
    /// <returns>True if the specified <see cref="System.Object" /> is equal to this instance; otherwise, false.</returns>
    public readonly override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is VertexPositionColor)
      {
        VertexPositionColor other = (VertexPositionColor) obj;
        return Position.Equals(other.Position) && VertexColor.Equals(other.VertexColor);
      }

      return false;
    }

    /// <summary>
    /// Tests equality between two vertices.
    /// </summary>
    /// <param name="other">Other vertex to compare to.</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    readonly bool IEquatable<VertexPositionColor>.Equals(VertexPositionColor other)
    {
      return Position.Equals(other.Position) && VertexColor.Equals(other.VertexColor);
    }

    /// <summary>
    /// Tests equality between two vertices.
    /// </summary>
    /// <param name="other">Other vertex to compare to.</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    public readonly bool Equals(in VertexPositionColor other)
    {
      return Position.Equals(other.Position) && VertexColor.Equals(other.VertexColor);
    }

    /// <summary>
    /// Tests equality between two vertices within tolerance.
    /// </summary>
    /// <param name="other">Other vertex to compare to.</param>
    /// <param name="tolerance">Tolerance</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    public readonly bool Equals(in VertexPositionColor other, float tolerance)
    {
      return Position.Equals(other.Position, tolerance) && VertexColor.Equals(other.VertexColor);
    }

    /// <summary>
    /// Gets the layout of the vertex.
    /// </summary>
    /// <returns>Vertex layout</returns>
    public readonly VertexLayout GetVertexLayout()
    {
      return VertexLayout;
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
    public readonly override int GetHashCode()
    {
      unchecked
      {
        return Position.GetHashCode() + VertexColor.GetHashCode();
      }
    }

    /// <summary>
    /// Returns the fully qualified type name of this instance.
    /// </summary>
    /// <returns>A <see cref="T:System.String" /> containing a fully qualified type name. </returns>
    public readonly override string ToString()
    {
      return String.Format(CultureInfo.CurrentCulture, "Position:{0}, VertexColor:{1}", new object[] { Position.ToString(), VertexColor.ToString() });
    }

    /// <summary>
    /// Writes the primitive data to the output.
    /// </summary>
    /// <param name="output">Primitive writer.</param>
    public readonly void Write(IPrimitiveWriter output)
    {
      output.Write<Vector3>("Position", Position);
      output.Write<Color>("VertexColor", VertexColor);
    }

    /// <summary>
    /// Reads the primitive data from the input.
    /// </summary>
    /// <param name="input">Primitive reader.</param>
    public void Read(IPrimitiveReader input)
    {
      input.Read<Vector3>("Position", out Position);
      input.Read<Color>("VertexColor", out VertexColor);
    }
  }

  /// <summary>
  /// Represents a vertex with position, vertex color, and texture coordinate data.
  /// </summary>
  [Serializable, StructLayout(LayoutKind.Sequential)]
  public struct VertexPositionColorTexture : IEquatable<VertexPositionColorTexture>, IRefEquatable<VertexPositionColorTexture>, IVertexType, IPrimitiveValue
  {
    /// <summary>
    /// Vertex Position
    /// </summary>
    public Vector3 Position;

    /// <summary>
    /// Vertex color.
    /// </summary>
    public Color VertexColor;

    /// <summary>
    /// Vertex Texture Coordinate
    /// </summary>
    public Vector2 TextureCoordinate;

    /// <summary>
    /// Companion vertex layout to use when dealing with this vertex type.
    /// </summary>
    public static readonly VertexLayout VertexLayout;

    /// <summary>
    /// Size of the structure in bytes.
    /// </summary>
    public static readonly int SizeInBytes;

    static VertexPositionColorTexture()
    {
      SizeInBytes = BufferHelper.SizeOf<VertexPositionColorTexture>();
      VertexLayout = new VertexLayout(new VertexElement[] {
                new VertexElement(VertexSemantic.Position, 0, VertexFormat.Float3, 0),
                new VertexElement(VertexSemantic.Color, 0, VertexFormat.Color, 12),
                new VertexElement(VertexSemantic.TextureCoordinate, 0, VertexFormat.Float2, 16)
            });
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="VertexPositionColorTexture"/> struct.
    /// </summary>
    /// <param name="position">The vertex position.</param>
    /// <param name="vertexColor">The vertex color.</param>
    /// <param name="textureCoordinate">The vertex texture coordinate.</param>
    public VertexPositionColorTexture(in Vector3 position, Color vertexColor, in Vector2 textureCoordinate)
    {
      Position = position;
      VertexColor = vertexColor;
      TextureCoordinate = textureCoordinate;
    }

    /// <summary>
    /// Tests equality between two vertices.
    /// </summary>
    /// <param name="a">First vertex</param>
    /// <param name="b">Second vertex</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    public static bool operator ==(in VertexPositionColorTexture a, in VertexPositionColorTexture b)
    {
      return a.Position.Equals(b.Position) && a.VertexColor.Equals(b.VertexColor) && a.TextureCoordinate.Equals(b.TextureCoordinate);
    }

    /// <summary>
    /// Tests inequality between two vertices.
    /// </summary>
    /// <param name="a">First vertex</param>
    /// <param name="b">Second vertex</param>
    /// <returns>True if both are not equal, false otherwise.</returns>
    public static bool operator !=(in VertexPositionColorTexture a, in VertexPositionColorTexture b)
    {
      return !a.Position.Equals(b.Position) || !a.VertexColor.Equals(b.VertexColor) || !a.TextureCoordinate.Equals(b.TextureCoordinate);
    }

    /// <summary>
    /// Indicates whether this instance and a specified object are equal.
    /// </summary>
    /// <param name="obj">Another object to compare to.</param>
    /// <returns>True if <paramref name="obj" /> and this instance are the same type and represent the same value; otherwise, false.</returns>
    public readonly override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is VertexPositionColorTexture)
      {
        VertexPositionColorTexture other = (VertexPositionColorTexture) obj;
        return Position.Equals(other.Position) && VertexColor.Equals(other.VertexColor) && TextureCoordinate.Equals(other.TextureCoordinate);
      }

      return false;
    }

    /// <summary>
    /// Tests equality between two vertices.
    /// </summary>
    /// <param name="other">Other vertex to compare to.</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    readonly bool IEquatable<VertexPositionColorTexture>.Equals(VertexPositionColorTexture other)
    {
      return Position.Equals(other.Position) && VertexColor.Equals(other.VertexColor) && TextureCoordinate.Equals(other.TextureCoordinate);
    }

    /// <summary>
    /// Tests equality between two vertices.
    /// </summary>
    /// <param name="other">Other vertex to compare to.</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    public readonly bool Equals(in VertexPositionColorTexture other)
    {
      return Position.Equals(other.Position) && VertexColor.Equals(other.VertexColor) && TextureCoordinate.Equals(other.TextureCoordinate);
    }

    /// <summary>
    /// Tests equality between two vertices within tolerance.
    /// </summary>
    /// <param name="other">Other vertex to compare to.</param>
    /// <param name="tolerance">Tolerance</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    public readonly bool Equals(in VertexPositionColorTexture other, float tolerance)
    {
      return Position.Equals(other.Position, tolerance) && VertexColor.Equals(other.VertexColor) && TextureCoordinate.Equals(other.TextureCoordinate, tolerance);
    }

    /// <summary>
    /// Gets the layout of the vertex.
    /// </summary>
    /// <returns>Vertex layout</returns>
    public readonly VertexLayout GetVertexLayout()
    {
      return VertexLayout;
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
    public readonly override int GetHashCode()
    {
      unchecked
      {
        return Position.GetHashCode() + VertexColor.GetHashCode() + TextureCoordinate.GetHashCode();
      }
    }

    /// <summary>
    /// Returns a <see cref="System.String"/> that represents this instance.
    /// </summary>
    /// <returns>
    /// A <see cref="System.String"/> that represents this instance.
    /// </returns>
    public readonly override string ToString()
    {
      return String.Format(CultureInfo.CurrentCulture, "Position:{0}, VertexColor:{1}, TextureCoordinate:{2}", new object[] { Position.ToString(), VertexColor.ToString(), TextureCoordinate.ToString() });
    }

    /// <summary>
    /// Writes the primitive data to the output.
    /// </summary>
    /// <param name="output">Primitive writer.</param>
    public readonly void Write(IPrimitiveWriter output)
    {
      output.Write<Vector3>("Position", Position);
      output.Write<Color>("VertexColor", VertexColor);
      output.Write<Vector2>("TextureCoordinate", TextureCoordinate);
    }

    /// <summary>
    /// Reads the primitive data from the input.
    /// </summary>
    /// <param name="input">Primitive reader.</param>
    public void Read(IPrimitiveReader input)
    {
      input.Read<Vector3>("Position", out Position);
      input.Read<Color>("VertexColor", out VertexColor);
      input.Read<Vector2>("TextureCoordinate", out TextureCoordinate);
    }
  }

  /// <summary>
  /// Represents a vertex with position, normal, and texture coordinate data.
  /// </summary>
  [Serializable, StructLayout(LayoutKind.Sequential)]
  public struct VertexPositionNormalTexture : IEquatable<VertexPositionNormalTexture>, IRefEquatable<VertexPositionNormalTexture>, IVertexType, IPrimitiveValue
  {
    /// <summary>
    /// Vertex Position
    /// </summary>
    public Vector3 Position;

    /// <summary>
    /// Vertex Normal.
    /// </summary>
    public Vector3 Normal;

    /// <summary>
    /// Vertex Texture Coordinate
    /// </summary>
    public Vector2 TextureCoordinate;

    /// <summary>
    /// Companion vertex layout to use when dealing with this vertex type.
    /// </summary>
    public static readonly VertexLayout VertexLayout;

    /// <summary>
    /// Size of the structure in bytes.
    /// </summary>
    public static readonly int SizeInBytes;

    static VertexPositionNormalTexture()
    {
      SizeInBytes = BufferHelper.SizeOf<VertexPositionNormalTexture>();
      VertexLayout = new VertexLayout(new VertexElement[] {
                new VertexElement(VertexSemantic.Position, 0, VertexFormat.Float3, 0),
                new VertexElement(VertexSemantic.Normal, 0, VertexFormat.Float3, 12),
                new VertexElement(VertexSemantic.TextureCoordinate, 0, VertexFormat.Float2, 24)
            });
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="VertexPositionNormalTexture"/> struct.
    /// </summary>
    /// <param name="position">The vertex position.</param>
    /// <param name="normal">The vertex normal.</param>
    /// <param name="textureCoordinate">The vertex texture coordinate.</param>
    public VertexPositionNormalTexture(in Vector3 position, in Vector3 normal, in Vector2 textureCoordinate)
    {
      Position = position;
      Normal = normal;
      TextureCoordinate = textureCoordinate;
    }

    /// <summary>
    /// Tests equality between two vertices.
    /// </summary>
    /// <param name="a">First vertex</param>
    /// <param name="b">Second vertex</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    public static bool operator ==(in VertexPositionNormalTexture a, in VertexPositionNormalTexture b)
    {
      return a.Position.Equals(b.Position) && a.Normal.Equals(b.Normal) && a.TextureCoordinate.Equals(b.TextureCoordinate);
    }

    /// <summary>
    /// Tests inequality between two vertices.
    /// </summary>
    /// <param name="a">First vertex</param>
    /// <param name="b">Second vertex</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    public static bool operator !=(in VertexPositionNormalTexture a, in VertexPositionNormalTexture b)
    {
      return !a.Position.Equals(b.Position) || !a.Normal.Equals(b.Normal) || !a.TextureCoordinate.Equals(b.TextureCoordinate);
    }

    /// <summary>
    /// Indicates whether this instance and a specified object are equal.
    /// </summary>
    /// <param name="obj">Another object to compare to.</param>
    /// <returns>True if <paramref name="obj" /> and this instance are the same type and represent the same value; otherwise, false.</returns>
    public readonly override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is VertexPositionNormalTexture)
      {
        VertexPositionNormalTexture other = (VertexPositionNormalTexture) obj;
        return Position.Equals(other.Position) && Normal.Equals(other.Normal) && TextureCoordinate.Equals(other.TextureCoordinate);
      }

      return false;
    }

    /// <summary>
    /// Tests equality between two vertices.
    /// </summary>
    /// <param name="other">Other vertex to compare to.</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    readonly bool IEquatable<VertexPositionNormalTexture>.Equals(VertexPositionNormalTexture other)
    {
      return Position.Equals(other.Position) && Normal.Equals(other.Normal) && TextureCoordinate.Equals(other.TextureCoordinate);
    }

    /// <summary>
    /// Tests equality between two vertices.
    /// </summary>
    /// <param name="other">Other vertex to compare to.</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    public readonly bool Equals(in VertexPositionNormalTexture other)
    {
      return Position.Equals(other.Position) && Normal.Equals(other.Normal) && TextureCoordinate.Equals(other.TextureCoordinate);
    }

    /// <summary>
    /// Tests equality between two vertices within tolerance.
    /// </summary>
    /// <param name="other">Other vertex to compare to.</param>
    /// <param name="tolerance">Tolerance</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    public readonly bool Equals(in VertexPositionNormalTexture other, float tolerance)
    {
      return Position.Equals(other.Position, tolerance) && Normal.Equals(other.Normal, tolerance) && TextureCoordinate.Equals(other.TextureCoordinate, tolerance);
    }

    /// <summary>
    /// Gets the layout of the vertex.
    /// </summary>
    /// <returns>Vertex layout</returns>
    public readonly VertexLayout GetVertexLayout()
    {
      return VertexLayout;
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
    public readonly override int GetHashCode()
    {
      unchecked
      {
        return Position.GetHashCode() + Normal.GetHashCode() + TextureCoordinate.GetHashCode();
      }
    }

    /// <summary>
    /// Returns the fully qualified type name of this instance.
    /// </summary>
    /// <returns>A <see cref="T:System.String" /> containing a fully qualified type name. </returns>
    public readonly override string ToString()
    {
      return String.Format(CultureInfo.CurrentCulture, "Position:{0}, Normal:{1}, TextureCoordinate:{2}", new object[] { Position.ToString(), Normal.ToString(), TextureCoordinate.ToString() });
    }

    /// <summary>
    /// Writes the primitive data to the output.
    /// </summary>
    /// <param name="output">Primitive writer.</param>
    public readonly void Write(IPrimitiveWriter output)
    {
      output.Write<Vector3>("Position", Position);
      output.Write<Vector3>("Normal", Normal);
      output.Write<Vector2>("TextureCoordinate", TextureCoordinate);
    }

    /// <summary>
    /// Reads the primitive data from the input.
    /// </summary>
    /// <param name="input">Primitive reader.</param>
    public void Read(IPrimitiveReader input)
    {
      input.Read<Vector3>("Position", out Position);
      input.Read<Vector3>("Normal", out Normal);
      input.Read<Vector2>("TextureCoordinate", out TextureCoordinate);
    }
  }

  /// <summary>
  /// Represents a vertex with position, normal, vertex color, and texture coordinate data.
  /// </summary>
  [Serializable, StructLayout(LayoutKind.Sequential)]
  public struct VertexPositionNormalColorTexture : IEquatable<VertexPositionNormalColorTexture>, IRefEquatable<VertexPositionNormalColorTexture>, IVertexType, IPrimitiveValue
  {
    /// <summary>
    /// Vertex Position
    /// </summary>
    public Vector3 Position;

    /// <summary>
    /// Vertex Normal.
    /// </summary>
    public Vector3 Normal;

    /// <summary>
    /// Vertex Color.
    /// </summary>
    public Color VertexColor;

    /// <summary>
    /// Vertex Texture Coordinate
    /// </summary>
    public Vector2 TextureCoordinate;

    /// <summary>
    /// Companion vertex layout to use when dealing with this vertex type.
    /// </summary>
    public static readonly VertexLayout VertexLayout;

    /// <summary>
    /// Size of the structure in bytes.
    /// </summary>
    public static readonly int SizeInBytes;

    static VertexPositionNormalColorTexture()
    {
      SizeInBytes = BufferHelper.SizeOf<VertexPositionNormalColorTexture>();
      VertexLayout = new VertexLayout(new VertexElement[] {
                new VertexElement(VertexSemantic.Position, 0, VertexFormat.Float3, 0),
                new VertexElement(VertexSemantic.Normal, 0, VertexFormat.Float3, 12),
                new VertexElement(VertexSemantic.Color, 0, VertexFormat.Color, 24),
                new VertexElement(VertexSemantic.TextureCoordinate, 0, VertexFormat.Float2, 28)
            });
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="VertexPositionNormalColorTexture"/> struct.
    /// </summary>
    /// <param name="position">The vertex position.</param>
    /// <param name="normal">The vertex normal.</param>
    /// <param name="vertexColor">The vertex color.</param>
    /// <param name="textureCoordinate">The vertex texture coordinate.</param>
    public VertexPositionNormalColorTexture(in Vector3 position, in Vector3 normal, Color vertexColor, in Vector2 textureCoordinate)
    {
      Position = position;
      Normal = normal;
      VertexColor = vertexColor;
      TextureCoordinate = textureCoordinate;
    }

    /// <summary>
    /// Tests equality between two vertices.
    /// </summary>
    /// <param name="a">First vertex</param>
    /// <param name="b">Second vertex</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    public static bool operator ==(in VertexPositionNormalColorTexture a, in VertexPositionNormalColorTexture b)
    {
      return a.Position.Equals(b.Position) && a.Normal.Equals(b.Normal) && a.VertexColor.Equals(b.VertexColor) && a.TextureCoordinate.Equals(b.TextureCoordinate);
    }

    /// <summary>
    /// Tests inequality between two vertices.
    /// </summary>
    /// <param name="a">First vertex</param>
    /// <param name="b">Second vertex</param>
    /// <returns>True if both are not equal, false otherwise.</returns>
    public static bool operator !=(in VertexPositionNormalColorTexture a, in VertexPositionNormalColorTexture b)
    {
      return !a.Position.Equals(b.Position) || !a.Normal.Equals(b.Normal) || !a.VertexColor.Equals(b.VertexColor) || !a.TextureCoordinate.Equals(b.TextureCoordinate);
    }

    /// <summary>
    /// Indicates whether this instance and a specified object are equal.
    /// </summary>
    /// <param name="obj">Another object to compare to.</param>
    /// <returns>True if <paramref name="obj" /> and this instance are the same type and represent the same value; otherwise, false.</returns>
    public readonly override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is VertexPositionNormalColorTexture)
      {
        VertexPositionNormalColorTexture other = (VertexPositionNormalColorTexture) obj;
        return Position.Equals(other.Position) && Normal.Equals(other.Normal) && VertexColor.Equals(other.VertexColor) && TextureCoordinate.Equals(other.TextureCoordinate);
      }

      return false;
    }

    /// <summary>
    /// Tests equality between two vertices.
    /// </summary>
    /// <param name="other">Other vertex to compare to.</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    readonly bool IEquatable<VertexPositionNormalColorTexture>.Equals(VertexPositionNormalColorTexture other)
    {
      return Position.Equals(other.Position) && Normal.Equals(other.Normal) && VertexColor.Equals(other.VertexColor) && TextureCoordinate.Equals(other.TextureCoordinate);
    }

    /// <summary>
    /// Tests equality between two vertices.
    /// </summary>
    /// <param name="other">Other vertex to compare to.</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    public readonly bool Equals(in VertexPositionNormalColorTexture other)
    {
      return Position.Equals(other.Position) && Normal.Equals(other.Normal) && VertexColor.Equals(other.VertexColor) && TextureCoordinate.Equals(other.TextureCoordinate);
    }

    /// <summary>
    /// Tests equality between two vertices within tolerance.
    /// </summary>
    /// <param name="other">Other vertex to compare to.</param>
    /// <param name="tolerance">Tolerance</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    public readonly bool Equals(in VertexPositionNormalColorTexture other, float tolerance)
    {
      return Position.Equals(other.Position, tolerance) && Normal.Equals(other.Normal, tolerance) && VertexColor.Equals(other.VertexColor) && TextureCoordinate.Equals(other.TextureCoordinate, tolerance);
    }

    /// <summary>
    /// Gets the layout of the vertex.
    /// </summary>
    /// <returns>Vertex layout</returns>
    public readonly VertexLayout GetVertexLayout()
    {
      return VertexLayout;
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
    public readonly override int GetHashCode()
    {
      unchecked
      {
        return Position.GetHashCode() + Normal.GetHashCode() + VertexColor.GetHashCode() + TextureCoordinate.GetHashCode();
      }
    }

    /// <summary>
    /// Returns the fully qualified type name of this instance.
    /// </summary>
    /// <returns>A <see cref="T:System.String" /> containing a fully qualified type name.</returns>
    public readonly override string ToString()
    {
      return String.Format(CultureInfo.CurrentCulture, "Position:{0}, Normal:{1}, VertexColor:{2}, TextureCoordinate:{3}", new object[] { Position.ToString(), Normal.ToString(), VertexColor.ToString(), TextureCoordinate.ToString() });
    }

    /// <summary>
    /// Writes the primitive data to the output.
    /// </summary>
    /// <param name="output">Primitive writer.</param>
    public readonly void Write(IPrimitiveWriter output)
    {
      output.Write<Vector3>("Position", Position);
      output.Write<Vector3>("Normal", Normal);
      output.Write<Color>("VertexColor", VertexColor);
      output.Write<Vector2>("TextureCoordinate", TextureCoordinate);
    }

    /// <summary>
    /// Reads the primitive data from the input.
    /// </summary>
    /// <param name="input">Primitive reader.</param>
    public void Read(IPrimitiveReader input)
    {
      input.Read<Vector3>("Position", out Position);
      input.Read<Vector3>("Normal", out Normal);
      input.Read<Color>("VertexColor", out VertexColor);
      input.Read<Vector2>("TextureCoordinate", out TextureCoordinate);
    }
  }
}
