﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Content;
using Tesla.Graphics.Implementation;

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// Represents a render state that configures the behavior of the depth-stencil buffer.
  /// </summary>
  [SavableVersion(1)]
  public class DepthStencilState : RenderState
  {
    private RenderStateKey m_key;

    #region Predefined DepthStencilStates

    /// <inheritdoc cref="IPredefinedDepthStencilStateProvider.IsReverseDepth" />
    public static bool IsReverseDepth
    {
      get
      {
        return IRenderSystem.Current.PredefinedDepthStencilStates.IsReverseDepth;
      }
    }

    /// <inheritdoc cref="IPredefinedDepthStencilStateProvider.None" />
    public static DepthStencilState None
    {
      get
      {
        return IRenderSystem.Current.PredefinedDepthStencilStates.None;
      }
    }

    /// <inheritdoc cref="IPredefinedDepthStencilStateProvider.DepthWriteOff" />
    public static DepthStencilState DepthWriteOff
    {
      get
      {
        return IRenderSystem.Current.PredefinedDepthStencilStates.DepthWriteOff;
      }
    }

    /// <inheritdoc cref="IPredefinedDepthStencilStateProvider.Default" />
    public static DepthStencilState Default
    {
      get
      {
        return IRenderSystem.Current.PredefinedDepthStencilStates.Default;
      }
    }

    #endregion

    #region RenderState Properties

    /// <summary>
    /// Gets if the render state has been bound to the pipeline, once bound the state becomes read-only.
    /// </summary>
    public override bool IsBound
    {
      get
      {
        return DepthStencilStateImpl.IsBound;
      }
    }

    /// <summary>
    /// Gets the render state type.
    /// </summary>
    public override RenderStateType StateType
    {
      get
      {
        return RenderStateType.DepthStencilState;
      }
    }

    /// <summary>
    /// Gets the key that identifies this render state type and configuration for comparing states.
    /// </summary>
    public override RenderStateKey RenderStateKey
    {
      get
      {
        if (!DepthStencilStateImpl.IsBound)
          return ComputeRenderStateKey();

        return m_key;
      }
    }

    #endregion

    #region Public DepthStencilState Properties

    /// <summary>
    /// Gets or sets if the depth buffer should be enabled. By default, this value is true.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public bool DepthEnable
    {
      get
      {
        return DepthStencilStateImpl.DepthEnable;
      }
      set
      {
        DepthStencilStateImpl.DepthEnable = value;
      }
    }

    /// <summary>
    /// Gets or sets if the depth buffer should be writable. By default, this value is true.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public bool DepthWriteEnable
    {
      get
      {
        return DepthStencilStateImpl.DepthWriteEnable;
      }
      set
      {
        DepthStencilStateImpl.DepthWriteEnable = value;
      }
    }

    /// <summary>
    /// Gets or sets the depth comparison function for the depth test. By default, this value is <see cref="ComparisonFunction.LessEqual"/>.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public ComparisonFunction DepthFunction
    {
      get
      {
        return DepthStencilStateImpl.DepthFunction;
      }
      set
      {
        DepthStencilStateImpl.DepthFunction = value;
      }
    }

    /// <summary>
    /// Gets or sets if the stencil buffer should be enabled. By default, this value is false.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public bool StencilEnable
    {
      get
      {
        return DepthStencilStateImpl.StencilEnable;
      }
      set
      {
        DepthStencilStateImpl.StencilEnable = value;
      }
    }

    /// <summary>
    /// Gets or sets the reference stencil value used for stencil testing. By default, this value is zero.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception> 
    public int ReferenceStencil
    {
      get
      {
        return DepthStencilStateImpl.ReferenceStencil;
      }
      set
      {
        DepthStencilStateImpl.ReferenceStencil = value;
      }
    }

    /// <summary>
    /// Gets or sets the value that identifies a portion of the depth-stencil buffer for reading stencil data. By default, this value is <see cref="int.MaxValue"/>.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public int StencilReadMask
    {
      get
      {
        return DepthStencilStateImpl.StencilReadMask;
      }
      set
      {
        DepthStencilStateImpl.StencilReadMask = value;
      }
    }

    /// <summary>
    /// Gets or sets the value that identifies a portion of the depth-stencil buffer for writing stencil data. By default, this value is <see cref="int.MaxValue"/>.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public int StencilWriteMask
    {
      get
      {
        return DepthStencilStateImpl.StencilWriteMask;
      }
      set
      {
        DepthStencilStateImpl.StencilWriteMask = value;
      }
    }

    /// <summary>
    /// Gets or sets if two sided stenciling is enabled, where if back face stencil testing/operations should be conducted in addition to the front face (as dictated by the winding order
    /// of the primitive). By default, this value is false.
    /// </summary>
    public bool TwoSidedStencilEnable
    {
      get
      {
        return DepthStencilStateImpl.TwoSidedStencilEnable;
      }
      set
      {
        DepthStencilStateImpl.TwoSidedStencilEnable = value;
      }
    }

    /// <summary>
    /// Gets or sets the comparison function used for testing a front facing triangle. By default, this value is <see cref="ComparisonFunction.Always"/>.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public ComparisonFunction StencilFunction
    {
      get
      {
        return DepthStencilStateImpl.StencilFunction;
      }
      set
      {
        DepthStencilStateImpl.StencilFunction = value;
      }
    }

    /// <summary>
    /// Gets or sets the stencil operation done when the stencil test passes, but the depth test fails for a front facing triangle. By default, this value is
    /// <see cref="StencilOperation.Keep"/>.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public StencilOperation StencilDepthFail
    {
      get
      {
        return DepthStencilStateImpl.StencilDepthFail;
      }
      set
      {
        DepthStencilStateImpl.StencilDepthFail = value;
      }
    }

    /// <summary>
    /// Gets or sets the stencil operation done when the stencil test fails for a front facing triangle. By default, this value is <see cref="StencilOperation.Keep"/>.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public StencilOperation StencilFail
    {
      get
      {
        return DepthStencilStateImpl.StencilFail;
      }
      set
      {
        DepthStencilStateImpl.StencilFail = value;
      }
    }

    /// <summary>
    /// Gets or sets the stencil operation done when the stencil test passes for a front facing triangle. By default, this value is <see cref="StencilOperation.Keep"/>.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public StencilOperation StencilPass
    {
      get
      {
        return DepthStencilStateImpl.StencilPass;
      }
      set
      {
        DepthStencilStateImpl.StencilPass = value;
      }
    }

    /// <summary>
    /// Gets or sets the comparison function used for testing a back facing triangle. By default, this value is <see cref="ComparisonFunction.Always"/>.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception> 
    public ComparisonFunction BackFaceStencilFunction
    {
      get
      {
        return DepthStencilStateImpl.BackFaceStencilFunction;
      }
      set
      {
        DepthStencilStateImpl.BackFaceStencilFunction = value;
      }
    }

    /// <summary>
    /// Gets or sets the stencil operation done when the stencil test passes, but the depth test fails for a back facing triangle. By default, this value is 
    /// <see cref="StencilOperation.Keep"/>.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public StencilOperation BackFaceStencilDepthFail
    {
      get
      {
        return DepthStencilStateImpl.BackFaceStencilDepthFail;
      }
      set
      {
        DepthStencilStateImpl.BackFaceStencilDepthFail = value;
      }
    }

    /// <summary>
    /// Gets or sets the stencil operation done when the stencil test fails for a back facing triangle. By default, this value is <see cref="StencilOperation.Keep"/>.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public StencilOperation BackFaceStencilFail
    {
      get
      {
        return DepthStencilStateImpl.BackFaceStencilFail;
      }
      set
      {
        DepthStencilStateImpl.BackFaceStencilFail = value;
      }
    }

    /// <summary>
    /// Gets or sets the stencil operation done when the stencil test passes for a back facing triangle. By default, this value is <see cref="StencilOperation.Keep"/>.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public StencilOperation BackFaceStencilPass
    {
      get
      {
        return DepthStencilStateImpl.BackFaceStencilPass;
      }
      set
      {
        DepthStencilStateImpl.BackFaceStencilPass = value;
      }
    }

    #endregion

    //Private property to cast the implementation
    private IDepthStencilStateImpl DepthStencilStateImpl
    {
      get
      {
        return GetImplAs<IDepthStencilStateImpl>();
      }
      set
      {
        BindImplementation(value);
      }
    }

    /// <summary>
    /// For ISavable only.
    /// </summary>
    protected DepthStencilState() { }

    /// <summary>
    /// Constructs a new instance of the <see cref="DepthStencilState"/> class.
    /// </summary>
    /// <param name="renderSystem">Render system used to create the underlying implementation.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the render system is null.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if creating the underlying implementation fails or is unsupported, see inner exception for a more detailed error.</exception>
    public DepthStencilState(IRenderSystem renderSystem)
    {
      CreateImplementation(renderSystem);
      SetDefaults();
    }

    /// <summary>
    /// Binds the render state to the graphics pipeline. If not called after the state is created, it is automatically done the first time the render state
    /// is applied. Once bound, the render state becomes immutable.
    /// </summary>
    public override void BindRenderState()
    {
      if (!DepthStencilStateImpl.IsBound)
      {
        DepthStencilStateImpl.BindDepthStencilState();
        m_key = ComputeRenderStateKey();
      }
    }

    #region ISavable Methods

    /// <summary>
    /// Reads the object data from the input.
    /// </summary>
    /// <param name="input">Savable reader</param>
    public override void Read(ISavableReader input)
    {
      IRenderSystem renderSystem = GraphicsHelper.GetRenderSystem(input.ServiceProvider);
      IDepthStencilStateImpl impl = CreateImplementation(renderSystem);

      Name = input.ReadString("Name");
      impl.DepthEnable = input.ReadBoolean("DepthEnable");
      impl.DepthWriteEnable = input.ReadBoolean("DepthWriteEnable");
      impl.DepthFunction = input.ReadEnum<ComparisonFunction>("DepthFunction");
      impl.StencilEnable = input.ReadBoolean("StencilEnable");
      impl.ReferenceStencil = input.ReadInt32("ReferenceStencil");
      impl.StencilReadMask = input.ReadInt32("StencilReadMask");
      impl.StencilWriteMask = input.ReadInt32("StencilWriteMask");
      impl.TwoSidedStencilEnable = input.ReadBoolean("TwoSidedStencilEnable");
      impl.StencilFunction = input.ReadEnum<ComparisonFunction>("StencilFunction");
      impl.StencilDepthFail = input.ReadEnum<StencilOperation>("StencilDepthFail");
      impl.StencilFail = input.ReadEnum<StencilOperation>("StencilFail");
      impl.StencilPass = input.ReadEnum<StencilOperation>("StencilPass");
      impl.BackFaceStencilFunction = input.ReadEnum<ComparisonFunction>("BackFaceStencilFunction");
      impl.BackFaceStencilDepthFail = input.ReadEnum<StencilOperation>("BackFaceStencilDepthFail");
      impl.BackFaceStencilFail = input.ReadEnum<StencilOperation>("BackFaceStencilFail");
      impl.BackFaceStencilPass = input.ReadEnum<StencilOperation>("BackFaceStencilPass");

      impl.BindDepthStencilState();
    }

    /// <summary>
    /// Writes the object data to the output.
    /// </summary>
    /// <param name="output">Savable writer</param>
    public override void Write(ISavableWriter output)
    {
      IDepthStencilStateImpl impl = DepthStencilStateImpl;

      output.Write("Name", Name);
      output.Write("DepthEnable", impl.DepthEnable);
      output.Write("DepthWriteEnable", impl.DepthWriteEnable);
      output.WriteEnum<ComparisonFunction>("DepthFunction", impl.DepthFunction);
      output.Write("StencilEnable", impl.StencilEnable);
      output.Write("ReferenceStencil", impl.ReferenceStencil);
      output.Write("StencilReadMask", impl.StencilReadMask);
      output.Write("StencilWriteMask", impl.StencilWriteMask);
      output.Write("TwoSidedStencilEnable", impl.TwoSidedStencilEnable);
      output.WriteEnum<ComparisonFunction>("StencilFunction", impl.StencilFunction);
      output.WriteEnum<StencilOperation>("StencilDepthFail", impl.StencilDepthFail);
      output.WriteEnum<StencilOperation>("StencilFail", impl.StencilFail);
      output.WriteEnum<StencilOperation>("StencilPass", impl.StencilPass);
      output.WriteEnum<ComparisonFunction>("BackFaceStencilFunction", impl.BackFaceStencilFunction);
      output.WriteEnum<StencilOperation>("BackFaceStencilDepthFail", impl.BackFaceStencilDepthFail);
      output.WriteEnum<StencilOperation>("BackFaceStencilFail", impl.BackFaceStencilFail);
      output.WriteEnum<StencilOperation>("BackFaceStencilPass", impl.BackFaceStencilPass);
    }

    #endregion

    #region Implementation Creation and State Key Methods

    private IDepthStencilStateImpl CreateImplementation(IRenderSystem renderSystem)
    {
      if (renderSystem == null)
        throw new ArgumentNullException("renderSystem", StringLocalizer.Instance.GetLocalizedString("RenderSystemNull"));

      IDepthStencilStateImplFactory factory;
      if (!renderSystem.TryGetImplementationFactory<IDepthStencilStateImplFactory>(out factory))
        throw GraphicsExceptionHelper.NewUnsupportedFeatureException(renderSystem, typeof(DepthStencilState));

      try
      {
        DepthStencilStateImpl = factory.CreateImplementation();
      }
      catch (Exception e)
      {
        throw GraphicsExceptionHelper.NewErrorCreatingImplementation(typeof(DepthStencilState), e);
      }

      return DepthStencilStateImpl;
    }

    private void SetDefaults()
    {
      IDepthStencilStateImpl impl = DepthStencilStateImpl;

      impl.DepthEnable = true;
      impl.DepthWriteEnable = true;
      impl.DepthFunction = ComparisonFunction.LessEqual;
      impl.StencilEnable = false;
      impl.ReferenceStencil = 0;
      impl.BackFaceStencilFunction = ComparisonFunction.Always;
      impl.BackFaceStencilDepthFail = StencilOperation.Keep;
      impl.BackFaceStencilFail = StencilOperation.Keep;
      impl.BackFaceStencilPass = StencilOperation.Keep;
      impl.StencilFunction = ComparisonFunction.Always;
      impl.StencilDepthFail = StencilOperation.Keep;
      impl.StencilFail = StencilOperation.Keep;
      impl.StencilPass = StencilOperation.Keep;
      impl.TwoSidedStencilEnable = false;
      impl.StencilReadMask = int.MaxValue;
      impl.StencilWriteMask = int.MaxValue;
    }

    private RenderStateKey ComputeRenderStateKey()
    {
      unchecked
      {
        IDepthStencilStateImpl impl = DepthStencilStateImpl;

        //Casting enums to ints to avoid boxing

        int hash = 17;

        hash = (hash * 31) + (int) StateType;
        hash = (hash * 31) + ((impl.DepthEnable) ? 1 : 0);
        hash = (hash * 31) + ((impl.DepthWriteEnable) ? 1 : 0);
        hash = (hash * 31) + (int) impl.DepthFunction;
        hash = (hash * 31) + ((impl.StencilEnable) ? 1 : 0);
        hash = (hash * 31) + ((impl.TwoSidedStencilEnable) ? 1 : 0);
        hash = (hash * 31) + impl.ReferenceStencil;
        hash = (hash * 31) + (int) impl.StencilFunction;
        hash = (hash * 31) + (int) impl.StencilDepthFail;
        hash = (hash * 31) + (int) impl.StencilFail;
        hash = (hash * 31) + (int) impl.StencilPass;
        hash = (hash * 31) + (int) impl.BackFaceStencilFunction;
        hash = (hash * 31) + (int) impl.BackFaceStencilDepthFail;
        hash = (hash * 31) + (int) impl.BackFaceStencilFail;
        hash = (hash * 31) + (int) impl.BackFaceStencilPass;
        hash = (hash * 31) + (impl.StencilWriteMask % int.MaxValue);
        hash = (hash * 31) + (impl.StencilReadMask % int.MaxValue);

        return new RenderStateKey(StateType, hash);
      }
    }

    #endregion
  }
}
