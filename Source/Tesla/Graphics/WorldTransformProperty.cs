﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// Render property for a <see cref="Transform"/> that represents an object's world transform.
  /// </summary>
  public sealed class WorldTransformProperty : RenderPropertyAccessor<Transform>
  {
    private Transform? m_value;

    /// <summary>
    /// Unique ID for this render property.
    /// </summary>
    public static RenderPropertyID PropertyID = GetPropertyID<WorldTransformProperty>();

    /// <summary>
    /// Constructs a new instance of the <see cref="WorldTransformProperty"/> class.
    /// </summary>
    /// <param name="accessor">Accessor function.</param>
    public WorldTransformProperty(GetPropertyValue<Transform> accessor) : base(PropertyID, accessor) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="WorldTransformProperty"/> class.
    /// </summary>
    public WorldTransformProperty() : this(new Transform()) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="WorldTransformProperty"/> class.
    /// </summary>
    /// <param name="transform">World transform.</param>
    public WorldTransformProperty(Transform transform)
        : base(PropertyID)
    {
      m_value = transform ?? new Transform();
      Accessor = GetValue;
    }

    private Transform GetValue()
    {
      return m_value!;
    }
  }
}
