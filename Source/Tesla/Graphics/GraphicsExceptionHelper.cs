﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;

#nullable enable

namespace Tesla.Graphics
{
  internal static class GraphicsExceptionHelper
  {
    public static TeslaGraphicsException NewMustSupplyDataForImmutableException()
    {
      return new TeslaGraphicsException(StringLocalizer.Instance.GetLocalizedString("MustSupplyDataForImmutable"));
    }

    public static TeslaGraphicsException NewUnsupportedFeatureException(IRenderSystem renderSystem, Type graphicsResource)
    {
      return new TeslaGraphicsException(StringLocalizer.Instance.GetLocalizedString("FeatureNotSupported", renderSystem.Platform, graphicsResource.FullName));
    }

    public static TeslaGraphicsException NewErrorWritingToResourceException(Type graphicsResource, Exception? platformException)
    {
      if (platformException is null)
        return new TeslaGraphicsException(StringLocalizer.Instance.GetLocalizedString("ErrorWritingToResource", graphicsResource.FullName, String.Empty));

      return new TeslaGraphicsException(StringLocalizer.Instance.GetLocalizedString("ErrorWritingToResource", graphicsResource.FullName, platformException.Message), platformException);
    }

    public static TeslaGraphicsException NewErrorReadingFromResourceException(Type graphicsResource, Exception? platformException)
    {
      if (platformException is null)
        return new TeslaGraphicsException(StringLocalizer.Instance.GetLocalizedString("ErrorReadingFromResource", graphicsResource.FullName, String.Empty));

      return new TeslaGraphicsException(StringLocalizer.Instance.GetLocalizedString("ErrorReadingFromResource", graphicsResource.FullName, platformException.Message), platformException);
    }

    public static TeslaGraphicsException NewErrorCreatingImplementation(Type graphicsResource, Exception? platformException)
    {
      if (platformException is null)
        return new TeslaGraphicsException(StringLocalizer.Instance.GetLocalizedString("ErrorCreatingResourceImplementation", graphicsResource.FullName, String.Empty));

      return new TeslaGraphicsException(StringLocalizer.Instance.GetLocalizedString("ErrorCreatingResourceImplementation", graphicsResource.FullName, platformException.Message), platformException);
    }

    public static TeslaGraphicsException NewBadTextureFormatException(SurfaceFormat format, TextureDimension texDim)
    {
      return new TeslaGraphicsException(StringLocalizer.Instance.GetLocalizedString("BadTextureFormat", format.ToString(), texDim.ToString()));
    }

    public static TeslaGraphicsException NewBlockCompressionMultipleOfFourException(SurfaceFormat format)
    {
      return new TeslaGraphicsException(StringLocalizer.Instance.GetLocalizedString("BlockCompressionMultipleOfFour", format.ToString()));
    }

    public static TeslaGraphicsException NewBadRenderTargetFormatException(SurfaceFormat format)
    {
      return new TeslaGraphicsException(StringLocalizer.Instance.GetLocalizedString("BadRenderTargetFormat", format.ToString()));
    }

    public static TeslaGraphicsException NewBadBackBufferFormatException(SurfaceFormat format)
    {
      return new TeslaGraphicsException(StringLocalizer.Instance.GetLocalizedString("BadBackBufferFormat", format.ToString()));
    }

    public static TeslaGraphicsException NewQueryFailedOnBegin(Exception platformException)
    {
      return new TeslaGraphicsException(StringLocalizer.Instance.GetLocalizedString("QueryFailedOnBegin"), platformException);
    }

    public static TeslaGraphicsException NewQueryFailedOnEnd(Exception platformException)
    {
      return new TeslaGraphicsException(StringLocalizer.Instance.GetLocalizedString("QueryFailedOnEnd"), platformException);
    }

    public static TeslaGraphicsException NewDeferredWriteDataException()
    {
      return new TeslaGraphicsException(StringLocalizer.Instance.GetLocalizedString("DeferredSetDataMustBeDynamic"));
    }

    public static TeslaGraphicsException NewDynamicResourceMustBeSingleSubresource()
    {
      return new TeslaGraphicsException(StringLocalizer.Instance.GetLocalizedString("DynamicResourceMustBeSingleSubresource"));
    }

    public static ObjectDisposedException NewObjectDisposedException(Type type)
    {
      return new ObjectDisposedException(type.FullName);
    }
  }
}
