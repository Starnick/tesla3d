﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Runtime.CompilerServices;
using Tesla.Content;

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// A camera represents how a 3D scene is viewed. It handles updating the view and projection matrices and has a 2D viewport which the 3D scene is
  /// mapped to during rendering. 
  /// </summary>
  [SavableVersion(1)]
  public class Camera : ISavable, INamable
  {
    /// <summary>
    /// Occurs when the camera's viewport changes.
    /// </summary>
    public event TypedEventHandler<Camera>? ViewportChanged;

    /// <summary>
    /// Occurs when the camera is updated.
    /// </summary>
    public event TypedEventHandler<Camera>? Updated;

    private String m_name;
    private Matrix m_view;
    private Matrix m_proj;
    private Matrix m_viewProj;
    private Viewport m_viewport;
    private Vector3 m_position;
    private Vector3 m_direction;
    private Vector3 m_up;
    private Vector3 m_right;
    private ProjectionMode m_projectionMode;
    private BoundingFrustum m_frustum;
    private float m_nearPlaneDistance, m_farPlaneDistance;

    /// <summary>
    /// Gets or sets the name of the camera.
    /// </summary>
    public String Name
    {
      get
      {
        return m_name;
      }
      set
      {
        m_name = value;
      }
    }

    /// <summary>
    /// Gets the view matrix of the camera.
    /// </summary>
    public ref readonly Matrix ViewMatrix
    {
      get
      {
        return ref m_view;
      }
    }

    /// <summary>
    /// Gets the projection matrix of the camera.
    /// </summary>
    public ref readonly Matrix ProjectionMatrix
    {
      get
      {
        return ref m_proj;
      }
    }

    /// <summary>
    /// Gets the View x Projection matrix.
    /// </summary>
    public ref readonly Matrix ViewProjectionMatrix
    {
      get
      {
        return ref m_viewProj;
      }
    }

    /// <summary>
    /// Gets or sets the viewport associated with the camera.
    /// </summary>
    public Viewport Viewport
    {
      get
      {
        return m_viewport;
      }
      set
      {
        m_viewport = value;

        OnViewportChanged();
      }
    }

    /// <summary>
    /// Gets or sets the position of the camera in world space.
    /// </summary>
    public Vector3 Position
    {
      get
      {
        return m_position;
      }
      set
      {
        m_position = value;
      }
    }

    /// <summary>
    /// Gets or sets the direction the camera is facing in (forward vector of the camera's frame). This will be normalized automatically.
    /// </summary>
    public Vector3 Direction
    {
      get
      {
        return m_direction;
      }
      set
      {
        m_direction = value;
        m_direction.Normalize();
      }
    }

    /// <summary>
    /// Gets or sets the left vector of the camera's frame. This will be normalized automatically.
    /// </summary>
    public Vector3 Right
    {
      get
      {
        return m_right;
      }
      set
      {
        m_right = value;
        m_right.Normalize();
      }
    }

    /// <summary>
    /// Gets or sets the up vector of the camera's frame. This will be normalized automatically.
    /// </summary>
    public Vector3 Up
    {
      get
      {
        return m_up;
      }
      set
      {
        m_up = value;
        m_up.Normalize();
      }
    }

    /// <summary>
    /// Gets the projection mode of the camera. This can be set in the SetProjection methods.
    /// </summary>
    public ProjectionMode ProjectionMode
    {
      get
      {
        return m_projectionMode;
      }
    }

    /// <summary>
    /// Gets the bounding frustum of the camera. This represents the visible space of the camera and can be used
    /// for a number of containment and intersection queries.
    /// </summary>
    public BoundingFrustum Frustum
    {
      get
      {
        return m_frustum;
      }
    }

    /// <summary>
    /// Gets the distance to the near view plane that was used to construct the projection matrix. This distance is in viewspace, so its relative to the origin.
    /// </summary>
    public float NearPlaneDistance
    {
      get
      {
        return m_nearPlaneDistance;
      }
    }

    /// <summary>
    /// Gets the distance to the far view plane that was used to construct the projection matrix. This distance is in viewspace, so its relative to the origin.
    /// </summary>
    public float FarPlaneDistance
    {
      get
      {
        return m_farPlaneDistance;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Camera"/> class.
    /// </summary>
    public Camera()
    {
      m_name = "Camera";
      m_viewport = new Viewport(0, 0, 1, 1);
      m_position = new Vector3(0, 0, 0);
      m_right = Vector3.Right;
      m_up = Vector3.Up;
      m_direction = Vector3.Forward;
      m_projectionMode = ProjectionMode.Perspective;
      m_nearPlaneDistance = 0.0f;
      m_farPlaneDistance = 0.0f;
      m_frustum = new BoundingFrustum();
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Camera"/> class.
    /// </summary>
    /// <param name="viewport">Camera viewport</param>
    public Camera(in Viewport viewport)
    {
      m_name = "Camera";
      m_viewport = viewport;
      m_position = new Vector3(0, 0, 0);
      m_right = Vector3.Right;
      m_up = Vector3.Up;
      m_direction = Vector3.Forward;
      m_projectionMode = ProjectionMode.Perspective;
      m_frustum = new BoundingFrustum();
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Camera"/> class.
    /// </summary>
    /// <param name="name">Name of the camera</param>
    public Camera(String name)
    {
      m_name = name;
      m_viewport = new Viewport(0, 0, 1, 1);
      m_position = new Vector3(0, 0, 0);
      m_right = Vector3.Right;
      m_up = Vector3.Up;
      m_direction = Vector3.Forward;
      m_projectionMode = ProjectionMode.Perspective;
      m_frustum = new BoundingFrustum();
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Camera"/> class.
    /// </summary>
    /// <param name="name">Name of the camera</param>
    /// <param name="viewport">Camera viewport</param>
    public Camera(String name, in Viewport viewport)
    {
      m_name = name;
      m_viewport = viewport;
      m_position = new Vector3(0, 0, 0);
      m_right = Vector3.Right;
      m_up = Vector3.Up;
      m_direction = Vector3.Forward;
      m_projectionMode = ProjectionMode.Perspective;
      m_frustum = new BoundingFrustum();
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Camera"/> class.
    /// </summary>
    /// <param name="camToClone">Camera to copy from</param>
    public Camera(Camera camToClone)
    {
      m_name = String.Empty;
      m_frustum = new BoundingFrustum();
      Set(camToClone);
    }

    /// <summary>
    /// Sets this camera to match the values of the source camera.
    /// </summary>
    /// <param name="source">Source camera to copy from</param>
    public virtual void Set(Camera source)
    {
      m_name = source.Name;
      m_viewport = source.Viewport;
      m_position = source.Position;
      m_right = source.Right;
      m_up = source.Up;
      m_direction = source.Direction;
      m_projectionMode = source.ProjectionMode;
      m_proj = source.ProjectionMatrix;
      m_view = source.ViewMatrix;
      m_nearPlaneDistance = source.m_nearPlaneDistance;
      m_farPlaneDistance = source.m_farPlaneDistance;

      Update();
    }

    /// <summary>
    /// Set the camera's projection, either an orthographic camera or a perspective camera.
    /// </summary>
    /// <param name="mode">Projection mode, perspective or orthographic</param>
    /// <param name="width">Width of the view volume at the near view plane</param>
    /// <param name="height">Height of the view volume at the near view plane</param>
    /// <param name="near">Distance to the near view plane (or min-Z value if ortho)</param>
    /// <param name="far">Distance to the far view plane (or max-Z value if ortho)</param>
    public void SetProjection(ProjectionMode mode, float width, float height, float near, float far)
    {
      if (mode == ProjectionMode.Perspective)
      {
        m_proj = Matrix.CreatePerspectiveMatrix(width, height, near, far);
      }
      else
      {
        m_proj = Matrix.CreateOrthographicMatrix(width, height, near, far);
      }

      m_projectionMode = mode;
      m_nearPlaneDistance = near;
      m_farPlaneDistance = far;
    }

    /// <summary>
    /// Sets the camera's projection. This will always create a perspective camera.
    /// </summary>
    /// <param name="fieldOfView">Field of view angle.</param>
    /// <param name="near">Distance to the near view plane</param>
    /// <param name="far">Distance to the far view plane</param>
    public void SetProjection(Angle fieldOfView, float near, float far)
    {
      SetProjection(fieldOfView, near, far, DepthStencilState.IsReverseDepth);
    }

    /// <summary>
    /// Sets the camera's projection. This will always create a perspective camera.
    /// </summary>
    /// <param name="fieldOfView">Field of view angle.</param>
    /// <param name="near">Distance to the near view plane</param>
    /// <param name="far">Distance to the far view plane</param>
    /// <param name="isInfinite">True if the projection matrix should be an infinite.</param>
    public void SetProjection(Angle fieldOfView, float near, float far, bool isInfinite = false)
    {
      if (!isInfinite)
        m_proj = Matrix.CreatePerspectiveFOVMatrix(fieldOfView, m_viewport.AspectRatio, near, far);
      else
        Matrix.CreateInfinitePerspectiveFOVMatrix(fieldOfView, m_viewport.AspectRatio, near, out m_proj);

      m_projectionMode = ProjectionMode.Perspective;
      m_nearPlaneDistance = near;
      m_farPlaneDistance = far;
    }

    /// <summary>
    /// Sets the camera's projection, either an orthographic camera or a perspective camera.
    /// </summary>
    /// <param name="mode">Projection mode, perspective or orthographic</param>
    /// <param name="left">Minimum x value of the view volume at the near view plane</param>
    /// <param name="right">Maximum x value of the view volume at the near view plane</param>
    /// <param name="bottom">Minimum y value of the view volume at the near view plane</param>
    /// <param name="top">Maximum y value of the view volume at the near view plane</param>
    /// <param name="near">Distance to the near view plane (or min-Z value if ortho)</param>
    /// <param name="far">Distance to the far view plane (or max-Z value if ortho)</param>
    public void SetProjection(ProjectionMode mode, float left, float right, float bottom, float top, float near, float far)
    {
      if (mode == ProjectionMode.Perspective)
      {
        m_proj = Matrix.CreatePerspectiveMatrix(left, right, bottom, top, near, far);
      }
      else
      {
        m_proj = Matrix.CreateOrthographicMatrix(left, right, bottom, top, near, far);
      }

      m_projectionMode = mode;
      m_nearPlaneDistance = near;
      m_farPlaneDistance = far;
    }

    /// <summary>
    /// Sets the axes of the camera's frame. This will normalize the axes.
    /// </summary>
    /// <param name="right">Right vector</param>
    /// <param name="up">Up vector</param>
    /// <param name="direction">Forward vector</param>
    public void SetAxes(in Vector3 right, in Vector3 up, in Vector3 direction)
    {
      m_right = right;
      m_right.Normalize();

      m_up = up;
      m_up.Normalize();

      m_direction = direction;
      m_direction.Normalize();
    }

    /// <summary>
    /// Sets the axes of the camera's frame from a quaternion rotation. This will normalize the axes.
    /// </summary>
    /// <param name="axes">Quaternion rotation</param>
    public void SetAxes(in Quaternion axes)
    {
      Matrix temp;
      Matrix.FromQuaternion(axes, out temp);

      m_right = temp.Right;
      m_right.Normalize();

      m_up = temp.Up;
      m_up.Normalize();

      m_direction = temp.Forward;
      m_direction.Normalize();
    }

    /// <summary>
    /// Sets the axes of the camera's frame from a matrix rotation. This will normalize the axes.
    /// </summary>
    /// <param name="axes">Matrix rotation</param>
    public void SetAxes(in Matrix axes)
    {
      m_right = axes.Right;
      m_right.Normalize();

      m_up = axes.Up;
      m_up.Normalize();

      m_direction = axes.Forward;
      m_direction.Normalize();
    }

    /// <summary>
    /// Set's the camera's frame. This will normalize the axes.
    /// </summary>
    /// <param name="position">Camera position</param>
    /// <param name="right">Right vector</param>
    /// <param name="up">Up vector</param>
    /// <param name="direction">Forward vector</param>
    public void SetFrame(in Vector3 position, in Vector3 right, in Vector3 up, in Vector3 direction)
    {
      m_position = position;

      m_right = right;
      m_right.Normalize();

      m_up = up;
      m_up.Normalize();

      m_direction = direction;
      m_direction.Normalize();
    }

    /// <summary>
    /// Sets the camera's frame from the Quaternion rotation. This will normalize the axes.
    /// </summary>
    /// <param name="position">Camera position</param>
    /// <param name="axes">Quaterion rotation to get the axes from</param>
    public void SetFrame(in Vector3 position, in Quaternion axes)
    {
      m_position = position;

      Matrix temp;
      Matrix.FromQuaternion(axes, out temp);

      m_right = temp.Right;
      m_right.Normalize();

      m_up = temp.Up;
      m_up.Normalize();

      m_direction = temp.Forward;
      m_direction.Normalize();
    }

    /// <summary>
    /// Set's the camera's frame from the Matrix rotation. This will normalize the axes.
    /// </summary>
    /// <param name="position">Camera position</param>
    /// <param name="axes">Matrix rotation to get the axes from</param>
    public void SetFrame(in Vector3 position, in Matrix axes)
    {
      m_position = position;

      m_right = axes.Right;
      m_right.Normalize();

      m_up = axes.Up;
      m_up.Normalize();

      m_direction = axes.Forward;
      m_direction.Normalize();
    }

    /// <summary>
    /// Updates the camera's view matrix and bounding frustum. This should be called whenever the projection matrix is changed or 
    /// the camera's frame is changed.
    /// </summary>
    public virtual void Update()
    {
      //Temp _position + _direction
      Vector3 posd;
      Vector3.Add(m_position, m_direction, out posd);

      //Create the new view matrix
      Matrix.CreateViewMatrix(m_position, posd, m_up, out m_view);

      //Create the View x Projection matrix
      Matrix.Multiply(m_view, m_proj, out m_viewProj);

      //Update the frustum
      m_frustum.Set(m_viewProj);

      OnUpdated();
    }

    /// <summary>
    /// Makes the camera look at the target.
    /// </summary>
    /// <param name="target">Target vector</param>
    /// <param name="worldUp">The up vector in the world</param>
    public void LookAt(in Vector3 target, Vector3 worldUp)
    {
      //Ensure world up is normalized + valid
      worldUp.Normalize();
      if (worldUp.IsAlmostZero())
        worldUp = Vector3.Up;

      //Find the direction
      Vector3.NormalizedSubtract(m_position, target, out Vector3 backward);
      Vector3.Negate(backward, out m_direction);

      //Find right axis
      Vector3.NormalizedCross(worldUp, backward, out m_right);

      //Find local up axis
      Vector3.NormalizedCross(backward, m_right, out m_up);

      //Update the view matrix
      Update();
    }

    /// <summary>
    /// Gets the world position based on the x,y screen coordinates.
    /// </summary>
    /// <param name="screenPosition">Screen position (x,y) and z being the depth (0 - near, 1 - far planes)</param>
    /// <returns>Constructed world position</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public Vector3 GetWorldCoordinates(in Vector3 screenPosition)
    {
      m_viewport.UnProject(screenPosition, Matrix.Identity, m_view, m_proj, out Vector3 result);

      return result;
    }

    /// <summary>
    /// Gets the screen position based on the world position.
    /// </summary>
    /// <param name="worldPosition">World position</param>
    /// <returns>Screen position</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public Vector3 GetScreenCoordinates(in Vector3 worldPosition)
    {
      m_viewport.Project(worldPosition, m_viewProj, out Vector3 result);

      return result;
    }


    /// <summary>
    /// Creates a ray to do picking tests, where the origin is on the near plane. This is the same as using
    /// GetWorldCoordinates for (x,y,0) and (x,y,1) and using the normalized difference as the ray's direction.
    /// </summary>
    /// <param name="screenPos">Screen position (x,y) and z being the depth (0 - near, 1 - far planes)</param>
    /// <returns>Resulting ray</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public Ray CreatePickRay(in Vector2 screenPos)
    {
      return CreatePickRay(screenPos, DepthStencilState.IsReverseDepth);
    }

    /// <summary>
    /// Creates a ray to do picking tests, where the origin is on the near plane. This is the same as using
    /// GetWorldCoordinates for (x,y,0) and (x,y,1) and using the normalized difference as the ray's direction.
    /// </summary>
    /// <param name="screenPos">Screen position (x,y) and z being the depth (0 - near, 1 - far planes)</param>
    /// <param name="reverseDepth">Optional flag if the projection matrix was created for reverse depth (meaning the near/far planes are switched).</param>
    /// <returns>Resulting ray</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public Ray CreatePickRay(in Vector2 screenPos, bool reverseDepth = false)
    {
      float near = 0;
      float far = 1.0f;

      if (reverseDepth)
      {
        near = 1.0f;
        far = 0.0f;
      }

      Vector3 posNear = new Vector3(screenPos, near);
      Vector3 posFar = new Vector3(screenPos, far);

      m_viewport.UnProject(posNear, m_viewProj, out posNear);
      m_viewport.UnProject(posFar, m_viewProj, out posFar);

      Vector3.NormalizedSubtract(posFar, posNear, out Vector3 dir);

      Ray ray;
      ray.Direction = dir;
      ray.Origin = posNear;

      return ray;
    }

    /// <summary>
    /// Given a 3D point in space, determine a size (in world units) that the pixel over it represents.
    /// As objects get closer or further from the camera, the number of pixels to represent the same area
    /// either increases or decreases.
    /// </summary>
    /// <param name="worldPos">World position.</param>
    /// <returns>Pixel size in world units.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public float GetPixelSizeAtPoint(in Vector3 worldPos)
    {
      Vector3 screenPos = GetScreenCoordinates(worldPos);
      Vector3 screenOffsetPos = new Vector3(screenPos.X + 1.0f, screenPos.Y, screenPos.Z);
      Vector3 worldOffsetPos = GetWorldCoordinates(screenOffsetPos);
      float pixelSize = Vector3.Distance(worldPos, worldOffsetPos);
      return pixelSize;
    }

    /// <summary>
    /// Given a bounding volume, come up with the representation of the volume by a rectangle
    /// in screen coordinates.
    /// </summary>
    /// <param name="bv">Bounding volume.</param>
    /// <param name="padding">Optional padding to apply.</param>
    /// <returns>Extents in screen coordinates.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public Rectangle GetScreenExtents(BoundingVolume bv, int padding = 0)
    {
      if (bv is BoundingSphere bs)
      {
        Vector3 centerScreen = GetScreenCoordinates(bs.Center);
        Vector3 centerOnSphereScreen = GetScreenCoordinates(bs.Center + (Right * bs.Radius));
        Vector3 centerOnSphereScreen2 = GetScreenCoordinates(bs.Center + (Up * bs.Radius));

        int radiusScreen = (int) MathF.Ceiling(Vector3.DistanceXY(centerScreen, centerOnSphereScreen)) + padding;
        int diameterScreen = radiusScreen * 2;
        return new Rectangle((int) centerScreen.X - radiusScreen, (int) centerScreen.Y - radiusScreen, diameterScreen, diameterScreen);
      }

      ReadOnlySpan<Vector3> corners = bv.Corners;
      Int2 min = new Int2(int.MaxValue, int.MaxValue);
      Int2 max = new Int2(-int.MaxValue, -int.MaxValue);

      for (int i = 0; i < corners.Length; i++)
      {
        Vector3 screenPos = GetScreenCoordinates(corners[i]);
        Int2 pos = new Int2((int) screenPos.X, (int) screenPos.Y);
        min = Int2.Min(min, pos);
        max = Int2.Max(max, pos);
      }

      Rectangle rect = Rectangle.FromPoints(min, max);

      if (padding > 0)
        rect.Inflate(padding);

      return rect;
    }

    /// <summary>
    /// Serializes the object and writes it to the output.
    /// </summary>
    /// <param name="output">Savable Output</param>
    public virtual void Write(ISavableWriter output)
    {
      output.Write("Name", m_name);
      output.Write("Viewport", new Vector4(m_viewport.X, m_viewport.Y, m_viewport.Width, m_viewport.Height));
      output.Write("ViewportDepth", new Vector2(m_viewport.MinDepth, m_viewport.MaxDepth));
      output.Write<Vector3>("Position", m_position);
      output.Write<Vector3>("Up", m_up);
      output.Write("Direction", m_direction);
      output.WriteEnum<ProjectionMode>("ProjectionMode", m_projectionMode);
      output.Write("ProjectionMatrix", m_proj);
    }

    /// <summary>
    /// Deserializes the object and populates it from the input.
    /// </summary>
    /// <param name="input">Savable input</param>
    public virtual void Read(ISavableReader input)
    {
      m_name = input.ReadStringOrDefault("Name");
      Vector4 vp = input.Read<Vector4>("Viewport");
      Vector2 depth = input.Read<Vector2>("ViewportDepth");
      m_viewport = new Viewport((int) vp.X, (int) vp.Y, (int) vp.Z, (int) vp.W);
      m_viewport.MinDepth = depth.X;
      m_viewport.MaxDepth = depth.Y;

      m_position = input.Read<Vector3>("Position");
      m_up = input.Read<Vector3>("Up");
      m_direction = input.Read<Vector3>("Direction");
      m_projectionMode = input.ReadEnum<ProjectionMode>("ProjectionMode");
      m_proj = input.Read<Matrix>("ProjectionMatrix");

      Update();
    }

    private void OnViewportChanged()
    {
      TypedEventHandler<Camera>? handler = ViewportChanged;
      if (handler is not null)
        handler(this, EventArgs.Empty);
    }

    private void OnUpdated()
    {
      TypedEventHandler<Camera>? handler = Updated;
      if (handler is not null)
        handler(this, EventArgs.Empty);
    }
  }
}
