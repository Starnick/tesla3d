﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Content;
using Tesla.Graphics.Implementation;

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// Represents a render state that controls how geometry is rasterized.
  /// </summary>
  [SavableVersion(1)]
  public class RasterizerState : RenderState
  {
    private RenderStateKey m_key;

    #region Predefined RasterizerStates

    /// <summary>
    /// Gets a predefined state object where culling is disabled.
    /// </summary>
    public static RasterizerState CullNone
    {
      get
      {
        return IRenderSystem.Current.PredefinedRasterizerStates.CullNone;
      }
    }

    /// <summary>
    /// Gets a predefined state object where back faces are culled and front faces have
    /// a clockwise vertex winding. This is the default state.
    /// </summary>
    public static RasterizerState CullBackClockwiseFront
    {
      get
      {
        return IRenderSystem.Current.PredefinedRasterizerStates.CullBackClockwiseFront;
      }
    }

    /// <summary>
    /// Gets a predefined state object where back faces are culled and front faces have a counterclockwise
    /// vertex winding.
    /// </summary>
    public static RasterizerState CullBackCounterClockwiseFront
    {
      get
      {
        return IRenderSystem.Current.PredefinedRasterizerStates.CullBackCounterClockwiseFront;
      }
    }

    /// <summary>
    /// Gets a predefined state object where culling is disabled and fillmode is wireframe.
    /// </summary>
    public static RasterizerState CullNoneWireframe
    {
      get
      {
        return IRenderSystem.Current.PredefinedRasterizerStates.CullNoneWireframe;
      }
    }
    #endregion

    #region RenderState Properties

    /// <summary>
    /// Gets if the render state has been bound to the pipeline, once bound the state becomes read-only.
    /// </summary>
    public override bool IsBound
    {
      get
      {
        return RasterizerStateImpl.IsBound;
      }
    }

    /// <summary>
    /// Gets the render state type.
    /// </summary>
    public override RenderStateType StateType
    {
      get
      {
        return RenderStateType.RasterizerState;
      }
    }

    /// <summary>
    /// Gets the key that identifies this render state type and configuration for comparing states.
    /// </summary>
    public override RenderStateKey RenderStateKey
    {
      get
      {
        if (!RasterizerStateImpl.IsBound)
          return ComputeRenderStateKey();

        return m_key;
      }
    }

    #endregion

    #region Public RasterizerState Properties

    /// <summary>
    /// Gets if the <see cref="AntialiasedLineEnable" /> property is supported. This can vary by implementation.
    /// </summary>
    public bool IsAntialiasedLineOptionSupported
    {
      get
      {
        return RasterizerStateImpl.IsAntialiasedLineOptionSupported;
      }
    }

    /// <summary>
    /// Gets if the <see cref="DepthClipEnable" /> property is supported. This can vary by implementation.
    /// </summary>
    public bool IsDepthClipOptionSupported
    {
      get
      {
        return RasterizerStateImpl.IsDepthClipOptionSupported;
      }
    }

    /// <summary>
    /// Gets or sets how primitives are to be culled. By default, this value is <see cref="CullMode.Back" />.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public CullMode Cull
    {
      get
      {
        return RasterizerStateImpl.Cull;
      }
      set
      {
        RasterizerStateImpl.Cull = value;
      }
    }

    /// <summary>
    /// Gets or sets the vertex winding of a primitive, specifying the front face of the triangle. By default, this value is <see cref="Graphics.VertexWinding.Clockwise" />.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public VertexWinding VertexWinding
    {
      get
      {
        return RasterizerStateImpl.VertexWinding;
      }
      set
      {
        RasterizerStateImpl.VertexWinding = value;
      }
    }

    /// <summary>
    /// Gets or sets the fill mode of a primitive. By default, this value is <see cref="FillMode.Solid" />.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public FillMode Fill
    {
      get
      {
        return RasterizerStateImpl.Fill;
      }
      set
      {
        RasterizerStateImpl.Fill = value;
      }
    }

    /// <summary>
    /// Gets or sets the depth bias, which is a value added to the depth value at a given pixel. By default, this value is zero.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception> 
    public int DepthBias
    {
      get
      {
        return RasterizerStateImpl.DepthBias;
      }
      set
      {
        RasterizerStateImpl.DepthBias = value;
      }
    }

    /// <summary>
    /// Gets or sets the depth bias clamp (maximum value) of a pixel. By default, this value is zero.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public float DepthBiasClamp
    {
      get
      {
        return RasterizerStateImpl.DepthBiasClamp;
      }
      set
      {
        RasterizerStateImpl.DepthBiasClamp = value;
      }
    }

    /// <summary>
    /// Gets or sets the slope scaled depth bias, a scalar on a given pixel's slope. By default, this value is zero.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public float SlopeScaledDepthBias
    {
      get
      {
        return RasterizerStateImpl.SlopeScaledDepthBias;
      }
      set
      {
        RasterizerStateImpl.SlopeScaledDepthBias = value;
      }
    }

    /// <summary>
    /// Gets or sets if depth clipping is enabled. If false, the hardware skips z-clipping. By default, this value is true.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public bool DepthClipEnable
    {
      get
      {
        return RasterizerStateImpl.DepthClipEnable;
      }
      set
      {
        RasterizerStateImpl.DepthClipEnable = value;
      }
    }

    /// <summary>
    /// Gets or sets whether to use the quadrilateral or alpha line anti-aliasing algorithm on MSAA render targets. If set to true, the quadrilaterla line anti-aliasing algorithm is used.
    /// Otherwise the alpha line-anti-aliasing algorithm is used, if <see cref="MultiSampleEnable"/> is set to false and is supported. By default, this value is true.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public bool MultiSampleEnable
    {
      get
      {
        return RasterizerStateImpl.MultiSampleEnable;
      }
      set
      {
        RasterizerStateImpl.MultiSampleEnable = value;
      }
    }

    /// <summary>
    /// Gets or sets whether to enable line antialising. This only applies if doing line drawing and <see cref="MultiSampleEnable" /> is set to false. 
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public bool AntialiasedLineEnable
    {
      get
      {
        return RasterizerStateImpl.AntialiasedLineEnable;
      }
      set
      {
        RasterizerStateImpl.AntialiasedLineEnable = value;
      }
    }

    /// <summary>
    /// Gets or sets if scissor rectangle culling should be enabled or not. All pixels outside an active scissor rectangle are culled. By default, this value is set to false.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception> 
    public bool ScissorTestEnable
    {
      get
      {
        return RasterizerStateImpl.ScissorTestEnable;
      }
      set
      {
        RasterizerStateImpl.ScissorTestEnable = value;
      }
    }

    #endregion

    //Private property to cast the implementation
    private IRasterizerStateImpl RasterizerStateImpl
    {
      get
      {
        return GetImplAs<IRasterizerStateImpl>();
      }
      set
      {
        BindImplementation(value);
      }
    }

    /// <summary>
    /// For ISavable only.
    /// </summary>
    protected RasterizerState() { }

    /// <summary>
    /// Constructs a new instance of the <see cref="RasterizerState"/> class.
    /// </summary>
    /// <param name="renderSystem">Render system used to create the underlying implementation.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the render system is null.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if creating the underlying implementation fails or is unsupported, see inner exception for a more detailed error.</exception>
    public RasterizerState(IRenderSystem renderSystem)
    {
      CreateImplementation(renderSystem);
      SetDefaults();
    }

    /// <summary>
    /// Binds the render state to the graphics pipeline. If not called after the state is created, it is automatically done the first time the render state
    /// is applied. Once bound, the render state becomes immutable.
    /// </summary>
    public override void BindRenderState()
    {
      if (!RasterizerStateImpl.IsBound)
      {
        RasterizerStateImpl.BindRasterizerState();
        m_key = ComputeRenderStateKey();
      }
    }

    #region ISavable Methods

    /// <summary>
    /// Reads the object data from the input.
    /// </summary>
    /// <param name="input">Savable reader</param>
    public override void Read(ISavableReader input)
    {
      if (Implementation != null)
        Implementation.Dispose();

      IRenderSystem renderSystem = GraphicsHelper.GetRenderSystem(input.ServiceProvider);
      IRasterizerStateImpl impl = CreateImplementation(renderSystem);

      Name = input.ReadString("Name");
      impl.Cull = input.ReadEnum<CullMode>("Cull");
      impl.VertexWinding = input.ReadEnum<VertexWinding>("VertexWinding");
      impl.Fill = input.ReadEnum<FillMode>("Fill");
      impl.DepthBias = input.ReadInt32("DepthBias");
      impl.DepthBiasClamp = input.ReadSingle("DepthBiasClamp");
      impl.SlopeScaledDepthBias = input.ReadSingle("SlopeScaledDepthBias");
      impl.DepthClipEnable = input.ReadBoolean("DepthClipEnable");
      impl.MultiSampleEnable = input.ReadBoolean("MultiSampleEnable");
      impl.AntialiasedLineEnable = input.ReadBoolean("AntialiasedLineEnable");
      impl.ScissorTestEnable = input.ReadBoolean("ScissorTestEnable");

      impl.BindRasterizerState();
    }

    /// <summary>
    /// Writes the object data to the output.
    /// </summary>
    /// <param name="output">Savable writer</param>
    public override void Write(ISavableWriter output)
    {
      IRasterizerStateImpl impl = RasterizerStateImpl;

      output.Write("Name", Name);
      output.WriteEnum<CullMode>("Cull", impl.Cull);
      output.WriteEnum<VertexWinding>("VertexWinding", impl.VertexWinding);
      output.WriteEnum<FillMode>("Fill", impl.Fill);
      output.Write("DepthBias", impl.DepthBias);
      output.Write("DepthBiasClamp", impl.DepthBiasClamp);
      output.Write("SlopeScaledDepthBias", impl.SlopeScaledDepthBias);
      output.Write("DepthClipEnable", impl.DepthClipEnable);
      output.Write("MultiSampleEnable", impl.MultiSampleEnable);
      output.Write("AntialiasedLineEnable", impl.AntialiasedLineEnable);
      output.Write("ScissorTestEnable", impl.ScissorTestEnable);
    }

    #endregion

    #region Implementation Creation and State Key Methods

    private IRasterizerStateImpl CreateImplementation(IRenderSystem renderSystem)
    {
      if (renderSystem == null)
        throw new ArgumentNullException("renderSystem", StringLocalizer.Instance.GetLocalizedString("RenderSystemNull"));

      IRasterizerStateImplFactory factory;
      if (!renderSystem.TryGetImplementationFactory<IRasterizerStateImplFactory>(out factory))
        throw GraphicsExceptionHelper.NewUnsupportedFeatureException(renderSystem, typeof(RasterizerState));

      try
      {
        RasterizerStateImpl = factory.CreateImplementation();
      }
      catch (Exception e)
      {
        throw GraphicsExceptionHelper.NewErrorCreatingImplementation(typeof(RasterizerState), e);
      }

      return RasterizerStateImpl;
    }

    private void SetDefaults()
    {
      IRasterizerStateImpl impl = RasterizerStateImpl;

      impl.Cull = CullMode.Back;
      impl.VertexWinding = VertexWinding.Clockwise;
      impl.Fill = FillMode.Solid;
      impl.DepthBias = 0;
      impl.DepthBiasClamp = 0.0f;
      impl.SlopeScaledDepthBias = 0.0f;
      impl.DepthClipEnable = true;
      impl.MultiSampleEnable = true;
      impl.AntialiasedLineEnable = false;
      impl.ScissorTestEnable = false;
    }

    private RenderStateKey ComputeRenderStateKey()
    {
      unchecked
      {
        IRasterizerStateImpl impl = RasterizerStateImpl;

        //Casting enums to ints to avoid boxing

        int hash = 17;

        hash = (hash * 31) + (int) StateType;
        hash = (hash * 31) + (int) impl.Cull;
        hash = (hash * 31) + (int) impl.VertexWinding;
        hash = (hash * 31) + (int) impl.Fill;
        hash = (hash * 31) + impl.DepthBias;
        hash = (hash * 31) + impl.DepthBiasClamp.GetHashCode();
        hash = (hash * 31) + impl.SlopeScaledDepthBias.GetHashCode();
        hash = (hash * 31) + ((impl.DepthClipEnable) ? 1 : 0);
        hash = (hash * 31) + ((impl.MultiSampleEnable) ? 1 : 0);
        hash = (hash * 31) + ((impl.AntialiasedLineEnable) ? 1 : 0);
        hash = (hash * 31) + ((impl.ScissorTestEnable) ? 1 : 0);

        return new RenderStateKey(StateType, hash);
      }
    }

    #endregion
  }
}
