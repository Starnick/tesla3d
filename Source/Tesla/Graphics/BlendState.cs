﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Content;
using Tesla.Graphics.Implementation;

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// Represents a render state that controls how pixels are blended during rendering. Blending is specified on a per-render target basis.
  /// </summary>
  [SavableVersion(1)]
  public class BlendState : RenderState
  {
    private RenderStateKey m_key;

    #region Predefined BlendStates

    /// <summary>
    /// Gets a predefined state object for opaque blending, where no blending occurs and destination color overwrites source color. This is 
    /// the default state.
    /// </summary>
    public static BlendState Opaque
    {
      get
      {
        return IRenderSystem.Current.PredefinedBlendStates.Opaque;
      }
    }

    /// <summary>
    /// Gets a predefined state object for premultiplied alpha blending, where source and destination colors are blended by using
    /// alpha, and where the color contains alpha pre-multiplied.
    /// </summary>
    public static BlendState AlphaBlendPremultiplied
    {
      get
      {
        return IRenderSystem.Current.PredefinedBlendStates.AlphaBlendPremultiplied;
      }
    }

    /// <summary>
    /// Gets a predefined state object for additive blending, where source and destination color are blended without using alpha.
    /// </summary>
    public static BlendState AdditiveBlend
    {
      get
      {
        return IRenderSystem.Current.PredefinedBlendStates.AdditiveBlend;
      }
    }

    /// <summary>
    ///Gets a predefined state object for non-premultiplied alpha blending, where the source and destination color are blended by using alpha,
    ///and where the color does not contain the alpha pre-multiplied.
    /// </summary>
    public static BlendState AlphaBlendNonPremultiplied
    {
      get
      {
        return IRenderSystem.Current.PredefinedBlendStates.AlphaBlendNonPremultiplied;
      }
    }

    #endregion

    #region RenderState Properties

    /// <summary>
    /// Gets if the render state has been bound to the pipeline, once bound the state becomes read-only.
    /// </summary>
    public override bool IsBound
    {
      get
      {
        return BlendStateImpl.IsBound;
      }
    }

    /// <summary>
    /// Gets the render state type.
    /// </summary>
    public override RenderStateType StateType
    {
      get
      {
        return RenderStateType.BlendState;
      }
    }

    /// <summary>
    /// Gets the key that identifies this render state type and configuration for comparing states.
    /// </summary>
    public override RenderStateKey RenderStateKey
    {
      get
      {
        if (!BlendStateImpl.IsBound)
          return ComputeRenderStateKey();

        return m_key;
      }
    }

    #endregion

    #region Common BlendState Properties

    /// <summary>
    /// Gets the number of render targets that allow for independent blending. This can vary by implementation, at least one is always guaranteed.
    /// </summary>
    public int RenderTargetBlendCount
    {
      get
      {
        return BlendStateImpl.RenderTargetBlendCount;
      }
    }

    /// <summary>
    /// Checks if alpha-to-coverage is supported. This can vary by implementation.
    /// </summary>
    public bool IsAlphaToCoverageSupported
    {
      get
      {
        return BlendStateImpl.IsAlphaToCoverageSupported;
      }
    }

    /// <summary>
    /// Checks if independent blending of multiple render targets (MRT) is supported. This can vary by implementation. If not supported, then the blending options
    /// specified for the first render target index are used for all other bound render targets, if those targets blend are enabled.
    /// </summary>
    public bool IsIndependentBlendSupported
    {
      get
      {
        return BlendStateImpl.IsIndependentBlendSupported;
      }
    }

    /// <summary>
    /// Gets or sets whether alpha-to-coverage should be used as a multisampling technique when writing a pixel to a render target. Support for this may vary by implementation. By
    /// default, this value is false.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public bool AlphaToCoverageEnable
    {
      get
      {
        return BlendStateImpl.AlphaToCoverageEnable;
      }
      set
      {
        BlendStateImpl.AlphaToCoverageEnable = value;
      }
    }

    /// <summary>
    /// Gets or sets whether independent blending is enabled for multiple render targets (MRT). If this is false, the blending options specified for the first render target index
    /// is used for all render targets currently bound. Support for this may vary by implementation. By default, this value is false.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public bool IndependentBlendEnable
    {
      get
      {
        return BlendStateImpl.IndependentBlendEnable;
      }
      set
      {
        BlendStateImpl.IndependentBlendEnable = value;
      }
    }

    /// <summary>
    /// Gets or sets the blend factor color. By default, this value is <see cref="Color.White" />.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public Color BlendFactor
    {
      get
      {
        return BlendStateImpl.BlendFactor;
      }
      set
      {
        BlendStateImpl.BlendFactor = value;
      }
    }

    /// <summary>
    /// Gets or sets the multisample mask. By default, this value is 0xffffffff.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public int MultiSampleMask
    {
      get
      {
        return BlendStateImpl.MultiSampleMask;
      }
      set
      {
        BlendStateImpl.MultiSampleMask = value;
      }
    }

    #endregion

    #region Public Properties for first Render Target

    /// <summary>
    /// Gets or sets if blending is enabled for the first render target. By default, this value is false.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public bool BlendEnable
    {
      get
      {
        return GetBlendEnable(0);
      }
      set
      {
        SetBlendEnable(0, value);
      }
    }

    /// <summary>
    /// Gets or sets the alpha blend function for the first render target. By default, this value is <see cref="BlendFunction.Add"/>.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public BlendFunction AlphaBlendFunction
    {
      get
      {
        return GetAlphaBlendFunction(0);
      }
      set
      {
        SetAlphaBlendFunction(0, value);
      }
    }

    /// <summary>
    /// Gets or sets the alpha source blend for the first render target. By default, this value is <see cref="Blend.One"/>.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public Blend AlphaSourceBlend
    {
      get
      {
        return GetAlphaSourceBlend(0);
      }
      set
      {
        SetAlphaSourceBlend(0, value);
      }
    }

    /// <summary>
    /// Gets or sets the alpha destination blend for the first render target. By default, this value is <see cref="Blend.Zero"/>.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception> 
    public Blend AlphaDestinationBlend
    {
      get
      {
        return GetAlphaDestinationBlend(0);
      }
      set
      {
        SetAlphaDestinationBlend(0, value);
      }
    }

    /// <summary>
    /// Gets or sets the color blend function for the first render target. By default, this value is <see cref="BlendFunction.Add"/>.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public BlendFunction ColorBlendFunction
    {
      get
      {
        return GetColorBlendFunction(0);
      }
      set
      {
        SetColorBlendFunction(0, value);
      }
    }

    /// <summary>
    /// Gets or sets the color source blend for the first render target. By default, this value is <see cref="Blend.One"/>.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public Blend ColorSourceBlend
    {
      get
      {
        return GetColorSourceBlend(0);
      }
      set
      {
        SetColorSourceBlend(0, value);
      }
    }

    /// <summary>
    /// Gets or sets the color destination blend for the first render target. By default, this value is <see cref="Blend.Zero"/>.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public Blend ColorDestinationBlend
    {
      get
      {
        return GetColorDestinationBlend(0);
      }
      set
      {
        SetColorDestinationBlend(0, value);
      }
    }

    /// <summary>
    /// Gets or sets the color write channels for the first render target. By default, this value is <see cref="ColorWriteChannels.All"/>.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    public ColorWriteChannels WriteChannels
    {
      get
      {
        return GetWriteChannels(0);
      }
      set
      {
        SetWriteChannels(0, value);
      }
    }

    #endregion

    //Private property to cast the implementation
    private IBlendStateImpl BlendStateImpl
    {
      get
      {
        return GetImplAs<IBlendStateImpl>();
      }
      set
      {
        BindImplementation(value);
      }
    }

    /// <summary>
    /// For ISavable only.
    /// </summary>
    protected BlendState() { }

    /// <summary>
    /// Constructs a new instance of the <see cref="BlendState"/> class.
    /// </summary>
    /// <param name="renderSystem">Render system used to create the underlying implementation.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the render system is null.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if creating the underlying implementation fails or is unsupported, see inner exception for a more detailed error.</exception>
    public BlendState(IRenderSystem renderSystem)
    {
      CreateImplementation(renderSystem);
      SetDefaults();
    }

    #region Public methods

    /// <summary>
    /// Gets if blending is enabled for the render target at the specified index.
    /// </summary>
    /// <param name="renderTargetIndex">Zero-based index of the render target that is bound to the pipeline.</param>
    /// <returns>True if blending is enabled, false otherwise.</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the render target index is out of range.</exception>
    public bool GetBlendEnable(int renderTargetIndex)
    {
      RenderTargetBlendDescription blendDesc;
      BlendStateImpl.GetRenderTargetBlendDescription(renderTargetIndex, out blendDesc);

      return blendDesc.BlendEnable;
    }

    /// <summary>
    /// Sets if blending should be enabled for the render target at the specified index.
    /// </summary>
    /// <param name="renderTargetIndex">Zero-based index of the render target that is bound to the pipeline.</param>
    /// <param name="blendEnable">True if blending should be enabled, false otherwise.</param>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the render target index is out of range.</exception>
    public void SetBlendEnable(int renderTargetIndex, bool blendEnable)
    {
      IBlendStateImpl impl = BlendStateImpl;
      RenderTargetBlendDescription blendDesc;
      impl.GetRenderTargetBlendDescription(renderTargetIndex, out blendDesc);

      blendDesc.BlendEnable = blendEnable;
      impl.SetRenderTargetBlendDescription(renderTargetIndex, ref blendDesc);
    }

    /// <summary>
    /// Gets the alpha blend function for the render target at the specified index.
    /// </summary>
    /// <param name="renderTargetIndex">Zero-based index of the render target that is bound to the pipeline.</param>
    /// <returns>The alpha blend function</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the render target index is out of range.</exception>
    public BlendFunction GetAlphaBlendFunction(int renderTargetIndex)
    {
      RenderTargetBlendDescription blendDesc;
      BlendStateImpl.GetRenderTargetBlendDescription(renderTargetIndex, out blendDesc);

      return blendDesc.AlphaBlendFunction;
    }

    /// <summary>
    /// Sets the alpha blend function for the render target at the specified index.
    /// </summary>
    /// <param name="renderTargetIndex">Zero-based index of the render target that is bound to the pipeline.</param>
    /// <param name="alphaBlendFunction">Alpha blend function</param>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the render target index is out of range.</exception>
    public void SetAlphaBlendFunction(int renderTargetIndex, BlendFunction alphaBlendFunction)
    {
      IBlendStateImpl impl = BlendStateImpl;
      RenderTargetBlendDescription blendDesc;
      impl.GetRenderTargetBlendDescription(renderTargetIndex, out blendDesc);

      blendDesc.AlphaBlendFunction = alphaBlendFunction;
      impl.SetRenderTargetBlendDescription(renderTargetIndex, ref blendDesc);
    }

    /// <summary>
    /// Gets the alpha source blend for the render target at the specified index.
    /// </summary>
    /// <param name="renderTargetIndex">Zero-based index of the render target that is bound to the pipeline.</param>
    /// <returns>The alpha source blend</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the render target index is out of range.</exception>
    public Blend GetAlphaSourceBlend(int renderTargetIndex)
    {
      RenderTargetBlendDescription blendDesc;
      BlendStateImpl.GetRenderTargetBlendDescription(renderTargetIndex, out blendDesc);

      return blendDesc.AlphaSourceBlend;
    }

    /// <summary>
    /// Sets the alpha source blend for the render target at the specified index.
    /// </summary>
    /// <param name="renderTargetIndex">Zero-based index of the render target that is bound to the pipeline.</param>
    /// <param name="alphaSourceBlend">Alpha source blend to set</param>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the render target index is out of range.</exception>
    public void SetAlphaSourceBlend(int renderTargetIndex, Blend alphaSourceBlend)
    {
      IBlendStateImpl impl = BlendStateImpl;
      RenderTargetBlendDescription blendDesc;
      impl.GetRenderTargetBlendDescription(renderTargetIndex, out blendDesc);

      blendDesc.AlphaSourceBlend = alphaSourceBlend;
      impl.SetRenderTargetBlendDescription(renderTargetIndex, ref blendDesc);
    }

    /// <summary>
    /// Gets the alpha destination blend for the render target at the specified index.
    /// </summary>
    /// <param name="renderTargetIndex">Zero-based index of the render target that is bound to the pipeline.</param>
    /// <returns>The alpha destination blend</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the render target index is out of range.</exception>
    public Blend GetAlphaDestinationBlend(int renderTargetIndex)
    {
      RenderTargetBlendDescription blendDesc;
      BlendStateImpl.GetRenderTargetBlendDescription(renderTargetIndex, out blendDesc);

      return blendDesc.AlphaDestinationBlend;
    }

    /// <summary>
    /// Sets the alpha destination blend for the render target at the specified index.
    /// </summary>
    /// <param name="renderTargetIndex">Zero-based index of the render target that is bound to the pipeline.</param>
    /// <param name="alphaDestinationBlend">Alpha destination blend to set</param>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the render target index is out of range.</exception>
    public void SetAlphaDestinationBlend(int renderTargetIndex, Blend alphaDestinationBlend)
    {
      IBlendStateImpl impl = BlendStateImpl;
      RenderTargetBlendDescription blendDesc;
      impl.GetRenderTargetBlendDescription(renderTargetIndex, out blendDesc);

      blendDesc.AlphaDestinationBlend = alphaDestinationBlend;
      impl.SetRenderTargetBlendDescription(renderTargetIndex, ref blendDesc);
    }

    /// <summary>
    /// Gets the color blend function for the render target at the specified index.
    /// </summary>
    /// <param name="renderTargetIndex">Zero-based index of the render target that is bound to the pipeline.</param>
    /// <returns>The color blend function</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the render target index is out of range.</exception>
    public BlendFunction GetColorBlendFunction(int renderTargetIndex)
    {
      RenderTargetBlendDescription blendDesc;
      BlendStateImpl.GetRenderTargetBlendDescription(renderTargetIndex, out blendDesc);

      return blendDesc.ColorBlendFunction;
    }

    /// <summary>
    /// Sets the color blend function for the render target at the specified index.
    /// </summary>
    /// <param name="renderTargetIndex">Zero-based index of the render target that is bound to the pipeline.</param>
    /// <param name="colorBlendFunction">Color blend function</param>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the render target index is out of range.</exception>
    public void SetColorBlendFunction(int renderTargetIndex, BlendFunction colorBlendFunction)
    {
      IBlendStateImpl impl = BlendStateImpl;
      RenderTargetBlendDescription blendDesc;
      impl.GetRenderTargetBlendDescription(renderTargetIndex, out blendDesc);

      blendDesc.ColorBlendFunction = colorBlendFunction;
      impl.SetRenderTargetBlendDescription(renderTargetIndex, ref blendDesc);
    }

    /// <summary>
    /// Gets the color source blend for the render target at the specified index.
    /// </summary>
    /// <param name="renderTargetIndex">Zero-based index of the render target that is bound to the pipeline.</param>
    /// <returns>The color source blend</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the render target index is out of range.</exception>
    public Blend GetColorSourceBlend(int renderTargetIndex)
    {
      RenderTargetBlendDescription blendDesc;
      BlendStateImpl.GetRenderTargetBlendDescription(renderTargetIndex, out blendDesc);

      return blendDesc.ColorSourceBlend;
    }

    /// <summary>
    /// Sets the color source blend for the render target at the specified index.
    /// </summary>
    /// <param name="renderTargetIndex">Zero-based index of the render target that is bound to the pipeline.</param>
    /// <param name="colorSourceBlend">Color source blend to set</param>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the render target index is out of range.</exception>
    public void SetColorSourceBlend(int renderTargetIndex, Blend colorSourceBlend)
    {
      IBlendStateImpl impl = BlendStateImpl;
      RenderTargetBlendDescription blendDesc;
      impl.GetRenderTargetBlendDescription(renderTargetIndex, out blendDesc);

      blendDesc.ColorSourceBlend = colorSourceBlend;
      impl.SetRenderTargetBlendDescription(renderTargetIndex, ref blendDesc);
    }

    /// <summary>
    /// Gets the color destination blend for the render target at the specified index.
    /// </summary>
    /// <param name="renderTargetIndex">Zero-based index of the render target that is bound to the pipeline.</param>
    /// <returns>The color destination blend</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the render target index is out of range.</exception>
    public Blend GetColorDestinationBlend(int renderTargetIndex)
    {
      RenderTargetBlendDescription blendDesc;
      BlendStateImpl.GetRenderTargetBlendDescription(renderTargetIndex, out blendDesc);

      return blendDesc.ColorDestinationBlend;
    }

    /// <summary>
    /// Sets the color destination blend for the render target at the specified index.
    /// </summary>
    /// <param name="renderTargetIndex">Zero-based index of the render target that is bound to the pipeline.</param>
    /// <param name="colorDestinationBlend">Color destination blend to set</param>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the render target index is out of range.</exception>
    public void SetColorDestinationBlend(int renderTargetIndex, Blend colorDestinationBlend)
    {
      IBlendStateImpl impl = BlendStateImpl;
      RenderTargetBlendDescription blendDesc;
      impl.GetRenderTargetBlendDescription(renderTargetIndex, out blendDesc);

      blendDesc.ColorDestinationBlend = colorDestinationBlend;
      impl.SetRenderTargetBlendDescription(renderTargetIndex, ref blendDesc);
    }

    /// <summary>
    /// Gets the color write channels for the render target at the specified index.
    /// </summary>
    /// <param name="renderTargetIndex">Zero-based index of the render target that is bound to the pipeline.</param>
    /// <returns>The color write channel mask</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the render target index is out of range.</exception>
    public ColorWriteChannels GetWriteChannels(int renderTargetIndex)
    {
      RenderTargetBlendDescription blendDesc;
      BlendStateImpl.GetRenderTargetBlendDescription(renderTargetIndex, out blendDesc);

      return blendDesc.WriteChannels;
    }

    /// <summary>
    /// Sets the color write channels for the render target at the specified index.
    /// </summary>
    /// <param name="renderTargetIndex">Zero-based index of the render target that is bound to the pipeline.</param>
    /// <param name="writeChannels">Color write channel mask to set</param>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the render target index is out of range.</exception>
    public void SetWriteChannels(int renderTargetIndex, ColorWriteChannels writeChannels)
    {
      IBlendStateImpl impl = BlendStateImpl;
      RenderTargetBlendDescription blendDesc;
      impl.GetRenderTargetBlendDescription(renderTargetIndex, out blendDesc);

      blendDesc.WriteChannels = writeChannels;
      impl.SetRenderTargetBlendDescription(renderTargetIndex, ref blendDesc);
    }

    /// <summary>
    /// Gets the complete blend description for a render target bound at the specified index.
    /// </summary>
    /// <param name="renderTargetIndex">Zero-based index of the render target that is bound to the pipeline.</param>
    /// <param name="blendDesc">The blend description that holds the blending options for the render target.</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the render target index is out of range.</exception>
    public void GetRenderTargetBlendDescription(int renderTargetIndex, out RenderTargetBlendDescription blendDesc)
    {
      BlendStateImpl.GetRenderTargetBlendDescription(renderTargetIndex, out blendDesc);
    }

    /// <summary>
    /// Sets the complete blend description for a render target bound at the specified index.
    /// </summary>
    /// <param name="renderTargetIndex">Zero-based index of the render target that is bound to the pipeline.</param>
    /// <param name="blendDesc">The blend description that holds the blending options for the render target.</param>
    /// <exception cref="InvalidOperationException">Thrown if the implementation is already bound. Once bound, render states are immutable.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the render target index is out of range.</exception>
    public void SetRenderTargetBlendDescription(int renderTargetIndex, ref RenderTargetBlendDescription blendDesc)
    {
      BlendStateImpl.SetRenderTargetBlendDescription(renderTargetIndex, ref blendDesc);
    }

    /// <summary>
    /// Binds the render state to the graphics pipeline. If not called after the state is created, it is automatically done the first time the render state
    /// is applied. Once bound, the render state becomes immutable.
    /// </summary>
    public override void BindRenderState()
    {
      if (!BlendStateImpl.IsBound)
      {
        BlendStateImpl.BindBlendState();
        m_key = ComputeRenderStateKey();
      }
    }

    #endregion

    #region ISavable Methods

    /// <summary>
    /// Reads the object data from the input.
    /// </summary>
    /// <param name="input">Savable reader</param>
    public override void Read(ISavableReader input)
    {
      IRenderSystem renderSystem = GraphicsHelper.GetRenderSystem(input.ServiceProvider);
      IBlendStateImpl impl = CreateImplementation(renderSystem);

      Name = input.ReadString("Name");
      impl.AlphaToCoverageEnable = input.ReadBoolean("AlphaToCoverageEnable");
      impl.IndependentBlendEnable = input.ReadBoolean("IndependentBlendEnable");
      impl.BlendFactor = input.Read<Color>("BlendFactor");
      impl.MultiSampleMask = input.ReadInt32("MultiSampleMask");

      int rtBlendCount = input.BeginReadGroup("RenderTargetBlendDescriptors");
      int supportedCount = impl.RenderTargetBlendCount;

      for (int i = 0; i < rtBlendCount; i++)
      {
        RenderTargetBlendDescription desc;
        input.Read<RenderTargetBlendDescription>("BlendDescriptor", out desc);

        if (i < supportedCount)
          impl.SetRenderTargetBlendDescription(i, ref desc);
      }

      input.EndReadGroup();

      impl.BindBlendState();
    }

    /// <summary>
    /// Writes the object data to the output.
    /// </summary>
    /// <param name="output">Savable writer</param>
    public override void Write(ISavableWriter output)
    {
      IBlendStateImpl impl = BlendStateImpl;

      output.Write("Name", Name);
      output.Write("AlphaToCoverageEnable", impl.AlphaToCoverageEnable);
      output.Write("IndependentBlendEnable", impl.IndependentBlendEnable);
      output.Write("BlendFactor", impl.BlendFactor);
      output.Write("MultiSampleMask", impl.MultiSampleMask);

      output.BeginWriteGroup("RenderTargetBlendDescriptors", impl.RenderTargetBlendCount);

      for (int i = 0; i < impl.RenderTargetBlendCount; i++)
      {
        RenderTargetBlendDescription desc;
        impl.GetRenderTargetBlendDescription(i, out desc);

        output.Write<RenderTargetBlendDescription>("BlendDescriptor", desc);
      }

      output.EndWriteGroup();
    }

    #endregion

    #region Implementation Creation and State Key Methods

    private IBlendStateImpl CreateImplementation(IRenderSystem renderSystem)
    {
      GraphicsHelper.ThrowIfRenderSystemNull(renderSystem);

      IBlendStateImplFactory factory;
      if (!renderSystem.TryGetImplementationFactory<IBlendStateImplFactory>(out factory))
        throw GraphicsExceptionHelper.NewUnsupportedFeatureException(renderSystem, typeof(BlendState));

      try
      {
        BlendStateImpl = factory.CreateImplementation();
      }
      catch (Exception e)
      {
        throw GraphicsExceptionHelper.NewErrorCreatingImplementation(typeof(BlendState), e);
      }

      return BlendStateImpl;
    }

    private void SetDefaults()
    {
      IBlendStateImpl impl = BlendStateImpl;

      impl.AlphaToCoverageEnable = false;
      impl.IndependentBlendEnable = false;
      impl.BlendFactor = Color.White;
      impl.MultiSampleMask = int.MaxValue;

      for (int i = 0; i < impl.RenderTargetBlendCount; i++)
      {
        RenderTargetBlendDescription desc = RenderTargetBlendDescription.Default;

        if (i == 0)
          desc.BlendEnable = true; //If creating a blend state, most likely want the first one enabled

        impl.SetRenderTargetBlendDescription(i, ref desc);
      }
    }

    private RenderStateKey ComputeRenderStateKey()
    {
      unchecked
      {
        IBlendStateImpl impl = BlendStateImpl;

        //Casting enums to ints to avoid boxing

        int hash = 17;

        hash = (hash * 31) + (int) StateType;
        hash = (hash * 31) + ((impl.AlphaToCoverageEnable) ? 1 : 0);
        hash = (hash * 31) + ((impl.IndependentBlendEnable) ? 1 : 0);
        hash = (hash * 31) + impl.BlendFactor.GetHashCode();
        hash = (hash * 31) + (impl.MultiSampleMask % int.MaxValue);

        for (int i = 0; i < impl.RenderTargetBlendCount; i++)
        {
          RenderTargetBlendDescription desc;
          impl.GetRenderTargetBlendDescription(i, out desc);

          if (!desc.IsDefault)
          {
            hash = (hash * 31) + i;
            hash = (hash * 31) + desc.GetHashCode();
          }
        }

        return new RenderStateKey(StateType, hash);
      }
    }

    #endregion
  }
}
