﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics;
using System.Drawing;
using Tesla.Content;
using Tesla.Graphics.Implementation;

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// Represents a Cube-Dimensional texture (6 faces) where each face is a square 2D texture with width/height.
  /// </summary>
  [SavableVersion(1)]
  public class TextureCube : Texture
  {
    private static readonly String[] s_cubemapFaceNames = new String[] { "PositiveX", "NegativeX", "PositiveY", "NegativeY", "PositiveZ", "NegativeZ" };

    /// <summary>
    /// Gets the number of faces in a cubemap, as defined in <see cref="CubeMapFace"/>.
    /// </summary>
    public const int CubeMapFaceCount = 6;

    #region Public Properties

    /// <inheritdoc />
    public override TextureDimension Dimension
    {
      get
      {
        return TextureDimension.Cube;
      }
    }

    /// <inheritdoc />
    public override ShaderResourceType ResourceType
    {
      get
      {
        return ShaderResourceType.TextureCube;
      }
    }

    /// <summary>
    /// Gets the texture size, in texels.
    /// </summary>
    public int Size
    {
      get
      {
        return TextureCubeImpl.Size;
      }
    }

    #endregion

    //Private property to cast the implementation
    private ITextureCubeImpl TextureCubeImpl
    {
      get
      {
        return GetImplAs<ITextureCubeImpl>();
      }
      set
      {
        BindImplementation(value);
      }
    }

    /// <summary>
    /// For ISavable only.
    /// </summary>
    protected TextureCube() { }

    /// <summary>
    /// Constructs a new instance of the <see cref="TextureCube"/> class.
    /// </summary>
    /// <param name="renderSystem">Render system used to create the underlying implementation.</param>
    /// <param name="size">Size (width/height) of the texture, in texels.</param>
    /// <param name="options">Common <see cref="TextureOptions"/> that may also contain initial data.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the render system is null or if any input is null when it is required (e.g. initial data is required for immutable resources).</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the size (width/height) is less than or equal to zero, or greater than the maximum texture size.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if creating the underlying implementation fails or is unsupported, see inner exception for a more detailed error.</exception>
    public TextureCube(IRenderSystem renderSystem, int size, TextureOptions options = default)
    {
      CreateImplementation(renderSystem, size, options);
    }

    #region Public Methods

    /// <summary>
    /// Reads data from the texture into the specified data buffer.
    /// </summary>
    /// <typeparam name="T">Type of data to read from the texture.</typeparam>
    /// <param name="data">Buffer to hold data copied from the texture.</param>
    /// <param name="face">Face index to access.</param>
    /// <param name="subimage">The subimage region, in texels, of the 2D texture to read from, if null the whole image is read from.</param>
    /// <exception cref="ObjectDisposedException">Thrown if the resource has already been disposed.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if an error occurs while reading from the resource.</exception>
    /// <remarks>See implementors for details on specific exceptions that can be thrown.</remarks>
    public void GetData<T>(Span<T> data, CubeMapFace face, ResourceRegion2D? subimage = null) where T : unmanaged
    {
      GetData<T>(data, SubResourceAt.Face(face), subimage);
    }

    /// <summary>
    /// Reads data from the texture into the specified buffer.
    /// </summary>
    /// <typeparam name="T">Type of data to read from the texture.</typeparam>
    /// <param name="data">Buffer to hold data copied from the texture.</param>
    /// <param name="subIndex">Optional index to identify which subresource (e.g. mipmap, face, cubemap array index) to access.</param>
    /// <param name="subimage">The subimage region, in texels, of the 2D texture to read from, if null the whole image is read from.</param>
    /// <exception cref="ObjectDisposedException">Thrown if the resource has already been disposed.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if an error occurs while reading from the resource.</exception>
    /// <remarks>See implementors for details on specific exceptions that can be thrown.</remarks>
    public void GetData<T>(Span<T> data, SubResourceAt subIndex = default(SubResourceAt), ResourceRegion2D? subimage = null) where T : unmanaged
    {
      CheckDisposed();

      try
      {
        TextureCubeImpl.GetData<T>(data, subIndex, subimage);
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw GraphicsExceptionHelper.NewErrorReadingFromResourceException(GetType(), e);
      }
    }

    /// <summary>
    /// Writes data to the texture from the specified buffer.
    /// </summary>
    /// <typeparam name="T">Type of data to write to the texture.</typeparam>
    /// <param name="renderContext">The current render context.</param>
    /// <param name="data">Data buffer that holds the contents that are to be copied to the texture.</param>
    /// <param name="face">Face index to access.</param>
    /// <param name="mipLevel">Zero-based index specifying the mip map level to access.</param>
    /// <param name="subimage">The subimage region, in texels, of the 2D texture to write to, if null the whole image is written to.</param>
    /// <param name="startIndex">Starting index in the data buffer to start reading from.</param>
    /// <exception cref="ObjectDisposedException">Thrown if the resource has already been disposed.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if an error occurs while writing to the resource, or if trying to write to a non-DYNAMIC resource 
    /// using a deferred render context.</exception>
    /// <remarks>See implementors for details on specific exceptions that can be thrown.</remarks>
    public void SetData<T>(IRenderContext renderContext, ReadOnlySpan<T> data, CubeMapFace face, ResourceRegion2D? subimage = null, DataWriteOptions writeOptions = DataWriteOptions.Discard) where T : unmanaged
    {
      SetData<T>(renderContext, data, SubResourceAt.Face(face), subimage, writeOptions);
    }

    /// <summary>
    /// Writes data to the texture from the specified buffer.
    /// </summary>
    /// <typeparam name="T">Type of data to write to the texture.</typeparam>
    /// <param name="renderContext">The current render context.</param>
    /// <param name="data">Buffer with the data to be copied to the texture.</param>
    /// <param name="subIndex">Index to identify which subresource (e.g. mipmap, face, cubemap array index) to access.</param>
    /// <param name="subimage">The subimage region, in texels, of the 2D texture to write to, if null the whole image is written to.</param>
    /// <param name="writeOptions">Optional writing options, valid only for dynamic textures.</param>
    /// <exception cref="ObjectDisposedException">Thrown if the resource has already been disposed.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if an error occurs while writing to the resource, or if trying to write to a non-DYNAMIC resource 
    /// using a deferred render context.</exception>
    /// <remarks>See implementors for details on specific exceptions that can be thrown.</remarks>
    public void SetData<T>(IRenderContext renderContext, ReadOnlySpan<T> data, SubResourceAt subIndex, ResourceRegion2D? subimage = null, DataWriteOptions writeOptions = DataWriteOptions.Discard) where T : unmanaged
    {
      CheckDisposed();
      CheckDeferredSetDataIsPermitted(renderContext, ResourceUsage);

      try
      {
        TextureCubeImpl.SetData<T>(renderContext, data, subIndex, subimage, writeOptions);
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw GraphicsExceptionHelper.NewErrorWritingToResourceException(GetType(), e);
      }
    }

    #endregion

    #region ISavable Methods

    /// <inheritdoc />
    public override void Read(ISavableReader input)
    {
      IRenderSystem renderSystem = GraphicsHelper.GetRenderSystem(input.ServiceProvider);

      Name = input.ReadString("Name");
      SurfaceFormat format = input.ReadEnum<SurfaceFormat>("Format");
      ResourceUsage usage = input.ReadEnum<ResourceUsage>("ResourceUsage");
      ResourceBindFlags bindFlags = input.ReadEnum<ResourceBindFlags>("ResourceBindFlags");

      int size = input.ReadInt32("Size");
      int expectedMipCount = input.ReadInt32("MipCount");
      Debug.Assert(expectedMipCount > 0);

      int faceCount = input.BeginReadGroup("Faces");
      Debug.Assert(faceCount == CubeMapFaceCount); //Faces must always be an array of 6

      // Pool buffers to initialize texture
      using (PooledArray<IReadOnlyDataBuffer?> data = new PooledArray<IReadOnlyDataBuffer?>(faceCount * expectedMipCount) { DisposeContents = true })
      {
        for (int i = 0; i < faceCount; i++)
        {
          CubeMapFace face = (CubeMapFace) i;
          int mipChainCount = input.BeginReadGroup("MipMapChain");
          Debug.Assert(expectedMipCount == mipChainCount);

          for (int j = 0; j < mipChainCount; j++)
          {
            SubResourceAt subIndex = SubResourceAt.MipFace(j, face);
            data[subIndex.CalculateSubResourceIndex(expectedMipCount)] = input.ReadBlob<byte>("MipMap", MemoryAllocatorStrategy.DefaultPooled);
          }

          input.EndReadGroup();
        }

        input.EndReadGroup();

        CreateImplementation(renderSystem, size, TextureOptions.Init(data, expectedMipCount > 1, format, usage, bindFlags));
      }
    }

    /// <inheritdoc />
    public override void Write(ISavableWriter output)
    {
      int mipCount = MipCount;
      int size = Size;
      SurfaceFormat format = Format;

      output.Write("Name", Name);
      output.WriteEnum<SurfaceFormat>("Format", format);
      output.WriteEnum<ResourceUsage>("ResourceUsage", ResourceUsage);
      output.WriteEnum<ResourceBindFlags>("ResourceBindFlags", ResourceBindFlags);
      output.Write("Size", size);
      output.Write("MipCount", mipCount);

      output.BeginWriteGroup("Faces", CubeMapFaceCount);

      // Create a pooled buffer for the largest surface and reuse it to gather and write out each surface
      using (DataBuffer<byte> byteBuffer = DataBuffer.Create<byte>(SubResourceAt.Zero.CalculateMipLevelSizeInBytes(size, size, format), MemoryAllocatorStrategy.DefaultPooled))
      {
        for (int i = 0; i < CubeMapFaceCount; i++)
        {
          output.BeginWriteGroup("MipMapChain", mipCount);

          CubeMapFace face = (CubeMapFace) i;
          for (int j = 0; j < mipCount; j++)
          {
            SubResourceAt subIndex = SubResourceAt.MipFace(j, face);
            int byteCount = subIndex.CalculateMipLevelSizeInBytes(size, size, format);
            Span<byte> span = byteBuffer.Span.Slice(0, byteCount);
            GetData<byte>(span, subIndex);

            output.WriteBlob<byte>("MipMap", span);
          }

          output.EndWriteGroup();
        }
      }

      output.EndWriteGroup();
    }

    #endregion

    #region Creation Parameter Validation

    /// <summary>
    /// Validates creation parameters.
    /// </summary>
    /// <param name="adapter">Graphics adapter from the render system.</param>
    /// <param name="size">Size of the texture (width/height), in texels.</param>
    /// <param name="arrayCount">Number of cubemaps in the array, must be greater than zero, specify 1 for non-array.</param>
    /// <param name="format">Format of the texture.</param>
    /// <exception cref="System.ArgumentOutOfRangeException">Thrown if the size (width/height) is less than or equal to zero or greater than the maximum texture size.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if the surface format is invalid for the texture dimension.</exception>
    protected void ValidateCreationParameters(IGraphicsAdapter adapter, ref int size, int arrayCount, SurfaceFormat format)
    {
      if (size <= 0 || size > adapter.MaximumTextureCubeSize)
        throw new ArgumentOutOfRangeException(nameof(size), StringLocalizer.Instance.GetLocalizedString("TextureDimensionOutOfRange"));

      if (arrayCount <= 0 || (CubeMapFaceCount * arrayCount) > adapter.MaximumTextureCubeArrayCount)
        throw new ArgumentOutOfRangeException(nameof(arrayCount), StringLocalizer.Instance.GetLocalizedString("TextureArrayCountOutOfRange"));

      if (!adapter.CheckTextureFormat(format, TextureDimension.Cube))
        throw GraphicsExceptionHelper.NewBadTextureFormatException(format, TextureDimension.Cube);

      CheckBlockCompressionMinimumSize(format, ref size);
    }

    /// <summary>
    /// Validates creation parameters.
    /// </summary>
    /// <param name="adapter">Graphics adapter from the render system.</param>
    /// <param name="size">Size of the texture (width/height), in texels.</param>
    /// <param name="arrayCount">Number of cubemaps in the array, must be greater than zero, specify 1 for non-array.</param>
    /// <param name="mipCount">Number of mip levels in the texture.</param>
    /// <param name="options">Common <see cref="TextureOptions"/> that may also contain initial data.</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the size (width/height) is less than or equal to zero, or greater than the maximum texture size, or if the length of the data 
    /// buffer array does not match the number of sub resources (6 * arrayCount * mipCount), or if any individual data buffer's size in bytes is larger than the corresponding mip level's size in bytes.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if the surface format is invalid for the texture dimension.</exception>
    protected void ValidateCreationParameters(IGraphicsAdapter adapter, ref int size, int arrayCount, int mipCount, TextureOptions options)
    {
      // TextureCube by definition is a 2D array, so disallow dynamic completely
      if (options.ResourceUsage == ResourceUsage.Dynamic)
        throw GraphicsExceptionHelper.NewDynamicResourceMustBeSingleSubresource();

      ValidateCreationParameters(adapter, ref size, arrayCount, options.Format);

      if (options.Data.IsEmpty)
      {
        if (options.ResourceUsage == ResourceUsage.Immutable)
          throw GraphicsExceptionHelper.NewMustSupplyDataForImmutableException();

        return;
      }

      // Validate buffers if span is non-empty
      if (options.Data.Length != (CubeMapFaceCount * arrayCount * mipCount))
        throw new ArgumentOutOfRangeException(nameof(options.Data), StringLocalizer.Instance.GetLocalizedString("DataArrayLengthNotEqualToSubresourceCount"));

      // Only check if span of buffers matches subresource dimensions if non-zero
      for (int cubeMapIndex = 0; cubeMapIndex < arrayCount; cubeMapIndex++)
      {
        for (int i = 0; i < CubeMapFaceCount; i++)
        {
          CubeMapFace face = (CubeMapFace) i;
          for (int mipLevel = 0; mipLevel < mipCount; mipLevel++)
          {
            SubResourceAt subIndex = SubResourceAt.MipFace(mipLevel, face, cubeMapIndex);
            int subresourceIndex = subIndex.CalculateSubResourceIndex(mipCount);
            int mipSizeInBytes = subIndex.CalculateMipLevelSizeInBytes(size, size, options.Format); //Accounts for compressed texture size

            IReadOnlyDataBuffer? db = options.Data[subresourceIndex];

            // If surface is not null, make sure it matches the expected byte size. Allow individual null surfaces even for immutable.
            if (db is not null && db.SizeInBytes != mipSizeInBytes)
              throw new ArgumentOutOfRangeException(nameof(options.Data), StringLocalizer.Instance.GetLocalizedString("DataSizeMustMatchMipLevelSize", mipLevel.ToString(), face.ToString()));
          }
        }
      }
    }

    #endregion

    #region Implementation Creation

    private void CreateImplementation(IRenderSystem renderSystem, int size, TextureOptions options)
    {
      GraphicsHelper.ThrowIfRenderSystemNull(renderSystem);

      int mipLevels = (options.WantMipMaps) ? CalculateMipMapCount(size, size) : 1;

      ValidateCreationParameters(renderSystem.Adapter, ref size, 1, mipLevels, options);

      ITextureCubeImplFactory factory;
      if (!renderSystem.TryGetImplementationFactory<ITextureCubeImplFactory>(out factory))
        throw GraphicsExceptionHelper.NewUnsupportedFeatureException(renderSystem, typeof(TextureCube));

      try
      {
        TextureCubeImpl = factory.CreateImplementation(size, mipLevels, options);
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw GraphicsExceptionHelper.NewErrorCreatingImplementation(typeof(TextureCube), e);
      }
    }

    #endregion
  }
}
