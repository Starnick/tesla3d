﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics;
using Tesla.Content;
using Tesla.Graphics.Implementation;

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// Represents a One-Dimensional texture resource that has width.
  /// </summary>
  [SavableVersion(1)]
  public class Texture1D : Texture
  {

    #region Public Properties

    /// <inheritdoc />
    public override TextureDimension Dimension
    {
      get
      {
        return TextureDimension.One;
      }
    }

    /// <inheritdoc />
    public override ShaderResourceType ResourceType
    {
      get
      {
        return ShaderResourceType.Texture1D;
      }
    }

    /// <summary>
    /// Gets the texture width, in texels.
    /// </summary>
    public int Width
    {
      get
      {
        return Texture1DImpl.Width;
      }
    }

    #endregion

    //Private property to cast the implementation
    private ITexture1DImpl Texture1DImpl
    {
      get
      {
        return GetImplAs<ITexture1DImpl>();
      }
      set
      {
        BindImplementation(value);
      }
    }

    /// <summary>
    /// For ISavable only.
    /// </summary>
    protected Texture1D() { }

    /// <summary>
    /// Constructs a new instance of the <see cref="Texture1D"/> class.
    /// </summary>
    /// <param name="renderSystem">Render system used to create the underlying implementation.</param>
    /// <param name="width">Width of the texture, in texels.</param>
    /// <param name="options">Common <see cref="TextureOptions"/> that may also contain initial data.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the render system is null or if any input is null when it is required (e.g. initial data is required for immutable resources).</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the width is less than or equal to zero, or greater than the maximum texture size.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if creating the underlying implementation fails or is unsupported, see inner exception for a more detailed error.</exception>
    public Texture1D(IRenderSystem renderSystem, int width, TextureOptions options = default)
    {
      CreateImplementation(renderSystem, width, options);
    }

    #region Public Methods

    /// <summary>
    /// Reads data from the texture into the specified buffer.
    /// </summary>
    /// <typeparam name="T">Type of data to read from the texture.</typeparam>
    /// <param name="data">Buffer to hold data copied from the texture.</param>
    /// <param name="subimage">Optional subimage region, in texels, of the 1D texture to read from, if null the whole image is read from.</param>
    /// <exception cref="ObjectDisposedException">Thrown if the resource has already been disposed.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if an error occurs while reading from the resource.</exception>
    /// <remarks>See implementors for details on specific exceptions that can be thrown.</remarks>
    public void GetData<T>(Span<T> data, ResourceRegion1D? subimage) where T : unmanaged
    {
      GetData<T>(data, SubResourceAt.Zero, subimage);
    }

    /// <summary>
    /// Reads data from the texture into the specified buffer.
    /// </summary>
    /// <typeparam name="T">Type of data to read from the texture.</typeparam>
    /// <param name="data">Buffer to hold data copied from the texture.</param>
    /// <param name="subIndex">Optional index to identify which subresource (e.g. mipmap, array slice) to access.</param>
    /// <param name="subimage">Optional subimage region, in texels, of the 1D texture to read from, if null the whole image is read from.</param>
    /// <exception cref="ObjectDisposedException">Thrown if the resource has already been disposed.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if an error occurs while reading from the resource.</exception>
    /// <remarks>See implementors for details on specific exceptions that can be thrown.</remarks>
    public void GetData<T>(Span<T> data, SubResourceAt subIndex = default, ResourceRegion1D? subimage = null) where T : unmanaged
    {
      CheckDisposed();

      try
      {
        Texture1DImpl.GetData<T>(data, subIndex, subimage);
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw GraphicsExceptionHelper.NewErrorWritingToResourceException(GetType(), e);
      }
    }

    /// <summary>
    /// Writes data to the texture from the specified buffer.
    /// </summary>
    /// <typeparam name="T">Type of data to write to the texture.</typeparam>
    /// <param name="renderContext">The current render context.</param>
    /// <param name="data">Buffer with the data to be copied to the texture.</param>
    /// <param name="writeOptions">Optional writing options, valid only for dynamic textures.</param>
    /// <exception cref="ObjectDisposedException">Thrown if the resource has already been disposed.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if an error occurs while writing to the resource, or if trying to write to a non-DYNAMIC resource 
    /// using a deferred render context.</exception>
    /// <remarks>See implementors for details on specific exceptions that can be thrown.</remarks>
    public void SetData<T>(IRenderContext renderContext, ReadOnlySpan<T> data, DataWriteOptions writeOptions = DataWriteOptions.Discard) where T : unmanaged
    {
      SetData<T>(renderContext, data, SubResourceAt.Zero, null, writeOptions);
    }

    /// <summary>
    /// Writes data to the texture from the specified buffer.
    /// </summary>
    /// <typeparam name="T">Type of data to write to the texture.</typeparam>
    /// <param name="renderContext">The current render context.</param> 
    /// <param name="data">Buffer with the data to be copied to the texture.</param>
    /// <param name="subimage">The subimage region, in texels, of the 1D texture to write to, if null the whole image is written to.</param>
    /// <param name="writeOptions">Optional writing options, valid only for dynamic textures.</param>
    /// <exception cref="ObjectDisposedException">Thrown if the resource has already been disposed.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if an error occurs while writing to the resource, or if trying to write to a non-DYNAMIC resource 
    /// using a deferred render context.</exception>
    /// <remarks>See implementors for details on specific exceptions that can be thrown.</remarks>
    public void SetData<T>(IRenderContext renderContext, ReadOnlySpan<T> data, ResourceRegion1D? subimage, DataWriteOptions writeOptions = DataWriteOptions.Discard) where T : unmanaged
    {
      SetData<T>(renderContext, data, SubResourceAt.Zero, subimage, writeOptions);
    }

    /// <summary>
    /// Writes data to the texture from the specified buffer.
    /// </summary>
    /// <typeparam name="T">Type of data to write to the texture.</typeparam>
    /// <param name="renderContext">The current render context.</param> 
    /// <param name="data">Buffer with the data to be copied to the texture.</param>
    /// <param name="subIndex">Optional index to identify which subresource (e.g. mipmap, array slice) to access.</param>
    /// <param name="subimage">The subimage region, in texels, of the 1D texture to write to, if null the whole image is written to.</param>
    /// <param name="writeOptions">Optional writing options, valid only for dynamic textures.</param>
    /// <exception cref="ObjectDisposedException">Thrown if the resource has already been disposed.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if an error occurs while writing to the resource, or if trying to write to a non-DYNAMIC resource 
    /// using a deferred render context.</exception>
    /// <remarks>See implementors for details on specific exceptions that can be thrown.</remarks>
    public void SetData<T>(IRenderContext renderContext, ReadOnlySpan<T> data, SubResourceAt subIndex, ResourceRegion1D? subimage = null, DataWriteOptions writeOptions = DataWriteOptions.Discard) where T : unmanaged
    {
      CheckDisposed();
      CheckDeferredSetDataIsPermitted(renderContext, ResourceUsage);

      try
      {
        Texture1DImpl.SetData<T>(renderContext, data, subIndex, subimage, writeOptions);
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw GraphicsExceptionHelper.NewErrorWritingToResourceException(GetType(), e);
      }
    }

    #endregion

    #region ISavable Methods

    /// <inheritdoc />
    public override void Read(ISavableReader input)
    {
      IRenderSystem renderSystem = GraphicsHelper.GetRenderSystem(input.ServiceProvider);

      Name = input.ReadString("Name");
      SurfaceFormat format = input.ReadEnum<SurfaceFormat>("Format");
      ResourceUsage usage = input.ReadEnum<ResourceUsage>("ResourceUsage");
      ResourceBindFlags bindFlags = input.ReadEnum<ResourceBindFlags>("ResourceBindFlags");
      int width = input.ReadInt32("Width");

      int mipCount = input.BeginReadGroup("MipMapChain");
      Debug.Assert(mipCount > 0);

      // Pool buffers to initialize texture
      using (PooledArray<IReadOnlyDataBuffer?> data = new PooledArray<IReadOnlyDataBuffer?>(mipCount) { DisposeContents = true })
      {
        for (int i = 0; i < mipCount; i++)
          data[i] = input.ReadBlob<byte>("MipMap", MemoryAllocatorStrategy.DefaultPooled);

        input.EndReadGroup();

        CreateImplementation(renderSystem, width, TextureOptions.Init(data, mipCount > 1, format, usage, bindFlags));
      }
    }

    /// <inheritdoc />
    public override void Write(ISavableWriter output)
    {
      int mipCount = MipCount;
      int width = Width;
      SurfaceFormat format = Format;

      output.Write("Name", Name);
      output.WriteEnum<SurfaceFormat>("Format", format);
      output.WriteEnum<ResourceUsage>("ResourceUsage", ResourceUsage);
      output.WriteEnum<ResourceBindFlags>("ResourceBindFlags", ResourceBindFlags);
      output.Write("Width", width);

      output.BeginWriteGroup("MipMapChain", mipCount);

      // Create a pooled buffer for the largest surface and reuse it to gather and write out each surface
      using (DataBuffer<byte> byteBuffer = DataBuffer.Create<byte>(SubResourceAt.Zero.CalculateMipLevelSizeInBytes(width, format), MemoryAllocatorStrategy.DefaultPooled))
      {
        for (int i = 0; i < mipCount; i++)
        {
          SubResourceAt subIndex = SubResourceAt.Mip(i);
          int byteCount = subIndex.CalculateMipLevelSizeInBytes(width, format);
          Span<byte> span = byteBuffer.Span.Slice(0, byteCount);
          GetData<byte>(span, subIndex);

          output.WriteBlob<byte>("MipMap", span);
        }
      }

      output.EndWriteGroup();
    }

    #endregion

    #region Creation Parameter Validation

    /// <summary>
    /// Validates creation parameters.
    /// </summary>
    /// <param name="adapter">Graphics adapter from the render system.</param>
    /// <param name="width">Width of the texture, in texels.</param>
    /// <param name="format">Format of the texture.</param>
    /// <param name="arrayCount">Number of array slices in the texture array, specify 1 for non-array.</param>
    /// <exception cref="System.ArgumentOutOfRangeException">Thrown if the width is less than or equal to zero or greater than the maximum texture size, or if the array count
    /// is less than or equal to zero or greater than the maximum array size.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if the surface format is invalid for the texture dimension.</exception>
    protected void ValidateCreationParameters(IGraphicsAdapter adapter, int width, SurfaceFormat format, int arrayCount = 1)
    {
      if (width <= 0 || width > adapter.MaximumTexture1DSize)
        throw new ArgumentOutOfRangeException(nameof(width), StringLocalizer.Instance.GetLocalizedString("TextureDimensionOutOfRange"));

      if (arrayCount <= 0 || arrayCount > adapter.MaximumTexture1DArrayCount)
        throw new ArgumentOutOfRangeException(nameof(arrayCount), StringLocalizer.Instance.GetLocalizedString("TextureArrayCountOutOfRange"));

      if (!adapter.CheckTextureFormat(format, TextureDimension.One))
        throw GraphicsExceptionHelper.NewBadTextureFormatException(format, TextureDimension.One);

      //No block compression check because those formats are not valid for 1D textures.
    }

    /// <summary>
    /// Validates creation parameters.
    /// </summary>
    /// <param name="adapter">Graphics adapter from the render system.</param>
    /// <param name="width">Width of the texture, in texels.</param>
    /// <param name="arrayCount">Number of array slices in the texture array, specify 1 for non-array.</param>
    /// <param name="mipCount">Number of mip levels in the texture.</param>
    /// <param name="options">Common <see cref="TextureOptions"/> that may also contain initial data.</param>
    /// <exception cref="TeslaGraphicsException">Thrown if the surface format is invalid for the texture dimension.</exception>
    protected void ValidateCreationParameters(IGraphicsAdapter adapter, int width, int arrayCount, int mipCount, TextureOptions options)
    {
      ValidateCommonCreationParameters(options, mipCount, arrayCount);
      ValidateCreationParameters(adapter, width, options.Format, arrayCount);
  
      if (options.Data.IsEmpty)
      {
        if (options.ResourceUsage == ResourceUsage.Immutable)
          throw GraphicsExceptionHelper.NewMustSupplyDataForImmutableException();

        return;
      }
      
      // Validate buffers if span is non-empty
      if (options.Data.Length != (mipCount * arrayCount))
        throw new ArgumentOutOfRangeException(nameof(options.Data), StringLocalizer.Instance.GetLocalizedString("DataArrayLengthNotEqualToSubresourceCount"));

      for (int slice = 0; slice < arrayCount; slice++)
      {
        for (int mipLevel = 0; mipLevel < mipCount; mipLevel++)
        {
          int subresourceIndex = CalculateSubResourceIndex(slice, mipLevel, mipCount);
          int mipSizeInBytes = CalculateMipLevelSizeInBytes(mipLevel, width, options.Format);

          IReadOnlyDataBuffer? db = options.Data[subresourceIndex];

          // If surface is not null, make sure it matches the expected byte size. Allow individual null surfaces even for immutable.
          if (db is not null && db.SizeInBytes != mipSizeInBytes)
            throw new ArgumentOutOfRangeException(nameof(options.Data), StringLocalizer.Instance.GetLocalizedString("DataSizeMustMatchMipLevelSize", mipLevel.ToString(), slice.ToString()));
        }
      }
    }

    #endregion

    #region Implementation Creation

    private void CreateImplementation(IRenderSystem renderSystem, int width, TextureOptions options)
    {
      GraphicsHelper.ThrowIfRenderSystemNull(renderSystem);

      int mipLevels = (options.WantMipMaps) ? CalculateMipMapCount(width) : 1;

      ValidateCreationParameters(renderSystem.Adapter, width, 1, mipLevels, options);

      ITexture1DImplFactory factory;
      if (!renderSystem.TryGetImplementationFactory<ITexture1DImplFactory>(out factory))
        throw GraphicsExceptionHelper.NewUnsupportedFeatureException(renderSystem, typeof(Texture1D));

      try
      {
        Texture1DImpl = factory.CreateImplementation(width, mipLevels, options);
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw GraphicsExceptionHelper.NewErrorCreatingImplementation(typeof(Texture1D), e);
      }
    }

    #endregion
  }
}
