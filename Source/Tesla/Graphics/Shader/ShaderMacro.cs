﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;
using Tesla.Content;

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// Represents a shader macro that defines a value that gets used during shader compilation.
  /// </summary>
  public struct ShaderMacro : INamable, IPrimitiveValue, IEquatable<ShaderMacro>
  {
    /// <summary>
    /// Gets or sets the name of the macro.
    /// </summary>
    public string Name { readonly get; set; }

    /// <summary>
    /// Gets or sets the macro definition.
    /// </summary>
    public string? Definition { readonly get; set; }

    /// <summary>
    /// Queries if the shader macro has a valid name and definition strings.
    /// </summary>
    public readonly bool IsValid { get { return !String.IsNullOrEmpty(Name); } }

    /// <summary>
    /// Constructs a new instance of the <see cref="ShaderMacro"/> class.
    /// </summary>
    /// <param name="name">Name of the macro.</param>
    /// <param name="definition">The macro value.</param>
    public ShaderMacro(string name, object? definition = null)
    {
      Name = name ?? String.Empty;
      Definition = definition?.ToString();
    }

    /// <inheritdoc />
    public readonly override string ToString()
    {
      if (Definition is not null)
        return $"Name = {Name}, Definition = {Definition}";
      else
        return $"Name = {Name}";
    }

    /// <inheritdoc />
    public readonly override int GetHashCode()
    {
      if (Definition is not null)
        return HashCode.Combine(Name, Definition);

      return Name.GetHashCode();
    }

    /// <inheritdoc />
    public readonly override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is ShaderMacro)
        return Equals((ShaderMacro) obj);

      return false;
    }

    /// <inheritdoc />
    public readonly bool Equals(ShaderMacro other)
    {
      return String.Equals(Name, other.Name) && String.Equals(Definition, other.Definition);
    }

    /// <summary>
    /// Writes the primitive data to the output.
    /// </summary>
    /// <param name="output">Primitive writer.</param>
    public readonly void Write(IPrimitiveWriter output)
    {
      output.Write("Name", Name);
      output.Write("Definition", Definition);
    }

    /// <summary>
    /// Reads the primitive data from the input.
    /// </summary>
    /// <param name="input">Primitive reader.</param>
    public void Read(IPrimitiveReader input)
    {
      Name = input.ReadStringOrDefault("Name");
      Definition = input.ReadStringOrDefault("Definition");
    }
  }
}
