﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.IO;
using Tesla.Content;

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// Interface that specifies how shader source code is loaded and how #include files are located when compiling shaders.
  /// </summary>
  public interface IShaderSourceHandler : IDisposable
  {
    /// <summary>
    /// User implemented method for closing the #include file stream.
    /// </summary>
    /// <param name="stream">Stream that was opened</param>
    void CloseInclude(Stream stream);

    /// <summary>
    /// User implemented method for opening and reading the contents of a shader #include file.
    /// </summary>
    /// <param name="fileName">Name of the #include file</param>
    /// <param name="includeType">Determines the location of the #include file</param>
    /// <returns>Stream to the file data.</returns>
    Stream OpenInclude(string fileName, IncludeType includeType);

    /// <summary>
    /// Opens the specified file and retrieves the shader source code from it.
    /// </summary>
    /// <param name="fileName">Relative path to the file.</param>
    /// <param name="includeType">Optional include type, if any.</param>
    /// <returns>Uncompiled shader source code.</returns>
    string GetSourceCode(string fileName, IncludeType? includeType = null);
  }

  /// <summary>
  /// Represents a shader compiler that takes a shader manifest format and compiles the shaders defined in it
  /// into a structure that can be loaded at runtime or saved for later.
  /// </summary>
  public interface IShaderCompiler : IDisposable
  {
    /// <summary>
    /// Compiles a set of shaders defined in a manifest format.
    /// </summary>
    /// <param name="input">Manifest input that describes all the shader fragments and options.</param>
    /// <param name="compileFlags">Compilation flags.</param>
    /// <param name="shaderSrcHandler">Include handler for resolving shader source code and #include directives.</param>
    /// <returns>A result with the compiled haders or a list of compile errors.</returns>
    ShaderCompileResult Compile(ShaderManifest input, ShaderCompileFlags compileFlags, IShaderSourceHandler shaderSrcHandler);

    /// <summary>
    /// Compiles a set of shaders from a manifest format from a file.
    /// </summary>
    /// <param name="manifestFile">File handle to a manifest file.</param>
    /// <param name="compileFlags">Compilation flags.</param>
    /// <param name="shaderSrcHandler">Optional include handler for resolving shader source code #include directives in shader code, if not specified all includes are handled relatively to the specified file.</param>
    /// <returns>A result with the compiled haders or a list of compile errors.</returns>
    ShaderCompileResult CompileFromFile(IResourceFile manifestFile, ShaderCompileFlags compileFlags, IShaderSourceHandler? shaderSrcHandler = null);
  }
}
