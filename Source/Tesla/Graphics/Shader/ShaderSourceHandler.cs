﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.IO;
using Tesla.Content;

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// Generic shader source handler that locates source and #include files based on a resource file or resource repository.
  /// </summary>
  public class ShaderSourceHandler : IShaderSourceHandler
  {
    private object m_sync = new object();

    private IResourceRepository m_repo;
    private IResourceFile? m_file;
    private bool m_ownsRepo;

    private Dictionary<string, LoadedShaderFile> m_loadedLocalIncludes;
    private Dictionary<string, LoadedShaderFile> m_loadedSystemIncludes;
    private Dictionary<string, LoadedShaderFile> m_loadedSources;

    /// <summary>
    /// Gets the optional relative file handle.
    /// </summary>
    public IResourceFile? RelativeFile { get { return m_file; } }

    /// <summary>
    /// Gets the resource repository that files will be located from.
    /// </summary>
    public IResourceRepository Repository { get { return m_repo; } }

    /// <summary>
    /// Constructs a new instance of the <see cref="ShaderSourceHandler"/> class.
    /// </summary>
    /// <param name="rootFile">Resource file that all local includes are relative to.</param>
    /// <exception cref="ArgumentNullException">Thrown if the repository is null.</exception>
    /// <exception cref="TeslaContentException">Thrown if the repository is not opened.</exception>
    public ShaderSourceHandler(IResourceFile rootFile)
      : this(rootFile?.Repository!)
    {
      TeslaContentException.ThrowIfDoesNotExist(rootFile, nameof(rootFile));

      m_file = rootFile;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="ShaderSourceHandler"/> class.
    /// </summary>
    /// <param name="rootFolder">Rooted or non-rooted path to the folder to which all local includes are relative to. If not rooted, the application directory.</param>
    /// <exception cref="TeslaContentException">Thrown if the path is invalid.</exception>
    public ShaderSourceHandler(string rootFolder)
      : this(FileResourceRepository.CreateOpen(rootFolder), true) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="ShaderSourceHandler"/> class.
    /// </summary>
    /// <param name="rootFolder">Resource repository representing the root folder to locate local includes.</param>
    /// <exception cref="ArgumentNullException">Thrown if the repository is null.</exception>
    /// <exception cref="TeslaContentException">Thrown if the repository is not opened.</exception>
    public ShaderSourceHandler(IResourceRepository rootFolder, bool ownsRepository = false)
    {
      ArgumentNullException.ThrowIfNull(rootFolder, nameof(rootFolder));
      TeslaContentException.ThrowIfNotOpened(rootFolder, nameof(rootFolder));

      m_repo = rootFolder;
      m_ownsRepo = ownsRepository;
      m_loadedLocalIncludes = new Dictionary<string, LoadedShaderFile>();
      m_loadedSystemIncludes = new Dictionary<string, LoadedShaderFile>();
      m_loadedSources = new Dictionary<string, LoadedShaderFile>();
    }

    /// <inheritdoc />
    public void Dispose()
    {
      if (m_ownsRepo)
        m_repo.CloseConnection();
    }

    /// <inheritdoc />
    public void CloseInclude(Stream stream)
    {
      stream.Close();
    }

    /// <inheritdoc />
    /// <exception cref="TeslaContentException">Thrown if the file could not be found.</exception>
    public Stream OpenInclude(string fileName, IncludeType includeType)
    {
      LoadedShaderFile file = GetOrLoadSource(fileName, includeType);
      return new MemoryStream(file.Data);
    }

    /// <inheritdoc />
    /// <exception cref="TeslaContentException">Thrown if the file could not be found.</exception>
    public string GetSourceCode(string fileName, IncludeType? includeType = null)
    {
      LoadedShaderFile file = GetOrLoadSource(fileName, includeType);
      return file.SourceCode;
    }

    /// <summary>
    /// Allows special handling if the include type is system (e.g. search in other directories).
    /// </summary>
    /// <param name="fileName">Name of the #include file</param>
    /// <returns>Handle to the file data.</returns>
    protected virtual IResourceFile LocateSystemInclude(string fileName)
    {
      // By default, do nothing extra
      return GetResourceFile(fileName);
    }

    /// <summary>
    /// Gets the resource file handle to the file.
    /// </summary>
    /// <param name="fileName">File name to locate.</param>
    /// <returns>Handle to the file data.</returns>
    protected IResourceFile GetResourceFile(string fileName)
    {
      IResourceFile? file;
      if (m_file is not null)
        file = m_repo.GetResourceFileRelativeTo(fileName, m_file);
      else
        file = m_repo.GetResourceFile(fileName);

      TeslaContentException.ThrowIfDoesNotExist(file, nameof(fileName));

      return file;
    }

    private LoadedShaderFile GetOrLoadSource(string fileName, IncludeType? includeType)
    {
      lock (m_sync)
      {
        Dictionary<string, LoadedShaderFile> loaded = GetLoadedFileCache(includeType);
        if (loaded.TryGetValue(fileName, out LoadedShaderFile? loadedFile))
          return loadedFile;

        IResourceFile file;

        if (!includeType.HasValue)
          file = GetResourceFile(fileName);
        else
          file = (includeType == IncludeType.System) ? LocateSystemInclude(fileName) : GetResourceFile(fileName);

        loadedFile = LoadedShaderFile.Load(file);
        loaded.Add(fileName, loadedFile);

        return loadedFile;
      }
    }

    private Dictionary<string, LoadedShaderFile> GetLoadedFileCache(IncludeType? includeType)
    {
      if (!includeType.HasValue)
        return m_loadedSources;

      if (includeType == IncludeType.System)
        return m_loadedSystemIncludes;

      return m_loadedLocalIncludes;
    }

    // Helper for holding onto both the raw file contents and string text
    private sealed class LoadedShaderFile
    {
      public readonly byte[] Data;

      private string? m_sourceCode;

      public string SourceCode
      {
        get
        {
          if (m_sourceCode is not null)
            return m_sourceCode;

          using StreamReader reader = new StreamReader(new MemoryStream(Data));
          m_sourceCode = reader.ReadToEnd();
          return m_sourceCode;
        }
      }

      public LoadedShaderFile(byte[] data)
      {
        Data = data;
      }

      public static LoadedShaderFile Load(IResourceFile file)
      {
        using Stream str = file.OpenRead();
        byte[] data = BufferHelper.ReadStreamFully(str);

        return new LoadedShaderFile(data);
      }
    }
  }
}
