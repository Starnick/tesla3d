﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics;
using System.IO;
using Tesla.Content;

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// Container for compiled shaders and the metadata describing their layout and what resources are used, allowing for
  /// resource sharing.
  /// </summary>
  [SavableVersion(1)]
  [DebuggerDisplay("Name = {Name}, ConstantBuffers = {ConstantBuffers.Length}, ResourceVariables = {ResourceVariables.Length}, Shaders = {Shaders.Length}, ShaderGroups= {ShaderGroups.Length}")]
  public class ShaderData : ISavable
  {
    public string Name = String.Empty;
    public ShaderByteCodeFormat Format;
    public string[] FamilyNames = Array.Empty<string>();
    public string[] ParameterNamespaces = Array.Empty<string>();
    public string[] ComputedParameterNames = Array.Empty<string>();
    public string[] FeatureNames = Array.Empty<string>();
    public ConstantBuffer[] ConstantBuffers = Array.Empty<ConstantBuffer>();
    public ResourceVariable[] ResourceVariables = Array.Empty<ResourceVariable>();
    public Shader[] Shaders = Array.Empty<Shader>();
    public ShaderGroup[] ShaderGroups = Array.Empty<ShaderGroup>();

    public void Read(ISavableReader input)
    {
      Name = input.ReadStringOrDefault("Name");
      Format = input.ReadEnum<ShaderByteCodeFormat>("Format");
      FamilyNames = input.ReadStringArrayOrDefault("FamilyNames");
      ParameterNamespaces = input.ReadStringArrayOrDefault("ParameterNamespaces");
      ComputedParameterNames = input.ReadStringArrayOrDefault("ComputedParameterNames");
      FeatureNames = input.ReadStringArrayOrDefault("FeatureNames");

      int cbCount = input.BeginReadGroup("ConstantBuffers");
      if (cbCount > 0)
      {
        ConstantBuffers = new ConstantBuffer[cbCount];
        for (int i = 0; i < cbCount; i++)
          ConstantBuffers[i] = ConstantBuffer.Read(input);
      }
      input.EndReadGroup();

      int rVarCount = input.BeginReadGroup("ResourceVariables");
      if (rVarCount > 0)
      {
        ResourceVariables = new ResourceVariable[rVarCount];
        for (int i = 0; i < rVarCount; i++)
          ResourceVariables[i] = ResourceVariable.Read(input);
      }
      input.EndReadGroup();

      int shCount = input.BeginReadGroup("Shaders");
      if (shCount > 0)
      {
        Shaders = new Shader[shCount];
        for (int i = 0; i < shCount; i++)
          Shaders[i] = Shader.Read(input);
      }
      input.EndReadGroup();

      int grpCount = input.BeginReadGroup("ShaderGroups");
      if (grpCount > 0)
      {
        ShaderGroups = new ShaderGroup[grpCount];
        for (int i = 0; i < grpCount; i++)
          ShaderGroups[i] = ShaderGroup.Read(input);
      }
      input.EndReadGroup();
    }

    public void Write(ISavableWriter output)
    {
      output.Write("Name", Name);
      output.WriteEnum<ShaderByteCodeFormat>("Format", Format);
      output.Write("FamilyNames", FamilyNames);
      output.Write("ParameterNamespaces", ParameterNamespaces);
      output.Write("ComputedParameterNames", ComputedParameterNames);
      output.Write("FeatureNames", FeatureNames);

      output.BeginWriteGroup("ConstantBuffers", ConstantBuffers.Length);
      foreach (ConstantBuffer cb in ConstantBuffers)
        ConstantBuffer.Write(output, cb);
      output.EndWriteGroup();

      output.BeginWriteGroup("ResourceVariables", ResourceVariables.Length);
      foreach (ResourceVariable v in ResourceVariables)
        ResourceVariable.Write(output, v);
      output.EndWriteGroup();

      output.BeginWriteGroup("Shaders", Shaders.Length);
      foreach (Shader sh in Shaders)
        Shader.Write(output, sh);
      output.EndWriteGroup();

      output.BeginWriteGroup("ShaderGroups", ShaderGroups.Length);
      foreach (ShaderGroup grp in ShaderGroups)
        ShaderGroup.Write(output, grp);
      output.EndWriteGroup();
    }

    public void Dump(TextWriter output)
    {
      output.WriteLine("TEFX Text Dump");
      output.WriteLine();

      DumpText(output, String.Empty, "Version", SavableVersionCache.GetVersion(GetType()));
      DumpText(output, String.Empty, nameof(Name), Name);
      DumpText(output, String.Empty, nameof(Format), Format);
      DumpTextArray<string>(output, String.Empty, nameof(FamilyNames), FamilyNames);
      DumpTextArray<string>(output, String.Empty, nameof(ParameterNamespaces), ParameterNamespaces);
      DumpTextArray<string>(output, String.Empty, nameof(ComputedParameterNames), ComputedParameterNames);
      DumpTextArray<string>(output, String.Empty, nameof(FeatureNames), FeatureNames);

      output.WriteLine();
      output.WriteLine("===================================================================");
      output.WriteLine("Variable Section");
      output.WriteLine();

      string nextPrefix = "\t";

      DumpText(output, String.Empty, "# Constant Buffers", ConstantBuffers.Length);
      output.WriteLine();

      foreach (ConstantBuffer cb in ConstantBuffers)
      {
        cb.Dump(output, nextPrefix);
        output.WriteLine();
      }

      DumpText(output, String.Empty, "# Resource Variables", ResourceVariables.Length);
      output.WriteLine();

      foreach (ResourceVariable v in ResourceVariables)
      {
        v.Dump(output, nextPrefix);
        output.WriteLine();
      }

      output.WriteLine();
      output.WriteLine("===================================================================");
      output.WriteLine("Shader Section");
      output.WriteLine();

      DumpText(output, String.Empty, "# Shaders", Shaders.Length);
      output.WriteLine();

      foreach (Shader sh in Shaders)
      {
        sh.Dump(output, nextPrefix);
        output.WriteLine();
      }

      output.WriteLine("===================================================================");
      output.WriteLine("ShaderGroups Section");
      output.WriteLine();

      DumpText(output, String.Empty, "# ShaderGroups", ShaderGroups.Length);
      output.WriteLine();

      foreach (ShaderGroup grp in ShaderGroups)
      {
        grp.Dump(output, nextPrefix);
        output.WriteLine();
      }

      output.Flush();
    }

    public static ShaderData? Read(Stream input)
    {
      if (input is null || !input.CanRead)
        return null;

      using (BinarySavableReader reader = new BinarySavableReader(null, input, true))
        return reader.ReadSavable<ShaderData>("Object");
    }

    public static bool Write(ShaderData data, Stream output, bool compressData = false)
    {
      if (data is null || output is null || !output.CanWrite)
        return false;

      SavableWriteFlags writeFlags = (compressData) ? SavableWriteFlags.UseCompression : SavableWriteFlags.None;
      using (BinarySavableWriter writer = new BinarySavableWriter(output, writeFlags, true))
        writer.WriteSavable<ShaderData>("Object", data);

      return true;
    }

    #region Nested types

    /// <summary>
    /// Represents all the shaders that should be assigned to the rendering pipeline to render an object. E.g. vertex/pixel
    /// shaders.
    /// </summary>
    [DebuggerDisplay("Name = {Name}")]
    public sealed class ShaderGroup
    {
      /// <summary>
      /// Name of the shader group, should be unique among all in the <see cref="ShaderData"/>.
      /// </summary>
      public string Name = String.Empty;
      public uint FamilyNameIndex;
      public uint ParameterNamespaceIndex;
      public uint[] FeatureIndices = Array.Empty<uint>();
      public uint[] ComputedParameterIndices = Array.Empty<uint>();
      public uint[] ShaderIndices = Array.Empty<uint>();

      internal static ShaderGroup Read(ISavableReader input, string name = "ShaderGroup")
      {
        ShaderGroup grp = new ShaderGroup();

        input.BeginReadGroup(name);

        grp.Name = input.ReadStringOrDefault("Name");
        grp.FamilyNameIndex = input.ReadUInt32("FamilyNameIndex");
        grp.ParameterNamespaceIndex = input.ReadUInt32("ParameterNamespaceIndex");
        grp.FeatureIndices = input.ReadUInt32Array("FeatureIndices");
        grp.ComputedParameterIndices = input.ReadUInt32Array("ComputedParameterIndices");
        grp.ShaderIndices = input.ReadUInt32Array("ShaderIndices");

        input.EndReadGroup();

        return grp;
      }

      internal static void Write(ISavableWriter output, ShaderGroup grp, string name = "ShaderGroup")
      {
        output.BeginWriteGroup(name);

        output.Write("Name", grp.Name);
        output.Write("FamilyNameIndex", grp.FamilyNameIndex);
        output.Write("ParameterNamespaceIndex", grp.ParameterNamespaceIndex);
        output.Write("FeatureIndices", grp.FeatureIndices);
        output.Write("ComputedParameterIndices", grp.ComputedParameterIndices);
        output.Write("ShaderIndices", grp.ShaderIndices);

        output.EndWriteGroup();
      }

      internal void Dump(TextWriter output, string linePrefix)
      {
        DumpText(output, linePrefix, nameof(Name), Name);
        DumpText(output, linePrefix, nameof(FamilyNameIndex), FamilyNameIndex);
        DumpText(output, linePrefix, nameof(ParameterNamespaceIndex), ParameterNamespaceIndex);
        DumpTextArray<uint>(output, linePrefix, nameof(FeatureIndices), FeatureIndices);
        DumpTextArray<uint>(output, linePrefix, nameof(ComputedParameterIndices), ComputedParameterIndices);
        DumpTextArray<uint>(output, linePrefix, nameof(ShaderIndices), ShaderIndices);
      }
    }

    /// <summary>
    /// Represents a group of <see cref="ValueVariable"/> shader variables, where they are packed inside a single buffer that is uploaded
    /// to the GPU.
    /// </summary>
    [DebuggerDisplay("Name = {Name}, SizeInBytes = {SizeInBytes}, VariableCount = {Variables.Length}")]
    public sealed class ConstantBuffer
    {
      /// <summary>
      /// Name of the constant buffer. Should be unique among all constant buffers in a <see cref="ShaderData"/>.
      /// </summary>
      public string Name = String.Empty;
      public uint SizeInBytes;
      public ShaderVariableFlags Flags;
      public ConstantBufferType BufferType;
      public bool IsUpdatedPerFrame;
      public ValueVariable[] Variables = Array.Empty<ValueVariable>();

      internal static ConstantBuffer Read(ISavableReader input, string name = "ConstantBuffer")
      {
        ConstantBuffer cb = new ConstantBuffer();

        input.BeginReadGroup(name);

        cb.Name = input.ReadStringOrDefault("Name");
        cb.SizeInBytes = input.ReadUInt32("SizeInBytes");
        cb.Flags = input.ReadEnum<ShaderVariableFlags>("Flags");
        cb.BufferType = input.ReadEnum<ConstantBufferType>("BufferType");
        cb.IsUpdatedPerFrame = input.ReadBoolean("IsUpdatedPerFrame");

        int variableCount = input.BeginReadGroup("Variables");
        if (variableCount > 0)
        {
          cb.Variables = new ValueVariable[variableCount];
          for (int i = 0; i < variableCount; i++)
            cb.Variables[i] = ValueVariable.Read(input);

        }
        input.EndReadGroup();

        input.EndReadGroup();

        return cb;
      }

      internal static void Write(ISavableWriter output, ConstantBuffer cb, string name = "ConstantBuffer")
      {
        output.BeginWriteGroup(name);

        output.Write("Name", cb.Name);
        output.Write("SizeInBytes", cb.SizeInBytes);
        output.WriteEnum<ShaderVariableFlags>("Flags", cb.Flags);
        output.WriteEnum<ConstantBufferType>("BufferType", cb.BufferType);
        output.Write("IsUpdatedPerFrame", cb.IsUpdatedPerFrame);

        output.BeginWriteGroup("Variables", cb.Variables.Length);
        for (int i = 0; i < cb.Variables.Length; i++)
          ValueVariable.Write(output, cb.Variables[i]);
        output.EndWriteGroup();

        output.EndWriteGroup();
      }

      internal void Dump(TextWriter output, string linePrefix)
      {
        DumpText(output, linePrefix, nameof(Name), Name);
        DumpText(output, linePrefix, nameof(SizeInBytes), SizeInBytes);
        DumpText(output, linePrefix, nameof(Flags), Flags);
        DumpText(output, linePrefix, nameof(BufferType), BufferType);
        DumpText(output, linePrefix, nameof(IsUpdatedPerFrame), IsUpdatedPerFrame);

        string nextPrefix = linePrefix + "\t";

        output.WriteLine();
        DumpText(output, linePrefix, "# Variables", Variables.Length);
        output.WriteLine();

        for (int i = 0; i < Variables.Length; i++)
        {
          Variables[i].Dump(output, nextPrefix);

          if (i < (Variables.Length - 1))
            output.WriteLine();
        }
      }
    }

    /// <summary>
    /// Represents a shader variable that takes in an object input (buffer, texture, or sampler).
    /// </summary>
    [DebuggerDisplay("Name = {Name}, ResourceType = {ResourceType}")]
    public sealed class ResourceVariable
    {
      /// <summary>
      /// Name of the resource variable. Should be unique among all resource and value variables within the <see cref="ShaderData"/>.
      /// </summary>
      public string Name = String.Empty;
      public uint? ComputedParameterNameIndex;
      public uint? FeatureNameIndex;
      public ShaderInputType ResourceType;
      public ResourceDimension ResourceDimension;
      public ResourceReturnType ReturnType;
      public ShaderInputFlags InputFlags;
      public uint SampleCount;
      public uint ElementCount;

      /// <summary>
      /// If <see cref="IsSampler"/> is true, this will contain the declared initial state of the sampler from the shader source.
      /// </summary>
      public SamplerStateData? SamplerData;

      /// <summary>
      /// If <see cref="IsStructuredBuffer"/> is true, this will contain a layout representing each element. Otherwise, null.
      /// </summary>
      public ValueVariable? StructuredBufferLayout;

      /// <summary>
      /// Gets whether or not this resource is a sampler state.
      /// </summary>
      public bool IsSampler
      {
        get
        {
          return ResourceType == ShaderInputType.Sampler;
        }
      }

      /// <summary>
      /// Gets whether or not this resource is a structure buffer.
      /// </summary>
      public bool IsStructuredBuffer
      {
        get
        {
          return ResourceType == ShaderInputType.Structured || ResourceType == ShaderInputType.RWStructured ||
            ResourceType == ShaderInputType.AppendStructured || ResourceType == ShaderInputType.ConsumeStructured ||
            ResourceType == ShaderInputType.RWStructuredWithCounter;
        }
      }

      internal static ResourceVariable Read(ISavableReader input, string name = "ResourceVariable")
      {
        ResourceVariable v = new ResourceVariable();

        input.BeginReadGroup(name);

        v.Name = input.ReadStringOrDefault("Name");
        v.ComputedParameterNameIndex = input.ReadNullableUInt32("ComputedParameterIndex");
        v.FeatureNameIndex = input.ReadNullableUInt32("FeatureIndex");
        v.ResourceType = input.ReadEnum<ShaderInputType>("ResourceType");
        v.ResourceDimension = input.ReadEnum<ResourceDimension>("ResourceDimension");
        v.ReturnType = input.ReadEnum<ResourceReturnType>("ReturnType");
        v.InputFlags = input.ReadEnum<ShaderInputFlags>("InputFlags");
        v.SampleCount = input.ReadUInt32("SampleCount");
        v.ElementCount = input.ReadUInt32("ElementCount");

        if (v.IsSampler)
          v.SamplerData = input.ReadNullable<SamplerStateData>("SamplerData");
        else if (v.IsStructuredBuffer)
          v.StructuredBufferLayout = ValueVariable.Read(input, "StructuredBufferLayout");

        input.EndReadGroup();

        return v;
      }

      internal static void Write(ISavableWriter output, ResourceVariable v, string name = "ResourceVariable")
      {
        output.BeginWriteGroup(name);

        output.Write("Name", v.Name);
        output.Write("ComputedParameterIndex", v.ComputedParameterNameIndex);
        output.Write("FeatureIndex", v.FeatureNameIndex);
        output.WriteEnum<ShaderInputType>("ResourceType", v.ResourceType);
        output.WriteEnum<ResourceDimension>("ResourceDimension", v.ResourceDimension);
        output.WriteEnum<ResourceReturnType>("ReturnType", v.ReturnType);
        output.WriteEnum<ShaderInputFlags>("InputFlags", v.InputFlags);
        output.Write("SampleCount", v.SampleCount);
        output.Write("ElementCount", v.ElementCount);

        if (v.IsSampler)
          output.Write<SamplerStateData>("SamplerData", v.SamplerData);
        else if (v.IsStructuredBuffer)
          ValueVariable.Write(output, v.StructuredBufferLayout ?? new ValueVariable(), "StructuredBufferLayout");

        output.EndWriteGroup();
      }

      internal void Dump(TextWriter output, string linePrefix)
      {
        DumpText(output, linePrefix, nameof(Name), Name);

        if (ComputedParameterNameIndex.HasValue)
          DumpText(output, linePrefix, nameof(ComputedParameterNameIndex), ComputedParameterNameIndex.Value);

        if (FeatureNameIndex.HasValue)
          DumpText(output, linePrefix, nameof(FeatureNameIndex), FeatureNameIndex.Value);

        DumpText(output, linePrefix, nameof(ResourceType), ResourceType);
        DumpText(output, linePrefix, nameof(ResourceDimension), ResourceDimension);
        DumpText(output, linePrefix, nameof(ReturnType), ReturnType);
        DumpText(output, linePrefix, nameof(InputFlags), InputFlags);
        DumpText(output, linePrefix, nameof(SampleCount), SampleCount);
        DumpText(output, linePrefix, nameof(ElementCount), ElementCount);
        DumpText(output, linePrefix, nameof(IsSampler), IsSampler);
        DumpText(output, linePrefix, nameof(IsStructuredBuffer), IsStructuredBuffer);

        string nextPrefix = linePrefix + "\t";

        if (IsSampler && SamplerData.HasValue)
        {
          DumpText(output, linePrefix, nameof(SamplerData), String.Empty);
          SamplerData.Value.Dump(output, nextPrefix);
        }

        if (IsStructuredBuffer && StructuredBufferLayout is not null)
        {
          DumpText(output, linePrefix, nameof(StructuredBufferLayout), String.Empty);
          StructuredBufferLayout.Dump(output, nextPrefix);
        }
      }
    }

    /// <summary>
    /// Represents a member of a <see cref="ValueVariable"/>, if the shader variable is a struct.
    /// </summary>
    [DebuggerDisplay("Name = {Name}, Class = {VariableClass}, Type = {VariableType}, ElementCount = {ElementCount}, MemberCount = {Members.Length}")]
    public sealed class ValueVariableMember
    {
      public string Name = String.Empty;
      public ShaderVariableClass VariableClass;
      public ShaderVariableType VariableType;
      public uint ColumnCount;
      public uint RowCount;
      public uint ElementCount;
      public uint OffsetFromParentStructure;
      public uint SizeInBytes;
      public uint AlignedElementSizeInBytes;
      public ValueVariableMember[] Members = Array.Empty<ValueVariableMember>();

      internal static ValueVariableMember Read(ISavableReader input, string name = "Member")
      {
        ValueVariableMember v = new ValueVariableMember();

        input.BeginReadGroup(name);

        v.Name = input.ReadStringOrDefault("Name");
        v.VariableClass = input.ReadEnum<ShaderVariableClass>("VariableClass");
        v.VariableType = input.ReadEnum<ShaderVariableType>("VariableType");
        v.ColumnCount = input.ReadUInt32("ColumnCount");
        v.RowCount = input.ReadUInt32("RowCount");
        v.ElementCount = input.ReadUInt32("ElementCount");
        v.OffsetFromParentStructure = input.ReadUInt32("OffsetFromParentStructure");
        v.SizeInBytes = input.ReadUInt32("SizeInBytes");
        v.AlignedElementSizeInBytes = input.ReadUInt32("AlignedElementSizeInBytes");

        int memberCount = input.BeginReadGroup("Members");
        if (memberCount > 0)
        {
          v.Members = new ValueVariableMember[memberCount];
          for (int i = 0; i < memberCount; i++)
            v.Members[i] = ValueVariableMember.Read(input);
        }

        input.EndReadGroup();

        input.EndReadGroup();

        return v;
      }

      internal static void Write(ISavableWriter output, ValueVariableMember v, string name = "Member")
      {
        output.BeginWriteGroup(name);

        output.Write("Name", v.Name);
        output.WriteEnum<ShaderVariableClass>("VariableClass", v.VariableClass);
        output.WriteEnum<ShaderVariableType>("VariableType", v.VariableType);
        output.Write("ColumnCount", v.ColumnCount);
        output.Write("RowCount", v.RowCount);
        output.Write("ElementCount", v.ElementCount);
        output.Write("OffsetFromParentStructure", v.OffsetFromParentStructure);
        output.Write("SizeInBytes", v.SizeInBytes);
        output.Write("AlignedElementSizeInBytes", v.AlignedElementSizeInBytes);

        output.BeginWriteGroup("Members", v.Members.Length);
        foreach (ValueVariableMember m in v.Members)
          ValueVariableMember.Write(output, m);
        output.EndWriteGroup();

        output.EndWriteGroup();
      }

      internal void Dump(TextWriter output, string linePrefix)
      {
        DumpText(output, linePrefix, nameof(Name), Name);
        DumpText(output, linePrefix, nameof(VariableClass), VariableClass);
        DumpText(output, linePrefix, nameof(VariableType), VariableType);
        DumpText(output, linePrefix, nameof(ColumnCount), ColumnCount);
        DumpText(output, linePrefix, nameof(RowCount), RowCount);
        DumpText(output, linePrefix, nameof(ElementCount), ElementCount);
        DumpText(output, linePrefix, nameof(OffsetFromParentStructure), OffsetFromParentStructure);
        DumpText(output, linePrefix, nameof(SizeInBytes), SizeInBytes);
        DumpText(output, linePrefix, nameof(AlignedElementSizeInBytes), AlignedElementSizeInBytes);

        string nextPrefix = linePrefix + "\t";

        output.WriteLine();
        DumpText(output, linePrefix, "# Members", Members.Length);
        output.WriteLine();
        
        foreach (ValueVariableMember m in Members)
        {
          m.Dump(output, nextPrefix);
          output.WriteLine();
        }
      }
    }

    /// <summary>
    /// Represents a shader variable that takes in a value (scalar, vector, matrix, struct) and always is part of a <see cref="ConstantBuffer"/>.
    /// While these are grouped in <see cref="ConstantBuffer"/> the variable name should be unique.
    /// </summary>
    [DebuggerDisplay("Name = {Name}, Class = {VariableClass}, Type = {VariableType}, ElementCount = {ElementCount}, MemberCount = {Members.Length}")]
    public sealed class ValueVariable
    {
      /// <summary>
      /// Name of the variable, should be unique across all constant buffers / resource variables within a <see cref="ShaderData"/>.
      /// </summary>
      public string Name = String.Empty;
      public uint? ComputedParameterNameIndex;
      public uint? FeatureNameIndex;
      public uint SizeInBytes;
      public uint AlignedElementSizeInBytes;
      public uint StartOffset;
      public ShaderVariableClass VariableClass;
      public ShaderVariableType VariableType;
      public uint ColumnCount;
      public uint RowCount;
      public ShaderVariableFlags Flags;
      public uint StartSampler;
      public uint SamplerSize;
      public uint StartTexture;
      public uint TextureSize;
      public ReadOnlyMemory<byte> DefaultValue;
      public uint ElementCount;

      public ValueVariableMember[] Members = Array.Empty<ValueVariableMember>();

      internal static ValueVariable Read(ISavableReader input, string name = "ValueVariable")
      {
        ValueVariable v = new ValueVariable();

        input.BeginReadGroup(name);

        v.Name = input.ReadStringOrDefault("Name");
        v.ComputedParameterNameIndex = input.ReadNullableUInt32("ComputedParameterIndex");
        v.FeatureNameIndex = input.ReadNullableUInt32("FeatureIndex");
        v.SizeInBytes = input.ReadUInt32("SizeInBytes");
        v.AlignedElementSizeInBytes = input.ReadUInt32("AlignedElementSizeInBytes");
        v.StartOffset = input.ReadUInt32("StartOffset");
        v.VariableClass = input.ReadEnum<ShaderVariableClass>("VariableClass");
        v.VariableType = input.ReadEnum<ShaderVariableType>("VariableType");
        v.ColumnCount = input.ReadUInt32("ColumnCount");
        v.RowCount = input.ReadUInt32("RowCount");
        v.Flags = input.ReadEnum<ShaderVariableFlags>("Flags");
        v.StartSampler = input.ReadUInt32("StartSampler");
        v.SamplerSize = input.ReadUInt32("SamplerSize");
        v.StartTexture = input.ReadUInt32("StartTexture");
        v.TextureSize = input.ReadUInt32("TextureSize");
        v.DefaultValue = input.ReadBlobAsMemory<byte>("DefaultValue");
        v.ElementCount = input.ReadUInt32("ElementCount");

        int memberCount = input.BeginReadGroup("Members");
        if (memberCount > 0)
        {
          v.Members = new ValueVariableMember[memberCount];
          for (int i = 0; i < memberCount; i++)
            v.Members[i] = ValueVariableMember.Read(input);
        }
        input.EndReadGroup();

        input.EndReadGroup();

        return v;
      }

      internal static void Write(ISavableWriter output, ValueVariable v, string name = "ValueVariable")
      {
        output.BeginWriteGroup(name);

        output.Write("Name", v.Name);
        output.Write("ComputedParameterIndex", v.ComputedParameterNameIndex);
        output.Write("FeatureIndex", v.FeatureNameIndex);
        output.Write("SizeInBytes", v.SizeInBytes);
        output.Write("AlignedElementSizeInBytes", v.AlignedElementSizeInBytes);
        output.Write("StartOffset", v.StartOffset);
        output.WriteEnum<ShaderVariableClass>("VariableClass", v.VariableClass);
        output.WriteEnum<ShaderVariableType>("VariableType", v.VariableType);
        output.Write("ColumnCount", v.ColumnCount);
        output.Write("RowCount", v.RowCount);
        output.WriteEnum<ShaderVariableFlags>("Flags", v.Flags);
        output.Write("StartSampler", v.StartSampler);
        output.Write("SamplerSize", v.SamplerSize);
        output.Write("StartTexture", v.StartTexture);
        output.Write("TextureSize", v.TextureSize);
        output.WriteBlob<byte>("DefaultValue", v.DefaultValue.Span);
        output.Write("ElementCount", v.ElementCount);

        output.BeginWriteGroup("Members", v.Members.Length);
        foreach (ValueVariableMember m in v.Members)
          ValueVariableMember.Write(output, m);
        output.EndWriteGroup();

        output.EndWriteGroup();
      }

      internal void Dump(TextWriter output, string linePrefix)
      {
        DumpText(output, linePrefix, nameof(Name), Name);

        if (ComputedParameterNameIndex.HasValue)
          DumpText(output, linePrefix, nameof(ComputedParameterNameIndex), ComputedParameterNameIndex.Value);

        if (FeatureNameIndex.HasValue)
          DumpText(output, linePrefix, nameof(FeatureNameIndex), FeatureNameIndex.Value);

        DumpText(output, linePrefix, nameof(SizeInBytes), SizeInBytes);
        DumpText(output, linePrefix, nameof(AlignedElementSizeInBytes), AlignedElementSizeInBytes);
        DumpText(output, linePrefix, nameof(StartOffset), StartOffset);
        DumpText(output, linePrefix, nameof(VariableClass), VariableClass);
        DumpText(output, linePrefix, nameof(VariableType), VariableType);
        DumpText(output, linePrefix, nameof(ColumnCount), ColumnCount);
        DumpText(output, linePrefix, nameof(RowCount), RowCount);
        DumpText(output, linePrefix, nameof(Flags), Flags);
        DumpText(output, linePrefix, nameof(StartSampler), StartSampler);
        DumpText(output, linePrefix, nameof(SamplerSize), SamplerSize);
        DumpText(output, linePrefix, nameof(StartTexture), StartTexture);
        DumpText(output, linePrefix, nameof(TextureSize), TextureSize);
        DumpText(output, linePrefix, nameof(DefaultValue), $"Byte[{DefaultValue.Length}]");
        DumpText(output, linePrefix, nameof(ElementCount), ElementCount);

        string nextPrefix = linePrefix + "\t";

        output.WriteLine();
        DumpText(output, linePrefix, "# Members", Members.Length);
        output.WriteLine();

        foreach (ValueVariableMember m in Members)
        {
          m.Dump(output, nextPrefix);
          output.WriteLine();
        }
      }
    }

    /// <summary>
    /// Represents a compiled shader with metadata about it's input, output signatures, what resources it uses, and what stage of the pipeline
    /// it controls.
    /// </summary>
    [DebuggerDisplay("{ShaderType}, {Name}")]
    public sealed class Shader
    {
      public ShaderStage ShaderType;

      /// <summary>
      /// Name for the shader, does not have to be unique and often used as a debug name/description. Usually the entry function name + defines used to preprocess the source code to get a unique value.
      /// </summary>
      public string Name = String.Empty;
      public string ShaderProfile = String.Empty;
      public ReadOnlyMemory<byte> ShaderByteCode;
      public InputPrimitive GeometryShaderInputPrimitive;
      public PrimitiveTopology GeometryShaderOutputTopology;
      public uint GeometryShaderInstanceCount;
      public uint MaxGeometryShaderOutputVertexCount;
      public InputPrimitive GeometryOrHullShaderInputPrimitive;
      public TessellatorOutputPrimitive HullShaderOutputPrimitive;
      public TessellatorPartitioning HullShaderPartitioning;
      public TessellatorDomain TessellatorDomain;
      public bool IsSampleFrequencyShader;
      public ShaderCompileFlags ShaderFlags;
      public Signature InputSignature = new Signature();
      public Signature OutputSignature = new Signature();
      public BoundResource[] BoundResources = Array.Empty<BoundResource>();

      internal static Shader Read(ISavableReader input, string name = "Shader")
      {
        Shader s = new Shader();

        input.BeginReadGroup(name);

        s.ShaderType = input.ReadEnum<ShaderStage>("ShaderType");
        s.Name = input.ReadStringOrDefault("Name");
        s.ShaderProfile = input.ReadStringOrDefault("ShaderProfile");
        s.ShaderByteCode = input.ReadBlobAsMemory<byte>("ShaderByteCode");
        s.GeometryShaderInputPrimitive = input.ReadEnum<InputPrimitive>("GeometryShaderInputPrimitive");
        s.GeometryShaderOutputTopology = input.ReadEnum<PrimitiveTopology>("GeometryShaderOutputTopology");
        s.GeometryShaderInstanceCount = input.ReadUInt32("GeometryShaderInstanceCount");
        s.MaxGeometryShaderOutputVertexCount = input.ReadUInt32("MaxGeometryShaderOutputVertexCount");
        s.GeometryOrHullShaderInputPrimitive = input.ReadEnum<InputPrimitive>("GeometryOrHullShaderInputPrimitive");
        s.HullShaderOutputPrimitive = input.ReadEnum<TessellatorOutputPrimitive>("HullShaderOutputPrimitive");
        s.HullShaderPartitioning = input.ReadEnum<TessellatorPartitioning>("HullShaderPartitioning");
        s.TessellatorDomain = input.ReadEnum<TessellatorDomain>("TessellatorDomain");
        s.IsSampleFrequencyShader = input.ReadBoolean("IsSampleFrequencyShader");
        s.ShaderFlags = input.ReadEnum<ShaderCompileFlags>("ShaderFlags");

        s.InputSignature = Signature.Read(input, "InputSignature");
        s.OutputSignature = Signature.Read(input, "OutputSignature");

        s.BoundResources = input.ReadArray<BoundResource>("BoundResources");

        input.EndReadGroup();

        return s;
      }

      internal static void Write(ISavableWriter output, Shader s, string name = "Shader")
      {
        output.BeginWriteGroup(name);

        output.WriteEnum<ShaderStage>("ShaderType", s.ShaderType);
        output.Write("Name", s.Name);
        output.Write("ShaderProfile", s.ShaderProfile);
        output.WriteBlob<byte>("ShaderByteCode", s.ShaderByteCode.Span);
        output.WriteEnum<InputPrimitive>("GeometryShaderInputPrimitive", s.GeometryShaderInputPrimitive);
        output.WriteEnum<PrimitiveTopology>("GeometryShaderOutputTopology", s.GeometryShaderOutputTopology);
        output.Write("GeometryShaderInstanceCount", s.GeometryShaderInstanceCount);
        output.Write("MaxGeometryShaderOutputVertexCount", s.MaxGeometryShaderOutputVertexCount);
        output.WriteEnum<InputPrimitive>("GeometryOrHullShaderInputPrimitive", s.GeometryOrHullShaderInputPrimitive);
        output.WriteEnum<TessellatorOutputPrimitive>("HullShaderOutputPrimitive", s.HullShaderOutputPrimitive);
        output.WriteEnum<TessellatorPartitioning>("HullShaderPartitioning", s.HullShaderPartitioning);
        output.WriteEnum<TessellatorDomain>("TessellatorDomain", s.TessellatorDomain);
        output.Write("IsSampleFrequencyShader", s.IsSampleFrequencyShader);
        output.WriteEnum<ShaderCompileFlags>("ShaderFlags", s.ShaderFlags);

        Signature.Write(output, s.InputSignature, "InputSignature");
        Signature.Write(output, s.OutputSignature, "OutputSignature");

        output.Write<BoundResource>("BoundResources", s.BoundResources);

        output.EndWriteGroup();
      }

      internal void Dump(TextWriter output, string linePrefix)
      {
        DumpText(output, linePrefix, nameof(ShaderType), ShaderType);
        DumpText(output, linePrefix, nameof(Name), Name);
        DumpText(output, linePrefix, nameof(ShaderProfile), ShaderProfile);
        DumpText(output, linePrefix, nameof(ShaderByteCode), $"Byte[{ShaderByteCode.Length}]");
        DumpText(output, linePrefix, "ShaderByteCode Hash (FNV)", BufferHelper.ComputeFNVModifiedHashCode(ShaderByteCode.Span));
        DumpText(output, linePrefix, nameof(GeometryShaderInputPrimitive), GeometryShaderInputPrimitive);
        DumpText(output, linePrefix, nameof(GeometryShaderOutputTopology), GeometryShaderOutputTopology);
        DumpText(output, linePrefix, nameof(HullShaderOutputPrimitive), HullShaderOutputPrimitive);
        DumpText(output, linePrefix, nameof(HullShaderPartitioning), HullShaderPartitioning);
        DumpText(output, linePrefix, nameof(TessellatorDomain), TessellatorDomain);
        DumpText(output, linePrefix, nameof(IsSampleFrequencyShader), IsSampleFrequencyShader);
        DumpText(output, linePrefix, nameof(ShaderFlags), ShaderFlags);

        string nextPrefix = linePrefix + "\t";

        output.WriteLine();
        DumpText(output, linePrefix, "Input Signature", String.Empty);
        output.WriteLine();
        InputSignature.Dump(output, nextPrefix);

        DumpText(output, linePrefix, "Output Signature", String.Empty);
        output.WriteLine();
        OutputSignature.Dump(output, nextPrefix);

        DumpText(output, linePrefix, "# Bound Resources", BoundResources.Length);
        output.WriteLine();

        foreach (BoundResource r in BoundResources)
        {
          r.Dump(output, nextPrefix);
          output.WriteLine();
        }
      }
    }

    /// <summary>
    /// Represents a shader signature.
    /// </summary>
    [DebuggerDisplay("Count = {Parameters.Length}")]
    public sealed class Signature
    {
      public ReadOnlyMemory<byte> ByteCode;
      public SignatureParameter[] Parameters = Array.Empty<SignatureParameter>();

      internal static Signature Read(IPrimitiveReader input, string name = "Signature")
      {
        Signature sig = new Signature();

        input.BeginReadGroup(name);

        sig.ByteCode = input.ReadBlobAsMemory<byte>("ByteCode");
        sig.Parameters = input.ReadArray<SignatureParameter>("Parameters");

        input.EndReadGroup();

        return sig;
      }

      internal static void Write(IPrimitiveWriter output, Signature sig, string name = "Signature")
      {
        output.BeginWriteGroup(name);

        output.WriteBlob<byte>("ByteCode", sig.ByteCode.Span);
        output.Write<SignatureParameter>("Parameters", sig.Parameters);

        output.EndWriteGroup();
      }

      internal void Dump(TextWriter output, string linePrefix)
      {
        DumpText(output, linePrefix, nameof(ByteCode), $"Byte[{ByteCode.Length}]");
        DumpText(output, linePrefix, "ByteCode Hash (FNV)", BufferHelper.ComputeFNVModifiedHashCode(ByteCode.Span));

        string nextPrefix = linePrefix + "\t";

        output.WriteLine();
        DumpText(output, linePrefix, "# Signature Parameters", Parameters.Length);
        output.WriteLine();

        foreach (SignatureParameter p in Parameters)
        {
          p.Dump(output, nextPrefix);
          output.WriteLine();
        }
      }
    }

    /// <summary>
    /// Represents a used resource variable or constant buffer by a <see cref="Shader"/>.
    /// </summary>
    [DebuggerDisplay("{ResourceType}, Index = {ResourceIndex}, BindPoint = {BindPoint}, BindCount = {BindCount}")]
    public struct BoundResource : IPrimitiveValue
    {
      public ShaderInputType ResourceType;
      public uint ResourceIndex;
      public uint BindPoint;
      public uint BindCount;

      public void Read(IPrimitiveReader input)
      {
        ResourceType = input.ReadEnum<ShaderInputType>("ResourceType");
        ResourceIndex = input.ReadUInt32("ResourceIndex");
        BindPoint = input.ReadUInt32("BindPoint");
        BindCount = input.ReadUInt32("BindCount");
      }

      public void Write(IPrimitiveWriter output)
      {
        output.WriteEnum<ShaderInputType>("ResourceType", ResourceType);
        output.Write("ResourceIndex", ResourceIndex);
        output.Write("BindPoint", BindPoint);
        output.Write("BindCount", BindCount);
      }

      internal void Dump(TextWriter output, string linePrefix)
      {
        DumpText(output, linePrefix, nameof(ResourceType), ResourceType);
        DumpText(output, linePrefix, nameof(ResourceIndex), ResourceIndex);
        DumpText(output, linePrefix, nameof(BindPoint), BindPoint);
        DumpText(output, linePrefix, nameof(BindCount), BindCount);
      }
    }

    /// <summary>
    /// Represents a parameter within a <see cref="Signature"/>.
    /// </summary>
    [DebuggerDisplay("SemanticIndex = {SemanticName}, Index = {SemanticIndex}, Type = {ComponentType}, Usage = {UsageMask}")]
    public struct SignatureParameter : IPrimitiveValue
    {
      public string SemanticName;
      public uint SemanticIndex;
      public uint StreamIndex;
      public uint Register;
      public RegisterComponentUsage UsageMask;
      public RegisterComponentUsage ReadWriteMask;
      public RegisterComponentType ComponentType;
      public SystemValueType SystemType;
      public MinPrecision MinPrecision;

      public void Read(IPrimitiveReader input)
      {
        SemanticName = input.ReadStringOrDefault("SemanticName");
        SemanticIndex = input.ReadUInt32("SemanticIndex");
        StreamIndex = input.ReadUInt32("StreamIndex");
        Register = input.ReadUInt32("Register");
        UsageMask = input.ReadEnum<RegisterComponentUsage>("UsageMask");
        ReadWriteMask = input.ReadEnum<RegisterComponentUsage>("ReadWriteMask");
        ComponentType = input.ReadEnum<RegisterComponentType>("ComponentType");
        SystemType = input.ReadEnum<SystemValueType>("SystemType");
        MinPrecision = input.ReadEnum<MinPrecision>("MinPrecision");
      }

      public void Write(IPrimitiveWriter output)
      {
        output.Write("SemanticName", SemanticName);
        output.Write("SemanticIndex", SemanticIndex);
        output.Write("StreamIndex", StreamIndex);
        output.Write("Register", Register);
        output.WriteEnum<RegisterComponentUsage>("UsageMask", UsageMask);
        output.WriteEnum<RegisterComponentUsage>("ReadWriteMask", ReadWriteMask);
        output.WriteEnum<RegisterComponentType>("ComponentType", ComponentType);
        output.WriteEnum<SystemValueType>("SystemType", SystemType);
        output.WriteEnum<MinPrecision>("MinPrecision", MinPrecision);
      }

      internal void Dump(TextWriter output, string linePrefix)
      {
        DumpText(output, linePrefix, nameof(SemanticName), SemanticName);
        DumpText(output, linePrefix, nameof(SemanticIndex), SemanticIndex);
        DumpText(output, linePrefix, nameof(StreamIndex), StreamIndex);
        DumpText(output, linePrefix, nameof(Register), Register);
        DumpText(output, linePrefix, nameof(UsageMask), UsageMask);
        DumpText(output, linePrefix, nameof(ReadWriteMask), ReadWriteMask);
        DumpText(output, linePrefix, nameof(ComponentType), ComponentType);
        DumpText(output, linePrefix, nameof(SystemType), SystemType);
        DumpText(output, linePrefix, nameof(MinPrecision), MinPrecision);
      }
    }

    /// <summary>
    /// Represents sampler state values that a <see cref="ResourceVariable"/> should be initialized if it's a sampler.
    /// </summary>
    public struct SamplerStateData : IPrimitiveValue
    {
      public TextureAddressMode AddressU;
      public TextureAddressMode AddressV;
      public TextureAddressMode AddressW;
      public ComparisonFunction ComparisonFunction;
      public TextureFilter Filter;
      public int MaxAnisotropy;
      public float MipMapLevelOfDetailBias;
      public int MinMipMapLevel; //Min = 0
      public int MaxMipMapLevel; //Max = int.MaxValue
      public Color BorderColor;

      public void Read(IPrimitiveReader input)
      {
        AddressU = input.ReadEnum<TextureAddressMode>("AddressU");
        AddressV = input.ReadEnum<TextureAddressMode>("AddressV");
        AddressW = input.ReadEnum<TextureAddressMode>("AddressW");
        ComparisonFunction = input.ReadEnum<ComparisonFunction>("ComparisonFunction");
        Filter = input.ReadEnum<TextureFilter>("Filter");
        MaxAnisotropy = input.ReadInt32("MaxAnisotropy");
        MipMapLevelOfDetailBias = input.ReadSingle("MipMapLevelOfDetailBias");
        MinMipMapLevel = input.ReadInt32("MinMipMapLevel");
        MaxMipMapLevel = input.ReadInt32("MaxMipMapLevel");
        BorderColor = input.Read<Color>("BorderColor");
      }

      public void Write(IPrimitiveWriter output)
      {
        output.WriteEnum<TextureAddressMode>("AddressU", AddressU);
        output.WriteEnum<TextureAddressMode>("AddressV", AddressV);
        output.WriteEnum<TextureAddressMode>("AddressW", AddressW);
        output.WriteEnum<ComparisonFunction>("ComparisonFunction", ComparisonFunction);
        output.WriteEnum<TextureFilter>("Filter", Filter);
        output.Write("MaxAnisotropy", MaxAnisotropy);
        output.Write("MipMapLevelOfDetailBias", MipMapLevelOfDetailBias);
        output.Write("MinMipMapLevel", MinMipMapLevel);
        output.Write("MaxMipMapLevel", MaxMipMapLevel);
        output.Write<Color>("BorderColor", BorderColor);
      }

      internal void Dump(TextWriter output, string linePrefix)
      {
        DumpText(output, linePrefix, nameof(AddressU), AddressU);
        DumpText(output, linePrefix, nameof(AddressV), AddressV);
        DumpText(output, linePrefix, nameof(AddressW), AddressW);
        DumpText(output, linePrefix, nameof(ComparisonFunction), ComparisonFunction);
        DumpText(output, linePrefix, nameof(Filter), Filter);
        DumpText(output, linePrefix, nameof(MaxAnisotropy), MaxAnisotropy);
        DumpText(output, linePrefix, nameof(MipMapLevelOfDetailBias), MipMapLevelOfDetailBias);
        DumpText(output, linePrefix, nameof(MinMipMapLevel), MinMipMapLevel);
        DumpText(output, linePrefix, nameof(MaxMipMapLevel), MaxMipMapLevel);
        DumpText(output, linePrefix, nameof(BorderColor), BorderColor);
      }
    }

    private static void DumpText(TextWriter output, string linePrefix, string name, string value)
    {
      output.WriteLine($"{linePrefix}{name}: {value}");
    }

    private static void DumpText<T>(TextWriter output, string linePrefix, string name, T value)
    {
      output.WriteLine($"{linePrefix}{name}: {value}");
    }

    private static void DumpTextArray<T>(TextWriter output, string linePrefix, string name, ReadOnlySpan<T> values)
    {
      output.Write($"{linePrefix}{name}: [");
      for (int i = 0; i < values.Length; i++)
      {
        if (i > 0)
          output.Write(", ");

        output.Write($"{values[i]}");
      }

      output.WriteLine("]");
    }

    #region Enums

    public enum ShaderByteCodeFormat
    {
      DXBC = 0,
      DXIL = 1,
      SPIR_V = 2
    }

    public enum ShaderVariableClass
    {
      Scalar = 0,
      Vector = 1,
      MatrixRows = 2,
      MatrixColumns = 3,
      Object = 4,
      Struct = 5,
      InterfaceClass = 6,
      InterfacePointer = 7
    }

    public enum ShaderVariableType
    {
      AppendStructuredBuffer = 50,
      Blend = 24,
      Bool = 1,
      Buffer = 25,
      ByteaddressBuffer = 46,
      Computeshader = 38,
      Constantbuffer = 26,
      ConsumeStructuredBuffer = 51,
      Depthstencil = 23,
      Depthstencilview = 31,
      Domainshader = 36,
      Double = 39,
      Float = 3,
      Geometryshader = 21,
      Hullshader = 35,
      Int = 2,
      InterfacePointer = 37,
      Pixelfragment = 17,
      Pixelshade = 15,
      Rasterizer = 22,
      Rendertargetview = 30,
      RWBuffer = 45,
      RWByteAddressBuffer = 47,
      RwstructuredBuffer = 49,
      RWTexture1D = 40,
      RWTexture1DArray = 41,
      RWTexture2D = 42,
      RWTexture2DArray = 43,
      RWTexture3D = 44,
      Sampler = 10,
      Sampler1d = 11,
      Sampler2d = 12,
      Sampler3d = 13,
      Samplercube = 14,
      String = 4,
      StructuredBuffer = 48,
      Texture = 5,
      Texture1D = 6,
      Texture1DArray = 28,
      Texture2D = 7,
      Texture2DArray = 29,
      Texture2DMultisampled = 32,
      Texture2DMultisampledArray = 33,
      Texture3D = 8,
      TextureBuffer = 27,
      TextureCube = 9,
      TextureCubeArray = 34,
      UInt = 19,
      UInt8 = 20,
      Vertexfragment = 18,
      Vertexshader = 16,
      Void = 0
    }

    public enum ConstantBufferType
    {
      ConstantBuffer = 0,
      TextureBuffer = 1,
      InterfacePointers = 2,
      ResourceBindInformation = 3
    }

    public enum SystemValueType
    {
      ClipDistance = 2,
      Coverage = 66,
      CullDistance = 3,
      Depth = 65,
      DepthGreaterEqual = 67,
      DepthLessEqual = 68,
      FinalLineDensityTessFactor = 16,
      FinalLineDetailTessFactor = 15,
      FinalQuadEdgeTessFactor = 11,
      FinalQuadInsideTessFactor = 12,
      FinalTriangleEdgeTessFactor = 13,
      FinalTriangleInsideTessFactor = 14,
      InstanceId = 8,
      IsFrontFace = 9,
      Position = 1,
      PrimitiveId = 7,
      RenderTargetArrayIndex = 4,
      SampleIndex = 10,
      Target = 64,
      Undefined = 0,
      VertexId = 6,
      ViewportArrayIndex = 5,
    }

    public enum ResourceReturnType
    {
      UNorm = 1,
      SNorm = 2,
      SInt = 3,
      UInt = 4,
      Float = 5,
      Mixed = 6,
      Double = 7,
      Continued = 8
    }

    public enum ResourceDimension
    {
      Buffer = 1,
      Texture1D = 2,
      Texture1DArray = 3,
      Texture2D = 4,
      Texture2DArray = 5,
      Texture2DMultisampled = 6,
      Texture2DMultiSampledArray = 7,
      Texture3D = 8,
      TextureCube = 9,
      TextureCubeArray = 10,
      ExtendedBuffer = 11
    }

    [Flags]
    public enum ShaderInputFlags
    {
      None = 0,
      UserPacked = 1,
      ComparisonSampler = 2,
      TextureComponent0 = 4,
      TextureComponent1 = 8,
      TextureComponents = 12
    }

    public enum ShaderInputType
    {
      ConstantBuffer = 0,
      TextureBuffer = 1,
      Texture = 2,
      Sampler = 3,
      RWTyped = 4,
      Structured = 5,
      RWStructured = 6,
      ByteAddress = 7,
      RWByteAddress = 8,
      AppendStructured = 9,
      ConsumeStructured = 10,
      RWStructuredWithCounter = 11
    }

    [Flags]
    public enum ShaderVariableFlags
    {
      None = 0,
      UserPacked = 1,
      Used = 2,
      InterfacePointer = 4,
      InterfaceParameter = 8
    }

    public enum TessellatorDomain
    {
      Undefined = 0,
      Isoline = 1,
      Triangle = 2,
      Quad = 3
    }

    // Merge with PrimitieType
    public enum PrimitiveTopology
    {
      Undefined = 0,
      PointList = 1,
      LineList = 2,
      LineStrip = 3,
      TriangleList = 4,
      TriangleStrip = 5,
      LineListWithAdjacency = 10,
      LineStripWithAdjacency = 11,
      TriangleListWithAdjacency = 12,
      TriangleStripWithAdjacency = 13,
      PatchListWith1ControlPoint = 33,
      PatchListWith2ControlPoints = 34,
      PatchListWith3ControlPoints = 35,
      PatchListWith4ControlPoints = 36,
      PatchListWith5ControlPoints = 37,
      PatchListWith6ControlPoints = 38,
      PatchListWith7ControlPoints = 39,
      PatchListWith8ControlPoints = 40,
      PatchListWith9ControlPoints = 41,
      PatchListWith10ControlPoints = 42,
      PatchListWith11ControlPoints = 43,
      PatchListWith12ControlPoints = 44,
      PatchListWith13ControlPoints = 45,
      PatchListWith14ControlPoints = 46,
      PatchListWith15ControlPoints = 47,
      PatchListWith16ControlPoints = 48,
      PatchListWith17ControlPoints = 49,
      PatchListWith18ControlPoints = 50,
      PatchListWith19ControlPoints = 51,
      PatchListWith20ControlPoints = 52,
      PatchListWith21ControlPoints = 53,
      PatchListWith22ControlPoints = 54,
      PatchListWith23ControlPoints = 55,
      PatchListWith24ControlPoints = 56,
      PatchListWith25ControlPoints = 57,
      PatchListWith26ControlPoints = 58,
      PatchListWith27ControlPoints = 59,
      PatchListWith28ControlPoints = 60,
      PatchListWith29ControlPoints = 61,
      PatchListWith30ControlPoints = 62,
      PatchListWith31ControlPoints = 63,
      PatchListWith32ControlPoints = 64
    }

    public enum InputPrimitive
    {
      Undefined = 0,
      Point = 1,
      Line = 2,
      Triangle = 3,
      LineWithAdjacency = 6,
      TriangleWithAdjacency = 7,
      PatchWith1ControlPoint = 8,
      PatchWith2ControlPoints = 9,
      PatchWith3ControlPoints = 10,
      PatchWith4ControlPoints = 11,
      PatchWith5ControlPoints = 12,
      PatchWith6ControlPoints = 13,
      PatchWith7ControlPoints = 14,
      PatchWith8ControlPoints = 15,
      PatchWith9ControlPoints = 16,
      PatchWith10ControlPoints = 17,
      PatchWith11ControlPoints = 18,
      PatchWith12ControlPoints = 19,
      PatchWith13ControlPoints = 20,
      PatchWith14ControlPoints = 21,
      PatchWith15ControlPoints = 22,
      PatchWith16ControlPoints = 23,
      PatchWith17ControlPoints = 24,
      PatchWith18ControlPoints = 25,
      PatchWith19ControlPoints = 26,
      PatchWith20ControlPoints = 28,
      PatchWith21ControlPoints = 29,
      PatchWith22ControlPoints = 30,
      PatchWith23ControlPoints = 31,
      PatchWith24ControlPoints = 32,
      PatchWith25ControlPoints = 33,
      PatchWith26ControlPoints = 34,
      PatchWith27ControlPoints = 35,
      PatchWith28ControlPoints = 36,
      PatchWith29ControlPoints = 37,
      PatchWith30ControlPoints = 38,
      PatchWith31ControlPoints = 39,
      PatchWith32ControlPoints = 40
    }

    public enum TessellatorOutputPrimitive
    {
      Undefined = 0,
      Point = 1,
      Line = 2,
      TriangleClockwise = 3,
      TriangleCounterClockwise = 4
    }

    public enum TessellatorPartitioning
    {
      Undefined = 0,
      Integer = 1,
      PowerOfTwo = 2,
      FractionalOdd = 3,
      FractionalEven = 4
    }

    [Flags]
    public enum RequiresFlag : ulong
    {
      None = 0,
      Doubles = 1,
      EarlyDepthStencil = 2,
      UAVEveryStage = 4,
      UAV64 = 8,
      MinimumPrecision = 16,
      DoubleExtensions = 32,
      SAD4Extensions = 64,
      D3D9ComparisonFiltering = 128,
      TiledResources = 256
    }

    public enum MinPrecision
    {
      Default = 0,
      Float16 = 1,
      Float10 = 2,
      SInt16 = 4,
      UInt16 = 5,
      Any16 = 240,
      Any10 = 241
    }

    #endregion

    #endregion
  }
}
