﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// <para>
  /// Represents a complete listing of all shaders that are associated together by common inputs and permutations. This is the input
  /// to a shader compiler in order to produce a library of shaders to be used for rendering. Shaders may be standalone rendering techniques
  /// (e.g. each pass/stage of complex rendering effect) or "families" of similar shaders that vary based on material/geometry inputs where
  /// the correct shader is picked at runtime.
  /// </para>
  /// <para>
  /// Every shader group contained in this manifest must be uniquely named, including permutations. Permutations may be manually configured or automatically
  /// created or a combination of both.
  /// </para>
  /// </summary>
  public class ShaderManifest
  {
    /// <summary>
    /// Gets or sets the name of the shader library, often the name of the manifest file.
    /// </summary>
    public string Name;

    /// <summary>
    /// Gets or sets the shader source code file name.
    /// </summary>
    public string File;

    /// <summary>
    /// Gets or sets the file-wide parameter namespace that describes the parameters found in the shader(s). This can be overriden by individual
    /// shader groups. If this is null, it must be set on each shader group.
    /// </summary>
    public string? ParameterNamespace;

    /// <summary>
    /// Gets or sets the shader profile used by the compiler.
    /// </summary>
    public string ShaderProfile;

    /// <summary>
    /// Gets a list of shader groups that will be compiled.
    /// </summary>
    public readonly List<ShaderGroup> ShaderGroups;

    /// <summary>
    /// Constructs a new instance of the <see cref="ShaderManifest"/> class.
    /// </summary>
    /// <param name="name">Name of the shader library, often also the file name.</param>
    /// <param name="shaderProfile">Shader profile for the compiler.</param>
    /// <param name="fileName">Relative file path to the shader file.</param>
    /// <param name="parameterNamespace">Name of the parameters found in the shader. If not set file-wide, it must be set per-shader group.</param>
    public ShaderManifest(string name, string? shaderProfile = null, string? fileName = null, string? parameterNamespace = null)
    {
      Name = name;
      File = fileName ?? String.Empty;
      ParameterNamespace = parameterNamespace;
      ShaderProfile = shaderProfile ?? String.Empty;
      ShaderGroups = new List<ShaderGroup>();
    }

    /// <summary>
    /// Adds a new shader group to the manifest.
    /// </summary>
    /// <param name="name">Name of the shader group, this must be unique.</param>
    /// <param name="familyName">Optional family name of the shader. If none provided, it will be the shader group name.</param>
    /// <returns>Shader group object ready to have properties added.</returns>
    public ShaderGroup AddShaderGroup(string name, string? familyName = null)
    {
      ShaderGroup grp = new ShaderGroup(name, familyName);
      ShaderGroups.Add(grp);

      return grp;
    }

    /// <summary>
    /// Validates the shader manifest.
    /// </summary>
    /// <param name="errors">Optional error message list.</param>
    /// <returns>True if no errors were found, false if there were errors that need to be corrected.</returns>
    public bool Validate(IList<string>? errors = null)
    {
      if (String.IsNullOrEmpty(Name))
      {
        errors?.Add("Manifest Name is not valid.");
        return false;
      }

      if (String.IsNullOrEmpty(File))
      {
        errors?.Add("Manifest Shader File path is not valid.");
        return false;
      }

      if (String.Empty.Equals(ParameterNamespace))
      {
        errors?.Add("Maniest File-Wide Parameter Namespace is not valid.");
        return false;
      }


      if (String.IsNullOrEmpty(ShaderProfile))
      {
        errors?.Add("Manifest Shader Profile is not valid.");
        return false;
      }

      if (ShaderGroups.Count == 0)
      {
        errors?.Add("Manifest must declare at least one shader group.");
        return false;
      }

      bool checkNamespacePresence = ParameterNamespace is null;
      for (int i = 0; i < ShaderGroups.Count; i++)
      {
        ShaderGroup grp = ShaderGroups[i];
        if (!grp.Validate(Name, i, errors))
          return false;

        if (checkNamespacePresence && grp.ParameterNamespace is null)
        {
          errors?.Add($"ShaderGroup[{i}] ('{grp.Name}') must declare a Parameter Namespace, or a default file-wide one should be added to the manifest.");
          return false;
        }
      }

      return true;
    }

    /// <summary>
    /// Resolves all the shader groups in the manifest. This first validates. Then it expands any permutations and resolves
    /// inherited properties for each shader group.
    /// </summary>
    /// <param name="errors">Optional error message list.</param>
    /// <returns>List of resolved shader groups.</returns>
    public List<ResolvedShaderGroup> ResolveShaderGroups(IList<string>? errors = null)
    {
      List<ResolvedShaderGroup> groups = new List<ResolvedShaderGroup>();
      if (!Validate(errors))
        return groups;

      HashSet<string> uniqueNames = new HashSet<string>();
      foreach (ShaderGroup grp in ShaderGroups)
        grp.ResolveAll(ParameterNamespace, groups, uniqueNames, errors);

      return groups;
    }

    /// <summary>
    /// Writes the shader manifest out to a JSON stream. This validates the manifest first.
    /// </summary>
    /// <param name="writer">JSON writer.</param>
    /// <exception cref="ArgumentException">Thrown if the manifest is not valid.</exception>
    public void ToJSON(Utf8JsonWriter writer)
    {
      if (!Validate())
        throw new ArgumentException();

      writer.WriteStartObject();
      writer.WriteString("name", Name);
      writer.WriteString("file", File);
      writer.WriteString("profile", ShaderProfile);

      if (ParameterNamespace is not null)
        writer.WriteString("parameterNamespace", ParameterNamespace);

      writer.WriteStartArray("shaderGroups");

      foreach (ShaderGroup grp in ShaderGroups)
      {
        writer.WriteStartObject();
        grp.Write(writer);
        writer.WriteEndObject();
      }

      writer.WriteEndArray();
      writer.WriteEndObject();
    }

    /// <summary>
    /// Writes the shader manifest to a JSON string. This validates the manifest first.
    /// </summary>
    /// <exception cref="ArgumentException">Thrown if the manifest is not valid.</exception>
    /// <returns>JSON string.</returns>
    public string ToJSON()
    {
      using MemoryStream ms = new MemoryStream();
      ToJSON(ms);

      ms.Position = 0;
      using StreamReader strReader = new StreamReader(ms);

      return strReader.ReadToEnd();
    }

    /// <summary>
    /// Writes the shader manifest to a JSON stream. This validates the manifest first.
    /// </summary>
    /// <param name="stream">JSON stream.</param>
    /// <exception cref="ArgumentException">Thrown if the manifest is not valid.</exception>
    public void ToJSON(Stream stream)
    {
      using (Utf8JsonWriter writer = new Utf8JsonWriter(stream, new JsonWriterOptions() { Indented = true }))
        ToJSON(writer);
    }

    /// <summary>
    /// Reads a shader manifest from a JSON string.
    /// </summary>
    /// <param name="json">JSON string.</param>
    /// <returns>Parsed shader manifest, which may be empty.</returns>
    public static ShaderManifest FromJSON(string json)
    {
      using JsonDocument jsonDoc = JsonDocument.Parse(json, new JsonDocumentOptions() { AllowTrailingCommas = true, CommentHandling = JsonCommentHandling.Skip });
      return FromJSON(jsonDoc);
    }

    /// <summary>
    /// Reads a shader manifest from a JSON stream.
    /// </summary>
    /// <param name="stream">JSON stream.</param>
    /// <returns>Parsed shader manifest, which may be empty.</returns>
    public static ShaderManifest FromJSON(Stream stream)
    {
      using JsonDocument jsonDoc = JsonDocument.Parse(stream, new JsonDocumentOptions() { AllowTrailingCommas = true, CommentHandling = JsonCommentHandling.Skip });
      return FromJSON(jsonDoc);
    }

    private static ShaderManifest FromJSON(JsonDocument json)
    {
      ShaderManifest manifest = new ShaderManifest(String.Empty, String.Empty);
      JsonElement root = json.RootElement;
      if (TryGetProperty(root, "name", JsonValueKind.String, out JsonElement nameProp))
        manifest.Name = nameProp.GetString() ?? String.Empty;

      if (TryGetProperty(root, "file", JsonValueKind.String, out JsonElement fileProp))
        manifest.File = fileProp.GetString() ?? String.Empty;

      if (TryGetProperty(root, "profile", JsonValueKind.String, out JsonElement profileProp))
        manifest.ShaderProfile = profileProp.GetString() ?? String.Empty;

      if (TryGetProperty(root, "parameterNamespace", JsonValueKind.String, out JsonElement paramNamespaceProp))
        manifest.ParameterNamespace = paramNamespaceProp.GetString(); // Can be null

      if (TryGetProperty(root, "shaderGroups", JsonValueKind.Array, out JsonElement shaderGroupsProp))
      {
        foreach (JsonElement grpElem in shaderGroupsProp.EnumerateArray())
        {
          ShaderGroup? grp = ShaderGroup.FromJSON(grpElem);
          if (grp is not null)
            manifest.ShaderGroups.Add(grp);
        }
      }

      return manifest;
    }

    /// <summary>
    /// Defines a permutation option, combined with other options to form a complete <see cref="ShaderPermutation"/> automatically. Each option
    /// is required to define both a prefix and at least one feature, to ensure each combination of options produces a unique permutation. Options
    /// can override shader entry functions, but they are not compatible by default with other options that also override the same shader.
    /// </summary>
    public class PermutateOption
    {
      /// <summary>
      /// Gets or sets the prefix of the option. This is combined with other options to form the unique name of the permutation, e.g. "basename_optA_optB" where
      /// "_optA" and "_optB" are the prefixes for each.
      /// </summary>
      public string Prefix;

      /// <summary>
      /// Gets or sets the override name of the vertex shader entry function. This can only be overriden once per permutation.
      /// </summary>
      public string? Vertex;

      /// <summary>
      /// Gets or sets the override name of the pixel shader entry function. This can only be overriden once per permutation.
      /// </summary>
      public string? Pixel;

      /// <summary>
      /// Gets or sets the override name of the geometry shader entry function. This can only be overriden once per permutation.
      /// </summary>
      public string? Geometry;

      /// <summary>
      /// Gets or sets the override name of the hull shader entry function. This can only be overriden once per permutation.
      /// </summary>
      public string? Hull;

      /// <summary>
      /// Gets or sets the override name of the domain shader entry function. This can only be overriden once per permutation.
      /// </summary>
      public string? Domain;

      /// <summary>
      /// Gets or sets the override name of the compute shader entry function. This can only be overriden once per permutation.
      /// </summary>
      public string? Compute;

      /// <summary>
      /// Gets or sets optional shader macros. Optional but usually set in order to create slight shader variations.
      /// </summary>
      public List<ShaderMacro>? Macros;

      /// <summary>
      /// Gets or sets optional incompatible features. This allows some control over combinations that do not make any logical sense so they an be discarded.
      /// </summary>
      public List<string>? IncompatibleFeatures;

      /// <summary>
      /// Gets the list of features that represent the option. There must be at least one feature defined for an option to be valid. This is used to 
      /// differentiate functionality between different permutations.
      /// </summary>
      public readonly List<string> Features;

      /// <summary>
      /// Constructs a new instance of the <see cref="PermutateOption"/> class.
      /// </summary>
      /// <param name="prefix">Unique prefix, usually begins with an underscore.</param>
      /// <param name="features">One or more features.</param>
      public PermutateOption(string prefix, params string[] features)
      {
        Prefix = prefix;
        Macros = new List<ShaderMacro>();
        Features = new List<string>(features);
        IncompatibleFeatures = new List<string>();
      }

      /// <summary>
      /// Gets the shader entry function corresponding to the stage.
      /// </summary>
      /// <param name="stage">Shader stage</param>
      /// <returns>Shader entry function name.</returns>
      public string? GetShaderEntryFunction(ShaderStage stage)
      {
        switch (stage)
        {
          case ShaderStage.VertexShader:
            return Vertex;
          case ShaderStage.PixelShader:
            return Pixel;
          case ShaderStage.GeometryShader:
            return Geometry;
          case ShaderStage.HullShader:
            return Hull;
          case ShaderStage.DomainShader:
            return Domain;
          case ShaderStage.ComputeShader:
            return Compute;
          default:
            return null;
        }
      }

      /// <summary>
      /// Sets the shader entry function corresponding to the stage.
      /// </summary>
      /// <param name="stage">Shader stage.</param>
      /// <param name="entryFunctionName">Shader entry function name.</param>
      public void SetShaderEntryFunction(ShaderStage stage, string? entryFunctionName)
      {
        switch (stage)
        {
          case ShaderStage.VertexShader:
            Vertex = entryFunctionName;
            break;
          case ShaderStage.PixelShader:
            Pixel = entryFunctionName;
            break;
          case ShaderStage.GeometryShader:
            Geometry = entryFunctionName;
            break;
          case ShaderStage.HullShader:
            Hull = entryFunctionName;
            break;
          case ShaderStage.DomainShader:
            Domain = entryFunctionName;
            break;
          case ShaderStage.ComputeShader:
            Compute = entryFunctionName;
            break;
        }
      }

      internal bool Validate(string name, int index, IList<string>? errors = null)
      {
        if (String.IsNullOrEmpty(Prefix))
        {
          errors?.Add($"{name}.Permutate[{index}] needs a valid prefix.");
          return false;
        }

        if (Features.Count == 0)
        {
          errors?.Add($"{name}.Permutate[{index}] needs one or more features to be valid.");
          return false;
        }

        if (HasOneShaderOverride(name, index, errors) || (Macros is not null && Macros.Count > 0))
          return true;

        errors?.Add($"{name}.Permutate[{index}] needs to override a shader entry function OR define one macro to be valid.");
        return false;
      }

      private bool HasOneShaderOverride(string name, int index, IList<string>? errors = null)
      {
        return ValidateShaderOverride(Vertex, nameof(Vertex), name, index, errors) || ValidateShaderOverride(Pixel, nameof(Pixel), name, index, errors)
          || ValidateShaderOverride(Geometry, nameof(Geometry), name, index, errors) || ValidateShaderOverride(Compute, nameof(Compute), name, index, errors)
          || ValidateShaderOverride(Hull, nameof(Hull), name, index, errors) || ValidateShaderOverride(Domain, nameof(Domain), name, index, errors);
      }

      private bool ValidateShaderOverride(string? shader, string propName, string name, int index, IList<string>? errors = null)
      {
        if (shader is not null)
        {
          if (String.IsNullOrWhiteSpace(shader))
          {
            errors?.Add($"{name}.Permutate[{index}].{propName} is overridden but empty.");
            return false;
          }

          return true;
        }

        return false;
      }

      internal void Write(Utf8JsonWriter writer)
      {
        writer.WriteString("prefix", Prefix);

        if (Vertex is not null)
          writer.WriteString("vertex", Vertex);

        if (Pixel is not null)
          writer.WriteString("pixel", Pixel);

        if (Geometry is not null)
          writer.WriteString("geometry", Geometry);

        if (Hull is not null)
          writer.WriteString("hull", Hull);

        if (Domain is not null)
          writer.WriteString("domain", Domain);

        if (Compute is not null)
          writer.WriteString("compute", Compute);

        if (Macros is not null && Macros.Count > 0)
        {
          writer.WriteStartArray("macros");
          foreach (ShaderMacro macro in Macros)
            WriteShaderMacro(writer, macro);
          writer.WriteEndArray();
        }

        if (Features.Count > 0)
        {
          writer.WriteStartArray("features");
          foreach (string str in Features)
            writer.WriteStringValue(str);
          writer.WriteEndArray();
        }

        if (IncompatibleFeatures is not null && IncompatibleFeatures.Count > 0)
        {
          writer.WriteStartArray("incompatibleWith");
          foreach (string str in IncompatibleFeatures)
            writer.WriteStringValue(str);
          writer.WriteEndArray();
        }
      }

      internal static PermutateOption? FromJSON(JsonElement el)
      {
        if (el.ValueKind != JsonValueKind.Object)
          return null;

        string? prefix = ReadStringProperty(el, "prefix");
        if (String.IsNullOrEmpty(prefix) || !TryGetProperty(el, "features", JsonValueKind.Array, out JsonElement featuresProp))
          return null;

        string[] features = new string[featuresProp.GetArrayLength()];
        for (int i = 0; i < featuresProp.GetArrayLength(); i++)
        {
          string? feature = ReadStringArrayElement(featuresProp, i);
          if (!String.IsNullOrEmpty(feature))
            features[i] = feature;
        }

        PermutateOption opt = new PermutateOption(prefix, features);

        opt.Vertex = ReadStringProperty(el, "vertex");
        opt.Pixel = ReadStringProperty(el, "pixel");
        opt.Geometry = ReadStringProperty(el, "geometry");
        opt.Hull = ReadStringProperty(el, "hull");
        opt.Domain = ReadStringProperty(el, "domain");
        opt.Compute = ReadStringProperty(el, "compute");

        if (TryGetProperty(el, "macros", JsonValueKind.Array, out JsonElement macrosProp))
        {
          opt.Macros = new List<ShaderMacro>();

          for (int i = 0; i < macrosProp.GetArrayLength(); i++)
          {
            if (TryReadShaderMacro(macrosProp[i], out ShaderMacro macro))
              opt.Macros.Add(macro);
          }
        }

        if (TryGetProperty(el, "incompatibleWith", JsonValueKind.Array, out JsonElement incompatibleFeaturesProp))
        {
          opt.IncompatibleFeatures = new List<string>();

          for (int i = 0; i < incompatibleFeaturesProp.GetArrayLength(); i++)
          {
            string? incFeature = ReadStringArrayElement(incompatibleFeaturesProp, i);
            if (!String.IsNullOrEmpty(incFeature))
              opt.IncompatibleFeatures.Add(incFeature);
          }
        }

        return opt;
      }
    }

    /// <summary>
    /// Represents a permutation of a shader group that may have it's own set of permutations. Permutations inherit most properties from their parent,
    /// and allow for easily overriding one or more properties.
    /// </summary>
    public class ShaderPermutation
    {
      /// <summary>
      /// Gets or sets the parent shader group.
      /// </summary>
      public ShaderPermutation? Parent;

      /// <summary>
      /// Gets or sets the name of the shader group. This is <b>not</b> inheritable and must be uniquely named in context of the entire manifest.
      /// </summary>
      public string Name;

      /// <summary>
      /// Gets or sets the name of the vertex shader entry function. If not defined, this is inherited.
      /// </summary>
      public string? Vertex;

      /// <summary>
      /// Gets or sets the name of the pixel shader entry function. If not defined, this is inherited.
      /// </summary>
      public string? Pixel;

      /// <summary>
      /// Gets or sets the name of the geometry shader entry function. If not defined, this is inherited.
      /// </summary>
      public string? Geometry;

      /// <summary>
      /// Gets or sets the name of the hull shader entry function. If not defined, this is inherited.
      /// </summary>
      public string? Hull;

      /// <summary>
      /// Gets or sets the name of the domain shader entry function. If not defined, this is inherited.
      /// </summary>
      public string? Domain;

      /// <summary>
      /// Gets or sets the name of the compute shader entry function. If not defined, this is inherited.
      /// </summary>
      public string? Compute;

      /// <summary>
      /// Gets or sets the list of macros used by the shader group. If not defined, this is inherited.
      /// </summary>
      public List<ShaderMacro>? Macros;

      /// <summary>
      /// Gets or sets the list of features used by the shader group (more may be defined in the shaders). If not defined, this is inherited.
      /// </summary>
      public List<string>? Features;

      /// <summary>
      /// Gets or sets the list of auto-permutation options used to generate additional shader permutations. If not defined, this is inherited.
      /// Generally keep this number small, as it can generate up to 2^(N) combinations. Each combination's name must be unique in context of the entire manifest.
      /// </summary>
      public List<PermutateOption>? Permutate;

      /// <summary>
      /// Gets or sets the list of explicitly defined permutations. This is never inherited. Each permutation name must be uniquely named in context of the entire manifest.
      /// </summary>
      public readonly List<ShaderPermutation> Permutations;

      internal ShaderPermutation(string name)
      {
        Name = name;
        Permutations = new List<ShaderPermutation>();
      }

      /// <summary>
      /// Adds a new child permutation.
      /// </summary>
      /// <param name="name">Name of the permutation.</param>
      /// <returns>Shader permutation.</returns>
      public ShaderPermutation AddPermutation(string name)
      {
        ShaderPermutation grp = new ShaderPermutation(name);
        grp.Parent = this;
        Permutations.Add(grp);

        return grp;
      }

      /// <summary>
      /// Gets the shader entry function corresponding to the stage.
      /// </summary>
      /// <param name="stage">Shader stage</param>
      /// <returns>Shader entry function name.</returns>
      public string? GetShaderEntryFunction(ShaderStage stage)
      {
        switch (stage)
        {
          case ShaderStage.VertexShader:
            return Vertex;
          case ShaderStage.PixelShader:
            return Pixel;
          case ShaderStage.GeometryShader:
            return Geometry;
          case ShaderStage.HullShader:
            return Hull;
          case ShaderStage.DomainShader:
            return Domain;
          case ShaderStage.ComputeShader:
            return Compute;
          default:
            return null;
        }
      }

      /// <summary>
      /// Sets the shader entry function corresponding to the stage.
      /// </summary>
      /// <param name="stage">Shader stage.</param>
      /// <param name="entryFunctionName">Shader entry function name.</param>
      public void SetShaderEntryFunction(ShaderStage stage, string? entryFunctionName)
      {
        switch (stage)
        {
          case ShaderStage.VertexShader:
            Vertex = entryFunctionName;
            break;
          case ShaderStage.PixelShader:
            Pixel = entryFunctionName;
            break;
          case ShaderStage.GeometryShader:
            Geometry = entryFunctionName;
            break;
          case ShaderStage.HullShader:
            Hull = entryFunctionName;
            break;
          case ShaderStage.DomainShader:
            Domain = entryFunctionName;
            break;
          case ShaderStage.ComputeShader:
            Compute = entryFunctionName;
            break;
        }
      }

      private List<ShaderMacro>? ResolveMacros()
      {
        // Allow empty list to override
        if (Macros is not null)
          return Macros;

        if (Parent is not null)
          return Parent.ResolveMacros();

        return null;
      }

      private List<string>? ResolveFeatures()
      {
        // Allow empty list to override
        if (Features is not null)
          return Features;

        if (Parent is not null)
          return Parent.ResolveFeatures();

        return null;
      }

      private List<PermutateOption>? ResolveAutoPermutate()
      {
        // Allow empty list to override
        if (Permutate is not null)
          return Permutate;

        if (Parent is not null)
          return Parent.ResolveAutoPermutate();

        return null;
      }

      private string? ResolveShaderEntryFunction(ShaderStage stage)
      {
        string? shader = GetShaderEntryFunction(stage);

        if (shader is not null)
          return shader;

        if (Parent is not null)
          return Parent.ResolveShaderEntryFunction(stage);

        return null;
      }

      internal ResolvedShaderGroup Resolve(string familyName, string parameterNamespace)
      {
        return new ResolvedShaderGroup(familyName, parameterNamespace, Name, ResolveShaderEntryFunction(ShaderStage.VertexShader), 
          ResolveShaderEntryFunction(ShaderStage.PixelShader), ResolveShaderEntryFunction(ShaderStage.GeometryShader), 
          ResolveShaderEntryFunction(ShaderStage.HullShader), ResolveShaderEntryFunction(ShaderStage.DomainShader),
          ResolveShaderEntryFunction(ShaderStage.ComputeShader), ResolveMacros(), ResolveFeatures());
      }

      internal void ResolveAll(string familyName, string parameterNamespace, IList<ResolvedShaderGroup> grps, HashSet<string> uniqueNames, IList<string>? errors = null)
      {
        // Resole this shader
        ResolvedShaderGroup thisResolved = Resolve(familyName, parameterNamespace);
        if (IsUniqueName(thisResolved.Name, uniqueNames, errors))
          grps.Add(thisResolved);

        // Resolve explicit permutations
        foreach (ShaderPermutation perm in Permutations)
          perm.ResolveAll(familyName, parameterNamespace, grps, uniqueNames, errors);

        // Expand the auto permutations and resolve them
        List<PermutateOption>? permutateOptions = ResolveAutoPermutate();
        if (permutateOptions is not null && permutateOptions.Count > 0)
        {
          IEnumerable<IEnumerable<PermutateOption>> autoPerms = GetPowerSet<PermutateOption>(permutateOptions);
          PermutateContext context = new PermutateContext();
          foreach (IEnumerable<PermutateOption> permInputs in autoPerms)
          {
            ResolvedShaderGroup? perm = context.CreatePermutation(permInputs, thisResolved, uniqueNames, errors);
            if (perm is not null)
              grps.Add(perm);
          }
        }
      }

      internal virtual bool Validate(string parentName, int index, IList<string>? errors = null)
      {
        if (String.IsNullOrEmpty(Name))
        {
          errors?.Add($"{parentName}[{index}] needs a name.");
          return false;
        }

        if (String.IsNullOrEmpty(Vertex) && String.IsNullOrEmpty(Pixel) && String.IsNullOrEmpty(Geometry) && String.IsNullOrEmpty(Hull) && String.IsNullOrEmpty(Domain) && String.IsNullOrEmpty(Compute))
        {
          errors?.Add($"{Name} must have at least one shader defined.");
          return false;
        }

        HashSet<string>? uniqueNames = null;

        if (Macros is not null)
        {
          if (uniqueNames is null)
            uniqueNames = new HashSet<string>();
          else
            uniqueNames.Clear();

          for (int i = 0; i < Macros.Count; i++)
          {
            ShaderMacro macro = Macros[i];
            if (uniqueNames.Contains(macro.Name))
            {
              errors?.Add($"{Name}.Macros[{i}] has duplicate name '{macro.Name}', each macro must be unique.");
              return false;
            }

            uniqueNames.Add(macro.Name);
          }
        }

        if (Features is not null)
        {
          if (uniqueNames is null)
            uniqueNames = new HashSet<string>();
          else
            uniqueNames.Clear();

          for (int i = 0; i < Features.Count; i++)
          {
            string feature = Features[i];
            if (uniqueNames.Contains(feature))
            {
              errors?.Add($"{Name}.Features[{i}] has duplicate '{feature}', a feature should only appear once.");
              return false;
            }

            uniqueNames.Add(feature);
          }
        }

        if (Permutate is not null)
        {
          if (uniqueNames is null)
            uniqueNames = new HashSet<string>();
          else
            uniqueNames.Clear();

          for (int i = 0; i < Permutate.Count; i++)
          {
            PermutateOption opt = Permutate[i];
            if (!opt.Validate(Name, i, errors))
              return false;

            if (uniqueNames.Contains(opt.Prefix))
            {
              errors?.Add($"{Name}.Permutate[{i}] has duplicate prefix '{opt.Prefix}', each prefix must be unique.");
              return false;
            }

            uniqueNames.Add(opt.Prefix);
          }
        }

        if (Permutations.Count > 0)
        {
          if (uniqueNames is null)
            uniqueNames = new HashSet<string>();
          else
            uniqueNames.Clear();

          uniqueNames.Add(Name);

          for (int i = 0; i < Permutations.Count; i++)
          {
            ShaderPermutation permutation = Permutations[i];
            if (!permutation.Validate(Name, i, errors))
              return false;

            if (uniqueNames.Contains(permutation.Name))
            {
              errors?.Add($"{Name}.Permutations[{i}] has a duplicate name '{permutation.Name}', each permutation must have a distinct name.");
              return false;
            }

            uniqueNames.Add(permutation.Name);
          }
        }

        return true;
      }

      internal virtual void Write(Utf8JsonWriter writer)
      {
        writer.WriteString("name", Name);

        if (Vertex is not null)
          writer.WriteString("vertex", Vertex);

        if (Pixel is not null)
          writer.WriteString("pixel", Pixel);

        if (Geometry is not null)
          writer.WriteString("geometry", Geometry);

        if (Hull is not null)
          writer.WriteString("hull", Hull);

        if (Domain is not null)
          writer.WriteString("domain", Domain);

        if (Compute is not null)
          writer.WriteString("compute", Compute);

        // Write out even if empty, empty array overrides inherited values
        if (Macros is not null)
        {
          writer.WriteStartArray("macros");
          foreach (ShaderMacro macro in Macros)
            WriteShaderMacro(writer, macro);
          writer.WriteEndArray();
        }

        // Write out even if empty, empty array overrides inherited values
        if (Features is not null)
        {
          writer.WriteStartArray("features");
          foreach (string str in Features)
            writer.WriteStringValue(str);
          writer.WriteEndArray();
        }

        // Write out even if empty, empty array overrides inherited values
        if (Permutate is not null)
        {
          writer.WriteStartArray("permutate");
          foreach (PermutateOption opt in Permutate)
          {
            writer.WriteStartObject();
            opt.Write(writer);
            writer.WriteEndObject();
          }
          writer.WriteEndArray();
        }

        if (Permutations.Count > 0)
        {
          writer.WriteStartArray("permutations");
          foreach (ShaderPermutation perm in Permutations)
            perm.Write(writer);
          writer.WriteEndArray();
        }
      }

      protected bool Read(JsonElement el)
      {
        if (el.ValueKind != JsonValueKind.Object)
          return false;

        Name = ReadStringProperty(el, "name") ?? String.Empty;
        Vertex = ReadStringProperty(el, "vertex");
        Pixel = ReadStringProperty(el, "pixel");
        Geometry = ReadStringProperty(el, "geometry");
        Hull = ReadStringProperty(el, "hull");
        Domain = ReadStringProperty(el, "domain");
        Compute = ReadStringProperty(el, "compute");

        if (TryGetProperty(el, "macros", JsonValueKind.Array, out JsonElement macrosProp))
        {
          Macros = new List<ShaderMacro>();
          
          for (int i = 0; i < macrosProp.GetArrayLength(); i++)
          {
            if (TryReadShaderMacro(macrosProp[i], out ShaderMacro macro))
              Macros.Add(macro);
          }
        }

        if (TryGetProperty(el, "features", JsonValueKind.Array, out JsonElement featuresProp))
        {
          Features = new List<string>();

          for (int i = 0; i < featuresProp.GetArrayLength(); i++)
          {
            string? feature = ReadStringArrayElement(featuresProp, i);
            if (!String.IsNullOrEmpty(feature))
              Features.Add(feature);
          }
        }

        if (TryGetProperty(el, "permutate", JsonValueKind.Array, out JsonElement permutateProp))
        {
          Permutate = new List<PermutateOption>();

          foreach (JsonElement optProp in permutateProp.EnumerateArray())
          {
            PermutateOption? opt = PermutateOption.FromJSON(optProp);
            if (opt is not null)
              Permutate.Add(opt);
          }
        }

        if (TryGetProperty(el, "permutations", JsonValueKind.Array, out JsonElement permutationsProp))
        {
          Permutations.Clear();

          foreach (JsonElement grpProp in permutationsProp.EnumerateArray())
          {
            ShaderPermutation permGrp = new ShaderPermutation(String.Empty);
            if (permGrp.Read(grpProp))
              Permutations.Add(permGrp);
          }
        }

        return true;
      }
    }

    /// <summary>
    /// Represents a complete shader group that may have permutations.
    /// </summary>
    public class ShaderGroup : ShaderPermutation
    {
      /// <summary>
      /// Get or set the family name of the shader group. If not set then it is the name of the shader group. 
      /// All permutations inherit from this, but cannot override it since those are variations within the same family.
      /// </summary>
      public string? Family;

      /// <summary>
      /// Gets or sets the parameter namespace of the shader group. If this is not set, then it is inherited from
      /// the File-wide parameter namespace. All permutations inherit either source, but cannot override it since
      /// those are variations within the same parameter namespace.
      /// </summary>
      public string? ParameterNamespace;

      /// <summary>
      /// Gets the family name or the default value (name of the shader group).
      /// </summary>
      public string FamilyNameOrDefault { get { return Family ?? Name; } }

      internal ShaderGroup(string name, string? family = null, string? parameterNamespace = null)
        : base(name)
      {
        Family = family;
        ParameterNamespace = parameterNamespace;
      }

      internal void ResolveAll(string? defaultParameterNamespace, IList<ResolvedShaderGroup> grps, HashSet<string> uniqueNames, IList<string>? errors = null)
      {
        ResolveAll(FamilyNameOrDefault, ResolveParameterNamespace(defaultParameterNamespace), grps, uniqueNames, errors);
      }

      internal string ResolveParameterNamespace(string? defaultParameterNamespace)
      {
        // If group's parameter namespace is null, then the manifest should supply a default value
        if (ParameterNamespace is null)
        {
          ArgumentNullException.ThrowIfNullOrEmpty(defaultParameterNamespace, nameof(defaultParameterNamespace));
          return defaultParameterNamespace;
        }

        return ParameterNamespace;
      }

      internal override void Write(Utf8JsonWriter writer)
      {
        if (!String.IsNullOrEmpty(Family))
          writer.WriteString("family", Family);

        if (!String.IsNullOrEmpty(ParameterNamespace))
          writer.WriteString("parameterNamespace", ParameterNamespace);

        base.Write(writer);
      }

      internal static ShaderGroup? FromJSON(JsonElement el)
      {
        if (el.ValueKind != JsonValueKind.Object)
          return null;

        ShaderGroup grp = new ShaderGroup(String.Empty, ReadStringProperty(el, "family"), ReadStringProperty(el, "parameterNamespace"));
        if (!grp.Read(el))
          return null;

        return grp;
      }
    }

    /// <summary>
    /// Represents a shader group with all properties resolved and ready for compilation.
    /// </summary>
    public class ResolvedShaderGroup
    {
      /// <summary>
      /// Gets the family name of the shader group. If one was not declared, this will be the same as the name.
      /// </summary>
      public readonly string Family;

      /// <summary>
      /// Gets the parameter namespace of the variables used by the shader group.
      /// </summary>
      public readonly string ParameterNamespace;

      /// <summary>
      /// Gets the unique name of the shader group to differentiate with other sets of shaders within the same library.
      /// </summary>
      public readonly string Name;

      /// <summary>
      /// Gets the name of the vertex shader entry function, if defined.
      /// </summary>
      public readonly string? Vertex;

      /// <summary>
      /// Gets the name of the pixel shader entry function, if defined.
      /// </summary>
      public readonly string? Pixel;

      /// <summary>
      /// Gets the name of the geometry shader entry function, if defined.
      /// </summary>
      public readonly string? Geometry;

      /// <summary>
      /// Gets the name of the hull shader entry function, if defined.
      /// </summary>
      public readonly string? Hull;

      /// <summary>
      /// Gets the name of the domain shader entry function, if defined.
      /// </summary>
      public readonly string? Domain;

      /// <summary>
      /// Gets the name of the compute shader entry function, if defined.
      /// </summary>
      public readonly string? Compute;

      /// <summary>
      /// Gets the set of macros used during the compilation of the shaders.
      /// </summary>
      public readonly IReadOnlyDictionary<string, ShaderMacro> Macros;

      /// <summary>
      /// Gets the set of features used by the shader group. More may be defined by parameters in the shader itself.
      /// </summary>
      public readonly IReadOnlySet<string> Features;

      /// <summary>
      /// Constructs a new instance of the <see cref="ResolvedShaderGroup"/> class.
      /// </summary>
      /// <param name="family">Family name of the shader group, if not part of a family just the name of the group.</param>
      /// <param name="parameterNamespace">Parameter namespace of the variables used by the shaders in this grouping.</param>
      /// <param name="name">Unique name to identify the shader group within a collection.</param>
      /// <param name="vertex">Vertex shader entry function name, if any.</param>
      /// <param name="pixel">Pixel shader entry function name, if any.</param>
      /// <param name="geometry">Geometry shader entry function name, if any.</param>
      /// <param name="hull">Hull shader entry function name, if any.</param>
      /// <param name="domain">Domain shader entry function name, if any.</param>
      /// <param name="compute">Compute shader entry function name, if any.</param>
      /// <param name="macros">Any macros to be used to compile the set of shaders.</param>
      /// <param name="features">Any features that should be associated with the set of shaders (more may be defined in the shaders themselves). </param>
      public ResolvedShaderGroup(string family, string parameterNamespace, string name, string? vertex, string? pixel, string? geometry, string? hull, string? domain, string? compute, IReadOnlyList<ShaderMacro>? macros, IReadOnlyList<string>? features)
      {
        Family = family;
        ParameterNamespace = parameterNamespace;
        Name = name;
        Vertex = vertex;
        Pixel = pixel;
        Geometry = geometry;
        Hull = hull;
        Domain = domain;
        Compute = compute;
        Macros = GetUniqueMacros(macros);
        Features = new HashSet<string>(features ?? Array.Empty<string>());
      }

      /// <summary>
      /// Constructs a new instance of the <see cref="ResolvedShaderGroup"/> class.
      /// </summary>
      /// <param name="family">Family name of the shader group, if not part of a family just the name of the group.</param>
      /// <param name="parameterNamespace">Parameter namespace of the variables used by the shaders in this grouping.</param>
      /// <param name="name">Unique name to identify the shader group within a collection.</param>
      /// <param name="vertex">Vertex shader entry function name, if any.</param>
      /// <param name="pixel">Pixel shader entry function name, if any.</param>
      /// <param name="geometry">Geometry shader entry function name, if any.</param>
      /// <param name="hull">Hull shader entry function name, if any.</param>
      /// <param name="domain">Domain shader entry function name, if any.</param>
      /// <param name="compute">Compute shader entry function name, if any.</param>
      /// <param name="macros">Any macros to be used to compile the set of shaders.</param>
      /// <param name="features">Any features that should be associated with the set of shaders (more may be defined in the shaders themselves). </param>
      public ResolvedShaderGroup(string family, string parameterNamespace, string name, string? vertex, string? pixel, string? geometry, string? hull, string? domain, string? compute, IReadOnlyDictionary<string, ShaderMacro>? macros, IReadOnlySet<string>? features)
      {
        Family = family;
        ParameterNamespace = parameterNamespace;
        Name = name;
        Vertex = vertex;
        Pixel = pixel;
        Geometry = geometry;
        Hull = hull;
        Domain = domain;
        Macros = macros ?? new Dictionary<string, ShaderMacro>();
        Compute = compute;
        Features = features ?? new HashSet<string>();
      }

      /// <summary>
      /// Gets the shader entry function corresponding to the stage.
      /// </summary>
      /// <param name="stage">Shader stage</param>
      /// <returns>Shader entry function name.</returns>
      public string? GetShaderEntryFunction(ShaderStage stage)
      {
        switch (stage)
        {
          case ShaderStage.VertexShader:
            return Vertex;
          case ShaderStage.PixelShader:
            return Pixel;
          case ShaderStage.GeometryShader:
            return Geometry;
          case ShaderStage.HullShader:
            return Hull;
          case ShaderStage.DomainShader:
            return Domain;
          case ShaderStage.ComputeShader:
            return Compute;
          default:
            return null;
        }
      }

      /// <summary>
      /// Clones the shader group with a new name and additional inputs to append.
      /// </summary>
      /// <param name="name">Unique name for the new permutation</param>
      /// <param name="overrideMacros">If defined, overrides the macros from the current instance.</param>
      /// <param name="overrideFeatures">If defined, overrides the features from the current instance.</param>
      /// <returns>Cloned shader permutation with the additional inputs, if any.</returns>
      public ResolvedShaderGroup ClonePermutation(string name, IReadOnlyList<ShaderMacro>? overrideMacros = null, IReadOnlyList<string>? overrideFeatures = null)
      {
        IReadOnlyDictionary<string, ShaderMacro> uniqueMacros = (overrideMacros is not null) ? GetUniqueMacros(overrideMacros) : Macros;
        IReadOnlySet<string> uniqueFeatures = (overrideFeatures is not null) ? new HashSet<string>(overrideFeatures) : Features;
        return new ResolvedShaderGroup(Family, ParameterNamespace, name, Vertex, Pixel, Geometry, Hull, Domain, Compute, uniqueMacros, uniqueFeatures);
      }

      /// <summary>
      /// Clones the shader group with a new name and additional inputs to append.
      /// </summary>
      /// <param name="name">Unique name for the new permutation</param>
      /// <param name="overrideMacros">If defined, overrides the macros from the current instance.</param>
      /// <param name="overrideFeatures">If defined, overrides the features from the current instance.</param>
      /// <returns>Cloned shader permutation with the additional inputs, if any.</returns>
      public ResolvedShaderGroup ClonePermutation(string name, IReadOnlyDictionary<string, ShaderMacro>? overrideMacros = null, IReadOnlySet<string>? overrideFeatures = null)
      {
        return new ResolvedShaderGroup(Family, ParameterNamespace, name, Vertex, Pixel, Geometry, Hull, Domain, Compute, overrideMacros ?? Macros, overrideFeatures ?? Features);
      }

      private static IReadOnlyDictionary<string, ShaderMacro> GetUniqueMacros(IReadOnlyList<ShaderMacro>? macros)
      {
        Dictionary<string, ShaderMacro> uniqueMacros = new Dictionary<string, ShaderMacro>();
        if (macros is not null)
        {
          for (int i = 0; i < macros.Count; i++)
          {
            ShaderMacro macro = macros[i];
            uniqueMacros[macro.Name] = macro;
          }
        }

        return uniqueMacros;
      }
    }

    // Encapsulates context for evaluating an auto-permutate set
    private sealed class PermutateContext
    {
      private StringBuilder m_builder = new StringBuilder();
      private HashSet<string> m_incompatibleFeatures = new HashSet<string>();
      private Dictionary<string, ShaderMacro> m_macros = new Dictionary<string, ShaderMacro>();
      private HashSet<string> m_features = new HashSet<string>();
      private HashSet<string>?[] m_uniqueShaderNames = new HashSet<string>?[6];
      private string?[] m_shaderNames = new string?[6];

      private void Reset()
      {
        m_builder.Clear();
        m_macros.Clear();
        m_features.Clear();
        m_incompatibleFeatures.Clear();

        foreach (HashSet<string>? shaderNameSet in m_uniqueShaderNames)
          shaderNameSet?.Clear();

        Array.Clear(m_shaderNames);
      }

      public ResolvedShaderGroup? CreatePermutation(IEnumerable<PermutateOption> permInputs, ResolvedShaderGroup parent, HashSet<string> uniqueNames, IList<string>? errors = null)
      {
        Reset();

        if (!IsPermutationCompatible(permInputs))
          return null;

        // Start the permutation name based on the current shader group
        m_builder.Append(parent.Name);

        // Base macro / features should already be unique due to the validation check, but we may get duplicates due to the permutation,
        // so make sure that gets neated up. We no longer care about the order once we resolve.
        foreach (ShaderMacro baseMacro in parent.Macros.Values)
        {
          // Allow the options to override a macro defined in the base...
          m_macros.TryAdd(baseMacro.Name, baseMacro);
        }

        foreach (string baseFeature in parent.Features)
          m_features.Add(baseFeature);

        foreach (PermutateOption opt in permInputs)
        {
          foreach (string feature in opt.Features)
            m_features.Add(feature);

          m_builder.Append(opt.Prefix);
        }

        // Error if the name is not unique
        string permName = m_builder.ToString();
        if (!IsUniqueName(permName, uniqueNames, errors))
          return null;

        return new ResolvedShaderGroup(parent.Family, parent.ParameterNamespace, permName, ResolveShaderName(ShaderStage.VertexShader, parent), ResolveShaderName(ShaderStage.PixelShader, parent),
          ResolveShaderName(ShaderStage.GeometryShader, parent), ResolveShaderName(ShaderStage.HullShader, parent), ResolveShaderName(ShaderStage.DomainShader, parent),
          ResolveShaderName(ShaderStage.ComputeShader, parent), new Dictionary<string, ShaderMacro>(m_macros), new HashSet<string>(m_features));
      }

      private string? ResolveShaderName(ShaderStage stage, ResolvedShaderGroup parent)
      {
        return m_shaderNames[(int) stage] ?? parent.GetShaderEntryFunction(stage);
      }

      private bool HasShaderConflict(string? shader, ref HashSet<string>? shaderNames)
      {
        if (shader is null)
          return false;

        if (shaderNames is null)
          shaderNames = new HashSet<string>();

        if (shaderNames.Contains(shader))
          return true;

        shaderNames.Add(shader);
        return false;
      }

      private bool IsPermutationCompatible(IEnumerable<PermutateOption> set)
      {
        int count = 0;
        foreach (PermutateOption opt in set)
        {
          // Gather incompatible features for testing later
          if (opt.IncompatibleFeatures is not null)
          {
            foreach (string incFeature in opt.IncompatibleFeatures)
              m_incompatibleFeatures.Add(incFeature);
          }

          // Check if we have differing shader macro definitions, if so not compatible
          if (opt.Macros is not null)
          {
            foreach (ShaderMacro macro in opt.Macros)
            {
              if (m_macros.TryGetValue(macro.Name, out ShaderMacro other) && !other.Equals(macro))
                return false;

              m_macros[macro.Name] = macro;
            }
          }

          // Check if we have differing shader overrides. If one overrides vertex and another the pixel, the combination *should* be okay,
          // but if multiple options override vertex or pixel, then most likely the behavior is not going to make sense
          for (int i = 0; i < 6; i++)
          {
            ShaderStage stage = (ShaderStage) i;
            string? shader = opt.GetShaderEntryFunction(stage);

            if (HasShaderConflict(shader, ref m_uniqueShaderNames[i]))
              return false;

            m_shaderNames[i] = shader;
          }

          count++;
        }

        // If the combo doesnt have anything it, signal caller it's not compatible too
        if (count == 0)
          return false;

        // Iterate again to ensure no features are in the incompatible set
        bool isIncompatible = false;
        foreach (PermutateOption opt in set)
        {
          if (isIncompatible)
            break;

          foreach (string feature in opt.Features)
          {
            if (m_incompatibleFeatures.Contains(feature))
            {
              isIncompatible = true;
              break;
            }
          }
        }

        return !isIncompatible;
      }
    }

    private static void WriteShaderMacro(Utf8JsonWriter writer, ShaderMacro macro)
    {
      if (macro.Definition is null)
        writer.WriteStringValue(macro.Name);
      else
      {
        writer.WriteStartArray();
        writer.WriteStringValue(macro.Name);
        writer.WriteStringValue(macro.Definition);
        writer.WriteEndArray();
      }
    }

    private static bool TryReadShaderMacro(JsonElement el, out ShaderMacro macro)
    {
      macro = default;

      switch (el.ValueKind)
      {
        case JsonValueKind.String:
          macro = new ShaderMacro(el.GetString() ?? String.Empty);
          return macro.IsValid;
        case JsonValueKind.Array:
          if (el.GetArrayLength() != 2)
            return false;

          macro = new ShaderMacro(ReadStringArrayElement(el, 0) ?? String.Empty, ReadStringArrayElement(el, 1));
          return macro.IsValid;
        default:
          return false;
      }
    }

    private static string? ReadStringProperty(JsonElement elem, string propName)
    {
      if (TryGetProperty(elem, propName, JsonValueKind.String, out JsonElement prop))
        return prop.GetString();

      return null;
    }

    private static string? ReadStringArrayElement(JsonElement elem, int index)
    {
      JsonElement arrElem = elem[index];
      if (arrElem.ValueKind == JsonValueKind.String)
        return arrElem.GetString();

      return null;
    }

    private static bool TryGetProperty(JsonElement el, string propname, JsonValueKind valueKind, out JsonElement propEl)
    {
      return el.TryGetProperty(propname, out propEl) && propEl.ValueKind == valueKind;
    }

    private static bool IsUniqueName(string name, HashSet<string> names, IList<string>? errors = null)
    {
      if (names.Contains(name))
      {
        if (errors is not null)
          errors.Add($"{name} is not unique. Skipping.");

        return false;
      }

      names.Add(name);
      return true;
    }

    private static IEnumerable<IEnumerable<T>> GetPowerSet<T>(IReadOnlyList<T> list)
    {
      // Returns the sets in order they appear from the original list
      return from m in Enumerable.Range(0, 1 << list.Count) select from i in Enumerable.Range(0, list.Count) where (m & (1 << i)) != 0 select list[i];
    }
  }
}
