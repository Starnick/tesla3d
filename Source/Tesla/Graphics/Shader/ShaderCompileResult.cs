﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// Represents a compiled shader result that may have errors.
  /// </summary>
  [DebuggerDisplay("HasResult = {HasResult}, HasCompileErrors = {HasCompileErrors}")]
  public sealed class ShaderCompileResult
  {
    /// <summary>
    /// Compiled shader data. If errors, this will be null.
    /// </summary>
    public ShaderData? ShaderData { get; init; }

    /// <summary>
    /// Any compiler error messages.
    /// </summary>
    public IReadOnlyList<string>? CompileErrors { get; init; }

    /// <summary>
    /// Gets if there are compile errors.
    /// </summary>
    [MemberNotNullWhen(true, nameof(CompileErrors))]
    public bool HasCompileErrors { get { return CompileErrors is not null && CompileErrors.Count > 0; } }

    /// <summary>
    /// Gets if the shader data result is valid.
    /// </summary>
    [MemberNotNullWhen(true, nameof(ShaderData))]
    public bool HasResult { get { return ShaderData is not null && !HasCompileErrors; } }

    /// <summary>
    /// Constructs a new instance of the <see cref="ShaderCompileResult"/> class.
    /// </summary>
    /// <param name="resultData">Result data, if any.</param>
    /// <param name="errorMessages">Compile error messages, if any.</param>
    public ShaderCompileResult(ShaderData? resultData, IReadOnlyList<string>? errorMessages = null)
    {
      ShaderData = resultData;
      CompileErrors = errorMessages;
    }

    /// <summary>
    /// Creates a new <see cref="ShaderCompileResult"/> from a successful compilation.
    /// </summary>
    /// <param name="data">Result data.</param>
    /// <returns>Result object.</returns>
    public static ShaderCompileResult FromResult(ShaderData data)
    {
      return new ShaderCompileResult(data);
    }

    /// <summary>
    /// Creates a new <see cref="ShaderCompileResult"/> that has errors.
    /// </summary>
    /// <param name="errors">Error messages.</param>
    /// <returns>Result object.</returns>
    public static ShaderCompileResult FromError(IReadOnlyList<string> errors)
    {
      return new ShaderCompileResult(null, errors);
    }
  }
}
