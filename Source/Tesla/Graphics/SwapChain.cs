﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Graphics.Implementation;

#nullable enable

namespace Tesla.Graphics
{
  public class SwapChain : GraphicsResource
  {

    #region Public Properties

    /// <summary>
    /// Gets the default clear depth (depending on whether or not the system is set to use reverse depth technique).
    /// </summary>
    public static float DefaultClearDepth
    {
      get
      {
        return DepthStencilState.IsReverseDepth ? 0.0f : 1.0f;
      }
    }

    /// <summary>
    /// Gets the presentation parameters the swap chain is initialized to.
    /// </summary>
    public PresentationParameters PresentationParameters
    {
      get
      {
        return SwapChainImpl.PresentationParameters;
      }
    }

    /// <summary>
    /// Gets the current display mode of the swap chain.
    /// </summary>
    public DisplayMode CurrentDisplayMode
    {
      get
      {
        return SwapChainImpl.CurrentDisplayMode;
      }
    }

    /// <summary>
    /// Gets the handle of the window the swap chain presents to.
    /// </summary>
    public IntPtr WindowHandle
    {
      get
      {
        return SwapChainImpl.WindowHandle;
      }
    }

    /// <summary>
    /// Gets the handle to the monitor that contains the majority of the output.
    /// </summary>
    public IntPtr MonitorHandle
    {
      get
      {
        return SwapChainImpl.MonitorHandle;
      }
    }

    /// <summary>
    /// Gets if the swap chain is in full screen exclusive mode or not. By default, swap chains are not in full screen mode.
    /// </summary>
    public bool IsFullScreen
    {
      get
      {
        return SwapChainImpl.IsFullScreen;
      }
    }

    /// <summary>
    /// Gets if the current display mode is in wide screen or not.
    /// </summary>
    public bool IsWideScreen
    {
      get
      {
        return SwapChainImpl.IsWideScreen;
      }
    }

    #endregion

    //Private property to cast the implementation
    private ISwapChainImpl SwapChainImpl
    {
      get
      {
        return GetImplAs<ISwapChainImpl>();
      }
      set
      {
        BindImplementation(value);
      }
    }

    /// <summary>
    /// For sub classes.
    /// </summary>
    protected SwapChain() { }

    /// <summary>
    /// Constructs a new instance of the <see cref="SwapChain"/> class.
    /// </summary>
    /// <param name="renderSystem">Render system used to create the underlying implementation.</param>
    /// <param name="windowHandle">The window handle the swapchain will present to.</param>
    /// <param name="preferredPresentParams">The preferred presentation parameters, if non-format values are out of range, they will be adjusted accordingly.</param>
    /// <exception cref="ArgumentException">Thrown if the window handle is not valid.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if the format is not valid for backbuffers.</exception>
    public SwapChain(IRenderSystem renderSystem, IntPtr windowHandle, PresentationParameters preferredPresentParams)
    {
      CreateImplementation(renderSystem, windowHandle, ref preferredPresentParams);
    }

    /// <summary>
    /// Clears the backbuffer and depthbuffer to default values (1.0 for depth, 0 for stencil).
    /// </summary>
    /// <param name="renderContext">Render context used to clear.</param>
    /// <param name="color">Color value to clear to.</param>
    /// <exception cref="ArgumentNullException">Thrown if the render context is null.</exception>
    /// <remarks>See implementors for details on specific exceptions that can be thrown.</remarks>
    public void Clear(IRenderContext renderContext, Color color)
    {
      GraphicsHelper.ThrowIfRenderContextNull(renderContext);

      SwapChainImpl.Clear(renderContext, ClearOptions.All, color, DefaultClearDepth, 0);
    }

    /// <summary>
    /// Clears the backbuffer and depthbuffer.
    /// </summary>
    /// <param name="renderContext">Render context used to clear.</param>
    /// <param name="clearOptions">Specifies which buffer to clear.</param>
    /// <param name="color">Color value to clear to.</param>
    /// <param name="depth">Depth value to clear to.</param>
    /// <param name="stencil">Stencil value to clear to.</param>
    /// <exception cref="ArgumentNullException">Thrown if the render context is null.</exception>
    /// <remarks>See implementors for details on specific exceptions that can be thrown.</remarks>
    public void Clear(IRenderContext renderContext, ClearOptions clearOptions, Color color, float depth, int stencil = 0)
    {
      GraphicsHelper.ThrowIfRenderContextNull(renderContext);

      SwapChainImpl.Clear(renderContext, clearOptions, color, depth, stencil);
    }

    /// <summary>
    /// Clears the backbuffer and binds it to the render context as the currently active backbuffer. This means when the context's list of
    /// render targets are set to null, the context will switch to the targets that represent this backbuffer.
    /// </summary>
    /// <param name="renderContext">Render context used to clear.</param>
    /// <param name="color">Color value to clear to.</param>
    /// <exception cref="ArgumentNullException">Thrown if the render context is null.</exception>
    /// <remarks>See implementors for details on specific exceptions that can be thrown.</remarks>
    public void SetActiveAndClear(IRenderContext renderContext, Color color)
    {
      GraphicsHelper.ThrowIfRenderContextNull(renderContext);

      renderContext.BackBuffer = this;
      renderContext.SetRenderTarget(null); //Resolve any render targets already set, we expect the backbuffer to always be set

      SwapChainImpl.Clear(renderContext, ClearOptions.All, color, DefaultClearDepth, 0);
    }

    /// <summary>
    /// Clears the backbuffer and binds it to the render context as the currently active backbuffer. This means when the context's list of
    /// render targets are set to null, the context will switch to the targets that represent this backbuffer.
    /// </summary>
    /// <param name="renderContext">Render context used to clear.</param>
    /// <param name="clearOptions">Specifies which buffer to clear.</param>
    /// <param name="color">Color value to clear to.</param>
    /// <param name="depth">Depth value to clear to.</param>
    /// <param name="stencil">Stencil value to clear to.</param>
    /// <exception cref="ArgumentNullException">Thrown if the render context is null.</exception>
    /// <remarks>See implementors for details on specific exceptions that can be thrown.</remarks>
    public void SetActiveAndClear(IRenderContext renderContext, ClearOptions clearOptions, Color color, float depth, int stencil = 0)
    {
      GraphicsHelper.ThrowIfRenderContextNull(renderContext);

      renderContext.BackBuffer = this;
      renderContext.SetRenderTarget(null); //Resolve any render targets already set, we expect the backbuffer to always be set

      SwapChainImpl.Clear(renderContext, clearOptions, color, depth, stencil);
    }

    /// <summary>
    /// Binds the backbuffer to the render context. This means when the context's list of
    /// render targets are set to null, the context will switch to the targets that represent this backbuffer.
    /// </summary>
    /// <param name="renderContext">Render context used to clear.</param>
    /// <exception cref="ArgumentNullException">Thrown if the render context is null.</exception>
    /// <remarks>See implementors for details on specific exceptions that can be thrown.</remarks>
    public void SetActive(IRenderContext renderContext)
    {
      GraphicsHelper.ThrowIfRenderContextNull(renderContext);

      renderContext.BackBuffer = this;
      renderContext.SetRenderTarget(null); //Resolve any render targets already set, we expect the backbuffer to always be set
    }

    /// <summary>
    /// Reads data from the backbuffer into the specified data buffer.
    /// </summary>
    /// <typeparam name="T">Type of data to read from the backbuffer.</typeparam>
    /// <param name="data">Buffer to hold data copied from the backbuffer.</param>
    /// <exception cref="TeslaGraphicsException">Thrown if an error occurs while reading from the backbuffer.</exception>
    /// <remarks>See implementors for details on specific exceptions that can be thrown.</remarks>
    public void GetBackBufferData<T>(Span<T> data) where T : unmanaged
    {
      GetBackBufferData<T>(data, null);
    }

    /// <summary>
    /// Reads data from the backbuffer into the specified data buffer.
    /// </summary>
    /// <typeparam name="T">Type of data to read from the backbuffer.</typeparam>
    /// <param name="data">Buffer to hold data copied from the backbuffer.</param>
    /// <param name="subimage">The subimage region, in texels, of the backbuffer to read from, if null the whole image is read from.</param>
    /// <exception cref="TeslaGraphicsException">Thrown if an error occurs while reading from the backbuffer.</exception>
    /// <remarks>See implementors for details on specific exceptions that can be thrown.</remarks>
    public void GetBackBufferData<T>(Span<T> data, ResourceRegion2D? subimage) where T : unmanaged
    {
      try
      {
        SwapChainImpl.GetBackBufferData<T>(data, subimage);
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw GraphicsExceptionHelper.NewErrorReadingFromResourceException(GetType(), e);
      }
    }

    /// <summary>
    /// Presents the contents of the backbuffer to the screen and flips the front/back buffers.
    /// </summary>
    /// <remarks>See implementors for details on specific exceptions that can be thrown.</remarks> 
    public void Present()
    {
      SwapChainImpl.Present();
    }

    /// <summary>
    /// Resets the swapchain with new presentation parameters.
    /// </summary>
    /// <param name="windowHandle">New window handle to present to.</param>
    /// <param name="preferredPresentParams">New preferred presentation parameters, if non-format values are out of range, they will be adjusted accordingly.</param>
    /// <exception cref="ArgumentException">Thrown if the window handle is not valid.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if the format is not valid for backbuffers.</exception>
    /// <remarks>See implementors for details on specific exceptions that can be thrown.</remarks>
    public void Reset(IntPtr windowHandle, PresentationParameters preferredPresentParams)
    {
      ValidateCreationParameters(Implementation.RenderSystem.Adapter, windowHandle, ref preferredPresentParams);

      try
      {
        SwapChainImpl.Reset(windowHandle, preferredPresentParams);
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw new TeslaGraphicsException(StringLocalizer.Instance.GetLocalizedString("ErrorResettingSwapChain"), e);
      }
    }

    /// <summary>
    /// Resizes the backbuffer.
    /// </summary>
    /// <param name="width">Width of the backbuffer.</param>
    /// <param name="height">Height of the backbuffer.</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if either width or height are out of range.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if there was an error in resizing the swapchain.</exception>
    /// <remarks>See implementors for details on specific exceptions that can be thrown.</remarks>
    public void Resize(int width, int height)
    {
      if (width == 0 && height == 0)
        return;

      int maxSize = RenderSystem.Adapter.MaximumTexture2DSize;

      if (width < 0 || width > maxSize)
        throw new ArgumentOutOfRangeException(nameof(width), StringLocalizer.Instance.GetLocalizedString("TextureDimensionOutOfRange"));

      if (height < 0 || height > maxSize)
        throw new ArgumentOutOfRangeException(nameof(height), StringLocalizer.Instance.GetLocalizedString("TextureDimensionOutOfRange"));

      try
      {
        SwapChainImpl.Resize(width, height);
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw new TeslaGraphicsException(StringLocalizer.Instance.GetLocalizedString("ErrorResizingSwapChain"), e);
      }
    }

    /// <summary>
    /// Toggles the swapchain to full screen exclusive mode.
    /// </summary>
    /// <returns>True if the swapchain is in full screen exclusive mode, false if otherwise.</returns>
    /// <exception cref="TeslaGraphicsException">Thrown if there was an error in toggling full screen mode.</exception>
    /// <remarks>See implementors for details on specific exceptions that can be thrown.</remarks>
    public bool ToggleFullScreen()
    {
      try
      {
        return SwapChainImpl.ToggleFullScreen();
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw new TeslaGraphicsException(StringLocalizer.Instance.GetLocalizedString("ErrorTogglingFullScreenSwapChain"), e);
      }
    }

    #region Creation Parameter Validation

    /// <summary>
    /// Validates creation parameters.
    /// </summary>
    /// <param name="adapter">Graphics adapter from the render system.</param>
    /// <param name="windowHandle">The window handle the swapchain will present to.</param>
    /// <param name="preferredPresentParams">The preferred presentation parameters, if non-format values are out of range, they will be adjusted accordingly.</param>
    /// <exception cref="ArgumentException">Thrown if the window handle is not valid.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if the format is not valid for backbuffers.</exception>
    protected void ValidateCreationParameters(IGraphicsAdapter adapter, IntPtr windowHandle, ref PresentationParameters preferredPresentParams)
    {
      if (windowHandle == IntPtr.Zero)
        throw new ArgumentException(nameof(windowHandle), StringLocalizer.Instance.GetLocalizedString("SwapChainWindowHandleNotValid"));

      if (!PresentationParameters.CheckPresentationParameters(adapter, ref preferredPresentParams))
        throw GraphicsExceptionHelper.NewBadBackBufferFormatException(preferredPresentParams.BackBufferFormat);
    }

    #endregion

    #region Implementation Creation

    private void CreateImplementation(IRenderSystem renderSystem, IntPtr windowHandle, ref PresentationParameters preferredPresentParams)
    {
      GraphicsHelper.ThrowIfRenderSystemNull(renderSystem);

      ValidateCreationParameters(renderSystem.Adapter, windowHandle, ref preferredPresentParams);

      ISwapChainImplFactory factory;
      if (!renderSystem.TryGetImplementationFactory<ISwapChainImplFactory>(out factory))
        throw GraphicsExceptionHelper.NewUnsupportedFeatureException(renderSystem, typeof(SwapChain));

      try
      {
        SwapChainImpl = factory.CreateImplementation(windowHandle, preferredPresentParams);
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw GraphicsExceptionHelper.NewErrorCreatingImplementation(typeof(SwapChain), e);
      }
    }

    #endregion
  }
}
