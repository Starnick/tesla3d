﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using Tesla.Content;
using Tesla.Graphics.Implementation;

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// Common base class for all texture resources.
  /// </summary>
  [SavableVersion(1)]
  public abstract class Texture : GraphicsResource, ISavable, IShaderResource
  {
    private static Texture1D? s_default1D;
    private static Texture2D? s_default2D;
    private static Texture3D? s_default3D;
    private static TextureCube? s_defaultCube;

    private static Texture1DArray? s_default1DArray;
    private static Texture2DArray? s_default2DArray;
    private static TextureCubeArray? s_defaultCubeArray;

    /// <summary>
    /// Gets the default <see cref="Texture1D"/>, which is a 1x white texture.
    /// </summary>
    public static Texture1D Default1D
    {
      get
      {
        return s_default1D!;
      }
    }

    /// <summary>
    /// Gets the default <see cref="Texture1DArray"/>, which is a single slice with 1x white texture.
    /// </summary>
    public static Texture1DArray Default1DArray
    {
      get
      {
        return s_default1DArray!;
      }
    }

    /// <summary>
    /// Gets the default <see cref="Texture2D"/>, which is a 1x1 white texture.
    /// </summary>
    public static Texture2D Default2D
    {
      get
      {
        return s_default2D!;
      }
    }

    /// <summary>
    /// Gets the default <see cref="Texture2DArray"/>, which is a single slice with 1x1 white texture.
    /// </summary>
    public static Texture2DArray Default2DArray
    {
      get
      {
        return s_default2DArray!;
      }
    }

    /// <summary>
    /// Gets the default <see cref="Texture3D"/>, which is a 1x1x1 white texture.
    /// </summary>
    public static Texture3D Default3D
    {
      get
      {
        return s_default3D!;
      }
    }

    /// <summary>
    /// Gets the default <see cref="TextureCube"/>, where every face is a 1x1 white texture.
    /// </summary>
    public static TextureCube DefaultCube
    {
      get
      {
        return s_defaultCube!;
      }
    }

    /// <summary>
    /// Gets the default <see cref="TextureCubeArray"/>, where every face is a 1x1 white texture.
    /// </summary>
    public static TextureCubeArray DefaultCubeArray
    {
      get
      {
        return s_defaultCubeArray!;
      }
    }

    /// <summary>
    /// Gets the format of a texel in the texture.
    /// </summary>
    public virtual SurfaceFormat Format { get { return TextureImpl.Format; } }

    /// <summary>
    /// Gets the number of mip map levels in the texture resource. Mip levels may be indexed in the range of [0, MipCount).
    /// </summary>
    public virtual int MipCount { get { return TextureImpl.MipCount; } }

    /// <summary>
    /// Gets the resource usage of the texture.
    /// </summary>
    public virtual ResourceUsage ResourceUsage { get { return TextureImpl.ResourceUsage; } }

    /// <summary>
    /// Gets the resource binding of the texture.
    /// </summary>
    public virtual ResourceBindFlags ResourceBindFlags { get { return TextureImpl.ResourceBindFlags; } }

    /// <summary>
    /// Gets the texture dimension, identifying the shape of the texture resoure.
    /// </summary>
    public abstract TextureDimension Dimension { get; }

    /// <summary>
    /// Gets the shader resource type.
    /// </summary>
    public abstract ShaderResourceType ResourceType { get; }

    /// <summary>
    /// Get the implementation as the common texture interface.
    /// </summary>
    protected ITextureImpl TextureImpl { get { return GetImplAs<ITextureImpl>(); } }

    //Wire up engine events
    static Texture()
    {
      CoreServices.RenderSystem.TrackedServiceChanged += CreateDefaultTextures;
      CoreServices.RenderSystem.EngineDestroying += DestroyDefaultTextures;
    }

    /// <summary>
    /// For ISavable.
    /// </summary>
    protected Texture() { }

    /// <summary>
    /// Reads the object data from the input.
    /// </summary>
    /// <param name="input">Savable reader</param>
    public abstract void Read(ISavableReader input);

    /// <summary>
    /// Writes the object data to the output.
    /// </summary>
    /// <param name="output">Savable writer</param>
    public abstract void Write(ISavableWriter output);

    /// <summary>
    /// Checks if the number is a power of two.
    /// </summary>
    /// <param name="num">Integer value</param>
    /// <returns>True if the number is a power of two, false otherwise.</returns>
    public static bool IsPowerOfTwo(int num)
    {
      return (num != 0) && ((num & (num - 1)) == 0);
    }

    /// <summary>
    /// Gets the previous power of two value.
    /// </summary>
    /// <param name="v">Previous value.</param>
    /// <returns>Previous power of two.</returns>
    public static int PreviousPowerOfTwo(int v)
    {
      return NextPowerOfTwo(v + 1) / 2;
    }

    /// <summary>
    /// Gets the nearest power of two value.
    /// </summary>
    /// <param name="v">Starting value.</param>
    /// <returns>Nearest power of two.</returns>
    public static int NearestPowerOfTwo(int v)
    {
      int np2 = NextPowerOfTwo(v);
      int pp2 = PreviousPowerOfTwo(v);

      if (np2 - v <= v - pp2)
        return np2;
      else
        return pp2;
    }

    /// <summary>
    /// Get the next power of two value.
    /// </summary>
    /// <param name="v">Starting value.</param>
    /// <returns>Next power of two.</returns>
    public static int NextPowerOfTwo(int v)
    {
      int p = 1;
      while (v > p)
        p += p;

      return p;
    }

    /// <summary>
    /// Calculates the number of mip map levels for the given 1D width.
    /// </summary>
    /// <param name="width">Width of the texture, in texels.</param>
    /// <returns>The number of mip map levels.</returns>
    public static int CalculateMipMapCount(int width)
    {
      return CalculateMipMapCount(width, 1, 1);
    }

    /// <summary>
    /// Calculates the number of mip map levels for the given 2D width/height.
    /// </summary>
    /// <param name="width">Width of the texture, in texels.</param>
    /// <param name="height">Height of the texture, in texels.</param>
    /// <returns>The number of mip map levels.</returns>
    public static int CalculateMipMapCount(int width, int height)
    {
      return CalculateMipMapCount(width, height, 1);
    }

    /// <summary>
    /// Calculates the number of mip map levels for the given 3D width/height/depth.
    /// </summary>
    /// <param name="width">Width of the texture, in texels.</param>
    /// <param name="height">Height of the texture, in texels.</param>
    /// <param name="depth">Depth of the texture, in texels.</param>
    /// <returns>The number of mip map levels.</returns>
    public static int CalculateMipMapCount(int width, int height, int depth)
    {
      int max = Math.Max(Math.Max(width, height), depth);

      if (max == 0)
        return 0;

      return 1 + (int) Math.Floor((Math.Log(max) / Math.Log(2)));
    }

    /// <summary>
    /// Calculates the uncompressed width of the mip map level, given the mip level index and the width of the texture's
    /// first mip level.
    /// </summary>
    /// <param name="mipLevel">Zero-based index of the mip level.</param>
    /// <param name="width">Width of the texture at the first mip level. This will hold the resulting width at the specified mip level.</param>
    public static void CalculateMipLevelDimensions(int mipLevel, ref int width)
    {
      int height = 1;
      int depth = 1;
      CalculateMipLevelDimensions(mipLevel, ref width, ref height, ref depth);
    }

    /// <summary>
    /// Calculates the uncompressed width/height of the mip map level, given the mip level index and the width/height of the texture's
    /// first mip level.
    /// </summary>
    /// <param name="mipLevel">Zero-based index of the mip level.</param>
    /// <param name="width">Width of the texture at the first mip level. This will hold the resulting width at the specified mip level.</param>
    /// <param name="height">Height of the texture at the first mip level. This will hold the resulting height at the specified mip level.</param>
    public static void CalculateMipLevelDimensions(int mipLevel, ref int width, ref int height)
    {
      int depth = 1;
      CalculateMipLevelDimensions(mipLevel, ref width, ref height, ref depth);
    }

    /// <summary>
    /// Calculates the uncompressed width/height/depth of the mip map level, given the mip level index and the width/height/depth of the texture's
    /// first mip level.
    /// </summary>
    /// <param name="mipLevel">Zero-based index of the mip level.</param>
    /// <param name="width">Width of the texture at the first mip level. This will hold the resulting width at the specified mip level.</param>
    /// <param name="height">Height of the texture at the first mip level. This will hold the resulting height at the specified mip level.</param>
    /// <param name="depth">Depth of the texture at the first mip level. This will hold the resulting depth at the specified mip level.</param>
    public static void CalculateMipLevelDimensions(int mipLevel, ref int width, ref int height, ref int depth)
    {
      if (mipLevel < 0)
        return;

      width = Math.Max(1, width >> mipLevel);
      height = Math.Max(1, height >> mipLevel);
      depth = Math.Max(1, depth >> mipLevel);
    }

    /// <summary>
    /// Calculates the size of the specified mip level in bytes.
    /// </summary>
    /// <param name="mipLevel">Zero-based index of the mip level.</param>
    /// <param name="width">Width of the texture at the first mip level.</param>
    /// <param name="format">Format of the texture. 1D textures cannot be block compressed, so those are invalid formats.</param>
    /// <returns>The size of the mip level in bytes, if the calculation was not valid, zero is returned</returns>
    public static int CalculateMipLevelSizeInBytes(int mipLevel, int width, SurfaceFormat format)
    {
      if (mipLevel < 0 || format.IsCompressedFormat())
        return 0;

      CalculateMipLevelDimensions(mipLevel, ref width);

      int formatSize;
      return CalculateRegionSizeInBytes(format, ref width, out formatSize);
    }

    /// <summary>
    /// Calculates the size of the specified mip level in bytes.
    /// </summary>
    /// <param name="mipLevel">Zero-based index of the mip level.</param>
    /// <param name="width">Width of the texture at the first mip level. </param>
    /// <param name="height">Height of the texture at the first mip level. </param>
    /// <param name="format">Format of the texture.</param>
    /// <returns>The size of the mip level in bytes, if the calculation was not valid, zero is returned</returns>
    public static int CalculateMipLevelSizeInBytes(int mipLevel, int width, int height, SurfaceFormat format)
    {
      return CalculateMipLevelSizeInBytes(mipLevel, width, height, 1, format);
    }

    /// <summary>
    /// Calculates the size of the specified mip level in bytes.
    /// </summary>
    /// <param name="mipLevel">Zero-based index of the mip level.</param>
    /// <param name="width">Width of the texture at the first mip level.</param>
    /// <param name="height">Height of the texture at the first mip level.</param>
    /// <param name="depth">Depth of the texture at the first mip level.</param>
    /// <param name="format">Format of the texture.</param>
    /// <returns>The size of the mip level in bytes, if the calculation was not valid, zero is returned</returns>
    public static int CalculateMipLevelSizeInBytes(int mipLevel, int width, int height, int depth, SurfaceFormat format)
    {
      if (mipLevel < 0)
        return 0;

      CalculateMipLevelDimensions(mipLevel, ref width, ref height, ref depth);

      int formatSize;
      return CalculateRegionSizeInBytes(format, ref width, ref height, depth, out formatSize);
    }

    /// <summary>
    /// Calculates the data size in bytes, given a subimage region.
    /// </summary>
    /// <param name="format">Format of the texture. 1D textures cannot be block compressed, so those are invalid formats.</param>
    /// <param name="width">Width of the subimage region.</param>
    /// <param name="formatSize">Holds the format size upon method return. If the surface format is compressed, this will be zero.</param>
    /// <returns>The size of the subimage region for the given format, in bytes. If the surface format is compressed, this will be zero.</returns>
    public static int CalculateRegionSizeInBytes(SurfaceFormat format, ref int width, out int formatSize)
    {
      if (format.IsCompressedFormat())
      {
        formatSize = 0;
        return 0;
      }

      formatSize = format.SizeInBytes();
      return width * formatSize;
    }

    /// <summary>
    /// Calculates the data size in bytes, given a subimage region. For compressed surface formats, the region is adjusted appropiately.
    /// </summary>
    /// <param name="format">Format of the texture.</param>
    /// <param name="width">Width of the subimage region. This will be modified if the format is compressed.</param>
    /// <param name="height">Height of the subimage region. This will be modified if the format is compressed.</param>
    /// <param name="formatSize">Holds the format size upon method return.</param>
    /// <returns>The size of the subimage region for the given format, in bytes.</returns>
    public static int CalculateRegionSizeInBytes(SurfaceFormat format, ref int width, ref int height, out int formatSize)
    {
      if (format.IsCompressedFormat())
      {
        return CalculateCompressedDimensions(format, ref width, ref height, 1, out formatSize);
      }
      else
      {
        formatSize = format.SizeInBytes();
        return width * height * format.SizeInBytes();
      }
    }

    /// <summary>
    /// Calculates the data size in bytes, given a subimage region. For compressed surface formats, the region is adjusted appropiately.
    /// </summary>
    /// <param name="format">Format of the texture.</param>
    /// <param name="width">Width of the subimage region. This will be modified if the format is compressed.</param>
    /// <param name="height">Height of the subimage region. This will be modified if the format is compressed.</param>
    /// <param name="depth">Depth of the subimage region.</param>
    /// <param name="formatSize">Holds the format size upon method return.</param>
    /// <returns>The size of the subimage region for the given format, in bytes.</returns>
    public static int CalculateRegionSizeInBytes(SurfaceFormat format, ref int width, ref int height, int depth, out int formatSize)
    {
      if (format.IsCompressedFormat())
      {
        return CalculateCompressedDimensions(format, ref width, ref height, depth, out formatSize);
      }
      else
      {
        formatSize = format.SizeInBytes();
        return width * height * depth * formatSize;
      }
    }

    /// <summary>
    /// Calculates the compressed width/height of a texture given the format and uncompressed width/height.
    /// </summary>
    /// <param name="format">Compression format.</param>
    /// <param name="width">Uncompressed width of the texture. This will hold the resulting compressed width.</param>
    /// <param name="height">Uncompressed height of the texture. This will hold the resulting compressed height.</param>
    /// <param name="formatSize">The format size of the compression format, in bytes.</param>
    /// <returns>If the format was invalid, a value of zero is returned. Otherwise, the total compressed texture size, in bytes, is returned.</returns>
    public static int CalculateCompressedDimensions(SurfaceFormat format, ref int width, ref int height, out int formatSize)
    {
      return CalculateCompressedDimensions(format, ref width, ref height, 1, out formatSize);
    }

    /// <summary>
    /// Calculates the compressed width/height/depth of a texture given the format and uncompressed width/height.
    /// </summary>
    /// <param name="format">Compression format.</param>
    /// <param name="width">Uncompressed width of the texture. This will hold the resulting compressed width.</param>
    /// <param name="height">Uncompressed height of the texture. This will hold the resulting compressed height.</param>
    /// <param name="depth">Depth of the texture.</param>
    /// <param name="formatSize">The format size of the compression format, in bytes.</param>
    /// <returns>If the format was invalid, a value of zero is returned. Otherwise, the total compressed texture size, in bytes, is returned.</returns>
    public static int CalculateCompressedDimensions(SurfaceFormat format, ref int width, ref int height, int depth, out int formatSize)
    {
      formatSize = 0;

      if (format == SurfaceFormat.DXT1 || format == SurfaceFormat.DXT3 || format == SurfaceFormat.DXT5)
      {
        width = Math.Max(1, (width + 3) / 4);
        height = Math.Max(1, (height + 3) / 4);

        formatSize = (format == SurfaceFormat.DXT1) ? 8 : 16;

        return width * height * depth * formatSize;
      }

      return 0;
    }

    /// <summary>
    /// Calculates the sub resource index of a resource at the specified array and mip index, given the number of mip levels
    /// the resource contains.
    /// </summary>
    /// <param name="arraySlice">Zero-based array index.</param>
    /// <param name="mipSlice">Zero-based mip level index.</param>
    /// <param name="numMipLevels">Number of mip levels.</param>
    /// <returns>The calculated subresource index.</returns>
    public static int CalculateSubResourceIndex(int arraySlice, int mipSlice, int numMipLevels)
    {
      return (numMipLevels * arraySlice) + mipSlice;
    }

    /// <summary>
    /// Generate mipmaps for the texture.
    /// </summary>
    /// <param name="texture">Texture to generate mipmaps for.</param>
    /// <returns>True if mipmaps were generated, false otherwise.</returns>
    public static bool GenerateMipMaps(Texture texture)
    {
      if (texture is null)
        return false;

      IRenderSystem renderSystem = texture.RenderSystem;
      IGenerateMipMapsExtension? genMipMaps = renderSystem.ImmediateContext.GetExtension<IGenerateMipMapsExtension>();

      if (genMipMaps is null)
        return false;

      return genMipMaps.GenerateMipMaps(texture);
    }

    /// <summary>
    /// Creates a texture object populated with the specified texture data.
    /// </summary>
    /// <param name="texData">Surface data.</param>
    /// <param name="renderSystem">Render system to create the GPU resources.</param>
    /// <param name="resourceUsage">Type of usage the GPU resources are expected to be.</param>
    /// <returns>GPU texture, or null if it could not be created.</returns>
    public static Texture? From(TextureData texData, IRenderSystem renderSystem, ResourceUsage resourceUsage = ResourceUsage.Immutable)
    {
      if (!texData.Validate() || renderSystem is null || texData.MipChains is null)
        return null;

      int mipCount = texData.MipCount;
      int arrayCount = texData.ArrayCount;

      // Should match incoming mip count, but it is valid if they don't completely define all mips. The only caveat is all
      // mipmap chains should have the same number of mips.
      int expectedMipCount = 1;
      bool genMipMaps = mipCount > 1;
      if (genMipMaps)
        expectedMipCount = ComputeExpectedMipCount(texData);

      using (PooledArray<IReadOnlyDataBuffer?> surfaces = new PooledArray<IReadOnlyDataBuffer?>(arrayCount * expectedMipCount))
      {
        FlattenSurfaces(texData.MipChains, expectedMipCount, surfaces);
        TextureOptions options = TextureOptions.Init(surfaces, genMipMaps, texData.Format, resourceUsage);

        switch (texData.TextureType)
        {
          case TextureType.Texture1D:
            return new Texture1D(renderSystem, (int) texData.Width, options);
          case TextureType.Texture1DArray:
            return new Texture1DArray(renderSystem, (int) texData.Width, arrayCount, options);
          case TextureType.Texture2D:
            return new Texture2D(renderSystem, (int) texData.Width, (int) texData.Height, options);
          case TextureType.Texture2DArray:
            return new Texture2DArray(renderSystem, (int) texData.Width, (int) texData.Height, arrayCount, options);
          case TextureType.Texture3D:
            return new Texture3D(renderSystem, (int) texData.Width, (int) texData.Height, (int) texData.Depth, options);
          case TextureType.TextureCube:
            return new TextureCube(renderSystem, (int) texData.Width, options);
          case TextureType.TextureCubeArray:
            return new TextureCubeArray(renderSystem, (int) texData.Width, arrayCount, options);
          default:
            return null;
        }
      }
    }

    private static int ComputeExpectedMipCount(in TextureData data)
    {
      switch(data.TextureType)
      {
        case TextureType.Texture1D:
        case TextureType.Texture1DArray:
          return Texture.CalculateMipMapCount((int) data.Width);
        case TextureType.Texture2D:
        case TextureType.Texture2DArray:
        case TextureType.TextureCube:
        case TextureType.TextureCubeArray:
          return Texture.CalculateMipMapCount((int) data.Width, (int) data.Height);
        case TextureType.Texture3D:
          return Texture.CalculateMipMapCount((int) data.Width, (int) data.Height, (int) data.Depth);
        default:
          throw new ArgumentOutOfRangeException(nameof(data.TextureType));
      }
    }

    private static void FlattenSurfaces(List<TextureData.MipChain> data, int expectedMipCount, Span<IReadOnlyDataBuffer?> initialTextureData)
    {
      for (int arraySlice = 0; arraySlice < data.Count; arraySlice++)
      {
        TextureData.MipChain chain = data[arraySlice];
        if (chain is null)
          continue;

        for (int mipSlice = 0; mipSlice < chain.Count; mipSlice++)
        {
          int index = CalculateSubResourceIndex(arraySlice, mipSlice, expectedMipCount);
          initialTextureData[index] = chain[mipSlice].Data;
        }
      }
    }

    #region Validation

    /// <summary>
    /// Block compression requires a minimum of a 4x4 pixel size or multiples of 4. 4x4, 2x2, 1x1 sized textures all are the same size so we just treat them
    /// all as 4x4. This enforces the minimum and validates the dimensions are multiples of 4.
    /// </summary>
    /// <param name="format">Format of the texture.</param>
    /// <param name="size">Square size dimension of the texture.</param>
    protected void CheckBlockCompressionMinimumSize(SurfaceFormat format, ref int size)
    {
      //Block compression formats need to be in multiples of 4, which means the minimum size is 4. Textures that are technically 4x4, 2x2, 1x1 all are the same
      //size. Most likely we'll see these as the lowest levels of the mipchain.
      if (format.IsCompressedFormat())
      {
        //Ensure minimum is 4
        size = Math.Max(4, size);

        //Ensure dimensions are multiples of 4
        if ((size % 4) != 0)
          throw GraphicsExceptionHelper.NewBlockCompressionMultipleOfFourException(format);
      }
    }

    /// <summary>
    /// Block compression requires a minimum of a 4x4 pixel size or multiples of 4. 4x4, 2x2, 1x1 sized textures all are the same size so we just treat them
    /// all as 4x4. This enforces the minimum and validates the dimensions are multiples of 4.
    /// </summary>
    /// <param name="format">Format of the texture.</param>
    /// <param name="width">Width of the texture.</param>
    /// <param name="height">Height of the texture.</param>
    protected void CheckBlockCompressionMinimumSize(SurfaceFormat format, ref int width, ref int height)
    {
      //Block compression formats need to be in multiples of 4, which means the minimum size is 4. Textures that are technically 4x4, 2x2, 1x1 all are the same
      //size. Most likely we'll see these as the lowest levels of the mipchain.
      if (format.IsCompressedFormat())
      {
        //Ensure minimum is 4
        width = Math.Max(4, width);
        height = Math.Max(4, height);

        //Ensure dimensions are multiples of 4
        if ((width % 4) != 0 || (height % 4) != 0)
          throw GraphicsExceptionHelper.NewBlockCompressionMultipleOfFourException(format);
      }
    }

    /// <summary>
    /// Validates common creation parameters.
    /// </summary>
    /// <param name="mipCount">Number of mip levels in the texture.</param>
    /// <param name="options">Common <see cref="TextureOptions"/> that may also contain initial data.</param>
    /// <param name="arrayCount">Number of array slices in the texture array, specify 1 for non-array.</param>
    /// <exception cref="TeslaGraphicsException">Thrown if the a combination of the input parameters are invalid.</exception>
    protected void ValidateCommonCreationParameters(TextureOptions options, int mipCount, int arrayCount = 1)
    {
      if (options.ResourceUsage == ResourceUsage.Dynamic)
      {
        if (arrayCount > 1 || mipCount > 1)
          throw GraphicsExceptionHelper.NewDynamicResourceMustBeSingleSubresource();
      }
    }

    #endregion

    #region Default texture creation

    private static void CreateDefaultTextures(Engine engine, EngineServiceChangedEventArgs args)
    {
      if (!args.IsService<IRenderSystem>())
        return;

      IRenderSystem renderSystem = args.ServiceAs<IRenderSystem>();
      
      // Dispose of textures if they were previously allocated
      if (args.ChangeEventType == ServiceChangeEventType.Replaced)
        DestroyDefaultTextures(engine, EventArgs.Empty);

      EngineLog.Log(LogLevel.Info, "Creating Default Textures");

      using (DataBuffer<Color> singleColor = DataBuffer.Create<Color>(new ReadOnlySpan<Color>(Color.White), MemoryAllocatorStrategy.DefaultPooled))
      {
        TextureOptions options = TextureOptions.Init(singleColor, SurfaceFormat.Color, ResourceUsage.Immutable);

        s_default1D = new Texture1D(renderSystem, 1, options);
        s_default1DArray = new Texture1DArray(renderSystem, 1, 1, options);
        s_default2D = new Texture2D(renderSystem, 1, 1, options);
        s_default2DArray = new Texture2DArray(renderSystem, 1, 1, 1, options);
        s_default3D = new Texture3D(renderSystem, 1, 1, 1, options);

        // Set all 6 faces for cubemaps to the same white color
        using (PooledArray<IReadOnlyDataBuffer?> surfaces = new PooledArray<IReadOnlyDataBuffer?>(singleColor, singleColor, singleColor, singleColor, singleColor, singleColor))
        {
          options = TextureOptions.Init(surfaces, SurfaceFormat.Color, ResourceUsage.Immutable);
          s_defaultCube = new TextureCube(renderSystem, 1, options);
          s_defaultCubeArray = new TextureCubeArray(renderSystem, 1, 1, options);
        }
      }
    }

    private static void DestroyDefaultTextures(Engine engine, EventArgs args)
    {
      EngineLog.Log(LogLevel.Info, "Destroying Default Textures");

      if (s_default1D is not null)
      {
        s_default1D.Dispose();
        s_default1D = null;
      }

      if (s_default1DArray is not null)
      {
        s_default1DArray.Dispose();
        s_default1DArray = null;
      }

      if (s_default2D is not null)
      {
        s_default2D.Dispose();
        s_default2D = null;
      }

      if (s_default2DArray is not null)
      {
        s_default2DArray.Dispose();
        s_default2DArray = null;
      }

      if (s_default3D is not null)
      {
        s_default3D.Dispose();
        s_default3D = null;
      }

      if (s_defaultCube is not null)
      {
        s_defaultCube.Dispose();
        s_defaultCube = null;
      }

      if (s_defaultCubeArray is not null)
      {
        s_defaultCubeArray.Dispose();
        s_defaultCubeArray = null;
      }
    }

    #endregion
  }
}
