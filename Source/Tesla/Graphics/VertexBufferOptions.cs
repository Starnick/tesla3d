﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// Defines vertex buffer creation options.
  /// </summary>
  public ref struct VertexBufferOptions
  {
    /// <summary>
    /// Resource usage specifying the type of memory the buffer should use. If <see cref="ResourceUsage.Immutable"/> then initial data
    /// is required as the buffer cannot be modified once created. Default is <see cref="ResourceUsage.Static"/>.
    /// </summary>
    public ResourceUsage ResourceUsage;

    /// <summary>
    /// Resource binding flags specifying in what other ways the buffer can be bound to the graphics pipeline. Default is <see cref="ResourceBindFlags.Default"/>.
    /// </summary>
    public ResourceBindFlags ResourceBindFlags;

    /// <summary>
    /// Initial data to populate the vertex buffer. This must have at least one buffer if the usage is <see cref="ResourceUsage.Immutable"/>, otherwise it is 
    /// free to be empty. If multiple buffers, each buffer needs to be the same length and are expected to correspond to the vertex layout of the buffer (e.g. positions in one buffer, normals in another, etc) and will be
    /// assembled into an interleaved buffer at initialization. Default is an empty span.
    /// </summary>
    public ReadOnlySpan<IReadOnlyDataBuffer> Data;

    /// <summary>
    /// Constructs a new instance of the <see cref="BufferOptions"/> struct.
    /// </summary>
    /// <param name="usage">Resource usage specifying the type of memory the buffer should use.</param>
    /// <param name="bindFlags">Resource binding flags specifying in what other ways the buffer can be bound to the graphics pipeline.</param>
    /// <param name="data">Data to initialize the buffer with. If the buffer is immutable, this is required, but may be empty for other usage types.</param>
    public VertexBufferOptions(ResourceUsage usage, ResourceBindFlags bindFlags, ReadOnlySpan<IReadOnlyDataBuffer> data)
    {
      ResourceUsage = usage;
      ResourceBindFlags = bindFlags;
      Data = data;
    }

    /// <summary>
    /// Creates default buffer options for an empty buffer.
    /// </summary>
    /// <param name="usage">Optional resource usage. If <see cref="ResourceUsage.Immutable"/> then initial data must is required as the buffer cannot be modified once created.</param>
    /// <param name="bindFlags">Optional binding flags.</param>
    /// <returns>VertexBuffer options.</returns>
    public static VertexBufferOptions Init(ResourceUsage usage = ResourceUsage.Static, ResourceBindFlags bindFlags = ResourceBindFlags.Default)
    {
      return new VertexBufferOptions(usage, bindFlags, ReadOnlySpan<IReadOnlyDataBuffer>.Empty);
    }

    /// <summary>
    /// Creates default buffer options for a buffer with initial data.
    /// </summary>
    /// <param name="data">Data to initialize the buffer with.</param>
    /// <param name="usage">Optional resource usage. If <see cref="ResourceUsage.Immutable"/> then initial data must is required as the buffer cannot be modified once created.</param>
    /// <param name="bindFlags">Optional binding flags.</param>
    /// <returns>VertexBuffer options.</returns>
    public static VertexBufferOptions Init(in IReadOnlyDataBuffer data, ResourceUsage usage = ResourceUsage.Static, ResourceBindFlags bindFlags = ResourceBindFlags.Default)
    {
      return new VertexBufferOptions(usage, bindFlags, new ReadOnlySpan<IReadOnlyDataBuffer>(data));
    }

    /// <summary>
    /// Creates default buffer options for a buffer with initial data.
    /// </summary>
    /// <param name="data">Data to initialize the buffer with.</param>
    /// <param name="usage">Optional resource usage. If <see cref="ResourceUsage.Immutable"/> then initial data must is required as the buffer cannot be modified once created.</param>
    /// <param name="bindFlags">Resource bind flags specifying in what other ways the buffer can be bound to the graphics pipeline.</param>
    /// <returns>VertexBuffer options.</returns>
    public static VertexBufferOptions Init(in IReadOnlyDataBuffer data, ResourceBindFlags bindFlags)
    {
      return new VertexBufferOptions(ResourceUsage.Static, bindFlags, new ReadOnlySpan<IReadOnlyDataBuffer>(data));
    }

    /// <summary>
    /// Creates default buffer options for a buffer with multiple attributes as initial data.
    /// </summary>
    /// <param name="data">Data to initialize the buffer with.</param>
    /// <param name="usage">Optional resource usage. If <see cref="ResourceUsage.Immutable"/> then initial data must is required as the buffer cannot be modified once created.</param>
    /// <param name="bindFlags">Optional binding flags.</param>
    /// <returns>VertexBuffer options.</returns>
    public static VertexBufferOptions Init(ReadOnlySpan<IReadOnlyDataBuffer> data, ResourceUsage usage = ResourceUsage.Static, ResourceBindFlags bindFlags = ResourceBindFlags.Default)
    {
      return new VertexBufferOptions(usage, bindFlags, data);
    }

    /// <summary>
    /// Creates default buffer options for a buffer with multiple attributes as initial data.
    /// </summary>
    /// <param name="data">Data to initialize the buffer with.</param>
    /// <param name="bindFlags">Resource bind flags specifying in what other ways the buffer can be bound to the graphics pipeline.</param>
    /// <returns>VertexBuffer options.</returns>
    public static VertexBufferOptions Init(ReadOnlySpan<IReadOnlyDataBuffer> data, ResourceBindFlags bindFlags)
    {
      return new VertexBufferOptions(ResourceUsage.Static, bindFlags, data);
    }

    /// <summary>
    /// Convience conversion operator to create a default buffer options with the specified usage.
    /// </summary>
    /// <param name="usage">Resource usage.</param>
    public static implicit operator VertexBufferOptions(ResourceUsage usage)
    {
      return Init(usage);
    }

    /// <summary>
    /// Convience conversion operator to create a default buffer options with the specified binding flags.
    /// </summary>
    /// <param name="bindFlags">Resource binding flags specifying in what other ways the buffer can be bound to the graphics pipeline.</param>
    public static implicit operator VertexBufferOptions(ResourceBindFlags bindFlags)
    {
      return Init(ResourceUsage.Static, bindFlags);
    }
  }
}
