﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// Defines compression mode for data.
  /// </summary>
  public enum DataCompressionMode : byte
  {
    /// <summary>
    /// No compression.
    /// </summary>
    None = 0,

    /// <summary>
    /// GZIP compression. <see cref="System.IO.Compression.GZipStream"/>.
    /// </summary>
    Gzip = 1
  }

  /// <summary>
  /// Defines the type of texture.
  /// </summary>
  public enum TextureType : byte
  {
    /// <summary>
    /// 1D texture (X width, 1 height)
    /// </summary>
    Texture1D = 0,

    /// <summary>
    /// 1D texture array (N array of x width, 1 height)
    /// </summary>
    Texture1DArray = 1,

    /// <summary>
    /// 2D texture (X width, Y height)
    /// </summary>
    Texture2D = 2,

    /// <summary>
    /// 2D texture array (N array of X width, Y height)
    /// </summary>
    Texture2DArray = 3,

    /// <summary>
    /// 3D texture (X width, Y height, Z depth).
    /// </summary>
    Texture3D = 4,

    /// <summary>
    /// Cube texture (6 array of X width, X height), similar to a 2D texture array, but with exactly 6 faces. Each face is a square texture.
    /// </summary>
    TextureCube = 5,

    /// <summary>
    /// Cube texture array (N * 6 array of X width, X height), similar to a 2D texture array but each cubemap has exactly 6 faces. Each face is a square texture.
    /// </summary>
    TextureCubeArray = 6,
  }
}
