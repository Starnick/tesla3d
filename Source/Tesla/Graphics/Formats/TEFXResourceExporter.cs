﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System.IO;
using Tesla.Content;

// TODO NULLABLE

namespace Tesla.Graphics
{
  /// <summary>
  /// A resource exporter that can save <see cref="Effect"/> objects to the TEFX (Tesla Engine Effects) format.
  /// </summary>
  public sealed class TEFXResourceExporter : ResourceExporter<Effect>
  {

    /// <summary>
    /// Constructs a new instance of the <see cref="TEFXResourceExporter"/> class.
    /// </summary>
    public TEFXResourceExporter() : base(".tefx") { }

    /// <summary>
    /// Writes the specified content to a resource file.
    /// </summary>
    /// <param name="resourceFile">Resource file to write to</param>
    /// <param name="content">Content to write</param>
    /// <exception cref="ArgumentNullException">Thrown if either the resource file or content are null.</exception>
    public override void Save(IResourceFile resourceFile, Effect content)
    {
      ValidateParameters(resourceFile, content);

      using (Stream output = resourceFile.Create())
      {
        byte[] effectData = content.EffectByteCode;
        output.Write(effectData, 0, effectData.Length);
      }
    }

    /// <summary>
    /// Writes the specified content to a stream.
    /// </summary>
    /// <param name="output">Output stream to write to</param>
    /// <param name="content">Content to write</param>
    /// <exception cref="ArgumentNullException">Thrown if output stream/content is null or if the stream is not writeable.</exception>
    public override void Save(Stream output, Effect content)
    {
      ValidateParameters(output, content);

      byte[] effectData = content.EffectByteCode;
      output.Write(effectData, 0, effectData.Length);
    }
  }
}
