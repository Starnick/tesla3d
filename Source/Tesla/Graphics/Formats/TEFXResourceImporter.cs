﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System.IO;
using Tesla.Content;

// TODO NULLABLE

namespace Tesla.Graphics
{
  /// <summary>
  /// A resource importer that can load <see cref="Effect"/> objects from the TEFX (Tesla Engine Effects) format.
  /// </summary>
  public sealed class TEFXResourceImporter : ResourceImporter<Effect>
  {
    /// <summary>
    /// Constructs a new instance of the <see cref="TEFXResourceImporter"/> class.
    /// </summary>
    public TEFXResourceImporter() : base(".tefx") { }

    /// <summary>
    /// Loads content from the specified resource as the target runtime type.
    /// </summary>
    /// <param name="resourceFile">Resource file to read from</param>
    /// <param name="contentManager">Calling content manager</param>
    /// <param name="parameters">Optional loading parameters</param>
    /// <returns>The loaded object or null if it could not be loaded</returns>
    public override Effect Load(IResourceFile resourceFile, ContentManager contentManager, ImporterParameters parameters)
    {
      ValidateParameters(resourceFile, contentManager, ref parameters);

      using (Stream stream = resourceFile.OpenRead())
      {
        byte[] effectData = BufferHelper.ReadStreamFully(stream, 4096);
        if (effectData == null || effectData.Length == 0)
          return null;

        return new Effect(GraphicsHelper.GetRenderSystem(contentManager.ServiceProvider), effectData);
      }
    }

    /// <summary>
    /// Loads content from the specified stream as the target runtime type.
    /// </summary>
    /// <param name="input">Stream to read from.</param>
    /// <param name="contentManager">Calling content manager.</param>
    /// <param name="parameters">Optional loading parameters.</param>
    /// <returns>The loaded object or null if it could not be loaded.</returns>
    public override Effect Load(Stream input, ContentManager contentManager, ImporterParameters parameters)
    {
      ValidateParameters(input, contentManager, ref parameters);

      byte[] effectData = BufferHelper.ReadStreamFully(input, 4096);
      if (effectData == null || effectData.Length == 0)
        return null;

      return new Effect(GraphicsHelper.GetRenderSystem(contentManager.ServiceProvider), effectData);
    }
  }
}
