﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System.IO;
using Tesla.Content;

// TODO NULLABLE

namespace Tesla.Graphics
{
  /// <summary>
  /// A resource importer that can load <see cref="Texture"/> objects from the TEXU (Tesla Engine Texture) format.
  /// </summary>
  public sealed class TEXUResourceImporter : ResourceImporter<Texture>
  {
    /// <summary>
    /// Constructs a new instance of the <see cref="TEXUResourceImporter"/> class.
    /// </summary>
    public TEXUResourceImporter() : base(".texu") { }

    /// <summary>
    /// Loads content from the specified resource as the target runtime type.
    /// </summary>
    /// <param name="resourceFile">Resource file to read from</param>
    /// <param name="contentManager">Calling content manager</param>
    /// <param name="parameters">Optional loading parameters</param>
    /// <returns>The loaded object or null if it could not be loaded</returns>
    public override Texture Load(IResourceFile resourceFile, ContentManager contentManager, ImporterParameters parameters)
    {
      ValidateParameters(resourceFile, contentManager, ref parameters);

      using (Stream stream = resourceFile.OpenRead())
      {
        TextureData texData = TextureData.Read(stream);
        IRenderSystem renderSystem = GraphicsHelper.GetRenderSystem(contentManager.ServiceProvider);

        return Texture.From(texData, renderSystem, ResourceUsage.Immutable);
      }
    }

    /// <summary>
    /// Loads content from the specified stream as the target runtime type.
    /// </summary>
    /// <param name="input">Stream to read from.</param>
    /// <param name="contentManager">Calling content manager.</param>
    /// <param name="parameters">Optional loading parameters.</param>
    /// <returns>The loaded object or null if it could not be loaded.</returns>
    public override Texture Load(Stream input, ContentManager contentManager, ImporterParameters parameters)
    {
      ValidateParameters(input, contentManager, ref parameters);

      TextureData texData = TextureData.Read(input);
      IRenderSystem renderSystem = GraphicsHelper.GetRenderSystem(contentManager.ServiceProvider);

      return Texture.From(texData, renderSystem, ResourceUsage.Immutable);
    }
  }
}
