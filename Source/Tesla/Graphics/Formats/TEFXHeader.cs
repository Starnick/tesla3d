﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Tesla.Content;

// TODO NULLABLE

namespace Tesla.Graphics
{
  /// <summary>
  /// Represents the header of the TEFX (Tesla Engine Effect) format. The format is a binary blob
  /// representing a set of techniques and compiled shaders that determines how the rendering pipeline is configured.
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public struct TEFXHeader : IEquatable<TEFXHeader>, IPrimitiveValue
  {
    private static readonly FourCC s_tefxMagicNumber = new FourCC('T', 'E', 'F', 'X');
    private static readonly int s_sizeInBytes = BufferHelper.SizeOf<TEFXHeader>();

    /// <summary>
    /// The MagicNumber that identifies the format, this is always a four character code of "TEFX".
    /// </summary>
    public FourCC MagicNumber;

    /// <summary>
    /// The runtime ID denoting which effect system the data is compatible with.
    /// </summary>
    public byte RuntimeID;

    /// <summary>
    /// The Version of the format.
    /// </summary>
    public ushort Version;

    /// <summary>
    /// Reserved.
    /// </summary>
    public uint Reserved;

    /// <summary>
    /// Flags denoting compression of the data, if any.
    /// </summary>
    public DataCompressionMode CompressionMode;

    /// <summary>
    /// Size of the data in bytes.
    /// </summary>
    public uint DataSizeInBytes;

    /// <summary>
    /// Gets the "TEFX" magic number four character code identifying the format.
    /// </summary>
    public static FourCC TEFXMagicNumber
    {
      get
      {
        return s_tefxMagicNumber;
      }
    }

    /// <summary>
    /// Gets the size in bytes of the <see cref="TEFXHeader"/> structure in bytes.
    /// </summary>
    public static int SizeInBytes
    {
      get
      {
        return s_sizeInBytes;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="TEFXHeader"/> struct.
    /// </summary>
    /// <param name="runtimeID">Runtime ID of the effects system that the data is compatible with.</param>
    /// <param name="version">Format version</param>
    /// <param name="compressionMode">Compression mode of the data, if any.</param>
    /// <param name="dataSizeInBytes">The size of the data, in bytes.</param>
    public TEFXHeader(byte runtimeID, ushort version, DataCompressionMode compressionMode, uint dataSizeInBytes)
    {
      MagicNumber = TEFXMagicNumber;
      RuntimeID = runtimeID;
      Version = version;
      Reserved = 0;
      CompressionMode = compressionMode;
      DataSizeInBytes = dataSizeInBytes;
    }

    /// <summary>
    /// Tests equality between two TEFX headers.
    /// </summary>
    /// <param name="a">First TEFX header</param>
    /// <param name="b">Second TEFX header</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    public static bool operator ==(TEFXHeader a, TEFXHeader b)
    {
      return a.MagicNumber == b.MagicNumber && a.RuntimeID == b.RuntimeID && a.Version == b.Version && a.Reserved == b.Reserved
               && a.CompressionMode == b.CompressionMode && a.DataSizeInBytes == b.DataSizeInBytes;
    }

    /// <summary>
    /// Tests inequality between two TEFX headers.
    /// </summary>
    /// <param name="a">First TEFX header</param>
    /// <param name="b">Second TEFX header</param>
    /// <returns>True if both are not equal, false otherwise.</returns>
    public static bool operator !=(TEFXHeader a, TEFXHeader b)
    {
      return a.MagicNumber != b.MagicNumber || a.RuntimeID != b.RuntimeID || a.Version != b.Version || a.Reserved != b.Reserved
               || a.CompressionMode != b.CompressionMode || a.DataSizeInBytes != b.DataSizeInBytes;
    }

    /// <summary>
    /// Writes the TEFX header to a stream.
    /// </summary>
    /// <param name="header">TEFX header to write.</param>
    /// <param name="stream">Stream to write to.</param>
    /// <returns>True if writing to the stream was successful.</returns>
    public static bool WriteHeader(TEFXHeader header, Stream stream)
    {
      if (stream == null || !stream.CanWrite)
        return false;

      StreamHelper.WriteUInt32(header.MagicNumber, stream);
      stream.WriteByte(header.RuntimeID);
      StreamHelper.WriteUInt16(header.Version, stream);
      StreamHelper.WriteUInt32(header.Reserved, stream);
      stream.WriteByte((byte) header.CompressionMode);
      StreamHelper.WriteUInt32(header.DataSizeInBytes, stream);

      return true;
    }

    /// <summary>
    /// Reads a TEFX header from the stream.
    /// </summary>
    /// <param name="stream">Stream to read from.</param>
    /// <returns>The read TEFX header or null, if there was an error reading from the stream, or if the header data was not present.</returns>
    public static TEFXHeader? ReadHeader(Stream stream)
    {
      if (stream == null || !stream.CanRead)
        return null;

      uint magicNum, reserved, dataSizeInbytes;
      ushort version;
      byte runtimeID;
      DataCompressionMode compressionMode;

      //Check if we can read a magic number in fully, and once we have it, if its as expected
      if (!StreamHelper.ReadUInt32(stream, out magicNum) || magicNum != TEFXMagicNumber)
        return null;

      //Otherwise, we should be good to go with reading the rest
      runtimeID = (byte) stream.ReadByte();
      StreamHelper.ReadUInt16(stream, out version);
      StreamHelper.ReadUInt32(stream, out reserved);
      compressionMode = (DataCompressionMode) stream.ReadByte();
      StreamHelper.ReadUInt32(stream, out dataSizeInbytes);

      return new TEFXHeader(runtimeID, version, compressionMode, dataSizeInbytes);
    }

    /// <summary>
    /// Reads the primitive data from the input.
    /// </summary>
    /// <param name="input">Primitive reader.</param>
    public void Read(IPrimitiveReader input)
    {
      MagicNumber = input.Read<FourCC>("MagicNumber");
      RuntimeID = input.ReadByte("RuntimeID");
      Version = input.ReadUInt16("Version");
      Reserved = input.ReadUInt32("Reserved");
      CompressionMode = input.ReadEnum<DataCompressionMode>("CompressionMode");
      DataSizeInBytes = input.ReadUInt32("DataSizeInBytes");
    }

    /// <summary>
    /// Writes the primitive data to the output.
    /// </summary>
    /// <param name="output">Primitive writer</param>
    public readonly void Write(IPrimitiveWriter output)
    {
      output.Write<FourCC>("MagicNumber", MagicNumber);
      output.Write("RuntimeID", RuntimeID);
      output.Write("Version", Version);
      output.Write("Reserved", Reserved);
      output.WriteEnum<DataCompressionMode>("CompressionMode", CompressionMode);
      output.Write("DataSizeInBytes", DataSizeInBytes);
    }

    /// <summary>
    /// Determines whether the specified <see cref="System.Object" /> is equal to this instance.
    /// </summary>
    /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
    /// <returns>True if the specified <see cref="System.Object" /> is equal to this instance; otherwise, false.</returns>
    public override bool Equals(object obj)
    {
      if (obj is TEFXHeader)
      {
        return Equals((TEFXHeader) obj);
      }

      return false;
    }

    /// <summary>
    /// Tests equality between this instance and another.
    /// </summary>
    /// <param name="other">Other TEFX header instance.</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    public bool Equals(TEFXHeader other)
    {
      return MagicNumber == other.MagicNumber && RuntimeID == other.RuntimeID && Version == other.Version && Reserved == other.Reserved
          && CompressionMode == other.CompressionMode && DataSizeInBytes == other.DataSizeInBytes;
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
    public override int GetHashCode()
    {
      unchecked
      {
        return MagicNumber.GetHashCode() + RuntimeID.GetHashCode() + Version.GetHashCode() + Reserved.GetHashCode() +
            CompressionMode.GetHashCode() + DataSizeInBytes.GetHashCode();
      }
    }

    /// <summary>
    /// Returns the fully qualified type name of this instance.
    /// </summary>
    /// <returns>A <see cref="T:System.String" /> containing a fully qualified type name.</returns>
    public override String ToString()
    {
      return String.Format("RuntimeID: {0}, Version: {1}, CompressionMode: {2}, DataSizeInBytes: {3}",
          new String[] { RuntimeID.ToString(), Version.ToString(), CompressionMode.ToString(), DataSizeInBytes.ToString() });
    }
  }
}
