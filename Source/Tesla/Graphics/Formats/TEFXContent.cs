﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

// TODO NULLABLE

namespace Tesla.Graphics
{
  /// <summary>
  /// Represents the complete source code for an <see cref="Effect"/>. This is run through an effect compiler
  /// to produce a compiled TEFX file that can be consumed by a render system.
  /// </summary>
  [DebuggerDisplay("FileName = {FileName}, ShaderGroupCount = {ShaderGroupCount}")]
  public sealed class TEFXContent : IEnumerable<TEFXContent.ShaderGroupContent>
  {
    private List<ShaderGroupContent> m_shaderGroups;

    /// <summary>
    /// Gets the file name of the effect content.
    /// </summary>
    public String FileName { get; set; }

    /// <summary>
    /// Gets the shader group content at the specified index.
    /// </summary>
    /// <param name="index">Zero-based index of the technique content.</param>
    /// <returns>Shader group content contained, or null if out of range.</returns>
    public ShaderGroupContent this[int index]
    {
      get
      {
        if (index < 0 || index > m_shaderGroups.Count)
          return null;

        return m_shaderGroups[index];
      }
    }

    /// <summary>
    /// Gets the shader group content corresponding to the specified name.
    /// </summary>
    /// <param name="shaderGrpName">Name of the shader group to get.</param>
    /// <returns>Shader group content contained, or null if a shader group corresponding to the name could not be found.</returns>
    public ShaderGroupContent this[String shaderGrpName]
    {
      get
      {
        if (String.IsNullOrEmpty(shaderGrpName))
          return null;

        for (int i = 0; i < m_shaderGroups.Count; i++)
        {
          ShaderGroupContent grp = m_shaderGroups[i];
          if (grp.Name.Equals(shaderGrpName))
          {
            return grp;
          }
        }

        return null;
      }
    }

    /// <summary>
    /// Gets the number of shader groups contained in the effect.
    /// </summary>
    public int ShaderGroupCount
    {
      get
      {
        return m_shaderGroups.Count;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="TEFXContent"/> class.
    /// </summary>
    public TEFXContent()
    {
      FileName = String.Empty;
      m_shaderGroups = new List<ShaderGroupContent>();
    }

    /// <summary>
    /// Adds a shader group to the effect.
    /// </summary>
    /// <param name="shaderGrp">ShaderGroup content to add</param>
    /// <returns>True if successfully added, false otherwise.</returns>
    public bool AddShaderGroup(ShaderGroupContent shaderGrp)
    {
      if (shaderGrp != null)
      {
        m_shaderGroups.Add(shaderGrp);
        return true;
      }

      return false;
    }

    /// <summary>
    /// Removes a shader group, specified by a zero-based index, from the effect.
    /// </summary>
    /// <param name="index">Zero-based index of the pass content.</param>
    /// <returns>True if the shader group was removed, false otherwise.</returns>
    public bool RemoveShaderGroup(int index)
    {
      if (index < 0 || index > m_shaderGroups.Count)
        return false;

      m_shaderGroups.RemoveAt(index);
      return true;
    }

    /// <summary>
    /// Removes a shader group, specified by its name, from the effect. The first technique that
    /// matches the name exactly is removed.
    /// </summary>
    /// <param name="shaderGrpName">Name of the shader group content to remove.</param>
    /// <returns>True if the shader group was removed, false otherwise.</returns>
    public bool RemoveShaderGroup(String shaderGrpName)
    {
      if (String.IsNullOrEmpty(shaderGrpName))
        return false;

      for (int i = 0; i < m_shaderGroups.Count; i++)
      {
        ShaderGroupContent shaderGrp = m_shaderGroups[i];
        if (shaderGrp.Name.Equals(shaderGrpName))
        {
          m_shaderGroups.RemoveAt(i);
          return true;
        }
      }

      return false;
    }

    /// <summary>
    /// Removes a shader group from the effect.
    /// </summary>
    /// <param name="shaderGrp">ShaderGroup content to remove.</param>
    /// <returns>True if the shader group was removed, false otherwise.</returns>
    public bool RemoveShaderGroup(ShaderGroupContent shaderGrp)
    {
      if (shaderGrp == null)
        return false;

      return m_shaderGroups.Remove(shaderGrp);
    }

    /// <summary>
    /// Clears all shader groups currently contained in the effect.
    /// </summary>
    public void Clear()
    {
      m_shaderGroups.Clear();
    }

    /// <summary>
    /// Gets all shader groups contained in this effect as a list.
    /// </summary>
    /// <returns>A list of shader groups contained in this effect.</returns>
    public IList<ShaderGroupContent> GetShaderGroups()
    {
      return new List<ShaderGroupContent>(m_shaderGroups);
    }

    /// <summary>
    /// Queries if the effect contains a shader group corresponding to the name.
    /// </summary>
    /// <param name="shaderGrpName">Name of the shader group.</param>
    /// <returns>True if the shader group content was found, false otherwise.</returns>
    public bool IsShaderGroupPresent(String shaderGrpName)
    {
      if (String.IsNullOrEmpty(shaderGrpName))
        return false;

      for (int i = 0; i < m_shaderGroups.Count; i++)
      {
        ShaderGroupContent tech = m_shaderGroups[i];
        if (tech.Name.Equals(shaderGrpName))
        {
          return true;
        }
      }

      return false;
    }

    /// <summary>
    /// Returns an enumerator that iterates through the collection.
    /// </summary>
    /// <returns>A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.</returns>
    public IEnumerator<ShaderGroupContent> GetEnumerator()
    {
      return m_shaderGroups.GetEnumerator();
    }

    /// <summary>
    /// Returns an enumerator that iterates through a collection.
    /// </summary>
    /// <returns>An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.</returns>
    IEnumerator IEnumerable.GetEnumerator()
    {
      return m_shaderGroups.GetEnumerator();
    }

    #region Nested Content Classes

    /// <summary>
    /// Represents a group of shaders defined in the effect. Each group can have exactly one of each type of shader.
    /// </summary>
    [DebuggerDisplay("Name = {Name}, ShaderCount = {ShaderCount}")]
    public sealed class ShaderGroupContent : INamable, IEnumerable<ShaderContent>
    {
      private Dictionary<ShaderStage, ShaderContent> m_shaders;

      /// <summary>
      /// Gets the name of the shader group.
      /// </summary>
      public String Name { get; set; }

      /// <summary>
      /// Gets the shader content corresponding to the shader type, if it is present in the group.
      /// </summary>
      /// <param name="shaderType">Shader type to query</param>
      /// <returns>The contained shader content, or null if content of the specified type is not present.</returns>
      public ShaderContent this[ShaderStage shaderType]
      {
        get
        {
          return m_shaders[shaderType];
        }
        set
        {
          m_shaders[shaderType] = value;
        }
      }

      /// <summary>
      /// Gets the number of shaders contained in this group.
      /// </summary>
      public int ShaderCount
      {
        get
        {
          return m_shaders.Count;
        }
      }

      /// <summary>
      /// Constructs a new instance of the <see cref="ShaderGroupContent"/> class.
      /// </summary>
      public ShaderGroupContent()
      {
        Name = "ShaderGroup";
        m_shaders = new Dictionary<ShaderStage, ShaderContent>();
      }

      /// <summary>
      /// Constructs a new instance of the <see cref="ShaderGroupContent"/> class.
      /// </summary>
      /// <param name="name">Name of the shader group</param>
      public ShaderGroupContent(String name)
      {
        Name = (String.IsNullOrEmpty(name)) ? "ShaderGroup" : name;
        m_shaders = new Dictionary<ShaderStage, ShaderContent>();
      }

      /// <summary>
      /// Gets all shader content contained in this group as a list.
      /// </summary>
      /// <returns>A list of shader content contained in this group.</returns>
      public IList<ShaderContent> GetShaders()
      {
        List<ShaderContent> shaders = new List<ShaderContent>(m_shaders.Count);

        foreach (KeyValuePair<ShaderStage, ShaderContent> kv in m_shaders)
        {
          if (kv.Value != null)
            shaders.Add(kv.Value);
        }

        return shaders;
      }

      /// <summary>
      /// Clears all shaders currently contained in the group.
      /// </summary>
      public void Clear()
      {
        m_shaders.Clear();
      }

      /// <summary>
      /// Queries if the group contains shader content corresponding to the specified type.
      /// </summary>
      /// <param name="shaderType">Type of shader</param>
      /// <returns>True if shader content is contained, or false if not.</returns>
      public bool IsShaderPresent(ShaderStage shaderType)
      {
        return m_shaders.ContainsKey(shaderType);
      }

      /// <summary>
      /// Returns an enumerator that iterates through the collection.
      /// </summary>
      /// <returns>A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.</returns>
      public IEnumerator<ShaderContent> GetEnumerator()
      {
        return m_shaders.Values.GetEnumerator();
      }

      /// <summary>
      /// Returns an enumerator that iterates through a collection.
      /// </summary>
      /// <returns>An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.</returns>
      IEnumerator IEnumerable.GetEnumerator()
      {
        return m_shaders.Values.GetEnumerator();
      }
    }

    /// <summary>
    /// Represents source code for a single shader.
    /// </summary>
    [DebuggerDisplay("ShaderType = {ShaderType}, EntryPoint = {EntryPoint}, ShaderProfile = {ShaderProfile}")]
    public sealed class ShaderContent
    {
      /// <summary>
      /// Gets the type of shader the content represents.
      /// </summary>
      public ShaderStage ShaderType { get; set; }

      /// <summary>
      /// Gets the shader source code.
      /// </summary>
      public String SourceCode { get; set; }

      /// <summary>
      /// Gets the function name that serves as an entry point to the shader.
      /// </summary>
      public String EntryPoint { get; set; }

      /// <summary>
      /// Gets the shader profile the shader should be compiled against.
      /// </summary>
      public String ShaderProfile { get; set; }

      /// <summary>
      /// Constructs a new instance of the <see cref="ShaderContent"/> class.
      /// </summary>
      public ShaderContent()
      {
        ShaderType = ShaderStage.VertexShader;
        SourceCode = String.Empty;
        EntryPoint = String.Empty;
        ShaderProfile = String.Empty;
      }

      /// <summary>
      /// Constructs a new instance of the <see cref="ShaderContent"/> class.
      /// </summary>
      /// <param name="shaderType">Type of shader.</param>
      /// <param name="sourceCode">Source code.</param>
      /// <param name="entryPoint">Entry function name.</param>
      /// <param name="shaderProfile">Shader profile.</param>
      public ShaderContent(ShaderStage shaderType, String sourceCode, String entryPoint, String shaderProfile)
      {
        ShaderType = shaderType;
        SourceCode = sourceCode;
        EntryPoint = entryPoint;
        ShaderProfile = shaderProfile;
      }
    }

    #endregion
  }
}
