﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using Tesla.Utilities;

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// Represents the complete collection of image data that represents a single texture, be it a 3D, an array, cubemap, and all their mipmaps.
  /// </summary>
  public struct TextureData : IDisposable
  {
    /// <summary>
    /// Represents a single image.
    /// </summary>
    public struct MipSurface
    {
      /// <summary>
      /// Width of the image, in texels. Must be at least one.
      /// </summary>
      public uint Width;

      /// <summary>
      /// Height of the image, in texels. Must be at least one.
      /// </summary>
      public uint Height;

      /// <summary>
      /// Depth of the image, in texels. Must be at least one.
      /// </summary>
      public uint Depth;

      /// <summary>
      /// Image data.
      /// </summary>
      public IDataBuffer? Data;

      /// <summary>
      /// Constructs a new instance of the <see cref="MipSurface"/> struct.
      /// </summary>
      /// <param name="width">Width of the image.</param>
      /// <param name="data">Image data.</param>
      public MipSurface(uint width, IDataBuffer data)
      {
        Width = width;
        Height = 1;
        Depth = 1;
        Data = data;
      }

      /// <summary>
      /// Constructs a new instance of the <see cref="MipSurface"/> struct.
      /// </summary>
      /// <param name="width">Width of the image.</param>
      /// <param name="height">Height of the image.</param>
      /// <param name="data">Image data.</param>
      public MipSurface(uint width, uint height, IDataBuffer data)
      {
        Width = width;
        Height = height;
        Depth = 1;
        Data = data;
      }

      /// <summary>
      /// Constructs a new instance of the <see cref="MipSurface"/> struct.
      /// </summary>
      /// <param name="width">Width of the image.</param>
      /// <param name="height">Height of the image.</param>
      /// <param name="depth">Depth of the image.</param>
      /// <param name="data">Image data.</param>
      public MipSurface(uint width, uint height, uint depth, IDataBuffer data)
      {
        Width = width;
        Height = height;
        Depth = depth;
        Data = data;
      }

      /// <summary>
      /// Checks if the surface is valid.
      /// </summary>
      public readonly bool IsValid
      {
        get
        {
          return Data is not null && Width > 0 && Height > 0 && Depth > 0;
        }
      }
    }

    /// <summary>
    /// Represents a collection of mipmap surfaces. A valid texture always has at least one surface. The first surface is the largest image, and each subsequent mipmap
    /// is a scaled down texture by a factor of 2.
    /// </summary>
    public class MipChain : List<MipSurface>
    {
      /// <summary>
      /// Constructs a new <see cref="MipChain"/>.
      /// </summary>
      public MipChain() { }

      /// <summary>
      /// Constructs a new <see cref="MipChain"/>.
      /// </summary>
      /// <param name="capacity">Initial capacity.</param>
      public MipChain(int capacity) : base(capacity) { }

      /// <summary>
      /// Constructs a new <see cref="MipChain"/>.
      /// </summary>
      /// <param name="surfaces">Collection of images to add to this collection.</param>
      public MipChain(IEnumerable<MipSurface> surfaces) : base(surfaces) { }
    }

    /// <summary>
    /// The type of texture the data represents. 1D textures only have width, 2D/Cube textures have width/height, and 3D textures have width/height/depth.
    /// </summary>
    public TextureType TextureType;

    /// <summary>
    /// The format of the data. All surfaces contained in the texture are expected to be of this format.
    /// </summary>
    public SurfaceFormat Format;

    /// <summary>
    /// The width of the texture, in texels. Every first mipmap surface in each mipmap chain must be of this width. Must be greater than or equal to one.
    /// </summary>
    public uint Width;

    /// <summary>
    /// The height of the texture, in texels. Every first mipmap surface in each mipmap chain must be of this height. Must be greater than or equal to one.
    /// </summary>
    public uint Height;

    /// <summary>
    /// The depth of the texture, in texels. Every first mipmap surface in each mipmap chain must be of this depth. Must be greater than or equal to one.
    /// </summary>
    public uint Depth;

    /// <summary>
    /// Collection of mipmap chains that contain image data. If the texture is an 1D/2D array, this can be any number of images, but the first image in each chain must be the same
    /// dimensions as the others. If a cube texture there can only by 6 mipmap chains, one for each face of the cube, and in the order defined by <see cref="CubeMapFace"/>: +X, -X, +Y, -Y, +Z, -Z.
    /// </summary>
    public List<MipChain>? MipChains;

    /// <summary>
    /// Gets the number of mip maps (number of mipmaps in each mipmap chain). This will be non-zero if <see cref="Validate"/> returns true.
    /// </summary>
    public readonly int MipCount
    {
      get
      {
        if (MipChains is null || MipChains.Count == 0)
          return 0;

        return MipChains[0]?.Count ?? 0;
      }
    }

    /// <summary>
    /// Gets the array count (number of mipmap chains). This will be non-zero if <see cref="Validate"/> returns true.
    /// </summary>
    public readonly int ArrayCount
    {
      get
      {
        return MipChains?.Count ?? 0;
      }
    }

    /// <summary>
    /// Validates the dimensions and image data, based on the texture type.
    /// </summary>
    /// <returns>True if the data is valid for the texture type, false if otherwise.</returns>
    public readonly bool Validate()
    {
      //Check the basics
      if (Width == 0 || Height == 0 || Depth == 0 || MipChains is null || MipChains.Count == 0)
        return false;

      //Validate the type, if not set as an array, make sure we check the array
      switch (TextureType)
      {
        case TextureType.Texture1D:
        case TextureType.Texture2D:
        case TextureType.Texture3D:
          if (MipChains.Count != 1) //Must have only one mipchain, since not arrays
            return false;
          break;
        case TextureType.TextureCube:
          if (MipChains.Count != TextureCube.CubeMapFaceCount) //Must have only 6 mipchains
            return false;

          if (Width != Height) //Must be square texture
            return false;
          break;
        case TextureType.TextureCubeArray:
          if ((MipChains.Count % TextureCube.CubeMapFaceCount) != 0) //Must be multiples of 6
            return false;

          if (Width != Height) //Must be square texture
            return false;
          break;
      }

      //Look at each mipchain:
      // 1. Mipchains must exist.
      // 2. Each mipchain must have the same number of mip surfaces as other chains.
      // 3. Array entries must be the same dimensions (each first mip surface dimension checked against texture dimension).
      // 4. Each mip surface must exist and be smaller than the previous.

      int mipCount = -1;

      for (int i = 0; i < MipChains.Count; i++)
      {
        MipChain chain = MipChains[i];

        //Mip must exist...
        if (chain is null || chain.Count == 0)
          return false;

        //Grab a mip count from the first mipchain
        if (mipCount == -1)
          mipCount = chain.Count;

        //Each mipchain must have the same number of mip surfaces
        if (chain.Count != mipCount)
          return false;

        //Each mip surface must have data and check sizes
        MipSurface prevMip = chain[0];
        if (prevMip.Width != Width || prevMip.Height != Height || prevMip.Depth != Depth || prevMip.Data is null)
          return false;

        for (int mipLevel = 1; mipLevel < chain.Count; mipLevel++)
        {
          MipSurface nextMip = chain[mipLevel];

          if (nextMip.Width > prevMip.Width || nextMip.Height > prevMip.Height || nextMip.Depth > prevMip.Depth || nextMip.Data is null)
            return false;

          prevMip = nextMip;
        }
      }

      return true;
    }

    /// <summary>
    /// Disposes of the data buffers.
    /// </summary>
    public void Dispose()
    {
      if (MipChains is not null)
      {
        foreach (MipChain chain in MipChains)
        {
          for (int i = 0; i < chain.Count; i++)
          {
            MipSurface mip = chain[i];
            if (mip.Data is not null)
            {
              mip.Data.Dispose();
              mip.Data = null;
            }
          }
        }

        MipChains = null;
      }
    }

    /// <summary>
    /// Writes the texture data in the TEXU format (see <see cref="TEXUHeader"/>), to the stream.
    /// </summary>
    /// <param name="data">Data to write out.</param>
    /// <param name="output">Stream to write to.</param>
    /// <param name="compressionMode">Type of compression mode, if any, to compress the image data.</param>
    /// <returns>True if the image data was successfully written out.</returns>
    public static bool Write(TextureData data, Stream output, DataCompressionMode compressionMode = DataCompressionMode.None)
    {
      if (!data.Validate() || output is null || data.MipChains is null || !output.CanWrite)
        return false;

      int maxMipCount = data.MipCount;

      ulong uncompressedDataSize = GetTotalDataSizeInBytes(data.MipChains);
      TEXUHeader header = new TEXUHeader(TEXUHeader.CurrentVersion, data.TextureType, data.Format, data.Width, data.Height, data.Depth, (uint) data.MipChains.Count, (uint) maxMipCount, compressionMode, uncompressedDataSize);

      //No reason for small files to have a huge temp buffer to transfer data to stream
      int estimatedTempBufferSize = (int) uncompressedDataSize;

      if (compressionMode == DataCompressionMode.Gzip)
      {
        //Write header with uncompressed data size, fix up later
        TEXUHeader.WriteHeader(header, output);

        long posPostHeader = output.Position;

        //Write out image data and compress it
        using (GZipStream compressedStream = new GZipStream(output, CompressionMode.Compress, true))
          WriteToStream(data.MipChains, compressedStream, estimatedTempBufferSize);

        //Calculate the data payload size
        long posPostData = output.Position;
        ulong compressedDataSize = (ulong) (posPostData - posPostHeader);

        //Fixup the compressed data size
        output.Position = posPostHeader - sizeof(ulong);
        StreamHelper.WriteUInt64(compressedDataSize, output);

        //Set stream position back to end of data
        output.Position = posPostData;
      }
      else
      {
        //Write header with uncompressed data size
        TEXUHeader.WriteHeader(header, output);

        //Write uncompressed data
        WriteToStream(data.MipChains, output, estimatedTempBufferSize);
      }

      return true;
    }

    private static ulong GetTotalDataSizeInBytes(List<MipChain> mipChains)
    {
      ulong totalDataSize = 0;
      ulong sizeOfUint32 = sizeof(uint);

      foreach (MipChain mips in mipChains)
      {
        //MipCount, DataSize
        totalDataSize += (sizeOfUint32 * 2);
        totalDataSize += GetTotalMipChainSizeInBytes(mips);
      }

      return totalDataSize;
    }

    private static uint GetTotalMipChainSizeInBytes(MipChain mipChain)
    {
      uint sizeInBytes = 0;
      uint sizeOfUint32 = sizeof(uint);

      foreach (MipSurface surface in mipChain)
      {
        //Width, Height, Depth, DataSize
        sizeInBytes += (sizeOfUint32 * 4);
        sizeInBytes += (surface.Data != null) ? (uint) surface.Data.SizeInBytes : 0;
      }

      return sizeInBytes;
    }

    private static void WriteToStream(List<MipChain> mipChains, Stream output, int maxSizeInBytes)
    {
      //Create a transfer buffer, max size caps out at 85k bytes to avoid LOH allocation
      using (StreamTransferBuffer buffer = new StreamTransferBuffer(maxSizeInBytes))
      {
        foreach (MipChain mips in mipChains)
        {
          uint mipChainSizeInBytes = GetTotalMipChainSizeInBytes(mips);

          buffer.Write<uint>(output, (uint) mips.Count);
          buffer.Write<uint>(output, mipChainSizeInBytes);

          foreach (MipSurface surface in mips)
          {
            buffer.Write<uint>(output, surface.Width);
            buffer.Write<uint>(output, surface.Height);
            buffer.Write<uint>(output, surface.Depth);

            uint surfaceSizeInBytes = (uint) surface.Data!.SizeInBytes;

            buffer.Write<uint>(output, surfaceSizeInBytes);

            if (surfaceSizeInBytes > 0)
              buffer.Write(output, surface.Data.Bytes);
          }
        }
      }
    }

    /// <summary>
    /// Reads data from the GPU texture and returns it in the form of <see cref="TextureData"/>.
    /// </summary>
    /// <param name="texture">GPU texture to read data from.</param>
    /// <param name="allocatorStrategy">Optional buffer allocator strategy. By default uses <see cref="MemoryAllocatorStrategy.DefaultPooled"/>.</param>
    /// <returns>Texture data object, which may or may not be valid.</returns>
    public static TextureData From(Texture texture, MemoryAllocatorStrategy allocatorStrategy = MemoryAllocatorStrategy.DefaultPooled)
    {
      TextureData data = new TextureData();

      if (texture is null)
        return data;

      data.Format = texture.Format;

      switch (texture)
      {
        case Texture1D tex1D:
          {
            data.Width = (uint) tex1D.Width;
            data.Height = 1;
            data.Depth = 1;
            data.TextureType = TextureType.Texture1D;

            int arrayCount = 1;
            Texture1DArray? tex1DArray = tex1D as Texture1DArray;
            if (tex1DArray is not null)
            {
              data.TextureType = TextureType.Texture1DArray;
              arrayCount = tex1DArray.ArrayCount;
            }

            data.MipChains = new List<MipChain>(arrayCount);

            for (int arraySlice = 0; arraySlice < arrayCount; arraySlice++)
              data.MipChains.Add(GetMipChain(tex1D, allocatorStrategy, arraySlice));
          }
          break;
        case Texture2D tex2D:
          {
            data.Width = (uint) tex2D.Width;
            data.Height = (uint) tex2D.Height;
            data.Depth = 1;
            data.TextureType = TextureType.Texture2D;

            int arrayCount = 1;
            Texture2DArray? tex2DArray = tex2D as Texture2DArray;
            if (tex2DArray is not null)
            {
              data.TextureType = TextureType.Texture2DArray;
              arrayCount = tex2DArray.ArrayCount;
            }

            data.MipChains = new List<MipChain>(arrayCount);

            for (int arraySlice = 0; arraySlice < arrayCount; arraySlice++)
              data.MipChains.Add(GetMipChain(tex2D, allocatorStrategy, arraySlice));
          }
          break;
        case Texture3D tex3D:
          {
            data.Width = (uint) tex3D.Width;
            data.Height = (uint) tex3D.Height;
            data.Depth = (uint) tex3D.Depth;
            data.TextureType = TextureType.Texture3D;

            data.MipChains = new List<MipChain>(1);
            data.MipChains.Add(GetMipChain(tex3D, allocatorStrategy));
          }
          break;
        case TextureCube texCube:
          {
            data.Width = (uint) texCube.Size;
            data.Height = (uint) texCube.Size;
            data.Depth = 1;
            data.TextureType = TextureType.TextureCube;

            int arrayCount = 1;
            TextureCubeArray? texCubeArray = texCube as TextureCubeArray;
            if (texCubeArray is not null)
            {
              data.TextureType = TextureType.TextureCubeArray;
              arrayCount = texCubeArray.ArrayCount;
            }

            data.MipChains = new List<MipChain>(arrayCount * TextureCube.CubeMapFaceCount);

            for (int cubeMapIndex = 0; cubeMapIndex < arrayCount; cubeMapIndex++)
            {
              for (int cubemapFace = 0; cubemapFace < TextureCube.CubeMapFaceCount; cubemapFace++)
                data.MipChains.Add(GetMipChain(texCube, (CubeMapFace) cubemapFace, allocatorStrategy, cubeMapIndex));
            }
          }
          break;
      }

      return data;
    }

    /// <summary>
    /// Reads TEXU format (see <see cref="TEXUHeader"/>) texture data from the stream.
    /// </summary>
    /// <param name="input">Stream to read from.</param>
    /// <param name="allocatorStrategy">Optional allocator strategy for buffers. Default is <see cref="MemoryAllocatorStrategy.DefaultPooled"/>.</param>
    /// <returns>Texture data, which may or may not be valid.</returns>
    public static TextureData Read(Stream input, MemoryAllocatorStrategy allocatorStrategy = MemoryAllocatorStrategy.DefaultPooled)
    {
      if (input is null || !input.CanRead)
        return new TextureData();

      TEXUHeader? header = TEXUHeader.ReadHeader(input);

      if (header is null)
        return new TextureData();

      TEXUHeader h = header.Value;

      //Header specified data size, make sure the stream actually has that amount of data to read
      if (!StreamHelper.CanReadBytes(input, (long) h.DataSizeInBytes))
        return new TextureData();

      TextureData texData;
      texData.Width = h.Width;
      texData.Height = h.Height;
      texData.Depth = h.Depth;
      texData.Format = h.Format;
      texData.TextureType = h.TextureType;
      texData.MipChains = new List<MipChain>((int) h.ArrayCount);

      //No reason for small files to have a huge temp buffer to transfer data to stream
      int estimatedTempBufferSize = (int) h.DataSizeInBytes;

      //If data is compressed, decompress it
      if (h.CompressionMode == DataCompressionMode.Gzip)
      {
        using (GZipStream decompressedStream = new GZipStream(input, CompressionMode.Decompress, true))
          ReadFromStream(h.Format, texData.MipChains, h.ArrayCount, h.MipCount, decompressedStream, estimatedTempBufferSize, allocatorStrategy);
      }
      else
      {
        ReadFromStream(h.Format, texData.MipChains, h.ArrayCount, h.MipCount, input, estimatedTempBufferSize, allocatorStrategy);
      }

      return texData;
    }

    private static void ReadFromStream(SurfaceFormat format, List<MipChain> mipChains, uint arrayCount, uint mipCount, Stream input, int maxSizeInBytes, MemoryAllocatorStrategy allocatorStrategy)
    {
      //Create a transfer buffer, max size caps out at 85k bytes to avoid LOH allocation
      using (StreamTransferBuffer buffer = new StreamTransferBuffer(maxSizeInBytes))
      {
        for (int i = 0; i < arrayCount; i++)
        {
          uint thisMipCount = buffer.Read<uint>(input);
          uint mipchainDataSize = buffer.Read<uint>(input);

          System.Diagnostics.Debug.Assert(thisMipCount == mipCount, "# of Mips mismatch");

          MipChain mipchain = new MipChain((int) thisMipCount);

          for (int mip = 0; mip < thisMipCount; mip++)
          {
            MipSurface surface;

            surface.Width = buffer.Read<uint>(input);
            surface.Height = buffer.Read<uint>(input);
            surface.Depth = buffer.Read<uint>(input);
            surface.Data = null;

            uint surfaceSizeInBytes = buffer.Read<uint>(input);

            System.Diagnostics.Debug.Assert(surfaceSizeInBytes > 0, "Missing image");

            //Based on the format...create buffers that might be better inspected
            surface.Data = CreateTypedBuffer(format, (int) surfaceSizeInBytes, allocatorStrategy);

            if (surfaceSizeInBytes > 0)
              buffer.Read(input, surface.Data.Bytes);

            mipchain.Add(surface);
          }

          mipChains.Add(mipchain);
        }
      }
    }

    private static IDataBuffer CreateTypedBuffer(SurfaceFormat format, int surfaceSizeInBytes, MemoryAllocatorStrategy allocatorStrategy)
    {
      switch (format)
      {
        case SurfaceFormat.Color:
          return DataBuffer.Create<Color>(BufferHelper.ComputeElementCount<Color>(surfaceSizeInBytes), allocatorStrategy);
        case SurfaceFormat.BGRColor:
          return DataBuffer.Create<ColorBGRA>(BufferHelper.ComputeElementCount<ColorBGRA>(surfaceSizeInBytes), allocatorStrategy);
        case SurfaceFormat.Single:
          return DataBuffer.Create<float>(BufferHelper.ComputeElementCount<float>(surfaceSizeInBytes), allocatorStrategy);
        case SurfaceFormat.Vector2:
          return DataBuffer.Create<Vector2>(BufferHelper.ComputeElementCount<Vector2>(surfaceSizeInBytes), allocatorStrategy);
        case SurfaceFormat.Vector3:
          return DataBuffer.Create<Vector3>(BufferHelper.ComputeElementCount<Vector3>(surfaceSizeInBytes), allocatorStrategy);
        case SurfaceFormat.Vector4:
          return DataBuffer.Create<Vector4>(BufferHelper.ComputeElementCount<Vector4>(surfaceSizeInBytes), allocatorStrategy);
        default:
          return DataBuffer.Create<byte>(surfaceSizeInBytes, allocatorStrategy);
      }
    }

    private static void GetData(Texture1D tex, Span<byte> bytes, int mipLevel, int arrayIndex = 0)
    {
      tex.GetData<byte>(bytes, SubResourceAt.MipSlice(mipLevel, arrayIndex));
    }

    private static void GetData(Texture2D tex, Span<byte> bytes, int mipLevel, int arrayIndex = 0)
    {
      tex.GetData<byte>(bytes, SubResourceAt.MipSlice(mipLevel, arrayIndex));
    }

    private static void GetData(Texture3D tex, Span<byte> bytes, int mipLevel)
    {
      tex.GetData<byte>(bytes, SubResourceAt.Mip(mipLevel));
    }

    private static void GetData(TextureCube tex, Span<byte> bytes, int mipLevel, CubeMapFace face, int arrayIndex = 0)
    {
      tex.GetData<byte>(bytes, SubResourceAt.MipFace(mipLevel, face, arrayIndex));
    }

    private static MipChain GetMipChain(Texture1D tex, MemoryAllocatorStrategy allocatorStrategy, int arrayIndex = 0)
    {
      MipChain mips = new MipChain(tex.MipCount);

      //Get each mip level for the array index
      for (int mipLevel = 0; mipLevel < tex.MipCount; mipLevel++)
      {
        MipSurface mip = new MipSurface();
        int width = tex.Width;

        int sizeInbytes = Texture.CalculateMipLevelSizeInBytes(mipLevel, width, tex.Format);
        Texture.CalculateMipLevelDimensions(mipLevel, ref width);

        mip.Data = CreateTypedBuffer(tex.Format, sizeInbytes, allocatorStrategy);
        mip.Width = (uint) width;
        mip.Height = 1;
        mip.Depth = 1;

        GetData(tex, mip.Data.Bytes, mipLevel, arrayIndex);

        mips.Add(mip);
      }

      return mips;
    }

    private static MipChain GetMipChain(Texture2D tex, MemoryAllocatorStrategy allocatorStrategy, int arrayIndex = 0)
    {
      MipChain mips = new MipChain(tex.MipCount);

      //Get each mip level for the array index
      for (int mipLevel = 0; mipLevel < tex.MipCount; mipLevel++)
      {
        MipSurface mip = new MipSurface();
        int width = tex.Width;
        int height = tex.Height;

        int sizeInbytes = Texture.CalculateMipLevelSizeInBytes(mipLevel, width, height, tex.Format);
        Texture.CalculateMipLevelDimensions(mipLevel, ref width, ref height);

        mip.Data = CreateTypedBuffer(tex.Format, sizeInbytes, allocatorStrategy);
        mip.Width = (uint) width;
        mip.Height = (uint) height;
        mip.Depth = 1;

        GetData(tex, mip.Data.Bytes, mipLevel, arrayIndex);

        mips.Add(mip);
      }

      return mips;
    }

    private static MipChain GetMipChain(Texture3D tex, MemoryAllocatorStrategy allocatorStrategy)
    {
      MipChain mips = new MipChain(tex.MipCount);

      //Get each mip level for the array index
      for (int mipLevel = 0; mipLevel < tex.MipCount; mipLevel++)
      {
        MipSurface mip = new MipSurface();
        int width = tex.Width;
        int height = tex.Height;
        int depth = tex.Depth;

        int sizeInbytes = Texture.CalculateMipLevelSizeInBytes(mipLevel, width, height, depth, tex.Format);
        Texture.CalculateMipLevelDimensions(mipLevel, ref width, ref height, ref depth);

        mip.Data = CreateTypedBuffer(tex.Format, sizeInbytes, allocatorStrategy);
        mip.Width = (uint) width;
        mip.Height = (uint) height;
        mip.Depth = (uint) depth;

        GetData(tex, mip.Data.Bytes, mipLevel);

        mips.Add(mip);
      }

      return mips;
    }

    private static MipChain GetMipChain(TextureCube tex, CubeMapFace face, MemoryAllocatorStrategy allocatorStrategy, int cubeMapIndex = 0)
    {
      MipChain mips = new MipChain(tex.MipCount);

      //Get each mip level for the CubeMap face
      for (int mipLevel = 0; mipLevel < tex.MipCount; mipLevel++)
      {
        MipSurface mip = new MipSurface();
        int width = tex.Size;
        int height = tex.Size;

        int sizeinbytes = Texture.CalculateMipLevelSizeInBytes(mipLevel, width, height, tex.Format);
        Texture.CalculateMipLevelDimensions(mipLevel, ref width, ref height);

        mip.Data = CreateTypedBuffer(tex.Format, sizeinbytes, allocatorStrategy);
        mip.Width = (uint) width;
        mip.Height = (uint) height;
        mip.Depth = 1;

        GetData(tex, mip.Data.Bytes, mipLevel, face, cubeMapIndex);

        mips.Add(mip);
      }

      return mips;
    }
  }
}
