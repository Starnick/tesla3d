﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Tesla.Content;

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// Represents the header of the TEXU (Tesla Engine Texture) format. The format is a container for texture data, supporting 1D, 2D, 3D, Cube and their array varients. This is not
  /// a serialization of the engine's Texture .NET objects. The format is renderer-agnostic.
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public struct TEXUHeader : IPrimitiveValue
  {
    private static readonly FourCC s_texuMagicNumber = new FourCC('T', 'E', 'X', 'U');
    private static readonly int s_sizeInBytes = BufferHelper.SizeOf<TEXUHeader>();

    /// <summary>
    /// The current version that writers will write as.
    /// </summary>
    public const ushort CurrentVersion = 1;

    /// <summary>
    /// The MagicNumber that identifies the format, this is always a four character code of "TETX".
    /// </summary>
    public FourCC MagicNumber;

    /// <summary>
    /// The Version of the format.
    /// </summary>
    public ushort Version;

    /// <summary>
    /// The type of texture stored.
    /// </summary>
    public TextureType TextureType;

    /// <summary>
    /// The format of the texture.
    /// </summary>
    public SurfaceFormat Format;

    /// <summary>
    /// The width of the texture (if at least 1D).
    /// </summary>
    public uint Width;

    /// <summary>
    /// The height of the texture (if at least 2D or if Cube Width == Height).
    /// </summary>
    public uint Height;

    /// <summary>
    /// The depth of the texture (if at least 3D).
    /// </summary>
    public uint Depth;

    /// <summary>
    /// The number of textures (at least 1, Cube textures are always 6).
    /// </summary>
    public uint ArrayCount;

    /// <summary>
    /// The number of mip maps for each texture (at least 1).
    /// </summary>
    public uint MipCount;

    /// <summary>
    /// Reserved.
    /// </summary>
    public uint Reserved;

    /// <summary>
    /// Flags denoting compression of the data, if any.
    /// </summary>
    public DataCompressionMode CompressionMode;

    /// <summary>
    /// Size of the data in bytes.
    /// </summary>
    public ulong DataSizeInBytes;

    /// <summary>
    /// Gets the "TEXU" magic number four character code identifying the format.
    /// </summary>
    public static FourCC TEXUMagicNumber
    {
      get
      {
        return s_texuMagicNumber;
      }
    }

    /// <summary>
    /// Gets the size in bytes of the <see cref="TEXUHeader"/> structure in bytes.
    /// </summary>
    public static int SizeInBytes
    {
      get
      {
        return s_sizeInBytes;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="TEXUHeader"/> struct.
    /// </summary>
    /// <param name="version">Format version</param>
    /// <param name="type">Type of texture.</param>
    /// <param name="format">Format of the texture.</param>
    /// <param name="width">Width of the texture.</param>
    /// <param name="height">Height of the texture.</param>
    /// <param name="depth">Depth of the texture.</param>
    /// <param name="arrayCount">Number of textures slices if this is an array texture. If a value of one then it is a non-array texture.</param>
    /// <param name="mipCount">Number of mip map surfaces for each slice. Each texture must have a single mip surface.</param>
    /// <param name="compressionMode">Compression mode of the data, if any.</param>
    /// <param name="dataSizeInBytes">The size of the data, in bytes.</param>
    public TEXUHeader(ushort version, TextureType type, SurfaceFormat format, uint width, uint height, uint depth, uint arrayCount, uint mipCount, DataCompressionMode compressionMode, ulong dataSizeInBytes)
    {
      MagicNumber = TEXUMagicNumber;
      Version = version;
      TextureType = type;
      Format = format;
      Width = width;
      Height = height;
      Depth = depth;
      ArrayCount = arrayCount;
      MipCount = mipCount;
      Reserved = 0;
      CompressionMode = compressionMode;
      DataSizeInBytes = dataSizeInBytes;
    }

    /// <summary>
    /// Tests equality between two TEXU headers.
    /// </summary>
    /// <param name="a">First TEXU header</param>
    /// <param name="b">Second TEXU header</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    public static bool operator ==(TEXUHeader a, TEXUHeader b)
    {
      return a.MagicNumber == b.MagicNumber && a.Version == b.Version && a.TextureType == b.TextureType && a.Format == b.Format &&
          a.Width == b.Width && a.Height == b.Height && a.Depth == b.Depth && a.ArrayCount == b.ArrayCount && a.Reserved == b.Reserved &&
          a.CompressionMode == b.CompressionMode && a.DataSizeInBytes == b.DataSizeInBytes;
    }

    /// <summary>
    /// Tests inequality between two TEXU headers.
    /// </summary>
    /// <param name="a">First TEXU header</param>
    /// <param name="b">Second TEXU header</param>
    /// <returns>True if both are not equal, false otherwise.</returns>
    public static bool operator !=(TEXUHeader a, TEXUHeader b)
    {
      return a.MagicNumber != b.MagicNumber || a.Version != b.Version || a.TextureType != b.TextureType || a.Format != b.Format ||
          a.Width != b.Width || a.Height != b.Height || a.Depth != b.Depth || a.ArrayCount != b.ArrayCount || a.Reserved != b.Reserved ||
          a.CompressionMode != b.CompressionMode || a.DataSizeInBytes != b.DataSizeInBytes;
    }

    /// <summary>
    /// Writes the TEXU header to a stream.
    /// </summary>
    /// <param name="header">TEXU header to write.</param>
    /// <param name="stream">Stream to write to.</param>
    /// <returns>True if writing to the stream was successful.</returns>
    public static bool WriteHeader(in TEXUHeader header, Stream stream)
    {
      if (stream == null || !stream.CanWrite)
        return false;

      StreamHelper.WriteUInt32(header.MagicNumber, stream);
      StreamHelper.WriteUInt16(header.Version, stream);
      stream.WriteByte((byte) header.TextureType);
      StreamHelper.WriteUInt32((uint) header.Format, stream);
      StreamHelper.WriteUInt32(header.Width, stream);
      StreamHelper.WriteUInt32(header.Height, stream);
      StreamHelper.WriteUInt32(header.Depth, stream);
      StreamHelper.WriteUInt32(header.ArrayCount, stream);
      StreamHelper.WriteUInt32(header.MipCount, stream);
      StreamHelper.WriteUInt32(header.Reserved, stream);
      stream.WriteByte((byte) header.CompressionMode);
      StreamHelper.WriteUInt64(header.DataSizeInBytes, stream);

      return true;
    }

    /// <summary>
    /// Reads a TEXU header from the stream.
    /// </summary>
    /// <param name="stream">Stream to read from.</param>
    /// <returns>The read TEXU header or null, if there was an error reading from the stream, or if the header data was not present.</returns>
    public static TEXUHeader? ReadHeader(Stream stream)
    {
      if (stream == null || !stream.CanRead)
        return null;

      uint magicNum, format, reserved, width, height, depth, arrayCount, mipCount;
      ushort version;
      ulong dataSizeInBytes;
      TextureType type;
      DataCompressionMode compressionMode;

      //Check if we can read a magic number in fully, and once we have it, if its as expected
      if (!StreamHelper.ReadUInt32(stream, out magicNum) || magicNum != TEXUMagicNumber)
        return null;

      //Otherwise, we should be good to go with reading the rest
      StreamHelper.ReadUInt16(stream, out version);
      type = (TextureType) stream.ReadByte();
      StreamHelper.ReadUInt32(stream, out format);
      StreamHelper.ReadUInt32(stream, out width);
      StreamHelper.ReadUInt32(stream, out height);
      StreamHelper.ReadUInt32(stream, out depth);
      StreamHelper.ReadUInt32(stream, out arrayCount);
      StreamHelper.ReadUInt32(stream, out mipCount);
      StreamHelper.ReadUInt32(stream, out reserved);
      compressionMode = (DataCompressionMode) stream.ReadByte();
      StreamHelper.ReadUInt64(stream, out dataSizeInBytes);

      return new TEXUHeader(version, type, (SurfaceFormat) format, width, height, depth, arrayCount, mipCount, compressionMode, dataSizeInBytes);
    }

    /// <summary>
    /// Reads the primitive data from the input.
    /// </summary>
    /// <param name="input">Primitive reader.</param>
    public void Read(IPrimitiveReader input)
    {
      MagicNumber = input.Read<FourCC>("MagicNumber");
      Version = input.ReadUInt16("Version");
      TextureType = input.ReadEnum<TextureType>("TextureType");
      Format = input.ReadEnum<SurfaceFormat>("Format");
      Width = input.ReadUInt16("Width");
      Height = input.ReadUInt16("Height");
      Depth = input.ReadUInt16("Depth");
      ArrayCount = input.ReadUInt16("ArrayCount");
      MipCount = input.ReadUInt16("MipCount");
      Reserved = input.ReadUInt32("Reserved");
      CompressionMode = input.ReadEnum<DataCompressionMode>("CompressionMode");
      DataSizeInBytes = input.ReadUInt64("DataSizeInBytes");
    }

    /// <summary>
    /// Writes the primitive data to the output.
    /// </summary>
    /// <param name="output">Primitive writer.</param>
    public readonly void Write(IPrimitiveWriter output)
    {
      output.Write<FourCC>("MagicNumber", MagicNumber);
      output.Write("Version", Version);
      output.WriteEnum<TextureType>("TextureType", TextureType);
      output.WriteEnum<SurfaceFormat>("Format", Format);
      output.Write("Width", Width);
      output.Write("Height", Height);
      output.Write("Depth", Depth);
      output.Write("ArrayCount", ArrayCount);
      output.Write("MipCount", MipCount);
      output.Write("Reserved", Reserved);
      output.WriteEnum<DataCompressionMode>("CompressionMode", CompressionMode);
      output.Write("DataSizeInBytes", DataSizeInBytes);
    }

    /// <summary>
    /// Determines whether the specified <see cref="System.Object" /> is equal to this instance.
    /// </summary>
    /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
    /// <returns>True if the specified <see cref="System.Object" /> is equal to this instance; otherwise, false.</returns>
    public readonly override bool Equals([MaybeNullWhen(true)] object? obj)
    {
      if (obj is TEXUHeader)
        return Equals((TEXUHeader) obj);

      return false;
    }

    /// <summary>
    /// Tests equality between this instance and another.
    /// </summary>
    /// <param name="other">Other TETX header instance.</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    public readonly bool Equals(TEXUHeader other)
    {
      return MagicNumber == other.MagicNumber && Version == other.Version && TextureType == other.TextureType && Format == other.Format &&
          Width == other.Width && Height == other.Height && Depth == other.Depth && ArrayCount == other.ArrayCount && Reserved == other.Reserved &&
          CompressionMode == other.CompressionMode && DataSizeInBytes == other.DataSizeInBytes;
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
    public readonly override int GetHashCode()
    {
      unchecked
      {
        return MagicNumber.GetHashCode() + Version.GetHashCode() + TextureType.GetHashCode() + Format.GetHashCode() + Width.GetHashCode() + Height.GetHashCode() +
            Depth.GetHashCode() + ArrayCount.GetHashCode() + MipCount.GetHashCode() + Reserved.GetHashCode() +
            CompressionMode.GetHashCode() + DataSizeInBytes.GetHashCode();
      }
    }

    /// <summary>
    /// Returns the fully qualified type name of this instance.
    /// </summary>
    /// <returns>A <see cref="T:System.String" /> containing a fully qualified type name.</returns>
    public readonly override String ToString()
    {
      return String.Format("Version: {0}, TextureType: {1}, Format: {2}, Width: {3}, Height: {4}, Depth: {5}, ArrayCount: {6}, MipCount: {7}, CompressionMode: {8}, DataSizeInBytes: {9}",
          new String[] { Version.ToString(), TextureType.ToString(), Format.ToString(), Width.ToString(), Height.ToString(), Depth.ToString(), ArrayCount.ToString(), MipCount.ToString(), CompressionMode.ToString(), DataSizeInBytes.ToString() });
    }
  }
}
