﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System.Collections;
using System.Collections.Generic;
using Tesla.Content;

namespace Tesla.Graphics
{
    [SavableVersion(1)]
    public sealed class RenderPropertyCollection : IReadOnlyDictionary<RenderPropertyID, RenderProperty>, ISavable
    {
        private Dictionary<RenderPropertyID, RenderProperty> m_properties;

        public int Count
        {
            get
            {
                return m_properties.Count;
            }
        }

        public IEnumerable<RenderPropertyID> Keys
        {
            get
            {
                return m_properties.Keys;
            }
        }

        public IEnumerable<RenderProperty> Values
        {
            get
            {
                return m_properties.Values;
            }
        }

        public RenderProperty this[RenderPropertyID key]
        {
            get
            {
                if(key == RenderPropertyID.Invalid)
                    return null;

                return m_properties[key];
            }
            set
            {
                if(key == RenderPropertyID.Invalid || value == null)
                    return;

                m_properties[key] = value;
            }
        }

        public RenderPropertyCollection() : this(0) { }

        public RenderPropertyCollection(int initialCapacity)
        {
            m_properties = new Dictionary<RenderPropertyID, RenderProperty>(initialCapacity, new RenderPropertyIDEqualityComparer());
        }

        public RenderPropertyCollection(IEnumerable<RenderProperty> properties)
        {
            m_properties = new Dictionary<RenderPropertyID, RenderProperty>(new RenderPropertyIDEqualityComparer());

            if(properties != null)
            {
                foreach(RenderProperty prop in properties)
                {
                    if(prop != null)
                        m_properties.Add(prop.ID, prop);
                }
            }
        }

        public bool Add(RenderProperty property)
        {
            if(property == null || property.ID == RenderPropertyID.Invalid)
                return false;

            m_properties.Add(property.ID, property);

            return true;
        }

        public bool Remove(RenderPropertyID propID)
        {
            if(propID == RenderPropertyID.Invalid)
                return false;

            return m_properties.Remove(propID);
        }

        public bool ContainsKey(RenderPropertyID propID)
        {
            if(propID == RenderPropertyID.Invalid)
                return false;

            return m_properties.ContainsKey(propID);
        }

        public bool TryGetValue(RenderPropertyID key, out RenderProperty value)
        {
            if(key == RenderPropertyID.Invalid)
            {
                value = null;
                return false;
            }

            return m_properties.TryGetValue(key, out value);
        }

        public bool TryGet<T>(out T value) where T : RenderProperty
        {
            RenderPropertyID key = RenderProperty.GetPropertyID<T>();
            value = null;

            if(key == RenderPropertyID.Invalid)
                return false;

            RenderProperty prop;
            bool foundIt = m_properties.TryGetValue(key, out prop);
            value = prop as T;

            return foundIt;
        }

        public void Clear()
        {
            m_properties.Clear();
        }

        public RenderPropertyCollection Clone()
        {
            int canCloneCount = GetCanCloneCount();
            RenderPropertyCollection clone = new RenderPropertyCollection(canCloneCount);

            if(canCloneCount > 0)
            {
                foreach(KeyValuePair<RenderPropertyID, RenderProperty> kv in m_properties)
                {
                    RenderProperty oldProp = kv.Value;
                    if(oldProp.CanClone)
                    {
                        RenderProperty newProp = oldProp.Clone();
                        if(newProp != null)
                            clone.m_properties.Add(newProp.ID, newProp);
                    }
                }
            }

            return clone;
        }

        public Dictionary<RenderPropertyID, RenderProperty>.Enumerator GetEnumerator()
        {
            return m_properties.GetEnumerator();
        }

        IEnumerator<KeyValuePair<RenderPropertyID, RenderProperty>> IEnumerable<KeyValuePair<RenderPropertyID, RenderProperty>>.GetEnumerator()
        {
            return m_properties.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return m_properties.GetEnumerator();
        }

        /// <summary>
        /// Reads the object data from the input.
        /// </summary>
        /// <param name="input">Savable reader</param>
        public void Read(ISavableReader input)
        {
            int count = input.ReadInt32("Count");

            if(count > 0)
            {
                ISavable[] savables = input.ReadSavableArray<ISavable>("Properties");
                for(int i = 0; i < savables.Length; i++)
                {
                    RenderProperty prop = savables[i] as RenderProperty;
                    if(prop != null)
                        m_properties.Add(prop.ID, prop);
                }
            }
        }

        /// <summary>
        /// Writes the object data to the output.
        /// </summary>
        /// <param name="output">Savable writer</param>
        public void Write(ISavableWriter output)
        {
            int savableCount = GetSavableCount();

            output.Write("Count", savableCount);

            if(savableCount > 0)
            {
                ISavable[] savables = new ISavable[savableCount];
                int index = 0;
                foreach(KeyValuePair<RenderPropertyID, RenderProperty> kv in m_properties)
                {
                    if(kv.Value is ISavable)
                    {
                        savables[index] = kv.Value as ISavable;

                        index++;
                    }
                }

                output.WriteSavable<ISavable>("Properties", savables);
            }
        }

        private int GetSavableCount()
        {
            int numSavable = 0;
            foreach(KeyValuePair<RenderPropertyID, RenderProperty> kv in m_properties)
            {
                if(kv.Value is ISavable)
                    numSavable++;
            }

            return numSavable;
        }

        private int GetCanCloneCount()
        {
            int cloneCount = 0;
            foreach(KeyValuePair<RenderPropertyID, RenderProperty> kv in m_properties)
            {
                if(kv.Value.CanClone)
                    cloneCount++;
            }

            return cloneCount;
        }

        #region ID equality comparer

        private sealed class RenderPropertyIDEqualityComparer : IEqualityComparer<RenderPropertyID>
        {
            public RenderPropertyIDEqualityComparer() { }

            public bool Equals(RenderPropertyID x, RenderPropertyID y)
            {
                return x == y;
            }

            public int GetHashCode(RenderPropertyID obj)
            {
                return obj.Value;
            }
        }

        #endregion
    }
}
