﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Content;
using Tesla.Graphics.Implementation;

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// Represents an array of Two-Dimensional textures, each with a width and a height, that can be used as a render target. A render target is an output that is bound to the rendering 
  /// pipeline to receive rendered data (e.g. geometry drawn to the back buffer, or to an off-screen target), which then can be used later as a regular texture.
  /// </summary>
  /// <remarks>
  /// Each render target has a depth-stencil buffer associated with it, if the format is specified, that is bound to the graphics pipeline when the render target
  /// is also bound. In a Multi-Render Target (MRT) scenario, the first render target's depth-stencil buffer (if it has one) is used for all targets. Depth-stencil buffers
  /// can also be shared between two different targets, if it is supported.
  /// </remarks>
  [SavableVersion(1)]
  public class RenderTarget2DArray : Texture2DArray, IRenderTarget
  {

    #region RenderTarget Properties

    /// <summary>
    /// Gets the depth stencil buffer associated with the render target, if no buffer is associated then this will be null.
    /// </summary>
    public IDepthStencilBuffer? DepthStencilBuffer
    {
      get
      {
        return RenderTarget2DArrayImpl.DepthStencilBuffer;
      }
    }

    /// <summary>
    /// Gets the depth stencil format of the associated depth buffer, if any. If this does not exist, then the format is None.
    /// </summary>
    public DepthFormat DepthStencilFormat
    {
      get
      {
        IDepthStencilBuffer? depthBuffer = RenderTarget2DArrayImpl.DepthStencilBuffer;
        return (depthBuffer is null) ? DepthFormat.None : depthBuffer.DepthStencilFormat;
      }
    }

    /// <summary>
    /// Gets the multisample settings for the resource. The MSAA count, quality, and if the resource should be resolved to
    /// a non-MSAA resource for shader input. MSAA targets that do not resolve to a non-MSAA resource will only ever have one mip map per array slice.
    /// </summary>
    public MSAADescription MultisampleDescription
    {
      get
      {
        return RenderTarget2DArrayImpl.MultisampleDescription;
      }
    }

    /// <summary>
    /// Gets the target usage, specifying how the target should be handled when it is bound to the pipeline. Generally this is
    /// set to discard by default.
    /// </summary>
    public RenderTargetUsage TargetUsage
    {
      get
      {
        return RenderTarget2DArrayImpl.TargetUsage;
      }
    }

    /// <summary>
    /// Gets the depth of the target resource, in pixels.
    /// </summary>
    int IRenderTarget.Depth
    {
      get
      {
        return 1;
      }
    }

    /// <summary>
    /// Gets if the resource is an array resource.
    /// </summary>
    bool IRenderTarget.IsArrayResource
    {
      get
      {
        return true;
      }
    }

    /// <summary>
    /// Gets if the resource is a cube resource, a special type of array resource. A cube resource has six faces. If the geometry shader stage is not supported by
    /// the render system, then all six cube faces cannot be bound as a single binding. Instead the main cube target graphics resource is treated as the very first
    /// cube face, subsequent faces must be bound individually.
    /// </summary>
    bool IRenderTarget.IsCubeResource
    {
      get
      {
        return false;
      }
    }

    /// <summary>
    /// Gets if the resource is a sub-resource, representing an individual array slice if the main resource is an array resource or an
    /// individual face if its a cube resource. If this is a sub resource, then its sub resource index indicates the position in the array/cube.
    /// </summary>
    bool IRenderTarget.IsSubResource
    {
      get
      {
        return false;
      }
    }

    /// <summary>
    /// Gets the array index if the resource is a sub resource in an array. If not, then the index is -1.
    /// </summary>
    int IRenderTarget.SubResourceIndex
    {
      get
      {
        return -1;
      }
    }

    #endregion

    //Private property to cast the implementation
    private IRenderTarget2DArrayImpl RenderTarget2DArrayImpl
    {
      get
      {
        return GetImplAs<IRenderTarget2DArrayImpl>();
      }
      set
      {
        BindImplementation(value);
      }
    }

    /// <summary>
    /// For ISavable only.
    /// </summary>
    protected RenderTarget2DArray() { }

    /// <summary>
    /// Constructs a new instance of the <see cref="RenderTarget2DArray"/> class.
    /// </summary>
    /// <param name="renderSystem">Render system used to create the underlying implementation.</param>
    /// <param name="width">Width of the target, in texels.</param>
    /// <param name="height">Height of the target, in texels.</param>
    /// <param name="arrayCount">Number of array slices, must be greater than zero.</param>
    /// <exception cref="System.ArgumentOutOfRangeException">Thrown if the width/height is less than or equal to zero or greater than the maximum texture size,
    /// or if the array count is less than or equal to zero or greater than the maximum array size.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if creating the underlying implementation fails or is unsupported, see inner exception for a more detailed error.</exception>
    public RenderTarget2DArray(IRenderSystem renderSystem, int width, int height, int arrayCount)
    {
      CreateImplementation(renderSystem, width, height, arrayCount, false, SurfaceFormat.Color, MSAADescription.Default, DepthFormat.None, false, RenderTargetUsage.DiscardContents);
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="RenderTarget2DArray"/> class.
    /// </summary>
    /// <param name="renderSystem">Render system used to create the underlying implementation.</param>
    /// <param name="width">Width of the target, in texels.</param>
    /// <param name="height">Height of the target, in texels.</param>
    /// <param name="arrayCount">Number of array slices, must be greater than zero.</param>
    /// <param name="genMipMaps">True to generate the entire mip map chain of the render target, false otherwise (only the first mip level is created). If the target has MSAA it will only
    /// have one mip map level.</param>
    /// <param name="format">Surface format of the render target.</param>
    /// <param name="depthFormat">Depth format of the depth-stencil buffer, if any. If this is set to none, no depth buffer is created.</param>
    /// <exception cref="System.ArgumentOutOfRangeException">Thrown if the width/height is less than or equal to zero or greater than the maximum texture size,
    /// or if the array count is less than or equal to zero or greater than the maximum array size.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if the surface format is invalid for the texture dimension, or if the surface format/depth format settings are an invalid combination,
    /// or if creating the underlying implementation fails or is unsupported, see inner exception for a more detailed error.</exception>
    public RenderTarget2DArray(IRenderSystem renderSystem, int width, int height, int arrayCount, bool genMipMaps, SurfaceFormat format, DepthFormat depthFormat)
    {
      CreateImplementation(renderSystem, width, height, arrayCount, genMipMaps, format, MSAADescription.Default, depthFormat, false, RenderTargetUsage.DiscardContents);
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="RenderTarget2DArray"/> class.
    /// </summary>
    /// <param name="renderSystem">Render system used to create the underlying implementation.</param>
    /// <param name="width">Width of the target, in texels.</param>
    /// <param name="height">Height of the target, in texels.</param>
    /// <param name="arrayCount">Number of array slices, must be greater than zero.</param>
    /// <param name="genMipMaps">True to generate the entire mip map chain of the render target, false otherwise (only the first mip level is created). If the target has MSAA it will only
    /// have one mip map level.</param>
    /// <param name="format">Surface format of the render target.</param>
    /// <param name="depthFormat">Depth format of the depth-stencil buffer, if any. If this is set to none, no depth buffer is created.</param>
    /// <param name="targetUsage">Target usage, specifying how the render target should be handled when it is bound to the pipeline.</param>
    /// <exception cref="System.ArgumentOutOfRangeException">Thrown if the width/height is less than or equal to zero or greater than the maximum texture size,
    /// or if the array count is less than or equal to zero or greater than the maximum array size.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if the surface format is invalid for the texture dimension, or if the surface format/depth format settings are an invalid combination,
    /// or if creating the underlying implementation fails or is unsupported, see inner exception for a more detailed error.</exception>
    public RenderTarget2DArray(IRenderSystem renderSystem, int width, int height, int arrayCount, bool genMipMaps, SurfaceFormat format, DepthFormat depthFormat, RenderTargetUsage targetUsage)
    {
      CreateImplementation(renderSystem, width, height, arrayCount, genMipMaps, format, MSAADescription.Default, depthFormat, false, targetUsage);
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="RenderTarget2DArray"/> class.
    /// </summary>
    /// <param name="renderSystem">Render system used to create the underlying implementation.</param>
    /// <param name="width">Width of the target, in texels.</param>
    /// <param name="height">Height of the target, in texels.</param>
    /// <param name="arrayCount">Number of array slices, must be greater than zero.</param>
    /// <param name="genMipMaps">True to generate the entire mip map chain of the render target, false otherwise (only the first mip level is created). If the target has MSAA it will only
    /// have one mip map level.</param>
    /// <param name="format">Surface format of the render target.</param>
    /// <param name="preferredMSAA">Preferred MSAA settings, if not supported the next best possible valid setting will be used.</param>
    /// <param name="depthFormat">Depth format of the depth-stencil buffer, if any. If this is set to none, no depth buffer is created.</param>
    /// <exception cref="System.ArgumentOutOfRangeException">Thrown if the width/height is less than or equal to zero or greater than the maximum texture size,
    /// or if the array count is less than or equal to zero or greater than the maximum array size.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if the surface format is invalid for the texture dimension, or if the surface format/depth format/MSAA settings are an invalid combination,
    /// or if creating the underlying implementation fails or is unsupported, see inner exception for a more detailed error.</exception>
    public RenderTarget2DArray(IRenderSystem renderSystem, int width, int height, int arrayCount, bool genMipMaps, SurfaceFormat format, DepthFormat depthFormat, MSAADescription preferredMSAA)
    {
      CreateImplementation(renderSystem, width, height, arrayCount, genMipMaps, format, preferredMSAA, depthFormat, false, RenderTargetUsage.DiscardContents);
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="RenderTarget2DArray"/> class.
    /// </summary>
    /// <param name="renderSystem">Render system used to create the underlying implementation.</param>
    /// <param name="width">Width of the target, in texels.</param>
    /// <param name="height">Height of the target, in texels.</param>
    /// <param name="arrayCount">Number of array slices, must be greater than zero.</param>
    /// <param name="genMipMaps">True to generate the entire mip map chain of the render target, false otherwise (only the first mip level is created). If the target has MSAA it will only
    /// have one mip map level.</param>
    /// <param name="format">Surface format of the render target.</param>
    /// <param name="preferredMSAA">Preferred MSAA settings, if not supported the next best possible valid setting will be used.</param>
    /// <param name="depthFormat">Depth format of the depth-stencil buffer, if any. If this is set to none, no depth buffer is created.</param>
    /// <param name="targetUsage">Target usage, specifying how the render target should be handled when it is bound to the pipeline.</param>
    /// <exception cref="System.ArgumentOutOfRangeException">Thrown if the width/height is less than or equal to zero or greater than the maximum texture size,
    /// or if the array count is less than or equal to zero or greater than the maximum array size.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if the surface format is invalid for the texture dimension, or if the surface format/depth format/MSAA settings are an invalid combination,
    /// or if creating the underlying implementation fails or is unsupported, see inner exception for a more detailed error.</exception>
    public RenderTarget2DArray(IRenderSystem renderSystem, int width, int height, int arrayCount, bool genMipMaps, SurfaceFormat format, DepthFormat depthFormat, MSAADescription preferredMSAA, RenderTargetUsage targetUsage)
    {
      CreateImplementation(renderSystem, width, height, arrayCount, genMipMaps, format, preferredMSAA, depthFormat, false, targetUsage);
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="RenderTarget2DArray"/> class.
    /// </summary>
    /// <param name="renderSystem">Render system used to create the underlying implementation.</param>
    /// <param name="width">Width of the target, in texels.</param>
    /// <param name="height">Height of the target, in texels.</param>
    /// <param name="arrayCount">Number of array slices, must be greater than zero.</param>
    /// <param name="format">Surface format of the render target.</param>
    /// <param name="preferredMSAA">Preferred MSAA settings, if not supported the next best possible valid setting will be used.</param>
    /// <param name="depthFormat">Depth format of the depth-stencil buffer, if any. If this is set to none, no depth buffer is created.</param>
    /// <exception cref="System.ArgumentOutOfRangeException">Thrown if the width/height is less than or equal to zero or greater than the maximum texture size,
    /// or if the array count is less than or equal to zero or greater than the maximum array size.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if the surface format is invalid for the texture dimension, or if the surface format/depth format/MSAA settings are an invalid combination,
    /// or if creating the underlying implementation fails or is unsupported, see inner exception for a more detailed error.</exception>
    public RenderTarget2DArray(IRenderSystem renderSystem, int width, int height, int arrayCount, SurfaceFormat format, DepthFormat depthFormat, MSAADescription preferredMSAA)
    {
      CreateImplementation(renderSystem, width, height, arrayCount, false, format, preferredMSAA, depthFormat, false, RenderTargetUsage.DiscardContents);
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="RenderTarget2DArray"/> class.
    /// </summary>
    /// <param name="renderSystem">Render system used to create the underlying implementation.</param>
    /// <param name="width">Width of the target, in texels.</param>
    /// <param name="height">Height of the target, in texels.</param>
    /// <param name="arrayCount">Number of array slices, must be greater than zero.</param>
    /// <param name="format">Surface format of the render target.</param>
    /// <param name="preferredMSAA">Preferred MSAA settings, if not supported the next best possible valid setting will be used.</param>
    /// <param name="depthFormat">Depth format of the depth-stencil buffer, if any. If this is set to none, no depth buffer is created.</param>
    /// <param name="targetUsage">Target usage, specifying how the render target should be handled when it is bound to the pipeline.</param>
    /// <exception cref="System.ArgumentOutOfRangeException">Thrown if the width/height is less than or equal to zero or greater than the maximum texture size,
    /// or if the array count is less than or equal to zero or greater than the maximum array size.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if the surface format is invalid for the texture dimension, or if the surface format/depth format/MSAA settings are an invalid combination,
    /// or if creating the underlying implementation fails or is unsupported, see inner exception for a more detailed error.</exception>
    public RenderTarget2DArray(IRenderSystem renderSystem, int width, int height, int arrayCount, SurfaceFormat format, DepthFormat depthFormat, MSAADescription preferredMSAA, RenderTargetUsage targetUsage)
    {
      CreateImplementation(renderSystem, width, height, arrayCount, false, format, preferredMSAA, depthFormat, false, targetUsage);
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="RenderTarget2DArray"/> class.
    /// </summary>
    /// <param name="renderSystem">Render system used to create the underlying implementation.</param>
    /// <param name="width">Width of the target, in texels.</param>
    /// <param name="height">Height of the target, in texels.</param>
    /// <param name="arrayCount">Number of array slices, must be greater than zero.</param>
    /// <param name="genMipMaps">True to generate the entire mip map chain of the render target, false otherwise (only the first mip level is created). If the target has MSAA it will only
    /// have one mip map level.</param>
    /// <param name="format">Surface format of the render target.</param>
    /// <param name="preferredMSAA">Preferred MSAA settings, if not supported the next best possible valid setting will be used.</param>
    /// <param name="depthFormat">Depth format of the depth-stencil buffer, if any. If this is set to none, no depth buffer is created.</param>
    /// <param name="preferReadableDepth">True if it is preferred that the depth-stencil buffer is readable, that is if it can be bound as a shader resource, false otherwise. This may or may not be supported.</param>
    /// <exception cref="System.ArgumentOutOfRangeException">Thrown if the width/height is less than or equal to zero or greater than the maximum texture size,
    /// or if the array count is less than or equal to zero or greater than the maximum array size.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if the surface format is invalid for the texture dimension, or if the surface format/depth format/MSAA settings are an invalid combination,
    /// or if creating the underlying implementation fails or is unsupported, see inner exception for a more detailed error.</exception>
    public RenderTarget2DArray(IRenderSystem renderSystem, int width, int height, int arrayCount, bool genMipMaps, SurfaceFormat format, DepthFormat depthFormat, MSAADescription preferredMSAA, bool preferReadableDepth)
    {
      CreateImplementation(renderSystem, width, height, arrayCount, genMipMaps, format, preferredMSAA, depthFormat, preferReadableDepth, RenderTargetUsage.DiscardContents);
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="RenderTarget2DArray"/> class.
    /// </summary>
    /// <param name="renderSystem">Render system used to create the underlying implementation.</param>
    /// <param name="width">Width of the target, in texels.</param>
    /// <param name="height">Height of the target, in texels.</param>
    /// <param name="arrayCount">Number of array slices, must be greater than zero.</param>
    /// <param name="genMipMaps">True to generate the entire mip map chain of the render target, false otherwise (only the first mip level is created). If the target has MSAA it will only
    /// have one mip map level.</param>
    /// <param name="format">Surface format of the render target.</param>
    /// <param name="preferredMSAA">Preferred MSAA settings, if not supported the next best possible valid setting will be used.</param>
    /// <param name="depthFormat">Depth format of the depth-stencil buffer, if any. If this is set to none, no depth buffer is created.</param>
    /// <param name="preferReadableDepth">True if it is preferred that the depth-stencil buffer is readable, that is if it can be bound as a shader resource, false otherwise. This may or may not be supported.</param>
    /// <param name="targetUsage">Target usage, specifying how the render target should be handled when it is bound to the pipeline.</param>
    /// <exception cref="System.ArgumentOutOfRangeException">Thrown if the width/height is less than or equal to zero or greater than the maximum texture size,
    /// or if the array count is less than or equal to zero or greater than the maximum array size.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if the surface format is invalid for the texture dimension, or if the surface format/depth format/MSAA settings are an invalid combination,
    /// or if creating the underlying implementation fails or is unsupported, see inner exception for a more detailed error.</exception>
    public RenderTarget2DArray(IRenderSystem renderSystem, int width, int height, int arrayCount, bool genMipMaps, SurfaceFormat format, DepthFormat depthFormat, MSAADescription preferredMSAA, bool preferReadableDepth, RenderTargetUsage targetUsage)
    {
      CreateImplementation(renderSystem, width, height, arrayCount, genMipMaps, format, preferredMSAA, depthFormat, preferReadableDepth, targetUsage);
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="RenderTarget2DArray"/> class.
    /// </summary>
    /// <param name="renderSystem">Render system used to create the underlying implementation.</param>
    /// <param name="format">Surface format of the render target.</param>
    /// <param name="genMipMaps">True to generate the entire mip map chain of the render target, false otherwise (only the first mip level is created). If the target has MSAA it will only
    /// have one mip map level.</param>
    /// <param name="depthBuffer">Depth stencil buffer that is to be shared with this render target and dictate dimension and MSAA settings. It cannot be null.</param>
    /// <exception cref="ArgumentException">Thrown if the depth buffer does not match the resoruce dimensions or cannot be shared.</exception>
    /// <exception cref="System.ArgumentNullException">Thrown if the depth buffer is null.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if the surface format is invalid for the texture dimension, or if the surface format/depth format/MSAA settings are an invalid combination,
    /// or if creating the underlying implementation fails or is unsupported, see inner exception for a more detailed error.</exception>
    public RenderTarget2DArray(IRenderSystem renderSystem, SurfaceFormat format, bool genMipMaps, IDepthStencilBuffer depthBuffer)
    {
      CreateImplementation(renderSystem, format, genMipMaps, depthBuffer);
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="RenderTarget2DArray"/> class.
    /// </summary>
    /// <param name="renderSystem">Render system used to create the underlying implementation.</param>
    /// <param name="format">Surface format of the render target.</param>
    /// <param name="depthBuffer">Depth stencil buffer that is to be shared with this render target and dictate dimension and MSAA settings. It cannot be null.</param>
    /// <exception cref="ArgumentException">Thrown if the depth buffer does not match the resoruce dimensions or cannot be shared.</exception>
    /// <exception cref="System.ArgumentNullException">Thrown if the depth buffer is null.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if the surface format is invalid for the texture dimension, or if the surface format/depth format/MSAA settings are an invalid combination,
    /// or if creating the underlying implementation fails or is unsupported, see inner exception for a more detailed error.</exception>
    public RenderTarget2DArray(IRenderSystem renderSystem, SurfaceFormat format, IDepthStencilBuffer depthBuffer)
    {
      CreateImplementation(renderSystem, format, false, depthBuffer);
    }

    /// <summary>
    /// Gets a sub render target at the specified array index. For non-array resources, this will not be valid, unless if the resource is cube.
    /// </summary>
    /// <param name="arrayIndex">Zero-based index of the sub render target.</param>
    /// <returns>The sub render target.</returns>
    /// <exception cref="TeslaGraphicsException">Thrown if an error occurs while retrieving the sub render target.</exception>
    /// <remarks>See implementors for details on specific exceptions that can be thrown.</remarks>
    public IRenderTarget GetSubRenderTarget(int arrayIndex)
    {
      try
      {
        return RenderTarget2DArrayImpl.GetSubRenderTarget(arrayIndex);
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw new TeslaGraphicsException(StringLocalizer.Instance.GetLocalizedString("ErrorRetrievingSubRenderTarget"), e);
      }
    }

    #region Creation Parameter Validation

    /// <summary>
    /// Validates creation parameters.
    /// </summary>
    /// <param name="adapter">Graphics adapter from the render system.</param>
    /// <param name="width">Width of the target, in texels.</param>
    /// <param name="height">Height of the target, in texels.</param>
    /// <param name="arrayCount">Number of array slices in the texture array.</param>
    /// <param name="format">Surface format of the render target.</param>
    /// <param name="preferredMSAA">Preferred MSAA settings, if not supported the next best possible valid setting will be used.</param>
    /// <param name="depthFormat">Depth format of the depth-stencil buffer, if any. If this is set to none, no depth buffer is created.</param>
    /// <exception cref="System.ArgumentOutOfRangeException">Thrown if the width/height is less than or equal to zero or greater than the maximum texture size, or if the array count
    /// is less than or equal to zero or greater than the maximum array size.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if the surface format is invalid for the texture dimension, or if the surface format/depth format/MSAA settings are an invalid combination.</exception>
    protected void ValidateCreationParameters(IGraphicsAdapter adapter, int width, int height, int arrayCount, SurfaceFormat format, ref MSAADescription preferredMSAA, DepthFormat depthFormat)
    {
      if (width <= 0 || width > adapter.MaximumTexture2DSize)
        throw new ArgumentOutOfRangeException(nameof(width), StringLocalizer.Instance.GetLocalizedString("TextureDimensionOutOfRange"));

      if (height <= 0 || height > adapter.MaximumTexture2DSize)
        throw new ArgumentOutOfRangeException(nameof(height), StringLocalizer.Instance.GetLocalizedString("TextureDimensionOutOfRange"));

      if (arrayCount <= 0 || arrayCount > adapter.MaximumTexture2DArrayCount)
        throw new ArgumentOutOfRangeException(nameof(arrayCount), StringLocalizer.Instance.GetLocalizedString("TextureArrayCountOutOfRange"));

      if (!adapter.CheckTextureFormat(format, TextureDimension.Two))
        throw GraphicsExceptionHelper.NewBadTextureFormatException(format, TextureDimension.Two);

      MSAADescription.CheckMSAAConfiguration(adapter, format, ref preferredMSAA);

      if (!adapter.CheckRenderTargetFormat(format, depthFormat, preferredMSAA.Count))
        throw GraphicsExceptionHelper.NewBadRenderTargetFormatException(format);
    }

    /// <summary>
    /// Validates creation parameters.
    /// </summary>
    /// <param name="adapter">Graphics adapter from the render system.</param>
    /// <param name="format">Surface format of the render target.</param>
    /// <param name="depthBuffer">Depth stencil buffer that is to be shared with this render target and dictate dimension and MSAA settings. It cannot be null.</param>
    /// <exception cref="ArgumentException">Thrown if the depth buffer does not match the resoruce dimensions or cannot be shared.</exception>
    /// <exception cref="System.ArgumentNullException">Thrown if the depth buffer is null.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if the surface format is invalid for the texture dimension, or if the surface format/depth format/MSAA settings are an invalid combination.</exception>
    protected void ValidateCreationParameters(IGraphicsAdapter adapter, SurfaceFormat format, IDepthStencilBuffer depthBuffer)
    {
      if (depthBuffer is null)
        throw new ArgumentNullException(nameof(depthBuffer), StringLocalizer.Instance.GetLocalizedString("DepthBufferNull"));

      if (!depthBuffer.IsArrayResource || depthBuffer.IsCubeResource)
        throw new ArgumentException(nameof(depthBuffer), StringLocalizer.Instance.GetLocalizedString("DepthBufferNotValidForResource"));

      if (!depthBuffer.IsShareable)
        throw new ArgumentException(nameof(depthBuffer), StringLocalizer.Instance.GetLocalizedString("DepthBufferNotShareable"));

      if (!adapter.CheckTextureFormat(format, TextureDimension.Two))
        throw GraphicsExceptionHelper.NewBadTextureFormatException(format, TextureDimension.Two);

      if (!adapter.CheckRenderTargetFormat(format, depthBuffer.DepthStencilFormat, depthBuffer.MultisampleDescription.Count))
        throw GraphicsExceptionHelper.NewBadRenderTargetFormatException(format);
    }

    #endregion

    #region Implementation Creation

    private void CreateImplementation(IRenderSystem renderSystem, int width, int height, int arrayCount, bool genMipMaps, SurfaceFormat format, MSAADescription preferredMSAA, DepthFormat depthFormat, bool preferReadableDepth, RenderTargetUsage targetUsage)
    {
      GraphicsHelper.ThrowIfRenderSystemNull(renderSystem);

      ValidateCreationParameters(renderSystem.Adapter, width, height, arrayCount, format, ref preferredMSAA, depthFormat);

      bool canHaveMips = (preferredMSAA.IsMultisampled && preferredMSAA.ResolveShaderResource) || !preferredMSAA.IsMultisampled;
      int mipLevels = (genMipMaps && canHaveMips) ? CalculateMipMapCount(width, height) : 1;

      IRendertarget2DArrayImplFactory factory;
      if (!renderSystem.TryGetImplementationFactory<IRendertarget2DArrayImplFactory>(out factory))
        throw GraphicsExceptionHelper.NewUnsupportedFeatureException(renderSystem, typeof(RenderTarget2DArray));

      try
      {
        RenderTarget2DArrayImpl = factory.CreateImplementation(width, height, arrayCount, mipLevels, format, preferredMSAA, depthFormat, preferReadableDepth, targetUsage);
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw GraphicsExceptionHelper.NewErrorCreatingImplementation(typeof(RenderTarget2DArray), e);
      }
    }

    private void CreateImplementation(IRenderSystem renderSystem, SurfaceFormat format, bool genMipMaps, IDepthStencilBuffer depthBuffer)
    {
      GraphicsHelper.ThrowIfRenderSystemNull(renderSystem);

      ValidateCreationParameters(renderSystem.Adapter, format, depthBuffer);

      MSAADescription msaa = depthBuffer.MultisampleDescription;
      bool canHaveMips = (msaa.IsMultisampled && msaa.ResolveShaderResource) || !msaa.IsMultisampled;
      int mipLevels = (genMipMaps && canHaveMips) ? CalculateMipMapCount(depthBuffer.Width, depthBuffer.Height) : 1;

      IRendertarget2DArrayImplFactory factory;
      if (!renderSystem.TryGetImplementationFactory<IRendertarget2DArrayImplFactory>(out factory))
        throw GraphicsExceptionHelper.NewUnsupportedFeatureException(renderSystem, typeof(RenderTarget2DArray));

      try
      {
        RenderTarget2DArrayImpl = factory.CreateImplementation(format, mipLevels, depthBuffer);
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw GraphicsExceptionHelper.NewErrorCreatingImplementation(typeof(RenderTarget2DArray), e);
      }
    }

    #endregion
  }
}
