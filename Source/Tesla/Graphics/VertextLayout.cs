﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Tesla.Content;

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// Layout declaration that describes the structure of vertex data in a vertex buffer.
  /// </summary>
  [SavableVersion(1)]
  public sealed class VertexLayout : IEquatable<VertexLayout>, IEnumerable<VertexElement>, ISavable
  {
    private VertexElement[] m_elements;
    private int m_vertexStride;
    private int m_hash;

    /// <summary>
    /// Gets the stride of a single vertex, which is the total size of a vertex in bytes (position, color, etc).
    /// </summary>
    public int VertexStride
    {
      get
      {
        return m_vertexStride;
      }
    }

    /// <summary>
    /// Gets the number of vertex elements declared in the layout.
    /// </summary>
    public int ElementCount
    {
      get
      {
        return m_elements.Length;
      }
    }

    /// <summary>
    /// Gets the vertex element at the specified index in the layout.
    /// </summary>
    /// <param name="index">Zero-based index of the element</param>
    /// <returns>The vertex element</returns>
    /// <exception cref=" ArgumentOutOfRangeException">Thrown if the index is out of range.</exception>
    public VertexElement this[int index]
    {
      get
      {
        if (index < 0 || index >= m_elements.Length)
          throw new ArgumentOutOfRangeException(nameof(index), StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));

        return m_elements[index];
      }
    }

    /// <summary>
    /// For ISavable.
    /// </summary>
    private VertexLayout()
    {
      m_elements = Array.Empty<VertexElement>();
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="VertexLayout"/> class. The vertex stride automatically calculated from the elements.
    /// </summary>
    /// <param name="elements">Vertex elements that define the vertex layout.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the element array is null or empty.</exception>
    /// <exception cref="Tesla.Graphics.TeslaGraphicsException">Thrown if the elements are not valid.</exception>
    public VertexLayout(params VertexElement[] elements)
    {
      if (elements is null || elements.Length == 0)
        throw new ArgumentNullException(nameof(elements), StringLocalizer.Instance.GetLocalizedString("InputArrayIsNullOrEmpty"));

      m_elements = (VertexElement[]) elements.Clone();
      m_vertexStride = CalculateOffsetsAndGetVertexStride(m_elements);

      try
      {
        ValidateVertexElements(m_vertexStride, m_elements);
      }
      catch (Exception e)
      {
        throw new TeslaGraphicsException(StringLocalizer.Instance.GetLocalizedString("FailedToValidateVertexDeclaration"), e);
      }

      ComputeHashCode();
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="VertexLayout"/> class.
    /// </summary>
    /// <param name="vertexStride">Vertex stride of the vertex layout.</param>
    /// <param name="elements">Vertex elements that define the vertex layout.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the element array is null or empty.</exception>
    /// <exception cref="Tesla.Graphics.TeslaGraphicsException">Thrown if the elements are not valid.</exception>
    public VertexLayout(int vertexStride, params VertexElement[] elements)
    {
      if (elements is null || elements.Length == 0)
        throw new ArgumentNullException(nameof(elements), StringLocalizer.Instance.GetLocalizedString("InputArrayIsNullOrEmpty"));

      m_elements = (VertexElement[]) elements.Clone();
      m_vertexStride = vertexStride;

      try
      {
        ValidateVertexElements(m_vertexStride, m_elements);
      }
      catch (Exception e)
      {
        throw new TeslaGraphicsException(StringLocalizer.Instance.GetLocalizedString("FailedToValidateVertexDeclaration"), e);
      }

      ComputeHashCode();
    }

    /// <summary>
    /// Computes the vertex stride from the specified vertex layout.
    /// </summary>
    /// <param name="elements">Vertex elements of the layout.</param>
    /// <returns>Vertex stride of the layout.</returns>
    public static int CalculateOffsetsAndGetVertexStride(params VertexElement[] elements)
    {
      int stride = 0;

      if (elements is null || elements.Length == 0)
        return stride;

      for (int i = 0; i < elements.Length; i++)
      {
        VertexElement elem = elements[i];
        if (elem.Offset == VertexElement.AppendAligned)
        {
          elem.Offset = stride;
          elements[i] = elem;
        }
        else
        {
          stride = elem.Offset;
        }

        stride += elem.Format.SizeInBytes();
      }

      return stride;
    }

    /// <summary>
    /// Validates the vertex elements and specified vertex stride.
    /// </summary>
    /// <param name="vertexStride">The vertex stride of the layout.</param>
    /// <param name="elements">The vertex elements of the layout.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the vertex elements are null or empty.</exception>
    /// <exception cref="System.ArgumentOutOfRangeException">Thrown if the vertex stride is not positive or not a multiple of four. Also if any
    /// vertex element offset is not a multiple of four.
    /// </exception>
    /// <exception cref="System.ArgumentException">
    /// Thrown if any of the vertex elements have a duplicate semantic name/index or if any of the elements overlap with one another.
    /// </exception>
    public static void ValidateVertexElements(int vertexStride, params VertexElement[] elements)
    {
      if (elements is null || elements.Length == 0)
        throw new ArgumentNullException(nameof(elements), StringLocalizer.Instance.GetLocalizedString("InputArrayIsNullOrEmpty"));

      if (vertexStride <= 0)
        throw new ArgumentOutOfRangeException(nameof(vertexStride), StringLocalizer.Instance.GetLocalizedString("VertexStrideMustBePositive"));

      //Ensure stride is a multiple of four
      if ((vertexStride & 3) != 0)
        throw new ArgumentOutOfRangeException(nameof(vertexStride), StringLocalizer.Instance.GetLocalizedString("VertexStrideMultipleOfFour"));

      int expectedStride = 0;
      for (int i = 0; i < elements.Length; i++)
      {
        VertexElement elem = elements[i];
        int offset = elem.Offset;
        int elemSize = elem.Format.SizeInBytes();

        //Ensure offset is a multiple of four
        if ((offset & 3) != 0)
          throw new ArgumentOutOfRangeException(nameof(offset), StringLocalizer.Instance.GetLocalizedString("VertexOffsetMultipleOfFour"));

        //Make sure the offset in the element doesnt put us out of range
        for (int j = 0; j < i; j++)
        {
          VertexElement prev = elements[j];
          if (prev.SemanticName == elem.SemanticName && prev.SemanticIndex == elem.SemanticIndex)
            throw new ArgumentException(StringLocalizer.Instance.GetLocalizedString("DuplicateSemantic", prev.SemanticName.ToString(), prev.SemanticIndex.ToString()));
        }

        //Check for overlap between this element and the previous
        if ((expectedStride + elemSize) != (offset + elemSize))
          throw new ArgumentException(StringLocalizer.Instance.GetLocalizedString("VertexElementOverlap"));

        expectedStride = offset + elemSize;
      }
    }

    /// <summary>
    /// Gets a copy of the vertex elements in this declaration.
    /// </summary>
    /// <returns>Copy of the vertex elements.</returns>
    public VertexElement[] GetVertexElements()
    {
      return (VertexElement[]) m_elements.Clone();
    }

    /// <summary>
    /// Returns an enumerator that iterates through the collection.
    /// </summary>
    /// <returns>A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.</returns>
    public IEnumerator<VertexElement> GetEnumerator()
    {
      return ((IEnumerable<VertexElement>) m_elements).GetEnumerator();
    }

    /// <summary>
    /// Returns an enumerator that iterates through a collection.
    /// </summary>
    /// <returns>An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.</returns>
    IEnumerator IEnumerable.GetEnumerator()
    {
      return m_elements.GetEnumerator();
    }

    /// <summary>
    /// Determines whether the specified <see cref="T:System.Object" /> is equal to the current <see cref="T:System.Object" />.
    /// </summary>
    /// <param name="obj">The object to compare with the current object.</param>
    /// <returns>True if the specified object  is equal to the current object; otherwise, false.</returns>
    public override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is VertexLayout vl)
        return vl.m_hash == m_hash;

      return false;
    }

    /// <summary>
    /// Tests equality between this instance and another vertex layout.
    /// </summary>
    /// <param name="other">Other vertex declaration to compare to.</param>
    /// <returns>True if they are equal, false otherwise.</returns>
    public bool Equals([NotNullWhen(true)] VertexLayout? other)
    {
      if (other is not null)
        return other.m_hash == m_hash;

      return false;
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
    public override int GetHashCode()
    {
      return m_hash;
    }

    /// <summary>
    /// Reads the object data from the input.
    /// </summary>
    /// <param name="input">Savable reader</param>
    public void Read(ISavableReader input)
    {
      m_vertexStride = input.ReadInt32("VertexStride");
      m_elements = input.ReadArray<VertexElement>("Elements");

      //Precompute hash
      ComputeHashCode();
    }

    /// <summary>
    /// Writes the object data to the output.
    /// </summary>
    /// <param name="output">Savable writer</param>
    public void Write(ISavableWriter output)
    {
      output.Write("VertexStride", m_vertexStride);
      output.Write<VertexElement>("Elements", m_elements);
    }

    private void ComputeHashCode()
    {
      unchecked
      {
        int hash = 17;

        hash = (hash * 31) + m_vertexStride.GetHashCode();

        for (int i = 0; i < m_elements.Length; i++)
          hash = (hash * 31) + m_elements[i].GetHashCode();

        m_hash = hash;
      }
    }
  }
}
