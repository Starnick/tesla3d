﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tesla.Graphics
{
  public static class ComputedParameter
  {
    private static Dictionary<String, IComputedParameterProvider> s_providers;

    public static IGameTime GameTime { get; set; }
    public static Random Random { get; private set; }
    public static IComputedParameterProvider WorldMatrix { get; private set; }
    public static IComputedParameterProvider ViewMatrix { get; private set; }
    public static IComputedParameterProvider ProjectionMatrix { get; private set; }
    public static IComputedParameterProvider WorldViewMatrix { get; private set; }
    public static IComputedParameterProvider ViewProjectionMatrix { get; private set; }
    public static IComputedParameterProvider WorldViewProjection { get; private set; }
    public static IComputedParameterProvider WorldMatrixInverse { get; private set; }
    public static IComputedParameterProvider ViewMatrixInverse { get; private set; }
    public static IComputedParameterProvider ProjectionMatrixInverse { get; private set; }
    public static IComputedParameterProvider WorldViewMatrixInverse { get; private set; }
    public static IComputedParameterProvider ViewProjectionMatrixInverse { get; private set; }
    public static IComputedParameterProvider WorldViewProjectionInverse { get; private set; }
    public static IComputedParameterProvider WorldInverseTranspose { get; private set; }
    public static IComputedParameterProvider Viewport { get; private set; }
    public static IComputedParameterProvider Resolution { get; private set; }
    public static IComputedParameterProvider FrustumNearFar { get; private set; }
    public static IComputedParameterProvider FrustumNear { get; private set; }
    public static IComputedParameterProvider FrustumFar { get; private set; }
    public static IComputedParameterProvider AspectRatio { get; private set; }
    public static IComputedParameterProvider CameraPosition { get; private set; }
    public static IComputedParameterProvider CameraRight { get; private set; }
    public static IComputedParameterProvider CameraUp { get; private set; }
    public static IComputedParameterProvider CameraDirection { get; private set; }
    public static IComputedParameterProvider GlobalAmbientColor { get; private set; }
    public static IComputedParameterProvider LightList { get; private set; }
    public static IComputedParameterProvider LightCount { get; private set; }
    public static IComputedParameterProvider Time { get; private set; }
    public static IComputedParameterProvider TimePerFrame { get; private set; }
    public static IComputedParameterProvider FrameRate { get; private set; }
    public static IComputedParameterProvider RandomFloat { get; private set; }
    public static IComputedParameterProvider RandomInt { get; private set; }

    static ComputedParameter()
    {
      s_providers = new Dictionary<String, IComputedParameterProvider>();
      Random = Random.Shared;

      WorldMatrix = new WorldMatrixComputedParameter();
      Register(WorldMatrix);

      ViewMatrix = new ViewMatrixComputedParameter();
      Register(ViewMatrix);

      ProjectionMatrix = new ProjectionMatrixComputedParameter();
      Register(ProjectionMatrix);

      WorldViewMatrix = new WorldViewMatrixComputedParameter();
      Register(WorldViewMatrix);

      ViewProjectionMatrix = new ViewProjectionMatrixComputedParameter();
      Register(ViewProjectionMatrix);

      WorldViewProjection = new WorldViewProjectionComputedParameter();
      Register(WorldViewProjection);

      WorldMatrixInverse = new WorldMatrixInverseComputedParameter();
      Register(WorldMatrixInverse);

      ViewMatrixInverse = new ViewMatrixInverseComputedParameter();
      Register(ViewMatrixInverse);

      ProjectionMatrixInverse = new ProjectionMatrixInverseComputedParameter();
      Register(ProjectionMatrixInverse);

      WorldViewMatrixInverse = new WorldViewMatrixInverseComputedParameter();
      Register(WorldViewMatrixInverse);

      ViewProjectionMatrixInverse = new ViewProjectionMatrixInverseComputedParameter();
      Register(ViewProjectionMatrixInverse);

      WorldViewProjectionInverse = new WorldViewProjectionInverseComputedParameter();
      Register(WorldViewProjectionInverse);

      WorldInverseTranspose = new WorldInverseTransposeComputedParameter();
      Register(WorldInverseTranspose);

      Viewport = new ViewportComputedParameter();
      Register(Viewport);

      Resolution = new ResolutionComputedParameter();
      Register(Resolution);

      FrustumNearFar = new FrustumNearFarComputedParameter();
      Register(FrustumNearFar);

      FrustumNear = new FrustumNearComputedParameter();
      Register(FrustumNear);

      FrustumFar = new FrustumFarComputedParameter();
      Register(FrustumFar);

      AspectRatio = new AspectRatioComputedParameter();
      Register(AspectRatio);

      CameraPosition = new CameraPositionComputedParameter();
      Register(CameraPosition);

      CameraRight = new CameraRightComputedParameter();
      Register(CameraRight);

      CameraUp = new CameraUpComputedParameter();
      Register(CameraUp);

      CameraDirection = new CameraDirectionComputedParameter();
      Register(CameraDirection);

      GlobalAmbientColor = new GlobalAmbientComputedParameter();
      Register(GlobalAmbientColor);

      LightList = new LightListComputedParameter();
      Register(LightList);

      LightCount = new LightCountComputedParameter();
      Register(LightCount);

      Time = new TimeComputedParameter();
      Register(Time);

      TimePerFrame = new TimePerFrameComputedParameter();
      Register(TimePerFrame);

      FrameRate = new FrameRateComputedParameter();
      Register(FrameRate);

      RandomFloat = new RandomFloatComputedParameter();
      Register(RandomFloat);

      RandomInt = new RandomIntComputedParameter();
      Register(RandomInt);
    }

    public static bool Register(IComputedParameterProvider provider)
    {
      if (provider == null)
        return false;

      if (s_providers.ContainsKey(provider.ComputedParameterName))
        return false;

      s_providers.Add(provider.ComputedParameterName, provider);
      return true;
    }

    public static IComputedParameterProvider GetProvider(String computedParameterName)
    {
      if (String.IsNullOrEmpty(computedParameterName))
        return null;

      IComputedParameterProvider provider;
      if (s_providers.TryGetValue(computedParameterName, out provider))
        return provider;

      return null;
    }

    /// <summary>
    /// Sets the seed value of the random number generator.
    /// </summary>
    /// <param name="seed">Seed value.</param>
    public static void SetRandomSeed(int seed)
    {
      Random = new Random(seed);
    }

    /// <summary>
    /// Sets the random number generator from a shared instance.
    /// </summary>
    /// <param name="rand">Random number generator.</param>
    public static void SetRandom(Random rand)
    {
      Random = rand;
    }

    /// <summary>
    /// Gets the next non-negative integer.
    /// </summary>
    /// <returns>Random integer number.</returns>
    public static int NextRandomInt()
    {
      return Random.Next();
    }

    /// <summary>
    /// Gets the next non-negative integer less than the specified maximum range.
    /// </summary>
    /// <param name="maxValue">Maximum range.</param>
    /// <returns>Random integer number.</returns>
    public static int NextRandomInt(int maxValue)
    {
      return Random.Next(maxValue);
    }

    /// <summary>
    /// Gets the next random integer within the specified range.
    /// </summary>
    /// <param name="low">Minimum range.</param>
    /// <param name="high">Maximum range.</param>
    /// <returns>Random integer number in the range.</returns>
    public static int NextRandomInt(int low, int high)
    {
      return Random.Next(low, high);
    }

    /// <summary>
    /// Gets the next random float from the random number generator, in the range of [0.0, 1.0].
    /// </summary>
    /// <returns>Random float number between [0.0, 1.0].</returns>
    public static float NextRandomFloat()
    {
      return (float) Random.NextDouble();
    }
  }

  #region Predefined Computed Parameters

  #region Base classes

  public abstract class BaseComputedParameterProvider : IComputedParameterProvider
  {
    private String m_name;

    public String ComputedParameterName
    {
      get
      {
        return m_name;
      }
    }

    protected BaseComputedParameterProvider(String name)
    {
      m_name = name;
    }

    public abstract bool ValidateParameter(IEffectParameter parameter);

    public abstract void UpdateParameter(IRenderContext context, RenderPropertyCollection properties, Material material, IEffectParameter parameter, ref Object localState);
  }

  public abstract class BaseMatrixComputedParameterProvider : BaseComputedParameterProvider
  {
    protected BaseMatrixComputedParameterProvider(String name) : base(name) { }

    public override bool ValidateParameter(IEffectParameter parameter)
    {
      if (parameter == null)
        return false;

      return (parameter.ParameterClass == EffectParameterClass.MatrixColumns || parameter.ParameterClass == EffectParameterClass.MatrixRows)
          && parameter.RowCount == 4 && parameter.ColumnCount == 4 && parameter.SizeInBytes == Matrix.SizeInBytes;
    }
  }

  public abstract class BaseVector2ComputedParameterProvider : BaseComputedParameterProvider
  {
    protected BaseVector2ComputedParameterProvider(String name) : base(name) { }

    public override bool ValidateParameter(IEffectParameter parameter)
    {
      if (parameter == null)
        return false;

      return parameter.ParameterClass == EffectParameterClass.Vector && parameter.RowCount == 1 && parameter.ColumnCount == 2
          && parameter.SizeInBytes == Vector2.SizeInBytes;
    }
  }

  public abstract class BaseVector3ComputedParameterProvider : BaseComputedParameterProvider
  {
    protected BaseVector3ComputedParameterProvider(String name) : base(name) { }

    public override bool ValidateParameter(IEffectParameter parameter)
    {
      if (parameter == null)
        return false;

      return parameter.ParameterClass == EffectParameterClass.Vector && parameter.RowCount == 1 && parameter.ColumnCount == 3
          && parameter.SizeInBytes == Vector3.SizeInBytes;
    }
  }

  public abstract class BaseVector4ComputedParameterProvider : BaseComputedParameterProvider
  {
    protected BaseVector4ComputedParameterProvider(String name) : base(name) { }

    public override bool ValidateParameter(IEffectParameter parameter)
    {
      if (parameter == null)
        return false;

      return parameter.ParameterClass == EffectParameterClass.Vector && parameter.RowCount == 1 && parameter.ColumnCount == 4
          && parameter.SizeInBytes == Vector4.SizeInBytes;
    }
  }

  public abstract class BaseFloatComputedParameterProvider : BaseComputedParameterProvider
  {
    protected BaseFloatComputedParameterProvider(String name) : base(name) { }

    public override bool ValidateParameter(IEffectParameter parameter)
    {
      if (parameter == null)
        return false;

      return parameter.ParameterClass == EffectParameterClass.Scalar && parameter.ParameterType == EffectParameterType.Single &&
          parameter.RowCount == 1 && parameter.ColumnCount == 1 && parameter.SizeInBytes == sizeof(float);
    }
  }

  public abstract class BaseIntComputedParameterProvider : BaseComputedParameterProvider
  {
    protected BaseIntComputedParameterProvider(String name) : base(name) { }

    public override bool ValidateParameter(IEffectParameter parameter)
    {
      if (parameter == null)
        return false;

      return parameter.ParameterClass == EffectParameterClass.Scalar && parameter.ParameterType == EffectParameterType.Int32 &&
          parameter.RowCount == 1 && parameter.ColumnCount == 1 && parameter.SizeInBytes == sizeof(int);
    }
  }

  public abstract class BaseBooleanComputedParameterProvider : BaseComputedParameterProvider
  {
    protected BaseBooleanComputedParameterProvider(String name) : base(name) { }

    public override bool ValidateParameter(IEffectParameter parameter)
    {
      if (parameter == null)
        return false;

      return parameter.ParameterClass == EffectParameterClass.Scalar && parameter.ParameterType == EffectParameterType.Bool &&
          parameter.RowCount == 1 && parameter.ColumnCount == 1 && parameter.SizeInBytes == Bool.SizeInBytes;
    }
  }

  #endregion

  #region Matrix

  internal sealed class WorldMatrixComputedParameter : BaseMatrixComputedParameterProvider
  {
    public WorldMatrixComputedParameter() : base("WorldMatrix") { }

    public override void UpdateParameter(IRenderContext context, RenderPropertyCollection properties, Material material, IEffectParameter parameter, ref Object localState)
    {
      ref readonly Matrix world = ref ComputedParameterHelpers.GetWorldMatrixOrIdentity(properties);
      parameter.SetValue(world);
    }
  }

  internal sealed class ViewMatrixComputedParameter : BaseMatrixComputedParameterProvider
  {
    public ViewMatrixComputedParameter() : base("ViewMatrix") { }

    public override void UpdateParameter(IRenderContext context, RenderPropertyCollection properties, Material material, IEffectParameter parameter, ref Object localState)
    {
      Camera cam = ComputedParameterHelpers.GetCamera(context, properties);
      parameter.SetValue(cam.ViewMatrix);
    }
  }

  internal sealed class ProjectionMatrixComputedParameter : BaseMatrixComputedParameterProvider
  {
    public ProjectionMatrixComputedParameter() : base("ProjectionMatrix") { }

    public override void UpdateParameter(IRenderContext context, RenderPropertyCollection properties, Material material, IEffectParameter parameter, ref Object localState)
    {
      Camera cam = ComputedParameterHelpers.GetCamera(context, properties);
      parameter.SetValue(cam.ProjectionMatrix);
    }
  }

  internal sealed class WorldViewMatrixComputedParameter : BaseMatrixComputedParameterProvider
  {
    public WorldViewMatrixComputedParameter() : base("WorldViewMatrix") { }

    public override void UpdateParameter(IRenderContext context, RenderPropertyCollection properties, Material material, IEffectParameter parameter, ref Object localState)
    {
      ref readonly Matrix world = ref ComputedParameterHelpers.GetWorldMatrixOrIdentity(properties);
      Camera cam = ComputedParameterHelpers.GetCamera(context, properties);


      Matrix wv;
      Matrix.Multiply(world, cam.ViewMatrix, out wv);

      parameter.SetValue(wv);
    }
  }

  internal sealed class ViewProjectionMatrixComputedParameter : BaseMatrixComputedParameterProvider
  {
    public ViewProjectionMatrixComputedParameter() : base("ViewProjectionMatrix") { }

    public override void UpdateParameter(IRenderContext context, RenderPropertyCollection properties, Material material, IEffectParameter parameter, ref Object localState)
    {
      Camera cam = ComputedParameterHelpers.GetCamera(context, properties);
      parameter.SetValue(cam.ViewProjectionMatrix);
    }
  }

  internal sealed class WorldViewProjectionComputedParameter : BaseMatrixComputedParameterProvider
  {
    public WorldViewProjectionComputedParameter() : base("WorldViewProjection") { }

    public override void UpdateParameter(IRenderContext context, RenderPropertyCollection properties, Material material, IEffectParameter parameter, ref Object localState)
    {
      ref readonly Matrix world = ref ComputedParameterHelpers.GetWorldMatrixOrIdentity(properties);
      Camera cam = ComputedParameterHelpers.GetCamera(context, properties);
      Matrix.Multiply(world, cam.ViewProjectionMatrix, out Matrix wvp);

      parameter.SetValue(wvp);
    }
  }

  internal sealed class WorldMatrixInverseComputedParameter : BaseMatrixComputedParameterProvider
  {
    public WorldMatrixInverseComputedParameter() : base("WorldMatrixInverse") { }

    public override void UpdateParameter(IRenderContext context, RenderPropertyCollection properties, Material material, IEffectParameter parameter, ref Object localState)
    {
      WorldTransformProperty prop;
      if (properties.TryGet<WorldTransformProperty>(out prop))
      {
        Matrix world;
        Matrix.Invert(prop.Value.Matrix, out world);

        parameter.SetValue(world);
      }
      else
      {
        parameter.SetValue(Matrix.Identity);
      }
    }
  }

  internal sealed class ViewMatrixInverseComputedParameter : BaseMatrixComputedParameterProvider
  {
    public ViewMatrixInverseComputedParameter() : base("ViewMatrixInverse") { }

    public override void UpdateParameter(IRenderContext context, RenderPropertyCollection properties, Material material, IEffectParameter parameter, ref Object localState)
    {
      Camera cam = ComputedParameterHelpers.GetCamera(context, properties);
      Matrix.Invert(cam.ViewMatrix, out Matrix viewInverse);

      parameter.SetValue(viewInverse);
    }
  }

  internal sealed class ProjectionMatrixInverseComputedParameter : BaseMatrixComputedParameterProvider
  {
    public ProjectionMatrixInverseComputedParameter() : base("ProjectionMatrixInverse") { }

    public override void UpdateParameter(IRenderContext context, RenderPropertyCollection properties, Material material, IEffectParameter parameter, ref Object localState)
    {
      Camera cam = ComputedParameterHelpers.GetCamera(context, properties);
      Matrix.Invert(context.Camera.ProjectionMatrix, out Matrix proj);

      parameter.SetValue(proj);
    }
  }

  internal sealed class WorldViewMatrixInverseComputedParameter : BaseMatrixComputedParameterProvider
  {
    public WorldViewMatrixInverseComputedParameter() : base("WorldViewMatrixInverse") { }

    public override void UpdateParameter(IRenderContext context, RenderPropertyCollection properties, Material material, IEffectParameter parameter, ref Object localState)
    {
      ref readonly Matrix world = ref ComputedParameterHelpers.GetWorldMatrixOrIdentity(properties);
      Camera cam = ComputedParameterHelpers.GetCamera(context, properties);

      Matrix.Multiply(world, cam.ViewMatrix, out Matrix wv);
      wv.Invert();

      parameter.SetValue(wv);
    }
  }

  internal sealed class ViewProjectionMatrixInverseComputedParameter : BaseMatrixComputedParameterProvider
  {
    public ViewProjectionMatrixInverseComputedParameter() : base("ViewProjectionMatrixInverse") { }

    public override void UpdateParameter(IRenderContext context, RenderPropertyCollection properties, Material material, IEffectParameter parameter, ref Object localState)
    {
      Camera cam = ComputedParameterHelpers.GetCamera(context, properties);
      Matrix.Invert(cam.ViewMatrix, out Matrix viewProj);

      parameter.SetValue(viewProj);
    }
  }

  internal sealed class WorldViewProjectionInverseComputedParameter : BaseMatrixComputedParameterProvider
  {
    public WorldViewProjectionInverseComputedParameter() : base("WorldViewProjectionInverse") { }

    public override void UpdateParameter(IRenderContext context, RenderPropertyCollection properties, Material material, IEffectParameter parameter, ref Object localState)
    {
      ref readonly Matrix world = ref ComputedParameterHelpers.GetWorldMatrixOrIdentity(properties);
      Camera cam = ComputedParameterHelpers.GetCamera(context, properties);
      Matrix.Multiply(world, cam.ViewProjectionMatrix, out Matrix wvp);
      wvp.Invert();

      parameter.SetValue(wvp);
    }
  }

  internal sealed class WorldInverseTransposeComputedParameter : BaseMatrixComputedParameterProvider
  {
    public WorldInverseTransposeComputedParameter() : base("WorldInverseTranspose") { }

    public override void UpdateParameter(IRenderContext context, RenderPropertyCollection properties, Material material, IEffectParameter parameter, ref Object localState)
    {
      WorldTransformProperty prop;
      if (properties.TryGet<WorldTransformProperty>(out prop))
      {
        Matrix world;

        //Invert, then transpose
        Matrix.Invert(prop.Value.Matrix, out world);
        world.Transpose();

        parameter.SetValue(world);
      }
      else
      {
        parameter.SetValue(Matrix.Identity);
      }
    }
  }

  #endregion

  #region Camera

  internal sealed class ViewportComputedParameter : BaseVector4ComputedParameterProvider
  {
    public ViewportComputedParameter() : base("Viewport") { }

    public override void UpdateParameter(IRenderContext context, RenderPropertyCollection properties, Material material, IEffectParameter parameter, ref Object localState)
    {
      Camera cam = ComputedParameterHelpers.GetCamera(context, properties);
      Viewport vp = cam.Viewport;
      Vector4 v = new Vector4(vp.X, vp.Width, vp.Y, vp.Height);

      parameter.SetValue<Vector4>(v);
    }
  }

  internal sealed class ResolutionComputedParameter : BaseVector2ComputedParameterProvider
  {
    public ResolutionComputedParameter() : base("Resolution") { }

    public override void UpdateParameter(IRenderContext context, RenderPropertyCollection properties, Material material, IEffectParameter parameter, ref Object localState)
    {
      Camera cam = ComputedParameterHelpers.GetCamera(context, properties);
      Viewport vp = cam.Viewport;
      Vector2 v = new Vector2(vp.Width, vp.Height);

      parameter.SetValue<Vector2>(v);
    }
  }

  internal sealed class FrustumNearFarComputedParameter : BaseVector2ComputedParameterProvider
  {
    public FrustumNearFarComputedParameter() : base("FrustumNearFar") { }

    public override void UpdateParameter(IRenderContext context, RenderPropertyCollection properties, Material material, IEffectParameter parameter, ref Object localState)
    {
      Camera cam = ComputedParameterHelpers.GetCamera(context, properties);
      Vector2 v = new Vector2(cam.NearPlaneDistance, cam.FarPlaneDistance);

      parameter.SetValue<Vector2>(v);
    }
  }

  internal sealed class FrustumNearComputedParameter : BaseFloatComputedParameterProvider
  {
    public FrustumNearComputedParameter() : base("FrustumNear") { }

    public override void UpdateParameter(IRenderContext context, RenderPropertyCollection properties, Material material, IEffectParameter parameter, ref Object localState)
    {
      Camera cam = ComputedParameterHelpers.GetCamera(context, properties);
      parameter.SetValue<float>(cam.NearPlaneDistance);
    }
  }

  internal sealed class FrustumFarComputedParameter : BaseFloatComputedParameterProvider
  {
    public FrustumFarComputedParameter() : base("FrustumFar") { }

    public override void UpdateParameter(IRenderContext context, RenderPropertyCollection properties, Material material, IEffectParameter parameter, ref Object localState)
    {
      Camera cam = ComputedParameterHelpers.GetCamera(context, properties);
      parameter.SetValue<float>(cam.FarPlaneDistance);
    }
  }

  internal sealed class AspectRatioComputedParameter : BaseFloatComputedParameterProvider
  {
    public AspectRatioComputedParameter() : base("AspectRatio") { }

    public override void UpdateParameter(IRenderContext context, RenderPropertyCollection properties, Material material, IEffectParameter parameter, ref Object localState)
    {
      Camera cam = ComputedParameterHelpers.GetCamera(context, properties);
      float aspectRatio = cam.Viewport.AspectRatio;

      parameter.SetValue<float>(aspectRatio);
    }
  }

  internal sealed class CameraPositionComputedParameter : BaseVector3ComputedParameterProvider
  {
    public CameraPositionComputedParameter() : base("CameraPosition") { }

    public override void UpdateParameter(IRenderContext context, RenderPropertyCollection properties, Material material, IEffectParameter parameter, ref Object localState)
    {
      Camera cam = ComputedParameterHelpers.GetCamera(context, properties);
      Vector3 camPos = cam.Position;

      parameter.SetValue<Vector3>(camPos);
    }
  }

  internal sealed class CameraRightComputedParameter : BaseVector3ComputedParameterProvider
  {
    public CameraRightComputedParameter() : base("CameraRight") { }

    public override void UpdateParameter(IRenderContext context, RenderPropertyCollection properties, Material material, IEffectParameter parameter, ref Object localState)
    {
      Camera cam = ComputedParameterHelpers.GetCamera(context, properties);
      Vector3 camRight = cam.Right;

      parameter.SetValue<Vector3>(camRight);
    }
  }

  internal sealed class CameraUpComputedParameter : BaseVector3ComputedParameterProvider
  {
    public CameraUpComputedParameter() : base("CameraUp") { }

    public override void UpdateParameter(IRenderContext context, RenderPropertyCollection properties, Material material, IEffectParameter parameter, ref Object localState)
    {
      Camera cam = ComputedParameterHelpers.GetCamera(context, properties);
      Vector3 camUp = cam.Up;

      parameter.SetValue<Vector3>(camUp);
    }
  }

  internal sealed class CameraDirectionComputedParameter : BaseVector3ComputedParameterProvider
  {
    public CameraDirectionComputedParameter() : base("CameraDirection") { }

    public override void UpdateParameter(IRenderContext context, RenderPropertyCollection properties, Material material, IEffectParameter parameter, ref Object localState)
    {
      Camera cam = ComputedParameterHelpers.GetCamera(context, properties);
      Vector3 camDir = cam.Direction;

      parameter.SetValue<Vector3>(camDir);
    }
  }

  #endregion

  #region Lighting

  internal sealed class GlobalAmbientComputedParameter : BaseVector3ComputedParameterProvider
  {
    public GlobalAmbientComputedParameter() : base("GlobalAmbientColor") { }

    public override void UpdateParameter(IRenderContext context, RenderPropertyCollection properties, Material material, IEffectParameter parameter, ref Object localState)
    {
      LightCollectionProperty prop;
      if (!properties.TryGet<LightCollectionProperty>(out prop))
        return;

      LightCollection lights = prop.Value;
      if (!lights.UpdateShader || !lights.IsEnabled)
        return;

      parameter.SetValue<Vector3>(lights.GlobalAmbient.ToVector3());
    }
  }

  internal sealed class LightListComputedParameter : IComputedParameterProvider
  {
    public String ComputedParameterName
    {
      get
      {
        return "LightList";
      }
    }

    public LightListComputedParameter() { }

    public bool ValidateParameter(IEffectParameter parameter)
    {
      if (parameter.Elements.Count == 0)
      {
        return ValidateElement(parameter);
      }
      else
      {
        for (int i = 0; i < parameter.Elements.Count; i++)
        {
          if (!ValidateElement(parameter.Elements[i]))
            return false;
        }

        return true;
      }
    }

    public void UpdateParameter(IRenderContext context, RenderPropertyCollection properties, Material material, IEffectParameter parameter, ref Object localState)
    {
      LightCollectionProperty prop;
      if (!properties.TryGet<LightCollectionProperty>(out prop))
        return;

      LightCollection lights = prop.Value;
      if (!lights.UpdateShader || !lights.IsEnabled)
        return;

      int elemCount = parameter.Elements.Count;
      if (elemCount == 0)
      {
        SetLight(lights[0], parameter);
        return;
      }

      if (lights.Count <= elemCount)
        elemCount = lights.Count;

      for (int i = 0; i < elemCount; i++)
        SetLight(lights[i], parameter.Elements[i]);
    }

    private bool ValidateElement(IEffectParameter lParam)
    {
      if (lParam.StructureMembers.Count != 11)
        return false;

      return ValidateFloat(lParam.StructureMembers[0], 3) && ValidateBoolean(lParam.StructureMembers[1]) && ValidateFloat(lParam.StructureMembers[2], 3) &&
          ValidateFloat(lParam.StructureMembers[3], 1) && ValidateFloat(lParam.StructureMembers[4], 3) && ValidateFloat(lParam.StructureMembers[5], 1) &&
          ValidateFloat(lParam.StructureMembers[6], 3) && ValidateInt(lParam.StructureMembers[7], 1) && ValidateFloat(lParam.StructureMembers[8], 3)
          && ValidateFloat(lParam.StructureMembers[9], 1) && ValidateFloat(lParam.StructureMembers[10], 1);
    }

    private bool ValidateInt(IEffectParameter lParam, int count)
    {
      if (count == 1 && lParam.ParameterClass != EffectParameterClass.Scalar)
        return false;

      if (count > 1 && lParam.ParameterClass != EffectParameterClass.Vector)
        return false;

      return lParam.ParameterType == EffectParameterType.Int32 &&
          lParam.RowCount == 1 && lParam.ColumnCount == count && (lParam.SizeInBytes == sizeof(int) * count);
    }

    private bool ValidateFloat(IEffectParameter lParam, int count)
    {
      if (count == 1 && lParam.ParameterClass != EffectParameterClass.Scalar)
        return false;

      if (count > 1 && lParam.ParameterClass != EffectParameterClass.Vector)
        return false;

      return lParam.ParameterType == EffectParameterType.Single &&
          lParam.RowCount == 1 && lParam.ColumnCount == count && (lParam.SizeInBytes == sizeof(float) * count);
    }

    private bool ValidateBoolean(IEffectParameter lParam)
    {
      return lParam.ParameterClass == EffectParameterClass.Scalar && lParam.ParameterType == EffectParameterType.Bool &&
          lParam.RowCount == 1 && lParam.ColumnCount == 1 && lParam.SizeInBytes == Bool.SizeInBytes;
    }

    private void SetLight(Light light, IEffectParameter lParam)
    {
      if (light == null || lParam == null)
        return;

      float invRangeSquared = 0.0f;

      if (!MathHelper.IsNearlyZero(light.Range))
        invRangeSquared = 1.0f / (light.Range * light.Range);

      //Indices are out of order because in the shader we're grouping them in a way to be tightly packed
      //and reduce padding
      if (light.LightType == LightType.Directional)
      {
        DirectionalLight dl = light as DirectionalLight;
        lParam.StructureMembers[0].SetValue(dl.Ambient.ToVector3());
        lParam.StructureMembers[1].SetValue(new Bool(false));
        lParam.StructureMembers[2].SetValue(dl.Diffuse.ToVector3());
        lParam.StructureMembers[3].SetValue(invRangeSquared);
        lParam.StructureMembers[4].SetValue(dl.Specular.ToVector3());
        lParam.StructureMembers[5].SetValue(0f);
        lParam.StructureMembers[6].SetValue(Vector3.Zero);
        lParam.StructureMembers[7].SetValue((uint) dl.LightType);
        lParam.StructureMembers[8].SetValue(dl.Direction);
        lParam.StructureMembers[9].SetValue(0f);
        lParam.StructureMembers[10].SetValue(dl.Intensity);
      }
      else if (light.LightType == LightType.Point)
      {
        PointLight pl = light as PointLight;
        lParam.StructureMembers[0].SetValue(pl.Ambient.ToVector3());
        lParam.StructureMembers[1].SetValue(new Bool(pl.Attenuate));
        lParam.StructureMembers[2].SetValue(pl.Diffuse.ToVector3());
        lParam.StructureMembers[3].SetValue(invRangeSquared);
        lParam.StructureMembers[4].SetValue(pl.Specular.ToVector3());
        lParam.StructureMembers[5].SetValue(0f);
        lParam.StructureMembers[6].SetValue(pl.Position);
        lParam.StructureMembers[7].SetValue((uint) pl.LightType);
        lParam.StructureMembers[8].SetValue(Vector3.Down);
        lParam.StructureMembers[9].SetValue(0f);
        lParam.StructureMembers[10].SetValue(pl.Intensity);
      }
      else if (light.LightType == LightType.Spot)
      {
        SpotLight sl = light as SpotLight;
        lParam.StructureMembers[0].SetValue(sl.Ambient.ToVector3());
        lParam.StructureMembers[1].SetValue(new Bool(sl.Attenuate));
        lParam.StructureMembers[2].SetValue(sl.Diffuse.ToVector3());
        lParam.StructureMembers[3].SetValue(invRangeSquared);
        lParam.StructureMembers[4].SetValue(sl.Specular.ToVector3());
        lParam.StructureMembers[5].SetValue((sl.InnerAngle * 0.5f).Cos);
        lParam.StructureMembers[6].SetValue(sl.Position);
        lParam.StructureMembers[7].SetValue((uint) sl.LightType);
        lParam.StructureMembers[8].SetValue(sl.Direction);
        lParam.StructureMembers[9].SetValue((sl.OuterAngle * 0.5f).Cos);
        lParam.StructureMembers[10].SetValue(sl.Intensity);
      }
    }
  }

  internal sealed class LightCountComputedParameter : BaseIntComputedParameterProvider
  {
    public LightCountComputedParameter() : base("LightCount") { }

    public override void UpdateParameter(IRenderContext context, RenderPropertyCollection properties, Material material, IEffectParameter parameter, ref Object localState)
    {
      int lightCount = 0;
      LightCollectionProperty prop;
      if (!properties.TryGet<LightCollectionProperty>(out prop))
        return;

      LightCollection lights = prop.Value;

      //Return if no need to update, but if not enabled, we do want to set the lightcount to zero
      if (!lights.UpdateShader)
        return;

      //Create a local state object if not created
      if (localState == null)
      {
        //Find a parameter related to the light list to determine a maximum amount of lights
        IComputedParameterProvider lightListProvider = ComputedParameter.LightList;
        foreach (Material.ComputedParameterBinding binding in material.ComputedParameterBindings)
        {
          if (binding.Provider == lightListProvider)
          {
            localState = new LightCountMax(Math.Max(1, binding.Parameter.Elements.Count));
            break;
          }
        }

        //Shouldn't fail, but just in case...
        if (localState == null)
          return;
      }

      lightCount = (lights.IsEnabled) ? lights.Count : 0;
      lightCount = Math.Min((localState as LightCountMax).MaxLights, lightCount);

      parameter.SetValue<int>(lightCount);
    }

    private sealed class LightCountMax
    {
      public int MaxLights;

      public LightCountMax(int max)
      {
        MaxLights = max;
      }
    }
  }

  #endregion

  #region Time

  internal sealed class TimeComputedParameter : BaseFloatComputedParameterProvider
  {
    public TimeComputedParameter() : base("Time") { }

    public override void UpdateParameter(IRenderContext context, RenderPropertyCollection properties, Material material, IEffectParameter parameter, ref Object localState)
    {
      float time = (float) ComputedParameter.GameTime.TotalGameTime.TotalSeconds;

      parameter.SetValue<float>(time);
    }
  }

  internal sealed class TimePerFrameComputedParameter : BaseFloatComputedParameterProvider
  {
    public TimePerFrameComputedParameter() : base("TimePerFrame") { }

    public override void UpdateParameter(IRenderContext context, RenderPropertyCollection properties, Material material, IEffectParameter parameter, ref Object localState)
    {
      float tpf = ComputedParameter.GameTime.ElapsedTimeInSeconds;

      parameter.SetValue<float>(tpf);
    }
  }

  internal sealed class FrameRateComputedParameter : BaseFloatComputedParameterProvider
  {
    public FrameRateComputedParameter() : base("FrameRate") { }

    public override void UpdateParameter(IRenderContext context, RenderPropertyCollection properties, Material material, IEffectParameter parameter, ref Object localState)
    {
      float frameRate = 1.0f / (float) ComputedParameter.GameTime.ElapsedTimeInSeconds;

      parameter.SetValue<float>(frameRate);
    }
  }

  #endregion

  #region Random

  internal sealed class RandomFloatComputedParameter : BaseComputedParameterProvider
  {
    public RandomFloatComputedParameter() : base("RandomFloat") { }

    public override void UpdateParameter(IRenderContext context, RenderPropertyCollection properties, Material material, IEffectParameter parameter, ref Object localState)
    {
      int numValuesNeeded = parameter.ColumnCount;

      switch (numValuesNeeded)
      {
        case 1:
          float rFloat = ComputedParameter.NextRandomFloat();
          parameter.SetValue<float>(rFloat);
          break;
        case 2:
          Vector2 rFloat2 = new Vector2(ComputedParameter.NextRandomFloat(), ComputedParameter.NextRandomFloat());
          parameter.SetValue<Vector2>(rFloat2);
          break;
        case 3:
          Vector3 rFloat3 = new Vector3(ComputedParameter.NextRandomFloat(), ComputedParameter.NextRandomFloat(), ComputedParameter.NextRandomFloat());
          parameter.SetValue<Vector3>(rFloat3);
          break;
        case 4:
          Vector4 rFloat4 = new Vector4(ComputedParameter.NextRandomFloat(), ComputedParameter.NextRandomFloat(), ComputedParameter.NextRandomFloat(),
              ComputedParameter.NextRandomFloat());
          parameter.SetValue<Vector4>(rFloat4);
          break;
      }
    }

    public override bool ValidateParameter(IEffectParameter parameter)
    {
      if (parameter == null)
        return false;

      return (parameter.ParameterClass == EffectParameterClass.Scalar || parameter.ParameterClass == EffectParameterClass.Vector) &&
          parameter.ParameterType == EffectParameterType.Single && !parameter.IsArray;
    }
  }

  internal sealed class RandomIntComputedParameter : BaseComputedParameterProvider
  {
    public RandomIntComputedParameter() : base("RandomInt") { }

    public override void UpdateParameter(IRenderContext context, RenderPropertyCollection properties, Material material, IEffectParameter parameter, ref Object localState)
    {
      int numValuesNeeded = parameter.ColumnCount;

      Int2? range = null;

      RandomNumberRangeProperty prop;
      if (properties.TryGet<RandomNumberRangeProperty>(out prop))
        range = prop.Value;

      switch (numValuesNeeded)
      {
        case 1:
          int rInt = NextRandomInteger(ref range);
          parameter.SetValue<int>(rInt);
          break;
        case 2:
          Int2 rInt2 = new Int2(NextRandomInteger(ref range), NextRandomInteger(ref range));
          parameter.SetValue<Int2>(rInt2);
          break;
        case 3:
          Int3 rInt3 = new Int3(NextRandomInteger(ref range), NextRandomInteger(ref range), NextRandomInteger(ref range));
          parameter.SetValue<Int3>(rInt3);
          break;
        case 4:
          Int4 rInt4 = new Int4(NextRandomInteger(ref range), NextRandomInteger(ref range), NextRandomInteger(ref range), NextRandomInteger(ref range));
          parameter.SetValue<Int4>(rInt4);
          break;
      }
    }

    public override bool ValidateParameter(IEffectParameter parameter)
    {
      if (parameter == null)
        return false;

      return (parameter.ParameterClass == EffectParameterClass.Scalar || parameter.ParameterClass == EffectParameterClass.Vector) &&
          parameter.ParameterType == EffectParameterType.Int32 && !parameter.IsArray;
    }

    private int NextRandomInteger(ref Int2? range)
    {
      if (range.HasValue)
      {
        Int2 val = range.Value;
        return ComputedParameter.NextRandomInt(val.X, val.Y);
      }

      return ComputedParameter.NextRandomInt();
    }
  }

  #endregion

  #endregion

  public static class ComputedParameterHelpers
  {
    public static ref readonly Matrix GetWorldMatrixOrIdentity(RenderPropertyCollection properties)
    {
      WorldTransformProperty prop;
      if (properties.TryGet<WorldTransformProperty>(out prop))
        return ref prop.Value.Matrix;

      return ref Matrix.Identity;
    }

    public static Camera GetCamera(IRenderContext renderContext, RenderPropertyCollection properties)
    {
      CameraProperty prop;
      if (properties.TryGet<CameraProperty>(out prop))
      {
        return prop.Value;
      }

      return renderContext.Camera;
    }
  }
}