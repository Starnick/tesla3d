﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using Tesla.Content;

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// Common base class for all render state objects. Render states configure different stages of the graphics pipeline. When first created, the state is mutable,
  /// until it is bound either by calling <see cref="BindRenderState"/> directly or the first time the state is applied to the pipeline. This makes the state immutable thereafter,
  /// and exceptions will be thrown if properties are attempted to be set. It is best practice to re-use state objects as often as possible and to bind them early and up front. 
  /// </summary>
  [SavableVersion(1)]
  public abstract class RenderState : GraphicsResource, ISavable, IStandardLibraryContent, IEquatable<RenderState>
  {
    internal string? m_predefinedStateName;

    /// <summary>
    /// Gets if the render state has been bound to the pipeline, once bound the state becomes read-only.
    /// </summary>
    public abstract bool IsBound { get; }

    /// <summary>
    /// Gets the render state type.
    /// </summary>
    public abstract RenderStateType StateType { get; }

    /// <summary>
    /// Gets the key that identifies this render state type and configuration for comparing states.
    /// </summary>
    public abstract RenderStateKey RenderStateKey { get; }

    /// <summary>
    /// Gets the name of the content this instance represents. If <see cref="IsStandardContent" /> is false, then this returns an empty string.
    /// </summary>
    public string StandardContentName
    {
      get
      {
        return m_predefinedStateName ?? String.Empty;
      }
    }

    /// <summary>
    /// Gets if the instance represents a predefined state.
    /// </summary>
    public bool IsStandardContent
    {
      [MemberNotNullWhen(true, nameof(m_predefinedStateName))]
      get
      {
        return !String.IsNullOrEmpty(m_predefinedStateName);
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="RenderState"/> class.
    /// </summary>
    protected RenderState() { }

    /// <summary>
    /// Determines whether the specified <see cref="T:System.Object" /> is equal to the current <see cref="T:System.Object" />.
    /// </summary>
    /// <param name="obj">The object to compare with the current object.</param>
    /// <returns>True if the specified object  is equal to the current object; otherwise, false.</returns>
    public override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is RenderState rs)
        return Equals(rs);

      return false;
    }

    /// <summary>
    /// Tests equality between this render state and another, that is if they both represent the same state configuration.
    /// </summary>
    /// <param name="other">Other render state to compare against.</param>
    /// <returns>True if the two states are equal, false otherwise.</returns>
    public bool Equals([NotNullWhen(true)] RenderState? other)
    {
      if (other is null)
        return false;

      return RenderStateKey.Equals(other);
    }

    /// <summary>
    /// Tests if two objects represent the same state. This does not mean they are the same object. If both are null, then this returns true.
    /// </summary>
    /// <param name="a">First state to compare.</param>
    /// <param name="b">Second state to compare.</param>
    /// <returns>True if the two states are the same, false otherwise.</returns>
    public static bool IsSameState(RenderState? a, RenderState? b)
    {
      if (a is null && b is null)
        return true;

      if (a is not null)
        return a.IsSameState(b);

      if (b is not null)
        return b.IsSameState(a);

      return false;
    }

    /// <summary>
    /// Tests if this state represents the same state as the other. This does not mean they are the same object, however, just that the two states
    /// represent the same underlying render state.
    /// </summary>
    /// <param name="other">Other render state to compare against.</param>
    /// <returns>True if the two states are the same, false otherwise.</returns>
    public bool IsSameState([NotNullWhen(true)] RenderState? other)
    {
      if (other is null)
        return false;

      return RenderStateKey.Equals(other.RenderStateKey);
    }

    /// <summary>
    /// Gets a consistent hash code that identifies the content item. If it is not standard content, each instance should have a unique hash, if the instance is
    /// standard content, each instance should have the same hash code. This might differ from .NET's hash code and is only used to identify two instances that
    /// represent the same data (there may be situations where we want to differentiate the two instances, so we don't rely on the .NET's get hash code function).
    /// </summary>
    /// <returns>32-bit hash code.</returns>
    public int GetContentHashCode()
    {
      return RenderStateKey.Hash; //Same as normal hash code
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
    public override int GetHashCode()
    {
      return RenderStateKey.Hash;
    }

    /// <summary>
    /// Returns a string that represents the current object.
    /// </summary>
    /// <returns>A string that represents the current object.</returns>
    public override string ToString()
    {
      if (IsStandardContent)
        return m_predefinedStateName;

      CultureInfo info = CultureInfo.CurrentCulture;
      return String.Format(info, "IsBound: {0}, {1}",
          new object[] { IsBound.ToString(), RenderStateKey.ToString() });
    }

    /// <summary>
    /// Binds the render state to the graphics pipeline. If not called after the state is created, it is automatically done the first time the render state
    /// is applied. Once bound, the render state becomes immutable.
    /// </summary>
    public abstract void BindRenderState();

    /// <summary>
    /// Reads the object data from the input.
    /// </summary>
    /// <param name="input">Savable reader</param>
    public abstract void Read(ISavableReader input);

    /// <summary>
    /// Writes the object data to the output.
    /// </summary>
    /// <param name="output">Savable writer</param>
    public abstract void Write(ISavableWriter output);
  }
}
