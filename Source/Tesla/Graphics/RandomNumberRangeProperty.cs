﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// Render property that represents a [min, max] range for generating random integers for use in a shader.
  /// </summary>
  public sealed class RandomNumberRangeProperty : RenderPropertyAccessor<Int2>
  {
    private Int2 m_value;

    /// <summary>
    /// Unique ID for this render property.
    /// </summary>
    public static RenderPropertyID PropertyID = GetPropertyID<RandomNumberRangeProperty>();

    /// <summary>
    /// Constructs a new instance of the <see cref="RandomNumberRangeProperty"/> class.
    /// </summary>
    /// <param name="accessor">Accessor function.</param>
    public RandomNumberRangeProperty(GetPropertyValue<Int2> accessor) : base(PropertyID, accessor) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="RandomNumberRangeProperty"/> class.
    /// </summary>
    public RandomNumberRangeProperty() : this(new Int2(0, 0)) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="RandomNumberRangeProperty"/> class.
    /// </summary>
    /// <param name="range">Random number range.</param>
    public RandomNumberRangeProperty(Int2 range)
        : base(PropertyID)
    {
      m_value = range;
      Accessor = GetValue;
    }

    private Int2 GetValue()
    {
      return m_value;
    }
  }
}
