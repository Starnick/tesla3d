﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Tesla.Audio;
using Tesla.Content;
using Tesla.Graphics.Implementation;

// STILL TODO
#nullable enable

namespace Tesla.Graphics
{
  #region RenderSystem interfaces

  /// <summary>
  /// Defines the render system service manager, which is responsible for managing and creating all graphics objects. A render system can create render contexts and supports
  /// factories to create individual graphic objects such as buffers, textures, and shaders. Graphic resource creation is always guaranteed to be thread safe, either through
  /// concurrent creation or through locking schemes. Multithreaded rendering may or may not be supported by the implementation, if not then the <see cref="AreCommandListsSupported"/>
  /// property will be false, and <see cref="CreateDeferredRenderContext"/> will fail.
  /// </summary>
  public interface IRenderSystem : IDisposableEngineService, IEnumerable<IGraphicsResourceImplFactory>
  {
    /// <summary>
    /// Gets the current <see cref="IRenderSystem"/> registered to the <see cref="Engine.Instance"/>.
    /// </summary>
    /// <exception cref="ArgumentNullException">Thrown if <see cref="Engine.Initialize"/> has yet to be called -or- the current instance is null.</exception>
    public static IRenderSystem Current { get { return CoreServices.RenderSystem.ServiceOrThrow; } }

    /// <summary>
    /// Gets the identifier that describes the render system platform.
    /// </summary>
    string Platform { get; }

    /// <summary>
    /// Gets the immediate render context.
    /// </summary>
    IRenderContext ImmediateContext { get; }

    /// <summary>
    /// Gets the graphics adapter the render system was created with.
    /// </summary>
    IGraphicsAdapter Adapter { get; }

    /// <summary>
    /// Gets a list of optional supported features.
    /// </summary>
    SupportedFeatures FeatureSupport { get; }

    /// <summary>
    /// Gets the provider for predefined blend states.
    /// </summary>
    IPredefinedBlendStateProvider PredefinedBlendStates { get; }

    /// <summary>
    /// Gets the provider for predefined depthstencil states.
    /// </summary>
    IPredefinedDepthStencilStateProvider PredefinedDepthStencilStates { get; }

    /// <summary>
    /// Gets the provider for predefined rasterizer states.
    /// </summary>
    IPredefinedRasterizerStateProvider PredefinedRasterizerStates { get; }

    /// <summary>
    /// Gets the provider for predefined sampler states.
    /// </summary>
    IPredefinedSamplerStateProvider PredefinedSamplerStates { get; }

    /// <summary>
    /// Gets a cache for render states to promote maximum reuse of state objects.
    /// </summary>
    IRenderStateCache RenderStateCache { get;  }

    /// <summary>
    /// Gets the standard effect library for the render system.
    /// </summary>
    StandardEffectLibrary StandardEffects { get; }

    /// <summary>
    /// Creates a deferred render context. A deferred context is a thread-safe context that can be used to record graphics commands on a different
    /// thread other than the main rendering one.
    /// </summary>
    /// <returns>A newly created deferred render context.</returns>
    IDeferredRenderContext CreateDeferredRenderContext();

    /// <summary>
    /// Gets the implementation factory of the specified type.
    /// </summary>
    /// <typeparam name="T">Implementation factory type</typeparam>
    /// <returns>The registered implementation factory, if it exists. Otherwise, null is returned.</returns>
    T GetImplementationFactory<T>() where T : IGraphicsResourceImplFactory;

    /// <summary>
    /// Tries to get the implementation factory of the specified type.
    /// </summary>
    /// <typeparam name="T">Implementation factory type</typeparam>
    /// <param name="implementationFactory">The registered implementation factory, if it exists.</param>
    /// <returns>True if the factory was registered and found, false otherwise.</returns>
    bool TryGetImplementationFactory<T>(out T implementationFactory) where T : IGraphicsResourceImplFactory;

    /// <summary>
    /// Queries if the graphics resource type (e.g. VertexBuffer) is supported by any of the registered implementation factories.
    /// </summary>
    /// <typeparam name="T">Graphics resource type</typeparam>
    /// <returns>True if the type is supported by an implementation factory, false otherwise.</returns>
    bool IsSupported<T>() where T : GraphicsResource;
  }

  /// <summary>
  /// Defines an adapter that represents the physical GPU and enumerates useful information about features and resource limits supported by the hardware.
  /// </summary>
  public interface IGraphicsAdapter
  {
    /// <summary>
    /// Gets the collection of supported display modes.
    /// </summary>
    DisplayModeCollection SupportedDisplayModes { get; }

    /// <summary>
    /// Gets the collection of outputs (e.g. monitors).
    /// </summary>
    OutputCollection Outputs { get; }

    /// <summary>
    /// Gets the description of the device.
    /// </summary>
    string Description { get; }

    /// <summary>
    /// Gets the device ID which identifies the particular chipset.
    /// </summary>
    int DeviceId { get; }

    /// <summary>
    /// Gets the adapter's revision number for the particular chipset its associated with.
    /// </summary>
    int Revision { get; }

    /// <summary>
    /// Gets the value that identifies the adapter's subsystem.
    /// </summary>
    int SubSystemId { get; }

    /// <summary>
    /// Gets the value that identifies that chipset's manufacturer.
    /// </summary>
    int VendorId { get; }

    /// <summary>
    /// Gets if this is the default adapter, always the first adapter.
    /// </summary>
    bool IsDefaultAdapter { get; }

    /// <summary>
    /// Gets the adapter index.
    /// </summary>
    int AdapterIndex { get; }

    /// <summary>
    /// Gets the maximum (U) size of a Texture1D resource.
    /// </summary>
    int MaximumTexture1DSize { get; }

    /// <summary>
    /// Gets the maximum number of array slices in a Texture1DArray resource, if zero, arrays are not supported.
    /// </summary>
    int MaximumTexture1DArrayCount { get; }

    /// <summary>
    /// Gets the maximum size (U,V) of a Texture2D resource.
    /// </summary>
    int MaximumTexture2DSize { get; }

    /// <summary>
    /// Gets the maximum number of array slices in a Texture2DArray resource, if zero, arrays are not supported.
    /// </summary>
    int MaximumTexture2DArrayCount { get; }

    /// <summary>
    /// Gets the maximum size (U,V,W) of a Texture3D resource.
    /// </summary>
    int MaximumTexture3DSize { get; }

    /// <summary>
    /// Gets the maximum size of a TextureCube resource.
    /// </summary>
    int MaximumTextureCubeSize { get; }

    /// <summary>
    /// Gets the maximum number of array slices in a Texture2DArray resource, if zero, arrays are not supported.
    /// </summary>
    int MaximumTextureCubeArrayCount { get; }

    /// <summary>
    /// Gets the maximum size of any texture resource in bytes.
    /// </summary>
    int MaximumTextureResourceSize { get; }

    /// <summary>
    /// Gets the maximum number of render targets that can be set to the
    /// device at once (MRT).
    /// </summary>
    int MaximumMultiRenderTargets { get; }

    /// <summary>
    /// Gets the maximum number of vertex buffers that can be set to the device at once.
    /// </summary>
    int MaximumVertexStreams { get; }

    /// <summary>
    /// Gets the maximum number of stream output targets that can be set to the device at once.
    /// </summary>
    int MaximumStreamOutputTargets { get; }

    /// <summary>
    /// Gets the number of bytes of system memory not shared with the CPU.
    /// </summary>
    long DedicatedSystemMemory { get; }

    /// <summary>
    /// Gets the number of bytes of video memory not shared with the CPU.
    /// </summary>
    long DedicatedVideoMemory { get; }

    /// <summary>
    /// Gets the number of bytes of system memory shared with the CPU.
    /// </summary>
    long SharedSystemMemory { get; }

    /// <summary>
    /// Checks if the specified surface format is valid for texture resources.
    /// </summary>
    /// <param name="surfaceFormat">Surface format</param>
    /// <param name="texType">Type of texture</param>
    /// <returns>True if valid, false otherwise</returns>
    bool CheckTextureFormat(SurfaceFormat surfaceFormat, TextureDimension texType);

    /// <summary>
    /// Checks if the specified formats and sample counts are valid for a render target.
    /// </summary>
    /// <param name="format">Surface format</param>
    /// <param name="depthFormat">Depth format</param>
    /// <param name="multiSampleCount">Sample count</param>
    /// <returns>True if a valid combination, false otherwise.</returns>
    bool CheckRenderTargetFormat(SurfaceFormat format, DepthFormat depthFormat, int multiSampleCount);

    /// <summary>
    /// Checks if the specified formats and sample counts are valid for a back buffer.
    /// </summary>
    /// <param name="format">Surface format</param>
    /// <param name="depthFormat">Depth format</param>
    /// <param name="multiSampleCount">Sample count</param>
    /// <returns>True if a valid combination, false otherwise.</returns>
    bool CheckBackBufferFormat(SurfaceFormat format, DepthFormat depthFormat, int multiSampleCount);

    /// <summary>
    /// Checks if the render target format can be resolved if multisampled.
    /// </summary>
    /// <param name="format">Format to test</param>
    /// <returns>True if it is resolvable, false otherwise.</returns>
    bool IsMultisampleResolvable(SurfaceFormat format);

    /// <summary>
    /// Checks if the shader stage is supported or not.
    /// </summary>
    /// <param name="shaderStage">Shader stage</param>
    /// <returns>True if the shader stage is supported, false otherwise.</returns>
    bool IsShaderStageSupported(ShaderStage shaderStage);

    /// <summary>
    /// Checks for the number of multisample quality levels are supported for the sample count. A value of zero
    /// means the format/multisample count combination is not valid. A non-zero value determines the number of quality levels that can be
    /// set, so a value of one means quality level zero, a value of two means quality levels zero and one, etc. The meanings of each quality level is 
    /// vendor-specific.
    /// </summary>
    /// <param name="format">Specified format</param>
    /// <param name="multiSamplecount">Sample count</param>
    /// <returns>Number of supported quality levels</returns>
    int CheckMultisampleQualityLevels(SurfaceFormat format, int multiSamplecount);
  }

  /// <summary>
  /// Defines a render context which is responsible for generating draw commands for the GPU. A render context contains all the functionality to configure
  /// the programmable pipeline, by setting resources, render states, and targets. An implementation may also support optional extensions for platform-specific
  /// drawing functionality.
  /// </summary>
  public interface IRenderContext : IDisposable
  {
    /// <summary>
    /// Event for when the render context is in the process of being disposed.
    /// </summary>
    event TypedEventHandler<IRenderContext, EventArgs> Disposing;

    /// <summary>
    /// Gets if the context has been disposed or not.
    /// </summary>
    bool IsDisposed { get; }

    /// <summary>
    /// Gets if the render context is immediate. If false, then it is deferred.
    /// </summary>
    bool IsImmediateContext { get; }

    /// <summary>
    /// Gets the render system that this context belongs to.
    /// </summary>
    IRenderSystem RenderSystem { get; }

    /// <summary>
    /// Gets or sets the blend state. By default, this is <see cref="Tesla.Graphics.BlendState.Opaque"/>.
    /// </summary>
    [AllowNull]
    BlendState BlendState { get; set; }

    /// <summary>
    /// Gets or sets the rasterizer state. By default, this is <see cref="Tesla.Graphics.RasterizerState.CullBackClockwiseFront"/>.
    /// </summary>
    [AllowNull]
    RasterizerState RasterizerState { get; set; }

    /// <summary>
    /// Gets or sets the depth stencil state. By default, this is <see cref="Tesla.Graphics.DepthStencilState.Default"/>.
    /// </summary>
    [AllowNull]
    DepthStencilState DepthStencilState { get; set; }

    /// <summary>
    /// Gets or sets the currently enforced render state. If a state is enforced, then the currently active one is preserved and subsequent state setting
    /// is filtered.
    /// </summary>
    EnforcedRenderState EnforcedRenderState { get; set; }

    /// <summary>
    /// Gets or sets the rectangle used for scissor testing, if it is enabled.
    /// </summary>
    Rectangle ScissorRectangle { get; set; }

    /// <summary>
    /// Gets or sets the blend factor which is a constant color used for alpha blending. By default, this value is <see cref="Color.White"/>. This is
    /// a "high frequency" render state and setting a blend state to the context will also set this value.
    /// </summary>
    Color BlendFactor { get; set; }

    /// <summary>
    /// Gets or sets the bitmask which defines which samples can be written during multisampling. By default, this value is -1 (0xffffffff). This is
    /// a "high frequency" render state and setting a blend state to the context will also set this value.
    /// </summary>
    int BlendSampleMask { get; set; }

    /// <summary>
    /// Gets or sets the reference value for stencil testing. By default, this value is 0. This is a "high frequency" render state and setting a 
    /// depth stencil state to the context will also set this value.
    /// </summary>
    int ReferenceStencil { get; set; }

    /// <summary>
    /// Gets or sets the currently active camera. The camera controls the viewport which identifies the portion of the 
    /// currently bound render target which is being rendered to.
    /// </summary>
    Camera Camera { get; set; }

    /// <summary>
    /// Gets or sets the currently active backbuffer (swapchain). If render targets are set to null, the backbuffer is set as the current render target.
    /// </summary>
    SwapChain BackBuffer { get; set; }

    /// <summary>
    /// Gets the shader stage corresponding to the enumeration type. If not supported, this will return null. A shader stage manages
    /// resources that can be bound to that particular stage in the pipeline.
    /// </summary>
    /// <param name="shaderStage">Shader stage type</param>
    /// <returns>The shader stage, if it exists. Otherwise null.</returns>
    IShaderStage GetShaderStage(ShaderStage shaderStage);

    /// <summary>
    /// Gets all supported shader stages. A shader stage manages
    /// resources that can be bound to that particular stage in the pipeline.
    /// </summary>
    /// <returns>All supported shader stages.</returns>
    IEnumerable<IShaderStage> GetShaderStages();

    /// <summary>
    /// Queries if the specified shader stage type is supported or not. A shader stage manages
    /// resources that can be bound to that particular stage in the pipeline.
    /// </summary>
    /// <param name="shaderStage">Shader stage type</param>
    /// <returns>True if the shader stage is supported, false otherwise.</returns>
    bool IsShaderStageSupported(ShaderStage shaderStage);

    /// <summary>
    /// Gets a render extension of the specified type. Render extensions extend the functionality of a render context with platform-specific
    /// functionality which are not supported by the engine core.
    /// </summary>
    /// <typeparam name="T">Type of render extension.</typeparam>
    /// <returns>The render extension, if registered, otherwise null.</returns>
    T GetExtension<T>() where T : IRenderContextExtension;

    /// <summary>
    /// Gets all supported render extensions. Render extensions extend the functionality of a render context with platform-specific
    /// functionality which are not supported by the engine core.
    /// </summary>
    /// <returns>All supported render extensions.</returns>
    IEnumerable<IRenderContextExtension> GetExtensions();

    /// <summary>
    /// Queries if render extension of the specified type is supported or not. Render extensions extend the functionality of a render context with 
    /// platform-specific functionality which are not supported by the engine core.
    /// </summary>
    /// <typeparam name="T">Type of render extension.</typeparam>
    /// <returns>True if the extension is supported, false otherwise.</returns>
    bool IsExtensionSupported<T>() where T : IRenderContextExtension;

    /// <summary>
    /// Executes the command list by playbacking the recorded GPU commands contained in the list.
    /// </summary>
    /// <param name="commandList">Command list to execute</param>
    /// <param name="restoreImmediateContextState">True if the render context state should be preserved or not. If true, the context state is saved and then restored
    /// after playback. Typically this is set to false to prevent unnecessary state setting. If false, the context state returns to the defautl state (e.g. as if ClearState was called).</param>
    void ExecuteCommandList(ICommandList commandList, bool restoreImmediateContextState);

    /// <summary>
    /// Binds the index buffer to the render context. A value of null will unbind the currently bound index buffer.
    /// </summary>
    /// <param name="indexBuffer">Index buffer to bind.</param>
    void SetIndexBuffer(IndexBuffer indexBuffer);

    /// <summary>
    /// Binds the specified vertex buffer to the first slot and the remaining slots are set to null. A value of null will unbind all currently bound buffers.
    /// </summary>
    /// <param name="vertexBuffer">Vertex buffer to bind.</param>
    void SetVertexBuffer(VertexBufferBinding vertexBuffer);

    /// <summary>
    /// Binds the specified number of vertex buffers, starting at the first slot. Any remaining slots are set to null. A value of null
    /// will unbind all currently bound buffers.
    /// </summary>
    /// <param name="vertexBuffers">Vertexbuffers to bind.</param>
    void SetVertexBuffers(params VertexBufferBinding[] vertexBuffers);

    /// <summary>
    /// Binds the specified stream output buffer to the first slot and the remaining slots are set to null. A stream output buffer cannot be bound 
    /// as both input and output at the same time. A value of null will unbind all currently bound buffers.
    /// </summary>
    /// <param name="streamOutputBuffer">Stream output buffer to bind.</param>
    void SetStreamOutputTarget(StreamOutputBufferBinding streamOutputBuffer);

    /// <summary>
    /// Binds the specified number of stream output buffers, starting at the first slot. Any remaining slots are set to null. A stream output buffer cannot
    /// be bound as both input and output at the same time. A value of null will unbind all currently bound buffers.
    /// </summary>
    /// <param name="streamOutputBuffers">Stream output buffers to bind.</param>
    void SetStreamOutputTargets(params StreamOutputBufferBinding[] streamOutputBuffers);

    /// <summary>
    /// Binds the specified render target to the first slot and the remaining slots are set to null. A value of null will unbind all currently bound
    /// render targets.
    /// </summary>
    /// <param name="options">Options when setting the render target.</param>
    /// <param name="renderTarget">Render target to bind.</param>
    void SetRenderTarget(SetTargetOptions options, IRenderTarget renderTarget);

    /// <summary>
    /// Binds the specified number of render targets, starting at the first slot. Any remaining slots are set to null. A render target cannot be bound
    /// as both input and output at the same time. A value of null will unbind all currently
    /// bound render targets.
    /// </summary>
    /// <param name="options">Options when setting the render targets.</param>
    /// <param name="renderTargets">Render targets to bind.</param>
    void SetRenderTargets(SetTargetOptions options, params IRenderTarget[] renderTargets);

    /// <summary>
    /// Gets the currently bound index buffer, or null if one is not bound
    /// </summary>
    /// <returns>Currently bound index buffer.</returns>
    IndexBuffer GetIndexBuffer();

    /// <summary>
    /// Gets the currently bound vertex buffers.
    /// </summary>
    /// <returns>Currently bound vertex buffers.</returns>
    VertexBufferBinding[] GetVertexBuffers();

    /// <summary>
    /// Gets the currently bound stream output buffers.
    /// </summary>
    /// <returns>Currently bound stream output buffers.</returns>
    StreamOutputBufferBinding[] GetStreamOutputTargets();

    /// <summary>
    /// Gets the currently bound render targets.
    /// </summary>
    /// <returns>Currently bound render targets.</returns>
    IRenderTarget[] GetRenderTargets();

    /// <summary>
    /// Clears the state of the context to the default. This includes all render states, bound resources and targets, and other properties.
    /// </summary>
    void ClearState();

    /// <summary>
    /// Clears all bounded render targets to the specified color.
    /// </summary>
    /// <param name="color">Color to clear to.</param>
    void Clear(Color color);

    /// <summary>
    /// Clears all bounded render targets and depth buffer.
    /// </summary>
    /// <param name="options">Clear options specifying which buffer to clear.</param>
    /// <param name="color">Color to clear to</param>
    /// <param name="depth">Depth value to clear to</param>
    /// <param name="stencil">Stencil value to clear to</param>
    void Clear(ClearOptions options, Color color, float depth, int stencil);

    /// <summary>
    /// Draws non-indexed, non-instanced geometry.
    /// </summary>
    /// <param name="primitiveType">Type of primitives to draw.</param>
    /// <param name="vertexCount">Number of vertices to draw.</param>
    /// <param name="startVertexIndex">Starting index in a vertex buffer at which to read vertices from.</param>
    void Draw(PrimitiveType primitiveType, int vertexCount, int startVertexIndex);

    /// <summary>
    /// Draws indexed, non-instanced geometry.
    /// </summary>
    /// <param name="primitiveType">Type of primitives to draw.</param>
    /// <param name="indexCount">Number of indices to draw.</param>
    /// <param name="startIndex">Starting index in the index buffer at which to read vertex indices from.</param>
    /// <param name="baseVertexOffset">Offset to add to each index before reading a vertex from a vertex buffer.</param>
    void DrawIndexed(PrimitiveType primitiveType, int indexCount, int startIndex, int baseVertexOffset);

    /// <summary>
    /// Draws indexed, instanced geometry.
    /// </summary>
    /// <param name="primitiveType">Type of primitives to draw.</param>
    /// <param name="indexCountPerInstance">Number of indices per instance to draw.</param>
    /// <param name="instanceCount">Number of instances to draw.</param>
    /// <param name="startIndex">Starting index in the index buffer at which to read vertex indices from.</param>
    /// <param name="baseVertexOffset">Offset to add to each index before reading a vertex from a vertex buffer.</param>
    /// <param name="startInstanceOffset">Offset to add to each index before reading per-instance data from a vertex buffer.</param>
    void DrawIndexedInstanced(PrimitiveType primitiveType, int indexCountPerInstance, int instanceCount, int startIndex, int baseVertexOffset, int startInstanceOffset);

    /// <summary>
    /// Draws non-indexed, instanced geometry.
    /// </summary>
    /// <param name="primitiveType">Type of primitives to draw.</param>
    /// <param name="vertexCountPerInstance">Number of vertices per instance to draw.</param>
    /// <param name="instanceCount">Number of instances to draw.</param>
    /// <param name="startVertexIndex">Starting index in a vertex buffer at which to read vertices from.</param>
    /// <param name="startInstanceOffset">Offset to add to each index before reading per-instance data from a vertex buffer.</param>
    void DrawInstanced(PrimitiveType primitiveType, int vertexCountPerInstance, int instanceCount, int startVertexIndex, int startInstanceOffset);

    /// <summary>
    /// Draws geometry of an unknown size that has been streamed out.
    /// </summary>
    /// <param name="primitiveType">Type of primitives to draw.</param>
    void DrawAuto(PrimitiveType primitiveType);

    /// <summary>
    /// Sends queued up commands in the command buffer to the GPU.
    /// </summary>
    void Flush();
  }

  /// <summary>
  /// Defines a deferred render context which allows for GPU commands to be recorded on separate threads, which then are played back on the main render thread. This is
  /// used for multi-threaded rendering and has some limitations. Queries cannot be started and only updating of dynamic resources (initially with the discard write flag, then subsequently with
  /// no-overwrite) are allowed in a deferred context. Each deferred context instance is intended for usage on a single thread; contexts themselves are not threadsafe. The GPU commands do not actually get invoked until executed 
  /// by an immediate render context, and the resulting command list can be re-used as many times as desired.
  /// </summary>
  public interface IDeferredRenderContext : IRenderContext
  {
    /// <summary>
    /// Records the GPU commands that have been submitted to the deferred render context into a command list for playback by the immediate render context. The command
    /// list represents all the GPU commands submitted up to this point from the last time this method was called.
    /// </summary>
    /// <param name="restoreDeferredContextState">True if the context state should be preserved or not. If true, the state is saved and then restored afterwards. Typically this is set to false to
    /// prevent unnecessary state setting. This only affects the next command list, not the one produced by this call. If false, the context state returns to the default state (e.g. as if ClearState had been called).</param>
    /// <returns>The command list</returns>
    ICommandList FinishCommandList(bool restoreDeferredContextState);
  }

  /// <summary>
  /// Defines an extended functionality for a render context.
  /// </summary>
  public interface IRenderContextExtension
  {
    /// <summary>
    /// Gets the render context the extension is a part of.
    /// </summary>
    IRenderContext RenderContext { get; }
  }

  /// <summary>
  /// Defines an extension functionality for a render context to generate texture mipmaps.
  /// </summary>
  public interface IGenerateMipMapsExtension : IRenderContextExtension
  {
    /// <summary>
    /// Generate mipmaps for the texture.
    /// </summary>
    /// <param name="texture">Texture to generate mipmaps for.</param>
    /// <returns>True if mipmaps were generated, false otherwise.</returns>
    bool GenerateMipMaps(Texture texture);
  }

  /// <summary>
  /// Defines a command list, which is a list of GPU commands that can be played back by an immediate render context.
  /// </summary>
  public interface ICommandList : INamable, IDisposable
  {
    /// <summary>
    /// Gets if the command list has been disposed or not.
    /// </summary>
    bool IsDisposed { get; }
  }

  /// <summary>
  /// Defines a stage in the programmable graphics pipeline.
  /// </summary>
  public interface IShaderStage
  {
    /// <summary>
    /// Gets the maximum number of sampler state slots that are supported.
    /// </summary>
    int MaximumSamplerSlots { get; }

    /// <summary>
    /// Gets the maximum number of shader resource slots that are supported.
    /// </summary>
    int MaximumResourceSlots { get; }

    /// <summary>
    /// Gets the type of shader stage.
    /// </summary>
    ShaderStage StageType { get; }

    /// <summary>
    /// Sets a sampler state to the first slot. 
    /// </summary>
    /// <param name="sampler">Sampler state set. Null represents the default state.</param>
    void SetSampler(SamplerState sampler);

    /// <summary>
    /// Sets a sampler state at the specified slot index.
    /// </summary>
    /// <param name="slotIndex">Zero-based slot index, range is [0, MaxSamplerSlots - 1].</param>
    /// <param name="sampler">Sampler state to set. Null represents the default state.</param>
    void SetSampler(int slotIndex, SamplerState sampler);

    /// <summary>
    /// Sets an array of sampler states, starting at the first slot index.
    /// </summary>
    /// <param name="samplers">Array of sampler states. Null values represent the default state.</param>
    void SetSamplers(params SamplerState[] samplers);

    /// <summary>
    /// Sets an array of sampler states, starting at the specified slot index.
    /// </summary>
    /// <param name="startSlotIndex">Zero-based starting slot index, range is [0, MaxSamplerSlots - 1].</param>
    /// <param name="samplers">Array of sampler states to set. Null values represent the default state.</param>
    void SetSamplers(int startSlotIndex, params SamplerState[] samplers);

    /// <summary>
    /// Sets a shader resource to the first slot.
    /// </summary>
    /// <param name="resource">Shader resource to set. Null represents the default state.</param>
    void SetShaderResource(IShaderResource resource);

    /// <summary>
    /// Sets a shader resource at the specified slot index.
    /// </summary>
    /// <param name="slotIndex">Zero-based slot index, range is [0, MaxResourceSlots - 1].</param>
    /// <param name="resource">Shader resource to set. Null represents the default state.</param>
    void SetShaderResource(int slotIndex, IShaderResource resource);

    /// <summary>
    /// Sets an array of shader resources, starting at the first slot index.
    /// </summary>
    /// <param name="resources">Array of shader resources to set. Null values represent the default state.</param>
    void SetShaderResources(params IShaderResource[] resources);

    /// <summary>
    /// Sets an array of shader resources, starting at the specified slot index.
    /// </summary>
    /// <param name="startSlotIndex">Zero-based starting slot index, range is [0, MaxResourceSlots - 1].</param>
    /// <param name="resources">Array of shader resources to set. Null values represent the default state.</param>
    void SetShaderResources(int startSlotIndex, params IShaderResource[] resources);

    /// <summary>
    /// Gets all bound sampler states.
    /// </summary>
    /// <param name="startSlotIndex">Zero-based starting slot index, range is [0, MaxSamplerSlots - 1].</param>
    /// <param name="count">Number of resources to retrieve.</param>
    /// <returns>Array of sampler states that are currently bound to the stage.</returns>
    SamplerState[] GetSamplers(int startSlotIndex, int count);

    /// <summary>
    /// Gets all bound shader resources.
    /// </summary>
    /// <param name="startSlotIndex">Zero-based starting slot index, range is [0, MaxResourceSlots - 1].</param>
    /// <param name="count">Number of resources to retrieve.</param>
    /// <returns>Array of shader resources that are currently bound to the stage.</returns>
    IShaderResource[] GetShaderResources(int startSlotIndex, int count);
  }

  #endregion

  #region Resources interfaces

  /// <summary>
  /// Defines a resource that can be bound to a shader.
  /// </summary>
  public interface IShaderResource : INamable, IDebugNamable, IDisposable
  {
    /// <summary>
    /// Gets if the resource has been disposed or not.
    /// </summary>
    bool IsDisposed { get; }

    /// <summary>
    /// Gets the shader resource type.
    /// </summary>
    ShaderResourceType ResourceType { get; }
  }

  /// <summary>
  /// Defines a common interface for 1D non-texture resources such as vertex, index, compute/shader buffers.
  /// </summary>
  public interface IBufferResource : IShaderResource
  {
    /// <summary>
    /// Gets the number of elements in the buffer.
    /// </summary>
    int ElementCount { get; }

    /// <summary>
    /// Gets size in bytes of a single element in the buffer.
    /// </summary>
    int ElementStride { get; }

    /// <summary>
    /// Gets the total size in bytes of the buffer.
    /// </summary>
    int SizeInBytes { get; }

    /// <summary>
    /// Gets the resource usage of the buffer.
    /// </summary>
    ResourceUsage ResourceUsage { get; }

    /// <summary>
    /// Gets the resource binding of the buffer.
    /// </summary>
    ResourceBindFlags ResourceBindFlags { get; }

    /// <summary>
    /// Reads data from the graphics buffer into the data buffer
    /// </summary>
    /// <typeparam name="T">Type of data to read from the graphics buffer.</typeparam>
    /// <param name="data">Buffer to hold data copied from the graphics buffer.</param>
    /// <param name="offsetInBytes">Optional offset from the start of the graphics buffer at which to start copying from.</param>
    void GetData<T>(Span<T> data, int offsetInBytes = 0) where T : unmanaged;

    /// <summary>
    /// Writes data from the data buffer into the graphics buffer.
    /// </summary>
    /// <typeparam name="T">Type of data to write to the graphics buffer.</typeparam>
    /// <param name="renderContext">The current render context.</param>
    /// <param name="data">Buffer with the data to be copied to the graphics buffer.</param>
    /// <param name="offsetInBytes">Optional offset from the start of the graphics buffer at which to start writing at.</param>
    /// <param name="writeOptions">Optional write options, used only if this is a dynamic buffer. None, discard, no overwrite.</param>
    void SetData<T>(IRenderContext renderContext, ReadOnlySpan<T> data, int offsetInBytes = 0, DataWriteOptions writeOptions = DataWriteOptions.Discard) where T : unmanaged;
  }

  /// <summary>
  /// Defines a resource that serves as a render target.
  /// </summary>
  public interface IRenderTarget : IShaderResource
  {
    /// <summary>
    /// Gets the depth stencil buffer associated with the render target, if no buffer is associated then this will be null.
    /// </summary>
    IDepthStencilBuffer DepthStencilBuffer { get; }

    /// <summary>
    /// Gets the depth stencil format of the associated depth buffer, if any. If this does not exist, then the format is None.
    /// </summary>
    DepthFormat DepthStencilFormat { get; }

    /// <summary>
    /// Gets the surface format of the target.
    /// </summary>
    SurfaceFormat Format { get; }

    /// <summary>
    /// Gets the width of the target resource, in pixels.
    /// </summary>
    int Width { get; }

    /// <summary>
    /// Gets the height of the target resource, in pixels.
    /// </summary>
    int Height { get; }

    /// <summary>
    /// Gets the depth of the target resource, in pixels.
    /// </summary>
    int Depth { get; }

    /// <summary>
    /// Gets the number of mip maips for each array slice in the resource. MSAA targets that do not resolve to a non-MSAA resource will only ever have one mip map per array slice.
    /// </summary>
    int MipCount { get; }

    /// <summary>
    /// Gets the number of array slices in the resource. Slices may be indexed in the range [0, ArrayCount). Even if the resource is not an array resource, this may be greater than one (e.g. cube textures
    /// are a special 2D array resource with 6 slices).
    /// </summary>
    int ArrayCount { get; }

    /// <summary>
    /// Gets if the resource is an array resource.
    /// </summary>
    bool IsArrayResource { get; }

    /// <summary>
    /// Gets if the resource is a cube resource, a special type of array resource. A cube resource has six faces. If the geometry shader stage is not supported by 
    /// the render system, then all six cube faces cannot be bound as a single binding. Instead the main cube target graphics resource is treated as the very first
    /// cube face, subsequent faces must be bound individually.
    /// </summary>
    bool IsCubeResource { get; }

    /// <summary>
    /// Gets if the resource is a sub-resource, representing an individual array slice if the main resource is an array resource or an
    /// individual face if its a cube resource. If this is a sub resource, then its sub resource index indicates the position in the array/cube.
    /// </summary>
    bool IsSubResource { get; }

    /// <summary>
    /// Gets the array index if the resource is a sub resource in an array. If not, then the index is -1.
    /// </summary>
    int SubResourceIndex { get; }

    /// <summary>
    /// Gets the multisample settings for the resource. The MSAA count, quality, and if the resource should be resolved to
    /// a non-MSAA resource for shader input. MSAA targets that do not resolve to a non-MSAA resource will only ever have one mip map per array slice.
    /// </summary>
    MSAADescription MultisampleDescription { get; }

    /// <summary>
    /// Gets the target usage, specifying how the target should be handled when it is bound to the pipeline. Generally this is
    /// set to discard by default.
    /// </summary>
    RenderTargetUsage TargetUsage { get; }

    /// <summary>
    /// Gets a sub render target at the specified array index. For non-array resources, this will not be valid, unless if the resource is cube.
    /// </summary>
    /// <param name="arrayIndex">Zero-based index of the sub render target.</param>
    /// <returns>The sub render target.</returns>
    IRenderTarget GetSubRenderTarget(int arrayIndex);
  }

  /// <summary>
  /// Defines a resource that serves as a depth stencil buffer. Depth buffers are generally created alongside a render target.
  /// </summary>
  public interface IDepthStencilBuffer : IShaderResource
  {
    /// <summary>
    /// Gets if the depth stencil buffer is readable, meaning it can be used as input for a shader.
    /// </summary>
    bool IsReadable { get; }

    /// <summary>
    /// Gets if the depth stencil buffer can be shared between multiple render targets. if this is true, then render targets can be initialized using this
    /// depth stencil buffer, otherwise an exception will be thrown.
    /// </summary>
    bool IsShareable { get; }

    /// <summary>
    /// Gets the depth stencil format of the buffer.
    /// </summary>
    DepthFormat DepthStencilFormat { get; }

    /// <summary>
    /// Gets the width of the buffer.
    /// </summary>
    int Width { get; }

    /// <summary>
    /// Gets the height of the buffer.
    /// </summary>
    int Height { get; }

    /// <summary>
    /// Gets the number of array slices in the resource. Even if the resource is not an array resource, this may be greater than one (e.g. cube textures
    /// are a special 2D array resource with 6 slices).
    /// </summary>
    int ArrayCount { get; }

    /// <summary>
    /// Gets if the resource is an array resource.
    /// </summary>
    bool IsArrayResource { get; }

    /// <summary>
    /// Gets if the resource is a cube resource, a special type of array resource. A cube resource has six faces. If the geometry shader stage is not supported by 
    /// the render system, then all six cube faces cannot be bound as a single binding. Instead the main cube depth buffer resource is treated as the very first
    /// cube face, subsequent faces must be bound individually.
    /// </summary>
    bool IsCubeResource { get; }

    /// <summary>
    /// Gets if the resource is a sub-resource, representing an individual array slice if the main resource is an array resource or an
    /// individual face if its a cube resource. If this is a sub resource, then its sub resource index indicates the position in the array/cube.
    /// </summary>
    bool IsSubResource { get; }

    /// <summary>
    /// Gets the array index if the resource is a sub resource in an array. If not, then the index is -1.
    /// </summary>
    int SubResourceIndex { get; }

    /// <summary>
    /// Gets the multisample settings for the resource. The MSAA count, quality, and if the resource should be resolved to
    /// a non-MSAA resource for shader input. MSAA targets that do not resolve to a non-MSAA resource will only ever have one mip map per array slice.
    /// </summary>
    MSAADescription MultisampleDescription { get; }

    /// <summary>
    /// Gets the target usage, specifying how the target should be handled when it is bound to the pipeline. Generally this is
    /// set to discard by default.
    /// </summary>
    RenderTargetUsage TargetUsage { get; }
  }

  /// <summary>
  /// Defines a vertex that has a layout.
  /// </summary>
  public interface IVertexType
  {
    /// <summary>
    /// Gets the layout of the vertex.
    /// </summary>
    /// <returns>Vertex layout</returns>
    VertexLayout GetVertexLayout();
  }

  #endregion

  #region Material interfaces

  /// <summary>
  /// Defines logic that provides computed parameters to effect parameter instances.
  /// </summary>
  public interface IComputedParameterProvider
  {
    /// <summary>
    /// Gets the name of the computed parameter. This is used to bind effect parameter instances
    /// to this provider in material scripts.
    /// </summary>
    String ComputedParameterName { get; }

    /// <summary>
    /// Validates the specified effect parameter against the provider logic. If the effect parameter is not of the
    /// same format or type as the provider expects then it is not valid.
    /// </summary>
    /// <param name="parameter">Effect parameter instance to validate.</param>
    /// <returns>True if the effect parameter is valid, false otherwise.</returns>
    bool ValidateParameter(IEffectParameter parameter);

    /// <summary>
    /// Updates the specified effect parameter with the computed value from the provider.
    /// </summary>
    /// <param name="context">Render context.</param>
    /// <param name="properties">Render properties.</param>
    /// <param name="material">Calling material.</param>
    /// <param name="parameter">Effect parameter instance.</param>
    /// <param name="localState">Optional local state object, may be null and may be populated during the update call.</param>
    void UpdateParameter(IRenderContext context, RenderPropertyCollection properties, Material material, IEffectParameter parameter, ref Object localState);
  }

  #endregion

  #region Standard Library interfaces

  /// <summary>
  /// Defines a provider that can be registered to <see cref="StandardMaterialScriptLibrary"/> to supply a "standard library" of material scripts
  /// when materials are parsed and loaded.
  /// </summary>
  public interface IMaterialScriptProvider
  {
    /// <summary>
    /// Gets the folder path where the material scripts of this provider reside in. The Engine's default materials are in the topmost folder (e.g.
    /// <see cref="String.Empty"/> would be the value).
    /// </summary>
    String FolderPath { get; }

    /// <summary>
    /// Gets the string representation of the material script.
    /// </summary>
    /// <param name="name">Name of the material script to get. Should not include the .tem extension.</param>
    /// <returns>Returns the material script string representation, or <see cref="String.Empty"/> if it could not be found.</returns>
    String GetMaterialScript(String name);

    /// <summary>
    /// Gets all of the material scripts from the provider.
    /// </summary>
    /// <returns>List of pairs where the first property is the script name, and the second property is the script contents.</returns>
    List<Pair<String, String>> GetMaterialScripts();
  }

  public interface IEffectByteCodeProvider
  {
    /// <summary>
    /// Gets the folder path where the effect shader files of this provider reside in. The Engine's default effect shader files are in the topmost folder (e.g.
    /// <see cref="String.Empty"/> would be the value).
    /// </summary>
    String FolderPath { get; }

    /// <summary>
    /// Gets the effect byte code specified by the name.
    /// </summary>
    /// <param name="name">The name of the effect file to get byte code for. Should not include the .tefx extension.</param>
    /// <returns>Effect byte code, or null if the name does not correspond to an effect file.</returns>
    byte[] GetEffectByteCode(String name);
  }

  #endregion

  #region Render State Providers

  /// <summary>
  /// Provides a cache for maximizing reuse of render states.
  /// </summary>
  public interface IRenderStateCache
  {
    /// <summary>
    /// Gets or caches the given state. If a cached state exists, the incoming one is disposed of and the cached value is returned.
    /// The returned state is always bound.
    /// </summary>
    /// <typeparam name="T">Type of render state.</typeparam>
    /// <param name="state">Template render state. Does not have to be bound yet (preferable).</param>
    /// <returns>A cached state that is bound and no longer can be modified.</returns>
    T GetOrCache<T>(T state) where T : RenderState?;
  }

  /// <summary>
  /// A provider for creating and managing predefined blend states.
  /// </summary>
  public interface IPredefinedBlendStateProvider
  {
    /// <summary>
    /// Gets a predefined state object for opaque blending, where no blending occurs and destination color overwrites source color. This is 
    /// the default state.
    /// </summary>
    BlendState Opaque { get; }

    /// <summary>
    /// Gets a predefined state object for premultiplied alpha blending, where source and destination colors are blended by using
    /// alpha, and where the color contains alpha pre-multiplied.
    /// </summary>
    BlendState AlphaBlendPremultiplied { get; }

    /// <summary>
    /// Gets a predefined state object for additive blending, where source and destination color are blended without using alpha.
    /// </summary>
    BlendState AdditiveBlend { get; }

    /// <summary>
    ///Gets a predefined state object for non-premultiplied alpha blending, where the source and destination color are blended by using alpha,
    ///and where the color does not contain the alpha pre-multiplied.
    /// </summary>
    BlendState AlphaBlendNonPremultiplied { get; }

    /// <summary>
    /// Queries a predefined blend state by name.
    /// </summary>
    /// <param name="name">Name of the blend state.</param>
    /// <returns>Blend state, or null if it does not exist.</returns>
    BlendState? GetBlendStateByName(string name);
  }

  /// <summary>
  /// A provider for creaing and managing predefined depthstencil states.
  /// </summary>
  public interface IPredefinedDepthStencilStateProvider
  {
    /// <summary>
    /// Gets a flag denoting whether the default state objects use the "reverse depth" or "traditional" technique.
    /// <para>
    /// The "traditional" technique maps near to 0.0 and far to 1.0 and uses <see cref="ComparisonFunction.LessEqual"/>.
    /// </para>
    /// <para>
    /// The "reverse depth" technique maps near to 1.0 and far to 0.0 in order to have better use of the precision
    /// for large scenes, and uses <see cref="ComparisonFunction.GreaterEqual"/>. 32-bit precision is recommended
    /// and this requires everything related to the depth buffer to be reversed in thinking.
    /// </para>
    /// </summary>
    bool IsReverseDepth { get; }

    /// <summary>
    /// Gets a predefined state object where the depth buffer is disabled and writing to it is disabled.
    /// </summary>
    DepthStencilState None { get; }

    /// <summary>
    /// Gets a predefined state object where the depth buffer is enabled and writing to it is disabled. The depth
    /// function will vary based on <see cref="IsReverseDepth"/>.
    /// <para>
    /// The "traditional" technique maps near to 0.0 and far to 1.0 and uses <see cref="ComparisonFunction.LessEqual"/>.
    /// </para>
    /// <para>
    /// The "reverse depth" technique maps near to 1.0 and far to 0.0 in order to have better use of the precision
    /// for large scenes, and uses <see cref="ComparisonFunction.GreaterEqual"/>. 32-bit precision is recommended
    /// and this requires everything related to the depth buffer to be reversed in thinking.
    /// </para>
    /// </summary>
    DepthStencilState DepthWriteOff { get; }

    /// <summary>
    /// Gets a predefined state object where the depth buffer is enabled and writing to it is enabled. The depth
    /// function will vary based on <see cref="IsReverseDepth"/>.
    /// <para>
    /// The "traditional" technique maps near to 0.0 and far to 1.0 and uses <see cref="ComparisonFunction.LessEqual"/>.
    /// </para>
    /// <para>
    /// The "reverse depth" technique maps near to 1.0 and far to 0.0 in order to have better use of the precision
    /// for large scenes, and uses <see cref="ComparisonFunction.GreaterEqual"/>. 32-bit precision is recommended
    /// and this requires everything related to the depth buffer to be reversed in thinking.
    /// </para>
    /// </summary>
    DepthStencilState Default { get; }

    /// <summary>
    /// Queries a predefined depth stencil state by name.
    /// </summary>
    /// <param name="name">Name of the depth stencil state.</param>
    /// <returns>DepthStencil state, or null if it does not exist.</returns>
    DepthStencilState? GetDepthStencilStateByName(string name);
  }

  /// <summary>
  /// A provider for creating and managing predefined rasterizer states.
  /// </summary>
  public interface IPredefinedRasterizerStateProvider
  {
    /// <summary>
    /// Gets a predefined state object where culling is disabled.
    /// </summary>
    RasterizerState CullNone { get; }

    /// <summary>
    /// Gets a predefined state object where back faces are culled and front faces have
    /// a clockwise vertex winding. This is the default state.
    /// </summary>
    RasterizerState CullBackClockwiseFront { get; }

    /// <summary>
    /// Gets a predefined state object where back faces are culled and front faces have a counterclockwise
    /// vertex winding.
    /// </summary>
    RasterizerState CullBackCounterClockwiseFront { get; }

    /// <summary>
    /// Gets a predefined state object where culling is disabled and fillmode is wireframe.
    /// </summary>
    RasterizerState CullNoneWireframe { get; }

    /// <summary>
    /// Queries a predefined rasterizer state by name.
    /// </summary>
    /// <param name="name">Name of the rasterizer state.</param>
    /// <returns>Rasterizer state, or null if it does not exist.</returns>
    RasterizerState? GetRasterizerStateByName(string name);
  }

  /// <summary>
  /// A provider for creating and managing predefined sampler states.
  /// </summary>
  public interface IPredefinedSamplerStateProvider
  {
    /// <summary>
    /// Gets the predefined state object where point filtering is used and UVW coordinates wrap.
    /// </summary>
    SamplerState PointWrap { get; }

    /// <summary>
    /// Gets the predefined state object where point filtering is used and UVW coordinates are clamped in the range of [0, 1]. This
    /// is the default state.
    /// </summary>
    SamplerState PointClamp { get; }

    /// <summary>
    /// Gets the predefined state object where linear filtering is used and UVW coordinates wrap.
    /// </summary>
    SamplerState LinearWrap { get; }

    /// <summary>
    /// Gets the predefined state object where linear filtering is used and UVW coordinates are clamped in the range of [0, 1].
    /// </summary>
    SamplerState LinearClamp { get; }

    /// <summary>
    /// Gets the predefined state object where anisotropic filtering is used and UVW coordinates wrap.
    /// </summary>
    SamplerState AnisotropicWrap { get; }

    /// <summary>
    /// Gets the predefined state object where anisotropic filtering is used and UVW coordinates are
    /// clamped in the range of [0, 1].
    /// </summary>
    SamplerState AnisotropicClamp { get; }

    /// <summary>
    /// Queries a predefined sampler state by name.
    /// </summary>
    /// <param name="name">Name of the sampler state.</param>
    /// <returns>Sampler state, or null if it does not exist.</returns>
    SamplerState? GetSamplerStateByName(string name);
  }

  #endregion
}
