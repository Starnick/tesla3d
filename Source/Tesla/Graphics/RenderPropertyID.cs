﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Runtime.InteropServices;
using System.Threading;

namespace Tesla.Graphics
{
    /// <summary>
    /// Represents a unique value for identifying render properties to allow for fast lookup.
    /// </summary>
    [Serializable]
    [StructLayout(LayoutKind.Sequential, Size = 4)]
    public struct RenderPropertyID : IEquatable<RenderPropertyID>, IComparable<RenderPropertyID>
    {
        private static int s_globalCurrentId = -1;

        private int m_idValue;

        /// <summary>
        /// Gets the invalid render property ID value.
        /// </summary>
        public static RenderPropertyID Invalid
        {
            get
            {
                return new RenderPropertyID(-1);
            }
        }

        /// <summary>
        /// Gets the integer value of the ID.
        /// </summary>
        public int Value 
        { 
            get 
            {
                return m_idValue;
            }
        }

        /// <summary>
        /// Gets if the ID is valid (greater than or equal to zero).
        /// </summary>
        public bool IsValid
        {
            get
            {
                return m_idValue >= 0;
            }
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="RenderPropertyID"/> struct.
        /// </summary>
        /// <param name="idValue">The integer ID value.</param>
        public RenderPropertyID(int idValue)
        {
            if(idValue < 0)
                idValue = -1;

            m_idValue = idValue;
        }

        /// <summary>
        /// Generates a new unique ID. This is only unique for the current session and is thread safe.
        /// </summary>
        /// <returns>The new render property ID.</returns>
        public static RenderPropertyID GenerateNewUniqueID()
        {
            return new RenderPropertyID(Interlocked.Increment(ref s_globalCurrentId));
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
        public override int GetHashCode()
        {
            unchecked
            {
                return m_idValue;
            }
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object" />, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns><c>True</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.</returns>
        public override bool Equals(object obj)
        {
            if(obj is RenderPropertyID)
                return ((RenderPropertyID) obj).m_idValue == m_idValue;

            return false;
        }

        /// <summary>
        /// Compares the current object with another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>A value that indicates the relative order of the objects being compared. The return value has the following meanings: Value Meaning Less than zero This object is less than the <paramref name="other" /> parameter.Zero This object is equal to <paramref name="other" />. Greater than zero This object is greater than <paramref name="other" />.</returns>
        public int CompareTo(RenderPropertyID other)
        {
            if(m_idValue < other.m_idValue)
                return -1;

            if(m_idValue > other.m_idValue)
                return 1;

            return 0;
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>True if the current object is equal to the <paramref name="other" /> parameter; otherwise, false.</returns>
        public bool Equals(RenderPropertyID other)
        {
            return other.m_idValue == m_idValue;
        }

        /// <summary>
        /// Implicitly converts the render property ID to an integer value.
        /// </summary>
        /// <param name="id">Render property ID.</param>
        /// <returns>Integer value.</returns>
        public static implicit operator int(RenderPropertyID id)
        {
            return id.m_idValue;
        }

        /// <summary>
        /// Implicitly converts the integer value to a render property ID.
        /// </summary>
        /// <param name="idValue">Integer value.</param>
        /// <returns>Render property ID.</returns>
        public static implicit operator RenderPropertyID(int idValue)
        {
            return new RenderPropertyID(idValue);
        }

        /// <summary>
        /// Checks inequality between two render property IDs.
        /// </summary>
        /// <param name="a">First ID.</param>
        /// <param name="b">Second ID.</param>
        /// <returns>True if the two values are not the same, false otherwise.</returns>
        public static bool operator !=(RenderPropertyID a, RenderPropertyID b)
        {
            return a.m_idValue != b.m_idValue;
        }

        /// <summary>
        /// Checks equality between two render property IDs.
        /// </summary>
        /// <param name="a">First ID.</param>
        /// <param name="b">Second ID.</param>
        /// <returns>True if the two values are the same, false otherwise.</returns>
        public static bool operator ==(RenderPropertyID a, RenderPropertyID b)
        {
            return a.m_idValue == b.m_idValue;
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
        public override String ToString()
        {
            return m_idValue.ToString();
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <param name="formatProvider">The format provider.</param>
        /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
        public String ToString(IFormatProvider formatProvider)
        {
            if(formatProvider == null)
                return ToString();

            return m_idValue.ToString(formatProvider);
        }
    }
}
