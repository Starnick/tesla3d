﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesla.Graphics.Implementation;
using Tesla.Content;

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// Represents an effect that is used to render an object. This completely describes how to configure the graphics pipeline - what render states, resources,
  /// and shaders are bound to the GPU. An effect is necessary to render any object.
  /// </summary>
  [SavableVersion(1)]
  public class Effect : GraphicsResource, ISavable, IStandardLibraryContent
  {
    private string m_standardContentName;

    #region Public Properties

    /// <summary>
    /// Occurs when an effect shader group that is contained in this effect is about to be applied to a render context.
    /// </summary>
    public event OnApplyDelegate OnShaderGroupApply
    {
      add
      {
        EffectImpl.OnShaderGroupApply += value;
      }
      remove
      {
        EffectImpl.OnShaderGroupApply -= value;
      }
    }

    /// <summary>
    /// Gets the effect sort key, used to compare effects as a first step in sorting objects to render. The sort key is the same
    /// for cloned effects.
    /// </summary>
    public int SortKey
    {
      get
      {
        return EffectImpl.SortKey;
      }
    }

    /// <summary>
    /// Gets or sets the currently active shader group.
    /// </summary>
    public IEffectShaderGroup CurrentShaderGroup
    {
      get
      {
        return EffectImpl.CurrentShaderGroup;
      }
      set
      {
        if (value is null || !value.IsPartOf(this))
          return;

        EffectImpl.CurrentShaderGroup = value;
      }
    }

    /// <summary>
    /// Gets the shader groups contained in this effect.
    /// </summary>
    public EffectShaderGroupCollection ShaderGroups
    {
      get
      {
        return EffectImpl.ShaderGroups;
      }
    }

    /// <summary>
    /// Gets all effect parameters contained in this effect.
    /// </summary>
    public EffectParameterCollection Parameters
    {
      get
      {
        return EffectImpl.Parameters;
      }
    }

    /// <summary>
    /// Gets all constant buffers that contain all value type parameters used by all passes.
    /// </summary>
    public EffectConstantBufferCollection ConstantBuffers
    {
      get
      {
        return EffectImpl.ConstantBuffers;
      }
    }

    /// <summary>
    /// Gets the compiled byte code that represents this effect.
    /// </summary>
    public byte[] EffectByteCode
    {
      get
      {
        return EffectImpl.EffectByteCode;
      }
    }

    /// <summary>
    /// Gets the name of the content this instance represents. If <see cref="IsStandardContent" /> is false, then this returns an empty string.
    /// </summary>
    public string StandardContentName
    {
      get
      {
        return m_standardContentName;
      }
    }

    /// <summary>
    /// Gets if the instance represents standard library content or not.
    /// </summary>
    public bool IsStandardContent
    {
      get
      {
        return !String.IsNullOrEmpty(m_standardContentName);
      }
    }

    #endregion

    //For casting purposes
    private IEffectImpl EffectImpl
    {
      get
      {
        return GetImplAs<IEffectImpl>();
      }
      set
      {
        BindImplementation(value);
      }
    }

    /// <summary>
    /// For ISavable.
    /// </summary>
    protected Effect() 
    {
      m_standardContentName = String.Empty;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Effect"/> class.
    /// </summary>
    /// <param name="renderSystem">Render system used to create the underlying implementation.</param>
    /// <param name="effectByteCode">Compiled effect code that represents this effect.</param>
    /// <param name="standardContentName">Optional standard effect library name, leave blank if not a standard effect.</param>
    /// <exception cref="ArgumentNullException">Thrown if the render system or effect byte code are null.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if creating the underlying implementation fails or is unsupported, see inner exception for a more detailed error.</exception>
    public Effect(IRenderSystem renderSystem, byte[] effectByteCode, string? standardContentName = null)
    {
      m_standardContentName = (standardContentName is null) ? String.Empty : standardContentName;
      CreateImplementation(renderSystem, effectByteCode);
    }

    /// <summary>
    /// Clones the effect, and possibly sharing relevant underlying resources. Cloned instances are guaranteed to be
    /// completely separate from the source effect in terms of parameters, changing one will not change the other. But unlike
    /// creating a new effect from the same compiled byte code, native resources can still be shared more effectively.
    /// </summary>
    /// <returns>Cloned effect instance.</returns>
    public Effect Clone()
    {
      ObjectDisposedException.ThrowIf(IsDisposed, this);

      IEffectImpl impl = EffectImpl.Clone();
      Effect clonedEffect = new Effect();
      clonedEffect.EffectImpl = impl;
      clonedEffect.Name = Name;
      clonedEffect.m_standardContentName = m_standardContentName;

      return clonedEffect;
    }

    /// <summary>
    /// Sets the effect's current shader group by name.
    /// </summary>
    /// <param name="shaderGroupName">Name of the shader group to set to.</param>
    /// <returns>True if the shader group was found and set as the current active shader group, false otherwise.</returns>
    public bool SetCurrentShaderGroup(string shaderGroupName)
    {
      if (String.IsNullOrEmpty(shaderGroupName))
        return false;

      IEffectShaderGroup shaderGroup = ShaderGroups[shaderGroupName];
      if (shaderGroup is null)
        return false;

      EffectImpl.CurrentShaderGroup = shaderGroup;
      return true;
    }

    /// <summary>
    /// Gets a consistent hash code that identifies the content item. If it is not standard content, each instance should have a unique hash, if the instance is
    /// standard content, each instance should have the same hash code. This might differ from .NET's hash code and is only used to identify two instances that
    /// represent the same data (there may be situations where we want to differentiate the two instances, so we don't rely on the .NET's get hash code function).
    /// </summary>
    /// <returns>32-bit hash code.</returns>
    public int GetContentHashCode()
    {
      return SortKey;
    }


    #region ISavable Methods

    /// <summary>
    /// Reads the object data from the input.
    /// </summary>
    /// <param name="input">Savable reader</param>
    public void Read(ISavableReader input)
    {
      IRenderSystem renderSystem = GraphicsHelper.GetRenderSystem(input.ServiceProvider);

      Name = input.ReadString("Name");
      m_standardContentName = String.Empty;

      if (input.ReadBoolean("IsStandardContent"))
        m_standardContentName = input.ReadStringOrDefault("StandardContentName");

      byte[] effectByteCode = input.ReadByteArray("EffectByteCode");

      CreateImplementation(renderSystem, effectByteCode);
    }

    /// <summary>
    /// Writes the object data to the output.
    /// </summary>
    /// <param name="output">Savable writer</param>
    public void Write(ISavableWriter output)
    {
      output.Write("Name", Name);
      output.Write("IsStandardContent", IsStandardContent);

      if (IsStandardContent)
        output.Write("StandardContentName", m_standardContentName);

      output.Write("EffectByteCode", EffectImpl.EffectByteCode);
    }

    #endregion

    #region Implementation Creation

    private void CreateImplementation(IRenderSystem renderSystem, byte[] effectByteCode)
    {
      GraphicsHelper.ThrowIfRenderSystemNull(renderSystem);

      if (effectByteCode is null || effectByteCode.Length == 0)
        throw new ArgumentNullException(nameof(effectByteCode));

      IEffectImplFactory factory;
      if (!renderSystem.TryGetImplementationFactory<IEffectImplFactory>(out factory))
        throw GraphicsExceptionHelper.NewUnsupportedFeatureException(renderSystem, typeof(Effect));

      try
      {
        EffectImpl = factory.CreateImplementation(effectByteCode);
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw GraphicsExceptionHelper.NewErrorCreatingImplementation(typeof(Effect), e);
      }
    }

    #endregion
  }
}
