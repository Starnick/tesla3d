﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics;
using Tesla.Content;
using Tesla.Graphics.Implementation;

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// Represents an array of One-Dimensional texture resources, each with a width.
  /// </summary>
  [SavableVersion(1)]
  public class Texture1DArray : Texture1D
  {
    #region Public Properties

    /// <summary>
    /// Gets the number of array slices in the texture. Slices may be indexed in the range [0, ArrayCount).
    /// </summary>
    public int ArrayCount
    {
      get
      {
        return Texture1DArrayImpl.ArrayCount;
      }
    }

    /// <inheritdoc />
    public override ShaderResourceType ResourceType
    {
      get
      {
        return ShaderResourceType.Texture1DArray;
      }
    }

    #endregion

    //Private property to cast the implementation
    private ITexture1DArrayImpl Texture1DArrayImpl
    {
      get
      {
        return GetImplAs<ITexture1DArrayImpl>();
      }
      set
      {
        BindImplementation(value);
      }
    }

    /// <summary>
    /// For ISavable only.
    /// </summary>
    protected Texture1DArray() { }

    /// <summary>
    /// Constructs a new instance of the <see cref="Texture1DArray"/> class.
    /// </summary>
    /// <param name="renderSystem">Render system used to create the underlying implementation.</param>
    /// <param name="width">Width of the texture, in texels.</param>
    /// <param name="arrayCount">Number of array slices, must be greater than zero.</param>
    /// <param name="options">Common <see cref="TextureOptions"/> that may also contain initial data.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the render system is null or if any input is null when it is required (e.g. initial data is required for immutable resources).</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the width is less than or equal to zero or greater than the maximum texture size, or if the array count
    /// is less than or equal to zero or greater than the maximum array size.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if creating the underlying implementation fails or is unsupported, see inner exception for a more detailed error.</exception>
    public Texture1DArray(IRenderSystem renderSystem, int width, int arrayCount, TextureOptions options = default)
    {
      CreateImplementation(renderSystem, width, arrayCount, options);
    }

    #region Public Methods

    /// <summary>
    /// Gets a sub texture at the specified array index.
    /// </summary>
    /// <param name="arrayIndex">Zero-based index of the sub texture.</param>
    /// <returns>The sub texture.</returns>
    /// <exception cref="TeslaGraphicsException">Thrown if an error occurs while retrieving the sub texture.</exception>
    /// <remarks>See implementors for details on specific exceptions that can be thrown.</remarks>
    public IShaderResource? GetSubTexture(int arrayIndex)
    {
      try
      {
        return Texture1DArrayImpl.GetSubTexture(arrayIndex);
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw new TeslaGraphicsException(StringLocalizer.Instance.GetLocalizedString("TextureArrayErrorRetrievingSubTexture"), e);
      }
    }

    #endregion

    #region ISavable Methods

    /// <inheritdoc />
    public override void Read(ISavableReader input)
    {
      IRenderSystem renderSystem = GraphicsHelper.GetRenderSystem(input.ServiceProvider);

      Name = input.ReadString("Name");
      SurfaceFormat format = input.ReadEnum<SurfaceFormat>("Format");
      ResourceUsage usage = input.ReadEnum<ResourceUsage>("ResourceUsage");
      ResourceBindFlags bindFlags = input.ReadEnum<ResourceBindFlags>("ResourceBindFlags");
      int width = input.ReadInt32("Width");
      int expectedMipCount = input.ReadInt32("MipCount");
      Debug.Assert(expectedMipCount > 0);

      int arrayCount = input.BeginReadGroup("Slices");
      Debug.Assert(arrayCount > 0);

      // Pool buffers to initialize texture
      using (PooledArray<IReadOnlyDataBuffer?> data = new PooledArray<IReadOnlyDataBuffer?>(arrayCount * expectedMipCount) { DisposeContents = true })
      {
        for (int i = 0; i < arrayCount; i++)
        {
          int mipChainCount = input.BeginReadGroup("MipMapChain");
          Debug.Assert(expectedMipCount == mipChainCount);

          for (int j = 0; j < mipChainCount; j++)
            data[CalculateSubResourceIndex(i, j, expectedMipCount)] = input.ReadBlob<byte>("MipMap", MemoryAllocatorStrategy.DefaultPooled);

          input.EndReadGroup();
        }

        input.EndReadGroup();

        CreateImplementation(renderSystem, width, arrayCount, TextureOptions.Init(data, expectedMipCount > 1, format, usage, bindFlags));
      }
    }

    /// <inheritdoc />
    public override void Write(ISavableWriter output)
    {
      int arrayCount = ArrayCount;
      int mipCount = MipCount;
      int width = Width;
      SurfaceFormat format = Format;

      output.Write("Name", Name);
      output.WriteEnum<SurfaceFormat>("Format", format);
      output.WriteEnum<ResourceUsage>("ResourceUsage", ResourceUsage);
      output.WriteEnum<ResourceBindFlags>("ResourceBindFlags", ResourceBindFlags);
      output.Write("Width", width);
      output.Write("MipCount", mipCount);

      output.BeginWriteGroup("Slices", arrayCount);

      // Create a pooled buffer for the largest surface and reuse it to gather and write out each surface
      using (DataBuffer<byte> byteBuffer = DataBuffer.Create<byte>(SubResourceAt.Zero.CalculateMipLevelSizeInBytes(width, format), MemoryAllocatorStrategy.DefaultPooled))
      {
        for (int i = 0; i < arrayCount; i++)
        {
          output.BeginWriteGroup("MipMapChain", mipCount);

          for (int j = 0; j < mipCount; j++)
          {
            SubResourceAt subIndex = SubResourceAt.MipSlice(j, i);
            int byteCount = subIndex.CalculateMipLevelSizeInBytes(width, format);
            Span<byte> span = byteBuffer.Span.Slice(0, byteCount);
            GetData<byte>(span, subIndex);

            output.WriteBlob<byte>("MipMap", span);
          }

          output.EndWriteGroup();
        }
      }

      output.EndWriteGroup();
    }

    #endregion

    #region Implementation Creation

    private void CreateImplementation(IRenderSystem renderSystem, int width, int arrayCount, TextureOptions options)
    {
      GraphicsHelper.ThrowIfRenderSystemNull(renderSystem);

      int mipLevels = (options.WantMipMaps) ? CalculateMipMapCount(width) : 1;

      ValidateCreationParameters(renderSystem.Adapter, width, arrayCount, mipLevels, options);

      ITexture1DArrayImplFactory factory;
      if (!renderSystem.TryGetImplementationFactory<ITexture1DArrayImplFactory>(out factory))
        throw GraphicsExceptionHelper.NewUnsupportedFeatureException(renderSystem, typeof(Texture1DArray));

      try
      {
        Texture1DArrayImpl = factory.CreateImplementation(width, arrayCount, mipLevels, options);
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw GraphicsExceptionHelper.NewErrorCreatingImplementation(typeof(Texture1DArray), e);
      }
    }

    #endregion
  }
}
