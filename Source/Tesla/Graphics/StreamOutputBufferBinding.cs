﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// Describes how a stream output buffer is to be bound to the graphics pipeline.
  /// </summary>
  public struct StreamOutputBufferBinding : IEquatable<StreamOutputBufferBinding>
  {
    private StreamOutputBuffer m_buffer;
    private int m_vertexOffset;

    /// <summary>
    /// Gets the buffer to be bound.
    /// </summary>
    public readonly StreamOutputBuffer StreamOutputBuffer
    {
      get
      {
        return m_buffer;
      }
    }

    /// <summary>
    /// Gets the vertex offset, indicating the first vertex to be used in the buffer (from the start of the buffer). This value is in whole indices, not bytes.
    /// </summary>
    public readonly int VertexOffset
    {
      get
      {
        return m_vertexOffset;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="StreamOutputBufferBinding"/> struct.
    /// </summary>
    /// <param name="buffer">The stream output buffer to bind.</param>
    public StreamOutputBufferBinding(StreamOutputBuffer buffer)
    {
      m_buffer = buffer;
      m_vertexOffset = 0;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="StreamOutputBufferBinding"/> struct.
    /// </summary>
    /// <param name="buffer">The stream output buffer to bind.</param>
    /// <param name="vertexOffset">The vertex offset from the start of the buffer to the first vertex that will be used.</param>
    public StreamOutputBufferBinding(StreamOutputBuffer buffer, int vertexOffset)
    {
      m_buffer = buffer;
      m_vertexOffset = vertexOffset;
    }

    /// <summary>
    /// Implicitly converts a stream output buffer to the binding with offset of zero.
    /// </summary>
    /// <param name="buffer">The stream output buffer to bind.</param>
    /// <returns>The stream output buffer binding.</returns>
    public static implicit operator StreamOutputBufferBinding(StreamOutputBuffer buffer)
    {
      return new StreamOutputBufferBinding(buffer);
    }


    /// <summary>
    /// Implicitly converts a stream output buffer binding to a stream output buffer.
    /// </summary>
    /// <param name="binding">The stream output buffer binding.</param>
    public static implicit operator StreamOutputBuffer(StreamOutputBufferBinding binding)
    {
      return binding.StreamOutputBuffer;
    }

    /// <summary>
    /// Tests equality between two stream output buffer bindings.
    /// </summary>
    /// <param name="a">First binding</param>
    /// <param name="b">Second binding</param>
    /// <returns>True if the two are equal, false otherwise.</returns>
    public static bool operator ==(StreamOutputBufferBinding a, StreamOutputBufferBinding b)
    {
      return (a.m_vertexOffset == b.m_vertexOffset) && Object.ReferenceEquals(a.m_buffer, b.m_buffer);
    }

    /// <summary>
    /// Tests inequality between two stream output buffer bindings.
    /// </summary>
    /// <param name="a">First binding</param>
    /// <param name="b">Second binding</param>
    /// <returns>True if the two are not equal, false otherwise.</returns>
    public static bool operator !=(StreamOutputBufferBinding a, StreamOutputBufferBinding b)
    {
      return (a.m_vertexOffset != b.m_vertexOffset) || !Object.ReferenceEquals(a.m_buffer, b.m_buffer);
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.</returns>
    public readonly override int GetHashCode()
    {
      unchecked
      {
        int hash = 17;

        hash = (hash * 31) + m_vertexOffset.GetHashCode();
        hash = (hash * 31) + m_buffer.VertexLayout.GetHashCode();

        return hash;
      }
    }

    /// <summary>
    /// Indicates whether this instance and a specified object are equal.
    /// </summary>
    /// <param name="obj">Another object to compare to.</param>
    /// <returns>True if <paramref name="obj" /> and this instance are the same type and represent the same value; otherwise, false.</returns>
    public readonly override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is StreamOutputBufferBinding)
        return Equals((StreamOutputBufferBinding) obj);

      return false;
    }

    /// <summary>
    /// Tests equality between this stream output buffer binding and another, that is if they both bind the same buffer and have the same binding configuration.
    /// </summary>
    /// <param name="other">Other stream output buffer binding to compare against.</param>
    /// <returns>True if the two bindings are equal, false otherwise.</returns>
    public readonly bool Equals(StreamOutputBufferBinding other)
    {
      return (other.m_vertexOffset == m_vertexOffset) && Object.ReferenceEquals(other.m_buffer, m_buffer);
    }

    /// <summary>
    /// Returns the fully qualified type name of this instance.
    /// </summary>
    /// <returns>A <see cref="T:System.String" /> containing a fully qualified type name.</returns>
    public readonly override string ToString()
    {
      return String.Format(CultureInfo.CurrentCulture, "VertexOffset: {0}, Buffer: {1}", new object?[] { VertexOffset.ToString(), (m_buffer is null) ? "null" : m_buffer.ToString() });
    }
  }
}
