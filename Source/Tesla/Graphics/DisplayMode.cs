﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Runtime.InteropServices;

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// Description for a display mode, that provide details of a display that the device is rendering to.
  /// </summary>
  [StructLayout(LayoutKind.Sequential)]
  public struct DisplayMode : IEquatable<DisplayMode>
  {
    private int m_width;
    private int m_height;
    private int m_refreshRate;
    private SurfaceFormat m_format;

    /// <summary>
    /// Gets the display width in pixels.
    /// </summary>
    public readonly int Width
    {
      get
      {
        return m_width;
      }
    }

    /// <summary>
    /// Gets the display height in pixels.
    /// </summary>
    public readonly int Height
    {
      get
      {
        return m_height;
      }
    }

    /// <summary>
    /// Gets the aspect ratio (width / height).
    /// </summary>
    public readonly float AspectRatio
    {
      get
      {
        if (m_width != 0 && m_height != 0)
          return (float) m_width / (float) m_height;

        return 0.0f;
      }
    }

    /// <summary>
    /// Gets the refresh rate of the display mode.
    /// </summary>
    public readonly float RefreshRate
    {
      get
      {
        return m_refreshRate;
      }
    }

    /// <summary>
    /// Gets the surface format supported by this display mode.
    /// </summary>
    public readonly SurfaceFormat SurfaceFormat
    {
      get
      {
        return m_format;
      }
    }

    /// <summary>
    /// Creates a new display mode description.
    /// </summary>
    /// <param name="width">Width of the display in pixels</param>
    /// <param name="height">Height of the display in pixels</param>
    /// <param name="refreshRate">Refresh rate of the display</param>
    /// <param name="format">Format of the display</param>
    public DisplayMode(int width, int height, int refreshRate, SurfaceFormat format)
    {
      m_width = width;
      m_height = height;
      m_refreshRate = refreshRate;
      m_format = format;
    }

    /// <summary>
    /// Tests equality between two display modes.
    /// </summary>
    /// <param name="a">First display mode</param>
    /// <param name="b">Second display mode</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    public static bool operator ==(DisplayMode a, DisplayMode b)
    {
      return (a.m_width == b.m_width) && (a.m_height == b.m_height) && (a.m_refreshRate == b.m_refreshRate) && (a.m_format == b.m_format);
    }

    /// <summary>
    /// Tests inequality between two display modes.
    /// </summary>
    /// <param name="a">First display mode</param>
    /// <param name="b">Second display mode</param>
    /// <returns>True if both are not equal, false otherwise.</returns>
    public static bool operator !=(DisplayMode a, DisplayMode b)
    {
      return (a.m_width != b.m_width) || (a.m_height != b.m_height) || (a.m_refreshRate != b.m_refreshRate) || (a.m_format != b.m_format);
    }

    /// <summary>
    /// Tests equality betwen this display mode and another.
    /// </summary>
    /// <param name="other">Other display mode to compare to.</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    public readonly bool Equals(DisplayMode other)
    {
      return (m_width == other.m_width) && (m_height == other.m_height) && (m_refreshRate == other.m_refreshRate) && (m_format == other.m_format);
    }

    /// <summary>
    /// Indicates whether this instance and a specified object are equal.
    /// </summary>
    /// <param name="obj">Another object to compare to.</param>
    /// <returns>True if <paramref name="obj" /> and this instance are the same type and represent the same value; otherwise, false. </returns>
    public readonly override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is DisplayMode)
      {
        DisplayMode other = (DisplayMode) obj;
        return (m_width == other.m_width) && (m_height == other.m_height) && (m_refreshRate == other.m_refreshRate) && (m_format == other.m_format);
      }
      return false;
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
    public readonly override int GetHashCode()
    {
      unchecked
      {
        return m_width.GetHashCode() + m_height.GetHashCode() + m_refreshRate.GetHashCode() + m_format.GetHashCode();
      }
    }

    /// <summary>
    /// Returns the fully qualified type name of this instance.
    /// </summary>
    /// <returns>A <see cref="T:System.String" /> containing a fully qualified type name. </returns>
    public readonly override string ToString()
    {
      CultureInfo info = CultureInfo.CurrentCulture;
      return String.Format(info, "Width: {0}, Height: {1}, AspectRatio: {2}, RefreshRate: {3},  SurfaceFormat: {4}",
          new object[] { m_width.ToString(), m_height.ToString(), AspectRatio.ToString(), m_refreshRate.ToString(), m_format.ToString() });
    }
  }
}
