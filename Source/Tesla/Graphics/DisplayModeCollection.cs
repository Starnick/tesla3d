﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System.Collections.Generic;

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// Read only collection for display modes.
  /// </summary>
  public sealed class DisplayModeCollection : ReadOnlyList<DisplayMode>
  {
    /// <summary>
    /// Gets all the display modes associated with the specified format.
    /// </summary>
    /// <param name="format">Format</param>
    /// <returns>List of all the display modes that are associated with the specified format.</returns>
    public IEnumerable<DisplayMode> this[SurfaceFormat format]
    {
      get
      {
        List<DisplayMode> modes = new List<DisplayMode>();
        for (int i = 0; i < Count; i++)
        {
          DisplayMode mode = this[i];

          if (mode.SurfaceFormat == format)
            modes.Add(mode);
        }

        return modes;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="DisplayModeCollection"/> class.
    /// </summary>
    /// <param name="displayModes">Display modes to initialize the collection with.</param>
    public DisplayModeCollection(IEnumerable<DisplayMode> displayModes) : base(displayModes) { }
  }
}
