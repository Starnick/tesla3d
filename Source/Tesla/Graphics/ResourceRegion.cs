﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Globalization;
using System.Runtime.InteropServices;

namespace Tesla.Graphics
{
  /// <summary>
  /// Region representing a section of a 1D resource.
  /// </summary>
  [Serializable]
  [StructLayout(LayoutKind.Sequential)]
  public struct ResourceRegion1D
  {
    /// <summary>
    /// Left most position in the resource at which to access (0 or greater).
    /// </summary>
    public int Left;

    /// <summary>
    /// Right most position in the resource at which to access (Width of the resource or less).
    /// </summary>
    public int Right;

    /// <summary>
    /// Gets the width of the region.
    /// </summary>
    public int Width
    {
      get
      {
        return Right - Left;
      }
    }

    /// <summary>
    /// Gets the number of texel elements encompassed in the region.
    /// </summary>
    public int ElementCount
    {
      get
      {
        return Right - Left;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="ResourceRegion1D"/> struct.
    /// </summary>
    /// <param name="left">The left most position in the 1D resource at which to access (0 or greater).</param>
    /// <param name="right">The right most position in the 1D resource at which to access (Width or less).</param>
    public ResourceRegion1D(int left, int right)
    {
      Left = left;
      Right = right;
    }

    /// <summary>
    /// Validates the resource region, ensuring that the region is in fact within bounds of the resource. Upon completion of the check, the resource
    /// dimensions will hold the dimensions of the subimage.
    /// </summary>
    /// <param name="width">Width of the resource, in texels. This will then hold the subimage width.</param>
    public void ValidateRegion(ref int width)
    {
      if (Left < 0 || Right <= 0)
        throw new ArgumentOutOfRangeException("subimage", StringLocalizer.Instance.GetLocalizedString("RegionLeftRightOutOfRange"));

      if (width < 0 ||
          Left > width || Right > width || Left == Right)
        throw new ArgumentOutOfRangeException("subimage", StringLocalizer.Instance.GetLocalizedString("ResourceRegionInvalid"));

      width = Right - Left;
    }

    /// <summary>
    /// Returns the fully qualified type name of this instance.
    /// </summary>
    /// <returns>A <see cref="T:System.String" /> containing a fully qualified type name.</returns>
    public override String ToString()
    {
      return String.Format(CultureInfo.CurrentCulture, "Left: {0}, Right: {1} - Width: {2}", Left.ToString(), Right.ToString(), Width.ToString());
    }
  }

  /// <summary>
  /// Region representing a section of a 2D resource.
  /// </summary>
  [Serializable]
  [StructLayout(LayoutKind.Sequential)]
  public struct ResourceRegion2D
  {
    /// <summary>
    /// Left most position in the resource at which to access (0 or greater).
    /// </summary>
    public int Left;

    /// <summary>
    /// Right most position in the resource at which to access (Width of the resource or less).
    /// </summary>
    public int Right;

    /// <summary>
    /// Top most position in the resource at which to access (0 or greater).
    /// </summary>
    public int Top;

    /// <summary>
    /// Bottom most position in the resource at which to to access (Height of the resource or less).
    /// </summary>
    public int Bottom;

    /// <summary>
    /// Gets the width of the region.
    /// </summary>
    public int Width
    {
      get
      {
        return Right - Left;
      }
    }

    /// <summary>
    /// Gets the height of the region.
    /// </summary>
    public int Height
    {
      get
      {
        return Bottom - Top;
      }
    }

    /// <summary>
    /// Gets the number of texel elements encompassed in the region.
    /// </summary>
    public int ElementCount
    {
      get
      {
        return (Right - Left) * (Bottom - Top);
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="ResourceRegion2D"/> struct.
    /// </summary>
    /// <param name="left">The left most position in the 2D resource at which to access (0 or greater).</param>
    /// <param name="right">The right most position in the 2D resource at which to access (Width or less).</param>
    /// <param name="top">The top most position in the 2D resource at which to access (0 or greater).</param>
    /// <param name="bottom">The bottom most position in the 2D resource at which to access (Height or less).</param>
    public ResourceRegion2D(int left, int right, int top, int bottom)
    {
      Left = left;
      Right = right;
      Top = top;
      Bottom = bottom;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="ResourceRegion2D"/> struct.
    /// </summary>
    /// <param name="region">One dimensional region to populate from.</param>
    public ResourceRegion2D(ResourceRegion1D region)
    {
      Left = region.Left;
      Right = region.Right;
      Top = 0;
      Bottom = 1;
    }

    /// <summary>
    /// Implicitly converts the rectangle to the resource region.
    /// </summary>
    /// <param name="rectangle">Rectangle to convert</param>
    /// <returns>The resource region</returns>
    public static implicit operator ResourceRegion2D(Rectangle rectangle)
    {
      return new ResourceRegion2D(rectangle.X, rectangle.Width, rectangle.Y, rectangle.Height);
    }

    /// <summary>
    /// Implicitly converts the resource region to a rectangle.
    /// </summary>
    /// <param name="region">Resource region to convert</param>
    /// <returns>The rectangle</returns>
    public static implicit operator Rectangle(ResourceRegion2D region)
    {
      return new Rectangle(region.Left, region.Top, region.Right, region.Bottom);
    }

    /// <summary>
    /// Validates the resource region, ensuring that the region is in fact within bounds of the resource. Upon completion of the check, the resource
    /// dimensions will hold the dimensions of the subimage.
    /// </summary>
    /// <param name="width">Width of the resource, in texels. This will then hold the subimage width.</param>
    /// <param name="height">Height of the resource, in texels. This will then hold the subimage height.</param>
    public void ValidateRegion(ref int width, ref int height)
    {
      if (Left < 0 || Right <= 0)
        throw new ArgumentOutOfRangeException("subimage", StringLocalizer.Instance.GetLocalizedString("RegionLeftRightOutOfRange"));

      if (Top < 0 || Bottom <= 0)
        throw new ArgumentOutOfRangeException("subimage", StringLocalizer.Instance.GetLocalizedString("RegionTopBottomOutOfRange"));

      if (width < 0 || height < 0 ||
          Left > width || Right > width || Left == Right ||
          Top > height || Bottom > height || Top == Bottom)
        throw new ArgumentOutOfRangeException("subimage", StringLocalizer.Instance.GetLocalizedString("ResourceRegionInvalid"));

      width = Right - Left;
      height = Bottom - Top;
    }

    /// <summary>
    /// Returns the fully qualified type name of this instance.
    /// </summary>
    /// <returns>A <see cref="T:System.String" /> containing a fully qualified type name.</returns>
    public override String ToString()
    {
      return String.Format(CultureInfo.CurrentCulture, "Left: {0}, Right: {1}, Top: {2}, Bottom: {3} - Width: {4}, Height: {5}",
          Left.ToString(), Right.ToString(), Top.ToString(), Bottom.ToString(), Width.ToString(), Height.ToString());
    }
  }

  /// <summary>
  /// Region representing a section of a 3D texture resource.
  /// </summary>
  [Serializable]
  [StructLayout(LayoutKind.Sequential)]
  public struct ResourceRegion3D
  {
    /// <summary>
    /// Left most position in the resource at which to access (0 or greater).
    /// </summary>
    public int Left;

    /// <summary>
    /// Right most position in the resource at which to access (Width of the resource or less).
    /// </summary>
    public int Right;

    /// <summary>
    /// Top most position in the resource at which to access (0 or greater).
    /// </summary>
    public int Top;

    /// <summary>
    /// Bottom most position in the resource at which to to access (Height of the resource or less).
    /// </summary>
    public int Bottom;

    /// <summary>
    /// Front most position in the resource at which to access (0 or greater).
    /// </summary>
    public int Front;

    /// <summary>
    /// Back most position in the resource at which to access (Depth or less).
    /// </summary>
    public int Back;

    /// <summary>
    /// Gets the width of the region.
    /// </summary>
    public int Width
    {
      get
      {
        return Right - Left;
      }
    }

    /// <summary>
    /// Gets the height of the region.
    /// </summary>
    public int Height
    {
      get
      {
        return Bottom - Top;
      }
    }

    /// <summary>
    /// Gets the depth of the region.
    /// </summary>
    public int Depth
    {
      get
      {
        return Back - Front;
      }
    }

    /// <summary>
    /// Gets the number of texel elements encompassed in the region.
    /// </summary>
    public int ElementCount
    {
      get
      {
        return (Right - Left) * (Bottom - Top) * (Back - Front);
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="ResourceRegion3D"/> struct.
    /// </summary>
    /// <param name="left">The left most position in the 3D resource at which to access (0 or greater).</param>
    /// <param name="right">The right most position in the 3D resource at which to access (Width or less).</param>
    /// <param name="top">The top most position in the 3D resource at which to access (0 or greater).</param>
    /// <param name="bottom">The bottom most position in the 2D resource at which to access (Height or less).</param>
    /// <param name="front">The front most position in the 3D resource at which to access (0 or greater).</param>
    /// <param name="back">The back most position in the 3D resource at which to access (Depth or less).</param>
    public ResourceRegion3D(int left, int right, int top, int bottom, int front, int back)
    {
      Left = left;
      Right = right;
      Top = top;
      Bottom = bottom;
      Front = front;
      Back = back;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="ResourceRegion3D"/> struct.
    /// </summary>
    /// <param name="region">One dimensional region to populate from.</param>
    public ResourceRegion3D(ResourceRegion1D region)
    {
      Left = region.Left;
      Right = region.Right;
      Top = 0;
      Bottom = 1;
      Front = 0;
      Back = 1;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="ResourceRegion3D"/> struct.
    /// </summary>
    /// <param name="region">Two dimensional region to populate from.</param>
    public ResourceRegion3D(ResourceRegion2D region)
    {
      Left = region.Left;
      Right = region.Right;
      Top = region.Top;
      Bottom = region.Bottom;
      Front = 0;
      Back = 1;
    }

    /// <summary>
    /// Validates the resource region, ensuring that the region is in fact within bounds of the resource. Upon completion of the check, the resource
    /// dimensions will hold the dimensions of the subimage.
    /// </summary>
    /// <param name="width">Width of the resource, in texels. This will then hold the subimage width.</param>
    /// <param name="height">Height of the resource, in texels. This will then hold the subimage height.</param>
    /// <param name="depth">Depth of the resource, in texels. This will then hold the subimage depth.</param>
    public void ValidateRegion(ref int width, ref int height, ref int depth)
    {
      if (Left < 0 || Right <= 0)
        throw new ArgumentOutOfRangeException("subimage", StringLocalizer.Instance.GetLocalizedString("RegionLeftRightOutOfRange"));

      if (Top < 0 || Bottom <= 0)
        throw new ArgumentOutOfRangeException("subimage", StringLocalizer.Instance.GetLocalizedString("RegionTopBottomOutOfRange"));

      if (Front < 0 || Back <= 0)
        throw new ArgumentOutOfRangeException("subimage", StringLocalizer.Instance.GetLocalizedString("RegionFrontBackOutOfRange"));

      if (width < 0 || height < 0 || depth < 0 ||
          Left > width || Right > width || Left == Right ||
          Top > height || Bottom > height || Top == Bottom ||
          Front > depth || Back > depth || Front == Back)
        throw new ArgumentOutOfRangeException("subimage", StringLocalizer.Instance.GetLocalizedString("ResourceRegionInvalid"));

      width = Right - Left;
      height = Bottom - Top;
      depth = Back - Front;
    }

    /// <summary>
    /// Returns the fully qualified type name of this instance.
    /// </summary>
    /// <returns>A <see cref="T:System.String" /> containing a fully qualified type name.</returns>
    public override String ToString()
    {
      return String.Format(CultureInfo.CurrentCulture, "Left: {0}, Right: {1}, Top: {2}, Bottom: {3}, Front: {4}, Back {5} - Width: {6}, Height: {7}, Depth: {8}",
          Left.ToString(), Right.ToString(), Top.ToString(), Bottom.ToString(), Front.ToString(), Back.ToString(), Width.ToString(), Height.ToString(), Depth.ToString());
    }
  }
}
