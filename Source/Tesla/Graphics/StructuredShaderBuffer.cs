﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Content;
using Tesla.Graphics.Implementation;

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// <para>
  /// Represents a structured buffer on the GPU that can be bound as a shader resource. The data is tightly packed and can
  /// be any scalar, vector, or struct type. Unlike a vertex buffer, the layout of an element is not needed ahead of time, just
  /// the size in bytes. An <see cref="IEffectParameter"/> will have a structure layout if it represents a structured buffer and that can be
  /// queried. In many ways this is analogus to a <see cref="DataBuffer{T}"/> except that it exists on the GPU.
  /// </para>
  /// <para>
  /// Since the buffer is tightly packed, it is up to the shader author to set up the alignment. These buffers are accessed more efficiently when they adhere to 16-byte boundaries,
  /// see <see href="https://developer.nvidia.com/content/understanding-structured-buffer-performance"/> for an example.
  /// </para>
  /// </summary>
  public class StructuredShaderBuffer : GraphicsResource, ISavable, IBufferResource
  {
    #region Public Properties

    /// <summary>
    /// Gets the number of elements in the buffer.
    /// </summary>
    public int ElementCount
    {
      get
      {
        return StructuredShaderBufferImpl.ElementCount;
      }
    }

    /// <summary>
    /// Gets size in bytes of a single element in the buffer.
    /// </summary>
    public int ElementStride
    {
      get
      {
        return StructuredShaderBufferImpl.ElementStride;
      }
    }

    /// <summary>
    /// Gets the total size in bytes of the buffer.
    /// </summary>
    public int SizeInBytes
    {
      get
      {
        return StructuredShaderBufferImpl.ElementStride * StructuredShaderBufferImpl.ElementCount;
      }
    }

    /// <summary>
    /// Gets the resource usage of the buffer.
    /// </summary>
    public ResourceUsage ResourceUsage
    {
      get
      {
        return StructuredShaderBufferImpl.ResourceUsage;
      }
    }

    /// <summary>
    /// Gets the resource binding of the buffer.
    /// </summary>
    public virtual ResourceBindFlags ResourceBindFlags
    {
      get
      {
        return StructuredShaderBufferImpl.ResourceBindFlags;
      }
    }

    /// <summary>
    /// Gets the shader resource type.
    /// </summary>
    public ShaderResourceType ResourceType
    {
      get
      {
        return ShaderResourceType.Buffer;
      }
    }

    #endregion

    //For casting purposes
    private IStructuredShaderBufferImpl StructuredShaderBufferImpl
    {
      get
      {
        return GetImplAs<IStructuredShaderBufferImpl>();
      }
      set
      {
        BindImplementation(value);
      }
    }

    /// <summary>
    /// For ISavable.
    /// </summary>
    protected StructuredShaderBuffer() { }

    /// <summary>
    /// Constructs a new instance of the <see cref="StructuredShaderBuffer"/> class.
    /// </summary>
    /// <param name="renderSystem">Render system used to create the underlying implementation.</param>
    /// <param name="elementStride">Size of a single element in bytes.</param>
    /// <param name="elementCount">Number of elements in the buffer.</param>
    /// <param name="options">Common <see cref="BufferOptions"/> that may also contain initial data.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the render system is null or if any input is null when it is required (e.g. initial data is required for immutable resources).</exception>
    /// <exception cref="System.ArgumentOutOfRangeException">Thrown if the element count is less than or equal to zero.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if creating the underlying implementation fails or is unsupported, see inner exception for a more detailed error.</exception>
    public StructuredShaderBuffer(IRenderSystem renderSystem, int elementStride, int elementCount, BufferOptions options = default)
    {
      CreateImplementation(renderSystem, elementStride, elementCount, options);
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="StructuredShaderBuffer"/> class. Initial data MUST be populated and may be a raw byte buffer.
    /// </summary>
    /// <param name="renderSystem">Render system used to create the underlying implementation.</param>
    /// <param name="options">Common <see cref="BufferOptions"/>. Initial data MUST exist and may be raw bytes. If raw bytes, be sure to set an appropiate element stride.</param>
    /// <param name="elementStride">Size of a single element in bytes. If null, the size is determined by the element size in the initial data.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the render system is null or if any input is null when it is required (e.g. initial data is required for immutable resources).</exception>
    /// <exception cref="System.ArgumentOutOfRangeException">Thrown if the element count is less than or equal to zero.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if creating the underlying implementation fails or is unsupported, see inner exception for a more detailed error.</exception>
    public StructuredShaderBuffer(IRenderSystem renderSystem, BufferOptions options, int? elementStride = null)
    {
      int elementCount = options.Data?.Length ?? 0;
      int stride = elementStride.HasValue ? elementStride.Value : options.Data?.ElementSizeInBytes ?? 0;
      CreateImplementation(renderSystem, stride, elementCount, options);
    }

    #region Public Methods

    /// <summary>
    /// Reads data from the graphics buffer into the data buffer
    /// </summary>
    /// <typeparam name="T">Type of data to read from the graphics buffer.</typeparam>
    /// <param name="data">Buffer to hold data copied from the graphics buffer.</param>
    /// <param name="offsetInBytes">Optional offset from the start of the graphics buffer at which to start copying from.</param>
    /// <exception cref="ObjectDisposedException">Thrown if the resource has already been disposed.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if an error occurs while reading from the resource.</exception>
    /// <remarks>See implementors for details on specific exceptions that can be thrown.</remarks>
    public void GetData<T>(Span<T> data, int offsetInBytes = 0) where T : unmanaged
    {
      CheckDisposed();

      try
      {
        StructuredShaderBufferImpl.GetData<T>(data, offsetInBytes);
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw GraphicsExceptionHelper.NewErrorReadingFromResourceException(typeof(StructuredShaderBuffer), e);
      }
    }

    /// <summary>
    /// Writes data from the data buffer into the graphics buffer.
    /// </summary>
    /// <typeparam name="T">Type of data to write to the graphics buffer.</typeparam>
    /// <param name="renderContext">The current render context.</param>
    /// <param name="data">Buffer with the data to be copied to the graphics buffer.</param>
    /// <param name="writeOptions">Optional write options, used only if this is a dynamic buffer. None, discard, no overwrite.</param>
    /// <exception cref="ObjectDisposedException">Thrown if the resource has already been disposed.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if an error occurs while writing to the resource, or if trying to write to a non-DYNAMIC resource 
    /// using a deferred render context.</exception>
    /// <remarks>See implementors for details on specific exceptions that can be thrown.</remarks>
    public void SetData<T>(IRenderContext renderContext, ReadOnlySpan<T> data, DataWriteOptions writeOptions = DataWriteOptions.Discard) where T : unmanaged
    {
      SetData<T>(renderContext, data, 0, writeOptions);
    }

    /// <summary>
    /// Writes data from the data buffer into the graphics buffer.
    /// </summary>
    /// <typeparam name="T">Type of data to write to the graphics buffer.</typeparam>
    /// <param name="renderContext">The current render context.</param>
    /// <param name="data">Buffer with the data to be copied to the graphics buffer.</param>
    /// <param name="offsetInBytes">Offset from the start of the graphics buffer at which to start writing at.</param>
    /// <param name="writeOptions">Optional write options, used only if this is a dynamic buffer. None, discard, no overwrite.</param>
    /// <exception cref="ObjectDisposedException">Thrown if the resource has already been disposed.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if an error occurs while writing to the resource, or if trying to write to a non-DYNAMIC resource 
    /// using a deferred render context.</exception>
    /// <remarks>See implementors for details on specific exceptions that can be thrown.</remarks>
    public void SetData<T>(IRenderContext renderContext, ReadOnlySpan<T> data, int offsetInBytes, DataWriteOptions writeOptions = DataWriteOptions.Discard) where T : unmanaged
    {
      CheckDisposed();
      CheckDeferredSetDataIsPermitted(renderContext, ResourceUsage);

      try
      {
        StructuredShaderBufferImpl.SetData<T>(renderContext, data, offsetInBytes, writeOptions);
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw GraphicsExceptionHelper.NewErrorWritingToResourceException(typeof(StructuredShaderBuffer), e);
      }
    }

    #endregion

    #region ISavable Methods

    /// <summary>
    /// Reads the object data from the input.
    /// </summary>
    /// <param name="input">Savable reader</param>
    public void Read(ISavableReader input)
    {
      IRenderSystem renderSystem = GraphicsHelper.GetRenderSystem(input.ServiceProvider);

      Name = input.ReadString("Name");
      int elementCount = input.ReadInt32("ElementCount");
      int elementStride = input.ReadInt32("ElementStride");
      ResourceUsage usage = input.ReadEnum<ResourceUsage>("ResourceUsage");
      ResourceBindFlags bindFlags = input.ReadEnum<ResourceBindFlags>("ResourceBindFlags");

      // Create a pooled buffer for the data
      using (DataBuffer<byte> data = input.ReadBlob<byte>("Data", MemoryAllocatorStrategy.DefaultPooled))
        CreateImplementation(renderSystem, elementStride, elementCount, BufferOptions.Init(data, usage, bindFlags));
    }

    /// <summary>
    /// Writes the object data to the output.
    /// </summary>
    /// <param name="output">Savable writer</param>
    public void Write(ISavableWriter output)
    {
      output.Write("Name", Name);
      output.Write("ElementCount", ElementCount);
      output.Write("ElementStride", ElementStride);
      output.WriteEnum<ResourceUsage>("ResourceUsage", ResourceUsage);
      output.WriteEnum<ResourceBindFlags>("ResourceBindFlags", ResourceBindFlags);

      // Create a pooled buffer for the data
      using (DataBuffer<byte> data = DataBuffer.Create<byte>(SizeInBytes, MemoryAllocatorStrategy.DefaultPooled))
      {
        GetData<byte>(data.Span);

        output.WriteBlob<byte>("Data", data.Span);
      }
    }

    #endregion

    #region Creation Parameter Validation


    /// <summary>
    /// Validates creation parameters.
    /// </summary>
    /// <param name="elementCount">The number of elements the buffer will contain.</param>
    /// <exception cref="System.ArgumentOutOfRangeException">Thrown if the element count is less than or equal to zero.</exception>
    protected void ValidateCreationParameters(int elementCount)
    {
      if (elementCount <= 0)
        throw new ArgumentOutOfRangeException(nameof(elementCount), StringLocalizer.Instance.GetLocalizedString("ResourceSizeMustBeGreaterThanZero"));
    }

    /// <summary>
    /// Validates creation parameters.
    /// </summary>
    /// <param name="elementStride">Size of an element in bytes.</param>
    /// <param name="elementCount">Number of elements the buffer will contain.</param>
    /// <param name="options">Common <see cref="BufferOptions"/> that may also contain initial data.</param>
    /// <exception cref="ArgumentNullException">Thrown if the initial data is null and the resource is set to immutable.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the element count and stride does not match the initial data size, if any.</exception>
    protected void ValidateCreationParameters(int elementStride, int elementCount, BufferOptions options)
    {
      ValidateCreationParameters(elementCount);

      if (options.Data is null)
      {
        if (options.ResourceUsage == ResourceUsage.Immutable)
          throw GraphicsExceptionHelper.NewMustSupplyDataForImmutableException();

        return;
      }

      IReadOnlyDataBuffer data = options.Data;
      if (data.Length == 0)
        throw new ArgumentNullException(nameof(options.Data), StringLocalizer.Instance.GetLocalizedString("DataBufferNull"));

      int totalSize = data.SizeInBytes;

      // Format size should divide evenly into the total buffer size
      if (totalSize % elementStride != 0 || BufferHelper.ComputeElementCount(totalSize, elementStride) != elementCount)
        throw new ArgumentOutOfRangeException(nameof(options.Data), StringLocalizer.Instance.GetLocalizedString("FormatSizeMismatch"));
    }

    #endregion

    #region Implementation Creation

    private void CreateImplementation(IRenderSystem renderSystem, int elementStride, int elementCount, BufferOptions options)
    {
      GraphicsHelper.ThrowIfRenderSystemNull(renderSystem);

      ValidateCreationParameters(elementStride, elementCount, options);

      IStructuredShaderBufferImplFactory factory;
      if (!renderSystem.TryGetImplementationFactory<IStructuredShaderBufferImplFactory>(out factory))
        throw GraphicsExceptionHelper.NewUnsupportedFeatureException(renderSystem, typeof(StructuredShaderBuffer));

      try
      {
        StructuredShaderBufferImpl = factory.CreateImplementation(elementStride, elementCount, options);
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw GraphicsExceptionHelper.NewErrorCreatingImplementation(typeof(StructuredShaderBuffer), e);
      }
    }

    #endregion
  }
}
