﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Graphics.Implementation;

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// Represents an occlusion query that is used to determine how many pixels were actually drawn during a set of draw calls, which can be useful
  /// to determine object visibility and to optimize rendering.
  /// </summary>
  public class OcclusionQuery : GraphicsResource
  {

    #region Public Properties

    /// <summary>
    /// Gets if the query has been completed or not.
    /// </summary>
    public bool IsComplete
    {
      get
      {
        return OcclusionQueryImpl.IsComplete;
      }
    }

    /// <summary>
    /// Gets the number of visible pixels, after the query has been completed.
    /// </summary>
    public long PixelCount
    {
      get
      {
        return OcclusionQueryImpl.PixelCount;
      }
    }

    #endregion

    /// <summary>
    /// Constructs a new instance of the <see cref="OcclusionQuery"/> class.
    /// </summary>
    /// <param name="renderSystem">Render system used to create the underlying implementation.</param>
    /// <exception cref="TeslaGraphicsException">Thrown if creating the underlying implementation fails or is unsupported, see inner exception for a more detailed error.</exception>
    public OcclusionQuery(IRenderSystem renderSystem)
    {
      CreateImplementation(renderSystem);
    }

    //Private property to cast the implementation
    private IOcclusionQueryImpl OcclusionQueryImpl
    {
      get
      {
        return GetImplAs<IOcclusionQueryImpl>();
      }
      set
      {
        BindImplementation(value);
      }
    }

    /// <summary>
    /// Begins the query.
    /// </summary>
    public void Begin()
    {
      try
      {
        OcclusionQueryImpl.Begin();
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw GraphicsExceptionHelper.NewQueryFailedOnBegin(e);
      }
    }

    /// <summary>
    /// Ends the query.
    /// </summary>
    public void End()
    {
      try
      {
        OcclusionQueryImpl.End();
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw GraphicsExceptionHelper.NewQueryFailedOnEnd(e);
      }
    }

    #region Implementation Creation

    private void CreateImplementation(IRenderSystem renderSystem)
    {
      ArgumentNullException.ThrowIfNull(renderSystem);

      IOcclusionQueryImplFactory factory;
      if (!renderSystem.TryGetImplementationFactory<IOcclusionQueryImplFactory>(out factory))
        throw GraphicsExceptionHelper.NewUnsupportedFeatureException(renderSystem, typeof(OcclusionQuery));

      try
      {
        OcclusionQueryImpl = factory.CreateImplementation();
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw GraphicsExceptionHelper.NewErrorCreatingImplementation(typeof(OcclusionQuery), e);
      }
    }

    #endregion
  }
}
