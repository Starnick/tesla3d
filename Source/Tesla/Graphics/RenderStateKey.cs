﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// Key representing a render state - the state type and the state's hash code.
  /// </summary>
  public readonly struct RenderStateKey : IEquatable<RenderStateKey>
  {
    /// <summary>
    /// Gets the render state type.
    /// </summary>
    public readonly RenderStateType StateType;

    /// <summary>
    /// Gets the render state's hash code.
    /// </summary>
    public readonly int Hash;

    /// <summary>
    /// Constructs a new instance of the <see cref="RenderStateKey"/> struct.
    /// </summary>
    /// <param name="stateType">Render state type.</param>
    /// <param name="hash">Render state hash code.</param>
    public RenderStateKey(RenderStateType stateType, int hash)
    {
      StateType = stateType;
      Hash = hash;
    }

    /// <summary>
    /// Tests equality between two render state keys.
    /// </summary>
    /// <param name="a">First render state key</param>
    /// <param name="b">Second render state key</param>
    /// <returns>True if the keys are equal, false otherwise.</returns>
    public static bool operator ==(RenderStateKey a, RenderStateKey b)
    {
      return (a.StateType == b.StateType) && (a.Hash == b.Hash);
    }

    /// <summary>
    /// Tests inequality between two render state keys.
    /// </summary>
    /// <param name="a">First render state key</param>
    /// <param name="b">Second render state key</param>
    /// <returns>True if the keys are not equal, false otherwise.</returns>
    public static bool operator !=(RenderStateKey a, RenderStateKey b)
    {
      return (a.StateType != b.StateType) || (a.Hash != b.Hash);
    }

    /// <summary>
    /// Indicates whether this instance and a specified object are equal.
    /// </summary>
    /// <param name="obj">Another object to compare to.</param>
    /// <returns>True if <paramref name="obj" /> and this instance are the same type and represent the same value; otherwise, false.</returns>
    public readonly override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is RenderStateKey)
        return Equals((RenderStateKey) obj);

      return false;
    }

    /// <summary>
    /// Indicates whether the current object is equal to another object of the same type.
    /// </summary>
    /// <param name="other">An object to compare with this object.</param>
    /// <returns>True if the current object is equal to the <paramref name="other" /> parameter; otherwise, false.</returns>
    public readonly bool Equals(RenderStateKey other)
    {
      return (StateType == other.StateType) && (Hash == other.Hash);
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
    public readonly override int GetHashCode()
    {
      return Hash;
    }

    /// <summary>
    /// Returns the fully qualified type name of this instance.
    /// </summary>
    /// <returns>A <see cref="T:System.String" /> containing a fully qualified type name.</returns>
    public readonly override string ToString()
    {
      CultureInfo info = CultureInfo.CurrentCulture;
      return String.Format(info, "StateType: {0}, Hash: {1}",
          new object[] { StateType.ToString(), Hash.ToString() });
    }
  }
}
