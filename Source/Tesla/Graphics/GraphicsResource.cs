﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;
using Tesla.Graphics.Implementation;

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// Base class for all graphics resources that are created and managed by a render system.
  /// </summary>
  public abstract class GraphicsResource : IDisposable, IDebugNamable, INamable
  {
    private IGraphicsResourceImpl? m_impl;
    private string m_name;
    private object? m_tag;

    /// <summary>
    /// Occurs when the graphics resource is disposed.
    /// </summary>
    public event TypedEventHandler<GraphicsResource>? Disposing;

    /// <summary>
    /// Gets or sets the name of the object.
    /// </summary>
    [AllowNull]
    public string Name
    {
      get
      {
        return m_name;
      }
      set
      {
        m_name = value ?? String.Empty;
      }
    }


    /// <summary>
    /// Gets or sets the debug name of the object, e.g. for a graphics debugger.
    /// </summary>
    [AllowNull]
    public string DebugName
    {
      get
      {
        if (m_impl is not null)
          return m_impl.DebugName;

        return String.Empty;
      }
      set
      {
        if (m_impl is not null)
          m_impl.DebugName = value ?? String.Empty;
      }
    }

    /// <summary>
    /// Gets or sets custom data.
    /// </summary>
    public object? Tag
    {
      get
      {
        return m_tag;
      }
      set
      {
        m_tag = value;
      }
    }

    /// <summary>
    /// Gets if this resource has been disposed or not.
    /// </summary>
    public bool IsDisposed
    {
      get
      {
        if (m_impl is null)
          return true;

        return m_impl.IsDisposed;
      }
    }

    /// <summary>
    /// Gets the ID of this resource.
    /// </summary>
    public int ResourceID
    {
      get
      {
        if (m_impl is null)
          return -1;

        return m_impl.ResourceID;
      }
    }

    /// <summary>
    /// Gets the render system that created and manages this resource.
    /// </summary>
    public IRenderSystem RenderSystem
    {
      get
      {
        if (m_impl is null)
          return null!;

        return m_impl.RenderSystem;
      }
    }

    /// <summary>
    /// Gets the underlying platform-specific implementation of this resource.
    /// </summary>
    public IGraphicsResourceImpl Implementation
    {
      get
      {
        return m_impl!;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="GraphicsResource"/> class.
    /// </summary>
    protected GraphicsResource() 
    {
      m_name = String.Empty;
    }

    /// <summary>
    /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
    /// </summary>
    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
    public override int GetHashCode()
    {
      return m_impl?.ResourceID ?? 0;
    }

    /// <summary>
    /// Releases unmanaged and - optionally - managed resources.
    /// </summary>
    /// <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
    protected virtual void Dispose(bool disposing)
    {
      if (m_impl is not null && !m_impl.IsDisposed)
      {
        if (disposing)
        {
          OnDispose();

          m_impl.Dispose();
        }
      }
    }

    /// <summary>
    /// Binds an implementation to the current graphics resource. An implementation can only be bound to a single graphics resource.
    /// </summary>
    /// <param name="implementor">Graphics resource implementation</param>
    /// <exception cref="ArgumentNullException">Thrown if the implementation is null.</exception>
    protected void BindImplementation(IGraphicsResourceImpl implementor)
    {
      if (implementor is null)
        throw new ArgumentNullException(nameof(implementor), StringLocalizer.Instance.GetLocalizedString("CannotBindNullImplementation"));

      // Dispose of the old implementation, if any.
      if (m_impl is not null)
        m_impl.Dispose();

      m_impl = implementor;
      m_impl.BindImplementation(this);
    }

    /// <summary>
    /// Checks if the resource was disposed and if so, throws an ObjectDisposedException.
    /// </summary>
    /// <exception cref="ObjectDisposedException">If resource has been disposed already.</exception>
    protected void CheckDisposed()
    {
      if (IsDisposed)
        throw new ObjectDisposedException(GetType().FullName);
    }

    /// <summary>
    /// Called right before the implementation is disposed.
    /// </summary>
    protected virtual void OnDispose()
    {
      TypedEventHandler<GraphicsResource>? handler = Disposing;
      if (handler is not null)
        handler(this, EventArgs.Empty);
    }

    /// <summary>
    /// Checks if writing to a resource is permitted with a deferred render context. Writing is valid if its the immediate
    /// context, or if deferred and the resource is a dynamic resource.
    /// </summary>
    /// <param name="renderContext">The current render context.</param>
    /// <param name="usage">Usage of the resource.</param>
    /// <exception cref="ArgumentNullException">Thrown if the render context is null.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if trying to write to a non-DYNAMIC resource using a deferred render context.</exception>
    protected void CheckDeferredSetDataIsPermitted(IRenderContext renderContext, ResourceUsage usage)
    {
      GraphicsHelper.ThrowIfRenderContextNull(renderContext);

      if (!renderContext.IsImmediateContext && usage != ResourceUsage.Dynamic)
        throw GraphicsExceptionHelper.NewDeferredWriteDataException();
    }

    /// <summary>
    /// Dangerously casts the current implementation as the target type. This may be null if not the appropiate type.
    /// </summary>
    /// <typeparam name="T">Concrete implementation type.</typeparam>
    /// <returns>Concrete implementation as the type.</returns>
    protected T GetImplAs<T>() where T : class, IGraphicsResourceImpl
    {
      return (m_impl as T)!;
    }
  }
}
