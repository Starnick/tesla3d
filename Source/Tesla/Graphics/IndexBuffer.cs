﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;
using Tesla.Content;
using Tesla.Graphics.Implementation;

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// Represents a buffer of indices that exists on the GPU. This allows for vertex data to be-reused, where duplicate vertices can
  /// be indexed multiple times, allowing for a smaller vertex buffer.
  /// </summary>
  [SavableVersion(1)]
  public class IndexBuffer : GraphicsResource, ISavable, IBufferResource
  {

    #region Public Properties

    /// <summary>
    /// Gets the number of indices in the buffer.
    /// </summary>
    public int IndexCount
    {
      get
      {
        return IndexBufferImpl.ElementCount;
      }
    }

    /// <summary>
    /// Gets the index format, 32-bit (int) or 16-bit (ushort).
    /// </summary>
    public IndexFormat IndexFormat
    {
      get
      {
        return IndexBufferImpl.IndexFormat;
      }
    }

    /// <summary>
    /// Gets the total size in bytes of the buffer.
    /// </summary>
    public int SizeInBytes
    {
      get
      {
        return IndexBufferImpl.ElementStride * IndexBufferImpl.ElementCount;
      }
    }

    /// <summary>
    /// Gets the resource usage of the buffer.
    /// </summary>
    public ResourceUsage ResourceUsage
    {
      get
      {
        return IndexBufferImpl.ResourceUsage;
      }
    }

    /// <summary>
    /// Gets the resource binding of the buffer.
    /// </summary>
    public virtual ResourceBindFlags ResourceBindFlags {
      get
      {
        return IndexBufferImpl.ResourceBindFlags;
      }
    }

    /// <summary>
    /// Gets the shader resource type.
    /// </summary>
    public ShaderResourceType ResourceType
    {
      get
      {
        return ShaderResourceType.Buffer;
      }
    }

    #endregion

    //For casting purposes
    private IIndexBufferImpl IndexBufferImpl
    {
      get
      {
        return GetImplAs<IIndexBufferImpl>();
      }
      set
      {
        BindImplementation(value);
      }
    }

    /// <inheritdoc />
    int IBufferResource.ElementCount { get { return IndexBufferImpl.ElementCount; } }

    /// <inheritdoc />
    int IBufferResource.ElementStride { get { return IndexBufferImpl.ElementStride; } }

    /// <summary>
    /// For ISavable.
    /// </summary>
    protected IndexBuffer() { }

    /// <summary>
    /// Constructs a new instance of the <see cref="IndexBuffer"/> class.
    /// </summary>
    /// <param name="renderSystem">Render system used to create the underlying implementation.</param>
    /// <param name="indexFormat">The index format.</param>
    /// <param name="indexCount">Number of indices the buffer will contain.</param>
    /// <param name="options">Common <see cref="BufferOptions"/> that may also contain initial data.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the render system is null or if any input is null when it is required (e.g. initial data is required for immutable resources).</exception>
    /// <exception cref="System.ArgumentOutOfRangeException">Thrown if the index count is less than or equal to zero.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if creating the underlying implementation fails or is unsupported, see inner exception for a more detailed error.</exception>
    public IndexBuffer(IRenderSystem renderSystem, IndexFormat indexFormat, int indexCount, BufferOptions options = default)
    {
      CreateImplementation(renderSystem, indexFormat, indexCount, options);
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="IndexBuffer"/> class. The number of indices is determined by the data.
    /// </summary>
    /// <param name="renderSystem">Render system used to create the underlying implementation.</param>
    /// <param name="data">Index data to initialize the buffer with, format and buffer length is derived from this data.</param>
    /// <param name="options">Common <see cref="BufferOptions"/>. Any initial data is ignored, this is to only set other common options.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the render system is null or if any input is null when it is required (e.g. initial data is required for immutable resources).</exception>
    /// <exception cref="System.ArgumentNullException">Thrown if the render system or index data are null.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if creating the underlying implementation fails or is unsupported, see inner exception for a more detailed error.</exception>
    public IndexBuffer(IRenderSystem renderSystem, in IndexData data, BufferOptions options = default)
    {
      if (data.IsValid)
        options.Data = data.UnderlyingDataBuffer;
      else
        options.Data = null;

      CreateImplementation(renderSystem, data.IndexFormat, data.Length, options);
    }

    #region Public Methods

    /// <summary>
    /// Reads data from the index buffer into the data buffer
    /// </summary>
    /// <typeparam name="T">Type of data to read from the index buffer.</typeparam>
    /// <param name="data">Buffer to hold data copied from the index buffer.</param>
    /// <param name="offsetInBytes">Optional offset from the start of the index buffer at which to start copying from</param>
    /// <exception cref="ObjectDisposedException">Thrown if the resource has already been disposed.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if an error occurs while reading from the resource.</exception>
    /// <remarks>See implementors for details on specific exceptions that can be thrown.</remarks>
    public void GetData<T>(Span<T> data, int offsetInBytes = 0) where T : unmanaged
    {
      CheckDisposed();

      try
      {
        IndexBufferImpl.GetData<T>(data, offsetInBytes);
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw GraphicsExceptionHelper.NewErrorReadingFromResourceException(typeof(IndexBuffer), e);
      }
    }

    /// <summary>
    /// Reads data from the index buffer into the data buffer
    /// </summary>
    /// <param name="data">Buffer to hold data copied from the index buffer.</param>
    /// <param name="startIndex">Starting index in the data buffer at which to start writing to.</param>
    /// <param name="elementCount">Number of indices to read.</param>
    /// <param name="offsetInBytes">Optional offset from the start of the index buffer at which to start copying from</param>
    /// <exception cref="ObjectDisposedException">Thrown if the resource has already been disposed.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if an error occurs while reading from the resource.</exception>
    /// <remarks>See implementors for details on specific exceptions that can be thrown.</remarks>
    public void GetData(IndexData data, int offsetInBytes = 0)
    {
      GetData(data, 0, (data.IsValid) ? data.Length : 0, offsetInBytes);
    }

    /// <summary>
    /// Reads data from the index buffer into the data buffer
    /// </summary>
    /// <param name="data">Buffer to hold data copied from the index buffer.</param>
    /// <param name="startIndex">Starting index in the data buffer at which to start writing to.</param>
    /// <param name="elementCount">Number of indices to read.</param>
    /// <param name="offsetInBytes">Optional offset from the start of the index buffer at which to start copying from</param>
    /// <exception cref="ObjectDisposedException">Thrown if the resource has already been disposed.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if an error occurs while reading from the resource.</exception>
    /// <remarks>See implementors for details on specific exceptions that can be thrown.</remarks>
    public void GetData(IndexData data, int startIndex, int elementCount, int offsetInBytes = 0)
    {
      CheckDisposed();

      try
      {
        switch (data.IndexFormat)
        {
          case IndexFormat.ThirtyTwoBits:
            IndexBufferImpl.GetData<int>(data.Span32.Slice(startIndex, elementCount), offsetInBytes);
            break;
          case IndexFormat.SixteenBits:
            IndexBufferImpl.GetData<ushort>(data.Span16.Slice(startIndex, elementCount), offsetInBytes);
            break;
        }
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw GraphicsExceptionHelper.NewErrorReadingFromResourceException(typeof(IndexBuffer), e);
      }
    }

    /// <summary>
    /// Writes data from the data buffer into the index buffer.
    /// </summary>
    /// <param name="renderContext">The current render context.</param>
    /// <param name="data">Buffer with the data to be copied to the index buffer.</param>
    /// <param name="writeOptions">Optional write options, used only if this is a dynamic buffer. None, discard, no overwrite</param>
    /// <exception cref="ObjectDisposedException">Thrown if the resource has already been disposed.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if an error occurs while writing to the resource, or if trying to write to a non-DYNAMIC resource 
    /// using a deferred render context.</exception>
    /// <remarks>See implementors for details on specific exceptions that can be thrown.</remarks>
    public void SetData(IRenderContext renderContext, IndexData data, DataWriteOptions writeOptions = DataWriteOptions.Discard)
    {
      SetData(renderContext, data, 0, (data.IsValid) ? data.Length : 0, 0, writeOptions);
    }

    /// <summary>
    /// Writes data from the data buffer into the index buffer.
    /// </summary>
    /// <param name="renderContext">The current render context.</param>
    /// <param name="data">Buffer with the data to be copied to the index buffer.</param>
    /// <param name="startIndex">Starting index in the data buffer at which to start copying from</param>
    /// <param name="elementCount">Number of indices to write.</param>
    /// <param name="writeOptions">Optional write options, used only if this is a dynamic buffer. None, discard, no overwrite</param>
    /// <exception cref="ObjectDisposedException">Thrown if the resource has already been disposed.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if an error occurs while writing to the resource, or if trying to write to a non-DYNAMIC resource 
    /// using a deferred render context.</exception>
    /// <remarks>See implementors for details on specific exceptions that can be thrown.</remarks>
    public void SetData(IRenderContext renderContext, IndexData data, int startIndex, int elementCount, DataWriteOptions writeOptions = DataWriteOptions.Discard)
    {
      SetData(renderContext, data, startIndex, elementCount, 0, writeOptions);
    }

    /// <summary>
    /// Writes data from the data buffer into the index buffer.
    /// </summary>
    /// <param name="renderContext">The current render context.</param>
    /// <param name="data">Buffer with the data to be copied to the index buffer.</param>
    /// <param name="startIndex">Starting index in the data buffer at which to start copying from</param>
    /// <param name="elementCount">Number of indices to write.</param>
    /// <param name="offsetInBytes">Optional offset from the start of the index buffer at which to start writing at.</param>
    /// <param name="writeOptions">Optional write options, used only if this is a dynamic buffer. None, discard, no overwrite</param>
    /// <exception cref="ObjectDisposedException">Thrown if the resource has already been disposed.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if an error occurs while writing to the resource, or if trying to write to a non-DYNAMIC resource 
    /// using a deferred render context.</exception>
    /// <remarks>See implementors for details on specific exceptions that can be thrown.</remarks>
    public void SetData(IRenderContext renderContext, IndexData data, int startIndex, int elementCount, int offsetInBytes = 0, DataWriteOptions writeOptions = DataWriteOptions.Discard)
    {
      CheckDisposed();
      CheckDeferredSetDataIsPermitted(renderContext, ResourceUsage);

      try
      {
        switch (data.IndexFormat)
        {
          case IndexFormat.ThirtyTwoBits:
            IndexBufferImpl.SetData<int>(renderContext, data.Span32.Slice(startIndex, elementCount), offsetInBytes, writeOptions);
            break;
          case IndexFormat.SixteenBits:
            IndexBufferImpl.SetData<ushort>(renderContext, data.Span16.Slice(startIndex, elementCount), offsetInBytes, writeOptions);
            break;
        }
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw GraphicsExceptionHelper.NewErrorWritingToResourceException(typeof(IndexBuffer), e);
      }
    }

    /// <summary>
    /// Writes data from the data buffer into the index buffer.
    /// </summary>
    /// <typeparam name="T">Type of data to write to the index buffer.</typeparam>
    /// <param name="renderContext">The current render context.</param>
    /// <param name="data">Buffer with the data to be copied to the index buffer.</param>
    /// <param name="writeOptions">Optional write options, used only if this is a dynamic buffer. None, discard, no overwrite</param>
    /// <exception cref="ObjectDisposedException">Thrown if the resource has already been disposed.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if an error occurs while writing to the resource, or if trying to write to a non-DYNAMIC resource 
    /// using a deferred render context.</exception>
    /// <remarks>See implementors for details on specific exceptions that can be thrown.</remarks>
    public void SetData<T>(IRenderContext renderContext, ReadOnlySpan<T> data, DataWriteOptions writeOptions = DataWriteOptions.Discard) where T : unmanaged
    {
      SetData<T>(renderContext, data, 0, writeOptions);
    }

    /// <summary>
    /// Writes data from the data buffer into the index buffer.
    /// </summary>
    /// <typeparam name="T">Type of data to write to the index buffer.</typeparam>
    /// <param name="renderContext">The current render context.</param>
    /// <param name="data">Buffer with the data to be copied to the index buffer.</param>
    /// <param name="offsetInBytes">Offset from the start of the index buffer at which to start writing at</param>
    /// <param name="writeOptions">Optional write options, used only if this is a dynamic buffer. None, discard, no overwrite</param>
    /// <exception cref="ObjectDisposedException">Thrown if the resource has already been disposed.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if an error occurs while writing to the resource, or if trying to write to a non-DYNAMIC resource 
    /// using a deferred render context.</exception>
    /// <remarks>See implementors for details on specific exceptions that can be thrown.</remarks>
    public void SetData<T>(IRenderContext renderContext, ReadOnlySpan<T> data, int offsetInBytes, DataWriteOptions writeOptions = DataWriteOptions.Discard) where T : unmanaged
    {
      CheckDisposed();
      CheckDeferredSetDataIsPermitted(renderContext, ResourceUsage);

      try
      {
        IndexBufferImpl.SetData<T>(renderContext, data, offsetInBytes, writeOptions);
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw GraphicsExceptionHelper.NewErrorWritingToResourceException(typeof(IndexBuffer), e);
      }
    }

    #endregion

    #region ISavable Methods

    /// <summary>
    /// Reads the object data from the input.
    /// </summary>
    /// <param name="input">Savable reader</param>
    public void Read(ISavableReader input)
    {
      IRenderSystem renderSystem = GraphicsHelper.GetRenderSystem(input.ServiceProvider);

      Name = input.ReadString("Name");
      int indexCount = input.ReadInt32("IndexCount");
      IndexFormat indexFormat = input.ReadEnum<IndexFormat>("IndexFormat");
      ResourceUsage usage = input.ReadEnum<ResourceUsage>("ResourceUsage");
      ResourceBindFlags bindFlags = input.ReadEnum<ResourceBindFlags>("ResourceBindFlags");

      using (IndexData indexData = input.ReadIndexDataBlob("IndexData", MemoryAllocatorStrategy.DefaultPooled))
        CreateImplementation(renderSystem, indexFormat, indexCount, BufferOptions.Init(indexData, usage, bindFlags));
    }

    /// <summary>
    /// Writes the object data to the output.
    /// </summary>
    /// <param name="output">Savable writer</param>
    public void Write(ISavableWriter output)
    {
      int indexCount = IndexCount;
      IndexFormat format = IndexFormat;

      output.Write("Name", Name);
      output.Write("IndexCount", indexCount);
      output.WriteEnum<IndexFormat>("IndexFormat", format);
      output.WriteEnum<ResourceUsage>("ResourceUsage", ResourceUsage);
      output.WriteEnum<ResourceBindFlags>("ResourceBindFlags", ResourceBindFlags);

      // Allocate pooled memory to write out data
      using (IndexData data = new IndexData(indexCount, format, MemoryAllocatorStrategy.DefaultPooled))
      {
        GetData<byte>(data.Bytes);

        output.WriteIndexDataBlob("IndexData", data);
      }
    }

    #endregion

    #region Creation Parameter Validation

    /// <summary>
    /// Validates creation parameters.
    /// </summary>
    /// <param name="indexCount">The number of indices the buffer will contain.</param>
    /// <exception cref="System.ArgumentOutOfRangeException">Thrown if the index count is less than or equal to zero.</exception>
    protected void ValidateCreationParameters(int indexCount)
    {
      if (indexCount <= 0)
        throw new ArgumentOutOfRangeException(nameof(indexCount), StringLocalizer.Instance.GetLocalizedString("ResourceSizeMustBeGreaterThanZero"));
    }

    /// <summary>
    /// Validates creation parameters.
    /// </summary>
    /// <param name="indexFormat">The index format.</param>
    /// <param name="indexCount">Number of indices the buffer will contain.</param>
    /// <param name="options">Common <see cref="BufferOptions"/> that may also contain initial data.</param>
    /// <exception cref="ArgumentNullException">Thrown if the index data is null.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the data does not match the index format.</exception>
    protected void ValidateCreationParameters(IndexFormat indexFormat, int indexCount, BufferOptions options)
    {
      ValidateCreationParameters(indexCount);

      if (options.Data is null)
      {
        if (options.ResourceUsage == ResourceUsage.Immutable)
          throw GraphicsExceptionHelper.NewMustSupplyDataForImmutableException();

        return;
      }

      IReadOnlyDataBuffer data = options.Data;
      if (data.Length == 0)
        throw new ArgumentNullException(nameof(options.Data), StringLocalizer.Instance.GetLocalizedString("DataBufferNull"));

      int totalSize = data.SizeInBytes;
      int indexSize = indexFormat.SizeInBytes();

      // Format size should divide evenly into the total buffer size
      if (totalSize % indexSize != 0 || BufferHelper.ComputeElementCount(totalSize, indexSize) != indexCount)
        throw new ArgumentOutOfRangeException(nameof(options.Data), StringLocalizer.Instance.GetLocalizedString("IndexFormatMismatch"));
    }

    #endregion

    #region Implementation Creation

    private void CreateImplementation(IRenderSystem renderSystem, IndexFormat indexFormat, int indexCount, BufferOptions options)
    {
      GraphicsHelper.ThrowIfRenderSystemNull(renderSystem);

      ValidateCreationParameters(indexFormat, indexCount, options);

      IIndexBufferImplFactory factory;
      if (!renderSystem.TryGetImplementationFactory<IIndexBufferImplFactory>(out factory))
        throw GraphicsExceptionHelper.NewUnsupportedFeatureException(renderSystem, typeof(IndexBuffer));

      try
      {
        IndexBufferImpl = factory.CreateImplementation(indexFormat, indexCount, options);
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw GraphicsExceptionHelper.NewErrorCreatingImplementation(typeof(IndexBuffer), e);
      }
    }

    #endregion
  }
}
