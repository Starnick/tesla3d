﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// Defines common buffer creation options.
  /// </summary>
  public ref struct BufferOptions
  {
    /// <summary>
    /// Resource usage specifying the type of memory the buffer should use. If <see cref="ResourceUsage.Immutable"/> then initial data
    /// is required as the buffer cannot be modified once created. Default is <see cref="ResourceUsage.Static"/>.
    /// </summary>
    public ResourceUsage ResourceUsage;

    /// <summary>
    /// Resource binding flags specifying in what other ways the buffer can be bound to the graphics pipeline. Default is <see cref="ResourceBindFlags.Default"/>.
    /// </summary>
    public ResourceBindFlags ResourceBindFlags;

    /// <summary>
    /// Initial data to populate the buffer. It may be null except in the case where <see cref="ResourceUsage.Immutable"/> is used, as initial data is required to exist. Default is null.
    /// </summary>
    public IReadOnlyDataBuffer? Data;

    /// <summary>
    /// Constructs a new instance of the <see cref="BufferOptions"/> struct.
    /// </summary>
    /// <param name="usage">Resource usage specifying the type of memory the buffer should use.</param>
    /// <param name="bindFlags">Resource binding flags specifying in what other ways the buffer can be bound to the graphics pipeline.</param>
    /// <param name="data">Data to initialize the buffer with. If the buffer is immutable, this is required, but may be empty for other usage types.</param>
    public BufferOptions(ResourceUsage usage, ResourceBindFlags bindFlags, IReadOnlyDataBuffer? data)
    {
      ResourceUsage = usage;
      ResourceBindFlags = bindFlags;
      Data = data;
    }

    /// <summary>
    /// Creates default buffer options for an empty buffer.
    /// </summary>
    /// <param name="usage">Optional resource usage. If <see cref="ResourceUsage.Immutable"/> then initial data must is required as the buffer cannot be modified once created.</param>
    /// <param name="bindFlags">Optional binding flags.</param>
    /// <returns>Buffer options.</returns>
    public static BufferOptions Init(ResourceUsage usage = ResourceUsage.Static, ResourceBindFlags bindFlags = ResourceBindFlags.Default)
    {
      return new BufferOptions(usage, bindFlags, null);
    }

    /// <summary>
    /// Creates default buffer options for a buffer with initial data.
    /// </summary>
    /// <param name="data">Data to initialize the buffer with.</param>
    /// <param name="usage">Optional resource usage. If <see cref="ResourceUsage.Immutable"/> then initial data must is required as the buffer cannot be modified once created.</param>
    /// <param name="bindFlags">Optional binding flags.</param>
    /// <returns>Buffer options.</returns>
    public static BufferOptions Init(in IReadOnlyDataBuffer data, ResourceUsage usage = ResourceUsage.Static, ResourceBindFlags bindFlags = ResourceBindFlags.Default)
    {
      return new BufferOptions(usage, bindFlags, data);
    }

    /// <summary>
    /// Creates default buffer options for a buffer with initial data.
    /// </summary>
    /// <param name="data">Data to initialize the buffer with.</param>
    /// <param name="usage">Optional resource usage. If <see cref="ResourceUsage.Immutable"/> then initial data must is required as the buffer cannot be modified once created.</param>
    /// <param name="bindFlags">Resource bind flags specifying in what other ways the buffer can be bound to the graphics pipeline.</param>
    /// <returns>Buffer options.</returns>
    public static BufferOptions Init(in IReadOnlyDataBuffer data, ResourceBindFlags bindFlags)
    {
      return new BufferOptions(ResourceUsage.Static, bindFlags, data);
    }

    /// <summary>
    /// Creates default buffer options for index data.
    /// </summary>
    /// <param name="indices">Data to initialize the buffer with.</param>
    /// <param name="usage">Optional resource usage. If <see cref="ResourceUsage.Immutable"/> then initial data must is required as the buffer cannot be modified once created.</param>
    /// <param name="bindFlags">Optional binding flags.</param>
    /// <returns>Buffer options.</returns>
    public static BufferOptions Init(in IndexData indices, ResourceUsage usage = ResourceUsage.Static, ResourceBindFlags bindFlags = ResourceBindFlags.Default)
    {
      if (!indices.IsValid)
        return new BufferOptions(usage, bindFlags, null);

      return new BufferOptions(usage, bindFlags, indices.UnderlyingDataBuffer);
    }

    /// <summary>
    /// Creates default buffer options for index data.
    /// </summary>
    /// <param name="indices">Data to initialize the buffer with.</param>
    /// <param name="bindFlags">Resource binding flags specifying in what other ways the buffer can be bound to the graphics pipeline.</param>
    /// <returns>Buffer options.</returns>
    public static BufferOptions Init(in IndexData indices, ResourceBindFlags bindFlags)
    {
      return Init(indices, ResourceUsage.Static, bindFlags);
    }

    /// <summary>
    /// Convience conversion operator to create a default buffer options with the specified usage.
    /// </summary>
    /// <param name="usage">Resource usage.</param>
    public static implicit operator BufferOptions(ResourceUsage usage)
    {
      return Init(usage);
    }

    /// <summary>
    /// Convience conversion operator to create a default buffer options with the specified binding flags.
    /// </summary>
    /// <param name="bindFlags">Resource binding flags specifying in what other ways the buffer can be bound to the graphics pipeline.</param>
    public static implicit operator BufferOptions(ResourceBindFlags bindFlags)
    {
      return Init(ResourceUsage.Static, bindFlags);
    }
  }
}
