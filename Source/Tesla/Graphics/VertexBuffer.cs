﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Content;
using Tesla.Graphics.Implementation;

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// Represents a buffer of vertices that exists on the GPU. A vertex is made up of individual attributes such as a position
  /// and other properties like colors, texture coordinates, or normals.
  /// </summary>
  [SavableVersion(1)]
  public class VertexBuffer : GraphicsResource, ISavable, IBufferResource
  {

    #region Public Properties

    /// <summary>
    /// Gets the vertex layout that describes the structure of vertex data contained in the buffer.
    /// </summary>
    public VertexLayout VertexLayout
    {
      get
      {
        return VertexBufferImpl.VertexLayout;
      }
    }

    /// <summary>
    /// Gets the number of vertices contained in the buffer.
    /// </summary>
    public int VertexCount
    {
      get
      {
        return VertexBufferImpl.ElementCount;
      }
    }

    /// <summary>
    /// Gets the total size in bytes of the buffer.
    /// </summary>
    public int SizeInBytes
    {
      get
      {
        return VertexBufferImpl.ElementStride * VertexBufferImpl.ElementCount;
      }
    }

    /// <summary>
    /// Gets the resource usage of the buffer.
    /// </summary>
    public ResourceUsage ResourceUsage
    {
      get
      {
        return VertexBufferImpl.ResourceUsage;
      }
    }

    /// <summary>
    /// Gets the resource binding of the buffer.
    /// </summary>
    public ResourceBindFlags ResourceBindFlags
    {
      get
      {
        return VertexBufferImpl.ResourceBindFlags;
      }
    }

    /// <summary>
    /// Gets the shader resource type.
    /// </summary>
    public ShaderResourceType ResourceType
    {
      get
      {
        return ShaderResourceType.Buffer;
      }
    }

    #endregion

    //For casting purposes
    private IVertexBufferImpl VertexBufferImpl
    {
      get
      {
        return GetImplAs<IVertexBufferImpl>();
      }
      set
      {
        BindImplementation(value);
      }
    }

    /// <inheritdoc />
    int IBufferResource.ElementCount { get { return VertexBufferImpl.ElementCount; } }

    /// <inheritdoc />
    int IBufferResource.ElementStride { get { return VertexBufferImpl.ElementStride; } }


    /// <summary>
    /// For ISavable only.
    /// </summary>
    protected VertexBuffer() { }

    /// <summary>
    /// Constructs a new instance of the <see cref="VertexBuffer"/> class.
    /// </summary>
    /// <param name="renderSystem">Render system used to create the underlying implementation.</param>
    /// <param name="vertexLayout">Vertex layout that defines the vertex data this buffer will contain.</param>
    /// <param name="vertexCount">Number of vertices the buffer will contain</param>
    /// <param name="options">Common <see cref="VertexBufferOptions"/> that may also contain initial data.</param>
    /// <remarks>
    /// The initial data is allowed to be a single interleaved buffer or many vertex attribute buffers. If the latter, the number of buffers
    /// must be 1:1 to the vertex layout and all have the same number of elements.
    /// </remarks>
    /// <exception cref="System.ArgumentNullException">Thrown if the render system is null or if any input is null when it is required (e.g. vertex layout or initial data is required for immutable resources).</exception>
    /// <exception cref="System.ArgumentOutOfRangeException">Thrown if the vertex count is less than or equal to zero.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if creating the underlying implementation fails or is unsupported, see inner exception for a more detailed error.</exception>
    public VertexBuffer(IRenderSystem renderSystem, VertexLayout vertexLayout, int vertexCount, VertexBufferOptions options = default)
    {
      CreateImplementation(renderSystem, vertexLayout, vertexCount, options);
    }


    /// <summary>
    /// Constructs a new instance of the <see cref="VertexBuffer"/> class. The options MUST contain at least one initial data buffer. The vertex
    /// count will be automatically computed from the contents.
    /// </summary>
    /// <param name="renderSystem">Render system used to create the underlying implementation.</param>
    /// <param name="vertexLayout">Vertex layout that defines the vertex data of this buffer</param>
    /// <param name="options">Common <see cref="VertexBufferOptions"/>. Initial data MUST be populated and may be raw byte buffers.</param>
    /// <remarks>
    /// <para>If the initial data is a single buffer, the vertex count is determined by total byte size / vertex layout stride.</para>
    /// <para>If the initial data has more than one buffer, it is expected each represents a single vertex attribute. The number of buffers must match 1:1 to the 
    /// vertex layout. The vertex count is determined by the first buffer's total byte size / first vertex layout element format size.</para>
    /// </remarks>
    /// <exception cref="System.ArgumentNullException">Thrown if the render system is null or if any input is null when it is required (e.g. vertex layout or initial data is required for immutable resources).</exception>
    /// <exception cref="System.ArgumentOutOfRangeException">Thrown if the layout's vertex stride is not the same as the data's vertex stride.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if creating the underlying implementation fails or is unsupported, see inner exception for a more detailed error.</exception>
    public VertexBuffer(IRenderSystem renderSystem, VertexLayout vertexLayout, VertexBufferOptions options = default)
    {
      int vertexCount = GraphicsHelper.CalculateVertexCount(vertexLayout, options.Data);
      CreateImplementation(renderSystem, vertexLayout, vertexCount, options);
    }

    #region Public Methods

    /// <summary>
    /// Reads interleaved vertex data from the vertexbuffer and stores it in a span of data buffers. Each data buffer represents
    /// a single vertex element (in the order declared by the vertex layout) and must all have the same number of elements and match
    /// the vertex layout in format size. The number of elements needs to match the number of vertex elements in the vertex buffer.
    /// </summary>
    /// <param name="data">Span of buffers representing each vertex attribute that will contain data read from the vertex buffer.</param>
    /// <exception cref="ObjectDisposedException">Thrown if the resource has already been disposed.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the buffers do not match the vertex layout or size.</exception>
    /// <exception cref="ArgumentNullException">Thrown if any of the buffers are null.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if an error occurs while reading from the resource.</exception>
    /// <remarks>See implementors for details on specific exceptions that can be thrown.</remarks>
    public void GetInterleavedData(ReadOnlySpan<IReadOnlyDataBuffer> data)
    {
      CheckDisposed();
      GraphicsHelper.ValidateInterleavedData(VertexLayout, data, VertexCount);

      try
      {
        VertexBufferImpl.GetInterleavedData(data);
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw GraphicsExceptionHelper.NewErrorReadingFromResourceException(GetType(), e);
      }
    }

    /// <summary>
    /// Writes interleaved vertex data from an array of data buffers into the vertex buffer. Each data buffer represents
    /// a single vertex element (in the order declared by the vertex layout) and must all have the same number of elements and match
    /// the vertex layout in format size. The number of elements needs to match the number of vertex elements in the vertex buffer. For dynamic
    /// vertex buffers, this always uses the <see cref="DataWriteOptions.Discard"/> flag.
    /// </summary>
    /// <param name="renderContext">The current render context.</param> 
    /// <param name="data">Span of buffers representing each vertex attribute whose data will be written to the vertex buffer.</param>
    /// <exception cref="ObjectDisposedException">Thrown if the resource has already been disposed.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the buffers do not match the vertex layout or size.</exception>
    /// <exception cref="ArgumentNullException">Thrown if any of the buffers are null.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if an error occurs while writing to the vertex buffer, or if trying to write to a non-DYNAMIC resource 
    /// using a deferred render context.</exception>
    /// <remarks>See implementors for details on specific exceptions that can be thrown.</remarks>
    public void SetInterleavedData(IRenderContext renderContext, ReadOnlySpan<IReadOnlyDataBuffer> data)
    {
      CheckDisposed();
      CheckDeferredSetDataIsPermitted(renderContext, ResourceUsage);
      GraphicsHelper.ValidateInterleavedData(VertexLayout, data, VertexCount);

      try
      {
        VertexBufferImpl.SetInterleavedData(renderContext, data);
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw GraphicsExceptionHelper.NewErrorWritingToResourceException(GetType(), e);
      }
    }

    /// <summary>
    /// Reads data from the vertex buffer into the specified buffer.
    /// </summary>
    /// <typeparam name="T">Type of data to read from the vertex buffer.</typeparam>
    /// <param name="data">Buffer to hold data copied from the vertex buffer.</param>
    /// <param name="offsetInBytes">Optional offset in bytes from the beginning of the vertex buffer to the data.</param>
    /// <param name="vertexStride">Optional vertex step size used to advance within the GPU buffer when iterating over individual vertex attributes. Specify 0 or the size specified in the vertex layout for most copy operations.</param>
    /// <exception cref="ObjectDisposedException">Thrown if the resource has already been disposed.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if an error occurs while reading from the resource.</exception>
    /// <remarks>See implementors for details on specific exceptions that can be thrown.</remarks>
    public void GetData<T>(Span<T> data, int offsetInBytes = 0, int vertexStride = 0) where T : unmanaged
    {
      CheckDisposed();

      try
      {
        VertexBufferImpl.GetData<T>(data, offsetInBytes, vertexStride);
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw GraphicsExceptionHelper.NewErrorReadingFromResourceException(GetType(), e);
      }
    }

    /// <inheritdoc />
    void IBufferResource.GetData<T>(Span<T> data, int offsetInBytes)
    {
      GetData<T>(data, offsetInBytes, 0);
    }

    /// <summary>
    /// Writes data to the vertex buffer from the specified buffer.
    /// </summary>
    /// <typeparam name="T">Type of data to write to the vertex buffer.</typeparam>
    /// <param name="renderContext">The current render context.</param>
    /// <param name="data">Buffer with the data to be copied to the vertex buffer.</param>
    /// <param name="writeOptions">Optional writing options, valid only for dynamic vertex buffers.</param>
    /// <exception cref="ObjectDisposedException">Thrown if the resource has already been disposed.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if an error occurs while writing to the resource, or if trying to write to a non-DYNAMIC resource 
    /// using a deferred render context.</exception>
    /// <remarks>See implementors for details on specific exceptions that can be thrown.</remarks>
    public void SetData<T>(IRenderContext renderContext, ReadOnlySpan<T> data, DataWriteOptions writeOptions = DataWriteOptions.Discard) where T : unmanaged
    {
      SetData<T>(renderContext, data, 0, 0, writeOptions);
    }

    /// <summary>
    /// Writes data to the vertex buffer from the specified buffer.
    /// </summary>
    /// <typeparam name="T">Type of data to write to the vertex buffer.</typeparam>
    /// <param name="renderContext">The current render context.</param>
    /// <param name="data">Buffer with the data to be copied to the vertex buffer.</param>
    /// <param name="offsetInBytes">Offset in bytes from the beginning of the vertex buffer to the data.</param>
    /// <param name="vertexStride">Optional vertex step size used to advance within the GPU buffer when iterating over individual vertex attributes. Specify 0 or the size specified in the vertex layout for most copy operations.</param>
    /// <param name="writeOptions">Optional writing options, valid only for dynamic vertex buffers.</param>
    /// <exception cref="ObjectDisposedException">Thrown if the resource has already been disposed.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if an error occurs while writing to the resource, or if trying to write to a non-DYNAMIC resource 
    /// using a deferred render context.</exception>
    /// <remarks>See implementors for details on specific exceptions that can be thrown.</remarks>
    public void SetData<T>(IRenderContext renderContext, ReadOnlySpan<T> data, int offsetInBytes, int vertexStride = 0, DataWriteOptions writeOptions = DataWriteOptions.Discard) where T : unmanaged
    {
      CheckDisposed();
      CheckDeferredSetDataIsPermitted(renderContext, ResourceUsage);

      try
      {
        VertexBufferImpl.SetData<T>(renderContext, data, offsetInBytes, vertexStride, writeOptions);
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw GraphicsExceptionHelper.NewErrorWritingToResourceException(GetType(), e);
      }
    }

    /// <inheritdoc />
    void IBufferResource.SetData<T>(IRenderContext renderContext, ReadOnlySpan<T> data, int offsetInBytes, DataWriteOptions writeOptions)
    {
      SetData<T>(renderContext, data, offsetInBytes, 0, writeOptions);
    }

    #endregion

    #region ISavable Methods

    /// <summary>
    /// Reads the specified input.
    /// </summary>
    /// <param name="input">The input.</param>
    public void Read(ISavableReader input)
    {
      IRenderSystem renderSystem = GraphicsHelper.GetRenderSystem(input.ServiceProvider);

      Name = input.ReadString("Name");
      VertexLayout vertexLayout = input.ReadSavable<VertexLayout>("VertexLayout");
      int vertexCount = input.ReadInt32("VertexCount");
      ResourceUsage usage = input.ReadEnum<ResourceUsage>("ResourceUsage");
      ResourceBindFlags bindFlags = input.ReadEnum<ResourceBindFlags>("ResourceBindFlags");

      // Create a pooled buffer for the data
      using (DataBuffer<byte> data = input.ReadBlob<byte>("VertexData", MemoryAllocatorStrategy.DefaultPooled))
        CreateImplementation(renderSystem, vertexLayout, vertexCount, VertexBufferOptions.Init(data, usage, bindFlags));
    }

    /// <summary>
    /// Writes the object data to the output.
    /// </summary>
    /// <param name="output">Savable writer</param>
    public void Write(ISavableWriter output)
    {
      int vertexCount = VertexCount;
      VertexLayout vertexLayout = VertexLayout;

      output.Write("Name", Name);
      output.WriteSavable<VertexLayout>("VertexLayout", vertexLayout);
      output.Write("VertexCount", vertexCount);
      output.WriteEnum<ResourceUsage>("ResourceUsage", ResourceUsage);
      output.WriteEnum<ResourceBindFlags>("ResourceBindFlags", ResourceBindFlags);

      // Create a pooled buffer for the data
      using (DataBuffer<byte> data = DataBuffer.Create<byte>(vertexLayout.VertexStride * vertexCount, MemoryAllocatorStrategy.DefaultPooled))
      {
        GetData<byte>(data.Span);

        output.WriteBlob<byte>("VertexData", data.Span);
      }
    }

    #endregion

    #region Creation Parameter Validation

    /// <summary>
    /// Validates creation parameters.
    /// </summary>
    /// <param name="vertexLayout">The vertex layout that describes the data.</param>
    /// <param name="vertexCount">The number of vertices the buffer will contain.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the vertex layout is null.</exception>
    /// <exception cref="System.ArgumentOutOfRangeException">Thrown if the vertex count is less than or equal to zero.</exception>
    protected void ValidateCreationParameters(VertexLayout vertexLayout, int vertexCount)
    {
      if (vertexLayout is null)
        throw new ArgumentNullException(nameof(vertexLayout), StringLocalizer.Instance.GetLocalizedString("VertexLayoutNull"));

      if (vertexCount <= 0)
        throw new ArgumentOutOfRangeException(nameof(vertexCount), StringLocalizer.Instance.GetLocalizedString("ResourceSizeMustBeGreaterThanZero"));
    }

    /// <summary>
    /// Validates creation parameters.
    /// </summary>
    /// <param name="vertexLayout">The vertex layout that describes the data.</param>
    /// <param name="vertexCount">The number of vertices the buffer will contain.</param>
    /// <param name="options">Common <see cref="VertexBufferOptions"/> that may also contain initial data, interleaved or multiple vertex attributes.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if either data or vertex layout are null.</exception>
    /// <exception cref="System.ArgumentOutOfRangeException">Thrown if the layout's vertex stride is not
    /// the same as the data's vertex stride, or the number of data buffers do not match the number of declaraed vertex elements, or the
    /// data buffers are not all the same length.</exception>
    protected void ValidateCreationParameters(VertexLayout vertexLayout, int vertexCount, VertexBufferOptions options)
    {
      ValidateCreationParameters(vertexLayout, vertexCount);

      if (options.Data.IsEmpty)
      {
        if (options.ResourceUsage == ResourceUsage.Immutable)
          throw GraphicsExceptionHelper.NewMustSupplyDataForImmutableException();

        return;
      }

      GraphicsHelper.ValidateInterleavedData(vertexLayout, options.Data, vertexCount);
    }

    #endregion

    #region Implementation Creation

    private void CreateImplementation(IRenderSystem renderSystem, VertexLayout vertexLayout, int vertexCount, VertexBufferOptions options)
    {
      GraphicsHelper.ThrowIfRenderSystemNull(renderSystem);

      ValidateCreationParameters(vertexLayout, vertexCount, options);

      IVertexBufferImplFactory factory;
      if (!renderSystem.TryGetImplementationFactory<IVertexBufferImplFactory>(out factory))
        throw GraphicsExceptionHelper.NewUnsupportedFeatureException(renderSystem, typeof(VertexBuffer));

      try
      {
        VertexBufferImpl = factory.CreateImplementation(vertexLayout, vertexCount, options);
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw GraphicsExceptionHelper.NewErrorCreatingImplementation(typeof(VertexBuffer), e);
      }
    }

    #endregion
  }
}
