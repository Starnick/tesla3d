﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Content;

namespace Tesla.Graphics
{
    /// <summary>
    /// Base representation for a light. Contains common color and attenuation properties shared by most types of lights.
    /// </summary>
    [SavableVersion(1)]
    public abstract class Light : ISavable
    {
        private TypedEvent<Light> m_lightChangedEvent;

        /// <summary>
        /// Occurs when the light property changes.
        /// </summary>
        public event TypedEventHandler<Light> LightChanged
        {
            add
            {
                m_lightChangedEvent.Add(value);
            }
            remove
            {
                m_lightChangedEvent.Remove(value);
            }
        }

        private String m_name;
        private Color m_ambient;
        private Color m_diffuse;
        private Color m_specular;
        private float m_range;
        private float m_intensity;
        private bool m_attenuate;
        private bool m_isEnabled;
        private int m_id; //Do not serialize
        private Object m_tag; //Do not serialize

        /// <summary>
        /// Gets or sets the name of this light.
        /// </summary>
        public String Name
        {
            get
            {
                return m_name;
            }
            set
            {
                m_name = value;
                OnLightChanged();
            }
        }

        /// <summary>
        /// Gets or sets the (non-persistent) ID for this light. This is used by various light management systems to track the light source.
        /// </summary>
        public int ID
        {
            get
            {
                return m_id;
            }
            set
            {
                m_id = value;
            }
        }

        /// <summary>
        /// Gets or sets custom data (non-persistent) for this light.
        /// </summary>
        public Object Tag
        {
            get
            {
                return m_tag;
            }
            set
            {
                m_tag = value;
            }
        }


        /// <summary>
        /// Gets or sets the ambient contribution of this light,
        /// which is the color that appears to be coming from everywhere. Default is
        /// Color(.4f, .4f, .4f, 1.0f).
        /// </summary>
        public Color Ambient
        {
            get
            {
                return m_ambient;
            }
            set
            {
                m_ambient = value;
                OnLightChanged();
            }
        }

        /// <summary>
        /// Gets or sets the diffuse contribution of this light,
        /// which is the color directly from the light source. Default is <see cref="Color.White"/>.
        /// </summary>
        public Color Diffuse
        {
            get
            {
                return m_diffuse;
            }
            set
            {
                m_diffuse = value;
                OnLightChanged();
            }
        }

        /// <summary>
        /// Gets or sets the specular contribution of this light,
        /// which is the color reflected by an object depending on
        /// how shiny it is. Default is <see cref="Color.White"/>;
        /// </summary>
        public Color Specular
        {
            get
            {
                return m_specular;
            }
            set
            {
                m_specular = value;
                OnLightChanged();
            }
        }

        /// <summary>
        /// Gets or sets the range of the light, used for attenuation. Default is <see cref="float.MaxValue"/>.
        /// </summary>
        public float Range
        {
            get
            {
                return m_range;
            }
            set
            {
                m_range = value;
                OnLightChanged();
            }
        }

        /// <summary>
        /// Gets or sets the intensity of the light energy, used to scale the light contribution. Default value is 1.0.
        /// </summary>
        public float Intensity
        {
            get
            {
                return m_intensity;
            }
            set
            {
                m_intensity = (value < 0) ? 0.0f : value;
                OnLightChanged();
            }
        }

        /// <summary>
        /// Gets or sets if this light should attenuate. Some lights may choose to
        /// ignore this (e.g. Directional lights should never attenuate). Default is false.
        /// </summary>
        public bool Attenuate
        {
            get
            {
                return m_attenuate;
            }
            set
            {
                m_attenuate = value;
                OnLightChanged();
            }
        }

        /// <summary>
        /// Gets or sets if this light should be enabled or not. Default is true.
        /// </summary>
        public bool IsEnabled
        {
            get
            {
                return m_isEnabled;
            }
            set
            {
                m_isEnabled = value;
                OnLightChanged();
            }
        }

        /// <summary>
        /// Gets the light type for this light.
        /// </summary>
        public abstract LightType LightType
        {
            get;
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="Light"/> class.
        /// </summary>
        public Light() : this(null) { }

        /// <summary>
        /// Constructs a new instance of the <see cref="Light"/> class.
        /// </summary>
        /// <param name="name">Name of the light</param>
        public Light(String name)
        {
            m_name = (String.IsNullOrEmpty(name)) ? String.Format("{0} Light", LightType.ToString()) : name;
            m_lightChangedEvent = new TypedEvent<Light>();
            m_ambient = new Color(0.4f, 0.4f, 0.4f, 1.0f);
            m_diffuse = Color.White;
            m_specular = Color.White;
            m_range = float.MaxValue;
            m_intensity = 1.0f;
            m_attenuate = false;
            m_isEnabled = true;
            m_id = -1;
            m_tag = null;
        }

        /// <summary>
        /// Clones the light.
        /// </summary>
        /// <returns>Copy of the light.</returns>
        public abstract Light Clone();

        /// <summary>
        /// Copies properties of the specified light.
        /// </summary>
        /// <param name="l">Light to copy propreties from</param>
        public void Set(Light l)
        {
            if (l == null)
                return;

            m_name = l.m_name;
            m_ambient = l.m_ambient;
            m_diffuse = l.m_diffuse;
            m_specular = l.m_specular;
            m_range = l.m_range;
            m_intensity = l.m_intensity;
            m_attenuate = l.m_attenuate;
            m_isEnabled = l.m_isEnabled;

            OnSet(l);
            OnLightChanged();
        }

        /// <summary>
        /// Sets all the color properties of the light.
        /// </summary>
        /// <param name="ambient">Ambient color.</param>
        /// <param name="diffuse">Diffuse color.</param>
        /// <param name="specular">Specular color.</param>
        public void SetColor(Color ambient, Color diffuse, Color specular)
        {
            m_ambient = ambient;
            m_diffuse = diffuse;
            m_specular = specular;
            OnLightChanged();
        }

        /// <summary>
        /// Sets the attenuation properties of the light.
        /// </summary>
        /// <param name="range">Distance from the light source at which the light influences.</param>
        /// <param name="attenuate">True if the light should attenuate, false otherwise.</param>
        public void SetAttenuation(float range, bool attenuate = true)
        {
            m_range = range;
            m_attenuate = attenuate;
            OnLightChanged();
        }

        /// <summary>
        /// Sets the attenuation properties of the light (approximately) based on the classic light attenuation function, where
        /// Attenuation = 1.0 / (constant + linear*D + quadratic*D*D) where D is distance from the light source to the object.
        /// </summary>
        /// <param name="constant">Constant parameter.</param>
        /// <param name="linear">Linear parameter.</param>
        /// <param name="quadratic">Quadratic parameter.</param>
        /// <param name="attenuate">True if the light should attenuate, false otherwise.</param>
        public void SetAttenuation(float constant, float linear, float quadratic, bool attenuate = true)
        {
            float result1, result2;
            if(MathHelper.SolveQuadraticEquation(quadratic, linear, constant - 70.0f, out result1, out result2))
                m_range = Math.Max(Math.Abs(result1), Math.Abs(result2));

            m_attenuate = attenuate;
            OnLightChanged();
        }

        /// <summary>
        /// Computes an attenuation factor (as it would in the standard shader library). Attenuation is between 0 and 1,
        /// where 0 is no effect on the object and 1 is the maximum intensity applied to the lighting.
        /// </summary>
        /// <param name="pos">Position of the object.</param>
        /// <param name="lightPos">Position of the light source.</param>
        /// <param name="lightRange">Range of the light.</param>
        /// <returns>Attenuation factor between 0 and 1.</returns>
        public static float ComputeAttenuationFactor(in Vector3 pos, in Vector3 lightPos, float lightRange)
        {
            float sqrDist = Vector3.DistanceSquared(lightPos, pos);

            float invRangeSquared = 0.0f;

            if(!MathHelper.IsNearlyZero(lightRange))
                invRangeSquared = 1.0f / (lightRange * lightRange);

            float atten = MathHelper.Clamp(1.0f - sqrDist * invRangeSquared, 0, 1);
            return atten * atten;
        }

        /// <summary>
        /// Reads the object data from the input.
        /// </summary>
        /// <param name="input">Savable reader</param>
        public virtual void Read(ISavableReader input)
        {
            m_name = input.ReadString("Name");
            input.Read<Color>("Ambient", out m_ambient);
            input.Read<Color>("Diffuse", out m_diffuse);
            input.Read<Color>("Specular", out m_specular);
            m_intensity = input.ReadSingle("Intensity");
            m_range = input.ReadSingle("AttenuationRange");
            m_attenuate = input.ReadBoolean("Attenuate");
            m_isEnabled = input.ReadBoolean("IsEnabled");
        }

        /// <summary>
        /// Writes the object data to the output.
        /// </summary>
        /// <param name="output">Savable writer</param>
        public virtual void Write(ISavableWriter output)
        {
            output.Write("Name", m_name);
            output.Write<Color>("Ambient", m_ambient);
            output.Write<Color>("Diffuse", m_diffuse);
            output.Write<Color>("Specular", m_specular);
            output.Write("Intensity", m_intensity);
            output.Write("AttenuationRange", m_range);
            output.Write("Attenuate", m_attenuate);
            output.Write("IsEnabled", m_isEnabled);
        }

        /// <summary>
        /// Notifies event handlers of light changes.
        /// </summary>
        protected virtual void OnLightChanged()
        {
            m_lightChangedEvent.Invoke(this);
        }

        /// <summary>
        /// Copies light properties.
        /// </summary>
        /// <param name="l">Light to copy from.</param>
        protected virtual void OnSet(Light l) { }
    }
}
