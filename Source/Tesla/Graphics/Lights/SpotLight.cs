﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Content;

namespace Tesla.Graphics
{
    /// <summary>
    /// Represents a light source that has a position in space but the influence of the light is restricted to a cone (opposed to a <see cref="PointLight"/> that is
    /// omni-directional). The cone is controlled by a direction and falloff angles to soften the edges.
    /// </summary>
    [SavableVersion(1)]
    public class SpotLight : Light
    {
        private Vector3 m_position;
        private Vector3 m_direction;
        private Angle m_innerAngle;
        private Angle m_outerAngle;

        /// <summary>
        /// Gets or sets the position of the light in space.
        /// 
        /// Default: (0, 0, 0)
        /// </summary>
        public Vector3 Position
        {
            get
            {
                return m_position;
            }
            set
            {
                m_position = value;
                OnLightChanged();
            }
        }

        /// <summary>
        /// Gets or sets the direction of the light. The value 
        /// is always normalized.
        /// 
        /// Default: (0, 0, -1)
        /// </summary>
        public Vector3 Direction
        {
            get
            {
                return m_direction;
            }
            set
            {
                m_direction = value;
                m_direction.Normalize();
                OnLightChanged();
            }
        }

        /// <summary>
        /// Gets or sets the innter cutoff angle in the range of [0, 180] for this light.
        /// This is the extent of the concentration of the light
        /// along the direction (the cone). This should always be smaller than
        /// the outer angle.
        /// 
        /// Default: 45 degrees
        /// </summary>
        public Angle InnerAngle
        {
            get
            {
                return m_innerAngle;
            }
            set
            {
                m_innerAngle = Angle.Clamp(value, Angle.Zero, Angle.Pi);
                OnLightChanged();
            }
        }

        /// <summary>
        /// Gest or sets the outer cutoff angle in the range of [0, 180] for this light. This
        /// is used to allow a soft blending from the main concentration
        /// of the light to the area outside of the spotlight's cone.
        /// 
        /// Default: 90 degrees
        /// </summary>
        public Angle OuterAngle
        {
            get
            {
                return m_outerAngle;
            }
            set
            {
                m_outerAngle = Angle.Clamp(value, Angle.Zero, Angle.Pi);
                OnLightChanged();
            }
        }

        /// <summary>
        /// Gets the light type for this light.
        /// </summary>
        public override LightType LightType
        {
            get
            {
                return LightType.Spot;
            }
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="SpotLight"/> class.
        /// </summary>
        public SpotLight() : this("SpotLight", Vector3.Zero, Vector3.Forward, Angle.PiOverFour, Angle.PiOverTwo) { }

        /// <summary>
        /// Constructs a new instance of the <see cref="SpotLight"/> class.
        /// </summary>
        /// <param name="name">Name of the light.</param>
        public SpotLight(String name) : this(name, Vector3.Zero, Vector3.Forward, Angle.PiOverFour, Angle.PiOverTwo) { }

        /// <summary>
        /// Constructs a new instance of the <see cref="SpotLight"/> class.
        /// </summary>
        /// <param name="position">Position of the light.</param>
        /// <param name="direction">Direction of the light.</param>
        /// <param name="innerAngle">Cone's innter angle.</param>
        /// <param name="outerAngle">Cone's outer angle.</param>
        public SpotLight(Vector3 position, Vector3 direction, Angle innerAngle, Angle outerAngle) : this("SpotLight", position, direction, innerAngle, outerAngle) { }

        /// <summary>
        /// Constructs a new instance of the <see cref="SpotLight"/> class.
        /// </summary>
        /// <param name="name">Name of the light.</param>
        /// <param name="position">Position of the light.</param>
        /// <param name="direction">Direction of the light.</param>
        /// <param name="innerAngle">Cone's innter angle.</param>
        /// <param name="outerAngle">Cone's outer angle.</param>
        public SpotLight(String name, Vector3 position, Vector3 direction, Angle innerAngle, Angle outerAngle)
            : base(name)
        {
            m_position = position;
            Direction = direction;
            InnerAngle = innerAngle;
            OuterAngle = outerAngle;
        }

        /// <summary>
        /// Clones the light.
        /// </summary>
        /// <returns>Copy of the light.</returns>
        public override Light Clone()
        {
            SpotLight sl = new SpotLight();
            sl.Set(this);

            return sl;
        }

        /// <summary>
        /// Sets the position of the light.
        /// </summary>
        /// <param name="x">X coordinate.</param>
        /// <param name="y">Y coordinate.</param>
        /// <param name="z">Z coordinate.</param>
        public void SetPosition(float x, float y, float z)
        {
            m_position = new Vector3(x, y, z);
            OnLightChanged();
        }

        /// <summary>
        /// Sets the direction of the light.
        /// </summary>
        /// <param name="x">X component.</param>
        /// <param name="y">Y component.</param>
        /// <param name="z">Z component.</param>
        public void SetDirection(float x, float y, float z)
        {
            m_direction = new Vector3(x, y, z);
            m_direction.Normalize();
            OnLightChanged();
        }

        /// <summary>
        /// Set the angle properties of the light.
        /// </summary>
        /// <param name="innerAngle">Inner angle.</param>
        /// <param name="outerAngle">Outer angle.</param>
        public void SetAngles(Angle innerAngle, Angle outerAngle)
        {
            m_innerAngle = Angle.Clamp(innerAngle, Angle.Zero, Angle.Pi);
            m_outerAngle = Angle.Clamp(outerAngle, Angle.Zero, Angle.Pi);
            OnLightChanged();
        }

        /// <summary>
        /// Reads the object data from the input.
        /// </summary>
        /// <param name="input">Savable reader</param>
        public override void Read(ISavableReader input)
        {
            base.Read(input);
            input.Read<Vector3>("Position", out m_position);
            input.Read<Vector3>("Direction", out m_direction);
            input.Read<Angle>("InnerAngle", out m_innerAngle);
            input.Read<Angle>("OuterAngle", out m_outerAngle);
        }

        /// <summary>
        /// Writes the object data to the output.
        /// </summary>
        /// <param name="output">Savable writer</param>
        public override void Write(ISavableWriter output)
        {
            base.Write(output);
            output.Write<Vector3>("Position", m_position);
            output.Write<Vector3>("Direction", m_direction);
            output.Write<Angle>("InnerAngle", m_innerAngle);
            output.Write<Angle>("OuterAngle", m_outerAngle);
        }

        /// <summary>
        /// Copies light properties.
        /// </summary>
        /// <param name="l">Light to copy from.</param>
        protected override void OnSet(Light l)
        {
            SpotLight sl = l as SpotLight;
            if (sl == null)
                return;

            m_position = sl.m_position;
            m_direction = sl.m_direction;
            m_innerAngle = sl.m_innerAngle;
            m_outerAngle = sl.m_outerAngle;
        }
    }
}
