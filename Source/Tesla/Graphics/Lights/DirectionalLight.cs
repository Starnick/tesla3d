﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Content;

namespace Tesla.Graphics
{
    /// <summary>
    /// Represents a light source that is infinitely away from the viewer, thus it only has a direction. For example, the Sun relative
    /// to the Earth "seems infinitely away". This is also known as a parallel light. Due to the nature of this type of light, it ignores attenuation
    /// properties from the base light class.
    /// </summary>
    [SavableVersion(1)]
    public class DirectionalLight : Light
    {
        private Vector3 m_direction;

        /// <summary>
        /// Gets or sets the direction of the light. The value is always normalized.
        /// 
        /// Default: (0, 0, -1.0f)
        /// </summary>
        public Vector3 Direction
        {
            get
            {
                return m_direction;
            }
            set
            {
                m_direction = value;
                m_direction.Normalize();
                OnLightChanged();
            }
        }

        /// <summary>
        /// Gets the light type for this light.
        /// </summary>
        public override LightType LightType
        {
            get
            {
                return LightType.Directional;
            }
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="DirectionalLight"/> class.
        /// </summary>
        public DirectionalLight() : this("DirectionalLight", Vector3.Forward) { }

        /// <summary>
        /// Constructs a new instance of the <see cref="DirectionalLight"/> class.
        /// </summary>
        /// <param name="name">Name of the light.</param>
        public DirectionalLight(String name) : this(name, Vector3.Forward) { }

        /// <summary>
        /// Constructs a new instance of the <see cref="DirectionalLight"/> class.
        /// </summary>
        /// <param name="direction">Direction of the light.</param>
        public DirectionalLight(Vector3 direction) : this("DirectionalLight", direction) { }

        /// <summary>
        /// Constructs a new instance of the <see cref="DirectionalLight"/> class.
        /// </summary>
        /// <param name="name">Name of the light.</param>
        /// <param name="direction">Direction of the light.</param>
        public DirectionalLight(String name, Vector3 direction)
            : base(name)
        {
            m_direction = direction;
            m_direction.Normalize();
        }

        /// <summary>
        /// Clones the light.
        /// </summary>
        /// <returns>Copy of the light.</returns>
        public override Light Clone()
        {
            DirectionalLight dl = new DirectionalLight();
            dl.Set(this);

            return dl;
        }

        /// <summary>
        /// Sets the direction of the light.
        /// </summary>
        /// <param name="x">X component.</param>
        /// <param name="y">Y component.</param>
        /// <param name="z">Z component.</param>
        public void SetDirection(float x, float y, float z)
        {
            m_direction = new Vector3(x, y, z);
            m_direction.Normalize();
            OnLightChanged();
        }

        /// <summary>
        /// Reads the object data from the input.
        /// </summary>
        /// <param name="input">Savable reader</param>
        public override void Read(ISavableReader input)
        {
            base.Read(input);
            input.Read<Vector3>("Direction", out m_direction);
        }

        /// <summary>
        /// Writes the object data to the output.
        /// </summary>
        /// <param name="output">Savable writer</param>
        public override void Write(ISavableWriter output)
        {
            base.Write(output);
            output.Write<Vector3>("Direction", m_direction);
        }

        /// <summary>
        /// Copies light properties.
        /// </summary>
        /// <param name="l">Light to copy from.</param>
        protected override void OnSet(Light l)
        {
            DirectionalLight dl = l as DirectionalLight;
            if (dl == null)
                return;

            m_direction = dl.m_direction;
        }
    }
}
