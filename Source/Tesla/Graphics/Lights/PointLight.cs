﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Content;

namespace Tesla.Graphics
{
    /// <summary>
    /// Represents an omni-directional light source that has a position in space.
    /// </summary>
    [SavableVersion(1)]
    public class PointLight : Light
    {
        private Vector3 m_position;

        /// <summary>
        /// Gets or sets the position of the light in space.
        /// 
        /// Default: (0, 0, 0)
        /// </summary>
        public Vector3 Position
        {
            get
            {
                return m_position;
            }
            set
            {
                m_position = value;
                OnLightChanged();
            }
        }

        /// <summary>
        /// Gets the light type for this light.
        /// </summary>
        public override LightType LightType
        {
            get 
            {
                return LightType.Point;
            }
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="PointLight"/> class.
        /// </summary>
        public PointLight() : this("PointLight", Vector3.Zero) { }

        /// <summary>
        /// Constructs a new instance of the <see cref="PointLight"/> class.
        /// </summary>
        /// <param name="name">Name of the light.</param>
        public PointLight(String name) : this(name, Vector3.Zero) { }

        /// <summary>
        /// Constructs a new instance of the <see cref="PointLight"/> class.
        /// </summary>
        /// <param name="position">Position of the light.</param>
        public PointLight(Vector3 position) : this("PointLight", position) { }

        /// <summary>
        /// Constructs a new instance of the <see cref="PointLight"/> class.
        /// </summary>
        /// <param name="name">Name of the light.</param>
        /// <param name="position">Position of the light.</param>
        public PointLight(String name, Vector3 position) 
            : base(name)
        {
            m_position = position;
        }

        /// <summary>
        /// Clones the light.
        /// </summary>
        /// <returns>Copy of the light.</returns>
        public override Light Clone()
        {
            PointLight pl = new PointLight();
            pl.Set(this);

            return pl;
        }

        /// <summary>
        /// Sets the position of the light.
        /// </summary>
        /// <param name="x">X coordinate.</param>
        /// <param name="y">Y coordinate.</param>
        /// <param name="z">Z coordinate.</param>
        public void SetPosition(float x, float y, float z)
        {
            m_position = new Vector3(x, y, z);
            OnLightChanged();
        }

        /// <summary>
        /// Reads the object data from the input.
        /// </summary>
        /// <param name="input">Savable reader</param>
        public override void Read(ISavableReader input)
        {
            base.Read(input);
            input.Read<Vector3>("Position", out m_position);
        }

        /// <summary>
        /// Writes the object data to the output.
        /// </summary>
        /// <param name="output">Savable writer</param>
        public override void Write(ISavableWriter output)
        {
            base.Write(output);
            output.Write<Vector3>("Position", m_position);
        }

        /// <summary>
        /// Copies light properties.
        /// </summary>
        /// <param name="l">Light to copy from.</param>
        protected override void OnSet(Light l)
        {
            PointLight pl = l as PointLight;
            if (pl == null)
                return;

            m_position = pl.m_position;
        }
    }
}
