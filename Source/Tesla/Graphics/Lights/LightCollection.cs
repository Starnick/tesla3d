﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using Tesla.Content;

namespace Tesla.Graphics
{
    /// <summary>
    /// Collection of lights which has some global light properties such as ambient color and can let shaders know if data has changed.
    /// </summary>
    [SavableVersion(1)]
    public class LightCollection : IList<Light>, IReadOnlyList<Light>, ISavable
    {
        /// <summary>
        /// Occurs when lights are added, removed, or cleared from the collection.
        /// </summary>
        public event TypedEventHandler<LightCollection> CollectionModified;

        private TypedEventHandler<Light> m_cachedLightChangeDelegate;

        private List<Light> m_lights;
        private Color m_globalAmbient;
        private bool m_updateShader;
        private bool m_isEnabled;
        private bool m_monitorLightChanges;
        private int m_version;

        /// <summary>
        /// Gets or sets the light at the specified index.
        /// </summary>
        /// <param name="index">The index.</param>
        public Light this[int index]
        {
            get 
            {
                if(index < 0 || index >= m_lights.Count)
                    return null;

                return m_lights[index];
            }
            set 
            {
                if(index < 0 || index >= m_lights.Count || value == null)
                    return;

                if(m_monitorLightChanges)
                {
                    value.LightChanged += CachedChangeDelegate;
                    Light oldLight = m_lights[index];
                    if(oldLight != null)
                        oldLight.LightChanged -= CachedChangeDelegate;
                }

                m_lights[index] = value;
                m_version++;
            }
        }

        /// <summary>
        /// Gets the number of lights contained in the collection.
        /// </summary>
        public int Count
        {
            get 
            {
                return m_lights.Count;
            }
        }

        /// <summary>
        /// Gets or sets the global ambient color.
        /// </summary>
        public Color GlobalAmbient
        {
            get
            {
                return m_globalAmbient;
            }
            set
            {
                m_globalAmbient = value;
                m_updateShader = true;
            }
        }

        /// <summary>
        /// Gets or sets if the light collection has changed in any way that may interest the shader.
        /// </summary>
        public bool UpdateShader
        {
            get
            {
                return m_updateShader;
            }
            set
            {
                m_updateShader = value;
            }
        }

        /// <summary>
        /// Gets or sets if the light collection is enabled. This is a hint to the shader that no data should be uploaded.
        /// </summary>
        public bool IsEnabled
        {
            get
            {
                return m_isEnabled;
            }
            set
            {
                m_isEnabled = value;
                m_updateShader = true;
            }
        }

        /// <summary>
        /// Gets or sets if the collection should monitor lights contained in it for when they change. If set to true, then when light properties
        /// change, the collection's <see cref="UpdateShader"/> property is set to true.
        /// </summary>
        public bool MonitorLightChanges
        {
            get
            {
                return m_monitorLightChanges;
            }
            set
            {
                if(m_monitorLightChanges != value)
                {

                    m_monitorLightChanges = value;

                    if(m_monitorLightChanges)
                        AddEvents();
                    else
                        RemoveEvents();
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.Generic.ICollection`1" /> is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get 
            {
                return false;
            }
        }

        private TypedEventHandler<Light> CachedChangeDelegate
        {
            get
            {
                if(m_cachedLightChangeDelegate == null)
                    m_cachedLightChangeDelegate = new TypedEventHandler<Light>(OnLightChanged);

                return m_cachedLightChangeDelegate;
            }
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="LightCollection"/> class.
        /// </summary>
        public LightCollection() : this(0) { }

        /// <summary>
        /// Constructs a new instance of the <see cref="LightCollection"/> class.
        /// </summary>
        /// <param name="capacity">Initial capacity of the collection.</param>
        public LightCollection(int capacity)
        {
            m_globalAmbient = Color.White;
            m_monitorLightChanges = false;
            m_updateShader = true;
            m_isEnabled = true;
            m_lights = new List<Light>(capacity);
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="LightCollection"/> class.
        /// </summary>
        /// <param name="collection">Collection of lights to copy.</param>
        public LightCollection(IEnumerable<Light> collection)
        {
            m_globalAmbient = Color.White;
            m_monitorLightChanges = false;
            m_updateShader = true;
            m_isEnabled = true;
            m_lights = new List<Light>(collection);
        }

        /// <summary>
        /// Adds the light to the collection.
        /// </summary>
        /// <param name="item">Light to add.</param>
        public void Add(Light item)
        {
            if(item == null)
                return;

            m_lights.Add(item);

            if(m_monitorLightChanges)
                item.LightChanged += CachedChangeDelegate;

            m_updateShader = true;
            m_version++;
            OnModified();
        }

        /// <summary>
        /// Adds a number of lights to the collection.
        /// </summary>
        /// <param name="items">Lights to add.</param>
        public void AddRange(IEnumerable<Light> items)
        {
            if(items == null)
                return;

             IList<Light> coll = items as IList<Light>;

            if(coll == null)
            {
                foreach(Light l in items)
                {
                    if(l != null)
                    {
                        m_lights.Add(l);

                        if(m_monitorLightChanges)
                            l.LightChanged += CachedChangeDelegate;
                    }
                }
            }
            else
            {
                for(int i = 0; i < coll.Count; i++)
                {
                    Light l = coll[i];
                    if(l != null)
                    {
                        m_lights.Add(l);

                        if(m_monitorLightChanges)
                            l.LightChanged += CachedChangeDelegate;
                    }
                }
            }

            m_updateShader = true;
            m_version++;
            OnModified();
        }

        /// <summary>
        /// Inserts a light at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index at which <paramref name="item" /> should be inserted.</param>
        /// <param name="item">The light to insert into the collection.</param>
        public void Insert(int index, Light item)
        {
            if(index < 0 || index > m_lights.Count || item == null)
                return;

            m_lights.Insert(index, item);

            if(m_monitorLightChanges)
                item.LightChanged += CachedChangeDelegate;

            m_updateShader = true;
            m_version++;
            OnModified();
        }

        /// <summary>
        /// Removes all lights from the collection.
        /// </summary>
        public void Clear()
        {
            RemoveEvents();
            m_lights.Clear();
            m_version++;
            OnModified();
        }

        /// <summary>
        /// Queries if the specified light is contained in the collection.
        /// </summary>
        /// <param name="item">Light to determine if it is contained in the collection.</param>
        /// <returns>True if the light is contained in the collection, false otherwise.</returns>
        public bool Contains(Light item)
        {
            if(item == null)
                return false;

            return m_lights.Contains(item);
        }

        /// <summary>
        /// Copies the entire contents of the collection to the array, starting at the specified array index.
        /// </summary>
        /// <param name="array">Array to copy to</param>
        /// <param name="arrayIndex">Index of the array to start copying to.</param>
        /// <exception cref="System.ArgumentNullException">Thrown if the array is null.</exception>
        /// <exception cref="System.ArgumentOutOfRangeException">Thrown if the array index is less than zero.</exception>
        /// <exception cref="System.ArgumentException">Thrown if the number of elements in the source collection is greater than the available space
        /// from the array index to the end of the destination array.</exception>
        public void CopyTo(Light[] array, int arrayIndex)
        {
            m_lights.CopyTo(array, arrayIndex);
        }

        /// <summary>
        /// Removes the light at the specified index in the collection.
        /// </summary>
        /// <param name="index">Index of light to remove at.</param>
        public void RemoveAt(int index)
        {
            if(index < 0 || index >= m_lights.Count)
                return;

            if(m_monitorLightChanges)
            {
                Light l = m_lights[index];
                l.LightChanged -= CachedChangeDelegate;
            }

            m_lights.RemoveAt(index);
            m_version++;
            OnModified();
        }

        /// <summary>
        /// Removes the specified light from the collection.
        /// </summary>
        /// <param name="item">Light to remove.</param>
        /// <returns>True if the light was successfully removed from the collection, false otherwise.</returns>
        public bool Remove(Light item)
        {
            bool removed = m_lights.Remove(item);

            if(removed)
            {
                m_version++;

                if(m_monitorLightChanges)
                {
                    item.LightChanged -= CachedChangeDelegate;
                    OnModified();
                }
            }

            return removed;
        }

        /// <summary>
        /// Queries for the index in the collection of the specified light.
        /// </summary>
        /// <param name="item">Light to locate in the collection.</param>
        /// <returns>The zero-based index of the first light within the entire collection, if not found then -1.</returns>
        public int IndexOf(Light item)
        {
            return m_lights.IndexOf(item);
        }

        /// <summary>
        /// Sorts the collection using a comparer.
        /// </summary>
        /// <param name="comparer">Comparer used to sort the contents of the collection.</param>
        public void Sort(IComparer<Light> comparer)
        {
            m_lights.Sort(comparer);
            m_version++;
        }

        /// <summary>
        /// Reads the object data from the input.
        /// </summary>
        /// <param name="input">Savable reader</param>
        public virtual void Read(ISavableReader input)
        {
            m_globalAmbient = input.Read<Color>("GlobalAmbient");
            m_monitorLightChanges = input.ReadBoolean("MonitorLightChanges");
            m_isEnabled = input.ReadBoolean("IsEnabled");

            Light[] lightArray = input.ReadSharedSavableArray<Light>("Lights");
            m_lights = (lightArray != null) ? new List<Light>(lightArray) : new List<Light>();
            m_updateShader = true;

            if(m_monitorLightChanges)
                AddEvents();
        }

        /// <summary>
        /// Writes the object data to the output.
        /// </summary>
        /// <param name="output">Savable writer</param>
        public virtual void Write(ISavableWriter output)
        {
            output.Write<Color>("GlobalAmbient", m_globalAmbient);
            output.Write("MonitorLightChanges", m_monitorLightChanges);
            output.Write("IsEnabled", m_isEnabled);
            output.WriteSharedSavable<Light>("Lights", m_lights.ToArray());
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>Light collection enumerator.</returns>
        public LightCollectionEnumerator GetEnumerator()
        {
            return new LightCollectionEnumerator(this);
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.</returns>
        IEnumerator<Light> IEnumerable<Light>.GetEnumerator()
        {
            return new LightCollectionEnumerator(this);
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return new LightCollectionEnumerator(this);
        }

        private void OnLightChanged(Light light, EventArgs e)
        {
            m_updateShader = true;
        }

        private void RemoveEvents()
        {
            for(int i = 0; i < m_lights.Count; i++)
            {
                Light l = m_lights[i];
                if(l != null)
                    l.LightChanged += CachedChangeDelegate;
            }
        }

        private void AddEvents()
        {
            for(int i = 0; i < m_lights.Count; i++)
            {
                Light l = m_lights[i];
                if(l != null)
                    l.LightChanged -= CachedChangeDelegate;
            }
        }

        private void OnModified()
        {
            TypedEventHandler<LightCollection> handler = CollectionModified;
            if(handler != null)
                handler(this, EventArgs.Empty);
        }

        #region Enumerator

        /// <summary>
        /// Enumerates elements of a <see cref="LightCollection"/>.
        /// </summary>
        public struct LightCollectionEnumerator : IEnumerator<Light>
        {
            private LightCollection m_collection;
            private List<Light> m_lights;
            private int m_index;
            private int m_version;
            private Light m_current;

            /// <summary>
            /// Gets the current light.
            /// </summary>
            public Light Current
            {
                get
                {
                    return m_current;
                }
            }

            Object IEnumerator.Current
            {
                get
                {
                    return m_current;
                }
            }

            internal LightCollectionEnumerator(LightCollection lightCollection)
            {
                m_collection = lightCollection;
                m_lights = lightCollection.m_lights;
                m_index = 0;
                m_version = lightCollection.m_version;
                m_current = null;
            }

            /// <summary>
            /// Advances the enumerator to the next element of the collection.
            /// </summary>
            /// <returns>True if the enumerator was successfully advanced to the next element; false if the enumerator has passed the end of the collection.</returns>
            public bool MoveNext()
            {
                ThrowIfChanged();

                if(m_index < m_lights.Count)
                {
                    m_current = m_lights[m_index];
                    m_index++;
                    return true;
                }
                else
                {
                    m_index++;
                    m_current = null;
                    return false;
                }
            }

            /// <summary>
            /// Sets the enumerator to its initial position, which is before the first element in the collection.
            /// </summary>
            public void Reset()
            {
                ThrowIfChanged();
                m_index = 0;
                m_current = null;
            }

            /// <summary>
            /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
            /// </summary>
            public void Dispose() { }

            private void ThrowIfChanged()
            {
                if(m_version != m_collection.m_version)
                    throw new InvalidOperationException(StringLocalizer.Instance.GetLocalizedString("CollectionModifiedDuringEnumeration"));
            }
        }

        #endregion
    }
}
