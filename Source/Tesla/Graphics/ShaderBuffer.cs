﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;
using Tesla.Content;
using Tesla.Graphics.Implementation;

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// Represents a simple buffer on the GPU that can be bound as a shader resource. This is very similar to a 1D Texture where
  /// each element is a 1-4 vector component. Similar to texture data and vertex buffer data, the format can be expanded on
  /// the GPU (e.g. 32-bit packed Color to 4-float vector)
  /// </summary>
  [SavableVersion(1)]
  public class ShaderBuffer : GraphicsResource, ISavable, IBufferResource
  {
    #region Public Properties

    /// <summary>
    /// Gets the number of elements in the buffer.
    /// </summary>
    public int ElementCount
    {
      get
      {
        return ShaderBufferImpl.ElementCount;
      }
    }

    /// <summary>
    /// Gets the format of an element in the buffer. Data type can be reintrepreted by the GPU similar to vertex buffers or textures.
    /// </summary>
    public VertexFormat Format
    {
      get
      {
        return ShaderBufferImpl.Format;
      }
    }

    /// <summary>
    /// Gets size in bytes of a single element in the buffer.
    /// </summary>
    public int ElementStride
    {
      get
      {
        return ShaderBufferImpl.ElementStride;
      }
    }

    /// <summary>
    /// Gets the total size in bytes of the buffer.
    /// </summary>
    public int SizeInBytes
    {
      get
      {
        return ShaderBufferImpl.ElementStride * ShaderBufferImpl.ElementCount;
      }
    }

    /// <summary>
    /// Gets the resource usage of the buffer.
    /// </summary>
    public ResourceUsage ResourceUsage
    {
      get
      {
        return ShaderBufferImpl.ResourceUsage;
      }
    }

    /// <summary>
    /// Gets the resource binding of the buffer.
    /// </summary>
    public virtual ResourceBindFlags ResourceBindFlags
    {
      get
      {
        return ShaderBufferImpl.ResourceBindFlags;
      }
    }

    /// <summary>
    /// Gets the shader resource type.
    /// </summary>
    public ShaderResourceType ResourceType
    {
      get
      {
        return ShaderResourceType.Buffer;
      }
    }

    #endregion

    //For casting purposes
    private IShaderBufferImpl ShaderBufferImpl
    {
      get
      {
        return GetImplAs<IShaderBufferImpl>();
      }
      set
      {
        BindImplementation(value);
      }
    }

    /// <summary>
    /// For ISavable.
    /// </summary>
    protected ShaderBuffer() { }

    /// <summary>
    /// Constructs a new instance of the <see cref="ShaderBuffer"/> class.
    /// </summary>
    /// <param name="renderSystem">Render system used to create the underlying implementation.</param>
    /// <param name="format">Element format.</param>
    /// <param name="elementCount">Number of elements in the buffer.</param>
    /// <param name="options">Common <see cref="BufferOptions"/> that may also contain initial data.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the render system is null or if any input is null when it is required (e.g. initial data is required for immutable resources).</exception>
    /// <exception cref="System.ArgumentOutOfRangeException">Thrown if the element count is less than or equal to zero.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if creating the underlying implementation fails or is unsupported, see inner exception for a more detailed error.</exception>
    public ShaderBuffer(IRenderSystem renderSystem, VertexFormat format, int elementCount, BufferOptions options = default)
    {
      CreateImplementation(renderSystem, format, elementCount, options);
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="ShaderBuffer"/> class. The options MUST contain exactly one initial data buffer. The element
    /// count will be automatically computed from the contents
    /// </summary>
    /// <param name="renderSystem">Render system used to create the underlying implementation.</param>
    /// <param name="format">Element format.</param>
    /// <param name="options">Common <see cref="BufferOptions"/>. Initial data MUST have exactly one buffer, which may be raw bytes.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the render system is null or if any input is null when it is required (e.g. initial data is required for immutable resources).</exception>
    /// <exception cref="System.ArgumentOutOfRangeException">Thrown if the element count is less than or equal to zero.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if creating the underlying implementation fails or is unsupported, see inner exception for a more detailed error.</exception>
    public ShaderBuffer(IRenderSystem renderSystem, VertexFormat format, BufferOptions options = default)
    {
      int totalSize = options.Data?.SizeInBytes ?? 0;
      int elementCount = BufferHelper.ComputeElementCount(totalSize, format.SizeInBytes());
      CreateImplementation(renderSystem, format, elementCount, options);
    }

    #region Public Methods

    /// <summary>
    /// Reads data from the graphics buffer into the data buffer
    /// </summary>
    /// <typeparam name="T">Type of data to read from the graphics buffer.</typeparam>
    /// <param name="data">Buffer to hold data copied from the graphics buffer.</param>
    /// <param name="offsetInBytes">Optional offset from the start of the graphics buffer at which to start copying from.</param>
    /// <exception cref="ObjectDisposedException">Thrown if the resource has already been disposed.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if an error occurs while reading from the resource.</exception>
    /// <remarks>See implementors for details on specific exceptions that can be thrown.</remarks>
    public void GetData<T>(Span<T> data, int offsetInBytes = 0) where T : unmanaged
    {
      CheckDisposed();

      try
      {
        ShaderBufferImpl.GetData<T>(data, offsetInBytes);
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw GraphicsExceptionHelper.NewErrorReadingFromResourceException(typeof(ShaderBuffer), e);
      }
    }

    /// <summary>
    /// Writes data from the data buffer into the graphics buffer.
    /// </summary>
    /// <typeparam name="T">Type of data to write to the graphics buffer.</typeparam>
    /// <param name="renderContext">The current render context.</param>
    /// <param name="data">Buffer with the data to be copied to the graphics buffer.</param>
    /// <param name="writeOptions">Optional write options, used only if this is a dynamic buffer. None, discard, no overwrite.</param>
    /// <exception cref="ObjectDisposedException">Thrown if the resource has already been disposed.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if an error occurs while writing to the resource, or if trying to write to a non-DYNAMIC resource 
    /// using a deferred render context.</exception>
    /// <remarks>See implementors for details on specific exceptions that can be thrown.</remarks>
    public void SetData<T>(IRenderContext renderContext, ReadOnlySpan<T> data, DataWriteOptions writeOptions = DataWriteOptions.Discard) where T : unmanaged
    {
      SetData<T>(renderContext, data, 0, writeOptions);
    }

    /// <summary>
    /// Writes data from the data buffer into the graphics buffer.
    /// </summary>
    /// <typeparam name="T">Type of data to write to the graphics buffer.</typeparam>
    /// <param name="renderContext">The current render context.</param>
    /// <param name="data">Buffer with the data to be copied to the graphics buffer.</param>
    /// <param name="offsetInBytes">Offset from the start of the graphics buffer at which to start writing at.</param>
    /// <param name="writeOptions">Optional write options, used only if this is a dynamic buffer. None, discard, no overwrite.</param>
    /// <exception cref="ObjectDisposedException">Thrown if the resource has already been disposed.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if an error occurs while writing to the resource, or if trying to write to a non-DYNAMIC resource 
    /// using a deferred render context.</exception>
    /// <remarks>See implementors for details on specific exceptions that can be thrown.</remarks>
    public void SetData<T>(IRenderContext renderContext, ReadOnlySpan<T> data, int offsetInBytes, DataWriteOptions writeOptions = DataWriteOptions.Discard) where T : unmanaged
    {
      CheckDisposed();
      CheckDeferredSetDataIsPermitted(renderContext, ResourceUsage);

      try
      {
        ShaderBufferImpl.SetData<T>(renderContext, data, offsetInBytes, writeOptions);
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw GraphicsExceptionHelper.NewErrorWritingToResourceException(typeof(ShaderBuffer), e);
      }
    }

    #endregion

    #region ISavable Methods

    /// <summary>
    /// Reads the object data from the input.
    /// </summary>
    /// <param name="input">Savable reader</param>
    public void Read(ISavableReader input)
    {
      IRenderSystem renderSystem = GraphicsHelper.GetRenderSystem(input.ServiceProvider);

      Name = input.ReadString("Name");
      int elementCount = input.ReadInt32("ElementCount");
      VertexFormat format = input.ReadEnum<VertexFormat>("Format");
      ResourceUsage usage = input.ReadEnum<ResourceUsage>("ResourceUsage");
      ResourceBindFlags bindFlags = input.ReadEnum<ResourceBindFlags>("ResourceBindFlags");

      // Create a pooled buffer for the data
      using (DataBuffer<byte> data = input.ReadBlob<byte>("Data", MemoryAllocatorStrategy.DefaultPooled))
        CreateImplementation(renderSystem, format, elementCount, BufferOptions.Init(data, usage, bindFlags));
    }

    /// <summary>
    /// Writes the object data to the output.
    /// </summary>
    /// <param name="output">Savable writer</param>
    public void Write(ISavableWriter output)
    {
      output.Write("Name", Name);
      output.Write("ElementCount", ElementCount);
      output.WriteEnum<VertexFormat>("Format", Format);
      output.WriteEnum<ResourceUsage>("ResourceUsage", ResourceUsage);
      output.WriteEnum<ResourceBindFlags>("ResourceBindFlags", ResourceBindFlags);

      // Create a pooled buffer for the data
      using (DataBuffer<byte> data = DataBuffer.Create<byte>(SizeInBytes, MemoryAllocatorStrategy.DefaultPooled))
      {
        GetData<byte>(data.Span);

        output.WriteBlob<byte>("Data", data.Span);
      }
    }

    #endregion

    #region Creation Parameter Validation


    /// <summary>
    /// Validates creation parameters.
    /// </summary>
    /// <param name="elementCount">The number of elements the buffer will contain.</param>
    /// <exception cref="System.ArgumentOutOfRangeException">Thrown if the element count is less than or equal to zero.</exception>
    protected void ValidateCreationParameters(int elementCount)
    {
      if (elementCount <= 0)
        throw new ArgumentOutOfRangeException(nameof(elementCount), StringLocalizer.Instance.GetLocalizedString("ResourceSizeMustBeGreaterThanZero"));
    }

    /// <summary>
    /// Validates creation parameters.
    /// </summary>
    /// <param name="format">Element format.</param>
    /// <param name="elementCount">Number of elements the buffer will contain.</param>
    /// <param name="options">Common <see cref="BufferOptions"/> that may also contain initial data.</param>
    /// <exception cref="ArgumentNullException">Thrown if the initial data is null and the resource is set to immutable.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the element count and stride does not match the initial data size, if any.</exception>
    protected void ValidateCreationParameters(VertexFormat format, int elementCount, BufferOptions options)
    {
      ValidateCreationParameters(elementCount);

      if (options.Data is null)
      {
        if (options.ResourceUsage == ResourceUsage.Immutable)
          throw GraphicsExceptionHelper.NewMustSupplyDataForImmutableException();

        return;
      }

      IReadOnlyDataBuffer data = options.Data;
      if (data.Length == 0)
        throw new ArgumentNullException(nameof(options.Data), StringLocalizer.Instance.GetLocalizedString("DataBufferNull"));

      int totalSize = data.SizeInBytes;
      int formatSizeInBytes = format.SizeInBytes();

      // Format size should divide evenly into the total buffer size
      if (totalSize % formatSizeInBytes != 0 || BufferHelper.ComputeElementCount(totalSize, formatSizeInBytes) != elementCount)
        throw new ArgumentOutOfRangeException(nameof(options.Data), StringLocalizer.Instance.GetLocalizedString("FormatSizeMismatch"));
    }

    #endregion

    #region Implementation Creation

    private void CreateImplementation(IRenderSystem renderSystem, VertexFormat format, int elementCount, BufferOptions options)
    {
      GraphicsHelper.ThrowIfRenderSystemNull(renderSystem);

      ValidateCreationParameters(format, elementCount, options);

      IShaderBufferImplFactory factory;
      if (!renderSystem.TryGetImplementationFactory<IShaderBufferImplFactory>(out factory))
        throw GraphicsExceptionHelper.NewUnsupportedFeatureException(renderSystem, typeof(ShaderBuffer));

      try
      {
        ShaderBufferImpl = factory.CreateImplementation(format, elementCount, options);
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw GraphicsExceptionHelper.NewErrorCreatingImplementation(typeof(ShaderBuffer), e);
      }
    }

    #endregion
  }
}
