﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics;
using System.Drawing;
using Tesla.Content;
using Tesla.Graphics.Implementation;

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// Represents a Three-Dimensional texture resource that has width, height, and depth.
  /// </summary>
  [SavableVersion(1)]
  public class Texture3D : Texture
  {

    #region Public Properties

    /// <inheritdoc />
    public override TextureDimension Dimension
    {
      get
      {
        return TextureDimension.Three;
      }
    }

    /// <inheritdoc />
    public override ShaderResourceType ResourceType
    {
      get
      {
        return ShaderResourceType.Texture3D;
      }
    }

    /// <summary>
    /// Gets the texture width, in texels.
    /// </summary>
    public int Width
    {
      get
      {
        return Texture3DImpl.Width;
      }
    }

    /// <summary>
    /// Gets the texture height, in texels.
    /// </summary>
    public int Height
    {
      get
      {
        return Texture3DImpl.Height;
      }
    }

    /// <summary>
    /// Gets the texture depth, in texels.
    /// </summary>
    public int Depth
    {
      get
      {
        return Texture3DImpl.Depth;
      }
    }

    #endregion

    //Private property to cast the implementation
    private ITexture3DImpl Texture3DImpl
    {
      get
      {
        return GetImplAs<ITexture3DImpl>();
      }
      set
      {
        BindImplementation(value);
      }
    }

    /// <summary>
    /// For ISavable only.
    /// </summary>
    protected Texture3D() { }

    /// <summary>
    /// Constructs a new instance of the <see cref="Texture3D"/> class.
    /// </summary>
    /// <param name="renderSystem">Render system used to create the underlying implementation.</param>
    /// <param name="width">Width of the texture, in texels.</param>
    /// <param name="height">Height of the texture, in texels.</param>
    /// <param name="depth">Depth of the texture, in texels.</param>
    /// <param name="options">Common <see cref="TextureOptions"/> that may also contain initial data.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the render system is null or if any input is null when it is required (e.g. initial data is required for immutable resources).</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the width/height/depth is less than or equal to zero, or greater than the maximum texture size.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if creating the underlying implementation fails or is unsupported, see inner exception for a more detailed error.</exception>
    public Texture3D(IRenderSystem renderSystem, int width, int height, int depth, TextureOptions options = default)
    {
      CreateImplementation(renderSystem, width, height, depth, options);
    }

    #region Public Methods

    /// <summary>
    /// Reads data from the texture into the specified buffer.
    /// </summary>
    /// <typeparam name="T">Type of data to read from the texture.</typeparam>
    /// <param name="data">Buffer to hold data copied from the texture.</param>
    /// <param name="subimage">The subimage region, in texels, of the 3D texture to read from, if null the whole image is read from.</param>
    /// <exception cref="ObjectDisposedException">Thrown if the resource has already been disposed.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if an error occurs while reading from the resource.</exception>
    /// <remarks>See implementors for details on specific exceptions that can be thrown.</remarks>
    public void GetData<T>(Span<T> data, ResourceRegion3D? subimage) where T : unmanaged
    {
      GetData<T>(data, SubResourceAt.Zero, subimage);
    }

    /// <summary>
    /// Reads data from the texture into the specified buffer.
    /// </summary>
    /// <typeparam name="T">Type of data to read from the texture.</typeparam>
    /// <param name="data">Buffer to hold data copied from the texture.</param>
    /// <param name="subIndex">Optional index to identify which subresource (e.g. mipmap) to access.</param>
    /// <param name="subimage">The subimage region, in texels, of the 3D texture to read from, if null the whole image is read from.</param>
    /// <param name="startIndex">Starting index in the data buffer to start writing to.</param>
    /// <exception cref="ObjectDisposedException">Thrown if the resource has already been disposed.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if an error occurs while reading from the resource.</exception>
    /// <remarks>See implementors for details on specific exceptions that can be thrown.</remarks>
    public void GetData<T>(Span<T> data, SubResourceAt subIndex = default(SubResourceAt), ResourceRegion3D? subimage = null) where T : unmanaged
    {
      CheckDisposed();

      try
      {
        Texture3DImpl.GetData<T>(data, subIndex, subimage);
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw GraphicsExceptionHelper.NewErrorReadingFromResourceException(GetType(), e);
      }
    }

    /// <summary>
    /// Writes data to the texture from the specified buffer.
    /// </summary>
    /// <typeparam name="T">Type of data to write to the texture.</typeparam>
    /// <param name="renderContext">The current render context.</param>
    /// <param name="data">Buffer with the data to be copied to the texture.</param>
    /// <param name="writeOptions">Optional writing options, valid only for dynamic textures.</param>
    /// <exception cref="ObjectDisposedException">Thrown if the resource has already been disposed.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if an error occurs while writing to the resource, or if trying to write to a non-DYNAMIC resource 
    /// using a deferred render context.</exception>
    /// <remarks>See implementors for details on specific exceptions that can be thrown.</remarks>
    public void SetData<T>(IRenderContext renderContext, ReadOnlySpan<T> data, DataWriteOptions writeOptions = DataWriteOptions.Discard) where T : unmanaged
    {
      SetData<T>(renderContext, data, SubResourceAt.Zero, null, writeOptions);
    }

    /// <summary>
    /// Writes data to the texture from the specified buffer.
    /// </summary>
    /// <typeparam name="T">Type of data to write to the texture.</typeparam>
    /// <param name="renderContext">The current render context.</param>
    /// <param name="data">Buffer with the data to be copied to the texture.</param>
    /// <param name="subimage">The subimage region, in texels, of the 3D texture to write to, if null the whole image is written to.</param>
    /// <param name="writeOptions">Optional writing options, valid only for dynamic textures.</param>
    /// <exception cref="ObjectDisposedException">Thrown if the resource has already been disposed.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if an error occurs while writing to the resource, or if trying to write to a non-DYNAMIC resource 
    /// using a deferred render context.</exception>
    /// <remarks>See implementors for details on specific exceptions that can be thrown.</remarks>
    public void SetData<T>(IRenderContext renderContext, ReadOnlySpan<T> data, ResourceRegion3D? subimage, DataWriteOptions writeOptions = DataWriteOptions.Discard) where T : unmanaged
    {
      SetData<T>(renderContext, data, SubResourceAt.Zero, subimage, writeOptions);
    }

    /// <summary>
    /// Writes data to the texture from the specified data buffer.
    /// </summary>
    /// <typeparam name="T">Type of data to write to the texture.</typeparam>
    /// <param name="renderContext">The current render context.</param>
    /// <param name="data">Buffer with the data to be copied to the texture.</param>
    /// <param name="subIndex">Optional index to identify which subresource (e.g. mipmap) to access.</param>
    /// <param name="subimage">The subimage region, in texels, of the 3D texture to write to, if null the whole image is written to.</param>
    /// <param name="writeOptions">Optional writing options, valid only for dynamic textures.</param>
    /// <exception cref="ObjectDisposedException">Thrown if the resource has already been disposed.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if an error occurs while writing to the resource, or if trying to write to a non-DYNAMIC resource 
    /// using a deferred render context.</exception>
    /// <remarks>See implementors for details on specific exceptions that can be thrown.</remarks>
    public void SetData<T>(IRenderContext renderContext, ReadOnlySpan<T> data, SubResourceAt subIndex, ResourceRegion3D? subimage = null, DataWriteOptions writeOptions = DataWriteOptions.Discard) where T : unmanaged
    {
      CheckDisposed();
      CheckDeferredSetDataIsPermitted(renderContext, ResourceUsage);

      try
      {
        Texture3DImpl.SetData<T>(renderContext, data, subIndex, subimage, writeOptions);
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw GraphicsExceptionHelper.NewErrorWritingToResourceException(GetType(), e);
      }
    }

    #endregion

    #region ISavable Methods

    /// <inheritdoc />
    public override void Read(ISavableReader input)
    {
      IRenderSystem renderSystem = GraphicsHelper.GetRenderSystem(input.ServiceProvider);

      Name = input.ReadString("Name");
      SurfaceFormat format = input.ReadEnum<SurfaceFormat>("Format");
      ResourceUsage usage = input.ReadEnum<ResourceUsage>("ResourceUsage");
      ResourceBindFlags bindFlags = input.ReadEnum<ResourceBindFlags>("ResourceBindFlags");
      int width = input.ReadInt32("Width");
      int height = input.ReadInt32("Height");
      int depth = input.ReadInt32("Depth");

      int mipCount = input.BeginReadGroup("MipMapChain");
      Debug.Assert(mipCount > 0);

      // Pool buffers to initialize texture
      using (PooledArray<IReadOnlyDataBuffer?> data = new PooledArray<IReadOnlyDataBuffer?>(mipCount) { DisposeContents = true })
      {
        for (int i = 0; i < mipCount; i++)
          data[i] = input.ReadBlob<byte>("MipMap", MemoryAllocatorStrategy.DefaultPooled);

        input.EndReadGroup();

        CreateImplementation(renderSystem, width, height, depth, TextureOptions.Init(data, mipCount > 1, format, usage, bindFlags));
      }
    }

    /// <inheritdoc />
    public override void Write(ISavableWriter output)
    {
      int mipCount = MipCount;
      int width = Width;
      int height = Height;
      int depth = Depth;
      SurfaceFormat format = Format;

      output.Write("Name", Name);
      output.WriteEnum<SurfaceFormat>("Format", format);
      output.WriteEnum<ResourceUsage>("ResourceUsage", ResourceUsage);
      output.WriteEnum<ResourceBindFlags>("ResourceBindFlags", ResourceBindFlags);
      output.Write("Width", width);
      output.Write("Height", height);
      output.Write("Depth", depth);

      output.BeginWriteGroup("MipMapChain", mipCount);

      // Create a pooled buffer for the largest surface and reuse it to gather and write out each surface
      using (DataBuffer<byte> byteBuffer = DataBuffer.Create<byte>(SubResourceAt.Zero.CalculateMipLevelSizeInBytes(width, height, depth, format), MemoryAllocatorStrategy.DefaultPooled))
      {
        for (int i = 0; i < mipCount; i++)
        {
          SubResourceAt subIndex = SubResourceAt.Mip(i);
          int byteCount = subIndex.CalculateMipLevelSizeInBytes(width, height, depth, format);
          Span<byte> span = byteBuffer.Span.Slice(0, byteCount);
          GetData<byte>(span, subIndex);

          output.WriteBlob<byte>("MipMap", span);
        }
      }

      output.EndWriteGroup();
    }

    #endregion

    #region Creation Parameter Validation

    /// <summary>
    /// Validates creation parameters.
    /// </summary>
    /// <param name="adapter">Graphics adapter from the render system.</param>
    /// <param name="width">Width of the texture, in texels.</param>
    /// <param name="height">Height of the texture, in texels.</param>
    /// <param name="depth">Depth of the texture, in texels.</param>
    /// <param name="format">Format of the texture.</param>
    /// <exception cref="System.ArgumentOutOfRangeException">Thrown if the width/height/depth is less than or equal to zero or greater than the maximum texture size.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if the surface format is invalid for the texture dimension.</exception>
    protected void ValidateCreationParameters(IGraphicsAdapter adapter, ref int width, ref int height, ref int depth, SurfaceFormat format)
    {
      int maxSize = adapter.MaximumTexture3DSize;

      if (width <= 0 || width > maxSize)
        throw new ArgumentOutOfRangeException(nameof(width), StringLocalizer.Instance.GetLocalizedString("TextureDimensionOutOfRange"));

      if (height <= 0 || height > maxSize)
        throw new ArgumentOutOfRangeException(nameof(height), StringLocalizer.Instance.GetLocalizedString("TextureDimensionOutOfRange"));

      if (depth <= 0 || depth > maxSize)
        throw new ArgumentOutOfRangeException(nameof(depth), StringLocalizer.Instance.GetLocalizedString("TextureDimensionOutOfRange"));

      if (!adapter.CheckTextureFormat(format, TextureDimension.Three))
        throw GraphicsExceptionHelper.NewBadTextureFormatException(format, TextureDimension.Three);

      CheckBlockCompressionMinimumSize(format, ref width, ref height);
    }

    /// <summary>
    /// Validates creation parameters.
    /// </summary>
    /// <param name="adapter">Graphics adapter from the render system.</param>
    /// <param name="width">Width of the texture, in texels.</param>
    /// <param name="height">Height of the texture, in texels.</param>
    /// <param name="depth">Depth of the texture, in texels.</param>
    /// <param name="mipCount">Number of mip levels in the texture.</param>
    /// <param name="options">Common <see cref="TextureOptions"/> that may also contain initial data.</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the width/height/depth is less than or equal to zero, or greater than the maximum texture size, or if the length of the data 
    /// buffer array does not match the number of mip levels, or if any individual data buffer's size in bytes is larger than the corresponding mip level's size in bytes.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if the surface format is invalid for the texture dimension.</exception>
    protected void ValidateCreationParameters(IGraphicsAdapter adapter, ref int width, ref int height, ref int depth, int mipCount, TextureOptions options)
    {
      ValidateCommonCreationParameters(options, mipCount);
      ValidateCreationParameters(adapter, ref width, ref depth, ref height, options.Format);

      if (options.Data.IsEmpty)
      {
        if (options.ResourceUsage == ResourceUsage.Immutable)
          throw GraphicsExceptionHelper.NewMustSupplyDataForImmutableException();

        return;
      }

      // Validate buffers if span is non-empty
      if (options.Data.Length != mipCount)
        throw new ArgumentOutOfRangeException(nameof(options.Data), StringLocalizer.Instance.GetLocalizedString("DataArrayLengthNotEqualToSubresourceCount"));

      for (int mipLevel = 0; mipLevel < mipCount; mipLevel++)
      {
        int mipSizeInBytes = CalculateMipLevelSizeInBytes(mipLevel, width, height, depth, options.Format); //Accounts for compressed texture size

        IReadOnlyDataBuffer? db = options.Data[mipLevel];

        // If surface is not null, make sure it matches the expected byte size. Allow individual null surfaces even for immutable.
        if (db is not null && db.SizeInBytes != mipSizeInBytes)
          throw new ArgumentOutOfRangeException(nameof(options.Data), StringLocalizer.Instance.GetLocalizedString("DataSizeMustMatchMipLevelSize", mipLevel.ToString(), "0"));
      }
    }

    #endregion

    #region Implementation Creation

    private void CreateImplementation(IRenderSystem renderSystem, int width, int height, int depth, TextureOptions options)
    {
      GraphicsHelper.ThrowIfRenderSystemNull(renderSystem);

      int mipLevels = (options.WantMipMaps) ? CalculateMipMapCount(width, height) : 1;

      ValidateCreationParameters(renderSystem.Adapter, ref width, ref height, ref depth, mipLevels, options);

      ITexture3DImplFactory factory;
      if (!renderSystem.TryGetImplementationFactory<ITexture3DImplFactory>(out factory))
        throw GraphicsExceptionHelper.NewUnsupportedFeatureException(renderSystem, typeof(Texture3D));

      try
      {
        Texture3DImpl = factory.CreateImplementation(width, height, depth, mipLevels, options);
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw GraphicsExceptionHelper.NewErrorCreatingImplementation(typeof(Texture3D), e);
      }
    }

    #endregion
  }
}
