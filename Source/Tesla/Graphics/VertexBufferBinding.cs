﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// Describes how a vertex buffer is to be bound to the graphics pipeline.
  /// </summary>
  public struct VertexBufferBinding : IEquatable<VertexBufferBinding>
  {
    private VertexBuffer m_buffer;
    private int m_vertexOffset;
    private int m_instanceFrequency;

    /// <summary>
    /// Gets the buffer to be bound.
    /// </summary>
    public readonly VertexBuffer VertexBuffer
    {
      get
      {
        return m_buffer;
      }
    }

    /// <summary>
    /// Gets the vertex offset, indicating the first vertex to be used in the buffer (from the start of the buffer). This
    /// value is in whole indices, not bytes.
    /// </summary>
    public readonly int VertexOffset
    {
      get
      {
        return m_vertexOffset;
      }
    }

    /// <summary>
    /// Gets the instance frequency (step rate), which specifies how many times to draw the instance before stepping
    /// one unit forward. If no instancing, this should be zero.
    /// </summary>
    public readonly int InstanceFrequency
    {
      get
      {
        return m_instanceFrequency;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="VertexBufferBinding"/> struct.
    /// </summary>
    /// <param name="buffer">The vertex buffer to bind.</param>
    public VertexBufferBinding(VertexBuffer buffer)
    {
      m_buffer = buffer;
      m_vertexOffset = 0;
      m_instanceFrequency = 0;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="VertexBufferBinding"/> struct.
    /// </summary>
    /// <param name="buffer">The vertex buffer to bind.</param>
    /// <param name="vertexOffset">The vertex offset from the start of the buffer to the first vertex that will be used.</param>
    public VertexBufferBinding(VertexBuffer buffer, int vertexOffset)
    {
      m_buffer = buffer;
      m_vertexOffset = vertexOffset;
      m_instanceFrequency = 0;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="VertexBufferBinding"/> struct.
    /// </summary>
    /// <param name="buffer">The vertex buffer to bind.</param>
    /// <param name="vertexOffset">The vertex offset from the start of the buffer to the first vertex that will be used.</param>
    /// <param name="instanceFrequency">Instance frequency (step rate), which specifies how many times to draw the instance before stepping
    /// one unit forward. If no instancing, this should be zero.</param>
    public VertexBufferBinding(VertexBuffer buffer, int vertexOffset, int instanceFrequency)
    {
      m_buffer = buffer;
      m_vertexOffset = vertexOffset;
      m_instanceFrequency = Math.Max(0, instanceFrequency);
    }

    /// <summary>
    /// Implicitly converts a vertex buffer to the binding with offset of zero and instance frequency of zero.
    /// </summary>
    /// <param name="buffer">The vertex buffer to bind.</param>
    /// <returns>The vertex buffer binding.</returns>
    public static implicit operator VertexBufferBinding(VertexBuffer buffer)
    {
      return new VertexBufferBinding(buffer);
    }

    /// <summary>
    /// Implicitly converts a vertex buffer binding to a vertex buffer.
    /// </summary>
    /// <param name="binding">The vertex buffer binding.</param>
    /// <returns>The vertex buffer.</returns>
    public static implicit operator VertexBuffer(VertexBufferBinding binding)
    {
      return binding.VertexBuffer;
    }

    /// <summary>
    /// Tests equality between two vertex buffer bindings.
    /// </summary>
    /// <param name="a">First binding</param>
    /// <param name="b">Second binding</param>
    /// <returns>True if the two are equal, false otherwise.</returns>
    public static bool operator ==(VertexBufferBinding a, VertexBufferBinding b)
    {
      return (a.m_vertexOffset == b.m_vertexOffset) && (a.m_instanceFrequency == b.m_instanceFrequency) && Object.ReferenceEquals(a.m_buffer, b.m_buffer);
    }

    /// <summary>
    /// Tests inequality between two vertex buffer bindings.
    /// </summary>
    /// <param name="a">First binding</param>
    /// <param name="b">Second binding</param>
    /// <returns>True if the two are not equal, false otherwise.</returns>
    public static bool operator !=(VertexBufferBinding a, VertexBufferBinding b)
    {
      return (a.m_vertexOffset != b.m_vertexOffset) || (a.m_instanceFrequency != b.m_instanceFrequency) || !Object.ReferenceEquals(a.m_buffer, b.m_buffer);
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.</returns>
    public readonly override int GetHashCode()
    {
      unchecked
      {
        int hash = 17;

        hash = (hash * 31) + m_instanceFrequency.GetHashCode();
        hash = (hash * 31) + m_vertexOffset.GetHashCode();
        hash = (hash * 31) + m_buffer.VertexLayout.GetHashCode();

        return hash;
      }
    }

    /// <summary>
    /// Indicates whether this instance and a specified object are equal.
    /// </summary>
    /// <param name="obj">Another object to compare to.</param>
    /// <returns>True if <paramref name="obj" /> and this instance are the same type and represent the same value; otherwise, false.</returns>
    public readonly override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is StreamOutputBufferBinding)
        return Equals((StreamOutputBufferBinding) obj);

      return false;
    }

    /// <summary>
    /// Tests equality between this vertex buffer buffer binding and another, that is if they both bind the same buffer and have the same binding configuration.
    /// </summary>
    /// <param name="other">Other vertex buffer binding to compare against.</param>
    /// <returns>True if the two bindings are equal, false otherwise.</returns>
    public readonly bool Equals(VertexBufferBinding other)
    {
      return (other.m_vertexOffset == m_vertexOffset) && (other.m_instanceFrequency == m_instanceFrequency) && Object.ReferenceEquals(other.m_buffer, m_buffer);
    }

    /// <summary>
    /// Returns the fully qualified type name of this instance.
    /// </summary>
    /// <returns>A <see cref="T:System.String" /> containing a fully qualified type name.</returns>
    public readonly override string ToString()
    {
      return String.Format(CultureInfo.CurrentCulture, "VertexOffset: {0}, InstanceFrequency: {1}, Buffer: {2}", new object?[] { VertexOffset.ToString(), InstanceFrequency.ToString(), (m_buffer is null) ? "null" : m_buffer.ToString() });
    }
  }
}
