﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics;

namespace Tesla.Graphics
{
    /// <summary>
    /// A material pass contains all the necessary state information to configure the render pipeline before a draw call is issued.
    /// </summary>
    [DebuggerDisplay("Name = {Name}, Index = {PassIndex}, ShaderGroup = {ShaderGroup.Name}")]
    public sealed class MaterialPass : INamable
    {
        private Material m_parent;
        private IEffectShaderGroup m_shaderGroup;
        private String m_name;
        private int m_passIndex;
        private bool m_isInherited;
        private bool m_isOverride;

        private BlendState m_blendState;
        private RasterizerState m_rasterizerState;
        private DepthStencilState m_depthStencilState;
        private EnforcedRenderState m_statesToApply;

        /// <summary>
        /// Gets the material that owns this pass.
        /// </summary>
        public Material Parent
        {
            get
            {
                return m_parent;
            }
        }

        /// <summary>
        /// Gets or sets the name of the object.
        /// </summary>
        public String Name
        {
            get
            {
                return m_name;
            }
            set
            {
                if(String.IsNullOrEmpty(value) || value.Equals(m_name))
                    return;

                m_name = value;

                //If the name is changed, then it shouldn't be inheriting anymore nor override
                m_isInherited = false;
                m_isOverride = false;

                //Everytime the pass name changes, make sure our fast look up stays updated. Hopefully this won't be called frequently
                m_parent.Passes.UpdateFastLookup();
            }
        }

        /// <summary>
        /// Gets the index of this pass in the collection it is contained in.
        /// </summary>
        public int PassIndex
        {
            get
            {
                return m_passIndex;
            }
            internal set
            {
                m_passIndex = value;
            }
        }

        /// <summary>
        /// Gets or sets if the pass is inherited from a parent script. If inherited, then it
        /// won't be written out when the material is serialized.
        /// </summary>
        public bool IsInherited
        {
            get
            {
                return m_isInherited;
            }
            set
            {
                m_isInherited = value;
            }
        }

        /// <summary>
        /// Gets if the pass overrides one from a parent script. If <see cref="IsInherited"/> is set to true, and shader/renderstates are changed then the pass is set to 
        /// override. If the name of the pass is changed then its not set to inherited nor override.
        /// </summary>
        public bool IsOverride
        {
            get
            {
                return m_isOverride;
            }
            internal set
            {
                m_isOverride = value;
            }
        }

        /// <summary>
        /// Gets or sets the blend state for the pass. By default, this is <see cref="Tesla.Graphics.BlendState.Opaque" />.
        /// </summary>
        public BlendState BlendState
        {
            get
            {
                return m_blendState;
            }
            set
            {
                if(value == null)
                    value = BlendState.Opaque;

                //If same state...no change if we're inherited
                if (m_blendState.IsSameState(value))
                    return;

                m_blendState = value;
                m_statesToApply |= EnforcedRenderState.BlendState;

                //Modifying value, so no longer inherited, but it now overrides
                ModifyInheritance();
            }
        }

        /// <summary>
        /// Gets or sets the rasterizer state for the pass. By default, this is <see cref="Tesla.Graphics.RasterizerState.CullBackClockwiseFront" />.
        /// </summary>
        public RasterizerState RasterizerState
        {
            get
            {
                return m_rasterizerState;
            }
            set
            {
                if(value == null)
                    value = RasterizerState.CullBackClockwiseFront;

                //If same state...no change if we're inherited
                if (m_rasterizerState.IsSameState(value))
                    return;

                m_rasterizerState = value;
                m_statesToApply |= EnforcedRenderState.RasterizerState;

                //Modifying value, so no longer inherited, but it now overrides
                ModifyInheritance();
            }
        }

        /// <summary>
        /// Gets or sets the depth stencil state for the pass. By default, this is <see cref="Tesla.Graphics.DepthStencilState.Default" />.
        /// </summary>
        public DepthStencilState DepthStencilState
        {
            get
            {
                return m_depthStencilState;
            }
            set
            {
                if(value == null)
                    value = DepthStencilState.Default;

                //If same state...no change if we're inherited
                if (m_depthStencilState.IsSameState(value))
                    return;

                m_depthStencilState = value;
                m_statesToApply |= EnforcedRenderState.DepthStencilState;

                //Modifying value, so no longer inherited, but it now overrides
                ModifyInheritance();
            }
        }

        /// <summary>
        /// Gets or sets the render states that will be applied in this pass. By default, none are. When a render state object is set to the pass, it will
        /// cause the pass to always apply that particular state type. This behavior can be disabled by removing the corresponding flag bit.
        /// </summary>
        public EnforcedRenderState RenderStatesToApply
        {
            get
            {
                return m_statesToApply;
            }
            set
            {
                if (m_statesToApply == value)
                    return;

                m_statesToApply = value;

                //Potentially causing a change in what states are set for pass, so no longer inherited, but it now overrides
                ModifyInheritance();
            }
        }

        /// <summary>
        /// Gets the group of shaders that will be set when this pass is applied.
        /// </summary>
        public IEffectShaderGroup ShaderGroup
        {
            get
            {
                return m_shaderGroup;
            }
            set
            {
                //Do not change if it's the same shader group, or if it's not valid for the effect
                if (value == null || !value.IsPartOf(m_parent.Effect) || m_shaderGroup == value)
                    return;

                m_shaderGroup = value;

                //Modifying value, so no longer inherited, but it now overrides
                ModifyInheritance();
            }
        }

        internal MaterialPass(Material parent, IEffectShaderGroup shaderGroup, String passName, bool isInherited = false, bool isOverride = false)
        {
            m_parent = parent;
            m_shaderGroup = shaderGroup;
            m_name = passName;
            m_passIndex = 0;
            m_isInherited = isInherited;
            m_isOverride = isOverride;

            m_blendState = BlendState.Opaque;
            m_rasterizerState = RasterizerState.CullBackClockwiseFront;
            m_depthStencilState = DepthStencilState.Default;
            m_statesToApply = EnforcedRenderState.All;
        }

        /// <summary>
        /// Applies the pass to the context which will configure the graphics pipeline by setting render states and binding shaders/resources.
        /// </summary>
        /// <param name="renderContext">Render context to apply to.</param>
        public void Apply(IRenderContext renderContext)
        {
            if(renderContext == null)
                return;

            //Apply render states
            if((m_statesToApply & EnforcedRenderState.BlendState) == EnforcedRenderState.BlendState)
                renderContext.BlendState = m_blendState;

            if((m_statesToApply & EnforcedRenderState.RasterizerState) == EnforcedRenderState.RasterizerState)
                renderContext.RasterizerState = m_rasterizerState;

            if((m_statesToApply & EnforcedRenderState.DepthStencilState) == EnforcedRenderState.DepthStencilState)
                renderContext.DepthStencilState = m_depthStencilState;

            //Apply shaders and bind resources
            m_shaderGroup.Apply(renderContext);
        }

        /// <summary>
        /// Sets the shader group to be used by the pass by its name.
        /// </summary>
        /// <param name="name">Name of the shader group contained in the parent material's effect.</param>
        /// <returns>True if the shader group was set, false if it could not be found.</returns>
        public bool SetShaderGroup(String name)
        {
            Effect e = m_parent.Effect;
            IEffectShaderGroup grp = e.ShaderGroups[name];
            if(grp == null || grp == m_shaderGroup)
                return false;

            m_shaderGroup = grp;

            //Modifying value, so no longer inherited, but it now overrides
            ModifyInheritance();

            return true;
        }

        private void ModifyInheritance()
        {
            if (m_isInherited)
            {
                m_isInherited = false;
                m_isOverride = true;
            }
        }
    }
}
