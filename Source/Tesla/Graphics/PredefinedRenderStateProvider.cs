﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// Standard implementation for creating all the engine's prebuilt render states. Also acts as a state cache.
  /// </summary>
  public sealed class PredefinedRenderStateProvider : IPredefinedBlendStateProvider, IPredefinedDepthStencilStateProvider, IPredefinedRasterizerStateProvider, IPredefinedSamplerStateProvider, IRenderStateCache, IDisposable
  {
    private IRenderSystem m_renderSystem;
    private bool m_isDisposed;
    private bool m_isReverseDepth;
    private Dictionary<string, RenderState> m_namesToStates;

    // Cached states, both predefined + user states exist in these
    private HashSet<BlendState> m_cachedBlendStates;
    private HashSet<RasterizerState> m_cachedRasterizerStates;
    private HashSet<DepthStencilState> m_cachedDepthStencilStates;
    private HashSet<SamplerState> m_cachedSamplerStates;

    //Blend states
    private BlendState m_opaque;
    private BlendState m_alphaBlendPremultiplied;
    private BlendState m_additiveBlend;
    private BlendState m_alphaBlendNonPremultiplied;

    //Depthstencil states
    private DepthStencilState m_none;
    private DepthStencilState m_depthWriteOff;
    private DepthStencilState m_defaultDepth;

    //Rasterizer states
    private RasterizerState m_cullNone;
    private RasterizerState m_cullBackClockwiseFront;
    private RasterizerState m_cullBackCounterClockwiseFront;
    private RasterizerState m_cullNonWireframe;

    //Sampler states
    private SamplerState m_pointWrap;
    private SamplerState m_pointClamp;
    private SamplerState m_linearWrap;
    private SamplerState m_linearClamp;
    private SamplerState m_anisotropicWrap;
    private SamplerState m_anisotropicClamp;

    /// <summary>
    /// Gets the rendersystem the prebuilt renderstates are associated with.
    /// </summary>
    public IRenderSystem RenderSystem
    {
      get
      {
        return m_renderSystem;
      }
    }

    /// <summary>
    /// Gets if the provider has been disposed or not.
    /// </summary>
    public bool IsDisposed
    {
      get
      {
        return m_isDisposed;
      }
    }

    /// <inheritdoc />
    public bool IsReverseDepth
    {
      get
      {
        return m_isReverseDepth;
      }
    }

    #region Blend States

    /// <summary>
    /// Gets a predefined state object for opaque blending, where no blending occurs and destination color overwrites source color. This is
    /// the default state.
    /// </summary>
    public BlendState Opaque
    {
      get
      {
        return m_opaque;
      }
    }

    /// <summary>
    /// Gets a predefined state object for premultiplied alpha blending, where source and destination colors are blended by using
    /// alpha, and where the color contains alpha pre-multiplied.
    /// </summary>
    public BlendState AlphaBlendPremultiplied
    {
      get
      {
        return m_alphaBlendPremultiplied;
      }
    }

    /// <summary>
    /// Gets a predefined state object for additive blending, where source and destination color are blended without using alpha.
    /// </summary>
    public BlendState AdditiveBlend
    {
      get
      {
        return m_additiveBlend;
      }
    }

    /// <summary>
    /// Gets a predefined state object for non-premultiplied alpha blending, where the source and destination color are blended by using alpha,
    /// and where the color does not contain the alpha pre-multiplied.
    /// </summary>
    public BlendState AlphaBlendNonPremultiplied
    {
      get
      {
        return m_alphaBlendNonPremultiplied;
      }
    }

    #endregion

    #region DepthStencil States

    /// <inheritdoc />
    public DepthStencilState None { get { return m_none; } }

    /// <inheritdoc />
    public DepthStencilState DepthWriteOff { get { return m_depthWriteOff; } }

    /// <inheritdoc />
    public DepthStencilState Default { get { return m_defaultDepth; } }

    #endregion

    #region Rasterizer States

    /// <summary>
    /// Gets a predefined state object where culling is disabled.
    /// </summary>
    public RasterizerState CullNone
    {
      get
      {
        return m_cullNone;
      }
    }

    /// <summary>
    /// Gets a predefined state object where back faces are culled and front faces have
    /// a clockwise vertex winding. This is the default state.
    /// </summary>
    public RasterizerState CullBackClockwiseFront
    {
      get
      {
        return m_cullBackClockwiseFront;
      }
    }

    /// <summary>
    /// Gets a predefined state object where back faces are culled and front faces have a counterclockwise
    /// vertex winding.
    /// </summary>
    public RasterizerState CullBackCounterClockwiseFront
    {
      get
      {
        return m_cullBackCounterClockwiseFront;
      }
    }

    /// <summary>
    /// Gets a predefined state object where culling is disabled and fillmode is wireframe.
    /// </summary>
    public RasterizerState CullNoneWireframe
    {
      get
      {
        return m_cullNonWireframe;
      }
    }

    #endregion

    #region Sampler States

    /// <summary>
    /// Gets the predefined state object where point filtering is used and UVW coordinates wrap.
    /// </summary>
    public SamplerState PointWrap
    {
      get
      {
        return m_pointWrap;
      }
    }

    /// <summary>
    /// Gets the predefined state object where point filtering is used and UVW coordinates are clamped in the range of [0, 1]. This
    /// is the default state.
    /// </summary>
    public SamplerState PointClamp
    {
      get
      {
        return m_pointClamp;
      }
    }

    /// <summary>
    /// Gets the predefined state object where linear filtering is used and UVW coordinates wrap.
    /// </summary>
    public SamplerState LinearWrap
    {
      get
      {
        return m_linearWrap;
      }
    }

    /// <summary>
    /// Gets the predefined state object where linear filtering is used and UVW coordinates are clamped in the range of [0, 1].
    /// </summary>
    public SamplerState LinearClamp
    {
      get
      {
        return m_linearClamp;
      }
    }

    /// <summary>
    /// Gets the predefined state object where anisotropic filtering is used and UVW coordinates wrap.
    /// </summary>
    public SamplerState AnisotropicWrap
    {
      get
      {
        return m_anisotropicWrap;
      }
    }

    /// <summary>
    /// Gets the predefined state object where anisotropic filtering is used and UVW coordinates are
    /// clamped in the range of [0, 1].
    /// </summary>
    public SamplerState AnisotropicClamp
    {
      get
      {
        return m_anisotropicClamp;
      }
    }

    #endregion

    /// <summary>
    /// Constructs a new instance of the <see cref="PredefinedRenderStateProvider"/> class.
    /// </summary>
    /// <param name="renderSystem">The render system used to create the renderstates.</param>
    /// <param name="useReverseDepth">True to use the reverse depth technique across the entire render system, false if to use the traditional technique.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the rendersystem is null.</exception>
    public PredefinedRenderStateProvider(IRenderSystem renderSystem, bool useReverseDepth = false)
    {
      GraphicsHelper.ThrowIfRenderSystemNull(renderSystem);

      m_renderSystem = renderSystem;
      m_isDisposed = false;
      m_isReverseDepth = useReverseDepth;
      m_namesToStates = new Dictionary<string, RenderState>();
      m_cachedBlendStates = new HashSet<BlendState>();
      m_cachedDepthStencilStates = new HashSet<DepthStencilState>();
      m_cachedRasterizerStates = new HashSet<RasterizerState>();
      m_cachedSamplerStates = new HashSet<SamplerState>();

      EngineLog.Log(LogLevel.Info, "Creating Predefined RenderStates");

      CreateBlendStates();
      CreateDepthStencilStates();
      CreateRasterizerStates();
      CreateSamplerStates();
    }

    /// <inheritdoc />
    public T GetOrCache<T>(T state) where T : RenderState?
    {
      if (state is null)
        return null!;

      switch (state)
      {
        case BlendState bs:
          return (GetOrCache<BlendState>(m_cachedBlendStates, bs) as T)!;
        case RasterizerState rs:
          return (GetOrCache<RasterizerState>(m_cachedRasterizerStates, rs) as T)!;
        case DepthStencilState ds:
          return (GetOrCache<DepthStencilState>(m_cachedDepthStencilStates, ds) as T)!;
        case SamplerState ss:
          return (GetOrCache<SamplerState>(m_cachedSamplerStates, ss) as T)!;
        default:
          return state; // Shouldn't happen
      }
    }

    /// <summary>
    /// Queries a predefined render state by its name.
    /// </summary>
    /// <typeparam name="T">Render state type</typeparam>
    /// <param name="name">Name of the render state</param>
    /// <returns>The render state, or null if it was not found.</returns>
    public T? GetRenderStateByName<T>(string name) where T : RenderState
    {
      RenderState? rs;
      if (m_namesToStates.TryGetValue(name, out rs))
        return rs as T;

      return null;
    }

    /// <summary>
    /// Queries a predefined blend state by name.
    /// </summary>
    /// <param name="name">Name of the blend state.</param>
    /// <returns>Blend state, or null if it does not exist.</returns>
    public BlendState? GetBlendStateByName(string name)
    {
      return GetRenderStateByName<BlendState>(name);
    }

    /// <summary>
    /// Queries a predefined depth stencil state by name.
    /// </summary>
    /// <param name="name">Name of the depth stencil state.</param>
    /// <returns>DepthStencil state, or null if it does not exist.</returns>
    public DepthStencilState? GetDepthStencilStateByName(string name)
    {
      return GetRenderStateByName<DepthStencilState>(name);
    }

    /// <summary>
    /// Queries a predefined rasterizer state by name.
    /// </summary>
    /// <param name="name">Name of the rasterizer state.</param>
    /// <returns>Rasterizer state, or null if it does not exist.</returns>
    public RasterizerState? GetRasterizerStateByName(string name)
    {
      return GetRenderStateByName<RasterizerState>(name);
    }

    /// <summary>
    /// Queries a predefined sampler state by name.
    /// </summary>
    /// <param name="name">Name of the sampler state.</param>
    /// <returns>Sampler state, or null if it does not exist.</returns>
    public SamplerState? GetSamplerStateByName(string name)
    {
      return GetRenderStateByName<SamplerState>(name);
    }

    /// <summary>
    /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
    /// </summary>
    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    private void Dispose(bool disposing)
    {
      if (!m_isDisposed)
      {
        if (disposing)
        {
          m_namesToStates.Clear();

          EngineLog.Log(LogLevel.Info, "Destroying Predefined RenderStates");

          foreach (BlendState bs in m_cachedBlendStates)
            bs.Dispose();

          foreach (RasterizerState rs in m_cachedRasterizerStates)
            rs.Dispose();

          foreach (DepthStencilState ds in m_cachedDepthStencilStates)
            ds.Dispose();

          foreach (SamplerState ss in m_cachedSamplerStates)
            ss.Dispose();

          m_cachedBlendStates.Clear();
          m_cachedRasterizerStates.Clear();
          m_cachedDepthStencilStates.Clear();
          m_cachedSamplerStates.Clear();
        }

        m_isDisposed = true;
      }
    }

    private T GetOrCache<T>(HashSet<T> set, T state) where T : RenderState
    {
      lock (set)
      {
        if (set.TryGetValue(state, out T? actualValue))
        {
          state.Dispose();
          return actualValue;
        }

        state.BindRenderState();
        set.Add(state);
        return state;
      }
    }

    [MemberNotNull(nameof(m_opaque), nameof(m_alphaBlendNonPremultiplied), nameof(m_alphaBlendPremultiplied), nameof(m_additiveBlend))]
    private void CreateBlendStates()
    {
      m_opaque = new BlendState(m_renderSystem);
      m_opaque.BlendEnable = false;
      m_opaque.BindRenderState();
      AddRenderState("Opaque", m_opaque);

      m_alphaBlendPremultiplied = new BlendState(m_renderSystem);
      m_alphaBlendPremultiplied.BlendEnable = true;
      m_alphaBlendPremultiplied.AlphaSourceBlend = Blend.One;
      m_alphaBlendPremultiplied.AlphaDestinationBlend = Blend.InverseSourceAlpha;
      m_alphaBlendPremultiplied.ColorSourceBlend = Blend.One;
      m_alphaBlendPremultiplied.ColorDestinationBlend = Blend.InverseSourceAlpha;
      m_alphaBlendPremultiplied.BindRenderState();
      AddRenderState("AlphaBlendPremultiplied", m_alphaBlendPremultiplied);

      m_alphaBlendNonPremultiplied = new BlendState(m_renderSystem);
      m_alphaBlendNonPremultiplied.BlendEnable = true;
      m_alphaBlendNonPremultiplied.AlphaSourceBlend = Blend.SourceAlpha;
      m_alphaBlendNonPremultiplied.AlphaDestinationBlend = Blend.InverseSourceAlpha;
      m_alphaBlendNonPremultiplied.ColorSourceBlend = Blend.SourceAlpha;
      m_alphaBlendNonPremultiplied.ColorDestinationBlend = Blend.InverseSourceAlpha;
      m_alphaBlendNonPremultiplied.BindRenderState();
      AddRenderState("AlphaBlendNonPremultiplied", m_alphaBlendNonPremultiplied);

      m_additiveBlend = new BlendState(m_renderSystem);
      m_additiveBlend.BlendEnable = true;
      m_additiveBlend.AlphaSourceBlend = Blend.SourceAlpha;
      m_additiveBlend.AlphaDestinationBlend = Blend.One;
      m_additiveBlend.ColorSourceBlend = Blend.SourceAlpha;
      m_additiveBlend.ColorDestinationBlend = Blend.One;
      m_additiveBlend.BindRenderState();
      AddRenderState("AdditiveBlend", m_additiveBlend);
    }

    [MemberNotNull(nameof(m_none), nameof(m_depthWriteOff), nameof(m_defaultDepth))]
    private void CreateDepthStencilStates()
    {
      m_none = new DepthStencilState(m_renderSystem);
      m_none.DepthEnable = false;
      m_none.DepthWriteEnable = false;
      m_none.BindRenderState();
      AddRenderState("None", m_none);

      m_depthWriteOff = new DepthStencilState(m_renderSystem);
      m_depthWriteOff.DepthEnable = true;
      m_depthWriteOff.DepthWriteEnable = false;

      if (m_isReverseDepth)
        m_depthWriteOff.DepthFunction = ComparisonFunction.GreaterEqual;

      m_depthWriteOff.BindRenderState();
      AddRenderState("DepthWriteOff", m_depthWriteOff);

      m_defaultDepth = new DepthStencilState(m_renderSystem);
      m_defaultDepth.DepthEnable = true;
      m_defaultDepth.DepthWriteEnable = true;
      if (m_isReverseDepth)
        m_defaultDepth.DepthFunction = ComparisonFunction.GreaterEqual;

      m_defaultDepth.BindRenderState();
      AddRenderState("Default", m_defaultDepth);
    }

    [MemberNotNull(nameof(m_cullNone), nameof(m_cullBackClockwiseFront), nameof(m_cullBackCounterClockwiseFront), nameof(m_cullNonWireframe))]
    private void CreateRasterizerStates()
    {
      m_cullNone = new RasterizerState(m_renderSystem);
      m_cullNone.Cull = CullMode.None;
      m_cullNone.BindRenderState();
      AddRenderState("CullNone", m_cullNone);

      m_cullBackClockwiseFront = new RasterizerState(m_renderSystem);
      m_cullBackClockwiseFront.Cull = CullMode.Back;
      m_cullBackClockwiseFront.VertexWinding = VertexWinding.Clockwise;
      m_cullBackClockwiseFront.BindRenderState();
      AddRenderState("CullBackClockwiseFront", m_cullBackClockwiseFront);

      m_cullBackCounterClockwiseFront = new RasterizerState(m_renderSystem);
      m_cullBackCounterClockwiseFront.Cull = CullMode.Back;
      m_cullBackCounterClockwiseFront.VertexWinding = VertexWinding.CounterClockwise;
      m_cullBackCounterClockwiseFront.BindRenderState();
      AddRenderState("CullBackCounterClockwiseFront", m_cullBackCounterClockwiseFront);

      m_cullNonWireframe = new RasterizerState(m_renderSystem);
      m_cullNonWireframe.Cull = CullMode.None;
      m_cullNonWireframe.Fill = FillMode.WireFrame;
      m_cullNonWireframe.BindRenderState();
      AddRenderState("CullNoneWireframe", m_cullNonWireframe);
    }

    [MemberNotNull(nameof(m_pointWrap), nameof(m_pointClamp), nameof(m_linearWrap), nameof(m_linearClamp), nameof(m_anisotropicWrap), nameof(m_anisotropicClamp))]
    private void CreateSamplerStates()
    {
      m_pointWrap = new SamplerState(m_renderSystem);
      m_pointWrap.Filter = TextureFilter.Point;
      m_pointWrap.AddressU = TextureAddressMode.Wrap;
      m_pointWrap.AddressV = TextureAddressMode.Wrap;
      m_pointWrap.AddressW = TextureAddressMode.Wrap;
      m_pointWrap.BindRenderState();
      AddRenderState("PointWrap", m_pointWrap);

      m_pointClamp = new SamplerState(m_renderSystem);
      m_pointClamp.Filter = TextureFilter.Point;
      m_pointClamp.AddressU = TextureAddressMode.Clamp;
      m_pointClamp.AddressV = TextureAddressMode.Clamp;
      m_pointClamp.AddressW = TextureAddressMode.Clamp;
      m_pointClamp.BindRenderState();
      AddRenderState("PointClamp", m_pointClamp);

      m_linearWrap = new SamplerState(m_renderSystem);
      m_linearWrap.Filter = TextureFilter.Linear;
      m_linearWrap.AddressU = TextureAddressMode.Wrap;
      m_linearWrap.AddressV = TextureAddressMode.Wrap;
      m_linearWrap.AddressW = TextureAddressMode.Wrap;
      m_linearWrap.BindRenderState();
      AddRenderState("LinearWrap", m_linearWrap);

      m_linearClamp = new SamplerState(m_renderSystem);
      m_linearClamp.Filter = TextureFilter.Linear;
      m_linearClamp.AddressU = TextureAddressMode.Clamp;
      m_linearClamp.AddressV = TextureAddressMode.Clamp;
      m_linearClamp.AddressW = TextureAddressMode.Clamp;
      m_linearClamp.BindRenderState();
      AddRenderState("LinearClamp", m_linearClamp);

      m_anisotropicWrap = new SamplerState(m_renderSystem);
      m_anisotropicWrap.Filter = TextureFilter.Anisotropic;
      m_anisotropicWrap.AddressU = TextureAddressMode.Wrap;
      m_anisotropicWrap.AddressV = TextureAddressMode.Wrap;
      m_anisotropicWrap.AddressW = TextureAddressMode.Wrap;
      m_anisotropicWrap.BindRenderState();
      AddRenderState("AnisotropicWrap", m_anisotropicWrap);

      m_anisotropicClamp = new SamplerState(m_renderSystem);
      m_anisotropicClamp.Filter = TextureFilter.Anisotropic;
      m_anisotropicClamp.AddressU = TextureAddressMode.Clamp;
      m_anisotropicClamp.AddressV = TextureAddressMode.Clamp;
      m_anisotropicClamp.AddressW = TextureAddressMode.Clamp;
      m_anisotropicClamp.BindRenderState();
      AddRenderState("AnisotropicClamp", m_anisotropicClamp);
    }

    private void AddRenderState(string name, RenderState rs)
    {
      rs.m_predefinedStateName = name;
      rs.Name = name;
      rs.DebugName = name;

      m_namesToStates.Add(name, rs);

      switch(rs)
      {
        case BlendState bs:
          m_cachedBlendStates.Add(bs);
          break;
        case DepthStencilState ds:
          m_cachedDepthStencilStates.Add(ds);
          break;
        case RasterizerState rrs:
          m_cachedRasterizerStates.Add(rrs);
          break;
        case SamplerState ss:
          m_cachedSamplerStates.Add(ss);
          break;
      }
    }
  }
}
