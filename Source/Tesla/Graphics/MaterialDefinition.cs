﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Tesla.Content;

namespace Tesla.Graphics
{
    /// <summary>
    /// Defines a group of materials that are used to draw geometry. Each material has a shader effect and parameters, and is
    /// associated with a render bucket. An object may require multiple materials to achieve a complete rendering (e.g. one for
    /// shadows, one for opaque, one for special effects like emissive glow, etc), and all those materials will be contained in this
    /// material definition.
    /// </summary>
    [SavableVersion(1)]
    [DebuggerDisplay("Name = {Name}, ScriptFile = {ScriptFileName}, Count = {Count}")]
    public class MaterialDefinition : ISavable, INamable, IReadOnlyDictionary<RenderBucketID, Material>
    {
        private String m_name;
        private String m_scriptFileName;
        private Dictionary<RenderBucketID, Material> m_materials;

        /// <summary>
        /// Gets or sets the name of the object.
        /// </summary>
        public String Name
        {
            get
            {
                return m_name;
            }
            set
            {
                m_name = value;
            }
        }

        /// <summary>
        /// Gets or sets the name of the script file, for serialization to a "TEMD" script.
        /// </summary>
        public String ScriptFileName
        {
            get
            {
                return m_scriptFileName;
            }
            set
            {
                m_scriptFileName = value;
            }
        }

        /// <summary>
        /// Gets the number of materials contained.
        /// </summary>
        public int Count
        {
            get
            {
                return m_materials.Count;
            }
        }

        /// <summary>
        /// Gets all the bucket IDs associated with materials.
        /// </summary>
        public Dictionary<RenderBucketID, Material>.KeyCollection Keys
        {
            get
            {
                return m_materials.Keys;
            }
        }

        /// <summary>
        /// Gets all the materials.
        /// </summary>
        public Dictionary<RenderBucketID, Material>.ValueCollection Values
        {
            get
            {
                return m_materials.Values;
            }
        }

        /// <summary>
        /// Gets all the bucket IDs associated with materials.
        /// </summary>
        IEnumerable<RenderBucketID> IReadOnlyDictionary<RenderBucketID, Material>.Keys
        {
            get
            {
                return m_materials.Keys;
            }
        }

        /// <summary>
        /// Gets all the materials.
        /// </summary>
        IEnumerable<Material> IReadOnlyDictionary<RenderBucketID, Material>.Values
        {
            get
            {
                return m_materials.Values;
            }
        }

        /// <summary>
        /// Gets a material associated with the render bucket ID.
        /// </summary>
        /// <param name="key">Render bucket ID.</param>
        /// <returns>The associated material.</returns>
        public Material this[RenderBucketID key]
        {
            get
            {
                return m_materials[key];
            }
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="MaterialDefinition"/> class.
        /// </summary>
        public MaterialDefinition() : this(null, 0) { }

        /// <summary>
        /// Constructs a new instance of the <see cref="MaterialDefinition"/> class.
        /// </summary>
        /// <param name="name">Name of the material definition</param>
        public MaterialDefinition(String name) : this(name, 0) { }

        /// <summary>
        /// Constructs a new instance of the <see cref="MaterialDefinition"/> class.
        /// </summary>
        /// <param name="name">Name of the material definition</param>
        /// <param name="initialCapacity">Initial capacity of the collection.</param>
        public MaterialDefinition(String name, int initialCapacity)
        {
            m_name = (String.IsNullOrEmpty(name)) ? "MaterialDefinition" : name;
            m_scriptFileName = String.Empty;
            m_materials = new Dictionary<RenderBucketID, Material>(initialCapacity, new RenderBucketIDEqualityComparer());
        }

        /// <summary>
        /// Queries if all materials are valid in the definition. If a material is null, then it is considered valid.
        /// </summary>
        /// <returns>True if all materials are valid, false if otherwise.</returns>
        public bool AreMaterialsValid()
        {
            foreach(KeyValuePair<RenderBucketID, Material> kv in m_materials)
            {
                //A null material does not mean it is invalid
                if(kv.Value != null && !kv.Value.IsValid)
                    return false;
            }

            return true;
        }

        /// <summary>
        /// Adds a material to the definition which will be used for drawing with the associated render bucket. Null values
        /// are permitted.
        /// </summary>
        /// <param name="bucketID">Bucket the material will be used in.</param>
        /// <param name="material">Material that will be applied when an object is drawn, it can safely be null.</param>
        /// <returns>True if the material was scuccessfully added, false if another entry in the definition is associated with the bucket id.</returns>
        public bool Add(RenderBucketID bucketID, Material material)
        {
            //Allow null
            if (!m_materials.ContainsKey(bucketID))
            {
                m_materials.Add(bucketID, material);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Queries a material associated with <see cref="RenderBucketID.Opaque"/> and if there isn't one, then the first material
        /// defined in the collection.
        /// </summary>
        /// <returns>The material, or null if no materials are present. Possibly may be null for a null material.</returns>
        public Material GetOpaqueOrFirstMaterial()
        {
            if(m_materials.Count == 0)
                return null;

            Material mat;
            if(m_materials.TryGetValue(RenderBucketID.Opaque, out mat))
                return mat;

            foreach(KeyValuePair<RenderBucketID, Material> kv in m_materials)
            {
                mat = kv.Value;
                break;
            }

            return mat;
        }

        public bool ReAssign(RenderBucketID oldBucketID, RenderBucketID newBucketID)
        {
            Material mat;
            if(m_materials.TryGetValue(oldBucketID, out mat))
            {
                m_materials.Remove(oldBucketID);
                if(m_materials.ContainsKey(newBucketID))
                    m_materials.Remove(newBucketID);

                m_materials.Add(newBucketID, mat);
            }

            return false;
        }

        public bool Remove(RenderBucketID bucketID)
        {
            return m_materials.Remove(bucketID);
        }

        public bool ContainsKey(RenderBucketID bucketID)
        {
            return m_materials.ContainsKey(bucketID);
        }

        public bool ContainsValue(Material material)
        {
            return m_materials.ContainsValue(material);
        }

        public bool TryGetValue(RenderBucketID bucketID, out Material material)
        {
            return m_materials.TryGetValue(bucketID, out material);
        }

        public void Clear()
        {
            m_materials.Clear();
        }

        public MaterialDefinition Clone()
        {
            MaterialDefinition matDef = new MaterialDefinition(m_name, m_materials.Count);
            matDef.m_scriptFileName = m_scriptFileName;

            foreach(KeyValuePair<RenderBucketID, Material> kv in m_materials)
                matDef.Add(kv.Key, (kv.Value != null) ? kv.Value.Clone() : null);

            return matDef;
        }

        public Dictionary<RenderBucketID, Material>.Enumerator GetEnumerator()
        {
            return m_materials.GetEnumerator();
        }

        IEnumerator<KeyValuePair<RenderBucketID, Material>> IEnumerable<KeyValuePair<RenderBucketID, Material>>.GetEnumerator()
        {
            return m_materials.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return m_materials.GetEnumerator();
        }

        /// <summary>
        /// Reads the object data from the input.
        /// </summary>
        /// <param name="input">Savable reader</param>
        public void Read(ISavableReader input)
        {
            int count = input.BeginReadGroup("Materials");

            for(int i = 0; i < count; i++)
            {
                input.BeginReadGroup("Entry");

                String bucketName = input.ReadString("RenderBucket");
                Material mat = input.ReadSavable<Material>("Material");

                m_materials.Add(RenderBucketID.QueryID(bucketName), mat);

                input.EndReadGroup();
            }

            input.EndReadGroup();
        }

        /// <summary>
        /// Writes the object data to the output.
        /// </summary>
        /// <param name="output">Savable writer</param>
        public void Write(ISavableWriter output)
        {
            output.BeginWriteGroup("Materials", m_materials.Count);

            foreach(KeyValuePair<RenderBucketID, Material> kv in m_materials)
            {
                output.BeginWriteGroup("Entry");

                output.Write("RenderBucket", kv.Key.Name);
                output.WriteSavable<Material>("Material", kv.Value);

                output.EndWriteGroup();
            }

            output.EndWriteGroup();
        }

        #region ID equality comparer

        private sealed class RenderBucketIDEqualityComparer : IEqualityComparer<RenderBucketID>
        {
            public RenderBucketIDEqualityComparer() { }

            public bool Equals(RenderBucketID x, RenderBucketID y)
            {
                return x == y;
            }

            public int GetHashCode(RenderBucketID obj)
            {
                return obj.Value;
            }
        }

        #endregion
    }
}
