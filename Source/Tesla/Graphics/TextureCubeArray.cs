﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics;
using Tesla.Content;
using Tesla.Graphics.Implementation;

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// Represents an array of Cube-Dimensional textures (6 faces) where each face is a square 2D texture with width/height.
  /// </summary>
  [SavableVersion(1)]
  public class TextureCubeArray : TextureCube
  {
    #region Public Properties

    /// <summary>
    /// Gets the number of cubemap textures in the array. Cubemaps may be indexed in the range [0, ArrayCount).
    /// </summary>
    public int ArrayCount
    {
      get
      {
        return TextureCubeArrayImpl.ArrayCount;
      }
    }

    /// <inheritdoc />
    public override ShaderResourceType ResourceType
    {
      get
      {
        return ShaderResourceType.TextureCubeArray;
      }
    }

    #endregion

    //Private property to cast the implementation
    private ITextureCubeArrayImpl TextureCubeArrayImpl
    {
      get
      {
        return GetImplAs<ITextureCubeArrayImpl>();
      }
      set
      {
        BindImplementation(value);
      }
    }

    /// <summary>
    /// For ISavable only.
    /// </summary>
    protected TextureCubeArray() { }

    /// <summary>
    /// Constructs a new instance of the <see cref="TextureCubeArray"/> class.
    /// </summary>
    /// <param name="renderSystem">Render system used to create the underlying implementation.</param>
    /// <param name="size">Size (width/height) of the texture, in texels.</param>
    /// <param name="arrayCount">Number of cubemaps, must be greater than zero.</param>
    /// <param name="options">Common <see cref="TextureOptions"/> that may also contain initial data.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the render system is null or if any input is null when it is required (e.g. initial data is required for immutable resources).</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the size (width/height) is less than or equal to zero, or greater than the maximum texture size.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if creating the underlying implementation fails or is unsupported, see inner exception for a more detailed error.</exception>
    public TextureCubeArray(IRenderSystem renderSystem, int size, int arrayCount, TextureOptions options = default)
    {
      CreateImplementation(renderSystem, size, arrayCount, options);
    }

    #region Public Methods

    /// <summary>
    /// Gets a sub texture at the specified array index.
    /// </summary>
    /// <param name="arrayIndex">Zero-based index of the sub texture.</param>
    /// <returns>The sub texture.</returns>
    /// <exception cref="TeslaGraphicsException">Thrown if an error occurs while retrieving the sub texture.</exception>
    /// <remarks>See implementors for details on specific exceptions that can be thrown.</remarks>
    public IShaderResource? GetSubTexture(int arrayIndex)
    {
      try
      {
        return TextureCubeArrayImpl.GetSubTexture(arrayIndex);
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw new TeslaGraphicsException(StringLocalizer.Instance.GetLocalizedString("TextureArrayErrorRetrievingSubTexture"), e);
      }
    }

    #endregion

    #region ISavable Methods

    /// <inheritdoc />
    public override void Read(ISavableReader input)
    {
      IRenderSystem renderSystem = GraphicsHelper.GetRenderSystem(input.ServiceProvider);

      Name = input.ReadString("Name");
      SurfaceFormat format = input.ReadEnum<SurfaceFormat>("Format");
      ResourceUsage resourceUsage = input.ReadEnum<ResourceUsage>("ResourceUsage");
      ResourceBindFlags bindFlags = input.ReadEnum<ResourceBindFlags>("ResourceBindFlags");
      int size = input.ReadInt32("Size");
      int expectedMipCount = input.ReadInt32("MipCount");
      Debug.Assert(expectedMipCount > 0);

      int arrayCount = input.BeginReadGroup("Slices");
      Debug.Assert(arrayCount > 0);

      // Pool buffers to initialize texture
      using (PooledArray<IReadOnlyDataBuffer?> data = new PooledArray<IReadOnlyDataBuffer?>(CubeMapFaceCount * arrayCount * expectedMipCount) { DisposeContents = true })
      {
        for (int cubeMapIndex = 0; cubeMapIndex < arrayCount; cubeMapIndex++)
        {
          int faceCount = input.BeginReadGroup("Faces");
          Debug.Assert(faceCount == CubeMapFaceCount); //Faces must always be an array of 6

          for (int i = 0; i < faceCount; i++)
          {
            CubeMapFace face = (CubeMapFace) i;
            int mipChainCount = input.BeginReadGroup("MipMapChain");
            Debug.Assert(expectedMipCount == mipChainCount);

            for (int j = 0; j < mipChainCount; j++)
            {
              SubResourceAt subIndex = SubResourceAt.MipFace(j, face, cubeMapIndex);
              data[subIndex.CalculateSubResourceIndex(expectedMipCount)] = input.ReadBlob<byte>("MipMap", MemoryAllocatorStrategy.DefaultPooled);
            }

            input.EndReadGroup();
          }

          input.EndReadGroup();
        }

        input.EndReadGroup();

        CreateImplementation(renderSystem, size, arrayCount, TextureOptions.Init(data, expectedMipCount > 1, format, resourceUsage, bindFlags));
      }
    }

    /// <inheritdoc />
    public override void Write(ISavableWriter output)
    {
      int arrayCount = ArrayCount;
      int mipCount = MipCount;
      int size = Size;
      SurfaceFormat format = Format;

      output.Write("Name", Name);
      output.WriteEnum<SurfaceFormat>("Format", format);
      output.WriteEnum<ResourceUsage>("ResourceUsage", ResourceUsage);
      output.WriteEnum<ResourceBindFlags>("ResourceBindFlags", ResourceBindFlags);
      output.Write("Size", size);
      output.Write("MipCount", mipCount);

      output.BeginWriteGroup("Slices", arrayCount);

      // Create a pooled buffer for the largest surface and reuse it to gather and write out each surface
      using (DataBuffer<byte> byteBuffer = DataBuffer.Create<byte>(SubResourceAt.Zero.CalculateMipLevelSizeInBytes(size, size, format), MemoryAllocatorStrategy.DefaultPooled))
      {
        for (int cubeMapIndex = 0; cubeMapIndex < arrayCount; cubeMapIndex++)
        {
          output.BeginWriteGroup("Faces", CubeMapFaceCount);

          for (int i = 0; i < CubeMapFaceCount; i++)
          {
            output.BeginWriteGroup("MipMapChain", mipCount);

            CubeMapFace face = (CubeMapFace) i;
            for (int j = 0; j < mipCount; j++)
            {
              SubResourceAt subIndex = SubResourceAt.MipFace(j, face);
              int byteCount = subIndex.CalculateMipLevelSizeInBytes(size, size, format);
              Span<byte> span = byteBuffer.Span.Slice(0, byteCount);
              GetData<byte>(span, subIndex);

              output.WriteBlob<byte>("MipMap", span);
            }

            output.EndWriteGroup();
          }

          output.EndWriteGroup();
        }
      }

      output.EndWriteGroup();
    }

    #endregion

    #region Implementation Creation

    private void CreateImplementation(IRenderSystem renderSystem, int size, int arrayCount, TextureOptions options)
    {
      GraphicsHelper.ThrowIfRenderSystemNull(renderSystem);

      int mipLevels = (options.WantMipMaps) ? CalculateMipMapCount(size, size) : 1;

      ValidateCreationParameters(renderSystem.Adapter, ref size, arrayCount, mipLevels, options);

      ITextureCubeArrayImplFactory factory;
      if (!renderSystem.TryGetImplementationFactory<ITextureCubeArrayImplFactory>(out factory))
        throw GraphicsExceptionHelper.NewUnsupportedFeatureException(renderSystem, typeof(TextureCubeArray));

      try
      {
        TextureCubeArrayImpl = factory.CreateImplementation(size, arrayCount, mipLevels, options);
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw GraphicsExceptionHelper.NewErrorCreatingImplementation(typeof(TextureCubeArray), e);
      }
    }

    #endregion
  }
}
