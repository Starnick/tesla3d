﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

// TODO Nullable

namespace Tesla.Graphics
{
  /// <summary>
  /// Represents a collection of effect parameters.
  /// </summary>
  public sealed class EffectParameterCollection : ReadOnlyNamedListFast<IEffectParameter>
  {
    /// <summary>
    /// Empty parameter collection.
    /// </summary>
    public static readonly EffectParameterCollection EmptyCollection = new EffectParameterCollection();

    /// <summary>
    /// Constructs a new instance of the <see cref="EffectParameterCollection"/> class.
    /// </summary>
    /// <param name="parameters">Effect parameters.</param>
    public EffectParameterCollection(params IEffectParameter[] parameters) : base(parameters) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="EffectParameterCollection"/> class.
    /// </summary>
    /// <param name="parameters">Effect parameters.</param>
    public EffectParameterCollection(IEnumerable<IEffectParameter> parameters) : base(parameters) { }

    /// <summary>
    /// Prevents a default instance of the <see cref="EffectParameterCollection"/> class from being created.
    /// </summary>
    private EffectParameterCollection() { }
  }

  /// <summary>
  /// Represents a collection of shader groups.
  /// </summary>
  public sealed class EffectShaderGroupCollection : ReadOnlyNamedListFast<IEffectShaderGroup>
  {
    /// <summary>
    /// Empty shader group collection.
    /// </summary>
    public static readonly EffectShaderGroupCollection EmptyCollection = new EffectShaderGroupCollection();

    /// <summary>
    /// Constructs a new instance of the <see cref="EffectShaderGroupCollection"/> class.
    /// </summary>
    /// <param name="shaderGroups">Effect shader groups.</param>
    public EffectShaderGroupCollection(params IEffectShaderGroup[] shaderGroups) : base(shaderGroups) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="EffectShaderGroupCollection"/> class.
    /// </summary>
    /// <param name="shaderGroups">Effect shader groups.</param>
    public EffectShaderGroupCollection(IEnumerable<IEffectShaderGroup> shaderGroups) : base(shaderGroups) { }

    /// <summary>
    /// Prevents a default instance of the <see cref="EffectShaderGroupCollection"/> class from being created.
    /// </summary>
    private EffectShaderGroupCollection() { }
  }

  /// <summary>
  /// Represents a collection of effect constant buffers.
  /// </summary>
  public sealed class EffectConstantBufferCollection : ReadOnlyNamedListFast<IEffectConstantBuffer>
  {
    /// <summary>
    /// Empty constant buffer collection.
    /// </summary>
    public static readonly EffectConstantBufferCollection EmptyCollection = new EffectConstantBufferCollection();

    /// <summary>
    /// Constructs a new instance of the <see cref="EffectConstantBufferCollection"/> class.
    /// </summary>
    /// <param name="constantBuffers">Effect constant buffers.</param>
    public EffectConstantBufferCollection(params IEffectConstantBuffer[] constantBuffers) : base(constantBuffers) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="EffectConstantBufferCollection"/> class.
    /// </summary>
    /// <param name="constantBuffers">Effect constant buffers.</param>
    public EffectConstantBufferCollection(IEnumerable<IEffectConstantBuffer> constantBuffers) : base(constantBuffers) { }

    /// <summary>
    /// Prevents a default instance of the <see cref="EffectConstantBufferCollection"/> class from being created.
    /// </summary>
    private EffectConstantBufferCollection() { }
  }

  /// <summary>
  /// Represents an enumerable collection of parameters from a collection of constant buffers and resource variables.
  /// </summary>
  public struct CompositeEffectParameterCollection : IEnumerable<IEffectParameter>
  {
    private IReadOnlyList<IEffectConstantBuffer> m_constantBuffers;
    private IReadOnlyList<IEffectParameter> m_resourceVariables;

    /// <summary>
    /// Constructs a new instance of the <see cref="CompositeEffectParameterCollection"/> struct.
    /// </summary>
    /// <param name="constantBuffers">The constant buffers.</param>
    /// <param name="resourceVariables">The resource variables.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if either collection is null.</exception>
    public CompositeEffectParameterCollection(IReadOnlyList<IEffectConstantBuffer> constantBuffers, IReadOnlyList<IEffectParameter> resourceVariables)
    {
      if (constantBuffers == null)
        throw new ArgumentNullException("constantBuffers");

      if (resourceVariables == null)
        throw new ArgumentNullException("resourecVariables");

      m_constantBuffers = constantBuffers;
      m_resourceVariables = resourceVariables;
    }

    /// <summary>
    /// Returns an enumerator that iterates through the collection.
    /// </summary>
    /// <returns>A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.</returns>
    public CompositeEffectParameterEnumerator GetEnumerator()
    {
      return new CompositeEffectParameterEnumerator(m_constantBuffers, m_resourceVariables);
    }

    /// <summary>
    /// Returns an enumerator that iterates through the collection.
    /// </summary>
    /// <returns>A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.</returns>
    IEnumerator<IEffectParameter> IEnumerable<IEffectParameter>.GetEnumerator()
    {
      return new CompositeEffectParameterEnumerator(m_constantBuffers, m_resourceVariables);
    }

    /// <summary>
    /// Returns an enumerator that iterates through a collection.
    /// </summary>
    /// <returns>An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.</returns>
    IEnumerator IEnumerable.GetEnumerator()
    {
      return new CompositeEffectParameterEnumerator(m_constantBuffers, m_resourceVariables);
    }
  }

  /// <summary>
  /// Composite enumerator to enumerate constant buffer effect parameter collections and a non-constant buffer parameter collection.
  /// </summary>
  public struct CompositeEffectParameterEnumerator : IEnumerator<IEffectParameter>
  {
    private IReadOnlyList<IEffectConstantBuffer> m_constantBuffers;
    private IReadOnlyList<IEffectParameter> m_resourceVariables;
    private int m_currentCollectionIndex;
    private int m_currentIndexInCollection;
    private int m_countOfCurrentCollection;

    /// <summary>
    /// Constructs a new instance of the <see cref="CompositeEffectParameterEnumerator"/> struct.
    /// </summary>
    /// <param name="constantBuffers">The constant buffers collection.</param>
    /// <param name="resourceVariables">The resource variable collection.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if either the constant buffer or resource variable collections are null.
    /// </exception>
    public CompositeEffectParameterEnumerator(IReadOnlyList<IEffectConstantBuffer> constantBuffers, IReadOnlyList<IEffectParameter> resourceVariables)
    {
      if (constantBuffers == null)
        throw new ArgumentNullException("constantBuffers");

      if (resourceVariables == null)
        throw new ArgumentNullException("resourecVariables");

      m_constantBuffers = constantBuffers;
      m_resourceVariables = resourceVariables;
      m_currentCollectionIndex = -1;
      m_currentIndexInCollection = -1;
      m_countOfCurrentCollection = 0;
    }

    /// <summary>
    /// Gets the current value.
    /// </summary>
    public IEffectParameter Current
    {
      get
      {
        if (m_currentCollectionIndex < 0)
          return null;

        if (m_currentCollectionIndex < m_constantBuffers.Count)
        {
          return m_constantBuffers[m_currentCollectionIndex].Parameters[m_currentIndexInCollection];
        }
        else if (m_currentCollectionIndex == m_constantBuffers.Count)
        {
          return m_resourceVariables[m_currentIndexInCollection];
        }

        return null;
      }
    }

    object IEnumerator.Current
    {
      get
      {
        return Current;
      }
    }

    /// <summary>
    /// Advances the enumerator to the next element of the collection.
    /// </summary>
    /// <returns>True if the enumerator was successfully advanced to the next element; false if the enumerator has passed the end of the collection.</returns>
    public bool MoveNext()
    {
      int currIndex = m_currentIndexInCollection + 1;

      if (currIndex < m_countOfCurrentCollection)
      {
        m_currentIndexInCollection = currIndex;
        return true;
      }
      else if (m_currentCollectionIndex < m_constantBuffers.Count)
      {
        int currCollIndex = m_currentCollectionIndex + 1;
        int count = GetCountOfCollection(currCollIndex);

        if (count > 0)
        {
          m_currentCollectionIndex = currCollIndex;
          m_countOfCurrentCollection = count;
          m_currentIndexInCollection = 0;
          return true;
        }
        else
        {
          return false;
        }
      }

      return false;
    }

    /// <summary>
    /// Sets the enumerator to its initial position, which is before the first element in the collection.
    /// </summary>
    public void Reset()
    {
      m_currentCollectionIndex = -1;
      m_currentIndexInCollection = -1;
      m_countOfCurrentCollection = 0;
    }

    /// <summary>
    /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
    /// </summary>
    public void Dispose() { }

    private int GetCountOfCollection(int collectionIndex)
    {
      if (collectionIndex < 0)
        return 0;

      if (collectionIndex < m_constantBuffers.Count)
      {
        return m_constantBuffers[collectionIndex].Parameters.Count;
      }
      else if (collectionIndex == m_constantBuffers.Count)
      {
        return m_resourceVariables.Count;
      }

      return 0;
    }
  }
}
