﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Content;
using Tesla.Graphics.Implementation;

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// Represents a buffer of vertices that exists on the GPU. Unlike a regular vertex buffer, this data is streamed out from the geometry shader stage (or vertex shader
  /// stage if the geometry shader stage is inactive). The streamed out vertex data then can be bound as input to the graphics pipeline, in a subsequent rendering pass.
  /// </summary>
  [SavableVersion(1)]
  public class StreamOutputBuffer : VertexBuffer
  {
    //For casting purposes
    private IStreamOutputBufferImpl StreamOutputBufferImpl
    {
      get
      {
        return GetImplAs<IStreamOutputBufferImpl>();
      }
      set
      {
        BindImplementation(value);
      }
    }

    /// <summary>
    /// For ISavable only.
    /// </summary>
    protected StreamOutputBuffer() { }

    /// <summary>
    /// Constructs a new instance of the <see cref="StreamOutputBuffer"/> class.
    /// </summary>
    /// <param name="renderSystem">Render system used to create the underlying implementation.</param>
    /// <param name="vertexLayout">Vertex layout that defines the vertex data this buffer will contain</param>
    /// <param name="vertexCount">Number of vertices the buffer will contain</param>
    /// <param name="options">Common <see cref="VertexBufferOptions"/> that may also contain initial data.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the render system are vertex declaration are null.</exception>
    /// <exception cref="System.ArgumentOutOfRangeException">Thrown if the vertex count is less than or equal to zero.</exception>
    /// <exception cref="TeslaGraphicsException">Thrown if creating the underlying implementation fails or is unsupported, see inner exception for a more detailed error.</exception>
    public StreamOutputBuffer(IRenderSystem renderSystem, VertexLayout vertexLayout, int vertexCount, VertexBufferOptions options = default)
    {
      CreateImplementation(renderSystem, vertexLayout, vertexCount, options);
    }

    private void CreateImplementation(IRenderSystem renderSystem, VertexLayout vertexLayout, int vertexCount, VertexBufferOptions options)
    {
      GraphicsHelper.ThrowIfRenderSystemNull(renderSystem);

      ValidateCreationParameters(vertexLayout, vertexCount);

      IStreamOutputBufferImplFactory factory;
      if (!renderSystem.TryGetImplementationFactory<IStreamOutputBufferImplFactory>(out factory))
        throw GraphicsExceptionHelper.NewUnsupportedFeatureException(renderSystem, typeof(StreamOutputBuffer));

      try
      {
        StreamOutputBufferImpl = factory.CreateImplementation(vertexLayout, vertexCount, options);
      }
      catch (Exception e)
      {
        throw GraphicsExceptionHelper.NewErrorCreatingImplementation(typeof(StreamOutputBuffer), e);
      }
    }
  }
}
