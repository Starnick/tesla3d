﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using Tesla.Content;

// TODO Nullable

namespace Tesla.Graphics
{
  /// <summary>
  /// The standard material script library contains materials that do not require a content repository. These generally are materials intended to be used as templates
  /// by custom material scripts. The standard library can be extended to include material scripts from other sources.
  /// </summary>
  public static class StandardMaterialScriptLibrary
  {
    private static Dictionary<String, IMaterialScriptProvider> s_providers;

    static StandardMaterialScriptLibrary()
    {
      s_providers = new Dictionary<String, IMaterialScriptProvider>();

      //Load the engine's default material script library
      LoadProvider(new DefaultMaterialScriptProvider());
    }

    /// <summary>
    /// Loads the material script provider. Each provider manages their material scripts.
    /// </summary>
    /// <param name="provider">The provider to be loaded.</param>
    /// <returns>True if the provider was added, false if it was already present.</returns>
    public static bool LoadProvider(IMaterialScriptProvider provider)
    {
      if (provider == null)
        return false;

      String folderPath = ContentHelper.NormalizePath(provider.FolderPath);

      lock (s_providers)
      {
        if (folderPath == null || s_providers.ContainsKey(folderPath))
          return false;

        s_providers.Add(folderPath, provider);
        return true;
      }
    }

    /// <summary>
    /// Queries if the provider is already loaded in the standard material library.
    /// </summary>
    /// <param name="provider">Provider that may be loaded or not.</param>
    /// <returns>True if the provider was loaded, false if it is not present.</returns>
    public static bool IsProviderLoaded(IMaterialScriptProvider provider)
    {
      if (provider == null)
        return false;

      lock (s_providers)
      {
        foreach (KeyValuePair<String, IMaterialScriptProvider> kv in s_providers)
        {
          if (kv.Value == provider)
            return true;
        }

        return false;
      }
    }

    /// <summary>
    /// Queries if the folder path is registered with a provider in the standard material library.
    /// </summary>
    /// <param name="folderPath">Folder path</param>
    /// <returns>True if the folder path is registered with a provider, false if a provider is not present.</returns>
    public static bool IsProviderLoaded(String folderPath)
    {
      if (folderPath == null)
        return false;

      String normalizedFolderPath = ContentHelper.NormalizePath(folderPath);

      lock (s_providers)
      {
        if (!s_providers.ContainsKey(normalizedFolderPath))
          return false;

        return true;
      }
    }

    /// <summary>
    /// Unloads the material script provider using the specified folder path.
    /// </summary>
    /// <param name="folderPath">The folder path of the provider to be unloaded.</param>
    /// <returns>True if the provider was removed, false if it wasn't present.</returns>
    public static bool UnloadProvider(String folderPath)
    {
      if (folderPath == null)
        return false;

      folderPath = ContentHelper.NormalizePath(folderPath);

      lock (s_providers)
      {
        IMaterialScriptProvider provider;
        if (s_providers.TryGetValue(folderPath, out provider))
        {
          s_providers.Remove(folderPath);
          if (provider is IDisposable)
            (provider as IDisposable).Dispose();

          return true;
        }

        return false;
      }
    }

    /// <summary>
    /// Gets the material script corresponding to the full name (without extension). The folder path in the name corresponds
    /// to the provider the material script resides in.
    /// </summary>
    /// <param name="fullName">The full name, the folder path + the name of the material script to return.</param>
    /// <returns>The material script text.</returns>
    public static String GetMaterialScript(String fullName)
    {
      if (String.IsNullOrEmpty(fullName) || Path.HasExtension(fullName))
        return String.Empty;

      String folderPath = Path.GetDirectoryName(fullName);
      String name = Path.GetFileNameWithoutExtension(fullName);

      if (folderPath == null || String.IsNullOrEmpty(name))
        return String.Empty;

      String materialScript = String.Empty;
      lock (s_providers)
      {
        IMaterialScriptProvider provider;
        if (s_providers.TryGetValue(folderPath, out provider))
          materialScript = provider.GetMaterialScript(name);
      }

      return materialScript;
    }

    /// <summary>
    /// Helper method for creating a new material quickly from a standard material script without having to define a new
    /// script file. Useful for debugging. The content manager should have a <see cref="MaterialResourceImporter"/> defined
    /// to maximize resource reuse. If not defined, one is created but not added to the content manager. Most standard materials
    /// will define only one material for each file.
    /// </summary>
    /// <param name="fullName">The full name, the folder path + the name of the material script to return.</param>
    /// <param name="contentManager">Content manager used to load dependent resources.</param>
    /// <param name="matName">Optional name to give the material. If null, then the name will be derived from the standard material name.</param>
    /// <returns>The standard material.</returns>
    public static Material CreateStandardMaterial(String fullName, ContentManager contentManager, String matName = null)
    {
      return CreateStandardMaterial(fullName, contentManager, null, matName);
    }

    /// <summary>
    /// Helper method for creating a new material quickly from a standard material script without having to define a new
    /// script file. Useful for debugging. The content manager should have a <see cref="MaterialResourceImporter"/> defined
    /// to maximize resource reuse. If not defined, one is created but not added to the content manager. Most standard materials
    /// will define only one material for each file.
    /// </summary>
    /// <param name="fullName">The full name, the folder path + the name of the material script to return.</param>
    /// <param name="contentManager">Content manager used to load dependent resources.</param>
    /// <param name="importParams">Parameters for the material import.</param>
    /// <param name="matName">Optional name to give the material. If null, then the name will be derived from the standard material name.</param>
    /// <returns>The standard material.</returns>
    public static Material CreateStandardMaterial(String fullName, ContentManager contentManager, MaterialImporterParameters importParams, String matName = null)
    {
      if (String.IsNullOrEmpty(fullName) || contentManager == null)
        return null;

      MaterialResourceImporter importer = null;
      foreach (IResourceImporter imp in contentManager.ResourceImporters)
      {
        if (imp is MaterialResourceImporter)
        {
          importer = imp as MaterialResourceImporter;
          break;
        }
      }

      if (importer == null)
        importer = new MaterialResourceImporter();

      return importer.CreateStandardMaterial(fullName, contentManager, importParams, matName);
    }

    /// <summary>
    /// Gets all the loaded material script providers.
    /// </summary>
    /// <returns>List of material script providers.</returns>
    public static List<IMaterialScriptProvider> GetProviders()
    {
      lock (s_providers)
      {
        List<IMaterialScriptProvider> list = new List<IMaterialScriptProvider>(s_providers.Count);

        foreach (KeyValuePair<String, IMaterialScriptProvider> kv in s_providers)
          list.Add(kv.Value);

        return list;
      }
    }

    /// <summary>
    /// Gets a loaded material script provider associated with the folder path. May return null.
    /// </summary>
    /// <param name="folderPath">The folder path.</param>
    /// <returns>The material script provider, if it exists.</returns>
    public static IMaterialScriptProvider GetProvider(String folderPath)
    {
      lock (s_providers)
      {
        IMaterialScriptProvider provider = null;
        s_providers.TryGetValue(folderPath, out provider);

        return provider;
      }
    }

    /// <summary>
    /// Base implementation for a <see cref="IMaterialScriptProvider"/>. Scripts get preloaded so no locking is necessary.
    /// </summary>
    public abstract class BaseProvider : IMaterialScriptProvider
    {
      private String m_folderPath;
      private Dictionary<String, String> m_materialScripts;

      /// <summary>
      /// Gets the folder path where the material scripts of this provider reside in. This must exist as the Engine's default materials are in the topmost folder (e.g.
      /// <see cref="String.Empty" /> would be the value).
      /// </summary>
      public String FolderPath
      {
        get
        {
          return m_folderPath;
        }
      }

      /// <summary>
      /// Constructs a new instance of the <see cref="BaseProvider"/> class.
      /// </summary>
      /// <param name="folderPath">The folder path that identifies the location of the material scripts.</param>
      /// <exception cref="ArgumentNullException">Thrown if the folder path is null.</exception>
      protected BaseProvider(String folderPath)
      {
        if (folderPath == null)
          throw new ArgumentNullException("folderPath");

        m_folderPath = folderPath;
        m_materialScripts = new Dictionary<String, String>();

        Preload(m_materialScripts);
      }

      /// <summary>
      /// Gets the string representation of the material script.
      /// </summary>
      /// <param name="name">Full name of the material, folder path and the material name. Should not include the .tem extension.</param>
      /// <returns>Returns the material script string representation, or <see cref="String.Empty" /> if it could not be found.</returns>
      public String GetMaterialScript(String name)
      {
        if (String.IsNullOrEmpty(name))
          return String.Empty;

        String scriptText;
        if (!m_materialScripts.TryGetValue(name, out scriptText))
          scriptText = String.Empty;

        return scriptText;
      }

      /// <summary>
      /// Gets all of the material scripts from the provider.
      /// </summary>
      /// <returns>List of pairs where the first property is the script name, and the second property is the script contents.</returns>
      public List<Pair<String, String>> GetMaterialScripts()
      {
        List<Pair<String, String>> list = new List<Pair<String, String>>(m_materialScripts.Count);

        foreach (KeyValuePair<String, String> kv in m_materialScripts)
        {
          list.Add(new Pair<String, String>(kv.Key, kv.Value));
        }

        return list;
      }

      /// <summary>
      /// Called to preload all the material scripts.
      /// </summary>
      /// <param name="materialScripts">Cache that will hold the material scripts.</param>
      protected abstract void Preload(Dictionary<String, String> materialScripts);

      /// <summary>
      /// Gets the script text from a byte buffer.
      /// </summary>
      /// <param name="buffer">Byte buffer.</param>
      /// <returns>Material script text.</returns>
      protected String GetScriptText(byte[] buffer)
      {
        using (MemoryStream ms = new MemoryStream(buffer, false))
        {
          using (StreamReader reader = new StreamReader(ms))
          {
            return reader.ReadToEnd();
          }
        }
      }
    }

    //Default script provider for the standard material library
    private sealed class DefaultMaterialScriptProvider : BaseProvider
    {
      public DefaultMaterialScriptProvider() : base(String.Empty) { }

      protected override void Preload(Dictionary<String, String> materialScripts)
      {
        //Key should always be a string (name of the file) and value should always be a byte[]
        foreach (DictionaryEntry kv in StandardMaterials.ResourceManager.GetResourceSet(CultureInfo.InvariantCulture, true, false))
        {
          materialScripts.Add((String) kv.Key, GetScriptText((byte[]) kv.Value));
        }
      }
    }
  }
}
