﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

// TODO NULLABLE

namespace Tesla.Graphics
{
  public class LineBatch : IDisposable
  {
    private const int MaxBatchSize = 2048;

    private bool m_isDisposed;
    private PrimitiveBatch<VertexPositionColorTexture> m_batchTexture;
    private PrimitiveBatch<VertexPositionColor> m_batchNoTexture;

    private bool m_inBeginEnd;
    private bool m_usingTexture;
    private Effect m_effect;
    private Matrix m_transform;
    private Vector3 m_camPos;
    private IEffectParameter m_wvpParam;
    private IEffectParameter m_textureParam;
    private IEffectParameter m_samplerParam;
    private IEffectShaderGroup m_textureLinesPass, m_colorLinesPass;
    private IEffectShaderGroup m_customShaderGroup;

    public bool IsDisposed
    {
      get
      {
        return m_isDisposed;
      }
    }

    internal PrimitiveBatch<VertexPositionColorTexture> BatchTexture
    {
      get
      {
        return m_batchTexture;
      }
    }

    internal PrimitiveBatch<VertexPositionColor> BatchNoTexture
    {
      get
      {
        return m_batchNoTexture;
      }
    }

    public LineBatch() : this(IRenderSystem.Current, MaxBatchSize) { }

    public LineBatch(IRenderSystem renderSystem) : this(renderSystem, MaxBatchSize) { }

    public LineBatch(int maxBatchSize) : this(IRenderSystem.Current, maxBatchSize) { }

    public LineBatch(IRenderSystem renderSystem, int maxBatchSize)
    {
      if (maxBatchSize <= 0)
        throw new ArgumentOutOfRangeException("maxBatchSize", StringLocalizer.Instance.GetLocalizedString("BatchSizeMustBePositive"));

      if (renderSystem == null)
        throw new ArgumentNullException("renderSystem");

      int maxVertexCount = GetMaxTriangleVertices(maxBatchSize);

      m_batchTexture = new PrimitiveBatch<VertexPositionColorTexture>(maxVertexCount);
      m_batchNoTexture = new PrimitiveBatch<VertexPositionColor>(maxVertexCount);

      m_effect = renderSystem.StandardEffects.CreateEffect("Sprite");
      m_textureParam = m_effect.Parameters["SpriteMap"];
      m_wvpParam = m_effect.Parameters["SpriteTransform"];
      m_samplerParam = m_effect.Parameters["SpriteMapSampler"];

      m_textureLinesPass = m_effect.ShaderGroups["SpriteTexture"];
      m_colorLinesPass = m_effect.ShaderGroups["SpriteNoTexture"];

      m_effect.CurrentShaderGroup = m_colorLinesPass;
    }

    public void Begin(IRenderContext renderContext)
    {
      Begin(renderContext, null, null, null, null, null, null, Matrix.Identity);
    }

    public void Begin(IRenderContext renderContext, Texture2D texture)
    {
      Begin(renderContext, null, null, null, null, texture, null, Matrix.Identity);
    }

    public void Begin(IRenderContext renderContext, BlendState blendState, DepthStencilState depthStencilState, RasterizerState rasterizerState)
    {
      Begin(renderContext, blendState, depthStencilState, rasterizerState, null, null, null, Matrix.Identity);
    }

    public void Begin(IRenderContext renderContext, BlendState blendState, DepthStencilState depthStencilState, RasterizerState rasterizerState, Texture2D texture)
    {
      Begin(renderContext, blendState, depthStencilState, rasterizerState, null, texture, null, Matrix.Identity);
    }

    public void Begin(IRenderContext renderContext, BlendState blendState, DepthStencilState depthStencilState, RasterizerState rasterizerState, IEffectShaderGroup customShaderGroup)
    {
      Begin(renderContext, blendState, depthStencilState, rasterizerState, null, null, customShaderGroup, Matrix.Identity);
    }

    public void Begin(IRenderContext renderContext, BlendState blendState, DepthStencilState depthStencilState, RasterizerState rasterizerState, IEffectShaderGroup customShaderGroup, in Matrix worldMatrix)
    {
      Begin(renderContext, blendState, depthStencilState, rasterizerState, null, null, customShaderGroup, worldMatrix);
    }

    public void Begin(IRenderContext renderContext, BlendState blendState, DepthStencilState depthStencilState, RasterizerState rasterizerState, SamplerState samplerState, Texture2D texture)
    {
      Begin(renderContext, blendState, depthStencilState, rasterizerState, samplerState, texture, null, Matrix.Identity);
    }

    public void Begin(IRenderContext renderContext, BlendState blendState, DepthStencilState depthStencilState, RasterizerState rasterizerState, SamplerState samplerState, Texture2D texture, in Matrix worldMatrix)
    {
      Begin(renderContext, blendState, depthStencilState, rasterizerState, samplerState, texture, null, worldMatrix);
    }

    public void Begin(IRenderContext renderContext, BlendState blendState, DepthStencilState depthStencilState, RasterizerState rasterizerState, SamplerState samplerState, Texture2D texture, IEffectShaderGroup customShaderGroup)
    {
      Begin(renderContext, blendState, depthStencilState, rasterizerState, samplerState, texture, customShaderGroup, Matrix.Identity);
    }

    public void Begin(IRenderContext renderContext, BlendState blendState, DepthStencilState depthStencilState, RasterizerState rasterizerState, SamplerState samplerState, Texture2D texture, IEffectShaderGroup customShaderGroup, in Matrix worldMatrix)
    {
      CheckDisposed();

      if (m_inBeginEnd)
        throw new InvalidOperationException(StringLocalizer.Instance.GetLocalizedString("CannotNestBeginCalls"));

      if (renderContext == null)
        throw new ArgumentNullException("renderContext", StringLocalizer.Instance.GetLocalizedString("NullRenderContext"));

      m_inBeginEnd = true;

      //Set render states
      if (blendState == null)
        blendState = BlendState.Opaque;

      if (depthStencilState == null)
        depthStencilState = DepthStencilState.Default;

      if (rasterizerState == null)
        rasterizerState = RasterizerState.CullNone;

      if (samplerState == null)
        samplerState = SamplerState.LinearWrap;

      renderContext.BlendState = blendState;
      renderContext.DepthStencilState = depthStencilState;
      renderContext.RasterizerState = rasterizerState;

      m_customShaderGroup = customShaderGroup;

      //Apply shader states
      if (m_customShaderGroup == null)
      {
        //Set the World-View-Projection transform
        Camera cam = renderContext.Camera;
        m_transform = worldMatrix * cam.ViewProjectionMatrix;
        m_camPos = cam.Position;
        m_wvpParam.SetValue(m_transform);

        //Set which batch we're using and the texture
        if (texture != null)
        {
          m_textureParam.SetResource<Texture2D>(texture);
          m_samplerParam.SetResource<SamplerState>(samplerState);
          m_usingTexture = true;
          m_batchTexture.Begin(renderContext);
          m_textureLinesPass.Apply(renderContext);
        }
        else
        {
          m_usingTexture = false;
          m_batchNoTexture.Begin(renderContext);
          m_colorLinesPass.Apply(renderContext);
        }
      }
      else
      {
        m_usingTexture = (texture == null) ? false : true;

        //Apply the custom effect shader group, then AFTER set the sprite resources (ensures they won't get overwrriten)
        m_customShaderGroup.Apply(renderContext);

        if (m_usingTexture)
        {
          IShaderStage shaderStage = renderContext.GetShaderStage(ShaderStage.PixelShader);
          shaderStage.SetShaderResource(0, texture);
          shaderStage.SetSampler(0, samplerState);
        }
      }
    }

    #region DrawPoint

    public void DrawPoint(in Vector3 pt, in Color color, float size)
    {
      InputData data = new InputData(new VertexPositionColor(pt, color));

      if (m_usingTexture)
      {
        DrawTexturedPoints(data, size, size);
      }
      else
      {
        DrawPoints(data, size, size);
      }
    }

    public void DrawPoint(in Vector3 pt, in Color color, float width, float height)
    {
      InputData data = new InputData(new VertexPositionColor(pt, color));

      if (m_usingTexture)
      {
        DrawTexturedPoints(data, width, height);
      }
      else
      {
        DrawPoints(data, width, height);
      }
    }

    #endregion

    #region DrawLine

    #region DrawLine Separate points/colors

    public void DrawLine(in Vector3 start, in Vector3 end, in Color color, float thickness)
    {
      Span<Vector3> proxy = stackalloc Vector3[2];
      proxy[0] = start;
      proxy[1] = end;

      Draw(LineTopology.LineString, LineJoinMode.None, proxy, color, thickness, thickness);
    }

    public void DrawLine(in Vector3 start, in Vector3 end, in Color color, float startThickness, float endThickness)
    {
      Span<Vector3> proxy = stackalloc Vector3[2];
      proxy[0] = start;
      proxy[1] = end;

      Draw(LineTopology.LineString, LineJoinMode.None, proxy, color, startThickness, endThickness);
    }

    public void DrawLine(in Vector3 start, in Vector3 end, in Color startColor, in Color endColor, float thickness)
    {
      Span<VertexPositionColor> proxy = stackalloc VertexPositionColor[2];
      proxy[0] = new VertexPositionColor(start, startColor);
      proxy[1] = new VertexPositionColor(end, endColor);

      Draw(LineTopology.LineString, LineJoinMode.None, proxy, thickness, thickness);
    }

    public void DrawLine(in Vector3 start, in Vector3 end, in Color startColor, in Color endColor, float startThickness, float endThickness)
    {
      Span<VertexPositionColor> proxy = stackalloc VertexPositionColor[2];
      proxy[0] = new VertexPositionColor(start, startColor);
      proxy[1] = new VertexPositionColor(end, endColor);

      Draw(LineTopology.LineString, LineJoinMode.None, proxy, startThickness, endThickness);
    }

    #endregion

    #region DrawLine VertexPositionColor

    public void DrawLine(in VertexPositionColor start, in VertexPositionColor end, float thickness)
    {
      Span<VertexPositionColor> proxy = stackalloc VertexPositionColor[2];
      proxy[0] = start;
      proxy[1] = end;

      Draw(LineTopology.LineString, LineJoinMode.None, proxy, thickness);
    }

    public void DrawLine(in VertexPositionColor start, in VertexPositionColor end, float startThickness, float endThickness)
    {
      Span<VertexPositionColor> proxy = stackalloc VertexPositionColor[2];
      proxy[0] = start;
      proxy[1] = end;

      Draw(LineTopology.LineString, LineJoinMode.None, proxy, startThickness, endThickness);
    }

    #endregion

    #endregion

    #region Draw

    public void Draw(LineTopology topology, LineJoinMode joinMode, ReadOnlySpan<Vector3> points, Color color, float thickness)
    {
      Draw(topology, joinMode, points, color, thickness, thickness);
    }

    public void Draw(LineTopology topology, LineJoinMode joinMode, ReadOnlySpan<Vector3> points, Color color, float startThickness, float endThickness)
    {
      if (points.IsEmpty || (topology != LineTopology.Points && points.Length < 2))
        return;

      DrawInternal(new InputData(points, color), topology, joinMode, startThickness, endThickness);
    }

    public void Draw(LineTopology topology, LineJoinMode joinMode, ReadOnlySpan<Vector3> points, ReadOnlySpan<Color> colors, float thickness)
    {
      Draw(topology, joinMode, points, colors, thickness, thickness);
    }

    public void Draw(LineTopology topology, LineJoinMode joinMode, ReadOnlySpan<Vector3> points, ReadOnlySpan<Color> colors, float startThickness, float endThickness)
    {
      if (points.IsEmpty || colors.Length != points.Length || (topology != LineTopology.Points && points.Length < 2))
        return;

      DrawInternal(new InputData(points, colors), topology, joinMode, startThickness, endThickness);
    }

    public void Draw(LineTopology topology, LineJoinMode joinMode, ReadOnlySpan<VertexPositionColor> pointsAndColors, float thickness)
    {
      Draw(topology, joinMode, pointsAndColors, thickness, thickness);
    }

    public void Draw(LineTopology topology, LineJoinMode joinMode, ReadOnlySpan<VertexPositionColor> pointsAndColors, float startThickness, float endThickness)
    {
      if (pointsAndColors.IsEmpty || (topology != LineTopology.Points && pointsAndColors.Length < 2))
        return;

      DrawInternal(new InputData(pointsAndColors), topology, joinMode, startThickness, endThickness);
    }

    private void DrawInternal(in InputData data, LineTopology topology, LineJoinMode joinMode, float startThickness, float endThickness)
    {
      switch (topology)
      {
        case LineTopology.Points:
          if (m_usingTexture)
            DrawTexturedPoints(data, startThickness, endThickness);
          else
            DrawPoints(data, startThickness, endThickness);
          break;
        case LineTopology.LineString:
        case LineTopology.ClosedLineString:
          bool isClosed = topology == LineTopology.ClosedLineString;

          if (startThickness <= 0.0f && endThickness <= 0.0f)
          {
            if (m_usingTexture)
              DrawTexturedThinLines(data, isClosed);
            else
              DrawThinLines(data, isClosed);
          }
          else
          {
            switch (joinMode)
            {
              case LineJoinMode.None:
                if (m_usingTexture)
                  DrawTexturedNoJoinLines(data, startThickness, endThickness);
                else
                  DrawNoJoinLines(data, startThickness, endThickness);
                break;
              case LineJoinMode.Simple:
                if (m_usingTexture)
                  DrawTexturedSimpleLines(data, startThickness, endThickness, isClosed);
                else
                  DrawSimpleLines(data, startThickness, endThickness, isClosed);
                break;
              case LineJoinMode.Round:
                if (m_usingTexture)
                  DrawTexturedRoundLines(data, startThickness, endThickness, isClosed);
                else
                  DrawRoundLines(data, startThickness, endThickness, isClosed);
                break;
              case LineJoinMode.Miter:
                if (m_usingTexture)
                  DrawTexturedMiterLines(data, startThickness, endThickness, isClosed);
                else
                  DrawMiterLines(data, startThickness, endThickness, isClosed);
                break;
              case LineJoinMode.Curve:
                if (m_usingTexture)
                  DrawTexturedCurve(data, startThickness, endThickness, isClosed);
                else
                  DrawCurve(data, startThickness, endThickness, isClosed);
                break;
            }
          }

          break;
      }
    }

    #endregion

    #region End

    public void End()
    {
      CheckDisposed();

      if (!m_inBeginEnd)
        throw new InvalidOperationException(StringLocalizer.Instance.GetLocalizedString("EndCalledBeforeBegin"));

      if (m_usingTexture)
      {
        m_batchTexture.End();

        if (m_customShaderGroup == null)
        {
          //Make sure we don't hold onto references...
          m_textureParam.SetResource<Texture2D>((Texture2D) null);
          m_samplerParam.SetResource<SamplerState>((SamplerState) null);
        }
      }
      else
      {
        m_batchNoTexture.End();
      }

      m_customShaderGroup = null;
      m_inBeginEnd = false;
    }

    #endregion

    #region Draw methods

    #region Draw Points

    private void DrawPoints(in InputData data, float width, float height)
    {
      int pointCount = data.GetInputPointCount();

      //Each point quad needs 4 vertices, v0 is upper right and clockwise order
      VertexPositionColor v0, v1, v2, v3;

      for (int i = 0; i < pointCount; i++)
      {
        VertexPositionColor point;
        data.GetInputPoint(i, out point);

        ComputePointSprite(width, height, point.Position, out v0.Position, out v1.Position, out v2.Position, out v3.Position);

        v0.VertexColor = point.VertexColor;
        v1.VertexColor = point.VertexColor;
        v2.VertexColor = point.VertexColor;
        v3.VertexColor = point.VertexColor;

        m_batchNoTexture.DrawQuad(v0, v1, v2, v3);
      }
    }

    private void DrawTexturedPoints(in InputData data, float width, float height)
    {
      int pointCount = data.GetInputPointCount();

      //Each point quad needs 4 vertices, v0 is upper right and clockwise order
      VertexPositionColorTexture v0, v1, v2, v3;

      for (int i = 0; i < pointCount; i++)
      {
        VertexPositionColor point;
        data.GetInputPoint(i, out point);

        ComputePointSprite(width, height, point.Position, out v0.Position, out v1.Position, out v2.Position, out v3.Position);

        v0.VertexColor = point.VertexColor;
        v1.VertexColor = point.VertexColor;
        v2.VertexColor = point.VertexColor;
        v3.VertexColor = point.VertexColor;

        v0.TextureCoordinate = new Vector2(1.0f, 0.0f);
        v1.TextureCoordinate = new Vector2(1.0f, 1.0f);
        v2.TextureCoordinate = new Vector2(0.0f, 1.0f);
        v3.TextureCoordinate = new Vector2(0.0f, 0.0f);

        m_batchTexture.DrawQuad(v0, v1, v2, v3);
      }
    }

    #endregion

    #region Draw Thin Lines

    private void DrawThinLines(in InputData data, bool isClosed)
    {
      int pointCount = data.GetInputPointCount();

      //Convert linestring into a list of lines
      for (int i = 0; i < pointCount - 1; i++)
      {
        VertexPositionColor p0;
        VertexPositionColor p1;

        data.GetInputPoint(i, out p0);
        data.GetInputPoint(i + 1, out p1);

        m_batchNoTexture.DrawLine(p0, p1);
      }
    }

    private void DrawTexturedThinLines(in InputData data, bool isClosed)
    {
      int pointCount = data.GetInputPointCount();
      float maxUV = (float) (pointCount - 2);

      //Convert linestring into a list of lines
      for (int i = 0; i < pointCount - 1; i++)
      {
        VertexPositionColorTexture p0;
        data.GetInputPoint(i, out p0);
        p0.TextureCoordinate = new Vector2((float) i / maxUV, 0.0f);

        VertexPositionColorTexture p1;
        data.GetInputPoint(i + 1, out p1);
        p1.TextureCoordinate = new Vector2((float) i / maxUV, 1.0f);

        m_batchTexture.DrawLine(p0, p1);
      }
    }

    #endregion

    #region Draw NoJoin Lines

    private void DrawNoJoinLines(in InputData data, float startThickness, float endThickness)
    {
      int pointCount = data.GetInputPointCount();

      //Each line segment needs 4 vertices
      VertexPositionColor v1;
      VertexPositionColor v2;
      VertexPositionColor v3;
      VertexPositionColor v4;

      Vector3 g0;
      Vector3 g1;
      Vector3 h0;
      Vector3 h1;

      VertexPositionColor p0, p1;

      float maxUV = (float) (pointCount - 1);
      float prevThickness = startThickness;

      for (int i = 0; i < pointCount - 1; i++)
      {
        data.GetInputPoint(i, out p0);
        data.GetInputPoint(i + 1, out p1);

        float interThickness = MathHelper.Lerp(startThickness, endThickness, (float) (i + 1) / maxUV);

        ComputePolyBoardPoints(prevThickness, interThickness, p0.Position, p1.Position, out g0, out g1, out h0, out h1);
        prevThickness = interThickness;

        v1 = new VertexPositionColor(g0, p0.VertexColor);
        v2 = new VertexPositionColor(h1, p1.VertexColor);
        v3 = new VertexPositionColor(h0, p0.VertexColor);
        v4 = new VertexPositionColor(g1, p1.VertexColor);

        m_batchNoTexture.DrawTriangle(v1, v2, v3);
        m_batchNoTexture.DrawTriangle(v1, v4, v2);
      }
    }

    private void DrawTexturedNoJoinLines(in InputData data, float startThickness, float endThickness)
    {
      int pointCount = data.GetInputPointCount();

      //Each line segment needs 4 vertices
      VertexPositionColorTexture v1;
      VertexPositionColorTexture v2;
      VertexPositionColorTexture v3;
      VertexPositionColorTexture v4;

      Vector3 g0;
      Vector3 g1;
      Vector3 h0;
      Vector3 h1;

      VertexPositionColorTexture p0, p1;

      float maxUV = (float) (pointCount - 1);
      float prevThickness = startThickness;

      for (int i = 0; i < pointCount - 1; i++)
      {
        data.GetInputPoint(i, out p0);
        data.GetInputPoint(i + 1, out p1);

        float interThickness = MathHelper.Lerp(startThickness, endThickness, (float) (i + 1) / maxUV);

        ComputePolyBoardPoints(prevThickness, interThickness, p0.Position, p1.Position, out g0, out g1, out h0, out h1);
        prevThickness = interThickness;

        v1 = new VertexPositionColorTexture(g0, p0.VertexColor, new Vector2((float) i / maxUV, 0.0f));
        v2 = new VertexPositionColorTexture(h1, p1.VertexColor, new Vector2((float) (i + 1) / maxUV, 1.0f));
        v3 = new VertexPositionColorTexture(h0, p0.VertexColor, new Vector2((float) i / maxUV, 1.0f));
        v4 = new VertexPositionColorTexture(g1, p1.VertexColor, new Vector2((float) (i + 1) / maxUV, 0.0f));

        m_batchTexture.DrawTriangle(v1, v2, v3);
        m_batchTexture.DrawTriangle(v1, v4, v2);
      }
    }

    #endregion

    #region Draw Simple Lines

    private void DrawSimpleLines(in InputData data, float startThickness, float endThickness, bool isClosed)
    {

    }

    private void DrawTexturedSimpleLines(in InputData data, float startThickness, float endThickness, bool isClosed)
    {
      int pointCount = data.GetInputPointCount();

      //Each line segment needs 4 vertices
      VertexPositionColorTexture v1;
      VertexPositionColorTexture v2;
      VertexPositionColorTexture v3;
      VertexPositionColorTexture v4;

      Vector3 g0;
      Vector3 g1;
      Vector3 h0;
      Vector3 h1;

      VertexPositionColorTexture p0, p1;

      float maxUV = (float) (pointCount - 1);
      float prevThickness = startThickness;
      Segment prevGSeg, prevHSeg;

      for (int i = 0; i < pointCount - 1; i++)
      {
        data.GetInputPoint(i, out p0);
        data.GetInputPoint(i + 1, out p1);

        float interThickness = MathHelper.Lerp(startThickness, endThickness, (float) (i + 1) / maxUV);

        ComputePolyBoardPoints(prevThickness, interThickness, p0.Position, p1.Position, out g0, out g1, out h0, out h1);
        prevThickness = interThickness;

        Segment gSeg = new Segment(g0, g1);
        Segment hSeg = new Segment(h0, h1);

        if (i > 0)
        {

        }

        prevGSeg = gSeg;
        prevHSeg = hSeg;

        v1 = new VertexPositionColorTexture(g0, p0.VertexColor, new Vector2((float) i / maxUV, 0.0f));
        v2 = new VertexPositionColorTexture(h1, p1.VertexColor, new Vector2((float) (i + 1) / maxUV, 1.0f));
        v3 = new VertexPositionColorTexture(h0, p0.VertexColor, new Vector2((float) i / maxUV, 1.0f));
        v4 = new VertexPositionColorTexture(g1, p1.VertexColor, new Vector2((float) (i + 1) / maxUV, 0.0f));

        m_batchTexture.DrawTriangle(v1, v2, v3);
        m_batchTexture.DrawTriangle(v1, v4, v2);
      }
    }

    #endregion

    #region Draw Round Lines

    private void DrawRoundLines(in InputData data, float startThickness, float endThickness, bool isClosed)
    {

    }

    private void DrawTexturedRoundLines(in InputData data, float startThickness, float endThickness, bool isClosed)
    {

    }

    #endregion

    #region Draw Miter Lines

    private void DrawMiterLines(in InputData data, float startThickness, float endThickness, bool isClosed)
    {

    }

    private void DrawTexturedMiterLines(in InputData data, float startThickness, float endThickness, bool isClosed)
    {

    }

    #endregion

    #region Draw Curve

    private void DrawCurve(in InputData data, float startThickness, float endThickness, bool isClosed)
    {
      int pointCount = data.GetInputPointCount();

      VertexPositionColor v1;
      VertexPositionColor v2;
      VertexPositionColor v3;
      VertexPositionColor v4;

      Vector3 g0;
      Vector3 h0;
      Vector3 g1;
      Vector3 h1;

      VertexPositionColor p0;
      VertexPositionColor p1;
      VertexPositionColor p2;
      Vector3 tangent;

      data.GetInputPoint(0, out p0);
      data.GetInputPoint(1, out p1);

      //Compute start tangent
      Vector3.Subtract(p1.Position, p0.Position, out tangent);
      tangent.Normalize();

      float maxUV = (float) (pointCount - 1);

      //Compute start points
      ComputePolyBoardPoints(startThickness, p0.Position, tangent, out g0, out h0);

      VertexPositionColor firstG0 = new VertexPositionColor(g0, p0.VertexColor);
      VertexPositionColor firstH0 = new VertexPositionColor(h0, p0.VertexColor);

      //Quads look like this:
      //
      // g0-------g1-------gn
      // |  \     |  \     |
      // |    \   |    \   |
      // |      \ |      \ |
      // h0-------h1-------hn
      //

      //Compute points for every subsequent line segment except for the end point
      for (int i = 1; i < pointCount - 1; i++)
      {
        data.GetInputPoint(i + 1, out p2);

        Vector3.Subtract(p2.Position, p0.Position, out tangent);
        tangent.Normalize();

        float interThickness = MathHelper.Lerp(startThickness, endThickness, (float) i / maxUV);

        ComputePolyBoardPoints(interThickness, p1.Position, tangent, out g1, out h1);

        //Set up the vertices
        v1 = new VertexPositionColor(g0, p0.VertexColor);
        v2 = new VertexPositionColor(h1, p1.VertexColor);
        v3 = new VertexPositionColor(h0, p0.VertexColor);
        v4 = new VertexPositionColor(g1, p1.VertexColor);

        m_batchNoTexture.DrawTriangle(v1, v2, v3);
        m_batchNoTexture.DrawTriangle(v1, v4, v2);

        //Save current data as previous
        p0 = p1;
        p1 = p2;

        g0 = g1;
        h0 = h1;
      }

      //Compute end point - p1 should be the last point and p0, the next to last now
      Vector3.Subtract(p1.Position, p0.Position, out tangent);
      tangent.Normalize();

      ComputePolyBoardPoints(endThickness, p1.Position, tangent, out g1, out h1);

      v1 = new VertexPositionColor(g0, p0.VertexColor);
      v2 = new VertexPositionColor(h1, p1.VertexColor);
      v3 = new VertexPositionColor(h0, p0.VertexColor);
      v4 = new VertexPositionColor(g1, p1.VertexColor);

      if (isClosed)
      {
        m_batchNoTexture.DrawTriangle(firstG0, firstH0, v1);
        m_batchNoTexture.DrawTriangle(firstH0, v3, v1);
      }
      else
      {
        m_batchNoTexture.DrawTriangle(v1, v2, v3);
        m_batchNoTexture.DrawTriangle(v1, v4, v2);
      }
    }

    private void DrawTexturedCurve(in InputData data, float startThickness, float endThickness, bool isClosed)
    {
      int pointCount = data.GetInputPointCount();

      VertexPositionColorTexture v1;
      VertexPositionColorTexture v2;
      VertexPositionColorTexture v3;
      VertexPositionColorTexture v4;

      Vector3 g0;
      Vector3 h0;
      Vector3 g1;
      Vector3 h1;

      VertexPositionColorTexture p0;
      VertexPositionColorTexture p1;
      VertexPositionColorTexture p2;
      Vector3 tangent;

      data.GetInputPoint(0, out p0);
      data.GetInputPoint(1, out p1);

      //Compute start tangent
      Vector3.Subtract(p1.Position, p0.Position, out tangent);
      tangent.Normalize();

      float maxUV = (float) (pointCount - 1);

      //Compute start points
      ComputePolyBoardPoints(startThickness, p0.Position, tangent, out g0, out h0);

      VertexPositionColorTexture firstG0 = new VertexPositionColorTexture(g0, p0.VertexColor, new Vector2(0.0f, 0.0f));
      VertexPositionColorTexture firstH0 = new VertexPositionColorTexture(h0, p0.VertexColor, new Vector2(0.0f, 1.0f));

      //Quads look like this:
      //
      // g0-------g1-------gn
      // |  \     |  \     |
      // |    \   |    \   |
      // |      \ |      \ |
      // h0-------h1-------hn
      //

      //Compute points for every subsequent line segment except for the end point
      for (int i = 1; i < pointCount - 1; i++)
      {
        data.GetInputPoint(i + 1, out p2);

        Vector3.Subtract(p2.Position, p0.Position, out tangent);
        tangent.Normalize();

        float interThickness = MathHelper.Lerp(startThickness, endThickness, (float) i / maxUV);

        ComputePolyBoardPoints(interThickness, p1.Position, tangent, out g1, out h1);

        //Set up the vertices
        v1 = new VertexPositionColorTexture(g0, p0.VertexColor, new Vector2((float) i / maxUV, 0.0f));
        v2 = new VertexPositionColorTexture(h1, p1.VertexColor, new Vector2((float) (i + 1) / maxUV, 1.0f));
        v3 = new VertexPositionColorTexture(h0, p0.VertexColor, new Vector2((float) i / maxUV, 1.0f));
        v4 = new VertexPositionColorTexture(g1, p1.VertexColor, new Vector2((float) (i + 1) / maxUV, 0.0f));

        m_batchTexture.DrawTriangle(v1, v2, v3);
        m_batchTexture.DrawTriangle(v1, v4, v2);

        //Save current data as previous
        p0 = p1;
        p1 = p2;

        g0 = g1;
        h0 = h1;
      }

      //Compute end point - p1 should be the last point and p0, the next to last now
      Vector3.Subtract(p1.Position, p0.Position, out tangent);
      tangent.Normalize();

      ComputePolyBoardPoints(endThickness, p1.Position, tangent, out g1, out h1);

      float lastUCoord = (maxUV - 1) / maxUV;

      v1 = new VertexPositionColorTexture(g0, p0.VertexColor, new Vector2(lastUCoord, 0.0f));
      v2 = new VertexPositionColorTexture(h1, p1.VertexColor, new Vector2(1.0f, 1.0f));
      v3 = new VertexPositionColorTexture(h0, p0.VertexColor, new Vector2(lastUCoord, 1.0f));
      v4 = new VertexPositionColorTexture(g1, p1.VertexColor, new Vector2(1.0f, 0.0f));

      if (isClosed)
      {
        m_batchTexture.DrawTriangle(firstG0, firstH0, v1);
        m_batchTexture.DrawTriangle(firstH0, v3, v1);
      }
      else
      {
        m_batchTexture.DrawTriangle(v1, v2, v3);
        m_batchTexture.DrawTriangle(v1, v4, v2);
      }
    }

    #endregion

    #endregion

    #region Helpers

    private int GetMaxTriangleVertices(int batchSize)
    {
      return batchSize * 6;
    }

    //Expands a point into a quad that is billboarded so it faces the camera. V0 corresponds to the top right, and the rest are in clockwise winding
    private void ComputePointSprite(float rightExtent, float upExtent, in Vector3 point, out Vector3 v0, out Vector3 v1, out Vector3 v2, out Vector3 v3)
    {
      //Compute vector to camera
      Vector3 zDir;
      Vector3.Subtract(m_camPos, point, out zDir);
      zDir.Normalize();

      //Compute right and up vectors using +Y as the world up
      Vector3 up = Vector3.UnitY;
      Vector3 right;

      Vector3.NormalizedCross(zDir, up, out right);
      Vector3.NormalizedCross(zDir, right, out up);
      Vector3.Multiply(up, upExtent, out up);
      Vector3.Multiply(right, rightExtent, out right);

      //Upper right
      Vector3.Add(point, right, out v0);
      Vector3.Add(v0, up, out v0);

      //Lower right
      Vector3.Add(point, right, out v1);
      Vector3.Subtract(v1, up, out v1);

      //Lower left
      Vector3.Subtract(point, right, out v2);
      Vector3.Subtract(v2, up, out v2);

      //Upper left
      Vector3.Subtract(point, right, out v3);
      Vector3.Add(v3, up, out v3);
    }

    //Computes points offseted from the line point along the normal, the tangent usually relies on the previous and next points to compute a "smooth"
    //curve. G represents the "plus" point along the normal, and H the "minus" point along the normal where thickness is the scaling factor along the normal.
    private void ComputePolyBoardPoints(float thickness, in Vector3 p0, in Vector3 tangent, out Vector3 g0, out Vector3 h0)
    {
      //Compute vector to camera
      Vector3 zDir;
      Vector3.Subtract(m_camPos, p0, out zDir);
      zDir.Normalize();

      //Compute normal
      Vector3 norm;
      Vector3.Cross(tangent, zDir, out norm);
      norm.Normalize();

      //Scale normal by thickness
      Vector3.Multiply(norm, thickness, out norm);

      //Compute g0 and h0
      Vector3.Add(p0, norm, out g0);
      Vector3.Subtract(p0, norm, out h0);
    }

    //Computes points for both the first and second point of a single line segment, allowing for discontinuities for non-curve lines. The tangent is computed
    //using these two points and the four vertex points are computed by scaling the normal at both points by the thickness. G0 and H0 represent
    //the first two poitns and G1 and H1 the last two.
    private void ComputePolyBoardPoints(float startThickness, float endThickness, in Vector3 p0, in Vector3 p1, out Vector3 g0, out Vector3 g1, out Vector3 h0, out Vector3 h1)
    {
      //Find tangent of line
      Vector3 tangent;
      Vector3.Subtract(p1, p0, out tangent);
      tangent.Normalize();

      Vector3 midpoint = (p0 + p1) / 2.0f;

      //Compute vector to camera of the first part
      Vector3 zDir0;
      Vector3.Subtract(m_camPos, p0, out zDir0);
      zDir0.Normalize();

      //Compute vector to camera of the second part
      Vector3 zDir1;
      Vector3.Subtract(m_camPos, p1, out zDir1);
      zDir1.Normalize();

      //Compute normal of the first part
      Vector3 norm0;
      Vector3.Cross(tangent, zDir0, out norm0);
      norm0.Normalize();

      //Scale normal by thickness
      Vector3.Multiply(norm0, startThickness, out norm0);

      //Compute normal of the second part
      Vector3 norm1;
      Vector3.Cross(tangent, zDir1, out norm1);
      norm1.Normalize();

      //Scale normal by thickness
      Vector3.Multiply(norm1, endThickness, out norm1);

      //Compute start pts g0, h0
      Vector3.Add(p0, norm0, out g0);
      Vector3.Subtract(p0, norm0, out h0);

      //Compute end pts g1, h1
      Vector3.Add(p1, norm1, out g1);
      Vector3.Subtract(p1, norm1, out h1);
    }

    #endregion

    /// <summary>
    /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
    /// </summary>
    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    /// <summary>
    /// Releases unmanaged and - optionally - managed resources.
    /// </summary>
    /// <param name="isDisposing"><c>True</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
    protected void Dispose(bool isDisposing)
    {
      if (!m_isDisposed)
      {
        if (isDisposing)
        {
          if (m_batchTexture != null)
          {
            m_batchTexture.Dispose();
            m_batchTexture = null;
          }

          if (m_batchNoTexture != null)
          {
            m_batchNoTexture.Dispose();
            m_batchNoTexture = null;
          }
        }

        m_isDisposed = true;
      }
    }

    private void CheckDisposed()
    {
      ObjectDisposedException.ThrowIf(m_isDisposed, this);
    }

    private enum InputDataType
    {
      PointsAndColors,
      PointsSingleColor,
      PointsMultiColors
    }

    private readonly ref struct InputData
    {
      public readonly InputDataType InputType;
      public readonly ReadOnlySpan<Vector3> Points;
      public readonly ReadOnlySpan<Color> Colors;
      public readonly ReadOnlySpan<VertexPositionColor> PointsAndColors;

      public InputData(ReadOnlySpan<Vector3> pts, in Color singleColor)
      {
        InputType = InputDataType.PointsSingleColor;
        Points = pts;
        Colors = new ReadOnlySpan<Color>(singleColor);
        PointsAndColors = default;
      }

      public InputData(in VertexPositionColor ptAndColor)
      {
        InputType = InputDataType.PointsAndColors;
        PointsAndColors = new ReadOnlySpan<VertexPositionColor>(ptAndColor);
        Points = default;
        Colors = default;
      }

      public InputData(ReadOnlySpan<Vector3> pts, ReadOnlySpan<Color> colors)
      {
        InputType = InputDataType.PointsMultiColors;
        Points = pts;
        Colors = colors;
        PointsAndColors = default;
      }

      public InputData(ReadOnlySpan<VertexPositionColor> ptsAndColors)
      {
        InputType = InputDataType.PointsAndColors;
        PointsAndColors = ptsAndColors;
        Points = default;
        Colors = default;
      }

      public int GetInputPointCount()
      {
        switch (InputType)
        {
          case InputDataType.PointsAndColors:
            return PointsAndColors.Length;
          case InputDataType.PointsMultiColors:
          case InputDataType.PointsSingleColor:
            return Points.Length;
          default:
            return 0;
        }
      }

      public void GetInputPoint(int index, out VertexPositionColor point)
      {
        switch (InputType)
        {
          case InputDataType.PointsAndColors:
            point = PointsAndColors[index];
            break;
          case InputDataType.PointsMultiColors:
            point.Position = Points[index];
            point.VertexColor = Colors[index];
            break;
          case InputDataType.PointsSingleColor:
            point.Position = Points[index];
            point.VertexColor = Colors[0];
            break;
          default:
            point.Position = Vector3.Zero;
            point.VertexColor = Color.White;
            break;
        }
      }

      public readonly void GetInputPoint(int index, out VertexPositionColorTexture point)
      {
        point.TextureCoordinate = Vector2.Zero;

        switch (InputType)
        {
          case InputDataType.PointsAndColors:
            ref readonly VertexPositionColor vvc = ref PointsAndColors[index];
            point.Position = vvc.Position;
            point.VertexColor = vvc.VertexColor;
            break;
          case InputDataType.PointsMultiColors:
            point.Position = Points[index];
            point.VertexColor = Colors[index];
            break;
          case InputDataType.PointsSingleColor:
            point.Position = Points[index];
            point.VertexColor = Colors[0];
            break;
          default:
            point.Position = Vector3.Zero;
            point.VertexColor = Color.White;
            break;
        }
      }
    }
  }
}
