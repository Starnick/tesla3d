﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Buffers;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Tesla.Utilities;

// TODO NULLABLE

namespace Tesla.Graphics
{
  public class MeshClipper
  {
    private static ObjectPool<MeshClipTask> TaskPool = new ObjectPool<MeshClipTask>(new SimplePoolManager<MeshClipTask>(() =>
      {
        return new MeshClipTask();
      }));

    private RefList<Plane> m_clipPlanes;
    private List<MeshClipTask> m_tasks;
    private List<MeshClipTask> m_tasksTemp;

    public RefList<Plane> ClipPlanes
    {
      get
      {
        return m_clipPlanes;
      }
    }

    public MeshClipper()
    {
      m_clipPlanes = new RefList<Plane>();
      m_tasks = new List<MeshClipTask>();
      m_tasksTemp = new List<MeshClipTask>();
    }

    public static void FreePooledResources()
    {
      TaskPool.Clear();
    }

    public MeshData Clip(MeshData meshToClip, in SubMeshRange? submesh = null, BoundingVolume bounds = null)
    {
      MeshData resultMesh = new MeshData();
      if (Clip(meshToClip, resultMesh, submesh, bounds))
        return resultMesh;

      return null;
    }

    public bool Clip(MeshData meshToClip, MeshData clippedMesh, in SubMeshRange? submesh = null, BoundingVolume bounds = null)
    {
      //Need to have AT LEAST triangles and positions
      if (meshToClip == null || clippedMesh == null || !meshToClip.HasTriangles || meshToClip.Positions == null || m_clipPlanes.Count == 0)
        return false;

      //Check if this mesh intersects with the clip plane set (if null, it is considered "inside" so we check the triangles anyways).
      if (PlaneSetContains(m_clipPlanes.Span, bounds) == ContainmentType.Outside)
        return false;

      int startPrimIndex;
      int primCount;
      meshToClip.GetPrimitiveRange(submesh, out startPrimIndex, out primCount);

      int baseVertexOffset = 0;
      if (submesh.HasValue)
        baseVertexOffset = submesh.Value.BaseVertexOffset;

      if (primCount == 0)
        return false;

      //Setup as many tasks as there are processors, we may not use all of them if the mesh isn't large enough
      int maxTaskCount = Environment.ProcessorCount;

      //Compute a rough estimate of how many primitives per task...with a minimum
      int endPrimIndexExclusive = startPrimIndex + primCount;
      int primsPerTask = Math.Max((int) Math.Floor((float) primCount / (float) maxTaskCount), 500); //Pick some min later...
      int numPrimsLeft = primCount;

      for (int i = 0; i < maxTaskCount; i++)
      {
        //End creating tasks if no more primitives
        if (numPrimsLeft <= 0)
          break;

        int taskEndPrimIndexExclusive = Math.Min(endPrimIndexExclusive, startPrimIndex + primsPerTask);
        int numPrimsThisTask = taskEndPrimIndexExclusive - startPrimIndex;
        if (numPrimsThisTask <= 0)
          break;

        MeshClipTask task;
        TaskPool.TryFetch(out task);

        task.InitializePassOne(startPrimIndex, taskEndPrimIndexExclusive, baseVertexOffset, meshToClip, m_clipPlanes);

        numPrimsLeft -= numPrimsThisTask;
        startPrimIndex += numPrimsThisTask;
        m_tasks.Add(task);
      }

      //Execute the clipping in two passes.
      //Pass 1: Clip the mesh against all planes, setting up the position and index buffer with results (either clipped triangles or triangles fully contained). Intermediate
      //        data containing interpolation weights will be stored for each "new" vertex.
      //
      //Pass 2: Interpolate other vertex attributes based on results of the first pass and copy ALL buffers to a final destination buffer.
      //
      // The final mesh data will ALWAYS contain indexed geometry, even if the original mesh is non-indexed.
      bool status = false;

      try
      {
        RunPassOne(m_tasks);
        status = RunPassTwo(m_tasks, clippedMesh);
      }
      finally
      {
        foreach (MeshClipTask t in m_tasks)
          t.Reset();

        TaskPool.Return(m_tasks);
        m_tasks.Clear();
      }

      return status;
    }

    private void RunPassOne(List<MeshClipTask> tasks)
    {
      bool runSync = (tasks.Count == 1);
      for (int i = 0; i < tasks.Count; i++)
        tasks[i].RunTask(runSync);
    }

    private bool RunPassTwo(List<MeshClipTask> tasks, MeshData clippedMesh)
    {
      int vertexCount = 0;
      int indexCount = 0;

      //Wait on all tasks to execute, then gather up the final counts and initialize second pass.
      for (int i = 0; i < tasks.Count; i++)
      {
        MeshClipTask t = tasks[i];
        t.WaitTask();

        if (t.VertexCount == 0 || t.IndexCount == 0)
        {
          t.Reset();
          continue;
        }

        t.InitializePassTwo(indexCount, vertexCount, clippedMesh);

        vertexCount += t.VertexCount;
        indexCount += t.IndexCount;
        m_tasksTemp.Add(t);
      }

      tasks.Clear();

      //No more tasks? Nothing was inside the clip area!!
      if (m_tasksTemp.Count == 0)
        return false;

      //Create each databuffer in the result mesh based on one of the clip tasks. This also clears the mesh of any previous buffers
      m_tasksTemp[0].CreatePrototypeBuffersFor(clippedMesh, vertexCount, indexCount);

      //Map all destination buffers, as it unsafe for each task to do that!
      using PooledArray<MemoryHandle> handles = new PooledArray<MemoryHandle>(clippedMesh.BufferCount);
      PinMeshBuffers(clippedMesh, handles);

      try
      {
        bool runSync = (m_tasksTemp.Count == 1);

        //Initialize + start remaining tasks
        foreach (MeshClipTask task in m_tasksTemp)
        {
          task.RunTask(runSync);

          //Add back to the task list
          tasks.Add(task);
        }

        //Wait
        for (int i = 0; i < tasks.Count; i++)
          tasks[i].WaitTask();
      }
      finally
      {
        BufferHelper.Unpin(handles);
      }

      return true;
    }

    private void PinMeshBuffers(MeshData md, Span<MemoryHandle> handles)
    {
      int index = 0;
      foreach (VertexAttributeData vd in md)
      {
        handles[index] = vd.Buffer.Pin();
        index++;
      }
    }

    private class MeshClipTask : IDisposable
    {
      private static Action<DataBufferBuilder<InterpolatedVertex>, int, Dictionary<int, int>, VertexStreamEntry, MeshData>[] s_additionalAttributeConverters;

      //Pass 1 - Clip our portion of the mesh and hold intermediate results
      private DataBufferBuilder<Vector3> m_positions;
      private IndexDataBuilder m_indices;
      private int m_indexCount;
      private int m_vertexCount;
      private bool m_isNonIndexed;
      private int m_startPrimitiveInclusive;
      private int m_endPrimitiveExclusive;
      private int m_baseVertexOffset;
      private List<VertexStreamEntry> m_additionalVertexAttributes;
      private MeshData m_meshToClip;
      private IReadOnlyRefList<Plane> m_clipPlanes;

      //Manages mappings from the old mesh to the new mesh, vertices either survive clipping (and may be shared) so we just copy over, or new vertices
      //are generated and need to interpolate old mesh attributes
      private Dictionary<int, int> m_sharedIndices;
      private DataBufferBuilder<InterpolatedVertex> m_newVertices;

      //These are small result lists from the clipping operation, at most will only hold a handful of vertices
      private RefList<Vector3> m_polygon;
      private RefList<Vector3> m_clippedResult;
      private RefList<float> m_tempList;
      private RefList<int> m_tempIndices;
      private RefList<Vector4> m_baryCentricCoordinates;

      //Switch to execute each pass over the data
      private bool m_executePhase1;
      private Task m_task;

      //Pass 2 - Given global buffers, copy our data into them at given offsets and re-index our indices
      private int m_indexOffset;
      private int m_vertexOffset;
      private MeshData m_resultMesh;

      public int IndexCount
      {
        get
        {
          return m_indexCount;
        }
      }

      public int VertexCount
      {
        get
        {
          return m_vertexCount;
        }
      }

      static MeshClipTask()
      {
        InitializeAttributeConverters();
      }

      public MeshClipTask()
      {
        m_additionalVertexAttributes = new List<VertexStreamEntry>();
        m_sharedIndices = new Dictionary<int, int>();
        m_polygon = new RefList<Vector3>();
        m_clippedResult = new RefList<Vector3>();
        m_tempList = new RefList<float>();
        m_tempIndices = new RefList<int>();
        m_baryCentricCoordinates = new RefList<Vector4>();

        m_positions = new DataBufferBuilder<Vector3>(0, MemoryAllocator<Vector3>.DefaultPooled);
        m_indices = new IndexDataBuilder(0, IndexFormat.ThirtyTwoBits, MemoryAllocatorStrategy.DefaultPooled);
        m_newVertices = new DataBufferBuilder<InterpolatedVertex>(1024, MemoryAllocator<InterpolatedVertex>.DefaultPooled);
      }

      public void CreatePrototypeBuffersFor(MeshData resultMesh, int vertexCount, int indexCount)
      {
        resultMesh.ClearData();
        resultMesh.Positions = DataBuffer.Create<Vector3>(vertexCount);
        resultMesh.Indices = new IndexData(indexCount);

        //Look at the additional vertex attributes
        for (int i = 0; i < m_additionalVertexAttributes.Count; i++)
        {
          VertexStreamEntry entry = m_additionalVertexAttributes[i];
          resultMesh.AddBuffer(entry.Semantic, entry.Index, entry.Format, CreateBufferFor(entry.Format, vertexCount));
        }
      }

      public void InitializePassOne(int startPrimitiveInclusive, int endPrimitiveExclusive, int baseVertexOffset, MeshData meshToClip, IReadOnlyRefList<Plane> clipPlanes)
      {
        m_executePhase1 = true;
        m_indexCount = 0;
        m_vertexCount = 0;
        m_isNonIndexed = !meshToClip.UseIndexedPrimitives;
        m_startPrimitiveInclusive = startPrimitiveInclusive;
        m_endPrimitiveExclusive = endPrimitiveExclusive;
        m_baseVertexOffset = baseVertexOffset;
        m_meshToClip = meshToClip;
        m_clipPlanes = clipPlanes;

        int triCount = endPrimitiveExclusive - startPrimitiveInclusive;
        int estimatedIndexCount = triCount * 3;

        //If data is indexed, we probably will have a lot less vertices. If not indexed then use the previous estimate
        int estimatedVertCount = (m_isNonIndexed) ? estimatedIndexCount : estimatedIndexCount / 3;

        m_positions.Position = 0;
        m_positions.EnsureCapacity(estimatedVertCount);
        
        m_indices.Position = 0;
        m_indices.EnsureCapacity(estimatedIndexCount);

        m_newVertices.Position = 0;
      }

      public void InitializePassTwo(int indexOffset, int vertexOffset, MeshData resultMesh)
      {
        m_executePhase1 = false;
        m_indexOffset = indexOffset;
        m_vertexOffset = vertexOffset;
        m_resultMesh = resultMesh;

        //Initializes these for phase 2, the first pass may have zero results, so it doesn't make sense to pre-allocate everything
        CollectAdditionalAttributes(m_additionalVertexAttributes, m_vertexCount, m_meshToClip);
      }

      public void RunTask(bool synchronously = false)
      {
        if (synchronously)
        {
          Execute();
        }
        else
        {
          m_task = new Task(Execute);
          m_task.Start();
        }
      }

      public void WaitTask()
      {
        if (m_task is not null)
          m_task.Wait();
      }

      private void Execute()
      {
        if (m_executePhase1)
        {
          if (m_isNonIndexed)
            ExecuteForNonIndexed();
          else
            ExecuteForIndexed();
        }
        else
        {
          //Done clipping, now do the interpolation for the rest of the attributes in bulk
          ProcessAdditionalAttributes();

          ExecuteCopyIntoFinalBuffers();
        }
      }

      private void ExecuteCopyIntoFinalBuffers()
      {
        //Copy positions...
        CopyBuffer(m_positions.UnderlyingBuffer, m_resultMesh.Positions, m_vertexOffset, m_vertexCount);

        //Copy indices...
        CopyIndices(m_indices.UnderlyingBuffer, m_resultMesh.Indices.Value, m_indexOffset, m_indexCount, m_vertexOffset);

        //Copy additional vertices
        for (int i = 0; i < m_additionalVertexAttributes.Count; i++)
        {
          VertexStreamEntry entry = m_additionalVertexAttributes[i];
          IDataBuffer dst = m_resultMesh.GetBuffer(entry.Semantic, entry.Index);
          CopyBuffer(entry.Buffer, dst, m_vertexOffset, m_vertexCount);
        }
      }

      private void CopyBuffer(IDataBuffer src, IDataBuffer dst, int offsetInDst, int numToCopy)
      {
        int offsetInBytes = offsetInDst * dst.ElementSizeInBytes;
        int numBytesToCopy = numToCopy * dst.ElementSizeInBytes;
        src.Bytes.Slice(0, numBytesToCopy).CopyTo(dst.Bytes.Slice(offsetInBytes, numBytesToCopy));
      }

      private void CopyIndices(IndexData src, IndexData dst, int offsetInDst, int numToCopy, int baseVertexOffset)
      {
        //If same format, fast path!
        if (src.IndexFormat == dst.IndexFormat)
        {
          if (baseVertexOffset > 0)
          {
            //If we have a vertex offset...need to add it to each index... 
            DataBuffer<int> srcInt = src.UnderlyingDataBuffer as DataBuffer<int>;
            DataBuffer<int> dstInt = dst.UnderlyingDataBuffer as DataBuffer<int>;

            for (int i = 0; i < numToCopy; i++, offsetInDst++)
              dstInt[offsetInDst] = srcInt[i] + baseVertexOffset;
          }
          else
          {
            CopyBuffer(src.UnderlyingDataBuffer, dst.UnderlyingDataBuffer, offsetInDst, numToCopy);
          }
        }
        else
        {
          //Otherwise, the dst is short indices (we only ever use 32-bit ints in the clipper). So we need to go one by one and cast down
          DataBuffer<int> srcInt = src.UnderlyingDataBuffer as DataBuffer<int>;
          DataBuffer<ushort> dstShort = dst.UnderlyingDataBuffer as DataBuffer<ushort>;

          for (int i = 0; i < numToCopy; i++, offsetInDst++)
            dstShort[offsetInDst] = (ushort) (srcInt[i] + baseVertexOffset);
        }
      }

      public void Dispose()
      {
        Reset();

        if (m_positions is not null)
          m_positions.Clear();

        if (m_newVertices is not null)
          m_newVertices.Clear();

        if (m_indices is not null)
          m_indices.Clear();

        m_task = null;
      }

      public void Reset()
      {
        m_task = null;
        m_clipPlanes = null;
        m_meshToClip = null;
        m_resultMesh = null;
        m_positions.Position = 0;
        m_newVertices.Position = 0;
        m_indices.Position = 0;

        for (int i = 0; i < m_additionalVertexAttributes.Count; i++)
        {
          IDataBuffer buffer = m_additionalVertexAttributes[i].Buffer;
          DataBufferPool.GetPool(buffer.ElementType).ReturnDataBuffer(buffer);
        }

        m_sharedIndices.Clear();
        m_additionalVertexAttributes.Clear();
      }

      private void ExecuteForIndexed()
      {
        DataBuffer<Vector3> positions = m_meshToClip.Positions;
        IndexData indices = m_meshToClip.Indices.Value;

        for (int i = m_startPrimitiveInclusive; i < m_endPrimitiveExclusive; i++)
        {
          Int3 indexInIB;
          indexInIB.X = m_meshToClip.GetIndexForPrimitive(i);
          indexInIB.Y = indexInIB.X + 1;
          indexInIB.Z = indexInIB.X + 2;

          Int3 indexInVB;
          indexInVB.X = indices[indexInIB.X] + m_baseVertexOffset;
          indexInVB.Y = indices[indexInIB.Y] + m_baseVertexOffset;
          indexInVB.Z = indices[indexInIB.Z] + m_baseVertexOffset;

          ref readonly Vector3 p0 = ref positions[indexInVB.X];
          ref readonly Vector3 p1 = ref positions[indexInVB.Y];
          ref readonly Vector3 p2 = ref positions[indexInVB.Z];

          m_polygon.Clear();
          m_polygon.Add(p0);
          m_polygon.Add(p1);
          m_polygon.Add(p2);

          bool completelyClipped = false;
          bool completelyContained = true;

          for (int planeIndex = 0; planeIndex < m_clipPlanes.Count; planeIndex++)
          {
            ContainmentType clipResult = PolygonHelper.Clip3DPolygon(m_polygon, m_clipPlanes[planeIndex], m_clippedResult, m_tempList);

            //If completely clipped, or result is not a triangle, skip further clipping and reject the triangle
            if (clipResult == ContainmentType.Outside || m_clippedResult.Count < 3)
            {
              m_clippedResult.Clear();
              completelyClipped = true;
              break;
            }

            if (clipResult == ContainmentType.Intersects)
              completelyContained = false;

            //Swap polygon lists - but not on the last plane
            if (planeIndex < (m_clipPlanes.Count - 1))
            {
              RefList<Vector3> temp = m_polygon;
              m_polygon = m_clippedResult;
              m_clippedResult = temp;
            }
          }

          //If completely clipped...go to the next triangle
          if (completelyClipped)
            continue;

          //If completely contained...don't need to do any extra computations, just add the triangle as it is
          if (completelyContained)
          {
            AddContainedIndexedTriangle(indexInVB, p0, p1, p2);
            continue;
          }

          //Add triangles for the polygon
          TriangulateClipResultsIndexed(indexInVB, p0, p1, p2);
        }
      }

      private void ExecuteForNonIndexed()
      {
        DataBuffer<Vector3> positions = m_meshToClip.Positions;

        for (int i = m_startPrimitiveInclusive; i < m_endPrimitiveExclusive; i++)
        {
          Int3 indexInVB;
          indexInVB.X = m_meshToClip.GetIndexForPrimitive(i);
          indexInVB.Y = indexInVB.X + 1;
          indexInVB.Z = indexInVB.X + 2;

          ref readonly Vector3 p0 = ref positions[indexInVB.X];
          ref readonly Vector3 p1 = ref positions[indexInVB.Y];
          ref readonly Vector3 p2 = ref positions[indexInVB.Z];

          m_polygon.Clear();
          m_polygon.Add(p0);
          m_polygon.Add(p1);
          m_polygon.Add(p2);

          bool completelyClipped = false;
          bool completelyContained = true;

          for (int planeIndex = 0; planeIndex < m_clipPlanes.Count; planeIndex++)
          {
            ContainmentType clipResult = PolygonHelper.Clip3DPolygon(m_polygon, m_clipPlanes[planeIndex], m_clippedResult, m_tempList);

            //If completely clipped, or result is not a triangle, skip further clipping and reject the triangle
            if (clipResult == ContainmentType.Outside || m_clippedResult.Count < 3)
            {
              m_clippedResult.Clear();
              completelyClipped = true;
              break;
            }

            if (clipResult == ContainmentType.Intersects)
              completelyContained = false;

            //Swap polygon lists - but not on the last plane
            if (planeIndex < (m_clipPlanes.Count - 1))
            {
              RefList<Vector3> temp = m_polygon;
              m_polygon = m_clippedResult;
              m_clippedResult = temp;
            }
          }

          //If completely clipped...go to the next triangle
          if (completelyClipped)
            continue;

          //If completely contained...don't need to do any extra computations, just add the triangle as it is
          if (completelyContained)
          {
            AddContainedIndexedTriangle(indexInVB, p0, p1, p2);
            continue;
          }

          //Add triangles for the polygon
          TriangulateClipResultsIndexed(indexInVB, p0, p1, p2);
        }
      }

      private void TriangulateClipResultsIndexed(in Int3 indicesInVB, in Vector3 p0, in Vector3 p1, in Vector3 p2)
      {
        int primCount = m_clippedResult.Count - 2;

        //Ensure we have capacity...
        EnsureBuffersCapacity(primCount);

        //Compute interpolation weights
        ComputeInterpolation(m_clippedResult, m_baryCentricCoordinates, p0, p1, p2, indicesInVB);

        //For each clipped vertex, add it to the position buffer and generate an index for it
        for (int i = 0; i < m_clippedResult.Count; i++)
        {
          ref readonly Vector4 interpolation = ref m_baryCentricCoordinates[i];
          ref readonly Vector3 clipPoint = ref m_clippedResult[i];

          //If new vertex, W will be -1. Otherwise it will be one of the 3 original VB indices.
          if (interpolation.W < 0.0)
          {
            //Unless if we weld nearby vertices, if we generate new vertices from the clipping, we won't be sharing them with other primitives...
            InterpolatedVertex newVertex;
            newVertex.Weights.X = interpolation.X;
            newVertex.Weights.Y = interpolation.Y;
            newVertex.Weights.Z = interpolation.Z;
            newVertex.OriginalVBIndices = indicesInVB;
            newVertex.NewVBIndex = m_vertexCount;

            m_vertexCount++;

            m_tempIndices.Add(newVertex.NewVBIndex);
            m_positions.Set(clipPoint);

            //We still need to create interpolations for other vertex attributes at this location. Instead of doing that now, we defer those computations
            //for later.
            m_newVertices.Set(newVertex);
          }
          else
          {
            //Otherwise it is not interpolated, so we can just look at the original points in the mesh. Potentially it may be shared!
            int originalVBIndex = (int) interpolation.W;
            int newVBIndex;
            if (m_sharedIndices.TryGetValue(originalVBIndex, out newVBIndex))
            {
              m_tempIndices.Add(newVBIndex);
            }
            else
            {
              //First time processing this (potentially shared) vertex, just copy over the position and update the index buffer

              newVBIndex = m_vertexCount;
              m_vertexCount++;
              m_sharedIndices.Add(originalVBIndex, newVBIndex);

              m_tempIndices.Add(newVBIndex);
              m_positions.Set(clipPoint);
            }
          }
        }

        //Add fan triangulation based on the indices
        for (int index = 0; index < primCount; index++)
        {
          m_indices.Set(m_tempIndices[0]);
          m_indices.Set(m_tempIndices[index + 1]);
          m_indices.Set(m_tempIndices[index + 2]);

          m_indexCount += 3;
        }

        m_tempIndices.Clear();
      }

      private void AddIndexedTriangle(in Int3 indicesInResultList, in Int3 indicesInVB, in Vector3 p0, in Vector3 p1, in Vector3 p2)
      {
        //Ensure we have capacity...
        EnsureBuffersCapacity();

        for (int i = 0; i < 3; i++)
        {
          ref readonly Vector4 interpolation = ref m_baryCentricCoordinates[i];
          ref readonly Vector3 clipPoint = ref m_clippedResult[i];

          //If new vertex, W will be -1. Otherwise it will be one of the 3 original VB indices.
          if (interpolation.W < 0.0)
          {
            //Unless if we weld nearby vertices, if we generate new vertices from the clipping, we won't be sharing them with other primitives...
            InterpolatedVertex newVertex;
            newVertex.Weights.X = interpolation.X;
            newVertex.Weights.Y = interpolation.Y;
            newVertex.Weights.Z = interpolation.Z;
            newVertex.OriginalVBIndices = indicesInVB;
            newVertex.NewVBIndex = m_vertexCount;

            m_vertexCount++;
            m_indices.Set(newVertex.NewVBIndex);
            m_positions.Set(clipPoint);

            //We still need to create interpolations for other vertex attributes at this location. Instead of doing that now, we defer those computations
            //for later.
            m_newVertices.Set(newVertex);
          }
          else
          {
            //Otherwise it is not interpolated, so we can just look at the original points in the mesh. Potentially it may be shared!
            int originalVBIndex = (int) interpolation.W;
            int newVBIndex;
            if (m_sharedIndices.TryGetValue(originalVBIndex, out newVBIndex))
            {
              m_indices.Set(newVBIndex);
            }
            else
            {
              //First time processing this (potentially shared) vertex, just copy over the position and update the index buffer

              newVBIndex = m_vertexCount;
              m_vertexCount++;
              m_sharedIndices.Add(originalVBIndex, newVBIndex);
              m_indices.Set(newVBIndex);
              m_positions.Set(clipPoint);
            }
          }

          m_indexCount++;
        }
      }

      private void AddContainedIndexedTriangle(in Int3 indicesInVB, in Vector3 p0, in Vector3 p1, in Vector3 p2)
      {
        //Ensure we have capacity...
        EnsureBuffersCapacity();

        //Check if this is the first time we're seeing this vertex
        int index0;
        if (!m_sharedIndices.TryGetValue(indicesInVB.X, out index0))
        {
          index0 = m_vertexCount;
          m_vertexCount++;
          m_sharedIndices.Add(indicesInVB.X, index0);
          m_positions.Set(p0);
        }

        //Add p1
        int index1;
        if (!m_sharedIndices.TryGetValue(indicesInVB.Y, out index1))
        {
          index1 = m_vertexCount;
          m_vertexCount++;
          m_sharedIndices.Add(indicesInVB.Y, index1);
          m_positions.Set(p1);
        }

        //Add p2
        int index2;
        if (!m_sharedIndices.TryGetValue(indicesInVB.Z, out index2))
        {
          index2 = m_vertexCount;
          m_vertexCount++;
          m_sharedIndices.Add(indicesInVB.Z, index2);
          m_positions.Set(p2);
        }

        //Add to indices
        m_indices.Set(index0);
        m_indices.Set(index1);
        m_indices.Set(index2);

        m_indexCount += 3;
      }

      private void EnsureBuffersCapacity(int primCount = 1)
      {
        int numVerts = primCount * 3;

        m_indices.EnsureCapacity(numVerts);
        m_positions.EnsureCapacity(numVerts);
        m_newVertices.EnsureCapacity(numVerts);
      }

      private void ProcessAdditionalAttributes()
      {
        //If there are other vertex attributes...go through each entry and fill the new vertex buffer with clipped/interpolated data
        if (m_additionalVertexAttributes.Count == 0)
          return;

        if (m_additionalVertexAttributes.Count == 1)
        {
          ProcessSingleVertexAttributeBuffer(0);
        }
        else
        {
          //Process multiple in parallel
          Parallel.For(0, m_additionalVertexAttributes.Count, ProcessSingleVertexAttributeBuffer);
        }
      }

      private void ProcessSingleVertexAttributeBuffer(int index)
      {
        VertexStreamEntry entry = m_additionalVertexAttributes[index];

        //Make sure the vertex data buffer is sized properly
        if (entry.Buffer.Length < m_vertexCount)
          entry.Buffer.Resize(m_vertexCount);

        s_additionalAttributeConverters[entry.ConversionIndex](m_newVertices, m_newVertices.Position, m_sharedIndices, entry, m_meshToClip);
      }

      private static void ComputeInterpolation(RefList<Vector3> polygon, RefList<Vector4> barycentricCoordinates, in Vector3 p0, in Vector3 p1, in Vector3 p2, in Int3 indexInVB)
      {
        barycentricCoordinates.Clear();

        for (int i = 0; i < polygon.Count; i++)
        {
          ref readonly Vector3 p = ref polygon[i];

          //Determine if the position is one of the triangle points or needs to be interpolated, and we set the known index. If its interpolated,
          //then we need a new index.

          //UVW weights as follows:
          // 1,0,0 = p0
          // 0,1,0 = p1
          // 0,0,1 = p2
          if (p.Equals(p0))
          {
            barycentricCoordinates.Add(new Vector4(1, 0, 0, indexInVB.X));
          }
          else if (p.Equals(p1))
          {
            barycentricCoordinates.Add(new Vector4(0, 1, 0, indexInVB.Y));
          }
          else if (p.Equals(p2))
          {
            barycentricCoordinates.Add(new Vector4(0, 0, 1, indexInVB.Z));
          }
          else
          {
            Vector3 bary;
            Vector3.Barycentric(p, p0, p1, p2, out bary);

            barycentricCoordinates.Add(new Vector4(bary, -1));
          }
        }
      }

      #region Attribute Converters

      private static void CollectAdditionalAttributes(List<VertexStreamEntry> entries, int estimatedVertCount, MeshData targetMesh)
      {
        foreach (VertexAttributeData vbData in targetMesh)
        {
          bool normalize = false;

          //Ignore the following semantics...
          switch (vbData.Semantic)
          {
            case VertexSemantic.BlendIndices:
            case VertexSemantic.BlendWeight:
              continue;
            //Ignore the "Positions" buffer, since we require that during the main execution
            case VertexSemantic.Position:
              if (vbData.SemanticIndex == 0)
                continue;
              break;
            case VertexSemantic.Normal:
            case VertexSemantic.Bitangent:
            case VertexSemantic.Tangent:
              normalize = true;
              break;
          }

          int converterIndex = -1;

          //Allow the following formats...
          switch (vbData.Format)
          {
            case VertexFormat.Color:
              converterIndex = 0;
              if (vbData.Buffer.ElementType == typeof(ColorBGRA))
                converterIndex = 1;
              break;
            case VertexFormat.Float:
              converterIndex = 2;
              break;
            case VertexFormat.Float2:
              converterIndex = 3;
              break;
            case VertexFormat.Float3:
              converterIndex = 4;
              break;
            case VertexFormat.Float4:
              converterIndex = 5;
              break;
            default:
              continue;
          }

          VertexStreamEntry entry;
          entry.ConversionIndex = converterIndex;
          entry.Semantic = vbData.Semantic;
          entry.Index = vbData.SemanticIndex;
          entry.Format = vbData.Format;
          entry.Normalize = normalize;
          entry.Buffer = DataBufferPool.GetPool(vbData.Buffer.ElementType).FetchDataBuffer(estimatedVertCount);

          entries.Add(entry);
        }
      }

      private IDataBuffer CreateBufferFor(VertexFormat format, int vertexCount)
      {
        switch (format)
        {
          case VertexFormat.Color:
            return DataBuffer.Create<Color>(vertexCount);
          case VertexFormat.Float:
            return DataBuffer.Create<float>(vertexCount);
          case VertexFormat.Float2:
            return DataBuffer.Create<Vector2>(vertexCount);
          case VertexFormat.Float3:
            return DataBuffer.Create<Vector3>(vertexCount);
          case VertexFormat.Float4:
            return DataBuffer.Create<Vector4>(vertexCount);
          default:
            throw new InvalidOperationException();
        }
      }

      private static void InitializeAttributeConverters()
      {
        s_additionalAttributeConverters = new Action<DataBufferBuilder<InterpolatedVertex>, int, Dictionary<int, int>, VertexStreamEntry, MeshData>[]
        {
          ConvertColorAttributes, ConvertColorBGRAAttributes, ConvertFloatAttributes, ConvertVector2Attributes, ConvertVector3Attributes, ConvertVector4Attributes
        };
      }

      private static void ConvertColorAttributes(DataBufferBuilder<InterpolatedVertex> newVertices, int newVertexCount, Dictionary<int, int> sharedVertices, VertexStreamEntry vbEntry, MeshData oldData)
      {
        DataBuffer<Color> vertices = vbEntry.Buffer as DataBuffer<Color>;
        DataBuffer<Color> oldVertices = oldData.GetBuffer<Color>(vbEntry.Semantic, vbEntry.Index);

        //Copy over non-interpolated data, key is old index, value is new index
        foreach (KeyValuePair<int, int> remap in sharedVertices)
          vertices[remap.Value] = oldVertices[remap.Key];

        //Copy over interpolated data
        for (int i = 0; i < newVertexCount; i++)
        {
          ref readonly InterpolatedVertex newVertex = ref newVertices.Get(i);

          //Look up the original three vertices, convert to 0-1.0 range

          Vector4 p0 = oldVertices[newVertex.OriginalVBIndices.X].ToVector4();
          Vector4 p1 = oldVertices[newVertex.OriginalVBIndices.Y].ToVector4();
          Vector4 p2 = oldVertices[newVertex.OriginalVBIndices.Z].ToVector4();

          Vector4.Multiply(p0, newVertex.Weights.X, out p0);
          Vector4.Multiply(p1, newVertex.Weights.Y, out p1);
          Vector4.Multiply(p2, newVertex.Weights.Z, out p2);

          vertices[newVertex.NewVBIndex] = new Color(p0.X + p1.X + p2.X, p0.Y + p1.Y + p2.Y, p0.Z + p1.Z + p2.Z, p0.W + p1.W + p2.W);
        }
      }

      private static void ConvertColorBGRAAttributes(DataBufferBuilder<InterpolatedVertex> newVertices, int newVertexCount, Dictionary<int, int> sharedVertices, VertexStreamEntry vbEntry, MeshData oldData)
      {
        DataBuffer<ColorBGRA> vertices = vbEntry.Buffer as DataBuffer<ColorBGRA>;
        DataBuffer<ColorBGRA> oldVertices = oldData.GetBuffer<ColorBGRA>(vbEntry.Semantic, vbEntry.Index);

        //Copy over non-interpolated data, key is old index, value is new index
        foreach (KeyValuePair<int, int> remap in sharedVertices)
          vertices[remap.Value] = oldVertices[remap.Key];

        //Copy over interpolated data
        for (int i = 0; i < newVertexCount; i++)
        {
          ref readonly InterpolatedVertex newVertex = ref newVertices.Get(i);

          //Look up the original three vertices, convert to 0-1.0 range

          Vector4 p0 = oldVertices[newVertex.OriginalVBIndices.X].ToVector4();
          Vector4 p1 = oldVertices[newVertex.OriginalVBIndices.Y].ToVector4();
          Vector4 p2 = oldVertices[newVertex.OriginalVBIndices.Z].ToVector4();

          Vector4.Multiply(p0, newVertex.Weights.X, out p0);
          Vector4.Multiply(p1, newVertex.Weights.Y, out p1);
          Vector4.Multiply(p2, newVertex.Weights.Z, out p2);

          vertices[newVertex.NewVBIndex] = new ColorBGRA(p0.X + p1.X + p2.X, p0.Y + p1.Y + p2.Y, p0.Z + p1.Z + p2.Z, p0.W + p1.W + p2.W);
        }
      }

      private static void ConvertFloatAttributes(DataBufferBuilder<InterpolatedVertex> newVertices, int newVertexCount, Dictionary<int, int> sharedVertices, VertexStreamEntry vbEntry, MeshData oldData)
      {
        DataBuffer<float> vertices = vbEntry.Buffer as DataBuffer<float>;
        DataBuffer<float> oldVertices = oldData.GetBuffer<float>(vbEntry.Semantic, vbEntry.Index);

        //Copy over non-interpolated data, key is old index, value is new index
        foreach (KeyValuePair<int, int> remap in sharedVertices)
          vertices[remap.Value] = oldVertices[remap.Key];

        //Copy over interpolated data
        for (int i = 0; i < newVertexCount; i++)
        {
          ref readonly InterpolatedVertex newVertex = ref newVertices.Get(i);

          //Look up the original three vertices
          float p0 = oldVertices[newVertex.OriginalVBIndices.X];
          float p1 = oldVertices[newVertex.OriginalVBIndices.Y];
          float p2 = oldVertices[newVertex.OriginalVBIndices.Z];

          vertices[newVertex.NewVBIndex] = (p0 * newVertex.Weights.X) + (p1 * newVertex.Weights.Y) + (p2 * newVertex.Weights.Z);
        }
      }

      private static void ConvertVector2Attributes(DataBufferBuilder<InterpolatedVertex> newVertices, int newVertexCount, Dictionary<int, int> sharedVertices, VertexStreamEntry vbEntry, MeshData oldData)
      {
        DataBuffer<Vector2> vertices = vbEntry.Buffer as DataBuffer<Vector2>;
        DataBuffer<Vector2> oldVertices = oldData.GetBuffer<Vector2>(vbEntry.Semantic, vbEntry.Index);

        //Copy over non-interpolated data, key is old index, value is new index
        foreach (KeyValuePair<int, int> remap in sharedVertices)
          vertices[remap.Value] = oldVertices[remap.Key];

        //Copy over interpolated data
        for (int i = 0; i < newVertexCount; i++)
        {
          ref readonly InterpolatedVertex newVertex = ref newVertices.Get(i);

          //Look up the original three vertices
          Vector2 p0, p1, p2;

          Vector2.Multiply(oldVertices[newVertex.OriginalVBIndices.X], newVertex.Weights.X, out p0);
          Vector2.Multiply(oldVertices[newVertex.OriginalVBIndices.Y], newVertex.Weights.Y, out p1);
          Vector2.Multiply(oldVertices[newVertex.OriginalVBIndices.Z], newVertex.Weights.Z, out p2);

          Vector2 p = new Vector2(p0.X + p1.X + p2.X, p0.Y + p1.Y + p2.Y); ;
          if (vbEntry.Normalize)
            p.Normalize();

          vertices[newVertex.NewVBIndex] = p;
        }
      }

      private static void ConvertVector3Attributes(DataBufferBuilder<InterpolatedVertex> newVertices, int newVertexCount, Dictionary<int, int> sharedVertices, VertexStreamEntry vbEntry, MeshData oldData)
      {
        DataBuffer<Vector3> vertices = vbEntry.Buffer as DataBuffer<Vector3>;
        DataBuffer<Vector3> oldVertices = oldData.GetBuffer<Vector3>(vbEntry.Semantic, vbEntry.Index);

        //Copy over non-interpolated data, key is old index, value is new index
        foreach (KeyValuePair<int, int> remap in sharedVertices)
          vertices[remap.Value] = oldVertices[remap.Key];

        //Copy over interpolated data
        for (int i = 0; i < newVertexCount; i++)
        {
          ref readonly InterpolatedVertex newVertex = ref newVertices.Get(i);

          //Look up the original three vertices
          Vector3 p0, p1, p2;

          Vector3.Multiply(oldVertices[newVertex.OriginalVBIndices.X], newVertex.Weights.X, out p0);
          Vector3.Multiply(oldVertices[newVertex.OriginalVBIndices.Y], newVertex.Weights.Y, out p1);
          Vector3.Multiply(oldVertices[newVertex.OriginalVBIndices.Z], newVertex.Weights.Z, out p2);

          Vector3 p = new Vector3(p0.X + p1.X + p2.X, p0.Y + p1.Y + p2.Y, p0.Z + p1.Z + p2.Z);
          if (vbEntry.Normalize)
            p.Normalize();

          vertices[newVertex.NewVBIndex] = p;
        }
      }

      private static void ConvertVector4Attributes(DataBufferBuilder<InterpolatedVertex> newVertices, int newVertexCount, Dictionary<int, int> sharedVertices, VertexStreamEntry vbEntry, MeshData oldData)
      {
        DataBuffer<Vector4> vertices = vbEntry.Buffer as DataBuffer<Vector4>;
        DataBuffer<Vector4> oldVertices = oldData.GetBuffer<Vector4>(vbEntry.Semantic, vbEntry.Index);

        //Copy over non-interpolated data, key is old index, value is new index
        foreach (KeyValuePair<int, int> remap in sharedVertices)
          vertices[remap.Value] = oldVertices[remap.Key];

        //Copy over interpolated data
        for (int i = 0; i < newVertexCount; i++)
        {
          ref readonly InterpolatedVertex newVertex = ref newVertices.Get(i);

          //Look up the original three vertices
          Vector4 p0, p1, p2;

          Vector4.Multiply(oldVertices[newVertex.OriginalVBIndices.X], newVertex.Weights.X, out p0);
          Vector4.Multiply(oldVertices[newVertex.OriginalVBIndices.Y], newVertex.Weights.Y, out p1);
          Vector4.Multiply(oldVertices[newVertex.OriginalVBIndices.Z], newVertex.Weights.Z, out p2);

          Vector4 p = new Vector4(p0.X + p1.X + p2.X, p0.Y + p1.Y + p2.Y, p0.Z + p1.Z + p2.Z, p0.W + p1.W + p2.W);
          if (vbEntry.Normalize)
            p.Normalize();

          vertices[newVertex.NewVBIndex] = p;
        }
      }

      #endregion
    }

    #region Structs for Clipping

    [StructLayout(LayoutKind.Sequential, Pack = 4)]
    private struct InterpolatedVertex
    {
      public Vector3 Weights;
      public Int3 OriginalVBIndices;
      public int NewVBIndex;
    }


    private struct VertexStreamEntry
    {
      public VertexSemantic Semantic;
      public int Index;
      public VertexFormat Format;
      public int ConversionIndex;
      public bool Normalize;
      public IDataBuffer Buffer;
    }

    #endregion

    #region DataBuffer Pooling - Keep it isolated

    // TODO - Review, this may not be needed since DataBuffer backing memory is already pooled. Question though is if it's
    // expensive to create the databuffer wrapper object and if that needs to be pooled?

    protected abstract class DataBufferPool
    {
      private static Dictionary<Type, DataBufferPool> s_pools;

      static DataBufferPool()
      {
        s_pools = new Dictionary<Type, DataBufferPool>();

        s_pools.Add(typeof(float), new DataBufferPool<float>());
        s_pools.Add(typeof(Vector2), new DataBufferPool<Vector2>());
        s_pools.Add(typeof(Vector3), new DataBufferPool<Vector3>());
        s_pools.Add(typeof(Vector4), new DataBufferPool<Vector4>());
        s_pools.Add(typeof(Color), new DataBufferPool<Color>());
        s_pools.Add(typeof(ColorBGRA), new DataBufferPool<ColorBGRA>());
        s_pools.Add(typeof(InterpolatedVertex), new DataBufferPool<InterpolatedVertex>());
      }

      public static DataBufferPool GetPool(Type type)
      {
        DataBufferPool pool;
        if (s_pools.TryGetValue(type, out pool))
          return pool;

        return null;
      }

      public static void ClearPools()
      {
        foreach (KeyValuePair<Type, DataBufferPool> kv in s_pools)
          s_pools.Clear();
      }

      public abstract IDataBuffer FetchDataBuffer(int count);
      public abstract void ReturnDataBuffer(IDataBuffer db);
      public abstract void ClearDataBuffers();
    }

    protected class DataBufferPool<T> : DataBufferPool where T : unmanaged
    {
      private static ObjectPool<DataBuffer<T>> s_pool = new ObjectPool<DataBuffer<T>>(new SimplePoolManager<DataBuffer<T>>(() =>
      {
        return DataBuffer.Create<T>(1024, MemoryAllocatorStrategy.DefaultPooled);
      }));

      public override IDataBuffer FetchDataBuffer(int count)
      {
        return Fetch(count);
      }

      public override void ReturnDataBuffer(IDataBuffer db)
      {
        Return(db as DataBuffer<T>);
      }

      public override void ClearDataBuffers()
      {
        s_pool.Clear();
      }

      public static DataBuffer<T> Fetch(int count)
      {
        DataBuffer<T> db;
        if (!s_pool.TryFetch(out db))
          return null;

        if (db.Length < count)
          db.Resize(count);

        return db;
      }

      public static void Return(DataBuffer<T> db)
      {
        s_pool.Return(db);
      }
    }

    protected class IndexBufferPool
    {
      private static ObjectPool<DataBuffer<int>> s_intPool = new ObjectPool<DataBuffer<int>>(new SimplePoolManager<DataBuffer<int>>(() =>
      {
        return DataBuffer.Create<int>(1024, MemoryAllocatorStrategy.DefaultPooled);
      }));

      public static IndexData Fetch(int indexCount)
      {
        DataBuffer<int> db;
        if (!s_intPool.TryFetch(out db))
          return new IndexData();

        if (db.Length < indexCount)
          db.Resize(indexCount);

        return new IndexData(db);
      }

      public static void Return(IndexData data)
      {
        if (data.IndexFormat == IndexFormat.ThirtyTwoBits)
          s_intPool.Return(data.UnderlyingDataBuffer as DataBuffer<int>);
      }

      public static void Clear()
      {
        s_intPool.Clear();
      }
    }

    #endregion

    #region Bounds Checking -- Adapted from BoundingFrustum

    private static ContainmentType PlaneSetContains(ReadOnlySpan<Plane> planes, BoundingVolume bv)
    {
      //If null, we need to check the triangles against the planes
      if (bv is null)
        return ContainmentType.Inside;

      switch (bv.BoundingType)
      {
        case BoundingType.AxisAlignedBoundingBox:
          {
            //return PlaneSetContains(planes, bv.Corners);
            return PlaneSetContains(planes, bv as BoundingBox);
          }
        case BoundingType.Sphere:
        case BoundingType.Capsule:
        case BoundingType.OrientedBoundingBox:
          {
            bool intersects = false;
            for (int i = 0; i < 6; i++)
            {
              ref readonly Plane p = ref planes[i];
              switch (bv.Intersects(p))
              {
                case PlaneIntersectionType.Back:
                  return ContainmentType.Outside;
                case PlaneIntersectionType.Intersects:
                  intersects = true;
                  break;
              }
            }

            if (intersects)
              return ContainmentType.Intersects;

            return ContainmentType.Inside;
          }
        case BoundingType.Frustum:
        case BoundingType.Mesh:
        default:
          {
            return PlaneSetContains(planes, bv.Corners);
          }
      }
    }

    private static ContainmentType PlaneSetContains(ReadOnlySpan<Plane> planes, BoundingBox aabb)
    {
      Vector3 center = aabb.Center;
      Vector3 extents = aabb.Extents;

      Vector3 max = new Vector3(center.X + extents.X, center.Y + extents.Y, center.Z + extents.Z);
      Vector3 min = new Vector3(center.X - extents.X, center.Y - extents.Y, center.Z - extents.Z);

      ContainmentType result = ContainmentType.Inside;

      for (int i = 0; i < planes.Length; i++)
      {
        ref readonly Plane plane = ref planes[i];

        Vector3 p = min;
        Vector3 n = max;

        if (plane.Normal.X >= 0.0f)
        {
          p.X = max.X;
          n.X = min.X;
        }

        if (plane.Normal.Y >= 0.0f)
        {
          p.Y = max.Y;
          n.Y = min.Y;
        }

        if (plane.Normal.Z >= 0.0f)
        {
          p.Z = max.Z;
          n.Z = min.Z;
        }

        if (plane.WhichSide(p) == PlaneIntersectionType.Back)
          return ContainmentType.Outside;

        if (plane.WhichSide(n) == PlaneIntersectionType.Back)
          result = ContainmentType.Intersects;
      }

      return result;
    }

    private static ContainmentType PlaneSetContains(ReadOnlySpan<Plane> planes, ReadOnlySpan<Vector3> points)
    {
      if (points.IsEmpty)
        return ContainmentType.Outside;

      bool outside = false, inside = false, intersects = false;

      for (int i = 0; i < points.Length; i++)
      {
        ref readonly Vector3 point = ref points[i];
        switch (PlaneSetContains(planes, point))
        {
          case ContainmentType.Outside:
            outside = true;
            break;
          case ContainmentType.Inside:
            inside = true;
            break;
          case ContainmentType.Intersects:
            intersects = true;
            break;
        }
      }

      if ((outside && inside) || intersects)
      {
        return ContainmentType.Intersects;
      }
      else if (inside && !outside)
      {
        return ContainmentType.Inside;
      }
      else
      {
        return ContainmentType.Outside;
      }
    }

    private static ContainmentType PlaneSetContains(ReadOnlySpan<Plane> planes, in Vector3 point)
    {
      PlaneIntersectionType result = PlaneIntersectionType.Front;

      for (int i = 0; i < planes.Length; i++)
      {
        ref readonly Plane p = ref planes[i];

        switch (p.WhichSide(point))
        {
          case PlaneIntersectionType.Back:
            return ContainmentType.Outside; //If on negative side, then it's outside (planes pointing inward). Don't need to check the rest.
          case PlaneIntersectionType.Intersects:
            result = PlaneIntersectionType.Intersects; //If intersecting, it may still be outside the frustum so we need to continue checking the rest
            break;
        }
      }

      return (result == PlaneIntersectionType.Intersects) ? ContainmentType.Intersects : ContainmentType.Inside;
    }

    #endregion
  }
}
