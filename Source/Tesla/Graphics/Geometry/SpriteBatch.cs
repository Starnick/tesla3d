﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

// TODO NULLABLE

namespace Tesla.Graphics
{
  /// <summary>
  /// Batcher that specializes in drawing many sprites to the screen. The primary 
  /// function is to enable batching of sprites based on their texture to only
  /// use the minimum number of draw calls needed to render all the requested sprites
  /// to the screen.
  /// </summary>
  public class SpriteBatch : IDisposable
  {
    private const int MaxBatchSize = 2048;

    private VertexBuffer m_vertexBuffer;
    private IndexBuffer m_indexBuffer;
    private DataBuffer<VertexPositionColorTexture> m_vertices;
    private int m_spriteVbPos;

    private PooledArray<Sprite> m_spriteQueue;
    private int[] m_sortedSpriteIndices;
    private PooledArray<Sprite> m_sortedSprites;
    private int m_spriteQueueCount;
    private int m_batchSize;

    private bool m_isDisposed;
    private bool m_inBeginEnd;
    private bool m_applyCameraViewProjection;
    private SpriteSortMode m_sortMode;
    private Matrix m_worldMatrix;
    private IEffectShaderGroup m_customShaderGroup;
    private Effect m_spriteEffect;
    private IEffectParameter m_matrixParam;
    private IEffectParameter m_samplerParam;
    private IEffectShaderGroup m_pass;

    private IRenderContext m_renderContext;
    private IShaderStage m_pixelStage;
    private SamplerState m_ss;

    private TextureComparer m_textureComparer;
    private OrthoComparer m_frontToBackComparer;
    private OrthoComparer m_backToFrontComparer;

    /// <summary>
    /// Gets if the batcher has been diposed.
    /// </summary>
    public bool IsDisposed
    {
      get
      {
        return m_isDisposed;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="SpriteBatch"/> class.
    /// </summary>
    public SpriteBatch() : this(IRenderSystem.Current, MaxBatchSize) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="SpriteBatch"/> class.
    /// </summary>
    /// <param name="maxBatchSize">Maximum number of sprite batches that can be buffered.</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the max batch size is less than or equal to zero.</exception>
    public SpriteBatch(int maxBatchSize) : this(IRenderSystem.Current, maxBatchSize) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="SpriteBatch"/> class.
    /// </summary>
    /// <param name="renderSystem">Render system used to create resources.</param>
    /// <exception cref="ArgumentNullException">Thrown if the render system is null.</exception>
    public SpriteBatch(IRenderSystem renderSystem) : this(renderSystem, MaxBatchSize) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="SpriteBatch"/> class.
    /// </summary>
    /// <param name="renderSystem">Render system used to create resources.</param>
    /// <param name="maxBatchSize">Maximum number of sprite batches that can be buffered.</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the max batch size is less than or equal to zero.</exception>
    /// <exception cref="ArgumentNullException">Thrown if the render system is null.</exception>
    public SpriteBatch(IRenderSystem renderSystem, int maxBatchSize)
    {
      if (maxBatchSize <= 0)
        throw new ArgumentOutOfRangeException(nameof(maxBatchSize), StringLocalizer.Instance.GetLocalizedString("BatchSizeMustBePositive"));

      GraphicsHelper.ThrowIfRenderSystemNull(renderSystem);

      m_vertexBuffer = new VertexBuffer(renderSystem, VertexPositionColorTexture.VertexLayout, maxBatchSize * 4, VertexBufferOptions.Init(ResourceUsage.Dynamic));
      m_vertices = DataBuffer.Create<VertexPositionColorTexture>(maxBatchSize * 4, MemoryAllocatorStrategy.DefaultPooled);
      m_indexBuffer = new IndexBuffer(renderSystem, CreateIndexData(MaxBatchSize), BufferOptions.Init(ResourceUsage.Immutable));
      m_spriteVbPos = 0;

      m_spriteQueue = new PooledArray<Sprite>(maxBatchSize);
      m_spriteQueueCount = 0;
      m_batchSize = maxBatchSize;

      m_isDisposed = false;
      m_inBeginEnd = false;
      m_sortMode = SpriteSortMode.Deferred;
      m_worldMatrix = Matrix.Identity;
      m_customShaderGroup = null;
      m_spriteEffect = renderSystem.StandardEffects.CreateEffect("Sprite");
      m_matrixParam = m_spriteEffect.Parameters["SpriteTransform"];
      m_samplerParam = m_spriteEffect.Parameters["SpriteMapSampler"];
      m_pass = m_spriteEffect.ShaderGroups["SpriteTexture"];

      m_textureComparer = new TextureComparer(this);
      m_frontToBackComparer = new OrthoComparer(this, true);
      m_backToFrontComparer = new OrthoComparer(this, false);
    }

    /// <summary>
    /// Creates a sprite projection matrix that sprite batch uses internally, useful if using a custom shader with spritebatch.
    /// </summary>
    /// <param name="viewport">The camera viewport.</param>
    /// <returns>The sprite projection transform.</returns>
    public static Matrix ComputeSpriteProjection(Viewport viewport)
    {
      Matrix spriteTransform;

      ComputeSpriteProjection(ref viewport, out spriteTransform);

      return spriteTransform;
    }

    /// <summary>
    /// Creates a sprite projection matrix that sprite batch uses internally, useful if using a custom shader with spritebatch.
    /// </summary>
    /// <param name="viewport">The camera viewport.</param>
    /// <param name="spriteTransform">The sprite projection transform.</param>
    public static void ComputeSpriteProjection(ref Viewport viewport, out Matrix spriteTransform)
    {
      float invWidth = (viewport.Width > 0) ? (1f / ((float) viewport.Width)) : 0f;
      float invHeight = (viewport.Height > 0) ? (-1f / ((float) viewport.Height)) : 0f;

      spriteTransform.M11 = invWidth * 2f;
      spriteTransform.M12 = 0f;
      spriteTransform.M13 = 0f;
      spriteTransform.M14 = 0f;

      spriteTransform.M21 = 0f;
      spriteTransform.M22 = invHeight * 2f;
      spriteTransform.M23 = 0f;
      spriteTransform.M24 = 0f;

      spriteTransform.M31 = 0f;
      spriteTransform.M32 = 0f;
      spriteTransform.M33 = 1f;
      spriteTransform.M34 = 0f;

      spriteTransform.M41 = -1f;
      spriteTransform.M42 = 1f;
      spriteTransform.M43 = 0f;
      spriteTransform.M44 = 1f;
    }

    /// <summary>
    /// Begins a sprite batch operation.
    /// </summary>
    /// <param name="renderContext">Render context</param>
    public void Begin(IRenderContext renderContext)
    {
      Begin(renderContext, SpriteSortMode.Deferred, null, null, null, null, null, Matrix.Identity, false);
    }

    /// <summary>
    /// Begins a sprite batch operation.
    /// </summary>
    /// <param name="renderContext">Render context</param>
    /// <param name="sortMode">Sort mode to use</param>
    public void Begin(IRenderContext renderContext, SpriteSortMode sortMode)
    {
      Begin(renderContext, sortMode, null, null, null, null, null, Matrix.Identity, false);
    }

    /// <summary>
    /// Begins a sprite batch operation.
    /// </summary>
    /// <param name="renderContext">Render context</param>
    /// <param name="sortMode">Sort mode to use</param>
    /// <param name="worldMatrix">Transformation matrix for scale, rotation, and translation of each sprite.</param>
    public void Begin(IRenderContext renderContext, SpriteSortMode sortMode, Matrix worldMatrix)
    {
      Begin(renderContext, sortMode, null, null, null, null, worldMatrix, false);
    }

    /// <summary>
    /// Begins a sprite batch operation.
    /// </summary>
    /// <param name="renderContext">Render context</param>
    /// <param name="sortMode">Sort mode to use</param>
    /// <param name="worldMatrix">Transformation matrix for scale, rotation, and translation of each sprite.</param>
    /// <param name="applyCameraViewProj">Applies the view-projection matrix from the camera, otherwise creates a projection to draw in screen space.</param>
    public void Begin(IRenderContext renderContext, SpriteSortMode sortMode, Matrix worldMatrix, bool applyCameraViewProj)
    {
      Begin(renderContext, sortMode, null, null, null, null, worldMatrix, applyCameraViewProj);
    }

    /// <summary>
    /// Begins a sprite batch operation.
    /// </summary>
    /// <param name="renderContext">Render context</param>
    /// <param name="sortMode">Sort mode to use</param>
    /// <param name="blendState">Blend state to use, if null then <see cref="BlendState.AlphaBlendNonPremultiplied"/> is used.</param>
    public void Begin(IRenderContext renderContext, SpriteSortMode sortMode, BlendState blendState)
    {
      Begin(renderContext, sortMode, blendState, null, null, null, null, Matrix.Identity, false);
    }

    /// <summary>
    /// Begins a sprite batch operation.
    /// </summary>
    /// <param name="renderContext">Render context</param>
    /// <param name="sortMode">Sort mode to use</param>
    /// <param name="blendState">Blend state to use, if null then <see cref="BlendState.AlphaBlendNonPremultiplied"/> is used.</param>
    /// <param name="worldMatrix">Transformation matrix for scale, rotation, and translation of each sprite.</param>
    public void Begin(IRenderContext renderContext, SpriteSortMode sortMode, BlendState blendState, Matrix worldMatrix)
    {
      Begin(renderContext, sortMode, blendState, null, null, null, worldMatrix, false);
    }

    /// <summary>
    /// Begins a sprite batch operation.
    /// </summary>
    /// <param name="renderContext">Render context</param>
    /// <param name="sortMode">Sort mode to use</param>
    /// <param name="blendState">Blend state to use, if null then <see cref="BlendState.AlphaBlendNonPremultiplied"/> is used.</param>
    /// <param name="worldMatrix">Transformation matrix for scale, rotation, and translation of each sprite.</param>
    /// <param name="applyCameraViewProj">Applies the view-projection matrix from the camera, otherwise creates a projection to draw in screen space.</param>
    public void Begin(IRenderContext renderContext, SpriteSortMode sortMode, BlendState blendState, Matrix worldMatrix, bool applyCameraViewProj)
    {
      Begin(renderContext, sortMode, blendState, null, null, null, worldMatrix, applyCameraViewProj);
    }

    /// <summary>
    /// Begins a sprite batch operation.
    /// </summary>
    /// <param name="renderContext">Render context</param>
    /// <param name="sortMode">Sort mode to use</param>
    /// <param name="blendState">Blend state to use, if null then <see cref="BlendState.AlphaBlendNonPremultiplied"/> is used.</param>
    /// <param name="rasterizerState">Rasterizer state to use, if null then <see cref="RasterizerState.CullBackClockwiseFront"/> is used.</param>
    /// <param name="samplerState">Sampler state to use, if null then <see cref="SamplerState.LinearClamp"/> is used.</param>
    /// <param name="depthStencilState">Depth stencil state to use, if null then <see cref="DepthStencilState.None"/> is used.</param>
    public void Begin(IRenderContext renderContext, SpriteSortMode sortMode, BlendState blendState, RasterizerState rasterizerState, SamplerState samplerState, DepthStencilState depthStencilState)
    {
      Begin(renderContext, sortMode, blendState, rasterizerState, samplerState, depthStencilState, null, Matrix.Identity, false);
    }

    /// <summary>
    /// Begins a sprite batch operation.
    /// </summary>
    /// <param name="renderContext">Render context</param>
    /// <param name="sortMode">Sort mode to use</param>
    /// <param name="blendState">Blend state to use, if null then <see cref="BlendState.AlphaBlendNonPremultiplied"/> is used.</param>
    /// <param name="rasterizerState">Rasterizer state to use, if null then <see cref="RasterizerState.CullBackClockwiseFront"/> is used.</param>
    /// <param name="samplerState">Sampler state to use, if null then <see cref="SamplerState.LinearClamp"/> is used.</param>
    /// <param name="depthStencilState">Depth stencil state to use, if null then <see cref="DepthStencilState.None"/> is used.</param>
    /// <param name="customShaderGroup">Custom effect to use in leui of the sprite effect, if null then the sprite effect is used.</param>
    public void Begin(IRenderContext renderContext, SpriteSortMode sortMode, BlendState blendState, RasterizerState rasterizerState, SamplerState samplerState, DepthStencilState depthStencilState, IEffectShaderGroup customShaderGroup)
    {
      Begin(renderContext, sortMode, blendState, rasterizerState, samplerState, depthStencilState, customShaderGroup, Matrix.Identity, false);
    }

    /// <summary>
    /// Begins a sprite batch operation.
    /// </summary>
    /// <param name="renderContext">Render context</param>
    /// <param name="sortMode">Sort mode to use</param>
    /// <param name="blendState">Blend state to use, if null then <see cref="BlendState.AlphaBlendNonPremultiplied"/> is used.</param>
    /// <param name="rasterizerState">Rasterizer state to use, if null then <see cref="RasterizerState.CullBackClockwiseFront"/> is used.</param>
    /// <param name="samplerState">Sampler state to use, if null then <see cref="SamplerState.LinearClamp"/> is used.</param>
    /// <param name="depthStencilState">Depth stencil state to use, if null then <see cref="DepthStencilState.None"/> is used.</param>
    /// <param name="worldMatrix">Transformation matrix for scale, rotation, and translation of each sprite.</param>
    public void Begin(IRenderContext renderContext, SpriteSortMode sortMode, BlendState blendState, RasterizerState rasterizerState, SamplerState samplerState, DepthStencilState depthStencilState, Matrix worldMatrix)
    {
      Begin(renderContext, sortMode, blendState, rasterizerState, samplerState, depthStencilState, null, worldMatrix, false);
    }

    /// <summary>
    /// Begins a sprite batch operation.
    /// </summary>
    /// <param name="renderContext">Render context</param>
    /// <param name="sortMode">Sort mode to use</param>
    /// <param name="blendState">Blend state to use, if null then <see cref="BlendState.AlphaBlendNonPremultiplied"/> is used.</param>
    /// <param name="rasterizerState">Rasterizer state to use, if null then <see cref="RasterizerState.CullBackClockwiseFront"/> is used.</param>
    /// <param name="samplerState">Sampler state to use, if null then <see cref="SamplerState.LinearClamp"/> is used.</param>
    /// <param name="depthStencilState">Depth stencil state to use, if null then <see cref="DepthStencilState.None"/> is used.</param>
    /// <param name="worldMatrix">Transformation matrix for scale, rotation, and translation of each sprite.</param>
    /// <param name="applyCameraViewProj">Applies the view-projection matrix from the camera, otherwise creates a projection to draw in screen space.</param>
    public void Begin(IRenderContext renderContext, SpriteSortMode sortMode, BlendState blendState, RasterizerState rasterizerState, SamplerState samplerState, DepthStencilState depthStencilState, Matrix worldMatrix, bool applyCameraViewProj)
    {
      Begin(renderContext, sortMode, blendState, rasterizerState, samplerState, depthStencilState, null, worldMatrix, applyCameraViewProj);
    }

    /// <summary>
    /// Begins a sprite batch operation.
    /// </summary>
    /// <param name="renderContext">Render context</param>
    /// <param name="sortMode">Sort mode to use</param>
    /// <param name="blendState">Blend state to use, if null then <see cref="BlendState.AlphaBlendNonPremultiplied"/> is used.</param>
    /// <param name="rasterizerState">Rasterizer state to use, if null then <see cref="RasterizerState.CullBackClockwiseFront"/> is used.</param>
    /// <param name="samplerState">Sampler state to use, if null then <see cref="SamplerState.LinearClamp"/> is used.</param>
    /// <param name="depthStencilState">Depth stencil state to use, if null then <see cref="DepthStencilState.None"/> is used.</param>
    /// <param name="customShaderGroup">Custom effect shader group to use in leui of the sprite effect, if null then the sprite effect is used.</param>
    /// <param name="worldMatrix">Transformation matrix for scale, rotation, and translation of each sprite.</param>
    /// <param name="applyCameraViewProj">Applies the view-projection matrix from the camera, otherwise creates a projection to draw in screen space.</param>
    public void Begin(IRenderContext renderContext, SpriteSortMode sortMode, BlendState blendState, RasterizerState rasterizerState, SamplerState samplerState, DepthStencilState depthStencilState, IEffectShaderGroup customShaderGroup, Matrix worldMatrix, bool applyCameraViewProj)
    {
      CheckDisposed();

      if (m_inBeginEnd)
        throw new InvalidOperationException(StringLocalizer.Instance.GetLocalizedString("CannotNestBeginCalls"));

      if (renderContext == null)
        throw new ArgumentNullException("renderContext", StringLocalizer.Instance.GetLocalizedString("NullRenderContext"));

      m_pixelStage = renderContext.GetShaderStage(ShaderStage.PixelShader);
      m_renderContext = renderContext;
      m_sortMode = sortMode;

      if (blendState == null)
        blendState = BlendState.AlphaBlendNonPremultiplied;

      if (rasterizerState == null)
        rasterizerState = RasterizerState.CullBackClockwiseFront;

      if (samplerState == null)
        samplerState = SamplerState.LinearClamp;

      if (depthStencilState == null)
        depthStencilState = DepthStencilState.None;

      renderContext.BlendState = blendState;
      renderContext.RasterizerState = rasterizerState;
      renderContext.DepthStencilState = depthStencilState;
      m_ss = samplerState;

      m_customShaderGroup = customShaderGroup;
      m_worldMatrix = worldMatrix;
      m_inBeginEnd = true;
      m_applyCameraViewProjection = applyCameraViewProj;

      SetBuffers();

      if (customShaderGroup == null)
        ApplyEffect();
    }

    /// <summary>
    /// Adds a sprite to the batch for rendering using the specified parameters. Depending on the sort mode
    /// this may be an immediate draw operation or one queued for later processing.
    /// </summary>
    /// <param name="texture">Sprite texture, if null then <see cref="Texture.Default2D"/> is used.</param>
    /// <param name="position">Position to draw the sprite, in screen coordinates.</param>
    /// <param name="tintColor">Color to tint the sprite. Use <see cref="Color.White"/> for no tinting.</param>
    public void Draw(Texture2D texture, Vector2 position, Color tintColor)
    {
      Vector4 dstRect = new Vector4(position.X, position.Y, 1, 1);
      Rectangle srcRect = new Rectangle(0, 0, texture.Width, texture.Height);
      DrawInternal(texture, dstRect, srcRect, true, tintColor, Angle.Zero, Vector2.Zero, SpriteFlipEffect.None, 0);
    }

    /// <summary>
    /// Adds a sprite to the batch for rendering using the specified parameters. Depending on the sort mode
    /// this may be an immediate draw operation or one queued for later processing.
    /// </summary>
    /// <param name="texture">Sprite texture, if null then <see cref="Texture.Default2D"/> is used.</param>
    /// <param name="position">Position to draw the sprite, in screen coordinates.</param>
    /// <param name="tintColor">Color to tint the sprite. Use <see cref="Color.White"/> for no tinting.</param>
    /// <param name="angle">Rotation angle to rotate the sprite about its origin.</param>
    public void Draw(Texture2D texture, Vector2 position, Color tintColor, Angle angle)
    {
      Vector4 dstRect = new Vector4(position.X, position.Y, 1, 1);
      Rectangle srcRect = new Rectangle(0, 0, texture.Width, texture.Height);
      DrawInternal(texture, dstRect, srcRect, true, tintColor, angle, Vector2.Zero, SpriteFlipEffect.None, 0);
    }

    /// <summary>
    /// Adds a sprite to the batch for rendering using the specified parameters. Depending on the sort mode
    /// this may be an immediate draw operation or one queued for later processing.
    /// </summary>
    /// <param name="texture">Sprite texture, if null then <see cref="Texture.Default2D"/> is used.</param>
    /// <param name="position">Position to draw the sprite, in screen coordinates.</param>
    /// <param name="tintColor">Color to tint the sprite. Use <see cref="Color.White"/> for no tinting.</param>
    /// <param name="angle">Rotation angle to rotate the sprite about its origin.</param>
    /// <param name="origin">Sprite origin, (0,0) is the default which represents the upper-left corner of the sprite.</param>
    public void Draw(Texture2D texture, Vector2 position, Color tintColor, Angle angle, Vector2 origin)
    {
      Vector4 dstRect = new Vector4(position.X, position.Y, 1, 1);
      Rectangle srcRect = new Rectangle(0, 0, texture.Width, texture.Height);
      DrawInternal(texture, dstRect, srcRect, true, tintColor, angle, origin, SpriteFlipEffect.None, 0);
    }

    /// <summary>
    /// Adds a sprite to the batch for rendering using the specified parameters. Depending on the sort mode
    /// this may be an immediate draw operation or one queued for later processing.
    /// </summary>
    /// <param name="texture">Sprite texture, if null then <see cref="Texture.Default2D"/> is used.</param>
    /// <param name="position">Position to draw the sprite, in screen coordinates.</param>
    /// <param name="tintColor">Color to tint the sprite. Use <see cref="Color.White"/> for no tinting.</param>
    /// <param name="angle">Rotation angle to rotate the sprite about its origin.</param>
    /// <param name="origin">Sprite origin, (0,0) is the default which represents the upper-left corner of the sprite.</param>
    /// <param name="flipEffect">Flip mirror effects to apply to the sprite.</param>
    public void Draw(Texture2D texture, Vector2 position, Color tintColor, Angle angle, Vector2 origin, SpriteFlipEffect flipEffect)
    {
      Vector4 dstRect = new Vector4(position.X, position.Y, 1, 1);
      Rectangle srcRect = new Rectangle(0, 0, texture.Width, texture.Height);
      DrawInternal(texture, dstRect, srcRect, true, tintColor, angle, origin, flipEffect, 0);
    }

    /// <summary>
    /// Adds a sprite to the batch for rendering using the specified parameters. Depending on the sort mode
    /// this may be an immediate draw operation or one queued for later processing.
    /// </summary>
    /// <param name="texture">Sprite texture, if null then <see cref="Texture.Default2D"/> is used.</param>
    /// <param name="destinationRectangle">Rectangle that specifies (in screen coordinates) the destination, position and size, for drawing the sprite. If this rectangle is not the same size as the source,
    /// the sprite will be scaled to fit.</param>
    /// <param name="sourceRectangle">Rectangle that specifies the source texels from a texture, if null then the entire texture is used.</param>
    /// <param name="tintColor">Color to tint the sprite. Use <see cref="Color.White"/> for no tinting.</param>
    public void Draw(Texture2D texture, Rectangle destinationRectangle, Rectangle? sourceRectangle, Color tintColor)
    {
      Vector4 dstRect = new Vector4(destinationRectangle.X, destinationRectangle.Y, destinationRectangle.Width, destinationRectangle.Height);
      DrawInternal(texture, dstRect, sourceRectangle, false, tintColor, Angle.Zero, Vector2.Zero, SpriteFlipEffect.None, 0);
    }

    /// <summary>
    /// Adds a sprite to the batch for rendering using the specified parameters. Depending on the sort mode
    /// this may be an immediate draw operation or one queued for later processing.
    /// </summary>
    /// <param name="texture">Sprite texture, if null then <see cref="Texture.Default2D"/> is used.</param>
    /// <param name="destinationRectangle">Rectangle that specifies (in screen coordinates) the destination, position and size, for drawing the sprite. If this rectangle is not the same size as the source,
    /// the sprite will be scaled to fit.</param>
    /// <param name="sourceRectangle">Rectangle that specifies the source texels from a texture, if null then the entire texture is used.</param>
    /// <param name="tintColor">Color to tint the sprite. Use <see cref="Color.White"/> for no tinting.</param>
    /// <param name="angle">Rotation angle to rotate the sprite about its origin.</param>
    public void Draw(Texture2D texture, Rectangle destinationRectangle, Rectangle? sourceRectangle, Color tintColor, Angle angle)
    {
      Vector4 dstRect = new Vector4(destinationRectangle.X, destinationRectangle.Y, destinationRectangle.Width, destinationRectangle.Height);
      DrawInternal(texture, dstRect, sourceRectangle, false, tintColor, angle, Vector2.Zero, SpriteFlipEffect.None, 0);
    }

    /// <summary>
    /// Adds a sprite to the batch for rendering using the specified parameters. Depending on the sort mode
    /// this may be an immediate draw operation or one queued for later processing.
    /// </summary>
    /// <param name="texture">Sprite texture, if null then <see cref="Texture.Default2D"/> is used.</param>
    /// <param name="destinationRectangle">Rectangle that specifies (in screen coordinates) the destination, position and size, for drawing the sprite. If this rectangle is not the same size as the source,
    /// the sprite will be scaled to fit.</param>
    /// <param name="sourceRectangle">Rectangle that specifies the source texels from a texture, if null then the entire texture is used.</param>
    /// <param name="tintColor">Color to tint the sprite. Use <see cref="Color.White"/> for no tinting.</param>
    /// <param name="angle">Rotation angle to rotate the sprite about its origin.</param>
    /// <param name="origin">Sprite origin, (0,0) is the default which represents the upper-left corner of the sprite.</param>
    public void Draw(Texture2D texture, Rectangle destinationRectangle, Rectangle? sourceRectangle, Color tintColor, Angle angle, Vector2 origin)
    {
      Vector4 dstRect = new Vector4(destinationRectangle.X, destinationRectangle.Y, destinationRectangle.Width, destinationRectangle.Height);
      DrawInternal(texture, dstRect, sourceRectangle, false, tintColor, angle, origin, SpriteFlipEffect.None, 0);
    }

    /// <summary>
    /// Adds a sprite to the batch for rendering using the specified parameters. Depending on the sort mode
    /// this may be an immediate draw operation or one queued for later processing.
    /// </summary>
    /// <param name="texture">Sprite texture, if null then <see cref="Texture.Default2D"/> is used.</param>
    /// <param name="destinationRectangle">Rectangle that specifies (in screen coordinates) the destination, position and size, for drawing the sprite. If this rectangle is not the same size as the source,
    /// the sprite will be scaled to fit.</param>
    /// <param name="sourceRectangle">Rectangle that specifies the source texels from a texture, if null then the entire texture is used.</param>
    /// <param name="tintColor">Color to tint the sprite. Use <see cref="Color.White"/> for no tinting.</param>
    /// <param name="angle">Rotation angle to rotate the sprite about its origin.</param>
    /// <param name="origin">Sprite origin, (0,0) is the default which represents the upper-left corner of the sprite.</param>
    /// <param name="flipEffect">Flip mirror effects to apply to the sprite.</param>
    public void Draw(Texture2D texture, Rectangle destinationRectangle, Rectangle? sourceRectangle, Color tintColor, Angle angle, Vector2 origin, SpriteFlipEffect flipEffect)
    {
      Vector4 dstRect = new Vector4(destinationRectangle.X, destinationRectangle.Y, destinationRectangle.Width, destinationRectangle.Height);
      DrawInternal(texture, dstRect, sourceRectangle, false, tintColor, angle, origin, flipEffect, 0);
    }

    /// <summary>
    /// Adds a sprite to the batch for rendering using the specified parameters. Depending on the sort mode
    /// this may be an immediate draw operation or one queued for later processing.
    /// </summary>
    /// <param name="texture">Sprite texture, if null then <see cref="Texture.Default2D"/> is used.</param>
    /// <param name="destinationRectangle">Rectangle that specifies (in screen coordinates) the destination, position and size, for drawing the sprite. If this rectangle is not the same size as the source,
    /// the sprite will be scaled to fit.</param>
    /// <param name="tintColor">Color to tint the sprite. Use <see cref="Color.White"/> for no tinting.</param>
    public void Draw(Texture2D texture, Rectangle destinationRectangle, Color tintColor)
    {
      Vector4 dstRect = new Vector4(destinationRectangle.X, destinationRectangle.Y, destinationRectangle.Width, destinationRectangle.Height);
      DrawInternal(texture, dstRect, null, false, tintColor, Angle.Zero, Vector2.Zero, SpriteFlipEffect.None, 0);
    }

    /// <summary>
    /// Adds a sprite to the batch for rendering using the specified parameters. Depending on the sort mode
    /// this may be an immediate draw operation or one queued for later processing.
    /// </summary>
    /// <param name="texture">Sprite texture, if null then <see cref="Texture.Default2D"/> is used.</param>
    /// <param name="destinationRectangle">Rectangle that specifies (in screen coordinates) the destination, position and size, for drawing the sprite. If this rectangle is not the same size as the source,
    /// the sprite will be scaled to fit.</param>
    /// <param name="tintColor">Color to tint the sprite. Use <see cref="Color.White"/> for no tinting.</param>
    /// <param name="angle">Rotation angle to rotate the sprite about its origin.</param>
    public void Draw(Texture2D texture, Rectangle destinationRectangle, Color tintColor, Angle angle)
    {
      Vector4 dstRect = new Vector4(destinationRectangle.X, destinationRectangle.Y, destinationRectangle.Width, destinationRectangle.Height);
      DrawInternal(texture, dstRect, null, false, tintColor, angle, Vector2.Zero, SpriteFlipEffect.None, 0);
    }

    /// <summary>
    /// Adds a sprite to the batch for rendering using the specified parameters. Depending on the sort mode
    /// this may be an immediate draw operation or one queued for later processing.
    /// </summary>
    /// <param name="texture">Sprite texture, if null then <see cref="Texture.Default2D"/> is used.</param>
    /// <param name="destinationRectangle">Rectangle that specifies (in screen coordinates) the destination, position and size, for drawing the sprite. If this rectangle is not the same size as the source,
    /// the sprite will be scaled to fit.</param>
    /// <param name="tintColor">Color to tint the sprite. Use <see cref="Color.White"/> for no tinting.</param>
    /// <param name="angle">Rotation angle to rotate the sprite about its origin.</param>
    /// <param name="origin">Sprite origin, (0,0) is the default which represents the upper-left corner of the sprite.</param>
    public void Draw(Texture2D texture, Rectangle destinationRectangle, Color tintColor, Angle angle, Vector2 origin)
    {
      Vector4 dstRect = new Vector4(destinationRectangle.X, destinationRectangle.Y, destinationRectangle.Width, destinationRectangle.Height);
      DrawInternal(texture, dstRect, null, false, tintColor, angle, origin, SpriteFlipEffect.None, 0);
    }

    /// <summary>
    /// Adds a sprite to the batch for rendering using the specified parameters. Depending on the sort mode
    /// this may be an immediate draw operation or one queued for later processing.
    /// </summary>
    /// <param name="texture">Sprite texture, if null then <see cref="Texture.Default2D"/> is used.</param>
    /// <param name="destinationRectangle">Rectangle that specifies (in screen coordinates) the destination, position and size, for drawing the sprite. If this rectangle is not the same size as the source,
    /// the sprite will be scaled to fit.</param>
    /// <param name="tintColor">Color to tint the sprite. Use <see cref="Color.White"/> for no tinting.</param>
    /// <param name="angle">Rotation angle to rotate the sprite about its origin.</param>
    /// <param name="origin">Sprite origin, (0,0) is the default which represents the upper-left corner of the sprite.</param>
    /// <param name="flipEffect">Flip mirror effects to apply to the sprite.</param>
    public void Draw(Texture2D texture, Rectangle destinationRectangle, Color tintColor, Angle angle, Vector2 origin, SpriteFlipEffect flipEffect)
    {
      Vector4 dstRect = new Vector4(destinationRectangle.X, destinationRectangle.Y, destinationRectangle.Width, destinationRectangle.Height);
      DrawInternal(texture, dstRect, null, false, tintColor, angle, origin, flipEffect, 0);
    }

    /// <summary>
    /// Adds a sprite to the batch for rendering using the specified parameters. Depending on the sort mode
    /// this may be an immediate draw operation or one queued for later processing.
    /// </summary>
    /// <param name="texture">Sprite texture, if null then <see cref="Texture.Default2D"/> is used.</param>
    /// <param name="position">Position to draw the sprite, in screen coordinates.</param>
    /// <param name="sourceRectangle">Rectangle that specifies the source texels from a texture, if null then the entire texture is used.</param>
    /// <param name="tintColor">Color to tint the sprite. Use <see cref="Color.White"/> for no tinting.</param>
    public void Draw(Texture2D texture, Vector2 position, Rectangle? sourceRectangle, Color tintColor)
    {
      Vector4 dstRect = new Vector4(position.X, position.Y, 1, 1);
      DrawInternal(texture, dstRect, sourceRectangle, true, tintColor, Angle.Zero, Vector2.Zero, SpriteFlipEffect.None, 0);
    }

    /// <summary>
    /// Adds a sprite to the batch for rendering using the specified parameters. Depending on the sort mode
    /// this may be an immediate draw operation or one queued for later processing.
    /// </summary>
    /// <param name="texture">Sprite texture, if null then <see cref="Texture.Default2D"/> is used.</param>
    /// <param name="position">Position to draw the sprite, in screen coordinates.</param>
    /// <param name="sourceRectangle">Rectangle that specifies the source texels from a texture, if null then the entire texture is used.</param>
    /// <param name="tintColor">Color to tint the sprite. Use <see cref="Color.White"/> for no tinting.</param>
    /// <param name="angle">Rotation angle to rotate the sprite about its origin.</param>
    public void Draw(Texture2D texture, Vector2 position, Rectangle? sourceRectangle, Color tintColor, Angle angle)
    {
      Vector4 dstRect = new Vector4(position.X, position.Y, 1, 1);
      DrawInternal(texture, dstRect, sourceRectangle, true, tintColor, angle, Vector2.Zero, SpriteFlipEffect.None, 0);
    }

    /// <summary>
    /// Adds a sprite to the batch for rendering using the specified parameters. Depending on the sort mode
    /// this may be an immediate draw operation or one queued for later processing.
    /// </summary>
    /// <param name="texture">Sprite texture, if null then <see cref="Texture.Default2D"/> is used.</param>
    /// <param name="position">Position to draw the sprite, in screen coordinates.</param>
    /// <param name="sourceRectangle">Rectangle that specifies the source texels from a texture, if null then the entire texture is used.</param>
    /// <param name="tintColor">Color to tint the sprite. Use <see cref="Color.White"/> for no tinting.</param>
    /// <param name="angle">Rotation angle to rotate the sprite about its origin.</param>
    /// <param name="origin">Sprite origin, (0,0) is the default which represents the upper-left corner of the sprite.</param>
    public void Draw(Texture2D texture, Vector2 position, Rectangle? sourceRectangle, Color tintColor, Angle angle, Vector2 origin)
    {
      Vector4 dstRect = new Vector4(position.X, position.Y, 1, 1);
      DrawInternal(texture, dstRect, sourceRectangle, true, tintColor, angle, origin, SpriteFlipEffect.None, 0);
    }

    /// <summary>
    /// Adds a sprite to the batch for rendering using the specified parameters. Depending on the sort mode
    /// this may be an immediate draw operation or one queued for later processing.
    /// </summary>
    /// <param name="texture">Sprite texture, if null then <see cref="Texture.Default2D"/> is used.</param>
    /// <param name="position">Position to draw the sprite, in screen coordinates.</param>
    /// <param name="sourceRectangle">Rectangle that specifies the source texels from a texture, if null then the entire texture is used.</param>
    /// <param name="tintColor">Color to tint the sprite. Use <see cref="Color.White"/> for no tinting.</param>
    /// <param name="angle">Rotation angle to rotate the sprite about its origin.</param>
    /// <param name="origin">Sprite origin, (0,0) is the default which represents the upper-left corner of the sprite.</param>
    /// <param name="flipEffect">Flip mirror effects to apply to the sprite.</param>
    public void Draw(Texture2D texture, Vector2 position, Rectangle? sourceRectangle, Color tintColor, Angle angle, Vector2 origin, SpriteFlipEffect flipEffect)
    {
      Vector4 dstRect = new Vector4(position.X, position.Y, 1, 1);
      DrawInternal(texture, dstRect, sourceRectangle, true, tintColor, angle, origin, flipEffect, 0);
    }

    /// <summary>
    /// Adds a sprite to the batch for rendering using the specified parameters. Depending on the sort mode
    /// this may be an immediate draw operation or one queued for later processing.
    /// </summary>
    /// <param name="texture">Sprite texture, if null then <see cref="Texture.Default2D"/> is used.</param>
    /// <param name="destinationRectangle">Rectangle that specifies (in screen coordinates) the destination, position and size, for drawing the sprite. If this rectangle is not the same size as the source,
    /// the sprite will be scaled to fit.</param>
    /// <param name="sourceRectangle">Rectangle that specifies the source texels from a texture, if null then the entire texture is used.</param>
    /// <param name="tintColor">Color to tint the sprite. Use <see cref="Color.White"/> for no tinting.</param>
    /// <param name="angle">Rotation angle to rotate the sprite about its origin.</param>
    /// <param name="origin">Sprite origin, (0,0) is the default which represents the upper-left corner of the sprite.</param>
    /// <param name="flipEffect">Flip mirror effects to apply to the sprite.</param>
    /// <param name="depth">Depth of the sprite for use with sorting. Lower values indicate sprites that are in the front, higher indicate those in back.</param>
    public void Draw(Texture2D texture, Rectangle destinationRectangle, Rectangle? sourceRectangle, Color tintColor, Angle angle, Vector2 origin, SpriteFlipEffect flipEffect, float depth)
    {
      Vector4 dstRect = new Vector4(destinationRectangle.X, destinationRectangle.Y, destinationRectangle.Width, destinationRectangle.Height);
      DrawInternal(texture, dstRect, sourceRectangle, false, tintColor, angle, origin, flipEffect, depth);
    }

    /// <summary>
    /// Adds a sprite to the batch for rendering using the specified parameters. Depending on the sort mode
    /// this may be an immediate draw operation or one queued for later processing.
    /// </summary>
    /// <param name="texture">Sprite texture, if null then <see cref="Texture.Default2D"/> is used.</param>
    /// <param name="position">Position to draw the sprite, in screen coordinates.</param>
    /// <param name="scale">Scaling factor.</param>
    /// <param name="sourceRectangle">Rectangle that specifies the source texels from a texture, if null then the entire texture is used.</param>
    /// <param name="tintColor">Color to tint the sprite. Use <see cref="Color.White"/> for no tinting.</param>
    /// <param name="angle">Rotation angle to rotate the sprite about its origin.</param>
    /// <param name="origin">Sprite origin, (0,0) is the default which represents the upper-left corner of the sprite.</param>
    /// <param name="flipEffect">Flip mirror effects to apply to the sprite.</param>
    /// <param name="depth">Depth of the sprite for use with sorting. Lower values indicate sprites that are in the front, higher indicate those in back.</param>
    public void Draw(Texture2D texture, Vector2 position, Vector2 scale, Rectangle? sourceRectangle, Color tintColor, Angle angle, Vector2 origin, SpriteFlipEffect flipEffect, float depth)
    {
      Vector4 dstRect = new Vector4(position.X, position.Y, scale.X, scale.Y);
      DrawInternal(texture, dstRect, sourceRectangle, true, tintColor, angle, origin, flipEffect, depth);
    }

    /// <summary>
    /// Adds a sprite to the batch for rendering using the specified parameters. Depending on the sort mode
    /// this may be an immediate draw operation or one queued for later processing.
    /// </summary>
    /// <param name="texture">Sprite texture, if null then <see cref="Texture.Default2D"/> is used.</param>
    /// <param name="position">Position to draw the sprite, in screen coordinates.</param>
    /// <param name="scale">Scaling factor.</param>
    /// <param name="sourceRectangle">Rectangle that specifies the source texels from a texture, if null then the entire texture is used.</param>
    /// <param name="tintColor">Color to tint the sprite. Use <see cref="Color.White"/> for no tinting.</param>
    /// <param name="angle">Rotation angle to rotate the sprite about its origin.</param>
    /// <param name="origin">Sprite origin, (0,0) is the default which represents the upper-left corner of the sprite.</param>
    /// <param name="flipEffect">Flip mirror effects to apply to the sprite.</param>
    /// <param name="depth">Depth of the sprite for use with sorting. Lower values indicate sprites that are in the front, higher indicate those in back.</param>
    public void Draw(Texture2D texture, Vector2 position, float scale, Rectangle? sourceRectangle, Color tintColor, Angle angle, Vector2 origin, SpriteFlipEffect flipEffect, float depth)
    {
      Vector4 dstRect = new Vector4(position.X, position.Y, scale, scale);
      DrawInternal(texture, dstRect, sourceRectangle, true, tintColor, angle, origin, flipEffect, depth);
    }

    /// <summary>
    /// Adds a string to the batch for rendering using the specified parameters. The text is rendered as a series of sprites, one for each character. Depending on the sort mode
    /// this may be an immediate draw operation or one queued for later processing.
    /// </summary>
    /// <param name="font">Font that represents the character data that is used to draw the text.</param>
    /// <param name="text">Text to draw.</param>
    /// <param name="position">Position to draw the sprite, in screen coordinates.</param>
    /// <param name="tintColor">Color to tint the sprite. Use <see cref="Color.White"/> for no tinting.</param>
    public void DrawString(SpriteFont font, String text, Vector2 position, Color tintColor)
    {
      StringReference str = new StringReference(text);
      font.DrawString(this, str, position, Vector2.One, tintColor, Angle.Zero, Vector2.Zero, SpriteFlipEffect.None, 0);
    }

    /// <summary>
    /// Adds a string to the batch for rendering using the specified parameters. The text is rendered as a series of sprites, one for each character. Depending on the sort mode
    /// this may be an immediate draw operation or one queued for later processing.
    /// </summary>
    /// <param name="font">Font that represents the character data that is used to draw the text.</param>
    /// <param name="text">Text to draw.</param>
    /// <param name="position">Position to draw the sprite, in screen coordinates.</param>
    /// <param name="tintColor">Color to tint the sprite. Use <see cref="Color.White"/> for no tinting.</param>
    /// <param name="angle">Rotation angle to rotate the sprite about its origin.</param>
    public void DrawString(SpriteFont font, String text, Vector2 position, Color tintColor, Angle angle)
    {
      StringReference str = new StringReference(text);
      font.DrawString(this, str, position, Vector2.One, tintColor, angle, Vector2.Zero, SpriteFlipEffect.None, 0);
    }

    /// <summary>
    /// Adds a string to the batch for rendering using the specified parameters. The text is rendered as a series of sprites, one for each character. Depending on the sort mode
    /// this may be an immediate draw operation or one queued for later processing.
    /// </summary>
    /// <param name="font">Font that represents the character data that is used to draw the text.</param>
    /// <param name="text">Text to draw.</param>
    /// <param name="position">Position to draw the sprite, in screen coordinates.</param>
    /// <param name="tintColor">Color to tint the sprite. Use <see cref="Color.White"/> for no tinting.</param>
    /// <param name="angle">Rotation angle to rotate the sprite about its origin.</param>
    /// <param name="origin">Sprite origin, (0,0) is the default which represents the upper-left corner of the sprite.</param>
    public void DrawString(SpriteFont font, String text, Vector2 position, Color tintColor, Angle angle, Vector2 origin)
    {
      StringReference str = new StringReference(text);
      font.DrawString(this, str, position, Vector2.One, tintColor, angle, origin, SpriteFlipEffect.None, 0);
    }

    /// <summary>
    /// Adds a string to the batch for rendering using the specified parameters. The text is rendered as a series of sprites, one for each character. Depending on the sort mode
    /// this may be an immediate draw operation or one queued for later processing.
    /// </summary>
    /// <param name="font">Font that represents the character data that is used to draw the text.</param>
    /// <param name="text">Text to draw.</param>
    /// <param name="position">Position to draw the sprite, in screen coordinates.</param>
    /// <param name="tintColor">Color to tint the sprite. Use <see cref="Color.White"/> for no tinting.</param>
    /// <param name="angle">Rotation angle to rotate the sprite about its origin.</param>
    /// <param name="origin">Sprite origin, (0,0) is the default which represents the upper-left corner of the sprite.</param>
    /// <param name="flipEffect">Flip mirror effects to apply to the sprite.</param>
    public void DrawString(SpriteFont font, String text, Vector2 position, Color tintColor, Angle angle, Vector2 origin, SpriteFlipEffect flipEffect)
    {
      StringReference str = new StringReference(text);
      font.DrawString(this, str, position, Vector2.One, tintColor, angle, origin, flipEffect, 0);
    }

    /// <summary>
    /// Adds a string to the batch for rendering using the specified parameters. The text is rendered as a series of sprites, one for each character. Depending on the sort mode
    /// this may be an immediate draw operation or one queued for later processing.
    /// </summary>
    /// <param name="font">Font that represents the character data that is used to draw the text.</param>
    /// <param name="text">Text to draw.</param>
    /// <param name="position">Position to draw the sprite, in screen coordinates.</param>
    /// <param name="scale">Scaling factor.</param>
    /// <param name="tintColor">Color to tint the sprite. Use <see cref="Color.White"/> for no tinting.</param>
    /// <param name="angle">Rotation angle to rotate the sprite about its origin.</param>
    /// <param name="origin">Sprite origin, (0,0) is the default which represents the upper-left corner of the sprite.</param>
    /// <param name="flipEffect">Flip mirror effects to apply to the sprite.</param>
    /// <param name="depth">Depth of the sprite for use with sorting. Lower values indicate sprites that are in the front, higher indicate those in back.</param>
    public void DrawString(SpriteFont font, String text, Vector2 position, float scale, Color tintColor, Angle angle, Vector2 origin, SpriteFlipEffect flipEffect, float depth)
    {
      StringReference str = new StringReference(text);
      font.DrawString(this, str, position, new Vector2(scale, scale), tintColor, angle, origin, flipEffect, depth);
    }

    /// <summary>
    /// Adds a string to the batch for rendering using the specified parameters. The text is rendered as a series of sprites, one for each character. Depending on the sort mode
    /// this may be an immediate draw operation or one queued for later processing.
    /// </summary>
    /// <param name="font">Font that represents the character data that is used to draw the text.</param>
    /// <param name="text">Text to draw.</param>
    /// <param name="position">Position to draw the sprite, in screen coordinates.</param>
    /// <param name="scale">Scaling factor.</param>
    /// <param name="tintColor">Color to tint the sprite. Use <see cref="Color.White"/> for no tinting.</param>
    /// <param name="angle">Rotation angle to rotate the sprite about its origin.</param>
    /// <param name="origin">Sprite origin, (0,0) is the default which represents the upper-left corner of the sprite.</param>
    /// <param name="flipEffect">Flip mirror effects to apply to the sprite.</param>
    /// <param name="depth">Depth of the sprite for use with sorting. Lower values indicate sprites that are in the front, higher indicate those in back.</param>
    public void DrawString(SpriteFont font, String text, Vector2 position, Vector2 scale, Color tintColor, Angle angle, Vector2 origin, SpriteFlipEffect flipEffect, float depth)
    {
      StringReference str = new StringReference(text);
      font.DrawString(this, str, position, scale, tintColor, angle, origin, flipEffect, depth);
    }

    /// <summary>
    /// Adds a string to the batch for rendering using the specified parameters. The text is rendered as a series of sprites, one for each character. Depending on the sort mode
    /// this may be an immediate draw operation or one queued for later processing.
    /// </summary>
    /// <param name="font">Font that represents the character data that is used to draw the text.</param>
    /// <param name="text">Text to draw.</param>
    /// <param name="position">Position to draw the sprite, in screen coordinates.</param>
    /// <param name="tintColor">Color to tint the sprite. Use <see cref="Color.White"/> for no tinting.</param>
    public void DrawString(SpriteFont font, StringBuilder text, Vector2 position, Color tintColor)
    {
      StringReference str = new StringReference(text);
      font.DrawString(this, str, position, Vector2.One, tintColor, Angle.Zero, Vector2.Zero, SpriteFlipEffect.None, 0);
    }

    /// <summary>
    /// Adds a string to the batch for rendering using the specified parameters. The text is rendered as a series of sprites, one for each character. Depending on the sort mode
    /// this may be an immediate draw operation or one queued for later processing.
    /// </summary>
    /// <param name="font">Font that represents the character data that is used to draw the text.</param>
    /// <param name="text">Text to draw.</param>
    /// <param name="position">Position to draw the sprite, in screen coordinates.</param>
    /// <param name="tintColor">Color to tint the sprite. Use <see cref="Color.White"/> for no tinting.</param>
    /// <param name="angle">Rotation angle to rotate the sprite about its origin.</param>
    public void DrawString(SpriteFont font, StringBuilder text, Vector2 position, Color tintColor, Angle angle)
    {
      StringReference str = new StringReference(text);
      font.DrawString(this, str, position, Vector2.One, tintColor, angle, Vector2.Zero, SpriteFlipEffect.None, 0);
    }

    /// <summary>
    /// Adds a string to the batch for rendering using the specified parameters. The text is rendered as a series of sprites, one for each character. Depending on the sort mode
    /// this may be an immediate draw operation or one queued for later processing.
    /// </summary>
    /// <param name="font">Font that represents the character data that is used to draw the text.</param>
    /// <param name="text">Text to draw.</param>
    /// <param name="position">Position to draw the sprite, in screen coordinates.</param>
    /// <param name="tintColor">Color to tint the sprite. Use <see cref="Color.White"/> for no tinting.</param>
    /// <param name="angle">Rotation angle to rotate the sprite about its origin.</param>
    /// <param name="origin">Sprite origin, (0,0) is the default which represents the upper-left corner of the sprite.</param>
    public void DrawString(SpriteFont font, StringBuilder text, Vector2 position, Color tintColor, Angle angle, Vector2 origin)
    {
      StringReference str = new StringReference(text);
      font.DrawString(this, str, position, Vector2.One, tintColor, angle, origin, SpriteFlipEffect.None, 0);
    }

    /// <summary>
    /// Adds a string to the batch for rendering using the specified parameters. The text is rendered as a series of sprites, one for each character. Depending on the sort mode
    /// this may be an immediate draw operation or one queued for later processing.
    /// </summary>
    /// <param name="font">Font that represents the character data that is used to draw the text.</param>
    /// <param name="text">Text to draw.</param>
    /// <param name="position">Position to draw the sprite, in screen coordinates.</param>
    /// <param name="tintColor">Color to tint the sprite. Use <see cref="Color.White"/> for no tinting.</param>
    /// <param name="angle">Rotation angle to rotate the sprite about its origin.</param>
    /// <param name="origin">Sprite origin, (0,0) is the default which represents the upper-left corner of the sprite.</param>
    /// <param name="flipEffect">Flip mirror effects to apply to the sprite.</param>
    public void DrawString(SpriteFont font, StringBuilder text, Vector2 position, Color tintColor, Angle angle, Vector2 origin, SpriteFlipEffect flipEffect)
    {
      StringReference str = new StringReference(text);
      font.DrawString(this, str, position, Vector2.One, tintColor, angle, origin, flipEffect, 0);
    }

    /// <summary>
    /// Adds a string to the batch for rendering using the specified parameters. The text is rendered as a series of sprites, one for each character. Depending on the sort mode
    /// this may be an immediate draw operation or one queued for later processing.
    /// </summary>
    /// <param name="font">Font that represents the character data that is used to draw the text.</param>
    /// <param name="text">Text to draw.</param>
    /// <param name="position">Position to draw the sprite, in screen coordinates.</param>
    /// <param name="scale">Scaling factor.</param>
    /// <param name="tintColor">Color to tint the sprite. Use <see cref="Color.White"/> for no tinting.</param>
    /// <param name="angle">Rotation angle to rotate the sprite about its origin.</param>
    /// <param name="origin">Sprite origin, (0,0) is the default which represents the upper-left corner of the sprite.</param>
    /// <param name="flipEffect">Flip mirror effects to apply to the sprite.</param>
    /// <param name="depth">Depth of the sprite for use with sorting. Lower values indicate sprites that are in the front, higher indicate those in back.</param>
    public void DrawString(SpriteFont font, StringBuilder text, Vector2 position, float scale, Color tintColor, Angle angle, Vector2 origin, SpriteFlipEffect flipEffect, float depth)
    {
      StringReference str = new StringReference(text);
      font.DrawString(this, str, position, new Vector2(scale, scale), tintColor, angle, origin, flipEffect, depth);
    }

    /// <summary>
    /// Adds a string to the batch for rendering using the specified parameters. The text is rendered as a series of sprites, one for each character. Depending on the sort mode
    /// this may be an immediate draw operation or one queued for later processing.
    /// </summary>
    /// <param name="font">Font that represents the character data that is used to draw the text.</param>
    /// <param name="text">Text to draw.</param>
    /// <param name="position">Position to draw the sprite, in screen coordinates.</param>
    /// <param name="scale">Scaling factor.</param>
    /// <param name="tintColor">Color to tint the sprite. Use <see cref="Color.White"/> for no tinting.</param>
    /// <param name="angle">Rotation angle to rotate the sprite about its origin.</param>
    /// <param name="origin">Sprite origin, (0,0) is the default which represents the upper-left corner of the sprite.</param>
    /// <param name="flipEffect">Flip mirror effects to apply to the sprite.</param>
    /// <param name="depth">Depth of the sprite for use with sorting. Lower values indicate sprites that are in the front, higher indicate those in back.</param>
    public void DrawString(SpriteFont font, StringBuilder text, Vector2 position, Vector2 scale, Color tintColor, Angle angle, Vector2 origin, SpriteFlipEffect flipEffect, float depth)
    {
      StringReference str = new StringReference(text);
      font.DrawString(this, str, position, scale, tintColor, angle, origin, flipEffect, depth);
    }

    /// <summary>
    /// Draws a selection marquee without a border.
    /// </summary>
    /// <param name="rect">Rectangle (in screen coordinates) that represents the extent of the marquee.</param>
    /// <param name="color">Color of the selection marquee.</param>
    public void DrawMarquee(Rectangle rect, Color color)
    {
      DrawMarquee(rect, color, 0, Color.White);
    }

    /// <summary>
    /// Draws a selection marquee that can optionally have a border.
    /// </summary>
    /// <param name="rect">Rectangle (in screen coordinates) that represents the extent of the marquee.</param>
    /// <param name="color">Color of the selection marquee.</param>
    /// <param name="outlineWidth">Width of the outline border (in pixels), if zero then no border is drawn.</param>
    /// <param name="outlineColor">Color of the outline border.</param>
    /// <param name="depth">Optional depth of the marquee for use with sorting. Default is zero.</param>
    public void DrawMarquee(Rectangle rect, Color color, int outlineWidth, Color outlineColor, float depth = 0)
    {
      Vector4 dstRect = new Vector4(rect.X, rect.Y, rect.Width, rect.Height);
      DrawInternal(null, dstRect, null, false, color, Angle.Zero, Vector2.Zero, SpriteFlipEffect.None, depth);

      if (outlineWidth > 0)
      {
        //Draw top horiz
        Int2 topLeft = new Int2(rect.X, rect.Y);
        Int2 topRight = new Int2(rect.X + rect.Width, rect.Y);
        Int2 bottomLeft = new Int2(rect.X, rect.Y + rect.Height);
        Int2 bottomRight = new Int2(rect.X + rect.Width, rect.Y + rect.Height);

        dstRect = new Vector4(topLeft.X, topLeft.Y, rect.Width, outlineWidth);
        DrawInternal(null, dstRect, null, false, outlineColor, Angle.Zero, Vector2.Zero, SpriteFlipEffect.None, depth + 1);

        //Draw bottom horiz
        dstRect = new Vector4(topLeft.X, topLeft.Y + rect.Height - outlineWidth, rect.Width, outlineWidth);
        DrawInternal(null, dstRect, null, false, outlineColor, Angle.Zero, Vector2.Zero, SpriteFlipEffect.None, depth + 1);

        //Draw left vert
        dstRect = new Vector4(topLeft.X, topLeft.Y, outlineWidth, rect.Height);
        DrawInternal(null, dstRect, null, false, outlineColor, Angle.Zero, Vector2.Zero, SpriteFlipEffect.None, depth + 1);

        //Draw right vert
        dstRect = new Vector4(topLeft.X + rect.Width - outlineWidth, topLeft.Y, outlineWidth, rect.Height);
        DrawInternal(null, dstRect, null, false, outlineColor, Angle.Zero, Vector2.Zero, SpriteFlipEffect.None, depth + 1);
      }
    }

    internal void DrawInternal(Texture2D texture, Vector4 dstRect, Rectangle? srcRect, bool scaleDstFromSrc, Color color, Angle angle, Vector2 origin, SpriteFlipEffect flipEffect, float depth)
    {
      CheckDisposed();

      if (!m_inBeginEnd)
        throw new InvalidOperationException(StringLocalizer.Instance.GetLocalizedString("DrawCalledBeforeBegin"));

      if (texture == null)
        texture = Texture.Default2D;

      if (m_spriteQueueCount >= m_spriteQueue.Length)
        m_spriteQueue.Resize(m_spriteQueue.Length * 2);
      
      Sprite sprite;
      sprite.Texture = texture;
      sprite.Color = color;
      sprite.Angle = angle;
      sprite.Origin = origin;
      sprite.OrthoOrder = depth;
      sprite.FlipEffect = flipEffect;

      //Compute our cropping and screen offsets. Scale the screen offsets if user hasn't supplied us with their own dest rectangle
      float dWidth = dstRect.Z;
      float dHeight = dstRect.W;

      //Using only a subset of the texture
      if (srcRect.HasValue)
      {
        Rectangle rect = srcRect.Value;
        if (rect.Height == 0 || rect.Width == 0)
          throw new ArgumentOutOfRangeException(StringLocalizer.Instance.GetLocalizedString("NullSpriteRectangle"));

        Vector4 uvOffsets;
        uvOffsets.X = rect.X;
        uvOffsets.Y = rect.Y;
        uvOffsets.Z = rect.Width;
        uvOffsets.W = rect.Height;
        sprite.TextureOffsets = uvOffsets;

        //If scaling dest rect based on src rect width/height
        Vector4 scOffsets;
        scOffsets.X = dstRect.X;
        scOffsets.Y = dstRect.Y;
        if (scaleDstFromSrc)
        {
          scOffsets.Z = dstRect.Z * rect.Width;
          scOffsets.W = dstRect.W * rect.Height;
        }
        else
        {
          scOffsets.Z = dstRect.Z;
          scOffsets.W = dstRect.W;
        }
        sprite.ScreenOffsets = scOffsets;
      }
      //Otherwise using the whole texture
      else
      {
        Vector4 uvOffsets;
        uvOffsets.X = 0f;
        uvOffsets.Y = 0f;
        uvOffsets.Z = texture.Width;
        uvOffsets.W = texture.Height;
        sprite.TextureOffsets = uvOffsets;

        //If scaling dest rect based on texture width/height
        Vector4 scOffsets;
        scOffsets.X = dstRect.X;
        scOffsets.Y = dstRect.Y;
        if (scaleDstFromSrc)
        {
          scOffsets.Z = dstRect.Z * texture.Width;
          scOffsets.W = dstRect.W * texture.Height;
        }
        else
        {
          scOffsets.Z = dstRect.Z;
          scOffsets.W = dstRect.W;
        }
        sprite.ScreenOffsets = scOffsets;
      }

      //Add sprite to the queue
      m_spriteQueue[m_spriteQueueCount] = sprite;
      if (m_sortMode == SpriteSortMode.Immediate)
        RenderBatch(texture, m_spriteQueue, 0, 1);
      else
        m_spriteQueueCount++;
    }

    /// <summary>
    /// Flushes the sprite batch and performs any remaining draw operations, then sets
    /// the batcher to it's default state. This does not restore the state on the render context.
    /// </summary>
    public void End()
    {
      CheckDisposed();

      if (!m_inBeginEnd)
        throw new InvalidOperationException(StringLocalizer.Instance.GetLocalizedString("EndCalledBeforeBegin"));

      if (m_sortMode != SpriteSortMode.Immediate && m_spriteQueueCount > 0)
        ProcessRenderQueue();

      //Make sure we don't hold onto references...
      if (m_customShaderGroup == null)
        m_samplerParam.SetResource<SamplerState>((SamplerState) null);

      m_customShaderGroup = null;
      m_renderContext = null;
      m_pixelStage = null;
      m_applyCameraViewProjection = false;
      m_inBeginEnd = false;
    }

    /// <summary>
    /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
    /// </summary>
    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    /// <summary>
    /// Releases unmanaged and - optionally - managed resources.
    /// </summary>
    /// <param name="isDisposing"><c>True</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
    protected void Dispose(bool isDisposing)
    {
      if (!m_isDisposed)
      {
        if (isDisposing)
        {
          if (m_vertexBuffer != null)
          {
            m_vertexBuffer.Dispose();
            m_vertexBuffer = null;
          }

          if (m_indexBuffer != null)
          {
            m_indexBuffer.Dispose();
            m_indexBuffer = null;
          }

          if (m_spriteEffect != null)
          {
            m_spriteEffect.Dispose();
            m_spriteEffect = null;
            m_matrixParam = null;
            m_samplerParam = null;
            m_customShaderGroup = null;
          }

          if (m_vertices != null)
          {
            m_vertices.Dispose();
            m_vertices = null;
          }

          m_spriteQueue.Dispose();
          m_sortedSpriteIndices = null;
          m_sortedSprites.Dispose();
        }

        m_isDisposed = true;
      }
    }

    private void RenderBatch(Texture2D texture, ReadOnlySpan<Sprite> sprites, int index, int numBatches)
    {
      //Setup pass/texture/state setting
      if (m_customShaderGroup != null)
      {
        //Apply the custom effect shader group, then AFTER set the sprite resources (ensures they won't get overwrriten)
        m_customShaderGroup.Apply(m_renderContext);
        m_pixelStage.SetShaderResource(0, texture);
        m_pixelStage.SetSampler(0, m_ss);

        OnRenderBatch(texture, sprites, index, numBatches);
      }
      else
      {
        m_pixelStage.SetShaderResource(0, texture);
        //Don't need to set samplerstate again, because we did that at the beginning

        OnRenderBatch(texture, sprites, index, numBatches);
      }
    }

    private void OnRenderBatch(Texture2D texture, ReadOnlySpan<Sprite> sprites, int index, int numBatches)
    {
      float scaleWidth = 1f / (float) texture.Width;
      float scaleHeight = 1f / (float) texture.Height;

      //Setup the vertices for all the batches. The vertexbuffer acts like a wrap around queue
      while (numBatches > 0)
      {
        DataWriteOptions writeOptions = DataWriteOptions.NoOverwrite;
        int actCount = numBatches;
        if (actCount > (m_batchSize - m_spriteVbPos))
        {
          //Need to split up, so set actual count to the remaining space
          actCount = m_batchSize - m_spriteVbPos;

          //Need to check if actually we've reached the end of the VB,
          //if so we need to wrap around and set discard
          if (m_spriteVbPos % m_batchSize == 0)
          {
            writeOptions = DataWriteOptions.Discard;
            m_spriteVbPos = 0;

            //Reset actual count to the maximum space available, either
            //requested batch number, or max batch size - whichever is smallest
            actCount = Math.Min(m_batchSize, numBatches);
          }
        }

        //Loop through sprite array, create the geometry
        for (int i = index, j = 0; i < index + actCount; i++)
        {
          Sprite sprite = sprites[i];
          float cos = 1.0f;
          float sin = 0.0f;

          Angle angle = sprite.Angle;
          if (angle != Angle.Zero)
          {
            cos = angle.Cos;
            sin = angle.Sin;
          }

          Vector4 scOffsets = sprite.ScreenOffsets;
          float sx = scOffsets.X;
          float sy = scOffsets.Y;
          float sw = scOffsets.Z;
          float sh = scOffsets.W;

          Vector4 texOffsets = sprite.TextureOffsets;
          float tu = texOffsets.X * scaleWidth;
          float tv = texOffsets.Y * scaleHeight;
          float tw = texOffsets.Z * scaleWidth;
          float th = texOffsets.W * scaleHeight;

          Vector2 origin = sprite.Origin;
          float oriX = origin.X / texOffsets.Z;
          float oriY = origin.Y / texOffsets.W;

          bool flipH = (sprite.FlipEffect & SpriteFlipEffect.FlipHorizontally) == SpriteFlipEffect.FlipHorizontally;
          bool flipV = (sprite.FlipEffect & SpriteFlipEffect.FlipVertically) == SpriteFlipEffect.FlipVertically;

          float z = sprite.OrthoOrder;

          //Apply change of origin to screen width/height for rotations
          float sw1 = -oriX * sw;
          float sw2 = (1 - oriX) * sw;
          float sh1 = -oriY * sh;
          float sh2 = (1 - oriY) * sh;

          //top-left
          Vector3 v = new Vector3((sx + (sw1 * cos)) - (sh1 * sin), (sy + (sw1 * sin)) + (sh1 * cos), z);
          Vector2 uv = new Vector2(tu, tv);

          if (flipH)
            uv.X = 1.0f - uv.X;

          if (flipV)
            uv.Y = 1.0f - uv.Y;

          m_vertices[j++] = new VertexPositionColorTexture(v, sprite.Color, uv);

          //lower-left
          v = new Vector3((sx + (sw1 * cos)) - (sh2 * sin), (sy + (sw1 * sin)) + (sh2 * cos), z);
          uv = new Vector2(tu, tv + th);

          if (flipH)
            uv.X = 1.0f - uv.X;

          if (flipV)
            uv.Y = 1.0f - uv.Y;

          m_vertices[j++] = new VertexPositionColorTexture(v, sprite.Color, uv);

          //lower-right
          v = new Vector3((sx + (sw2 * cos)) - (sh2 * sin), (sy + (sw2 * sin)) + (sh2 * cos), z);
          uv = new Vector2(tu + tw, tv + th);

          if (flipH)
            uv.X = 1.0f - uv.X;

          if (flipV)
            uv.Y = 1.0f - uv.Y;

          m_vertices[j++] = new VertexPositionColorTexture(v, sprite.Color, uv);

          //upper-right
          v = new Vector3((sx + (sw2 * cos)) - (sh1 * sin), (sy + (sw2 * sin)) + (sh1 * cos), z);
          uv = new Vector2(tu + tw, tv);

          if (flipH)
            uv.X = 1.0f - uv.X;

          if (flipV)
            uv.Y = 1.0f - uv.Y;

          m_vertices[j++] = new VertexPositionColorTexture(v, sprite.Color, uv);
        }

        //Write to VB
        int stride = VertexPositionColorTexture.SizeInBytes;
        int offset = m_spriteVbPos * stride * 4;
        m_vertexBuffer.SetData<VertexPositionColorTexture>(m_renderContext, m_vertices.Span.Slice(0, actCount * 4), offset, stride, writeOptions);

        //Render
        m_renderContext.DrawIndexed(PrimitiveType.TriangleList, actCount * 6, m_spriteVbPos * 6, 0);

        index += actCount;
        m_spriteVbPos += actCount;
        numBatches -= actCount;
      }
    }

    private void SortRenderQueue()
    {
      //Queue indices for sprite sorting, so we don't have to keep copying the Sprite struct
      //all over the place.
      if (m_sortedSpriteIndices == null || m_sortedSpriteIndices.Length < m_spriteQueueCount)
      {
        m_sortedSpriteIndices = new int[m_spriteQueueCount];
        m_sortedSprites = new PooledArray<Sprite>(m_spriteQueueCount);
      }

      IComparer<int> comparer = m_textureComparer;
      if (m_sortMode == SpriteSortMode.BackToFront)
        comparer = m_backToFrontComparer;
      else if (m_sortMode == SpriteSortMode.FrontToBack)
        comparer = m_frontToBackComparer;

      //Set the indices
      for (int i = 0; i < m_spriteQueueCount; i++)
        m_sortedSpriteIndices[i] = i;

      //Sort
      Array.Sort<int>(m_sortedSpriteIndices, 0, m_spriteQueueCount, comparer);
    }

    private void ProcessRenderQueue()
    {
      PooledArray<Sprite> sprites;
      if (m_sortMode == SpriteSortMode.Deferred)
      {
        sprites = m_spriteQueue;
      }
      else
      {
        SortRenderQueue();
        sprites = m_sortedSprites; //Set this after sort, since we may be resizing the sorted array
      }

      int index = 0;
      Texture2D prevTex = null;

      //Loop through the queue and accumulate sprites with the same texture,
      //when we hit a new texture, we render the accumulated sprites.
      for (int i = 0; i < m_spriteQueueCount; i++)
      {
        Texture2D currTex;
        if (m_sortMode == SpriteSortMode.Deferred)
        {
          currTex = sprites[i].Texture;
        }
        else
        {
          //Using sorted array - grab the sprite from the queue and place it in the sorted array
          Sprite sprite = m_spriteQueue[m_sortedSpriteIndices[i]];
          currTex = sprite.Texture;
          sprites[i] = sprite;
        }

        //If texture is not the same, we render any accumulated sprites that have
        //not been rendered since they all had the same texture
        if (prevTex == null || prevTex.ResourceID != currTex.ResourceID)
        {
          //Render accumulated sprites with the previous texture
          if (i > index)
            RenderBatch(prevTex, sprites, index, i - index);

          index = i;
          prevTex = currTex;
        }
      }

      //Perform final rendering for any sprites not yet rendered
      RenderBatch(prevTex, sprites, index, m_spriteQueueCount - index);

      //Clear sprite queue
      m_spriteQueue.Clear(0, m_spriteQueueCount);

      //And sorted sprite queue if that was used
      if (m_sortMode != SpriteSortMode.Deferred)
        m_sortedSprites.Clear(0, m_spriteQueueCount);

      m_spriteQueueCount = 0;
    }

    private void CheckDisposed()
    {
      ObjectDisposedException.ThrowIf(m_isDisposed, this);
    }

    private void SetBuffers()
    {
      m_renderContext.SetVertexBuffer(m_vertexBuffer);
      m_renderContext.SetIndexBuffer(m_indexBuffer);
    }

    private void ApplyEffect()
    {
      Matrix matrix = Matrix.Identity;

      if (m_applyCameraViewProjection && m_renderContext.Camera != null)
      {
        matrix = m_renderContext.Camera.ViewProjectionMatrix;
      }
      else
      {
        Viewport viewport = (m_renderContext.Camera == null) ? new Viewport(0, 0, 800, 600) : m_renderContext.Camera.Viewport;

        ComputeSpriteProjection(ref viewport, out matrix);
      }

      Matrix spriteTransform;
      Matrix.Multiply(m_worldMatrix, matrix, out spriteTransform);
      m_matrixParam.SetValue(spriteTransform);
      m_samplerParam.SetResource<SamplerState>(m_ss);

      m_pass.Apply(m_renderContext);
    }

    private DataBuffer<ushort> CreateIndexData(int maxBatchSize)
    {
      DataBuffer<ushort> indices = DataBuffer.Create<ushort>(maxBatchSize * 6);

      for (ushort i = 0; i < maxBatchSize; i++)
      {
        ushort startIndex = (ushort) (i * 6);
        ushort index = (ushort) (i * 4);
        indices[startIndex] = index;
        indices[startIndex + 1] = (ushort) (index + 2);
        indices[startIndex + 2] = (ushort) (index + 1);
        indices[startIndex + 3] = index;
        indices[startIndex + 4] = (ushort) (index + 3);
        indices[startIndex + 5] = (ushort) (index + 2);
      }

      return indices;
    }

    [StructLayout(LayoutKind.Sequential)]
    private struct Sprite
    {
      public Texture2D Texture;
      public Vector4 TextureOffsets;
      public Vector4 ScreenOffsets;
      public Vector2 Origin;
      public Angle Angle;
      public float OrthoOrder;
      public SpriteFlipEffect FlipEffect;
      public Color Color;
    }

    private class TextureComparer : IComparer<int>
    {
      private SpriteBatch m_batch;

      public TextureComparer(SpriteBatch batch)
      {
        m_batch = batch;
      }

      public int Compare(int x, int y)
      {
        Texture2D xTex = m_batch.m_spriteQueue[x].Texture;
        Texture2D yTex = m_batch.m_spriteQueue[y].Texture;

        return (int) (xTex.ResourceID - yTex.ResourceID);
      }
    }

    private class OrthoComparer : IComparer<int>
    {
      private SpriteBatch m_batch;
      private bool m_frontToBack;

      public OrthoComparer(SpriteBatch batch, bool frontToBack)
      {
        m_batch = batch;
        m_frontToBack = frontToBack;
      }

      public int Compare(int x, int y)
      {
        float xDepth = m_batch.m_spriteQueue[x].OrthoOrder;
        float yDepth = m_batch.m_spriteQueue[y].OrthoOrder;

        if (m_frontToBack)
        {
          if (xDepth > yDepth)
            return -1;
          else if (xDepth < yDepth)
            return 1;
        }
        else
        {
          if (xDepth < yDepth)
            return -1;
          else if (xDepth > yDepth)
            return 1;
        }

        return 0;
      }
    }
  }
}
