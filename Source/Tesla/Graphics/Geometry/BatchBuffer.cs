﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.CompilerServices;

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// Describes a complete batch submitted to a buffer. This resembles a non-indexed draw call.
  /// </summary>
  /// <typeparam name="BufferT">Buffer type.</typeparam>
  public struct Batch<BufferT> where BufferT : class, IBufferResource
  {
    /// <summary>
    /// Buffer that contains the batch.
    /// </summary>
    public BufferT Buffer;

    /// <summary>
    /// Starting index of the batch.
    /// </summary>
    public int StartIndex;

    /// <summary>
    /// Number of elements in the batch.
    /// </summary>
    public int ElementCount;

    /// <summary>
    /// Gets if the batch is valid, that is the buffer exists and there exists one or more elements.
    /// </summary>
    public readonly bool IsValid { get { return Buffer is not null && ElementCount > 0; } }
  }

  /// <summary>
  /// Describes a complete indexed batch submitted to a pair of vertex/index buffers. This resembles an indexed draw call.
  /// </summary>
  /// <typeparam name="IndexBufferT">Index buffer type.</typeparam>
  /// <typeparam name="VertexBufferT">Vertex buffer type.</typeparam>
  public struct IndexedBatch<IndexBufferT, VertexBufferT> where IndexBufferT : class, IBufferResource where VertexBufferT : class, IBufferResource
  {
    /// <summary>
    /// Buffer that contains the "vertex" data.
    /// </summary>
    public VertexBufferT VertexBuffer;

    /// <summary>
    /// Buffer that contains the index data.
    /// </summary>
    public IndexBufferT IndexBuffer;

    /// <summary>
    /// Starting index of the batch inside the index buffer.
    /// </summary>
    public int StartIndex;

    /// <summary>
    /// Number of indices of the batch inside the index buffer.
    /// </summary>
    public int IndexCount;

    /// <summary>
    /// The value to add to each index before reading from the index buffer (e.g. the starting index of the batch's vertex data in that buffer, useful if the indices start at zero).
    /// </summary>
    public int BaseVertexOffset;

    /// <summary>
    /// Gets if the batch is valid, that is the buffers exist and there exists one or more elements.
    /// </summary>
    public readonly bool IsValid { get { return VertexBuffer is not null && IndexBuffer is not null && IndexCount > 0; } }
  }

  /// <summary>
  /// A rotating dynamic buffer that is meant to merge small bits of data into larger batches to reduce the number of draw calls needed
  /// to draw that data. Each batch that is submitted to the buffer is written using <see cref="DataWriteOptions.NoOverwrite"/> and when the
  /// buffer rotates back to the beginning, the first batch is written using <see cref="DataWriteOptions.Discard"/>.
  /// </summary>
  /// <typeparam name="T">Type of data that is contained in the buffer.</typeparam>
  /// <typeparam name="BufferT">Type of GPU buffer that data is submitted to.</typeparam>
  public abstract class BatchBuffer<T, BufferT> : INamable, IDebugNamable, IDisposable where T : unmanaged where BufferT : class, IBufferResource
  {
    private IRenderSystem m_renderSystem;
    private BufferT m_buffer;
    private bool m_isDisposed;

    private bool m_isBatchStarted;
    private int m_remainingCapacity;
    private int m_batchStartIndex;
    private int m_batchElementCount;
    private DataBuffer<T> m_pendingBatchData;

    private string? m_name;
    private string? m_debugName;

    /// <summary>
    /// Gets whether this has been disposed or not.
    /// </summary>
    public bool IsDisposed { get { return m_isDisposed; } }

    /// <summary>
    /// Gets the render system used to create buffers.
    /// </summary>
    public IRenderSystem RenderSystem { get { return m_renderSystem; } }

    /// <summary>
    /// Gets whether a batch is currently started. Call <see cref="Finish(IRenderContext, out Batch{BufferT})"/> to end a pending batch.
    /// </summary>
    public bool IsBatchStarted { get { return m_isBatchStarted; } }

    /// <summary>
    /// Gets the capacity of the buffer. This can grow to accomodate large data that is appended, but if small bits are appended the buffer
    /// attempts to wrap around instead of growing.
    /// </summary>
    public int Capacity { get { return m_buffer.ElementCount; } }

    /// <summary>
    /// Gets the remaining capacity until the buffer wraps around.
    /// </summary>
    public int RemainingCapacity { get { return m_remainingCapacity; } }

    /// <inheritdoc />
    public string Name
    {
      get
      {
        return m_buffer.Name;
      }
      set
      {
        m_name = value;
        m_buffer.Name = value;
      }
    }

    /// <inheritdoc />
    public string DebugName
    {
      get
      {
        return m_buffer.DebugName;
      }
      set
      {
        m_debugName = value;
        m_buffer.DebugName = value;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="BatchBuffer{T, BufferT}"/> class.
    /// </summary>
    /// <param name="renderSystem">Render system used to construct the buffer.</param>
    /// <param name="initialElementCapacity">Initial number of elements in the buffer.</param>
    /// <param name="pendingBufferAllocator">Optional allocator for the CPU buffer that batch data accumulates in before being submitted to the GPU.</param>
    protected BatchBuffer(IRenderSystem renderSystem, int initialElementCapacity, MemoryAllocatorStrategy pendingBufferAllocator = MemoryAllocatorStrategy.Default)
    {
      initialElementCapacity = Math.Max(1, initialElementCapacity);

      m_isDisposed = false;
      m_isBatchStarted = false;
      m_renderSystem = renderSystem;
      DoCreateBuffer(initialElementCapacity);

      m_remainingCapacity = initialElementCapacity;
      m_batchStartIndex = 0;
      m_batchElementCount = 0;
      m_pendingBatchData = DataBuffer.Create<T>(initialElementCapacity, pendingBufferAllocator);
    }

    /// <summary>
    /// Appends data to the buffer. This may or may not complete a pending batch.
    /// </summary>
    /// <param name="context">Render context used to submit the batch.</param>
    /// <param name="data">Data to submit to be batched.</param>
    /// <param name="batch">A completed batch, if one was finished. Not valid if the method does not return true.</param>
    /// <returns>True if there is a valid finished batch to be handled, false if not.</returns>
    public bool Append(IRenderContext context, ReadOnlySpan<T> data, out Batch<BufferT> batch)
    {
      return Append(context, data, false, out batch);
    }

    /// <summary>
    /// Appends data to the buffer. This may or may not complete a pending batch.
    /// </summary>
    /// <param name="context">Render context used to submit the batch.</param>
    /// <param name="data">Data to submit to be batched.</param>
    /// <param name="forceFinish">True if the append should complete a batch, resizing the GPU buffer if necessary (in case of a pending batch not having enough space).</param>
    /// <param name="batch">A completed batch, if one was finished. Not valid if the method does not return true.</param>
    /// <returns>True if there is a valid finished batch to be handled, false if not.</returns>
    public bool Append(IRenderContext context, ReadOnlySpan<T> data, bool forceFinish, out Batch<BufferT> batch)
    {
      batch = default;

      if (data.IsEmpty)
        return false;

      // If can't be merged with the current batch due to a space limitation, finish the previous batch
      if (!IsRoomInBatch(data.Length))
      {
        // If force finish, then we forcibly merge by resizing the buffer. Usually implies a state change so it will be unexpected
        // if the incoming data is left dangling
        if (forceFinish)
        {
          int pendingStartIndex = m_batchElementCount;
          m_remainingCapacity -= data.Length;
          m_batchElementCount += data.Length;
          m_batchStartIndex = 0; // Will discard the old buffer, so start at zero again

          // Resize pending if necessary
          if (m_pendingBatchData.Length < m_batchElementCount)
            m_pendingBatchData.Resize(CalculateNewCapacity(m_batchElementCount));

          // Copy to the rest of pending and submit the entire batch
          data.CopyTo(m_pendingBatchData.Span.Slice(pendingStartIndex, data.Length));

          return TrySubmitBatch(context, out batch);
        }
        else
        {
          TrySubmitBatch(context, out batch);

          m_isBatchStarted = true;
          m_batchElementCount = data.Length;
          m_remainingCapacity = m_buffer.ElementCount - data.Length;
          if (m_remainingCapacity <= 0)
          {
            m_batchStartIndex = 0;
            m_remainingCapacity = 0;
          }

          // Resize pending if necessary, on the next append or finish we will need to resize the buffer
          if (m_pendingBatchData.Length < m_batchElementCount)
            m_pendingBatchData.Resize(CalculateNewCapacity(m_batchElementCount));

          // Copy to pending, wait for next append/finish
          data.CopyTo(m_pendingBatchData.Span.Slice(0, m_batchElementCount));

          return true;
        }
      }

      // Otherwise we either are starting a new batch or have enough space to append to an existing batch
      m_isBatchStarted = true;

      int numPending = m_batchElementCount;
      m_batchElementCount += data.Length;
      m_remainingCapacity -= data.Length;

      bool wantToFinish = forceFinish;

      // If hit the end of the buffer, finish now...
      if (m_remainingCapacity <= 0)
      {
        m_batchStartIndex = 0;
        wantToFinish = true;
      }

      // If no data is pending and we want to finish, copy directly from incoming data
      if (numPending == 0 && wantToFinish)
        return TrySubmitBatch(context, data, out batch);

      // Otherwise copy to pending
      data.CopyTo(m_pendingBatchData.Span.Slice(numPending, data.Length));

      if (wantToFinish)
        return TrySubmitBatch(context, out batch);

      return false;
    }

    /// <summary>
    /// Appends data and completes a batch. There may or may not be a pending batch, and if there is,
    /// the buffer may be resized in order to accommodate the data.
    /// </summary>
    /// <param name="context">Render context used to submit the batch.</param>
    /// <param name="data">Data to submit to be batched.</param>
    /// <returns>Info about the batch inside the GPU buffer.</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if an empty span was submitted.</exception>
    public Batch<BufferT> AppendBatch(IRenderContext context, ReadOnlySpan<T> data)
    {
      if (this.Append(context, data, true, out Batch<BufferT> batch))
        return batch;

      throw new ArgumentOutOfRangeException(nameof(data));
    }

    /// <summary>
    /// Finishes a pending batch.
    /// </summary>
    /// <param name="context">Render context used to submit the batch.</param>
    /// <param name="batch">Batch that has been submitted to the GPU buffer.</param>
    /// <returns>True if the batch was completed and submitted, false if there was no pending batch.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public bool Finish(IRenderContext context, out Batch<BufferT> batch)
    {
      return TrySubmitBatch(context, out batch);
    }

    /// <summary>
    /// Resets the buffer and invalidates any current batch.
    /// </summary>
    public void Reset()
    {
      m_batchStartIndex = 0;
      m_batchElementCount = 0;
      m_isBatchStarted = false;
      m_remainingCapacity = m_buffer.ElementCount;
    }

    /// <inheritdoc />
    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    /// <summary>
    /// Creates the underlying GPU buffer. It must be created as a dynamic buffer.
    /// </summary>
    /// <param name="renderSystem">RenderSystem used to create the resource.</param>
    /// <param name="requestedNumElements">Minimum number of elements requested.</param>
    /// <returns>Created GPU buffer</returns>
    protected abstract BufferT CreateBuffer(IRenderSystem renderSystem, int requestedNumElements);

    /// <summary>
    /// Disposes of resources.
    /// </summary>
    /// <param name="disposing">True if <see cref="Dispose"/> was called, false if not.</param>
    protected virtual void Dispose(bool disposing)
    {
      if (disposing && !m_isDisposed)
        m_buffer.Dispose();

      m_isDisposed = true;
    }

    private bool TrySubmitBatch(IRenderContext context, out Batch<BufferT> batch)
    {
      batch = default;

      if (!m_isBatchStarted)
        return false;

      ReadOnlySpan<T> data = m_pendingBatchData.Span.Slice(0, m_batchElementCount);
      SubmitBatch(context, data, out batch);

      return true;
    }

    private bool IsRoomInBatch(int numIncoming)
    {
      if (!m_isBatchStarted)
        return true;

      return numIncoming <= m_remainingCapacity;
    }

    private bool TrySubmitBatch(IRenderContext context, ReadOnlySpan<T> data, out Batch<BufferT> batch)
    {
      batch = default;

      if (!m_isBatchStarted)
        return false;

      SubmitBatch(context, data, out batch);

      return true;
    }

    private int CalculateNewCapacity(int numRequested)
    {
      return (int) MathF.Ceiling(numRequested * 1.5f);
    }

    [MemberNotNull(nameof(m_buffer))]
    private void DoCreateBuffer(int capacity)
    {
      if (m_buffer is not null)
        m_buffer.Dispose();

      m_buffer = CreateBuffer(m_renderSystem, capacity);

      if (m_name is not null)
        m_buffer.Name = m_name;

      if (m_debugName is not null)
        m_buffer.DebugName = m_debugName;
    }

    private void SubmitBatch(IRenderContext context, ReadOnlySpan<T> data, out Batch<BufferT> batch)
    {
      // Check if the current batch exceeds the buffer size, if so then we need to expand it
      if (m_batchElementCount > m_buffer.ElementCount)
      {
        m_batchStartIndex = 0; // Since new buffer, okay to start at beginning

        int capacity = CalculateNewCapacity(m_batchElementCount);
        DoCreateBuffer(capacity);

        m_remainingCapacity = capacity - m_batchElementCount;
      }

      // Copy pending data to the GPU buffer
      int byteOffset = m_batchStartIndex * m_buffer.ElementStride;
      DataWriteOptions options = (byteOffset == 0) ? DataWriteOptions.Discard : DataWriteOptions.NoOverwrite;
      m_buffer.SetData<T>(context, data, byteOffset, options);

      batch = new Batch<BufferT>() { Buffer = m_buffer, StartIndex = m_batchStartIndex, ElementCount = m_batchElementCount };

      // Setup for the next batch...
      m_batchStartIndex = m_batchStartIndex + m_batchElementCount;
      m_batchElementCount = 0;
      m_isBatchStarted = false;

      // Already at the end of the buffer, reset to beginning...
      if (m_remainingCapacity <= 0)
      {
        m_batchStartIndex = 0;
        m_remainingCapacity = m_buffer.ElementCount;
      }

      // Ensure the pending buffer matches the buffer size in cases where we resized things...do so after the batch is submitted
      // so we don't mess with the incoming data span (may be from pending)
      if (m_buffer.ElementCount != m_pendingBatchData.Length)
        m_pendingBatchData.Resize(m_buffer.ElementCount);
    }
  }

  /// <summary>
  /// Represents a batch buffer specifically for index data, where the underlying data type is either 32 or 16 bit integers.
  /// Data that is submitted to the buffer can be either, with a conversion of the index format as necessary.
  /// </summary>
  /// <typeparam name="BufferT">Type of GPU buffer that data is submitted to.</typeparam>
  public interface IBatchIndexBuffer<BufferT> : IDisposable, IDebugNamable, INamable where BufferT : class, IBufferResource
  {
    /// <summary>
    /// Gets whether this has been disposed or not.
    /// </summary>
    bool IsDisposed { get; }

    /// <summary>
    /// Gets the render system used to create buffers.
    /// </summary>
    IRenderSystem RenderSystem { get; }

    /// <summary>
    /// Gets whether a batch is currently started. Call <see cref="Finish(IRenderContext, out Batch{BufferT})"/> to end a pending batch.
    /// </summary>
    bool IsBatchStarted { get; }

    /// <summary>
    /// Gets the capacity of the buffer. This can grow to accomodate large data that is appended, but if small bits are appended the buffer
    /// attempts to wrap around instead of growing.
    /// </summary>
    int Capacity { get; }

    /// <summary>
    /// Gets the remaining capacity until the buffer wraps around.
    /// </summary>
    int RemainingCapacity { get; }

    /// <summary>
    /// Format of the underlying index data.
    /// </summary>
    IndexFormat IndexFormat { get; }

    /// <summary>
    /// Appends data to the buffer. This may or may not complete a pending batch.
    /// </summary>
    /// <param name="context">Render context used to submit the batch.</param>
    /// <param name="data">Data to submit to be batched.</param>
    /// <param name="batch">A completed batch, if one was finished. Not valid if the method does not return true.</param>
    /// <returns>True if there is a valid finished batch to be handled, false if not.</returns>
    bool Append(IRenderContext context, ReadOnlySpan<int> data, out Batch<BufferT> batch);

    /// <summary>
    /// Appends data to the buffer. This may or may not complete a pending batch.
    /// </summary>
    /// <param name="context">Render context used to submit the batch.</param>
    /// <param name="data">Data to submit to be batched.</param>
    /// <param name="forceFinish">True if the append should complete a batch, resizing the GPU buffer if necessary (in case of a pending batch not having enough space).</param>
    /// <param name="batch">A completed batch, if one was finished. Not valid if the method does not return true.</param>
    /// <returns>True if there is a valid finished batch to be handled, false if not.</returns>
    bool Append(IRenderContext context, ReadOnlySpan<int> data, bool forceFinish, out Batch<BufferT> batch);

    /// <summary>
    /// Appends data to the buffer. This may or may not complete a pending batch.
    /// </summary>
    /// <param name="context">Render context used to submit the batch.</param>
    /// <param name="data">Data to submit to be batched.</param>
    /// <param name="batch">A completed batch, if one was finished. Not valid if the method does not return true.</param>
    /// <returns>True if there is a valid finished batch to be handled, false if not.</returns>
    bool Append(IRenderContext context, ReadOnlySpan<ushort> data, out Batch<BufferT> batch);

    /// <summary>
    /// Appends data to the buffer. This may or may not complete a pending batch.
    /// </summary>
    /// <param name="context">Render context used to submit the batch.</param>
    /// <param name="data">Data to submit to be batched.</param>
    /// <param name="forceFinish">True if the append should complete a batch, resizing the GPU buffer if necessary (in case of a pending batch not having enough space).</param>
    /// <param name="batch">A completed batch, if one was finished. Not valid if the method does not return true.</param>
    /// <returns>True if there is a valid finished batch to be handled, false if not.</returns>
    bool Append(IRenderContext context, ReadOnlySpan<ushort> data, bool forceFinish, out Batch<BufferT> batch);

    /// <summary>
    /// Appends data and completes a batch. There may or may not be a pending batch, and if there is,
    /// the buffer may be resized in order to accommodate the data.
    /// </summary>
    /// <param name="context">Render context used to submit the batch.</param>
    /// <param name="data">Data to submit to be batched.</param>
    /// <returns>Info about the batch inside the GPU buffer.</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if an empty span was submitted.</exception>
    Batch<BufferT> AppendBatch(IRenderContext context, ReadOnlySpan<int> data);

    /// <summary>
    /// Appends data and completes a batch. There may or may not be a pending batch, and if there is,
    /// the buffer may be resized in order to accommodate the data.
    /// </summary>
    /// <param name="context">Render context used to submit the batch.</param>
    /// <param name="data">Data to submit to be batched.</param>
    /// <returns>Info about the batch inside the GPU buffer.</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if an empty span was submitted.</exception>
    Batch<BufferT> AppendBatch(IRenderContext context, ReadOnlySpan<ushort> data);

    /// <summary>
    /// Finishes a pending batch.
    /// </summary>
    /// <param name="context">Render context used to submit the batch.</param>
    /// <param name="batch">Batch that has been submitted to the GPU buffer.</param>
    /// <returns>True if the batch was completed and submitted, false if there was no pending batch.</returns>
    bool Finish(IRenderContext context, out Batch<BufferT> batch);

    /// <summary>
    /// Resets the buffer and invalidates any current batch.
    /// </summary>
    void Reset();
  }

  /// <summary>
  /// Batch buffer that manages a 32-bit <see cref="IndexBuffer"/>.
  /// </summary>
  public class Batch32BitIndexBuffer : BatchBuffer<int, IndexBuffer>, IBatchIndexBuffer<IndexBuffer>
  {
    private DataBuffer<int>? m_conversion;

    /// <inheritdoc />
    public IndexFormat IndexFormat { get { return IndexFormat.ThirtyTwoBits; } }

    /// <summary>
    /// Constructs a new instance of the <see cref="Batch32BitIndexBuffer"/> class.
    /// </summary>
    /// <param name="renderSystem">Render system used to construct the buffer.</param>
    /// <param name="initialElementCapacity">Initial number of elements in the buffer.</param>
    /// <param name="pendingBufferAllocator">Optional allocator for the CPU buffer that batch data accumulates in before being submitted to the GPU.</param>
    public Batch32BitIndexBuffer(IRenderSystem renderSystem, int initialElementCapacity, MemoryAllocatorStrategy pendingBufferAllocator = MemoryAllocatorStrategy.Default)
      : base(renderSystem, initialElementCapacity, pendingBufferAllocator)
    {
    }

    /// <inheritdoc />
    public bool Append(IRenderContext context, ReadOnlySpan<ushort> data, out Batch<IndexBuffer> batch)
    {
      return Append(context, data, false, out batch);
    }

    /// <inheritdoc />
    public bool Append(IRenderContext context, ReadOnlySpan<ushort> data, bool forceFinish, out Batch<IndexBuffer> batch)
    {
      // Convert the 16-bit data in a scratch buffer
      Span<int> intData = (data.Length <= 512) ? stackalloc int[data.Length] : GetConversionBuffer(data.Length);

      for (int i = 0; i < data.Length; i++)
        intData[i] = (int) data[i];

      return Append(context, intData, forceFinish, out batch);
    }

    /// <inheritdoc />
    public Batch<IndexBuffer> AppendBatch(IRenderContext context, ReadOnlySpan<ushort> data)
    {
      if (Append(context, data, true, out Batch<IndexBuffer> batch))
        return batch;

      throw new ArgumentOutOfRangeException(nameof(data));
    }

    /// <inheritdoc />
    protected override IndexBuffer CreateBuffer(IRenderSystem renderSystem, int requestedNumElements)
    {
      return new IndexBuffer(renderSystem, IndexFormat, requestedNumElements, ResourceUsage.Dynamic);
    }

    /// <inheritdoc />
    protected override void Dispose(bool disposing)
    {
      base.Dispose(disposing);

      if (disposing)
      {
        m_conversion?.Dispose();
        m_conversion = null;
      }
    }

    /// <summary>
    /// Gets a scratch buffer to convert indices.
    /// </summary>
    /// <param name="numElements">Buffer size.</param>
    /// <returns>Span into the scratch buffer.</returns>
    protected Span<int> GetConversionBuffer(int numElements)
    {
      if (m_conversion is null)
        m_conversion = DataBuffer.Create<int>(numElements);

      if (m_conversion.Length < numElements)
        m_conversion.Resize(numElements);

      return m_conversion.Span;
    }
  }

  /// <summary>
  /// Batch buffer that manages a 16-bit <see cref="IndexBuffer"/>.
  /// </summary>
  public class Batch16BitIndexBuffer : BatchBuffer<ushort, IndexBuffer>, IBatchIndexBuffer<IndexBuffer>
  {
    private DataBuffer<ushort>? m_conversion;

    /// <inheritdoc />
    public IndexFormat IndexFormat { get { return IndexFormat.SixteenBits; } }

    /// <summary>
    /// Constructs a new instance of the <see cref="Batch16BitIndexBuffer"/> class.
    /// </summary>
    /// <param name="renderSystem">Render system used to construct the buffer.</param>
    /// <param name="initialElementCapacity">Initial number of elements in the buffer.</param>
    /// <param name="pendingBufferAllocator">Optional allocator for the CPU buffer that batch data accumulates in before being submitted to the GPU.</param>
    public Batch16BitIndexBuffer(IRenderSystem renderSystem, int initialElementCapacity, MemoryAllocatorStrategy pendingBufferAllocator = MemoryAllocatorStrategy.Default)
      : base(renderSystem, initialElementCapacity, pendingBufferAllocator)
    {
    }

    /// <inheritdoc />
    public bool Append(IRenderContext context, ReadOnlySpan<int> data, out Batch<IndexBuffer> batch)
    {
      return Append(context, data, false, out batch);
    }

    /// <inheritdoc />
    public bool Append(IRenderContext context, ReadOnlySpan<int> data, bool forceFinish, out Batch<IndexBuffer> batch)
    {
      // Convert the 32-bit data in a scratch buffer
      Span<ushort> shortData = (data.Length <= 512) ? stackalloc ushort[data.Length] : GetConversionBuffer(data.Length);

      for (int i = 0; i < data.Length; i++)
        shortData[i] = (ushort) data[i];

      return Append(context, shortData, forceFinish, out batch);
    }

    /// <inheritdoc />
    public Batch<IndexBuffer> AppendBatch(IRenderContext context, ReadOnlySpan<int> data)
    {
      if (Append(context, data, true, out Batch<IndexBuffer> batch))
        return batch;

      throw new ArgumentOutOfRangeException(nameof(data));
    }

    /// <inheritdoc />
    protected override IndexBuffer CreateBuffer(IRenderSystem renderSystem, int requestedNumElements)
    {
      return new IndexBuffer(renderSystem, IndexFormat, requestedNumElements, ResourceUsage.Dynamic);
    }

    /// <inheritdoc />
    protected override void Dispose(bool disposing)
    {
      base.Dispose(disposing);

      if (disposing)
      {
        m_conversion?.Dispose();
        m_conversion = null;
      }
    }

    /// <summary>
    /// Gets a scratch buffer to convert indices.
    /// </summary>
    /// <param name="numElements">Buffer size.</param>
    /// <returns>Span into the scratch buffer.</returns>
    protected Span<ushort> GetConversionBuffer(int numElements)
    {
      if (m_conversion is null)
        m_conversion = DataBuffer.Create<ushort>(numElements);

      if (m_conversion.Length < numElements)
        m_conversion.Resize(numElements);

      return m_conversion.Span;
    }
  }

  /// <summary>
  /// Batch buffer that specifically writes batches to a <see cref="VertexBuffer"/>.
  /// </summary>
  /// <typeparam name="T">Type of vertex data.</typeparam>
  public class BatchVertexBuffer<T> : BatchBuffer<T, VertexBuffer> where T : unmanaged, IVertexType
  {
    /// <summary>
    /// Constructs a new instance of the <see cref="BatchVertexBuffer{T}"/> class.
    /// </summary>
    /// <param name="renderSystem">Render system used to construct the buffer.</param>
    /// <param name="initialElementCapacity">Initial number of elements in the buffer.</param>
    /// <param name="pendingBufferAllocator">Optional allocator for the CPU buffer that batch data accumulates in before being submitted to the GPU.</param>
    public BatchVertexBuffer(IRenderSystem renderSystem, int initialElementCapacity, MemoryAllocatorStrategy pendingBufferAllocator = MemoryAllocatorStrategy.Default) 
      : base(renderSystem, initialElementCapacity, pendingBufferAllocator)
    {
    }

    /// <inheritdoc />
    protected override VertexBuffer CreateBuffer(IRenderSystem renderSystem, int requestedNumElements)
    {
      T vtx = default;
      VertexLayout layout = vtx.GetVertexLayout();

      return new VertexBuffer(renderSystem, layout, requestedNumElements, ResourceUsage.Dynamic);
    }
  }

  /// <summary>
  /// Manages two batch buffers for indexed vertex data. Both 32 and 16-bit index data may be submitted and converted to the underlying index format.
  /// </summary>
  /// <typeparam name="T">Type of vertex data.</typeparam>
  /// <typeparam name="IndexBufferT"></typeparam>
  /// <typeparam name="VertexBufferT"></typeparam>
  public class IndexedBatchBuffer<T, IndexBufferT, VertexBufferT> : IDisposable, IDebugNamable, INamable where T : unmanaged where IndexBufferT : class, IBufferResource where VertexBufferT : class, IBufferResource
  {
    private bool m_isDisposed;
    private BatchBuffer<T, VertexBufferT> m_vertexBatchBuffer;
    private IBatchIndexBuffer<IndexBufferT> m_indexBatchBuffer;

    private string? m_name;
    private string? m_debugName;

    /// <summary>
    /// Gets whether this has been disposed or not.
    /// </summary>
    public bool IsDisposed { get { return m_isDisposed; } }

    /// <summary>
    /// Gets the render system used to create buffers.
    /// </summary>
    public IRenderSystem RenderSystem { get { return m_vertexBatchBuffer.RenderSystem; } }

    /// <summary>
    /// Gets whether a batch is currently started. Call <see cref="Finish(IRenderContext, out IndexedBatch{IndexBufferT, VertexBufferT})"/> to end a pending batch.
    /// </summary>
    public bool IsBatchStarted { get { return m_vertexBatchBuffer.IsBatchStarted; } }

    /// <summary>
    /// Gets the remaining vertex capacity until the buffer wraps around.
    /// </summary>
    public int RemainingVertexCapacity { get { return m_vertexBatchBuffer.RemainingCapacity; } }

    /// <summary>
    /// Gets the remaining index capacity until the buffer wraps around.
    /// </summary>
    public int RemainingIndexCapacity { get { return m_indexBatchBuffer.RemainingCapacity; } }

    /// <summary>
    /// Gets the vertex capacity of the buffer. This can grow to accomodate large data that is appended, but if small bits are appended the buffer
    /// attempts to wrap around instead of growing.
    /// </summary>
    public int VertexCapacity { get { return m_vertexBatchBuffer.Capacity; } }

    /// <summary>
    /// Gets the index capacity of the buffer. This can grow to accomodate large data that is appended, but if small bits are appended the buffer
    /// attempts to wrap around instead of growing.
    /// </summary>
    public int IndexCapacity { get { return m_indexBatchBuffer.Capacity; } }

    /// <summary>
    /// Gets the index format of the indices.
    /// </summary>
    public IndexFormat IndexFormat { get { return m_indexBatchBuffer.IndexFormat; } }

    /// <inheritdoc />
    public string Name { get { return m_name ?? ""; } set { m_name = value; } }

    /// <inheritdoc />
    public string DebugName
    {
      get
      {
        return m_debugName ?? "";
      }
      set
      {
        m_debugName = value;
        m_vertexBatchBuffer.DebugName = $"{m_debugName}_VertexBuffer";
        m_indexBatchBuffer.DebugName = $"{m_debugName}_IndexBuffer";
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="IndexedBatchBuffer{T, IndexBufferT, VertexBufferT}"/> class.
    /// </summary>
    /// <param name="vertexBatchBuffer"></param>
    /// <param name="indexBatchBuffer"></param>
    public IndexedBatchBuffer(BatchBuffer<T, VertexBufferT> vertexBatchBuffer, IBatchIndexBuffer<IndexBufferT> indexBatchBuffer)
    {
      ArgumentNullException.ThrowIfNull(vertexBatchBuffer, nameof(vertexBatchBuffer));
      ArgumentNullException.ThrowIfNull(indexBatchBuffer, nameof(indexBatchBuffer));

      m_isDisposed = false;
      m_indexBatchBuffer = indexBatchBuffer;
      m_vertexBatchBuffer = vertexBatchBuffer;
    }

    /// <summary>
    /// Appends vertex and index data to the buffers. This may or may not complete a pending batch.
    /// </summary>
    /// <param name="context">Render context used to submit the batch.</param>
    /// <param name="vertices">Vertex data to submit to be batched.</param>
    /// <param name="indices">Index data to submit to be batched.</param>
    /// <param name="batch">A completed batch, if one was finished. Not valid if the method does not return true.</param>
    /// <returns>True if there is a valid finished batch to be handled, false if not.</returns>
    public bool Append(IRenderContext context, ReadOnlySpan<T> vertices, ReadOnlySpan<int> indices, out IndexedBatch<IndexBufferT, VertexBufferT> batch)
    {
      return Append(context, vertices, indices, false, out batch);
    }

    /// <summary>
    /// Appends vertex and index data to the buffers. This may or may not complete a pending batch.
    /// </summary>
    /// <param name="context">Render context used to submit the batch.</param>
    /// <param name="vertices">Vertex data to submit to be batched.</param>
    /// <param name="indices">Index data to submit to be batched.</param>
    /// <param name="forceFinish">True if the append should complete a batch, resizing the GPU buffers if necessary (in case of a pending batch not having enough space).</param>
    /// <param name="batch">A completed batch, if one was finished. Not valid if the method does not return true.</param>
    /// <returns>True if there is a valid finished batch to be handled, false if not.</returns>
    public bool Append(IRenderContext context, ReadOnlySpan<T> vertices, ReadOnlySpan<int> indices, bool forceFinish, out IndexedBatch<IndexBufferT, VertexBufferT> batch)
    {
      // If no more room, force finish which will cause this to merge with the pending batch so we don't lose data
      bool wantFinish = forceFinish;
      if (!IsRoomInBatch(vertices.Length, indices.Length))
        wantFinish = true;

      bool hasVertex = m_vertexBatchBuffer.Append(context, vertices, wantFinish, out Batch<VertexBufferT> vBatch);
      bool hasIndex = m_indexBatchBuffer.Append(context, indices, wantFinish, out Batch<IndexBufferT> iBatch);

      if (!hasVertex || !hasIndex)
      {
        batch = default;
        return false;
      }

      batch = new IndexedBatch<IndexBufferT, VertexBufferT>()
      { IndexBuffer = iBatch.Buffer, VertexBuffer = vBatch.Buffer, StartIndex = iBatch.StartIndex, IndexCount = iBatch.ElementCount, BaseVertexOffset = vBatch.StartIndex };

      return true;
    }

    /// <summary>
    /// Appends vertex and index data to the buffers. This may or may not complete a pending batch.
    /// </summary>
    /// <param name="context">Render context used to submit the batch.</param>
    /// <param name="vertices">Vertex data to submit to be batched.</param>
    /// <param name="indices">Index data to submit to be batched.</param>
    /// <param name="batch">A completed batch, if one was finished. Not valid if the method does not return true.</param>
    /// <returns>True if there is a valid finished batch to be handled, false if not.</returns>
    public bool Append(IRenderContext context, ReadOnlySpan<T> vertices, ReadOnlySpan<ushort> indices, out IndexedBatch<IndexBufferT, VertexBufferT> batch)
    {
      return Append(context, vertices, indices, false, out batch);
    }

    /// <summary>
    /// Appends vertex and index data to the buffers. This may or may not complete a pending batch.
    /// </summary>
    /// <param name="context">Render context used to submit the batch.</param>
    /// <param name="vertices">Vertex data to submit to be batched.</param>
    /// <param name="indices">Index data to submit to be batched.</param>
    /// <param name="forceFinish">True if the append should complete a batch, resizing the GPU buffers if necessary (in case of a pending batch not having enough space).</param>
    /// <param name="batch">A completed batch, if one was finished. Not valid if the method does not return true.</param>
    /// <returns>True if there is a valid finished batch to be handled, false if not.</returns>
    public bool Append(IRenderContext context, ReadOnlySpan<T> vertices, ReadOnlySpan<ushort> indices, bool forceFinish, out IndexedBatch<IndexBufferT, VertexBufferT> batch)
    {
      // If no more room, force finish which will cause this to merge with the pending batch so we don't lose data
      bool wantFinish = forceFinish;
      if (!IsRoomInBatch(vertices.Length, indices.Length))
        wantFinish = true;

      bool hasVertex = m_vertexBatchBuffer.Append(context, vertices, wantFinish, out Batch<VertexBufferT> vBatch);
      bool hasIndex = m_indexBatchBuffer.Append(context, indices, wantFinish, out Batch<IndexBufferT> iBatch);

      if (!hasVertex || !hasIndex)
      {
        batch = default;
        return false;
      }

      batch = new IndexedBatch<IndexBufferT, VertexBufferT>()
      { IndexBuffer = iBatch.Buffer, VertexBuffer = vBatch.Buffer, StartIndex = iBatch.StartIndex, IndexCount = iBatch.ElementCount, BaseVertexOffset = vBatch.StartIndex };

      return true;
    }

    /// <summary>
    /// Appends vertex and index data and completes a batch. There may or may not be a pending batch, and if there is,
    /// the buffer may be resized in order to accommodate the data.
    /// </summary>
    /// <param name="context">Render context used to submit the batch.</param>
    /// <param name="vertices">Vertex data to submit to be batched.</param>
    /// <param name="indices">Index data to submit to be batched.</param>
    /// <returns>Info about the batch inside the GPU buffer.</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if an empty span was submitted.</exception>
    public IndexedBatch<IndexBufferT, VertexBufferT> AppendBatch(IRenderContext context, ReadOnlySpan<T> vertices, ReadOnlySpan<int> indices)
    {
      if (Append(context, vertices, indices, true, out IndexedBatch<IndexBufferT, VertexBufferT> batch))
        return batch;

      throw new ArgumentOutOfRangeException("Batch must have vertices/indices");
    }

    /// <summary>
    /// Appends vertex and index data and completes a batch. There may or may not be a pending batch, and if there is,
    /// the buffer may be resized in order to accommodate the data.
    /// </summary>
    /// <param name="context">Render context used to submit the batch.</param>
    /// <param name="vertices">Vertex data to submit to be batched.</param>
    /// <param name="indices">Index data to submit to be batched.</param>
    /// <returns>Info about the batch inside the GPU buffer.</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if an empty span was submitted.</exception>
    public IndexedBatch<IndexBufferT, VertexBufferT> AppendBatch(IRenderContext context, ReadOnlySpan<T> vertices, ReadOnlySpan<ushort> indices)
    {
      if (Append(context, vertices, indices, true, out IndexedBatch<IndexBufferT, VertexBufferT> batch))
        return batch;

      throw new ArgumentOutOfRangeException("Batch must have vertices/indices");
    }

    /// <summary>
    /// Finishes a pending batch.
    /// </summary>
    /// <param name="context">Render context used to submit the batch.</param>
    /// <param name="batch">Batch that has been submitted to the GPU buffer.</param>
    /// <returns>True if the batch was completed and submitted, false if there was no pending batch.</returns>
    public bool Finish(IRenderContext context, out IndexedBatch<IndexBufferT, VertexBufferT> batch)
    {
      bool hasVertex = m_vertexBatchBuffer.Finish(context, out Batch<VertexBufferT> vBatch);
      bool hasIndex = m_indexBatchBuffer.Finish(context, out Batch<IndexBufferT> iBatch);

      if (!hasVertex || !hasIndex)
      {
        batch = default;
        return false;
      }

      batch = new IndexedBatch<IndexBufferT, VertexBufferT>() 
        { IndexBuffer = iBatch.Buffer, VertexBuffer = vBatch.Buffer, StartIndex = iBatch.StartIndex, IndexCount = iBatch.ElementCount, BaseVertexOffset = vBatch.StartIndex };
      
      return true;
    }

    /// <summary>
    /// Resets the buffer and invalidates any current batch.
    /// </summary>
    public void Reset()
    {
      m_vertexBatchBuffer.Reset();
      m_indexBatchBuffer.Reset();
    }

    /// <inheritdoc />
    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    /// <summary>
    /// Disposes of resources.
    /// </summary>
    /// <param name="disposing">True if <see cref="Dispose"/> was called, false if not.</param>
    protected virtual void Dispose(bool disposing)
    {
      if (disposing && !m_isDisposed)
      {
        m_vertexBatchBuffer.Dispose();
        m_indexBatchBuffer.Dispose();
      }

      m_isDisposed = true;
    }

    private bool IsRoomInBatch(int numVertices, int numIndices)
    {
      if (!m_vertexBatchBuffer.IsBatchStarted)
        return true;

      return numVertices <= m_vertexBatchBuffer.RemainingCapacity && numIndices <= m_indexBatchBuffer.RemainingCapacity;
    }
  }

  /// <summary>
  /// Batch buffer that manages a rotating <see cref="VertexBuffer"/> and <see cref="IndexBuffer"/>.
  /// </summary>
  /// <typeparam name="T">Type of vertex data.</typeparam>
  public class IndexedBatchVertexBuffer<T> : IndexedBatchBuffer<T, IndexBuffer, VertexBuffer> where T : unmanaged, IVertexType
  {
    /// <summary>
    /// Constructs a new instance of the <see cref="IndexedBatchVertexBuffer{T}"/> class.
    /// </summary>
    /// <param name="renderSystem">Render system used to construct the buffer.</param>
    /// <param name="initialVertexCount">Initial capacity of the vertex buffer.</param>
    /// <param name="indexFormat">Index format for the index buffer, either 32 or 16-bit.</param>
    /// <param name="initialIndexCount">Initial capacity of the index buffer.</param>
    /// <param name="pendingBufferAllocator">Optional allocator for the CPU buffer that batch data accumulates in before being submitted to the GPU.</param>
    public IndexedBatchVertexBuffer(IRenderSystem renderSystem, int initialVertexCount, IndexFormat indexFormat, int initialIndexCount, MemoryAllocatorStrategy pendingBufferAllocator = MemoryAllocatorStrategy.Default)
      : base(new BatchVertexBuffer<T>(renderSystem, initialVertexCount, pendingBufferAllocator), (indexFormat == IndexFormat.ThirtyTwoBits) ? new Batch32BitIndexBuffer(renderSystem, initialIndexCount, pendingBufferAllocator) : new Batch16BitIndexBuffer(renderSystem, initialIndexCount, pendingBufferAllocator))
    {
    }
  }
}