﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;

#nullable enable

namespace Tesla.Graphics
{
  public sealed class SphereGenerator : GeometryGenerator
  {
    private Vector3 m_center;
    private float m_radius;
    private int m_tessellation;
    private int m_vertSegmentCount;
    private int m_horizSegmentCount;

    public Vector3 Center
    {
      get
      {
        return m_center;
      }
      set
      {
        m_center = value;
      }
    }

    public float Radius
    {
      get
      {
        return m_radius;
      }
      set
      {
        m_radius = value;
      }
    }

    public int Tessellation
    {
      get
      {
        return m_tessellation;
      }
      set
      {
        m_tessellation = (value < 3) ? 3 : value;

        m_vertSegmentCount = m_tessellation;
        m_horizSegmentCount = m_tessellation * 2;
      }
    }

    public SphereGenerator() : this(Vector3.Zero, 1.0f, 16, false) { }

    public SphereGenerator(float radius) : this(Vector3.Zero, radius, 16, false) { }

    public SphereGenerator(float radius, int tessellation) : this(Vector3.Zero, radius, tessellation, false) { }

    public SphereGenerator(in Vector3 center, float radius) : this(center, radius, 16, false) { }

    public SphereGenerator(in Vector3 center, float radius, int tessellation) : this(center, radius, tessellation, false) { }

    public SphereGenerator(in Vector3 center, float radius, int tessellation, bool insideOut)
    {
      m_center = center;
      m_radius = radius;
      Tessellation = (tessellation < 3) ? 3 : tessellation;
      InsideOut = insideOut;
    }

    public override void CalculateVertexIndexCounts(out int vertexCount, out int indexCount)
    {
      vertexCount = (m_vertSegmentCount + 1) * (m_horizSegmentCount + 1);
      indexCount = m_vertSegmentCount * (m_horizSegmentCount + 1) * 6;
    }

    public override void Build([AllowNull] ref DataBuffer<VertexPositionNormalTexture> vertices, ref IndexData indices)
    {
      int vertCount, indexCount;
      CalculateVertexIndexCounts(out vertCount, out indexCount);

      DataBufferBuilder<VertexPositionNormalTexture> vertexBuilder = CreateVertexBuilder<VertexPositionNormalTexture>(vertCount, vertices);
      IndexDataBuilder indexBuilder = CreateIndexBuilder(indexCount, indices);

      GenerateData(vertexBuilder, m_center, m_radius, m_vertSegmentCount, m_horizSegmentCount, InsideOut);
      GenerateIndexData(indexBuilder, m_vertSegmentCount, m_horizSegmentCount, InsideOut);

      vertices = vertexBuilder.Claim(true);
      indices = indexBuilder.Claim(true);
    }

    public override void Build([AllowNull] ref DataBuffer<Vector3> positions, [AllowNull] ref DataBuffer<Vector3> normals, [AllowNull] ref DataBuffer<Vector2> textureCoordinates, ref IndexData indices, GenerateOptions options)
    {
      if (options == GenerateOptions.None)
        return;

      int vertCount, indexCount;
      CalculateVertexIndexCounts(out vertCount, out indexCount);

      DataBufferBuilder<Vector3>? posBuilder = null;
      DataBufferBuilder<Vector3>? normalBuilder = null;
      DataBufferBuilder<Vector2>? texCoordBuilder = null;
      IndexDataBuilder indexBuilder = CreateIndexBuilder(indexCount, indices);

      if ((options & GenerateOptions.Positions) == GenerateOptions.Positions)
        posBuilder = CreateVertexBuilder<Vector3>(vertCount, positions);

      if ((options & GenerateOptions.Normals) == GenerateOptions.Normals)
        normalBuilder = CreateVertexBuilder<Vector3>(vertCount, normals);

      if ((options & GenerateOptions.TextureCoordinates) == GenerateOptions.TextureCoordinates)
        texCoordBuilder = CreateVertexBuilder<Vector2>(vertCount, textureCoordinates);

      GenerateData(posBuilder, normalBuilder, texCoordBuilder, m_center, m_radius, m_vertSegmentCount, m_horizSegmentCount, InsideOut);
      GenerateIndexData(indexBuilder, m_vertSegmentCount, m_horizSegmentCount, InsideOut);

      if (posBuilder is not null)
        positions = posBuilder.Claim(true);

      if (normalBuilder is not null)
        normals = normalBuilder.Claim(true);

      if (texCoordBuilder is not null)
        textureCoordinates = texCoordBuilder.Claim(true);

      indices = indexBuilder.Claim(true);
    }

    private static void GenerateData(DataBufferBuilder<Vector3>? positions, DataBufferBuilder<Vector3>? normals, DataBufferBuilder<Vector2>? textureCoordinates, in Vector3 center, float radius, int vertSegmentCount, int horizSegmentCount, bool flip)
    {
      if (positions is not null)
        positions.Position = 0;

      if (normals is not null)
        normals.Position = 0;

      if (textureCoordinates is not null)
        textureCoordinates.Position = 0;

      //Create rings of vertices at progressively higher latitudes
      for (int i = 0; i <= vertSegmentCount; i++)
      {
        float v = 1.0f - ((float) i / vertSegmentCount);

        Angle latitude = Angle.FromRadians(((i * MathHelper.Pi) / vertSegmentCount) - MathHelper.PiOverTwo);
        float dy = latitude.Sin;
        float dxz = latitude.Cos;

        //Create a single ring of vertices at this latitude
        for (int j = 0; j <= horizSegmentCount; j++)
        {
          float u = (float) j / horizSegmentCount;

          Angle longitude = Angle.FromRadians((j * 2.0f * MathHelper.Pi) / horizSegmentCount);
          float dx = longitude.Sin;
          float dz = longitude.Cos;

          dx *= dxz;
          dz *= dxz;

          Vector3 normal = new Vector3(dx, dy, dz);
          Vector2 texCoords = new Vector2(u, v);

          Vector3 pos;
          Vector3.Multiply(normal, radius, out pos);
          Vector3.Add(pos, center, out pos);

          if (flip)
            normal.Negate();

          if (positions is not null)
            positions.Set(pos);

          if (normals is not null)
            normals.Set(normal);

          if (textureCoordinates is not null)
            textureCoordinates.Set(texCoords);
        }
      }
    }

    private static void GenerateData(DataBufferBuilder<VertexPositionNormalTexture> data, in Vector3 center, float radius, int vertSegmentCount, int horizSegmentCount, bool flip)
    {
      data.Position = 0;

      //Create rings of vertices at progressively higher latitudes
      for (int i = 0; i <= vertSegmentCount; i++)
      {
        float v = 1.0f - ((float) i / vertSegmentCount);

        Angle latitude = Angle.FromRadians(((i * MathHelper.Pi) / vertSegmentCount) - MathHelper.PiOverTwo);
        float dy = latitude.Sin;
        float dxz = latitude.Cos;

        //Create a single ring of vertices at this latitude
        for (int j = 0; j <= horizSegmentCount; j++)
        {
          float u = (float) j / horizSegmentCount;

          Angle longitude = Angle.FromRadians((j * 2.0f * MathHelper.Pi) / horizSegmentCount);
          float dx = longitude.Sin;
          float dz = longitude.Cos;

          dx *= dxz;
          dz *= dxz;

          Vector3 normal = new Vector3(dx, dy, dz);
          Vector2 texCoords = new Vector2(u, v);

          Vector3 pos;
          Vector3.Multiply(normal, radius, out pos);
          Vector3.Add(pos, center, out pos);

          if (flip)
            normal.Negate();

          VertexPositionNormalTexture vert = new VertexPositionNormalTexture(pos, normal, texCoords);
          data.Set(vert);
        }
      }
    }

    private static void GenerateIndexData(IndexDataBuilder indices, int vertSegmentCount, int horizSegmentCount, bool flip)
    {
      indices.Position = 0;

      //Create triangles joining each pair of latitude rings
      int stride = horizSegmentCount + 1;

      if (flip)
      {
        for (int i = 0; i < vertSegmentCount; i++)
        {
          for (int j = 0; j <= horizSegmentCount; j++)
          {
            int nextI = i + 1;
            int nextJ = (j + 1) % stride;

            indices.Set(i * stride + nextJ);
            indices.Set(nextI * stride + j);
            indices.Set(i * stride + j);

            indices.Set(nextI * stride + nextJ);
            indices.Set(nextI * stride + j);
            indices.Set(i * stride + nextJ);
          }
        }
      }
      else
      {
        for (int i = 0; i < vertSegmentCount; i++)
        {
          for (int j = 0; j <= horizSegmentCount; j++)
          {
            int nextI = i + 1;
            int nextJ = (j + 1) % stride;

            indices.Set(i * stride + j);
            indices.Set(nextI * stride + j);
            indices.Set(i * stride + nextJ);

            indices.Set(i * stride + nextJ);
            indices.Set(nextI * stride + j);
            indices.Set(nextI * stride + nextJ);
          }
        }
      }
    }
  }
}
