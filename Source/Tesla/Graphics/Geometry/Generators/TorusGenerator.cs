﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System.Diagnostics.CodeAnalysis;

#nullable enable

namespace Tesla.Graphics
{
  public sealed class TorusGenerator : GeometryGenerator
  {
    private Vector3 m_center;
    private float m_radius;
    private float m_thickness;
    private int m_tessellation;

    public Vector3 Center
    {
      get
      {
        return m_center;
      }
      set
      {
        m_center = value;
      }
    }

    public float Radius
    {
      get
      {
        return m_radius;
      }
      set
      {
        m_radius = value;
      }
    }

    public float Thickness
    {
      get
      {
        return m_thickness;
      }
      set
      {
        m_thickness = value;
      }
    }

    public int Tessellation
    {
      get
      {
        return m_tessellation;
      }
      set
      {
        m_tessellation = (value < 3) ? 3 : value;
      }
    }

    public TorusGenerator() : this(Vector3.Zero, 1.0f, MathHelper.TwoThird, 32, false) { }

    public TorusGenerator(float radius, float thickness) : this(Vector3.Zero, radius, thickness, 32, false) { }

    public TorusGenerator(float radius, float thickness, int tessellation) : this(Vector3.Zero, radius, thickness, tessellation, false) { }

    public TorusGenerator(Vector3 center, float radius, float thickness) : this(center, radius, thickness, 32, false) { }

    public TorusGenerator(Vector3 center, float radius, float thickness, int tessellation) : this(center, radius, thickness, tessellation, false) { }

    public TorusGenerator(Vector3 center, float radius, float thickness, int tessellation, bool insideOut)
    {
      m_center = center;
      m_radius = radius;
      m_thickness = thickness;
      Tessellation = tessellation;
      InsideOut = insideOut;
    }

    public override void CalculateVertexIndexCounts(out int vertexCount, out int indexCount)
    {
      vertexCount = (m_tessellation + 1) * (m_tessellation + 1);
      indexCount = m_tessellation * (m_tessellation + 1) * 6;
    }

    public override void Build([AllowNull] ref DataBuffer<VertexPositionNormalTexture> vertices, ref IndexData indices)
    {
      int vertCount, indexCount;
      CalculateVertexIndexCounts(out vertCount, out indexCount);

      DataBufferBuilder<VertexPositionNormalTexture> vertexBuilder = CreateVertexBuilder<VertexPositionNormalTexture>(vertCount, vertices);
      IndexDataBuilder indexBuilder = CreateIndexBuilder(indexCount, indices);

      GenerateData(vertexBuilder, indexBuilder, ref m_center, m_radius, m_thickness, m_tessellation, InsideOut);

      vertices = vertexBuilder.Claim(true);
      indices = indexBuilder.Claim(true);
    }

    public override void Build([AllowNull] ref DataBuffer<Vector3> positions, [AllowNull] ref DataBuffer<Vector3> normals, [AllowNull] ref DataBuffer<Vector2> textureCoordinates, ref IndexData indices, GenerateOptions options)
    {
      if (options == GenerateOptions.None)
        return;

      int vertCount, indexCount;
      CalculateVertexIndexCounts(out vertCount, out indexCount);

      DataBufferBuilder<Vector3>? posBuilder = null;
      DataBufferBuilder<Vector3>? normalBuilder = null;
      DataBufferBuilder<Vector2>? texCoordBuilder = null;
      IndexDataBuilder indexBuilder = CreateIndexBuilder(indexCount, indices);

      if ((options & GenerateOptions.Positions) == GenerateOptions.Positions)
        posBuilder = CreateVertexBuilder<Vector3>(vertCount, positions);

      if ((options & GenerateOptions.Normals) == GenerateOptions.Normals)
        normalBuilder = CreateVertexBuilder<Vector3>(vertCount, normals);

      if ((options & GenerateOptions.TextureCoordinates) == GenerateOptions.TextureCoordinates)
        texCoordBuilder = CreateVertexBuilder<Vector2>(vertCount, textureCoordinates);

      GenerateData(posBuilder, normalBuilder, texCoordBuilder, indexBuilder, ref m_center, m_radius, m_thickness, m_tessellation, InsideOut);

      if (posBuilder is not null)
        positions = posBuilder.Claim(true);

      if (normalBuilder is not null)
        normals = normalBuilder.Claim(true);

      if (texCoordBuilder is not null)
        textureCoordinates = texCoordBuilder.Claim(true);

      indices = indexBuilder.Claim(true);
    }

    private static void GenerateData(DataBufferBuilder<VertexPositionNormalTexture> data, IndexDataBuilder indices, ref Vector3 center, float radius, float thickness, int tessellation, bool flip)
    {
      data.Position = 0;
      indices.Position = 0;

      int stride = tessellation + 1;

      //Loop around the main ring
      for (int i = 0; i <= tessellation; i++)
      {
        float u = (float) i / tessellation;

        Angle outerAngle = Angle.FromRadians(((i * MathHelper.TwoPi) / (float) tessellation) - MathHelper.PiOverTwo);

        Matrix transMat, rotMat, transform;
        Matrix.FromTranslation(radius + center.X, center.Y, center.Z, out transMat);
        Matrix.FromRotationY(outerAngle, out rotMat);
        Matrix.Multiply(transMat, rotMat, out transform);

        //Loop along other axis, around side of the tube
        for (int j = 0; j <= tessellation; j++)
        {
          float v = 1.0f - ((float) j / tessellation);

          Angle innerAngle = Angle.FromRadians(((j * MathHelper.TwoPi) / tessellation) + MathHelper.Pi);
          float dx = innerAngle.Cos;
          float dy = innerAngle.Sin;

          Vector3 normal = new Vector3(dx, dy, 0);
          Vector2 texCoord = new Vector2(u, v);
          Vector3 position;
          Vector3.Multiply(normal, thickness * 0.5f, out position);

          Vector3.Transform(position, transform, out position);
          Vector3.TransformNormal(normal, transform, out normal);

          if (flip)
            normal.Negate();

          VertexPositionNormalTexture vertex = new VertexPositionNormalTexture(position, normal, texCoord);
          data.Set(vertex);

          int nextI = (i + 1) % stride;
          int nextJ = (j + 1) % stride;

          if (flip)
          {
            indices.Set((nextI * stride) + j);
            indices.Set((i * stride) + nextJ);
            indices.Set((i * stride) + j);

            indices.Set((nextI * stride) + j);
            indices.Set((nextI * stride) + nextJ);
            indices.Set((i * stride) + nextJ);
          }
          else
          {
            indices.Set((i * stride) + j);
            indices.Set((i * stride) + nextJ);
            indices.Set((nextI * stride) + j);

            indices.Set((i * stride) + nextJ);
            indices.Set((nextI * stride) + nextJ);
            indices.Set((nextI * stride) + j);
          }
        }
      }
    }

    private static void GenerateData(DataBufferBuilder<Vector3>? positions, DataBufferBuilder<Vector3>? normals, DataBufferBuilder<Vector2>? textureCoordinates, IndexDataBuilder indices, ref Vector3 center, float radius, float thickness, int tessellation, bool flip)
    {
      if (positions is not null)
        positions.Position = 0;

      if (normals is not null)
        normals.Position = 0;

      if (textureCoordinates is not null)
        textureCoordinates.Position = 0;

      indices.Position = 0;

      int stride = tessellation + 1;

      //Loop around the main ring
      for (int i = 0; i <= tessellation; i++)
      {
        float u = (float) i / tessellation;

        Angle outerAngle = Angle.FromRadians(((i * MathHelper.TwoPi) / (float) tessellation) - MathHelper.PiOverTwo);

        Matrix transMat, rotMat, transform;
        Matrix.FromTranslation(radius + center.X, center.Y, center.Z, out transMat);
        Matrix.FromRotationY(outerAngle, out rotMat);
        Matrix.Multiply(transMat, rotMat, out transform);

        //Loop along other axis, around side of the tube
        for (int j = 0; j <= tessellation; j++)
        {
          float v = 1.0f - ((float) j / tessellation);

          Angle innerAngle = Angle.FromRadians(((j * MathHelper.TwoPi) / tessellation) + MathHelper.Pi);
          float dx = innerAngle.Cos;
          float dy = innerAngle.Sin;

          Vector3 normal = new Vector3(dx, dy, 0);
          Vector3 position;
          Vector3.Multiply(normal, thickness, out position);
          Vector3.Multiply(position, 0.5f, out position);

          if (flip)
            normal.Negate();

          if (positions is not null)
          {
            Vector3.Transform(position, transform, out position);
            positions.Set(position);
          }

          if (normals is not null)
          {
            Vector3.TransformNormal(normal, transform, out normal);
            normals.Set(normal);
          }

          if (textureCoordinates is not null)
          {
            Vector2 texCoord = new Vector2(u, v);
            textureCoordinates.Set(texCoord);
          }

          int nextI = (i + 1) % stride;
          int nextJ = (j + 1) % stride;

          if (flip)
          {
            indices.Set(nextI * stride + j);
            indices.Set(i * stride + nextJ);
            indices.Set(i * stride + j);

            indices.Set(nextI * stride + j);
            indices.Set(nextI * stride + nextJ);
            indices.Set(i * stride + nextJ);
          }
          else
          {
            indices.Set(i * stride + j);
            indices.Set(i * stride + nextJ);
            indices.Set(nextI * stride + j);

            indices.Set(i * stride + nextJ);
            indices.Set(nextI * stride + nextJ);
            indices.Set(nextI * stride + j);
          }
        }
      }
    }
  }
}
