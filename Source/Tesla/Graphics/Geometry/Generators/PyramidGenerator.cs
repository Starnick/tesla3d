﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;

#nullable enable

namespace Tesla.Graphics
{
  public sealed class PyramidGenerator : GeometryGenerator
  {
    private const int MaxIndices = 18;
    private const int MaxVertices = 16;

    private static readonly int[] s_indices = new int[] { 0, 1, 2, 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
    private static readonly int[] s_indicesFlip = new int[] { 0, 2, 1, 0, 3, 2, 4, 6, 5, 7, 9, 8, 10, 12, 11, 13, 15, 14 };

    private Vector3 m_center;
    private float m_baseWidth;
    private float m_height;

    public Vector3 Center
    {
      get
      {
        return m_center;
      }
      set
      {
        m_center = value;
      }
    }

    public float BaseWidth
    {
      get
      {
        return m_baseWidth;
      }
      set
      {
        m_baseWidth = value;
      }
    }

    public float Height
    {
      get
      {
        return m_height;
      }
      set
      {
        m_height = value;
      }
    }

    public PyramidGenerator() : this(Vector3.Zero, 1.0f, 1.0f, false) { }

    public PyramidGenerator(float baseWidth, float height) : this(Vector3.Zero, baseWidth, height, false) { }

    public PyramidGenerator(Vector3 center, float baseWidth, float height, bool insideOut)
    {
      m_center = center;
      m_baseWidth = baseWidth;
      m_height = height;
      InsideOut = insideOut;
    }

    public override void CalculateVertexIndexCounts(out int vertexCount, out int indexCount)
    {
      vertexCount = MaxVertices;
      indexCount = MaxIndices;
    }

    public override void Build([AllowNull] ref DataBuffer<VertexPositionNormalTexture> vertices, ref IndexData indices)
    {
      DataBufferBuilder<VertexPositionNormalTexture> vertexBuilder = CreateVertexBuilder<VertexPositionNormalTexture>(MaxVertices, vertices);
      IndexDataBuilder indexBuilder = CreateIndexBuilder(MaxIndices, indices);

      GenerateData(vertexBuilder, ref m_center, m_baseWidth, m_height, InsideOut);
      GenerateIndexData(indexBuilder, InsideOut);

      vertices = vertexBuilder.Claim(true);
      indices = indexBuilder.Claim(true);
    }

    public override void Build([AllowNull] ref DataBuffer<Vector3> positions, [AllowNull] ref DataBuffer<Vector3> normals, [AllowNull] ref DataBuffer<Vector2> textureCoordinates, ref IndexData indices, GenerateOptions options)
    {
      if (options == GenerateOptions.None)
        return;

      int vertCount = MaxVertices;
      int indexCount = MaxIndices;

      DataBufferBuilder<Vector3>? posBuilder = null;
      DataBufferBuilder<Vector3>? normalBuilder = null;
      DataBufferBuilder<Vector2>? texCoordBuilder = null;
      IndexDataBuilder indexBuilder = CreateIndexBuilder(indexCount, indices);

      if ((options & GenerateOptions.Positions) == GenerateOptions.Positions)
        posBuilder = CreateVertexBuilder<Vector3>(vertCount, positions);

      if ((options & GenerateOptions.Normals) == GenerateOptions.Normals)
        normalBuilder = CreateVertexBuilder<Vector3>(vertCount, normals);

      if ((options & GenerateOptions.TextureCoordinates) == GenerateOptions.TextureCoordinates)
        texCoordBuilder = CreateVertexBuilder<Vector2>(vertCount, textureCoordinates);

      if (posBuilder is not null || normalBuilder is not null)
        GeneratePositionNormalData(posBuilder, normalBuilder, ref m_center, m_baseWidth, m_height, InsideOut);

      if (texCoordBuilder is not null)
        GenerateTextureData(texCoordBuilder, InsideOut);

      GenerateIndexData(indexBuilder, InsideOut);

      if (posBuilder is not null)
        positions = posBuilder.Claim(true);

      if (normalBuilder is not null)
        normals = normalBuilder.Claim(true);

      if (texCoordBuilder is not null)
        textureCoordinates = texCoordBuilder.Claim(true);

      indices = indexBuilder.Claim(true);
    }

    private static void ComputeVertices(ref Vector3 center, float baseWidth, float height, out Vector3 b0, out Vector3 b1, out Vector3 b2, out Vector3 b3, out Vector3 topPoint)
    {
      //Calculating the vertices of the pyramid looking down at it, with +X to the right and -Z up
      float halfBaseWidth = baseWidth * 0.5f;
      float halfHeight = height * 0.5f;

      b0 = new Vector3(center.X - halfBaseWidth, center.Y - halfHeight, center.Z - halfBaseWidth);
      b1 = new Vector3(center.X + halfBaseWidth, center.Y - halfHeight, center.Z - halfBaseWidth);
      b2 = new Vector3(center.X + halfBaseWidth, center.Y - halfHeight, center.Z + halfBaseWidth);
      b3 = new Vector3(center.X - halfBaseWidth, center.Y - halfHeight, center.Z + halfBaseWidth);

      topPoint = new Vector3(center.X, center.Y + halfHeight, center.Z);
    }

    private static void GenerateData(DataBufferBuilder<VertexPositionNormalTexture> data, ref Vector3 center, float baseWidth, float height, bool flip)
    {
      data.Position = 0;

      //Calculating the vertices of the pyramid looking down at it, with +X to the right and -Z up
      Vector3 b0, b1, b2, b3, topPoint;
      ComputeVertices(ref center, baseWidth, height, out b0, out b1, out b2, out b3, out topPoint);

      //Setup UV coordinates
      Vector2 uvUpperLeft = new Vector2(0, 0);
      Vector2 uvUpperRight = new Vector2(1, 0);
      Vector2 uvLowerRight = new Vector2(1, 1);
      Vector2 uvLowerLeft = new Vector2(0, 1);
      Vector2 uvUpperMid = new Vector2(.5f, 0.0f);

      //Setup base - in clockwise ordering as viewed from the bottom looking +Y
      Vector3 n = (flip) ? new Vector3(0, 1, 0) : new Vector3(0, -1, 0);
      VertexPositionNormalTexture v = new VertexPositionNormalTexture(b1, n, uvUpperLeft);
      data.Set(v);

      v = new VertexPositionNormalTexture(b0, n, uvUpperRight);
      data.Set(v);

      v = new VertexPositionNormalTexture(b3, n, uvLowerRight);
      data.Set(v);

      v = new VertexPositionNormalTexture(b2, n, uvLowerLeft);
      data.Set(v);

      //Front face
      Triangle tri = new Triangle(topPoint, b2, b3);
      n = tri.Normal;
      if (flip)
        n.Negate();

      v = new VertexPositionNormalTexture(topPoint, n, uvUpperMid);
      data.Set(v);

      v = new VertexPositionNormalTexture(b2, n, uvLowerLeft);
      data.Set(v);

      v = new VertexPositionNormalTexture(b3, n, uvLowerRight);
      data.Set(v);

      //Back face
      tri = new Triangle(topPoint, b0, b1);
      n = tri.Normal;
      if (flip)
        n.Negate();

      v = new VertexPositionNormalTexture(topPoint, n, uvUpperMid);
      data.Set(v);

      v = new VertexPositionNormalTexture(b0, n, uvLowerLeft);
      data.Set(v);

      v = new VertexPositionNormalTexture(b1, n, uvLowerRight);
      data.Set(v);

      //Left face
      tri = new Triangle(topPoint, b3, b0);
      n = tri.Normal;
      if (flip)
        n.Negate();

      v = new VertexPositionNormalTexture(topPoint, n, uvUpperMid);
      data.Set(v);

      v = new VertexPositionNormalTexture(b3, n, uvLowerLeft);
      data.Set(v);

      v = new VertexPositionNormalTexture(b0, n, uvLowerRight);
      data.Set(v);

      //Right face
      tri = new Triangle(topPoint, b1, b2);
      n = tri.Normal;
      if (flip)
        n.Negate();

      v = new VertexPositionNormalTexture(topPoint, n, uvUpperMid);
      data.Set(v);

      v = new VertexPositionNormalTexture(b1, n, uvLowerLeft);
      data.Set(v);

      v = new VertexPositionNormalTexture(b0, n, uvLowerRight);
      data.Set(v);
    }

    private static void GeneratePositionNormalData(DataBufferBuilder<Vector3>? positions, DataBufferBuilder<Vector3>? normals, ref Vector3 center, float baseWidth, float height, bool flip)
    {
      //Calculating the vertices of the pyramid looking down at it, with +X to the right and -Z up
      Vector3 b0, b1, b2, b3, topPoint;
      ComputeVertices(ref center, baseWidth, height, out b0, out b1, out b2, out b3, out topPoint);

      if (positions is not null)
      {
        positions.Position = 0;

        //Setup base - in clockwise ordering as viewed from the bottom looking +Y
        positions.Set(b1);
        positions.Set(b0);
        positions.Set(b3);
        positions.Set(b2);

        //Front face
        positions.Set(topPoint);
        positions.Set(b2);
        positions.Set(b3);

        //Back face
        positions.Set(topPoint);
        positions.Set(b0);
        positions.Set(b1);

        //Left face
        positions.Set(topPoint);
        positions.Set(b3);
        positions.Set(b0);

        //Right face
        positions.Set(topPoint);
        positions.Set(b1);
        positions.Set(b2);
      }

      if (normals is not null)
      {
        normals.Position = 0;

        Vector3 n = (flip) ? new Vector3(0, 1, 0) : new Vector3(0, -1, 0);

        //Base
        normals.Set(n);
        normals.Set(n);
        normals.Set(n);
        normals.Set(n);

        //Front face
        Triangle tri = new Triangle(topPoint, b2, b3);
        n = tri.Normal;
        if (flip)
          n.Negate();

        normals.Set(n);
        normals.Set(n);
        normals.Set(n);

        //Back face
        tri = new Triangle(topPoint, b0, b1);
        n = tri.Normal;
        if (flip)
          n.Negate();

        normals.Set(n);
        normals.Set(n);
        normals.Set(n);

        //Left face
        tri = new Triangle(topPoint, b3, b0);
        n = tri.Normal;
        if (flip)
          n.Negate();

        normals.Set(n);
        normals.Set(n);
        normals.Set(n);

        //Right face
        tri = new Triangle(topPoint, b1, b2);
        n = tri.Normal;
        if (flip)
          n.Negate();

        normals.Set(n);
        normals.Set(n);
        normals.Set(n);
      }
    }

    private static void GenerateTextureData(DataBufferBuilder<Vector2> texCoords, bool flip)
    {
      texCoords.Position = 0;

      Vector2 uvUpperLeft = new Vector2(0, 0);
      Vector2 uvUpperRight = new Vector2(1, 0);
      Vector2 uvLowerRight = new Vector2(1, 1);
      Vector2 uvLowerLeft = new Vector2(0, 1);
      Vector2 uvUpperMid = new Vector2(.5f, 0.0f);

      //Setup base
      texCoords.Set(uvUpperLeft);
      texCoords.Set(uvUpperRight);
      texCoords.Set(uvLowerRight);
      texCoords.Set(uvLowerLeft);

      //Front face
      texCoords.Set(uvUpperMid);
      texCoords.Set(uvLowerLeft);
      texCoords.Set(uvLowerRight);

      //Back face
      texCoords.Set(uvUpperMid);
      texCoords.Set(uvLowerLeft);
      texCoords.Set(uvLowerRight);

      //Left face
      texCoords.Set(uvUpperMid);
      texCoords.Set(uvLowerLeft);
      texCoords.Set(uvLowerRight);

      //Right face
      texCoords.Set(uvUpperMid);
      texCoords.Set(uvLowerLeft);
      texCoords.Set(uvLowerRight);
    }

    private static void GenerateIndexData(IndexDataBuilder indices, bool flip)
    {
      indices.Position = 0;

      if (flip)
      {
        indices.SetRange(s_indicesFlip);
      }
      else
      {
        indices.SetRange(s_indices);
      }
    }
  }
}
