﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;

#nullable enable

namespace Tesla.Graphics
{
  public sealed class CylinderGenerator : GeometryGenerator
  {
    private Segment m_centerLine;
    private float m_topRadius;
    private float m_bottomRadius;
    private int m_vertSegmentCount;
    private int m_horizSegmentCount;

    public Segment CenterLine
    {
      get
      {
        return m_centerLine;
      }
      set
      {
        m_centerLine = value;
      }
    }

    public float TopRadius
    {
      get
      {
        return m_topRadius;
      }
      set
      {
        m_topRadius = value;
      }
    }

    public float BottomRadius
    {
      get
      {
        return m_bottomRadius;
      }
      set
      {
        m_bottomRadius = value;
      }
    }

    public int VerticalSegmentCount
    {
      get
      {
        return m_vertSegmentCount;
      }
      set
      {
        m_vertSegmentCount = (value < 1) ? 1 : value;
      }
    }

    public int HorizontalSegmentCount
    {
      get
      {
        return m_horizSegmentCount;
      }
      set
      {
        m_horizSegmentCount = (value < 3) ? 3 : value;
      }
    }

    public CylinderGenerator() : this(new Segment(new Vector3(0.0f, -1.0f, 0.0f), new Vector3(0.0f, 1.0f, 0.0f)), 0.5f, 0.5f, 8, false) { }

    public CylinderGenerator(in Segment centerLine, float radius) : this(centerLine, radius, radius, 8, false) { }

    public CylinderGenerator(in Segment centerLine, float radius, int tessellation) : this(centerLine, radius, radius, tessellation, false) { }

    public CylinderGenerator(in Segment centerLine, float radius, int tessellation, bool insideOut) : this(centerLine, radius, radius, tessellation, insideOut) { }

    public CylinderGenerator(in Segment centerLine, float bottomRadius, float topRadius) : this(centerLine, bottomRadius, topRadius, 8, false) { }

    public CylinderGenerator(in Segment centerLine, float bottomRadius, float topRadius, int tessellation) : this(centerLine, bottomRadius, topRadius, tessellation, false) { }

    public CylinderGenerator(in Segment centerLine, float bottomRadius, float topRadius, int tessellation, bool insideOut)
    {
      m_centerLine = centerLine;
      m_bottomRadius = bottomRadius;
      m_topRadius = topRadius;
      SetTessellation(tessellation);
      InsideOut = insideOut;
    }

    public void SetTessellation(int tessellation)
    {
      VerticalSegmentCount = tessellation;
      HorizontalSegmentCount = tessellation * 2;
    }

    public override void CalculateVertexIndexCounts(out int vertexCount, out int indexCount)
    {
      //Cylinder side
      vertexCount = (m_vertSegmentCount + 1) * (m_horizSegmentCount + 1);
      indexCount = m_vertSegmentCount * m_horizSegmentCount * 6;

      //Top disk
      if (!MathHelper.IsNearlyZero(m_topRadius))
      {
        vertexCount += m_horizSegmentCount + 1;
        indexCount += m_horizSegmentCount * 3;
      }

      //Bottom disk
      if (!MathHelper.IsNearlyZero(m_bottomRadius))
      {
        vertexCount += m_horizSegmentCount + 1;
        indexCount += m_horizSegmentCount * 3;
      }
    }

    public override void Build([AllowNull] ref DataBuffer<VertexPositionNormalTexture> vertices, ref IndexData indices)
    {
      int vertCount, indexCount;
      CalculateVertexIndexCounts(out vertCount, out indexCount);

      DataBufferBuilder<VertexPositionNormalTexture> vertexBuilder = CreateVertexBuilder<VertexPositionNormalTexture>(vertCount, vertices);
      IndexDataBuilder indexBuilder = CreateIndexBuilder(indexCount, indices);

      GenerateData(vertexBuilder, indexBuilder, m_centerLine, m_topRadius, m_bottomRadius, m_vertSegmentCount, m_horizSegmentCount, InsideOut);

      vertices = vertexBuilder.Claim(true);
      indices = indexBuilder.Claim(true);
    }

    public override void Build([AllowNull] ref DataBuffer<Vector3> positions, [AllowNull] ref DataBuffer<Vector3> normals, [AllowNull] ref DataBuffer<Vector2> textureCoordinates, ref IndexData indices, GenerateOptions options)
    {
      if (options == GenerateOptions.None)
        return;

      int vertCount, indexCount;
      CalculateVertexIndexCounts(out vertCount, out indexCount);

      DataBufferBuilder<Vector3>? posBuilder = null;
      DataBufferBuilder<Vector3>? normalBuilder = null;
      DataBufferBuilder<Vector2>? texCoordBuilder = null;
      IndexDataBuilder indexBuilder = CreateIndexBuilder(indexCount, indices);

      if ((options & GenerateOptions.Positions) == GenerateOptions.Positions)
        posBuilder = CreateVertexBuilder<Vector3>(vertCount, positions);

      if ((options & GenerateOptions.Normals) == GenerateOptions.Normals)
        normalBuilder = CreateVertexBuilder<Vector3>(vertCount, normals);

      if ((options & GenerateOptions.TextureCoordinates) == GenerateOptions.TextureCoordinates)
        texCoordBuilder = CreateVertexBuilder<Vector2>(vertCount, textureCoordinates);

      GenerateData(posBuilder, normalBuilder, texCoordBuilder, indexBuilder, m_centerLine, m_topRadius, m_bottomRadius, m_vertSegmentCount, m_horizSegmentCount, InsideOut);

      if (posBuilder is not null)
        positions = posBuilder.Claim(true);

      if (normalBuilder is not null)
        normals = normalBuilder.Claim(true);

      if (texCoordBuilder is not null)
        textureCoordinates = texCoordBuilder.Claim(true);

      indices = indexBuilder.Claim(true);
    }

    private static void GenerateData(DataBufferBuilder<VertexPositionNormalTexture> vertices, IndexDataBuilder indices, in Segment centerLine, float topRadius, float bottomRadius, int vertSegmentCount, int horizSegmentCount, bool flip)
    {
      vertices.Position = 0;
      indices.Position = 0;

      Triad axes = Triad.FromZComplementBasis(-centerLine.Direction); //Cylinder slices exist on the XY plane with Z being the vertical axis

      float sliceAmt = MathHelper.Pi / horizSegmentCount;

      //Create rings of vertices at progressively higher elevations
      for (int i = 0; i <= vertSegmentCount; i++)
      {
        float elevationPercent = (float) i / vertSegmentCount;
        float v = 1.0f - elevationPercent;
        float radius = MathHelper.Lerp(bottomRadius, topRadius, elevationPercent);

        Vector3 center;
        Vector3.Lerp(centerLine.StartPoint, centerLine.EndPoint, elevationPercent, out center);

        //Create a single ring of vertices at this elevation
        for (int j = 0; j <= horizSegmentCount; j++)
        {
          float u = (float) j / horizSegmentCount;

          Vector3 normal;
          GetCylinderNormal(sliceAmt, j, axes, out normal);

          VertexPositionNormalTexture vert;

          Vector3.Multiply(normal, radius, out vert.Position);
          Vector3.Add(vert.Position, center, out vert.Position);

          if (flip)
            normal.Negate();

          vert.Normal = normal;
          vert.TextureCoordinate = new Vector2(u, v);

          vertices.Set(vert);
        }
      }

      //Generate indices
      int stride = horizSegmentCount + 1;

      if (flip)
      {
        for (int i = 0; i < vertSegmentCount; i++)
        {
          for (int j = 0; j < horizSegmentCount; j++)
          {
            int index = (stride * i) + j;

            indices.Set(index + 1);
            indices.Set(index + stride + 1);
            indices.Set(index + stride);

            indices.Set(index + 1);
            indices.Set(index + stride);
            indices.Set(index);
          }
        }
      }
      else
      {
        for (int i = 0; i < vertSegmentCount; i++)
        {
          for (int j = 0; j < horizSegmentCount; j++)
          {
            int index = (stride * i) + j;

            indices.Set(index + stride);
            indices.Set(index + stride + 1);
            indices.Set(index + 1);

            indices.Set(index);
            indices.Set(index + stride);
            indices.Set(index + 1);
          }
        }
      }

      if (!MathHelper.IsNearlyZero(bottomRadius))
        GenerateCylinderCap(vertices, indices, axes, centerLine.StartPoint, bottomRadius, true, horizSegmentCount, flip);

      if (!MathHelper.IsNearlyZero(topRadius))
        GenerateCylinderCap(vertices, indices, axes, centerLine.EndPoint, topRadius, false, horizSegmentCount, flip);
    }

    private static void GenerateData(DataBufferBuilder<Vector3>? positions, DataBufferBuilder<Vector3>? normals, DataBufferBuilder<Vector2>? textureCoordinates, IndexDataBuilder indices, in Segment centerLine, float topRadius, float bottomRadius, int vertSegmentCount, int horizSegmentCount, bool flip)
    {
      if (positions is not null)
        positions.Position = 0;

      if (normals is not null)
        normals.Position = 0;

      if (textureCoordinates is not null)
        textureCoordinates.Position = 0;

      indices.Position = 0;

      Triad axes = Triad.FromZComplementBasis(-centerLine.Direction); //Cylinder slices exist on the XY plane with Z being the vertical axis

      float sliceAmt = MathHelper.Pi / horizSegmentCount;

      //Create rings of vertices at progressively higher elevations
      for (int i = 0; i <= vertSegmentCount; i++)
      {
        float elevationPercent = (float) i / vertSegmentCount;
        float v = 1.0f - elevationPercent;
        float radius = MathHelper.Lerp(bottomRadius, topRadius, elevationPercent);

        Vector3 center;
        Vector3.Lerp(centerLine.StartPoint, centerLine.EndPoint, elevationPercent, out center);

        //Create a single ring of vertices at this elevation
        for (int j = 0; j <= horizSegmentCount; j++)
        {
          float u = (float) j / horizSegmentCount;

          Vector3 normal;
          GetCylinderNormal(sliceAmt, j, axes, out normal);

          if (positions is not null)
          {
            Vector3 position;
            Vector3.Multiply(normal, radius, out position);
            Vector3.Add(position, center, out position);

            positions.Set(position);
          }

          if (normals is not null)
          {
            if (flip)
              normal.Negate();

            normals.Set(normal);
          }

          if (textureCoordinates is not null)
          {
            Vector2 texCoords = new Vector2(u, v);
            textureCoordinates.Set(texCoords);
          }
        }
      }

      //Generate indices
      int stride = horizSegmentCount + 1;

      if (flip)
      {
        for (int i = 0; i < vertSegmentCount; i++)
        {
          for (int j = 0; j < horizSegmentCount; j++)
          {
            int index = (stride * i) + j;

            indices.Set(index + 1);
            indices.Set(index + stride + 1);
            indices.Set(index + stride);

            indices.Set(index + 1);
            indices.Set(index + stride);
            indices.Set(index);
          }
        }
      }
      else
      {
        for (int i = 0; i < vertSegmentCount; i++)
        {
          for (int j = 0; j < horizSegmentCount; j++)
          {
            int index = (stride * i) + j;

            indices.Set(index + stride);
            indices.Set(index + stride + 1);
            indices.Set(index + 1);

            indices.Set(index);
            indices.Set(index + stride);
            indices.Set(index + 1);
          }
        }
      }

      if (!MathHelper.IsNearlyZero(bottomRadius))
        GenerateCylinderCap(positions, normals, textureCoordinates, indices, axes, centerLine.StartPoint, bottomRadius, true, horizSegmentCount, flip);

      if (!MathHelper.IsNearlyZero(topRadius))
        GenerateCylinderCap(positions, normals, textureCoordinates, indices, axes, centerLine.EndPoint, topRadius, false, horizSegmentCount, flip);
    }

    private static void GetCylinderNormal(float sliceAmt, int slice, in Triad axes, out Vector3 normal)
    {
      Angle angle = Angle.FromRadians(slice * 2.0f * sliceAmt);
      Vector3 dx = axes.XAxis;
      Vector3 dy = axes.YAxis;

      Vector3.Multiply(dx, angle.Sin, out dx);
      Vector3.Multiply(dy, angle.Cos, out dy);

      Vector3.Add(dx, dy, out normal);
      normal.Normalize();
    }

    private static void GenerateCylinderCap(DataBufferBuilder<VertexPositionNormalTexture> vertices, IndexDataBuilder indices, in Triad axes, in Vector3 center, float radius, bool isBottom, int tessellation, bool flip)
    {
      Vector3 normal = (flip) ? axes.ZAxis : -axes.ZAxis;
      Vector2 texScale = new Vector2(0.5f);

      if (isBottom)
        normal.Negate();

      int vBaseIndex = vertices.Position;
      int stride = tessellation + 1;

      //Create cap indices
      for (int i = 0; i < tessellation; i++)
      {
        int index0 = i + 1;
        int index1 = Math.Max(1, (i + 2) % stride); //Wrap around, but to 1 not zero

        if (flip)
        {
          if (!isBottom)
          {
            indices.Set(vBaseIndex + index0);
            indices.Set(vBaseIndex + index1);
            indices.Set(vBaseIndex);
          }
          else
          {
            indices.Set(vBaseIndex);
            indices.Set(vBaseIndex + index1);
            indices.Set(vBaseIndex + index0);
          }
        }
        else
        {
          if (isBottom)
          {
            indices.Set(vBaseIndex + index0);
            indices.Set(vBaseIndex + index1);
            indices.Set(vBaseIndex);
          }
          else
          {
            indices.Set(vBaseIndex);
            indices.Set(vBaseIndex + index1);
            indices.Set(vBaseIndex + index0);
          }
        }
      }

      //Create center vertex
      VertexPositionNormalTexture vert = new VertexPositionNormalTexture(center, normal, texScale);
      vertices.Set(vert);

      float sliceAmt = MathHelper.Pi / tessellation;

      //Create cap vertices
      for (int i = 0; i < tessellation; i++)
      {
        Vector3 sideNormal;
        GetCylinderNormal(sliceAmt, i, axes, out sideNormal);

        Vector3.Multiply(sideNormal, radius, out vert.Position);
        Vector3.Add(vert.Position, center, out vert.Position);

        vert.Normal = normal;
        vert.TextureCoordinate = new Vector2(sideNormal.X * texScale.X + 0.5f, sideNormal.Z * texScale.Y + 0.5f);

        vertices.Set(vert);
      }
    }

    private static void GenerateCylinderCap(DataBufferBuilder<Vector3>? positions, DataBufferBuilder<Vector3>? normals, DataBufferBuilder<Vector2>? textureCoordinates, IndexDataBuilder indices, in Triad axes, in Vector3 center, float radius, bool isBottom, int tessellation, bool flip)
    {
      Vector3 normal = (flip) ? axes.ZAxis : -axes.ZAxis;
      Vector2 texScale = new Vector2(0.5f);

      if (isBottom)
        normal.Negate();

      int vBaseIndex = GetBaseVertexIndex(positions, normals, textureCoordinates);
      int stride = tessellation + 1;

      //Create cap indices
      for (int i = 0; i < tessellation; i++)
      {
        int index0 = i + 1;
        int index1 = Math.Max(1, (i + 2) % stride); //Wrap around, but to 1 not zero

        if (flip)
        {
          if (!isBottom)
          {
            indices.Set(vBaseIndex + index0);
            indices.Set(vBaseIndex + index1);
            indices.Set(vBaseIndex);
          }
          else
          {
            indices.Set(vBaseIndex);
            indices.Set(vBaseIndex + index1);
            indices.Set(vBaseIndex + index0);
          }
        }
        else
        {
          if (isBottom)
          {
            indices.Set(vBaseIndex + index0);
            indices.Set(vBaseIndex + index1);
            indices.Set(vBaseIndex);
          }
          else
          {
            indices.Set(vBaseIndex);
            indices.Set(vBaseIndex + index1);
            indices.Set(vBaseIndex + index0);
          }
        }
      }

      //Create center vertex
      if (normals is not null)
        normals.Set(normal);

      if (positions is not null)
        positions.Set(center);

      if (textureCoordinates is not null)
        textureCoordinates.Set(texScale);

      float sliceAmt = MathHelper.Pi / tessellation;

      //Create cap vertices
      for (int i = 0; i < tessellation; i++)
      {
        Vector3 sideNormal;
        GetCylinderNormal(sliceAmt, i, axes, out sideNormal);

        if (normals is not null)
          normals.Set(normal);

        if (positions is not null)
        {
          Vector3 position;
          Vector3.Multiply(sideNormal, radius, out position);
          Vector3.Add(position, center, out position);

          positions.Set(position);
        }

        if (textureCoordinates is not null)
        {
          Vector2 texCoords = new Vector2(sideNormal.X * texScale.X + 0.5f, sideNormal.Z * texScale.Y + 0.5f);
          textureCoordinates.Set(texCoords);
        }
      }
    }
  }
}
