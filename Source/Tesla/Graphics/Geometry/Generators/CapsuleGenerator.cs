﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;

#nullable enable

namespace Tesla.Graphics
{
  public sealed class CapsuleGenerator : GeometryGenerator
  {
    private Segment m_centerLine;
    private float m_radius;
    private int m_sphereTessellation;
    private int m_vertSegmentCount;
    private int m_horizSegmentCount;
    private int m_sphereVertSegmentCount;
    private int m_sphereHorizSegmentCount;

    public Segment CenterLine
    {
      get
      {
        return m_centerLine;
      }
      set
      {
        m_centerLine = value;
      }
    }

    public float Radius
    {
      get
      {
        return m_radius;
      }
      set
      {
        m_radius = value;
      }
    }

    public int SphereTessellation
    {
      get
      {
        return m_sphereTessellation;
      }
      set
      {
        m_sphereTessellation = (value < 3) ? 3 : value;

        m_sphereVertSegmentCount = m_sphereTessellation; //same as sphere vert segment count
        m_sphereHorizSegmentCount = m_sphereTessellation * 4; //2x sphere horiz segment count
      }
    }

    public int VerticalSegmentCount
    {
      get
      {
        return m_vertSegmentCount;
      }
      set
      {
        m_vertSegmentCount = (value < 1) ? 1 : value;
      }
    }

    public int HorizontalSegmentCount
    {
      get
      {
        return m_horizSegmentCount;
      }
      set
      {
        m_horizSegmentCount = (value < 3) ? 3 : value;
      }
    }

    public CapsuleGenerator() : this(new Segment(new Vector3(0.0f, -1.0f, 0.0f), new Vector3(0.0f, 1.0f, 0.0f)), 0.5f, 8, false) { }

    public CapsuleGenerator(in Segment centerLine, float radius) : this(centerLine, radius, 8, false) { }

    public CapsuleGenerator(in Segment centerLine, float radius, int tessellation) : this(centerLine, radius, tessellation, false) { }

    public CapsuleGenerator(in Segment centerLine, float radius, int tessellation, bool insideOut)
    {
      m_centerLine = centerLine;
      m_radius = radius;
      SetTessellation(tessellation);
      InsideOut = insideOut;
    }

    public void SetTessellation(int tessellation)
    {
      tessellation = (tessellation < 3) ? 3 : tessellation;

      VerticalSegmentCount = tessellation * 2;
      HorizontalSegmentCount = tessellation * 4;
      SphereTessellation = tessellation;
    }

    public override void CalculateVertexIndexCounts(out int vertexCount, out int indexCount)
    {
      vertexCount = ((m_sphereVertSegmentCount + 1) * (m_sphereHorizSegmentCount + 1) * 2) //2 half spheres
          + ((m_vertSegmentCount + 1) * (m_horizSegmentCount + 1)); //1 cylinder without caps

      indexCount = (m_sphereVertSegmentCount * (m_sphereHorizSegmentCount + 1) * 12) //2 half spheres
          + (m_vertSegmentCount * m_horizSegmentCount * 6); //1 cylinder without caps
    }

    public override void Build([AllowNull] ref DataBuffer<VertexPositionNormalTexture> vertices, ref IndexData indices)
    {
      int vertCount, indexCount;
      CalculateVertexIndexCounts(out vertCount, out indexCount);

      DataBufferBuilder<VertexPositionNormalTexture> vertexBuilder = CreateVertexBuilder<VertexPositionNormalTexture>(vertCount, vertices);
      IndexDataBuilder indexBuilder = CreateIndexBuilder(indexCount, indices);

      GenerateData(vertexBuilder, indexBuilder, m_centerLine, m_radius, m_sphereVertSegmentCount, m_sphereHorizSegmentCount, m_vertSegmentCount, m_horizSegmentCount, InsideOut);

      vertices = vertexBuilder.Claim(true);
      indices = indexBuilder.Claim(true);
    }

    public override void Build([AllowNull] ref DataBuffer<Vector3> positions, [AllowNull] ref DataBuffer<Vector3> normals, [AllowNull] ref DataBuffer<Vector2> textureCoordinates, ref IndexData indices, GenerateOptions options)
    {
      if (options == GenerateOptions.None)
        return;

      int vertCount, indexCount;
      CalculateVertexIndexCounts(out vertCount, out indexCount);

      DataBufferBuilder<Vector3>? posBuilder = null;
      DataBufferBuilder<Vector3>? normalBuilder = null;
      DataBufferBuilder<Vector2>? texCoordBuilder = null;
      IndexDataBuilder indexBuilder = CreateIndexBuilder(indexCount, indices);

      if ((options & GenerateOptions.Positions) == GenerateOptions.Positions)
        posBuilder = CreateVertexBuilder<Vector3>(vertCount, positions);

      if ((options & GenerateOptions.Normals) == GenerateOptions.Normals)
        normalBuilder = CreateVertexBuilder<Vector3>(vertCount, normals);

      if ((options & GenerateOptions.TextureCoordinates) == GenerateOptions.TextureCoordinates)
        texCoordBuilder = CreateVertexBuilder<Vector2>(vertCount, textureCoordinates);

      GenerateData(posBuilder, normalBuilder, texCoordBuilder, indexBuilder, m_centerLine, m_radius, m_sphereVertSegmentCount, m_sphereHorizSegmentCount, m_vertSegmentCount, m_horizSegmentCount, InsideOut);

      if (posBuilder is not null)
        positions = posBuilder.Claim(true);

      if (normalBuilder is not null)
        normals = normalBuilder.Claim(true);

      if (texCoordBuilder is not null)
        textureCoordinates = texCoordBuilder.Claim(true);

      indices = indexBuilder.Claim(true);
    }

    private static void GenerateData(DataBufferBuilder<VertexPositionNormalTexture> vertices, IndexDataBuilder indices, in Segment centerLine, float radius, int sphereVertSegmentCount, int sphereHorizSegmentCount, int vertSegmentCount, int horizSegmentCount, bool flip)
    {
      vertices.Position = 0;
      indices.Position = 0;

      //Make sure line isn't degenerate
      Vector3 topNormal = (centerLine.IsDegenerate) ? Vector3.UnitY : centerLine.Direction;
      Vector3 bottomNormal = topNormal;
      bottomNormal.Negate();

      GenerateHalfSphereData(vertices, indices, centerLine.StartPoint, bottomNormal, radius, sphereVertSegmentCount, sphereHorizSegmentCount, flip);
      GenerateCylinderData(vertices, indices, centerLine, radius, vertSegmentCount, horizSegmentCount, flip);
      GenerateHalfSphereData(vertices, indices, centerLine.EndPoint, topNormal, radius, sphereVertSegmentCount, sphereHorizSegmentCount, flip);
    }

    private static void GenerateData(DataBufferBuilder<Vector3>? positions, DataBufferBuilder<Vector3>? normals, DataBufferBuilder<Vector2>? textureCoordinates, IndexDataBuilder indices, in Segment centerLine, float radius, int sphereVertSegmentCount, int sphereHorizSegmentCount, int vertSegmentCount, int horizSegmentCount, bool flip)
    {
      if (positions is not null)
        positions.Position = 0;

      if (normals is not null)
        normals.Position = 0;

      if (textureCoordinates is not null)
        textureCoordinates.Position = 0;

      indices.Position = 0;

      //Make sure line isn't degenerate
      Vector3 topNormal = (centerLine.IsDegenerate) ? Vector3.UnitY : centerLine.Direction;
      Vector3 bottomNormal = topNormal;
      bottomNormal.Negate();

      GenerateHalfSphereData(positions, normals, textureCoordinates, indices, centerLine.StartPoint, bottomNormal, radius, sphereVertSegmentCount, sphereHorizSegmentCount, flip);
      GenerateCylinderData(positions, normals, textureCoordinates, indices, centerLine, radius, vertSegmentCount, horizSegmentCount, flip);
      GenerateHalfSphereData(positions, normals, textureCoordinates, indices, centerLine.EndPoint, topNormal, radius, sphereVertSegmentCount, sphereHorizSegmentCount, flip);
    }

    private static void GenerateHalfSphereData(DataBufferBuilder<VertexPositionNormalTexture> vertices, IndexDataBuilder indices, in Vector3 center, in Vector3 domeNormal, float radius, int vertSegmentCount, int horizSegmentCount, bool flip)
    {
      Matrix rot = Matrix.Identity;

      if (!domeNormal.Equals(Vector3.UnitY))
      {
        Triad axes;
        Triad.FromYComplementBasis(domeNormal, out axes);
        axes.XAxis.Negate();
        Matrix.FromAxes(axes.XAxis, axes.YAxis, axes.ZAxis, out rot);
      }

      int vBaseIndex = vertices.Position;

      //Create rings of vertices at progressively higher latitudes
      for (int i = 0; i <= vertSegmentCount; i++)
      {
        float v = 1.0f - ((float) i / vertSegmentCount);

        Angle latitude = Angle.FromRadians(((i * MathHelper.PiOverTwo) / vertSegmentCount));

        float dy = latitude.Sin;
        float dxz = latitude.Cos;

        //Create a single ring of vertices at this latitude
        for (int j = 0; j <= horizSegmentCount; j++)
        {
          float u = (float) j / horizSegmentCount;

          Angle longitude = Angle.FromRadians((j * 2.0f * MathHelper.Pi) / horizSegmentCount);

          float dx = longitude.Sin;
          float dz = longitude.Cos;

          dx *= dxz;
          dz *= dxz;

          Vector3 normal = new Vector3(dx, dy, dz);
          Vector3.TransformNormal(normal, rot, out normal);

          Vector2 texCoords = new Vector2(u, v);

          Vector3 pos;
          Vector3.Multiply(normal, radius, out pos);
          Vector3.Add(pos, center, out pos);

          if (flip)
            normal.Negate();

          VertexPositionNormalTexture vert = new VertexPositionNormalTexture(pos, normal, texCoords);
          vertices.Set(vert);
        }
      }

      //Generate indices
      int stride = horizSegmentCount + 1;

      if (flip)
      {
        for (int i = 0; i < vertSegmentCount; i++)
        {
          for (int j = 0; j <= horizSegmentCount; j++)
          {
            int nextI = i + 1;
            int nextJ = (j + 1) % stride;

            indices.Set(vBaseIndex + (i * stride) + nextJ);
            indices.Set(vBaseIndex + (nextI * stride) + j);
            indices.Set(vBaseIndex + (i * stride) + j);

            indices.Set(vBaseIndex + (nextI * stride) + nextJ);
            indices.Set(vBaseIndex + (nextI * stride) + j);
            indices.Set(vBaseIndex + (i * stride) + nextJ);
          }
        }
      }
      else
      {
        for (int i = 0; i < vertSegmentCount; i++)
        {
          for (int j = 0; j <= horizSegmentCount; j++)
          {
            int nextI = i + 1;
            int nextJ = (j + 1) % stride;

            indices.Set(vBaseIndex + (i * stride) + j);
            indices.Set(vBaseIndex + (nextI * stride) + j);
            indices.Set(vBaseIndex + (i * stride) + nextJ);

            indices.Set(vBaseIndex + (i * stride) + nextJ);
            indices.Set(vBaseIndex + (nextI * stride) + j);
            indices.Set(vBaseIndex + (nextI * stride) + nextJ);
          }
        }
      }
    }

    private static void GenerateHalfSphereData(DataBufferBuilder<Vector3>? positions, DataBufferBuilder<Vector3>? normals, DataBufferBuilder<Vector2>? textureCoordinates, IndexDataBuilder indices, in Vector3 center, in Vector3 domeNormal, float radius, int vertSegmentCount, int horizSegmentCount, bool flip)
    {
      Matrix rot = Matrix.Identity;

      if (!domeNormal.Equals(Vector3.UnitY))
      {
        Triad axes;
        Triad.FromYComplementBasis(domeNormal, out axes);
        axes.XAxis.Negate();
        Matrix.FromAxes(axes.XAxis, axes.YAxis, axes.ZAxis, out rot);
      }

      int vBaseIndex = GetBaseVertexIndex(positions, normals, textureCoordinates);

      //Create rings of vertices at progressively higher latitudes
      for (int i = 0; i <= vertSegmentCount; i++)
      {
        float v = 1.0f - ((float) i / vertSegmentCount);

        Angle latitude = Angle.FromRadians(((i * MathHelper.PiOverTwo) / vertSegmentCount));

        float dy = latitude.Sin;
        float dxz = latitude.Cos;

        //Create a single ring of vertices at this latitude
        for (int j = 0; j <= horizSegmentCount; j++)
        {
          float u = (float) j / horizSegmentCount;

          Angle longitude = Angle.FromRadians((j * 2.0f * MathHelper.Pi) / horizSegmentCount);

          float dx = longitude.Sin;
          float dz = longitude.Cos;

          dx *= dxz;
          dz *= dxz;

          Vector3 normal = new Vector3(dx, dy, dz);
          Vector3.TransformNormal(normal, rot, out normal);

          Vector2 texCoords = new Vector2(u, v);

          Vector3 pos;
          Vector3.Multiply(normal, radius, out pos);
          Vector3.Add(pos, center, out pos);

          if (flip)
            normal.Negate();

          if (positions is not null)
            positions.Set(pos);

          if (normals is not null)
            normals.Set(normal);

          if (textureCoordinates is not null)
            textureCoordinates.Set(texCoords);
        }
      }

      //Generate indices
      int stride = horizSegmentCount + 1;

      if (flip)
      {
        for (int i = 0; i < vertSegmentCount; i++)
        {
          for (int j = 0; j <= horizSegmentCount; j++)
          {
            int nextI = i + 1;
            int nextJ = (j + 1) % stride;

            indices.Set(vBaseIndex + (i * stride) + nextJ);
            indices.Set(vBaseIndex + (nextI * stride) + j);
            indices.Set(vBaseIndex + (i * stride) + j);

            indices.Set(vBaseIndex + (nextI * stride) + nextJ);
            indices.Set(vBaseIndex + (nextI * stride) + j);
            indices.Set(vBaseIndex + (i * stride) + nextJ);
          }
        }
      }
      else
      {
        for (int i = 0; i < vertSegmentCount; i++)
        {
          for (int j = 0; j <= horizSegmentCount; j++)
          {
            int nextI = i + 1;
            int nextJ = (j + 1) % stride;

            indices.Set(vBaseIndex + (i * stride) + j);
            indices.Set(vBaseIndex + (nextI * stride) + j);
            indices.Set(vBaseIndex + (i * stride) + nextJ);

            indices.Set(vBaseIndex + (i * stride) + nextJ);
            indices.Set(vBaseIndex + (nextI * stride) + j);
            indices.Set(vBaseIndex + (nextI * stride) + nextJ);
          }
        }
      }
    }

    private static void GenerateCylinderData(DataBufferBuilder<VertexPositionNormalTexture> vertices, IndexDataBuilder indices, in Segment centerLine, float radius, int vertSegmentCount, int horizSegmentCount, bool flip)
    {
      Triad axes = Triad.FromZComplementBasis(-centerLine.Direction); //Cylinder slices exist on the XY plane with Z being the vertical axis

      float sliceAmt = MathHelper.Pi / horizSegmentCount;
      int vBaseIndex = vertices.Position;

      //Create rings of vertices at progressively higher elevations
      for (int i = 0; i <= vertSegmentCount; i++)
      {
        float elevationPercent = (float) i / vertSegmentCount;
        float v = 1.0f - elevationPercent;

        Vector3 center;
        Vector3.Lerp(centerLine.StartPoint, centerLine.EndPoint, elevationPercent, out center);

        //Create a single ring of vertices at this elevation
        for (int j = 0; j <= horizSegmentCount; j++)
        {
          float u = (float) j / horizSegmentCount;

          Vector3 normal;
          GetCylinderNormal(sliceAmt, j, axes, out normal);

          VertexPositionNormalTexture vert;

          Vector3.Multiply(normal, radius, out vert.Position);
          Vector3.Add(vert.Position, center, out vert.Position);

          if (flip)
            normal.Negate();

          vert.Normal = normal;
          vert.TextureCoordinate = new Vector2(u, v);

          vertices.Set(vert);
        }
      }

      //Generate indices
      int stride = horizSegmentCount + 1;

      if (flip)
      {
        for (int i = 0; i < vertSegmentCount; i++)
        {
          for (int j = 0; j < horizSegmentCount; j++)
          {
            int index = (stride * i) + j;

            indices.Set(vBaseIndex + index + 1);
            indices.Set(vBaseIndex + index + stride + 1);
            indices.Set(vBaseIndex + index + stride);

            indices.Set(vBaseIndex + index + 1);
            indices.Set(vBaseIndex + index + stride);
            indices.Set(vBaseIndex + index);
          }
        }
      }
      else
      {
        for (int i = 0; i < vertSegmentCount; i++)
        {
          for (int j = 0; j < horizSegmentCount; j++)
          {
            int index = (stride * i) + j;

            indices.Set(vBaseIndex + index + stride);
            indices.Set(vBaseIndex + index + stride + 1);
            indices.Set(vBaseIndex + index + 1);

            indices.Set(vBaseIndex + index);
            indices.Set(vBaseIndex + index + stride);
            indices.Set(vBaseIndex + index + 1);
          }
        }
      }
    }

    private static void GenerateCylinderData(DataBufferBuilder<Vector3>? positions, DataBufferBuilder<Vector3>? normals, DataBufferBuilder<Vector2>? textureCoordinates, IndexDataBuilder indices, in Segment centerLine, float radius, int vertSegmentCount, int horizSegmentCount, bool flip)
    {
      Triad axes = Triad.FromZComplementBasis(-centerLine.Direction); //Cylinder slices exist on the XY plane with Z being the vertical axis

      float sliceAmt = MathHelper.Pi / horizSegmentCount;
      int vBaseIndex = GetBaseVertexIndex(positions, normals, textureCoordinates);

      //Create rings of vertices at progressively higher elevations
      for (int i = 0; i <= vertSegmentCount; i++)
      {
        float elevationPercent = (float) i / vertSegmentCount;
        float v = 1.0f - elevationPercent;

        Vector3 center;
        Vector3.Lerp(centerLine.StartPoint, centerLine.EndPoint, elevationPercent, out center);

        //Create a single ring of vertices at this elevation
        for (int j = 0; j <= horizSegmentCount; j++)
        {
          float u = (float) j / horizSegmentCount;

          Vector3 normal;
          GetCylinderNormal(sliceAmt, j, axes, out normal);

          if (positions is not null)
          {
            Vector3 position;
            Vector3.Multiply(normal, radius, out position);
            Vector3.Add(position, center, out position);

            positions.Set(position);
          }

          if (normals is not null)
          {
            if (flip)
              normal.Negate();

            normals.Set(normal);
          }

          if (textureCoordinates is not null)
          {
            Vector2 texCoords = new Vector2(u, v);
            textureCoordinates.Set(texCoords);
          }
        }
      }

      //Generate indices
      int stride = horizSegmentCount + 1;

      if (flip)
      {
        for (int i = 0; i < vertSegmentCount; i++)
        {
          for (int j = 0; j < horizSegmentCount; j++)
          {
            int index = (stride * i) + j;

            indices.Set(vBaseIndex + index + 1);
            indices.Set(vBaseIndex + index + stride + 1);
            indices.Set(vBaseIndex + index + stride);

            indices.Set(vBaseIndex + index + 1);
            indices.Set(vBaseIndex + index + stride);
            indices.Set(vBaseIndex + index);
          }
        }
      }
      else
      {
        for (int i = 0; i < vertSegmentCount; i++)
        {
          for (int j = 0; j < horizSegmentCount; j++)
          {
            int index = (stride * i) + j;

            indices.Set(vBaseIndex + index + stride);
            indices.Set(vBaseIndex + index + stride + 1);
            indices.Set(vBaseIndex + index + 1);

            indices.Set(vBaseIndex + index);
            indices.Set(vBaseIndex + index + stride);
            indices.Set(vBaseIndex + index + 1);
          }
        }
      }
    }

    private static void GetCylinderNormal(float sliceAmt, int slice, in Triad axes, out Vector3 normal)
    {
      Angle angle = Angle.FromRadians(slice * 2.0f * sliceAmt);
      Vector3 dx = axes.XAxis;
      Vector3 dy = axes.YAxis;

      Vector3.Multiply(dx, angle.Sin, out dx);
      Vector3.Multiply(dy, angle.Cos, out dy);

      Vector3.Add(dx, dy, out normal);
      normal.Normalize();
    }
  }
}
