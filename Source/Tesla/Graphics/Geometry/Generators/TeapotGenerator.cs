﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;

#nullable enable

namespace Tesla.Graphics
{
  public sealed class TeapotGenerator : GeometryGenerator
  {
    private Vector3 m_center;
    private float m_size;
    private int m_tessellation;

    private readonly Vector3[] m_controlPoints = new Vector3[16];

    public Vector3 Center
    {
      get
      {
        return m_center;
      }
      set
      {
        m_center = value;
      }
    }

    public float Size
    {
      get
      {
        return m_size;
      }
      set
      {
        m_size = value;
      }
    }

    public int Tessellation
    {
      get
      {
        return m_tessellation;
      }
      set
      {
        m_tessellation = (value < 1) ? 1 : value;
      }
    }

    public TeapotGenerator() : this(Vector3.Zero, 1.0f, 8, false) { }

    public TeapotGenerator(float size) : this(Vector3.Zero, size, 8, false) { }

    public TeapotGenerator(float size, int tessellation) : this(Vector3.Zero, size, tessellation, false) { }

    public TeapotGenerator(in Vector3 center) : this(Vector3.Zero, 1.0f, 8, false) { }

    public TeapotGenerator(in Vector3 center, float size) : this(center, size, 8, false) { }

    public TeapotGenerator(in Vector3 center, float size, int tessellation) : this(center, size, tessellation, false) { }

    public TeapotGenerator(in Vector3 center, float size, int tessellation, bool insideOut)
    {
      m_center = center;
      m_size = size;
      Tessellation = tessellation;
      InsideOut = insideOut;
    }

    public override void CalculateVertexIndexCounts(out int vertexCount, out int indexCount)
    {
      vertexCount = (m_tessellation + 1) * (m_tessellation + 1) * NumberTessellatePatchCalled;
      indexCount = m_tessellation * m_tessellation * 6 * NumberTessellatePatchCalled;
    }

    public override void Build([AllowNull] ref DataBuffer<VertexPositionNormalTexture> vertices, ref IndexData indices)
    {
      int vertCount, indexCount;
      CalculateVertexIndexCounts(out vertCount, out indexCount);

      DataBufferBuilder<VertexPositionNormalTexture> vertexBuilder = CreateVertexBuilder<VertexPositionNormalTexture>(vertCount, vertices);
      IndexDataBuilder indexBuilder = CreateIndexBuilder(indexCount, indices);

      GenerateData(vertexBuilder, indexBuilder, m_center, m_size, m_tessellation, m_controlPoints, InsideOut);

      vertices = vertexBuilder.Claim(true);
      indices = indexBuilder.Claim(true);
    }

    public override void Build([AllowNull] ref DataBuffer<Vector3> positions, [AllowNull] ref DataBuffer<Vector3> normals, [AllowNull] ref DataBuffer<Vector2> textureCoordinates, ref IndexData indices, GenerateOptions options)
    {
      if (options == GenerateOptions.None)
        return;

      int vertCount, indexCount;
      CalculateVertexIndexCounts(out vertCount, out indexCount);

      DataBufferBuilder<Vector3>? posBuilder = null;
      DataBufferBuilder<Vector3>? normalBuilder = null;
      DataBufferBuilder<Vector2>? texCoordBuilder = null;
      IndexDataBuilder indexBuilder = CreateIndexBuilder(indexCount, indices);

      if ((options & GenerateOptions.Positions) == GenerateOptions.Positions)
        posBuilder = CreateVertexBuilder<Vector3>(vertCount, positions);

      if ((options & GenerateOptions.Normals) == GenerateOptions.Normals)
        normalBuilder = CreateVertexBuilder<Vector3>(vertCount, normals);

      if ((options & GenerateOptions.TextureCoordinates) == GenerateOptions.TextureCoordinates)
        texCoordBuilder = CreateVertexBuilder<Vector2>(vertCount, textureCoordinates);

      GenerateData(posBuilder, normalBuilder, texCoordBuilder, indexBuilder, m_center, m_size, m_tessellation, m_controlPoints, InsideOut);

      if (posBuilder is not null)
        positions = posBuilder.Claim(true);

      if (normalBuilder is not null)
        normals = normalBuilder.Claim(true);

      if (texCoordBuilder is not null)
        textureCoordinates = texCoordBuilder.Claim(true);

      indices = indexBuilder.Claim(true);
    }

    private static void GenerateData(DataBufferBuilder<VertexPositionNormalTexture> vertices, IndexDataBuilder indices, in Vector3 center, float size, int tessellation, Vector3[] controlPointsTemp, bool flip)
    {
      vertices.Position = 0;
      indices.Position = 0;

      Vector3 scale = new Vector3(size, size, size);
      Vector3 scaleNegX = new Vector3(-size, size, size);
      Vector3 scaleNegZ = new Vector3(size, size, -size);
      Vector3 scaleNegXZ = new Vector3(-size, size, -size);

      //Data and algorithm largely from and inspired by DirectXTK's Teapot generator
      for (int i = 0; i < TeapotPatches.Length; i++)
      {
        PatchData patch = TeapotPatches[i];

        //Teapot is symmetrical left to right so we only store one side, tessellating it twice and mirroring one along X axis
        TessellatePatch(vertices, indices, patch, center, scale, tessellation, controlPointsTemp, false, flip);
        TessellatePatch(vertices, indices, patch, center, scaleNegX, tessellation, controlPointsTemp, true, flip);

        if (patch.MirrorZ)
        {
          //Some parts (body, lid and rim) are symmetric from front to back, tessellate twice and mirroing along z and x
          TessellatePatch(vertices, indices, patch, center, scaleNegZ, tessellation, controlPointsTemp, true, flip);
          TessellatePatch(vertices, indices, patch, center, scaleNegXZ, tessellation, controlPointsTemp, false, flip);
        }
      }
    }

    private static void GenerateData(DataBufferBuilder<Vector3>? positions, DataBufferBuilder<Vector3>? normals, DataBufferBuilder<Vector2>? textureCoordinates, IndexDataBuilder indices, in Vector3 center, float size, int tessellation, Vector3[] controlPointsTemp, bool flip)
    {
      if (positions is not null)
        positions.Position = 0;

      if (normals is not null)
        normals.Position = 0;

      if (textureCoordinates is not null)
        textureCoordinates.Position = 0;

      indices.Position = 0;

      Vector3 scale = new Vector3(size, size, size);
      Vector3 scaleNegX = new Vector3(-size, size, size);
      Vector3 scaleNegZ = new Vector3(size, size, -size);
      Vector3 scaleNegXZ = new Vector3(-size, size, -size);

      //Data and algorithm largely from and inspired by DirectXTK's Teapot generator
      for (int i = 0; i < TeapotPatches.Length; i++)
      {
        PatchData patch = TeapotPatches[i];

        //Teapot is symmetrical left to right so we only store one side, tessellating it twice and mirror one along X axis
        TessellatePatch(positions, normals, textureCoordinates, indices, patch, center, ref scale, tessellation, controlPointsTemp, false, flip);
        TessellatePatch(positions, normals, textureCoordinates, indices, patch, center, ref scaleNegX, tessellation, controlPointsTemp, true, flip);

        if (patch.MirrorZ)
        {
          //Some parts (body, lid and rim) are symmetric from front to back, tessellate twice and mirror along z and x
          TessellatePatch(positions, normals, textureCoordinates, indices, patch, center, ref scaleNegZ, tessellation, controlPointsTemp, true, flip);
          TessellatePatch(positions, normals, textureCoordinates, indices, patch, center, ref scaleNegXZ, tessellation, controlPointsTemp, false, flip);
        }
      }
    }

    private static void TessellatePatch(DataBufferBuilder<VertexPositionNormalTexture> vertices, IndexDataBuilder indices, in PatchData patch, in Vector3 center, in Vector3 scale, int tessellation, Vector3[] controlPointsTemp, bool mirror, bool flip)
    {
      //Look up the 16 control points and scale them if necessary
      for (int i = 0; i < 16; i++)
      {
        ref readonly Vector3 controlPoint = ref TeapotControlPoints[patch.Indices[i]];
        Vector3.Multiply(controlPoint, scale, out controlPointsTemp[i]);
      }

      //Create the index data
      CreatePatchIndices(indices, tessellation, vertices.Position, mirror, flip);

      //Create the vertex data
      CreatePatchVertices(vertices, controlPointsTemp, center, tessellation, mirror, flip);
    }

    private static void TessellatePatch(DataBufferBuilder<Vector3>? positions, DataBufferBuilder<Vector3>? normals, DataBufferBuilder<Vector2>? textureCoordinates, IndexDataBuilder indices, in PatchData patch, in Vector3 center, ref Vector3 scale, int tessellation, Span<Vector3> controlPointsTemp, bool mirror, bool flip)
    {
      //Look up the 16 control points and scale them if necessary
      for (int i = 0; i < 16; i++)
      {
        ref readonly Vector3 controlPoint = ref TeapotControlPoints[patch.Indices[i]];
        Vector3.Multiply(controlPoint, scale, out controlPointsTemp[i]);
      }

      //Create the index data
      CreatePatchIndices(indices, tessellation, GetBaseVertexIndex(positions, normals, textureCoordinates), mirror, flip);

      //Create the vertex data
      CreatePatchVertices(positions, normals, textureCoordinates, controlPointsTemp, center, tessellation, mirror, flip);
    }

    //Creates tessellated vertices for the specified patch control points
    private static void CreatePatchVertices(DataBufferBuilder<VertexPositionNormalTexture> vertices, ReadOnlySpan<Vector3> patchControlPoints, in Vector3 center, int tessellation, bool mirror, bool flip)
    {
      for (int i = 0; i <= tessellation; i++)
      {
        float u = (float) i / tessellation;

        for (int j = 0; j <= tessellation; j++)
        {
          float v = (float) j / tessellation;

          //Perform four horizontal bezier interpolations
          Vector3 p1, p2, p3, p4;

          CubicInterpolate(patchControlPoints[0], patchControlPoints[1], patchControlPoints[2], patchControlPoints[3], u, out p1);
          CubicInterpolate(patchControlPoints[4], patchControlPoints[5], patchControlPoints[6], patchControlPoints[7], u, out p2);
          CubicInterpolate(patchControlPoints[8], patchControlPoints[9], patchControlPoints[10], patchControlPoints[11], u, out p3);
          CubicInterpolate(patchControlPoints[12], patchControlPoints[13], patchControlPoints[14], patchControlPoints[15], u, out p4);

          //Perform a vertical interpolation
          Vector3 position;
          CubicInterpolate(p1, p2, p3, p4, v, out position);

          //Perform four vertical bezier interpolations
          Vector3 q1, q2, q3, q4;

          CubicInterpolate(patchControlPoints[0], patchControlPoints[4], patchControlPoints[8], patchControlPoints[12], v, out q1);
          CubicInterpolate(patchControlPoints[1], patchControlPoints[5], patchControlPoints[9], patchControlPoints[13], v, out q2);
          CubicInterpolate(patchControlPoints[2], patchControlPoints[6], patchControlPoints[10], patchControlPoints[14], v, out q3);
          CubicInterpolate(patchControlPoints[3], patchControlPoints[7], patchControlPoints[11], patchControlPoints[15], v, out q4);

          //Compute vertical and horizontal tangent vectors
          Vector3 vertTan, horizTan;
          CubicTangent(p1, p2, p3, p4, v, out vertTan);
          CubicTangent(q1, q2, q3, q4, u, out horizTan);

          //Cross the tangents to compute the normal
          Vector3 normal;
          Vector3.Cross(vertTan, horizTan, out normal);

          if (!normal.IsAlmostZero())
          {
            normal.Normalize();

            if ((mirror && !flip) || (!mirror && flip))
              normal.Negate();
          }
          else
          {
            //Classic teapot model contains degenerate geometry where a patch has several control points in the same place, causing tangent computation to 
            //produce a zero normal. Fix this with a hardcoded up/down normal depending on if we're on the top or bottom of the teapot
            normal.X = 0.0f;
            normal.Y = (position.Y < 0.0f) ? -1.0f : 1.0f;
            normal.Z = 0.0f;
          }

          //Translate position
          Vector3.Add(position, center, out position);

          //Compute the texture coordinates
          Vector2 texCoords = new Vector2((mirror) ? (1 - u) : u, v);

          VertexPositionNormalTexture vertex = new VertexPositionNormalTexture(position, normal, texCoords);
          vertices.Set(vertex);
        }
      }
    }

    //Creates tessellated vertices for the specified patch control points
    private static void CreatePatchVertices(DataBufferBuilder<Vector3>? positions, DataBufferBuilder<Vector3>? normals, DataBufferBuilder<Vector2>? textureCoordinates, ReadOnlySpan<Vector3> patchControlPoints, in Vector3 center, int tessellation, bool mirror, bool flip)
    {
      for (int i = 0; i <= tessellation; i++)
      {
        float u = (float) i / tessellation;

        for (int j = 0; j <= tessellation; j++)
        {
          float v = (float) j / tessellation;

          //Perform four horizontal bezier interpolations
          Vector3 p1, p2, p3, p4;

          CubicInterpolate(patchControlPoints[0], patchControlPoints[1], patchControlPoints[2], patchControlPoints[3], u, out p1);
          CubicInterpolate(patchControlPoints[4], patchControlPoints[5], patchControlPoints[6], patchControlPoints[7], u, out p2);
          CubicInterpolate(patchControlPoints[8], patchControlPoints[9], patchControlPoints[10], patchControlPoints[11], u, out p3);
          CubicInterpolate(patchControlPoints[12], patchControlPoints[13], patchControlPoints[14], patchControlPoints[15], u, out p4);

          //Perform a vertical interpolation
          Vector3 position;
          CubicInterpolate(p1, p2, p3, p4, v, out position);

          if (normals is not null)
          {
            //Perform four vertical bezier interpolations
            Vector3 q1, q2, q3, q4;

            CubicInterpolate(patchControlPoints[0], patchControlPoints[4], patchControlPoints[8], patchControlPoints[12], v, out q1);
            CubicInterpolate(patchControlPoints[1], patchControlPoints[5], patchControlPoints[9], patchControlPoints[13], v, out q2);
            CubicInterpolate(patchControlPoints[2], patchControlPoints[6], patchControlPoints[10], patchControlPoints[14], v, out q3);
            CubicInterpolate(patchControlPoints[3], patchControlPoints[7], patchControlPoints[11], patchControlPoints[15], v, out q4);

            //Compute vertical and horizontal tangent vectors
            Vector3 vertTan, horizTan;
            CubicTangent(p1, p2, p3, p4, v, out vertTan);
            CubicTangent(q1, q2, q3, q4, u, out horizTan);

            //Cross the tangents to compute the normal
            Vector3 normal;
            Vector3.Cross(vertTan, horizTan, out normal);

            if (!normal.IsAlmostZero())
            {
              normal.Normalize();

              if ((mirror && !flip) || (!mirror && flip))
                normal.Negate();
            }
            else
            {
              //Classic teapot model contains degenerate geometry where a patch has several control points in the same place, causing tangent computation to 
              //produce a zero normal. Fix this with a hardcoded up/down normal depending on if we're on the top or bottom of the teapot
              normal.X = 0.0f;
              if (flip)
                normal.Y = (position.Y < 0.0f) ? 1.0f : -1.0f;
              else
                normal.Y = (position.Y < 0.0f) ? -1.0f : 1.0f;
              normal.Z = 0.0f;
            }

            normals.Set(normal);
          }

          if (positions is not null)
          {
            //Translate position
            Vector3.Add(position, center, out position);

            positions.Set(position);
          }

          if (textureCoordinates is not null)
          {
            //Compute the texture coordinates
            Vector2 texCoords = new Vector2((mirror) ? (1 - u) : u, v);
            textureCoordinates.Set(texCoords);
          }
        }
      }
    }

    //Create indices for the specified tessellation level
    private static void CreatePatchIndices(IndexDataBuilder indices, int tessellation, int vBaseIndex, bool mirror, bool flip)
    {
      int stride = tessellation + 1;

      if (flip)
      {
        for (int i = 0; i < tessellation; i++)
        {
          for (int j = 0; j < tessellation; j++)
          {
            if (mirror)
            {
              indices.Set(vBaseIndex + i * stride + j);
              indices.Set(vBaseIndex + (i + 1) * stride + j + 1);
              indices.Set(vBaseIndex + i * stride + j + 1);

              indices.Set(vBaseIndex + i * stride + j);
              indices.Set(vBaseIndex + (i + 1) * stride + j);
              indices.Set(vBaseIndex + (i + 1) * stride + j + 1);
            }
            else
            {
              indices.Set(vBaseIndex + (i + 1) * stride + j + 1);
              indices.Set(vBaseIndex + (i + 1) * stride + j);
              indices.Set(vBaseIndex + i * stride + j);

              indices.Set(vBaseIndex + i * stride + j + 1);
              indices.Set(vBaseIndex + (i + 1) * stride + j + 1);
              indices.Set(vBaseIndex + i * stride + j);

            }
          }
        }
      }
      else
      {
        for (int i = 0; i < tessellation; i++)
        {
          for (int j = 0; j < tessellation; j++)
          {
            if (mirror)
            {
              indices.Set(vBaseIndex + i * stride + j + 1);
              indices.Set(vBaseIndex + (i + 1) * stride + j + 1);
              indices.Set(vBaseIndex + i * stride + j);

              indices.Set(vBaseIndex + (i + 1) * stride + j + 1);
              indices.Set(vBaseIndex + (i + 1) * stride + j);
              indices.Set(vBaseIndex + i * stride + j);
            }
            else
            {
              indices.Set(vBaseIndex + i * stride + j);
              indices.Set(vBaseIndex + (i + 1) * stride + j);
              indices.Set(vBaseIndex + (i + 1) * stride + j + 1);

              indices.Set(vBaseIndex + i * stride + j);
              indices.Set(vBaseIndex + (i + 1) * stride + j + 1);
              indices.Set(vBaseIndex + i * stride + j + 1);
            }
          }
        }
      }
    }

    //Computes the cubic bezier interpolation between four control points, returning the value at the specified parameter between [0, 1]
    private static void CubicInterpolate(in Vector3 p1, in Vector3 p2, in Vector3 p3, in Vector3 p4, float t, out Vector3 result)
    {
      float tSquared = t * t;
      float oneTSquared = (1 - t) * (1 - t);
      float oneT = 1 - t;

      Vector3 temp;

      Vector3.Multiply(p1, oneT * oneTSquared, out result);

      Vector3.Multiply(p2, 3 * t * oneTSquared, out temp);
      Vector3.Add(result, temp, out result);

      Vector3.Multiply(p3, 3 * tSquared * oneT, out temp);
      Vector3.Add(result, temp, out result);

      Vector3.Multiply(p4, t * tSquared, out temp);
      Vector3.Add(result, temp, out result);
    }

    //Computes the tangent of a cubic bezier curve at  the specified parameter
    private static void CubicTangent(in Vector3 p1, in Vector3 p2, in Vector3 p3, in Vector3 p4, float t, out Vector3 result)
    {
      float tSquared = t * t;

      Vector3 temp;

      Vector3.Multiply(p1, -1 + (2 * t) - tSquared, out result);

      Vector3.Multiply(p2, 1 - (4 * t) + (3 * tSquared), out temp);
      Vector3.Add(result, temp, out result);

      Vector3.Multiply(p3, (2 * t) - (3 * tSquared), out temp);
      Vector3.Add(result, temp, out result);

      Vector3.Multiply(p4, tSquared, out temp);
      Vector3.Add(result, temp, out result);
    }

    #region Teapot Data

    private struct PatchData
    {
      public bool MirrorZ;
      public int[] Indices;

      public PatchData(bool mirrorZ, int[] indices)
      {
        MirrorZ = mirrorZ;
        Indices = indices;
      }
    }

    //Defines bezier patches that make up the teapot
    private static readonly PatchData[] TeapotPatches = new PatchData[]
        {
          //Rim
          new PatchData(true, new[] {102, 103, 104, 105, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15}),

          //Body
          new PatchData(true, new[] {12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27}),
          new PatchData(true, new[] {24, 25, 26, 27, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40}),

          //Lid
          new PatchData(true, new[] {96, 96, 96, 96, 97, 98, 99, 100, 101, 101, 101, 101, 0, 1, 2, 3}),
          new PatchData(true, new[] {0, 1, 2, 3, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117}),

          //Handle
          new PatchData(false, new[] {41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56}),
          new PatchData(false, new[] {53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 28, 65, 66, 67}),

          //Spout
          new PatchData(false, new[] {68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83}),
          new PatchData(false, new[] {80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95}),

          //Bottom
          new PatchData(true, new[] {118, 118, 118, 118, 124, 122, 119, 121, 123, 126, 125, 120, 40, 39, 38, 37})
        };

    private static readonly int NumberTessellatePatchCalled = 32;

    //Defines the control point positions that make up the teapot
    private static readonly Vector3[] TeapotControlPoints = new Vector3[]
        {
          new Vector3(0, 0.345f, -0.05f),
          new Vector3(-0.028f, 0.345f, -0.05f),
          new Vector3(-0.05f, 0.345f, -0.028f),
          new Vector3(-0.05f, 0.345f, -0),
          new Vector3(0, 0.3028125f, -0.334375f),
          new Vector3(-0.18725f, 0.3028125f, -0.334375f),
          new Vector3(-0.334375f, 0.3028125f, -0.18725f),
          new Vector3(-0.334375f, 0.3028125f, -0),
          new Vector3(0, 0.3028125f, -0.359375f),
          new Vector3(-0.20125f, 0.3028125f, -0.359375f),
          new Vector3(-0.359375f, 0.3028125f, -0.20125f),
          new Vector3(-0.359375f, 0.3028125f, -0),
          new Vector3(0, 0.27f, -0.375f),
          new Vector3(-0.21f, 0.27f, -0.375f),
          new Vector3(-0.375f, 0.27f, -0.21f),
          new Vector3(-0.375f, 0.27f, -0),
          new Vector3(0, 0.13875f, -0.4375f),
          new Vector3(-0.245f, 0.13875f, -0.4375f),
          new Vector3(-0.4375f, 0.13875f, -0.245f),
          new Vector3(-0.4375f, 0.13875f, -0),
          new Vector3(0, 0.007499993f, -0.5f),
          new Vector3(-0.28f, 0.007499993f, -0.5f),
          new Vector3(-0.5f, 0.007499993f, -0.28f),
          new Vector3(-0.5f, 0.007499993f, -0),
          new Vector3(0, -0.105f, -0.5f),
          new Vector3(-0.28f, -0.105f, -0.5f),
          new Vector3(-0.5f, -0.105f, -0.28f),
          new Vector3(-0.5f, -0.105f, -0),
          new Vector3(0, -0.105f, 0.5f),
          new Vector3(0, -0.2175f, -0.5f),
          new Vector3(-0.28f, -0.2175f, -0.5f),
          new Vector3(-0.5f, -0.2175f, -0.28f),
          new Vector3(-0.5f, -0.2175f, -0),
          new Vector3(0, -0.27375f, -0.375f),
          new Vector3(-0.21f, -0.27375f, -0.375f),
          new Vector3(-0.375f, -0.27375f, -0.21f),
          new Vector3(-0.375f, -0.27375f, -0),
          new Vector3(0, -0.2925f, -0.375f),
          new Vector3(-0.21f, -0.2925f, -0.375f),
          new Vector3(-0.375f, -0.2925f, -0.21f),
          new Vector3(-0.375f, -0.2925f, -0),
          new Vector3(0, 0.17625f, 0.4f),
          new Vector3(-0.075f, 0.17625f, 0.4f),
          new Vector3(-0.075f, 0.2325f, 0.375f),
          new Vector3(0, 0.2325f, 0.375f),
          new Vector3(0, 0.17625f, 0.575f),
          new Vector3(-0.075f, 0.17625f, 0.575f),
          new Vector3(-0.075f, 0.2325f, 0.625f),
          new Vector3(0, 0.2325f, 0.625f),
          new Vector3(0, 0.17625f, 0.675f),
          new Vector3(-0.075f, 0.17625f, 0.675f),
          new Vector3(-0.075f, 0.2325f, 0.75f),
          new Vector3(0, 0.2325f, 0.75f),
          new Vector3(0, 0.12f, 0.675f),
          new Vector3(-0.075f, 0.12f, 0.675f),
          new Vector3(-0.075f, 0.12f, 0.75f),
          new Vector3(0, 0.12f, 0.75f),
          new Vector3(0, 0.06375f, 0.675f),
          new Vector3(-0.075f, 0.06375f, 0.675f),
          new Vector3(-0.075f, 0.007499993f, 0.75f),
          new Vector3(0, 0.007499993f, 0.75f),
          new Vector3(0, -0.04875001f, 0.625f),
          new Vector3(-0.075f, -0.04875001f, 0.625f),
          new Vector3(-0.075f, -0.09562501f, 0.6625f),
          new Vector3(0, -0.09562501f, 0.6625f),
          new Vector3(-0.075f, -0.105f, 0.5f),
          new Vector3(-0.075f, -0.18f, 0.475f),
          new Vector3(0, -0.18f, 0.475f),
          new Vector3(0, 0.02624997f, -0.425f),
          new Vector3(-0.165f, 0.02624997f, -0.425f),
          new Vector3(-0.165f, -0.18f, -0.425f),
          new Vector3(0, -0.18f, -0.425f),
          new Vector3(0, 0.02624997f, -0.65f),
          new Vector3(-0.165f, 0.02624997f, -0.65f),
          new Vector3(-0.165f, -0.12375f, -0.775f),
          new Vector3(0, -0.12375f, -0.775f),
          new Vector3(0, 0.195f, -0.575f),
          new Vector3(-0.0625f, 0.195f, -0.575f),
          new Vector3(-0.0625f, 0.17625f, -0.6f),
          new Vector3(0, 0.17625f, -0.6f),
          new Vector3(0, 0.27f, -0.675f),
          new Vector3(-0.0625f, 0.27f, -0.675f),
          new Vector3(-0.0625f, 0.27f, -0.825f),
          new Vector3(0, 0.27f, -0.825f),
          new Vector3(0, 0.28875f, -0.7f),
          new Vector3(-0.0625f, 0.28875f, -0.7f),
          new Vector3(-0.0625f, 0.2934375f, -0.88125f),
          new Vector3(0, 0.2934375f, -0.88125f),
          new Vector3(0, 0.28875f, -0.725f),
          new Vector3(-0.0375f, 0.28875f, -0.725f),
          new Vector3(-0.0375f, 0.298125f, -0.8625f),
          new Vector3(0, 0.298125f, -0.8625f),
          new Vector3(0, 0.27f, -0.7f),
          new Vector3(-0.0375f, 0.27f, -0.7f),
          new Vector3(-0.0375f, 0.27f, -0.8f),
          new Vector3(0, 0.27f, -0.8f),
          new Vector3(0, 0.4575f, -0),
          new Vector3(0, 0.4575f, -0.2f),
          new Vector3(-0.1125f, 0.4575f, -0.2f),
          new Vector3(-0.2f, 0.4575f, -0.1125f),
          new Vector3(-0.2f, 0.4575f, -0),
          new Vector3(0, 0.3825f, -0),
          new Vector3(0, 0.27f, -0.35f),
          new Vector3(-0.196f, 0.27f, -0.35f),
          new Vector3(-0.35f, 0.27f, -0.196f),
          new Vector3(-0.35f, 0.27f, -0),
          new Vector3(0, 0.3075f, -0.1f),
          new Vector3(-0.056f, 0.3075f, -0.1f),
          new Vector3(-0.1f, 0.3075f, -0.056f),
          new Vector3(-0.1f, 0.3075f, -0),
          new Vector3(0, 0.3075f, -0.325f),
          new Vector3(-0.182f, 0.3075f, -0.325f),
          new Vector3(-0.325f, 0.3075f, -0.182f),
          new Vector3(-0.325f, 0.3075f, -0),
          new Vector3(0, 0.27f, -0.325f),
          new Vector3(-0.182f, 0.27f, -0.325f),
          new Vector3(-0.325f, 0.27f, -0.182f),
          new Vector3(-0.325f, 0.27f, -0),
          new Vector3(0, -0.33f, -0),
          new Vector3(-0.1995f, -0.33f, -0.35625f),
          new Vector3(0, -0.31125f, -0.375f),
          new Vector3(0, -0.33f, -0.35625f),
          new Vector3(-0.35625f, -0.33f, -0.1995f),
          new Vector3(-0.375f, -0.31125f, -0),
          new Vector3(-0.35625f, -0.33f, -0),
          new Vector3(-0.21f, -0.31125f, -0.375f),
          new Vector3(-0.375f, -0.31125f, -0.21f)
        };

    #endregion
  }
}
