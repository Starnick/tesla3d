﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// A tesselated plane that will be constructed around a center point with a width/height. Default orientation is a +Y up right handed coordinate system, where the width will be along the X axis,
  /// the height along the Z axis, and the normal vector along the Y axis. In such a system, the axes will be [1,0,0], [0,1,0], [0,0,-1] for X, Y, Z axes.
  /// </summary>
  public sealed class PlaneGenerator : GeometryGenerator
  {
    private Vector3 m_center;
    private float m_width;
    private float m_height;
    private Triad m_axes;
    private Vector2 m_uvScale;
    private int m_tessellation;

    public Vector3 Center
    {
      get
      {
        return m_center;
      }
      set
      {
        m_center = value;
      }
    }

    public float Width
    {
      get
      {
        return m_width;
      }
      set
      {
        m_width = value;
      }
    }

    public float Height
    {
      get
      {
        return m_height;
      }
      set
      {
        m_height = value;
      }
    }

    public Triad Axes
    {
      get
      {
        return m_axes;
      }
      set
      {
        m_axes = value;
      }
    }

    public ref readonly Vector3 Normal
    {
      get
      {
        return ref m_axes.YAxis;
      }
    }

    public ref readonly Vector3 WidthAxis
    {
      get
      {
        return ref m_axes.XAxis;
      }
    }

    public ref readonly Vector3 HeightAxis
    {
      get
      {
        return ref m_axes.ZAxis;
      }
    }

    public Vector2 UVScale
    {
      get
      {
        return m_uvScale;
      }
      set
      {
        m_uvScale = value;
      }
    }

    public int Tessellation
    {
      get
      {
        return m_tessellation;
      }
      set
      {
        m_tessellation = (value < 1) ? 1 : value;
      }
    }

    public PlaneGenerator() : this(Vector3.Zero, 1.0f, 1.0f, Triad.UnitAxesNegativeZ, Vector2.One, 1, false) { }

    public PlaneGenerator(float width, float height) : this(Vector3.Zero, width, height, Triad.UnitAxesNegativeZ, Vector2.One, 1, false) { }

    public PlaneGenerator(float width, float height, int tessellation) : this(Vector3.Zero, width, height, Triad.UnitAxesNegativeZ, Vector2.One, tessellation, false) { }

    public PlaneGenerator(float width, float height, in Triad axes) : this(Vector3.Zero, width, height, axes, Vector2.One, 1, false) { }

    public PlaneGenerator(float width, float height, in Triad axes, int tessellation) : this(Vector3.Zero, width, height, axes, Vector2.One, tessellation, false) { }

    public PlaneGenerator(in Vector3 center, float width, float height) : this(center, width, height, Triad.UnitAxesNegativeZ, Vector2.One, 1, false) { }

    public PlaneGenerator(in Vector3 center, float width, float height, in Triad axes) : this(center, width, height, axes, Vector2.One, 1, false) { }

    public PlaneGenerator(in Vector3 center, float width, float height, in Triad axes, int tessellation) : this(center, width, height, axes, Vector2.One, tessellation, false) { }

    public PlaneGenerator(in Vector3 center, float width, float height, in Triad axes, in Vector2 uvScale) : this(center, width, height, axes, uvScale, 1, false) { }

    public PlaneGenerator(in Vector3 center, float width, float height, in Triad axes, in Vector2 uvScale, int tessellation) : this(center, width, height, axes, uvScale, tessellation, false) { }

    public PlaneGenerator(in Vector3 center, float width, float height, in Triad axes, in Vector2 uvScale, int tessellation, bool insideOut)
    {
      m_center = center;
      m_width = width;
      m_height = height;
      m_uvScale = uvScale;
      m_axes = axes;
      Tessellation = tessellation;
      InsideOut = insideOut;
    }

    public void SetAxes(in Vector3 widthAxis, in Vector3 heightAxis)
    {
      m_axes.XAxis = widthAxis;
      m_axes.ZAxis = heightAxis;

      m_axes.XAxis.Normalize();
      m_axes.ZAxis.Normalize();

      Vector3.NormalizedCross(widthAxis, heightAxis, out m_axes.YAxis);
    }

    public override void CalculateVertexIndexCounts(out int vertexCount, out int indexCount)
    {
      vertexCount = (m_tessellation + 1) * (m_tessellation + 1);
      indexCount = m_tessellation * m_tessellation * 6;
    }

    public override void Build([AllowNull] ref DataBuffer<VertexPositionNormalTexture> vertices, ref IndexData indices)
    {
      int vertCount, indexCount;
      CalculateVertexIndexCounts(out vertCount, out indexCount);

      DataBufferBuilder<VertexPositionNormalTexture> vertexBuilder = CreateVertexBuilder<VertexPositionNormalTexture>(vertCount, vertices);
      IndexDataBuilder indexBuilder = CreateIndexBuilder(indexCount, indices);

      GenerateData(vertexBuilder, m_center, m_width, m_height, m_axes, m_uvScale, m_tessellation, InsideOut);
      GenerateIndexData(indexBuilder, m_tessellation, InsideOut);

      vertices = vertexBuilder.Claim(true);
      indices = indexBuilder.Claim(true);
    }

    public override void Build([AllowNull] ref DataBuffer<Vector3> positions, [AllowNull] ref DataBuffer<Vector3> normals, [AllowNull] ref DataBuffer<Vector2> textureCoordinates, ref IndexData indices, GenerateOptions options)
    {
      if (options == GenerateOptions.None)
        return;

      int vertCount, indexCount;
      CalculateVertexIndexCounts(out vertCount, out indexCount);

      DataBufferBuilder<Vector3>? posBuilder = null;
      DataBufferBuilder<Vector3>? normalBuilder = null;
      DataBufferBuilder<Vector2>? texCoordBuilder = null;
      IndexDataBuilder indexBuilder = CreateIndexBuilder(indexCount, indices);

      if ((options & GenerateOptions.Positions) == GenerateOptions.Positions)
        posBuilder = CreateVertexBuilder<Vector3>(vertCount, positions);

      if ((options & GenerateOptions.Normals) == GenerateOptions.Normals)
        normalBuilder = CreateVertexBuilder<Vector3>(vertCount, normals);

      if ((options & GenerateOptions.TextureCoordinates) == GenerateOptions.TextureCoordinates)
        texCoordBuilder = CreateVertexBuilder<Vector2>(vertCount, textureCoordinates);

      GenerateData(posBuilder, normalBuilder, texCoordBuilder, m_center, m_width, m_height, m_axes, m_uvScale, m_tessellation, InsideOut);
      GenerateIndexData(indexBuilder, m_tessellation, InsideOut);

      if (posBuilder is not null)
        positions = posBuilder.Claim(true);

      if (normalBuilder is not null)
        normals = normalBuilder.Claim(true);

      if (texCoordBuilder is not null)
        textureCoordinates = texCoordBuilder.Claim(true);

      indices = indexBuilder.Claim(true);
    }

    private static void GenerateData(DataBufferBuilder<VertexPositionNormalTexture> data, in Vector3 center, float width, float height, in Triad axes, in Vector2 uvScale, int tessellation, bool flip)
    {
      data.Position = 0;

      int stride = tessellation + 1;
      float deltaWidth = width / tessellation;
      float deltaHeight = height / tessellation;

      width *= 0.5f;
      height *= 0.5f;

      Vector3 normal = axes.YAxis;

      if (flip)
        normal.Negate();

      for (int i = 0; i < stride; i++)
      {
        for (int j = 0; j < stride; j++)
        {
          Vector3 pos = center;
          Vector3 right = axes.XAxis;
          Vector3.Multiply(right, -width + (deltaWidth * j), out right);

          Vector3 up = axes.ZAxis;
          Vector3.Multiply(up, height - (deltaHeight * i), out up);

          Vector3.Add(pos, right, out pos);
          Vector3.Add(pos, up, out pos);

          Vector2 texCoords = new Vector2((1.0f * j) / (tessellation * uvScale.X), (1.0f * i) / (tessellation * uvScale.Y));

          VertexPositionNormalTexture vert = new VertexPositionNormalTexture(pos, normal, texCoords);
          data.Set(vert);
        }
      }
    }

    private static void GenerateData(DataBufferBuilder<Vector3>? positions, DataBufferBuilder<Vector3>? normals, DataBufferBuilder<Vector2>? textureCoordinates, in Vector3 center, float width, float height, in Triad axes, in Vector2 uvScale, int tessellation, bool flip)
    {
      if (positions is not null)
        positions.Position = 0;

      if (normals is not null)
        normals.Position = 0;

      if (textureCoordinates is not null)
        textureCoordinates.Position = 0;

      int stride = tessellation + 1;
      float deltaWidth = width / tessellation;
      float deltaHeight = height / tessellation;

      width *= 0.5f;
      height *= 0.5f;

      Vector3 normal = axes.YAxis;

      if (flip)
        normal.Negate();

      for (int i = 0; i < stride; i++)
      {
        for (int j = 0; j < stride; j++)
        {
          if (positions is not null)
          {
            Vector3 pos = center;
            Vector3 right = axes.XAxis;
            Vector3.Multiply(right, -width + (deltaWidth * j), out right);

            Vector3 up = axes.ZAxis;
            Vector3.Multiply(up, height - (deltaHeight * i), out up);

            Vector3.Add(pos, right, out pos);
            Vector3.Add(pos, up, out pos);

            positions.Set(pos);
          }

          if (normals is not null)
            normals.Set(normal);

          if (textureCoordinates is not null)
          {
            Vector2 texCoords = new Vector2((1.0f * j) / (tessellation * uvScale.X), (1.0f * i) / (tessellation * uvScale.Y));
            textureCoordinates.Set(texCoords);
          }
        }
      }
    }

    private static void GenerateIndexData(IndexDataBuilder indices, int tessellation, bool flip)
    {
      indices.Position = 0;

      int stride = tessellation + 1;

      if (flip)
      {
        for (int i = 0; i < tessellation; i++)
        {
          for (int j = 0; j < tessellation; j++)
          {
            int index = (stride * i) + j;

            indices.Set(index + stride);
            indices.Set(index + stride + 1);
            indices.Set(index + 1);

            indices.Set(index);
            indices.Set(index + stride);
            indices.Set(index + 1);
          }
        }
      }
      else
      {
        for (int i = 0; i < tessellation; i++)
        {
          for (int j = 0; j < tessellation; j++)
          {
            int index = (stride * i) + j;

            indices.Set(index + 1);
            indices.Set(index + stride + 1);
            indices.Set(index + stride);

            indices.Set(index + 1);
            indices.Set(index + stride);
            indices.Set(index);
          }
        }
      }
    }
  }
}
