﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;
using System.Reflection.PortableExecutable;

#nullable enable

namespace Tesla.Graphics
{
  public sealed class BoxGenerator : GeometryGenerator
  {
    private const int MaxIndices = 36;
    private const int MaxVertices = 24;

    private Triad m_axes;
    private Vector3 m_center;
    private Vector3 m_extents;

    public Triad Axes
    {
      get
      {
        return m_axes;
      }
      set
      {
        m_axes = value;
      }
    }

    public Vector3 Center
    {
      get
      {
        return m_center;
      }
      set
      {
        m_center = value;
      }
    }

    public Vector3 Extents
    {
      get
      {
        return m_extents;
      }
      set
      {
        m_extents = value;
      }
    }

    public BoxGenerator() : this(Triad.UnitAxes, Vector3.Zero, Vector3.One, false) { }

    public BoxGenerator(in Vector3 extents) : this(Triad.UnitAxes, Vector3.Zero, extents, false) { }

    public BoxGenerator(in Triad axes, in Vector3 extents) : this(axes, Vector3.Zero, extents, false) { }

    public BoxGenerator(in Vector3 center, in Vector3 extents) : this(Triad.UnitAxes, center, extents, false) { }

    public BoxGenerator(in Triad axes, in Vector3 center, in Vector3 extents) : this(axes, center, extents, false) { }

    public BoxGenerator(in Triad axes, in Vector3 center, in Vector3 extents, bool insideOut)
    {
      m_axes = axes;
      m_center = center;
      m_extents = extents;
      InsideOut = insideOut;
    }

    public override void CalculateVertexIndexCounts(out int vertexCount, out int indexCount)
    {
      vertexCount = MaxVertices;
      indexCount = MaxIndices;
    }

    public override void Build([AllowNull] ref DataBuffer<VertexPositionNormalTexture> vertices, ref IndexData indices)
    {
      DataBufferBuilder<VertexPositionNormalTexture> vertexBuilder = CreateVertexBuilder<VertexPositionNormalTexture>(MaxVertices, vertices);
      IndexDataBuilder indexBuilder = CreateIndexBuilder(MaxIndices, indices);

      GenerateData(vertexBuilder, m_axes, m_center, m_extents, InsideOut);
      GenerateIndexData(indexBuilder, InsideOut);

      vertices = vertexBuilder.Claim(true);
      indices = indexBuilder.Claim(true);
    }

    public override void Build([AllowNull] ref DataBuffer<Vector3> positions, [AllowNull] ref DataBuffer<Vector3> normals, [AllowNull] ref DataBuffer<Vector2> textureCoordinates, ref IndexData indices, GenerateOptions options)
    {
      if (options == GenerateOptions.None)
        return;

      DataBufferBuilder<Vector3>? posBuilder = null;
      DataBufferBuilder<Vector3>? normalBuilder = null;
      DataBufferBuilder<Vector2>? texCoordBuilder = null;
      IndexDataBuilder indexBuilder = CreateIndexBuilder(MaxIndices, indices);

      if ((options & GenerateOptions.Positions) == GenerateOptions.Positions)
      {
        posBuilder = CreateVertexBuilder<Vector3>(MaxVertices, positions);
        GeneratePositionData(posBuilder, m_axes, m_center, m_extents);
      }

      if ((options & GenerateOptions.Normals) == GenerateOptions.Normals)
      {
        normalBuilder = CreateVertexBuilder<Vector3>(MaxVertices, normals);
        GenerateNormalData(normalBuilder, InsideOut);
      }

      if ((options & GenerateOptions.TextureCoordinates) == GenerateOptions.TextureCoordinates)
      {
        texCoordBuilder = CreateVertexBuilder<Vector2>(MaxVertices, textureCoordinates);
        GenerateTextureData(texCoordBuilder, InsideOut);
      }

      GenerateIndexData(indexBuilder, InsideOut);

      if (posBuilder is not null)
        positions = posBuilder.Claim(true);

      if (normalBuilder is not null)
        normals = normalBuilder.Claim(true);

      if (texCoordBuilder is not null)
        textureCoordinates = texCoordBuilder.Claim(true);

      indices = indexBuilder.Claim(true);
    }

    private static void ComputeVertices(in Triad axes, in Vector3 center, in Vector3 extents, out Vector3 v0, out Vector3 v1, out Vector3 v2, out Vector3 v3,
        out Vector3 v4, out Vector3 v5, out Vector3 v6, out Vector3 v7)
    {
      Triad ExAxes;
      Vector3.Multiply(axes.XAxis, extents.X, out ExAxes.XAxis);
      Vector3.Multiply(axes.YAxis, extents.Y, out ExAxes.YAxis);
      Vector3.Multiply(axes.ZAxis, extents.Z, out ExAxes.ZAxis);

      //Use scaled axes to computer the corners
      Vector3 temp;
      Vector3.Subtract(center, ExAxes.XAxis, out temp);
      Vector3.Add(temp, ExAxes.YAxis, out temp);
      Vector3.Add(temp, ExAxes.ZAxis, out v0);

      Vector3.Add(center, ExAxes.XAxis, out temp);
      Vector3.Add(temp, ExAxes.YAxis, out temp);
      Vector3.Add(temp, ExAxes.ZAxis, out v1);

      Vector3.Add(center, ExAxes.XAxis, out temp);
      Vector3.Subtract(temp, ExAxes.YAxis, out temp);
      Vector3.Add(temp, ExAxes.ZAxis, out v2);

      Vector3.Subtract(center, ExAxes.XAxis, out temp);
      Vector3.Subtract(temp, ExAxes.YAxis, out temp);
      Vector3.Add(temp, ExAxes.ZAxis, out v3);

      Vector3.Add(center, ExAxes.XAxis, out temp);
      Vector3.Add(temp, ExAxes.YAxis, out temp);
      Vector3.Subtract(temp, ExAxes.ZAxis, out v4);

      Vector3.Subtract(center, ExAxes.XAxis, out temp);
      Vector3.Add(temp, ExAxes.YAxis, out temp);
      Vector3.Subtract(temp, ExAxes.ZAxis, out v5);

      Vector3.Subtract(center, ExAxes.XAxis, out temp);
      Vector3.Subtract(temp, ExAxes.YAxis, out temp);
      Vector3.Subtract(temp, ExAxes.ZAxis, out v6);

      Vector3.Add(center, ExAxes.XAxis, out temp);
      Vector3.Subtract(temp, ExAxes.YAxis, out temp);
      Vector3.Subtract(temp, ExAxes.ZAxis, out v7);
    }

    private static void GenerateData(DataBufferBuilder<VertexPositionNormalTexture> data, in Triad axes, in Vector3 center, in Vector3 extents, bool flip)
    {
      data.Position = 0;

      //Setup position data
      Vector3 v0, v1, v2, v3, v4, v5, v6, v7;
      ComputeVertices(axes, center, extents, out v0, out v1, out v2, out v3, out v4, out v5, out v6, out v7);

      //Setup normal data
      Vector3 xDir, yDir, zDir, negXDir, negYDir, negZDir;

      if (flip)
      {
        xDir = new Vector3(-1, 0, 0);
        yDir = new Vector3(0, -1, 0);
        zDir = new Vector3(0, 0, -1);

        negXDir = new Vector3(1, 0, 0);
        negYDir = new Vector3(0, 1, 0);
        negZDir = new Vector3(0, 0, 1);
      }
      else
      {
        xDir = new Vector3(1, 0, 0);
        yDir = new Vector3(0, 1, 0);
        zDir = new Vector3(0, 0, 1);

        negXDir = new Vector3(-1, 0, 0);
        negYDir = new Vector3(0, -1, 0);
        negZDir = new Vector3(0, 0, -1);
      }

      //Setup UV data
      Vector2 uv0, uv1, uv2, uv3;

      if (flip)
      {
        uv0 = new Vector2(1, 0);
        uv1 = new Vector2(0, 0);
        uv2 = new Vector2(0, 1);
        uv3 = new Vector2(1, 1);
      }
      else
      {
        uv0 = new Vector2(0, 0);
        uv1 = new Vector2(1, 0);
        uv2 = new Vector2(1, 1);
        uv3 = new Vector2(0, 1);
      }

      //Set vertex data

      //Front
      VertexPositionNormalTexture vert = new VertexPositionNormalTexture(v0, zDir, uv0);
      data.Set(vert);

      vert = new VertexPositionNormalTexture(v1, zDir, uv1);
      data.Set(vert);

      vert = new VertexPositionNormalTexture(v2, zDir, uv2);
      data.Set(vert);

      vert = new VertexPositionNormalTexture(v3, zDir, uv3);
      data.Set(vert);

      //Back
      vert = new VertexPositionNormalTexture(v4, negZDir, uv0);
      data.Set(vert);

      vert = new VertexPositionNormalTexture(v5, negZDir, uv1);
      data.Set(vert);

      vert = new VertexPositionNormalTexture(v6, negZDir, uv2);
      data.Set(vert);

      vert = new VertexPositionNormalTexture(v7, negZDir, uv3);
      data.Set(vert);

      //Left
      vert = new VertexPositionNormalTexture(v5, negXDir, uv0);
      data.Set(vert);

      vert = new VertexPositionNormalTexture(v0, negXDir, uv1);
      data.Set(vert);

      vert = new VertexPositionNormalTexture(v3, negXDir, uv2);
      data.Set(vert);

      vert = new VertexPositionNormalTexture(v6, negXDir, uv3);
      data.Set(vert);

      //Right
      vert = new VertexPositionNormalTexture(v1, xDir, uv0);
      data.Set(vert);

      vert = new VertexPositionNormalTexture(v4, xDir, uv1);
      data.Set(vert);

      vert = new VertexPositionNormalTexture(v7, xDir, uv2);
      data.Set(vert);

      vert = new VertexPositionNormalTexture(v2, xDir, uv3);
      data.Set(vert);

      //Top
      vert = new VertexPositionNormalTexture(v5, yDir, uv0);
      data.Set(vert);

      vert = new VertexPositionNormalTexture(v4, yDir, uv1);
      data.Set(vert);

      vert = new VertexPositionNormalTexture(v1, yDir, uv2);
      data.Set(vert);

      vert = new VertexPositionNormalTexture(v0, yDir, uv3);
      data.Set(vert);

      //Bottom
      vert = new VertexPositionNormalTexture(v7, negYDir, uv0);
      data.Set(vert);

      vert = new VertexPositionNormalTexture(v6, negYDir, uv1);
      data.Set(vert);

      vert = new VertexPositionNormalTexture(v3, negYDir, uv2);
      data.Set(vert);

      vert = new VertexPositionNormalTexture(v2, negYDir, uv3);
      data.Set(vert);
    }

    private static void GeneratePositionData(DataBufferBuilder<Vector3> positions, in Triad axes, in Vector3 center, in Vector3 extents)
    {
      positions.Position = 0;

      Vector3 v0, v1, v2, v3, v4, v5, v6, v7;
      ComputeVertices(axes, center, extents, out v0, out v1, out v2, out v3, out v4, out v5, out v6, out v7);

      //Front
      positions.Set(v0);
      positions.Set(v1);
      positions.Set(v2);
      positions.Set(v3);

      //Back
      positions.Set(v4);
      positions.Set(v5);
      positions.Set(v6);
      positions.Set(v7);

      //Left
      positions.Set(v5);
      positions.Set(v0);
      positions.Set(v3);
      positions.Set(v6);

      //Right
      positions.Set(v1);
      positions.Set(v4);
      positions.Set(v7);
      positions.Set(v2);

      //Top
      positions.Set(v5);
      positions.Set(v4);
      positions.Set(v1);
      positions.Set(v0);

      //Bottom
      positions.Set(v7);
      positions.Set(v6);
      positions.Set(v3);
      positions.Set(v2);
    }

    private static void GenerateNormalData(DataBufferBuilder<Vector3> normals, bool flip)
    {
      normals.Position = 0;

      Vector3 xDir, yDir, zDir, negXDir, negYDir, negZDir;

      if (flip)
      {
        xDir = new Vector3(-1, 0, 0);
        yDir = new Vector3(0, -1, 0);
        zDir = new Vector3(0, 0, -1);

        negXDir = new Vector3(1, 0, 0);
        negYDir = new Vector3(0, 1, 0);
        negZDir = new Vector3(0, 0, 1);
      }
      else
      {
        xDir = new Vector3(1, 0, 0);
        yDir = new Vector3(0, 1, 0);
        zDir = new Vector3(0, 0, 1);

        negXDir = new Vector3(-1, 0, 0);
        negYDir = new Vector3(0, -1, 0);
        negZDir = new Vector3(0, 0, -1);
      }

      //Front
      for (int i = 0; i < 4; i++)
        normals.Set(zDir);

      //Back
      for (int i = 4; i < 8; i++)
        normals.Set(negZDir);

      //Left
      for (int i = 8; i < 12; i++)
        normals.Set(negXDir);

      //Right
      for (int i = 12; i < 16; i++)
        normals.Set(xDir);

      //Top
      for (int i = 16; i < 20; i++)
        normals.Set(yDir);

      //Bottom
      for (int i = 20; i < 24; i++)
        normals.Set(negYDir);
    }

    private static void GenerateTextureData(DataBufferBuilder<Vector2> texCoords, bool flip)
    {
      texCoords.Position = 0;

      Vector2 uv0, uv1, uv2, uv3;

      if (flip)
      {
        uv0 = new Vector2(1, 0);
        uv1 = new Vector2(0, 0);
        uv2 = new Vector2(0, 1);
        uv3 = new Vector2(1, 1);
      }
      else
      {
        uv0 = new Vector2(0, 0);
        uv1 = new Vector2(1, 0);
        uv2 = new Vector2(1, 1);
        uv3 = new Vector2(0, 1);
      }

      for (int i = 0; i < 6; i++)
      {
        texCoords.Set(uv0);
        texCoords.Set(uv1);
        texCoords.Set(uv2);
        texCoords.Set(uv3);
      }
    }

    private static void GenerateIndexData(IndexDataBuilder indices, bool flip)
    {
      indices.Position = 0;

      if (flip)
      {
        for (int i = 0; i < MaxVertices; i += 4)
        {
          indices.Set(i + 2);
          indices.Set(i + 1);
          indices.Set(i);

          indices.Set(i + 3);
          indices.Set(i + 2);
          indices.Set(i);
        }
      }
      else
      {
        for (int i = 0; i < MaxVertices; i += 4)
        {
          indices.Set(i);
          indices.Set(i + 1);
          indices.Set(i + 2);

          indices.Set(i);
          indices.Set(i + 2);
          indices.Set(i + 3);
        }
      }
    }
  }
}
