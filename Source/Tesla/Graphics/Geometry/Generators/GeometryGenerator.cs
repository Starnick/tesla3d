﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System.Diagnostics.CodeAnalysis;

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// Base class for geometry generators.
  /// </summary>
  public abstract class GeometryGenerator
  {
    private bool m_use32BitIndices;
    private bool m_insideOut;

    /// <summary>
    /// Gets or sets if the generator should compute 32-bit or 16-bit indices.
    /// </summary>
    public bool Use32BitIndices
    {
      get
      {
        return m_use32BitIndices;
      }
      set
      {
        m_use32BitIndices = value;
      }
    }

    /// <summary>
    /// Gets or sets if the geometry should be generated "inside out", meaning normals are reversed
    /// and so are vertex windings.
    /// </summary>
    public bool InsideOut
    {
      get
      {
        return m_insideOut;
      }
      set
      {
        m_insideOut = value;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="GeometryGenerator"/> class.
    /// </summary>
    protected GeometryGenerator()
    {
      m_use32BitIndices = false;
      m_insideOut = false;
    }

    /// <summary>
    /// Calculates the number of vertices and indices that will be created by the generator.
    /// </summary>
    /// <param name="vertexCount">The number of vertices that will be created.</param>
    /// <param name="indexCount">The number of indices that will be created.</param>
    public abstract void CalculateVertexIndexCounts(out int vertexCount, out int indexCount);

    /// <summary>
    /// Builds an interleaved vertex buffer of type <see cref="VertexPositionNormalTexture"/>. To reduce garbage, existing data buffers
    /// can be passed into this method. If they are either null or not the correct size, new instances are created and returned.
    /// </summary>
    /// <param name="vertices">Vertex buffer, if null, a new one is created.</param>
    /// <param name="indices">Index buffer, if null, a new one is created.</param>
    public abstract void Build([AllowNull] ref DataBuffer<VertexPositionNormalTexture> vertices, ref IndexData indices);

    /// <summary>
    /// Builds individual vertex attribute buffers, based on the specified generation options. To reduce garbage, existing data buffers
    /// can be passed into this method. If they are either null or not the correct size, new instances are created and returned.
    /// </summary>
    /// <param name="positions">Positions buffer, if null and positions are to be generated, a new one is created.</param>
    /// <param name="normals">Normals buffer, if null and normals are to be generated, a new one is created.</param>
    /// <param name="textureCoordinates">Texture coordinates buffer, if null and texture coordinates are to be generated, a new one is created.</param>
    /// <param name="indices">Index buffer, if null, a new one is always created.</param>
    /// <param name="options">Generation options for vertex data.</param>
    public abstract void Build([AllowNull] ref DataBuffer<Vector3> positions, [AllowNull] ref DataBuffer<Vector3> normals, [AllowNull] ref DataBuffer<Vector2> textureCoordinates, ref IndexData indices, GenerateOptions options);

    /// <summary>
    /// Builds position and index buffers. To reduce garbage, existing data buffers
    /// can be passed into this method. If they are either null or not the correct size, new instances are created and returned.
    /// </summary>
    /// <param name="positions">Positions buffer, if null, a new one is created.</param>
    /// <param name="indices">Index buffer, if null, a new one is created.</param>
    public void Build([AllowNull] ref DataBuffer<Vector3> positions, ref IndexData indices)
    {
      DataBuffer<Vector3>? normals = null;
      DataBuffer<Vector2>? texCoords = null;

      Build(ref positions, ref normals, ref texCoords, ref indices, GenerateOptions.Positions);
    }

    /// <summary>
    /// Builds position, normals, and index buffers. To reduce garbage, existing data buffers
    /// can be passed into this method. If they are either null or not the correct size, new instances are created and returned.
    /// </summary>
    /// <param name="positions">Positions buffer, if null, a new one is created.</param>
    /// <param name="normals">Normals buffer, if null, a new one is created.</param>
    /// <param name="indices">Index buffer, if null, a new one is created.</param>
    public void Build([AllowNull] ref DataBuffer<Vector3> positions, [AllowNull] ref DataBuffer<Vector3> normals, ref IndexData indices)
    {
      DataBuffer<Vector2>? texCoords = null;

      Build(ref positions, ref normals, ref texCoords, ref indices, GenerateOptions.Positions | GenerateOptions.Normals);
    }

    /// <summary>
    /// Builds position, texture coordinate, and index buffers.To reduce garbage, existing data buffers
    /// can be passed into this method. If they are either null or not the correct size, new instances are created and returned.
    /// </summary>
    /// <param name="positions">Positions buffer, if null, a new one is created.</param>
    /// <param name="textureCoordinates">Texture coordinates buffer, if null, a new one is created.</param>
    /// <param name="indices">Index buffer, if null, a new one is created.</param>
    public void Build([AllowNull] ref DataBuffer<Vector3> positions, [AllowNull] ref DataBuffer<Vector2> textureCoordinates, ref IndexData indices)
    {
      DataBuffer<Vector3>? normals = null;

      Build(ref positions, ref normals, ref textureCoordinates, ref indices, GenerateOptions.Positions | GenerateOptions.TextureCoordinates);
    }

    /// <summary>
    /// Builds indexed position, normal, and texture coordinate vertex data and populates the <see cref="MeshData"/> object with the data. 
    /// <remarks>
    /// If the <see cref="MeshData"/> has existing buffers, then an attempt will be made to use them, otherwise new data buffers will be created and set. 
    /// The vertex data buffers are always set  to the appropiate buffer semantics of index zero. This method does not call <see cref="MeshData.ClearData"/> 
    /// and <see cref="MeshData.Compile()"/>. Those are left to the caller in case of situations where additional data will be computed or data buffers first need to be
    /// cleared.
    /// </remarks>
    /// </summary>
    /// <param name="meshData">Meshdata to populate.</param>
    public void BuildMeshData(MeshData meshData)
    {
      BuildMeshData(meshData, GenerateOptions.All);
    }

    /// <summary>
    /// Builds indexed position, normal, and/or texture coordinate vertex data and populates the <see cref="MeshData"/> object with the data. 
    /// <remarks>
    /// If the <see cref="MeshData"/> has existing buffers, then an attempt will be made to use them, otherwise new data buffers will be created and set. 
    /// The vertex data buffers are always set to the appropiate buffer semantics of index zero. This method does not call <see cref="MeshData.ClearData"/> 
    /// and <see cref="MeshData.Compile()"/>. Those are left to the caller in case of situations where additional data will be computed or data buffers first need to be
    /// cleared.
    /// </remarks>
    /// </summary>
    /// <param name="meshData">Meshdata to populate.</param>
    /// <param name="options">Generation options for vertex data.</param>
    public void BuildMeshData(MeshData meshData, GenerateOptions options)
    {
      if (meshData is null || options == GenerateOptions.None)
        return;

      DataBuffer<Vector3>? positions = ((options & GenerateOptions.Positions) == GenerateOptions.Positions) ? meshData.Positions : null;
      DataBuffer<Vector3>? normals = ((options & GenerateOptions.Normals) == GenerateOptions.Normals) ? meshData.Normals : null;
      DataBuffer<Vector2>? texCoords = ((options & GenerateOptions.TextureCoordinates) == GenerateOptions.TextureCoordinates) ? meshData.TextureCoordinates : null;
      IndexData indices = new IndexData();

      if (meshData.Indices.HasValue)
      {
        indices = meshData.Indices.Value;

        // If wrong index format, dispose current buffer
        if (Use32BitIndices != indices.Is32Bit)
        {
          indices.Dispose();
          meshData.Indices = null;
          indices = new IndexData();
        }
      }

      Build(ref positions, ref normals, ref texCoords, ref indices, options);

      if (positions != null)
        meshData.Positions = positions;

      if (normals != null)
        meshData.Normals = normals;

      if (texCoords != null)
        meshData.TextureCoordinates = texCoords;

      if (indices.IsValid)
        meshData.Indices = indices;
    }

    /// <summary>
    /// Convienence method for returning the current position index from any of the input buffers, whichever is not null first.
    /// </summary>
    /// <param name="positions">Positions buffer builder.</param>
    /// <param name="normals">Normals buffer builder.</param>
    /// <param name="textureCoordinates">Texture coordinate buffer builder.</param>
    /// <returns>Current position index.</returns>
    protected static int GetBaseVertexIndex(DataBufferBuilder<Vector3>? positions, DataBufferBuilder<Vector3>? normals, DataBufferBuilder<Vector2>? textureCoordinates)
    {
      if (positions is not null)
        return positions.Position;

      if (normals is not null)
        return normals.Position;

      if (textureCoordinates is not null)
        return textureCoordinates.Position;

      return 0;
    }

    /// <summary>
    /// Convienence method for creating a vertex buffer builder.
    /// </summary>
    /// <typeparam name="T">Vertex element.</typeparam>
    /// <param name="requiredCount">Initial number of elements that are required.</param>
    /// <param name="userDb">A possible user buffer that may be captured.</param>
    /// <returns>Buffer builder.</returns>
    protected DataBufferBuilder<T> CreateVertexBuilder<T>(int requiredCount, DataBuffer<T>? userDb = null) where T : unmanaged
    {
      if (userDb is null)
        return new DataBufferBuilder<T>(requiredCount);

      DataBufferBuilder<T> builder = new DataBufferBuilder<T>();
      builder.CaptureBuffer(userDb);
      builder.EnsureCapacity(requiredCount);

      return builder;
    }

    /// <summary>
    /// Convienence method for creating an index buffer builder.
    /// </summary>
    /// <param name="requiredCount">Initial number of elements that are required.</param>
    /// <param name="userDb">A possible user buffer that may be captured.</param>
    /// <returns>Buffer builder.</returns>
    protected IndexDataBuilder CreateIndexBuilder(int requiredCount, IndexData? userDb = null)
    {
      IndexFormat format = Use32BitIndices ? IndexFormat.ThirtyTwoBits : IndexFormat.SixteenBits;

      if (userDb is null || !userDb.Value.IsValid || Use32BitIndices != userDb.Value.Is32Bit)
        return new IndexDataBuilder(requiredCount, format);

      IndexDataBuilder builder = new IndexDataBuilder(0, format);
      builder.CaptureBuffer(userDb.Value);
      builder.EnsureCapacity(requiredCount);

      return builder;
    }
  }
}
