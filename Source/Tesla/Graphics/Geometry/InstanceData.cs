﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesla.Content;

// TODO NULLABLE

namespace Tesla.Graphics
{
  [SavableVersion(1)]
  public sealed class WorldMatrixInstanceDataProvider : IInstanceDataProvider
  {
    private static readonly VertexElement[] s_layout;
    private static readonly int s_dataSizeInBytes = Matrix.SizeInBytes;

    private bool m_transpose;

    public String InstanceDataName
    {
      get
      {
        return "WorldMatrix";
      }
    }

    public VertexElement[] DataLayout
    {
      get
      {
        return s_layout;
      }
    }

    public int DataSizeInBytes
    {
      get
      {
        return s_dataSizeInBytes;
      }
    }

    public bool TransposeMatrix
    {
      get
      {
        return m_transpose;
      }
      set
      {
        m_transpose = value;
      }
    }

    static WorldMatrixInstanceDataProvider()
    {
      s_layout = new VertexElement[]
      {
                new VertexElement(VertexSemantic.TextureCoordinate, 0, VertexFormat.Float4, 0),
                new VertexElement(VertexSemantic.TextureCoordinate, 1, VertexFormat.Float4, 16),
                new VertexElement(VertexSemantic.TextureCoordinate, 2, VertexFormat.Float4, 32),
                new VertexElement(VertexSemantic.TextureCoordinate, 3, VertexFormat.Float4, 48)
      };
    }

    public WorldMatrixInstanceDataProvider() : this(true) { }

    public WorldMatrixInstanceDataProvider(bool transpose)
    {
      m_transpose = transpose;
    }

    public void SetData(InstanceDefinition instanceDef, RenderPropertyCollection properties, Span<byte> dstBuffer)
    {
      Matrix world = Matrix.Identity;

      WorldTransformProperty prop;
      if (properties.TryGet<WorldTransformProperty>(out prop))
      {
        world = prop.Value.Matrix;

        if (m_transpose)
          world.Transpose();
      }

      dstBuffer.Write<Matrix>(world);
    }

    public void Read(ISavableReader input)
    {
      m_transpose = input.ReadBoolean("Transpose");
    }

    public void Write(ISavableWriter output)
    {
      output.Write("Transpose", m_transpose);
    }
  }

  [SavableVersion(1)]
  public sealed class WorldInverseTransposeInstanceDataProvider : IInstanceDataProvider
  {
    private static readonly VertexElement[] s_layout;
    private static readonly int s_dataSizeInBytes = Matrix.SizeInBytes;

    private bool m_transpose;

    public String InstanceDataName
    {
      get
      {
        return "WorldInverseTranspose";
      }
    }

    public VertexElement[] DataLayout
    {
      get
      {
        return s_layout;
      }
    }

    public int DataSizeInBytes
    {
      get
      {
        return s_dataSizeInBytes;
      }
    }

    public bool TransposeMatrix
    {
      get
      {
        return m_transpose;
      }
      set
      {
        m_transpose = value;
      }
    }

    public WorldInverseTransposeInstanceDataProvider() : this(true) { }

    public WorldInverseTransposeInstanceDataProvider(bool transpose)
    {
      m_transpose = transpose;
    }

    static WorldInverseTransposeInstanceDataProvider()
    {
      s_layout = new VertexElement[]
      {
                new VertexElement(VertexSemantic.TextureCoordinate, 0, VertexFormat.Float4, 0),
                new VertexElement(VertexSemantic.TextureCoordinate, 1, VertexFormat.Float4, 16),
                new VertexElement(VertexSemantic.TextureCoordinate, 2, VertexFormat.Float4, 32),
                new VertexElement(VertexSemantic.TextureCoordinate, 3, VertexFormat.Float4, 48)
      };
    }

    public void SetData(InstanceDefinition instanceDef, RenderPropertyCollection properties, Span<byte> dataBuffer)
    {
      Matrix world = Matrix.Identity;

      WorldTransformProperty prop;
      if (properties.TryGet<WorldTransformProperty>(out prop))
      {
        world = prop.Value.Matrix;

        world.Invert();

        //Yeah I know...basically, if we want to transpose to account for row major, then the world inverse is already correct.
        if (!m_transpose)
          world.Transpose();
      }

      dataBuffer.Write<Matrix>(world);
    }

    public void Read(ISavableReader input)
    {
      m_transpose = input.ReadBoolean("Transpose");
    }

    public void Write(ISavableWriter output)
    {
      output.Write("Transpose", m_transpose);
    }
  }
}
