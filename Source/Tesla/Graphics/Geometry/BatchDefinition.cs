﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// TODO NULLABLE

namespace Tesla.Graphics
{
  public class BatchDefinition<T> : IMarkedRenderable
  {
    private Transform m_worldTransform;
    private LightCollection m_worldLights;
    private RenderPropertyCollection m_renderProperties;

    private T m_batcher;
    private Action<IRenderContext, T> m_begin;
    private Action<T> m_end;
    private List<IBatchRenderable<T>> m_batchesToDraw;
    private MaterialDefinition m_matDef;

    public MaterialDefinition MaterialDefinition
    {
      get
      {
        return m_matDef;
      }
      set
      {
        m_matDef = value;
      }
    }

    public Transform WorldTransform
    {
      get
      {

        return m_worldTransform;
      }
    }

    public RenderPropertyCollection RenderProperties
    {
      get
      {
        return m_renderProperties;
      }
    }

    public bool IsValidForDraw
    {
      get
      {
        return m_batchesToDraw.Count > 0;
      }
    }

    public T Batcher
    {
      get
      {
        return m_batcher;
      }
    }

    public IReadOnlyList<IBatchRenderable<T>> BatchesToDraw
    {
      get
      {
        return m_batchesToDraw;
      }
    }

    public BatchDefinition(T batcher, Action<IRenderContext, T> beginDelegate, Action<T> endDelegate)
    {
      if (batcher == null)
        throw new ArgumentNullException("batcher");

      if (beginDelegate == null)
        throw new ArgumentNullException("beginDelegate");

      if (endDelegate == null)
        throw new ArgumentNullException("endDelegate");

      m_batcher = batcher;
      m_begin = beginDelegate;
      m_end = endDelegate;
      m_batchesToDraw = new List<IBatchRenderable<T>>();
      m_matDef = new MaterialDefinition();
      m_matDef.Add(RenderBucketID.Opaque, null); //Set default to opaque, no material
      m_worldTransform = new Transform();
      m_worldLights = new LightCollection();
      m_renderProperties = new RenderPropertyCollection();

      SetDefaultRenderProperties();
    }

    public void SetupDrawCall(IRenderContext renderContext, RenderBucketID currentBucketID, MaterialPass currentPass)
    {
      if (m_batchesToDraw.Count == 0)
        return;

      m_begin(renderContext, m_batcher);

      for (int i = 0; i < m_batchesToDraw.Count; i++)
        m_batchesToDraw[i].Draw(renderContext, m_batcher);

      m_end(m_batcher);
    }

    public void NotifyToDraw(IBatchRenderable<T> batchRenderable)
    {
      if (batchRenderable == null)
        return;

      m_batchesToDraw.Add(batchRenderable);
    }

    protected void SetDefaultRenderProperties()
    {
      m_renderProperties.Add(new WorldTransformProperty(m_worldTransform));
      m_renderProperties.Add(new LightCollectionProperty(m_worldLights));
      m_renderProperties.Add(new OrthoOrderProperty(0));
    }

    void IMarkedRenderable.OnMarkCleared(MarkID id, RenderQueue queue)
    {
      m_batchesToDraw.Clear();
    }
  }
}
