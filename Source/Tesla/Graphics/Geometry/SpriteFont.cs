﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using Tesla.Content;

// TODO NULLABLE

namespace Tesla.Graphics
{
  /// <summary>
  /// Represents a font that is used to render text as a series of sprites on the screen. Each sprite is a single character represented by a sub-image from
  /// a single or multiple glyph texture atlasses and includes spacing data such as kernings to properly render text to the screen.
  /// </summary>
  [SavableVersion(1)]
  public sealed class SpriteFont : ISavable, INamable
  {
    private static readonly Texture2D[] EmptyTextures = new Texture2D[0];

    private String m_name;
    private Dictionary<char, Glyph> m_glyphs;
    private Dictionary<char, Dictionary<char, int>> m_kerningMap;
    private int m_lineHeight;
    private int m_horizPadding;
    private Texture2D[] m_textures;
    private char? m_defaultChar;

    /// <summary>
    /// Gets or sets the name of the object.
    /// </summary>
    public String Name
    {
      get
      {
        return m_name;
      }
      set
      {
        m_name = value;
      }
    }

    /// <summary>
    /// Gets the default character that is rendered if a character in text being rendered is missing. May be null.
    /// </summary>
    public char? DefaultCharacter
    {
      get
      {
        return m_defaultChar;
      }
    }

    /// <summary>
    /// Gets the vertical distance between consecutive lines of text, which includes the blank space between the lines and the height of the characters on the line, in pixels.
    /// </summary>
    public int LineHeight
    {
      get
      {
        return m_lineHeight;
      }
      set
      {
        m_lineHeight = value;
      }
    }

    /// <summary>
    /// Gets the horizontal padding between characters on the same line, in pixels.
    /// </summary>
    public int HorizontalPadding
    {
      get
      {
        return m_horizPadding;
      }
      set
      {
        m_horizPadding = value;
      }
    }

    /// <summary>
    /// Gets the textures that hold the textural glyph information used during rendering.
    /// </summary>
    public IReadOnlyList<Texture2D> Textures
    {
      get
      {
        return m_textures;
      }
    }

    /// <summary>
    /// Gets the characters and their glyph data represented in this sprite font.
    /// </summary>
    public IReadOnlyDictionary<char, Glyph> Characters
    {
      get
      {
        return m_glyphs;
      }
    }

    /// <summary>
    /// Prevents a default instance of the <see cref="SpriteFont"/> class from being created.
    /// </summary>
    private SpriteFont() : this(0, 0, null, EmptyTextures) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="SpriteFont"/> class.
    /// </summary>
    /// <param name="lineHeight">Vertical height between consecutive lines, includes blank space and height of the characters on the line.</param>
    /// <param name="textures">Array of textures that glyphs in the font map to.</param>
    public SpriteFont(int lineHeight, params Texture2D[] textures) : this(lineHeight, 0, null, textures) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="SpriteFont"/> class.
    /// </summary>
    /// <param name="lineHeight">Vertical height between consecutive lines, includes blank space and height of the characters on the line.</param>
    /// <param name="defaultChar">Default character, if any.</param>
    /// <param name="textures">Array of textures that glyphs in the font map to.</param>
    public SpriteFont(int lineHeight, Glyph? defaultChar, params Texture2D[] textures) : this(lineHeight, 0, defaultChar, textures) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="SpriteFont"/> class.
    /// </summary>
    /// <param name="lineHeight">Vertical height between consecutive lines, includes blank space and height of the characters on the line.</param>
    /// <param name="horizontalPadding">Horizontal padding between consecutive characters on the same line.</param>
    /// <param name="textures">Array of textures that glyphs in the font map to.</param>
    public SpriteFont(int lineHeight, int horizontalPadding, params Texture2D[] textures) : this(lineHeight, horizontalPadding, null, textures) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="SpriteFont"/> class.
    /// </summary>
    /// <param name="lineHeight">Vertical height between consecutive lines, includes blank space and height of the characters on the line.</param>
    /// <param name="horizontalPadding">Horizontal padding between consecutive characters on the same line.</param>
    /// <param name="defaultChar">Default character, if any.</param>
    /// <param name="textures">Array of textures that glyphs in the font map to.</param>
    public SpriteFont(int lineHeight, int horizontalPadding, Glyph? defaultChar, params Texture2D[] textures)
    {
      m_name = String.Empty;
      m_glyphs = new Dictionary<char, Glyph>();
      m_kerningMap = new Dictionary<char, Dictionary<char, int>>();
      m_lineHeight = lineHeight;
      m_horizPadding = horizontalPadding;
      m_defaultChar = null;
      m_textures = (textures == null) ? EmptyTextures : textures;

      if (defaultChar.HasValue)
      {
        m_defaultChar = defaultChar.Value.Literal;
        m_glyphs.Add(defaultChar.Value.Literal, defaultChar.Value);
      }
    }

    /// <summary>
    /// Sets the default character of the sprite font, this is used if a character to be rendered
    /// is not present in the font.
    /// </summary>
    /// <param name="glyph">Glyph representing the default character.</param>
    /// <returns>True if the operation was successful, false otherwise.</returns>
    public bool SetDefaultCharacter(Glyph glyph)
    {
      if (!m_defaultChar.HasValue)
      {
        AddGlyph(glyph);
        m_defaultChar = glyph.Literal;
        return true;
      }

      if (m_defaultChar.Value == glyph.Literal)
        return false;

      RemoveGlyph(m_defaultChar.Value);
      return AddGlyph(glyph);
    }

    /// <summary>
    /// Adds a glyph to the sprite font.
    /// </summary>
    /// <param name="glyph">Glyph to add</param>
    public bool AddGlyph(Glyph glyph)
    {
      if (m_glyphs.ContainsKey(glyph.Literal))
        return false;

      m_glyphs.Add(glyph.Literal, glyph);
      return true;
    }

    /// <summary>
    /// Removes a glyph from the sprite font.
    /// </summary>
    /// <param name="literal">Character to remove</param>
    /// <returns>True if the operation was successful, false otherwise.</returns>
    public bool RemoveGlyph(char literal)
    {
      bool removed = m_glyphs.Remove(literal);

      if (removed)
        m_kerningMap.Remove(literal);

      return removed;
    }

    /// <summary>
    /// Adds a kerning pair to the sprite font. Kerning is the process of adjusting a character's horizontal space
    /// with the preceeding character.
    /// </summary>
    /// <param name="second">Current character.</param>
    /// <param name="first">Preceeding character.</param>
    /// <param name="amount">Amount to move the current character horizontally.</param>
    /// <returns>True if the operation was successful, false otherwise.</returns>
    public bool AddKerning(char second, char first, int amount)
    {
      Dictionary<char, int> kernings;
      if (!m_kerningMap.TryGetValue(second, out kernings))
      {
        kernings = new Dictionary<char, int>();
        m_kerningMap.Add(second, kernings);
      }

      if (kernings.ContainsKey(first))
        return false;

      kernings.Add(first, amount);
      return true;
    }

    /// <summary>
    /// Removes a kerning pair from the sprite font.
    /// </summary>
    /// <param name="second">Current character.</param>
    /// <param name="first">Preceeding character.</param>
    /// <returns>True if the operation was succesful, false otherwise.</returns>
    public bool RemoveKerning(char second, char first)
    {
      Dictionary<char, int> kernings;
      if (!m_kerningMap.TryGetValue(second, out kernings))
        return true;

      return kernings.Remove(first);
    }

    /// <summary>
    /// Queries if a kerning pair is contained in the sprite font.
    /// </summary>
    /// <param name="second">Current character.</param>
    /// <param name="first">Preceeding character.</param>
    /// <returns>True if the kerning pair is present in the sprite font, false otherwise.</returns>
    public bool ContainsKerning(char second, char first)
    {
      Dictionary<char, int> kernings;
      if (!m_kerningMap.TryGetValue(second, out kernings))
        return false;

      return kernings.ContainsKey(first);
    }

    /// <summary>
    /// Gets the kerning amount for the kerning pair.
    /// </summary>
    /// <param name="second">Current character.</param>
    /// <param name="first">Preceeding character.</param>
    /// <returns>Kerning amount between the pair of characters.</returns>
    public int GetKerning(char second, char first)
    {
      Dictionary<char, int> kernings;
      if (m_kerningMap.TryGetValue(second, out kernings))
      {
        int amount;
        if (kernings.TryGetValue(first, out amount))
          return amount;
      }

      return 0;
    }

    /// <summary>
    /// Gets all the kerning pairs corresponding to the character literal.
    /// </summary>
    /// <param name="second">The character literal, which is the "second" character in the kerning pair.</param>
    /// <returns>Array of kerning pairs.</returns>
    public KerningPair[] GetKernings(char second)
    {
      Dictionary<char, int> kernings;
      if (m_kerningMap.TryGetValue(second, out kernings))
      {
        KerningPair[] pairs = new KerningPair[kernings.Count];
        int index = 0;
        foreach (KeyValuePair<char, int> kv in kernings)
        {
          pairs[index] = new KerningPair(kv.Key, second, kv.Value);
          index++;
        }
      }

      return new KerningPair[0];
    }

    /// <summary>
    /// Measures the string, returning the maximum width and height (stored in X and Y respectively)
    /// of the string as it would appear when rendered.
    /// </summary>
    /// <param name="text">Text to measure</param>
    /// <returns>Maximum width and height stored in X and Y coordinates.</returns>
    public Vector2 MeasureString(String text)
    {
      Vector2 result;
      MeasureStringInternal(new StringReference(text), out result);

      return result;
    }

    /// <summary>
    /// Measures the string, returning the maximum width and height (stored in X and Y respectively)
    /// of the string as it would appear when rendered.
    /// </summary>
    /// <param name="text">Text to measure</param>
    /// <returns>Maximum width and height stored in X and Y coordinates.</returns>
    public Vector2 MeasureString(StringBuilder text)
    {
      Vector2 result;
      MeasureStringInternal(new StringReference(text), out result);

      return result;
    }

    internal void DrawString(SpriteBatch batch, StringReference text, Vector2 position, Vector2 scale, Color tintColor, Angle angle, Vector2 origin, SpriteFlipEffect flipEffect, float depth)
    {
      //TODO - Angle, Scale aren't quite right. And need to implement flip effect

      Matrix transform;
      Matrix translation;
      Matrix.FromRotationZ(angle, out transform);
      Matrix.FromTranslation(-origin.X * scale.X, -origin.Y * scale.Y, 0.0f, out translation);
      Matrix.Multiply(translation, transform, out transform);

      float lx = 0.0f;
      float ly = 0.0f;

      char prevChar = '\0';
      for (int i = 0; i < text.Length; i++)
      {
        char literal = text[i];
        switch (literal)
        {
          case '\r':
            break;
          case '\n':
            lx = 0.0f;
            ly += LineHeight;
            break;
          default:
            Glyph glyph;
            bool hasIt = true;
            if (!m_glyphs.TryGetValue(literal, out glyph))
            {
              if (m_defaultChar.HasValue)
              {
                glyph = m_glyphs[m_defaultChar.Value];
              }
              else
              {
                hasIt = false;
              }
            }

            if (hasIt)
            {
              Vector2 pos = new Vector2(lx + glyph.Offset.X, ly + glyph.Offset.Y);
              Vector2.Transform(pos, transform, out pos);

              pos.X += position.X;
              pos.Y += position.Y;

              batch.Draw(m_textures[glyph.TextureIndex], pos, scale, glyph.Cropping, tintColor, angle, Vector2.Zero, flipEffect, depth);

              lx += ComputeAdvance(ref glyph, ref prevChar);
            }

            prevChar = literal;
            break;
        }
      }
    }

    private void MeasureStringInternal(StringReference stringRef, out Vector2 result)
    {
      result = Vector2.Zero;

      if (stringRef.Length == 0)
        return;

      float maxLineWidth = 0.0f;
      int newLineCount = 1;
      char prevChar = '\0';

      for (int i = 0; i < stringRef.Length; i++)
      {
        char literal = stringRef[i];
        switch (literal)
        {
          case '\r':
            break;
          case '\n':
            maxLineWidth = Math.Max(result.X, maxLineWidth);

            result.X = 0.0f;

            newLineCount++;
            break;
          default:
            Glyph glyph;
            bool hasIt = true;
            if (!m_glyphs.TryGetValue(literal, out glyph))
            {
              if (m_defaultChar.HasValue)
              {
                glyph = m_glyphs[m_defaultChar.Value];
              }
              else
              {
                hasIt = false;
              }
            }

            if (hasIt)
              result.X += ComputeAdvance(ref glyph, ref prevChar);

            break;
        }

        prevChar = literal;
      }

      result.X = Math.Max(result.X, maxLineWidth);
      result.Y = newLineCount * m_lineHeight;
    }

    private float ComputeAdvance(ref Glyph g, ref char prevChar)
    {
      return g.XAdvance + GetKerning(g.Literal, prevChar) + m_horizPadding;
    }

    /// <summary>
    /// Reads the object data from the input.
    /// </summary>
    /// <param name="input">Savable reader</param>
    public void Read(ISavableReader input)
    {
      m_name = input.ReadString("Name");
      m_textures = input.ReadSharedSavableArray<Texture2D>("Textures");
      if (m_textures == null)
        m_textures = EmptyTextures;


      if (input.ReadBoolean("HasDefaultCharacter"))
        m_defaultChar = input.ReadChar("DefaultCharacter");
      else
        m_defaultChar = null;

      m_lineHeight = input.ReadInt32("VerticalSpacing");
      m_horizPadding = input.ReadInt32("HorizontalSpacing");

      int glyphCount = input.BeginReadGroup("Glyphs");
      m_glyphs.Clear();
      m_kerningMap.Clear();

      for (int i = 0; i < glyphCount; i++)
      {
        input.BeginReadGroup("Item");

        Glyph g = input.Read<Glyph>("Glyph");
        m_glyphs.Add(g.Literal, g);

        int kerningCount = input.BeginReadGroup("KerningPairs");

        if (kerningCount > 0)
        {
          Dictionary<char, int> kernings = new Dictionary<char, int>();
          m_kerningMap.Add(g.Literal, kernings);

          for (int j = 0; j < kerningCount; j++)
          {
            input.BeginReadGroup("KerningPair");

            char firstChar = input.ReadChar("FirstCharacter");
            int kerningAmount = input.ReadInt32("KerningAmount");

            kernings.Add(firstChar, kerningAmount);

            input.EndReadGroup();
          }
        }

        input.EndReadGroup();
        input.EndReadGroup();
      }

      input.EndReadGroup();
    }

    /// <summary>
    /// Writes the object data to the output.
    /// </summary>
    /// <param name="output">Savable writer</param>
    public void Write(ISavableWriter output)
    {
      output.Write("Name", m_name);
      output.WriteSharedSavable<Texture2D>("Textures", m_textures);

      output.Write("HasDefaultCharacter", m_defaultChar.HasValue);

      if (m_defaultChar.HasValue)
        output.Write("DefaultCharacter", m_defaultChar.Value);

      output.Write("VerticalSpacing", m_lineHeight);
      output.Write("HorizontalSpacing", m_horizPadding);

      output.BeginWriteGroup("Glyphs", m_glyphs.Count);

      foreach (Glyph glyph in m_glyphs.Values)
      {
        output.BeginWriteGroup("Item");

        output.Write<Glyph>("Glyph", glyph);
        Dictionary<char, int> kernings;
        m_kerningMap.TryGetValue(glyph.Literal, out kernings);

        int kerningCount = (kernings != null) ? kernings.Count : 0;
        output.BeginWriteGroup("KerningPairs", kerningCount);

        if (kerningCount > 0)
        {
          foreach (KeyValuePair<char, int> kv in kernings)
          {
            output.BeginWriteGroup("KerningPair");

            output.Write("FirstCharacter", kv.Key);
            output.Write("KerningAmount", kv.Value);

            output.EndWriteGroup();
          }
        }

        output.EndWriteGroup();
        output.EndWriteGroup();
      }

      output.EndWriteGroup();
    }
  }

  /// <summary>
  /// Represents a kerning amount between two characters. The kerning is the amount of 
  /// space on the X axis between the "second" character and the preceeding "first" character.
  /// </summary>
  public struct KerningPair : IPrimitiveValue
  {
    /// <summary>
    /// First character in the pair.
    /// </summary>
    public char FirstCharacter;

    /// <summary>
    /// Second character in the pair.
    /// </summary>
    public char SecondCharacter;

    /// <summary>
    /// Kerning amount between the second and first characters.
    /// </summary>
    public int KerningAmount;

    /// <summary>
    /// Constructs a new instance of the <see cref="KerningPair"/> struct.
    /// </summary>
    /// <param name="firstChar">The first character.</param>
    /// <param name="secondChar">The second character.</param>
    /// <param name="amount">The kerning amount between the second character and the first.</param>
    public KerningPair(char firstChar, char secondChar, int amount)
    {
      FirstCharacter = firstChar;
      SecondCharacter = secondChar;
      KerningAmount = amount;
    }

    /// <summary>
    /// Writes the primitive data to the output.
    /// </summary>
    /// <param name="output">Primitive writer.</param>
    public readonly void Write(IPrimitiveWriter output)
    {
      output.Write("FirstCharacter", FirstCharacter);
      output.Write("SecondCharacter", SecondCharacter);
      output.Write("KerningAmount", KerningAmount);
    }

    /// <summary>
    /// Reads the primitive data from the input.
    /// </summary>
    /// <param name="input">Primitive reader.</param>
    public void Read(IPrimitiveReader input)
    {
      FirstCharacter = input.ReadChar("FirstCharacter");
      SecondCharacter = input.ReadChar("SecondCharacter");
      KerningAmount = input.ReadInt32("KerningAmount");
    }
  }

  /// <summary>
  /// Represents a single glyph and its layout information found on a texture.
  /// </summary>
  [StructLayout(LayoutKind.Sequential)]
  public struct Glyph : IPrimitiveValue
  {
    /// <summary>
    /// Character the glyph corresponds to.
    /// </summary>
    public char Literal;

    /// <summary>
    /// Cropping rectangle in pixels, the sub-image representing the glyph from the texture.
    /// </summary>
    public Rectangle Cropping;

    /// <summary>
    /// The index in the sprite font representing which texture the glyph is found on.
    /// </summary>
    public int TextureIndex;

    /// <summary>
    /// Represents the top-left origin of the glyph quad in pixels.
    /// </summary>
    public Vector2 Offset;

    /// <summary>
    /// The amount of space to add from the origin of the glyph quad to the next glyph's top-left origin along the X axis in pixels.
    /// </summary>
    public float XAdvance;

    /// <summary>
    /// Constructs a new instance of the <see cref="Glyph"/> struct.
    /// </summary>
    /// <param name="literal">The character literal.</param>
    /// <param name="cropping">The cropping rectangle, in pixels.</param>
    /// <param name="textureIndex">The texture index the glyph is found on.</param>
    /// <param name="offset">The offset representing the top-left corner of the glyph.</param>
    /// <param name="xAdvance">The amount to advance to the next glyph along the X axis.</param>
    public Glyph(char literal, Rectangle cropping, int textureIndex, Vector2 offset, float xAdvance)
    {
      Literal = literal;
      Cropping = cropping;
      TextureIndex = textureIndex;
      Offset = offset;
      XAdvance = xAdvance;
    }

    /// <summary>
    /// Writes the primitive data to the output.
    /// </summary>
    /// <param name="output">Primitive writer.</param>
    public readonly void Write(IPrimitiveWriter output)
    {
      output.Write("Literal", Literal);
      output.Write<Rectangle>("Cropping", Cropping);
      output.Write("TextureIndex", TextureIndex);
      output.Write<Vector2>("Offset", Offset);
      output.Write("XAdvance", XAdvance);
    }

    /// <summary>
    /// Reads the primitive data from the input.
    /// </summary>
    /// <param name="input">Primitive reader.</param>
    public void Read(IPrimitiveReader input)
    {
      Literal = input.ReadChar("Literal");
      input.Read<Rectangle>("Cropping", out Cropping);
      TextureIndex = input.ReadInt32("TextureIndex");
      input.Read<Vector2>("Offset", out Offset);
      XAdvance = input.ReadSingle("XAdvance");
    }
  }

  [StructLayout(LayoutKind.Sequential)]
  internal readonly struct StringReference
  {
    private readonly String m_string;
    private readonly StringBuilder m_stringBuilder;

    public char this[int index]
    {
      get
      {
        if (m_string != null)
          return m_string[index];
        else if (m_stringBuilder != null)
          return m_stringBuilder[index];

        return '\0';
      }
    }

    public int Length
    {
      get
      {
        if (m_string != null)
          return m_string.Length;
        else if (m_stringBuilder != null)
          return m_stringBuilder.Length;

        return 0;
      }
    }

    public StringReference(String str)
    {
      m_string = str;
      m_stringBuilder = null;
    }

    public StringReference(StringBuilder strBuilder)
    {
      m_string = null;
      m_stringBuilder = strBuilder;
    }
  }
}
