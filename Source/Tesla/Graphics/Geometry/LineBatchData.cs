﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// TODO NULLABLE

namespace Tesla.Graphics
{
  public sealed class PointBatchData : ILineBatchData
  {
    public struct PointData
    {
      public Vector3 Position;
      public Color Color;
      public float Size;

      public PointData(Vector3 pos, Color color, float size)
      {
        Position = pos;
        Color = color;
        Size = size;
      }
    }

    private DataBuffer<VertexPositionColor> m_data;
    private BoundingBox.Data m_range;

    public BoundingBox.Data Range
    {
      get
      {
        return m_range;
      }
    }

    public DataBuffer<VertexPositionColor> Points
    {
      get
      {
        return m_data;
      }
    }

    public PointBatchData(int capacity, MemoryAllocatorStrategy allocatorStrategy = MemoryAllocatorStrategy.Default)
    {
      m_data = DataBuffer.Create<VertexPositionColor>(capacity, allocatorStrategy);
    }

    public void Resize(int capacity)
    {
      if (capacity <= m_data.Length)
        return;

      m_data.Resize(capacity);
    }
    public void UpdateRange()
    {
      m_range = new BoundingBox.Data();
      // m_range = BoundingBox.Data.FromPoints(m_positions); // TODO: Need something easy to expand a box from just the point attributes
    }

    public void Draw(IRenderContext renderContext, LineBatch batcher)
    {

    }
  }

  public sealed class LineBatchData : ILineBatchData
  {
    private DataBuffer<Vector3> m_positions;
    private DataBuffer<Color> m_colors;
    private float m_startThickness;
    private float m_endThickness;
    private LineTopology m_lineTopology;
    private LineJoinMode m_joinMode;
    private BoundingBox.Data m_range;

    public float StartThickness
    {
      get
      {
        return m_startThickness;
      }
      set
      {
        m_startThickness = value;
      }
    }

    public float EndThickness
    {
      get
      {
        return m_endThickness;
      }
      set
      {
        m_endThickness = value;
      }
    }

    public LineTopology LineTopology
    {
      get
      {
        return m_lineTopology;
      }
      set
      {
        m_lineTopology = value;
      }
    }

    public LineJoinMode JoinMode
    {
      get
      {
        return m_joinMode;
      }
      set
      {
        m_joinMode = value;
      }
    }

    public DataBuffer<Vector3> Positions
    {
      get
      {
        return m_positions;
      }
    }

    public DataBuffer<Color> Colors
    {
      get
      {
        return m_colors;
      }
    }

    public BoundingBox.Data Range
    {
      get
      {
        return m_range;
      }
    }

    public LineBatchData(int capacity, MemoryAllocatorStrategy allocatorStrategy = MemoryAllocatorStrategy.Default)
    {
      m_positions = DataBuffer.Create<Vector3>(capacity, allocatorStrategy);
      m_colors = DataBuffer.Create<Color>(capacity, allocatorStrategy);

      for (int i = 0; i < capacity; i++)
        m_colors[i] = Color.White;
    }

    public void Resize(int capacity)
    {
      if (capacity <= m_positions.Length)
        return;

      m_positions.Resize(capacity);
      m_colors.Resize(capacity);
    }

    public void UpdateRange()
    {
      m_range = BoundingBox.Data.FromPoints(m_positions);
    }

    public void Draw(IRenderContext renderContext, LineBatch batcher)
    {
      batcher.Draw(m_lineTopology, m_joinMode, m_positions, m_colors, m_startThickness, m_endThickness);
    }
  }
}
