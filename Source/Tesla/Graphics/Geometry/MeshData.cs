﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using Tesla.Content;

// TODO NULLABLE

namespace Tesla.Graphics
{
  /// <summary>
  /// Represents a mesh of primitives that are organized in a well defined topology (e.g. triangle lists). The data structure stores both CPU and GPU buffers - a CPU buffer for each vertex attribute, and 
  /// an interleaved GPU vertex buffer. Optionally all vertices can be indexed in order to share duplicate vertices between primitives. Each vertex attribute buffer must have the same size as each other, typically
  /// driven by the contents of the <see cref="Positions"/> buffer. The CPU buffers can be used for CPU queries such as picking.
  /// </summary>
  [SavableVersion(1)]
  public sealed class MeshData : ISavable, IDeepCloneable, IEnumerable<VertexAttributeData>
  {
    private VertexBuffer m_vertexBuffer;
    private IndexBuffer m_indexBuffer;

    private SortedDictionary<TupleKey<VertexSemantic, int>, VertexStream> m_bufferMap;
    private IndexData? m_indices;
    private PrimitiveType m_primitiveType;
    private int m_vertexCount;
    private int m_primitiveCount;

    private bool m_useIndexedPrimitives;
    private bool m_useDynamicVertexBuffer;
    private bool m_useDynamicIndexBuffer;
    private bool m_vbIsDirty;
    private bool m_ibIsDirty;

    /// <summary>
    /// Gets the mesh's GPU vertex buffer.
    /// </summary>
    public VertexBuffer VertexBuffer
    {
      get
      {
        return m_vertexBuffer;
      }
    }

    /// <summary>
    /// Gets the mesh's GPU index buffer.
    /// </summary>
    public IndexBuffer IndexBuffer
    {
      get
      {
        return m_indexBuffer;
      }
    }

    /// <summary>
    /// Gets if the mesh is using indices (if at all) that are 16-bit in size.
    /// </summary>
    public bool IsUsingShortIndices
    {
      get
      {
        if (m_indices.HasValue && m_indices.Value.IndexFormat == IndexFormat.SixteenBits)
          return true;

        return false;
      }
    }

    /// <summary>
    /// Gets if the vertex buffer needs to be recompiled due to data change (such as the number of vertices change or vertex elements are added/removed).
    /// </summary>
    public bool IsVertexBufferDirty
    {
      get
      {
        return m_vbIsDirty;
      }
    }

    /// <summary>
    /// Gets if the index buffer needs to be recompiled due to data change (such as the number of indices change).
    /// </summary>
    public bool IsIndexBufferDirty
    {
      get
      {
        return m_ibIsDirty;
      }
    }

    /// <summary>
    /// Gets the total number of vertices in the mesh.
    /// </summary>
    public int VertexCount
    {
      get
      {
        return m_vertexCount;
      }
    }

    /// <summary>
    /// Gets the total number of indices in the mesh.
    /// </summary>
    public int IndexCount
    {
      get
      {
        if (m_indices.HasValue)
          return m_indices.Value.Length;

        return 0;
      }
    }

    /// <summary>
    /// Gets the total number of primitives in the mesh.
    /// </summary>
    public int PrimitiveCount
    {
      get
      {
        return m_primitiveCount;
      }
    }

    /// <summary>
    /// Gets or sets if the geometry should be indexed data.
    /// </summary>
    public bool UseIndexedPrimitives
    {
      get
      {
        return m_useIndexedPrimitives;
      }
      set
      {
        if (m_useIndexedPrimitives != value)
        {
          m_useIndexedPrimitives = value;
          m_ibIsDirty = true;
        }
      }
    }

    /// <summary>
    /// Gets or sets if the mesh should use a dynamic vertex buffer.
    /// </summary>
    public bool UseDynamicVertexBuffer
    {
      get
      {
        return m_useDynamicVertexBuffer;
      }
      set
      {
        if (m_useDynamicVertexBuffer != value)
        {
          m_useDynamicVertexBuffer = value;
          m_vbIsDirty = true;
        }
      }
    }

    /// <summary>
    /// Gets or sets if the mesh should use a dynamic index buffer.
    /// </summary>
    public bool UseDynamicIndexBuffer
    {
      get
      {
        return m_useDynamicIndexBuffer;
      }
      set
      {
        if (m_useDynamicIndexBuffer != value)
        {
          m_useDynamicIndexBuffer = value;
          m_ibIsDirty = true;
        }
      }
    }

    /// <summary>
    /// Gets or sets the geometric primitive type of the mesh.
    /// </summary>
    public PrimitiveType PrimitiveType
    {
      get
      {
        return m_primitiveType;
      }
      set
      {
        if (m_primitiveType != value)
        {
          m_primitiveType = value;
          m_vbIsDirty = true;
          m_ibIsDirty = true;
        }
      }
    }

    /// <summary>
    /// Queries if the data constists are lines, either <see cref="PrimitiveType.LineList"/> -or- <see cref="PrimitiveType.LineStrip"/>.
    /// </summary>
    public bool HasLines
    {
      get
      {
        switch (m_primitiveType)
        {
          case PrimitiveType.LineList:
          case PrimitiveType.LineStrip:
            return true;
          default:
            return false;
        }
      }
    }

    /// <summary>
    /// Queries if the data constists are triangles, either <see cref="PrimitiveType.TriangleList"/> -or- <see cref="PrimitiveType.TriangleStrip"/>.
    /// </summary>
    public bool HasTriangles
    {
      get
      {
        switch (m_primitiveType)
        {
          case PrimitiveType.TriangleList:
          case PrimitiveType.TriangleStrip:
            return true;
          default:
            return false;
        }
      }
    }

    /// <summary>
    /// Queries if the data constists of type <see cref="PrimitiveType.PointList"/>
    /// </summary>
    public bool HasPoints
    {
      get
      {
        return m_primitiveType == PrimitiveType.PointList;
      }
    }

    /// <summary>
    /// Gets the number of vertex element buffers contained in the mesh.
    /// </summary>
    public int BufferCount
    {
      get
      {
        return m_bufferMap.Count;
      }
    }

    #region Data Buffers

    /// <summary>
    /// Queries if there exists valid positional data (e.g. if non-indexed, <see cref="Positions"/> must exist, if indexed <see cref="Indices"/> must also exist.
    /// </summary>
    public bool HasValidPositions
    {
      get
      {
        if (Positions == null || (m_useIndexedPrimitives && m_indices == null))
          return false;

        return true;
      }
    }

    /// <summary>
    /// Gets or sets the indices of the mesh. This also sets the property <seealso cref="UseIndexedPrimitives"/> based on the validity of the index buffer.
    /// </summary>
    public IndexData? Indices
    {
      get
      {
        return m_indices;
      }
      set
      {
        m_indices = value;
        m_useIndexedPrimitives = (m_indices.HasValue) ? true : false;
        m_ibIsDirty = true;
      }
    }

    /// <summary>
    /// Gets or sets the vertex positions of the mesh (Semantic: Position, SemanticIndex: 0).
    /// </summary>
    public DataBuffer<Vector3> Positions
    {
      get
      {
        TupleKey<VertexSemantic, int> key = new TupleKey<VertexSemantic, int>(VertexSemantic.Position, 0);
        VertexStream vs;
        if (m_bufferMap.TryGetValue(key, out vs))
          return vs.Data as DataBuffer<Vector3>;

        return null;
      }
      set
      {
        TupleKey<VertexSemantic, int> key = new TupleKey<VertexSemantic, int>(VertexSemantic.Position, 0);

        if (value == null)
          m_bufferMap.Remove(key);
        else
          m_bufferMap[key] = new VertexStream(VertexFormat.Float3, value);

        m_vbIsDirty = true;
      }
    }

    /// <summary>
    /// Gets or sets the vertex normals of the mesh (Semantic: Normal, SemanticIndex: 0).
    /// </summary>
    public DataBuffer<Vector3> Normals
    {
      get
      {
        TupleKey<VertexSemantic, int> key = new TupleKey<VertexSemantic, int>(VertexSemantic.Normal, 0);
        VertexStream vs;
        if (m_bufferMap.TryGetValue(key, out vs))
          return vs.Data as DataBuffer<Vector3>;

        return null;
      }
      set
      {
        TupleKey<VertexSemantic, int> key = new TupleKey<VertexSemantic, int>(VertexSemantic.Normal, 0);

        if (value == null)
          m_bufferMap.Remove(key);
        else
          m_bufferMap[key] = new VertexStream(VertexFormat.Float3, value);

        m_vbIsDirty = true;
      }
    }

    /// <summary>
    /// Gets or sets the vertex colors of the mesh (Semantic: Color, SemanticIndex: 0).
    /// </summary>
    public DataBuffer<Color> Colors
    {
      get
      {
        TupleKey<VertexSemantic, int> key = new TupleKey<VertexSemantic, int>(VertexSemantic.Color, 0);
        VertexStream vs;
        if (m_bufferMap.TryGetValue(key, out vs))
          return vs.Data as DataBuffer<Color>;

        return null;
      }
      set
      {
        TupleKey<VertexSemantic, int> key = new TupleKey<VertexSemantic, int>(VertexSemantic.Color, 0);

        if (value == null)
          m_bufferMap.Remove(key);
        else
          m_bufferMap[key] = new VertexStream(VertexFormat.Color, value);

        m_vbIsDirty = true;
      }
    }

    /// <summary>
    /// Gets or sets the vertex texture coordinates of the mesh (Semantic: TextureCoordinate, SemanticIndex: 0).
    /// </summary>
    public DataBuffer<Vector2> TextureCoordinates
    {
      get
      {
        TupleKey<VertexSemantic, int> key = new TupleKey<VertexSemantic, int>(VertexSemantic.TextureCoordinate, 0);
        VertexStream vs;
        if (m_bufferMap.TryGetValue(key, out vs))
          return vs.Data as DataBuffer<Vector2>;

        return null;
      }
      set
      {
        TupleKey<VertexSemantic, int> key = new TupleKey<VertexSemantic, int>(VertexSemantic.TextureCoordinate, 0);

        if (value == null)
          m_bufferMap.Remove(key);
        else
          m_bufferMap[key] = new VertexStream(VertexFormat.Float2, value);

        m_vbIsDirty = true;
      }
    }

    /// <summary>
    /// Gets or sets the vertex bitangents of the mesh (Semantic: Bitangent, SemanticIndex: 0).
    /// </summary>
    public DataBuffer<Vector3> Bitangents
    {
      get
      {
        TupleKey<VertexSemantic, int> key = new TupleKey<VertexSemantic, int>(VertexSemantic.Bitangent, 0);
        VertexStream vs;
        if (m_bufferMap.TryGetValue(key, out vs))
          return vs.Data as DataBuffer<Vector3>;

        return null;
      }
      set
      {
        TupleKey<VertexSemantic, int> key = new TupleKey<VertexSemantic, int>(VertexSemantic.Bitangent, 0);

        if (value == null)
          m_bufferMap.Remove(key);
        else
          m_bufferMap[key] = new VertexStream(VertexFormat.Float3, value);

        m_vbIsDirty = true;
      }
    }

    /// <summary>
    /// Gets or sets the vertex tangents of the mesh (Semantic: Tangent, SemanticIndex: 0).
    /// </summary>
    public DataBuffer<Vector3> Tangents
    {
      get
      {
        TupleKey<VertexSemantic, int> key = new TupleKey<VertexSemantic, int>(VertexSemantic.Tangent, 0);
        VertexStream vs;
        if (m_bufferMap.TryGetValue(key, out vs))
          return vs.Data as DataBuffer<Vector3>;

        return null;
      }
      set
      {
        TupleKey<VertexSemantic, int> key = new TupleKey<VertexSemantic, int>(VertexSemantic.Tangent, 0);

        if (value == null)
          m_bufferMap.Remove(key);
        else
          m_bufferMap[key] = new VertexStream(VertexFormat.Float3, value);

        m_vbIsDirty = true;
      }
    }

    /// <summary>
    /// Gets or sets the vertex blend indices of the mesh (Semantic: BlendIndices, SemanticIndex: 0).
    /// </summary>
    public DataBuffer<Vector4> BlendIndices
    {
      get
      {
        TupleKey<VertexSemantic, int> key = new TupleKey<VertexSemantic, int>(VertexSemantic.BlendIndices, 0);
        VertexStream vs;
        if (m_bufferMap.TryGetValue(key, out vs))
          return vs.Data as DataBuffer<Vector4>;

        return null;
      }
      set
      {
        TupleKey<VertexSemantic, int> key = new TupleKey<VertexSemantic, int>(VertexSemantic.BlendIndices, 0);

        if (value == null)
          m_bufferMap.Remove(key);
        else
          m_bufferMap[key] = new VertexStream(VertexFormat.Float4, value);

        m_vbIsDirty = true;
      }
    }

    /// <summary>
    /// Gets or sets the vertex blend weights of the mesh (Semantic: BlendWeight, SemanticIndex: 0).
    /// </summary>
    public DataBuffer<Vector4> BlendWeights
    {
      get
      {
        TupleKey<VertexSemantic, int> key = new TupleKey<VertexSemantic, int>(VertexSemantic.BlendWeight, 0);
        VertexStream vs;
        if (m_bufferMap.TryGetValue(key, out vs))
          return vs.Data as DataBuffer<Vector4>;

        return null;
      }
      set
      {
        TupleKey<VertexSemantic, int> key = new TupleKey<VertexSemantic, int>(VertexSemantic.BlendWeight, 0);

        if (value == null)
          m_bufferMap.Remove(key);
        else
          m_bufferMap[key] = new VertexStream(VertexFormat.Float4, value);

        m_vbIsDirty = true;
      }
    }

    #endregion

    public MeshData()
    {
      m_bufferMap = new SortedDictionary<TupleKey<VertexSemantic, int>, VertexStream>();
      m_primitiveType = PrimitiveType.TriangleList;
      m_indices = null;
      m_vertexBuffer = null;
      m_indexBuffer = null;
      m_useIndexedPrimitives = false;
      m_useDynamicIndexBuffer = false;
      m_useDynamicVertexBuffer = false;
      m_vbIsDirty = true;
      m_ibIsDirty = true;
      m_primitiveCount = 0;
      m_vertexCount = 0;
    }

    public void AddBuffer(VertexSemantic semantic, IDataBuffer buffer)
    {
      VertexFormat format = (buffer != null) ? VertexStream.InferVertexFormat(buffer) : VertexFormat.Float;
      AddBuffer(semantic, 0, format, buffer);
    }

    public void AddBuffer(VertexSemantic semantic, int index, IDataBuffer buffer)
    {
      VertexFormat format = (buffer != null) ? VertexStream.InferVertexFormat(buffer) : VertexFormat.Float;
      AddBuffer(semantic, index, format, buffer);
    }

    public void AddBuffer(VertexSemantic semantic, int index, VertexFormat format, IDataBuffer buffer)
    {
      if (buffer == null)
        throw new ArgumentNullException(StringLocalizer.Instance.GetLocalizedString("DataBufferNull"));

      TupleKey<VertexSemantic, int> key = new TupleKey<VertexSemantic, int>(semantic, index);
      if (m_bufferMap.ContainsKey(key))
        throw new ArgumentException(StringLocalizer.Instance.GetLocalizedString("KeyAlreadyPresent", key.ToString()));

      VertexStream vs = new VertexStream(format, buffer);
      m_bufferMap.Add(key, vs);

      m_vbIsDirty = true;
    }

    public bool RemoveBuffer(VertexSemantic semantic)
    {
      TupleKey<VertexSemantic, int> key = new TupleKey<VertexSemantic, int>(semantic, 0);
      return m_bufferMap.Remove(key);
    }

    public bool RemoveBuffer(VertexSemantic semantic, int index)
    {
      TupleKey<VertexSemantic, int> key = new TupleKey<VertexSemantic, int>(semantic, index);
      return m_bufferMap.Remove(key);
    }

    public DataBuffer<T> GetBuffer<T>(VertexSemantic semantic) where T : unmanaged
    {
      VertexStream vs;
      if (m_bufferMap.TryGetValue(new TupleKey<VertexSemantic, int>(semantic, 0), out vs))
        return vs.Data as DataBuffer<T>;

      return null;
    }

    public DataBuffer<T> GetBuffer<T>(VertexSemantic semantic, int index) where T : unmanaged
    {
      VertexStream vs;
      if (m_bufferMap.TryGetValue(new TupleKey<VertexSemantic, int>(semantic, index), out vs))
        return vs.Data as DataBuffer<T>;

      return null;
    }

    public IDataBuffer GetBuffer(VertexSemantic semantic)
    {
      VertexStream vs;
      if (m_bufferMap.TryGetValue(new TupleKey<VertexSemantic, int>(semantic, 0), out vs))
        return vs.Data;

      return null;
    }

    public IDataBuffer GetBuffer(VertexSemantic semantic, int index)
    {
      VertexStream vs;
      if (m_bufferMap.TryGetValue(new TupleKey<VertexSemantic, int>(semantic, index), out vs))
        return vs.Data;

      return null;
    }

    public bool ContainsBuffer(VertexSemantic semantic)
    {
      return m_bufferMap.ContainsKey(new TupleKey<VertexSemantic, int>(semantic, 0));
    }

    public bool ContainsBuffer(VertexSemantic semantic, int index)
    {
      return m_bufferMap.ContainsKey(new TupleKey<VertexSemantic, int>(semantic, index));
    }

    public void ClearData()
    {
      //Dispose the data buffers first
      foreach (KeyValuePair<TupleKey<VertexSemantic, int>, VertexStream> kv in m_bufferMap)
        kv.Value.Data.Dispose();

      if (m_indices.HasValue)
        m_indices.Value.Dispose();

      m_bufferMap.Clear();
      m_indices = null;
      m_useIndexedPrimitives = false;
      m_vbIsDirty = true;
      m_ibIsDirty = true;
      m_vertexCount = 0;
      m_primitiveCount = 0;

      if (m_vertexBuffer != null)
      {
        m_vertexBuffer.Dispose();
        m_vertexBuffer = null;
      }

      if (m_indexBuffer != null)
      {
        m_indexBuffer.Dispose();
        m_indexBuffer = null;
      }
    }

    public MeshData Clone()
    {
      MeshData mshd = new MeshData();

      foreach (KeyValuePair<TupleKey<VertexSemantic, int>, VertexStream> kv in m_bufferMap)
      {
        VertexStream strm = kv.Value;

        VertexStream newStrm = new VertexStream();
        newStrm.Data = strm.Data.Clone();
        newStrm.Format = strm.Format;

        mshd.m_bufferMap.Add(kv.Key, strm);
      }

      if (m_indices.HasValue)
        mshd.m_indices = m_indices.Value.Clone();

      mshd.m_primitiveType = m_primitiveType;
      mshd.m_vertexCount = m_vertexCount;
      mshd.m_primitiveCount = m_primitiveCount;

      mshd.m_useIndexedPrimitives = m_useIndexedPrimitives;
      mshd.m_useDynamicVertexBuffer = m_useDynamicVertexBuffer;
      mshd.m_useDynamicIndexBuffer = m_useDynamicIndexBuffer;
      mshd.m_vbIsDirty = true;
      mshd.m_vbIsDirty = true;

      IRenderSystem renderSystem = (m_vertexBuffer == null) ? null : m_vertexBuffer.RenderSystem;
      if (renderSystem != null)
        mshd.Compile(renderSystem);
      else
        mshd.Compile();

      return mshd;
    }

    /// <summary>
    /// Get a copy of the object.
    /// </summary>
    /// <returns>Cloned copy.</returns>
    IDeepCloneable IDeepCloneable.Clone()
    {
      return Clone();
    }

    public void Compile()
    {
      Compile(IRenderSystem.Current);
    }

    public void Compile(IRenderSystem renderSystem)
    {
      GraphicsHelper.ThrowIfRenderSystemNull(renderSystem);

      UpdateVertexAndPrimitiveCount();
      ConstructIndexBuffer(renderSystem);
      ConstructVertexBuffer(renderSystem);
    }

    public void Read(ISavableReader input)
    {
      m_useIndexedPrimitives = input.ReadBoolean("UseIndexedPrimitives");
      m_useDynamicVertexBuffer = input.ReadBoolean("UseDynamicVertexBuffer");
      m_useDynamicIndexBuffer = input.ReadBoolean("UseDynamicIndexBuffer");
      m_primitiveType = input.ReadEnum<PrimitiveType>("PrimitiveType");

      int count = input.BeginReadGroup("VertexAttributes");

      for (int i = 0; i < count; i++)
      {
        input.BeginReadGroup("VertexAttribute");

        VertexSemantic semantic = input.ReadEnum<VertexSemantic>("VertexSemantic");
        int index = input.ReadInt32("SemanticIndex");
        VertexFormat format = input.ReadEnum<VertexFormat>("VertexFormat");
        IDataBuffer data = input.ReadBlob("Buffer");
        AddBuffer(semantic, index, format, data);

        input.EndReadGroup();
      }

      input.EndReadGroup();

      if (m_useIndexedPrimitives)
      {
        m_indices = null;
        IndexData indexData = input.ReadIndexDataBlob("Indices");
        if (indexData.IsValid)
          m_indices = indexData;
      }

      IRenderSystem renderSystem = GraphicsHelper.GetRenderSystem(input.ServiceProvider);
      Compile(renderSystem);
    }

    public void Write(ISavableWriter output)
    {
      output.Write("UseIndexedPrimitives", m_useIndexedPrimitives);
      output.Write("UseDynamicVertexBuffer", m_useDynamicVertexBuffer);
      output.Write("UseDynamicIndexBuffer", m_useDynamicIndexBuffer);
      output.WriteEnum<PrimitiveType>("PrimitiveType", m_primitiveType);

      output.BeginWriteGroup("VertexAttributes", m_bufferMap.Count);

      foreach (KeyValuePair<TupleKey<VertexSemantic, int>, VertexStream> kv in m_bufferMap)
      {
        output.BeginWriteGroup("VertexAttribute");

        output.WriteEnum<VertexSemantic>("VertexSemantic", kv.Key.First);
        output.Write("SemanticIndex", kv.Key.Second);
        output.WriteEnum<VertexFormat>("VertexFormat", kv.Value.Format);
        output.WriteBlob("Buffer", kv.Value.Data);

        output.EndWriteGroup();
      }

      output.EndWriteGroup();

      if (m_useIndexedPrimitives)
        output.WriteIndexDataBlob("Indices", m_indices ?? new IndexData());
    }

    public Enumerator GetEnumerator()
    {
      return new Enumerator(this);
    }

    IEnumerator<VertexAttributeData> IEnumerable<VertexAttributeData>.GetEnumerator()
    {
      return new Enumerator(this);
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return new Enumerator(this);
    }

    //TEMP - Even more now with sub mesh support.
    public void ComputeTangentBasis()
    {
      if (m_primitiveType == Graphics.PrimitiveType.LineList || m_primitiveType == Graphics.PrimitiveType.LineStrip ||
          m_primitiveType == Graphics.PrimitiveType.PointList)
        return;

      DataBuffer<Vector2> texCoords = TextureCoordinates;
      DataBuffer<Vector3> normals = Normals;
      DataBuffer<Vector3> pos = Positions;

      if (texCoords == null || normals == null || pos == null)
        return;

      DataBuffer<Vector3> bitangents = Bitangents;
      DataBuffer<Vector3> tangents = Tangents;

      if (bitangents == null)
      {
        bitangents = DataBuffer.Create<Vector3>(pos.Length);
        Bitangents = bitangents;
      }

      if (tangents == null)
      {
        tangents = DataBuffer.Create<Vector3>(pos.Length);
        Tangents = tangents;
      }

      UpdateVertexAndPrimitiveCount();

      for (int i = 0; i < m_primitiveCount; i++)
      {
        int index0, index1, index2;
        GetPrimitiveVertexIndices(i, out index0, out index1, out index2);

        //Get the triangle verts
        ref readonly Vector3 p0 = ref pos[index0];
        ref readonly Vector3 p1 = ref pos[index1];
        ref readonly Vector3 p2 = ref pos[index2];

        //Get the triangle uv coords
        ref readonly Vector2 uv0 = ref texCoords[index0];
        ref readonly Vector2 uv1 = ref texCoords[index1];
        ref readonly Vector2 uv2 = ref texCoords[index2];

        float x1 = p1.X - p0.X;
        float x2 = p2.X - p0.X;
        float y1 = p1.Y - p0.Y;
        float y2 = p2.Y - p0.Y;
        float z1 = p1.Z - p0.Z;
        float z2 = p2.Z - p0.Z;

        float s1 = uv1.X - uv0.X;
        float s2 = uv2.X - uv0.X;
        float t1 = uv1.Y - uv0.Y;
        float t2 = uv2.Y - uv0.Y;

        //calculate determinate
        float r = 1.0f / (s1 * t2 - s2 * t1);

        //Calculate the s, t directions
        Vector3 sdir = new Vector3((t2 * x1 - t1 * x2) * r, (t2 * y1 - t1 * y2) * r,
                (t2 * z1 - t1 * z2) * r);
        Vector3 tdir = new Vector3((s1 * x2 - s2 * x1) * r, (s1 * y2 - s2 * y1) * r,
                (s1 * z2 - s2 * z1) * r);

        //Add them to the existing tangent, binormal arrays
        tangents[index0] += sdir;
        tangents[index1] += sdir;
        tangents[index2] += sdir;

        bitangents[index0] += tdir;
        bitangents[index1] += tdir;
        bitangents[index2] += tdir;
      }

      //Gram-Schmidt orthogonalization
      for (int i = 0; i < normals.Length; i++)
      {
        ref readonly Vector3 n = ref normals[i];
        ref Vector3 t = ref tangents[i];
        ref Vector3 b = ref bitangents[i];

        Vector3 tangent = Vector3.Normalize(t - n * Vector3.Dot(n, t));

        float handy = (Vector3.Dot(Vector3.Cross(n, t), b) < 0) ? -1.0f : 1.0f;

        Vector3.NormalizedCross(n, t, out b);
        b *= handy;

        t = tangent;
      }

      Compile();
    }

    public bool Intersects(in Ray rayInWorldSpace, bool ignoreBackfaces = false)
    {
      return Intersects(rayInWorldSpace, Matrix.Identity, null, null, ignoreBackfaces);
    }

    public bool Intersects(in Ray rayInWorldSpace, in SubMeshRange? subMeshRange, bool ignoreBackfaces = false)
    {
      return Intersects(rayInWorldSpace, Matrix.Identity, subMeshRange, null, ignoreBackfaces);
    }

    public bool Intersects(in Ray rayInWorldSpace, in Matrix worldMatrix, IRefList<Pair<LineIntersectionResult, Triangle?>> results, bool ignoreBackfaces = false)
    {
      return Intersects(rayInWorldSpace, worldMatrix, null, results, ignoreBackfaces);
    }

    public bool Intersects(in Ray rayInWorldSpace, in Matrix worldMatrix, in SubMeshRange? subMeshRange, IRefList<Pair<LineIntersectionResult, Triangle?>> results, bool ignoreBackfaces = false)
    {
      if (!HasTriangles || !HasValidPositions)
        return false;

      Matrix worldToObjMatrix;
      Ray rayInObjSpace;

      bool noTransformNeeded = worldMatrix.IsIdentity;

      if (!noTransformNeeded)
      {
        Matrix.Invert(worldMatrix, out worldToObjMatrix);
        Ray.Transform(rayInWorldSpace, worldToObjMatrix, out rayInObjSpace);
      }
      else
      {
        worldToObjMatrix = Matrix.Identity;
        rayInObjSpace = rayInWorldSpace;
      }

      bool haveOne = false;
      DataBuffer<Vector3> pos = Positions;

      int startPrimitive, primitiveCount;
      GetPrimitiveRange(subMeshRange, out startPrimitive, out primitiveCount);

      int baseVertexOffset = 0;
      if (m_useIndexedPrimitives && subMeshRange.HasValue)
        baseVertexOffset = subMeshRange.Value.BaseVertexOffset;

      for (int i = startPrimitive; i < startPrimitive + primitiveCount; i++)
      {
        Triangle tri;
        GetPrimitive(i, baseVertexOffset, out tri);

        LineIntersectionResult intersection;
        if (rayInObjSpace.Intersects(tri, out intersection, ignoreBackfaces))
        {
          haveOne = true;

          if (results == null)
            break;

          //Transform triangle to world space, as well as the intersection result
          if (!noTransformNeeded)
          {
            Vector3 normal = intersection.Normal.Value;
            Vector3 pt = intersection.Point;

            Vector3.TransformNormal(normal, worldMatrix, out normal);
            Vector3.Transform(pt, worldMatrix, out pt);

            float distance = Vector3.Distance(pt, rayInWorldSpace.Origin);

            intersection = new LineIntersectionResult(pt, distance, normal);

            Triangle.Transform(tri, worldMatrix, out tri);
          }

          results.Add(new Pair<LineIntersectionResult, Triangle?>(intersection, tri));
        }
      }

      return haveOne;
    }

    /// <summary>
    /// Gets the range of primitives as described by the <see cref="SubMeshRange"/>.
    /// </summary>
    /// <param name="range">Submesh range.</param>
    /// <param name="startPrimitive">The index of the first primitive.</param>
    /// <param name="primitiveCount">Numer of primitives in the range.</param>
    public void GetPrimitiveRange(in SubMeshRange? range, out int startPrimitive, out int primitiveCount)
    {
      if (!range.HasValue)
      {
        startPrimitive = 0;
        primitiveCount = m_primitiveCount;
        return;
      }

      SubMeshRange meshRange = range.Value;

      switch (m_primitiveType)
      {
        case PrimitiveType.TriangleList:
          startPrimitive = meshRange.Offset / 3;
          primitiveCount = meshRange.Count / 3;
          break;
        case PrimitiveType.LineList:
          startPrimitive = meshRange.Offset / 2;
          primitiveCount = meshRange.Count / 2;
          break;
        default:
          startPrimitive = meshRange.Offset;
          primitiveCount = meshRange.Count;
          break;
      }
    }

    /// <summary>
    /// Gets the number of vertices (points, colors, normals, etc) expected to describe a single primitive in the data.
    /// </summary>
    /// <returns>Number of vertices: Triangles - 3, Lines - 2, Point - 1.</returns>
    public int GetPrimitiveVertexCount()
    {
      int numPoints = 0;
      switch (m_primitiveType)
      {
        case Graphics.PrimitiveType.TriangleList:
        case Graphics.PrimitiveType.TriangleStrip:
          numPoints = 3;
          break;
        case Graphics.PrimitiveType.LineList:
        case Graphics.PrimitiveType.LineStrip:
          numPoints = 2;
          break;
        case Graphics.PrimitiveType.PointList:
          numPoints = 1;
          break;
      }

      return numPoints;
    }

    /// <summary>
    /// Gets the index buffer index (OR vertex buffer index, if not indexed) for the primitive.
    /// </summary>
    /// <param name="primitiveIndex">The index of the primitive to query indices for.</param>
    /// <param name="indexOffset">Optional index offset.</param>
    /// <returns>Index of the primitive. If data is indexed this is value can be used with the index buffer, if not indexed then it can be used with the vertex buffers.</returns>
    public int GetIndexForPrimitive(int primIndex, int indexOffset = 0)
    {
      switch (m_primitiveType)
      {
        case PrimitiveType.TriangleList:
          return (primIndex * 3) + indexOffset;
        case PrimitiveType.TriangleStrip:
          return primIndex + indexOffset;
        case PrimitiveType.LineList:
          return (primIndex * 2) + indexOffset;
        case PrimitiveType.LineStrip:
          return primIndex + indexOffset;
        default:
          return primIndex;
      }
    }

    /// <summary>
    /// Gets the vertex buffer indices for the primitive. For indexed geometry, this will query the index buffer.
    /// </summary>
    /// <param name="primitiveIndex">The index of the primitive to query vertex buffer indices for.</param>
    /// <param name="indices">Array containing the vertex buffer indices. If it is too small, it is resized. Expected index counts as followed: Triangles - 3, Lines - 2, Point - 1.</param>
    /// <returns>True if indices were successfully returned; false if otherwise.</returns>
    public bool GetPrimitiveVertexIndices(int primitiveIndex, ref int[] indices)
    {
      int ptCount = GetPrimitiveVertexCount();

      if (ptCount == 0)
        return false;

      if (indices == null || indices.Length < ptCount)
        indices = new int[ptCount];

      for (int i = 0; i < ptCount; i++)
      {
        if (m_useIndexedPrimitives)
        {
          indices[i] = m_indices.Value[GetIndexForPrimitive(primitiveIndex, i)];
        }
        else
        {
          indices[i] = GetIndexForPrimitive(primitiveIndex, i);
        }
      }

      return true;
    }

    /// <summary>
    /// Gets the vertex buffer index for a point primitive (topology is <see cref="PrimitiveBatchTopology.PointList"/>). For indexed geometry, this will query the index buffer.
    /// </summary>
    /// <param name="primitiveIndex">The index of the primitive to query vertex buffer indices for.</param>
    /// <param name="index">Vertex buffer index for the point primitive.</param>
    /// <returns>True if indices were successfully returned; false if otherwise.</returns>
    public bool GetPrimitiveVertexIndices(int primitiveIndex, out int index)
    {
      int ptCount = GetPrimitiveVertexCount();

      if (ptCount == 0)
      {
        index = 0;
        return false;
      }

      if (m_useIndexedPrimitives)
      {
        index = m_indices.Value[GetIndexForPrimitive(primitiveIndex, 0)];
      }
      else
      {
        index = GetIndexForPrimitive(primitiveIndex, 0);
      }

      return true;
    }

    /// <summary>
    /// Gets the vertex buffer indices for a line primitive (topology is either <see cref="PrimitiveBatchTopology.LineList"/> -or- <see cref="PrimitiveBatchTopology.LineStrip"/>.
    /// For indexed geometry, this will query the index buffer.
    /// </summary>
    /// <param name="primitiveIndex">The index of the primitive to query vertex buffer indices for.</param>
    /// <param name="index0">First vertex buffer index for the line primitive.</param>
    /// <param name="index1">Second vertex buffer index for the line primitive.</param>
    /// <returns>True if indices were successfully returned; false if otherwise.</returns>
    public bool GetPrimitiveVertexIndices(int primitiveIndex, out int index0, out int index1)
    {
      int ptCount = GetPrimitiveVertexCount();

      if (ptCount <= 1)
      {
        index0 = 0;
        index1 = 0;
        return false;
      }

      if (m_useIndexedPrimitives)
      {
        int startVertexIndex = GetIndexForPrimitive(primitiveIndex, 0);

        index0 = m_indices.Value[startVertexIndex];
        index1 = m_indices.Value[startVertexIndex + 1];
      }
      else
      {
        index0 = GetIndexForPrimitive(primitiveIndex, 0);
        index1 = index0 + 1;
      }

      return true;
    }

    /// <summary>
    /// Gets the vertex buffer indices for a triangle primitive (topology is either <see cref="PrimitiveBatchTopology.TriangleList"/> -or- <see cref="PrimitiveBatchTopology.TriangleStrip"/>.
    /// For indexed geometry, this will query the index buffer.
    /// </summary>
    /// <param name="primitiveIndex">The index of the primitive to query vertex buffer indices for.</param>
    /// <param name="index0">First vertex buffer index for the triangle primitive.</param>
    /// <param name="index1">Second vertex buffer index for the triangle primitive.</param>
    /// <param name="index2">Third vertex buffer index for the triangle primitive.</param>
    /// <returns>True if indices were successfully returned; false if otherwise.</returns>
    public bool GetPrimitiveVertexIndices(int primitiveIndex, out int index0, out int index1, out int index2)
    {
      int ptCount = GetPrimitiveVertexCount();

      if (ptCount <= 2)
      {
        index0 = 0;
        index1 = 0;
        index2 = 0;
        return false;
      }

      if (m_useIndexedPrimitives)
      {
        int startVertexIndex = GetIndexForPrimitive(primitiveIndex, 0);

        index0 = m_indices.Value[startVertexIndex];
        index1 = m_indices.Value[startVertexIndex + 1];
        index2 = m_indices.Value[startVertexIndex + 2];
      }
      else
      {
        index0 = GetIndexForPrimitive(primitiveIndex, 0);
        index1 = index0 + 1;
        index2 = index0 + 2;
      }

      return true;
    }

    /// <summary>
    /// Gets the line primitive at the specified primitive index.
    /// </summary>
    /// <param name="primitiveIndex">Index of the primitive to query.</param>
    /// <param name="baseVertexOffset">Offset to add to each index before reading positions from the vertex buffer.</param>
    /// <param name="line">Triangle primitive result.</param>
    /// <returns>True if the data consists of lines, false if otherwise.</returns>
    public bool GetPrimitive(int primitiveIndex, out Segment line)
    {
      return GetPrimitive(primitiveIndex, 0, out line);
    }

    /// <summary>
    /// Gets the line primitive at the specified primitive index.
    /// </summary>
    /// <param name="primitiveIndex">Index of the primitive to query.</param>
    /// <param name="baseVertexOffset">Offset to add to each index before reading positions from the vertex buffer.</param>
    /// <param name="line">Triangle primitive result.</param>
    /// <returns>True if the data consists of lines, false if otherwise.</returns>
    public bool GetPrimitive(int primitiveIndex, int baseVertexOffset, out Segment line)
    {
      if (!HasLines)
      {
        line = new Segment();
        return false;
      }

      //Sanity checks
      System.Diagnostics.Debug.Assert(primitiveIndex < m_primitiveCount);
      System.Diagnostics.Debug.Assert(primitiveIndex >= 0);
      System.Diagnostics.Debug.Assert(HasValidPositions);

      DataBuffer<Vector3> pos = Positions;

      //Compute the indices of the two vertices that make up the segment
      int firstVertexIndex = GetIndexForPrimitive(primitiveIndex, 0);
      int secondVertexIndex = firstVertexIndex + 1;

      if (m_useIndexedPrimitives)
      {
        IndexData indices = m_indices.Value;

        line.StartPoint = pos[indices[firstVertexIndex] + baseVertexOffset];
        line.EndPoint = pos[indices[secondVertexIndex] + baseVertexOffset];
      }
      else
      {
        line.StartPoint = pos[firstVertexIndex];
        line.EndPoint = pos[secondVertexIndex];
      }

      return true;
    }

    /// <summary>
    /// Gets the triangle primitive at the specified primitive index.
    /// </summary>
    /// <param name="primitiveIndex">Index of the primitive to query.</param>
    /// <param name="triangle">Triangle primitive result.</param>
    /// <returns>True if the data consists of triangles, false if otherwise.</returns>
    public bool GetPrimitive(int primitiveIndex, out Triangle triangle)
    {
      return GetPrimitive(primitiveIndex, 0, out triangle);
    }

    /// <summary>
    /// Gets the triangle primitive at the specified primitive index.
    /// </summary>
    /// <param name="primitiveIndex">Index of the primitive to query.</param>
    /// <param name="baseVertexOffset">Offset to add to each index before reading positions from the vertex buffer.</param>
    /// <param name="triangle">Triangle primitive result.</param>
    /// <returns>True if the data consists of triangles, false if otherwise.</returns>
    public bool GetPrimitive(int primitiveIndex, int baseVertexOffset, out Triangle triangle)
    {
      if (!HasTriangles)
      {
        triangle = new Triangle();
        return false;
      }

      //Sanity checks
      System.Diagnostics.Debug.Assert(primitiveIndex < m_primitiveCount);
      System.Diagnostics.Debug.Assert(primitiveIndex >= 0);
      System.Diagnostics.Debug.Assert(HasValidPositions);

      DataBuffer<Vector3> pos = Positions;

      //Compute the indices of the three vertices that make up the triangle
      int firstVertexIndex = GetIndexForPrimitive(primitiveIndex, 0);
      int secondVertexIndex = firstVertexIndex + 1;
      int thirdVertexIndex = firstVertexIndex + 2;

      if (m_useIndexedPrimitives)
      {
        IndexData indices = m_indices.Value;

        triangle.PointA = pos[indices[firstVertexIndex] + baseVertexOffset];
        triangle.PointB = pos[indices[secondVertexIndex] + baseVertexOffset];
        triangle.PointC = pos[indices[thirdVertexIndex] + baseVertexOffset];
      }
      else
      {
        triangle.PointA = pos[firstVertexIndex];
        triangle.PointB = pos[secondVertexIndex];
        triangle.PointC = pos[thirdVertexIndex];
      }

      return true;
    }

    public void Transform(Transform transform)
    {
      if (transform == null)
        return;

      Transform(transform.Matrix);
    }

    public void Transform(in Vector3 translation)
    {
      Matrix transform;
      Matrix.FromTranslation(translation, out transform);

      Transform(transform);
    }

    public void Transform(in Quaternion rotation)
    {
      Matrix transform;
      Matrix.FromQuaternion(rotation, out transform);

      Transform(transform);
    }

    public void Transform(in Quaternion rotation, in Vector3 translation)
    {
      Matrix transform;
      Matrix.CreateTransformationMatrix(rotation, translation, out transform);

      Transform(transform);
    }

    public void Transform(in Matrix transform)
    {
      if (transform.IsIdentity)
        return;

      DataBuffer<Vector3> positions = Positions;
      DataBuffer<Vector3> normals = Normals;
      DataBuffer<Vector3> tangents = Tangents;
      DataBuffer<Vector3> bitangents = Bitangents;

      if (positions != null)
        Transform(positions, transform, false, false);

      if (normals != null)
        Transform(normals, transform, true, true);

      if (tangents != null)
        Transform(tangents, transform, true, true);

      if (bitangents != null)
        Transform(bitangents, transform, true, true);
    }

    private void Transform(DataBuffer<Vector3> buffer, in Matrix transform, bool rotateOnly = false, bool normalize = false)
    {
      if (rotateOnly)
      {
        for (int i = 0; i < buffer.Length; i++)
        {
          ref Vector3 v = ref buffer[i];

          Vector3.TransformNormal(v, transform, out v);
          if (normalize)
            v.Normalize();
        }
      }
      else
      {
        for (int i = 0; i < buffer.Length; i++)
        {
          ref Vector3 v = ref buffer[i];

          Vector3.Transform(v, transform, out v);

          //Normalizing doesn't make sense in this context, ignore the option
        }
      }
    }

    public void Merge(MeshData other)
    {
      if (other == null && HasValidPositions && other.HasValidPositions)
        return;

      IndexData? thisIndexData = m_indices;
      IndexData? otherIndexData = other.m_indices;
      int thisNumVerts = Positions.Length;
      int otherNumVerts = other.Positions.Length;

      //Cases:
      //Both have indexed - enlarge current, copy over + offset from other
      //Both have non-indexed - Do nothing
      //This has indexed, other has non-indexed - enlarge current, offset and create dumb indices for other
      //This has non-indexed, other has indexed - create a new databuffer that can contain both, create dumb indices for current, copy + offset other

      if (thisIndexData.HasValue && otherIndexData.HasValue)
      {
        IndexData thisIndices = thisIndexData.Value;
        IndexData otherIndices = otherIndexData.Value;

        int startIndex = thisIndices.Length;
        int indexOffset = thisNumVerts;

        //Safely expand the index buffer (if short and we're about to go out of range, make int)
        SafeRecreateIndices(thisIndices.Length + otherIndices.Length);
        thisIndices = m_indices.Value; //Get the databuffer reference again, since it may have changed

        //Copy and offset other indices
        for (int i = startIndex, j = 0; j < otherIndices.Length; i++, j++)
        {
          int indx = otherIndices[j];
          indx += indexOffset;
          thisIndices[i] = indx;
        }

        m_ibIsDirty = true;
      }
      else if (!thisIndexData.HasValue && otherIndexData.HasValue)
      {
        IndexData thisIndices;
        IndexData otherIndices = otherIndexData.Value;
        int numIndices = thisNumVerts;
        int newSize = numIndices + otherIndices.Length;

        if (otherIndices.IndexFormat == IndexFormat.SixteenBits &&
            ((uint) (newSize) > (uint) (ushort.MaxValue)))
        {
          thisIndices = new IndexData(DataBuffer.Create<int>(newSize));
        }
        else if (otherIndices.IndexFormat == IndexFormat.SixteenBits)
        {
          thisIndices = new IndexData(DataBuffer.Create<ushort>(newSize));
        }
        else
        {
          thisIndices = new IndexData(DataBuffer.Create<int>(newSize));
        }

        //Fill dumb indices
        for (int i = 0; i < numIndices; i++)
        {
          thisIndices[i] = i;
        }

        //Copy over and offset other indices
        for (int i = numIndices, j = 0; j < otherIndices.Length; i++, j++)
        {
          int indx = otherIndices[j];
          indx += numIndices;
          thisIndices[i] = indx;
        }

        Indices = thisIndices;
      }
      else if (thisIndexData.HasValue && !otherIndexData.HasValue)
      {
        IndexData thisIndices = thisIndexData.Value;
        int startIndex = thisIndices.Length;
        int indexOffset = thisNumVerts;
        int newSize = thisIndices.Length + otherNumVerts;

        //Safely expand the index buffer (if short and we're about to go out of range, make int)
        SafeRecreateIndices(newSize);
        thisIndices = m_indices.Value; //Get the databuffer reference again, since it may have changed

        //Create dumb indices for other mesh
        for (int i = startIndex, j = 0; i < thisIndices.Length; i++, j++)
        {
          int indx = j + indexOffset;
          thisIndices[i] = indx;
        }

        m_ibIsDirty = true;
      }

      int newTotalVerts = thisNumVerts + otherNumVerts;

      foreach (KeyValuePair<TupleKey<VertexSemantic, int>, VertexStream> kv in other.m_bufferMap)
      {
        IDataBuffer db = GetBuffer(kv.Key.First, kv.Key.Second);
        int numBytesToCopy = kv.Value.Data.SizeInBytes;

        //If we have the buffer, need to resize and append new data, otherwise just insert a clone and set a portion to zero
        if (db != null && db.ElementSizeInBytes == kv.Value.Data.ElementSizeInBytes)
        {
          db.Resize(newTotalVerts);
          kv.Value.Data.Bytes.Slice(0, numBytesToCopy).CopyTo(db.Bytes.Slice(thisNumVerts * db.ElementSizeInBytes, numBytesToCopy));
        }
        else if (db == null)
        {
          db = DataBuffer.Create(newTotalVerts, kv.Value.Data.ElementType);
          kv.Value.Data.Bytes.Slice(0, numBytesToCopy).CopyTo(db.Bytes.Slice(thisNumVerts * db.ElementSizeInBytes, numBytesToCopy));

          AddBuffer(kv.Key.First, kv.Key.Second, db);

          EngineLog.Log(LogLevel.Warn, "Merge MeshData has added mismatched data.");
        }
        else
        {
          System.Diagnostics.Debug.Assert(true, "Element size mismatch?");
        }
      }

      m_vbIsDirty = true;
    }

    private void SafeRecreateIndices(int newSize)
    {
      IndexData db = m_indices.Value;
      if (!db.IsValid || newSize <= db.Length)
        return;

      if (m_indices.Value.IndexFormat == IndexFormat.SixteenBits)
      {
        if ((uint) newSize > (uint) (ushort.MaxValue))
        {
          IndexData newIndices = new IndexData(DataBuffer.Create<int>(newSize));
          for (int i = 0; i < newSize; i++)
            newIndices[i] = db[i];

          m_indices = newIndices;
        }
        else
        {
          db.Resize(newSize);
        }
      }
      else
      {
        //Internally resizes
        db.Resize(newSize);
      }
    }

    public void UpdateVertexAndPrimitiveCount()
    {
      IDataBuffer positions = GetBuffer(VertexSemantic.Position, 0);

      if (m_useIndexedPrimitives && m_indices == null)
      {
        m_vertexCount = 0;
        m_primitiveCount = 0;
        return;
      }
      else if (positions == null)
      {
        m_vertexCount = 0;
        m_primitiveCount = 0;
        return;
      }

      m_vertexCount = (m_useIndexedPrimitives) ? m_indices.Value.Length : positions.Length;

      switch (m_primitiveType)
      {
        case PrimitiveType.TriangleStrip:
          m_primitiveCount = m_vertexCount - 2;
          break;
        case PrimitiveType.TriangleList:
          m_primitiveCount = m_vertexCount / 3;
          break;
        case PrimitiveType.LineStrip:
          m_primitiveCount = m_vertexCount - 1;
          break;
        case PrimitiveType.LineList:
          m_primitiveCount = m_vertexCount / 2;
          break;
        case PrimitiveType.PointList:
          m_primitiveCount = m_vertexCount;
          break;
        default:
          m_primitiveCount = 0;
          break;
      }
    }

    #region Private methods

    private void ConstructIndexBuffer(IRenderSystem renderSystem)
    {
      m_ibIsDirty = false;
      ResourceUsage usage = (m_useDynamicIndexBuffer) ? ResourceUsage.Dynamic : ResourceUsage.Static;

      if (!m_useIndexedPrimitives)
        return;

      if (m_indexBuffer != null)
      {
        //Not valid indices, dispose and set to null
        if (!m_indices.HasValue || !m_indices.Value.IsValid)
        {
          m_indexBuffer.Dispose();
          m_indexBuffer = null;
          return;
        }

        IndexData data = m_indices.Value;

        //If buffer is same size format and usage, can just set data, don't need to recreate
        if (m_indexBuffer.IndexCount == data.Length &&
            m_indexBuffer.IndexFormat == data.IndexFormat &&
            m_indexBuffer.ResourceUsage == usage)
        {
          DataWriteOptions writeOptions = (usage == ResourceUsage.Dynamic) ? DataWriteOptions.Discard : DataWriteOptions.None;
          m_indexBuffer.SetData(renderSystem.ImmediateContext, data);
          return;
        }
        else
        {
          m_indexBuffer.Dispose();
          m_indexBuffer = null;
        }
      }

      if (!m_indices.HasValue)
        return;

      //Else create a new index buffer
      m_indexBuffer = new IndexBuffer(renderSystem, m_indices.Value);
    }

    private void ConstructVertexBuffer(IRenderSystem renderSystem)
    {
      m_vbIsDirty = false;
      ResourceUsage usage = (m_useDynamicVertexBuffer) ? ResourceUsage.Dynamic : ResourceUsage.Static;
      using PooledArray<IReadOnlyDataBuffer> buffers = GetBuffers();

      if (m_vertexBuffer != null)
      {
        //If no vertex data, dispose and set to null
        if (m_bufferMap.Count == 0)
        {
          m_vertexBuffer.Dispose();
          m_vertexBuffer = null;
          return;
        }

        IReadOnlyDataBuffer db = buffers[0];

        //If buffer is same size format and usage, can just set data, don't need to recreate
        if (m_vertexBuffer.VertexCount == db.Length &&
            m_vertexBuffer.ResourceUsage == usage && buffers.Length == m_vertexBuffer.VertexLayout.ElementCount)
        {
          DataWriteOptions writeOptions = (usage == ResourceUsage.Dynamic) ? DataWriteOptions.Discard : DataWriteOptions.None;
          m_vertexBuffer.SetInterleavedData(renderSystem.ImmediateContext, buffers);
          return;
        }
        else
        {
          m_vertexBuffer.Dispose();
          m_vertexBuffer = null;
        }
      }

      if (m_bufferMap.Count == 0)
        return;

      //Else create a new vertex buffer
      m_vertexBuffer = new VertexBuffer(renderSystem, CreateVertexLayout(), VertexBufferOptions.Init(buffers, usage));

    }

    private PooledArray<IReadOnlyDataBuffer> GetBuffers()
    {
      PooledArray<IReadOnlyDataBuffer> buffers = new PooledArray<IReadOnlyDataBuffer>(m_bufferMap.Count);
      int index = 0;
      foreach (KeyValuePair<TupleKey<VertexSemantic, int>, VertexStream> kv in m_bufferMap)
        buffers[index++] = kv.Value.Data;

      return buffers;
    }

    private VertexLayout CreateVertexLayout()
    {
      VertexElement[] vs = new VertexElement[m_bufferMap.Count];
      int index = 0;
      foreach (KeyValuePair<TupleKey<VertexSemantic, int>, VertexStream> kv in m_bufferMap)
        vs[index++] = new VertexElement(kv.Key.First, kv.Key.Second, kv.Value.Format);

      return new VertexLayout(vs);
    }

    #endregion

    #region VertexStream type

    private struct VertexStream
    {
      private static Dictionary<Type, VertexFormat> s_typesToFormats;
      public VertexFormat Format;
      public IDataBuffer Data;

      static VertexStream()
      {
        s_typesToFormats = new Dictionary<Type, VertexFormat>();

        s_typesToFormats.Add(typeof(Color), VertexFormat.Color);
        s_typesToFormats.Add(typeof(float), VertexFormat.Float);
        s_typesToFormats.Add(typeof(Vector2), VertexFormat.Float2);
        s_typesToFormats.Add(typeof(Vector3), VertexFormat.Float3);
        s_typesToFormats.Add(typeof(Vector4), VertexFormat.Float4);
        s_typesToFormats.Add(typeof(Half), VertexFormat.Half);
        //s_typesToFormats.Add(typeof(Half2), VertexFormat.Half2); //TODO
        //s_typesToFormats.Add(typeof(Half4), VertexFormat.Half4); //TODO
        s_typesToFormats.Add(typeof(int), VertexFormat.Int);
        s_typesToFormats.Add(typeof(Int2), VertexFormat.Int2);
        s_typesToFormats.Add(typeof(Int3), VertexFormat.Int3);
        s_typesToFormats.Add(typeof(Int4), VertexFormat.Int4);
        s_typesToFormats.Add(typeof(uint), VertexFormat.UInt);
        //s_typesToFormats.Add(typeof(Uint2), VertexFormat.UInt2); //Unlikely
        //s_typesToFormats.Add(typeof(Uint3), VertexFormat.UInt3); //Unlikely
        //s_typesToFormats.Add(typeof(Uint4), VertexFormat.UInt4); //Unlikely
        s_typesToFormats.Add(typeof(short), VertexFormat.Short);
        //s_typesToFormats.Add(typeof(Short2), VertexFormat.Short2); //Maybe
        //s_typesToFormats.Add(typeof(Short4), VertexFormat.Short4); //Maybe
        s_typesToFormats.Add(typeof(ushort), VertexFormat.UShort);
        //s_typesToFormats.Add(typeof(UShort2), VertexFormat.UShort2); //Unlikely
        //s_typesToFormats.Add(typeof(UShort4), VertexFormat.UShort4); //Unlikely
        //s_typesToFormats.Add(typeof(NormalizedShort), VertexFormat.NormalizedShort); //TODO
        //s_typesToFormats.Add(typeof(NormalizedShort2), VertexFormat.NormalizedShort2); //TODO
        //s_typesToFormats.Add(typeof(NormalizedShort4), VertexFormat.NormalizedShort4); //TODO
        //s_typesToFormats.Add(typeof(NormalizedUShort), VertexFormat.NormalizedUShort); //Unlikely
        //s_typesToFormats.Add(typeof(NormalizedUShort2), VertexFormat.NormalizedUShort2); //Unlikely
        //s_typesToFormats.Add(typeof(NormalizedUShort4), VertexFormat.NormalizedUShort4); //Unlikely
      }

      public VertexStream(IDataBuffer data)
      {
        Format = InferVertexFormat(data);
        Data = data;
      }

      public VertexStream(VertexFormat format, IDataBuffer data)
      {
        Format = format;
        Data = data;
      }

      public static VertexFormat InferVertexFormat(IDataBuffer data)
      {
        VertexFormat format;
        if (s_typesToFormats.TryGetValue(data.ElementType, out format))
          return format;

        throw new TeslaException(StringLocalizer.Instance.GetLocalizedString("UnsupportedVertexFormat"));
      }
    }

    #endregion

    #region Enumerator

    /// <summary>
    /// Enumerates the vertex attribute buffers in <see cref="MeshData"/>.
    /// </summary>
    public struct Enumerator : IEnumerator<VertexAttributeData>
    {
      private SortedDictionary<TupleKey<VertexSemantic, int>, VertexStream>.Enumerator m_bufferEnumerator;
      private VertexAttributeData m_current;

      /// <summary>
      /// Gets the current vertex attribute buffer entry.
      /// </summary>
      public VertexAttributeData Current
      {
        get
        {
          return m_current;
        }
      }

      object IEnumerator.Current
      {
        get
        {
          return m_current;
        }
      }

      internal Enumerator(MeshData md)
      {
        m_bufferEnumerator = md.m_bufferMap.GetEnumerator();
        m_current = default(VertexAttributeData);
      }

      /// <summary>
      /// Moves to the next vertex attribute buffer.
      /// </summary>
      /// <returns>True if there is a current buffer, false if we're done enumerating.</returns>
      public bool MoveNext()
      {
        if (m_bufferEnumerator.MoveNext())
        {
          KeyValuePair<TupleKey<VertexSemantic, int>, VertexStream> kv = m_bufferEnumerator.Current;

          m_current.Semantic = kv.Key.First;
          m_current.SemanticIndex = kv.Key.Second;
          m_current.Format = kv.Value.Format;
          m_current.Buffer = kv.Value.Data;

          return true;
        }
        else
        {
          m_current = default(VertexAttributeData);
          return false;
        }
      }

      void IDisposable.Dispose()
      {
      }

      void IEnumerator.Reset()
      {
      }
    }

    #endregion
  }

  /// <summary>
  /// Defines a complete vertex attribute buffer entry in a <see cref="MeshData"/>.
  /// </summary>
  public struct VertexAttributeData
  {
    /// <summary>
    /// The Vertex Semantic of the attribute.
    /// </summary>
    public VertexSemantic Semantic;

    /// <summary>
    /// The Index of the attribute - if multiple attributes have the same semantic.
    /// </summary>
    public int SemanticIndex;

    /// <summary>
    /// The format of the data.
    /// </summary>
    public VertexFormat Format;

    /// <summary>
    /// Buffer containing the vertex attribute information.
    /// </summary>
    public IDataBuffer Buffer;
  }
}
