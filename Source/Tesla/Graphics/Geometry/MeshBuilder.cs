﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

// TODO NULLABLE

namespace Tesla.Graphics
{
  public class MeshBuilder
  {
    private static Dictionary<Type, MethodInfo> s_dataConversionMethods;

    private bool m_useIndexedPrimitives;
    private Dictionary<TupleKey<VertexSemantic, int>, IList> m_vertexAttributeMap;

    private Dictionary<Type, List<IList>> m_bufferPool;
    private IndexDataBuilder m_indices;

    public bool UseIndexedPrimitives
    {
      get
      {
        return m_useIndexedPrimitives;
      }
    }

    public bool Prefer16BitIndices
    {
      get
      {
        return m_indices.IndexFormat == IndexFormat.SixteenBits;
      }
    }

    public int VertexChannelCount
    {
      get
      {
        return m_vertexAttributeMap.Count;
      }
    }

    public int CurrentIndexCount
    {
      get
      {
        if (!m_useIndexedPrimitives)
          return 0;

        return m_indices.Position;
      }
    }

    public int CurrentVertexCount
    {
      get
      {
        TupleKey<VertexSemantic, int> key = new TupleKey<VertexSemantic, int>(VertexSemantic.Position, 0);

        IList list;
        if (!m_vertexAttributeMap.TryGetValue(key, out list))
          return 0;

        return list.Count;
      }
    }

    static MeshBuilder()
    {
      s_dataConversionMethods = new Dictionary<Type, MethodInfo>();
    }

    public MeshBuilder() : this(true, false) { }

    public MeshBuilder(bool useIndexedPrimitives) : this(useIndexedPrimitives, false) { }

    public MeshBuilder(bool useIndexedPrimitives, bool prefer16BitIndices)
    {
      m_useIndexedPrimitives = useIndexedPrimitives;

      m_bufferPool = new Dictionary<Type, List<IList>>();
      m_vertexAttributeMap = new Dictionary<TupleKey<VertexSemantic, int>, IList>();
      m_indices = new IndexDataBuilder(0, (prefer16BitIndices) ? IndexFormat.SixteenBits : IndexFormat.ThirtyTwoBits, MemoryAllocatorStrategy.DefaultPooled);
      m_indices.AutoPromote = true;
    }

    public int AppendVertexData<T>(ReadOnlySpan<T> data, VertexSemantic semantic, int semanticIndex = 0) where T : struct
    {
      if (data.IsEmpty)
        return 0;

      RefList<T> list = GetListFor<T>(semantic, semanticIndex, true);

      if ((list.Count + data.Length) > list.Capacity)
        list.Capacity = Math.Max(list.Capacity * 2, list.Capacity + data.Length);

      for (int i = 0; i < data.Length; i++)
        list.Add(data[i]);

      return list.Count - 1;
    }

    public int AppendVertexData<T>(in T data, VertexSemantic semantic, int index = 0) where T : struct
    {
      RefList<T> list = GetListFor<T>(semantic, index, true);

      list.Add(data);

      return list.Count - 1;
    }

    public int AppendPosition(in Vector3 pos)
    {
      return AppendVertexData<Vector3>(pos, VertexSemantic.Position, 0);
    }

    public int AppendNormal(in Vector3 normal)
    {
      return AppendVertexData<Vector3>(normal, VertexSemantic.Normal, 0);
    }

    public int AppendTangent(in Vector3 tangent)
    {
      return AppendVertexData<Vector3>(tangent, VertexSemantic.Tangent, 0);
    }

    public int AppendBitangent(in Vector3 bitangent)
    {
      return AppendVertexData<Vector3>(bitangent, VertexSemantic.Bitangent, 0);
    }

    public int AppendTextureCoordinates(in Vector2 texCoords)
    {
      return AppendVertexData<Vector2>(texCoords, VertexSemantic.TextureCoordinate, 0);
    }

    public int AppendColor(in Color color)
    {
      return AppendVertexData<Color>(color, VertexSemantic.Color, 0);
    }

    public int AppendBlendIndices(in Vector4 blendIndices)
    {
      return AppendVertexData<Vector4>(blendIndices, VertexSemantic.BlendIndices, 0);
    }

    public int AppendBlendWeights(in Vector4 blendWeights)
    {
      return AppendVertexData<Vector4>(blendWeights, VertexSemantic.BlendWeight, 0);
    }

    public void AppendIndexData(IndexData indexData, bool offsetIndices = false)
    {
      AppendIndexData(indexData, (indexData.IsValid) ? indexData.Length : 0, offsetIndices);
    }

    public void AppendIndexData(IndexData indexData, int count, bool offsetIndices = false)
    {
      if (!m_useIndexedPrimitives)
        throw new InvalidOperationException(StringLocalizer.Instance.GetLocalizedString("MeshBuilder_CannotAddIndices"));

      if (!indexData.IsValid || count <= 0)
        throw new ArgumentNullException(nameof(indexData));

      int baseVertexOffset = 0;

      if (offsetIndices)
        baseVertexOffset = CurrentVertexCount;

      m_indices.SetRange(indexData, baseVertexOffset);
    }

    public void AppendIndexData(int vertexIndex, bool offsetIndices = false)
    {
      if (m_useIndexedPrimitives)
        throw new InvalidOperationException(StringLocalizer.Instance.GetLocalizedString("MeshBuilder_CannotAddIndices"));

      if (vertexIndex < 0)
      {
        System.Diagnostics.Debug.Assert(true, "Index cannot be negative.");
        return;
      }

      int baseVertexOffset = 0;

      if (offsetIndices)
        baseVertexOffset = CurrentVertexCount;

      m_indices.Set(vertexIndex + baseVertexOffset);
    }

    public void Reset(bool useIndexedPrimitives = true, bool prefer16BitIndices = false)
    {
      //Save the indices buffer if the previous mesh was using them
      if (m_useIndexedPrimitives)
        m_indices.Position = 0;

      m_useIndexedPrimitives = useIndexedPrimitives;
      m_indices.IndexFormat = (prefer16BitIndices) ? IndexFormat.SixteenBits : IndexFormat.ThirtyTwoBits;

      //Put the used buffers back into the pool
      foreach (KeyValuePair<TupleKey<VertexSemantic, int>, IList> kv in m_vertexAttributeMap)
      {
        PutBackInPool(kv.Value);
      }

      m_vertexAttributeMap.Clear();
    }

    public MeshData CreateMesh()
    {
      MeshData mesh = new MeshData();
      SaveToMesh(mesh);

      return mesh;
    }

    public void CreateMesh(MeshData existingMesh, bool mergeWithExisting = false)
    {
      ArgumentNullException.ThrowIfNull(existingMesh, nameof(existingMesh));

      if (mergeWithExisting && existingMesh.BufferCount != 0)
      {
        MeshData temp = CreateMesh();
        existingMesh.Merge(temp);
        temp.ClearData();
      }
      else
      {
        existingMesh.ClearData();
        SaveToMesh(existingMesh);
      }
    }

    private void SaveToMesh(MeshData mesh)
    {
      mesh.Indices = CreateIndexData(); //Sets use indexed primitives automatically

      foreach (KeyValuePair<TupleKey<VertexSemantic, int>, IList> kv in m_vertexAttributeMap)
      {
        VertexSemantic semantic = kv.Key.First;
        int index = kv.Key.Second;

        IDataBuffer data = CreateVertexData(semantic, index, kv.Value);
        mesh.AddBuffer(semantic, index, data);
      }
    }

    private IndexData? CreateIndexData()
    {
      if (m_useIndexedPrimitives)
        return m_indices.Claim(true);

      return null;
    }

    private IDataBuffer CreateVertexData(VertexSemantic semantic, int index, IList data)
    {
      Type dataType = data.GetType().GenericTypeArguments[0];
      MethodInfo method;

      lock (s_dataConversionMethods)
      {
        if (!s_dataConversionMethods.TryGetValue(dataType, out method))
        {
          method = typeof(MeshBuilder).GetMethod("CreateVertexDataInternal", BindingFlags.NonPublic | BindingFlags.Static);
          method = method.MakeGenericMethod(dataType);
          s_dataConversionMethods.Add(dataType, method);
        }
      }

      return method.Invoke(null, new Object[] { semantic, index, data }) as IDataBuffer;
    }

    private static IDataBuffer CreateVertexDataInternal<T>(VertexSemantic semantic, int index, IList data) where T : unmanaged
    {
      RefList<T> typedData = data as RefList<T>;
      DataBuffer<T> db = DataBuffer.Create<T>(typedData.Count);
      typedData.Span.CopyTo(db.Span);
      return db;
    }

    private RefList<T> GetListFor<T>(VertexSemantic semantic, int index, bool addIfNotFound) where T : struct
    {
      TupleKey<VertexSemantic, int> key = new TupleKey<VertexSemantic, int>(semantic, index);

      IList list;
      if (!m_vertexAttributeMap.TryGetValue(key, out list))
      {
        list = GetFromPool<T>();
        m_vertexAttributeMap.Add(key, list);
      }

      return list as RefList<T>;
    }

    private RefList<T> GetFromPool<T>() where T : struct
    {
      Type type = typeof(T);

      List<IList> pool;
      if (!m_bufferPool.TryGetValue(type, out pool))
      {
        pool = new List<IList>();
        m_bufferPool.Add(type, pool);
      }

      if (pool.Count > 0)
      {
        RefList<T> list = pool[pool.Count - 1] as RefList<T>;
        pool.RemoveAt(pool.Count - 1);

        return list;
      }
      else
      {
        return new RefList<T>();
      }
    }

    private void PutBackInPool(IList list)
    {
      list.Clear();

      Type type = list.GetType().GenericTypeArguments[0];

      List<IList> pool;
      if (!m_bufferPool.TryGetValue(type, out pool))
      {
        pool = new List<IList>();
        m_bufferPool.Add(type, pool);
      }

      pool.Add(list);
    }
  }
}
