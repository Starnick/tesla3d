﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Content;

// TODO NULLABLE

namespace Tesla.Graphics
{
  public interface IBatchData<T>
  {
    BoundingBox.Data Range { get; }

    void Draw(IRenderContext renderContext, T batcher);
  }

  public interface ILineBatchData
  {
    BoundingBox.Data Range { get; }

    void Draw(IRenderContext renderContext, LineBatch batcher);
  }

  public interface IBatchRenderable<T> : IRenderable
  {
    void Draw(IRenderContext renderContext, T batcher);
  }

  /// <summary>
  /// A renderable that can be instanced. Every instance is associated with a single instance definition that
  /// holds the logic to setup hardware instancing, the materials and geometry that will be drawn.
  /// </summary>
  public interface IInstancedRenderable : IRenderable
  {
    /// <summary>
    /// Gets the instance definition associated with this instance.
    /// </summary>
    InstanceDefinition InstanceDefinition { get; }

    /// <summary>
    /// Sets the instance definition and associate it with this object. This should not be called directly, instead
    /// call <see cref="InstanceDefinition.AddInstance(IInstancedRenderable)"/>.
    /// </summary>
    /// <param name="instanceDef">Instance definition to associate with.</param>
    /// <returns>True if the instance is now associated, false otherwise.</returns>
    bool SetInstanceDefinition(InstanceDefinition instanceDef);

    /// <summary>
    /// Removes the association between this object and the instance definition. This should not be called directly,
    /// instead call <see cref="InstanceDefinition.RemoveInstance(IInstancedRenderable)"/>.
    /// </summary>
    void RemoveInstanceDefinition();
  }

  /// <summary>
  /// A provider for per-instance data. A concrete example is the world transform of every instance. A single data provider can supply all
  /// the necessary data, or multiple ones can be used (and then mix-and-matched at will, all independent of each other).
  /// </summary>
  public interface IInstanceDataProvider : ISavable
  {
    /// <summary>
    /// Gets the name of the provider. Each provider should have a unique name to be identified by.
    /// </summary>
    String InstanceDataName { get; }

    /// <summary>
    /// Gets the data layout of the data that will be written in the <see cref="SetData"/> method.
    /// </summary>
    VertexElement[] DataLayout { get; }

    /// <summary>
    /// Gets the size of the data in bytes.
    /// </summary>
    int DataSizeInBytes { get; }

    /// <summary>
    /// Queries data from an instance's render properties and sets the data to a mapped data pointer. The data provider is responsible for
    /// incrementing the data pointer.
    /// </summary>
    /// <param name="instanceDefinition">Instance definition that is calling the data provider.</param>
    /// <param name="renderProperties">Render properties of the instance.</param>
    /// <param name="dstBuffer">Destination buffer at which to write data to.</param>
    void SetData(InstanceDefinition instanceDefinition, RenderPropertyCollection renderProperties, Span<byte> dstBuffer);
  }

  /// <summary>
  /// Defines an object that contains a <see cref="MeshData"/>.
  /// </summary>
  public interface IMeshDataContainer
  {
    /// <summary>
    /// Gets the mesh data.
    /// </summary>
    MeshData MeshData { get; }

    /// <summary>
    /// Gets submesh information, if it exists. This is the range in the geometry buffers of <see cref="MeshData"/> that
    /// make up the mesh, as <see cref="MeshData"/> may contain more than one mesh. If null, then the entire range of <see cref="MeshData"/> 
    /// is a single mesh.
    /// </summary>
    SubMeshRange? MeshRange { get; }
  }

  /// <summary>
  /// Defines an object that exists in world space. A spatial may be a single mesh or a collection of meshes.
  /// </summary>
  public interface ISpatial : IExtendedProperties
  {
    /// <summary>
    /// Gets the bounding of the object in world space.
    /// </summary>
    BoundingVolume WorldBounding { get; }

    /// <summary>
    /// Find intersections between the spatial and a ray pick query.
    /// </summary>
    /// <param name="query">Pick query</param>
    /// <returns>True if a pick was found, false if not.</returns>
    bool FindPicks(PickQuery query);

    /// <summary>
    /// Processes visible rendereables in the spatial, by doing frustum culling and then adding them
    /// to the renderer.
    /// </summary>
    /// <param name="renderer">Renderer to process renderables.</param>
    /// <param name="skipCullCheck">True if frustum culling should be skipped, false if renderables should be checked against the camera's frustum to ensure they are visible before rendering.</param>
    void ProcessVisibleSet(IRenderer renderer, bool skipCullCheck);
  }
}
