﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;

// TODO NULLABLE

namespace Tesla.Graphics
{
  /// <summary>
  /// Batcher for interleaved mesh data to draw many pieces of data in as few draw calls as possible. Supports all primitive 
  /// topologies as indexed and non-indexed, including drawing quads (as triangle lists). It is best to order batch submission based on topology and indexing,
  /// in order to reduce the number of draw calls (e.g. switching between lines and triangles or indexed geometry and non-indexed results in the batcher flushing
  /// queued data). The batcher also does not split geometry, so if a batch is submitted that is too large to fit then an exception will be thrown.
  /// Unlike other batchers, this is a low-level batcher, which means no material/shader/state setting is done. Setting up the render pipeline must be done before the batcher 
  /// does any drawing.
  /// </summary>
  /// <typeparam name="T">Type of vertex.</typeparam>
  public class PrimitiveBatch<T> : IDisposable where T : unmanaged, IVertexType
  {
    private const int MaxBatchVertexCount = 65535; //Let's take D3D9.1 max vertex index as a good starting max

    private VertexBuffer m_vertexBuffer;
    private IndexBuffer m_indexBuffer;
    private DataBuffer<T> m_vertices;
    private IndexData m_indices;

    public VertexBufferBinding[] InstancedVertexBuffers;
    public int Slot = 0;

    //For Draw*() methods
    private DataBuffer<T> m_tempVertices;
    private IndexData m_quadIndices;

    private int m_maxIndexCount;
    private int m_maxVertexCount;
    private int m_vertexStride;
    private bool m_isDisposed;

    private IRenderContext m_renderContext;
    private bool m_inBeginEnd;
    private bool m_isCurrentlyIndexed;
    private PrimitiveBatchTopology m_currentTopology;
    private int m_currentIndex;
    private int m_numIndicesInBatch;
    private int m_currentVertex;
    private int m_numVerticesInBatch;

    /// <summary>
    /// Gets if the batcher has been diposed.
    /// </summary>
    public bool IsDisposed
    {
      get
      {
        return m_isDisposed;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="PrimitiveBatch{T}"/> class.
    /// </summary>
    public PrimitiveBatch() : this(IRenderSystem.Current, MaxBatchVertexCount, false) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="PrimitiveBatch{T}"/> class.
    /// </summary>
    /// <param name="maxBatchVertexCount">Maximum number of vertices that can be buffered.</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the max batch size is less than or equal to zero.</exception>
    public PrimitiveBatch(int maxBatchVertexCount) : this(IRenderSystem.Current, maxBatchVertexCount, false) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="PrimitiveBatch{T}"/> class.
    /// </summary>
    /// <param name="maxBatchVertexCount">Maximum number of vertices that can be buffered.</param>
    /// <param name="noIndexBuffer">True if no index buffer should be created, false if an index buffer should be. Use this if you
    /// never will be submitted indexed batches.</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the max batch size is less than or equal to zero.</exception>
    public PrimitiveBatch(int maxBatchVertexCount, bool noIndexBuffer) : this(IRenderSystem.Current, maxBatchVertexCount, noIndexBuffer) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="PrimitiveBatch{T}"/> class.
    /// </summary>
    /// <param name="renderSystem">Render system used to create resources.</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the max batch size is less than or equal to zero.</exception>
    /// <exception cref="ArgumentNullException">Thrown if the render system is null.</exception>
    public PrimitiveBatch(IRenderSystem renderSystem) : this(renderSystem, MaxBatchVertexCount, false) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="PrimitiveBatch{T}"/> class.
    /// </summary>
    /// <param name="renderSystem">Render system used to create resources.</param>
    /// <param name="maxBatchVertexCount">Maximum number of vertices that can be buffered.</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the max batch size is less than or equal to zero.</exception>
    /// <exception cref="ArgumentNullException">Thrown if the render system is null.</exception>
    public PrimitiveBatch(IRenderSystem renderSystem, int maxBatchVertexCount) : this(renderSystem, maxBatchVertexCount, false) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="PrimitiveBatch{T}"/> class.
    /// </summary>
    /// <param name="renderSystem">Render system used to create resources.</param>
    /// <param name="maxBatchVertexCount">Maximum number of vertices that can be buffered.</param>
    /// <param name="noIndexBuffer">True if no index buffer should be created, false if an index buffer should be. Use this if you
    /// never will be submitted indexed batches. 16-bit or 32-bit indices automatically will be chose based on the max batch size.</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the max batch size is less than or equal to zero.</exception>
    /// <exception cref="ArgumentNullException">Thrown if the render system is null.</exception>
    public PrimitiveBatch(IRenderSystem renderSystem, int maxBatchVertexCount, bool noIndexBuffer)
    {
      //At least be able to fit the largest non-indexed primitive we can draw (a quad, split into two triangles)
      if (maxBatchVertexCount <= 6)
        throw new ArgumentOutOfRangeException(nameof(maxBatchVertexCount), StringLocalizer.Instance.GetLocalizedString("BatchSizeMustBePositive"));

      GraphicsHelper.ThrowIfRenderSystemNull(renderSystem);

      m_maxVertexCount = maxBatchVertexCount;
      m_maxIndexCount = (noIndexBuffer) ? 0 : maxBatchVertexCount;

      VertexLayout layout = new T().GetVertexLayout();
      m_vertexBuffer = new VertexBuffer(renderSystem, layout, m_maxVertexCount, VertexBufferOptions.Init(ResourceUsage.Dynamic));
      m_vertices = DataBuffer.Create<T>(m_maxVertexCount, MemoryAllocatorStrategy.DefaultPooled);
      m_tempVertices = DataBuffer.Create<T>(4, MemoryAllocatorStrategy.DefaultPooled);
      m_vertexStride = layout.VertexStride;

      if (!noIndexBuffer)
      {
        //Automatically determine if short or int indices will be used
        IndexFormat indexFormat = IndexFormat.ThirtyTwoBits;
        if (m_maxIndexCount < (int) short.MaxValue)
          indexFormat = IndexFormat.SixteenBits;

        m_indexBuffer = new IndexBuffer(renderSystem, indexFormat, m_maxIndexCount, BufferOptions.Init(ResourceUsage.Dynamic));
        m_indices = new IndexData(m_maxIndexCount, indexFormat, MemoryAllocatorStrategy.DefaultPooled);

        Span<ushort> shortIndices = stackalloc ushort[4] { 0, 1, 2, 3 };
        m_quadIndices = new IndexData(shortIndices, MemoryAllocatorStrategy.DefaultPooled);
      }

      m_inBeginEnd = false;
      m_isCurrentlyIndexed = false;
      m_currentTopology = PrimitiveBatchTopology.TriangleList;
      m_currentVertex = 0;
      m_numVerticesInBatch = 0;
      m_currentIndex = 0;
      m_numIndicesInBatch = 0;
    }

    /// <summary>
    /// Queries the maximum number of primitives that can be in a single batch. This will vary based on topology.
    /// </summary>
    /// <param name="topology">Topology of the geometry.</param>
    /// <returns>The maximum number of primitives that can be submitted in a single batch.</returns>
    public int QueryMaximumPrimitiveCount(PrimitiveBatchTopology topology)
    {
      switch (topology)
      {
        //Quadlist is a bit tricky since we create triangles, so a straight up # of quads based on vertex count won't be accurate, so take the number
        //of triangles and then divide that by 2 since worst case, if we have non-indexed geometry of N quads, we'll have 2N triangles to deal with.
        case PrimitiveBatchTopology.QuadList:
          return (m_maxVertexCount / 3) / 2;
        case PrimitiveBatchTopology.TriangleList:
          return m_maxVertexCount / 3;
        case PrimitiveBatchTopology.LineList:
          return m_maxVertexCount / 2;
        case PrimitiveBatchTopology.PointList:
          return m_maxVertexCount;
        case PrimitiveBatchTopology.TriangleStrip:
          return m_maxVertexCount - 2;
        case PrimitiveBatchTopology.LineStrip:
          return m_maxVertexCount - 1;
        default:
          return 0;
      }
    }

    /// <summary>
    /// Begins a primitive batch operation. No state is changed on the render context, those changes should be set before this
    /// method is called.
    /// </summary>
    /// <param name="renderContext">Render context to be used to issue draw calls.</param>
    public void Begin(IRenderContext renderContext)
    {
      CheckDisposed();

      if (m_inBeginEnd)
        throw new InvalidOperationException(StringLocalizer.Instance.GetLocalizedString("CannotNestBeginCalls"));

      if (renderContext == null)
        throw new ArgumentNullException("renderContext", StringLocalizer.Instance.GetLocalizedString("NullRenderContext"));

      m_renderContext = renderContext;
      m_inBeginEnd = true;

      //If deferred, ensure we always do a write discard for the first call
      if (!m_renderContext.IsImmediateContext)
      {
        m_currentVertex = 0;
        m_currentIndex = 0;
      }

      SetBuffers();
    }

    /// <summary>
    /// Flushes the primitive batch, causing any queued batches to be drawn. This does not change any state on the render context.
    /// </summary>
    public void End()
    {
      CheckDisposed();

      if (!m_inBeginEnd)
        throw new InvalidOperationException(StringLocalizer.Instance.GetLocalizedString("EndCalledBeforeBegin"));

      ProcessRenderQueue();
      m_inBeginEnd = false;
    }

    /// <summary>
    /// Convenience method for adding a line to the batch. This will always draw in non-indexed mode.
    /// </summary>
    /// <param name="v0">First vertex.</param>
    /// <param name="v1">Second vertex.</param>
    public void DrawLine(in T v0, in T v1)
    {
      m_tempVertices[0] = v0;
      m_tempVertices[1] = v1;

      Draw(PrimitiveBatchTopology.LineList, m_tempVertices, 0, 2);
    }

    /// <summary>
    /// Convenience method for adding a triangle to the batch. Assumed clockwise winding with the first vertex
    /// being the top left corner. This will always draw in non-indexed mode.
    /// </summary>
    /// <param name="v0">First vertex.</param>
    /// <param name="v1">Second vertex.</param>
    /// <param name="v2">Third vertex.</param>
    public void DrawTriangle(in T v0, in T v1, in T v2)
    {
      m_tempVertices[0] = v0;
      m_tempVertices[1] = v1;
      m_tempVertices[2] = v2;

      Draw(PrimitiveBatchTopology.TriangleList, m_tempVertices, 0, 3);
    }

    /// <summary>
    /// Convenience method for adding a quad to the batch. Assumed clockwise winding with the first vertex
    /// being the top left corner. This will follow the current index mode.
    /// </summary>
    /// <param name="v0">First vertex.</param>
    /// <param name="v1">Second vertex.</param>
    /// <param name="v2">Third vertex.</param>
    /// <param name="v3">Fourth vertex.</param>
    public void DrawQuad(in T v0, in T v1, in T v2, in T v3)
    {
      m_tempVertices[0] = v0;
      m_tempVertices[1] = v1;
      m_tempVertices[2] = v2;
      m_tempVertices[3] = v3;

      if (m_maxIndexCount > 0)
      {
        DrawIndexed(PrimitiveBatchTopology.QuadList, m_tempVertices, 0, 4, m_quadIndices, 0, 4);
      }
      else
      {
        Draw(PrimitiveBatchTopology.QuadList, m_tempVertices, 0, 4);
      }
    }

    /// <summary>
    /// Submits a mesh for batching. This may result in a draw call of queued batches if the capacity of the internal buffers 
    /// cannot fit the incoming batch, or if the topology/index mode changes, or if the batch is a triangle strip. 
    /// </summary>
    /// <param name="primTopology">Topology of the vertex data.</param>
    /// <param name="vertices">Vertex data.</param>
    /// <param name="vertexCount">Number of vertices to read.</param>
    /// <exception cref="ObjectDisposedException">Thrown if the batcher has been disposed.</exception>
    /// <exception cref="InvalidOperationException">Thrown if called before begin has been called.</exception>
    /// <exception cref="ArgumentNullException">Thrown if input data is null.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if offsets or counts to read from data will exceed capacity.</exception>
    public void Draw(PrimitiveBatchTopology primTopology, IReadOnlyDataBuffer<T> vertices)
    {
      Draw(primTopology, vertices, 0, (vertices != null) ? vertices.Length : 0);
    }

    /// <summary>
    /// Submits a mesh for batching. This may result in a draw call of queued batches if the capacity of the internal buffers 
    /// cannot fit the incoming batch, or if the topology/index mode changes, or if the batch is a triangle strip. 
    /// </summary>
    /// <param name="primTopology">Topology of the vertex data.</param>
    /// <param name="vertices">Vertex data.</param>
    /// <param name="startVertex">Offset in the vertex buffer to start reading from.</param>
    /// <param name="vertexCount">Number of vertices to read.</param>
    /// <exception cref="ObjectDisposedException">Thrown if the batcher has been disposed.</exception>
    /// <exception cref="InvalidOperationException">Thrown if called before begin has been called.</exception>
    /// <exception cref="ArgumentNullException">Thrown if input data is null.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if offsets or counts to read from data will exceed capacity.</exception>
    public void Draw(PrimitiveBatchTopology primTopology, IReadOnlyDataBuffer<T> vertices, int startVertex, int vertexCount)
    {
      CheckDisposed();

      if (!m_inBeginEnd)
        throw new InvalidOperationException(StringLocalizer.Instance.GetLocalizedString("DrawCalledBeforeBegin"));

      if (vertices == null)
        throw new ArgumentNullException("vertices");

      CheckArrayIndex(vertices.Length, startVertex, vertexCount, "startVertex", "vertexCount");

      int actualVertexCount = vertexCount;

      if (primTopology == PrimitiveBatchTopology.QuadList)
      {
        if ((actualVertexCount % 4) != 0)
          throw new ArgumentOutOfRangeException("vertexCount", StringLocalizer.Instance.GetLocalizedString("BadQuadIndexCount"));

        actualVertexCount = vertexCount + (vertexCount / 4) * 2;
      }

      if (actualVertexCount > m_maxVertexCount)
        throw new ArgumentOutOfRangeException("vertexCount", StringLocalizer.Instance.GetLocalizedString("TooManyIndices"));

      //Check if we can merge with previous batch
      bool wrapVertexBuffer = (m_currentVertex + actualVertexCount) > m_maxVertexCount;
      bool sameTopology = (primTopology == m_currentTopology) ||
          (primTopology == PrimitiveBatchTopology.QuadList && m_currentTopology == PrimitiveBatchTopology.TriangleList) ||
          (primTopology == PrimitiveBatchTopology.TriangleList && m_currentTopology == PrimitiveBatchTopology.QuadList);

      if (!sameTopology || m_isCurrentlyIndexed || !CanBatch(primTopology) || wrapVertexBuffer)
        ProcessRenderQueue();

      if (wrapVertexBuffer)
        m_currentVertex = 0;

      m_currentTopology = primTopology;
      m_isCurrentlyIndexed = false;

      //Each quad becomes two triangles (so we're adding new points), assumed to be clockwise from the top left corner:
      //
      //  P0   P1   P2   P3
      //     \         \
      //  P7   P6   P5   P4
      //
      // Has two quads {0, 1, 6, 7} and {2, 3, 4, 5} which becomes four triangles {0,1,6},{0,6,7}
      // and {2,3,4}, {2,4,5}.
      //
      if (m_currentTopology == PrimitiveBatchTopology.QuadList)
      {
        for (int i = startVertex; i < (startVertex + vertexCount); i += 4)
        {
          T p0 = vertices[i];
          T p1 = vertices[i + 1];
          T p2 = vertices[i + 2];
          T p3 = vertices[i + 3];

          m_vertices[m_currentVertex++] = p0;
          m_vertices[m_currentVertex++] = p1;
          m_vertices[m_currentVertex++] = p2;
          m_vertices[m_currentVertex++] = p0;
          m_vertices[m_currentVertex++] = p2;
          m_vertices[m_currentVertex++] = p3;
        }

        m_numVerticesInBatch += actualVertexCount;
      }
      else
      {
        vertices.Span.Slice(startVertex, actualVertexCount).CopyTo(m_vertices.Span.Slice(m_currentVertex, actualVertexCount));
        m_currentVertex += actualVertexCount;
        m_numVerticesInBatch += actualVertexCount;
      }
    }

    /// <summary>
    /// Submits an indexed mesh for batching. This may result in a draw call of queued batches if the capacity of the internal buffers 
    /// cannot fit the incoming batch, or if the topology/index mode changes, or if the batch is a triangle strip. 
    /// </summary>
    /// <param name="primTopology">Topology of the vertex data.</param>
    /// <param name="vertices">Vertex data.</param>
    /// <param name="indices">Index data.</param>
    /// <exception cref="ObjectDisposedException">Thrown if the batcher has been disposed.</exception>
    /// <exception cref="InvalidOperationException">Thrown if called before begin has been called.</exception>
    /// <exception cref="ArgumentNullException">Thrown if input data is null.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if offsets or counts to read from data will exceed capacity.</exception>
    public void DrawIndexed(PrimitiveBatchTopology primTopology, IReadOnlyDataBuffer<T> vertices, IndexData indices)
    {
      DrawIndexed(primTopology, vertices, 0, (vertices != null) ? vertices.Length : 0, indices, 0, (indices.IsValid) ? indices.Length : 0);
    }

    /// <summary>
    /// Submits an indexed mesh for batching. This may result in a draw call of queued batches if the capacity of the internal buffers 
    /// cannot fit the incoming batch, or if the topology/index mode changes, or if the batch is a triangle strip. 
    /// </summary>
    /// <param name="primTopology">Topology of the vertex data.</param>
    /// <param name="vertices">Vertex data.</param>
    /// <param name="startVertex">Offset in the vertex buffer to start reading from.</param>
    /// <param name="vertexCount">Number of vertices to read.</param>
    /// <param name="indices">Index data.</param>
    /// <param name="startIndex">Offset in the index buffer to start reading from.</param>
    /// <param name="indexCount">Number of indices to read.</param>
    /// <exception cref="ObjectDisposedException">Thrown if the batcher has been disposed.</exception>
    /// <exception cref="InvalidOperationException">Thrown if called before begin has been called.</exception>
    /// <exception cref="ArgumentNullException">Thrown if input data is null.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if offsets or counts to read from data will exceed capacity.</exception>
    public void DrawIndexed(PrimitiveBatchTopology primTopology, IReadOnlyDataBuffer<T> vertices, int startVertex, int vertexCount, IndexData indices, int startIndex, int indexCount)
    {
      CheckDisposed();

      if (!m_inBeginEnd)
        throw new InvalidOperationException(StringLocalizer.Instance.GetLocalizedString("DrawCalledBeforeBegin"));

      if (!indices.IsValid)
        throw new ArgumentNullException("indices");

      if (vertices == null)
        throw new ArgumentNullException("vertices");

      CheckArrayIndex(vertices.Length, startVertex, vertexCount, "startVertex", "vertexCount");
      CheckArrayIndex(indices.Length, startIndex, indexCount, "startIndex", "indexCount");

      int actualIndexCount = indexCount;

      if (primTopology == PrimitiveBatchTopology.QuadList)
      {
        if ((actualIndexCount % 4) != 0)
          throw new ArgumentOutOfRangeException("indexCount", StringLocalizer.Instance.GetLocalizedString("BadQuadIndexCount"));

        actualIndexCount = indexCount + ((indexCount / 4) * 2);
      }

      if (vertexCount > m_maxVertexCount)
        throw new ArgumentOutOfRangeException("vertexCount", StringLocalizer.Instance.GetLocalizedString("TooManyIndices"));

      if (actualIndexCount > m_maxIndexCount)
        throw new ArgumentOutOfRangeException("indexCount", StringLocalizer.Instance.GetLocalizedString("TooManyIndices"));

      //Check if we can merge with previous batch
      bool wrapVertexBuffer = (m_currentVertex + vertexCount) > m_maxVertexCount;
      bool wrapIndexBuffer = (m_currentIndex + actualIndexCount) > m_maxIndexCount;
      bool sameTopology = (primTopology == m_currentTopology) ||
          (primTopology == PrimitiveBatchTopology.QuadList && m_currentTopology == PrimitiveBatchTopology.TriangleList) ||
          (primTopology == PrimitiveBatchTopology.TriangleList && m_currentTopology == PrimitiveBatchTopology.QuadList);

      if (!sameTopology || !m_isCurrentlyIndexed || !CanBatch(primTopology) || wrapIndexBuffer || wrapVertexBuffer)
        ProcessRenderQueue();

      if (wrapIndexBuffer)
        m_currentIndex = 0;

      if (wrapVertexBuffer)
        m_currentVertex = 0;

      m_currentTopology = primTopology;
      m_isCurrentlyIndexed = true;
      m_numIndicesInBatch += actualIndexCount;

      if (m_currentTopology == PrimitiveBatchTopology.QuadList)
      {
        //Copy over index data and triangulate the indices (this will add indices)
        //Quad indices are assumed to be in clockwise fashion starting from the top left corner:
        //
        //  P0   P1   P2
        //     \    \
        //  P5   P4   P3
        //
        // Has indices {0,1,4,5} and {1,2,3,4} becomes six triangles with indices {0,1,4},{0,4,5}
        // and {1,2,3},{1,3,4}.
        //

        for (int i = startIndex; i < (startIndex + indexCount); i += 4)
        {
          int i0 = indices[i] + m_currentVertex;
          int i1 = indices[i + 1] + m_currentVertex;
          int i2 = indices[i + 2] + m_currentVertex;
          int i3 = indices[i + 3] + m_currentVertex;

          m_indices[m_currentIndex++] = i0;
          m_indices[m_currentIndex++] = i1;
          m_indices[m_currentIndex++] = i2;
          m_indices[m_currentIndex++] = i0;
          m_indices[m_currentIndex++] = i2;
          m_indices[m_currentIndex++] = i3;
        }
      }
      else
      {
        //Copy over index data
        for (int i = startIndex; i < (startIndex + actualIndexCount); i++)
          m_indices[m_currentIndex++] = (int) (indices[i] + m_currentVertex);
      }

      //Copy over vertex data
      vertices.Span.Slice(startVertex, vertexCount).CopyTo(m_vertices.Span.Slice(m_currentVertex, vertexCount));
      m_currentVertex += vertexCount;
      m_numVerticesInBatch += vertexCount;
    }

    /// <summary>
    /// Disposes of unmanaged resources.
    /// </summary>
    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    /// <summary>
    /// Disposes of unmanaged resources.
    /// </summary>
    /// <param name="isDisposing">True if dispose has been called, false if from destructor.</param>
    protected virtual void Dispose(bool isDisposing)
    {
      if (!m_isDisposed)
      {
        if (isDisposing)
        {
          if (m_vertexBuffer is not null)
          {
            m_vertexBuffer.Dispose();
            m_vertexBuffer = null;
          }

          if (m_indexBuffer is not null)
          {
            m_indexBuffer.Dispose();
            m_indexBuffer = null;
          }

          if (m_vertices is not null)
          {
            m_vertices.Dispose();
            m_vertices = null;
          }

          if (m_tempVertices is not null)
          {
            m_tempVertices.Dispose();
            m_tempVertices = null;
          }

          m_indices.Dispose();
          m_quadIndices.Dispose();
        }

        m_isDisposed = true;
      }
    }

    private void CheckDisposed()
    {
      ObjectDisposedException.ThrowIf(m_isDisposed, this);
    }

    private void SetBuffers()
    {
      if (InstancedVertexBuffers == null)
        m_renderContext.SetVertexBuffer(m_vertexBuffer);
      else
      {
        InstancedVertexBuffers[Slot] = new VertexBufferBinding(m_vertexBuffer, 0, 1);
        m_renderContext.SetVertexBuffers(InstancedVertexBuffers);
      }

      if (m_maxIndexCount > 0)
        m_renderContext.SetIndexBuffer(m_indexBuffer);
    }

    //Flush the currently queued primitives
    private void ProcessRenderQueue()
    {
      if (m_numVerticesInBatch == 0 || (m_isCurrentlyIndexed && m_numIndicesInBatch == 0))
        return;

      WriteVertexData();

      if (m_isCurrentlyIndexed)
        WriteIndexData();

      if (m_isCurrentlyIndexed)
      {
        m_renderContext.DrawIndexed(ToPrimitiveType(m_currentTopology), m_numIndicesInBatch, m_currentIndex - m_numIndicesInBatch, 0);
      }
      else
      {
        if (InstancedVertexBuffers == null)
          m_renderContext.Draw(ToPrimitiveType(m_currentTopology), m_numVerticesInBatch, m_currentVertex - m_numVerticesInBatch);
        else
          m_renderContext.DrawInstanced(ToPrimitiveType(m_currentTopology), 4, m_numVerticesInBatch, 0, m_currentVertex - m_numVerticesInBatch);
      }

      m_numIndicesInBatch = 0;
      m_numVerticesInBatch = 0;
    }

    private void WriteIndexData()
    {
      int startIndex = m_currentIndex - m_numIndicesInBatch;
      int offsetInBytes = ((m_indexBuffer.IndexFormat == IndexFormat.ThirtyTwoBits) ? sizeof(int) : sizeof(short)) * startIndex;

      DataWriteOptions writeOptions = (startIndex == 0) ? DataWriteOptions.Discard : DataWriteOptions.NoOverwrite;
      m_indexBuffer.SetData(m_renderContext, m_indices, startIndex, m_numIndicesInBatch, offsetInBytes, writeOptions);
    }

    private void WriteVertexData()
    {
      int startIndex = m_currentVertex - m_numVerticesInBatch;
      int offsetInBytes = m_vertexBuffer.VertexLayout.VertexStride * startIndex;

      DataWriteOptions writeOptions = (startIndex == 0) ? DataWriteOptions.Discard : DataWriteOptions.NoOverwrite;
      m_vertexBuffer.SetData<T>(m_renderContext, m_vertices.Span.Slice(startIndex, m_numVerticesInBatch), offsetInBytes, 0, writeOptions);
    }

    private static bool CanBatch(PrimitiveBatchTopology primTopology)
    {
      switch (primTopology)
      {
        case PrimitiveBatchTopology.TriangleList:
        case PrimitiveBatchTopology.LineList:
        case PrimitiveBatchTopology.PointList:
        case PrimitiveBatchTopology.QuadList:
          return true;
        default:
          return false;
      }
    }

    private static PrimitiveType ToPrimitiveType(PrimitiveBatchTopology primTopology)
    {
      switch (primTopology)
      {
        case PrimitiveBatchTopology.TriangleList:
        case PrimitiveBatchTopology.QuadList:
          return PrimitiveType.TriangleList;
        case PrimitiveBatchTopology.TriangleStrip:
          return PrimitiveType.TriangleStrip;
        case PrimitiveBatchTopology.LineList:
          return PrimitiveType.LineList;
        case PrimitiveBatchTopology.LineStrip:
          return PrimitiveType.LineStrip;
        case PrimitiveBatchTopology.PointList:
          return PrimitiveType.PointList;
        default:
          throw new TeslaGraphicsException();
      }
    }

    private static void CheckArrayIndex(int length, int index, int count, String paramIndexName, String paramCountName)
    {
      if (count < 0)
        throw new ArgumentOutOfRangeException(paramCountName, StringLocalizer.Instance.GetLocalizedString("CountMustBeGreaterThanOrEqualToZero"));

      if (index < 0)
        throw new ArgumentOutOfRangeException(paramIndexName, StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));

      if (index + count > length)
        throw new ArgumentOutOfRangeException(paramIndexName + paramCountName, StringLocalizer.Instance.GetLocalizedString("IndexAndCountOutOfRange"));
    }
  }
}
