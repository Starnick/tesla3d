﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#nullable enable

namespace Tesla.Graphics
{
  public enum LineJoinMode
  {
    None = 0,
    Simple = 1,
    Round = 2,
    Curve = 3,
    Miter = 4
  }

  public enum LineTopology
  {
    LineString = 1,
    ClosedLineString = 2,
    Points = 3
  }

  /// <summary>
  /// Defines the topology of mesh data drawn using <see cref="PrimitiveBatch{T}"/>.
  /// </summary>
  public enum PrimitiveBatchTopology
  {
    /// <summary>
    /// Vertex data is defined as a list of triangles, where every three new vertices make up a triangle.
    /// </summary>
    TriangleList = 0,

    /// <summary>
    /// Vertex data is defined as a list of triangles where every two new vertices, and a vertex from the 
    /// previous triangle make up a triangle.
    /// </summary>
    TriangleStrip = 1,

    /// <summary>
    /// Vertex data is defined as a list of lines where every two new vertices make up a line.
    /// </summary>
    LineList = 2,

    /// <summary>
    /// Vertex data is defined as a list of lines where every one new vertex and a vertex from the previous line
    /// make up a new line.
    /// </summary>
    LineStrip = 3,

    /// <summary>
    /// Vertex data is defined as a list of points.
    /// </summary>
    PointList = 4,

    /// <summary>
    /// Vertex data is defined as a list of quads, where every four new vertices make up a quad.
    /// </summary>
    QuadList = 5
  }

  /// <summary>
  /// Options for vertex attributes during geometry generation.
  /// </summary>
  [Flags]
  public enum GenerateOptions
  {
    /// <summary>
    /// Generate no vertex attributes.
    /// </summary>
    None = 0,

    /// <summary>
    /// Generate vertex positions.
    /// </summary>
    Positions = 1,

    /// <summary>
    /// Generate vertex normals.
    /// </summary>
    Normals = 2,

    /// <summary>
    /// Generate vertex texture coordinates.
    /// </summary>
    TextureCoordinates = 4,

    /// <summary>
    /// Generate all vertex attributes.
    /// </summary>
    All = Positions | Normals | TextureCoordinates
  }

  /// <summary>
  /// Sprite sort mode enumeration.
  /// </summary>
  public enum SpriteSortMode
  {
    /// <summary>
    /// Default, where sprites are queued and rendered all together in the order that they are received. No sorting
    /// is done.
    /// </summary>
    Deferred = 0,

    /// <summary>
    /// Same as deferred, except sort back to front using the sprite's depth order. Useful for transparent sprites.
    /// </summary>
    BackToFront = 1,

    /// <summary>
    /// Same as deferred, except sort front to back using the sprite's depth order. Useful for opaque sprites.
    /// </summary>
    FrontToBack = 2,

    /// <summary>
    /// Same as deferred, except sort by texture, where all sprites that use the same texture will be drawn as a single batch.
    /// </summary>
    Texture = 3,

    /// <summary>
    /// Incoming sprites are not sorted or queued, instead they are drawn immediately.
    /// </summary>
    Immediate = 4
  }

  /// <summary>
  /// Defines different flip effects for sprites.
  /// </summary>
  [Flags]
  public enum SpriteFlipEffect
  {
    /// <summary>
    /// No rotation.
    /// </summary>
    None = 0,

    /// <summary>
    /// Rotate 180 degrees about the Y-axis, mirroring horizontally.
    /// </summary>
    FlipHorizontally = 1,

    /// <summary>
    /// Rotate 180 degrees about the X-Axis, mirroring vertically.
    /// </summary>
    FlipVertically = 2
  }
}
