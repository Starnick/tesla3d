﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Runtime.InteropServices;
using Tesla.Content;

#nullable enable

namespace Tesla.Graphics
{
  /// <summary>
  /// Represents a vertex element in a vertex buffer.
  /// </summary>
  [Serializable, StructLayout(LayoutKind.Sequential)]
  public struct VertexElement : IEquatable<VertexElement>, IPrimitiveValue
  {
    /// <summary>
    /// Indicates if the offset of the vertex element should be aligned directly after the previous element, including any packing if necessary. Used
    /// for automatically calculating offsets.
    /// </summary>
    public const int AppendAligned = -1;

    /// <summary>
    /// Semantic name that this element is bound to.
    /// </summary>
    public VertexSemantic SemanticName;

    /// <summary>
    /// Semantic index this element is bound to.
    /// </summary>
    public int SemanticIndex;

    /// <summary>
    /// Format of this element.
    /// </summary>
    public VertexFormat Format;

    /// <summary>
    /// Offset in bytes from the start of the vertex data.
    /// </summary>
    public int Offset;

    /// <summary>
    /// Constructs a new instance of the <see cref="VertexElement"/> struct.
    /// </summary>
    /// <param name="semantic">The element's shader semantic.</param>
    /// <param name="semanticIndex">The element's shader semantic index</param>
    /// <param name="format">The element's format.</param>
    /// <param name="offset">The element's offset from the start of the vertex data.</param>
    public VertexElement(VertexSemantic semantic, int semanticIndex, VertexFormat format, int offset)
    {
      SemanticName = semantic;
      SemanticIndex = semanticIndex;
      Format = format;
      Offset = offset;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="VertexElement"/> struct. The element's offset is
    /// set to <see cref="AppendAligned"/>.
    /// </summary>
    /// <param name="semantic">The element's shader semantic.</param>
    /// <param name="semanticIndex">The element's shader semantic index</param>
    /// <param name="format">The element's format.</param>
    public VertexElement(VertexSemantic semantic, int semanticIndex, VertexFormat format)
    {
      SemanticName = semantic;
      SemanticIndex = semanticIndex;
      Format = format;
      Offset = AppendAligned;
    }

    /// <summary>
    /// Tests equality between two vertex elements.
    /// </summary>
    /// <param name="a">First vertex element</param>
    /// <param name="b">Second vertex element</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    public static bool operator ==(VertexElement a, VertexElement b)
    {
      return (a.SemanticName == b.SemanticName) && (a.SemanticIndex == b.SemanticIndex) && (a.Offset == b.Offset) && (a.Format == b.Format);
    }

    /// <summary>
    /// Tests inequality between two vertex elements.
    /// </summary>
    /// <param name="a">First vertex element</param>
    /// <param name="b">Second vertex element</param>
    /// <returns>True if both are not equal, false otherwise.</returns>
    public static bool operator !=(VertexElement a, VertexElement b)
    {
      return (a.SemanticName != b.SemanticName) || (a.SemanticIndex != b.SemanticIndex) || (a.Offset != b.Offset) || (a.Format != b.Format);
    }

    /// <summary>
    /// Indicates whether this instance and a specified object are equal.
    /// </summary>
    /// <param name="obj">Another object to compare to.</param>
    /// <returns>True if <paramref name="obj" /> and this instance are the same type and represent the same value; otherwise, false.</returns>
    public readonly override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is VertexElement)
      {
        VertexElement other = (VertexElement) obj;
        return (SemanticName == other.SemanticName) && (SemanticIndex == other.SemanticIndex) && (Offset == other.Offset) && (Format == other.Format);
      }

      return false;
    }

    /// <summary>
    /// Tests equality between this vertex element and another.
    /// </summary>
    /// <param name="other">Other vertex element to compare to</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    public readonly bool Equals(VertexElement other)
    {
      return (SemanticName == other.SemanticName) && (SemanticIndex == other.SemanticIndex) && (Offset == other.Offset) && (Format == other.Format);
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
    public readonly override int GetHashCode()
    {
      unchecked
      {
        int hash = 17;

        hash = (hash * 31) + SemanticName.GetHashCode();
        hash = (hash * 31) + SemanticIndex.GetHashCode();
        hash = (hash * 31) + Format.GetHashCode();
        hash = (hash * 31) + Offset.GetHashCode();

        return hash;
      }
    }

    /// <summary>
    /// Returns the fully qualified type name of this instance.
    /// </summary>
    /// <returns>A <see cref="T:System.String" /> containing a fully qualified type name.</returns>
    public readonly override string ToString()
    {
      return String.Format(CultureInfo.CurrentCulture, "SemanticName: {0}, SemanticIndex: {1}, Format: {2}, Offset: {3}", new object[] { SemanticName.ToString(), SemanticIndex.ToString(), Format.ToString(), Offset.ToString() });
    }

    /// <summary>
    /// Writes the primitive data to the output.
    /// </summary>
    /// <param name="output">Primitive writer.</param>
    public readonly void Write(IPrimitiveWriter output)
    {
      output.WriteEnum<VertexSemantic>("SemanticName", SemanticName);
      output.Write("SemanticIndex", SemanticIndex);
      output.WriteEnum<VertexFormat>("VertexFormat", Format);
      output.Write("Offset", Offset);
    }

    /// <summary>
    /// Reads the primitive data from the input.
    /// </summary>
    /// <param name="input">Primitive reader.</param>
    public void Read(IPrimitiveReader input)
    {
      SemanticName = input.ReadEnum<VertexSemantic>("SemanticName");
      SemanticIndex = input.ReadInt32("SemanticIndex");
      Format = input.ReadEnum<VertexFormat>("VertexFormat");
      Offset = input.ReadInt32("Offset");
    }
  }
}
