﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Globalization;

#nullable enable

namespace Tesla.Input
{
  /// <summary>
  /// Represents a button that can either be a keyboard key or a mouse button.
  /// </summary>
  public struct KeyOrMouseButton : IEquatable<KeyOrMouseButton>
  {
    private Keys m_key;
    private MouseButton m_mouseButton;
    private bool m_isMouseButton;

    /// <summary>
    /// gets the key.
    /// </summary>
    public Keys Key
    {
      get
      {
        return m_key;
      }
    }

    /// <summary>
    /// Gets the mouse button.
    /// </summary>
    public MouseButton MouseButton
    {
      get
      {
        return m_mouseButton;
      }
    }

    /// <summary>
    /// Gets if the button is a <see cref="MouseButton"/>, if false then
    /// it is a <see cref="Key"/>.
    /// </summary>
    public bool IsMouseButton
    {
      get
      {
        return m_isMouseButton;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="KeyOrMouseButton"/> struct.
    /// </summary>
    /// <param name="key">Key.</param>
    public KeyOrMouseButton(Keys key)
    {
      m_key = key;
      m_mouseButton = Input.MouseButton.Left;
      m_isMouseButton = false;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="KeyOrMouseButton"/> struct.
    /// </summary>
    /// <param name="button">Mouse button.</param>
    public KeyOrMouseButton(MouseButton button)
    {
      m_key = Keys.A;
      m_mouseButton = button;
      m_isMouseButton = true;
    }

    /// <summary>
    /// Performs an implicit conversion from <see cref="Keys"/> to <see cref="KeyOrMouseButton"/>.
    /// </summary>
    /// <param name="key">Key.</param>
    /// <returns>The result of the conversion.</returns>
    public static implicit operator KeyOrMouseButton(Keys key)
    {
      return new KeyOrMouseButton(key);
    }

    /// <summary>
    /// Performs an implicit conversion from <see cref="MouseButton"/> to <see cref="KeyOrMouseButton"/>.
    /// </summary>
    /// <param name="button">Mouse button.</param>
    /// <returns>The result of the conversion.</returns>
    public static implicit operator KeyOrMouseButton(MouseButton button)
    {
      return new KeyOrMouseButton(button);
    }

    /// <summary>
    /// Queries if the set of bindings has at least one mouse binding.
    /// </summary>
    /// <param name="bindings">Mouse bindings.</param>
    /// <returns>True if the set has a mouse binding, false if otherwise.</returns>
    public bool HasMouse(params KeyOrMouseButton[] bindings)
    {
      if (bindings == null || bindings.Length == 0)
        return false;

      for (int i = 0; i < bindings.Length; i++)
      {
        if (bindings[i].IsMouseButton)
          return true;
      }

      return false;
    }

    /// <summary>
    /// Creates a default input condition based on the input binding.
    /// </summary>
    /// <param name="binding">Key or mouse binding.</param>
    /// <param name="onRelease">True if the condition should be on released, false if on pressed.</param>
    /// <returns>Input condition.</returns>
    public static InputCondition CreateInputCondition(KeyOrMouseButton binding, bool onRelease)
    {
      return CreateInputCondition(binding, onRelease, false, true);
    }

    /// <summary>
    /// Creates a default input condition based on the input binding.
    /// </summary>
    /// <param name="binding">Key or mouse binding.</param>
    /// <param name="onRelease">True if the condition should be on released, false if on pressed.</param>
    /// <param name="allowRepeats">True if the condition should allow repeats, false otherwise. Only valid if the condition is on pressed.</param>
    /// <returns>Input condition.</returns>
    public static InputCondition CreateInputCondition(KeyOrMouseButton binding, bool onRelease, bool allowRepeats)
    {
      return CreateInputCondition(binding, onRelease, allowRepeats, true);
    }

    /// <summary>
    /// Creates a default input condition based on the input binding.
    /// </summary>
    /// <param name="binding">Key or mouse binding.</param>
    /// <param name="onRelease">True if the condition should be on released, false if on pressed.</param>
    /// <param name="allowRepeats">True if the condition should allow repeats, false otherwise. Only valid if the condition is on pressed.</param>
    /// <param name="allowMultipleButtons">True if the condition can evaluate true even if other buttons are pressed, false if the condition can only evaluate true
    /// if only the specified button is pressed.</param>
    /// <returns>Input condition.</returns>
    public static InputCondition CreateInputCondition(KeyOrMouseButton binding, bool onRelease, bool allowRepeats, bool allowMultipleButtons)
    {
      if (onRelease)
      {
        return new KeyReleasedCondition(binding, allowMultipleButtons);
      }
      else
      {
        return new KeyPressedCondition(binding, allowRepeats, allowMultipleButtons);
      }
    }

    /// <summary>
    /// Creates a default input condition based on the input binding.
    /// </summary>
    /// <param name="onRelease">True if the condition should be on released, false if on pressed.</param>
    /// <param name="allowRepeats">True if the condition should allow repeats, false otherwise. Only valid if the condition is on pressed.</param>
    /// <param name="bindings">Key or mouse binding.</param>
    /// <returns>Input condition.</returns>
    public static InputCondition CreateInputCondition(bool onRelease, bool allowRepeats, params KeyOrMouseButton[] bindings)
    {
      if (bindings is null || bindings.Length == 0)
        throw new ArgumentNullException(nameof(bindings));

      if (bindings.Length == 1)
        return CreateInputCondition(bindings[0], onRelease, allowRepeats);

      if (onRelease)
      {
        return new MultiKeyReleasedCondition(bindings);
      }
      else
      {
        return new MultiKeyPressedCondition(allowRepeats, bindings);
      }
    }

    /// <summary>
    /// Creates an input condition based on a map of key bindings, if the entry exists, otherwise create a condition based on the default binding.
    /// </summary>
    /// <param name="keyBindings">Collection of input bindings.</param>
    /// <param name="bindingName">Name of the binding to search in the dictionary.</param>
    /// <param name="defaultKey">Default key or mouse binding.</param>
    /// <param name="onRelease">True if the condition should be on released, false if on pressed.</param>
    /// <param name="allowRepeats">True if the condition should allow repeats, false otherwise. Only valid if the condition is on pressed.</param>
    /// <returns>Input condition.</returns>
    public static InputCondition CreateInputCondition(IReadOnlyDictionary<String, KeyOrMouseButton[]> keyBindings, String bindingName, KeyOrMouseButton defaultKey, bool onRelease, bool allowRepeats)
    {
      if (keyBindings == null || String.IsNullOrEmpty(bindingName))
        return KeyOrMouseButton.CreateInputCondition(defaultKey, onRelease, allowRepeats);

      KeyOrMouseButton[] binding;
      if (!keyBindings.TryGetValue(bindingName, out binding))
        return KeyOrMouseButton.CreateInputCondition(defaultKey, onRelease, allowRepeats);

      return KeyOrMouseButton.CreateInputCondition(onRelease, allowRepeats, binding);
    }

    /// <summary>
    /// Determines whether the specified <see cref="System.Object" />, is equal to this instance.
    /// </summary>
    /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
    /// <returns><c>True</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.</returns>
    public override bool Equals(object obj)
    {
      if (obj is KeyOrMouseButton)
        return Equals((KeyOrMouseButton) obj);

      return false;
    }

    /// <summary>
    /// Indicates whether the current object is equal to another object of the same type.
    /// </summary>
    /// <param name="other">An object to compare with this object.</param>
    /// <returns>True if the current object is equal to the <paramref name="other" /> parameter; otherwise, false.</returns>
    public bool Equals(KeyOrMouseButton other)
    {
      if (IsMouseButton == other.IsMouseButton)
      {
        if (IsMouseButton)
        {
          return MouseButton == other.MouseButton;
        }
        else
        {
          return Key == other.Key;
        }
      }

      return false;
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
    public override int GetHashCode()
    {
      unchecked
      {
        return (m_isMouseButton) ? m_mouseButton.GetHashCode() : m_key.GetHashCode();
      }
    }

    /// <summary>
    /// Returns the fully qualified type name of this instance.
    /// </summary>
    /// <returns>A <see cref="T:System.String" /> containing a fully qualified type name.</returns>
    public override String ToString()
    {
      CultureInfo info = CultureInfo.CurrentCulture;
      if (m_isMouseButton)
      {
        return String.Format(info, "MouseButton: {0}", m_mouseButton.ToString());
      }
      else
      {
        return String.Format(info, "Key: {0}", m_key.ToString());
      }
    }
  }
}
