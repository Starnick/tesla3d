﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System.Runtime.CompilerServices;

#nullable enable

namespace Tesla.Input
{
  /// <summary>
  /// Provides convienent access to a registered <see cref="IKeyboardInputSystem"/> service.
  /// </summary>
  public static class Keyboard
  {
    /// <summary>
    /// Gets if the keyboard is available.
    /// </summary>
    public static bool IsAvailable
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return CoreServices.KeyboardInputSystem.Service is not null;
      }
    }

    /// <summary>
    /// Gets or sets what system currently has keyboard focus and should respond to input events.
    /// </summary>
    public static InputFocusState FocusState
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return CoreServices.KeyboardInputSystem.Service?.FocusState ?? InputFocusState.None;
      }
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      set
      {
        IKeyboardInputSystem? keyboardSystem = CoreServices.KeyboardInputSystem.Service;
        if (keyboardSystem is not null)
          keyboardSystem.FocusState = value;
      }
    }

    /// <summary>
    /// Queries the current state of the keyboard - e.g. which keys are pressed.
    /// </summary>
    /// <returns>The current keyboard state.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static KeyboardState GetKeyboardState()
    {
      IKeyboardInputSystem? keyboardSystem = CoreServices.KeyboardInputSystem.Service;
      if (keyboardSystem is null)
        return new KeyboardState();

      keyboardSystem.GetKeyboardState(out KeyboardState state);
      return state;
    }
  }
}
