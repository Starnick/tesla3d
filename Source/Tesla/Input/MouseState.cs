﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.InteropServices;

#nullable enable

namespace Tesla.Input
{
  /// <summary>
  /// Represents the state of the mouse, e.g. where the cursor is located on the screen (client coordinates), and what buttons are pressed.
  /// </summary>
  [StructLayout(LayoutKind.Sequential)]
  public struct MouseState : IEquatable<MouseState>
  {
    private int m_x;
    private int m_y;
    private int m_wheel;
    private int m_wheelDelta;
    private ButtonState m_leftButton;
    private ButtonState m_rightButton;
    private ButtonState m_middleButton;
    private ButtonState m_xb1;
    private ButtonState m_xb2;
    private InputFocusState m_focusState;

    /// <summary>
    /// Gets the position of the mouse as a float vector.
    /// </summary>
    public readonly Vector2 Position
    {
      get
      {
        return new Vector2(m_x, m_y);
      }
    }

    /// <summary>
    /// Gets the position of the mouse as an int vector.
    /// </summary>
    public readonly Int2 PositionInt
    {
      get
      {
        return new Int2(m_x, m_y);
      }
    }

    /// <summary>
    /// Gets the x (horizontal) coordinate of the mouse position.
    /// </summary>
    public readonly int X
    {
      get
      {
        return m_x;
      }
    }

    /// <summary>
    /// Gets the y (vertical) coordinate of the mouse position.
    /// </summary>
    public readonly int Y
    {
      get
      {
        return m_y;
      }
    }

    /// <summary>
    /// Gets the mouse scroll absolute wheel value.
    /// </summary>
    public readonly int ScrollWheelValue
    {
      get
      {
        return m_wheel;
      }
    }

    /// <summary>
    /// Gets the mouse scroll wheel delta.
    /// </summary>
    public readonly int ScrollWheelDelta
    {
      get
      {
        return m_wheelDelta;
      }
    }

    /// <summary>
    /// Gets the mouse scroll wheel delta normalized into the range [-1, 1].
    /// </summary>
    public readonly int ScrollWheelDeltaNormalized
    {
      get
      {
        return MathHelper.Clamp(m_wheelDelta, -1, 1);
      }
    }

    /// <summary>
    /// Gets the state of the left mouse button.
    /// </summary>
    public readonly ButtonState LeftButton
    {
      get
      {
        return m_leftButton;
      }
    }

    /// <summary>
    /// Gets the state of the right mouse button.
    /// </summary>
    public readonly ButtonState RightButton
    {
      get
      {
        return m_rightButton;
      }
    }

    /// <summary>
    /// Gets the state of the middle mouse button.
    /// </summary>
    public readonly ButtonState MiddleButton
    {
      get
      {
        return m_middleButton;
      }
    }

    /// <summary>
    /// Gets the state of the XButton1.
    /// </summary>
    public readonly ButtonState XButton1
    {
      get
      {
        return m_xb1;
      }
    }

    /// <summary>
    /// Gets the state of the XButton2.
    /// </summary>
    public readonly ButtonState XButton2
    {
      get
      {
        return m_xb2;
      }
    }

    /// <summary>
    /// Gets which system has mouse focus.
    /// </summary>
    public readonly InputFocusState FocusState
    {
      get
      {
        return m_focusState;
      }
    }

    /// <summary>
    /// Query the button state for the specified mouse button.
    /// </summary>
    /// <param name="button">Specified mouse button</param>
    /// <returns>Button state</returns>
    public readonly ButtonState this[MouseButton button]
    {
      get
      {
        switch (button)
        {
          case MouseButton.Left:
            return m_leftButton;
          case MouseButton.Middle:
            return m_middleButton;
          case MouseButton.Right:
            return m_rightButton;
          case MouseButton.XButton1:
            return m_xb1;
          case MouseButton.XButton2:
            return m_xb2;
          default:
            return ButtonState.Released;
        }
      }
    }

    /// <summary>
    /// Queries the number of mouse buttons currently pressed.
    /// </summary>
    public readonly int PressedButtonCount
    {
      get
      {
        int count = 0;
        if (m_leftButton == ButtonState.Pressed)
          count++;
        if (m_rightButton == ButtonState.Pressed)
          count++;
        if (m_middleButton == ButtonState.Pressed)
          count++;
        if (m_xb1 == ButtonState.Pressed)
          count++;
        if (m_xb2 == ButtonState.Pressed)
          count++;

        return count;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="MouseState"/> struct.
    /// </summary>
    /// <param name="x">The x (horizontal) position of the mouse. Typically these are client coordinates.</param>
    /// <param name="y">The y (verticaly) position of the mouse. Typically these are client coordinates.</param>
    /// <param name="scrollWheel">The scroll wheel value.</param>
    /// <param name="scrollWheelDelta">The scroll wheel delta value.</param>
    /// <param name="leftButton">The left button state.</param>
    /// <param name="rightButton">The right button state.</param>
    /// <param name="middleButton">The middle button state.</param>
    /// <param name="xButton1">The XButton1 state. Default value is <see cref="ButtonState.Released"/>.</param>
    /// <param name="xButton2">The XButton2 state. Default value is <see cref="ButtonState.Released"/>.</param>
    /// <param name="focusState">Which system has keyboard focus. Default is <see cref="InputFocusState.App"/>.</param>
    public MouseState(int x, int y, int scrollWheel, int scrollWheelDelta, ButtonState leftButton, ButtonState rightButton,
        ButtonState middleButton, ButtonState xButton1 = ButtonState.Released, ButtonState xButton2 = ButtonState.Released, InputFocusState focusState = InputFocusState.App)
    {
      m_x = x;
      m_y = y;
      m_wheel = scrollWheel;
      m_wheelDelta = scrollWheelDelta;
      m_leftButton = leftButton;
      m_rightButton = rightButton;
      m_middleButton = middleButton;
      m_xb1 = xButton1;
      m_xb2 = xButton2;
      m_focusState = focusState;
    }

    /// <summary>
    /// Queries if any button is currently pressed.
    /// </summary>
    /// <returns>True if any button is pressed, false otherwise.</returns>
    public readonly bool IsAnyButtonPressed()
    {
      return m_leftButton == ButtonState.Pressed || m_rightButton == ButtonState.Pressed || m_middleButton == ButtonState.Pressed || m_xb1 == ButtonState.Pressed
          || m_xb2 == ButtonState.Pressed;
    }

    /// <summary>
    /// Query if the specified button is pressed.
    /// </summary>
    /// <param name="button">Mousebutton to query</param>
    /// <returns>True if the button is pressed, false otherwise.</returns>
    public readonly bool IsButtonPressed(MouseButton button)
    {
      return this[button] == ButtonState.Pressed;
    }

    /// <summary>
    /// Query if the specified button is released.
    /// </summary>
    /// <param name="button">Mousebutton to query</param>
    /// <returns>True if the button is released, false otherwise.</returns>
    public readonly bool IsButtonReleased(MouseButton button)
    {
      return this[button] == ButtonState.Released;
    }

    /// <summary>
    /// Gets all the pressed buttons in the current state.
    /// </summary>
    /// <param name="pressedKeys">List to add pressed buttons to.</param>
    public readonly void GetPressedButtons(IList<MouseButton> pressedButtons)
    {
      if (pressedButtons is null)
        return;

      if (m_leftButton == ButtonState.Pressed)
        pressedButtons.Add(MouseButton.Left);

      if (m_rightButton == ButtonState.Pressed)
        pressedButtons.Add(MouseButton.Right);

      if (m_middleButton == ButtonState.Pressed)
        pressedButtons.Add(MouseButton.Middle);

      if (m_xb1 == ButtonState.Pressed)
        pressedButtons.Add(MouseButton.XButton1);

      if (m_xb2 == ButtonState.Pressed)
        pressedButtons.Add(MouseButton.XButton2);
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
    public readonly override int GetHashCode()
    {
      unchecked
      {
        int hash = 17;

        hash = (hash * 31) + m_x.GetHashCode();
        hash = (hash * 31) + m_y.GetHashCode();
        hash = (hash * 31) + m_wheel.GetHashCode();
        hash = (hash * 31) + m_leftButton.GetHashCode();
        hash = (hash * 31) + m_middleButton.GetHashCode();
        hash = (hash * 31) + m_rightButton.GetHashCode();
        hash = (hash * 31) + m_xb1.GetHashCode();
        hash = (hash * 31) + m_xb2.GetHashCode();
        return hash;
      }
    }

    /// <summary>
    /// Determines whether the specified <see cref="System.Object" /> is equal to this instance.
    /// </summary>
    /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
    /// <returns>True if the specified <see cref="System.Object" /> is equal to this instance; otherwise, false.</returns>
    public readonly override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is MouseState)
        return (this == (MouseState) obj);

      return false;
    }

    /// <summary>
    /// Indicates whether the current object is equal to another object of the same type.
    /// </summary>
    /// <param name="other">An object to compare with this object.</param>
    /// <returns>True if the current object is equal to the <paramref name="other" /> parameter; otherwise, false.</returns>
    public readonly bool Equals(MouseState other)
    {
      return (this == other);
    }

    /// <summary>
    /// Tests if two mouse states are equal.
    /// </summary>
    /// <param name="a">First mouse state</param>
    /// <param name="b">Second mouse state</param>
    /// <returns>True if equal, false otherwise</returns>
    public static bool operator ==(MouseState a, MouseState b)
    {
      return (a.m_x == b.m_x) && (a.m_y == b.m_y) && (a.m_wheel == b.m_wheel)
          && (a.m_leftButton == b.m_leftButton) && (a.m_middleButton == b.m_middleButton) && (a.m_rightButton == b.m_rightButton)
          && (a.m_xb1 == b.m_xb1) && (a.m_xb2 == b.m_xb2);
    }

    /// <summary>
    /// Tests if two mouse states are not equal.
    /// </summary>
    /// <param name="a">First mouse state</param>
    /// <param name="b">Second mouse state</param>
    /// <returns>True if not equal, false otherwise</returns>
    public static bool operator !=(MouseState a, MouseState b)
    {
      return (a.m_x != b.m_x) || (a.m_y != b.m_y) || (a.m_wheel != b.m_wheel)
          || (a.m_leftButton != b.m_leftButton) || (a.m_middleButton != b.m_middleButton) || (a.m_rightButton != b.m_rightButton)
          || (a.m_xb1 != b.m_xb1) || (a.m_xb2 != b.m_xb2);
    }
  }
}
