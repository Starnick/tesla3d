﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;

#nullable enable

namespace Tesla.Input
{
  /// <summary>
  /// Represents a composite input condition where all conditions must be satisfied by some input. If the condition is true,
  /// then the action can be performed. This is paired with an InputAction inside an InputTrigger. Actions and conditions can 
  /// vary independently from one another, allowing for their reuse.
  /// </summary>
  public sealed class CompositeInputCondition : InputCondition
  {
    private List<InputCondition> m_conditions;
    private bool[] m_states;

    /// <summary>
    /// Gets the list of contained conditions.
    /// </summary>
    public IReadOnlyList<InputCondition> Conditions
    {
      get
      {
        return m_conditions;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="CompositeInputCondition"/> class.
    /// </summary>
    /// <param name="conditions">Input conditions.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the array of conditions is null or any condition are null.</exception>
    public CompositeInputCondition(params InputCondition[] conditions)
    {
      if (conditions is null || conditions.Length == 0)
        throw new ArgumentNullException(nameof(conditions));

      m_conditions = new List<InputCondition>(conditions.Length);
      for (int i = 0; i < conditions.Length; i++)
      {
        InputCondition condition = conditions[i];
        if (condition is null)
          throw new ArgumentNullException(nameof(condition));

        m_conditions.Add(condition);
      }
      m_states = new bool[conditions.Length];
    }

    /// <summary>
    /// Checks if the condition has been satisfied or not.
    /// </summary>
    /// <param name="time">Time elapsed since the last update.</param>
    /// <returns>True if the condition has been satisfied, false otherwise.</returns>
    public override bool Check(IGameTime time)
    {
      //First have every condition checked, to ensure all states get updated
      for (int i = 0; i < m_conditions.Count; i++)
        m_states[i] = m_conditions[i].Check(time);

      //Check if any failed, if so return
      for (int i = 0; i < m_states.Length; i++)
      {
        if (!m_states[i])
          return false;
      }

      //Otherwise all satisfied, return true
      return true;
    }
  }
}
