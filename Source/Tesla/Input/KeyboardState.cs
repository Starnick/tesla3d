﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.InteropServices;

#nullable enable

namespace Tesla.Input
{
  /// <summary>
  /// Represents the state of the keyboard, e.g. which keys are pressed.
  /// </summary>
  [StructLayout(LayoutKind.Sequential)]
  public struct KeyboardState : IEquatable<KeyboardState>
  {
    private static uint s_maskValue = uint.MaxValue;

    private uint m_keyState0;
    private uint m_keyState1;
    private uint m_keyState2;
    private uint m_keyState3;
    private uint m_keyState4;
    private uint m_keyState5;
    private uint m_keyState6;
    private uint m_keyState7;
    private InputFocusState m_focusState;

    /// <summary>
    /// Gets which system has keyboard focus.
    /// </summary>
    public readonly InputFocusState FocusState
    {
      get
      {
        return m_focusState;
      }
    }

    /// <summary>
    /// Query the keystate for the specified key.
    /// </summary>
    /// <param name="key">Specified key</param>
    /// <returns>The keystate</returns>
    public readonly KeyState this[Keys key]
    {
      get
      {
        uint groupState = 0;
        int grouping = ((int) key) >> 5;
        switch (grouping)
        {
          case 0:
            groupState = m_keyState0;
            break;
          case 1:
            groupState = m_keyState1;
            break;
          case 2:
            groupState = m_keyState2;
            break;
          case 3:
            groupState = m_keyState3;
            break;
          case 4:
            groupState = m_keyState4;
            break;
          case 5:
            groupState = m_keyState5;
            break;
          case 6:
            groupState = m_keyState6;
            break;
          case 7:
            groupState = m_keyState7;
            break;
        }
        uint keyState = ((uint) 1) << (int) key;
        if ((groupState & keyState) == 0)
        {
          return KeyState.Up;
        }
        else
        {
          return KeyState.Down;
        }
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="KeyboardState"/> struct with the specified keys
    /// pressed.
    /// </summary>
    /// <param name="focusState">Which system has keyboard focus.</param>
    /// <param name="keys">Keys that are pressed</param>
    public KeyboardState(InputFocusState focusState, params Keys[] keys)
    {
      m_keyState0 = m_keyState1 = m_keyState2 = m_keyState3 = m_keyState4 = m_keyState5 = m_keyState6 = m_keyState7 = 0;
      m_focusState = focusState;
      if (keys is not null)
      {
        for (int i = 0; i < keys.Length; i++)
        {
          AddPressedKey(keys[i]);
        }
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="KeyboardState"/> struct with the specified keys
    /// pressed.
    /// </summary>
    /// <param name="keys">Keys that are pressed</param>
    /// <param name="focusState">Which system has keyboard focus. Default is <see cref="InputFocusState.App"/>.</param>
    public KeyboardState(ReadOnlySpan<Keys> keys, InputFocusState focusState = InputFocusState.App)
    {
      m_keyState0 = m_keyState1 = m_keyState2 = m_keyState3 = m_keyState4 = m_keyState5 = m_keyState6 = m_keyState7 = 0;
      m_focusState = focusState;
      for (int i = 0; i < keys.Length; i++)
      {
        AddPressedKey(keys[i]);
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="KeyboardState"/> struct with the specified key group masks. Each key is a bit in each mask (32 bits x 8 = 256 keys)
    /// </summary>
    /// <param name="group0">First group</param>
    /// <param name="group1">Second group</param>
    /// <param name="group2">Third group</param>
    /// <param name="group3">Fourth group</param>
    /// <param name="group4">Fifth group</param>
    /// <param name="group5">Sixth group</param>
    /// <param name="group6">Seventh group</param>
    /// <param name="group7">Eighth group</param>
    /// <param name="focusState">Which system has keyboard focus. Default is <see cref="InputFocusState.App"/>.</param>
    internal KeyboardState(uint group0, uint group1, uint group2, uint group3, uint group4, uint group5, uint group6, uint group7, InputFocusState focusState = InputFocusState.App)
    {
      m_keyState0 = group0;
      m_keyState1 = group1;
      m_keyState2 = group2;
      m_keyState3 = group3;
      m_keyState4 = group4;
      m_keyState5 = group5;
      m_keyState6 = group6;
      m_keyState7 = group7;
      m_focusState = focusState;
    }

    /// <summary>
    /// Gets the internal group key state masks.
    /// </summary>
    /// <param name="group0">First group</param>
    /// <param name="group1">Second group</param>
    /// <param name="group2">Third group</param>
    /// <param name="group3">Fourth group</param>
    /// <param name="group4">Fifth group</param>
    /// <param name="group5">Sixth group</param>
    /// <param name="group6">Seventh group</param>
    /// <param name="group7">Eighth group</param>
    internal readonly void GetKeyStates(out uint group0, out uint group1, out uint group2, out uint group3, out uint group4, out uint group5, out uint group6, out uint group7)
    {
      group0 = m_keyState0;
      group1 = m_keyState1;
      group2 = m_keyState2;
      group3 = m_keyState3;
      group4 = m_keyState4;
      group5 = m_keyState5;
      group6 = m_keyState6;
      group7 = m_keyState7;
    }

    /// <summary>
    /// Queries if multiple keys are currently pressed down.
    /// </summary>
    /// <returns>True if two or more keys are currently pressed, false otherwise.</returns>
    public readonly bool AreMultipleKeysDown()
    {
      int count = 0;

      count += CheckAtMostTwoKeys(m_keyState0);
      if (count >= 2)
        return true;

      count += CheckAtMostTwoKeys(m_keyState1);
      if (count >= 2)
        return true;

      count += CheckAtMostTwoKeys(m_keyState2);
      if (count >= 2)
        return true;

      count += CheckAtMostTwoKeys(m_keyState3);
      if (count >= 2)
        return true;

      count += CheckAtMostTwoKeys(m_keyState4);
      if (count >= 2)
        return true;

      count += CheckAtMostTwoKeys(m_keyState5);
      if (count >= 2)
        return true;

      count += CheckAtMostTwoKeys(m_keyState6);
      if (count >= 2)
        return true;

      count += CheckAtMostTwoKeys(m_keyState7);

      return count >= 2;
    }

    /// <summary>
    /// Queries if any key is currently down.
    /// </summary>
    /// <returns>True if any key is pressed, false otherwise.</returns>
    public readonly bool IsAnyKeyDown()
    {
      return m_keyState0 != 0 || m_keyState1 != 0 || m_keyState2 != 0 || m_keyState3 != 0 || m_keyState4 != 0 || m_keyState5 != 0 || m_keyState6 != 0 || m_keyState7 != 0;
    }

    /// <summary>
    /// Query if the specified key is down.
    /// </summary>
    /// <param name="key">Key to query</param>
    /// <returns>True if the key is pressed, false otherwise</returns>
    public readonly bool IsKeyDown(Keys key)
    {
      return this[key] == KeyState.Down;
    }

    /// <summary>
    /// Query if the specified key is up.
    /// </summary>
    /// <param name="key">Key to query</param>
    /// <returns>True if the key is not pressed, false otherwise</returns>
    public readonly bool IsKeyUp(Keys key)
    {
      return this[key] == KeyState.Up;
    }

    private void AddPressedKey(Keys key)
    {
      //Get the bit place in the state for the key
      uint place = ((uint) 1) << (int) key;
      //Get the state grouping for the key
      int grouping = (int) key >> 5;
      //Add the key to the proper state
      switch (grouping)
      {
        case 0:
          m_keyState0 |= place & s_maskValue;
          return;
        case 1:
          m_keyState1 |= place & s_maskValue;
          return;
        case 2:
          m_keyState2 |= place & s_maskValue;
          return;
        case 3:
          m_keyState3 |= place & s_maskValue;
          return;
        case 4:
          m_keyState4 |= place & s_maskValue;
          return;
        case 5:
          m_keyState5 |= place & s_maskValue;
          return;
        case 6:
          m_keyState6 |= place & s_maskValue;
          return;
        case 7:
          m_keyState7 |= place & s_maskValue;
          return;
      }
    }

    private void RemovePressedKey(Keys key)
    {
      //Get the bit place for the key
      uint place = ((uint) 1) << (int) key;
      //Get the state grouping for the key
      int grouping = (int) key >> 5;
      //Remove the key from the proper state
      switch (grouping)
      {
        case 0:
          m_keyState0 &= ~(place & s_maskValue);
          return;
        case 1:
          m_keyState1 &= ~(place & s_maskValue);
          return;
        case 2:
          m_keyState2 &= ~(place & s_maskValue);
          return;
        case 3:
          m_keyState3 &= ~(place & s_maskValue);
          return;
        case 4:
          m_keyState4 &= ~(place & s_maskValue);
          return;
        case 5:
          m_keyState5 &= ~(place & s_maskValue);
          return;
        case 6:
          m_keyState6 &= ~(place & s_maskValue);
          return;
        case 7:
          m_keyState7 &= ~(place & s_maskValue);
          return;
      }
    }

    /// <summary>
    /// Gets all the pressed keys in the current state.
    /// </summary>
    /// <param name="pressedKeys">List to add pressed keys to.</param>
    public readonly void GetPressedKeys(IList<Keys> pressedKeys)
    {
      if (pressedKeys is null)
        return;

      CheckPressedKeys(m_keyState0, 0, pressedKeys);
      CheckPressedKeys(m_keyState1, 1, pressedKeys);
      CheckPressedKeys(m_keyState2, 2, pressedKeys);
      CheckPressedKeys(m_keyState3, 3, pressedKeys);
      CheckPressedKeys(m_keyState4, 4, pressedKeys);
      CheckPressedKeys(m_keyState5, 5, pressedKeys);
      CheckPressedKeys(m_keyState6, 6, pressedKeys);
      CheckPressedKeys(m_keyState7, 7, pressedKeys);
    }

    private readonly void CheckPressedKeys(uint keyState, int grouping, IList<Keys> pressedKeys)
    {
      //If the state is zero, we have nothing pressed
      if (keyState != 0)
      {
        //For each key, check to see if its pressed
        for (int i = 0; i < 32; i++)
        {
          //If its pressed, get the keycode
          if ((keyState & (((uint) 1) << i)) != 0)
          {
            pressedKeys.Add((Keys) ((grouping * 32) + i));
          }
        }
      }
    }

    private readonly int CheckAtMostTwoKeys(uint keyState)
    {
      int count = 0;
      if (keyState != 0)
      {
        for (int i = 0; i < 32; i++)
        {
          if ((keyState & (((uint) 1) << i)) != 0)
            count++;

          if (count >= 2)
            return 2;
        }
      }

      return count;
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
    public readonly override int GetHashCode()
    {
      unchecked
      {
        int hash = 17;

        hash = (hash * 31) + m_keyState0.GetHashCode();
        hash = (hash * 31) + m_keyState1.GetHashCode();
        hash = (hash * 31) + m_keyState2.GetHashCode();
        hash = (hash * 31) + m_keyState3.GetHashCode();
        hash = (hash * 31) + m_keyState4.GetHashCode();
        hash = (hash * 31) + m_keyState5.GetHashCode();
        hash = (hash * 31) + m_keyState6.GetHashCode();
        hash = (hash * 31) + m_keyState7.GetHashCode();
        return hash;
      }
    }

    /// <summary>
    /// Determines whether the specified <see cref="System.Object" /> is equal to this instance.
    /// </summary>
    /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
    /// <returns>True if the specified <see cref="System.Object" /> is equal to this instance; otherwise, false.</returns>
    public readonly override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is KeyboardState)
        return (this == (KeyboardState) obj);

      return false;
    }

    /// <summary>
    /// Indicates whether the current object is equal to another object of the same type.
    /// </summary>
    /// <param name="other">An object to compare with this object.</param>
    /// <returns>True if the current object is equal to the <paramref name="other" /> parameter; otherwise, false.</returns>
    public readonly bool Equals(KeyboardState other)
    {
      return (this == other);
    }

    /// <summary>
    /// Tests if two KeyboardStates are equal
    /// </summary>
    /// <param name="a">First KeyboardState</param>
    /// <param name="b">Second KeyboardState</param>
    /// <returns>True if equal, false otherwise</returns>
    public static bool operator ==(KeyboardState a, KeyboardState b)
    {
      return (a.m_keyState0 == b.m_keyState0) && (a.m_keyState1 == b.m_keyState1)
          && (a.m_keyState2 == b.m_keyState2) && (a.m_keyState3 == b.m_keyState3)
          && (a.m_keyState4 == b.m_keyState4) && (a.m_keyState5 == b.m_keyState5)
          && (a.m_keyState6 == b.m_keyState6) && (a.m_keyState7 == b.m_keyState7);
    }

    /// <summary>
    /// Tests if two KeyboardStates are not equal
    /// </summary>
    /// <param name="a">First KeyboardState</param>
    /// <param name="b">Second KeyboardState</param>
    /// <returns>True if not equal, false otherwise</returns>
    public static bool operator !=(KeyboardState a, KeyboardState b)
    {
      return (a.m_keyState0 != b.m_keyState0) || (a.m_keyState1 != b.m_keyState1)
          || (a.m_keyState2 != b.m_keyState2) || (a.m_keyState3 != b.m_keyState3)
          || (a.m_keyState4 != b.m_keyState4) || (a.m_keyState5 != b.m_keyState5)
          || (a.m_keyState6 != b.m_keyState6) || (a.m_keyState7 != b.m_keyState7);
    }
  }
}
