﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections;
using System.Collections.Generic;

#nullable enable

namespace Tesla.Input
{
  /// <summary>
  /// Manages a group of <see cref="InputTrigger"/> objects. Generally triggers are grouped
  /// logically so they can all be enabled or checked together.
  /// </summary>
  public sealed class InputGroup : IReadOnlyList<InputTrigger>, INamable
  {
    private string m_name;
    private List<InputTrigger> m_triggers;
    private bool m_isEnabled;

    /// <summary>
    /// Gets or sets the (optional) name of the input group.
    /// </summary>
    public string Name
    {
      get
      {
        return m_name;
      }
      set
      {
        m_name = String.IsNullOrEmpty(value) ? String.Empty : value;
      }
    }

    /// <summary>
    /// Gets or sets if the input group is enabled. If disabled then no triggers will be checked during update.
    /// </summary>
    public bool IsEnabled
    {
      get
      {
        return m_isEnabled;
      }
      set
      {
        m_isEnabled = value;
      }
    }

    /// <summary>
    /// Gets the input trigger at the specified index.
    /// </summary>
    /// <param name="index">Zero based index.</param>
    /// <returns>Trigger at the specified index.</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the index is out of range.</exception>
    public InputTrigger this[int index]
    {
      get
      {
        if (index < 0 || index >= m_triggers.Count)
          throw new ArgumentOutOfRangeException(nameof(index));

        return m_triggers[index];
      }
    }

    /// <summary>
    /// Gets the input trigger corresponding to the specified name.
    /// </summary>
    /// <param name="name">Name of the input trigger.</param>
    /// <returns>Trigger corresponding to the name, or null if it does not exist.</returns>
    public InputTrigger? this[string name]
    {
      get
      {
        if (String.IsNullOrEmpty(name))
          return null;

        for (int i = 0; i < m_triggers.Count; i++)
        {
          InputTrigger trigger = m_triggers[i];
          if (trigger.Name.Equals(name))
            return trigger;
        }

        return null;
      }
    }

    /// <summary>
    /// Gets the number of input triggers in the group.
    /// </summary>
    public int Count
    {
      get
      {
        return m_triggers.Count;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="InputGroup"/> class.
    /// </summary>
    public InputGroup() : this(String.Empty) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="InputGroup"/> class.
    /// </summary>
    /// <param name="name">The name.</param>
    public InputGroup(string name)
    {
      m_name = String.IsNullOrEmpty(name) ? String.Empty : name;
      m_triggers = new List<InputTrigger>();
      m_isEnabled = true;
    }

    /// <summary>
    /// Checks each trigger in the group if its condition is true, then performs
    /// the trigger's action.
    /// </summary>
    /// <param name="time">Time elapsed since the last update.</param>
    public void CheckAndPerformTriggers(IGameTime time)
    {
      if (!m_isEnabled)
        return;

      for (int i = 0; i < m_triggers.Count; i++)
        m_triggers[i].CheckAndPerform(time);
    }

    /// <summary>
    /// Adds the specified input trigger to the group. If the value is null, it will not be added.
    /// </summary>
    /// <param name="item">Input trigger to add.</param>
    public void Add(InputTrigger item)
    {
      if (item is null)
        return;

      m_triggers.Add(item);
    }

    /// <summary>
    /// Inserts the specified input trigger into the group. If the value is null or the index is out of
    /// range, it will not be added.
    /// </summary>
    /// <param name="index">Zero based index at which the item will be inserted.</param>
    /// <param name="item">Input trigger to insert.</param>
    public void Insert(int index, InputTrigger item)
    {
      if (index < 0 || index > m_triggers.Count || item == null)
        return;

      m_triggers.Insert(index, item);
    }

    /// <summary>
    /// Queries if the specific input trigger is in the group.
    /// </summary>
    /// <param name="item">Input trigger to check.</param>
    /// <returns>True if the trigger is contained in the group, false otherwise.</returns>
    public bool Contains(InputTrigger? item)
    {
      if (item is null)
        return false;

      return m_triggers.Contains(item);
    }

    /// <summary>
    /// Queries if there exists an input trigger corresponding to the specified name.
    /// </summary>
    /// <param name="itemName">Name of input trigger.</param>
    /// <returns>True if an input trigger with that name exists, false otherwise.</returns>
    public bool Contains(string? itemName)
    {
      if (itemName is null)
        return false;

      for (int i = 0; i < m_triggers.Count; i++)
      {
        if (m_triggers[i].Name.Equals(itemName))
          return true;
      }

      return false;
    }

    /// <summary>
    /// Gets the index of the specified input trigger in the group.
    /// </summary>
    /// <param name="item">Input trigger to get the index of in the group.</param>
    /// <returns>Zero based index. If the input trigger was not present, -1 is returned.</returns>
    public int IndexOf(InputTrigger? item)
    {
      if (item is null)
        return -1;

      return m_triggers.IndexOf(item);
    }

    /// <summary>
    /// Gets the index of the input trigger corresponding to the specified name.
    /// </summary>
    /// <param name="itemName">Name of the input trigger.</param>
    /// <returns>Zero based index. If the input trigger was not present, -1 is returned.</returns>
    public int IndexOf(string? itemName)
    {
      if (itemName is null)
        return -1;

      for (int i = 0; i < m_triggers.Count; i++)
      {
        if (m_triggers[i].Name.Equals(itemName))
          return i;
      }

      return -1;
    }

    /// <summary>
    /// Enables or disables an input trigger at the specified index.
    /// </summary>
    /// <param name="index">Zero based index of the input trigger in the group.</param>
    /// <param name="isEnabled">True if the trigger should be enabled, false if disabled.</param>
    /// <returns>True if the trigger was succesfully modified, false if it was not found or the index was out of range.</returns>
    public bool EnableAt(int index, bool isEnabled)
    {
      if (index < 0 || index >= m_triggers.Count)
        return false;

      InputTrigger trigger = m_triggers[index];
      if (trigger is not null)
      {
        trigger.IsEnabled = isEnabled;
        return true;
      }

      return false;
    }

    /// <summary>
    /// Removes all input triggers from the group.
    /// </summary>
    public void Clear()
    {
      m_triggers.Clear();
    }

    /// <summary>
    /// Removes the specified input trigger from the group.
    /// </summary>
    /// <param name="item">Input trigger to remove.</param>
    /// <returns>True if the item was removed, false otherwise.</returns>
    public bool Remove(InputTrigger item)
    {
      return m_triggers.Remove(item);
    }

    /// <summary>
    /// Removes the input trigger corresponding to the specified name, if it exists.
    /// </summary>
    /// <param name="itemName">Name of input trigger to remove.</param>
    /// <returns>True if the item was removed, false otherwise.</returns>
    public bool Remove(string? itemName)
    {
      if (itemName is null)
        return false;

      for (int i = 0; i < m_triggers.Count; i++)
      {
        if (m_triggers[i].Name.Equals(itemName))
        {
          m_triggers.RemoveAt(i);
          return true;
        }
      }

      return false;
    }

    /// <summary>
    /// Removes the input trigger at the specified index.
    /// </summary>
    /// <param name="index">Zero based index.</param>
    public void RemoveAt(int index)
    {
      if (index < 0 || index >= m_triggers.Count)
        return;

      m_triggers.RemoveAt(index);
    }

    /// <summary>
    /// Returns an enumerator that iterates through the collection.
    /// </summary>
    /// <returns>A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.</returns>
    public IEnumerator<InputTrigger> GetEnumerator()
    {
      return m_triggers.GetEnumerator();
    }

    /// <summary>
    /// Returns an enumerator that iterates through a collection.
    /// </summary>
    /// <returns>An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.</returns>
    IEnumerator IEnumerable.GetEnumerator()
    {
      return m_triggers.GetEnumerator();
    }
  }
}
