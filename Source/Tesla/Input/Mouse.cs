﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Runtime.CompilerServices;

#nullable enable

namespace Tesla.Input
{
  /// <summary>
  /// Provides convienent access to a registered <see cref="IMouseInputSystem"/> service.
  /// </summary>
  public static class Mouse
  {
    /// <summary>
    /// Gets if the mouse is available.
    /// </summary>
    public static bool IsAvailable
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return CoreServices.MouseInputSystem.Service is not null;
      }
    }

    /// <summary>
    /// Gets or sets the window handle the mouse will receive its coordinates from. Mouse screen coordinates
    /// are relative to the top-left corner of this window.
    /// </summary>
    public static IntPtr WindowHandle
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return CoreServices.MouseInputSystem.Service?.WindowHandle ?? IntPtr.Zero;
      }
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      set
      {
        IMouseInputSystem? mouseSystem = CoreServices.MouseInputSystem.Service;
        if (mouseSystem is not null)
          mouseSystem.WindowHandle = value;
      }
    }

    /// <summary>
    /// Gets or sets what system currently has mouse focus and should respond to input events.
    /// </summary>
    public static InputFocusState FocusState
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return CoreServices.MouseInputSystem.Service?.FocusState ?? InputFocusState.None;
      }
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      set
      {
        IMouseInputSystem? mouseSystem = CoreServices.MouseInputSystem.Service;
        if (mouseSystem is not null)
          mouseSystem.FocusState = value;
      }
    }

    /// <summary>
    /// Queries the current state of the mouse button and screen position.
    /// </summary>
    /// <returns>The current mouse state.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static MouseState GetMouseState()
    {
      IMouseInputSystem? mouseSystem = CoreServices.MouseInputSystem.Service;
      if (mouseSystem is null)
        return new MouseState();

      mouseSystem.GetMouseState(out MouseState state);
      return state;
    }

    /// <summary>
    /// Queries the current state of the mouse buttons and screen position, using the specified window handle.
    /// </summary>
    /// <param name="windowHandle">Window handle to get the screen coordinates in</param>
    /// <returns>The current mouse state.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static MouseState GetMouseState(IntPtr windowHandle)
    {
      IMouseInputSystem? mouseSystem = CoreServices.MouseInputSystem.Service;
      if (mouseSystem is null)
        return new MouseState();

      mouseSystem.GetMouseState(windowHandle, out MouseState state);
      return state;
    }

    /// <summary>
    /// Sets the position of the mouse relative to the top-left corner of the window that the mouse is currently bound to.
    /// </summary>
    /// <param name="x">X position</param>
    /// <param name="y">Y position</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void SetPosition(int x, int y)
    {
      CoreServices.MouseInputSystem.Service?.SetPosition(x, y);
    }
  }
}
