﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;

#nullable enable

namespace Tesla.Input.Utilities
{
  /// <summary>
  /// Builder helper for constructing mouse states.
  /// </summary>
  public sealed class MouseStateBuilder
  {
    private static int s_numberOfButtons = Enum.GetValues(typeof(MouseButton)).Length;

    private ButtonState[] m_pressedButtons;
    private int m_wheelValue;
    private int m_deltaWheelValue;
    private Int2 m_cursorPos;
    private InputFocusState m_focusState;

    /// <summary>
    /// Gets or sets the current cursor position.
    /// </summary>
    public Int2 CursorPosition
    {
      get
      {
        return m_cursorPos;
      }
      set
      {
        m_cursorPos = value;
      }
    }

    /// <summary>
    /// Gets or sets the current mouse wheel value.
    /// </summary>
    public int WheelValue
    {
      get
      {
        return m_wheelValue;
      }
      set
      {
        m_deltaWheelValue = value - m_wheelValue;
        m_wheelValue = value;
      }
    }

    /// <summary>
    /// Gets or sets which system has mouse focus.
    /// </summary>
    public InputFocusState FocusState
    {
      get
      {
        return m_focusState;
      }
      set
      {
        m_focusState = value;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="MouseStateBuilder"/> class.
    /// </summary>
    public MouseStateBuilder()
    {
      m_pressedButtons = new ButtonState[s_numberOfButtons];
      m_wheelValue = 0;
      m_deltaWheelValue = 0;
      m_cursorPos = Int2.Zero;
      m_focusState = InputFocusState.None;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="MouseStateBuilder"/> class.
    /// </summary>
    /// <param name="state">Initial mouse state to populate from.</param>
    public MouseStateBuilder(MouseState state)
    {
      m_pressedButtons = new ButtonState[s_numberOfButtons];
      SetButtonState(MouseButton.Left, state.LeftButton);
      SetButtonState(MouseButton.Middle, state.MiddleButton);
      SetButtonState(MouseButton.Right, state.RightButton);
      SetButtonState(MouseButton.XButton1, state.XButton1);
      SetButtonState(MouseButton.XButton2, state.XButton2);

      m_wheelValue = state.ScrollWheelValue;
      m_deltaWheelValue = state.ScrollWheelDelta;
      m_cursorPos = state.PositionInt;
      m_focusState = state.FocusState;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="MouseStateBuilder"/> class.
    /// </summary>
    /// <param name="from">Other mouse state builder to populate from.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the builder to copy from is null.</exception>
    public MouseStateBuilder(MouseStateBuilder from)
    {
      if (from is null)
        throw new ArgumentNullException(nameof(from));

      m_pressedButtons = (from.m_pressedButtons.Clone() as ButtonState[])!;
      m_wheelValue = from.m_wheelValue;
      m_deltaWheelValue = from.m_deltaWheelValue;
      m_cursorPos = from.m_cursorPos;
      m_focusState = from.m_focusState;
    }

    /// <summary>
    /// Constructs a new <see cref="MouseState"/> from the state the builder is maintaining.
    /// </summary>
    /// <param name="state">Mouse state</param>
    public void ConstructState(out MouseState state)
    {
      state = new MouseState(m_cursorPos.X, m_cursorPos.Y, m_wheelValue, m_deltaWheelValue, m_pressedButtons[(int) MouseButton.Left],
          m_pressedButtons[(int) MouseButton.Right], m_pressedButtons[(int) MouseButton.Middle], m_pressedButtons[(int) MouseButton.XButton1],
          m_pressedButtons[(int) MouseButton.XButton2], m_focusState);
    }

    /// <summary>
    /// Clears the mouse state.
    /// </summary>
    public void Clear()
    {
      m_cursorPos = Int2.Zero;
      m_wheelValue = 0;
      m_deltaWheelValue = 0;
      m_focusState = InputFocusState.None;
      Array.Clear(m_pressedButtons, 0, m_pressedButtons.Length);
    }

    /// <summary>
    /// Sets the current button state for the specified mouse button.
    /// </summary>
    /// <param name="mouseButton">Mouse button to set.</param>
    /// <param name="state">Button state.</param>
    public void SetButtonState(MouseButton mouseButton, ButtonState state)
    {
      m_pressedButtons[(int) mouseButton] = state;
    }

    /// <summary>
    /// Adds the number of pressed buttons to the builder's pressed state.
    /// </summary>
    /// <param name="mouseButtons">Mouse buttons to add as currently pressed.</param>
    public void AddPressedButtons(params MouseButton[] mouseButtons)
    {
      if (mouseButtons is null)
        return;

      foreach (MouseButton button in mouseButtons)
        AddPressedButton(button);
    }

    /// <summary>
    /// Adds the number of pressed buttons to the builder's pressed state.
    /// </summary>
    /// <param name="mouseButtons">Mouse buttons to add as currently pressed.</param>
    public void AddPressedButtons(ReadOnlySpan<MouseButton> mouseButtons)
    {
      foreach (MouseButton button in mouseButtons)
        AddPressedButton(button);
    }

    /// <summary>
    /// Adds the button to the builder's pressed state.
    /// </summary>
    /// <param name="mouseButton">Mouse button to add as currently pressed.</param>
    public void AddPressedButton(MouseButton mouseButton)
    {
      m_pressedButtons[(int) mouseButton] = ButtonState.Pressed;
    }

    /// <summary>
    /// Removes the number of pressed buttons from the builder's pressed state.
    /// </summary>
    /// <param name="mouseButtons">Mouse buttons to remove as currently pressed.</param>
    public void RemovePressedButtons(params MouseButton[] mouseButtons)
    {
      if (mouseButtons is null)
        return;

      foreach (MouseButton button in mouseButtons)
        RemovePressedButton(button);
    }

    /// <summary>
    /// Removes the number of pressed buttons from the builder's pressed state.
    /// </summary>
    /// <param name="mouseButtons">Mouse buttons to remove as currently pressed.</param>
    public void RemovePressedButtons(ReadOnlySpan<MouseButton> mouseButtons)
    {
      foreach (MouseButton button in mouseButtons)
        RemovePressedButton(button);
    }


    /// <summary>
    /// Removes the button from the builder's pressed state.
    /// </summary>
    /// <param name="mouseButton">Mouse button to remove as currently pressed.</param>
    public void RemovePressedButton(MouseButton mouseButton)
    {
      m_pressedButtons[(int) mouseButton] = ButtonState.Released;
    }
  }
}
