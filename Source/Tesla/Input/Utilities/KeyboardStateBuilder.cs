﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;

#nullable enable

namespace Tesla.Input.Utilities
{
  /// <summary>
  /// Builder helper for constructing keyboard states.
  /// </summary>
  public sealed class KeyboardStateBuilder
  {
    private static uint s_maskValue = uint.MaxValue;

    private uint m_keyState0;
    private uint m_keyState1;
    private uint m_keyState2;
    private uint m_keyState3;
    private uint m_keyState4;
    private uint m_keyState5;
    private uint m_keyState6;
    private uint m_keyState7;
    private InputFocusState m_focusState;

    /// <summary>
    /// Gets or sets which system has keyboard focus.
    /// </summary>
    public InputFocusState FocusState
    {
      get
      {
        return m_focusState;
      }
      set
      {
        m_focusState = value;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="KeyboardStateBuilder"/> class.
    /// </summary>
    public KeyboardStateBuilder()
    {
      m_keyState0 = m_keyState1 = m_keyState2 = m_keyState3 = m_keyState4 = m_keyState5 = m_keyState6 = m_keyState7 = 0;
      m_focusState = InputFocusState.None;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="KeyboardStateBuilder"/> class.
    /// </summary>
    /// <param name="state">Initial keyboard state to populate from.</param>
    public KeyboardStateBuilder(KeyboardState state)
    {
      state.GetKeyStates(out m_keyState0, out m_keyState1, out m_keyState2, out m_keyState3, out m_keyState4, out m_keyState5, out m_keyState6, out m_keyState7);
      m_focusState = state.FocusState;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="KeyboardStateBuilder"/> class.
    /// </summary>
    /// <param name="from">Other keyboard state builder to populate from.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the builder to copy from is null.</exception>
    public KeyboardStateBuilder(KeyboardStateBuilder from)
    {
      if (from is null)
        throw new ArgumentNullException(nameof(from));

      m_keyState0 = from.m_keyState0;
      m_keyState1 = from.m_keyState1;
      m_keyState2 = from.m_keyState2;
      m_keyState3 = from.m_keyState3;
      m_keyState4 = from.m_keyState4;
      m_keyState5 = from.m_keyState5;
      m_keyState6 = from.m_keyState6;
      m_keyState7 = from.m_keyState7;
      m_focusState = from.m_focusState;
    }

    /// <summary>
    /// Constructs a new <see cref="KeyboardState"/> from the state the builder is maintaining.
    /// </summary>
    /// <param name="state">Keyboard state</param>
    public void ConstructState(out KeyboardState state)
    {
      state = new KeyboardState(m_keyState0, m_keyState1, m_keyState2, m_keyState3, m_keyState4, m_keyState5, m_keyState6, m_keyState7, m_focusState);
    }

    /// <summary>
    /// Clears the keyboard state.
    /// </summary>
    public void Clear()
    {
      m_keyState0 = m_keyState1 = m_keyState2 = m_keyState3 = m_keyState4 = m_keyState5 = m_keyState6 = m_keyState7 = 0;
      m_focusState = InputFocusState.None;
    }

    /// <summary>
    /// Sets the current keystate for the specified key.
    /// </summary>
    /// <param name="key">Key to set.</param>
    /// <param name="state">Key state.</param>
    public void SetKeyState(Keys key, KeyState state)
    {
      if (state == KeyState.Up)
        RemovePressedKey(key);
      else
        AddPressedKey(key);
    }

    /// <summary>
    /// Adds the number of pressed keys to the builder's pressed state.
    /// </summary>
    /// <param name="keys">Keys to add as currently pressed.</param>
    public void AddPressedKeys(params Keys[] keys)
    {
      if (keys is null)
        return;

      foreach (Keys key in keys)
        AddPressedKey(key);
    }

    /// <summary>
    /// Adds the number of pressed keys to the builder's pressed state.
    /// </summary>
    /// <param name="keys">Keys to add as currently pressed.</param>
    public void AddPressedKeys(ReadOnlySpan<Keys> keys)
    {
      foreach (Keys key in keys)
        AddPressedKey(key);
    }

    /// <summary>
    /// Adds the key to the builder's pressed state.
    /// </summary>
    /// <param name="key">Key to add as currently pressed.</param>
    public void AddPressedKey(Keys key)
    {
      //Get the bit place in the state for the key
      uint place = ((uint) 1) << (int) key;
      //Get the state grouping for the key
      int grouping = (int) key >> 5;
      //Add the key to the proper state
      switch (grouping)
      {
        case 0:
          m_keyState0 |= place & s_maskValue;
          return;
        case 1:
          m_keyState1 |= place & s_maskValue;
          return;
        case 2:
          m_keyState2 |= place & s_maskValue;
          return;
        case 3:
          m_keyState3 |= place & s_maskValue;
          return;
        case 4:
          m_keyState4 |= place & s_maskValue;
          return;
        case 5:
          m_keyState5 |= place & s_maskValue;
          return;
        case 6:
          m_keyState6 |= place & s_maskValue;
          return;
        case 7:
          m_keyState7 |= place & s_maskValue;
          return;
      }
    }

    /// <summary>
    /// Removes the number of pressed keys from the builder's pressed state.
    /// </summary>
    /// <param name="keys">Keys to remove as currently pressed.</param>
    public void RemovePressedKeys(params Keys[] keys)
    {
      if (keys is null)
        return;

      foreach (Keys key in keys)
        RemovePressedKey(key);
    }

    /// <summary>
    /// Removes the number of pressed keys from the builder's pressed state.
    /// </summary>
    /// <param name="keys">Keys to remove as currently pressed.</param>
    public void RemovePressedKeys(ReadOnlySpan<Keys> keys)
    {
      foreach (Keys key in keys)
        RemovePressedKey(key);
    }

    /// <summary>
    /// Removes the key from the builder's pressed state.
    /// </summary>
    /// <param name="key">Key to remove as currently pressed.</param>
    public void RemovePressedKey(Keys key)
    {
      //Get the bit place for the key
      uint place = ((uint) 1) << (int) key;
      //Get the state grouping for the key
      int grouping = (int) key >> 5;
      //Remove the key from the proper state
      switch (grouping)
      {
        case 0:
          m_keyState0 &= ~(place & s_maskValue);
          return;
        case 1:
          m_keyState1 &= ~(place & s_maskValue);
          return;
        case 2:
          m_keyState2 &= ~(place & s_maskValue);
          return;
        case 3:
          m_keyState3 &= ~(place & s_maskValue);
          return;
        case 4:
          m_keyState4 &= ~(place & s_maskValue);
          return;
        case 5:
          m_keyState5 &= ~(place & s_maskValue);
          return;
        case 6:
          m_keyState6 &= ~(place & s_maskValue);
          return;
        case 7:
          m_keyState7 &= ~(place & s_maskValue);
          return;
      }
    }
  }
}
