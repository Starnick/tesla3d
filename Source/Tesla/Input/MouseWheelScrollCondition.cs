﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#nullable enable

namespace Tesla.Input
{
  /// <summary>
  /// Checks if the mouse wheel has scrolled since the last update.
  /// </summary>
  public sealed class MouseWheelScrollCondition : InputCondition
  {
    /// <summary>
    /// Enumerates the valid move directions.
    /// </summary>
    public enum MoveDirection
    {
      /// <summary>
      /// Condition evaluates true if the wheel is scrolled forward (positive). 
      /// </summary>
      Forward = 0,

      /// <summary>
      /// Condition evaluates true if the wheel is scrolled backward (negative).
      /// </summary>
      Backward = 1,

      /// <summary>
      /// Condition evaluates true if the wheel is scrolled in any direction.
      /// </summary>
      Both = 2
    }

    private MoveDirection m_moveDir;

    /// <summary>
    /// Sets the valid move direction.
    /// </summary>
    public MoveDirection Direction
    {
      get
      {
        return m_moveDir;
      }
      set
      {
        m_moveDir = value;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="MouseWheelScrollCondition"/> class.
    /// </summary>
    public MouseWheelScrollCondition() : this(MoveDirection.Both) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="MouseWheelScrollCondition"/> class.
    /// </summary>
    /// <param name="moveDir">Move direction to check.</param>
    public MouseWheelScrollCondition(MoveDirection moveDir)
    {
      m_moveDir = moveDir;
    }

    /// <summary>
    /// Checks if the condition has been satisfied or not.
    /// </summary>
    /// <param name="time">Time elapsed since the last update.</param>
    /// <returns>True if the condition has been satisfied, false otherwise.</returns>
    public override bool Check(IGameTime time)
    {
      MouseState currState = Mouse.GetMouseState();

      bool hasMovedValidDir = false;

      switch (m_moveDir)
      {
        case MoveDirection.Forward:
          hasMovedValidDir = currState.ScrollWheelDelta > 0;
          break;
        case MoveDirection.Backward:
          hasMovedValidDir = currState.ScrollWheelDelta < 0;
          break;
        case MoveDirection.Both:
          hasMovedValidDir = currState.ScrollWheelDelta != 0;
          break;
      }

      return hasMovedValidDir && currState.FocusState == InputFocusState.App;
    }
  }
}
