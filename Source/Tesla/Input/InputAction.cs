﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;

#nullable enable

namespace Tesla.Input
{
  /// <summary>
  /// Delegate for logic that is executed by an InputAction, used in leui of a subclass.
  /// </summary>
  /// <param name="time">Time elapsed since the last update.</param>
  public delegate void InputActionDelegate(IGameTime time);

  /// <summary>
  /// Represents the logic that should be performed when an input condition evaluates to true. This is paired with an InputCondition
  /// inside an InputTrigger. Actions and conditions can vary independently from one another, allowing for their reuse.
  /// </summary>
  public class InputAction
  {
    private InputActionDelegate? m_action;

    /// <summary>
    /// Constructs a new instance of the <see cref="InputAction"/> class.
    /// </summary>
    protected InputAction() { }

    /// <summary>
    /// Constructs a new instance of the <see cref="InputAction"/> class with the specified action delegate.
    /// </summary>
    /// <param name="action">Input action delegate</param>
    /// <exception cref="ArgumentNullException">Thrown if the action delegate is null.</exception>
    public InputAction(InputActionDelegate action)
    {
      if (action is null)
        throw new ArgumentNullException(nameof(action));

      m_action = action;
    }

    /// <summary>
    /// Called by an input trigger when the condition first succeeds, allowing the action to do first-time setup.
    /// </summary>
    /// <param name="time"></param>
    public virtual void OnBegin(IGameTime time) { }

    /// <summary>
    /// Performs the action.
    /// </summary>
    /// <param name="time">Time elapsed since the last update.</param>
    public virtual void Perform(IGameTime time)
    {
      if (m_action is null)
        return;

      m_action(time);
    }

    /// <summary>
    /// Called by an input trigger when the condition fails, allowing the action to do last-time cleanup.
    /// </summary>
    public virtual void OnEnd(IGameTime time) { }
  }
}
