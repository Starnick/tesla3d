﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;

#nullable enable

namespace Tesla.Input
{
  /// <summary>
  /// Defines an input binding that is either a key or mouse button.
  /// </summary>
  public interface IKeyInputBinding
  {
    /// <summary>
    /// Gets or sets the input binding.
    /// </summary>
    KeyOrMouseButton InputBinding { get; set; }
  }

  /// <summary>
  /// Defines a multi input binding that can be a combination of key or mouse buttons or both.
  /// </summary>
  public interface IMultiKeyInputBinding
  {
    /// <summary>
    /// Gets the input binding combination.
    /// </summary>
    IReadOnlyList<KeyOrMouseButton> InputBindings { get; }

    /// <summary>
    /// Sets the input binding combination.
    /// </summary>
    /// <param name="bindings">Input bindings.</param>
    void SetInputBindings(params KeyOrMouseButton[] bindings);
  }

  /// <summary>
  /// Defines a service that polls a keyboard device and maintains the current device's state.
  /// </summary>
  public interface IKeyboardInputSystem : IUpdatableEngineService
  {
    /// <summary>
    /// Gets or sets what system currently has keyboard focus and should respond to input events.
    /// </summary>
    InputFocusState FocusState { get; set; }

    /// <summary>
    /// Queries the current state of the ke yboard - e.g. which keys are pressed.
    /// </summary>
    /// <param name="state">The current keyboard state.</param>
    void GetKeyboardState(out KeyboardState state);
  }

  /// <summary>
  /// Defines a service that polls a mouse device and maintains the current device's state.
  /// </summary>
  public interface IMouseInputSystem : IUpdatableEngineService
  {
    /// <summary>
    /// Gets or sets the window handle the mouse will receive its coordinates from. Mouse screen coordinates
    /// are relative to the top-left corner of this window.
    /// </summary>
    IntPtr WindowHandle { get; set; }

    /// <summary>
    /// Gets or sets what system currently has mouse focus and should respond to input events.
    /// </summary>
    InputFocusState FocusState { get; set; }

    /// <summary>
    /// Queries the current state of the mouse button and screen position.
    /// </summary>
    /// <param name="state">The current mouse state.</param>
    void GetMouseState(out MouseState state);

    /// <summary>
    /// Queries the current state of the mouse buttons and screen position, using the specified window handle.
    /// </summary>
    /// <param name="windowHandle">Window handle to get the screen coordinates in</param>
    /// <param name="state">The current mouse state.</param>
    void GetMouseState(IntPtr windowHandle, out MouseState state);

    /// <summary>
    /// Sets the position of the mouse relative to the top-left corner of the window that the mouse is currently bound to.
    /// </summary>
    /// <param name="x">X position</param>
    /// <param name="y">Y position</param>
    void SetPosition(int x, int y);
  }
}
