﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;

#nullable enable

namespace Tesla.Input
{
  /// <summary>
  /// Groups a single input condition with a single action that is only triggered when the condition is true.
  /// </summary>
  public class InputTrigger : INamable
  {
    private string m_name;
    private InputCondition m_condition;
    private InputAction m_action;
    private bool m_isEnabled;
    private bool m_prevWasSuccess = false;

    /// <summary>
    /// Gets or sets the (optional) name of the trigger. Useful for key mappings.
    /// </summary>
    public string Name
    {
      get
      {
        return m_name;
      }
      set
      {
        m_name = String.IsNullOrEmpty(value) ? String.Empty : value;
      }
    }

    /// <summary>
    /// Gets or sets the condition of this trigger.
    /// </summary>
    public InputCondition Condition
    {
      get
      {
        return m_condition;
      }
      set
      {
        m_condition = value;
      }
    }

    /// <summary>
    /// Gets or sets the action of this trigger.
    /// </summary>
    public InputAction Action
    {
      get
      {
        return m_action;
      }
      set
      {
        m_action = value;
      }
    }

    /// <summary>
    /// Gets or sets if this trigger is enabled.
    /// </summary>
    public bool IsEnabled
    {
      get
      {
        return m_isEnabled;
      }
      set
      {
        m_isEnabled = value;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="InputTrigger"/> class.
    /// </summary>
    /// <param name="condition">The input condition to check.</param>
    /// <param name="action">The input action to perform when the condition is satisfied.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if either the condition or action are null.</exception>
    public InputTrigger(InputCondition condition, InputAction action) : this(String.Empty, condition, action) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="InputTrigger"/> class.
    /// </summary>
    /// <param name="name">The name of the trigger, optional.</param>
    /// <param name="condition">The input condition to check.</param>
    /// <param name="action">The input action to perform when the condition is satisfied.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if either the condition or action are null.</exception>
    public InputTrigger(String name, InputCondition condition, InputAction action)
    {
      if (condition is null)
        throw new ArgumentNullException(nameof(condition));

      if (action is null)
        throw new ArgumentNullException(nameof(action));

      m_name = String.IsNullOrEmpty(name) ? String.Empty : name;
      m_condition = condition;
      m_action = action;
      m_isEnabled = true;
    }

    /// <summary>
    /// Checks if the condition is true and performs the action if so.
    /// </summary>
    /// <param name="time">Represents current time snapshot.</param>
    /// <returns>True if the action successfully was performed, false otherwise.</returns>
    public bool CheckAndPerform(IGameTime time)
    {
      if (!m_isEnabled)
        return false;

      if (m_condition.Check(time))
      {
        if (!m_prevWasSuccess)
        {
          m_prevWasSuccess = true;
          m_action.OnBegin(time);
        }

        m_action.Perform(time);
        return true;
      }

      if (m_prevWasSuccess)
      {
        m_prevWasSuccess = false;
        m_action.OnEnd(time);
      }

      return false;
    }
  }
}
