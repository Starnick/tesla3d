﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;

#nullable enable

namespace Tesla.Input
{
  /// <summary>
  /// Represents a composite input action where all actions are performed when an input condition evaluates to true. This is paired with an InputCondition
  /// inside an InputTrigger. Actions and conditions can vary independently from one another, allowing for their reuse.
  /// </summary>
  public sealed class CompositeInputAction : InputAction
  {
    private List<InputAction> m_actions;

    /// <summary>
    /// Gets a list of contained actions.
    /// </summary>
    public IReadOnlyList<InputAction> Actions
    {
      get
      {
        return m_actions;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="CompositeInputAction"/> class.
    /// </summary>
    /// <param name="actions">Input actions.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the array of actions is null or any actions are null.</exception>
    public CompositeInputAction(params InputAction[] actions)
    {
      if (actions == null || actions.Length == 0)
        throw new ArgumentNullException("actions");

      m_actions = new List<InputAction>(actions.Length);
      for (int i = 0; i < actions.Length; i++)
      {
        InputAction action = actions[i];
        if (action == null)
          throw new ArgumentNullException("action");

        m_actions.Add(action);
      }
    }

    /// <summary>
    /// Performs the action.
    /// </summary>
    /// <param name="time">Time elapsed since the last update.</param>
    public override void Perform(IGameTime time)
    {
      for (int i = 0; i < m_actions.Count; i++)
        m_actions[i].Perform(time);
    }
  }
}
