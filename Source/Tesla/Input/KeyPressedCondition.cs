﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;

#nullable enable

namespace Tesla.Input
{
  /// <summary>
  /// Checks if a key has been pressed since the last update, that is, moved from an up to down state. It can also account for
  /// key repeats, where the key is continually down between multiple updates.
  /// </summary>
  public sealed class KeyPressedCondition : InputCondition, IKeyInputBinding
  {
    private KeyOrMouseButton m_binding;
    private bool m_allowRepeat;
    private bool m_allowMultipleButtons;
    private KeyboardState m_oldKeyState;
    private MouseState m_oldMouseState;

    /// <summary>
    /// Gets or sets the input binding.
    /// </summary>
    public KeyOrMouseButton InputBinding
    {
      get
      {
        return m_binding;
      }
      set
      {
        m_binding = value;

        Reset();
      }
    }

    /// <summary>
    /// Gets or sets if the condition should allow key repeats or not.
    /// </summary>
    public bool AllowRepeat
    {
      get
      {
        return m_allowRepeat;
      }
      set
      {
        m_allowRepeat = value;
      }
    }

    /// <summary>
    /// Gets or sets if the condition can be true even if there are multiple key presses. If true, then as long as the specified button is pressed
    /// then the condition will evaluate as true, if false then only the specified button pressed and no other will the condition be true.
    /// </summary>
    public bool AllowMultipleButtons
    {
      get
      {
        return m_allowMultipleButtons;
      }
      set
      {
        m_allowMultipleButtons = value;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="KeyPressedCondition"/> class.
    /// </summary>
    /// <param name="binding">Input binding.</param>
    /// <param name="allowRepeat">Allow for repeating</param>
    public KeyPressedCondition(KeyOrMouseButton binding, bool allowRepeat) : this(binding, allowRepeat, true) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="KeyPressedCondition"/> class.
    /// </summary>
    /// <param name="binding">Input binding.</param>
    /// <param name="allowRepeat">Allow for repeating</param>
    /// <param name="allowMultipleButtons">Allow if the condition should evaluate to true even if multiple buttons are currently pressed, false if the condition
    /// should evaluate true as long as the specified button is pressed.</param>
    public KeyPressedCondition(KeyOrMouseButton binding, bool allowRepeat, bool allowMultipleButtons)
    {
      m_binding = binding;
      m_allowRepeat = allowRepeat;
      m_allowMultipleButtons = allowMultipleButtons;

      Reset();
    }

    /// <summary>
    /// Resets the condition's state.
    /// </summary>
    public void Reset()
    {
      if (m_binding.IsMouseButton)
      {
        m_oldKeyState = new KeyboardState();
        m_oldMouseState = Mouse.GetMouseState();
      }
      else
      {
        m_oldKeyState = Keyboard.GetKeyboardState();
        m_oldMouseState = new MouseState();
      }
    }

    /// <summary>
    /// Checks if the condition has been satisfied or not.
    /// </summary>
    /// <param name="time">Time elapsed since the last update.</param>
    /// <returns>True if the condition has been satisfied, false otherwise.</returns>
    public override bool Check(IGameTime time)
    {
      bool hasChanged = false;

      if (m_binding.IsMouseButton)
      {
        MouseState currState = Mouse.GetMouseState();
        MouseButton button = m_binding.MouseButton;

        bool ignoreIfOtherKeysPressed = (m_allowMultipleButtons) ? false : currState.PressedButtonCount > 1;

        if (currState.FocusState == InputFocusState.App)
        {
          if (m_allowRepeat)
          {
            if (!ignoreIfOtherKeysPressed && currState.IsButtonPressed(button))
              hasChanged = true;
          }
          else
          {
            if (!ignoreIfOtherKeysPressed && m_oldMouseState.IsButtonReleased(button) && currState.IsButtonPressed(button))
              hasChanged = true;
          }
        }

        m_oldMouseState = currState;
      }
      else
      {
        KeyboardState currState = Keyboard.GetKeyboardState();
        Keys key = m_binding.Key;

        bool ignoreIfOtherKeysPressed = (m_allowMultipleButtons) ? false : currState.AreMultipleKeysDown();

        if (currState.FocusState == InputFocusState.App)
        {
          if (m_allowRepeat)
          {
            if (!ignoreIfOtherKeysPressed && currState.IsKeyDown(key))
              hasChanged = true;
          }
          else
          {
            if (!ignoreIfOtherKeysPressed && m_oldKeyState.IsKeyUp(key) && currState.IsKeyDown(key))
              hasChanged = true;
          }
        }

        m_oldKeyState = currState;
      }

      return hasChanged;
    }
  }
}
