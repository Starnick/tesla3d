﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

namespace Tesla.Input
{
  /// <summary>
  /// Checks if a key has been released since the last update, that is, moved from a down to up state.
  /// </summary>
  public sealed class KeyReleasedCondition : InputCondition, IKeyInputBinding
  {
    private KeyOrMouseButton m_binding;
    private KeyboardState m_oldKeyState;
    private MouseState m_oldMouseState;
    private bool m_allowMultipleButtons;

    /// <summary>
    /// Gets or sets the input binding.
    /// </summary>
    public KeyOrMouseButton InputBinding
    {
      get
      {
        return m_binding;
      }
      set
      {
        m_binding = value;

        Reset();
      }
    }

    /// <summary>
    /// Gets or sets if the condition can be true even if the previous state had multiple key presses. If true, then as long as the specified button is released
    /// then the condition will evaluate as true, if false then the condition will only be true if the specified button is released and the previous state had no other key press.
    /// </summary>
    public bool AllowMultipleButtons
    {
      get
      {
        return m_allowMultipleButtons;
      }
      set
      {
        m_allowMultipleButtons = value;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="KeyReleasedCondition"/> class.
    /// </summary>
    /// <param name="binding">Input binding.</param>
    public KeyReleasedCondition(KeyOrMouseButton binding) : this(binding, true) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="KeyReleasedCondition"/> class.
    /// </summary>
    /// <param name="binding">Input binding.</param>
    /// <param name="allowMultipleButtons">Allow if the condition should evaluate to true even if multiple buttons are currently pressed, false if the condition
    /// should evaluate true as long as other buttons are not held down when the specified button is released.</param>
    public KeyReleasedCondition(KeyOrMouseButton binding, bool allowMultipleButtons)
    {
      m_binding = binding;
      m_allowMultipleButtons = allowMultipleButtons;

      Reset();
    }


    /// <summary>
    /// Resets the condition's state.
    /// </summary>
    public void Reset()
    {
      if (m_binding.IsMouseButton)
      {
        m_oldKeyState = new KeyboardState();
        m_oldMouseState = Mouse.GetMouseState();
      }
      else
      {
        m_oldKeyState = Keyboard.GetKeyboardState();
        m_oldMouseState = new MouseState();
      }
    }

    /// <summary>
    /// Checks if the condition has been satisfied or not.
    /// </summary>
    /// <param name="time">Time elapsed since the last update.</param>
    /// <returns>True if the condition has been satisfied, false otherwise.</returns>
    public override bool Check(IGameTime time)
    {
      bool hasChanged = false;

      if (m_binding.IsMouseButton)
      {
        MouseState currState = Mouse.GetMouseState();
        MouseButton button = m_binding.MouseButton;

        bool ignoreIfOtherKeysPressed = (m_allowMultipleButtons) ? false : m_oldMouseState.PressedButtonCount > 1;

        if (currState.FocusState == InputFocusState.App && !ignoreIfOtherKeysPressed && m_oldMouseState.IsButtonPressed(button) && currState.IsButtonReleased(button))
          hasChanged = true;

        m_oldMouseState = currState;
      }
      else
      {
        KeyboardState currState = Keyboard.GetKeyboardState();
        Keys key = m_binding.Key;

        bool ignoreIfOtherKeysPressed = (m_allowMultipleButtons) ? false : m_oldKeyState.AreMultipleKeysDown();

        if (currState.FocusState == InputFocusState.App && !ignoreIfOtherKeysPressed && m_oldKeyState.IsKeyDown(key) && currState.IsKeyUp(key))
          hasChanged = true;

        m_oldKeyState = currState;
      }

      return hasChanged;
    }
  }
}
