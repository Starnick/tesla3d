﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;

#nullable enable

namespace Tesla.Input
{
  /// <summary>
  /// Delegate for logic that checks some condition and evaluates true or false.
  /// </summary>
  /// <param name="time">Time elapsed since the last update</param>
  /// <returns>True if the condition has been satisfied, false otherwise.</returns>
  public delegate bool InputConditionDelegate(IGameTime time);

  /// <summary>
  /// Represents the condition that must be satisfied by some input. If the condition is true, then an
  /// action can be performed. This is paired with an InputAction inside an InputTrigger. Actions and conditions can 
  /// vary independently from one another, allowing for their reuse.
  /// </summary>
  public class InputCondition
  {
    private InputConditionDelegate? m_condition;

    /// <summary>
    /// Constructs a new instance of the <see cref="InputCondition"/> class.
    /// </summary>
    protected InputCondition() { }

    /// <summary>
    /// Constructs a new instance of the <see cref="InputCondition"/> class with the specified condition delegate.
    /// </summary>
    /// <param name="condition">Input condition delegate</param>
    /// <exception cref="ArgumentNullException">Thrown if the condition delegate is null</exception>
    public InputCondition(InputConditionDelegate condition)
    {
      if (condition is null)
        throw new ArgumentNullException(nameof(condition));

      m_condition = condition;
    }

    /// <summary>
    /// Checks if the condition has been satisfied or not.
    /// </summary>
    /// <param name="time">Time elapsed since the last update.</param>
    /// <returns>True if the condition has been satisfied, false otherwise.</returns>
    public virtual bool Check(IGameTime time)
    {
      if (m_condition is null)
        return false;

      return m_condition(time);
    }
  }
}
