﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System.Collections.Generic;

#nullable enable

namespace Tesla.Input
{
  /// <summary>
  /// Checks if a series of input bindings have been released since the last update.
  /// </summary>
  public sealed class MultiKeyReleasedCondition : InputCondition, IMultiKeyInputBinding
  {
    private List<KeyOrMouseButton> m_bindings;
    private List<bool> m_keysThatAreDown;
    private bool m_hasKey, m_hasMouse, m_allPressedPreviously;

    /// <summary>
    /// Gets the input bindings.
    /// </summary>
    public IReadOnlyList<KeyOrMouseButton> InputBindings
    {
      get
      {
        return m_bindings;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="MultiKeyReleasedCondition"/> class.
    /// </summary>
    /// <param name="bindings">Input bindings.</param>
    public MultiKeyReleasedCondition(params KeyOrMouseButton[] bindings)
    {
      m_bindings = new List<KeyOrMouseButton>(bindings.Length);
      m_keysThatAreDown = new List<bool>(bindings.Length);
      m_allPressedPreviously = false;

      SetInputBindings(bindings);
    }

    /// <summary>
    /// Resets the condition's state.
    /// </summary>
    public void Reset()
    {
      m_allPressedPreviously = false;
      for (int i = 0; i < m_keysThatAreDown.Count; i++)
        m_keysThatAreDown[i] = false;
    }

    /// <summary>
    /// Sets the input binding combination of the condition.
    /// </summary>
    /// <param name="bindings">Input bindings.</param>
    public void SetInputBindings(params KeyOrMouseButton[] bindings)
    {
      m_bindings.Clear();
      m_keysThatAreDown.Clear();
      m_hasKey = false;
      m_hasMouse = false;

      if (bindings == null)
        return;

      m_bindings.AddRange(bindings);

      for (int i = 0; i < bindings.Length; i++)
      {
        m_keysThatAreDown.Add(false);

        if (bindings[i].IsMouseButton)
        {
          m_hasMouse = true;
        }
        else
        {
          m_hasKey = true;
        }
      }

      Reset();
    }

    /// <summary>
    /// Checks if the condition has been satisfied or not.
    /// </summary>
    /// <param name="time">Time elapsed since the last update.</param>
    /// <returns>True if the condition has been satisfied, false otherwise.</returns>
    public override bool Check(IGameTime time)
    {
      KeyboardState currKeyState = (m_hasKey && Keyboard.FocusState == InputFocusState.App) ? Keyboard.GetKeyboardState() : new KeyboardState();
      MouseState currMouseState = (m_hasMouse && Mouse.FocusState == InputFocusState.App) ? Mouse.GetMouseState() : new MouseState();

      GatherPressedKeys(ref currKeyState, ref currMouseState);

      return EvaluateReleasedCondition(time);
    }

    private void GatherPressedKeys(ref KeyboardState keyState, ref MouseState mouseState)
    {
      for (int i = 0; i < m_bindings.Count; i++)
      {
        KeyOrMouseButton binding = m_bindings[i];
        if (binding.IsMouseButton)
        {
          if (mouseState.IsButtonPressed(binding.MouseButton))
            m_keysThatAreDown[i] = true;
          else
            m_keysThatAreDown[i] = false;
        }
        else
        {
          if (keyState.IsKeyDown(binding.Key))
            m_keysThatAreDown[i] = true;
          else
            m_keysThatAreDown[i] = false;
        }
      }
    }

    private bool AreAllKeysDown()
    {
      for (int i = 0; i < m_keysThatAreDown.Count; i++)
      {
        if (!m_keysThatAreDown[i])
          return false;
      }

      return true;
    }

    private bool AreAllKeysUp()
    {
      for (int i = 0; i < m_keysThatAreDown.Count; i++)
      {
        if (m_keysThatAreDown[i])
          return false;
      }

      return true;
    }

    private bool EvaluateReleasedCondition(IGameTime time)
    {
      if (m_allPressedPreviously)
      {
        bool areAllKeysUp = AreAllKeysUp();

        //If all were pressed and we're releasing some, then evaluate to true and reset state
        if (AreAllKeysUp())
        {
          m_allPressedPreviously = false;
          return true;
        }

        //If any keys are up, don't reset the state - else you'd have to time the release perfectly
      }
      else
      {
        bool areAllKeysDown = AreAllKeysDown();

        //If there wasn't a previous state where all keys were down, the condition evaluates to false but we take note that all keys are now down
        if (areAllKeysDown)
          m_allPressedPreviously = true;
      }

      return false;
    }
  }
}
