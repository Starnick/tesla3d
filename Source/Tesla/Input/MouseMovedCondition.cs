﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;

#nullable enable

namespace Tesla.Input
{
  /// <summary>
  /// Checks if the mouse has moved since the last update.
  /// </summary>
  public sealed class MouseMovedCondition : InputCondition
  {
    /// <summary>
    /// Enumerates the valid move directions.
    /// </summary>
    public enum MoveDirection
    {
      /// <summary>
      /// Condition evaluates true if mouse only moves along X axis.
      /// </summary>
      XOnly = 0,

      /// <summary>
      /// Condition evaluates true if mouse only moves along Y axis.
      /// </summary>
      YOnly = 1,

      /// <summary>
      /// Condition evaluates true if mouse moves along any axis.
      /// </summary>
      XAndY = 2
    }

    private Int2 m_pos;
    private MoveDirection m_moveDir;
    private Int2 m_minDistance;

    /// <summary>
    /// Sets the valid move direction.
    /// </summary>
    public MoveDirection Direction
    {
      get
      {
        return m_moveDir;
      }
      set
      {
        m_moveDir = value;
      }
    }

    /// <summary>
    /// Gets or sets the minimum X,Y distances the mouse needs to move before the condition is satisfied.
    /// </summary>
    public Int2 MoveMinimum
    {
      get
      {
        return m_minDistance;
      }
      set
      {
        m_minDistance = value;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="MouseMovedCondition"/> class.
    /// </summary>
    public MouseMovedCondition() : this(MoveDirection.XAndY) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="MouseMovedCondition"/> class.
    /// </summary>
    /// <param name="moveDir">Move direction to check.</param>
    public MouseMovedCondition(MoveDirection moveDir)
    {
      m_pos = Mouse.GetMouseState().PositionInt;
      m_moveDir = moveDir;
      m_minDistance = new Int2(1, 1);
    }

    /// <summary>
    /// Checks if the condition has been satisfied or not.
    /// </summary>
    /// <param name="time">Time elapsed since the last update.</param>
    /// <returns>True if the condition has been satisfied, false otherwise.</returns>
    public override bool Check(IGameTime time)
    {
      MouseState currState = Mouse.GetMouseState();

      bool hasMoved = ((Math.Abs(m_pos.X - currState.X) >= m_minDistance.X && m_moveDir != MouseMovedCondition.MoveDirection.YOnly) || (Math.Abs(m_pos.Y - currState.Y) >= m_minDistance.Y && m_moveDir != MouseMovedCondition.MoveDirection.XOnly)) ? true : false;
      m_pos.X = currState.X;
      m_pos.Y = currState.Y;

      return hasMoved && currState.FocusState == InputFocusState.App;
    }
  }
}
