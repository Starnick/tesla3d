﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;

#nullable enable

namespace Tesla.Content
{
  /// <summary>
  /// Engine exception for an error related to content management.
  /// </summary>
  [Serializable]
  public class TeslaContentException : TeslaException
  {
    /// <summary>
    /// Constructs a new instance of the <see cref="TeslaContentException"/> class.
    /// </summary>
    public TeslaContentException() : base() { }

    /// <summary>
    /// Constructs a new instance of the <see cref="TeslaContentException"/> class.
    /// </summary>
    /// <param name="msg">Error message that explains the reason for the exception</param>
    public TeslaContentException(string? msg) : base(msg) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="TeslaContentException"/> class.
    /// </summary>
    /// <param name="paramName">Parameter name that caused the exception.</param>
    /// <param name="msg">Error message that explains the reason for the exception.</param>
    public TeslaContentException(string paramName, string? msg) : base(paramName, msg) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="TeslaContentException"/> class.
    /// </summary>
    /// <param name="msg">Error message that explains the reason for the exception.</param>
    /// <param name="innerException">Exception that caused this exception to be thrown, generally a more specific exception.</param>
    public TeslaContentException(string? msg, Exception? innerException) : base(msg, innerException) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="TeslaContentException"/> class.
    /// </summary>
    /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that holds the serialized object data about the exception being thrown.</param>
    /// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains contextual information about the source or destination.</param>
    public TeslaContentException(SerializationInfo info, StreamingContext context) : base(info, context) { }

    /// <summary>
    /// Throws a <see cref="TeslaContentException"/> if the resource file is null or does not exist.
    /// </summary>
    /// <param name="argument">Resource file.</param>
    /// <param name="paramName">Optional parameter name.</param>
    public static void ThrowIfDoesNotExist([NotNull] IResourceFile? argument, [CallerArgumentExpression("argument")] string? paramName = null)
    {
      if (argument is null || !argument.Exists)
        Throw(StringLocalizer.Instance.GetLocalizedString("ResourceFileDoesNotExist"), paramName);
    }

    /// <summary>
    /// Throws a <see cref="TeslaContentException"/> if the string representing a resource file is null or empty.
    /// </summary>
    /// <param name="argument">File path.</param>
    /// <param name="paramName">Optional parameter name.</param>
    public static void ThrowIfDoesNotExist([NotNull] string? argument, [CallerArgumentExpression("argument")] string? paramName = null)
    {
      if (string.IsNullOrWhiteSpace(argument))
        Throw(StringLocalizer.Instance.GetLocalizedString("ResourceFileDoesNotExist"), paramName);
    }

    /// <summary>
    /// Throws a <see cref="TeslaContentException"/> if the repository is null or not opened.
    /// </summary>
    /// <param name="argument">Resource repository.</param>
    /// <param name="paramName">Optional parameter name.</param>
    public static void ThrowIfNotOpened([NotNull] IResourceRepository? argument, [CallerArgumentExpression("argument")] string? paramName = null)
    {
      if (argument is null || !argument.IsOpen)
        Throw(StringLocalizer.Instance.GetLocalizedString("ResourceRepositoryNotOpened"), paramName);
    }

    /// <summary>
    /// Throws a <see cref="TeslaContentException"/> if the repository is null or already opened.
    /// </summary>
    /// <param name="argument">Resource repository.</param>
    /// <param name="paramName">Optional parameter name.</param>
    public static void ThrowIfAlreadyOpened([NotNull] IResourceRepository? argument, [CallerArgumentExpression("argument")] string? paramName = null)
    {
      if (argument is null || argument.IsOpen)
        Throw(StringLocalizer.Instance.GetLocalizedString("ResourceRepositoryAlreadyOpened"), paramName);
    }

    [DoesNotReturn]
    private static void Throw(string msg, string? paramName = null)
    {
      if (string.IsNullOrEmpty(paramName))
        throw new TeslaContentException(msg);
      else
        throw new TeslaContentException(paramName, msg);
    }
  }
}
