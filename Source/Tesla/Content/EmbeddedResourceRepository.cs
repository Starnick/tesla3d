﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Reflection;
using System.Resources;
using System.Threading;

#nullable enable

namespace Tesla.Content
{
  /// <summary>
  /// Represents an embedded resource repository, a resx file inside an assembly.
  /// </summary>
  [DebuggerDisplay("RootPath = {RootPath}")]
  public sealed class EmbeddedResourceRepository : IResourceRepository
  {
    private static IResourceFile NullResource = new NullResource();

    private Dictionary<string, EmbeddedResourceFile> m_embeddedResources;
    private ResourceManager m_resourceManager;
    private bool m_isOpen;
    private DateTime m_creationTime;
    private DateTime m_lastAccessedTime;

    /// <inheritdoc />
    public string RootPath { get { return m_resourceManager.BaseName; } }

    /// <inheritdoc />
    public bool IsOpen { get { return m_isOpen; } }

    /// <inheritdoc />
    public bool Exists { get { return true; } }

    /// <inheritdoc />
    public bool IsReadOnly { get { return true; } }

    /// <inheritdoc />
    public DateTime CreationTime { get { return m_creationTime; } }

    /// <inheritdoc />
    public DateTime LastAccessTime { get { return m_lastAccessedTime; } }

    /// <inheritdoc />
    public DateTime LastWriteTime { get { return m_creationTime; } }

    /// <summary>
    /// Constructs a new instance of the <see cref="EmbeddedResourceRepository"/> class.
    /// </summary>
    /// <param name="rootName">Root name of the resource (resx) file in the assembly.</param>
    /// <param name="assembly">The assembly that the resx file resides.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the root name or the assembly are null.</exception>
    public EmbeddedResourceRepository(string rootName, Assembly assembly)
    {
      ArgumentNullException.ThrowIfNullOrEmpty(rootName, nameof(rootName));
      ArgumentNullException.ThrowIfNull(assembly, nameof(assembly));

      m_resourceManager = new ResourceManager(rootName, assembly);
      m_embeddedResources = new Dictionary<string, EmbeddedResourceFile>();
      m_creationTime = m_lastAccessedTime = DateTime.UtcNow;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="EmbeddedResourceRepository"/> class.
    /// </summary>
    /// <param name="resourceManager">The resource manager containing the resources.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the resource manager is null.</exception>
    public EmbeddedResourceRepository(ResourceManager resourceManager)
    {
      ArgumentNullException.ThrowIfNull(resourceManager, nameof(resourceManager));

      m_resourceManager = resourceManager;
      m_embeddedResources = new Dictionary<string, EmbeddedResourceFile>();
      m_creationTime = m_lastAccessedTime = DateTime.UtcNow;
    }

    /// <inheritdoc />
    /// <exception cref="TeslaContentException">Thrown if the repository is not open.</exception>
    public void OpenConnection()
    {
      OpenConnection(ResourceFileMode.Open, ResourceFileShare.Read);
    }

    /// <inheritdoc />
    /// <exception cref="TeslaContentException">Thrown if the repository is already opened, or if trying to open with non-read only file flags.</exception>
    public void OpenConnection(ResourceFileMode mode)
    {
      OpenConnection(mode, ResourceFileShare.None);
    }

    /// <inheritdoc />
    /// <exception cref="TeslaContentException">Thrown if the repository is already opened, or if trying to open with non-read only file flags.</exception>
    public void OpenConnection(ResourceFileMode mode, ResourceFileShare shareMode)
    {
      TeslaContentException.ThrowIfAlreadyOpened(this);

      if (mode != ResourceFileMode.Open)
        ThrowReadOnly();

      m_isOpen = true;
      m_lastAccessedTime = DateTime.UtcNow;
      GrabResources();
    }

    /// <inheritdoc />
    /// <exception cref="TeslaContentException">Thrown if the repository is not open.</exception>
    public void CloseConnection()
    {
      TeslaContentException.ThrowIfNotOpened(this);

      m_isOpen = false;
      m_lastAccessedTime = DateTime.UtcNow;
      m_embeddedResources.Clear();
    }

    /// <inheritdoc />
    /// <exception cref="TeslaContentException">Thrown if the repository is not open.</exception>
    public IResourceFile GetResourceFile(string resourceName)
    {
      TeslaContentException.ThrowIfNotOpened(this);

      m_lastAccessedTime = DateTime.UtcNow;

      EmbeddedResourceFile? resourceFile;
      if (m_embeddedResources.TryGetValue(GetResourceName(resourceName), out resourceFile))
        return resourceFile;

      return NullResource;
    }

    /// <inheritdoc />
    /// <exception cref="TeslaContentException">Thrown if the repository is not open.</exception>
    public IResourceFile GetResourceFileRelativeTo(string resourceName, IResourceFile resource)
    {
      TeslaContentException.ThrowIfNotOpened(this);

      if (!(resource is EmbeddedResourceFile))
        return NullResource;

      m_lastAccessedTime = DateTime.UtcNow;

      //All resoure names (including full path) should be "flat"
      EmbeddedResourceFile? resourceFile;
      if (m_embeddedResources.TryGetValue(GetResourceName(resourceName), out resourceFile))
        return resourceFile;

      return NullResource;
    }

    /// <inheritdoc />
    /// <exception cref="TeslaContentException">Thrown if the repository is not open.</exception>
    public IEnumerable<IResourceFile> EnumerateResourceFiles(bool recursive, string? searchPattern = null)
    {
      TeslaContentException.ThrowIfNotOpened(this);

      m_lastAccessedTime = DateTime.UtcNow;
      return m_embeddedResources.Values;
    }

    /// <inheritdoc />
    /// <exception cref="TeslaContentException">Thrown if the repository is not open.</exception>
    public bool ContainsResource(string resourceName)
    {
      TeslaContentException.ThrowIfNotOpened(this);

      if (String.IsNullOrEmpty(resourceName))
        return false;

      return m_resourceManager.GetObject(GetResourceName(resourceName)) is not null;
    }

    /// <inheritdoc />
    /// <exception cref="TeslaContentException">Thrown if the repository is not open.</exception>
    public string GetFullPath(string resourceName)
    {
      TeslaContentException.ThrowIfNotOpened(this);

      return String.Concat(RootPath, ".", resourceName);
    }

    /// <inheritdoc />
    /// <exception cref="TeslaContentException">Thrown if the repository is not open.</exception>
    public string GetFullPathRelativeTo(string resourceName, IResourceFile resource)
    {
      TeslaContentException.ThrowIfNotOpened(this);

      if (!(resource is EmbeddedResourceFile))
        return String.Empty;

      return GetFullPath(resourceName);
    }

    /// <inheritdoc />
    /// <exception cref="TeslaContentException">Thrown if the repository is not open.</exception>
    public void Refresh()
    {
      TeslaContentException.ThrowIfNotOpened(this);
    }

    private string GetResourceName(string fullPath)
    {
      string fullPathNoExt = Path.GetFileNameWithoutExtension(fullPath);
      string[] components = fullPathNoExt.Split('.');

      if (components is null || components.Length == 0)
        return fullPath;

      return components[components.Length - 1];
    }

    private void GrabResources()
    {
      ResourceSet? resourceSet = m_resourceManager.GetResourceSet(Thread.CurrentThread.CurrentUICulture, true, true);
      if (resourceSet is null)
        return;

      foreach (DictionaryEntry entry in resourceSet)
      {
        string? key = entry.Key as string;
        if (key is not null && !m_embeddedResources.ContainsKey(key))
          m_embeddedResources.Add(key, new EmbeddedResourceFile(m_resourceManager, key, this));
      }
    }

    [DoesNotReturn]
    private void ThrowReadOnly()
    {
      throw new TeslaContentException(StringLocalizer.Instance.GetLocalizedString("EmbeddedResourceFileReadOnly"));
    }
  }
}
