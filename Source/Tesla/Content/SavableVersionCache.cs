﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Buffers;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using Tesla.Utilities;

#nullable enable

namespace Tesla.Content
{
  /// <summary>
  /// Represents an ordered list of versions for an <see cref="ISavable"/> type. This always will have at least one entry (the root),
  /// and the next entry is it's base class if it has one, and so on. The order matches the class hierarchy of the type where the last entry is
  /// the last <see cref="ISavable"/> ancestor type.
  /// </summary>
  [DebuggerDisplay("{ToString(),nq}")]
  public sealed class SavableVersionHierarchy : IReadOnlyList<SavableVersionHierarchy.Entry>
  {
    /// <summary>
    /// Version entry for a savable type.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    public readonly struct Entry
    {
      /// <summary>
      /// Current runtime Savable version.
      /// </summary>
      public readonly int Version;

      /// <summary>
      /// Savable type.
      /// </summary>
      public readonly Type Type;

      /// <summary>
      /// Fully qualified name of the type from <see cref="SmartActivator.GetAssemblyQualifiedName(Type)"/>. This will not have culture/publickey/assembly version metadata.
      /// </summary>
      public readonly string AssemblyQualifiedTypeName;

      internal Entry(Type type, int version)
      {
        Type = type;
        Version = version;
        AssemblyQualifiedTypeName = SmartActivator.GetAssemblyQualifiedName(type);
        Debug.Assert(AssemblyQualifiedTypeName is not null);
      }

      /// <inheritdoc />
      public readonly override string ToString()
      {
        return $"[\"{AssemblyQualifiedTypeName}\", {Version}]";
      }
    }

    private List<Entry> m_hierarchy;

    /// <summary>
    /// Gets the root version entry. This is always the entry at index 0.
    /// </summary>
    public Entry Root { get { return (m_hierarchy.Count > 0) ? m_hierarchy[0] : default; } }

    /// <summary>
    /// Gets the number of version entries in the hierarchy (including the root). This always will be at least one.
    /// </summary>
    public int Count { get { return m_hierarchy.Count; } }

    /// <summary>
    /// Gets the version entry at the specified index.
    /// </summary>
    /// <param name="index">Zero-based index.</param>
    /// <returns>Version entry.</returns>
    public Entry this[int index] { get { return m_hierarchy[index]; } }

    internal SavableVersionHierarchy()
    {
      m_hierarchy = new List<Entry>();
    }

    internal void Add(Entry entry)
    {
      m_hierarchy.Add(entry);
    }

    internal void AddRange(SavableVersionHierarchy other)
    {
      m_hierarchy.AddRange(other.m_hierarchy);
    }

    /// <summary>
    /// Returns an enumerator for the entries.
    /// </summary>
    /// <returns>Enumerator.</returns>
    public List<Entry>.Enumerator GetEnumerator()
    {
      return m_hierarchy.GetEnumerator();
    }

    /// <inheritdoc />
    IEnumerator<Entry> IEnumerable<Entry>.GetEnumerator() { return GetEnumerator(); }

    /// <inheritdoc />
    IEnumerator IEnumerable.GetEnumerator() { return GetEnumerator(); }

    /// <inheritdoc />
    public override string ToString()
    {
      return $"{Root.ToString()} + {Math.Max(0, Count - 1)}";
    }
  }

  /// <summary>
  /// Cache for maintaining version numbers for objects that implement <see cref="ISavable"/>. By default, all savable objects have a default "version 1" which
  /// may be modified by decorating the class with a <see cref="SavableVersionAttribute"/>. Since every type has a version associated with it (even an abstract base class, 
  /// which may or may not write out properties during serialization), types can implement backwards compatability loading of old published data.
  /// </summary>
  public static class SavableVersionCache
  {
    private static ConcurrentDictionary<Type, SavableVersionHierarchy> s_cache = new ConcurrentDictionary<Type, SavableVersionHierarchy>();
    private static Type s_savableType = typeof(ISavable);
    private static Type s_versionAttributeType = typeof(SavableVersionAttribute);

    /// <summary>
    /// Determines if the type is an <see cref="ISavable"/> object. It must implement the interface and be a class.
    /// </summary>
    /// <param name="type">Type.</param>
    /// <returns>True if savable, false if not.</returns>
    public static bool IsSavable([NotNullWhen(true)] Type? type)
    {
      if (type is null)
        return false;

      return type.IsClass && s_savableType.IsAssignableFrom(type);
    }

    /// <summary>
    /// Gets the version number for the <see cref="ISavable"/> type. If the type is not <see cref="ISavable"/> this always
    /// returns the version 1. This is thread-safe.
    /// </summary>
    /// <param name="type">Type to get versioning for.</param>
    /// <returns>A version number (1 or greater) associated with the type.</returns>
    public static int GetVersion(Type? type)
    {
      // If not null, guaranteed to have at least one entry.
      SavableVersionHierarchy? versionList = GetVersions(type);
      return (versionList is null) ? 1 : versionList[0].Version;
    }

    /// <summary>
    /// Gets the version hierarchy for the <see cref="ISavable"/> type. This is thread-safe.
    /// </summary>
    /// <param name="type">Type to get versioning for.</param>
    /// <returns>A <see cref="SavableVersionHierarchy"/> for the type and it's base types. If not null, this has at least one entry. If null the type was null OR does not implement <see cref="ISavable"/>.</returns>
    public static SavableVersionHierarchy? GetVersions(Type? type)
    {
      if (!IsSavable(type))
        return null;

      SavableVersionHierarchy? hierarchy;
      if (!s_cache.TryGetValue(type, out hierarchy))
      {
        hierarchy = new SavableVersionHierarchy();
        PopulateVersionHierarchy(type, hierarchy);

        // Something went wrong...hierarchy at this point is guaranteed to always have at least one element.
        if (hierarchy.Count == 0)
          throw new InvalidOperationException(StringLocalizer.Instance.GetLocalizedString("TypeMustBeSavable", type.AssemblyQualifiedName));

        hierarchy = s_cache.GetOrAdd(type, hierarchy);
      }

      return hierarchy;
    }

    private static void PopulateVersionHierarchy(Type type, SavableVersionHierarchy list)
    {
      object[] attributes = type.GetCustomAttributes(s_versionAttributeType, false);
      int version = -1;
      bool addedVersion = false;

      if (attributes is not null)
      {
        for (int i = 0; i < attributes.Length; i++)
        {
          SavableVersionAttribute? versionAttr = attributes[i] as SavableVersionAttribute;
          if (versionAttr is null)
            continue;

          version = versionAttr.Version;
          addedVersion = true;
          break;
        }
      }

      if (!addedVersion)
        version = 1;

      list.Add(new SavableVersionHierarchy.Entry(type, version));

      // Gather base type versions, if any
      Type? baseType = type.BaseType;
      if (baseType is null)
        return;

      SavableVersionHierarchy? baseVersionList = GetVersions(baseType);
      if (baseVersionList is not null)
        list.AddRange(baseVersionList);
    }
  }
}
