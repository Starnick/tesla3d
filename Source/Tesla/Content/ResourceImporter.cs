﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.IO;

#nullable enable

namespace Tesla.Content
{
  /// <summary>
  /// A base resource importer.
  /// </summary>
  /// <typeparam name="T">Content target type</typeparam>
  public abstract class ResourceImporter<T> : IResourceImporter where T : class
  {
    private string[] m_extensions;
    private Type m_targetType;

    /// <inheritdoc />
    public IEnumerable<string> Extensions { get { return m_extensions; } }

    /// <inheritdoc />
    public Type TargetType { get { return m_targetType; } }

    /// <summary>
    /// Constructs a new instance of the <see cref="ResourceImporter{T}"/> class.
    /// </summary>
    /// <param name="extensions">Format extensions that the importer can import</param>
    /// <exception cref="System.ArgumentNullException">Thrown if no extensions are specified</exception>
    protected ResourceImporter(params string[] extensions)
    {
      m_extensions = extensions;
      m_targetType = typeof(T);

      if (extensions is null || extensions.Length == 0)
        throw new ArgumentNullException(nameof(extensions));
    }

    /// <summary>
    /// Loads content from the specified resource as the target runtime type.
    /// </summary>
    /// <param name="resourceFile">Resource file to read from</param>
    /// <param name="contentManager">Calling content manager</param>
    /// <param name="parameters">Optional loading parameters</param>
    /// <returns>The loaded object or null if it could not be loaded</returns>
    public abstract T? Load(IResourceFile resourceFile, ContentManager contentManager, ImporterParameters parameters);

    /// <summary>
    /// Loads content from the specified stream as the target runtime type.
    /// </summary>
    /// <param name="input">Stream to read from.</param>
    /// <param name="contentManager">Calling content manager.</param>
    /// <param name="parameters">Optional loading parameters.</param>
    /// <returns>The loaded object or null if it could not be loaded.</returns>
    public abstract T? Load(Stream input, ContentManager contentManager, ImporterParameters parameters);

    /// <inheritdoc />
    /// <returns>The loaded object or null if it could not be loaded</returns>
    object? IResourceImporter.Load(IResourceFile resourceFile, ContentManager contentManager, ImporterParameters parameters)
    {
      return Load(resourceFile, contentManager, parameters);
    }

    /// <inheritdoc />
    /// <returns>The loaded object or null if it could not be loaded.</returns>
    object? IResourceImporter.Load(Stream input, ContentManager contentManager, ImporterParameters parameters)
    {
      return Load(input, contentManager, parameters);
    }

    /// <summary>
    /// Returns true if the specified type can be handled by this importer, false otherwise.
    /// </summary>
    /// <param name="contentType">Content type to be loaded.</param>
    /// <returns>True if the type can be loaded by this importer, false otherwise.</returns>
    public virtual bool CanLoadType(Type contentType)
    {
      if (contentType is null)
        return false;

      //Most importers will have a target type that will allow downcasting to more concrete types (e.g. Texture -> Texture2D)
      //but upcasting is also valid (Texture -> ISavable), but that may be iffy because then a collision may occur with another
      //extension-importer pair (how this importer is stored in the content manager)
      return m_targetType.IsAssignableFrom(contentType) || contentType.IsAssignableFrom(m_targetType);
    }

    /// <summary>
    /// Validates input parameters - whether the resource file exists or if the importer can handle its extension. Be aware that
    /// the format of the data is not validated, just the file extension.
    /// </summary>
    /// <param name="resourceFile">The resource file to load from</param>
    /// <param name="contentManager">The calling content manager</param>
    /// <param name="parameters">Optional parameters, if null <see cref="ImporterParameters.None"/> is set.</param>
    /// <exception cref="ArgumentNullException">Thrown if the resource file/content manager are null</exception>
    /// <exception cref="ArgumentException">Thrown if the resource to load has an unsupported format extension.</exception>
    protected void ValidateParameters(IResourceFile resourceFile, ContentManager contentManager, ref ImporterParameters parameters)
    {
      if (resourceFile is null || !resourceFile.Exists)
        throw new ArgumentNullException(nameof(resourceFile), StringLocalizer.Instance.GetLocalizedString("ResourceFileDoesNotExist"));

      ThrowIfBadExtension(resourceFile);
      ArgumentNullException.ThrowIfNull(contentManager, nameof(contentManager));

      if (parameters is null)
        parameters = ImporterParameters.None;

      string reason;
      if (!parameters.Validate(out reason))
        throw new TeslaContentException(StringLocalizer.Instance.GetLocalizedString("InvalidImporterParams", reason));
    }

    /// <summary>
    /// Validates input parameters - if we can read from the stream and the like. Be aware that the format of the data is
    /// not validated.
    /// </summary>
    /// <param name="input">The stream to load from</param>
    /// <param name="contentManager">The calling content manager</param>
    /// <param name="parameters">Optional parameters, if null <see cref="ImporterParameters.None"/> is set.</param>
    /// <exception cref="ArgumentNullException">Thrown if the stream/content manager are null or if we cannot read from the stream.</exception>
    protected void ValidateParameters(Stream input, ContentManager contentManager, ref ImporterParameters parameters)
    {
      if (input is null || !input.CanRead)
        throw new ArgumentNullException(nameof(input), StringLocalizer.Instance.GetLocalizedString("CannotReadFromStream"));
      
      ArgumentNullException.ThrowIfNull(contentManager, nameof(contentManager));

      if (parameters is null)
        parameters = ImporterParameters.None;

      string reason;
      if (!parameters.Validate(out reason))
        throw new TeslaContentException(StringLocalizer.Instance.GetLocalizedString("InvalidImporterParams", reason));
    }

    /// <summary>
    /// Validates input parameters - if we can read from the stream and the like. Be aware that the format of the data is
    /// not validated.
    /// </summary>
    /// <param name="input">The stream to load from</param>
    /// <param name="contentManager">The calling content manager</param>
    /// <param name="parameters">Optional parameters, if null <see cref="ImporterParameters.None"/> is set.</param>
    /// <param name="externalResolver">Resolver for resolving the stream's external references.</param>
    /// <exception cref="ArgumentNullException">Thrown if the stream/content manager are null or if we cannot read from the stream.</exception>
    protected void ValidateParameters(Stream input, ContentManager contentManager, ref ImporterParameters parameters, IExternalReferenceResolver externalResolver)
    {
      ArgumentNullException.ThrowIfNull(externalResolver, nameof(externalResolver));
      ArgumentNullException.ThrowIfNull(contentManager, nameof(contentManager));

      if (externalResolver.ParentResourceFile is null || !externalResolver.ParentResourceFile.Exists)
        throw new ArgumentNullException(nameof(externalResolver.ParentResourceFile), StringLocalizer.Instance.GetLocalizedString("ResourceFileDoesNotExist"));

      if (input is null || !input.CanRead)
        throw new ArgumentNullException(nameof(input), StringLocalizer.Instance.GetLocalizedString("CannotReadFromStream"));

      if (parameters is null)
        parameters = ImporterParameters.None;

      string reason;
      if (!parameters.Validate(out reason))
        throw new TeslaContentException(StringLocalizer.Instance.GetLocalizedString("InvalidImporterParams", reason));
    }

    private void ThrowIfBadExtension(IResourceFile resourceFile)
    {
      bool canHandle = false;

      foreach (string extension in m_extensions)
      {
        if (resourceFile.Extension.Equals(extension, StringComparison.OrdinalIgnoreCase))
          canHandle = true;
      }

      if (!canHandle)
        throw new ArgumentException(StringLocalizer.Instance.GetLocalizedString("InvalidFileExtension", resourceFile.Extension));
    }
  }
}
