﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.IO;
using System.Reflection;

namespace Tesla.Content
{
  /// <summary>
  /// Utilities for resoure management.
  /// </summary>
  public static class ContentHelper
  {
    private static char[] s_pathTrimChars = new char[] { Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar, Path.VolumeSeparatorChar };

    /// <summary>
    /// Gets the directory location of the application
    /// </summary>
    public static String AppLocation
    {
      get
      {
        return AppContext.BaseDirectory;
      }
    }

    /// <summary>
    /// Gets a resource name for the object.
    /// </summary>
    /// <typeparam name="T">Object type</typeparam>
    /// <param name="resource">Resource object</param>
    /// <returns>Resource name</returns>
    public static String GetResourceName<T>(T resource)
    {
      if (resource == null)
        return String.Empty;

      if (resource is INamed)
        return (resource as INamed).Name;

      return typeof(T).Name;
    }

    /// <summary>
    /// Parses a resource path that may optionally have a subresource name. A double colon identifies the separator between
    /// the resource file path and the sub resource name. A resource file may contain several resources that may want to be loaded up
    /// singular and not have the rest of the content be loaded up.
    /// </summary>
    /// <remarks>
    /// Format is: Path/To/Resource/ResourceFileNameWithExtension::subresourceName.
    /// </remarks>
    /// <param name="resourcePathWithName">Resource path with an optional sub resource name.</param>
    /// <param name="path">Resource path without the sub resource name.</param>
    /// <param name="subresourceName">Sub resource name, or an empty string if no name was specified.</param>
    public static void ParseSubresourceName(String resourcePathWithName, out String path, out String subresourceName)
    {
      path = resourcePathWithName;
      subresourceName = String.Empty;

      if (String.IsNullOrEmpty(resourcePathWithName))
        return;

      int indexOfFirst = -1;
      for (int i = 0; i < resourcePathWithName.Length; i++)
      {
        if (resourcePathWithName[i] == ':')
        {
          int nextIndex = i + 1;
          if (nextIndex < resourcePathWithName.Length && resourcePathWithName[nextIndex] == ':')
          {
            indexOfFirst = i;
            break;
          }
        }
      }

      if (indexOfFirst == -1)
        return;

      path = (indexOfFirst == 0) ? String.Empty : resourcePathWithName.Substring(0, indexOfFirst);
      subresourceName = resourcePathWithName.Substring(indexOfFirst + 2, resourcePathWithName.Length - (indexOfFirst + 2));
    }

    /// <summary>
    /// Normalizes the path separators in the file path. This will return a path with the same
    /// separators used by <see cref="Path.GetDirectoryName"/>.
    /// </summary>
    /// <param name="filePath">File path to normalize.</param>
    /// <returns>Normalized file path.</returns>
    public static String NormalizePath(String filePath)
    {
      if (String.IsNullOrEmpty(filePath))
        return filePath;

      return Path.Combine(Path.GetDirectoryName(filePath), Path.GetFileName(filePath));
    }

    /// <summary>
    /// Trims the rooted path.
    /// </summary>
    /// <param name="filePath">The file path.</param>
    /// <returns></returns>
    public static String TrimRootedPath(String filePath)
    {
      if (String.IsNullOrEmpty(filePath) || !Path.IsPathRooted(filePath))
        return filePath;

      int length = filePath.Length;

      if (length >= 1)
      {
        if (filePath[0] == Path.DirectorySeparatorChar || filePath[0] == Path.AltDirectorySeparatorChar)
          return filePath.TrimStart(s_pathTrimChars);
      }

      if (length >= 2)
      {
        //Ignore the volume letter
        if (filePath[1] == Path.VolumeSeparatorChar)
          return filePath.Substring(1).TrimStart(s_pathTrimChars);
      }

      return filePath;
    }
  }
}
