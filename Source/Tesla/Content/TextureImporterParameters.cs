﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;

namespace Tesla.Content
{
    /// <summary>
    /// Defines a set of common parameters for texture importers.
    /// </summary>
    [SavableVersion(1)]
    public class TextureImporterParameters : ImporterParameters
    {
        /// <summary>
        /// Gets or sets if images should be flipped along the V axis when they are
        /// imported. Default is false.
        /// </summary>
        public bool FlipImage { get; set; }

        /// <summary>
        /// Gets or sets if mip maps should be generated for images that do not have them.
        /// Existing mip maps are not changed. Default is false.
        /// </summary>
        public bool GenerateMipMaps { get; set; }

        /// <summary>
        /// Gets or sets if the image should be resized to the nearest power of two
        /// dimension for compatibility with older GPUs and performance. Default is false.
        /// </summary>
        public bool ResizePowerOfTwo { get; set; }

        /// <summary>
        /// Gets or sets what format the image should be converted to upon import. If
        /// <see cref="TextureConversionFormat.NoChange"/> (the default) then the image's existing format is used
        /// if possible. If not possible, then the standard color format is used.
        /// </summary>
        public TextureConversionFormat TextureFormat { get; set; }

        /// <summary>
        /// Gets or sets the color key. Any pixel that matches this color will get replaced
        /// by transparent black.  Default color is Magenta (255, 0, 255, 255).
        /// </summary>
        public Color ColorKey { get; set; }

        /// <summary>
        /// Gets or sets if the color key should be enabled. Default is false.
        /// </summary>
        public bool ColorKeyEnabled { get; set; }

        /// <summary>
        /// Gets or sets if the image's alpha should be pre-multiplied with the pixel color values. Default is false.
        /// </summary>
        public bool PreMultiplyAlpha { get; set; }

        /// <summary>
        /// Gets or sets if the image is to be used as a normal map. Default is false.
        /// </summary>
        public bool IsNormalMap { get; set; }

        /// <summary>
        /// Constructs a new instance of the <see cref="TextureImporterParameters"/> class.
        /// </summary>
        public TextureImporterParameters()
            : this(false, false, false)
        {
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="TextureImporterParameters"/> class.
        /// </summary>
        /// <param name="flipImage">True if the image should be flipped about its V axis, false otherwise.</param>
        /// <param name="genMipMaps">True if the image should have mipmaps generated if none are present, false otherwise.</param>
        /// <param name="resizePowerTwo">True if the image should be resized to a power of two, false otherwise.</param>
        public TextureImporterParameters(bool flipImage, bool genMipMaps, bool resizePowerTwo)
        {
            FlipImage = flipImage;
            GenerateMipMaps = genMipMaps;
            ResizePowerOfTwo = resizePowerTwo;
            TextureFormat = TextureConversionFormat.NoChange;
            ColorKey = new Color(255, 0, 255, 255);
            ColorKeyEnabled = false;
            PreMultiplyAlpha = false;
            IsNormalMap = false;
        }

        /// <summary>
        /// Copies the importer parameters from the specified instance.
        /// </summary>
        /// <param name="parameters">Importer parameter instance to copy from.</param>
        public override void Set(ImporterParameters parameters)
        {
            base.Set(parameters);

            TextureImporterParameters imageParams = parameters as TextureImporterParameters;

            if(imageParams == null)
                return;

            FlipImage = imageParams.FlipImage;
            GenerateMipMaps = imageParams.GenerateMipMaps;
            ResizePowerOfTwo = imageParams.ResizePowerOfTwo;
            TextureFormat = imageParams.TextureFormat;
            ColorKey = imageParams.ColorKey;
            ColorKeyEnabled = imageParams.ColorKeyEnabled;
            PreMultiplyAlpha = imageParams.PreMultiplyAlpha;
            IsNormalMap = imageParams.IsNormalMap;
        }

        /// <summary>
        /// Writes the object data to the output.
        /// </summary>
        /// <param name="output">Savable writer</param>
        public override void Write(ISavableWriter output)
        {
            base.Write(output);

            output.Write("FlipImage", FlipImage);
            output.Write("GenerateMipMaps", GenerateMipMaps);
            output.Write("ResizePowerOfTwo", ResizePowerOfTwo);
            output.WriteEnum<TextureConversionFormat>("TextureFormat", TextureFormat);
            output.Write<Color>("ColorKey", ColorKey);
            output.Write("ColorKeyEnabled", ColorKeyEnabled);
            output.Write("PreMultiplyAlpha", PreMultiplyAlpha);
            output.Write("IsNormalMap", IsNormalMap);
        }

        /// <summary>
        /// Reads the object data from the input.
        /// </summary>
        /// <param name="input">Savable reader</param>
        public override void Read(ISavableReader input)
        {
            base.Read(input);

            FlipImage = input.ReadBoolean("FlipImage");
            GenerateMipMaps = input.ReadBoolean("GenerateMipMaps");
            ResizePowerOfTwo = input.ReadBoolean("ResizePowerOfTwo");
            TextureFormat = input.ReadEnum<TextureConversionFormat>("TextureFormat");
            ColorKey = input.Read<Color>("ColorKey");
            ColorKeyEnabled = input.ReadBoolean("ColorKeyEnabled");
            PreMultiplyAlpha = input.ReadBoolean("PreMultiplyAlpha");
            IsNormalMap = input.ReadBoolean("IsNormalMap");
        }
    }
}
