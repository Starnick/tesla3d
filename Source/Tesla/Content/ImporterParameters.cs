﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;

namespace Tesla.Content
{
    /// <summary>
    /// Defines a set of parameters that configure how an asset is loaded.
    /// </summary>
    [SavableVersion(1)]
    public class ImporterParameters : ISavable
    {
        private static readonly ImporterParameters s_none = new ImporterParameters();

        private String m_subresourceName;
        private Dictionary<String, String> m_keyValuePairs;

        /// <summary>
        /// Gets an empty set of loading parameters.
        /// </summary>
        public static ImporterParameters None
        {
            get
            {
                return s_none;
            }
        }

        /// <summary>
        /// Gets or sets an optional subresource name. When a resource file is imported it may contain multiple pieces of
        /// content of which only one needs to be imported, as identified by this name.
        /// </summary>
        public String SubresourceName
        {
            get
            {
                return m_subresourceName;
            }
            set
            {
                m_subresourceName = value;
                if(String.IsNullOrEmpty(m_subresourceName))
                    m_subresourceName = String.Empty;
            }
        }

        /// <summary>
        /// Gets the extended parameter key-value pair collection.
        /// </summary>
        public Dictionary<String, String> ExtendedParameters
        {
            get
            {
                return m_keyValuePairs;
            }
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="ImporterParameters"/> class.
        /// </summary>
        public ImporterParameters()
        {
            m_keyValuePairs = new Dictionary<String, String>();
        }

        /// <summary>
        /// Copies the importer parameters from the specified instance.
        /// </summary>
        /// <param name="parameters">Importer parameter instance to copy from.</param>
        public virtual void Set(ImporterParameters parameters)
        {
            if(parameters == null)
                return;

            m_subresourceName = parameters.m_subresourceName;
            m_keyValuePairs.Clear();

            foreach(KeyValuePair<String, String> kv in parameters.m_keyValuePairs)
                m_keyValuePairs.Add(kv.Key, kv.Value);
        }

        /// <summary>
        /// Resets the importer parameters to default values.
        /// </summary>
        public virtual void Reset()
        {
            m_subresourceName = String.Empty;
            m_keyValuePairs.Clear();
        }

        /// <summary>
        /// Validates parameters.
        /// </summary>
        /// <returns>True if the parameters are valid.</returns>
        public bool Validate()
        {
            String reason;
            return Validate(out reason);
        }

        /// <summary>
        /// Validates parameters.
        /// </summary>
        /// <param name="reason">Reason why the parameters are not valid.</param>
        /// <returns>True if the parameters are valid.</returns>
        public virtual bool Validate(out String reason)
        {
            reason = String.Empty;
            return true;
        }

        /// <summary>
        /// Reads the object data from the input.
        /// </summary>
        /// <param name="input">Savable reader</param>
        public virtual void Read(ISavableReader input)
        {
            m_subresourceName = input.ReadString("SubresourceName");

            int count = input.BeginReadGroup("ExtendedParameters");

            for(int i = 0; i < count; i++)
            {
                input.BeginReadGroup("Parameter");

                String key = input.ReadString("Key");
                String value = input.ReadString("Value");

                if(!String.IsNullOrEmpty(key) && !String.IsNullOrEmpty(value))
                    m_keyValuePairs.Add(key, value);

                input.EndReadGroup();
            }

            input.EndReadGroup();
        }

        /// <summary>
        /// Writes the object data to the output.
        /// </summary>
        /// <param name="output">Savable writer</param>
        public virtual void Write(ISavableWriter output)
        {
            output.Write("SubresourceName", m_subresourceName);

            output.BeginWriteGroup("ExtendedParameters", m_keyValuePairs.Count);

            foreach(KeyValuePair<String, String> kv in m_keyValuePairs)
            {
                output.BeginWriteGroup("Parameter");

                output.Write("Key", kv.Key);
                output.Write("Value", kv.Value);

                output.EndWriteGroup();
            }

            output.EndWriteGroup();
        }
    }
}
