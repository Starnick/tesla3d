﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Resources;

#nullable enable

namespace Tesla.Content
{
  /// <summary>
  /// A resource file that can access files in an embedded resource file.
  /// </summary>
  [DebuggerDisplay("name = {Name}, Exists = {Exists}")]
  public sealed class EmbeddedResourceFile : IResourceFile
  {
    private ResourceManager m_resourceManager;
    private string m_name;
    private string m_extension;
    private string m_fullName;
    private bool m_exists;
    private IResourceRepository m_repository;

    private byte[]? m_byteData;
    private string? m_stringData;

    /// <inheritdoc />
    public string Name { get { return m_name; } }

    /// <inheritdoc />
    public string NameWithExtension { get { return m_fullName; } }

    /// <inheritdoc />
    public string Extension { get { return m_extension; } }

    /// <inheritdoc />
    public string FullName { get { return m_fullName; } }

    /// <inheritdoc />
    public string RelativeName { get { return m_fullName; } }

    /// <inheritdoc />
    public bool Exists { get { return m_exists; } }

    /// <inheritdoc />
    public bool IsReadOnly { get { return true; } }

    /// <inheritdoc />
    public DateTime CreationTime { get { return m_repository.CreationTime; } }

    /// <inheritdoc />
    public DateTime LastAccessTime { get { return m_repository.LastAccessTime; } }

    /// <inheritdoc />
    public DateTime LastWriteTime { get { return m_repository.LastWriteTime; } }

    /// <inheritdoc />
    public IResourceRepository Repository { get { return m_repository; } }

    internal EmbeddedResourceFile(ResourceManager resourceManager, string embeddedResourceName, IResourceRepository repository)
    {
      m_resourceManager = resourceManager;
      m_extension = Path.GetFileNameWithoutExtension(embeddedResourceName).ToLower();
      m_name = Path.GetFileNameWithoutExtension(embeddedResourceName);
      m_fullName = embeddedResourceName;
      m_repository = repository;
      m_exists = false;

      object? obj = m_resourceManager.GetObject(m_name);
      if (obj is byte[] bytes)
      {
        m_byteData = bytes;
        m_exists = true;
      }
      else if (obj is string str)
      {
        m_stringData = str;
        m_exists = true;
      }
    }

    /// <inheritdoc />
    public Stream Open()
    {
      return Open(ResourceFileMode.Open, ResourceFileAccess.Read, ResourceFileShare.Read);
    }

    /// <inheritdoc />
    /// <exception cref="Tesla.Content.TeslaContentException">Thrown if the parent repository is not opened or if any of the file mode is non-read only.</exception>
    public Stream Open(ResourceFileMode fileMode)
    {
      return Open(fileMode, ResourceFileAccess.Read, ResourceFileShare.Read);
    }

    /// <inheritdoc />
    /// <exception cref="Tesla.Content.TeslaContentException">Thrown if the parent repository is not opened or if any of the file mode or access mode are non-read only.</exception>
    public Stream Open(ResourceFileMode fileMode, ResourceFileAccess accessMode)
    {
      return Open(fileMode, accessMode, ResourceFileShare.Read);
    }

    /// <inheritdoc />
    /// <exception cref="Tesla.Content.TeslaContentException">Thrown if the parent repository is not opened or if any of the file mode or access modes are non-read only.</exception>
    public Stream Open(ResourceFileMode fileMode, ResourceFileAccess accessMode, ResourceFileShare fileShare)
    {
      TeslaContentException.ThrowIfNotOpened(m_repository);

      if (fileMode != ResourceFileMode.Open)
        ThrowReadOnly();

      if (accessMode != ResourceFileAccess.Read)
        ThrowReadOnly();

      if (!m_exists)
        throw new TeslaContentException(StringLocalizer.Instance.GetLocalizedString("EmbeddedResourceDoesNotExist"));

      if (m_byteData is not null)
      {
        return new MemoryStream(m_byteData);
      }
      else
      {
        MemoryStream ms = new MemoryStream();
        StreamWriter writer = new StreamWriter(ms);
        writer.Write(m_stringData);
        writer.Flush();
        ms.Position = 0;
        return ms;
      }
    }

    /// <summary>
    /// Embedded resource files cannot be written to.
    /// </summary>
    /// <returns>Resource stream</returns>
    /// <exception cref="Tesla.Content.TeslaContentException">Thrown always since writing is unsupported.</exception>
    public Stream OpenWrite()
    {
      throw new TeslaContentException(StringLocalizer.Instance.GetLocalizedString("EmbeddedResourceFileReadOnly"));
    }

    /// <inheritdoc />
    public Stream OpenRead()
    {
      return Open(ResourceFileMode.Open, ResourceFileAccess.Read, ResourceFileShare.Read);
    }

    /// <summary>
    /// Embedded resource files cannot be created.
    /// </summary>
    /// <returns>Resource stream</returns>
    /// <exception cref="Tesla.Content.TeslaContentException">Thrown always since writing is unsupported.</exception>
    public Stream Create()
    {
      throw new TeslaContentException(StringLocalizer.Instance.GetLocalizedString("EmbeddedResourceFileReadOnly"));
    }

    /// <summary>
    /// Embedded resource files cannot be deleted.
    /// </summary>
    /// <exception cref="Tesla.Content.TeslaContentException">Thrown always since deletion is unsupported.</exception>
    public void Delete()
    {
      ThrowReadOnly();
    }

    /// <inheritdoc />
    /// <exception cref="Tesla.Content.TeslaContentException">Thrown if the parent repository is not opened.</exception>
    public void Refresh()
    {
      TeslaContentException.ThrowIfNotOpened(m_repository);
    }

    [DoesNotReturn]
    private void ThrowReadOnly()
    {
      throw new TeslaContentException(StringLocalizer.Instance.GetLocalizedString("EmbeddedResourceFileReadOnly"));
    }
  }
}
