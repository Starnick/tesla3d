﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Graphics;

namespace Tesla.Content
{
  /// <summary>
  /// Defines a set of common parameters that configures how shader files are compiled.
  /// </summary>
  [SavableVersion(1)]
  public class EffectImporterParameters : ImporterParameters
  {
    /// <summary>
    /// Gets or sets the TEFX compression mode. Default value is to use compression.
    /// </summary>
    public DataCompressionMode CompressionMode { get; set; }

    /// <summary>
    /// Gets or sets shader macros. A null array is valid.
    /// </summary>
    public ShaderMacro[] ShaderMacros { get; set; }

    /// <summary>
    /// Gets or sets additional include directories. A null array is valid.
    /// </summary>
    public String[] IncludeDirectories { get; set; }

    /// <summary>
    /// Gets or sets compile flags. Not all options may be available for all platforms. Default value is <see cref="ShaderCompileFlags.None"/>.
    /// </summary>
    public ShaderCompileFlags CompileFlags { get; set; }

    /// <summary>
    /// Gets or sets additional, platform specific compile flags.
    /// </summary>
    public int AdditionalCompileFlags { get; set; }

    /// <summary>
    /// Constructs a new instance of the <see cref="EffectImporterParameters"/> class.
    /// </summary>
    public EffectImporterParameters()
    {
      CompressionMode = DataCompressionMode.Gzip;
      ShaderMacros = null;
      IncludeDirectories = null;
      CompileFlags = ShaderCompileFlags.None;
      AdditionalCompileFlags = 0;
    }

    /// <summary>
    /// Copies the importer parameters from the specified instance.
    /// </summary>
    /// <param name="parameters">Importer parameter instance to copy from.</param>
    public override void Set(ImporterParameters parameters)
    {
      base.Set(parameters);

      EffectImporterParameters effectParams = parameters as EffectImporterParameters;

      if (effectParams == null)
        return;

      CompressionMode = effectParams.CompressionMode;
      ShaderMacros = (effectParams.ShaderMacros == null) ? null : effectParams.ShaderMacros.Clone() as ShaderMacro[];
      IncludeDirectories = (effectParams.IncludeDirectories == null) ? null : effectParams.IncludeDirectories.Clone() as String[];
      CompileFlags = effectParams.CompileFlags;
    }

    /// <summary>
    /// Writes the object data to the output.
    /// </summary>
    /// <param name="output">Savable writer</param>
    public override void Write(ISavableWriter output)
    {
      base.Write(output);

      output.WriteEnum<DataCompressionMode>("CompressionMode", CompressionMode);
      output.Write<ShaderMacro>("ShaderMacros", ShaderMacros);
      output.Write("IncludeDirectories", IncludeDirectories);
      output.WriteEnum<ShaderCompileFlags>("CompileFlags", CompileFlags);
      output.Write("AdditionalCompileFlags", AdditionalCompileFlags);
    }

    /// <summary>
    /// Reads the object data from the input.
    /// </summary>
    /// <param name="input">Savable reader</param>
    public override void Read(ISavableReader input)
    {
      base.Read(input);

      CompressionMode = input.ReadEnum<DataCompressionMode>("CompressionMode");
      ShaderMacros = input.ReadArray<ShaderMacro>("ShaderMacros");
      IncludeDirectories = input.ReadStringArray("IncludeDirectories");
      CompileFlags = input.ReadEnum<ShaderCompileFlags>("CompileFlags");
      AdditionalCompileFlags = input.ReadInt32("AdditionalCompileFlags");
    }
  }
}
