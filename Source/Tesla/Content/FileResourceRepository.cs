﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

#nullable enable

namespace Tesla.Content
{
  /// <summary>
  /// Represents a file resource repository - a simple directory.
  /// </summary>
  [DebuggerDisplay("RootPath = {RootPath}")]
  public class FileResourceRepository : IResourceRepository
  {
    private static IResourceFile NullResource = new NullResource();

    private DirectoryInfo m_directoryInfo;
    private bool m_isOpen;

    /// <inheritdoc />
    public string RootPath { get { return m_directoryInfo.FullName; } }

    /// <inheritdoc />
    public bool IsOpen { get { return m_isOpen; } }

    /// <inheritdoc />
    public bool Exists 
    { 
      get 
      {
        if (!m_isOpen)
          m_directoryInfo.Refresh();

        return m_directoryInfo.Exists; 
      } 
    }

    /// <inheritdoc />
    public bool IsReadOnly { get { return false; } }

    /// <inheritdoc />
    public DateTime CreationTime { get { return m_directoryInfo.CreationTimeUtc; } }

    /// <inheritdoc />
    public DateTime LastAccessTime 
    { 
      get 
      {
        m_directoryInfo.Refresh();
        return m_directoryInfo.LastAccessTimeUtc; 
      } 
    }

    /// <inheritdoc />
    public DateTime LastWriteTime 
    { 
      get
      {
        m_directoryInfo.Refresh();
        return m_directoryInfo.LastWriteTimeUtc; 
      } 
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="FileResourceRepository"/> class. Uses the default location of
    /// the application as the root path.
    /// </summary>
    public FileResourceRepository() : this(ContentHelper.AppLocation) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="FileResourceRepository"/> class.
    /// </summary>
    /// <param name="pathToDirectory">The directory path. If it is not a rooted path, it is assumed to be relative to the app location.</param>
    /// <exception cref="TeslaContentException">Thrown if the path is invalid.</exception>
    public FileResourceRepository(string pathToDirectory)
    {
      try
      {
        //If not rooted, we assume its relative to the app location.
        if (!Path.IsPathRooted(pathToDirectory))
        {
          m_directoryInfo = new DirectoryInfo(Path.Combine(ContentHelper.AppLocation, pathToDirectory));
        }
        else
        {
          m_directoryInfo = new DirectoryInfo(pathToDirectory);
        }
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw new TeslaContentException(e.Message, e);
      }

      m_isOpen = false;
    }

    /// <summary>
    /// Creates a new <see cref="FileResourceRepository"/> that is already marked opened.
    /// </summary>
    /// <param name="pathToDirectory">The directory path. If it is not a rooted path, it is assumed to be relative to the app location.</param>
    /// <returns>Resource repository.</returns>
    /// <exception cref="TeslaContentException">Thrown if the path is invalid.</exception>
    public static FileResourceRepository CreateOpen(string pathToDirectory)
    {
      // Will throw if the above directory path is invalid
      FileResourceRepository repo = new FileResourceRepository(pathToDirectory);
      repo.OpenConnection();

      return repo;
    }

    /// <inheritdoc />
    /// <exception cref="TeslaContentException">Thrown if the repository is already opened.</exception>
    public void OpenConnection()
    {
      OpenConnection(ResourceFileMode.Open, ResourceFileShare.Read);
    }

    /// <inheritdoc />
    /// <exception cref="TeslaContentException">Thrown if the repository is already opened or depending on the file mode, e.g. CreateNew specified but the repository already exists.</exception>
    public void OpenConnection(ResourceFileMode mode)
    {
      OpenConnection(mode, ResourceFileShare.None);
    }

    /// <inheritdoc />
    /// <exception cref="TeslaContentException">Thrown if the repository is already opened or depending on the file mode, e.g. CreateNew specified but the repository already exists.</exception>
    public void OpenConnection(ResourceFileMode mode, ResourceFileShare shareMode)
    {
      TeslaContentException.ThrowIfAlreadyOpened(this);

      m_directoryInfo.Refresh();
      bool dirExists = m_directoryInfo.Exists;

      //Throw exceptions to follow expected behavior if the directory is there or not
      switch (mode)
      {
        case ResourceFileMode.CreateNew:
          if (dirExists)
            throw new TeslaContentException(nameof(mode), StringLocalizer.Instance.GetLocalizedString("ResourceRepositoryExists"));
          break;
        case ResourceFileMode.Open:
          if (!dirExists)
            throw new TeslaContentException(nameof(mode), StringLocalizer.Instance.GetLocalizedString("ResourceRepositoryDoesNotExist"));
          break;
        case ResourceFileMode.OpenOrCreate:
        case ResourceFileMode.Create:
          if (!dirExists)
          {
            try
            {
              m_directoryInfo.Create();
              m_directoryInfo.Refresh();
            }
            catch (IOException e)
            {
              EngineLog.LogException(LogLevel.Error, e);
              throw new TeslaContentException(e.Message, e);
            }
          }
          break;
      }

      m_isOpen = true;
    }

    /// <inheritdoc />
    /// <exception cref="TeslaContentException">Thrown if the repository is not open.</exception>
    public void CloseConnection()
    {
      TeslaContentException.ThrowIfNotOpened(this);

      m_isOpen = false;
    }

    /// <inheritdoc />
    /// <exception cref="TeslaContentException">Thrown if the repository is not open.</exception>
    public IResourceFile GetResourceFile(string resourceName)
    {
      TeslaContentException.ThrowIfNotOpened(this);

      if (String.IsNullOrEmpty(resourceName))
        return NullResource;

      if (!Path.IsPathRooted(resourceName))
        resourceName = GetFullPath(resourceName);

      return new FileResourceFile(resourceName, this);
    }

    /// <inheritdoc />
    /// <exception cref="TeslaContentException">Thrown if the repository is not open or if there was an error in getting the resource path.</exception>
    public IResourceFile GetResourceFileRelativeTo(string resourceName, IResourceFile resource)
    {
      TeslaContentException.ThrowIfNotOpened(this);

      if (resource is null || !(resource is FileResourceFile) || String.IsNullOrEmpty(resourceName))
        return NullResource;

      try
      {
        string dir = Path.GetDirectoryName(resource.FullName)!;
        return new FileResourceFile(Path.GetFullPath(Path.Combine(dir, resourceName)), this);
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw new TeslaContentException(e.Message, e);
      }
    }

    /// <inheritdoc />
    /// <exception cref="TeslaContentException">Thrown if the repository is not open or if there was an error in getting the full path.</exception>
    public bool ContainsResource(string resourceName)
    {
      TeslaContentException.ThrowIfNotOpened(this);

      if (String.IsNullOrEmpty(resourceName))
        return false;

      return File.Exists(GetFullPath(resourceName));
    }

    /// <inheritdoc />
    /// <exception cref="TeslaContentException">Thrown if the repository is not open or if there was an error in getting the full path.</exception>
    public string GetFullPath(string resourceName)
    {
      TeslaContentException.ThrowIfNotOpened(this);

      if (String.IsNullOrEmpty(resourceName))
        return String.Empty;

      try
      {
        return Path.GetFullPath(Path.Combine(RootPath, resourceName));
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw new TeslaContentException(e.Message, e);
      }
    }

    /// <inheritdoc />
    /// <exception cref="TeslaContentException">Thrown if the repository is not open or if there was an error in getting the full path.</exception>
    public string GetFullPathRelativeTo(string resourceName, IResourceFile resource)
    {
      TeslaContentException.ThrowIfNotOpened(this);

      if (resource is null || !(resource is FileResourceFile) || String.IsNullOrEmpty(resourceName))
        return String.Empty;

      try
      {
        string dir = Path.GetDirectoryName(resource.FullName)!;
        return Path.GetFullPath(Path.Combine(dir, resourceName));
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw new TeslaContentException(e.Message, e);
      }
    }

    /// <inheritdoc />
    /// <exception cref="TeslaContentException">Thrown if the repository is not open or if there was an error in enumerating the repository's files.</exception>
    public IEnumerable<IResourceFile> EnumerateResourceFiles(bool recursive = true, string? searchPattern = null)
    {
      TeslaContentException.ThrowIfNotOpened(this);

      SearchOption searchOption = (recursive) ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly;
      IEnumerable<string>? fileNames = null;

      try
      {
        fileNames = Directory.EnumerateFiles(RootPath, String.IsNullOrEmpty(searchPattern) ? "*" : searchPattern, searchOption);
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw new TeslaContentException(e.Message, e);
      }

      if (fileNames is not null)
      {
        foreach (string fileName in fileNames)
          yield return new FileResourceFile(fileName, this);
      }
    }

    /// <inheritdoc />
    /// <exception cref="TeslaContentException">Thrown if the repository is not open or if there was an error in refreshing the directory information.</exception>
    public void Refresh()
    {
      TeslaContentException.ThrowIfNotOpened(this);

      try
      {
        m_directoryInfo.Refresh();
      }
      catch (IOException e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw new TeslaContentException(e.Message, e);
      }
    }
  }
}
