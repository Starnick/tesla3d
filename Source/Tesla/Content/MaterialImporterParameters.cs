﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;

namespace Tesla.Content
{
    /// <summary>
    /// Defines a set of common parameters for material importing whether it is from a TEM or TEMD script.
    /// </summary>
    [SavableVersion(1)]
    public sealed class MaterialImporterParameters : ImporterParameters
    {
        private String m_overrideTexturePath;
        private TextureImporterParameters m_textureParameters;
        private EffectImporterParameters m_effectParameters;

        /// <summary>
        /// Gets or sets if materials defined multiple times in the same material definition script will all be unique or be shared. Default
        /// value is false.
        /// </summary>
        public bool UseSharedInstances { get; set; }

        /// <summary>
        /// Gets or sets the override texture path to search for textures to load. In most cases the material script
        /// will define the full path to the texture.
        /// </summary>
        public String OverrideTexturePath
        {
            get
            {
                return m_overrideTexturePath;
            }
            set
            {
                if(value == null)
                    value = String.Empty;

                m_overrideTexturePath = value;
            }
        }

        /// <summary>
        /// Gets the texture parameters used for importing texture resources.
        /// </summary>
        public TextureImporterParameters TextureParameters
        {
            get
            {
                return m_textureParameters;
            }
        }

        /// <summary>
        /// Gets the compile parameters used for importing effect resources.
        /// </summary>
        public EffectImporterParameters EffectParameters
        {
            get
            {
                return m_effectParameters;
            }
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="MaterialImporterParameters"/> class.
        /// </summary>
        public MaterialImporterParameters()
        {
            UseSharedInstances = false;

            m_overrideTexturePath = String.Empty;
            m_textureParameters = new TextureImporterParameters();
            m_effectParameters = new EffectImporterParameters();
        }

        /// <summary>
        /// Copies the importer parameters from the specified instance.
        /// </summary>
        /// <param name="parameters">Importer parameter instance to copy from.</param>
        public override void Set(ImporterParameters parameters)
        {
            base.Set(parameters);

            MaterialImporterParameters materialParams = parameters as MaterialImporterParameters;

            if(materialParams == null)
                return;

            m_textureParameters.Set(materialParams.m_textureParameters);
        }

        /// <summary>
        /// Writes the object data to the output.
        /// </summary>
        /// <param name="output">Savable writer</param>
        public override void Write(ISavableWriter output)
        {
            base.Write(output);

            output.Write("UseSharedInstances", UseSharedInstances);
            output.Write("OverrideTexturePath", m_overrideTexturePath);
            output.WriteSavable<TextureImporterParameters>("TextureParameters", m_textureParameters);
            output.WriteSavable<EffectImporterParameters>("EffectParameters", m_effectParameters);
        }

        /// <summary>
        /// Reads the object data from the input.
        /// </summary>
        /// <param name="input">Savable reader</param>
        public override void Read(ISavableReader input)
        {
            base.Read(input);

            UseSharedInstances = input.ReadBoolean("UseSharedInstances");

            m_overrideTexturePath = input.ReadString("OverrideTexturePath");
            if(m_overrideTexturePath == null)
                m_overrideTexturePath = String.Empty;

            m_textureParameters = input.ReadSavable<TextureImporterParameters>("TextureParameters");
            m_effectParameters = input.ReadSavable<EffectImporterParameters>("EffectParameters");
        }
    }
}
