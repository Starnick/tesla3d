﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics;
using System.IO;

#nullable enable

namespace Tesla.Content
{
  /// <summary>
  /// A resource file that can access files on a disk.
  /// </summary>
  [DebuggerDisplay("Name = {Name}, Exists = {Exists}")]
  public class FileResourceFile : IResourceFile
  {
    private FileInfo m_fileInfo;
    private IResourceRepository m_parent;

    /// <inheritdoc />
    public string Name { get { return Path.GetFileNameWithoutExtension(m_fileInfo.Name); } }

    /// <inheritdoc />
    public string NameWithExtension { get { return m_fileInfo.Name; } }

    /// <inheritdoc />
    public string Extension { get { return m_fileInfo.Extension; } }

    /// <inheritdoc />
    public string FullName { get { return m_fileInfo.FullName; } }

    /// <inheritdoc />
    public string RelativeName { get { return Path.GetRelativePath(m_parent.RootPath, m_fileInfo.FullName); } }

    /// <inheritdoc />
    public bool IsReadOnly { get { return m_fileInfo.IsReadOnly; } }

    /// <inheritdoc />
    public DateTime CreationTime { get { return m_fileInfo.CreationTimeUtc; } }

    /// <inheritdoc />
    public DateTime LastAccessTime { get { return m_fileInfo.LastAccessTimeUtc; } }

    /// <inheritdoc />
    public DateTime LastWriteTime { get { return m_fileInfo.LastWriteTimeUtc; } }

    /// <inheritdoc />
    public bool Exists { get { return m_fileInfo.Exists; } }

    /// <inheritdoc />
    public IResourceRepository Repository { get { return m_parent; } }

    /// <summary>
    /// Constructs a new instance of the <see cref="FileResourceFile"/> class.
    /// </summary>
    /// <param name="fullPathToFile">The full path to file.</param>
    /// <param name="parent">Parent repository</param>
    internal FileResourceFile(string fullPathToFile, IResourceRepository parent)
    {
      m_fileInfo = new FileInfo(fullPathToFile);
      m_parent = parent;
    }

    /// <summary>
    /// Creates an orphaned <see cref="FileResourceFile"/> with a parent repository that is set to the file's directory.
    /// </summary>
    /// <param name="fullPathToFile">Full path to the file.</param>
    /// <returns>Resource file.</returns>
    /// <exception cref="TeslaContentException">Thrown if the path is invalid.</exception>
    public static IResourceFile CreateOrphan(string fullPathToFile)
    {
      string dir = Path.GetDirectoryName(fullPathToFile)!;

      // Will throw if the above directory path is invalid
      FileResourceRepository repo = new FileResourceRepository(dir);
      repo.OpenConnection();

      return repo.GetResourceFile(Path.GetFileName(fullPathToFile));
    }

    /// <inheritdoc />
    /// <exception cref="TeslaContentException">Thrown if the parent repository is not opened or if there was an error in opening the file.</exception>
    public Stream Open()
    {
      return Open(ResourceFileMode.Open, ResourceFileAccess.Read, ResourceFileShare.Read);
    }

    /// <inheritdoc />
    /// <exception cref="TeslaContentException">Thrown if the parent repository is not opened or if there was an error in opening the file.</exception>
    public Stream Open(ResourceFileMode fileMode)
    {
      return Open(fileMode, ResourceFileAccess.Read, ResourceFileShare.Read);
    }

    /// <inheritdoc />
    /// <exception cref="TeslaContentException">Thrown if the parent repository is not opened or if there was an error in opening the file.</exception>
    public Stream Open(ResourceFileMode fileMode, ResourceFileAccess accessMode)
    {
      return Open(fileMode, accessMode, ResourceFileShare.Read);
    }

    /// <inheritdoc />
    /// <exception cref="TeslaContentException">Thrown if the parent repository is not opened or if there was an error in opening the file.</exception>
    public Stream Open(ResourceFileMode fileMode, ResourceFileAccess accessMode, ResourceFileShare fileShare)
    {
      TeslaContentException.ThrowIfNotOpened(m_parent);

      try
      {
        FileStream fs = m_fileInfo.Open((FileMode) fileMode, (FileAccess) accessMode, (FileShare) fileShare);
        m_fileInfo.Refresh();

        return fs;
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw new TeslaContentException(e.Message, e);
      }
    }

    /// <inheritdoc />
    /// <exception cref="TeslaContentException">Thrown if the parent repository is not opened or if there was an error in opening the file.</exception>
    public Stream OpenWrite()
    {
      return Open(ResourceFileMode.Create, ResourceFileAccess.Write, ResourceFileShare.None);
    }

    /// <inheritdoc />
    /// <exception cref="TeslaContentException">Thrown if the parent repository is not opened or if there was an error in opening the file.</exception>
    public Stream OpenRead()
    {
      return Open(ResourceFileMode.Open, ResourceFileAccess.Read, ResourceFileShare.Read);
    }

    /// <inheritdoc />
    /// <exception cref="TeslaContentException">Thrown if the parent repository is not opened or if there was an error in creating the file.</exception>
    public Stream Create()
    {
      TeslaContentException.ThrowIfNotOpened(m_parent);

      try
      {
        return m_fileInfo.Create();
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw new TeslaContentException(e.Message, e);
      }
    }

    /// <inheritdoc />
    /// <exception cref="TeslaContentException">Thrown if the parent repository is not opened or if there was an error in deleting the file.</exception> 
    public void Delete()
    {
      TeslaContentException.ThrowIfNotOpened(m_parent);

      try
      {
        m_fileInfo.Delete();
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw new TeslaContentException(e.Message, e);
      }
    }

    /// <inheritdoc />
    /// <exception cref="TeslaContentException">Thrown if the parent repository is not opened or if there was an error in refreshing the file.</exception> 
    public void Refresh()
    {
      TeslaContentException.ThrowIfNotOpened(m_parent);

      try
      {
        m_fileInfo.Refresh();
      }
      catch (Exception e)
      {
        EngineLog.LogException(LogLevel.Error, e);
        throw new TeslaContentException(e.Message, e);
      }
    }
  }
}
