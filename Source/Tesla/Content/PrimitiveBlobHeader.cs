﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;

namespace Tesla.Content
{
  /// <summary>
  /// Represents the metadata that is written out in a <see cref="IPrimitiveWriter2"/> for blob data. Blobs
  /// are treated as binary data, therefore can be reintrepreted to a different element type.
  /// </summary>
  public readonly struct PrimitiveBlobHeader
  {
    /// <summary>
    /// Number of elements in the blob of the saved type.
    /// </summary>
    public readonly int ElementCount;

    /// <summary>
    /// Size of the element type in bytes at the time of serialization. This an be used to validate if the type has changed.
    /// </summary>
    public readonly int ElementSizeInBytes;

    /// <summary>
    /// Type information of the element.
    /// </summary>
    public readonly Type ElementType;

    /// <summary>
    /// Gets the total blob size in bytes.
    /// </summary>
    public int BlobSizeInBytes
    {
      get
      {
        return ElementCount * ElementSizeInBytes;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="PrimitiveBlobHeader" /> struct.
    /// </summary>
    /// <param name="elemCount">Number of elements in the blob.</param>
    /// <param name="elemSize">Size of an element in bytes.</param>
    /// <param name="elemType">Element type.</param>
    public PrimitiveBlobHeader(int elemCount, int elemSize, Type elemType)
    {
      ElementCount = Math.Max(0, elemCount);
      ElementSizeInBytes = Math.Max(0, elemSize);
      ElementType = elemType;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="PrimitiveBlobHeader"/> struct.
    /// </summary>
    /// <param name="db">Data buffer holding the blob contents.</param>
    public PrimitiveBlobHeader(IReadOnlyDataBuffer db)
    {
      ElementCount = db.Length;
      ElementSizeInBytes = db.ElementSizeInBytes;
      ElementType = db.ElementType;
    }

    /// <summary>
    /// Gets the number of elements in the blob when using the specified element type.
    /// </summary>
    /// <typeparam name="T">Element type.</typeparam>
    /// <returns>Number of elements of the type in the blob, this may be less than the total blob size.</returns>
    public unsafe int GetElementCount<T>() where T : unmanaged
    {
      return (int) Math.Floor((double) (BlobSizeInBytes / sizeof(T)));
    }

    /// <summary>
    /// Creates a data buffer using the blob properties.
    /// </summary>
    /// <param name="allocatorStrategy">Optional memory allocator strategy.</param>
    /// <returns>Data buffer of size and type as the blob.</returns>
    public IDataBuffer CreateDataBuffer(MemoryAllocatorStrategy allocatorStrategy = MemoryAllocatorStrategy.Default)
    {
      return DataBuffer.Create(ElementCount, ElementType, allocatorStrategy);
    }

    /// <summary>
    /// Creates a data buffer using the blob properties with possibly a reintrepreted type.
    /// </summary>
    /// <typeparam name="T">Element type in buffer.</typeparam>
    /// <param name="allocator">Optional memory allocator. If null, default memory allocation strategy is used.</param>
    /// <returns>Data buffer of size as the blob.</returns>
    public DataBuffer<T> CreateDataBuffer<T>(IMemoryAllocator<T> allocator = null) where T : unmanaged
    {
      return DataBuffer.Create<T>(GetElementCount<T>(), allocator);
    }

    /// <summary>
    /// Creates a data buffer using the blob properties.
    /// </summary>
    /// <param name="allocatorStrategy">Memory allocator strategy.</param>
    /// <returns>Data buffer of size and type as the blob.</returns>
    public DataBuffer<T> CreateDataBuffer<T>(MemoryAllocatorStrategy allocatorStrategy) where T : unmanaged
    {
      return DataBuffer.Create<T>(GetElementCount<T>(), allocatorStrategy);
    }
  }
}
