﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.IO;

#nullable enable

namespace Tesla.Content
{
  /// <summary>
  /// Represents a null resource.
  /// </summary>
  public sealed class NullResource : IResourceFile
  {
    /// <inheritdoc />
    public String Name { get { return String.Empty; } }

    /// <inheritdoc />
    public String NameWithExtension { get { return String.Empty; } }

    /// <inheritdoc />
    public String Extension { get { return String.Empty; } }

    /// <inheritdoc />
    public String FullName { get { return String.Empty; } }

    /// <inheritdoc />
    public string RelativeName { get { return String.Empty; } }

    /// <inheritdoc />
    public bool Exists { get { return false; } }

    /// <inheritdoc />
    public bool IsReadOnly { get { return true; } }

    /// <inheritdoc />
    public IResourceRepository Repository { get { return null!; } }

    /// <inheritdoc />
    public DateTime CreationTime { get { return new DateTime(); } }

    /// <inheritdoc />
    public DateTime LastAccessTime { get { return new DateTime(); } }

    /// <inheritdoc />
    public DateTime LastWriteTime { get { return new DateTime(); } }

    /// <summary>
    /// Constructs a new instance of the <see cref="NullResource"/> class.
    /// </summary>
    public NullResource() { }

    /// <summary>
    /// A null resource cannot be opened.
    /// </summary>
    /// <returns>The resource stream.</returns>
    /// <exception cref="System.IO.IOException">Always gets thrown, because this is a null resource.</exception>
    public Stream Open()
    {
      throw new IOException(StringLocalizer.Instance.GetLocalizedString("FileDoesNotExist"));
    }

    /// <summary>
    /// A null resource cannot be opened.
    /// </summary>
    /// <param name="fileMode">Specifies the mode in which to open the file.</param>
    /// <returns>Resource stream</returns>
    /// <exception cref="System.IO.IOException">Always gets thrown, because this is a null resource.</exception>
    public Stream Open(ResourceFileMode fileMode)
    {
      throw new IOException(StringLocalizer.Instance.GetLocalizedString("FileDoesNotExist"));
    }

    /// <summary>
    /// A null resource cannot be opened.
    /// </summary>
    /// <param name="fileMode">Specifies the mode in which to open the file.</param>
    /// <param name="accessMode">Specifies the read or write access of the file.</param>
    /// <returns>Resource stream</returns>
    /// <exception cref="System.IO.IOException">Always gets thrown, because this is a null resource.</exception>
    public Stream Open(ResourceFileMode fileMode, ResourceFileAccess accessMode)
    {
      throw new IOException(StringLocalizer.Instance.GetLocalizedString("FileDoesNotExist"));
    }

    /// <summary>
    /// A null resource cannot be opened.
    /// </summary>
    /// <param name="fileMode">Specifies the mode in which to open the file.</param>
    /// <param name="accessMode">Specifies the read or write access of the file.</param>
    /// <param name="fileShare">Specifies how the same file should be shared with other resource streams</param>
    /// <returns>Resource stream</returns>
    /// <exception cref="System.IO.IOException">Always gets thrown, because this is a null resource.</exception>
    public Stream Open(ResourceFileMode fileMode, ResourceFileAccess accessMode, ResourceFileShare fileShare)
    {
      throw new IOException(StringLocalizer.Instance.GetLocalizedString("FileDoesNotExist"));
    }

    /// <summary>
    /// A null resource cannot be opened.
    /// </summary>
    /// <returns>Resource stream</returns>
    /// <exception cref="System.IO.IOException">Always gets thrown, because this is a null resource.</exception>
    public Stream OpenWrite()
    {
      throw new IOException(StringLocalizer.Instance.GetLocalizedString("FileDoesNotExist"));
    }

    /// <summary>
    /// A null resource cannot be opened.
    /// </summary>
    /// <returns>Resource stream</returns>
    /// <exception cref="System.IO.IOException">Always gets thrown, because this is a null resource.</exception>
    public Stream OpenRead()
    {
      throw new IOException(StringLocalizer.Instance.GetLocalizedString("FileDoesNotExist"));
    }

    /// <summary>
    /// A null resource cannot be opened.
    /// </summary>
    /// <returns>Resource stream</returns>
    /// <exception cref="System.IO.IOException">Always gets thrown, because this is a null resource.</exception>
    public Stream Create()
    {
      throw new IOException(StringLocalizer.Instance.GetLocalizedString("FileDoesNotExist"));
    }

    /// <inheritdoc />
    public void Delete() { }

    /// <inheritdoc />
    public void Refresh() { }
  }
}
