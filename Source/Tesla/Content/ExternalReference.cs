﻿/*
* Copyright (c) 2010-2024 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;

#nullable enable

namespace Tesla.Content
{
  /// <summary>
  /// Represents a reference to a resource that is external to some other resource.
  /// </summary>
  public class ExternalReference
  {
    private static readonly ExternalReference s_nullReference = new ExternalReference();

    private Type m_targetType;
    private string m_resourcePath;

    /// <summary>
    /// Gets a null external reference.
    /// </summary>
    public static ExternalReference NullReference {  get { return s_nullReference; } }

    /// <summary>
    /// Gets or sets the runtime target type of the reference.
    /// </summary>
    public Type TargetType { get { return m_targetType; } }

    /// <summary>
    /// Gets or sets the resource path which should include the resource extension.
    /// </summary>
    public string ResourcePath { get { return m_resourcePath; } }

    /// <summary>
    /// Constructs a new instance of the <see cref="ExternalReference"/> class.
    /// </summary>
    /// <param name="targetType">Runtime target type</param>
    /// <param name="resourcePath">Path to the external resource file.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the target type or the resource path is null or empty.</exception>
    public ExternalReference(Type targetType, string resourcePath)
    {
      ArgumentNullException.ThrowIfNull(targetType, StringLocalizer.Instance.GetLocalizedString("TypeIsNull"));
      ArgumentNullException.ThrowIfNullOrEmpty(resourcePath, StringLocalizer.Instance.GetLocalizedString("ResourcePathNull"));

      m_resourcePath = resourcePath;
      m_targetType = targetType;
    }

    // Default ctor
    private ExternalReference()
    {
      m_targetType = typeof(ISavable);
      m_resourcePath = String.Empty;
    }
  }
}
