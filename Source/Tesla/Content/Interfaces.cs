﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Reflection.Metadata;
using System.Threading.Tasks;
using Tesla.Graphics;

#nullable enable

namespace Tesla.Content
{
  /// <summary>
  /// Delegate for generating a custom resource name from a given savable object. This should not include any resource extension.
  /// </summary>
  /// <param name="handler">External handler that is handling this resource.</param>
  /// <param name="savable">Savable to generate a resource name for</param>
  /// <returns>Generated resource name</returns>
  public delegate String GetResourceNameDelegate(IExternalReferenceHandler handler, ISavable savable);

  #region Resource loading interfaces

  /// <summary>
  /// A handler that returns a "default" piece of content to serve as a placeholder for content that could not be located or loaded.
  /// </summary>
  public interface IMissingContentHandler
  {
    /// <summary>
    /// Gets the content target type this handler serves.
    /// </summary>
    Type TargetType { get; }

    /// <summary>
    /// Gets a place holder piece of content. The type must be the same as the target type or
    /// can be casted to it.
    /// </summary>
    /// <typeparam name="Content">Content type</typeparam>
    /// <returns>The placeholder content</returns>
    Content GetPlaceHolderContent<Content>() where Content : class;
  }

  /// <summary>
  /// A content item that supports conversion to another content item.
  /// </summary>
  public interface IContentCastable
  {
    /// <summary>
    /// Attempts to cast the content item to another type.
    /// </summary>
    /// <param name="targetType">Type to cast to.</param>
    /// <param name="subresourceName">Optional subresource name.</param>
    /// <returns>Casted type or null if the type could not be converted.</returns>
    object? CastTo(Type targetType, string subresourceName);
  }

  /// <summary>
  /// An object that represents a standard content item from a library.
  /// </summary>
  public interface IStandardLibraryContent
  {
    /// <summary>
    /// Gets the name of the content this instance represents. If <see cref="IsStandardContent"/> is false, then this returns an empty string.
    /// </summary>
    string StandardContentName { get; }

    /// <summary>
    /// Gets if the instance represents standard library content or not.
    /// </summary>
    bool IsStandardContent { get; }

    /// <summary>
    /// Gets a consistent hash code that identifies the content item. If it is not standard content, each instance should have a unique hash, if the instance is
    /// standard content, each instance should have the same hash code. This might differ from .NET's hash code and is only used to identify two instances that
    /// represent the same data (there may be situations where we want to differentiate the two instances, so we don't rely on the .NET's get hash code function).
    /// </summary>
    /// <returns>32-bit hash code.</returns>
    int GetContentHashCode();
  }

  /// <summary>
  /// A resource importer capable of loading a piece of content from a resource source. Since resources can be asynchronously loaded,
  /// importers should be designed to be stateless. Importers should be capable of handling content types that either match the target type or types that
  /// can be assigned to it (e.g. an ISavable importer should be able to handle ALL types that implement the ISavable interface).
  /// </summary>
  public interface IResourceImporter
  {
    /// <summary>
    /// Gets the format extensions that this importer supports.
    /// </summary>
    IEnumerable<string> Extensions { get; }

    /// <summary>
    /// Gets the content target type this importer serves.
    /// </summary>
    Type TargetType { get; }

    /// <summary>
    /// Loads content from the specified resource.
    /// </summary>
    /// <param name="resourceFile">Resource file to read from.</param>
    /// <param name="contentManager">Calling content manager.</param>
    /// <param name="parameters">Optional loading parameters.</param>
    /// <returns>The loaded object or null if it could not be loaded.</returns>
    object? Load(IResourceFile resourceFile, ContentManager contentManager, ImporterParameters parameters);

    /// <summary>
    /// Loads content from the specified stream.
    /// </summary>
    /// <param name="input">Stream to read from.</param>
    /// <param name="contentManager">Calling content manager.</param>
    /// <param name="parameters">Optional loading parameters.</param>
    /// <returns>The loaded object or null if it could not be loaded.</returns>
    object? Load(Stream input, ContentManager contentManager, ImporterParameters parameters);

    /// <summary>
    /// Returns true if the specified type can be handled by this importer, false otherwise.
    /// </summary>
    /// <param name="contentType">Content type to be loaded.</param>
    /// <returns>True if the type can be loaded by this importer, false otherwise.</returns>
    bool CanLoadType(Type contentType);
  }

  /// <summary>
  /// A resource exporter capable of saving a piece of content to some output. Since content can be saved asynchronously, the exporter should
  /// be designed to be stateless. Exporters should be capable of handling content types that either match the target type or types that can be assignable
  /// to it (e.g. an ISavable exporter should be able to handle ALL types that implement the ISavable interface).
  /// </summary>
  public interface IResourceExporter
  {
    /// <summary>
    /// Gets the format extension that this exporter supports.
    /// </summary>
    string Extension { get; }

    /// <summary>
    /// Gets the content target type this exporter serves.
    /// </summary>
    Type TargetType { get; }

    /// <summary>
    /// Writes the specified content to a resource file.
    /// </summary>
    /// <param name="resourceFile">Resource file to write to.</param>
    /// <param name="content">Content to write.</param>
    void Save(IResourceFile resourceFile, object content);

    /// <summary>
    /// Writes the specified content to a stream.
    /// </summary>
    /// <param name="output">Output stream to write to.</param>
    /// <param name="content">Content to write.</param>
    void Save(Stream output, object content);

    /// <summary>
    /// Returns true if the specified type can be handled by this exporter, false otherwise.
    /// </summary>
    /// <param name="contentType">Content type to be saved.</param>
    /// <returns>True if the type can be saved by this exporter, false otherwise.</returns>
    bool CanSaveType(Type contentType);
  }

  #endregion

  #region IResource interfaces

  /// <summary>
  /// Interface that defines access to an individual resource that can be read or written to. Typically the resource is a single file that holds a piece of content. 
  /// Since this object represents the access and not the actual resource object, the underlying asset may or may not actually exist.
  /// </summary>
  public interface IResourceFile
  {
    /// <summary>
    /// Gets the name of the resource file (without format extension).
    /// </summary>
    string Name { get; }

    /// <summary>
    /// Gets the resource file's format extension.
    /// </summary>
    string Extension { get; }

    /// <summary>
    /// Gets the name of the resource file with the format extension.
    /// </summary>
    string NameWithExtension { get; }

    /// <summary>
    /// Gets the full qualified name of the resource file - the path to the resource, its name, and its extension.
    /// </summary>
    string FullName { get; }

    /// <summary>
    /// Gets a relative path from the parent repository to the resource file, it's name, and it's extension.
    /// </summary>
    string RelativeName { get; }

    /// <summary>
    /// Gets if the resource file does in fact exist in the repository.
    /// </summary>
    bool Exists { get; }

    /// <summary>
    /// Gets if the resource file is read only (if it exists).
    /// </summary>
    bool IsReadOnly { get; }

    /// <summary>
    /// Gets the time, in coordinated universal time (UTC), that the resource was initially created.
    /// </summary>
    DateTime CreationTime { get; }

    /// <summary>
    /// Gets the time, in coordinated universal time (UTC), that the resource was last accessed.
    /// </summary>
    DateTime LastAccessTime { get; }

    /// <summary>
    /// Gets the time, in coordinated universal time (UTC), that the repository was last written to.
    /// </summary>
    DateTime LastWriteTime { get; }

    /// <summary>
    /// Gets the repository that this resource belongs to.
    /// </summary>
    IResourceRepository Repository { get; }

    /// <summary>
    /// Opens a resource stream to be read-only.
    /// </summary>
    /// <returns>Resource stream</returns>
    Stream Open();

    /// <summary>
    /// Opens a resource stream.
    /// </summary>
    /// <param name="fileMode">Specifies the mode in which to open the file.</param>
    /// <returns>Resource stream</returns>
    Stream Open(ResourceFileMode fileMode);

    /// <summary>
    /// Opens a resource stream.
    /// </summary>
    /// <param name="fileMode">Specifies the mode in which to open the file.</param>
    /// <param name="accessMode">Specifies the read or write access of the file.</param>
    /// <returns>Resource stream</returns>
    Stream Open(ResourceFileMode fileMode, ResourceFileAccess accessMode);

    /// <summary>
    /// Opens a resource steam.
    /// </summary>
    /// <param name="fileMode">Specifies the mode in which to open the file.</param>
    /// <param name="accessMode">Specifies the read or write access of the file.</param>
    /// <param name="fileShare">Specifies how the same file should be shared with other resource streams</param>
    /// <returns>Resource stream</returns>
    Stream Open(ResourceFileMode fileMode, ResourceFileAccess accessMode, ResourceFileShare fileShare);

    /// <summary>
    /// Opens a resource stream for writing. If the resource file does not exist, it will be created.
    /// </summary>
    /// <returns>Resource stream</returns>
    Stream OpenWrite();

    /// <summary>
    /// Opens a resource stream for reading.
    /// </summary>
    /// <returns>Resource stream</returns>
    Stream OpenRead();

    /// <summary>
    /// Creates a new file with the resource name and returns a writable stream.
    /// </summary>
    /// <returns>Resource stream</returns>
    Stream Create();

    /// <summary>
    /// Permanently deletes the resource file.
    /// </summary>
    void Delete();

    /// <summary>
    /// Refreshes information about the resource file.
    /// </summary>
    void Refresh();
  }

  /// <summary>
  /// Interface that defines a resource repository. A repository can be as simple as a file directory or something more complex like a compressed archive
  /// or remote web server. It is capable of locating and creating individual resources within the repository, so resource names are generally given to be a relative
  /// path or the file name of the resource. A repository should always be able to return a resource file, although the actual file may or may not exist (if it
  /// does not exist, it may be created via methods on the resource file).
  /// </summary>
  public interface IResourceRepository
  {
    /// <summary>
    /// Gets the root path of the resource repository. This can be the path directory on a file system, an URL, etc.
    /// </summary>
    string RootPath { get; }

    /// <summary>
    /// Gets whether the repository is currently opened.
    /// </summary>
    bool IsOpen { get; }

    /// <summary>
    /// Gets whether the root path points to a repository that exists.
    /// </summary>
    bool Exists { get; }

    /// <summary>
    /// Gets if the repository is read only (if it exists).
    /// </summary>
    bool IsReadOnly { get; }

    /// <summary>
    /// Gets the time, in coordinated universal time (UTC), that the repository was initially created.
    /// </summary>
    DateTime CreationTime { get; }

    /// <summary>
    /// Gets the time, in coordinated universal time (UTC), that the repository was last accessed.
    /// </summary>
    DateTime LastAccessTime { get; }

    /// <summary>
    /// Gets the time, in coordinated universal time (UTC), that the repository was last written to.
    /// </summary>
    DateTime LastWriteTime { get; }

    /// <summary>
    /// Opens a connection to the resource repository to be read-only. Once a connection has been opened, then the repository can be used to located resources.
    /// </summary>
    void OpenConnection();

    /// <summary>
    /// Opens a connection to the resource repository. Once a connection has been opened, then the repository can be used to located resources.
    /// </summary>
    /// <param name="mode">Specifies how the repository should be opened or created</param>
    void OpenConnection(ResourceFileMode mode);

    /// <summary>
    /// Opens a connection to the resource repository. Once a connection has been opened, then the repository can be used to located resources.
    /// </summary>
    /// <param name="mode">Specifies how the repository should be opened or created</param>
    /// <param name="shareMode">Specifies how other repositories can access the repository.</param>
    void OpenConnection(ResourceFileMode mode, ResourceFileShare shareMode);

    /// <summary>
    /// Closes a connection to the resource repository.
    /// </summary>
    void CloseConnection();

    /// <summary>
    /// Gets a resource file from the repository with the specified file name.
    /// </summary>
    /// <param name="resourceName">Resource file name, including its extension</param>
    /// <returns>Returns the resource, which may or may not be valid if the resource does not exist.</returns>
    IResourceFile GetResourceFile(string resourceName);

    /// <summary>
    /// Gets a resource file from the repository with the specified file name that is relative to a known resource. Typically the known
    /// resource is a primary content source that references the secondary content source (e.g. a texture or shader).
    /// </summary>
    /// <param name="resourceName">Resource file name, including its extension</param>
    /// <param name="resource">Known resource</param>
    /// <returns>Returns the resource, which may or may not be valid if the resource does not exist.</returns>
    IResourceFile GetResourceFileRelativeTo(string resourceName, IResourceFile resource);

    /// <summary>
    /// Enumerates all resources in the resource repository.
    /// </summary>
    /// <param name="recursive">True if files in sub directories should also be enumerated, false otherwise.</param>
    /// <param name="searchPattern">Optional search pattern, otherwise all files.</param>
    /// <returns>All resources in the repository</returns>
    IEnumerable<IResourceFile> EnumerateResourceFiles(bool recursive = true, string? searchPattern = null);

    /// <summary>
    /// Queries if the resource is contained in the repository.
    /// </summary>
    /// <param name="resourceName">Resource file name, including its extension</param>
    /// <returns>Returns true if the resource is present in the repository.</returns>
    bool ContainsResource(string resourceName);

    /// <summary>
    /// Gets the fully qualified path to the resource.
    /// </summary>
    /// <param name="resourceName">Name of the resource, including its extension</param>
    /// <returns>The fully qualified path to the resource</returns>
    string GetFullPath(string resourceName);

    /// <summary>
    /// Gets the fully qualified path to a resource that is relative to the specified resource file.
    /// </summary>
    /// <param name="resourceName">Resource file name, including its extension</param>
    /// <param name="resource">Known resource</param>
    /// <returns>Returns the fully qualified path of the resource</returns>
    string GetFullPathRelativeTo(string resourceName, IResourceFile resource);

    /// <summary>
    /// Refreshes information about the repository.
    /// </summary>
    void Refresh();
  }

  #endregion

  #region ISavable interfaces

  /// <summary>
  /// Interface for objects that can be written to an output and read from an input.
  /// </summary>
  public interface ISavable
  {
    /// <summary>
    /// Reads the object data from the input.
    /// </summary>
    /// <param name="input">Savable reader.</param>
    void Read(ISavableReader input);

    /// <summary>
    /// Writes the object data to the output.
    /// </summary>
    /// <param name="output">Savable writer.</param>
    void Write(ISavableWriter output);
  }

  /// <summary>
  /// Interface that represents a custom primitive data type that can be written to an output and read from
  /// an input.
  /// </summary>
  public interface IPrimitiveValue
  {
    /// <summary>
    /// Reads the primitive data from the input.
    /// </summary>
    /// <param name="input">Primitive reader.</param>
    void Read(IPrimitiveReader input);

    /// <summary>
    /// Writes the primitive data to the output.
    /// </summary>
    /// <param name="output">Primitive writer.</param>
    void Write(IPrimitiveWriter output);
  }

  /// <summary>
  /// Defines a reader that can read the following primitives from an input:
  /// <list type="bullet">
  /// <item>
  /// Formatted data for built-in value types, enums, strings, and custom <see cref="IPrimitiveValue"/> structs. In the following cases:
  /// <list type="bullet">
  /// <item>
  /// Single values
  /// </item>
  /// <item>
  /// Nullable values - some format writers may choose to exclude these completely if null, so a reader has the ability to treat a nullable as optional data that if
  /// not present will be substituted for a default value.
  /// </item>
  /// <item>
  /// Arrays of values
  /// </item>
  /// </list>
  /// </item>
  /// <item>
  /// Binary blob data for blittable types, e.g. data that can be stored inside a <see cref="IDataBuffer"/>.
  /// </item>
  /// </list>
  /// <para>
  /// Formatted data in this context means each piece of data is a named property, e.g. a <see cref="Vector3"/> will expect there to exist three
  /// values in the input named "X", "Y", and "Z". This allows for data to be read from both binary and human-readable textual formats in a unified, consistent manner.
  /// </para>
  /// <para>
  /// Conversely, if the input contains a blob of <see cref="Vector3"/>, the data is stored as a raw binary representation.
  /// This is useful if the data does not need extra human-readable context or needs to be stored more compactly, which can be 
  /// faster to load. A textual format may choose to store this as an array of byte integers or a Base64 string or similar means.
  /// </para>
  /// </summary>
  public interface IPrimitiveReader : IDisposable
  {
    /// <summary>
    /// Gets if this reader has been disposed or not.
    /// </summary>
    bool IsDisposed { get; }

    /// <summary>
    /// Closes the underlying stream.
    /// </summary>
    void Close();

    /// <summary>
    /// Reads a single byte from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>Byte value.</returns>
    byte ReadByte(string name);

    /// <summary>
    /// Reads a single nullable byte from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>Nullable byte value.</returns>
    byte? ReadNullableByte(string name);

    /// <summary>
    /// Reads an array of bytes from the input. If the receiving span is smaller than the number of elements that can be read, it will read
    /// as many as it can and skip the rest. Use <see cref="PeekArrayCount"/> to determine how large the receiving span should be.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Span to hold the array values.</param>
    /// <returns>Number of values read.</returns>
    int ReadByteArray(string name, Span<byte> values);

    /// <summary>
    /// Reads a single sbyte from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>SByte value.</returns>
    sbyte ReadSByte(string name);

    /// <summary>
    /// Reads a single nullable sbyte from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>Nullable sbyte value.</returns>
    sbyte? ReadNullableSByte(string name);

    /// <summary>
    /// Reads an array of sbytes from the input. If the receiving span is smaller than the number of elements that can be read, it will read
    /// as many as it can and skip the rest. Use <see cref="PeekArrayCount"/> to determine how large the receiving span should be.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Span to hold the array values.</param>
    /// <returns>Number of values read.</returns>
    int ReadSByteArray(string name, Span<sbyte> values);

    /// <summary>
    /// Reads a single char from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>Char value.</returns>
    char ReadChar(string name);

    /// <summary>
    /// Reads a single nullable char from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>Nullable char value.</returns>
    char? ReadNullableChar(string name);

    /// <summary>
    /// Reads an array of chars from the input. If the receiving span is smaller than the number of elements that can be read, it will read
    /// as many as it can and skip the rest. Use <see cref="PeekArrayCount"/> to determine how large the receiving span should be.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Span to hold the array values.</param>
    /// <returns>Number of values read.</returns>
    int ReadCharArray(string name, Span<char> values);

    /// <summary>
    /// Reads a single unsigned 16-bit int from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>UInt16 value.</returns>
    ushort ReadUInt16(string name);

    /// <summary>
    /// Reads a single nullable unsigned 16-bit int from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>Nullable uint16 value.</returns>
    ushort? ReadNullableUInt16(string name);

    /// <summary>
    /// Reads an array of unsigned 16-bit ints from the input. If the receiving span is smaller than the number of elements that can be read, it will read
    /// as many as it can and skip the rest. Use <see cref="PeekArrayCount"/> to determine how large the receiving span should be.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Span to hold the array values.</param>
    /// <returns>Number of values read.</returns>
    int ReadUInt16Array(string name, Span<ushort> values);

    /// <summary>
    /// Reads a single unsigned 32-bit int from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>UInt32 value.</returns>
    uint ReadUInt32(string name);

    /// <summary>
    /// Reads a single nullable unsigned 32-bit int from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>Nullable uint32 value.</returns>
    uint? ReadNullableUInt32(string name);

    /// <summary>
    /// Reads an array of unsigned 32-bits int from the input. If the receiving span is smaller than the number of elements that can be read, it will read
    /// as many as it can and skip the rest. Use <see cref="PeekArrayCount"/> to determine how large the receiving span should be.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Span to hold the array values.</param>
    /// <returns>Number of values read.</returns>
    int ReadUInt32Array(string name, Span<uint> values);

    /// <summary>
    /// Reads a single unsigned 64-bit int from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>UInt64 value.</returns>
    ulong ReadUInt64(string name);

    /// <summary>
    /// Reads a single nullable unsigned 64-bit int from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>Nullable uint64 value.</returns>
    ulong? ReadNullableUInt64(string name);

    /// <summary>
    /// Reads an array of unsigned 64-bits int from the input. If the receiving span is smaller than the number of elements that can be read, it will read
    /// as many as it can and skip the rest. Use <see cref="PeekArrayCount"/> to determine how large the receiving span should be.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Span to hold the array values.</param>
    /// <returns>Number of values read.</returns>
    int ReadUInt64Array(string name, Span<ulong> values);

    /// <summary>
    /// Reads a single 16-bit int from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>Int16 value.</returns>
    short ReadInt16(string name);

    /// <summary>
    /// Reads a single nullable 16-bit int from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>Nullable int16 value.</returns>
    short? ReadNullableInt16(string name);

    /// <summary>
    /// Reads an array of 16-bits int from the input. If the receiving span is smaller than the number of elements that can be read, it will read
    /// as many as it can and skip the rest. Use <see cref="PeekArrayCount"/> to determine how large the receiving span should be.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Span to hold the array values.</param>
    /// <returns>Number of values read.</returns>
    int ReadInt16Array(string name, Span<short> values);

    /// <summary>
    /// Reads a single 32-bit int from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>Int32 value.</returns>
    int ReadInt32(string name);

    /// <summary>
    /// Reads a single nullable 32-bit int from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>Nullable int32 value.</returns>
    int? ReadNullableInt32(string name);

    /// <summary>
    /// Reads an array of 32-bits int from the input. If the receiving span is smaller than the number of elements that can be read, it will read
    /// as many as it can and skip the rest. Use <see cref="PeekArrayCount"/> to determine how large the receiving span should be.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Span to hold the array values.</param>
    /// <returns>Number of values read.</returns>
    int ReadInt32Array(string name, Span<int> values);

    /// <summary>
    /// Reads a single 64-bit int from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>Int64 value.</returns>
    long ReadInt64(string name);

    /// <summary>
    /// Reads a single nullable 64-bit int from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>Nullable int64 value.</returns>
    long? ReadNullableInt64(string name);

    /// <summary>
    /// Reads an array of 64-bits int from the input. If the receiving span is smaller than the number of elements that can be read, it will read
    /// as many as it can and skip the rest. Use <see cref="PeekArrayCount"/> to determine how large the receiving span should be.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Span to hold the array values.</param>
    /// <returns>Number of values read.</returns>
    int ReadInt64Array(string name, Span<long> values);

    /// <summary>
    /// Reads a single half from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>Half value.</returns>
    Half ReadHalf(string name);

    /// <summary>
    /// Reads a single nullable half from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>Nullable half value.</returns>
    Half? ReadNullableHalf(string name);

    /// <summary>
    /// Reads an array of halfs from the input. If the receiving span is smaller than the number of elements that can be read, it will read
    /// as many as it can and skip the rest. Use <see cref="PeekArrayCount"/> to determine how large the receiving span should be.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Span to hold the array values.</param>
    /// <returns>Number of values read.</returns>
    int ReadHalfArray(string name, Span<Half> values);

    /// <summary>
    /// Reads a single float from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>Float value.</returns>
    float ReadSingle(string name);

    /// <summary>
    /// Reads a single nullable float from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>Float value.</returns>
    float? ReadNullableSingle(string name);

    /// <summary>
    /// Reads an array of floats from the input. If the receiving span is smaller than the number of elements that can be read, it will read
    /// as many as it can and skip the rest. Use <see cref="PeekArrayCount"/> to determine how large the receiving span should be.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Span to hold the array values.</param>
    /// <returns>Number of values read.</returns>
    int ReadSingleArray(string name, Span<float> values);

    /// <summary>
    /// Reads a single double from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>Double value.</returns>
    double ReadDouble(string name);

    /// <summary>
    /// Reads a single nullable double from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>Double value.</returns>
    double? ReadNullableDouble(string name);

    /// <summary>
    /// Reads an array of doubles from the input. If the receiving span is smaller than the number of elements that can be read, it will read
    /// as many as it can and skip the rest. Use <see cref="PeekArrayCount"/> to determine how large the receiving span should be.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Span to hold the array values.</param>
    /// <returns>Number of values read.</returns>
    int ReadDoubleArray(string name, Span<double> values);

    /// <summary>
    /// Reads a single decimal from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>Decimal value.</returns>
    decimal ReadDecimal(string name);

    /// <summary>
    /// Reads a single nullable decimal from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>Nullable decimal value.</returns>
    decimal? ReadNullableDecimal(string name);

    /// <summary>
    /// Reads an array of decimals from the input. If the receiving span is smaller than the number of elements that can be read, it will read
    /// as many as it can and skip the rest. Use <see cref="PeekArrayCount"/> to determine how large the receiving span should be.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Span to hold the array values.</param>
    /// <returns>Number of values read.</returns>
    int ReadDecimalArray(string name, Span<decimal> values);

    /// <summary>
    /// Reads a single boolean from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>Boolean value.</returns>
    bool ReadBoolean(string name);

    /// <summary>
    /// Reads a single nullable boolean from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>Nullable boolean value.</returns>
    bool? ReadNullableBoolean(string name);

    /// <summary>
    /// Reads an array of booleans from the input. If the receiving span is smaller than the number of elements that can be read, it will read
    /// as many as it can and skip the rest. Use <see cref="PeekArrayCount"/> to determine how large the receiving span should be.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Span to hold the array values.</param>
    /// <returns>Number of values read.</returns>
    int ReadBooleanArray(string name, Span<bool> values);

    /// <summary>
    /// Reads a string from the input.
    /// </summary>
    /// <param name="name">Name of the value</param>
    /// <returns>String value</returns>
    string? ReadString(string name);

    /// <summary>
    /// Reads an array of strings from the input. If the receiving span is smaller than the number of elements that can be read, it will read
    /// as many as it can and skip the rest. Use <see cref="PeekArrayCount"/> to determine how large the receiving span should be.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Span to hold the array values.</param>
    /// <returns>Number of values read.</returns>
    int ReadStringArray(string name, Span<string?> values);

    /// <summary>
    /// Reads a single <see cref="IPrimitiveValue"/> struct from the input.
    /// </summary>
    /// <typeparam name="T">Primitive struct type.</typeparam>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">Read <see cref="IPrimitiveValue"/> struct value.</param>
    void Read<T>(string name, out T value) where T : struct, IPrimitiveValue;

    /// <summary>
    /// Reads a single nullable <see cref="IPrimitiveValue"/> struct from the output.
    /// </summary>
    /// <typeparam name="T">Primitive struct type.</typeparam>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">Read nullable <see cref="IPrimitiveValue"/> struct value.</param>
    void ReadNullable<T>(string name, out T? value) where T : struct, IPrimitiveValue;
    
    /// <summary>
    /// Reads an array of <see cref="IPrimitiveValue"/> structs from the input. If the receiving span is smaller than the number of
    /// elements that can be read, it will read as many as it can and skip the rest. Use <see cref="PeekArrayCount"/> to determine how 
    /// large the receiving span should be.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Span to hold the array values.</param>
    /// <returns>Number of values read.</returns>
    int ReadArray<T>(string name, Span<T> values) where T : struct, IPrimitiveValue;

    /// <summary>
    /// Reads an enum value from the input.
    /// </summary>
    /// <typeparam name="T">Enum type.</typeparam>
    /// <param name="name">Name of the value.</param>
    /// <returns>Enum value.</returns>
    T ReadEnum<T>(string name) where T : struct, Enum;

    /// <summary>
    /// Reads a nullable enum value from the input.
    /// </summary>
    /// <typeparam name="T">Enum type.</typeparam>
    /// <param name="name">Name of the value.</param>
    /// <returns>Nullable enum value.</returns>
    T? ReadNullableEnum<T>(string name) where T : struct, Enum;

    /// <summary>
    /// Peeks ahead and returns the number of elements if the value is an array or group. Zero is returned if the
    /// value is null or not an array/group. This does not advance the reader.
    /// </summary>
    /// <param name="name">Name of the value</param>
    /// <returns>Non-zero if the value is an array/grouping with elements, zero if it is null.</returns>
    int PeekArrayCount(string name);

    /// <summary>
    /// Peeks ahead and returns the blob header if the value is a blob. Null is returned if the value is null or not a blob.
    /// This does not advance the reader.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>Valid header if the blob is not null, invalid header if the blob is null.</returns>
    PrimitiveBlobHeader? PeekBlob(string name);

    /// <summary>
    /// Reads the blob into the receiving span of the specified type. The type does not have to match the stored type metadata
    /// (e.g. can be raw bytes or reintrepret the data). If the receiving span's size is less than the blob size in bytes, 
    /// it will read as many bytes as it can and skip the rest. Use <see cref="PeekBlob"/> to determine how large the receiving span should be.
    /// </summary>
    /// <typeparam name="T">Blittable element type.</typeparam>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Span to hold the blob values.</param>
    /// <returns>Number of values read, as defined by the receiving span's type size in bytes.</returns>
    int ReadBlob<T>(string name, Span<T> values) where T : unmanaged;

    /// <summary>
    /// Reads the group header and returns the number of elements, if the object is a group. After reading each element, be sure to call <see cref="EndReadGroup"/>. Every begin must be paired with an end.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>Non-zero if the object is an array with elements, zero if empty array or just a grouping.</returns>
    int BeginReadGroup(string name);

    /// <summary>
    /// Finishes reading the group. Every begin must be paired with an end.
    /// </summary>
    void EndReadGroup();

    /// <summary>
    /// Skips the value, if it is present.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    void Skip(string name);
  }

  /// <summary>
  /// Defines a writer that can write the following primitives to an output:
  /// <list type="bullet">
  /// <item>
  /// Formatted data for built-in value types, enums, strings, and custom <see cref="IPrimitiveValue"/> structs. In the following cases:
  /// <list type="bullet">
  /// <item>
  /// Single values
  /// </item>
  /// <item>
  /// Nullable values
  /// </item>
  /// <item>
  /// Arrays of values
  /// </item>
  /// </list>
  /// </item>
  /// <item>
  /// Binary blob data for blittable types, e.g. contents of a <see cref="IReadOnlyDataBuffer"/>.
  /// </item>
  /// </list>
  /// <para>
  /// Formatted data in this context means each piece of data is a named property, e.g. a <see cref="Vector3"/> will output three values
  /// named "X", "Y", and "Z". This allows for data to be written out to both binary and human-readable textual formats in a unified, consistent manner.
  /// </para>
  /// <para>
  /// Conversely, since an array of <see cref="Vector3"/> is blittable, it may be written out as a blob of bytes with type metadata. This is useful
  /// if the data does not need extra human-readable context or needs to be stored more compactly. A textual format may choose to store this as an 
  /// array of byte integers or a Base64 string or similar means.
  /// </para>
  /// </summary>
  public interface IPrimitiveWriter : IDisposable
  {
    /// <summary>
    /// Gets if the writer has been disposed or not.
    /// </summary>
    bool IsDisposed { get; }

    /// <summary>
    /// Closes the underlying stream.
    /// </summary>
    void Close();

    /// <summary>
    /// Causes any buffered data to be written out and clears all pending buffers, effectively resetting the writer.
    /// </summary>
    void Flush();

    /// <summary>
    /// Writes a single byte to the output.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">Byte value.</param>
    void Write(string name, byte value);

    /// <summary>
    /// Writes a single nullable byte to the output.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">Nullable byte value.</param>
    void Write(string name, byte? value);

    /// <summary>
    /// Writes a span of bytes to the output.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Span of bytes.</param>
    void Write(string name, ReadOnlySpan<byte> values);

    /// <summary>
    /// Writes a single sbyte to the output.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">SByte value.</param>
    void Write(string name, sbyte value);

    /// <summary>
    /// Writes a single nullable sbyte to the output.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">Nullable sbyte value.</param>
    void Write(string name, sbyte? value);

    /// <summary>
    /// Writes an span of sbytes to the output.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Span of sbytes.</param>
    void Write(string name, ReadOnlySpan<sbyte> values);

    /// <summary>
    /// Writes a single char to the output.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">Char value.</param>
    void Write(string name, char value);

    /// <summary>
    /// Writes a single nullable char to the output.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">Nullable char value.</param>
    void Write(string name, char? value);

    /// <summary>
    /// Writes a span of chars to the output.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Span of chars.</param>
    void Write(string name, ReadOnlySpan<char> values);

    /// <summary>
    /// Writes a single unsigned 16-bit int to the output.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">UInt16 value.</param>
    void Write(string name, ushort value);

    /// <summary>
    /// Writes a single nullable unsigned 16-bit int to the output.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">Nullable uint16 value.</param>
    void Write(string name, ushort? value);

    /// <summary>
    /// Writes a span of unsigned 16-bit ints to the output.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Span of UInt16s.</param>
    void Write(string name, ReadOnlySpan<ushort> values);

    /// <summary>
    /// Writes a single unsigned 32-bit int to the output.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">UInt32 value.</param>
    void Write(string name, uint value);

    /// <summary>
    /// Writes a single nullable unsigned 32-bit int to the output.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">Nullable uint32 value.</param>
    void Write(string name, uint? value);

    /// <summary>
    /// Writes a span of unsigned 32-bit ints to the output.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Span of UInt32s.</param>
    void Write(string name, ReadOnlySpan<uint> values);

    /// <summary>
    /// Writes a single unsigned 64-bit int to the output.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">UInt64 value.</param>
    void Write(string name, ulong value);

    /// <summary>
    /// Writes a single nullable unsigned 64-bit int to the output.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">Nullable uint64 value.</param>
    void Write(string name, ulong? value);

    /// <summary>
    /// Writes a span of unsigned 64-bit ints to the output.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Span of UInt64s.</param>
    void Write(string name, ReadOnlySpan<ulong> values);

    /// <summary>
    /// Writes a single 16-bit int to the output.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">Int16 value.</param>
    void Write(string name, short value);

    /// <summary>
    /// Writes a single nullable 16-bit int to the output.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">Nullable int16 value.</param>
    void Write(string name, short? value);

    /// <summary>
    /// Writes a span of 16-bit ints to the output.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Span of Int16s.</param>
    void Write(string name, ReadOnlySpan<short> values);

    /// <summary>
    /// Writes a single 32-bit int to the output.
    /// </summary>
    /// <param name="name">Name of the value</param>
    /// <param name="value">Int32 value.</param>
    void Write(string name, int value);

    /// <summary>
    /// Writes a single nullable 32-bit int to the output.
    /// </summary>
    /// <param name="name">Name of the value</param>
    /// <param name="value">Nullable int32 value.</param>
    void Write(string name, int? value);

    /// <summary>
    /// Writes a span of 32-bit ints to the output.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Span of Int32s.</param>
    void Write(string name, ReadOnlySpan<int> values);

    /// <summary>
    /// Writes a single 64-bit int to the output.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">Int64 value.</param>
    void Write(string name, long value);

    /// <summary>
    /// Writes a single nullable 64-bit int to the output.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">Nullable int64 value.</param>
    void Write(string name, long? value);

    /// <summary>
    /// Writes a span of 64-bit ints to the output.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Span of Int64s.</param>
    void Write(string name, ReadOnlySpan<long> values);

    /// <summary>
    /// Writes a single half to the output.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">Half value.</param>
    void Write(string name, Half value);

    /// <summary>
    /// Writes a single nullable half to the output.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">Nullable half value.</param>
    void Write(string name, Half? value);

    /// <summary>
    /// Writes a span of halfs to the output.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Span of halfs.</param>
    void Write(string name, ReadOnlySpan<Half> values);

    /// <summary>
    /// Writes a single float to the output.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">Float value.</param>
    void Write(string name, float value);

    /// <summary>
    /// Writes a single nullable float to the output.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">Nullable float value.</param>
    void Write(string name, float? value);

    /// <summary>
    /// Writes a span of floats to the output.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Span of floats.</param>
    void Write(string name, ReadOnlySpan<float> values);

    /// <summary>
    /// Writes a single double to the output.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">Double value.</param>
    void Write(string name, double value);

    /// <summary>
    /// Writes a single nullable double to the output.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">Nullable double value.</param>
    void Write(string name, double? value);

    /// <summary>
    /// Writes a span of doubles to the output.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Span of doubles.</param>
    void Write(string name, ReadOnlySpan<double> values);

    /// <summary>
    /// Writes a single decimal to the output.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">Decimal value.</param>
    void Write(string name, decimal value);

    /// <summary>
    /// Writes a single nullable decimal to the output.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">Nullable decimal value.</param>
    void Write(string name, decimal? value);

    /// <summary>
    /// Writes a span of decimals to the output.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Span of decimals.</param>
    void Write(string name, ReadOnlySpan<decimal> values);

    /// <summary>
    /// Writes a single boolean to the output.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">Boolean value.</param>
    void Write(string name, bool value);

    /// <summary>
    /// Writes a single nullable boolean to the output.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">Nullable boolean value.</param>
    void Write(string name, bool? value);

    /// <summary>
    /// Writes a span of booleans to the output.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Span of booleans.</param>
    void Write(string name, ReadOnlySpan<bool> values);

    /// <summary>
    /// Writes a single string to the output.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">String value.</param>
    void Write(string name, string? value);

    /// <summary>
    /// Writes a span of strings to the output.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Span of strings.</param>
    void Write(string name, ReadOnlySpan<string?> values);

    /// <summary>
    /// Writes single <see cref="IPrimitiveValue"/> struct to the output.
    /// </summary>
    /// <typeparam name="T">Primitive struct type.</typeparam>
    /// <param name="name">Name of the value.</param>
    /// <param name="value"><see cref="IPrimitiveValue"/> struct value.</param>
    void Write<T>(string name, in T value) where T : struct, IPrimitiveValue;

    /// <summary>
    /// Writes single nullable <see cref="IPrimitiveValue"/> struct to the output.
    /// </summary>
    /// <typeparam name="T">Primitive struct type.</typeparam>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">Nullable <see cref="IPrimitiveValue"/> struct value.</param>
    void Write<T>(string name, in T? value) where T : struct, IPrimitiveValue;

    /// <summary>
    /// Writes a span of <see cref="IPrimitiveValue"/> structs to the output.
    /// </summary>
    /// <typeparam name="T">Primitive struct type.</typeparam>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Span of <see cref="IPrimitiveValue"/> structs.</param>
    void Write<T>(string name, ReadOnlySpan<T> values) where T : struct, IPrimitiveValue;

    /// <summary>
    /// Writes an enum value to the output.
    /// </summary>
    /// <typeparam name="T">Enum type.</typeparam>
    /// <param name="name">Name of the value.</param>
    /// <param name="enumValue">Enum value.</param>
    void WriteEnum<T>(string name, T enumValue) where T : struct, Enum;

    /// <summary>
    /// Writes a nullable enum value to the output.
    /// </summary>
    /// <typeparam name="T">Enum type</typeparam>
    /// <param name="name">Name of the value</param>
    /// <param name="enumValue">Nullable enum value</param>
    void WriteEnum<T>(string name, T? enumValue) where T : struct, Enum;

    /// <summary>
    /// Writes a span of values as an unformatted blob to the output. This is similar as writing out the data as a span of bytes, but will
    /// also capture type information. The element must be a blittable type.
    /// </summary>
    /// <typeparam name="T">Blittable element type.</typeparam>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Span of values.</param>
    void WriteBlob<T>(string name, ReadOnlySpan<T> values) where T : unmanaged;

    /// <summary>
    /// Writes the contents of the data buffer as an unformatted blob to the output. This is similar as writing out the data as a span of bytes,
    /// but will also capture type information.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Buffer containin the data.</param>
    void WriteBlob(string name, IReadOnlyDataBuffer? values);

    /// <summary>
    /// Writes a group header. Subsequent calls fill the contents of the group, be sure to call <see cref="EndWriteGroup"/> when done writing the grouped elements.
    /// </summary>
    /// <param name="name">Name of the group.</param>
    void BeginWriteGroup(string name);

    /// <summary>
    /// Writes a group header that represents a collection. Subsequent calls fill the contents of the group, be sure to call <see cref="EndWriteGroup"/> when done writing the grouped elements.
    /// </summary>
    /// <param name="name">Name of the group.</param>
    /// <param name="count">Number of elements in the group.</param>
    void BeginWriteGroup(string name, int count);


    /// <summary>
    /// Ends a grouping. Every begin must be paired with an end.
    /// </summary>
    void EndWriteGroup();
  }

  /// <summary>
  /// Interface that defines a reader that can read objects that implement the <see cref="ISavable"/> interface from an input. Some implementations may optionally allow
  /// for data to not be present and as such every read is required to include the name of the value, to mirror <see cref="ISavableWriter"/> write
  /// methods.
  /// </summary>
  public interface ISavableReader : IPrimitiveReader
  {
    /// <summary>
    /// Gets the service provider, used to locate any services that may be needed during content loading.
    /// </summary>
    IServiceProvider ServiceProvider { get; }

    /// <summary>
    /// Gets the external reference resolver of the reader.
    /// </summary>
    IExternalReferenceResolver? ExternalResolver { get; }

    /// <summary>
    /// Gets the persisted version for the type. If the type does not implement <see cref="ISavable"/> then the version is always 1.
    /// If the type does implement <see cref="ISavable"/> and was NOT persisted, then -1 is returned, indicating the type did not exist
    /// when the data was written.
    /// </summary>
    /// <param name="type">Type.</param>
    /// <returns>A version number that is 1 or greater. If the type was not persisted, then -1.</returns>
    int GetVersion(Type type);

    /// <summary>
    /// Reads a savable object from the input.
    /// </summary>
    /// <typeparam name="T">Type of object to read.</typeparam>
    /// <param name="name">Name of the object.</param>
    /// <returns>Read <see cref="ISavable"/> object.</returns>
    T ReadSavable<T>(string name) where T : class?, ISavable?;

    /// <summary>
    /// Reads an array of savable objects from the input. If the receiving span is smaller than the number of elements that can be read, it will read
    /// as many as it can and skip the rest. Use <see cref="PeekArrayCount"/> to determine how large the receiving span should be.
    /// </summary>
    /// <typeparam name="T">Type of object to read.</typeparam>
    /// <param name="name">Name of the object.</param>
    /// <param name="values">Span to hold the savable objects.</param>
    /// <returns>Number of values read.</returns>
    int ReadSavableArray<T>(string name, Span<T> values) where T : class?, ISavable?;

    /// <summary>
    /// Reads a shared savable object from the input.
    /// </summary>
    /// <typeparam name="T">Type of object to read.</typeparam>
    /// <param name="name">Name of the object.</param>
    /// <returns>Read <see cref="ISavable"/> object.</returns>
    T ReadSharedSavable<T>(string name) where T : class?, ISavable?;

    /// <summary>
    /// Reads an array of shared savable objects from the input. If the receiving span is smaller than the number of elements that can be read, it will read
    /// as many as it can and skip the rest. Use <see cref="PeekArrayCount"/> to determine how large the receiving span should be.
    /// </summary>
    /// <typeparam name="T">Type of object to read.</typeparam>
    /// <param name="name">Name of the object.</param>
    /// <param name="values">Span to hold the savable objects.</param>
    /// <returns>Number of values read.</returns>
    int ReadSharedSavableArray<T>(string name, Span<T> values) where T : class?, ISavable?;

    /// <summary>
    /// Reads a savable object that is external from the input, e.g. a separate file.
    /// </summary>
    /// <typeparam name="T">Type of object to read.</typeparam>
    /// <param name="name">Name of the object.</param>
    /// <returns>Read <see cref="ISavable"/> object.</returns>
    T ReadExternalSavable<T>(string name) where T : class?, ISavable?;

    /// <summary>
    /// Reads an array of savable objects that is external from the input, e.g. a separate file. If the receiving span 
    /// is smaller than the number of elements that can be read, it will read as many as it can and skip the rest. 
    /// Use <see cref="PeekArrayCount"/> to determine how large the receiving span should be.
    /// </summary>
    /// <typeparam name="T">Type of object to read.</typeparam>
    /// <param name="name">Name of the object.</param>
    /// <param name="values">Span to hold the savable objects..</param>
    /// <returns>Number of values read.</returns>
    int ReadExternalSavableArray<T>(string name, Span<T> values) where T : class?, ISavable?;
  }

  /// <summary>
  /// Interface that defines a writer that can write objects that implement the <see cref="ISavable"/> interface to an output.
  /// </summary>
  public interface ISavableWriter : IPrimitiveWriter
  {
    /// <summary>
    /// Gets the external handler that controls how savables are to be written externally from the output. If no handler is set,
    /// then external savables are always handled as shared references.
    /// </summary>
    IExternalReferenceHandler? ExternalHandler { get; }

    /// <summary>
    /// Gets the write flags that specify certain behaviors during serialization.
    /// </summary>
    SavableWriteFlags WriteFlags { get; }

    /// <summary>
    /// Writes a savable object to the output.
    /// </summary>
    /// <typeparam name="T">Type of object to write.</typeparam>
    /// <param name="name">Name of the object.</param>
    /// <param name="value">Savable object.</param>
    void WriteSavable<T>(string name, T value) where T : class?, ISavable?;

    /// <summary>
    /// Writes a span of savable objects to the output.
    /// </summary>
    /// <typeparam name="T">Type of object to write.</typeparam>
    /// <param name="name">Name of the object.</param>
    /// <param name="values">Span of savables.</param>
    void WriteSavable<T>(string name, ReadOnlySpan<T> values) where T : class?, ISavable?;

    /// <summary>
    /// Writes a savable object to the output as a shared resource. This ensures an object that is used
    /// in multiple places is only ever written once to the output. This behavior may be overridden where the savable is instead written
    /// as an external reference.
    /// </summary>
    /// <typeparam name="T">Type of object to write</typeparam>
    /// <param name="name">Name of the object<./param>
    /// <param name="value">Savable object.</param>
    void WriteSharedSavable<T>(string name, T value) where T : class?, ISavable?;

    /// <summary>
    /// Writes a span of savable objects to the output where each item is a shared resource. This ensures an object that is used
    /// in multiple places is only ever written once to the output. This behavior may be overridden where each individual savable is instead written
    /// as an external reference.
    /// </summary>
    /// <typeparam name="T">Type of object to write.</typeparam>
    /// <param name="name">Name of the object.</param>
    /// <param name="values">Span of savables.</param>
    void WriteSharedSavable<T>(string name, ReadOnlySpan<T> values) where T : class?, ISavable?;

    /// <summary>
    /// Writes a savable object to an external output that possibly is a different format. This behavior may be overridden where the savable is 
    /// instead written as a shared object.
    /// </summary>
    /// <typeparam name="T">Type of object to write.</typeparam>
    /// <param name="name">Name of the object.</param>
    /// <param name="value">Savable object.</param>
    void WriteExternalSavable<T>(string name, T value) where T : class?, ISavable?;

    /// <summary>
    /// Writes a span of savable objects to an external output that possibly is a different format. This behavior may be overridden where the savable is
    /// instead written as a shared object.
    /// </summary>
    /// <typeparam name="T">Type of object to write.</typeparam>
    /// <param name="name">Name of the object.</param>
    /// <param name="values">Span of savables.</param>
    void WriteExternalSavable<T>(string name, ReadOnlySpan<T> values) where T : class?, ISavable?;
  }

  /// <summary>
  /// Interface for a handler that resolves an external reference path to a full path.
  /// </summary>
  public interface IExternalReferenceResolver
  {
    /// <summary>
    /// Gets the content manager that can load the external resource.
    /// </summary>
    ContentManager ContentManager { get; }

    /// <summary>
    /// Gets the resource file this handler resolves references relative to.
    /// </summary>
    IResourceFile ParentResourceFile { get; }

    /// <summary>
    /// Resolves an external reference and loads the savable using the content manager.
    /// </summary>
    /// <typeparam name="T">Savable type</typeparam>
    /// <param name="externalReference">External reference</param>
    /// <returns>The read savable, may be null.</returns>
    T ResolveSavable<T>(ExternalReference externalReference) where T : class?, ISavable?;
  }

  /// <summary>
  /// Interface for a handler that processes savable objects that need to be written externally to some
  /// resource file.
  /// </summary>
  public interface IExternalReferenceHandler
  {
    /// <summary>
    /// Gets the resource file that this handler processes child resource files for. The resources
    /// that are created/updated are relative to this resource file. If this is null, then external references cannot be processed.
    /// </summary>
    IResourceFile ParentResourceFile { get; }

    /// <summary>
    /// Gets or sets the write flags that specify certain behaviors during serialization.
    /// </summary>
    SavableWriteFlags WriteFlags { get; set; }

    /// <summary>
    /// Gets all external writers registered to this handler.
    /// </summary>
    IEnumerable<IExternalReferenceWriter> Writers { get; }

    /// <summary>
    /// Registers an external writer that handles a specific savable type.
    /// </summary>
    /// <param name="writer">External savable writer</param>
    void RegisterWriter(IExternalReferenceWriter writer);

    /// <summary>
    /// Removes an external writer based on its target savable type it was registered to.
    /// </summary>
    /// <typeparam name="T">Target savable type</typeparam>
    /// <returns>True if the writer was removed, false otherwise</returns>
    bool RemoveWriter<T>() where T : class, ISavable;

    /// <summary>
    /// Gets a registered external writer based on its target savable type it was registered to.
    /// </summary>
    /// <typeparam name="T">Target savable type</typeparam>
    /// <returns>The external savable writer, or null if a writer is not registered to the type.</returns>
    IExternalReferenceWriter GetWriter<T>() where T : class, ISavable;

    /// <summary>
    /// Adds a delegate that returns a unique resource name for a given savable, registered to a specific
    /// savable type. This allows for custom name transformations for output files.
    /// </summary>
    /// <typeparam name="T">Target savable type</typeparam>
    /// <param name="getResourceNameDelegate">Get resource name delegate</param>
    void SetGetResourceNameDelegate<T>(GetResourceNameDelegate getResourceNameDelegate) where T : class, ISavable;

    /// <summary>
    /// Resets the handler to process external references relative to the specified resource file.
    /// </summary>
    /// <param name="parentResourceFile">The resource file that this handler will write external references relative to.</param>
    void SetParentResourceFile(IResourceFile parentResourceFile);

    /// <summary>
    /// Checks if the path/resource name is unique of all the references that the handler has processed so far. 
    /// </summary>
    /// <param name="resourcePath">Resource path (without extension)</param>
    /// <returns>True if the name is unique, false otherwise.</returns>
    bool IsUniqueResourcePath(string resourcePath);

    /// <summary>
    /// Processes a savable into an external reference.
    /// </summary>
    /// <typeparam name="T">Savable type</typeparam>
    /// <param name="value">Savable value</param>
    /// <returns>External reference to the savable</returns>
    ExternalReference ProcessSavable<T>(T value) where T : class?, ISavable?;

    /// <summary>
    /// Flushes any remaining data that needs to be written.
    /// </summary>
    void Flush();

    /// <summary>
    /// Clears cached external file references.
    /// </summary>
    void Clear();

    /// <summary>
    /// Clones the handler.
    /// </summary>
    /// <returns>Shallow copy of the handler.</returns>
    IExternalReferenceHandler Clone();
  }

  /// <summary>
  /// Interface for a writer that knows how to write a resource that is external to another resource.
  /// </summary>
  public interface IExternalReferenceWriter
  {
    /// <summary>
    /// Gets the target type of the savable object that this writer can output.
    /// </summary>
    Type TargetType { get; }

    /// <summary>
    /// Gets the resource extension of the output resource file this writer can create.
    /// </summary>
    string ResourceExtension { get; }

    /// <summary>
    /// Writes a savable object to a resource file.
    /// </summary>
    /// <typeparam name="T">Savable type</typeparam>
    /// <param name="outputResourceFile">Resource file to write to</param>
    /// <param name="externalHandler">The external handler that is calling the writer.</param>
    /// <param name="value">Savable to write</param>
    void WriteSavable<T>(IResourceFile outputResourceFile, IExternalReferenceHandler externalHandler, T value) where T : class, ISavable;
  }

  #endregion
}
