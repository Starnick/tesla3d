﻿/*
* Copyright (c) 2010-2024 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.IO;

#nullable enable

namespace Tesla.Content
{
  /// <summary>
  /// A resource exporter that can save <see cref="ISavable"/> objects to the TEBO (Tesla Engine Binary Object) format.
  /// </summary>
  public sealed class BinaryResourceExporter : ResourceExporter<ISavable>
  {
    /// <summary>
    /// Constructs a new instance of the <see cref="BinaryResourceExporter"/> class.
    /// </summary>
    public BinaryResourceExporter() : base(".tebo") { }

    /// <summary>
    /// Writes the specified content to a resource file, as dictated by the user supplied external handler. Use this method
    /// if custom content naming and external writers are required.
    /// </summary>
    /// <param name="externalHandler">User-specified external handler</param>
    /// <param name="content">Content to write</param>
    /// <param name="writeFlagsForPrimaryObject">Optional write flags that may differ from the external handler write flags, for the primary content object, otherwise the flags on the external handler are used.</param>
    /// <exception cref="ArgumentNullException">Thrown if the external handler is null, its parent resource file, or the content to write.</exception>
    public void Save(IExternalReferenceHandler externalHandler, ISavable content, SavableWriteFlags? writeFlagsForPrimaryObject = null)
    {
      ArgumentNullException.ThrowIfNull(externalHandler, nameof(externalHandler));

      ValidateParameters(externalHandler.ParentResourceFile, content);

      Stream output = externalHandler.ParentResourceFile.OpenWrite();
      SavableWriteFlags writeFlags = writeFlagsForPrimaryObject ?? externalHandler.WriteFlags;

      using (BinarySavableWriter writer = new BinarySavableWriter(output, writeFlags, externalHandler))
        writer.WriteRootSavable<ISavable>(content);
    }

    /// <summary>
    /// Writes the specified content to a resource file.
    /// </summary>
    /// <param name="resourceFile">Resource file to write to</param>
    /// <param name="content">Content to write</param>
    /// <param name="writeFlags">Optional savable write flags</param>
    /// <exception cref="ArgumentNullException">Thrown if either the resource file or content are null.</exception>
    public void Save(IResourceFile resourceFile, ISavable content, SavableWriteFlags writeFlags = SavableWriteFlags.OverwriteExistingResource | SavableWriteFlags.UseCompression)
    {
      ValidateParameters(resourceFile, content);

      IExternalReferenceHandler handler = ExternalReferenceHandler.CreateBinaryDefaultHandler(resourceFile, writeFlags);
      Stream output = resourceFile.OpenWrite();

      using (BinarySavableWriter writer = new BinarySavableWriter(output, writeFlags, handler))
        writer.WriteRootSavable<ISavable>(content);
    }

    /// <summary>
    /// Writes the specified content to a stream.
    /// </summary>
    /// <param name="output">Output stream to write to</param>
    /// <param name="content">Content to write</param>
    /// <param name="writeFlags">Optional savable write flags</param>
    /// <exception cref="ArgumentNullException">Thrown if output stream/content is null or if the stream is not writeable.</exception>
    public void Save(Stream output, ISavable content, SavableWriteFlags writeFlags = SavableWriteFlags.OverwriteExistingResource | SavableWriteFlags.UseCompression)
    {
      ValidateParameters(output, content);

      using (BinarySavableWriter writer = new BinarySavableWriter(output, writeFlags, true))
        writer.WriteRootSavable<ISavable>(content);
    }

    /// <inheritdoc />
    /// <exception cref="ArgumentNullException">Thrown if either the resource file or content are null.</exception>
    public override void Save(IResourceFile resourceFile, ISavable content)
    {
      Save(resourceFile, content);
    }

    /// <inheritdoc />
    /// <exception cref="ArgumentNullException">Thrown if output stream/content is null or if the stream is not writeable.</exception>
    public override void Save(Stream output, ISavable content)
    {
      Save(output, content);
    }
  }
}
