﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using Tesla.Utilities;

#nullable enable

namespace Tesla.Content
{
  /// <summary>
  /// A reader that reads formatted binary data from a stream. The binary data is tokenized, where each
  /// piece of data is preceded by a <see cref="BinaryTypeCode"/> or other header properties (such as array types).
  /// The reader supports forward seeking by allowing callers to skip the next token and it's data.
  /// </summary>
  public class BinaryPrimitiveReader : IPrimitiveReader
  {
    private bool m_isDisposed;
    private BinaryReader m_binaryReader;
    private int m_skipDepth;

    /// <inheritdoc />
    public bool IsDisposed { get { return m_isDisposed; } }

    /// <summary>
    /// Gets the underlying input stream.
    /// </summary>
    protected Stream InStream { get { return m_binaryReader.BaseStream; } }

    /// <summary>
    /// Gets or sets the underlying binary reader.
    /// </summary>
    protected BinaryReader UnderlyingBinaryReader { get { return m_binaryReader; } set { m_binaryReader = value; } }

    /// <summary>
    /// Constructs a new instance of the <see cref="BinaryPrimitiveReader"/> class.
    /// </summary>
    /// <param name="input">Input stream to read from.</param>
    /// <param name="leaveOpen">True if the output stream should NOT be disposed/closed when the reader is disposed/closed, false otherwise.</param>
    /// <exception cref="ArgumentNullException">Thrown if the stream is null.</exception>
    /// <exception cref="NotSupportedException">Thrown if the stream cannot be read from or does not support seeking.</exception>
    public BinaryPrimitiveReader(Stream input, bool leaveOpen = false)
    {
      ArgumentNullException.ThrowIfNull(input, nameof(input));

      if (!input.CanRead || !input.CanSeek)
        throw new NotSupportedException(StringLocalizer.Instance.GetLocalizedString("CannotReadFromStream"));
      
      m_binaryReader = new BinaryReader(input, Encoding.UTF8, leaveOpen);
      m_skipDepth = 0;
      m_isDisposed = false;
    }

    /// <inheritdoc />
    public virtual void Close()
    {
      Dispose();
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public byte ReadByte(string name)
    {
      CheckDisposed();

      ReadAndValidateTypeHeader(name, BinaryTypeCode.Byte1);
      return m_binaryReader.ReadByte();
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public byte? ReadNullableByte(string name)
    {
      CheckDisposed();

      if (ReadAndValidateTypeHeaderOrNull(name, BinaryTypeCode.Byte1))
        return m_binaryReader.ReadByte();

      return null;
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public int ReadByteArray(string name, Span<byte> values)
    {
      CheckDisposed();

      if (ReadAndValidateArrayHeader(name, BinaryTypeCode.Byte1, out int count))
      {
        int elemCount = Math.Min(values.Length, count);
        m_binaryReader.ReadExactly(values.Slice(0, elemCount));

        SeekToEndOfArray(count - elemCount, BinaryTypeCode.Byte1);

        return elemCount;
      }

      return 0;
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public sbyte ReadSByte(string name)
    {
      CheckDisposed();

      ReadAndValidateTypeHeader(name, BinaryTypeCode.Byte1);
      return m_binaryReader.ReadSByte();
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public sbyte? ReadNullableSByte(string name)
    {
      CheckDisposed();

      if (ReadAndValidateTypeHeaderOrNull(name, BinaryTypeCode.Byte1))
        return m_binaryReader.ReadSByte();

      return null;
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public int ReadSByteArray(string name, Span<sbyte> values)
    {
      CheckDisposed();

      if (ReadAndValidateArrayHeader(name, BinaryTypeCode.Byte1, out int count))
      {
        int elemCount = Math.Min(values.Length, count);
        m_binaryReader.ReadExactly(values.Slice(0, elemCount).AsBytes()); // Read as regular bytes

        SeekToEndOfArray(count - elemCount, BinaryTypeCode.Byte1);

        return elemCount;
      }

      return 0;
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public char ReadChar(string name)
    {
      CheckDisposed();

      ReadAndValidateTypeHeader(name, BinaryTypeCode.Char);
      return m_binaryReader.ReadChar();
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public char? ReadNullableChar(string name)
    {
      CheckDisposed();

      if (ReadAndValidateTypeHeaderOrNull(name, BinaryTypeCode.Char))
        return m_binaryReader.ReadChar();

      return null;
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public int ReadCharArray(string name, Span<char> values)
    {
      CheckDisposed();

      if (ReadAndValidateArrayHeader(name, BinaryTypeCode.Char, out int count))
      {
        int elemCount = Math.Min(values.Length, count);
        m_binaryReader.ReadExactly(values.Slice(0, elemCount));

        SeekToEndOfArray(count - elemCount, BinaryTypeCode.Char);

        return elemCount;
      }

      return 0;
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public ushort ReadUInt16(string name)
    {
      CheckDisposed();

      ReadAndValidateTypeHeader(name, BinaryTypeCode.Byte2);
      return m_binaryReader.ReadUInt16();
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public ushort? ReadNullableUInt16(string name)
    {
      CheckDisposed();

      if (ReadAndValidateTypeHeaderOrNull(name, BinaryTypeCode.Byte2))
        return m_binaryReader.ReadUInt16();

      return null;
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public int ReadUInt16Array(string name, Span<ushort> values)
    {
      CheckDisposed();

      if (ReadAndValidateArrayHeader(name, BinaryTypeCode.Byte2, out int count))
      {
        int elemCount = Math.Min(values.Length, count);
        for (int i = 0; i < elemCount; i++)
          values[i] = m_binaryReader.ReadUInt16();

        SeekToEndOfArray(count - elemCount, BinaryTypeCode.Byte2);

        return elemCount;
      }

      return 0;
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public uint ReadUInt32(string name)
    {
      CheckDisposed();

      ReadAndValidateTypeHeader(name, BinaryTypeCode.Byte4);
      return m_binaryReader.ReadUInt32();
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public uint? ReadNullableUInt32(string name)
    {
      CheckDisposed();

      if (ReadAndValidateTypeHeaderOrNull(name, BinaryTypeCode.Byte4))
        return m_binaryReader.ReadUInt32();

      return null;
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public int ReadUInt32Array(string name, Span<uint> values)
    {
      CheckDisposed();

      if (ReadAndValidateArrayHeader(name, BinaryTypeCode.Byte4, out int count))
      {
        int elemCount = Math.Min(values.Length, count);
        for (int i = 0; i < elemCount; i++)
          values[i] = m_binaryReader.ReadUInt32();

        SeekToEndOfArray(count - elemCount, BinaryTypeCode.Byte4);

        return elemCount;
      }

      return 0;
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public ulong ReadUInt64(string name)
    {
      CheckDisposed();

      ReadAndValidateTypeHeader(name, BinaryTypeCode.Byte8);
      return m_binaryReader.ReadUInt64();
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public ulong? ReadNullableUInt64(string name)
    {
      CheckDisposed();

      if (ReadAndValidateTypeHeaderOrNull(name, BinaryTypeCode.Byte8))
        return m_binaryReader.ReadUInt64();

      return null;
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public int ReadUInt64Array(string name, Span<ulong> values)
    {
      CheckDisposed();

      if (ReadAndValidateArrayHeader(name, BinaryTypeCode.Byte8, out int count))
      {
        int elemCount = Math.Min(values.Length, count);
        for (int i = 0; i < elemCount; i++)
          values[i] = m_binaryReader.ReadUInt64();

        SeekToEndOfArray(count - elemCount, BinaryTypeCode.Byte8);

        return elemCount;
      }

      return 0;
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public short ReadInt16(string name)
    {
      CheckDisposed();

      ReadAndValidateTypeHeader(name, BinaryTypeCode.Byte2);
      return m_binaryReader.ReadInt16();
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public short? ReadNullableInt16(string name)
    {
      CheckDisposed();

      if (ReadAndValidateTypeHeaderOrNull(name, BinaryTypeCode.Byte2))
        return m_binaryReader.ReadInt16();

      return null;
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public int ReadInt16Array(string name, Span<short> values)
    {
      CheckDisposed();

      if (ReadAndValidateArrayHeader(name, BinaryTypeCode.Byte2, out int count))
      {
        int elemCount = Math.Min(values.Length, count);
        for (int i = 0; i < elemCount; i++)
          values[i] = m_binaryReader.ReadInt16();

        SeekToEndOfArray(count - elemCount, BinaryTypeCode.Byte2);

        return elemCount;
      }

      return 0;
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public int ReadInt32(string name)
    {
      CheckDisposed();

      ReadAndValidateTypeHeader(name, BinaryTypeCode.Byte4);
      return m_binaryReader.ReadInt32();
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public int? ReadNullableInt32(string name)
    {
      CheckDisposed();

      if (ReadAndValidateTypeHeaderOrNull(name, BinaryTypeCode.Byte4))
        return m_binaryReader.ReadInt32();

      return null;
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public int ReadInt32Array(string name, Span<int> values)
    {
      CheckDisposed();

      if (ReadAndValidateArrayHeader(name, BinaryTypeCode.Byte4, out int count))
      {
        int elemCount = Math.Min(values.Length, count);
        for (int i = 0; i < elemCount; i++)
          values[i] = m_binaryReader.ReadInt32();

        SeekToEndOfArray(count - elemCount, BinaryTypeCode.Byte4);

        return elemCount;
      }

      return 0;
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public long ReadInt64(string name)
    {
      CheckDisposed();

      ReadAndValidateTypeHeader(name, BinaryTypeCode.Byte8);
      return m_binaryReader.ReadInt64();
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public long? ReadNullableInt64(string name)
    {
      CheckDisposed();

      if (ReadAndValidateTypeHeaderOrNull(name, BinaryTypeCode.Byte8))
        return m_binaryReader.ReadInt64();

      return null;
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public int ReadInt64Array(string name, Span<long> values)
    {
      CheckDisposed();

      if (ReadAndValidateArrayHeader(name, BinaryTypeCode.Byte8, out int count))
      {
        int elemCount = Math.Min(values.Length, count);
        for (int i = 0; i < elemCount; i++)
          values[i] = m_binaryReader.ReadInt64();

        SeekToEndOfArray(count - elemCount, BinaryTypeCode.Byte8);

        return elemCount;
      }

      return 0;
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public Half ReadHalf(string name)
    {
      CheckDisposed();

      ReadAndValidateTypeHeader(name, BinaryTypeCode.Byte2);
      return m_binaryReader.ReadHalf();
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public Half? ReadNullableHalf(string name)
    {
      CheckDisposed();

      if (ReadAndValidateTypeHeaderOrNull(name, BinaryTypeCode.Byte2))
        return m_binaryReader.ReadHalf();

      return null;
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public int ReadHalfArray(string name, Span<Half> values)
    {
      CheckDisposed();

      if (ReadAndValidateArrayHeader(name, BinaryTypeCode.Byte2, out int count))
      {
        int elemCount = Math.Min(values.Length, count);
        for (int i = 0; i < elemCount; i++)
          values[i] = m_binaryReader.ReadHalf();

        SeekToEndOfArray(count - elemCount, BinaryTypeCode.Byte2);

        return elemCount;
      }

      return 0;
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public float ReadSingle(string name)
    {
      CheckDisposed();

      ReadAndValidateTypeHeader(name, BinaryTypeCode.Byte4);
      return m_binaryReader.ReadSingle();
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public float? ReadNullableSingle(string name)
    {
      CheckDisposed();

      if (ReadAndValidateTypeHeaderOrNull(name, BinaryTypeCode.Byte4))
        return m_binaryReader.ReadSingle();

      return null;
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public int ReadSingleArray(string name, Span<float> values)
    {
      CheckDisposed();

      if (ReadAndValidateArrayHeader(name, BinaryTypeCode.Byte4, out int count))
      {
        int elemCount = Math.Min(values.Length, count);
        for (int i = 0; i < elemCount; i++)
          values[i] = m_binaryReader.ReadSingle();

        SeekToEndOfArray(count - elemCount, BinaryTypeCode.Byte4);

        return elemCount;
      }

      return 0;
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public double ReadDouble(string name)
    {
      CheckDisposed();

      ReadAndValidateTypeHeader(name, BinaryTypeCode.Byte8);
      return m_binaryReader.ReadDouble();
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public double? ReadNullableDouble(string name)
    {
      CheckDisposed();

      if (ReadAndValidateTypeHeaderOrNull(name, BinaryTypeCode.Byte8))
        return m_binaryReader.ReadDouble();

      return null;
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public int ReadDoubleArray(string name, Span<double> values)
    {
      CheckDisposed();

      if (ReadAndValidateArrayHeader(name, BinaryTypeCode.Byte8, out int count))
      {
        int elemCount = Math.Min(values.Length, count);
        for (int i = 0; i < elemCount; i++)
          values[i] = m_binaryReader.ReadDouble();

        SeekToEndOfArray(count - elemCount, BinaryTypeCode.Byte8);

        return elemCount;
      }

      return 0;
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public decimal ReadDecimal(string name)
    {
      CheckDisposed();

      ReadAndValidateTypeHeader(name, BinaryTypeCode.Byte16);
      return m_binaryReader.ReadDecimal();
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public decimal? ReadNullableDecimal(string name)
    {
      CheckDisposed();

      if (ReadAndValidateTypeHeaderOrNull(name, BinaryTypeCode.Byte16))
        return m_binaryReader.ReadDecimal();

      return null;
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public int ReadDecimalArray(string name, Span<decimal> values)
    {
      CheckDisposed();

      if (ReadAndValidateArrayHeader(name, BinaryTypeCode.Byte16, out int count))
      {
        int elemCount = Math.Min(values.Length, count);
        for (int i = 0; i < elemCount; i++)
          values[i] = m_binaryReader.ReadDecimal();

        SeekToEndOfArray(count - elemCount, BinaryTypeCode.Byte16);

        return elemCount;
      }

      return 0;
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public bool ReadBoolean(string name)
    {
      CheckDisposed();

      ReadAndValidateTypeHeader(name, BinaryTypeCode.Byte1);
      return m_binaryReader.ReadBoolean();
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public bool? ReadNullableBoolean(string name)
    {
      CheckDisposed();

      if (ReadAndValidateTypeHeaderOrNull(name, BinaryTypeCode.Byte1))
        return m_binaryReader.ReadBoolean();

      return null;
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public int ReadBooleanArray(string name, Span<bool> values)
    {
      CheckDisposed();

      if (ReadAndValidateArrayHeader(name, BinaryTypeCode.Byte1, out int count))
      {
        int elemCount = Math.Min(values.Length, count);
        for (int i = 0; i < elemCount; i++)
          values[i] = m_binaryReader.ReadBoolean();

        SeekToEndOfArray(count - elemCount, BinaryTypeCode.Byte1);

        return elemCount;
      }

      return 0;
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public string? ReadString(string name)
    {
      CheckDisposed();

      if (ReadAndValidateTypeHeaderOrNull(name, BinaryTypeCode.String))
        return m_binaryReader.ReadString();

      return null;
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public int ReadStringArray(string name, Span<string?> values)
    {
      CheckDisposed();

      if (ReadAndValidateArrayHeader(name, BinaryTypeCode.String, out int count))
      {
        int elemCount = Math.Min(values.Length, count);
        for (int i = 0; i < elemCount; i++)
          values[i] = (ReadAndValidateTypeHeaderOrNull(name, BinaryTypeCode.String)) ? m_binaryReader.ReadString() : null;

        SeekToEndOfArray(count - elemCount, BinaryTypeCode.String);

        return elemCount;
      }

      return 0;
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public void Read<T>(string name, out T value) where T : struct, IPrimitiveValue
    {
      CheckDisposed();

      ReadAndValidateTypeHeader(name, BinaryTypeCode.PrimitiveValue);

      value = default;
      value.Read(this);

      ReadAndValidateEndOfType(name, BinaryTypeCode.EndPrimitiveValue);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public void ReadNullable<T>(string name, out T? value) where T : struct, IPrimitiveValue
    {
      CheckDisposed();

      value = null;

      if (ReadAndValidateTypeHeaderOrNull(name, BinaryTypeCode.PrimitiveValue))
      {
        T val = default;
        val.Read(this);
        value = val;

        ReadAndValidateEndOfType(name, BinaryTypeCode.EndPrimitiveValue);
      }
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public int ReadArray<T>(string name, Span<T> values) where T : struct, IPrimitiveValue
    {
      CheckDisposed();

      if (ReadAndValidateArrayHeader(name, BinaryTypeCode.PrimitiveValue, out int count))
      {
        int elemCount = Math.Min(values.Length, count);
        for (int i = 0; i < elemCount; i++)
        {
          ref T value = ref values[i];
          value.Read(this);
        }

        SeekToEndOfArray(count - elemCount, BinaryTypeCode.PrimitiveValue);

        return elemCount;
      }

      return 0;
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public T ReadEnum<T>(string name) where T : struct, Enum
    {
      CheckDisposed();

      BinaryTypeCode typeCode = (BinaryTypeCode) m_binaryReader.ReadByte();
      return ReadEnumInternal<T>(name, typeCode);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public T? ReadNullableEnum<T>(string name) where T : struct, Enum
    {
      CheckDisposed();

      BinaryTypeCode typeCode = (BinaryTypeCode) m_binaryReader.ReadByte();
      if (typeCode == BinaryTypeCode.NullObject)
        return null;

      return ReadEnumInternal<T>(name, typeCode);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    private T ReadEnumInternal<T>(string name, BinaryTypeCode actualType) where T : struct, Enum
    {
      TypeCode typeCode = Type.GetTypeCode(typeof(T));
    
      switch (typeCode)
      {
        case TypeCode.Byte:
          BinaryTypeCodeHelper.ThrowIfInvalidType(name, actualType, BinaryTypeCode.Byte1);

          return BufferHelper.CastToEnum<byte, T>(m_binaryReader.ReadByte());
        case TypeCode.SByte:
          BinaryTypeCodeHelper.ThrowIfInvalidType(name, actualType, BinaryTypeCode.Byte1);

          return BufferHelper.CastToEnum<sbyte, T>(m_binaryReader.ReadSByte());
        case TypeCode.Int16:
          BinaryTypeCodeHelper.ThrowIfInvalidType(name, actualType, BinaryTypeCode.Byte2);

          return BufferHelper.CastToEnum<short, T>(m_binaryReader.ReadInt16());
        case TypeCode.UInt16:
          BinaryTypeCodeHelper.ThrowIfInvalidType(name, actualType, BinaryTypeCode.Byte2);

          return BufferHelper.CastToEnum<ushort, T>(m_binaryReader.ReadUInt16());
        case TypeCode.Int32:
          BinaryTypeCodeHelper.ThrowIfInvalidType(name, actualType, BinaryTypeCode.Byte4);

          return BufferHelper.CastToEnum<int, T>(m_binaryReader.ReadInt32());
        case TypeCode.UInt32:
          BinaryTypeCodeHelper.ThrowIfInvalidType(name, actualType, BinaryTypeCode.Byte4);

          return BufferHelper.CastToEnum<uint, T>(m_binaryReader.ReadUInt32());
        case TypeCode.Int64:
          BinaryTypeCodeHelper.ThrowIfInvalidType(name, actualType, BinaryTypeCode.Byte8);

          return BufferHelper.CastToEnum<long, T>(m_binaryReader.ReadInt64());
        case TypeCode.UInt64:
          BinaryTypeCodeHelper.ThrowIfInvalidType(name, actualType, BinaryTypeCode.Byte8);

          return BufferHelper.CastToEnum<ulong, T>(m_binaryReader.ReadUInt64());
        default:
          throw new InvalidCastException(StringLocalizer.Instance.GetLocalizedString("TokenNotAnEnum", name, typeof(T).FullName));
      }
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public int PeekArrayCount(string name)
    {
      CheckDisposed();

      int count = 0;
      long oldPos = InStream.Position;

      try
      {
        BinaryTypeCode typeCode = (BinaryTypeCode) m_binaryReader.ReadByte();
        if (typeCode == BinaryTypeCode.Array || typeCode == BinaryTypeCode.GroupWithCount)
          count = m_binaryReader.Read7BitEncodedInt();
      }
      finally
      {
        InStream.Position = oldPos;
      }

      return Math.Max(0, count);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public PrimitiveBlobHeader? PeekBlob(string name)
    {
      CheckDisposed();

      long oldPos = InStream.Position;
      try
      {
        BinaryTypeCode typeCode = (BinaryTypeCode) m_binaryReader.ReadByte();
        if (typeCode != BinaryTypeCode.Blob)
          return null;

        int elemCount = m_binaryReader.Read7BitEncodedInt();
        int elemSize = m_binaryReader.Read7BitEncodedInt();
        Type elemType = ReadBlobType();

        return new PrimitiveBlobHeader(elemCount, elemSize, elemType);
      } 
      finally
      {
        InStream.Position = oldPos;
      }
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public int ReadBlob<T>(string name, Span<T> values) where T : unmanaged
    {
      CheckDisposed();

      if (ReadAndValidateBlobHeader(name, out PrimitiveBlobHeader blobHeader))
      {
        Span<byte> dstBytes = values.AsBytes();

        int bytesToRead = Math.Min(dstBytes.Length, blobHeader.BlobSizeInBytes);
        m_binaryReader.ReadExactly(dstBytes.Slice(0, bytesToRead));

        // Skip any bits of the blob not read
        int remainingBytes = blobHeader.BlobSizeInBytes - bytesToRead;
        if (remainingBytes > 0)
          m_binaryReader.SkipReadBytes(remainingBytes);

        return BufferHelper.ComputeElementCount<T>(bytesToRead);
      }

      return 0;
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public int BeginReadGroup(string name)
    {
      CheckDisposed();

      BinaryTypeCode typeCode = (BinaryTypeCode) m_binaryReader.ReadByte();
      BinaryTypeCodeHelper.ThrowIfNeitherTypes(name, typeCode, BinaryTypeCode.GroupWithCount, BinaryTypeCode.GroupNoCount);

      if (typeCode == BinaryTypeCode.GroupWithCount)
        return m_binaryReader.Read7BitEncodedInt();

      return 0;
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public void EndReadGroup()
    {
      CheckDisposed();

      ReadAndValidateEndOfType(null, BinaryTypeCode.EndGroup);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader is disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the data to be read cannot be read as the requested type.</exception>
    public void Skip(string name)
    {
      SkipNextToken();
    }

    /// <summary>
    /// Skips the next token in the stream.
    /// </summary>
    /// <returns>Token that was skipped.</returns>
    protected BinaryTypeCode SkipNextToken()
    {
      BinaryTypeCode currToken = (BinaryTypeCode) m_binaryReader.ReadByte();

      switch (currToken)
      {
        // Fixed size
        case BinaryTypeCode.Byte1:
        case BinaryTypeCode.Byte2:
        case BinaryTypeCode.Byte4:
        case BinaryTypeCode.Byte8:
        case BinaryTypeCode.Byte16:
        case BinaryTypeCode.Byte32:
          m_binaryReader.SkipReadBytes(BinaryTypeCodeHelper.GetSizeInBytes(currToken));
          break;
        // Variable size due to encoding
        case BinaryTypeCode.Char:
          m_binaryReader.ReadChar();
          break;
        // Variable size due to encoding
        case BinaryTypeCode.String:
          m_binaryReader.SkipReadString();
          break;
        // Always 7-bit encoded
        case BinaryTypeCode.ExternalSavable:
        case BinaryTypeCode.SharedSavable:
          m_binaryReader.Read7BitEncodedInt();
          break;
        // Variable sizes
        case BinaryTypeCode.GroupNoCount:
        case BinaryTypeCode.GroupWithCount:
        case BinaryTypeCode.PrimitiveValue:
        case BinaryTypeCode.Savable:
          SkipGrouping(currToken, BinaryTypeCodeHelper.GetEndOfTypeCode(currToken));
          break;
        case BinaryTypeCode.Array:
          SkipArray();
          break;
        case BinaryTypeCode.Blob:
          SkipBlob();
          break;
        // Any other tokens have zero size, code was read and there's nothing else to do.
      }

      return currToken;
    }

    /// <summary>
    /// Skips the content of a grouping (group w/ count, group, or savable). Groupings always have start/end markers.
    /// </summary>
    /// <param name="beginCode">Header code that marks the beginning.</param>
    /// <param name="endCode">Footer code that marks the ending.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected void SkipGrouping(BinaryTypeCode beginCode, BinaryTypeCode endCode)
    {
      // Assume header code byte was read. If group w/ count, we have a count to read. If savable, we have a type index to read.
      if (beginCode == BinaryTypeCode.GroupWithCount || beginCode == BinaryTypeCode.Savable)
        m_binaryReader.Read7BitEncodedInt();

      SeekToEndOfType(endCode);
    }

    /// <summary>
    /// Seeks to the specified end marker, signifying the end of the token.
    /// </summary>
    /// <param name="endCode">Type's end marker.</param>
    /// <exception cref="InvalidOperationException">Thrown if we have a dangling end marker.</exception>
    protected void SeekToEndOfType(BinaryTypeCode endCode)
    {
      m_skipDepth++;
      int nestedDepth = m_skipDepth;

      // Keep skipping until the next token is our end token, and nesting depth is the same as we started
      BinaryTypeCode nextToken;
      do
      {
        nextToken = SkipNextToken();
      } while (!(m_skipDepth == nestedDepth && nextToken == endCode));

      if (m_skipDepth == 0)
        throw new InvalidOperationException("Missing an end type marker.");

      m_skipDepth--;
    }

    /// <summary>
    /// Seeks to the end of an array token.
    /// </summary>
    /// <param name="itemCountToSkip">Number of items to skip, if the array has been partially read.</param>
    /// <param name="itemType">Array's item type.</param>
    protected void SeekToEndOfArray(int itemCountToSkip, BinaryTypeCode itemType)
    {
      // Assume entire header has been read
      if (itemCountToSkip <= 0)
      {
        ReadAndValidateEndOfType(String.Empty, BinaryTypeCode.EndArray);
        return;
      }

      // Skip over any bulk fixed data (e.g. no header per item)
      switch (itemType)
      {
        case BinaryTypeCode.Byte1:
        case BinaryTypeCode.Byte2:
        case BinaryTypeCode.Byte4:
        case BinaryTypeCode.Byte8:
        case BinaryTypeCode.Byte16:
        case BinaryTypeCode.Byte32:
          m_binaryReader.SkipReadBytes(itemCountToSkip * BinaryTypeCodeHelper.GetSizeInBytes(itemType));
          break;
        case BinaryTypeCode.Char:
          m_binaryReader.SkipReadChars(itemCountToSkip);
          break;
      }

      // Seek until we find the closing end array marker, this will skip over all tokens with a header if a complex item type
      SeekToEndOfType(BinaryTypeCode.EndArray);
    }

    /// <summary>
    /// Skips the contents of an array completely. It is assumed the type code has been read, but not other header properties.
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected void SkipArray()
    {
      // Assume header code byte was read, which leaves the other array header properties to read
      ReadArrayHeaderProperties(out int count, out BinaryTypeCode itemType);

      SeekToEndOfArray(count, itemType);
    }

    /// <summary>
    /// Skips the contents of a blob completely. It is assumed the type code has been read, but not other header properties.
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected void SkipBlob()
    {
      // Assume header code byte has been read, which leaves the other blob header properties to read
      ReadBlobHeaderProperties(out PrimitiveBlobHeader blobHeader);

      m_binaryReader.SkipReadBytes(blobHeader.BlobSizeInBytes);
    }


    /// <inheritdoc />
    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }


    /// <summary>
    /// Releases unmanaged and - optionally - managed resources.
    /// </summary>
    /// <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
    protected virtual void Dispose(bool disposing)
    {
      if (!m_isDisposed)
      {
        if (disposing)
          m_binaryReader.Dispose();

        m_isDisposed = true;
      }
    }

    /// <summary>
    /// Used by subclassed readers to mark us disposed, if overriding the dispose method.
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected void MarkIsDisposed()
    {
      m_isDisposed = true;
    }

    /// <summary>
    /// Throws an exception if the current writer has been disposed.
    /// </summary>
    /// <exception cref="ObjectDisposedException">Thrown if the reader has been disposed.</exception>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected void CheckDisposed()
    {
      if (m_isDisposed)
        throw new ObjectDisposedException(GetType().Name);
    }

    /// <summary>
    /// Reads and validates the next token's type's header.
    /// </summary>
    /// <param name="name">Property name, for error reporting if necessary.</param>
    /// <param name="expectedType">Expected type.</param>
    /// <exception cref="InvalidCastException">Thrown if the type header is not the expected type.</exception>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected void ReadAndValidateTypeHeader(string name, BinaryTypeCode expectedType)
    {
      BinaryTypeCode actualType = (BinaryTypeCode) m_binaryReader.ReadByte();
      BinaryTypeCodeHelper.ThrowIfInvalidType(name, actualType, expectedType);
    }

    /// <summary>
    /// Reads and validates the next token's type header, allowing for it to be either type.
    /// </summary>
    /// <param name="name">Property name, for error reporting if necessary.</param>
    /// <param name="expected1">First type.</param>
    /// <param name="expected2">Second type.</param>
    /// <param name="actualType">Actual type out.</param>
    /// <exception cref="InvalidCastException">Thrown if the type is not either type.</exception>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected void ReadAndValidateEitherTypeHeader(string name, BinaryTypeCode expected1, BinaryTypeCode expected2, out BinaryTypeCode actualType)
    {
      actualType = (BinaryTypeCode) m_binaryReader.ReadByte();
      BinaryTypeCodeHelper.ThrowIfNeitherTypes(name, actualType, expected1, expected2);
    }

    /// <summary>
    /// Reads and validates a type's end marker.
    /// </summary>
    /// <param name="name">Property name, for error reporting if necessary.</param>
    /// <param name="expected">Expected end marker.</param>
    /// <exception cref="InvalidCastException">Thrown if the type end marker is not the expected type.</exception>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected void ReadAndValidateEndOfType(string? name, BinaryTypeCode expected)
    {
      BinaryTypeCode actual = (BinaryTypeCode) m_binaryReader.ReadByte();
      BinaryTypeCodeHelper.ThrowIfInvalidType(name, actual, expected);
    }

    /// <summary>
    /// Reads and validates if the next token is of the specified type or null.
    /// </summary>
    /// <param name="name">Property name, for error reporting if necessary.</param>
    /// <param name="expectedUnderlyingType">Expected type.</param>
    /// <returns>True if the type exists, false if null.</returns>
    /// <exception cref="InvalidCastException">Thrown if the next token is not the expected type.</exception>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected bool ReadAndValidateTypeHeaderOrNull(string name, BinaryTypeCode expectedUnderlyingType)
    {
      BinaryTypeCode actualType = (BinaryTypeCode) m_binaryReader.ReadByte();
      // Allow null
      if (actualType == BinaryTypeCode.NullObject)
        return false;

      // If not null, must be the expected type
      BinaryTypeCodeHelper.ThrowIfInvalidType(name, actualType, expectedUnderlyingType);

      return true;
    }

    /// <summary>
    /// Reads and validates if the next token is of either types or null.
    /// </summary>
    /// <param name="name">Property name, for error reporting if necessary.</param>
    /// <param name="expected1">First type.</param>
    /// <param name="expected2">Second type.</param>
    /// <param name="actualType">Actual type out.</param>
    /// <returns>True if the type exists, false if null.</returns>
    /// <exception cref="InvalidCastException">Thrown if the next token is neither type.</exception>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected bool ReadAndValidateEitherTypeHeaderOrNull(string name, BinaryTypeCode expected1, BinaryTypeCode expected2, out BinaryTypeCode actualType)
    {
      actualType = (BinaryTypeCode) m_binaryReader.ReadByte();
      // Allow null
      if (actualType == BinaryTypeCode.NullObject)
        return false;

      // If not null, must be one of the two types
      BinaryTypeCodeHelper.ThrowIfNeitherTypes(name, actualType, expected1, expected2);

      return true;
    }

    /// <summary>
    /// Reads and validates an array for the item type, or null.
    /// </summary>
    /// <param name="name">Property name, for error reporting if necessary.</param>
    /// <param name="expectedItemType">Item type.</param>
    /// <param name="count">Array count out.</param>
    /// <returns>True if the array exists, false if null.</returns>
    /// <exception cref="InvalidCastException">Thrown if the next token is not an array or the item type is not expected.</exception>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected bool ReadAndValidateArrayHeader(string name, BinaryTypeCode expectedItemType, out int count)
    {
      count = 0;

      BinaryTypeCode arrayType = (BinaryTypeCode) m_binaryReader.ReadByte();
      if (arrayType == BinaryTypeCode.NullObject)
        return false;

      BinaryTypeCodeHelper.ThrowIfInvalidType(name, arrayType, BinaryTypeCode.Array);

      ReadArrayHeaderProperties(out count, out BinaryTypeCode itemType);
      BinaryTypeCodeHelper.ThrowIfInvalidType(name, itemType, expectedItemType);

      return true;
    }

    /// <summary>
    /// Reads and validates an array for either item types, or null.
    /// </summary>
    /// <param name="name">Property name, for error reporting if necessary.</param>
    /// <param name="expectedItemType1">First item type.</param>
    /// <param name="expectedItemType2">Second item type.</param>
    /// <param name="itemType">Actual item type out.</param>
    /// <param name="count">Array count out.</param>
    /// <returns>True if the array exists, false if null.</returns>
    /// <exception cref="InvalidCastException">Thrown if the next token is not an array or the item type is not expected.</exception>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected bool ReadAndValidateArrayHeaderForEither(string name, BinaryTypeCode expectedItemType1, BinaryTypeCode expectedItemType2, out BinaryTypeCode itemType, out int count)
    {
      count = 0;
      itemType = BinaryTypeCode.NullObject;

      BinaryTypeCode arrayType = (BinaryTypeCode) m_binaryReader.ReadByte();
      if (arrayType == BinaryTypeCode.NullObject)
        return false;

      BinaryTypeCodeHelper.ThrowIfInvalidType(name, arrayType, BinaryTypeCode.Array);

      ReadArrayHeaderProperties(out count, out itemType);
      BinaryTypeCodeHelper.ThrowIfNeitherTypes(name, itemType, expectedItemType1, expectedItemType2);

      return true;
    }

    /// <summary>
    /// Reads an array type from the stream. Does not invalidate.
    /// </summary>
    /// <param name="count">Number of array items out.</param>
    /// <param name="itemType">Array item type out.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected void ReadArrayHeaderProperties(out int count, out BinaryTypeCode itemType)
    {
      count = m_binaryReader.Read7BitEncodedInt();
      itemType = (BinaryTypeCode) m_binaryReader.ReadByte();
    }

    /// <summary>
    /// Reads and validate blob header properties from the stream.
    /// </summary>
    /// <param name="name">Property name, for error reporting if necessary.</param>
    /// <param name="blobHeader">Blob header out.</param>
    /// <returns>True if the next token is valid, false if null.</returns>
    /// <exception cref="InvalidCastException">Thrown if the token is not a blob type.</exception>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected bool ReadAndValidateBlobHeader(string name, out PrimitiveBlobHeader blobHeader)
    {
      blobHeader = default;

      BinaryTypeCode blobType = (BinaryTypeCode) m_binaryReader.ReadByte();
      if (blobType == BinaryTypeCode.NullObject)
        return false;

      BinaryTypeCodeHelper.ThrowIfInvalidType(name, blobType, BinaryTypeCode.Blob);

      ReadBlobHeaderProperties(out blobHeader);

      return true;
    }

    /// <summary>
    /// Reads blob header properties from the stream. Does not validate.
    /// </summary>
    /// <param name="blobHeader">Blob header.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected void ReadBlobHeaderProperties(out PrimitiveBlobHeader blobHeader)
    {
      int elemCount = m_binaryReader.Read7BitEncodedInt();
      int elemSize = m_binaryReader.Read7BitEncodedInt();
      Type elemType = ReadBlobType();

      blobHeader = new PrimitiveBlobHeader(elemCount, elemSize, elemType);
    }

    /// <summary>
    /// Reads the blob element type from the stream.
    /// </summary>
    /// <returns>Blob element type.</returns>
    protected virtual Type ReadBlobType()
    {
      string typeName = m_binaryReader.ReadString();
      return SmartActivator.GetType(typeName);
    }
  }
}
