﻿/*
* Copyright (c) 2010-2024 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.IO;

#nullable enable

namespace Tesla.Content
{
  /// <summary>
  /// Default external reference writer for binary (TEBO) files.
  /// </summary>
  public class BinaryExternalWriter : IExternalReferenceWriter
  {
    private Type m_targetType;

    /// <inheritdoc />
    public Type TargetType { get { return m_targetType; } }

    /// <inheritdoc />
    public string ResourceExtension { get { return ".tebo"; } }

    /// <summary>
    /// Constructs a new instance of the <see cref="BinaryExternalWriter"/> class.
    /// </summary>
    /// <param name="type">Resource type to write externally. If not specified, type is <see cref="ISavable"/>.</param>
    public BinaryExternalWriter(Type? type = null)
    {
      m_targetType = type ?? typeof(ISavable);
    }

    /// <inheritdoc />
    /// <exception cref="ArgumentNullException">Thrown if the output resource file, external handler or savable is null.</exception>
    /// <exception cref="ArgumentException">Thrown if the output resource file is not the same as what the writer expects.</exception>
    public void WriteSavable<T>(IResourceFile outputResourceFile, IExternalReferenceHandler externalHandler, T value) where T : class, ISavable
    {
      ArgumentNullException.ThrowIfNull(outputResourceFile, nameof(outputResourceFile));
      ArgumentNullException.ThrowIfNull(externalHandler, nameof(externalHandler));
      ArgumentNullException.ThrowIfNull(value, nameof(value));

      if (!m_targetType.IsAssignableFrom(value.GetType()))
        throw new InvalidCastException(StringLocalizer.Instance.GetLocalizedString("TypeMismatch", m_targetType.Name, value.GetType().Name));

      if (outputResourceFile.Extension != ResourceExtension)
        throw new ArgumentException(StringLocalizer.Instance.GetLocalizedString("InvalidResourceExtension", ResourceExtension, outputResourceFile.Extension));

      Stream output = outputResourceFile.OpenWrite();

      using (BinarySavableWriter binaryWriter = new BinarySavableWriter(output, externalHandler.WriteFlags, externalHandler))
        binaryWriter.WriteRootSavable<ISavable>(value);
    }
  }
}
