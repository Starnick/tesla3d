﻿/*
* Copyright (c) 2010-2024 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Runtime.CompilerServices;

#nullable enable

namespace Tesla.Content
{
  /// <summary>
  /// Headers for tokens in a binary stream. Each token is categorized by an op-code, which then usually has some bit of data that follows.
  /// Some are fixed sized data, some variable, and some allow for nested op-codes (such as arrays, groups) that will be book-ended by a start/end op-code.
  /// </summary>
  public enum BinaryTypeCode : byte
  {
    /// <summary>
    /// Represents a null object. No additional data.
    /// </summary>
    NullObject = 0,

    /// <summary>
    /// Represents an array of objects (fixed or variable sized). An array "header" has two values, followed by the array objects. The two values in the header
    /// are 7-bit encoded ints indicating the element count and a <see cref="BinaryTypeCode"/> for the element type. The end of an array
    /// is indicated by a <see cref="EndArray"/> op code.
    /// </summary>
    Array = 1,

    /// <summary>
    /// Indicates the end of an <see cref="BinaryTypeCode.Array"/> object.
    /// </summary>
    EndArray = 2,

    /// <summary>
    /// Represents a blob of bytes with type information. A blob "header" has three values, followed by the byte data. The first two values in the header
    /// are 7-bit encoded ints indicating the element count and element size (in bytes). The third value is type information, which can be the fully qualified type string
    /// or an index into a type array (depending on implementation).
    /// </summary>
    Blob = 3,

    /// <summary>
    /// Indicates the beginning of a grouping that acts like an array, the next value is a 7-bit encoded int indicating the number of objects
    /// in the group. The end of a group is indicated by a <see cref="EndGroup"/> op code.
    /// </summary>
    GroupWithCount = 4,

    /// <summary>
    /// Indicates the beginning of a grouping that simply acts as a container. No additional data - while groups allow serializers to customize
    /// formatting, in binary serialization it is a no-op. The end of a group is indicated by a <see cref="EndGroup"/> op code.
    /// </summary>
    GroupNoCount = 5,

    /// <summary>
    /// Indicates the end of a <see cref="GroupWithCount"/> or <see cref="GroupNoCount"/>.
    /// </summary>
    EndGroup = 6,

    /// <summary>
    /// Represents a string. The next value is a 7-bit encoded int representing the string length in bytes, followed by the bytes representing the characters. Encoding depends on implementation.
    /// </summary>
    String = 7,

    /// <summary>
    /// Represents a single <see cref="System.Char"/>. Encoding depends on implementation.
    /// </summary>
    Char = 8,

    /// <summary>
    /// Represents any blittable type the size of a single byte (e.g. <see cref="System.Byte"/> and <see cref="System.Boolean"/>).
    /// </summary>
    Byte1 = 9,

    /// <summary>
    /// Represents any blittable type the size of two bytes (e.g. <see cref="System.Int16"/>).
    /// </summary>
    Byte2 = 10,

    /// <summary>
    /// Represents any blittable type the size of four bytes (e.g. <see cref="System.Int32"/> and <see cref="System.Single"/>).
    /// </summary>
    Byte4 = 11,

    /// <summary>
    /// Represents any blittable type the size of eight bytes (e.g. <see cref="System.Int64"/> and <see cref="System.Double"/>).
    /// </summary>
    Byte8 = 12,

    /// <summary>
    /// Represents any blittable type the size of sixteen bytes (e.g. <see cref="System.Decimal"/>).
    /// </summary>
    Byte16 = 13,

    /// <summary>
    /// Represents any blittable type the size of thirty-two bytes.
    /// </summary>
    Byte32 = 14,

    /// <summary>
    /// Indicates the beginning of an <see cref="IPrimitiveValue"/> struct. The contents determined by the type, which may not necessarily be
    /// blittable or wants to control how it is formatted. The end of the object is indicated by a <see cref="EndPrimitiveValue"/>.
    /// </summary>
    PrimitiveValue = 15,

    /// <summary>
    /// Indicates the end of an <see cref="PrimitiveValue"/> object.
    /// </summary>
    EndPrimitiveValue = 16,

    /// <summary>
    /// Indicates the beginning of an <see cref="ISavable"/> object. The contents determined by the type. The end of the object is indicated by a <see cref="EndSavable"/>.
    /// </summary>
    Savable = 17,

    /// <summary>
    /// Indicates the end of an <see cref="Savable"/> object.
    /// </summary>
    EndSavable = 18,

    /// <summary>
    /// Represents a shared savable, which are for objects that should not be duplicated. Shared data is stored as an embedded
    /// array of objects in the serialized data. The next value is a 7-bit encoded int that is an index into that array.
    /// </summary>
    SharedSavable = 19,

    /// <summary>
    /// Represents an externally referenced savable, which like shared savables are objects that should not be duplicated. The serialized
    /// data will have an array of external references that encode the resource path of each object. The next value is a 7-bit encoded int that is an index
    /// into that array.
    /// </summary>
    ExternalSavable = 20
  }

  /// <summary>
  /// Helper for dealing with binary serialization.
  /// </summary>
  public static class BinaryTypeCodeHelper
  {
    /// <summary>
    /// Gets the type of end op-code given the value type. Examples are: Arrays, Groups, PrimitiveValue, Savable.
    /// </summary>
    /// <param name="typeCode">Type op code.</param>
    /// <returns>End op-code for the type.</returns>
    /// <exception cref="ArgumentException">Thrown if the type does not have an "end".</exception>
    public static BinaryTypeCode GetEndOfTypeCode(BinaryTypeCode typeCode)
    {
      switch (typeCode)
      {
        case BinaryTypeCode.Array:
          return BinaryTypeCode.EndArray;
        case BinaryTypeCode.GroupNoCount:
        case BinaryTypeCode.GroupWithCount:
          return BinaryTypeCode.EndGroup;
        case BinaryTypeCode.PrimitiveValue:
          return BinaryTypeCode.EndPrimitiveValue;
        case BinaryTypeCode.Savable:
          return BinaryTypeCode.EndSavable;
        default:
          throw new ArgumentException(typeCode.ToString());
      }
    }

    /// <summary>
    /// Gets the size in bytes of the given type, if it has a fixed size.
    /// </summary>
    /// <param name="typeCode">Type op code.</param>
    /// <returns>Size in bytes, if the type has a fixed size. Will be zero if it is group, null object, or "end" op code. Will be -1 if it does not have a fixed size.</returns>
    public static int GetSizeInBytes(BinaryTypeCode typeCode)
    {
      switch (typeCode)
      {
        case BinaryTypeCode.Byte1:
          return 1;
        case BinaryTypeCode.Byte2:
          return 2;
        case BinaryTypeCode.Byte4:
          return 4;
        case BinaryTypeCode.Byte8:
          return 8;
        case BinaryTypeCode.Byte16:
          return 16;
        case BinaryTypeCode.Byte32:
          return 32;
        case BinaryTypeCode.GroupNoCount:
        case BinaryTypeCode.EndGroup:
        case BinaryTypeCode.EndPrimitiveValue:
        case BinaryTypeCode.EndSavable:
        case BinaryTypeCode.NullObject:
          return 0;
        // Return -1 for types without a fixed size. Char is determined by encoding, primitives and savables can be any size.
        // The rest have variable length counts that are not fixed, and potentially other data (in case of arrays/blobs)
        default:
          return -1;
      }
    }

    /// <summary>
    /// Converts a <see cref="TypeCode"/> to a <see cref="BinaryTypeCode"/>.
    /// </summary>
    /// <param name="typeCode">Type code.</param>
    /// <returns>Binary type code.</returns>
    /// <exception cref="InvalidCastException">Thrown if the code cannot be converted.</exception>
    public static BinaryTypeCode ToBinaryTypeCode(TypeCode typeCode)
    {
      switch (typeCode)
      {
        case TypeCode.Char:
          return BinaryTypeCode.Char;
        case TypeCode.Byte:
        case TypeCode.SByte:
        case TypeCode.Boolean:
          return BinaryTypeCode.Byte1;
        case TypeCode.UInt16:
        case TypeCode.Int16:
          return BinaryTypeCode.Byte2;
        case TypeCode.UInt32:
        case TypeCode.Int32:
        case TypeCode.Single:
          return BinaryTypeCode.Byte4;
        case TypeCode.UInt64:
        case TypeCode.Int64:
        case TypeCode.Double:
          return BinaryTypeCode.Byte8;
        case TypeCode.Decimal:
          return BinaryTypeCode.Byte16;
        case TypeCode.String:
          return BinaryTypeCode.String;
        default:
          throw new InvalidCastException(typeCode.ToString());
      }
    }

    /// <summary>
    /// Throws an exception if the op code does not match an expected value.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="actual">Actual op code from the stream.</param>
    /// <param name="expected">Expected op code.</param>
    /// <exception cref="InvalidCastException">Thrown if the actual op code does not match the expected op code.</exception>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void ThrowIfInvalidType(string? name, BinaryTypeCode actual, BinaryTypeCode expected)
    {
      if (expected == actual)
        return;

      string msg = (name is not null) ?
        StringLocalizer.Instance.GetLocalizedString("TokenMismatch_Named", name, actual.ToString(), expected.ToString())
        : StringLocalizer.Instance.GetLocalizedString("TokenMismatch", actual.ToString(), expected.ToString());

      throw new InvalidCastException(msg);
    }

    /// <summary>
    /// Throws an exception if the op code does not match either expected value.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="actual">Actual op code from the stream.</param>
    /// <param name="expected1">One possible expected op code.</param>
    /// <param name="expected2">Another possible expected op code.</param>
    /// <exception cref="InvalidCastException">Thrown if the actual op code does not match either expected op codes.</exception>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void ThrowIfNeitherTypes(string? name, BinaryTypeCode actual, BinaryTypeCode expected1, BinaryTypeCode expected2)
    {
      if (actual == expected1 || actual == expected2)
        return;

      string expected = $"{expected1.ToString()} or {expected2.ToString()}";

      string msg = (name is not null) ?
        StringLocalizer.Instance.GetLocalizedString("TokenMismatch_Named", name, actual.ToString(), expected)
        : StringLocalizer.Instance.GetLocalizedString("TokenMismatch", actual.ToString(), expected);

      throw new InvalidCastException(msg);
    }

    /// <summary>
    /// Gets the number of bytes needed to 7-bit encode the value.
    /// </summary>
    /// <param name="value">Integer value.</param>
    /// <returns>Number of bytes.</returns>
    public static int Get7BitEncodedIntSize(int value)
    {
      int sizeInBytes = 0;
      uint uValue = (uint) value;

      while (uValue > 0x7Fu)
      {
        sizeInBytes++;
        uValue >>= 7;
      }

      sizeInBytes++;
      return sizeInBytes;
    }

    /// <summary>
    /// Gets the number of bytes needed to 7-bit encode the value.
    /// </summary>
    /// <param name="value">Integer value.</param>
    /// <returns>Number of bytes.</returns>
    public static int Get7BitEncodedInt64Size(long value)
    {
      int sizeInBytes = 0;
      ulong uValue = (ulong) value;

      while (uValue > 0x7Fu)
      {
        sizeInBytes++;
        uValue >>= 7;
      }

      sizeInBytes++;
      return sizeInBytes;
    }
  }
}
