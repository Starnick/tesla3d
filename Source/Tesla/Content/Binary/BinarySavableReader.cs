﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Reflection.Metadata;
using Tesla.Utilities;

#nullable enable

namespace Tesla.Content
{
  /// <summary>
  /// Reads <see cref="ISavable"/> objects as tokenized binary data from a stream
  /// </summary>
  public sealed class BinarySavableReader : BinaryPrimitiveReader, ISavableReader
  {
    private const int VERSION_NUMBER = 3;
    private static readonly byte[] MAGIC_WORD = new byte[] { 84, 69, 66, 79 }; //TEBO characters

    private IServiceProvider m_serviceProvider;
    private IExternalReferenceResolver? m_externalResolver;

    private BinaryReader? m_cachedReader;
    private SharedSavableEntry[]? m_sharedData;
    private string[]? m_externalReferencePaths;
    private TypeVersionTable m_versionTable;
    private int m_version;

    /// <inheritdoc />
    public IServiceProvider ServiceProvider { get { return m_serviceProvider; } }

    /// <inheritdoc />
    public IExternalReferenceResolver? ExternalResolver { get { return m_externalResolver; } }

    /// <summary>
    /// Constructs a new instance of the <see cref="BinarySavableReader"/> class.
    /// </summary>
    /// <param name="serviceProvider">Service provider used to locate any services needed during loading. If null, an empty service provider is created.</param>
    /// <param name="input">Input stream to read from.</param>
    /// <param name="leaveOpen">True if the output stream should NOT be disposed/closed when the reader is disposed/closed, false otherwise.</param>
    /// <exception cref="ArgumentNullException">Thrown if the stream is null.</exception>
    /// <exception cref="NotSupportedException">Thrown if the stream cannot be read from or does not support seeking.</exception>
    public BinarySavableReader(IServiceProvider? serviceProvider, Stream inputStream, bool leaveOpen = false) : this(serviceProvider, inputStream, null, leaveOpen) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="BinarySavableReader"/> class.
    /// </summary>
    /// <param name="serviceProvider">Service provider used to locate any services needed during loading. If null, an empty service provider is created.</param>
    /// <param name="input">Input stream to read from.</param>
    /// <param name="externalResolver">Optional external reference resolver to locate objects external to this stream to load.</param>
    /// <param name="leaveOpen">True if the output stream should NOT be disposed/closed when the reader is disposed/closed, false otherwise.</param>
    /// <exception cref="ArgumentNullException">Thrown if the stream is null.</exception>
    /// <exception cref="NotSupportedException">Thrown if the stream cannot be read from or does not support seeking.</exception>
    public BinarySavableReader(IServiceProvider? serviceProvider, Stream inputStream, IExternalReferenceResolver? externalResolver, bool leaveOpen = false)
      : base(inputStream, leaveOpen)
    {
      m_serviceProvider = serviceProvider ?? new NullServiceProvider();
      m_externalResolver = externalResolver;
      m_versionTable = new TypeVersionTable();
      m_version = VERSION_NUMBER;

      PrepareStream();
    }

    /// <inheritdoc />
    public int GetVersion(Type type)
    {
      TypeVersionTable.Entry? vEntry = m_versionTable.GetVersion(type);
      if (vEntry.HasValue)
        return vEntry.Value.PersistedVersion;

      return -1;
    }

    /// <summary>
    /// Reads the root savable object from the input. This is mainly a convienence method as multiple objects could have been written out,
    /// but this ignores the object's name.
    /// </summary>
    /// <typeparam name="T">Type of object to read.</typeparam>
    /// <returns>Read <see cref="ISavable"/> object.</returns>
    /// <exception cref="ObjectDisposedException">Thrown if the reader has previously been disposed.</exception>
    public T ReadRootSavable<T>() where T : class?, ISavable?
    {
      CheckDisposed();

      return ReadSavableInternal<T>("Root");
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader has previously been disposed.</exception>
    public T ReadSavable<T>(string name) where T : class?, ISavable?
    {
      CheckDisposed();

      return ReadSavableInternal<T>(name);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader has previously been disposed.</exception>
    public int ReadSavableArray<T>(string name, Span<T> values) where T : class?, ISavable?
    {
      CheckDisposed();

      if (ReadAndValidateArrayHeader(name, BinaryTypeCode.Savable, out int count))
      {
        int elemCount = Math.Min(values.Length, count);
        for (int i = 0; i < elemCount; i++)
          values[i] = ReadSavableInternal<T>(name);

        SeekToEndOfArray(count - elemCount, BinaryTypeCode.Savable);

        return elemCount;
      }

      return 0;
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader has previously been disposed.</exception>
    public T ReadSharedSavable<T>(string name) where T : class?, ISavable?
    {
      CheckDisposed();

      return ReadSharedOrExternalSavable<T>(name);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader has previously been disposed.</exception>
    public int ReadSharedSavableArray<T>(string name, Span<T> values) where T : class?, ISavable?
    {
      CheckDisposed();

      return ReadSharedOrExternalSavableArray<T>(name, values);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader has previously been disposed.</exception>
    public T ReadExternalSavable<T>(string name) where T : class?, ISavable?
    {
      CheckDisposed();

      return ReadSharedOrExternalSavable<T>(name);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader has previously been disposed.</exception>
    public int ReadExternalSavableArray<T>(string name, Span<T> values) where T : class?, ISavable?
    {
      CheckDisposed();

      return ReadSharedOrExternalSavableArray<T>(name, values);
    }

    /// <inheritdoc />
    protected override void Dispose(bool disposing)
    {
      if (!IsDisposed)
      {
        if (disposing)
        {
          //If we read compressed data, switch the cached binary reader back and dispose what we were using
          if (m_cachedReader is not null)
          {
            UnderlyingBinaryReader.Dispose();
            UnderlyingBinaryReader = m_cachedReader;
            m_cachedReader = null;
          }

          UnderlyingBinaryReader.Dispose();
        }

        MarkIsDisposed();
      }
    }

    /// <inheritdoc />
    protected override Type ReadBlobType()
    {
      return ReadTypeIndex();
    }

    private Type ReadTypeIndex()
    {
      int index = UnderlyingBinaryReader.Read7BitEncodedInt();
      Type? type = m_versionTable.GetTypeAtIndex(index);
      Debug.Assert(type is not null);
      return type;
    }

    private T ReadSavableInternal<T>(string name) where T : class?, ISavable?
    {
      if (ReadAndValidateTypeHeaderOrNull(name, BinaryTypeCode.Savable))
      {
        // Create new instance from the type data
        Type objType = ReadTypeIndex();
        object obj = SmartActivator.CreateInstance(objType);
        Debug.Assert(obj is not null);

        if (!(obj is T))
          throw new InvalidCastException(StringLocalizer.Instance.GetLocalizedString("TypeMismatch", obj.GetType().Name, typeof(T).Name));

        // Have the object read it's data
        T value = (T) obj;
        value.Read(this);

        ReadAndValidateEndOfType(name, BinaryTypeCode.EndSavable);

        return value;
      }

      return null!;
    }

    private T ReadSharedOrExternalSavable<T>(string name) where T : class?, ISavable?
    {
      if (ReadAndValidateEitherTypeHeaderOrNull(name, BinaryTypeCode.SharedSavable, BinaryTypeCode.ExternalSavable, out BinaryTypeCode actualType))
      {
        if (actualType == BinaryTypeCode.SharedSavable)
        {
          return ReadSharedSavableData<T>();
        }
        else
        {
          return ReadExternalSavableData<T>();
        }
      }

      return null!;
    }

    private int ReadSharedOrExternalSavableArray<T>(string name, Span<T> values) where T : class?, ISavable?
    {
      if (ReadAndValidateArrayHeaderForEither(name, BinaryTypeCode.SharedSavable, BinaryTypeCode.ExternalSavable, out BinaryTypeCode actualType, out int count))
      {
        int elemCount = Math.Min(values.Length, count);
        for (int i = 0; i < elemCount; i++)
          values[i] = ReadSharedOrExternalSavable<T>(name);

        SeekToEndOfArray(count - elemCount, actualType);

        return elemCount;
      }

      return 0;
    }

    private T ReadExternalSavableData<T>() where T : class?, ISavable?
    {
      if (m_externalResolver is null || m_externalReferencePaths is null)
        return null!;

      // If we have an index, the string should exist
      int externalIndex = UnderlyingBinaryReader.Read7BitEncodedInt();
      Debug.Assert(externalIndex >= 0 && externalIndex < m_externalReferencePaths.Length);

      ExternalReference externalRef = new ExternalReference(typeof(T), m_externalReferencePaths[externalIndex]);
      return m_externalResolver.ResolveSavable<T>(externalRef);
    }

    private T ReadSharedSavableData<T>() where T : class?, ISavable?
    {
      // If we have an index, there should be an entry that exists
      int sharedIndex = UnderlyingBinaryReader.Read7BitEncodedInt();
      Debug.Assert(m_sharedData is not null);
      Debug.Assert(sharedIndex >= 0 && sharedIndex < m_sharedData.Length);

      ref SharedSavableEntry entry = ref m_sharedData[sharedIndex];
      entry.SeekToSharedSavable<T>(this);

      if (entry.Savable is null)
        return null!;

      if (!(entry.Savable is T))
        throw new InvalidCastException(StringLocalizer.Instance.GetLocalizedString("TypeMismatch", entry.Savable.GetType().Name, typeof(T).Name));

      return (T) entry.Savable;
    }

    private void PrepareStream()
    {
      //Read header
      Span<byte> magicWord = stackalloc byte[4];
      InStream.ReadExactly(magicWord);

      ThrowIfNotTEBO(magicWord);

      m_version = UnderlyingBinaryReader.Read7BitEncodedInt();

      bool compressed = UnderlyingBinaryReader.ReadBoolean();
      long versionTableDataSize = UnderlyingBinaryReader.Read7BitEncodedInt64();
      long externalRefDataSize = UnderlyingBinaryReader.Read7BitEncodedInt64();
      long sharedDataSize = UnderlyingBinaryReader.Read7BitEncodedInt64();
      long primaryDataSize = UnderlyingBinaryReader.Read7BitEncodedInt64();
      long totalCompressedDataSize = UnderlyingBinaryReader.Read7BitEncodedInt64();
      long totalNonCompressedDataSize = versionTableDataSize + externalRefDataSize + sharedDataSize + primaryDataSize;

      Debug.Assert(primaryDataSize > 0);

      // If compressed, decompress the portion of the data and set that decompressed stream to the writer to start reading from
      if (compressed)
      {
        m_cachedReader = UnderlyingBinaryReader;
        UnderlyingBinaryReader = new BinaryReader(DecompressStream(totalCompressedDataSize, totalNonCompressedDataSize, m_cachedReader.BaseStream));
      }

      // Read version table, usually should exist
      if (versionTableDataSize > 0)
        ReadVersionTable();

      // Read external table if any
      if (externalRefDataSize > 0)
        ReadExternalReferencesTable();

      // Read shared table if any
      if (sharedDataSize > 0)
        ReadSharedData();

      // Ready to read primary data
    }

    private Stream DecompressStream(long compressedTotalSize, long decompressedTotalSize, Stream inputStream)
    {
      // Read the compressed data into a temporary buffer - odds are the stream just contains this, but copy only the portion
      // as specified by the header. This allows the stream to potentially contain multiple distinct savables.
      DataBufferStream compressedData = DataBufferStream.FromPool(compressedTotalSize, true);
      inputStream.ReadExactly(compressedData.AsSpan());

      // Decompress the data
      DataBufferStream decompressedStream = DataBufferStream.FromPool(decompressedTotalSize);
      using (BrotliStream compressedStream = new BrotliStream(compressedData, CompressionMode.Decompress, false))
      {
        compressedStream.CopyTo(decompressedStream);
        decompressedStream.Position = 0;
      }

      return decompressedStream;
    }

    private void ReadVersionTable()
    {
      int count = UnderlyingBinaryReader.Read7BitEncodedInt();

      List<int> baseIndices = new List<int>();

      for (int i = 0; i < count; i++)
      {
        string typeName = UnderlyingBinaryReader.ReadString();
        int versionNumber = UnderlyingBinaryReader.Read7BitEncodedInt();

        m_versionTable.AddFromPersistence(typeName, versionNumber);
      }
    }

    private void ReadExternalReferencesTable()
    {
      int count = UnderlyingBinaryReader.Read7BitEncodedInt();
      m_externalReferencePaths = new string[count];

      for (int i = 0; i < count; i++)
        m_externalReferencePaths[i] = UnderlyingBinaryReader.ReadString();
    }

    private void ReadSharedData()
    {
      int count = UnderlyingBinaryReader.Read7BitEncodedInt();
      m_sharedData = new SharedSavableEntry[count];

      //Shared savables, especially ones that have nested shared savables, can be a bit tricky. We defer the loading until we actually need them,
      //we only want to know the position in the stream where they start at and the size of the savable. When we look up the savable later, we'll 
      //redirect to that position, read the savable, then reset to the previous position in the stream.
      for (int i = 0; i < count; i++)
      {
        long sizeInBytes = UnderlyingBinaryReader.Read7BitEncodedInt64();
        long startPos = InStream.Position;

        InStream.Position += sizeInBytes;

        m_sharedData[i] = new SharedSavableEntry(sizeInBytes, startPos);
      }
    }

    private void ThrowIfNotTEBO(ReadOnlySpan<byte> magicWord)
    {
      if (magicWord[0] != MAGIC_WORD[0] || magicWord[1] != MAGIC_WORD[1] || magicWord[2] != MAGIC_WORD[2] || magicWord[3] != MAGIC_WORD[3])
        throw new TeslaContentException(StringLocalizer.Instance.GetLocalizedString("BadTEBOFormat"));
    }

    private struct SharedSavableEntry
    {
      public long SizeInBytes;
      public long StartPosition;
      public ISavable? Savable;
      public bool Seeked;

      public SharedSavableEntry(long sizeInBytes, long startPos)
      {
        SizeInBytes = sizeInBytes;
        StartPosition = startPos;
        Savable = null;
        Seeked = false;
      }

      public void SeekToSharedSavable<T>(BinarySavableReader reader) where T : class?, ISavable?
      {
        if (Seeked)
          return;

        long currPos = reader.InStream.Position;
        reader.InStream.Position = StartPosition;

        Savable = reader.ReadSavableInternal<T>("SharedObject");
        Seeked = true;

        reader.InStream.Position = currPos;
      }
    }
  }
}