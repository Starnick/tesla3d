﻿/*
* Copyright (c) 2010-2024 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using Tesla.Utilities;

#nullable enable

namespace Tesla.Content
{
  /// <summary>
  /// Writes <see cref="ISavable"/> objects as tokenized binary data to a stream.
  /// </summary>
  public sealed class BinarySavableWriter : BinaryPrimitiveWriter, ISavableWriter
  {
    private const int VERSION_NUMBER = 3;
    private static readonly byte[] MAGIC_WORD = new byte[] { 84, 69, 66, 79 }; //TEBO characters

    private IExternalReferenceHandler? m_externalHandler;
    private CyclicReferenceDetector m_cyclicDetector;
    private SavableWriteFlags m_writeFlags;
    private ExternalSharedMode m_externalSharedMode;
    private bool m_compressData;

    private Queue<ISavable> m_sharedQueue;
    private Dictionary<ISavable, int> m_sharedIndices;
    private int m_sharedObjectCount;

    private Dictionary<ExternalReference, int> m_externalReferenceIndices;
    private List<ExternalReference> m_externalReferences;

    private TypeVersionTable m_versionTable;

    private Stream m_finalOutput;
    private DataBufferStream m_primaryData;

    /// <inheritdoc />
    public IExternalReferenceHandler? ExternalHandler { get { return m_externalHandler; } }

    /// <inheritdoc />
    public SavableWriteFlags WriteFlags { get { return m_writeFlags; } }

    /// <summary>
    /// Constructs a new instance of the <see cref="BinarySavableWriter"/> class. All external references are automatically treated as 
    /// shared and written to the output stream.
    /// </summary>
    /// <param name="output">Output stream to write to.</param>
    /// <param name="writeFlags">Write flags that specify certain behaviors.</param>
    /// <param name="leaveOpen">True if the output stream should NOT be disposed/closed when the writer is disposed/closed, false otherwise.</param>
    /// <exception cref="ArgumentNullException">Thrown if the stream is null.</exception>
    /// <exception cref="NotSupportedException">Thrown if the stream cannot be written to.</exception>
    public BinarySavableWriter(Stream output, SavableWriteFlags writeFlags = SavableWriteFlags.None, bool leaveOpen = false) : this(output, writeFlags, null, leaveOpen) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="BinarySavableWriter"/> class.
    /// </summary>
    /// <param name="output">Output stream to write to.</param>
    /// <param name="writeFlags">Write flags that specify certain behaviors.</param>
    /// <param name="externalHandler">External reference handler, if null then all external references are treated as shared and are written to the output stream.</param>
    /// <param name="leaveOpen">True if the output stream should NOT be disposed/closed when the writer is disposed/closed, false otherwise.</param>
    /// <exception cref="ArgumentNullException">Thrown if the stream is null.</exception>
    /// <exception cref="NotSupportedException">Thrown if the stream cannot be written to.</exception>
    public BinarySavableWriter(Stream output, SavableWriteFlags writeFlags, IExternalReferenceHandler? externalHandler, bool leaveOpen = false)
        : base(output, leaveOpen)
    {
      m_writeFlags = writeFlags;
      m_externalHandler = externalHandler;
      m_sharedQueue = new Queue<ISavable>();
      m_sharedIndices = new Dictionary<ISavable, int>(new ReferenceEqualityComparer<ISavable>());
      m_sharedObjectCount = 0;
      m_externalReferenceIndices = new Dictionary<ExternalReference, int>(new ReferenceEqualityComparer<ExternalReference>());
      m_externalReferences = new List<ExternalReference>();
      m_versionTable = new TypeVersionTable();
      m_cyclicDetector = new CyclicReferenceDetector();
      m_compressData = writeFlags.HasFlag(SavableWriteFlags.UseCompression) ? true : false;

      // Determine external/shared write mode
      if (externalHandler is null)
        m_externalSharedMode = ExternalSharedMode.AllShared;
      else if (writeFlags.HasFlag(SavableWriteFlags.SharedAsExternal))
        m_externalSharedMode = ExternalSharedMode.AllExternal;
      else
        m_externalSharedMode = ExternalSharedMode.Default;

      // Swap streams, we create an in-memory stream for the primary data then process shared/external data as a final step
      m_primaryData = DataBufferStream.FromPool();
      m_finalOutput = OutStream;
      OutStream = m_primaryData;
    }

    /// <inheritdoc />
    public override void Flush()
    {
      CheckDisposed();

      FlushInternal(true);
    }

    /// <summary>
    /// Writes a savable object to the output, calling it "Root". Multiple objects could in theory be written out, but typically
    /// there is a 1-to-1.
    /// </summary>
    /// <typeparam name="T">Type of object to write.</typeparam>
    /// <param name="value">Savable object.</param>
    /// <param name="flushImmediately">If true, flushes the writer and closes the root object, allowing no more values to be written to the root object. Default is true.</param>
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void WriteRootSavable<T>(T value, bool flushImmediately = true) where T : class?, ISavable?
    {
      CheckDisposed();

      WriteSavableInternal<T>(value);

      if (flushImmediately)
        Flush();
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void WriteSavable<T>(string name, T value) where T : class?, ISavable?
    {
      CheckDisposed();

      WriteSavableInternal<T>(value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void WriteSavable<T>(string name, ReadOnlySpan<T> values) where T : class?, ISavable?
    {
      CheckDisposed();

      if (WriteArrayHeader(values.Length, BinaryTypeCode.Savable))
      {
        for (int i = 0; i < values.Length; i++)
          WriteSavableInternal<T>(values[i]);

        WriteEndOfType(BinaryTypeCode.EndArray);
      }
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void WriteSharedSavable<T>(string name, T value) where T : class?, ISavable?
    {
      CheckDisposed();

      if (m_externalSharedMode == ExternalSharedMode.AllExternal)
        WriteExternalSavableInternal<T>(value);
      else
        WriteSharedSavableInternal<T>(value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void WriteSharedSavable<T>(string name, ReadOnlySpan<T> values) where T : class?, ISavable?
    {
      CheckDisposed();

      if (m_externalSharedMode == ExternalSharedMode.AllExternal)
        WriteExternalSavableInternal<T>(values);
      else
        WriteSharedSavableInternal<T>(values);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void WriteExternalSavable<T>(string name, T value) where T : class?, ISavable?
    {
      CheckDisposed();

      if (m_externalSharedMode == ExternalSharedMode.AllShared)
        WriteSharedSavableInternal<T>(value);
      else
        WriteExternalSavableInternal<T>(value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void WriteExternalSavable<T>(string name, ReadOnlySpan<T> values) where T : class?, ISavable?
    {
      CheckDisposed();

      if (m_externalSharedMode == ExternalSharedMode.AllShared)
        WriteSharedSavableInternal<T>(values);
      else
        WriteExternalSavableInternal<T>(values);
    }

    /// <inheritdoc />
    protected override void WriteBlobType(Type type)
    {
      WriteTypeIndex(type);
    }

    private void WriteTypeIndex(Type type)
    {
      int typeIndex = m_versionTable.AddOrGetFromRuntime(type);
      UnderlyingBinaryWriter.Write7BitEncodedInt(typeIndex);
    }

    private void WriteSavableInternal<T>(T value) where T : class?, ISavable?
    {
      if (WriteTypeHeaderOrNull(value, BinaryTypeCode.Savable))
      {
        m_cyclicDetector.AddReference(value);

        try
        {
          WriteTypeIndex(value.GetType());
          value.Write(this);
        }
        finally
        {
          m_cyclicDetector.RemoveReference(value);
        }

        WriteEndOfType(BinaryTypeCode.EndSavable);
      }
    }

    private void WriteSharedSavableInternal<T>(T value) where T : class?, ISavable?
    {
      if (WriteTypeHeaderOrNull(value, BinaryTypeCode.SharedSavable))
      {
        if (!m_sharedIndices.TryGetValue(value, out int index))
        {
          index = m_sharedObjectCount;
          m_sharedQueue.Enqueue(value);
          m_sharedIndices.Add(value, index);

          m_sharedObjectCount++;
        }

        UnderlyingBinaryWriter.Write7BitEncodedInt(index);
      }
    }

    private void WriteSharedSavableInternal<T>(ReadOnlySpan<T> values) where T : class?, ISavable?
    {
      if (WriteArrayHeader(values.Length, BinaryTypeCode.SharedSavable))
      {
        for (int i = 0; i < values.Length; i++)
          WriteSharedSavableInternal<T>(values[i]);

        WriteEndOfType(BinaryTypeCode.EndArray);
      }
    }

    private void WriteExternalSavableInternal<T>(T value) where T : class?, ISavable?
    {
      ExternalReference? externalRef = (value is null) ? null : m_externalHandler?.ProcessSavable<T>(value);
      if (externalRef == ExternalReference.NullReference)
        externalRef = null;

      if (WriteTypeHeaderOrNull(externalRef, BinaryTypeCode.ExternalSavable))
      {
        if (!m_externalReferenceIndices.TryGetValue(externalRef, out int index))
        {
          index = m_externalReferences.Count;
          m_externalReferences.Add(externalRef);
          m_externalReferenceIndices.Add(externalRef, index);
        }

        UnderlyingBinaryWriter.Write7BitEncodedInt(index);
      }
    }

    private void WriteExternalSavableInternal<T>(ReadOnlySpan<T> values) where T : class?, ISavable?
    {
      if (WriteArrayHeader(values.Length, BinaryTypeCode.ExternalSavable))
      {
        for (int i = 0; i < values.Length; i++)
          WriteExternalSavableInternal<T>(values[i]);

        WriteEndOfType(BinaryTypeCode.EndArray);
      }
    }

    /// <inheritdoc/>
    protected override void Dispose(bool disposing)
    {
      if (!IsDisposed)
      {
        if (disposing)
        {
          FlushInternal(false);
          OutStream = m_finalOutput; //Make sure we set outstream to final output
          UnderlyingBinaryWriter.Dispose();
          m_primaryData.Dispose();
        }

        MarkIsDisposed();
      }
    }

    private void FlushInternal(bool reset)
    {
      TEBODataSections data = new TEBODataSections(m_primaryData);

      // Process queued shared savables, this will capture all possible shared objects
      ProcessSharedData(ref data);

      // All savables, primary and shared now processed, our version table and external reference list should be complete, so process those
      ProcessVersionTable(ref data);
      ProcessExternalReferenceTable(ref data);

      // Finalize data preparation
      PrepareData(ref data);

      // Set outstream back to final output
      OutStream = m_finalOutput;

      // Write header and data
      WriteHeader(data);
      WriteData(data);

      // Flush externals last, so any external data gets written out
      if (m_externalHandler is not null)
        m_externalHandler.Flush();

      // Flushes final output
      base.Flush();

      // If reset, set the primary data back to out stream - all other buffers are disposed and primary data
      // is set to length of zero, waiting for additional input
      if (reset)
      {
        m_primaryData.SetLength(0);
        OutStream = m_primaryData;
      }
    }

    private void ProcessVersionTable(ref TEBODataSections data)
    {
      if (m_versionTable.Count == 0)
        return;

      DataBufferStream versionTableData = DataBufferStream.FromPool();
      OutStream = versionTableData;

      UnderlyingBinaryWriter.Write7BitEncodedInt(m_versionTable.Count);

      for (int i = 0; i < m_versionTable.Count; i++)
      {
        TypeVersionTable.Entry vEntry = m_versionTable[i];
        UnderlyingBinaryWriter.Write(vEntry.PersistedTypeName);
        UnderlyingBinaryWriter.Write7BitEncodedInt(vEntry.RuntimeVersion);
      }

      // Save section
      data.VersionTableData = versionTableData;
      data.VersionTableDataSize = versionTableData.Length;
    }

    private void ProcessExternalReferenceTable(ref TEBODataSections data)
    {
      if (m_externalReferences.Count == 0)
        return;

      DataBufferStream externalReferenceData = DataBufferStream.FromPool();
      OutStream = externalReferenceData;

      UnderlyingBinaryWriter.Write7BitEncodedInt(m_externalReferences.Count);

      for (int i = 0; i < m_externalReferences.Count; i++)
      {
        ExternalReference extRef = m_externalReferences[i];
        Debug.Assert(String.IsNullOrEmpty(extRef.ResourcePath));

        UnderlyingBinaryWriter.Write(extRef.ResourcePath);
      }

      // Save section
      data.ExternalReferenceData = externalReferenceData;
      data.ExternalReferenceDataSize = externalReferenceData.Length;

      //Reset external reference tracking
      m_externalReferences.Clear();
      m_externalReferenceIndices.Clear();
    }

    private void ProcessSharedData(ref TEBODataSections data)
    {
      if (m_sharedQueue.Count == 0)
        return;

      // Re-use the same stream to write each object out so we can write the size of each object as a 7-bit number
      using DataBufferStream singleObjectStream = DataBufferStream.FromPool();
      DataBufferStream sharedData = DataBufferStream.FromPool();

      OutStream = sharedData;

      //Process queued shared resources, each shared resource may also write other shared resources
      while (m_sharedQueue.Count > 0)
      {
        ISavable savable = m_sharedQueue.Dequeue();

        // Set temporary stream
        singleObjectStream.SetLength(0);
        OutStream = singleObjectStream;

        // Write object
        WriteSavableInternal<ISavable>(savable);

        // Set stream back to output and write the size of the single object + data
        OutStream = sharedData;
        UnderlyingBinaryWriter.Write7BitEncodedInt64(singleObjectStream.Length);
        OutStream.Write(singleObjectStream.AsSpan());
      }

      // Save section
      data.SharedData = sharedData;
      data.SharedObjectCount = m_sharedObjectCount;
      data.SharedDataSize = sharedData.Length + BinaryTypeCodeHelper.Get7BitEncodedIntSize(m_sharedObjectCount);

      //Reset shared object tracking
      m_sharedObjectCount = 0;
      m_sharedIndices.Clear();
    }

    private void PrepareData(ref TEBODataSections data)
    {
      if (!m_compressData)
        return;

      DataBufferStream compressedData = DataBufferStream.FromPool();
      using (BrotliStream compressionStream = new BrotliStream(compressedData, CompressionLevel.SmallestSize, true))
      {
        OutStream = compressionStream;
        data.WriteNonCompressedData(this);

        // Can dispose of the uncompressed streams now
        data.DisposeNonCompressed();

        //BrotliStream will be disposed, and compressed data will be flushed/written to the underlying stream
      }

      // Save section
      data.CompressedData = compressedData;
      data.CompressedDataSize = compressedData.Length;
    }

    private void WriteHeader(in TEBODataSections data)
    {
      UnderlyingBinaryWriter.Write(MAGIC_WORD);
      UnderlyingBinaryWriter.Write7BitEncodedInt(VERSION_NUMBER);
      UnderlyingBinaryWriter.Write(m_compressData);
      UnderlyingBinaryWriter.Write7BitEncodedInt64(data.VersionTableDataSize);
      UnderlyingBinaryWriter.Write7BitEncodedInt64(data.ExternalReferenceDataSize);
      UnderlyingBinaryWriter.Write7BitEncodedInt64(data.SharedDataSize);
      UnderlyingBinaryWriter.Write7BitEncodedInt64(data.PrimaryDataSize);
      UnderlyingBinaryWriter.Write7BitEncodedInt64(data.CompressedDataSize);
    }

    private void WriteData(in TEBODataSections data)
    {
      if (m_compressData)
      {
        data.WriteCompressedData(this);
      }
      else
      {
        data.WriteNonCompressedData(this);
      }
    }

    // Holds all the different data sections under one banner.
    private struct TEBODataSections : IDisposable
    {
      public DataBufferStream PrimaryData;
      public DataBufferStream? VersionTableData = null;
      public DataBufferStream? ExternalReferenceData = null;
      public DataBufferStream? SharedData = null;
      public DataBufferStream? CompressedData = null;

      public int SharedObjectCount = 0;
      public long PrimaryDataSize = 0L;
      public long SharedDataSize = 0L;
      public long VersionTableDataSize = 0L;
      public long ExternalReferenceDataSize = 0L;
      public long CompressedDataSize = 0L;

      public long TotalNonCompressedSize
      {
        get
        {
          return PrimaryDataSize + SharedDataSize + VersionTableDataSize + ExternalReferenceDataSize;
        }
      }

      public TEBODataSections(DataBufferStream primary)
      {
        PrimaryData = primary;
        PrimaryDataSize = primary.Length;
      }

      public readonly void WritePrimaryData(BinarySavableWriter writer)
      {
        writer.OutStream.Write(PrimaryData.AsSpan());
      }

      public readonly void WriteVersionTableData(BinarySavableWriter writer)
      {
        if (VersionTableData is null)
          return;

        writer.OutStream.Write(VersionTableData.AsSpan());
      }

      public readonly void WriteExternalReferenceData(BinarySavableWriter writer)
      {
        if (ExternalReferenceData is null)
          return;

        writer.OutStream.Write(ExternalReferenceData.AsSpan());
      }

      public readonly void WriteSharedData(BinarySavableWriter writer)
      {
        if (SharedData is null)
          return;

        writer.UnderlyingBinaryWriter.Write7BitEncodedInt(SharedObjectCount);
        writer.OutStream.Write(SharedData.AsSpan());
      }

      public readonly void WriteNonCompressedData(BinarySavableWriter writer)
      {
        WriteVersionTableData(writer);
        WriteExternalReferenceData(writer);
        WriteSharedData(writer);
        WritePrimaryData(writer);
      }

      public readonly void WriteCompressedData(BinarySavableWriter writer)
      {
        if (CompressedData is null)
          return;

        writer.OutStream.Write(CompressedData.AsSpan());
      }

      public void Dispose()
      {
        DisposeNonCompressed();

        if (CompressedData is not null)
        {
          CompressedData.Dispose();
          CompressedData = null;
        }
      }

      public void DisposeNonCompressed()
      {
        if (VersionTableData is not null)
        {
          VersionTableData.Dispose();
          VersionTableData = null;
        }

        if (ExternalReferenceData is not null)
        {
          ExternalReferenceData.Dispose();
          ExternalReferenceData = null;
        }

        if (SharedData is not null)
        {
          SharedData.Dispose();
          SharedData = null;
        }
      }
    }
  }
}