﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using Tesla.Utilities;

#nullable enable

namespace Tesla.Content
{
  /// <summary>
  /// A writer that writes tokenized binary data to a stream. Each piece of data has <see cref="BinaryTypeCode"/> preceding it
  /// to allow for a reader to skip data or validate.
  /// </summary>
  public class BinaryPrimitiveWriter : IPrimitiveWriter
  {
    private bool m_isDisposed;
    private SwappableBinaryWriter m_binaryWriter;

    /// <inheritdoc />
    public bool IsDisposed { get { return m_isDisposed; } }

    /// <summary>
    /// Gets the underlying binary writer.
    /// </summary>
    protected BinaryWriter UnderlyingBinaryWriter { get { return m_binaryWriter; } }

    /// <summary>
    /// Gets or sets the underlying outpt stream.
    /// </summary>
    protected Stream OutStream
    {
      get
      {
        return m_binaryWriter.BaseStream;
      }
      set
      {
        m_binaryWriter.SetOutStream(value);
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="BinaryPrimitiveWriter"/> class.
    /// </summary>
    /// <param name="output">Output stream to write to.</param>
    /// <param name="leaveOpen">True if the output stream should NOT be disposed/closed when the writer is disposed/closed, false otherwise.</param>
    /// <exception cref="ArgumentNullException">Thrown if the stream is null.</exception>
    /// <exception cref="NotSupportedException">Thrown if the stream cannot be written to.</exception>
    public BinaryPrimitiveWriter(Stream output, bool leaveOpen = false)
    {
      ArgumentNullException.ThrowIfNull(output, nameof(output));

      if (!output.CanWrite)
        throw new NotSupportedException(StringLocalizer.Instance.GetLocalizedString("CannotWriteToStream"));

      m_binaryWriter = new SwappableBinaryWriter(output, Encoding.UTF8, leaveOpen);
      m_isDisposed = false;
    }

    /// <inheritdoc />
    public virtual void Close()
    {
      Dispose();
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public virtual void Flush()
    {
      CheckDisposed();

      m_binaryWriter.BaseStream.Flush();
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void Write(string name, byte value)
    {
      CheckDisposed();

      WriteTypeHeader(BinaryTypeCode.Byte1);
      m_binaryWriter.Write(value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void Write(string name, byte? value)
    {
      CheckDisposed();

      if (WriteTypeHeaderOrNull(value, BinaryTypeCode.Byte1))
        m_binaryWriter.Write(value.Value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void Write(string name, ReadOnlySpan<byte> values)
    {
      CheckDisposed();

      if (WriteArrayHeader(values.Length, BinaryTypeCode.Byte1))
      {
        m_binaryWriter.Write(values);

        WriteEndOfType(BinaryTypeCode.EndArray);
      }
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void Write(string name, sbyte value)
    {
      CheckDisposed();

      WriteTypeHeader(BinaryTypeCode.Byte1);
      m_binaryWriter.Write(value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void Write(string name, sbyte? value)
    {
      CheckDisposed();

      if (WriteTypeHeaderOrNull(value, BinaryTypeCode.Byte1))
        m_binaryWriter.Write(value.Value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void Write(string name, ReadOnlySpan<sbyte> values)
    {
      CheckDisposed();

      if (WriteArrayHeader(values.Length, BinaryTypeCode.Byte1))
      {
        m_binaryWriter.Write(values.AsBytes()); // Write out as bytes

        WriteEndOfType(BinaryTypeCode.EndArray);
      }
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void Write(string name, char value)
    {
      CheckDisposed();

      WriteTypeHeader(BinaryTypeCode.Char);
      m_binaryWriter.Write(value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void Write(string name, char? value)
    {
      CheckDisposed();

      if (WriteTypeHeaderOrNull(value, BinaryTypeCode.Char))
        m_binaryWriter.Write(value.Value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void Write(string name, ReadOnlySpan<char> values)
    {
      CheckDisposed();

      if (WriteArrayHeader(values.Length, BinaryTypeCode.Char))
      {
        m_binaryWriter.Write(values);

        WriteEndOfType(BinaryTypeCode.EndArray);
      }
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void Write(string name, ushort value)
    {
      CheckDisposed();

      WriteTypeHeader(BinaryTypeCode.Byte2);
      m_binaryWriter.Write(value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void Write(string name, ushort? value)
    {
      CheckDisposed();

      if (WriteTypeHeaderOrNull(value, BinaryTypeCode.Byte2))
        m_binaryWriter.Write(value.Value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void Write(string name, ReadOnlySpan<ushort> values)
    {
      CheckDisposed();

      if (WriteArrayHeader(values.Length, BinaryTypeCode.Byte2))
      {
        for (int i = 0; i < values.Length; i++)
          m_binaryWriter.Write(values[i]);

        WriteEndOfType(BinaryTypeCode.EndArray);
      }
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void Write(string name, uint value)
    {
      CheckDisposed();

      WriteTypeHeader(BinaryTypeCode.Byte4);
      m_binaryWriter.Write(value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void Write(string name, uint? value)
    {
      CheckDisposed();

      if (WriteTypeHeaderOrNull(value, BinaryTypeCode.Byte4))
        m_binaryWriter.Write(value.Value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void Write(string name, ReadOnlySpan<uint> values)
    {
      CheckDisposed();

      if (WriteArrayHeader(values.Length, BinaryTypeCode.Byte4))
      {
        for (int i = 0; i < values.Length; i++)
          m_binaryWriter.Write(values[i]);

        WriteEndOfType(BinaryTypeCode.EndArray);
      }
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void Write(string name, ulong value)
    {
      CheckDisposed();

      WriteTypeHeader(BinaryTypeCode.Byte8);
      m_binaryWriter.Write(value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void Write(string name, ulong? value)
    {
      CheckDisposed();

      if (WriteTypeHeaderOrNull(value, BinaryTypeCode.Byte8))
        m_binaryWriter.Write(value.Value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void Write(string name, ReadOnlySpan<ulong> values)
    {
      CheckDisposed();

      if (WriteArrayHeader(values.Length, BinaryTypeCode.Byte8))
      {
        for (int i = 0; i < values.Length; i++)
          m_binaryWriter.Write(values[i]);

        WriteEndOfType(BinaryTypeCode.EndArray);
      }
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void Write(string name, short value)
    {
      CheckDisposed();

      WriteTypeHeader(BinaryTypeCode.Byte2);
      m_binaryWriter.Write(value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void Write(string name, short? value)
    {
      CheckDisposed();

      if (WriteTypeHeaderOrNull(value, BinaryTypeCode.Byte2))
        m_binaryWriter.Write(value.Value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void Write(string name, ReadOnlySpan<short> values)
    {
      CheckDisposed();

      if (WriteArrayHeader(values.Length, BinaryTypeCode.Byte2))
      {
        for (int i = 0; i < values.Length; i++)
          m_binaryWriter.Write(values[i]);

        WriteEndOfType(BinaryTypeCode.EndArray);
      }
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void Write(string name, int value)
    {
      CheckDisposed();

      WriteTypeHeader(BinaryTypeCode.Byte4);
      m_binaryWriter.Write(value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void Write(string name, int? value)
    {
      CheckDisposed();

      if (WriteTypeHeaderOrNull(value, BinaryTypeCode.Byte4))
        m_binaryWriter.Write(value.Value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void Write(string name, ReadOnlySpan<int> values)
    {
      CheckDisposed();

      if (WriteArrayHeader(values.Length, BinaryTypeCode.Byte4))
      {
        for (int i = 0; i < values.Length; i++)
          m_binaryWriter.Write(values[i]);

        WriteEndOfType(BinaryTypeCode.EndArray);
      }
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void Write(string name, long value)
    {
      CheckDisposed();

      WriteTypeHeader(BinaryTypeCode.Byte8);
      m_binaryWriter.Write(value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void Write(string name, long? value)
    {
      CheckDisposed();

      if (WriteTypeHeaderOrNull(value, BinaryTypeCode.Byte8))
        m_binaryWriter.Write(value.Value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void Write(string name, ReadOnlySpan<long> values)
    {
      CheckDisposed();

      if (WriteArrayHeader(values.Length, BinaryTypeCode.Byte8))
      {
        for (int i = 0; i < values.Length; i++)
          m_binaryWriter.Write(values[i]);

        WriteEndOfType(BinaryTypeCode.EndArray);
      }
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void Write(string name, Half value)
    {
      CheckDisposed();

      WriteTypeHeader(BinaryTypeCode.Byte2);
      m_binaryWriter.Write(value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void Write(string name, Half? value)
    {
      CheckDisposed();

      if (WriteTypeHeaderOrNull(value, BinaryTypeCode.Byte2))
        m_binaryWriter.Write(value.Value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void Write(string name, ReadOnlySpan<Half> values)
    {
      CheckDisposed();

      if (WriteArrayHeader(values.Length, BinaryTypeCode.Byte2))
      {
        for (int i = 0; i < values.Length; i++)
          m_binaryWriter.Write(values[i]);

        WriteEndOfType(BinaryTypeCode.EndArray);
      }
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void Write(string name, float value)
    {
      CheckDisposed();

      WriteTypeHeader(BinaryTypeCode.Byte4);
      m_binaryWriter.Write(value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void Write(string name, float? value)
    {
      CheckDisposed();

      if (WriteTypeHeaderOrNull(value, BinaryTypeCode.Byte4))
        m_binaryWriter.Write(value.Value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void Write(string name, ReadOnlySpan<float> values)
    {
      CheckDisposed();

      if (WriteArrayHeader(values.Length, BinaryTypeCode.Byte4))
      {
        for (int i = 0; i < values.Length; i++)
          m_binaryWriter.Write(values[i]);

        WriteEndOfType(BinaryTypeCode.EndArray);
      }
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void Write(string name, double value)
    {
      CheckDisposed();

      WriteTypeHeader(BinaryTypeCode.Byte8);
      m_binaryWriter.Write(value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void Write(string name, double? value)
    {
      CheckDisposed();

      if (WriteTypeHeaderOrNull(value, BinaryTypeCode.Byte8))
        m_binaryWriter.Write(value.Value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void Write(string name, ReadOnlySpan<double> values)
    {
      CheckDisposed();

      if (WriteArrayHeader(values.Length, BinaryTypeCode.Byte8))
      {
        for (int i = 0; i < values.Length; i++)
          m_binaryWriter.Write(values[i]);

        WriteEndOfType(BinaryTypeCode.EndArray);
      }
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void Write(string name, decimal value)
    {
      CheckDisposed();

      WriteTypeHeader(BinaryTypeCode.Byte16);
      m_binaryWriter.Write(value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void Write(string name, decimal? value)
    {
      CheckDisposed();

      if (WriteTypeHeaderOrNull(value, BinaryTypeCode.Byte16))
        m_binaryWriter.Write(value.Value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void Write(string name, ReadOnlySpan<decimal> values)
    {
      CheckDisposed();

      if (WriteArrayHeader(values.Length, BinaryTypeCode.Byte16))
      {
        for (int i = 0; i < values.Length; i++)
          m_binaryWriter.Write(values[i]);

        WriteEndOfType(BinaryTypeCode.EndArray);
      }
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void Write(string name, bool value)
    {
      CheckDisposed();

      WriteTypeHeader(BinaryTypeCode.Byte1);
      m_binaryWriter.Write(value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void Write(string name, bool? value)
    {
      CheckDisposed();

      if (WriteTypeHeaderOrNull(value, BinaryTypeCode.Byte1))
        m_binaryWriter.Write(value.Value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void Write(string name, ReadOnlySpan<bool> values)
    {
      CheckDisposed();

      if (WriteArrayHeader(values.Length, BinaryTypeCode.Byte1))
      {
        for (int i = 0; i < values.Length; i++)
          m_binaryWriter.Write(values[i]);

        WriteEndOfType(BinaryTypeCode.EndArray);
      }
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void Write(string name, string? value)
    {
      CheckDisposed();

      if (WriteTypeHeaderOrNull(value))
        m_binaryWriter.Write(value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void Write(string name, ReadOnlySpan<string?> values)
    {
      CheckDisposed();

      if (WriteArrayHeader(values.Length, BinaryTypeCode.String))
      {
        for (int i = 0; i < values.Length; i++)
        {
          string? value = values[i];
          if (WriteTypeHeaderOrNull(value))
            m_binaryWriter.Write(value);
        }

        WriteEndOfType(BinaryTypeCode.EndArray);
      }
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void Write<T>(string name, in T value) where T : struct, IPrimitiveValue
    {
      CheckDisposed();

      WriteTypeHeader(BinaryTypeCode.PrimitiveValue);

      value.Write(this);

      WriteEndOfType(BinaryTypeCode.EndPrimitiveValue);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void Write<T>(string name, in T? value) where T : struct, IPrimitiveValue
    {
      CheckDisposed();

      if (WriteTypeHeaderOrNull(value, BinaryTypeCode.PrimitiveValue))
      {
        ref readonly T primValue = ref Nullable.GetValueRefOrDefaultRef(in value);
        primValue.Write(this);

        WriteEndOfType(BinaryTypeCode.EndPrimitiveValue);
      }
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void Write<T>(string name, ReadOnlySpan<T> values) where T : struct, IPrimitiveValue
    {
      CheckDisposed();

      if (WriteArrayHeader(values.Length, BinaryTypeCode.PrimitiveValue))
      {
        // Purposefully omitting begin/end primitive value byte markers
        for (int i = 0; i < values.Length; i++)
        {
          ref readonly T value = ref values[i];
          value.Write(this);
        }

        WriteEndOfType(BinaryTypeCode.EndArray);
      }
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void WriteBlob<T>(string name, ReadOnlySpan<T> values) where T : unmanaged
    {
      CheckDisposed();

      if (WriteBlobHeader(new PrimitiveBlobHeader(values.Length, BufferHelper.SizeOf<T>(), typeof(T))))
        m_binaryWriter.Write(values.AsBytes());
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void WriteBlob(string name, IReadOnlyDataBuffer? values)
    {
      CheckDisposed();

      if (values is null || values.Length == 0)
      {
        WriteTypeHeader(BinaryTypeCode.NullObject);
        return;
      }

      WriteBlobHeader(new PrimitiveBlobHeader(values));
      m_binaryWriter.Write(values.Bytes);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void WriteEnum<T>(string name, T enumValue) where T : struct, Enum
    {
      CheckDisposed();

      WriteEnumInternal<T>(enumValue);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void WriteEnum<T>(string name, T? enumValue) where T : struct, Enum
    {
      CheckDisposed();

      if (WriteNullHeaderIfNull(enumValue))
        WriteEnumInternal<T>(enumValue.Value);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private void WriteEnumInternal<T>(T enumValue) where T : struct, Enum
    {
      TypeCode typeCode = enumValue.GetTypeCode();

      WriteTypeHeader(BinaryTypeCodeHelper.ToBinaryTypeCode(typeCode));

      switch (typeCode)
      {
        case TypeCode.Byte:
        case TypeCode.SByte:
          m_binaryWriter.Write(Unsafe.As<T, byte>(ref enumValue));
          break;
        case TypeCode.Int16:
          m_binaryWriter.Write(Unsafe.As<T, short>(ref enumValue));
          break;
        case TypeCode.Int32:
          m_binaryWriter.Write(Unsafe.As<T, int>(ref enumValue));
          break;
        case TypeCode.Int64:
          m_binaryWriter.Write(Unsafe.As<T, long>(ref enumValue));
          break;
        case TypeCode.UInt16:
          m_binaryWriter.Write(Unsafe.As<T, ushort>(ref enumValue));
          break;
        case TypeCode.UInt32:
          m_binaryWriter.Write(Unsafe.As<T, uint>(ref enumValue));
          break;
        case TypeCode.UInt64:
          m_binaryWriter.Write(Unsafe.As<T, ulong>(ref enumValue));
          break;
        default:
          throw new InvalidOperationException(); // Shouldn't happen
      }
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void BeginWriteGroup(string name)
    {
      CheckDisposed();

      m_binaryWriter.Write((byte) BinaryTypeCode.GroupNoCount);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void BeginWriteGroup(string name, int count)
    {
      CheckDisposed();

      m_binaryWriter.Write((byte) BinaryTypeCode.GroupWithCount);
      m_binaryWriter.Write7BitEncodedInt(Math.Max(0, count));
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer is disposed.</exception>
    public void EndWriteGroup()
    {
      CheckDisposed();

      m_binaryWriter.Write((byte) BinaryTypeCode.EndGroup);
    }

    /// <inheritdoc />
    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    /// <summary>
    /// Releases unmanaged and - optionally - managed resources.
    /// </summary>
    /// <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
    protected virtual void Dispose(bool disposing)
    {
      if (!m_isDisposed)
      {
        if (disposing)
        {
          Flush();
          m_binaryWriter.Dispose();
        }

        m_isDisposed = true;
      }
    }

    /// <summary>
    /// Used by subclassed writers to mark us disposed, if overriding the dispose method.
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected void MarkIsDisposed()
    {
      m_isDisposed = true;
    }

    /// <summary>
    /// Throws an exception if the current writer has been disposed.
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected void CheckDisposed()
    {
      if (m_isDisposed)
        throw new ObjectDisposedException(GetType().Name);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected void WriteTypeHeader(BinaryTypeCode typeCode)
    {
      m_binaryWriter.Write((byte) typeCode);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected bool WriteTypeHeaderOrNull<T>([NotNullWhen(true)] in T? obj, BinaryTypeCode underlyingType)
    {
      if (obj is null)
      {
        m_binaryWriter.Write((byte) BinaryTypeCode.NullObject);
        return false;
      }

      m_binaryWriter.Write((byte) underlyingType);
      return true;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected bool WriteTypeHeaderOrNull([NotNullWhen(true)] string? obj)
    {
      if (String.IsNullOrEmpty(obj))
      {
        m_binaryWriter.Write((byte) BinaryTypeCode.NullObject);
        return false;
      }

      m_binaryWriter.Write((byte) BinaryTypeCode.String);
      return true;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected void WriteEndOfType(BinaryTypeCode typeCode)
    {
      m_binaryWriter.Write((byte) typeCode);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected bool WriteNullHeaderIfNull<T>([NotNullWhen(true)] T? obj)
    {
      if (obj is null)
      {
        m_binaryWriter.Write((byte) BinaryTypeCode.NullObject);
        return false;
      }

      return true;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected bool WriteArrayHeader(int count, BinaryTypeCode underlyingType)
    {
      if (count <= 0)
      {
        m_binaryWriter.Write((byte) BinaryTypeCode.NullObject);
        return false;
      }

      m_binaryWriter.Write((byte) BinaryTypeCode.Array);
      m_binaryWriter.Write7BitEncodedInt(count);
      m_binaryWriter.Write((byte) underlyingType);

      return true;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected bool WriteBlobHeader(in PrimitiveBlobHeader blobHeader)
    {
      if (blobHeader.ElementCount <= 0)
      {
        m_binaryWriter.Write((byte) BinaryTypeCode.NullObject);
        return false;
      }

      m_binaryWriter.Write((byte) BinaryTypeCode.Blob);
      m_binaryWriter.Write7BitEncodedInt(blobHeader.ElementCount);
      m_binaryWriter.Write7BitEncodedInt(blobHeader.ElementSizeInBytes);
      WriteBlobType(blobHeader.ElementType);

      return true;
    }

    /// <summary>
    /// Writes the type of the blob. By default this writes out a string representing the fully qualified type name.
    /// </summary>
    /// <param name="type">Runtime type.</param>
    protected virtual void WriteBlobType(Type type)
    {
      string typeName = SmartActivator.GetAssemblyQualifiedName(type);
      m_binaryWriter.Write(typeName);
    }

    private class SwappableBinaryWriter : BinaryWriter
    {
      public SwappableBinaryWriter(Stream output, Encoding encoding, bool leaveOpen) : base(output, encoding, leaveOpen) { }

      public void SetOutStream(Stream outStream)
      {
        OutStream = outStream;
      }
    }
  }
}
