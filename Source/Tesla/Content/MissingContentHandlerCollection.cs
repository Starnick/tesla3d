﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections;
using System.Diagnostics;
using System.Collections.Generic;

namespace Tesla.Content
{
    /// <summary>
    /// Represents a collection of missing content handlers. Each handler is registered to a type and can serve place holder
    /// content for a resource that could not be found or loaded.
    /// </summary>
    [DebuggerDisplay("Count = {Count}")]
    public sealed class MissingContentHandlerCollection : IEnumerable<IMissingContentHandler>
    {
        private Dictionary<Type, IMissingContentHandler> m_handlers;

        /// <summary>
        /// Gets the number of handlers.
        /// </summary>
        public int Count
        {
            get
            {
                return m_handlers.Count;
            }
        }

        /// <summary>
        /// Gets the <see cref="IMissingContentHandler"/> with the specified target type.
        /// </summary>
        public IMissingContentHandler this[Type targetType]
        {
            get
            {
                if(targetType == null)
                    return null;

                IMissingContentHandler handler;
                if(!m_handlers.TryGetValue(targetType, out handler))
                    handler = null;

                return handler;
            }
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="MissingContentHandlerCollection"/> class.
        /// </summary>
        public MissingContentHandlerCollection()
        {
            m_handlers = new Dictionary<Type, IMissingContentHandler>();
        }

        /// <summary>
        /// Adds the content handler to the collection, which will be registered to its target type.
        /// </summary>
        /// <param name="handler">Missing content handler</param>
        public void Add(IMissingContentHandler handler)
        {
            if(handler == null)
                return;

            m_handlers[handler.TargetType] = handler;
        }

        /// <summary>
        /// Remove the the content handler registered to the specified type from the collection.
        /// </summary>
        /// <param name="targetType">Target type that the handler is registered to</param>
        /// <returns>True if the handler was removed, false otherwise</returns>
        public bool Remove(Type targetType)
        {
            if(targetType == null)
                return false;

            return m_handlers.Remove(targetType);
        }

        /// <summary>
        /// Clear all content handlers from this collection.
        /// </summary>
        public void Clear()
        {
            m_handlers.Clear();
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns> A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.</returns>
        public Dictionary<Type, IMissingContentHandler>.ValueCollection.Enumerator GetEnumerator()
        {
            return m_handlers.Values.GetEnumerator();
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns> A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.</returns>
        IEnumerator<IMissingContentHandler> IEnumerable<IMissingContentHandler>.GetEnumerator()
        {
            return m_handlers.Values.GetEnumerator();
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return m_handlers.Values.GetEnumerator();
        }
    }
}
