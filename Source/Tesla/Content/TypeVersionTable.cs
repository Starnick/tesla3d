﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using Tesla.Utilities;

#nullable enable

namespace Tesla.Content
{
  /// <summary>
  /// <para>
  /// Represents an ordered-list of type versions used for readers/writers. This table solves two problems:
  /// 
  /// <list type="number">
  /// <item>
  /// Reduce string footprint of writing out type information for every object (instead can be referenced by unique indices).
  /// </item>
  /// <item>
  /// To associate a version number to a type. Types annotated with <see cref="SavableVersionAttribute"/> to provide a version number.
  /// </item>
  /// </list>
  /// </para>
  /// <para>
  /// Types must be either classes that implement <see cref="ISavable"/> or unmanaged value types. Generally this can be thought of as an <see cref="ISavable"/>
  /// type version table, but an exception is made for writing out type information for unmanaged-type blobs.
  /// </para>
  /// <para>
  /// For security purposes, whitelisting types can be opted-in if reading from untrusted data.
  /// </para>
  /// <para>
  /// This class is not thread-safe, intended usage is for a writer/reader to own it's own instance.
  /// </para>
  /// </summary>
  public sealed class TypeVersionTable : IEnumerable<TypeVersionTable.Entry>
  {
    /// <summary>
    /// An entry in the version table.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    public readonly struct Entry
    {
      /// <summary>
      /// The persisted version number. Usually a value between 1 and the <see cref="RuntimeVersion"/>.
      /// </summary>
      public int PersistedVersion { get; init; }

      /// <summary>
      /// The current runtime version number defined in metadata on the type.
      /// </summary>
      public int RuntimeVersion { get; init; }

      /// <summary>
      /// Zero-based index ID of the entry in the version table.
      /// </summary>
      public int ID { get; init; }

      /// <summary>
      /// The type this entry represents the version of. If not resolved, this will be null (only applicable if added from persistence).
      /// </summary>
      public Type? RuntimeType { get; init; }

      /// <summary>
      /// The string representation that is persisted for the type.
      /// </summary>
      public string PersistedTypeName { get; init; }

      /// <summary>
      /// If the type is a class / implements <see cref="ISavable"/> then this is true, false if an unmanaged type.
      /// </summary>
      [MemberNotNullWhen(true, nameof(SavableVersionHierarchy))]
      public bool IsSavable { get; init; }

      /// <summary>
      /// If the runtime type is resolved this will be true. If false, then <see cref="RuntimeType"/> is null. This only can happen
      /// when reading type data from persistence.
      /// </summary>
      [MemberNotNullWhen(true, nameof(RuntimeType))]
      public bool IsResolved { get; init; }

      /// <summary>
      /// If the type is <see cref="ISavable"/> and is resolved, the version hierarchy for the type (first index is always the version of the type, then it's base
      /// class, and so on). Will be null for non-savables or if the type could not be resolved.
      /// </summary>
      public SavableVersionHierarchy? SavableVersionHierarchy { get; init; }

      /// <inheritdoc />
      public readonly override string ToString()
      {
        return $"ID: {ID}, [\"{PersistedTypeName}\", PVer: {PersistedVersion}, RVer: {RuntimeVersion}], IsSavable: {IsSavable}, IsResolved: {IsResolved}";
      }

      internal static Entry CreateUnresolved(int id, string persistedTypeName, int persistedVersion)
      {
        Debug.Assert(!String.IsNullOrEmpty(persistedTypeName));

        Entry entry = new Entry()
        {
          ID = id,
          RuntimeType = null!,
          PersistedTypeName = persistedTypeName,
          PersistedVersion = persistedVersion,
          RuntimeVersion = persistedVersion,

          // Guess, will be wrong if version is 1. But if a persisted version is greater we know we're dealing with a savable at least.
          IsSavable = persistedVersion > 1,
          SavableVersionHierarchy = null,
          IsResolved = false,
        };

        return entry;
      }

      internal static Entry Create(int id, Type runtimeType, string persistedTypeName, int persistedVersion, SavableVersionHierarchy? savableVersionHierarchy
        = null)
      {
        Debug.Assert(!String.IsNullOrEmpty(persistedTypeName));

        int runTimeVersion = 1;
        bool isSavable = false;

        if (savableVersionHierarchy is not null)
        {
          runTimeVersion = savableVersionHierarchy[0].Version;
          isSavable = true;
        }

        Entry entry = new Entry()
        {
          ID = id,
          RuntimeType = runtimeType!,
          PersistedTypeName = persistedTypeName,
          PersistedVersion = persistedVersion,
          RuntimeVersion = runTimeVersion,
          IsSavable = isSavable,
          SavableVersionHierarchy = savableVersionHierarchy,
          IsResolved = true
        };

        return entry;
      }
    }

    /// <summary>
    /// Enables or disables type white listing. If enabled, then persisted types must be first registered and if an incoming
    /// type name is not known, then an exception is thrown.
    /// </summary>
    public static bool EnableWhiteListing = false;

    private static HashSet<string> s_whiteListedTypeNames = new HashSet<string>();

    private Dictionary<Type, int> m_typeToIndex;
    private Dictionary<string, int> m_unresolvedTypeToIndex;
    private List<Entry> m_versionTable;

    /// <summary>
    /// Gets the number of version entries.
    /// </summary>
    public int Count
    {
      get
      {
        return m_versionTable.Count;
      }
    }

    /// <summary>
    /// Gets the <see cref="Entry"/> at the specified index.
    /// </summary>
    public Entry this[int index]
    {
      get
      {
        return m_versionTable[index];
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="TypeVersionTable"/> class.
    /// </summary>
    public TypeVersionTable()
    {
      m_versionTable = new List<Entry>();
      m_typeToIndex = new Dictionary<Type, int>();
      m_unresolvedTypeToIndex = new Dictionary<string, int>();
    }

    /// <summary>
    /// Clears the version table.
    /// </summary>
    public void Clear()
    {
      m_versionTable.Clear();
      m_typeToIndex.Clear();
      m_unresolvedTypeToIndex.Clear();
    }

    /// <summary>
    /// Adds an entry in the version table for the specified type. If the type is already present in the table, it returns the index of the entry. This should be
    /// used by readers to populate from a persisted version table. The runtime type is permitted to be unresolved (e.g. the type was deleted).
    /// </summary>
    /// <param name="fullTypeName">Typename to add to the version table.</param>
    /// <param name="persistedVersion">Version of the type from the persistence store.</param>
    /// <returns>The zero-based index of the entry in the table.</returns>
    /// <exception cref="ArgumentNullException">Thrown if the typename is null or empty.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the persisted version is less than 1 or greater than the runtime version.</exception>
    public int AddFromPersistence(string fullTypeName, int persistedVersion)
    {
      ArgumentNullException.ThrowIfNullOrEmpty(fullTypeName, nameof(fullTypeName));

      if (persistedVersion <= 0)
        throw new ArgumentOutOfRangeException(nameof(persistedVersion));

      if (EnableWhiteListing && !s_whiteListedTypeNames.Contains(fullTypeName))
        throw new InvalidOperationException(StringLocalizer.Instance.GetLocalizedString("TypeIsNotWhiteListed", fullTypeName));

      // Resolve the type...
      Type type = SmartActivator.GetType(fullTypeName, true);
      int index;

      // Allow unresolved type. Should be okay...just want to capture that we had something that no longer exists.
      if (type is null)
      {
        // Make sure we don't add duplicates
        if (m_unresolvedTypeToIndex.TryGetValue(fullTypeName, out index))
          return index;

        Entry vRecord = Entry.CreateUnresolved(m_versionTable.Count, fullTypeName, persistedVersion);
        index = vRecord.ID;
        m_versionTable.Add(vRecord);
        m_unresolvedTypeToIndex.Add(fullTypeName, index);
      } 
      else
      {
        // Resolved...maybe we have it already
        if (m_typeToIndex.TryGetValue(type, out index))
          return index;

        // If not savable, list will be null and runtime version defaults to 1
        SavableVersionHierarchy? svHierarchy = SavableVersionCache.GetVersions(type);
        Entry vRecord = Entry.Create(m_versionTable.Count, type, fullTypeName, persistedVersion, svHierarchy);

        if (vRecord.PersistedVersion > vRecord.RuntimeVersion)
          throw new ArgumentOutOfRangeException(nameof(persistedVersion));
        
        
        index = vRecord.ID;
        m_versionTable.Add(vRecord);
        m_typeToIndex.Add(type, index);
      }

      return index;
    }

    /// <summary>
    /// Adds an entry in the version table for the specified type. This should be used by writers to record all potential type versioning
    /// for persistence. The type must be either an unmanaged type or a class that implements <see cref="ISavable"/>. If it is <see cref="ISavable"/>,
    /// the entire type hierarchy is added. By convention, base types will have a lower ID than sub types and so will always appear first in the version list.
    /// </summary>
    /// <param name="type">Type to add to the version table.</param>
    /// <returns>The zero-based index of the entry in the table.</returns>
    /// <exception cref="ArgumentNullException">Thrown if the type is null.</exception>
    /// <exception cref="InvalidOperationException">Thrown if the type is not a class / <see cref="ISavable"/> or unmanaged type.</exception>
    public int AddOrGetFromRuntime(Type type)
    {
      return AddOrGetFromRuntime(type, out _);
    }

    /// <summary>
    /// Adds an entry in the version table for the specified type. This should be used by writers to record all potential type versioning
    /// for persistence. The type must be either an unmanaged type or a class that implements <see cref="ISavable"/>. If it is <see cref="ISavable"/>,
    /// the entire type hierarchy is added. By convention, base types will have a lower ID than sub types and so will always appear first in the version list.
    /// </summary>
    /// <param name="type">Type to add to the version table.</param>
    /// <param name="vEntry">Version entry for the type.</param>
    /// <returns>The zero-based index of the entry in the table.</returns>
    /// <exception cref="ArgumentNullException">Thrown if the type is null.</exception>
    /// <exception cref="InvalidOperationException">Thrown if the type is not a class / <see cref="ISavable"/> or unmanaged type.</exception>
    public int AddOrGetFromRuntime(Type type, out Entry vEntry)
    {
      ArgumentNullException.ThrowIfNull(type, nameof(type));

      if (m_typeToIndex.TryGetValue(type, out int index))
      {
        vEntry = m_versionTable[index];
        return index;
      }

      if (type.IsUnmanaged())
      {
        // Non-savable always has a runtime version of 1...
        vEntry = Entry.Create(m_versionTable.Count, type, SmartActivator.GetAssemblyQualifiedName(type), 1);
        m_versionTable.Add(vEntry);
        m_typeToIndex.Add(type, vEntry.ID);

        return vEntry.ID;
      }
      else if (SavableVersionCache.IsSavable(type))
      {
        // Get the ordered savable hierarchy version list. The first entry should be the type we're interested in, the rest base classes.
        SavableVersionHierarchy? svHierarchy = SavableVersionCache.GetVersions(type);
        ArgumentNullException.ThrowIfNull(svHierarchy);

        vEntry = default;

        // Go in reverse order so base classses have a lower ID value. Last record will be the type we're interested in.
        for (int i = svHierarchy.Count - 1; i >= 0; i--)
        {
          SavableVersionHierarchy.Entry svEntry = svHierarchy[i];

          //If the current base type is already contained then we can early out
          if (m_typeToIndex.ContainsKey(svEntry.Type))
            continue;

          // Get the version list for this type...
          SavableVersionHierarchy? vHierarchyFromThis = (i == 0) ? svHierarchy : SavableVersionCache.GetVersions(svEntry.Type);
          vEntry = Entry.Create(m_versionTable.Count, svEntry.Type, svEntry.AssemblyQualifiedTypeName, svEntry.Version, vHierarchyFromThis);
          m_versionTable.Add(vEntry);
          m_typeToIndex.Add(svEntry.Type, vEntry.ID);
        }

        // Last record will be the one we want to return the ID of
        return vEntry.ID;
      }

      throw new InvalidOperationException(StringLocalizer.Instance.GetLocalizedString("TypeMustBeSavableOrUnmanaged", type.AssemblyQualifiedName));
    }

    /// <summary>
    /// Gets an entry in the version table. If not present, null is returned. If null is returned and this version table was populated by a reader,
    /// that indicates the type hierarchy has changed and data might not be properly read.
    /// </summary>
    /// <param name="type">Type to get version information for.</param>
    /// <returns>Type version information or null if it was not added.</returns>
    /// <exception cref="ArgumentNullException">Thrown if the type is null.</exception>
    public Entry? GetVersion(Type type)
    {
      ArgumentNullException.ThrowIfNull(type, nameof(type));

      if (m_typeToIndex.TryGetValue(type, out int index))
        return m_versionTable[index];

      return null;
    }

    /// <summary>
    /// Gets the type at the specified index. If this returns null, the index may still be valid but the type was not resolved. Generally this
    /// only can happen if the table is read from persistence.
    /// </summary>
    /// <param name="index">Zero-based index.</param>
    /// <returns>Type of the object at the entry specified by the index, or null if the index is out of range.</returns>
    public Type? GetTypeAtIndex(int index)
    {
      if (index < 0 || index >= m_versionTable.Count)
        return null;

      return m_versionTable[index].RuntimeType;
    }

    /// <summary>
    /// Returns an enumerator that iterates through the collection.
    /// </summary>
    /// <returns>An enumerator that can be used to iterate through the collection.</returns>
    public List<Entry>.Enumerator GetEnumerator()
    {
      return m_versionTable.GetEnumerator();
    }

    /// <summary>
    /// Returns an enumerator that iterates through the collection.
    /// </summary>
    /// <returns>An enumerator that can be used to iterate through the collection.</returns>
    IEnumerator<Entry> IEnumerable<Entry>.GetEnumerator()
    {
      return m_versionTable.GetEnumerator();
    }

    /// <summary>
    /// Returns an enumerator that iterates through the collection.
    /// </summary>
    /// <returns>An enumerator that can be used to iterate through the collection.</returns>
    IEnumerator IEnumerable.GetEnumerator()
    {
      return m_versionTable.GetEnumerator();
    }

    /// <summary>
    /// Adds the type and it's type hierarchy (if implements <see cref="ISavable"/>) to the type white list. A type that is NOT
    /// <see cref="ISavable"/> <b>must be</b> an unmanaged type.
    /// </summary>
    /// <param name="type">Type to whitelist.</param>
    /// <exception cref="ArgumentException">Thrown if the type is null.</exception>
    /// <exception cref="InvalidOperationException">Thrown if the type is not valid.</exception>
    public static void AddToWhiteList(Type type)
    {
      ArgumentNullException.ThrowIfNull(type, nameof(type));

      lock (s_whiteListedTypeNames)
      {
        if (SavableVersionCache.IsSavable(type))
        {
          SavableVersionHierarchy? svHierarchy = SavableVersionCache.GetVersions(type);
          ArgumentNullException.ThrowIfNull(svHierarchy);

          for (int i = 0; i < svHierarchy.Count; i++)
            s_whiteListedTypeNames.Add(svHierarchy[i].AssemblyQualifiedTypeName);
        }
        else if (type.IsUnmanaged())
        {
          string typeName = SmartActivator.GetAssemblyQualifiedName(type);
          s_whiteListedTypeNames.Add(typeName);
        }
        else
        {
          throw new InvalidOperationException(StringLocalizer.Instance.GetLocalizedString("TypeMustBeSavableOrUnmanaged", type.AssemblyQualifiedName));
        }
      }
    }
  }
}
