﻿/*
* Copyright (c) 2010-2024 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics;
using System.IO;
using System.Text.Json;
using Tesla.Utilities;

#nullable enable

namespace Tesla.Content
{
  /// <summary>
  /// Reads <see cref="ISavable"/> objects as formatted JSON from a stream.
  /// </summary>
  public sealed class JsonSavableReader : JsonPrimitiveReader, ISavableReader
  {
    private static readonly string s_sharedPrefix = $"/{JsonPropertyNames.SharedObjects}/";

    private IServiceProvider m_serviceProvider;
    private IExternalReferenceResolver? m_externalResolver;

    private SharedSavableEntry[]? m_sharedData;
    private TypeVersionTable m_versionTable;

    /// <inheritdoc />
    public IServiceProvider ServiceProvider { get { return m_serviceProvider; } }

    /// <inheritdoc />
    public IExternalReferenceResolver? ExternalResolver { get { return m_externalResolver; } }

    /// <summary>
    /// Constructs a new instance of the <see cref="JsonSavableReader"/> class.
    /// </summary>
    /// <param name="serviceProvider">Service provider used to locate any services needed during loading. If null, an empty service provider is created.</param>
    /// <param name="input">Input stream to read from.</param>
    /// <param name="leaveOpen">True if the output stream should NOT be disposed/closed when the reader is disposed/closed, false otherwise.</param>
    /// <exception cref="ArgumentNullException">Thrown if the stream or service provider is null.</exception>
    /// <exception cref="NotSupportedException">Thrown if the stream cannot be read from or does not support seeking.</exception>
    public JsonSavableReader(IServiceProvider? serviceProvider, Stream inputStream, bool leaveOpen = false) : this(serviceProvider, inputStream, null, leaveOpen) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="JsonSavableReader"/> class.
    /// </summary>
    /// <param name="serviceProvider">Service provider used to locate any services needed during loading. If null, an empty service provider is created.</param>
    /// <param name="input">Input stream to read from.</param>
    /// <param name="externalResolver">Optional external reference resolver to locate objects external to this stream to load.</param>
    /// <param name="leaveOpen">True if the output stream should NOT be disposed/closed when the reader is disposed/closed, false otherwise.</param>
    /// <exception cref="ArgumentNullException">Thrown if the stream or service provider is null.</exception>
    /// <exception cref="NotSupportedException">Thrown if the stream cannot be read from or does not support seeking.</exception>
    public JsonSavableReader(IServiceProvider? serviceProvider, Stream inputStream, IExternalReferenceResolver? externalResolver, bool leaveOpen = false)
      : base(inputStream, leaveOpen)
    {
      m_serviceProvider = serviceProvider ?? new NullServiceProvider();
      m_externalResolver = externalResolver;
      m_versionTable = new TypeVersionTable();

      ReadSharedData();
    }

    /// <inheritdoc />
    public int GetVersion(Type type)
    {
      TypeVersionTable.Entry? vEntry = m_versionTable.GetVersion(type);
      if (vEntry.HasValue)
        return vEntry.Value.PersistedVersion;

      return -1;
    }

    /// <summary>
    /// Reads the "root" savable object from the input, this will be the root element of the JSON document.
    /// </summary>
    /// <typeparam name="T">Type of object to read.</typeparam>
    /// <returns>Read <see cref="ISavable"/> object.</returns>
    public T ReadRootSavable<T>() where T : class?, ISavable?
    {
      CheckDisposed();

      return ReadSavableInternal<T>(Document.RootElement, true);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader has previously been disposed.</exception>
    public T ReadSavable<T>(String name) where T : class?, ISavable?
    {
      CheckDisposed();

      if (GetAndValidateOrNull(name, JsonValueKind.Object, out JsonElement elem))
        return ReadSavableInternal<T>(elem, true);

      return null!;
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader has previously been disposed.</exception>
    public int ReadSavableArray<T>(String name, Span<T> values) where T : class?, ISavable?
    {
      CheckDisposed();

      if (PushAndValidateOrNull(name, JsonValueKind.Array, out JsonElement arrayElem))
      {
        int elemCount = Math.Min(values.Length, arrayElem.GetArrayLength());

        for (int i = 0; i < elemCount; i++)
        {
          JsonElement elemAt = arrayElem.GetElementAt(i, JsonValueKind.Object, name, true);
          values[i] = ReadSavableInternal<T>(elemAt, true);
        }

        PopElement();

        return elemCount;
      }

      return 0;
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader has previously been disposed.</exception>
    public T ReadSharedSavable<T>(String name) where T : class?, ISavable?
    {
      CheckDisposed();

      if (GetAndValidateOrNull(name, JsonValueKind.String, out JsonElement elem))
        return ReadSharedOrExternalSavable<T>(elem);

      return null!;
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader has previously been disposed.</exception>
    public int ReadSharedSavableArray<T>(String name, Span<T> values) where T : class?, ISavable?
    {
      CheckDisposed();

      if (GetAndValidateOrNull(name, JsonValueKind.Array, out JsonElement arrayElem))
      {
        int elemCount = Math.Min(values.Length, arrayElem.GetArrayLength());

        for (int i = 0; i < elemCount; i++)
        {
          JsonElement elemAt = arrayElem.GetElementAt(i, JsonValueKind.String, name, true);
          values[i] = ReadSharedOrExternalSavable<T>(elemAt);
        }

        return elemCount;
      }

      return 0;
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader has previously been disposed.</exception>
    public T ReadExternalSavable<T>(String name) where T : class?, ISavable?
    {
      CheckDisposed();

      if (GetAndValidateOrNull(name, JsonValueKind.String, out JsonElement elem))
        return ReadSharedOrExternalSavable<T>(elem);

      return null!;
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the reader has previously been disposed.</exception>
    public int ReadExternalSavableArray<T>(String name, Span<T> values) where T : class?, ISavable?
    {
      CheckDisposed();

      if (PushAndValidateOrNull(name, JsonValueKind.Array, out JsonElement arrayElem))
      {
        int elemCount = Math.Min(values.Length, arrayElem.GetArrayLength());

        for (int i = 0; i < elemCount; i++)
        {
          JsonElement elemAt = arrayElem.GetElementAt(i, JsonValueKind.String, name, true);
          values[i] = ReadSharedOrExternalSavable<T>(elemAt);
        }

        PopElement();

        return elemCount;
      }

      return 0;
    }

    /// <inheritdoc />
    protected override Type? ParseTypeInfo(JsonElement typeElem)
    {
      JsonValueKind elemKind = typeElem.ValueKind;

      // Simple case is a string with the assembly qualified type. If this was written out,
      // it's implicitly version one
      if (elemKind == JsonValueKind.String)
      {
        string? typeString = typeElem.GetString();
        if (typeString is null)
          return null;

        int index = m_versionTable.AddFromPersistence(typeString, 1);
        return m_versionTable.GetTypeAtIndex(index);
      }

      // Complex cases involve an array.
      // 1. ["typestring", versionNumber]
      // 2. ["typestring", versionNumber, baseVersion, superBaseVersion]
      // 3. [["typestring", versionNumber], ["basetypestring", baseVersion], ["superBasetypestring", superBaseVersion]]
      if (elemKind != JsonValueKind.Array)
        return null;

      int arrayCount = typeElem.GetArrayLength();
      if (arrayCount == 0)
        return null;

      // Case 1 if exactly type and version or case 2 if starts with a single type but has multiple versions
      if (IsSingleTypeInfo(typeElem, true))
      {
        if (arrayCount == 2)
          return ParseSingleTypeInfo(typeElem[0], typeElem[1]);

        return ParseShortHandTypeInfo(typeElem);
      }

      // Case 3 if the first element at least is an array
      if (IsSingleTypeInfo(typeElem[0]))
        return ParseVerboseTypeInfo(typeElem);

      return null;
    }

    private Type? ParseShortHandTypeInfo(JsonElement shortHandTypeElem)
    {
      Type? type = ParseSingleTypeInfo(shortHandTypeElem[0], shortHandTypeElem[1]);
      if (type is null)
        return null;

      // If we have more array elements, parse the version numbers and cross-reference with the savable version hierarchy in the supplied order
      int arrayCount = shortHandTypeElem.GetArrayLength();
      if (arrayCount > 2)
        return type;

      TypeVersionTable.Entry? vEntry = m_versionTable.GetVersion(type);
      if (!vEntry.HasValue || vEntry.Value.SavableVersionHierarchy is null)
        return type;

      SavableVersionHierarchy vHierarchy = vEntry.Value.SavableVersionHierarchy;
      for (int i = 2, vIndex = 1; i < arrayCount && vIndex < vHierarchy.Count; i++, vIndex++)
      {
        JsonElement elemAt = shortHandTypeElem[i];
        if (elemAt.ValueKind != JsonValueKind.Number)
        {
          Debug.Assert(true, "Version number expected");
          continue;
        }

        // Add the version for the base type
        SavableVersionHierarchy.Entry baseSVEntry = vHierarchy[vIndex];
        m_versionTable.AddFromPersistence(baseSVEntry.AssemblyQualifiedTypeName, elemAt.GetInt32());
      }

      return type;
    }

    private Type? ParseVerboseTypeInfo(JsonElement verboseTypeElem)
    {
      Type? returnType = null;

      for (int i = 0; i < verboseTypeElem.GetArrayLength(); i++)
      {
        JsonElement elemAt = verboseTypeElem[i];
        if (!IsSingleTypeInfo(elemAt))
        {
          Debug.Assert(true, "TypeInfo was malformed");
          continue;
        }

        Type? type = ParseSingleTypeInfo(elemAt[0], elemAt[1]);
        if (i == 0)
          returnType = type;
      }

      return returnType;
    }

    private Type? ParseSingleTypeInfo(JsonElement typeStringElem, JsonElement versionElem)
    {
      string? typeString = typeStringElem.GetString();
      if (typeString is null)
        return null;

      int versionNum = versionElem.GetInt32();

      int index = m_versionTable.AddFromPersistence(typeString, versionNum);
      return m_versionTable.GetTypeAtIndex(index);
    }

    private bool IsSingleTypeInfo(JsonElement typeStringElem, JsonElement versionElem)
    {
      return typeStringElem.ValueKind == JsonValueKind.String && versionElem.ValueKind == JsonValueKind.Number;
    }

    private bool IsSingleTypeInfo(JsonElement arrayElem, bool allowMultipleVersions = false)
    {
      if (arrayElem.ValueKind != JsonValueKind.Array)
        return false;

      int arrayCount = arrayElem.GetArrayLength();
      if (arrayCount == 2)
        return IsSingleTypeInfo(arrayElem[0], arrayElem[1]);

      if (allowMultipleVersions && arrayCount >= 2)
        return IsSingleTypeInfo(arrayElem[0], arrayElem[1]);

      return false;
    }

    private void ReadSharedData()
    {
      m_sharedData = null;

      string sharedDataPropertyName = JsonPropertyNames.SharedObjects.ToString();
      JsonElement rootElem = Document.RootElement;
      if (rootElem.ValueKind != JsonValueKind.Object || !rootElem.TryGetProperty(sharedDataPropertyName, out JsonElement sharedElem) || sharedElem.ValueKind != JsonValueKind.Array)
        return;

      m_sharedData = new SharedSavableEntry[sharedElem.GetArrayLength()];
      for (int i = 0; i < m_sharedData.Length; i++)
      {
        JsonElement elemAt = sharedElem.GetElementAt(i, JsonValueKind.Object, sharedDataPropertyName, true);
        m_sharedData[i] = new SharedSavableEntry(elemAt);
      }
    }

    private Type? ReadTypeInfo(JsonElement elem)
    {
      string objectTypePropertyName = JsonPropertyNames.ObjectType.ToString();
      if (elem.ValueKind != JsonValueKind.Object || !elem.TryGetProperty(objectTypePropertyName, out JsonElement typeElem))
        return null;

      return ParseTypeInfo(typeElem);
    }

    private T ReadSavableInternal<T>(JsonElement elem, bool pushElem = false) where T : class?, ISavable?
    {
      if (pushElem)
        PushElement(elem);

      try
      {
        Type? objType = ReadTypeInfo(elem);
        if (objType is null)
        {
          Debug.Assert(true, "Malformed Savable, missing type info.");
          return null!;
        }

        object obj = SmartActivator.CreateInstance(objType);
        Debug.Assert(obj is not null);

        if (!(obj is T))
          throw new InvalidCastException(StringLocalizer.Instance.GetLocalizedString("TypeMismatch", obj.GetType().Name, typeof(T).Name));

        // Have the object read it's data
        T value = (T) obj;
        value.Read(this);
        return value;
      }
      finally
      {
        if (pushElem)
          PopElement();
      }
    }

    private T ReadSharedOrExternalSavable<T>(JsonElement elem) where T : class?, ISavable?
    {
      // Both are always string types
      if (elem.ValueKind != JsonValueKind.String)
        return null!;

      string? path = elem.GetString();
      if (String.IsNullOrEmpty(path))
        return null!;

      // Test for shared savable first
      if (path.StartsWith(s_sharedPrefix))
      {
        if (m_sharedData is not null && int.TryParse(path.AsSpan(s_sharedPrefix.Length), out int sharedIndex) &&
            sharedIndex >= 0 && sharedIndex < m_sharedData.Length)
        {
          ref SharedSavableEntry entry = ref m_sharedData[sharedIndex];
          entry.SeekToSharedSavable<T>(this);

          if (entry.Savable is null)
            return null!;

          if (!(entry.Savable is T))
            throw new InvalidCastException(StringLocalizer.Instance.GetLocalizedString("TypeMismatch", entry.Savable.GetType().Name, typeof(T).Name));

          return (T) entry.Savable;
        }
      }
      else if (m_externalResolver is not null)
      {
        // Else try to have the external handler resolve it
        ExternalReference externalRef = new ExternalReference(typeof(T), path);
        return m_externalResolver.ResolveSavable<T>(externalRef);
      }

      return null!;
    }

    private struct SharedSavableEntry
    {
      public readonly JsonElement Element;
      public ISavable? Savable;
      public bool Seeked;

      public SharedSavableEntry(JsonElement elem)
      {
        Element = elem;
        Savable = null;
        Seeked = false;
      }

      public void SeekToSharedSavable<T>(JsonSavableReader reader) where T : class?, ISavable?
      {
        if (Seeked)
          return;

        Savable = reader.ReadSavableInternal<T>(Element, true);
        Seeked = true;
      }
    }
  }
}
