﻿/*
* Copyright (c) 2010-2024 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.IO;

#nullable enable

namespace Tesla.Content
{
  /// <summary>
  /// A resource importer that can load <see cref="ISavable"/> objects from the TEBO (Tesla Engine Binary Object) format.
  /// </summary>
  public sealed class JsonResourceImporter : ResourceImporter<ISavable>
  {
    /// <summary>
    /// Constructs a new instance of the <see cref="JsonResourceImporter"/> class. This queries the engine instance
    /// for the render system.
    /// </summary>
    public JsonResourceImporter() : base(".json") { }

    /// <inheritdoc />
    public override ISavable? Load(IResourceFile resourceFile, ContentManager contentManager, ImporterParameters parameters)
    {
      ValidateParameters(resourceFile, contentManager, ref parameters);

      ExternalReferenceResolver resolver = new ExternalReferenceResolver(contentManager, resourceFile);
      Stream stream = resourceFile.OpenRead();

      using (JsonSavableReader reader = new JsonSavableReader(contentManager.ServiceProvider, stream, resolver))
        return reader.ReadRootSavable<ISavable?>();
    }

    /// <inheritdoc />
    public override ISavable? Load(Stream input, ContentManager contentManager, ImporterParameters parameters)
    {
      ValidateParameters(input, contentManager, ref parameters);

      using (JsonSavableReader reader = new JsonSavableReader(contentManager.ServiceProvider, input, true))
        return reader.ReadRootSavable<ISavable?>();
    }

    /// <summary>
    /// Loads content from the specified stream as the target runtime type.
    /// </summary>
    /// <param name="input">Stream to read from.</param>
    /// <param name="contentManager">Calling content manager.</param>
    /// <param name="parameters">Optional loading parameters.</param>
    /// <param name="externalResolver">Resolver for resolving the stream's external references.</param>
    /// <returns>The loaded object or null if it could not be loaded.</returns>
    public ISavable? Load(Stream input, ContentManager contentManager, ImporterParameters parameters, IExternalReferenceResolver externalResolver)
    {
      ValidateParameters(input, contentManager, ref parameters, externalResolver);

      using (JsonSavableReader reader = new JsonSavableReader(contentManager.ServiceProvider, input, externalResolver, true))
        return reader.ReadRootSavable<ISavable?>();
    }
  }
}
