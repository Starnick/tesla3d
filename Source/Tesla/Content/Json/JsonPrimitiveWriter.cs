﻿/*
* Copyright (c) 2010-2024 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.IO.Compression;
using System.Runtime.CompilerServices;
using System.Text.Json;
using Tesla.Utilities;

#nullable enable

namespace Tesla.Content
{
  /// <summary>
  /// Writes primitive values as formatted JSON to a stream.
  /// </summary>
  public class JsonPrimitiveWriter : IPrimitiveWriter
  {
    private bool m_isDisposed;
    private Utf8JsonWriter m_jsonWriter;
    private Stream m_outStream;
    private bool m_compressBlobs;
    private bool m_leaveOpen;
    private Stack<bool> m_depthStack;

    /// <summary>
    /// Gets if the writer has been disposed or not.
    /// </summary>
    public bool IsDisposed
    {
      get
      {
        return m_isDisposed;
      }
    }

    /// <summary>
    /// Gets or sets the underlying outpt stream.
    /// </summary>
    protected Stream OutStream
    {
      get
      {
        return m_outStream;
      }
      set
      {
        m_jsonWriter.Flush();

        m_outStream = value;
        m_jsonWriter.Reset(value);

        // If this is greater than zero, most likely left something dangling open...
        Debug.Assert(m_depthStack.Count == 0);
      }
    }

    /// <summary>
    /// Gets the underlying binary writer.
    /// </summary>
    protected Utf8JsonWriter UnderlyingJsonWriter
    {
      get
      {
        return m_jsonWriter;
      }
    }

    /// <summary>
    /// Gets or sets if blobs should be compressed.
    /// </summary>
    protected bool CompressBlobs 
    {
      get
      {
        return m_compressBlobs;
      }
      set
      {
        m_compressBlobs = value;
      }
    }

    /// <summary>
    /// Gets if the current depth is an object. If false, then it is an array.
    /// Values written to an array discard their property names, if any are specified. This allows the same methods to write to either.
    /// </summary>
    protected bool IsInObject
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        if (m_depthStack.TryPeek(out bool isObject))
          return isObject;

        return false;
      }
    }

    /// <summary>
    /// Gets if the current depth is an array. If false, then it is an object.
    /// Values written to an array discard their property names, if any are specified. This allows the same methods to write to either.
    /// </summary>
    protected bool IsInArray
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        if (m_depthStack.TryPeek(out bool isObject))
          return !isObject;

        return false;
      }
    }

    /// <summary>
    /// Gets if the current depth is the "root" object.
    /// </summary>
    protected bool IsInRootObject
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        if (m_depthStack.Count == 1 && m_depthStack.TryPeek(out bool isObject))
          return isObject;

        return false;
      }
    }

    /// <summary>
    /// Gets if the current depth is the "root" array.
    /// </summary>
    protected bool IsInRootArray
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        if (m_depthStack.Count == 1 && m_depthStack.TryPeek(out bool isObject))
          return !isObject;

        return false;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="BinaryPrimitiveWriter"/> class.
    /// </summary>
    /// <param name="output">Output stream to write to.</param>
    /// <param name="leaveOpen">True if the output stream should NOT be disposed/closed when the writer is disposed/closed, false otherwise.</param>
    /// <exception cref="ArgumentNullException">Thrown if the stream is null.</exception>
    /// <exception cref="NotSupportedException">Thrown if the stream cannot be written to.</exception>
    public JsonPrimitiveWriter(Stream output, bool leaveOpen = false) : this(output, leaveOpen, true, true) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="BinaryPrimitiveWriter"/> class.
    /// </summary>
    /// <param name="output">Output stream to write to.</param>
    /// <param name="leaveOpen">True if the output stream should NOT be disposed/closed when the writer is disposed/closed, false otherwise.</param>
    /// <param name="startRootObject">True if the root object should be opened, false if not.</param>
    /// <param name="indent">True if the JSON should be indented, false if no prettyify.</param>
    /// <exception cref="ArgumentNullException">Thrown if the stream is null.</exception>
    /// <exception cref="NotSupportedException">Thrown if the stream cannot be written to.</exception>
    protected JsonPrimitiveWriter(Stream output, bool leaveOpen, bool startRootObject, bool indent)
    {
      ArgumentNullException.ThrowIfNull(output, nameof(output));

      if (!output.CanWrite)
        throw new NotSupportedException(StringLocalizer.Instance.GetLocalizedString("CannotWriteToStream"));

      m_jsonWriter = new Utf8JsonWriter(output, new JsonWriterOptions() { Indented = indent });
      m_outStream = output;
      m_depthStack = new Stack<bool>();
      m_leaveOpen = leaveOpen;

      if (startRootObject)
        StartRootObject();
    }

    /// <inheritdoc />
    public virtual void Close()
    {
      Dispose();
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public virtual void Flush()
    {
      CheckDisposed();

      if (m_depthStack.Count == 1)
        EndRootObject();

      m_jsonWriter.Flush();
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void Write(String name, byte value)
    {
      CheckDisposed();

      WritePropertyName(name);
      m_jsonWriter.WriteNumberValue((ulong) value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void Write(String name, byte? value)
    {
      CheckDisposed();

      if (WritePropertyNameIfWanted(name, value))
        m_jsonWriter.WriteNumberValueOrNull((ulong?) value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void Write(String name, ReadOnlySpan<byte> values)
    {
      CheckDisposed();

      if (WritePropertyNameIfWanted(name, values.IsEmpty))
      {
        if (WriteNullValueIfEmpty(values.IsEmpty))
        {
          WriteStartArray();

          for (int i = 0; i < values.Length; i++)
            m_jsonWriter.WriteNumberValue((ulong) values[i]);

          WriteEndArray();
        }
      }
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void Write(String name, sbyte value)
    {
      CheckDisposed();

      WritePropertyName(name);
      m_jsonWriter.WriteNumberValue((long) value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void Write(String name, sbyte? value)
    {
      CheckDisposed();

      if (WritePropertyNameIfWanted(name, value))
        m_jsonWriter.WriteNumberValueOrNull((long?) value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void Write(String name, ReadOnlySpan<sbyte> values)
    {
      CheckDisposed();

      if (WritePropertyNameIfWanted(name, values.IsEmpty))
      {
        if (WriteNullValueIfEmpty(values.IsEmpty))
        {
          WriteStartArray();

          for (int i = 0; i < values.Length; i++)
            m_jsonWriter.WriteNumberValue((long) values[i]);

          WriteEndArray();
        }
      }
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void Write(String name, char value)
    {
      CheckDisposed();

      WritePropertyName(name);
      m_jsonWriter.WriteStringValue(new ReadOnlySpan<char>(in value));
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void Write(String name, char? value)
    {
      CheckDisposed();

      if (WritePropertyNameIfWanted(name, value))
      {
        if (WriteNullValueIfNull(value))
          m_jsonWriter.WriteStringValue(new ReadOnlySpan<char>(in Nullable.GetValueRefOrDefaultRef(in value)));
      }
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void Write(String name, ReadOnlySpan<char> values)
    {
      CheckDisposed();

      if (WritePropertyNameIfWanted(name, values.IsEmpty))
      {
        if (WriteNullValueIfEmpty(values.IsEmpty))
        {
          WriteStartArray();

          for (int i = 0; i < values.Length; i++)
            m_jsonWriter.WriteStringValue(values.Slice(i, 1));

          WriteEndArray();
        }
      }
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void Write(String name, ushort value)
    {
      CheckDisposed();

      WritePropertyName(name);
      m_jsonWriter.WriteNumberValue((ulong) value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void Write(String name, ushort? value)
    {
      CheckDisposed();

      if (WritePropertyNameIfWanted(name, value))
        m_jsonWriter.WriteNumberValueOrNull((ulong?) value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void Write(String name, ReadOnlySpan<ushort> values)
    {
      CheckDisposed();

      if (WritePropertyNameIfWanted(name, values.IsEmpty))
      {
        if (WriteNullValueIfEmpty(values.IsEmpty))
        {
          WriteStartArray();

          for (int i = 0; i < values.Length; i++)
            m_jsonWriter.WriteNumberValue((ulong) values[i]);

          WriteEndArray();
        }
      }
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void Write(String name, uint value)
    {
      CheckDisposed();

      WritePropertyName(name);
      m_jsonWriter.WriteNumberValue((ulong) value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void Write(String name, uint? value)
    {
      CheckDisposed();

      if (WritePropertyNameIfWanted(name, value))
        m_jsonWriter.WriteNumberValueOrNull((ulong?) value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void Write(String name, ReadOnlySpan<uint> values)
    {
      CheckDisposed();

      if (WritePropertyNameIfWanted(name, values.IsEmpty))
      {
        if (WriteNullValueIfEmpty(values.IsEmpty))
        {
          WriteStartArray();

          for (int i = 0; i < values.Length; i++)
            m_jsonWriter.WriteNumberValue((ulong) values[i]);

          WriteEndArray();
        }
      }
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void Write(String name, ulong value)
    {
      CheckDisposed();

      WritePropertyName(name);
      m_jsonWriter.WriteNumberValue(value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void Write(String name, ulong? value)
    {
      CheckDisposed();

      if (WritePropertyNameIfWanted(name, value))
        m_jsonWriter.WriteNumberValueOrNull(value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void Write(String name, ReadOnlySpan<ulong> values)
    {
      CheckDisposed();

      if (WritePropertyNameIfWanted(name, values.IsEmpty))
      {
        if (WriteNullValueIfEmpty(values.IsEmpty))
        {
          WriteStartArray();

          for (int i = 0; i < values.Length; i++)
            m_jsonWriter.WriteNumberValue(values[i]);

          WriteEndArray();
        }
      }
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void Write(String name, short value)
    {
      CheckDisposed();

      WritePropertyName(name);
      m_jsonWriter.WriteNumberValue((long) value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void Write(String name, short? value)
    {
      CheckDisposed();

      if (WritePropertyNameIfWanted(name, value))
        m_jsonWriter.WriteNumberValueOrNull((long?) value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void Write(String name, ReadOnlySpan<short> values)
    {
      CheckDisposed();

      if (WritePropertyNameIfWanted(name, values.IsEmpty))
      {
        if (WriteNullValueIfEmpty(values.IsEmpty))
        {
          WriteStartArray();

          for (int i = 0; i < values.Length; i++)
            m_jsonWriter.WriteNumberValue((long) values[i]);

          WriteEndArray();
        }
      }
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void Write(String name, int value)
    {
      CheckDisposed();

      WritePropertyName(name);
      m_jsonWriter.WriteNumberValue((long) value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void Write(String name, int? value)
    {
      CheckDisposed();

      if (WritePropertyNameIfWanted(name, value))
        m_jsonWriter.WriteNumberValueOrNull((long?) value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void Write(String name, ReadOnlySpan<int> values)
    {
      CheckDisposed();

      if (WritePropertyNameIfWanted(name, values.IsEmpty))
      {
        if (WriteNullValueIfEmpty(values.IsEmpty))
        {
          WriteStartArray();

          for (int i = 0; i < values.Length; i++)
            m_jsonWriter.WriteNumberValue((long) values[i]);

          WriteEndArray();
        }
      }
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void Write(String name, long value)
    {
      CheckDisposed();

      WritePropertyName(name);
      m_jsonWriter.WriteNumberValue(value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void Write(String name, long? value)
    {
      CheckDisposed();

      if (WritePropertyNameIfWanted(name, value))
        m_jsonWriter.WriteNumberValueOrNull(value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void Write(String name, ReadOnlySpan<long> values)
    {
      CheckDisposed();

      if (WritePropertyNameIfWanted(name, values.IsEmpty))
      {
        if (WriteNullValueIfEmpty(values.IsEmpty))
        {
          WriteStartArray();

          for (int i = 0; i < values.Length; i++)
            m_jsonWriter.WriteNumberValue(values[i]);

          WriteEndArray();
        }
      }
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void Write(String name, Half value)
    {
      CheckDisposed();

      WritePropertyName(name);
      m_jsonWriter.WriteNumberValue((float) value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void Write(String name, Half? value)
    {
      CheckDisposed();

      if (WritePropertyNameIfWanted(name, value))
        m_jsonWriter.WriteNumberValueOrNull((float?) value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void Write(String name, ReadOnlySpan<Half> values)
    {
      CheckDisposed();

      if (WritePropertyNameIfWanted(name, values.IsEmpty))
      {
        if (WriteNullValueIfEmpty(values.IsEmpty))
        {
          WriteStartArray();

          for (int i = 0; i < values.Length; i++)
            m_jsonWriter.WriteNumberValue((float) values[i]);

          WriteEndArray();
        }
      }
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void Write(String name, float value)
    {
      CheckDisposed();

      WritePropertyName(name);
      m_jsonWriter.WriteNumberValue(value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void Write(String name, float? value)
    {
      CheckDisposed();

      if (WritePropertyNameIfWanted(name, value))
        m_jsonWriter.WriteNumberValueOrNull(value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void Write(String name, ReadOnlySpan<float> values)
    {
      CheckDisposed();

      if (WritePropertyNameIfWanted(name, values.IsEmpty))
      {
        if (WriteNullValueIfEmpty(values.IsEmpty))
        {
          WriteStartArray();

          for (int i = 0; i < values.Length; i++)
            m_jsonWriter.WriteNumberValue(values[i]);

          WriteEndArray();
        }
      }
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void Write(String name, double value)
    {
      CheckDisposed();

      WritePropertyName(name);
      m_jsonWriter.WriteNumberValue(value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void Write(String name, double? value)
    {
      CheckDisposed();

      if (WritePropertyNameIfWanted(name, value))
        m_jsonWriter.WriteNumberValueOrNull(value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void Write(String name, ReadOnlySpan<double> values)
    {
      CheckDisposed();

      if (WritePropertyNameIfWanted(name, values.IsEmpty))
      {
        if (WriteNullValueIfEmpty(values.IsEmpty))
        {
          WriteStartArray();

          for (int i = 0; i < values.Length; i++)
            m_jsonWriter.WriteNumberValue(values[i]);

          WriteEndArray();
        }
      }
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void Write(String name, decimal value)
    {
      CheckDisposed();

      WritePropertyName(name);
      m_jsonWriter.WriteNumberValue(value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void Write(String name, decimal? value)
    {
      CheckDisposed();

      if (WritePropertyNameIfWanted(name, value))
        m_jsonWriter.WriteNumberValueOrNull(value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void Write(String name, ReadOnlySpan<decimal> values)
    {
      CheckDisposed();

      if (WritePropertyNameIfWanted(name, values.IsEmpty))
      {
        if (WriteNullValueIfEmpty(values.IsEmpty))
        {
          WriteStartArray();

          for (int i = 0; i < values.Length; i++)
            m_jsonWriter.WriteNumberValue(values[i]);

          WriteEndArray();
        }
      }
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void Write(String name, bool value)
    {
      CheckDisposed();

      WritePropertyName(name);
      m_jsonWriter.WriteBooleanValue(value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void Write(String name, bool? value)
    {
      CheckDisposed();

      if (WritePropertyNameIfWanted(name, value))
        m_jsonWriter.WriteBooleanValueOrNull(value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void Write(String name, ReadOnlySpan<bool> values)
    {
      CheckDisposed();

      if (WritePropertyNameIfWanted(name, values.IsEmpty))
      {
        if (WriteNullValueIfEmpty(values.IsEmpty))
        {
          WriteStartArray();

          for (int i = 0; i < values.Length; i++)
            m_jsonWriter.WriteBooleanValue(values[i]);

          WriteEndArray();
        }
      }
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void Write(String name, String? value)
    {
      CheckDisposed();

      if (WritePropertyNameIfWanted(name, String.IsNullOrEmpty(value)))
        m_jsonWriter.WriteStringValueOrNull(value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void Write(String name, ReadOnlySpan<String?> values)
    {
      CheckDisposed();

      if (WritePropertyNameIfWanted(name, values.IsEmpty))
      {
        if (WriteNullValueIfEmpty(values.IsEmpty))
        {
          WriteStartArray();

          for (int i = 0; i < values.Length; i++)
            m_jsonWriter.WriteStringValueOrNull(values[i]);

          WriteEndArray();
        }
      }
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void Write<T>(String name, in T value) where T : struct, IPrimitiveValue
    {
      CheckDisposed();

      WriteStartObject(name);

      value.Write(this);

      WriteEndObject();
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void Write<T>(String name, in T? value) where T : struct, IPrimitiveValue
    {
      CheckDisposed();

      if (WritePropertyNameIfWanted(name, value))
      {
        if (WriteNullValueIfNull(value))
        {
          WriteStartObject();

          value.Value.Write(this);

          WriteEndObject();
        }
      }
    }
    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>

    public void Write<T>(String name, ReadOnlySpan<T> values) where T : struct, IPrimitiveValue
    {
      CheckDisposed();

      if (WritePropertyNameIfWanted(name, values.IsEmpty))
      {
        if (WriteNullValueIfEmpty(values.IsEmpty))
        {
          WriteStartArray();

          for (int i = 0; i < values.Length; i++)
          {
            WriteStartObject();

            ref readonly T value = ref values[i];
            value.Write(this);

            WriteEndObject();
          }

          WriteEndArray();
        }
      }
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void WriteBlob<T>(String name, ReadOnlySpan<T> values) where T : unmanaged
    {
      CheckDisposed();

      if (WritePropertyNameIfWanted(name, values.IsEmpty))
      {
        if (WriteNullValueIfNull(values.IsEmpty))
        {
          WriteStartObject();

          WriteBlobHeader(new PrimitiveBlobHeader(values.Length, BufferHelper.SizeOf<T>(), typeof(T)));
          WriteBase64String(JsonPropertyNames.Blob, values.AsBytes(), m_compressBlobs);

          WriteEndObject();
        }
      }
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void WriteBlob(String name, IReadOnlyDataBuffer? values)
    {
      CheckDisposed();

      if (WritePropertyNameIfWanted(name, values is null || values.Length == 0))
      {
        if (WriteNullValueIfNull(values is null || values.Length == 0))
        {
          WriteStartObject();

          WriteBlobHeader(new PrimitiveBlobHeader(values));
          WriteBase64String(JsonPropertyNames.Blob, values!.Bytes, m_compressBlobs);

          WriteEndObject();
        }
      }
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void WriteEnum<T>(String name, T enumValue) where T : struct, Enum
    {
      CheckDisposed();

      WritePropertyName(name);
      WriteEnumValue<T>(enumValue);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void WriteEnum<T>(String name, T? enumValue) where T : struct, Enum
    {
      CheckDisposed();

      if (WritePropertyNameIfWanted(name, enumValue))
      {
        if (enumValue.HasValue)
          WriteEnumValue<T>(enumValue.Value);
        else
          m_jsonWriter.WriteNullValue();
      }
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private void WriteEnumValue<T>(T enumValue) where T : struct, Enum
    {
      string? stringValue = Enum.GetName<T>(enumValue);
      if (stringValue is not null)
      {
        m_jsonWriter.WriteStringValue(stringValue);
        return;
      }

      // Fallback to a number of a string value couldn't be found
      TypeCode typeCode = enumValue.GetTypeCode();
      switch (typeCode)
      {
        case TypeCode.Byte:
          m_jsonWriter.WriteNumberValue((ulong) Unsafe.As<T, byte>(ref enumValue));
          break;
        case TypeCode.SByte:
          m_jsonWriter.WriteNumberValue((long) Unsafe.As<T, sbyte>(ref enumValue));
          break;
        case TypeCode.Int16:
          m_jsonWriter.WriteNumberValue((long) Unsafe.As<T, short>(ref enumValue));
          break;
        case TypeCode.Int32:
          m_jsonWriter.WriteNumberValue((long) Unsafe.As<T, int>(ref enumValue));
          break;
        case TypeCode.Int64:
          m_jsonWriter.WriteNumberValue(Unsafe.As<T, long>(ref enumValue));
          break;
        case TypeCode.UInt16:
          m_jsonWriter.WriteNumberValue((ulong) Unsafe.As<T, ushort>(ref enumValue));
          break;
        case TypeCode.UInt32:
          m_jsonWriter.WriteNumberValue((ulong) Unsafe.As<T, uint>(ref enumValue));
          break;
        case TypeCode.UInt64:
          m_jsonWriter.WriteNumberValue(Unsafe.As<T, ulong>(ref enumValue));
          break;
        default:
          throw new InvalidOperationException(); // Shouldn't happen
      }
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void BeginWriteGroup(String name)
    {
      CheckDisposed();

      WriteStartObject(name);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void BeginWriteGroup(String name, int count)
    {
      CheckDisposed();

      WriteStartArray(name);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void EndWriteGroup()
    {
      CheckDisposed();

      if (IsInObject)
        WriteEndObject();
      else
        WriteEndArray();
    }

    /// <inheritdoc />
    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    /// <inheritdoc />
    protected virtual void Dispose(bool disposing)
    {
      if (!m_isDisposed)
      {
        if (disposing)
        {
          Flush();
          m_jsonWriter.Dispose();

          if (!m_leaveOpen)
            m_outStream.Close();
        }

        m_isDisposed = true;
      }
    }

    /// <summary>
    /// Used by subclassed writers to mark us disposed, if overriding the dispose method.
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected void MarkIsDisposed()
    {
      m_isDisposed = true;
    }

    /// <summary>
    /// Throws an exception if the current writer has been disposed.
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected void CheckDisposed()
    {
      if (m_isDisposed)
        throw new ObjectDisposedException(GetType().Name);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected void WritePropertyName(String name)
    {
      if (IsInObject)
        m_jsonWriter.WritePropertyName(JsonPropertyNames.GetEncodedString(name));
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected void WritePropertyName(JsonEncodedText name)
    {
      if (IsInObject)
        m_jsonWriter.WritePropertyName(name);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected bool WritePropertyNameIfWanted<T>(String name, in T? obj)
    {
      // If writing to an object, if it's null we don't want to actually write it out
      if (IsInObject)
      {
        if (obj is null)
          return false;

        m_jsonWriter.WritePropertyName(JsonPropertyNames.GetEncodedString(name));
      }

      return true; // Always write the null value if in array, else the arrays won't have the correct order of values
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected bool WritePropertyNameIfWanted<T>(JsonEncodedText name, in T? obj)
    {
      // If writing to an object, if it's null we don't want to actually write it out
      if (IsInObject)
      {
        if (obj is null)
          return false;

        m_jsonWriter.WritePropertyName(name);
      }

      return true; // Always write the null value if in array, else the arrays won't have the correct order of values
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected bool WritePropertyNameIfWanted(String name, bool isNullOrEmpty)
    {
      // If writing to an object, if it's null we don't want to actually write it out
      if (IsInObject)
      {
        if (isNullOrEmpty)
          return false;

        m_jsonWriter.WritePropertyName(JsonPropertyNames.GetEncodedString(name));
      }

      return true; // Always write the null value if in array, else the arrays won't have the correct order of values
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected bool WritePropertyNameIfWanted(JsonEncodedText name, bool isNullOrEmpty)
    {
      // If writing to an object, if it's null we don't want to actually write it out
      if (IsInObject)
      {
        if (isNullOrEmpty)
          return false;

        m_jsonWriter.WritePropertyName(name);
      }

      return true; // Always write the null value if in array, else the arrays won't have the correct order of values
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected bool WriteNullValueIfNull<T>([NotNullWhen(true)] in T? obj)
    {
      if (obj is null)
      {
        // or empty object?
        m_jsonWriter.WriteNullValue();
        return false;
      }

      return true;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected bool WriteNullValueIfEmpty(bool isNullOrEmpty)
    {
      if (isNullOrEmpty)
      {
        // or empty array?
        m_jsonWriter.WriteNullValue();
        return false;
      }

      return true;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected void WriteStartArray(string? name = null)
    {
      if (name is not null && IsInObject)
        m_jsonWriter.WriteStartArray(JsonPropertyNames.GetEncodedString(name));
      else
        m_jsonWriter.WriteStartArray();

      m_depthStack.Push(false);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected void WriteStartArray(JsonEncodedText? name)
    {
      if (name is not null && IsInObject)
        m_jsonWriter.WriteStartArray(name.Value);
      else
        m_jsonWriter.WriteStartArray();

      m_depthStack.Push(false);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected void WriteEndArray()
    {
      m_jsonWriter.WriteEndArray();

      m_depthStack.Pop();
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected void WriteStartObject(string? name = null)
    {
      if (name is not null && IsInObject)
        m_jsonWriter.WriteStartObject(JsonPropertyNames.GetEncodedString(name));
      else
        m_jsonWriter.WriteStartObject();

      m_depthStack.Push(true);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected void WriteStartObject(JsonEncodedText? name)
    {
      if (name.HasValue && IsInObject)
        m_jsonWriter.WriteStartObject(name.Value);
      else
        m_jsonWriter.WriteStartObject();

      m_depthStack.Push(true);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected void WriteEndObject()
    {
      m_jsonWriter.WriteEndObject();

      m_depthStack.Pop();
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected void WriteEmptyArray(string name)
    {
      WriteStartArray(JsonPropertyNames.GetEncodedString(name));
      WriteEndArray();
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected void WriteEmptyObject(string name)
    {
      WriteStartObject(JsonPropertyNames.GetEncodedString(name));
      WriteEndObject();
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected bool WriteBlobHeader(in PrimitiveBlobHeader blobHeader)
    {
      m_jsonWriter.WriteNumber(JsonPropertyNames.ElementCount, blobHeader.ElementCount);
      m_jsonWriter.WriteNumber(JsonPropertyNames.ElementSizeInBytes, blobHeader.ElementSizeInBytes);
      WriteBlobType(blobHeader.ElementType);

      if (m_compressBlobs)
        m_jsonWriter.WriteBoolean(JsonPropertyNames.IsCompressed, true);

      return true;
    }

    /// <summary>
    /// Writes the type of the blob. By default this writes out a string representing the fully qualified type name.
    /// </summary>
    /// <param name="type">Runtime type.</param>
    protected virtual void WriteBlobType(Type type)
    {
      String typeName = SmartActivator.GetAssemblyQualifiedName(type);
      m_jsonWriter.WriteString(JsonPropertyNames.ElementType, JsonPropertyNames.GetEncodedString(typeName));
    }

    /// <summary>
    /// Writes a byte buffer as a base64 encoded string, with the option to first compress the data before encoding.
    /// </summary>
    /// <param name="propertyName">Optional property name, if null just the value is written.</param>
    /// <param name="bytes">Bytes to encode to base64.</param>
    /// <param name="compress">True if the bytes should be first compressed, false if not.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected void WriteBase64String(JsonEncodedText? propertyName, ReadOnlySpan<byte> bytes, bool compress = false)
    {
      if (propertyName.HasValue)
        m_jsonWriter.WritePropertyName(propertyName.Value);

      if (compress)
      {
        using (DataBufferStream compressed = DataBufferStream.FromPool(bytes.Length))
        {
          using (BrotliStream compressor = new BrotliStream(compressed, CompressionLevel.SmallestSize, true))
            compressor.Write(bytes);

          
          m_jsonWriter.WriteBase64StringValue(compressed.AsSpan());
        }
      }
      else
      {
        m_jsonWriter.WriteBase64StringValue(bytes);
      }
    }

    protected virtual void StartRootObject()
    {
      if (m_depthStack.Count == 0)
        WriteStartObject();

      Debug.Assert(m_depthStack.Count == 1);
    }

    protected virtual void EndRootObject()
    {
      if (m_depthStack.Count == 1)
        WriteEndObject();

      Debug.Assert(m_depthStack.Count == 0);
    }
  }
}