﻿/*
* Copyright (c) 2010-2024 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Runtime.CompilerServices;
using System.Text.Json;
using Tesla.Utilities;

#nullable enable

namespace Tesla.Content
{
  /// <summary>
  /// Reads formatted JSON data from a stream.
  /// </summary>
  public class JsonPrimitiveReader : IPrimitiveReader
  {
    private bool m_isDisposed;
    private JsonDocument m_document;
    private Stack<JsonElementEntry> m_depthStack;
    private JsonElementEntry m_current;
    private Stream? m_input;

    /// <summary>
    /// Gets if this reader has been disposed or not.
    /// </summary>
    public bool IsDisposed 
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get 
      { 
        return m_isDisposed; 
      } 
    }

    /// <summary>
    /// Gets the json document.
    /// </summary>
    protected JsonDocument Document
    {
      get
      {
        return m_document;
      }
    }

    /// <summary>
    /// Gets the current json element.
    /// </summary>
    protected JsonElement CurrentElement 
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get 
      { 
        return m_current.Element; 
      } 
    }

    /// <summary>
    /// Gets the current element's kind.
    /// </summary>
    protected JsonValueKind CurrentValueKind
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return m_current.ValueKind;
      }
    }

    /// <summary>
    /// Gets the index value of the current element, see <see cref="TryGetNextArrayElement(out JsonElement)"/>.
    /// </summary>
    protected int CurrentArrayIndex
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get 
      { 
        return m_current.ArrayIndex; 
      } 
    }

    /// <summary>
    /// Gets the current element's array length, if it's an array, else 0.
    /// </summary>
    protected int CurrentArrayLength 
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get 
      { 
        return m_current.ArrayLength; 
      } 
    }

    /// <summary>
    /// Gets if the current element is an object, if not then is an array.
    /// </summary>
    protected bool IsInObject
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return m_current.ValueKind == JsonValueKind.Object;
      }
    }

    /// <summary>
    /// Gets if the current element is an array, if not then is an object.
    /// </summary>
    protected bool IsInArray
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return m_current.ValueKind == JsonValueKind.Array;
      }
    }

    /// <summary>
    /// Gets if the current depth is the "root" object.
    /// </summary>
    protected bool IsInRootObject
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return m_depthStack.Count == 0 && IsInObject;
      }
    }

    /// <summary>
    /// Gets if the current depth is the "root" array.
    /// </summary>
    protected bool IsInRootArray
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return m_depthStack.Count == 0 && IsInArray;
      }
    }

    public JsonPrimitiveReader(Stream input, bool leaveOpen = false)
    {
      ArgumentNullException.ThrowIfNull(input, nameof(input));

      if (!input.CanRead || !input.CanSeek)
        throw new NotSupportedException(StringLocalizer.Instance.GetLocalizedString("CannotReadFromStream"));

      m_isDisposed = false;
      m_input = (leaveOpen) ? input : null;
      m_document = JsonDocument.Parse(input, new JsonDocumentOptions() { CommentHandling = JsonCommentHandling.Skip, AllowTrailingCommas = true });
      m_current = new JsonElementEntry(m_document.RootElement);
      m_depthStack = new Stack<JsonElementEntry>();
    }

    /// <inheritdoc />
    public void Close()
    {
      Dispose();
    }

    /// <inheritdoc />
    /// <exception cref="InvalidCastException">Thrown if the element is not of the specified type.</exception>
    /// <exception cref="FormatException">Thrown if the element could not be formatted as the specified type.</exception>
    public byte ReadByte(String name)
    {
      CheckDisposed();

      JsonElement elem = GetAndValidate(name, JsonValueKind.Number);
      return elem.GetByte();
    }

    /// <inheritdoc />
    /// <exception cref="InvalidCastException">Thrown if the element is not of the specified type.</exception>
    /// <exception cref="FormatException">Thrown if the element could not be formatted as the specified type.</exception>
    public byte? ReadNullableByte(String name)
    {
      CheckDisposed();

      if (GetAndValidateOrNull(name, JsonValueKind.Number, out JsonElement elem))
        return elem.GetByte();

      return null;
    }

    /// <inheritdoc />
    /// <exception cref="InvalidCastException">Thrown if the array elements are not of the specified type.</exception>
    /// <exception cref="FormatException">Thrown if the array elements could not be formatted as the specified type.</exception>
    public int ReadByteArray(String name, Span<byte> values)
    {
      CheckDisposed();

      if (GetAndValidateOrNull(name, JsonValueKind.Array, out JsonElement arrayElem))
      {
        int elemCount = Math.Min(values.Length, arrayElem.GetArrayLength());

        for (int i = 0; i < elemCount; i++)
        {
          JsonElement elemAt = arrayElem.GetElementAt(i, JsonValueKind.Number, name);
          values[i] = elemAt.GetByte();
        }

        return elemCount;
      }

      return 0;
    }

    /// <summary>
    /// Reads a single sbyte from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>SByte value.</returns>
    /// <exception cref="InvalidCastException">Thrown if the element is not of the specified type.</exception>
    /// <exception cref="FormatException">Thrown if the element could not be formatted as the specified type.</exception>
    public sbyte ReadSByte(String name)
    {
      CheckDisposed();

      JsonElement elem = GetAndValidate(name, JsonValueKind.Number);
      return elem.GetSByte();
    }

    /// <summary>
    /// Reads a single nullable sbyte from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>Nullable sbyte value.</returns>
    /// <exception cref="InvalidCastException">Thrown if the element is not of the specified type.</exception>
    /// <exception cref="FormatException">Thrown if the element could not be formatted as the specified type.</exception>
    public sbyte? ReadNullableSByte(String name)
    {
      CheckDisposed();

      if (GetAndValidateOrNull(name, JsonValueKind.Number, out JsonElement elem))
        return elem.GetSByte();

      return null;
    }

    /// <summary>
    /// Reads an array of sbytes from the input. If the receiving span is smaller than the number of elements that can be read, it will read
    /// as many as it can and skip the rest. Use <see cref="PeekArrayCount"/> to determine how large the receiving span should be.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Span to hold the array values.</param>
    /// <returns>Number of values read.</returns>
    /// <exception cref="InvalidCastException">Thrown if the array elements are not of the specified type.</exception>
    /// <exception cref="FormatException">Thrown if the array elements could not be formatted as the specified type.</exception>
    public int ReadSByteArray(String name, Span<sbyte> values)
    {
      CheckDisposed();

      if (GetAndValidateOrNull(name, JsonValueKind.Array, out JsonElement arrayElem))
      {
        int elemCount = Math.Min(values.Length, arrayElem.GetArrayLength());

        for (int i = 0; i < elemCount; i++)
        {
          JsonElement elemAt = arrayElem.GetElementAt(i, JsonValueKind.Number, name);
          values[i] = elemAt.GetSByte();
        }

        return elemCount;
      }

      return 0;
    }

    /// <summary>
    /// Reads a single char from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>Char value.</returns>
    /// <exception cref="InvalidCastException">Thrown if the element is not a string value of exactly one char.</exception>
    public char ReadChar(String name)
    {
      CheckDisposed();

      JsonElement elem = GetAndValidate(name, JsonValueKind.String);
      return elem.GetChar();
    }

    /// <summary>
    /// Reads a single nullable char from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>Nullable char value.</returns>
    /// <exception cref="InvalidCastException">Thrown if the element is not a string value of exactly one char.</exception>
    public char? ReadNullableChar(String name)
    {
      CheckDisposed();

      if (GetAndValidateOrNull(name, JsonValueKind.String, out JsonElement elem))
        return elem.GetChar();

      return null;
    }

    /// <summary>
    /// Reads an array of chars from the input. If the receiving span is smaller than the number of elements that can be read, it will read
    /// as many as it can and skip the rest. Use <see cref="PeekArrayCount"/> to determine how large the receiving span should be.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Span to hold the array values.</param>
    /// <returns>Number of values read.</returns>
    /// <exception cref="InvalidCastException">Thrown if the element is not a string value (array of characters).</exception>
    public int ReadCharArray(String name, Span<char> values)
    {
      CheckDisposed();

      if (GetAndValidateOrNull(name, JsonValueKind.Array, out JsonElement arrayElem))
      {
        int elemCount = Math.Min(values.Length, arrayElem.GetArrayLength());

        for (int i = 0; i < elemCount; i++)
        {
          JsonElement elemAt = arrayElem.GetElementAt(i, JsonValueKind.String, name);
          values[i] = elemAt.GetChar();
        }

        return elemCount;
      }

      return 0;
    }

    /// <summary>
    /// Reads a single unsigned 16-bit int from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>UInt16 value.</returns>
    /// <exception cref="InvalidCastException">Thrown if the element is not of the specified type.</exception>
    /// <exception cref="FormatException">Thrown if the element could not be formatted as the specified type.</exception>
    public ushort ReadUInt16(String name)
    {
      CheckDisposed();

      JsonElement elem = GetAndValidate(name, JsonValueKind.Number);
      return elem.GetUInt16();
    }

    /// <summary>
    /// Reads a single nullable unsigned 16-bit int from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>Nullable uint16 value.</returns>
    /// <exception cref="InvalidCastException">Thrown if the element is not of the specified type.</exception>
    /// <exception cref="FormatException">Thrown if the element could not be formatted as the specified type.</exception>
    public ushort? ReadNullableUInt16(String name)
    {
      CheckDisposed();

      if (GetAndValidateOrNull(name, JsonValueKind.Number, out JsonElement elem))
        return elem.GetUInt16();

      return null;
    }

    /// <summary>
    /// Reads an array of unsigned 16-bit ints from the input. If the receiving span is smaller than the number of elements that can be read, it will read
    /// as many as it can and skip the rest. Use <see cref="PeekArrayCount"/> to determine how large the receiving span should be.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Span to hold the array values.</param>
    /// <returns>Number of values read.</returns>
    /// <exception cref="InvalidCastException">Thrown if the array elements are not of the specified type.</exception>
    /// <exception cref="FormatException">Thrown if the array elements could not be formatted as the specified type.</exception>
    public int ReadUInt16Array(String name, Span<ushort> values)
    {
      CheckDisposed();

      if (GetAndValidateOrNull(name, JsonValueKind.Array, out JsonElement arrayElem))
      {
        int elemCount = Math.Min(values.Length, arrayElem.GetArrayLength());

        for (int i = 0; i < elemCount; i++)
        {
          JsonElement elemAt = arrayElem.GetElementAt(i, JsonValueKind.Number, name);
          values[i] = elemAt.GetUInt16();
        }

        return elemCount;
      }

      return 0;
    }

    /// <summary>
    /// Reads a single unsigned 32-bit int from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>UInt32 value.</returns>
    /// <exception cref="InvalidCastException">Thrown if the element is not of the specified type.</exception>
    /// <exception cref="FormatException">Thrown if the element could not be formatted as the specified type.</exception>
    public uint ReadUInt32(String name)
    {
      CheckDisposed();

      JsonElement elem = GetAndValidate(name, JsonValueKind.Number);
      return elem.GetUInt32();
    }

    /// <summary>
    /// Reads a single nullable unsigned 32-bit int from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>Nullable uint32 value.</returns>
    /// <exception cref="InvalidCastException">Thrown if the element is not of the specified type.</exception>
    /// <exception cref="FormatException">Thrown if the element could not be formatted as the specified type.</exception>
    public uint? ReadNullableUInt32(String name)
    {
      CheckDisposed();

      if (GetAndValidateOrNull(name, JsonValueKind.Number, out JsonElement elem))
        return elem.GetUInt32();

      return null;
    }

    /// <summary>
    /// Reads an array of unsigned 32-bits int from the input. If the receiving span is smaller than the number of elements that can be read, it will read
    /// as many as it can and skip the rest. Use <see cref="PeekArrayCount"/> to determine how large the receiving span should be.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Span to hold the array values.</param>
    /// <returns>Number of values read.</returns>
    /// <exception cref="InvalidCastException">Thrown if the array elements are not of the specified type.</exception>
    /// <exception cref="FormatException">Thrown if the array elements could not be formatted as the specified type.</exception>
    public int ReadUInt32Array(String name, Span<uint> values)
    {
      CheckDisposed();

      if (GetAndValidateOrNull(name, JsonValueKind.Array, out JsonElement arrayElem))
      {
        int elemCount = Math.Min(values.Length, arrayElem.GetArrayLength());

        for (int i = 0; i < elemCount; i++)
        {
          JsonElement elemAt = arrayElem.GetElementAt(i, JsonValueKind.Number, name);
          values[i] = elemAt.GetUInt32();
        }

        return elemCount;
      }

      return 0;
    }

    /// <summary>
    /// Reads a single unsigned 64-bit int from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>UInt64 value.</returns>
    /// <exception cref="InvalidCastException">Thrown if the element is not of the specified type.</exception>
    /// <exception cref="FormatException">Thrown if the element could not be formatted as the specified type.</exception>
    public ulong ReadUInt64(String name)
    {
      CheckDisposed();

      JsonElement elem = GetAndValidate(name, JsonValueKind.Number);
      return elem.GetUInt64();
    }

    /// <summary>
    /// Reads a single nullable unsigned 64-bit int from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>Nullable uint64 value.</returns>
    /// <exception cref="InvalidCastException">Thrown if the element is not of the specified type.</exception>
    /// <exception cref="FormatException">Thrown if the element could not be formatted as the specified type.</exception>
    public ulong? ReadNullableUInt64(String name)
    {
      CheckDisposed();

      if (GetAndValidateOrNull(name, JsonValueKind.Number, out JsonElement elem))
        return elem.GetUInt64();

      return null;
    }

    /// <summary>
    /// Reads an array of unsigned 64-bits int from the input. If the receiving span is smaller than the number of elements that can be read, it will read
    /// as many as it can and skip the rest. Use <see cref="PeekArrayCount"/> to determine how large the receiving span should be.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Span to hold the array values.</param>
    /// <returns>Number of values read.</returns>
    /// <exception cref="InvalidCastException">Thrown if the array elements are not of the specified type.</exception>
    /// <exception cref="FormatException">Thrown if the array elements could not be formatted as the specified type.</exception>
    public int ReadUInt64Array(String name, Span<ulong> values)
    {
      CheckDisposed();

      if (GetAndValidateOrNull(name, JsonValueKind.Array, out JsonElement arrayElem))
      {
        int elemCount = Math.Min(values.Length, arrayElem.GetArrayLength());

        for (int i = 0; i < elemCount; i++)
        {
          JsonElement elemAt = arrayElem.GetElementAt(i, JsonValueKind.Number, name);
          values[i] = elemAt.GetUInt64();
        }

        return elemCount;
      }

      return 0;
    }

    /// <summary>
    /// Reads a single 16-bit int from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>Int16 value.</returns>
    /// <exception cref="InvalidCastException">Thrown if the element is not of the specified type.</exception>
    /// <exception cref="FormatException">Thrown if the element could not be formatted as the specified type.</exception>
    public short ReadInt16(String name)
    {
      CheckDisposed();

      JsonElement elem = GetAndValidate(name, JsonValueKind.Number);
      return elem.GetInt16();
    }

    /// <summary>
    /// Reads a single nullable 16-bit int from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>Nullable int16 value.</returns>
    /// <exception cref="InvalidCastException">Thrown if the element is not of the specified type.</exception>
    /// <exception cref="FormatException">Thrown if the element could not be formatted as the specified type.</exception>
    public short? ReadNullableInt16(String name)
    {
      CheckDisposed();

      if (GetAndValidateOrNull(name, JsonValueKind.Number, out JsonElement elem))
        return elem.GetInt16();

      return null;
    }

    /// <summary>
    /// Reads an array of 16-bits int from the input. If the receiving span is smaller than the number of elements that can be read, it will read
    /// as many as it can and skip the rest. Use <see cref="PeekArrayCount"/> to determine how large the receiving span should be.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Span to hold the array values.</param>
    /// <returns>Number of values read.</returns>
    /// <exception cref="InvalidCastException">Thrown if the array elements are not of the specified type.</exception>
    /// <exception cref="FormatException">Thrown if the array elements could not be formatted as the specified type.</exception>
    public int ReadInt16Array(String name, Span<short> values)
    {
      CheckDisposed();

      if (GetAndValidateOrNull(name, JsonValueKind.Array, out JsonElement arrayElem))
      {
        int elemCount = Math.Min(values.Length, arrayElem.GetArrayLength());

        for (int i = 0; i < elemCount; i++)
        {
          JsonElement elemAt = arrayElem.GetElementAt(i, JsonValueKind.Number, name);
          values[i] = elemAt.GetInt16();
        }

        return elemCount;
      }

      return 0;
    }

    /// <summary>
    /// Reads a single 32-bit int from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>Int32 value.</returns>
    /// <exception cref="InvalidCastException">Thrown if the element is not of the specified type.</exception>
    /// <exception cref="FormatException">Thrown if the element could not be formatted as the specified type.</exception>
    public int ReadInt32(String name)
    {
      CheckDisposed();

      JsonElement elem = GetAndValidate(name, JsonValueKind.Number);
      return elem.GetInt32();
    }

    /// <summary>
    /// Reads a single nullable 32-bit int from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>Nullable int32 value.</returns>
    /// <exception cref="InvalidCastException">Thrown if the element is not of the specified type.</exception>
    /// <exception cref="FormatException">Thrown if the element could not be formatted as the specified type.</exception>
    public int? ReadNullableInt32(String name)
    {
      CheckDisposed();

      if (GetAndValidateOrNull(name, JsonValueKind.Number, out JsonElement elem))
        return elem.GetInt32();

      return null;
    }

    /// <summary>
    /// Reads an array of 32-bits int from the input. If the receiving span is smaller than the number of elements that can be read, it will read
    /// as many as it can and skip the rest. Use <see cref="PeekArrayCount"/> to determine how large the receiving span should be.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Span to hold the array values.</param>
    /// <returns>Number of values read.</returns>
    /// <exception cref="InvalidCastException">Thrown if the array elements are not of the specified type.</exception>
    /// <exception cref="FormatException">Thrown if the array elements could not be formatted as the specified type.</exception>
    public int ReadInt32Array(String name, Span<int> values)
    {
      CheckDisposed();

      if (GetAndValidateOrNull(name, JsonValueKind.Array, out JsonElement arrayElem))
      {
        int elemCount = Math.Min(values.Length, arrayElem.GetArrayLength());

        for (int i = 0; i < elemCount; i++)
        {
          JsonElement elemAt = arrayElem.GetElementAt(i, JsonValueKind.Number, name);
          values[i] = elemAt.GetInt32();
        }

        return elemCount;
      }

      return 0;
    }

    /// <summary>
    /// Reads a single 64-bit int from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>Int64 value.</returns>
    /// <exception cref="InvalidCastException">Thrown if the element is not of the specified type.</exception>
    /// <exception cref="FormatException">Thrown if the element could not be formatted as the specified type.</exception>
    public long ReadInt64(String name)
    {
      CheckDisposed();

      JsonElement elem = GetAndValidate(name, JsonValueKind.Number);
      return elem.GetInt64();
    }

    /// <summary>
    /// Reads a single nullable 64-bit int from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>Nullable int64 value.</returns>
    /// <exception cref="InvalidCastException">Thrown if the element is not of the specified type.</exception>
    /// <exception cref="FormatException">Thrown if the element could not be formatted as the specified type.</exception>
    public long? ReadNullableInt64(String name)
    {
      CheckDisposed();

      if (GetAndValidateOrNull(name, JsonValueKind.Number, out JsonElement elem))
        return elem.GetInt64();

      return null;
    }

    /// <summary>
    /// Reads an array of 64-bits int from the input. If the receiving span is smaller than the number of elements that can be read, it will read
    /// as many as it can and skip the rest. Use <see cref="PeekArrayCount"/> to determine how large the receiving span should be.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Span to hold the array values.</param>
    /// <returns>Number of values read.</returns>
    /// <exception cref="InvalidCastException">Thrown if the array elements are not of the specified type.</exception>
    /// <exception cref="FormatException">Thrown if the array elements could not be formatted as the specified type.</exception>
    public int ReadInt64Array(String name, Span<long> values)
    {
      CheckDisposed();

      if (GetAndValidateOrNull(name, JsonValueKind.Array, out JsonElement arrayElem))
      {
        int elemCount = Math.Min(values.Length, arrayElem.GetArrayLength());

        for (int i = 0; i < elemCount; i++)
        {
          JsonElement elemAt = arrayElem.GetElementAt(i, JsonValueKind.Number, name);
          values[i] = elemAt.GetInt64();
        }

        return elemCount;
      }

      return 0;
    }

    /// <summary>
    /// Reads a single half from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>Half value.</returns>
    /// <exception cref="InvalidCastException">Thrown if the element is not of the specified type.</exception>
    /// <exception cref="FormatException">Thrown if the element could not be formatted as the specified type.</exception>
    public Half ReadHalf(String name)
    {
      CheckDisposed();

      JsonElement elem = GetAndValidate(name, JsonValueKind.Number);
      return (Half) elem.GetSingle();
    }

    /// <summary>
    /// Reads a single nullable half from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>Nullable half value.</returns>
    /// <exception cref="InvalidCastException">Thrown if the element is not of the specified type.</exception>
    /// <exception cref="FormatException">Thrown if the element could not be formatted as the specified type.</exception>
    public Half? ReadNullableHalf(String name)
    {
      CheckDisposed();

      if (GetAndValidateOrNull(name, JsonValueKind.Number, out JsonElement elem))
        return (Half) elem.GetSingle();

      return null;
    }

    /// <summary>
    /// Reads an array of halfs from the input. If the receiving span is smaller than the number of elements that can be read, it will read
    /// as many as it can and skip the rest. Use <see cref="PeekArrayCount"/> to determine how large the receiving span should be.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Span to hold the array values.</param>
    /// <returns>Number of values read.</returns>
    /// <exception cref="InvalidCastException">Thrown if the array elements are not of the specified type.</exception>
    /// <exception cref="FormatException">Thrown if the array elements could not be formatted as the specified type.</exception>
    public int ReadHalfArray(String name, Span<Half> values)
    {
      CheckDisposed();

      if (GetAndValidateOrNull(name, JsonValueKind.Array, out JsonElement arrayElem))
      {
        int elemCount = Math.Min(values.Length, arrayElem.GetArrayLength());

        for (int i = 0; i < elemCount; i++)
        {
          JsonElement elemAt = arrayElem.GetElementAt(i, JsonValueKind.Number, name);
          values[i] = (Half) elemAt.GetSingle();
        }

        return elemCount;
      }

      return 0;
    }

    /// <summary>
    /// Reads a single float from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>Float value.</returns>
    /// <exception cref="InvalidCastException">Thrown if the element is not of the specified type.</exception>
    /// <exception cref="FormatException">Thrown if the element could not be formatted as the specified type.</exception>
    public float ReadSingle(String name)
    {
      CheckDisposed();

      JsonElement elem = GetAndValidate(name, JsonValueKind.Number);
      return elem.GetSingle();
    }

    /// <summary>
    /// Reads a single nullable float from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>Float value.</returns>
    /// <exception cref="InvalidCastException">Thrown if the element is not of the specified type.</exception>
    /// <exception cref="FormatException">Thrown if the element could not be formatted as the specified type.</exception>
    public float? ReadNullableSingle(String name)
    {
      CheckDisposed();

      if (GetAndValidateOrNull(name, JsonValueKind.Number, out JsonElement elem))
        return elem.GetSingle();

      return null;
    }

    /// <summary>
    /// Reads an array of floats from the input. If the receiving span is smaller than the number of elements that can be read, it will read
    /// as many as it can and skip the rest. Use <see cref="PeekArrayCount"/> to determine how large the receiving span should be.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Span to hold the array values.</param>
    /// <returns>Number of values read.</returns>
    /// <exception cref="InvalidCastException">Thrown if the array elements are not of the specified type.</exception>
    /// <exception cref="FormatException">Thrown if the array elements could not be formatted as the specified type.</exception>
    public int ReadSingleArray(String name, Span<float> values)
    {
      CheckDisposed();

      if (GetAndValidateOrNull(name, JsonValueKind.Array, out JsonElement arrayElem))
      {
        int elemCount = Math.Min(values.Length, arrayElem.GetArrayLength());

        for (int i = 0; i < elemCount; i++)
        {
          JsonElement elemAt = arrayElem.GetElementAt(i, JsonValueKind.Number, name);
          values[i] = elemAt.GetSingle();
        }

        return elemCount;
      }

      return 0;
    }

    /// <summary>
    /// Reads a single double from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>Double value.</returns>
    /// <exception cref="InvalidCastException">Thrown if the element is not of the specified type.</exception>
    /// <exception cref="FormatException">Thrown if the element could not be formatted as the specified type.</exception>
    public double ReadDouble(String name)
    {
      CheckDisposed();

      JsonElement elem = GetAndValidate(name, JsonValueKind.Number);
      return elem.GetDouble();
    }

    /// <summary>
    /// Reads a single nullable double from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>Double value.</returns>
    /// <exception cref="InvalidCastException">Thrown if the element is not of the specified type.</exception>
    /// <exception cref="FormatException">Thrown if the element could not be formatted as the specified type.</exception>
    public double? ReadNullableDouble(String name)
    {
      CheckDisposed();

      if (GetAndValidateOrNull(name, JsonValueKind.Number, out JsonElement elem))
        return elem.GetDouble();

      return null;
    }

    /// <summary>
    /// Reads an array of doubles from the input. If the receiving span is smaller than the number of elements that can be read, it will read
    /// as many as it can and skip the rest. Use <see cref="PeekArrayCount"/> to determine how large the receiving span should be.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Span to hold the array values.</param>
    /// <returns>Number of values read.</returns>
    /// <exception cref="InvalidCastException">Thrown if the array elements are not of the specified type.</exception>
    /// <exception cref="FormatException">Thrown if the array elements could not be formatted as the specified type.</exception>
    public int ReadDoubleArray(String name, Span<double> values)
    {
      CheckDisposed();

      if (GetAndValidateOrNull(name, JsonValueKind.Array, out JsonElement arrayElem))
      {
        int elemCount = Math.Min(values.Length, arrayElem.GetArrayLength());

        for (int i = 0; i < elemCount; i++)
        {
          JsonElement elemAt = arrayElem.GetElementAt(i, JsonValueKind.Number, name);
          values[i] = elemAt.GetDouble();
        }

        return elemCount;
      }

      return 0;
    }

    /// <summary>
    /// Reads a single decimal from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>Decimal value.</returns>
    /// <exception cref="InvalidCastException">Thrown if the element is not of the specified type.</exception>
    /// <exception cref="FormatException">Thrown if the element could not be formatted as the specified type.</exception>
    public decimal ReadDecimal(String name)
    {
      CheckDisposed();

      JsonElement elem = GetAndValidate(name, JsonValueKind.Number);
      return elem.GetDecimal();
    }

    /// <summary>
    /// Reads a single nullable decimal from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>Nullable decimal value.</returns>
    /// <exception cref="InvalidCastException">Thrown if the element is not of the specified type.</exception>
    /// <exception cref="FormatException">Thrown if the element could not be formatted as the specified type.</exception>
    public decimal? ReadNullableDecimal(String name)
    {
      CheckDisposed();

      if (GetAndValidateOrNull(name, JsonValueKind.Number, out JsonElement elem))
        return elem.GetDecimal();

      return null;
    }

    /// <summary>
    /// Reads an array of decimals from the input. If the receiving span is smaller than the number of elements that can be read, it will read
    /// as many as it can and skip the rest. Use <see cref="PeekArrayCount"/> to determine how large the receiving span should be.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Span to hold the array values.</param>
    /// <returns>Number of values read.</returns>
    /// <exception cref="InvalidCastException">Thrown if the array elements are not of the specified type.</exception>
    /// <exception cref="FormatException">Thrown if the array elements could not be formatted as the specified type.</exception>
    public int ReadDecimalArray(String name, Span<decimal> values)
    {
      CheckDisposed();

      if (GetAndValidateOrNull(name, JsonValueKind.Array, out JsonElement arrayElem))
      {
        int elemCount = Math.Min(values.Length, arrayElem.GetArrayLength());

        for (int i = 0; i < elemCount; i++)
        {
          JsonElement elemAt = arrayElem.GetElementAt(i, JsonValueKind.Number, name);
          values[i] = elemAt.GetDecimal();
        }

        return elemCount;
      }

      return 0;
    }

    /// <summary>
    /// Reads a single boolean from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>Boolean value.</returns>
    /// <exception cref="InvalidCastException">Thrown if the element is not of the specified type.</exception>
    /// <exception cref="FormatException">Thrown if the element could not be formatted as the specified type.</exception>
    public bool ReadBoolean(String name)
    {
      CheckDisposed();

      JsonElement elem = GetAndValidateEither(name, JsonValueKind.True, JsonValueKind.False);
      return elem.GetBoolean();
    }

    /// <summary>
    /// Reads a single nullable boolean from the input.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>Nullable boolean value.</returns>
    /// <exception cref="InvalidCastException">Thrown if the element is not of the specified type.</exception>
    /// <exception cref="FormatException">Thrown if the element could not be formatted as the specified type.</exception>
    public bool? ReadNullableBoolean(String name)
    {
      CheckDisposed();

      if (GetAndValidateEitherOrNull(name, JsonValueKind.True, JsonValueKind.False, out JsonElement elem))
        return elem.GetBoolean();

      return null;
    }

    /// <summary>
    /// Reads an array of booleans from the input. If the receiving span is smaller than the number of elements that can be read, it will read
    /// as many as it can and skip the rest. Use <see cref="PeekArrayCount"/> to determine how large the receiving span should be.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Span to hold the array values.</param>
    /// <returns>Number of values read.</returns>
    /// <exception cref="InvalidCastException">Thrown if the array elements are not of the specified type.</exception>
    public int ReadBooleanArray(String name, Span<bool> values)
    {
      CheckDisposed();

      if (GetAndValidateOrNull(name, JsonValueKind.Array, out JsonElement arrayElem))
      {
        int elemCount = Math.Min(values.Length, arrayElem.GetArrayLength());

        for (int i = 0; i < elemCount; i++)
        {
          JsonElement elemAt = arrayElem.GetElementAt(i, JsonValueKind.True, JsonValueKind.False, name);
          values[i] = elemAt.GetBoolean();
        }

        return elemCount;
      }

      return 0;
    }

    /// <summary>
    /// Reads a string from the input.
    /// </summary>
    /// <param name="name">Name of the value</param>
    /// <returns>String value</returns>
    /// <exception cref="InvalidCastException">Thrown if the element is not a string or null value.</exception>
    public String? ReadString(String name)
    {
      CheckDisposed();

      if (GetAndValidateOrNull(name, JsonValueKind.String, out JsonElement elem))
        return elem.GetString();

      return null;
    }

    /// <summary>
    /// Reads an array of strings from the input. If the receiving span is smaller than the number of elements that can be read, it will read
    /// as many as it can and skip the rest. Use <see cref="PeekArrayCount"/> to determine how large the receiving span should be.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Span to hold the array values.</param>
    /// <returns>Number of values read.</returns>
    /// <exception cref="InvalidCastException">Thrown if the array elements are not strings or null values.</exception>
    public int ReadStringArray(String name, Span<String?> values)
    {
      CheckDisposed();

      if (GetAndValidateOrNull(name, JsonValueKind.Array, out JsonElement arrayElem))
      {
        int elemCount = Math.Min(values.Length, arrayElem.GetArrayLength());

        for (int i = 0; i < elemCount; i++)
        {
          JsonElement elemAt = arrayElem.GetElementAt(i, JsonValueKind.String, name, true);
          values[i] = elemAt.GetString();
        }

        return elemCount;
      }

      return 0;
    }

    /// <summary>
    /// Reads a single <see cref="IPrimitiveValue"/> struct from the input.
    /// </summary>
    /// <typeparam name="T">Primitive struct type.</typeparam>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">Read <see cref="IPrimitiveValue"/> struct value.</param>
    /// <exception cref="InvalidCastException">Thrown if the element is not an object.</exception>
    public void Read<T>(String name, out T value) where T : struct, IPrimitiveValue
    {
      CheckDisposed();

      JsonElement elem = PushAndValidate(name, JsonValueKind.Object);

      value = default;
      value.Read(this);

      PopElement();
    }

    /// <summary>
    /// Reads a single nullable <see cref="IPrimitiveValue"/> struct from the output.
    /// </summary>
    /// <typeparam name="T">Primitive struct type.</typeparam>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">Read nullable <see cref="IPrimitiveValue"/> struct value.</param>
    /// <exception cref="InvalidCastException">Thrown if the element is not an object.</exception>
    public void ReadNullable<T>(String name, out T? value) where T : struct, IPrimitiveValue
    {
      CheckDisposed();

      value = null;

      if (PushAndValidateOrNull(name, JsonValueKind.Object, out JsonElement elem))
      {
        T val = default;
        val.Read(this);
        value = val;

        PopElement();
      }
    }

    /// <summary>
    /// Reads an array of <see cref="IPrimitiveValue"/> structs from the input. If the receiving span is smaller than the number of
    /// elements that can be read, it will read as many as it can and skip the rest. Use <see cref="PeekArrayCount"/> to determine how 
    /// large the receiving span should be.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Span to hold the array values.</param>
    /// <returns>Number of values read.</returns>
    /// <exception cref="InvalidCastException">Thrown if the array elements are not objects.</exception>
    public int ReadArray<T>(String name, Span<T> values) where T : struct, IPrimitiveValue
    {
      CheckDisposed();

      if (PushAndValidateOrNull(name, JsonValueKind.Array, out JsonElement arrayElem))
      {
        int elemCount = Math.Min(values.Length, arrayElem.GetArrayLength());

        for (int i = 0; i < elemCount; i++)
        {
          JsonElement elemAt = PushAndValidate(name, JsonValueKind.Object);

          ref T value = ref values[i];
          value.Read(this);

          PopElement();
        }

        PopElement();

        return elemCount;
      }

      return 0;
    }

    /// <summary>
    /// Reads an enum value from the input.
    /// </summary>
    /// <typeparam name="T">Enum type.</typeparam>
    /// <param name="name">Name of the value.</param>
    /// <returns>Enum value.</returns>
    public T ReadEnum<T>(String name) where T : struct, Enum
    {
      CheckDisposed();

      JsonElement elem = GetAndValidateEither(name, JsonValueKind.String, JsonValueKind.Number);
      return ReadEnumValue<T>(elem, name);
    }

    /// <summary>
    /// Reads a nullable enum value from the input.
    /// </summary>
    /// <typeparam name="T">Enum type.</typeparam>
    /// <param name="name">Name of the value.</param>
    /// <returns>Nullable enum value.</returns>
    public T? ReadNullableEnum<T>(String name) where T : struct, Enum
    {
      CheckDisposed();

      if (GetAndValidateEitherOrNull(name, JsonValueKind.String, JsonValueKind.Number, out JsonElement elem))
        return ReadEnumValue<T>(elem, name);

      return null;
    }

    private T ReadEnumValue<T>(JsonElement elem, string name) where T : struct, Enum
    {
      if (elem.ValueKind == JsonValueKind.String)
      {
        if (Enum.TryParse<T>(elem.GetString(), true, out T value))
          return value;

        // If we know it's a string and can't read it, throw
        throw new InvalidCastException(StringLocalizer.Instance.GetLocalizedString("TokenNotAnEnum", name, typeof(T).FullName));
      }

      TypeCode typeCode = Type.GetTypeCode(typeof(T));

      switch (typeCode)
      {
        case TypeCode.Byte:
          return BufferHelper.CastToEnum<byte, T>(elem.GetByte());
        case TypeCode.SByte:
          return BufferHelper.CastToEnum<sbyte, T>(elem.GetSByte());
        case TypeCode.Int16:
          return BufferHelper.CastToEnum<short, T>(elem.GetInt16());
        case TypeCode.UInt16:
          return BufferHelper.CastToEnum<ushort, T>(elem.GetUInt16());
        case TypeCode.Int32:
          return BufferHelper.CastToEnum<int, T>(elem.GetInt32());
        case TypeCode.UInt32:
          return BufferHelper.CastToEnum<uint, T>(elem.GetUInt32());
        case TypeCode.Int64:
          return BufferHelper.CastToEnum<long, T>(elem.GetInt64());
        case TypeCode.UInt64:
          return BufferHelper.CastToEnum<ulong, T>(elem.GetUInt64());
        default:
          throw new InvalidCastException(StringLocalizer.Instance.GetLocalizedString("TokenNotAnEnum", name, typeof(T).FullName));
      }
    }

    /// <summary>
    /// Peeks ahead and returns the number of elements if the value is an array or group. Zero is returned if the
    /// value is null or not an array/group. This does not advance the reader.
    /// </summary>
    /// <param name="name">Name of the value</param>
    /// <returns>Non-zero if the value is an array/grouping with elements, zero if it is null.</returns>
    public int PeekArrayCount(String name)
    {
      if (!TryPeekNext(name, out JsonElement nextElem) || nextElem.ValueKind != JsonValueKind.Array)
        return 0;

      return nextElem.GetArrayLength();
    }

    /// <summary>
    /// Peeks ahead and returns the blob header if the value is a blob. Null is returned if the value is null or not a blob.
    /// This does not advance the reader.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>Valid header if the blob is not null, invalid header if the blob is null.</returns>
    public PrimitiveBlobHeader? PeekBlob(String name)
    {
      CheckDisposed();

      if (TryPeekNext(name, out JsonElement headerElem))
        return ReadPrimitiveBlobHeader(headerElem, out _, out _);

      return null;
    }

    /// <summary>
    /// Reads the blob into the receiving span of the specified type. The type does not have to match the stored type metadata
    /// (e.g. can be raw bytes or reintrepret the data). If the receiving span's size is less than the blob size in bytes, 
    /// it will read as many bytes as it can and skip the rest. Use <see cref="PeekBlob"/> to determine how large the receiving span should be.
    /// </summary>
    /// <typeparam name="T">Blittable element type.</typeparam>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Span to hold the blob values.</param>
    /// <returns>Number of values read, as defined by the receiving span's type size in bytes.</returns>
    public int ReadBlob<T>(String name, Span<T> values) where T : unmanaged
    {
      CheckDisposed();

      if (!TryGetNext(name, out JsonElement headerElem))
        return 0;

      PrimitiveBlobHeader? blobHeader = ReadPrimitiveBlobHeader(headerElem, out bool isCompressed, out JsonElement blobElem);
      if (!blobHeader.HasValue)
        return 0;

      // Get the blob data
      if (!blobElem.TryGetBytesFromBase64(out byte[]? base64Encoding))
        return 0;

      // Decompress the blob if necessary
      using DataBufferStream? decompressedBase64 = (isCompressed) ? DecompressBlob(base64Encoding) : null;

      // Copy blob data
      Span<byte> dstBytes = values.AsBytes();
      ReadOnlySpan<byte> srcBytes = (decompressedBase64 is null) ? new ReadOnlySpan<byte>(base64Encoding) : decompressedBase64.AsReadOnlySpan();

      int bytesToRead = Math.Min(dstBytes.Length, srcBytes.Length);
      srcBytes.Slice(0, bytesToRead).CopyTo(dstBytes.Slice(0, bytesToRead));

      return BufferHelper.ComputeElementCount<T>(bytesToRead);
    }

    private DataBufferStream DecompressBlob(byte[] base64Encoding)
    {
      DataBufferStream decompressed = DataBufferStream.FromPool(base64Encoding.Length);
      using (BrotliStream decompresser = new BrotliStream(new MemoryStream(base64Encoding), CompressionMode.Decompress))
        decompresser.CopyTo(decompressed);

      decompressed.Position = 0;
      return decompressed;
    }

    /// <summary>
    /// Reads the group header and returns the number of elements, if the object is a group. After reading each element, be sure to call <see cref="EndReadGroup"/>. Every begin must be paired with an end.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    /// <returns>Non-zero if the object is an array with elements, zero if empty array or just a grouping.</returns>
    public int BeginReadGroup(String name)
    {
      JsonElement elem = PushAndValidateEither(name, JsonValueKind.Array, JsonValueKind.Object);

      return (elem.ValueKind == JsonValueKind.Array) ? elem.GetArrayLength() : 0;
    }

    /// <summary>
    /// Finishes reading the group. Every begin must be paired with an end.
    /// </summary>
    public void EndReadGroup()
    {
      PopElement();
    }

    /// <summary>
    /// Skips the value, if it is present.
    /// </summary>
    /// <param name="name">Name of the value.</param>
    public void Skip(String name) 
    {
      // If in an array, increment to the next array index or pop it off the depth stack
      if (IsInArray)
      {
        if (m_current.ArrayIndex < m_current.ArrayLength)
          m_current.ArrayIndex++;
        else
          PopElement();
      }
    }

    /// <summary>
    /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
    /// </summary>
    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    /// <summary>
    /// Releases unmanaged and - optionally - managed resources.
    /// </summary>
    /// <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
    protected virtual void Dispose(bool disposing)
    {
      if (!m_isDisposed)
      {
        if (disposing)
        {
          m_current = default;
          m_depthStack.Clear();
          m_document.Dispose();

          // If saved the input stream, means we don't want to leave it open
          if (m_input is not null)
            m_input.Close();
        }

        m_isDisposed = true;
      }
    }

    /// <summary>
    /// Used by subclassed writers to mark us disposed, if overriding the dispose method.
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected void MarkIsDisposed()
    {
      m_isDisposed = true;
    }

    /// <summary>
    /// Throws an exception if the current writer has been disposed.
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected void CheckDisposed()
    {
      if (m_isDisposed)
        throw new ObjectDisposedException(GetType().Name);
    }

    /// <summary>
    /// Pops the current element off, replacing it with the previous element that was on the depth stack.
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected void PopElement()
    {
      if (m_depthStack.Count == 0)
        return;

      m_depthStack.TryPop(out m_current);
    }

    /// <summary>
    /// Makes the specified element the current element and pushes the previous current onto the depth stack.
    /// </summary>
    /// <param name="elem"></param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected void PushElement(JsonElement elem)
    {
      m_depthStack.Push(m_current);
      m_current = new JsonElementEntry(elem);
    }

    /// <summary>
    /// Tries to get the named property from the current element, if it exists and if the current element is an object.
    /// This does not push to the depth stack.
    /// </summary>
    /// <param name="name">Name of the property.</param>
    /// <param name="nextElem">Property json element.</param>
    /// <returns>True if the element is valid, false if otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected bool TryGetProperty(string name, out JsonElement nextElem)
    {
      if (IsInObject && m_current.Element.TryGetProperty(name, out nextElem))
        return true;

      nextElem = default;
      return false;
    }

    /// <summary>
    /// Tries to get the next array item from the current element, if it exists and if the current element is an array. This increments
    /// the current array index. This does not push to the depth stack.
    /// </summary>
    /// <param name="nextElem">Next json element.</param>
    /// <returns>True if the element is valid, false if otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected bool TryGetNextArrayElement(out JsonElement nextElem)
    {
      if (IsInArray && m_current.ArrayIndex < m_current.ArrayLength)
      {
        nextElem = m_current.Element[m_current.ArrayIndex];
        m_current.ArrayIndex++;

        return true;
      }

      nextElem = default;
      return false;
    }

    /// <summary>
    /// Tries to get the next array item from the current element, if it exists and if the current element is an array. This does not increment
    /// the current array index. This does not push to the depth stack.
    /// </summary>
    /// <param name="nextElem">Next json element.</param>
    /// <returns>True if the element is valid, false if otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected bool TryPeekNextArrayElement(out JsonElement nextElem)
    {
      if (IsInArray && m_current.ArrayIndex < m_current.ArrayLength)
      {
        nextElem = m_current.Element[m_current.ArrayIndex];
        return true;
      }

      nextElem = default;
      return false;
    }

    /// <summary>
    /// Tries to get the property element -or- next array element (if it exists, the current array index is incremented). This does not push to the depth stack.
    /// </summary>
    /// <param name="name">Name of the property.</param>
    /// <param name="nextElem">Property json element.</param>
    /// <returns>True if the json element is valid, false if otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected bool TryGetNext(string name, out JsonElement nextElem)
    {
      return IsInObject ? TryGetProperty(name, out nextElem) : TryGetNextArrayElement(out nextElem);
    }

    /// <summary>
    /// Tries to [eek at the property element -or- next array element. This does not push to the depth stack.
    /// </summary>
    /// <param name="name">Name of the property.</param>
    /// <param name="nextElem">Property json element.</param>
    /// <returns>True if the json element is valid, false if otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected bool TryPeekNext(string name, out JsonElement nextElem)
    {
      return IsInObject ? TryGetProperty(name, out nextElem) : TryPeekNextArrayElement(out nextElem);
    }

    /// <summary>
    /// Gets the json element of the specified property -or- the next array element. This checks for the expected kind and throws for any other.
    /// This does not push the element to the depth stack.
    /// </summary>
    /// <param name="name">Property name, only for if the current element is an object.</param>
    /// <param name="expected">Expected element kind.</param>
    /// <returns>Json element.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected JsonElement GetAndValidate(string name, JsonValueKind expected)
    {
      if (!TryGetNext(name, out JsonElement element))
        throw new InvalidCastException(StringLocalizer.Instance.GetLocalizedString("KeyNotFound", name));

      element.ThrowIfInvalidKind(expected, name);
      return element;
    }

    /// <summary>
    /// Gets the json element of the specified property -or- the next array element. This checks for either expected kind and throws for any other.
    /// This does not push the element to the depth stack.
    /// </summary>
    /// <param name="name">Property name, only for if the current element is an object.</param>
    /// <param name="expected1">First expected kind.</param>
    /// <param name="expected2">Second expected kind.</param>
    /// <returns>Json element.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected JsonElement GetAndValidateEither(string name, JsonValueKind expected1, JsonValueKind expected2)
    {
      if (!TryGetNext(name, out JsonElement element))
        throw new InvalidCastException(StringLocalizer.Instance.GetLocalizedString("KeyNotFound", name));

      element.ThrowIfNeitherKinds(expected1, expected2, name);
      return element;
    }

    /// <summary>
    /// Gets the json element of the specified property -or- the next array element. This checks for either expected kind -or- null.
    /// This does not push the element to the depth stack.
    /// </summary>
    /// <param name="name">Property name, only for if the current element is an object.</param>
    /// <param name="expected1">First expected kind.</param>
    /// <param name="expected2">Second expected kind.</param>
    /// <param name="element">Json element.</param>
    /// <returns>True if the element is not null and of the specified kind.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected bool GetAndValidateEitherOrNull(string name, JsonValueKind expected1, JsonValueKind expected2, out JsonElement element)
    {
      if (!TryGetNext(name, out element) || element.ValueKind == JsonValueKind.Null)
        return false;

      element.ThrowIfNeitherKinds(expected1, expected2, name);
      return true;
    }

    /// <summary>
    /// Gets the json element of the specified property -or- the next array element. This checks for the expected kind -or- null. This
    /// does not push the element to the depth stack.
    /// </summary>
    /// <param name="name">Property name, only for if the current element is an object.</param>
    /// <param name="expected">Expected element kind.</param>
    /// <param name="element">Json element.</param>
    /// <returns>True if the element is not null and of the specified kind.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected bool GetAndValidateOrNull(string name, JsonValueKind expected, out JsonElement element)
    {
      if (!TryGetNext(name, out element) || element.ValueKind == JsonValueKind.Null)
        return false;

      element.ThrowIfInvalidKind(expected, name);
      return true;
    }


    /// <summary>
    /// Pushes the next json element of the specified property -or- the next array element onto the depth stack. 
    /// This checks for the expected kind and throws for any other.
    /// </summary>
    /// <param name="name">Property name, only for if the current element is an object.</param>
    /// <param name="expected">Expected element kind.</param>
    /// <returns>Json element.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected JsonElement PushAndValidate(string name, JsonValueKind expected)
    {
      if (!TryGetNext(name, out JsonElement element))
        throw new InvalidCastException(StringLocalizer.Instance.GetLocalizedString("KeyNotFound", name));

      element.ThrowIfInvalidKind(expected, name);
      PushElement(element);

      return element;
    }

    /// <summary>
    /// Pushes the next json element of the specified property -or- the next array element onto the depth stack. 
    /// This checks for either expected kind and throws for any other.
    /// </summary>
    /// <param name="name">Property name, only for if the current element is an object.</param>
    /// <param name="expected1">First expected kind.</param>
    /// <param name="expected2">Second expected kind.</param>
    /// <returns>Json element.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected JsonElement PushAndValidateEither(string name, JsonValueKind expected1, JsonValueKind expected2)
    {
      if (!TryGetNext(name, out JsonElement element))
        throw new InvalidCastException(StringLocalizer.Instance.GetLocalizedString("KeyNotFound", name));

      element.ThrowIfNeitherKinds(expected1, expected2);
      PushElement(element);

      return element;
    }

    /// <summary>
    /// Pushes the next json element of the specified property -or- the next array element onto the depth stack, provided it is not null.
    /// This checks for either expected kind -or- null.
    /// </summary>
    /// <param name="name">Property name, only for if the current element is an object.</param>
    /// <param name="expected1">First expected kind.</param>
    /// <param name="expected2">Second expected kind.</param>
    /// <param name="element">Json element.</param>
    /// <returns>True if the element is not null and of the specified kind.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected bool PushAndValidateEitherOrNull(string name, JsonValueKind expected1, JsonValueKind expected2, out JsonElement element)
    {
      if (!TryGetNext(name, out element) || element.ValueKind == JsonValueKind.Null)
        return false;

      element.ThrowIfNeitherKinds(expected1, expected2, name);
      PushElement(element);

      return true;
    }

    /// <summary>
    /// Pushes the next json element of the specified property -or- the next array element onto the depth stack, provided it is not null.
    /// This checks for expected kind -or- null.
    /// </summary>
    /// <param name="name">Property name, only for if the current element is an object.</param>
    /// <param name="expected">Expected element kind.</param>
    /// <param name="element">Json element.</param>
    /// <returns>True if the element is not null and of the specified kind.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected bool PushAndValidateOrNull(string name, JsonValueKind expected, out JsonElement element)
    {
      if (!TryGetNext(name, out element) || element.ValueKind == JsonValueKind.Null)
        return false;

      element.ThrowIfInvalidKind(expected, name);
      PushElement(element);

      return true;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected PrimitiveBlobHeader? ReadPrimitiveBlobHeader(JsonElement headerElem, out bool isCompressed, out JsonElement blobElem)
    {
      isCompressed = false;
      blobElem = default;

      if (headerElem.ValueKind != JsonValueKind.Object)
        return null;

      if (!headerElem.TryGetProperty(JsonPropertyNames.ElementCount.ToString(), out JsonElement elemCount) ||
          !headerElem.TryGetProperty(JsonPropertyNames.ElementSizeInBytes.ToString(), out JsonElement sizeInBytes) ||
          !headerElem.TryGetProperty(JsonPropertyNames.ElementType.ToString(), out JsonElement elemType) ||
          !headerElem.TryGetProperty(JsonPropertyNames.Blob.ToString(), out blobElem))
        return null;

      Type? elType = ParseTypeInfo(elemType);
      if (elType is null)
        return null;

      if (elemCount.ValueKind != JsonValueKind.Number || sizeInBytes.ValueKind != JsonValueKind.Number || blobElem.ValueKind != JsonValueKind.String)
        return null;

      // Might be compressed
      if (headerElem.TryGetProperty(JsonPropertyNames.IsCompressed.ToString(), out JsonElement isCompressedElem) && isCompressedElem.IsBoolean())
        isCompressed = isCompressedElem.GetBoolean();

      return new PrimitiveBlobHeader(elemCount.GetInt32(), sizeInBytes.GetInt32(), elType);
    }

    /// <summary>
    /// Reads type information from a json element.
    /// </summary>
    /// <param name="typeElem">Json element</param>
    /// <returns>Type or null.</returns>
    protected virtual Type? ParseTypeInfo(JsonElement typeElem)
    {
      if (typeElem.ValueKind != JsonValueKind.String)
        return null;

      string? typeString = typeElem.GetString();
      return (typeString is null) ? null : SmartActivator.GetType(typeString);
    }

    private struct JsonElementEntry
    {
      public readonly JsonElement Element;
      public readonly int ArrayLength;
      public readonly JsonValueKind ValueKind;
      public int ArrayIndex;

      public JsonElementEntry(JsonElement elem)
      {
        Element = elem;
        ValueKind = elem.ValueKind;
        ArrayIndex = 0;
        ArrayLength = (ValueKind == JsonValueKind.Array) ? elem.GetArrayLength() : 0;
      }
    }
  }
}
