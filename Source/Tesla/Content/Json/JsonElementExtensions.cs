﻿/*
* Copyright (c) 2010-2024 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

#nullable enable

namespace Tesla.Content
{
  /// <summary>
  /// Extension methods for <see cref="JsonElement"/>.
  /// </summary>
  public static class JsonElementExtensions
  {
    /// <summary>
    /// Gets if the json element is a boolean.
    /// </summary>
    /// <param name="elem">Json element.</param>
    /// <returns>True if boolean (true/false), false if otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool IsBoolean(in this JsonElement elem)
    {
      JsonValueKind kind = elem.ValueKind;
      return kind == JsonValueKind.True || kind == JsonValueKind.False;
    }

    /// <summary>
    /// Gets the value of the element as a char. Functionally, a string
    /// element where the value has exactly one character.
    /// </summary>
    /// <param name="elem">Json element.</param>
    /// <returns>Char value.</returns>
    /// <exception cref="InvalidCastException">Thrown if the string is not exactly one character or null.</exception>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static char GetChar(in this JsonElement elem)
    {
      string? strValue = elem.GetString();
      if (strValue is not null && strValue.Length == 1)
        return strValue[0];

      throw new InvalidCastException();
    }

    /// <summary>
    /// Throw if the specified element is not the expected kind.
    /// </summary>
    /// <param name="elem">Json element.</param>
    /// <param name="expected">Expected kind.</param>
    /// <param name="name">Property name.</param>
    /// <exception cref="InvalidCastException">Thrown if the actual kind does not match the expected kinds.</exception>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void ThrowIfInvalidKind(in this JsonElement elem, JsonValueKind expected, string? name)
    {
      JsonValueKind actual = elem.ValueKind;
      if (expected == actual)
        return;

      string msg = (name is not null) ?
        StringLocalizer.Instance.GetLocalizedString("TokenMismatch_Named", name, actual.ToString(), expected.ToString())
        : StringLocalizer.Instance.GetLocalizedString("TokenMismatch", actual.ToString(), expected.ToString());

      new InvalidCastException(msg);
    }


    /// <summary>
    /// Throw if the specified element is neither expected kinds.
    /// </summary>
    /// <param name="elem">Json element.</param>
    /// <param name="expected1">First expected kind.</param>
    /// <param name="expected2">Second expected kind.</param>
    /// <param name="name">Property name.</param>
    /// <exception cref="InvalidCastException">Thrown if the actual kind does not match the expected kinds.</exception>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void ThrowIfNeitherKinds(in this JsonElement elem, JsonValueKind expected1, JsonValueKind expected2, string? name = null)
    {
      JsonValueKind actual = elem.ValueKind;
      if (actual == expected1 || actual == expected2)
        return;

      string expected = $"{expected1.ToString()} or {expected2.ToString()}";

      string msg = (name is not null) ?
        StringLocalizer.Instance.GetLocalizedString("TokenMismatch_Named", name, actual.ToString(), expected)
        : StringLocalizer.Instance.GetLocalizedString("TokenMismatch", actual.ToString(), expected);

      throw new InvalidCastException(msg);
    }

    /// <summary>
    /// Gets and validates the kind of array element, but no other checking like if the index is within bounds.
    /// </summary>
    /// <param name="array">Array JSON element.</param>
    /// <param name="index">Zero-based index of the element.</param>
    /// <param name="expected">Expected element kind.</param>
    /// <param name="name">Name of the array property.</param>
    /// <param name="allowNull">True of null array elements are allowed, false if not.</param>
    /// <returns>Array element.</returns>
    /// <exception cref="InvalidCastException">Thrown if the actual kind does not match the expected kinds.</exception>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static JsonElement GetElementAt(in this JsonElement array, int index, JsonValueKind expected, string? name = null, bool allowNull = false)
    {
      JsonElement elemAt = array[index];
      if (allowNull && elemAt.ValueKind == JsonValueKind.Null)
        return elemAt;
      
      elemAt.ThrowIfInvalidKind(expected, name);

      return elemAt;
    }

    /// <summary>
    /// Gets and validates the kind of array element as either kinds, but no other checking like if the index is within bounds.
    /// </summary>
    /// <param name="array">Array JSON element.</param>
    /// <param name="index">Zero-based index of the element.</param>
    /// <param name="expected1">First expected kind.</param>
    /// <param name="expected2">Second expected kind.</param>
    /// <param name="name">Property name.</param>
    /// <param name="allowNull">True of null array elements are allowed, false if not.</param>
    /// <returns>Array element.</returns>
    /// <exception cref="InvalidCastException">Thrown if the actual kind does not match the expected kinds.</exception>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static JsonElement GetElementAt(in this JsonElement array, int index, JsonValueKind expected1, JsonValueKind expected2, string? name = null, bool allowNull = false)
    {
      JsonElement elemAt = array[index];
      if (allowNull && elemAt.ValueKind == JsonValueKind.Null)
        return elemAt;

      elemAt.ThrowIfNeitherKinds(expected1, expected2, name);

      return elemAt;
    }
  }
}
