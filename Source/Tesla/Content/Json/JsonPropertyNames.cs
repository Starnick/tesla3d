﻿/*
* Copyright (c) 2010-2024 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System.Collections.Concurrent;
using System.Text.Json;

#nullable enable

namespace Tesla.Content
{
  /// <summary>
  /// Contains common well-known JSON property names and a means to cache JSON strings.
  /// </summary>
  public static class JsonPropertyNames
  {
    private static ConcurrentDictionary<string, JsonEncodedText> s_cachedEncodedStrings = new ConcurrentDictionary<string, JsonEncodedText>();

    /// <summary>
    /// "elementCount" for PrimitiveBlobHeader.
    /// </summary>
    public static readonly JsonEncodedText ElementCount = GetEncodedString("elementCount");

    /// <summary>
    /// "elementSizeInBytes" for PrimitiveBlobHeader.
    /// </summary>
    public static readonly JsonEncodedText ElementSizeInBytes = GetEncodedString("elementSizeInBytes");

    /// <summary>
    /// "elementType" for PrimitiveBlobHeader.
    /// </summary>
    public static readonly JsonEncodedText ElementType = GetEncodedString("elementType");

    /// <summary>
    /// "isCompressed" for PrimitiveBlobHeader. Unique to JSON, blob data can be compressed before encoded to Base64 strings.
    /// </summary>
    public static readonly JsonEncodedText IsCompressed = GetEncodedString("isCompressed");

    /// <summary>
    /// "blob" for PrimitiveBlobHeader. Represents th
    /// </summary>
    public static readonly JsonEncodedText Blob = GetEncodedString("blob");

    /// <summary>
    /// "$type" information for an <see cref="ISavable"/> object. Can be in the following forms:
    /// <list type="number">
    /// <item><term>"Tesla.Content.ISavable, Tesla"</term><description>Assembly qualified type string, may be any object and often is used for non-savables as no version data is specified.</description></item>
    /// <item><term>["Tesla.Content.ISavable, Tesla", 1]</term><description>Single type string with the associated version number.</description></item>
    /// <item><term>["Tesla.Content.ISavable, Tesla", 1, 2, 3]</term><description>Shorthand version hierarchy, a single type string with the savable's version hierarchy The first number is the type's version, the second is the base, the third the base of that, etc.</description></item>
    /// <item><term>[["Tesla.Content.ISavable, Tesla", 1], ["Tesla.Content.Base, Tesla", 2]]</term><description>Verbose version hierarchy, multiple type strings each with a version number in an array of arrays. Each sub-array can only specify the single type. Order is in the type's inheritance order.</description></item>
    /// </list>
    /// </summary>
    public static readonly JsonEncodedText ObjectType = GetEncodedString("$type");

    /// <summary>
    /// "$shared" for an array of shared <see cref="ISavable"/> objects referenced by other properties in the JSON.
    /// </summary>
    public static readonly JsonEncodedText SharedObjects = GetEncodedString("$shared");

    /// <summary>
    /// "$schema" for a property pointing to the schema describing the JSON structure.
    /// </summary>
    public static readonly JsonEncodedText Schema = GetEncodedString("$schema");

    /// <summary>
    /// Encodes the string value to a JSON string, using the default encoder. The value is cached.
    /// </summary>
    /// <param name="value">String value</param>
    /// <returns>Encoded string value.</returns>
    public static JsonEncodedText GetEncodedString(string value)
    {
      if (s_cachedEncodedStrings.TryGetValue(value, out JsonEncodedText encodedValue))
        return encodedValue;

      encodedValue = JsonEncodedText.Encode(value);
      s_cachedEncodedStrings.TryAdd(value, encodedValue);

      return encodedValue;
    }
  }
}
