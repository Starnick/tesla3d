﻿/*
* Copyright (c) 2010-2024 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Tesla.Utilities;

#nullable enable

namespace Tesla.Content
{
  /// <summary>
  /// Writes <see cref="ISavable"/> objects as formatted JSON to a stream.
  /// </summary>
  public sealed class JsonSavableWriter : JsonPrimitiveWriter, ISavableWriter
  {
    private IExternalReferenceHandler? m_externalHandler;
    private CyclicReferenceDetector m_cyclicDetector;
    private SavableWriteFlags m_writeFlags;
    private ExternalSharedMode m_externalSharedMode;
    private bool m_writeVerboseTypes;

    private Queue<ISavable> m_sharedQueue;
    private Dictionary<ISavable, int> m_sharedIndices;
    private int m_sharedObjectCount;

    private TypeVersionTable m_versionTable;
    private SingleLineTypeWriter m_typeWriter;

    /// <inheritdoc />
    public IExternalReferenceHandler? ExternalHandler { get { return m_externalHandler; } }

    /// <inheritdoc />
    public SavableWriteFlags WriteFlags { get { return m_writeFlags; } }

    /// <summary>
    /// Constructs a new instance of the <see cref="JsonSavableWriter"/> class. All external references are automatically treated as 
    /// shared and written to the output stream.
    /// </summary>
    /// <param name="output">Output stream to write to.</param>
    /// <param name="writeFlags">Write flags that specify certain behaviors.</param>
    /// <param name="leaveOpen">True if the output stream should NOT be disposed/closed when the writer is disposed/closed, false otherwise.</param>
    /// <exception cref="ArgumentNullException">Thrown if the output stream is null or cannot be written to.</exception>
    public JsonSavableWriter(Stream output, SavableWriteFlags writeFlags = SavableWriteFlags.None, bool leaveOpen = false) : this(output, writeFlags, null, leaveOpen) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="JsonSavableWriter"/> class.
    /// </summary>
    /// <param name="output">Output stream to write to.</param>
    /// <param name="writeFlags">Write flags that specify certain behaviors.</param>
    /// <param name="externalHandler">External reference handler, if null then all external references are treated as shared and are written to the output stream.</param>
    /// <param name="leaveOpen">True if the output stream should NOT be disposed/closed when the writer is disposed/closed, false otherwise.</param>
    /// <exception cref="ArgumentNullException">Thrown if the output stream is null or cannot be written to.</exception>
    public JsonSavableWriter(Stream output, SavableWriteFlags writeFlags, IExternalReferenceHandler? externalHandler, bool leaveOpen = false)
        : base(output, leaveOpen, true, true)
    {
      m_writeFlags = writeFlags;
      m_externalHandler = externalHandler;
      m_sharedQueue = new Queue<ISavable>();
      m_sharedIndices = new Dictionary<ISavable, int>(new ReferenceEqualityComparer<ISavable>());
      m_sharedObjectCount = 0;
      m_cyclicDetector = new CyclicReferenceDetector();
      m_versionTable = new TypeVersionTable();
      m_typeWriter = new SingleLineTypeWriter();
      m_writeVerboseTypes = writeFlags.HasFlag(SavableWriteFlags.VerboseTypes) ? true : false;

      CompressBlobs = writeFlags.HasFlag(SavableWriteFlags.UseCompression) ? true : false;

      // Determine external/shared write mode
      if (externalHandler is null)
        m_externalSharedMode = ExternalSharedMode.AllShared;
      else if (writeFlags.HasFlag(SavableWriteFlags.SharedAsExternal))
        m_externalSharedMode = ExternalSharedMode.AllExternal;
      else
        m_externalSharedMode = ExternalSharedMode.Default;

      // Write the schema for ISavable
      UnderlyingJsonWriter.WriteString(JsonPropertyNames.Schema, JsonPropertyNames.GetEncodedString("https://bitbucket.org/Starnick/tesla3d/downloads/SavableSchema.json"));
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public override void Flush()
    {
      CheckDisposed();

      // Only process pending data if we're still in the root object...
      if (IsInRootObject)
        ProcessPendingData();

      base.Flush();
    }

    /// <summary>
    /// Writes the "root" savable object to the input, this will be the root element of the JSON document.
    /// </summary>
    /// <typeparam name="T">Type of object to write.</typeparam>
    /// <param name="value">Savable object.</param>
    /// <param name="flushImmediately">If true, flushes the writer and closes the root object, allowing no more values to be written to the root object. Default is true.</param>
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void WriteRootSavable<T>(T value, bool flushImmediately = true) where T : class?, ISavable?
    {
      CheckDisposed();

      if (value is not null)
      {
        m_cyclicDetector.AddReference(value);

        try
        {
          WriteObjectType(value.GetType());
          value.Write(this);
        }
        finally
        {
          m_cyclicDetector.RemoveReference(value);
        }
      }

      if (flushImmediately)
        Flush();
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void WriteSavable<T>(string name, T value) where T : class?, ISavable?
    {
      CheckDisposed();

      if (WritePropertyNameIfWanted(name, value))
        WriteSavableValue<T>(value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void WriteSavable<T>(string name, ReadOnlySpan<T> values) where T : class?, ISavable?
    {
      CheckDisposed();

      if (WritePropertyNameIfWanted(name, values.IsEmpty))
      {
        if (WriteNullValueIfEmpty(values.IsEmpty))
        {
          WriteStartArray();

          for (int i = 0; i < values.Length; i++)
            WriteSavableValue(values[i]);

          WriteEndArray();
        }
      }
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void WriteSharedSavable<T>(string name, T value) where T : class?, ISavable?
    {
      CheckDisposed();

      if (m_externalSharedMode == ExternalSharedMode.AllExternal)
        WriteExternalSavableInternal<T>(name, value);
      else
        WriteSharedSavableInternal<T>(name, value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void WriteSharedSavable<T>(string name, ReadOnlySpan<T> values) where T : class?, ISavable?
    {
      CheckDisposed();

      if (m_externalSharedMode == ExternalSharedMode.AllExternal)
        WriteExternalSavableInternal<T>(name, values);
      else
        WriteSharedSavableInternal<T>(name, values);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void WriteExternalSavable<T>(string name, T value) where T : class?, ISavable?
    {
      CheckDisposed();

      if (m_externalSharedMode == ExternalSharedMode.AllShared)
        WriteSharedSavableInternal<T>(name, value);
      else
        WriteExternalSavableInternal<T>(name, value);
    }

    /// <inheritdoc />
    /// <exception cref="ObjectDisposedException">Thrown if the writer has previously been disposed.</exception>
    public void WriteExternalSavable<T>(string name, ReadOnlySpan<T> values) where T : class?, ISavable?
    {
      CheckDisposed();

      if (m_externalSharedMode == ExternalSharedMode.AllShared)
        WriteSharedSavableInternal<T>(name, values);
      else
        WriteExternalSavableInternal<T>(name, values);
    }

    /// <inheritdoc />
    protected override void Dispose(bool disposing)
    {
      base.Dispose(disposing);

      if (!IsDisposed && disposing)
        m_typeWriter.Dispose();
    }

    /// <inheritdoc />
    protected override void WriteBlobType(Type type)
    {
      m_versionTable.AddOrGetFromRuntime(type, out TypeVersionTable.Entry vEntry);
      UnderlyingJsonWriter.WriteString(JsonPropertyNames.ElementType, JsonPropertyNames.GetEncodedString(vEntry.PersistedTypeName));
    }

    private void WriteObjectType(Type type)
    {
      m_versionTable.AddOrGetFromRuntime(type, out TypeVersionTable.Entry vEntry);
      m_typeWriter.WriteType(this, vEntry, m_writeVerboseTypes);
    }

    private void WriteSavableValue<T>(T value) where T : class?, ISavable?
    {
      if (WriteNullValueIfNull(value))
      {
        WriteStartObject();

        m_cyclicDetector.AddReference(value);

        try
        {
          WriteObjectType(value.GetType());
          value.Write(this);
        }
        finally
        {
          m_cyclicDetector.RemoveReference(value);
        }

        WriteEndObject();
      }
    }

    private void WriteSharedSavableValue<T>(T value) where T : class?, ISavable?
    {
      if (WriteNullValueIfNull(value))
      {
        if (!m_sharedIndices.TryGetValue(value, out int index))
        {
          index = m_sharedObjectCount;
          m_sharedQueue.Enqueue(value);
          m_sharedIndices.Add(value, index);

          m_sharedObjectCount++;
        }

        UnderlyingJsonWriter.WriteStringValue($"/{JsonPropertyNames.SharedObjects}/{index}");
      }
    }

    private void WriteExternalSavableValue<T>(T value) where T : class?, ISavable?
    {
      ExternalReference? externalRef = (value is null) ? null : m_externalHandler?.ProcessSavable<T>(value);
      if (externalRef == ExternalReference.NullReference)
        externalRef = null;

      if (WriteNullValueIfNull(externalRef))
      {
        Debug.Assert(!String.IsNullOrEmpty(externalRef.ResourcePath));

        UnderlyingJsonWriter.WriteStringValue(externalRef.ResourcePath);
      }
    }

    private void WriteExternalSavableInternal<T>(string name, T value) where T : class?, ISavable?
    {
      if (WritePropertyNameIfWanted(name, value))
        WriteExternalSavableValue<T>(value);
    }

    private void WriteSharedSavableInternal<T>(string name, T value) where T : class?, ISavable?
    {
      if (WritePropertyNameIfWanted(name, value))
        WriteSharedSavableValue<T>(value);
    }

    private void WriteSharedSavableInternal<T>(string name, ReadOnlySpan<T> values) where T : class?, ISavable?
    {
      if (WritePropertyNameIfWanted(name, values.IsEmpty))
      {
        if (WriteNullValueIfEmpty(values.IsEmpty))
        {
          WriteStartArray();

          for (int i = 0; i < values.Length; i++)
            WriteSharedSavableValue<T>(values[i]);

          WriteEndArray();
        }
      }
    }

    private void WriteExternalSavableInternal<T>(string name, ReadOnlySpan<T> values) where T : class?, ISavable?
    {
      if (WritePropertyNameIfWanted(name, values.IsEmpty))
      {
        if (WriteNullValueIfEmpty(values.IsEmpty))
        {
          WriteStartArray();

          for (int i = 0; i < values.Length; i++)
            WriteExternalSavableValue<T>(values[i]);

          WriteEndArray();
        }
      }
    }

    private void ProcessPendingData()
    {
      ProcessSharedData();

      // Flush externals last, so any external data gets written out
      if (m_externalHandler is not null)
        m_externalHandler.Flush();
    }

    private void ProcessSharedData()
    {
      if (m_sharedQueue.Count == 0)
        return;

      WriteStartArray(JsonPropertyNames.SharedObjects);

      while (m_sharedQueue.Count > 0)
      {
        ISavable savable = m_sharedQueue.Dequeue();

        WriteSavableValue<ISavable>(savable);
      }

      WriteEndArray();
    }

    private sealed class SingleLineTypeWriter : JsonPrimitiveWriter
    {
      public SingleLineTypeWriter() : base(DataBufferStream.FromPool(), false, false, false) { }

      public void WriteType(JsonSavableWriter writer, in TypeVersionTable.Entry vEntry, bool writeVerboseTypes)
      {
        DataBufferStream typeStream = (OutStream as DataBufferStream)!;
        typeStream.SetLength(0);
        UnderlyingJsonWriter.Reset();

        bool needsFlushing = false;

        // Several supported type writing:
        // 1. Shorthand single type, the assembly qualified string: "Namespace.Type, Assembly". Version 1 is implicit. Non-Savables always written like this.
        //
        // 2. Verbose single type, an array with string/number: ["Namespace.Type, Assembly", 2]. The number is the version number for the type.
        //
        // 3. Shorthand version hierarchy, array with string/multiple numbers: ["Namespace.Type, Assembly", 2, 1, 1]. The first number is the version
        // number for the type. The next numbers are the version numbers for the first base class, the next base, and so on. This is NOT order-independent,
        // so changes to the runtime class hierarchy will result in a mismatch.
        //
        // 4. Verbose version hierarchy, array where each item is a verbose single type (array with string/number): 
        // [["Namespace.Type, Assembly", 2], ["Namespace.BaseType, Assembly", 1], ["Namespace.SuperBaseType, Assembly", 1]]. Convention is in order
        // of the class hierarchy, but this is order-independent.

        if (!vEntry.IsSavable)
        {
          writer.UnderlyingJsonWriter.WriteString(JsonPropertyNames.ObjectType, JsonPropertyNames.GetEncodedString(vEntry.PersistedTypeName));
        }
        else
        {
          // Get the version hierarchy...
          SavableVersionHierarchy svHierarchy = vEntry.SavableVersionHierarchy;

          // Write shorthand
          if (!writeVerboseTypes)
          {
            // If only type in the list and version 1, can write the simplest representation
            if (svHierarchy.Count == 1 && vEntry.RuntimeVersion == 1)
            {
              writer.UnderlyingJsonWriter.WriteString(JsonPropertyNames.ObjectType, JsonPropertyNames.GetEncodedString(vEntry.PersistedTypeName));
            }
            else
            {
              needsFlushing = true;

              // Write only the type name for this one, but write all the version numbers we have (first is always this type).
              WriteStartArray();

              UnderlyingJsonWriter.WriteStringValue(JsonPropertyNames.GetEncodedString(vEntry.PersistedTypeName));

              for (int i = 0; i < svHierarchy.Count; i++)
                UnderlyingJsonWriter.WriteNumberValue(svHierarchy[i].Version);

              WriteEndArray();
            }
          }
          else
          {
            needsFlushing = true;

            // Write verbose for all types/versions

            // Only need outer array if more than one...
            if (svHierarchy.Count > 1)
              WriteStartArray();

            for (int i = 0; i < svHierarchy.Count; i++)
            {
              SavableVersionHierarchy.Entry svEntry = svHierarchy[i];

              WriteStartArray();

              UnderlyingJsonWriter.WriteStringValue(JsonPropertyNames.GetEncodedString(svEntry.AssemblyQualifiedTypeName));
              UnderlyingJsonWriter.WriteNumberValue(svEntry.Version);

              WriteEndArray();
            }

            if (svHierarchy.Count > 1)
              WriteEndArray();
          }
        }

        // Actually wrote something to the intermediate buffer, now write it raw
        if (needsFlushing)
        {
          UnderlyingJsonWriter.Flush();

          writer.WritePropertyName(JsonPropertyNames.ObjectType);
          writer.UnderlyingJsonWriter.WriteRawValue(typeStream.AsReadOnlySpan(), true);
        }
      }
    }
  }
}