﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;

namespace Tesla.Content
{
  /// <summary>
  /// Enumerates the options to tell the model importer how to generate the tangent basis for normal mapping. For the tangent basis
  /// to be calculated, the model must either already have normals or normals are to be generated, and it must have texture coordinates.
  /// </summary>
  public enum TangentBasisGeneration
  {
    /// <summary>
    /// Do not generate tangent basis.
    /// </summary>
    None,

    /// <summary>
    /// Generate only tangents. Typically the binormal is computed as a cross product of the normal and tangent on the vertex shader.
    /// </summary>
    TangentOnly,

    /// <summary>
    /// Generate both tangents and binormals.
    /// </summary>
    TangentAndBinormal
  }

  /// <summary>
  /// Enumerates the options to tell the model importer how to generate normals.
  /// </summary>
  public enum NormalGeneration
  {
    /// <summary>
    /// Do not generate normals.
    /// </summary>
    None,

    /// <summary>
    /// Use the triangle's face normal for each vertex.
    /// </summary>
    Face,

    /// <summary>
    /// Average the normal with the face normal of every triangle that shares the vertex.
    /// </summary>
    Smooth,

    /// <summary>
    /// Average the normal with the face normal of every triangle that shares the vertex, only when their angles
    /// are less than the crease angle.
    /// </summary>
    Crease
  }

  /// <summary>
  /// Enumerates the options to tell the image importer what texture format the image should be converted to.
  /// </summary>
  public enum TextureConversionFormat
  {
    /// <summary>
    /// Keep the image's format, the importer will try and match to supported SurfaceFormats, whether its Color, DXT,
    /// or some other texture data. This is the default value.
    /// </summary>
    NoChange,

    /// <summary>
    /// Convert to the Color RGBA format.
    /// </summary>
    Color,

    /// <summary>
    /// Convert to DXT Block Compression. The format will be automatically determined based on the existance of alpha (DXT1 for no alpha, DXT5 for alpha).
    /// </summary>
    DXTCompression
  }

  /// <summary>
  /// Enumerates possible error codes when searching for resources and loading a resource file's content via the content manager.
  /// </summary>
  public enum ContentErrorCode
  {
    /// <summary>
    /// Unknown error.
    /// </summary>
    Unknown = -1,

    /// <summary>
    /// No error, the operation was a success.
    /// </summary>
    Success = 0,

    /// <summary>
    /// The resource file could not be found.
    /// </summary>
    ResourceFileNotFound = 1,

    /// <summary>
    /// The resource importer for the specified content type could not be found.
    /// </summary>
    ResourceImporterNotFound = 2,

    /// <summary>
    /// The resource importer for the specified content type could not be found, but a default resource was found and returned. This is generally
    /// a sign of missing content files.
    /// </summary>
    ResourceImporterNotFoundDefaultUsed = 3
  }

  /// <summary>
  /// Enumerates how shared/external resources should be handled.
  /// </summary>
  public enum ExternalSharedMode
  {
    /// <summary>
    /// Default behavior.
    /// </summary>
    Default = 0,

    /// <summary>
    /// All savables to be written as external should be treated as shared.
    /// </summary>
    AllShared = 1,

    /// <summary>
    /// All savables to be written as shared should be treated as external.
    /// </summary>
    AllExternal = 2
  }

  /// <summary>
  /// Bit flags for controlling aspects of savable serialization.
  /// </summary>
  [Flags]
  public enum SavableWriteFlags
  {
    /// <summary>
    /// None.
    /// </summary>
    None = 0,

    /// <summary>
    /// External writers should overwrite existing resources.
    /// </summary>
    OverwriteExistingResource = 1,

    /// <summary>
    /// Writers should favor compression when its available.
    /// </summary>
    UseCompression = 2,

    /// <summary>
    /// Shared resources should be treated as external resources.
    /// </summary>
    SharedAsExternal = 4,

    /// <summary>
    /// Write out verbose <see cref="ISavable"/> type information when applicable. Mostly for text formats that may by default use shorthand and
    /// incomplete type information for brevity.
    /// </summary>
    VerboseTypes = 8
  }

  /// <summary>
  /// Enumerates how a resource/repository file should be opened.
  /// </summary>
  public enum ResourceFileMode
  {
    /// <summary>
    /// A new file is always created at the path specified, but if there is an existing file an exception gets thrown instead of
    /// it being overwritten.
    /// </summary>
    CreateNew = 1,

    /// <summary>
    /// A new file is always created at the path specified, overwriting an existing file.
    /// </summary>
    Create = 2,

    /// <summary>
    /// The file is opened, if it exists.
    /// </summary>
    Open = 3,

    /// <summary>
    /// If the file cannot be found, it will be created at the path specified and then opened. If it is found, it is simply opened.
    /// </summary>
    OpenOrCreate = 4
  }

  /// <summary>
  /// Enumerates how a resource file can be accessed.
  /// </summary>
  public enum ResourceFileAccess
  {
    /// <summary>
    /// The resource stream is open for read only.
    /// </summary>
    Read = 1,

    /// <summary>
    /// The resource stream is open for write only.
    /// </summary>
    Write = 2,

    /// <summary>
    /// The resource stream is open for both reading and writing.
    /// </summary>
    ReadWrite = 3
  }

  /// <summary>
  /// Enumerates what access other resource streams can have on an open file.
  /// </summary>
  [Flags]
  public enum ResourceFileShare
  {
    /// <summary>
    /// Declines sharing of the current file.
    /// </summary>
    None = 0,

    /// <summary>
    /// Allows subsequent opening of the file for reading.
    /// </summary>
    Read = 1,

    /// <summary>
    /// Allows subsequent opening of the file for writing.
    /// </summary>
    Write = 2,

    /// <summary>
    /// Allows subsequent opening of the file for reading or writing.
    /// </summary>
    ReadWrite = 3,

    /// <summary>
    /// Allows subsequent deleting of a file.
    /// </summary>
    Delete = 4
  }
}
