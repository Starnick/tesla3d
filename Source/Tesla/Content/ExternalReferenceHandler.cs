﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Threading;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Tesla.Utilities;

// #nullable enable

namespace Tesla.Content
{
  /// <summary>
  /// Generic external handler for processing external references during savable serialization. The handler is responsible for creating a reference
  /// to a savable that needs to be saved externally. External writers are responsible for doing the actual write out. This particular implementation
  /// queues the savables as they are processed and are written out when the handler is flushed. If no writer is registered for a particular savable type,
  /// a default catch-all writer can be set to the handler.
  /// </summary>
  public class ExternalReferenceHandler : IExternalReferenceHandler
  {
    private IResourceFile m_parentResourceFile;
    private SavableWriteFlags m_writeFlags;
    private Dictionary<Type, IExternalReferenceWriter> m_writers;
    private Dictionary<Type, GetResourceNameDelegate> m_namingDelegates;

    private Dictionary<string, int> m_nameIndices;
    private MultiKeyDictionary<Type, ISavable, ExternalReference> m_externalReferences;
    private ConcurrentQueue<Task> m_fileIOTasks;
    private Thread m_threadCreatedOn;
    private bool m_openedRepository;
    private Dictionary<string, ISavable> m_resourcePathToObject;

    private bool m_inFlushing;

    private IExternalReferenceWriter m_defaultWriter;

    /// <summary>
    /// Gets the resource file that this handler processes child resource files for. The resources
    /// that are created/updated are relative to this resource file.
    /// </summary>
    public IResourceFile ParentResourceFile
    {
      get
      {
        return m_parentResourceFile;
      }
    }

    /// <summary>
    /// Gets or sets the write flags that specify certain behaviors during serialization.
    /// </summary>
    public SavableWriteFlags WriteFlags
    {
      get
      {
        return m_writeFlags;
      }
      set
      {
        m_writeFlags = value;
      }
    }

    /// <summary>
    /// Gets all external writers registered to this handler.
    /// </summary>
    public IEnumerable<IExternalReferenceWriter> Writers
    {
      get
      {
        return m_writers.Values;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="ExternalReferenceHandler"/> class. By default, the external writers of this handler
    /// will overwrite existing resource files. This behavior can be modified by passing in custom write flags.
    /// </summary>
    /// <param name="defaultWriter">Default external writer, serves as a fallback that can handle any savable type. It MUST target ISavable.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the default writer is null.</exception>
    /// <exception cref="System.ArgumentException">Thrown if the default writer does not target ISavable.</exception>
    public ExternalReferenceHandler(IExternalReferenceWriter defaultWriter) : this(SavableWriteFlags.OverwriteExistingResource, defaultWriter, null) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="ExternalReferenceHandler"/> class.
    /// </summary>
    /// <param name="writeFlags">Write flags that specify certain behaviors.</param>
    /// <param name="defaultWriter">Default external writer, serves as a fallback that can handle any savable type. It MUST target ISavable.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the default writer is null.</exception>
    /// <exception cref="System.ArgumentException">Thrown if the default writer does not target ISavable.</exception>
    public ExternalReferenceHandler(SavableWriteFlags writeFlags, IExternalReferenceWriter defaultWriter) : this(writeFlags, defaultWriter, null) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="ExternalReferenceHandler"/> class.
    /// </summary>
    /// <param name="writeFlags">Write flags that specify certain behaviors.</param>
    /// <param name="defaultWriter">Default external writer, serves as a fallback that can handle any savable type. It MUST target ISavable.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the default writer is null.</exception>
    /// <exception cref="System.ArgumentException">Thrown if the default writer does not target ISavable.</exception>
    public ExternalReferenceHandler(SavableWriteFlags writeFlags, IExternalReferenceWriter defaultWriter, IEqualityComparer<ISavable> comparer)
    {
      ArgumentNullException.ThrowIfNull(defaultWriter, nameof(defaultWriter));

      m_writeFlags = writeFlags;

      m_fileIOTasks = new ConcurrentQueue<Task>();
      m_threadCreatedOn = Thread.CurrentThread;
      m_writers = new Dictionary<Type, IExternalReferenceWriter>();
      m_namingDelegates = new Dictionary<Type, GetResourceNameDelegate>();
      m_externalReferences = new MultiKeyDictionary<Type, ISavable, ExternalReference>(null, new StandardContentComparer(comparer ?? new ReferenceEqualityComparer<ISavable>()));
      m_resourcePathToObject = new Dictionary<string, ISavable>();
      m_nameIndices = new Dictionary<string, int>();
      m_inFlushing = false;

      if (defaultWriter.TargetType != typeof(ISavable))
        throw new ArgumentException(StringLocalizer.Instance.GetLocalizedString("NotISavableExternalWriter"));

      m_defaultWriter = defaultWriter;
      RegisterWriter(m_defaultWriter);
    }

    /// <summary>
    /// Helper method to create a new <see cref="IExternalReferenceHandler"/> that writes out Binary files by default.
    /// </summary>
    /// <param name="parentResourceFile">Optional parent resource file. If not null, it will be set to the handler.</param>
    /// <param name="writeFlags">Optional write flags.</param>
    /// <param name="comparer">Optional savable comparer.</param>
    /// <returns>External reference handler.</returns>
    public static IExternalReferenceHandler CreateBinaryDefaultHandler(IResourceFile parentResourceFile = null, SavableWriteFlags writeFlags = SavableWriteFlags.OverwriteExistingResource, IEqualityComparer<ISavable> comparer = null)
    {
      IExternalReferenceHandler handler = new ExternalReferenceHandler(writeFlags, new BinaryExternalWriter(), comparer);
      if (parentResourceFile is not null)
        handler.SetParentResourceFile(parentResourceFile);

      return handler;
    }

    /// <summary>
    /// Helper method to create a new <see cref="IExternalReferenceHandler"/> that writes out JSON files by default.
    /// </summary>
    /// <param name="parentResourceFile">Optional parent resource file. If not null, it will be set to the handler.</param>
    /// <param name="writeFlags">Optional write flags.</param>
    /// <param name="comparer">Optional savable comparer.</param>
    /// <returns>External reference handler.</returns>
    public static IExternalReferenceHandler CreateJsonDefaultHandler(IResourceFile parentResourceFile = null, SavableWriteFlags writeFlags = SavableWriteFlags.OverwriteExistingResource, IEqualityComparer<ISavable> comparer = null)
    {
      IExternalReferenceHandler handler = new ExternalReferenceHandler(writeFlags, new JsonExternalWriter(), comparer);
      if (parentResourceFile is not null)
        handler.SetParentResourceFile(parentResourceFile);

      return handler;
    }

    /// <summary>
    /// Registers an external writer that handles a specific savable type.
    /// </summary>
    /// <param name="writer">External savable writer</param>
    public void RegisterWriter(IExternalReferenceWriter writer)
    {
      if (writer == null || writer.TargetType == null)
        return;

      m_writers[writer.TargetType] = writer;
    }

    /// <summary>
    /// Removes an external writer based on its target savable type it was registered to.
    /// </summary>
    /// <typeparam name="T">Target savable type</typeparam>
    /// <returns>True if the writer was removed, false otherwise</returns>
    public bool RemoveWriter<T>() where T : class, ISavable
    {
      Type type = typeof(T);

      bool success = m_writers.Remove(type);

      //We can override the default writer, but if we remove it, always ensure we add the default.
      if (success && type == typeof(ISavable) && m_defaultWriter != null)
        m_writers[type] = m_defaultWriter;

      return success;
    }

    /// <summary>
    /// Gets a registered external writer based on its target savable type it was registered to.
    /// </summary>
    /// <typeparam name="T">Target savable type</typeparam>
    /// <returns>The external savable writer, or null if a writer is not registered to the type.</returns>
    public IExternalReferenceWriter GetWriter<T>() where T : class, ISavable
    {
      IExternalReferenceWriter writer;
      if (m_writers.TryGetValue(typeof(T), out writer))
        return writer;

      return null;
    }

    /// <summary>
    /// Adds a delegate that returns a unique resource name for a given savable, registered to a specific
    /// savable type. This allows for custom name transformations for output files.
    /// </summary>
    /// <typeparam name="T">Target savable type</typeparam>
    /// <param name="getResourceNameDelegate">Get resource name delegate</param>
    public void SetGetResourceNameDelegate<T>(GetResourceNameDelegate getResourceNameDelegate) where T : class, ISavable
    {
      Type type = typeof(T);

      if (getResourceNameDelegate == null)
      {
        m_namingDelegates.Remove(type);
      }
      else
      {
        m_namingDelegates[type] = getResourceNameDelegate;
      }
    }

    /// <summary>
    /// Resets the handler to process external references relative to the specified resource file.
    /// </summary>
    /// <param name="parentResourceFile">The resource file that this handler will write external references relative to.</param>
    /// <exception cref="ArgumentNullException">Thrown if the resource file is null.</exception>
    /// <exception cref="ArgumentException">Thrown if the resource repository does not exist or is read only.</exception>
    public void SetParentResourceFile(IResourceFile parentResourceFile)
    {
      if (parentResourceFile == null || parentResourceFile.Repository == null)
        throw new ArgumentNullException("parentResourceFile");

      if (!parentResourceFile.Repository.Exists)
        throw new ArgumentException(StringLocalizer.Instance.GetLocalizedString("ResourceRepositoryDoesNotExist"));

      if (parentResourceFile.Repository.IsReadOnly)
        throw new ArgumentException(StringLocalizer.Instance.GetLocalizedString("ResourceRepositoryIsReadOnly"));

      Clear();
      m_parentResourceFile = parentResourceFile;
      OpenRepositoryIfNecessary();
    }

    /// <summary>
    /// Processes a savable into an external reference.
    /// </summary>
    /// <typeparam name="T">Savable type</typeparam>
    /// <param name="value">Savable value</param>
    /// <returns>External reference to the savable</returns>
    public ExternalReference ProcessSavable<T>(T value) where T : class, ISavable
    {
      if (m_parentResourceFile is null || value is null)
        return ExternalReference.NullReference;

      ExternalReference externalRef;
      Type targetType = typeof(T);

      lock (m_externalReferences)
      {
        //First see if we have a reference for this value already
        if (m_externalReferences.TryGetValue(targetType, value, out externalRef))
          return externalRef;

        //If in flushing, we're in read-only mode
        // if(m_inFlushing)
        //    return ExternalReference.NullReference;

        //Create a reference to this guy
        String resourcePath = GetResourceName<T>(value);

        if (String.IsNullOrEmpty(resourcePath))
        {
          externalRef = ExternalReference.NullReference;
        }
        else
        {
          externalRef = new ExternalReference(targetType, resourcePath);

          if (m_resourcePathToObject.ContainsKey(externalRef.ResourcePath))
          {
            System.Diagnostics.Debug.Assert(true, String.Format("{0}, has been processed already, is there a duplicate cloned resource? Only the first processed will be saved.\nThis may be acceptable for some objects.", resourcePath));
          }
          else
          {
            m_resourcePathToObject.Add(externalRef.ResourcePath, value);
          }
        }

        m_externalReferences.Add(targetType, value, externalRef);
      }

      QueueAndRunTask(value, externalRef);

      return externalRef;
    }

    /// <summary>
    /// Checks if the path/resource name is unique of all the references that the handler has processed so far.
    /// </summary>
    /// <param name="resourcePath">Resource path (without extension)</param>
    /// <returns>True if the name is unique, false otherwise.</returns>
    public bool IsUniqueResourcePath(String resourcePath)
    {
      return !m_resourcePathToObject.ContainsKey(resourcePath);
    }

    /// <summary>
    /// Flushes any remaining data that needs to be written.
    /// </summary>
    public void Flush()
    {
      if (m_inFlushing)
        return;

      if (m_threadCreatedOn != Thread.CurrentThread)
        return;

      m_inFlushing = true;

      try
      {
        Task task;
        while (m_fileIOTasks.TryDequeue(out task))
        {
          EngineLog.Log(LogLevel.Info, "Task running...");
          if (task != null)
            task.Wait();
        }
      }
      finally
      {
        if (m_openedRepository)
          m_parentResourceFile.Repository.CloseConnection();

        m_inFlushing = false;
      }

      /*
      if(m_inFlushing)
          return;

      m_inFlushing = true;

      OpenRepositoryIfNecessary();

      bool overwriteExisting = m_writeFlags.HasFlag(SavableWriteFlags.OverwriteExistingResource);

      try
      {
          List<Task> tasks = new List<Task>();

          foreach(KeyValuePair<ISavable, ExternalReference> kv in m_externalReferences)
          {
              ExternalReference externalRef = kv.Value;
              IExternalReferenceWriter writer = FindSuitableWriter(externalRef.TargetType);
              IResourceFile outputFile = repo.GetResourceFileRelativeTo(externalRef.ResourcePath, m_parentResourceFile);

              if(!overwriteExisting && outputFile.Exists)
                  continue;

              Task task = new Task(() => WriteExternalFile(outputFile, writer, kv.Key));
              task.Start();

              tasks.Add(task);
          }

          if(tasks.Count > 0)
              Task.WaitAll(tasks.ToArray());

      }
      catch(AggregateException e)
      {
          throw new TeslaContentException(StringLocalizer.Instance.GetLocalizedString("FailedToWriteExternalReferences"), e.Flatten());

      }
      finally
      {
          if(m_openedRepository)
              m_parentResourceFile.Repository.CloseConnection();

          m_inFlushing = false;
      }*/
    }

    /// <summary>
    /// Clears cached external file references.
    /// </summary>
    public void Clear()
    {
      m_externalReferences.Clear();
      m_nameIndices.Clear();
      m_resourcePathToObject.Clear();
    }

    /// <summary>
    /// Clones the handler.
    /// </summary>
    /// <returns>Shallow copy of the handler.</returns>
    public IExternalReferenceHandler Clone()
    {
      ExternalReferenceHandler handler = new ExternalReferenceHandler(m_writeFlags, m_defaultWriter, m_externalReferences.MinorKeyComparer);

      //Copy over writers
      foreach (IExternalReferenceWriter writer in m_writers.Values)
        handler.RegisterWriter(writer);

      //Copy over naming delegates
      foreach (KeyValuePair<Type, GetResourceNameDelegate> kv in m_namingDelegates)
        m_namingDelegates.Add(kv.Key, kv.Value);

      return handler;
    }

    private void OpenRepositoryIfNecessary()
    {
      IResourceRepository repo = m_parentResourceFile.Repository;
      bool needToOpen = !repo.IsOpen;

      if (needToOpen)
        repo.OpenConnection(ResourceFileMode.Create);

      m_openedRepository = needToOpen;
    }

    private void QueueAndRunTask(ISavable obj, ExternalReference extReference)
    {
      bool overwriteExisting = m_writeFlags.HasFlag(SavableWriteFlags.OverwriteExistingResource);

      IExternalReferenceWriter writer = FindSuitableWriter(extReference.TargetType);
      IResourceFile outputFile = m_parentResourceFile.Repository.GetResourceFileRelativeTo(extReference.ResourcePath, m_parentResourceFile);

      if (!overwriteExisting && outputFile.Exists)
        return;

      Task task = new Task(() => WriteExternalFile(outputFile, writer, obj));
      task.Start();

      m_fileIOTasks.Enqueue(task);
    }

    private void WriteExternalFile(IResourceFile outputFile, IExternalReferenceWriter writer, ISavable savable)
    {
      writer.WriteSavable<ISavable>(outputFile, this, savable);
    }

    private String GetResourceName<T>(T value) where T : class, ISavable
    {
      GetResourceNameDelegate resourceNamer;
      Type type = typeof(T);
      IExternalReferenceWriter writer = FindSuitableWriter(type);
      string resourceName;

      if (!m_namingDelegates.TryGetValue(type, out resourceNamer))
      {
        if (value is INamed && !String.IsNullOrEmpty((value as INamed).Name))
          resourceName = Path.GetFileNameWithoutExtension((value as INamed).Name);
        else
          resourceName = $"{m_parentResourceFile.Name}-{type.Name}";
      }
      else
      {
        resourceName = resourceNamer(this, value);
      }

      // Ensure name is unique
      bool isSameNameAsParent = resourceName.Equals(m_parentResourceFile.Name) && writer.ResourceExtension.Equals(m_parentResourceFile.Extension);
      int index = 0;
      if (m_nameIndices.TryGetValue(resourceName, out index))
      {
        // Increment the index for the name non-unique name
        m_nameIndices[resourceName] = index + 1;
      }
      else
      {
        // If not yet seen, the next resource with this name will have the index appended. If same name as parent, this resource will have an index
        // appended.
        if (isSameNameAsParent)
          index = 1;

        m_nameIndices.Add(resourceName, index + 1);
      }

      // If index is greater than zero, it's not unique and needs an index appended
      if (index > 0)
        resourceName = $"{resourceName}-{index.ToString()}";

      return $"{resourceName}{writer.ResourceExtension}";
    }

    private IExternalReferenceWriter FindSuitableWriter(Type type)
    {
      IExternalReferenceWriter writer;
      if (!m_writers.TryGetValue(type, out writer))
        writer = m_writers[typeof(ISavable)];

      System.Diagnostics.Debug.Assert(writer != null); //Should never happen since we have the fallback writer

      return writer;
    }

    private class StandardContentComparer : IEqualityComparer<ISavable>
    {
      private IEqualityComparer<ISavable> m_comparer;

      public StandardContentComparer(IEqualityComparer<ISavable> comparer)
      {
        m_comparer = comparer;
      }

      public bool Equals(ISavable x, ISavable y)
      {
        IStandardLibraryContent xStandard = x as IStandardLibraryContent;
        IStandardLibraryContent yStandard = y as IStandardLibraryContent;

        if (xStandard != null && yStandard != null && xStandard.IsStandardContent && yStandard.IsStandardContent)
        {
          return xStandard.StandardContentName == yStandard.StandardContentName;
        }

        return m_comparer.Equals(x, y);
      }

      public int GetHashCode(ISavable obj)
      {
        if (obj == null)
          return 0;

        IStandardLibraryContent standard = obj as IStandardLibraryContent;
        if (standard != null)
          return standard.GetContentHashCode();

        return obj.GetHashCode();
      }
    }
  }
}
