﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Graphics;

#nullable enable

namespace Tesla.Content
{
  /// <summary>
  /// A handler that provides a default texture when image content could not be loaded. The default texture can be a checkerboard pattern
  /// that is adjustable in size and color (both colors being the same is also valid).
  /// </summary>
  /// <typeparam name="T">Type of texture</typeparam>
  public sealed class MissingTextureHandler<T> : IMissingContentHandler where T : Texture
  {
    private Texture m_tex;
    private Type m_type;

    /// <summary>
    /// Gets the content target type this handler serves.
    /// </summary>
    public Type TargetType
    {
      get
      {
        return m_type;
      }
    }

    /// <summary>
    /// Gets the default texture that the handler provides.
    /// </summary>
    public Texture DefaultTexture
    {
      get
      {
        return m_tex;
      }
    }

    /// <summary>
    /// Construct a new instance of the <see cref="MissingTextureHandler{T}"/> class.
    /// </summary>
    /// <param name="renderSystem">Render system to create the texture from</param>
    /// <param name="size">Size of the texture - all textures are always square (when appropiate).</param>
    public MissingTextureHandler(IRenderSystem renderSystem, int size)
        : this(renderSystem, size, Color.DarkMagenta, Color.White)
    { }

    /// <summary>
    /// Initializes a new instance of the <see cref="MissingTextureHandler{T}"/> class.
    /// </summary>
    /// <param name="renderSystem">Render system to create the texture from</param>
    /// <param name="size">Size of the texture - all textures are always square (when appropiate).</param>
    /// <param name="primaryColor">Primary checkerboard color</param>
    /// <param name="secondaryColor">Secondary checkerboard color</param>
    /// <exception cref="System.ArgumentNullException">renderSystem</exception>
    public MissingTextureHandler(IRenderSystem renderSystem, int size, Color primaryColor, Color secondaryColor)
    {
      if (renderSystem is null)
        throw new ArgumentNullException("renderSystem");

      m_type = typeof(T);

      size = Math.Max(size, 1);

      if (m_type == typeof(Texture1D))
      {
        m_tex = CreateTexture1D(renderSystem, size, primaryColor, secondaryColor);
      }
      else if (m_type == typeof(Texture2D))
      {
        m_tex = CreateTexture2D(renderSystem, size, primaryColor, secondaryColor);
      }
      else if (m_type == typeof(TextureCube))
      {
        m_tex = CreateTextureCube(renderSystem, size, primaryColor, secondaryColor);
      }

      System.Diagnostics.Debug.Assert(m_tex != null);
    }

    /// <summary>
    /// Gets a place holder piece of content. The type must be the same as the target type or
    /// can be casted to it.
    /// </summary>
    /// <typeparam name="Content">Content type</typeparam>
    /// <returns>The placeholder content</returns>
    public Content GetPlaceHolderContent<Content>() where Content : class
    {
      return (m_tex as Content)!;
    }

    private Texture1D CreateTexture1D(IRenderSystem renderSystem, int size, Color primaryColor, Color secondaryColor)
    {
      using (DataBuffer<Color> firstMip = Create1DCheckerboardPattern(size, primaryColor, secondaryColor))
        return new Texture1D(renderSystem, size, TextureOptions.Init(firstMip, SurfaceFormat.Color, ResourceUsage.Immutable));
    }

    private Texture2D CreateTexture2D(IRenderSystem renderSystem, int size, Color primaryColor, Color secondaryColor)
    {
      using (DataBuffer<Color> firstMip = Create2DCheckerboardPattern(size, primaryColor, secondaryColor))
        return new Texture2D(renderSystem, size, size, TextureOptions.Init(firstMip, SurfaceFormat.Color, ResourceUsage.Immutable));
    }

    private TextureCube CreateTextureCube(IRenderSystem renderSystem, int size, Color primaryColor, Color secondaryColor)
    {
      using (DataBuffer<Color> firstMip = Create2DCheckerboardPattern(size, primaryColor, secondaryColor))
      using (PooledArray<IReadOnlyDataBuffer?> surfaces = new PooledArray<IReadOnlyDataBuffer?>(firstMip, firstMip, firstMip, firstMip, firstMip, firstMip))
      {
        return new TextureCube(renderSystem, size, TextureOptions.Init(surfaces, SurfaceFormat.Color, ResourceUsage.Immutable));
      }
    }

    private DataBuffer<Color> Create1DCheckerboardPattern(int size, Color primaryColor, Color secondaryColor)
    {
      int patternRepeat = (size <= 4) ? 1 : 4;

      DataBufferBuilder<Color> db = new DataBufferBuilder<Color>(size);

      for (int i = 0; i < size; i++)
      {
        bool useFirstColor = ((i / patternRepeat) % 2) == 1;

        if (useFirstColor)
          db.Set(secondaryColor);
        else
          db.Set(secondaryColor);
      }

      return db.Claim();
    }

    private DataBuffer<Color> Create2DCheckerboardPattern(int size, Color primaryColor, Color secondaryColor)
    {
      int patternRepeat = (size <= 4) ? 1 : 4;

      DataBufferBuilder<Color> db = new DataBufferBuilder<Color>(size * size);

      for (int i = 0; i < size; i++) //Row (height)
      {
        for (int j = 0; j < size; j++) //Column (width)
        {
          bool useFirstColor = (((i / patternRepeat) + (j / patternRepeat)) % 2) == 1;

          if (useFirstColor)
            db.Set(primaryColor);
          else
            db.Set(secondaryColor);
        }
      }

      return db.Claim();
    }

  }
}
