﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;

#nullable enable

namespace Tesla.Content
{
  /// <summary>
  /// Generic external resolver that is able to resolve an external reference path to a savable object.
  /// </summary>
  public class ExternalReferenceResolver : IExternalReferenceResolver
  {
    private ContentManager m_contentManager;
    private IResourceFile m_parentResourceFile;

    /// <summary>
    /// Gets the content manager that can load the external resource.
    /// </summary>
    public ContentManager ContentManager
    {
      get
      {
        return m_contentManager;
      }
    }

    /// <summary>
    /// Gets the resource file this handler resolves references relative to.
    /// </summary>
    public IResourceFile ParentResourceFile
    {
      get
      {
        return m_parentResourceFile;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="ExternalReferenceResolver"/> class.
    /// </summary>
    /// <param name="contentManager">Content manager that loads the resource.</param>
    /// <param name="parentResourceFile">Resource file to load external references relative to.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the content manager or resource file are null.</exception>
    /// <exception cref="System.ArgumentException">Thrown if the resource file does not belong to the content manager's repository.</exception>
    public ExternalReferenceResolver(ContentManager contentManager, IResourceFile parentResourceFile)
    {
      ArgumentNullException.ThrowIfNull(contentManager, nameof(contentManager));
      ArgumentNullException.ThrowIfNull(parentResourceFile, nameof(parentResourceFile));

      m_contentManager = contentManager;
      m_parentResourceFile = parentResourceFile;
    }

    /// <summary>
    /// Resolves an external reference and loads the savable using the content manager.
    /// </summary>
    /// <typeparam name="T">Savable type</typeparam>
    /// <param name="externalReference">External reference</param>
    /// <returns>The read savable</returns>
    public T ResolveSavable<T>(ExternalReference externalReference) where T : class?, ISavable?
    {
      if (externalReference is null || externalReference == ExternalReference.NullReference)
        return null!;

      ISavable obj = m_contentManager.LoadRelativeTo<ISavable>(externalReference.ResourcePath, m_parentResourceFile, ImporterParameters.None);

      return (obj as T)!;
    }
  }
}
