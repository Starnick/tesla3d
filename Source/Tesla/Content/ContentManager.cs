﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Tesla.Graphics;
using Tesla.Utilities;

namespace Tesla.Content
{
  /// <summary>
  /// A manager that handles loading and caching of resources. Each manager has a primary resource repository that it loads
  /// resources from, which may or may not be owned by the content manager (allowing for multiple managers to utilize the same
  /// repository, that is externally managed). Additional repositories can be added that act as secondary search paths for when a resource
  /// cannot be resolved. Each content manager has a collection of resource importers that are keyed to a format extension and runtime target type
  /// that do the actual import and processing. Optionally, a content manager can be configured to throw exceptions for content that cannot be loaded
  /// or is missing, or be configured with place holder handlers that return non-cached default content.
  /// </summary>
  public sealed class ContentManager : IEnumerable<Pair<String, Object>>, IDisposable
  {
    private bool m_isDisposed;
    private bool m_throwForMissingContent;

    private Dictionary<IResourceRepository, bool> m_additionalRepositories;
    private IResourceRepository m_repository;
    private bool m_manageRepository;

    private ResourceImporterCollection m_importers;
    private MissingContentHandlerCollection m_missingContentHandlers;
    private IServiceProvider m_serviceProvider;

    private Dictionary<String, Object> m_loadedResources;
    private Dictionary<String, Object> m_resourceLockers;

    /// <summary>
    /// Gets if the content manager has been disposed. When disposed, the manager unloads all resources in its cache.
    /// </summary>
    public bool IsDisposed
    {
      get
      {
        return m_isDisposed;
      }
    }

    /// <summary>
    /// Gets or sets if the content manager should throw a content exception if a resource cannot be found or loaded.
    /// </summary>
    public bool ThrowForMissingContent
    {
      get
      {
        return m_throwForMissingContent;
      }
      set
      {
        m_throwForMissingContent = value;
      }
    }

    /// <summary>
    /// Gets or sets if the resource repository should be managed by the content manager, this is when the manager is disposed, should
    /// the connection to the repository also be closed.
    /// </summary>
    public bool ManageRepository
    {
      get
      {
        return m_manageRepository;
      }
      set
      {
        m_manageRepository = value;
      }
    }

    /// <summary>
    /// Gets or sets the resource repository that the content manager resolves resources from.
    /// </summary>
    /// <exception cref="ArgumentNullException">Thrown if the value is null.</exception>
    public IResourceRepository ResourceRepository
    {
      get
      {
        return m_repository;
      }
      set
      {
        if (value == null)
          throw new ArgumentNullException("value");

        m_repository = value;
      }
    }

    /// <summary>
    /// Gets the additional resource repositories that the content manager resolves resources from if it cannot resolve
    /// from the primary repository.
    /// </summary>
    public IEnumerable<IResourceRepository> AdditionalResourceRepositories
    {
      get
      {
        return m_additionalRepositories.Keys;
      }
    }

    /// <summary>
    /// Gets the collection of missing content handlers. These handlers are keyed to a specific resource type (e.g. Texture2D), if a handler
    /// is not present then the default value is used (e.g. null for classes).
    /// </summary>
    /// <remarks>Adding/removing content handlers is not thread-safe.</remarks>
    public MissingContentHandlerCollection MissingContentHandlers
    {
      get
      {
        return m_missingContentHandlers;
      }
    }

    /// <summary>
    /// Gets the collection of resource importers. These importers are keyed to a specific format extension and a target type. Adding/removing
    /// importers is not thread safe.
    /// </summary>
    /// <remarks>Adding/removing importers is not thread-safe.</remarks>
    public ResourceImporterCollection ResourceImporters
    {
      get
      {
        return m_importers;
      }
    }

    /// <summary>
    /// Gets the service provider, used to locate services that may be required during content loading.
    /// </summary>
    public IServiceProvider ServiceProvider
    {
      get
      {
        return m_serviceProvider;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="ContentManager"/> class. By default, the repository is a standard file repository
    /// whose root path is the application's root directory.
    /// </summary>
    public ContentManager() : this(null, new FileResourceRepository(), true) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="ContentManager"/> class.
    /// </summary>
    /// <param name="repository">The resource repository that the content manager that loads resources from and also manages.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the repository is null or does not exist.</exception>
    public ContentManager(IResourceRepository repository) : this(null, repository, true) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="ContentManager"/> class. By default, the repository is a standard file repository
    /// whose root path is the application's root directory.
    /// </summary>
    /// <param name="serviceProvider">Service provider the content manager should use to locate services during content loading. If null, then the 
    /// engine service register is used.</param>
    /// <exception cref="TeslaContentException">Thrown if no service provider has been set, and the engine has not been initialized (engine service registery is the default).</exception>
    public ContentManager(IServiceProvider serviceProvider) : this(serviceProvider, new FileResourceRepository(), true) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="ContentManager"/> class.
    /// </summary>
    /// <param name="serviceProvider">Service provider the content manager should use to locate services during content loading. If null, then the 
    /// engine service register is used.</param>
    /// <param name="repository">The resource repository that the content manager that loads resources from and also manages.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the repository is null or does not exist.</exception>
    /// <exception cref="TeslaContentException">Thrown if no service provider has been set, and the engine has not been initialized (engine service registery is the default).</exception>
    public ContentManager(IServiceProvider serviceProvider, IResourceRepository repository) : this(serviceProvider, repository, true) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="ContentManager"/> class.
    /// </summary>
    /// <param name="serviceProvider">Service provider the content manager should use to locate services during content loading. If null, then the 
    /// engine service register is used.</param>
    /// <param name="repository">The resource repository that the content manager that loads resources from</param>
    /// <param name="manageRepository">True if the repository should be managed, that is closed when the manager is disposed.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the repository is null or does not exist.</exception>
    /// <exception cref="TeslaContentException">Thrown if no service provider has been set, and the engine has not been initialized (engine service registery is the default).</exception>
    public ContentManager(IServiceProvider serviceProvider, IResourceRepository repository, bool manageRepository)
    {
      if (serviceProvider == null)
      {
        if (!Engine.IsInitialized)
          throw new TeslaContentException(StringLocalizer.Instance.GetLocalizedString("EngineNotInitialized"));

        m_serviceProvider = Engine.Instance.Services;
      }
      else
      {
        m_serviceProvider = serviceProvider;
      }

      if (repository == null)
        throw new ArgumentNullException("repository");

      if (!repository.Exists)
        throw new ArgumentNullException("repository", StringLocalizer.Instance.GetLocalizedString("ResourceRepositoryDoesNotExist"));

      m_repository = repository;
      m_manageRepository = manageRepository;

      if (!m_repository.IsOpen)
        m_repository.OpenConnection(ResourceFileMode.Open, ResourceFileShare.Read);

      m_isDisposed = false;
      m_throwForMissingContent = false;

      m_importers = new ResourceImporterCollection();
      m_importers.Add(new BinaryResourceImporter());
      m_importers.Add(new JsonResourceImporter());
      //m_importers.Add(new XmlResourceImporter());
      m_importers.Add(new TEFXResourceImporter());
      m_importers.Add(new TEXUResourceImporter());

      MaterialParserCache parserCache = new MaterialParserCache();
      m_importers.Add(new MaterialResourceImporter(parserCache));
      m_importers.Add(new MaterialDefinitionResourceImporter(parserCache));

      m_additionalRepositories = new Dictionary<IResourceRepository, bool>(new ReferenceEqualityComparer<IResourceRepository>());
      m_missingContentHandlers = new MissingContentHandlerCollection();
      m_loadedResources = new Dictionary<String, Object>();
      m_resourceLockers = new Dictionary<String, Object>();
    }

    #region Load

    /// <summary>
    /// Loads and processes a resource into its runtime type.
    /// </summary>
    /// <remarks>This method is thread-safe.</remarks>
    /// <typeparam name="T">Content type to load</typeparam>
    /// <param name="resourceName">Full resource name (with its extension)</param>
    /// <returns>The loaded resource</returns>
    /// <exception cref="ObjectDisposedException">Thrown if the content manager has previously been disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the content to be loaded cannot be casted to the specified type</exception>
    /// <exception cref="TeslaContentException">Thrown if the resource could not be found or loaded. The manager can be configured to return default content if the resource could not be resolved, or throw this exception.</exception>
    public T Load<T>(String resourceName) where T : class
    {
      CheckDisposed();

      return PrepareAndLoadInternal<T>(resourceName, null, false, null);
    }

    /// <summary>
    /// Loads and processes a resource into its runtime type.
    /// </summary>
    /// <remarks>This method is thread-safe.</remarks>
    /// <typeparam name="T">Content type to load</typeparam>
    /// <param name="resourceName">Full resource name (with its extension)</param>
    /// <param name="parameters">Optional importer parameters for the resource importer</param>
    /// <returns>The loaded resource</returns>
    /// <exception cref="ObjectDisposedException">Thrown if the content manager has previously been disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the content to be loaded cannot be casted to the specified type</exception>
    /// <exception cref="TeslaContentException">Thrown if the resource could not be found or loaded. The manager can be configured to return default content if the resource could not be resolved, or throw this exception.</exception>
    public T Load<T>(String resourceName, ImporterParameters parameters) where T : class
    {
      CheckDisposed();

      return PrepareAndLoadInternal<T>(resourceName, null, false, parameters);
    }

    /// <summary>
    /// Loads and processes a resource into its runtime type.
    /// </summary>
    /// <remarks>This method is thread-safe.</remarks>
    /// <typeparam name="T">Content type to load</typeparam>
    /// <param name="resourceName">Relative resource name (with its extension)</param>
    /// <param name="resourceFile">The resource file that the resource is relative to.</param>
    /// <returns>The loaded resource</returns>
    /// <exception cref="ObjectDisposedException">Thrown if the content manager has previously been disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the content to be loaded cannot be casted to the specified type</exception>
    /// <exception cref="TeslaContentException">Thrown if the resource could not be found or loaded. The manager can be configured to return default content if the resource could not be resolved, or throw this exception.</exception>
    public T LoadRelativeTo<T>(String resourceName, IResourceFile resourceFile) where T : class
    {
      CheckDisposed();

      return PrepareAndLoadInternal<T>(resourceName, resourceFile, false, null);
    }

    /// <summary>
    /// Loads and processes a resource into its runtime type.
    /// </summary>
    /// <remarks>This method is thread-safe.</remarks>
    /// <typeparam name="T">Content type to load</typeparam>
    /// <param name="resourceName">Relative resource name (with its extension)</param>
    /// <param name="resourceFile">The resource file that the resource is relative to.</param>
    /// <param name="parameters">Optional importer parameters for the resource importer</param>
    /// <returns>The loaded resource</returns>
    /// <exception cref="ObjectDisposedException">Thrown if the content manager has previously been disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the content to be loaded cannot be casted to the specified type</exception>
    /// <exception cref="TeslaContentException">Thrown if the resource could not be found or loaded. The manager can be configured to return default content if the resource could not be resolved, or throw this exception.</exception>
    public T LoadRelativeTo<T>(String resourceName, IResourceFile resourceFile, ImporterParameters parameters) where T : class
    {
      CheckDisposed();

      return PrepareAndLoadInternal<T>(resourceName, resourceFile, false, parameters);
    }

    /// <summary>
    /// Loads and processes a resource into its runtime type.
    /// </summary>
    /// <remarks>
    /// This first attempts to load the resource relative to another, but if that fails then the given resource path is considered to be a fully qualified path and will be used to attempt 
    /// to load the resource. This method is thread-safe.
    /// </remarks>
    /// <typeparam name="T">Content type to load</typeparam>
    /// <param name="resourceName">Relative resource name (with its extension)</param>
    /// <param name="optionalResourceFile">Optional resource file that the resource is relative to.</param>
    /// <returns>The loaded resource.</returns>
    /// <exception cref="ObjectDisposedException">Thrown if the content manager has previously been disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the content to be loaded cannot be casted to the specified type</exception>
    /// <exception cref="TeslaContentException">Thrown if the resource could not be found or loaded. The manager can be configured to return default content if the resource could not be resolved, or throw this exception.</exception>
    public T LoadOptionalRelativeTo<T>(String resourceName, IResourceFile optionalResourceFile) where T : class
    {
      CheckDisposed();

      return PrepareAndLoadInternal<T>(resourceName, optionalResourceFile, true, null);
    }

    /// <summary>
    /// Loads and processes a resource into its runtime type.
    /// </summary>
    /// <remarks>
    /// This first attempts to load the resource relative to another, but if that fails then the given resource path is considered to be a fully qualified path and will be used to attempt 
    /// to load the resource. This method is thread-safe.
    /// </remarks>
    /// <typeparam name="T">Content type to load</typeparam>
    /// <param name="resourceName">Relative resource name (with its extension)</param>
    /// <param name="optionalResourceFile">Optional resource file that the resource is relative to.</param>
    /// <param name="parameters">Optional importer parameters for the resource importer</param>
    /// <returns>The loaded resource.</returns>
    /// <exception cref="ObjectDisposedException">Thrown if the content manager has previously been disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the content to be loaded cannot be casted to the specified type</exception>
    /// <exception cref="TeslaContentException">Thrown if the resource could not be found or loaded. The manager can be configured to return default content if the resource could not be resolved, or throw this exception.</exception>
    public T LoadOptionalRelativeTo<T>(String resourceName, IResourceFile optionalResourceFile, ImporterParameters parameters) where T : class
    {
      CheckDisposed();

      return PrepareAndLoadInternal<T>(resourceName, optionalResourceFile, true, parameters);
    }

    #endregion

    #region LoadAsync

    /// <summary>
    /// Loads and processes a resource into its runtime type asynchronously.
    /// </summary>
    /// <remarks>This method is thread-safe.</remarks>
    /// <typeparam name="T">Content type to load</typeparam>
    /// <param name="resourceName">Full resource name (with its extension)</param>
    /// <returns>A task that represents the asynchronous loading operation</returns>
    /// <exception cref="ObjectDisposedException">Thrown if the content manager has previously been disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the content to be loaded cannot be casted to the specified type</exception>
    /// <exception cref="TeslaContentException">Thrown if the resource could not be found or loaded. The manager can be configured to return default content if the resource could not be resolved, or throw this exception.</exception>
    public Task<T> LoadAsync<T>(String resourceName) where T : class
    {
      CheckDisposed();

      return PrepareAndLoadInternalAsync<T>(resourceName, null, false, null);
    }

    /// <summary>
    /// Loads and processes a resource into its runtime type asynchronously.
    /// </summary>
    /// <remarks>This method is thread-safe.</remarks>
    /// <typeparam name="T">Content type to load</typeparam>
    /// <param name="resourceName">Full resource name (with its extension)</param>
    /// <param name="parameters">Optional importer parameters for the resource importer</param>
    /// <returns>A task that represents the asynchronous loading operation</returns>
    /// <exception cref="ObjectDisposedException">Thrown if the content manager has previously been disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the content to be loaded cannot be casted to the specified type</exception>
    /// <exception cref="TeslaContentException">Thrown if the resource could not be found or loaded. The manager can be configured to return default content if the resource could not be resolved, or throw this exception.</exception>
    public Task<T> LoadAsync<T>(String resourceName, ImporterParameters parameters) where T : class
    {
      CheckDisposed();

      return PrepareAndLoadInternalAsync<T>(resourceName, null, false, parameters);
    }

    /// <summary>
    /// Loads and processes a resource into its runtime type asynchronously.
    /// </summary>
    /// <remarks>This method is thread-safe.</remarks>
    /// <typeparam name="T">Content type to load</typeparam>
    /// <param name="resourceName">Relative resource name (with its extension)</param>
    /// <param name="resourceFile">The resource file that the resource is relative to.</param>
    /// <returns>A task that represents the asynchronous loading operation</returns>
    /// <exception cref="ObjectDisposedException">Thrown if the content manager has previously been disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the content to be loaded cannot be casted to the specified type</exception>
    /// <exception cref="TeslaContentException">Thrown if the resource could not be found or loaded. The manager can be configured to return default content if the resource could not be resolved, or throw this exception.</exception>
    public Task<T> LoadRelativeToAsync<T>(String resourceName, IResourceFile resourceFile) where T : class
    {
      CheckDisposed();

      return PrepareAndLoadInternalAsync<T>(resourceName, resourceFile, false, null);
    }

    /// <summary>
    /// Loads and processes a resource into its runtime type asynchronously.
    /// </summary>
    /// <remarks>This method is thread-safe.</remarks>
    /// <typeparam name="T">Content type to load</typeparam>
    /// <param name="resourceName">Relative resource name (with its extension)</param>
    /// <param name="resourceFile">The resource file that the resource is relative to.</param>
    /// <param name="parameters">Optional importer parameters for the resource importer</param>
    /// <returns>A task that represents the asynchronous loading operation</returns>
    /// <exception cref="ObjectDisposedException">Thrown if the content manager has previously been disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the content to be loaded cannot be casted to the specified type</exception>
    /// <exception cref="TeslaContentException">Thrown if the resource could not be found or loaded. The manager can be configured to return default content if the resource could not be resolved, or throw this exception.</exception>
    public Task<T> LoadRelativeToAsync<T>(String resourceName, IResourceFile resourceFile, ImporterParameters parameters) where T : class
    {
      CheckDisposed();

      return PrepareAndLoadInternalAsync<T>(resourceName, resourceFile, false, parameters);
    }

    /// <summary>
    /// Loads and processes a resource into its runtime type asynchronously.
    /// </summary>
    /// <remarks>
    /// This first attempts to load the resource relative to another, but if that fails then the given resource path is considered to be a fully qualified path and will be used to attempt 
    /// to load the resource. This method is thread-safe.
    /// </remarks>
    /// <typeparam name="T">Content type to load</typeparam>
    /// <param name="resourceName">Relative resource name (with its extension)</param>
    /// <param name="optionalResourceFile">Optional resource file that the resource is relative to.</param>
    /// <returns>A task that represents the asynchronous loading operation</returns>
    /// <exception cref="ObjectDisposedException">Thrown if the content manager has previously been disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the content to be loaded cannot be casted to the specified type</exception>
    /// <exception cref="TeslaContentException">Thrown if the resource could not be found or loaded. The manager can be configured to return default content if the resource could not be resolved, or throw this exception.</exception>
    public Task<T> LoadOptionalRelativeToAsync<T>(String resourceName, IResourceFile optionalResourceFile) where T : class
    {
      CheckDisposed();

      return PrepareAndLoadInternalAsync<T>(resourceName, optionalResourceFile, true, null);
    }

    /// <summary>
    /// Loads and processes a resource into its runtime type asynchronously.
    /// </summary>
    /// <remarks>
    /// This first attempts to load the resource relative to another, but if that fails then the given resource path is considered to be a fully qualified path and will be used to attempt 
    /// to load the resource. This method is thread-safe.
    /// </remarks>
    /// <typeparam name="T">Content type to load</typeparam>
    /// <param name="resourceName">Relative resource name (with its extension)</param>
    /// <param name="optionalResourceFile">Optional resource file that the resource is relative to.</param>
    /// <param name="parameters">Optional importer parameters for the resource importer</param>
    /// <returns>A task that represents the asynchronous loading operation</returns>
    /// <exception cref="ObjectDisposedException">Thrown if the content manager has previously been disposed.</exception>
    /// <exception cref="InvalidCastException">Thrown if the content to be loaded cannot be casted to the specified type</exception>
    /// <exception cref="TeslaContentException">Thrown if the resource could not be found or loaded. The manager can be configured to return default content if the resource could not be resolved, or throw this exception.</exception>
    public Task<T> LoadOptionalRelativeToAsync<T>(String resourceName, IResourceFile optionalResourceFile, ImporterParameters parameters) where T : class
    {
      CheckDisposed();

      return PrepareAndLoadInternalAsync<T>(resourceName, optionalResourceFile, true, parameters);
    }

    #endregion

    /// <summary>
    /// Unloads all cached loaded resources contained in the content manager.
    /// </summary>
    /// <remarks>This method is not thread-safe.</remarks>
    /// <param name="dispose">Optionally dispose of the resources if they are <see cref="IDisposable"/>.</param>
    /// <exception cref="ObjectDisposedException">Thrown if the content manager has previously been disposed.</exception>
    public void Unload(bool dispose = true)
    {
      CheckDisposed();

      lock (m_resourceLockers)
      {
        lock (m_loadedResources)
        {
          try
          {
            foreach (KeyValuePair<String, Object> kv in m_loadedResources)
            {
              Object obj = kv.Value;
              IDisposable disposable = kv.Value as IDisposable;
              if (dispose && disposable != null)
                disposable.Dispose();
            }
          }
          finally
          {
            m_loadedResources.Clear();
            m_resourceLockers.Clear();
          }
        }
      }
    }

    /// <summary>
    /// Unloads the cached loaded resource from the content manager
    /// </summary>
    /// <param name="obj">Object to remove from the content manager.</param>
    /// <param name="dispose">Optionally dispose of the resource if they are <see cref="IDisposable"/>.</param>
    /// <returns>True if the object was found to be contained in the manager, false if otherwise.</returns>
    public bool Unload(object obj, bool dispose = true)
    {
      if (obj == null)
        return false;

      CheckDisposed();

      lock (m_resourceLockers)
      {
        lock (m_loadedResources)
        {
          String path = null;
          foreach (KeyValuePair<String, Object> kv in m_loadedResources)
          {
            if (obj == kv.Value)
            {
              path = kv.Key;
              break;
            }
          }

          if (path != null)
          {
            //If in loaded resources, then we had a resource locker to it, so remove that as well
            m_resourceLockers.Remove(path);
            m_loadedResources.Remove(path);

            IDisposable disposable = obj as IDisposable;
            if (dispose && disposable != null)
              disposable.Dispose();
          }

          return path != null;
        }
      }
    }

    /// <summary>
    /// Selectively unloads a cached loaded resource contained in the content manager.
    /// </summary>
    /// <param name="resourceName">Fully qualified resource name (including subresource name, optionally).</param>
    /// <param name="dispose">Optionally dispose of the resource if they are <see cref="IDisposable"/>.</param>
    /// <returns>True if the resource was removed and possibly disposed of, false otherwise.</returns>
    public bool Unload(String resourceName, bool dispose = true)
    {
      if (String.IsNullOrEmpty(resourceName))
        return false;

      CheckDisposed();

      lock (m_resourceLockers)
      {
        lock (m_loadedResources)
        {
          Object obj;
          if (m_loadedResources.TryGetValue(resourceName, out obj))
          {
            IDisposable disposable = obj as IDisposable;
            if (dispose && disposable != null)
              disposable.Dispose();

            //If in loaded resources, then we had a resource locker to it, so remove that as well
            m_resourceLockers.Remove(resourceName);
            m_loadedResources.Remove(resourceName);
            return true;
          }

          return false;
        }
      }
    }

    /// <summary>
    /// Selectively unloads a collection of cached loaded resources contained in the content manager.
    /// </summary>
    /// <param name="resourceName">Collection of fully qualified resource names (including subresource name, optionally).</param>
    /// <param name="dispose">Optionally dispose of the resources if they are <see cref="IDisposable"/>.</param>
    /// <returns>True if any of the resources were removed and possibly disposed of, false otherwise.</returns>
    public bool Unload(IEnumerable<String> resourceNames, bool dispose = true)
    {
      CheckDisposed();

      lock (m_resourceLockers)
      {
        lock (m_loadedResources)
        {
          bool removedOne = false;

          foreach (String resourceName in resourceNames)
          {
            Object obj;
            if (m_loadedResources.TryGetValue(resourceName, out obj))
            {
              IDisposable disposable = obj as IDisposable;
              if (dispose && disposable != null)
                disposable.Dispose();

              //If in loaded resources, then we had a resource locker to it, so remove that as well
              m_resourceLockers.Remove(resourceName);
              m_loadedResources.Remove(resourceName);
              removedOne = true;
            }
          }

          return removedOne;
        }
      }
    }

    /// <summary>
    /// Opens a read-only stream to the resource.
    /// </summary>
    /// <remarks>
    /// This operation does not supprt subresource names in the specified path. The path needs to reference a single resource file.
    /// </remarks>
    /// <param name="resourceName">Full resource name (with its extension)</param>
    /// <returns>The stream.</returns>
    /// <exception cref="ObjectDisposedException">Thrown if the content manager has previously been disposed.</exception>
    /// <exception cref="TeslaContentException">Thrown if the resource file to open does not exist.</exception>
    public Stream OpenStream(String resourceName)
    {
      CheckDisposed();

      IResourceFile resourceFile = ResolveResourceFile(resourceName);
      TeslaContentException.ThrowIfDoesNotExist(resourceFile, nameof(resourceName));

      return resourceFile.OpenRead();
    }

    /// <summary>
    /// Opens a read-only stream to the resource that is relative to another resource file.
    /// </summary>
    /// <remarks>
    /// This operation does not supprt subresource names in the specified path. The path needs to reference a single resource file.
    /// </remarks>
    /// <param name="resourceName">Relative resource name (with its extension)</param>
    /// <param name="resourceFile">The resource file that the resource is relative to.</param>
    /// <returns>The stream.</returns>
    /// <exception cref="ObjectDisposedException">Thrown if the content manager has previously been disposed.</exception>
    /// <exception cref="TeslaContentException">Thrown if either the input resource file or the file to open does not exist.</exception>
    public Stream OpenStreamRelativeTo(String resourceName, IResourceFile resourceFile)
    {
      CheckDisposed();

      TeslaContentException.ThrowIfDoesNotExist(resourceName, nameof(resourceName));
      TeslaContentException.ThrowIfDoesNotExist(resourceFile, nameof(resourceFile));

      IResourceFile targetFile = ResolveResourceFile(resourceName, resourceFile);

      TeslaContentException.ThrowIfDoesNotExist(targetFile, nameof(resourceName));

      return targetFile.OpenRead();
    }

    /// <summary>
    /// Opens a read-only stream to the resource that is optionally relative to another resource file. If it could not be located
    /// relatively, then the resource name will be searched as if it were a fully qualified path.
    /// </summary>
    /// <remarks>
    /// This operation does not supprt subresource names in the specified path. The path needs to reference a single resource file.
    /// </remarks>
    /// <param name="resourceName">Relative resource name (with its extension)</param>
    /// <param name="optionalResourceFile">The optional resource file that the resource can be relative to.</param>
    /// <returns>The stream..</returns>
    /// <exception cref="ObjectDisposedException">Thrown if the content manager has previously been disposed.</exception>
    /// <exception cref="TeslaContentException">Thrown if either the input resource file or the file to open does not exist.</exception>
    public Stream OpenStreamOptionalRelativeTo(String resourceName, IResourceFile optionalResourceFile)
    {
      CheckDisposed();

      TeslaContentException.ThrowIfDoesNotExist(resourceName, nameof(resourceName));

      IResourceFile targetFile = ResolveResourceFile(resourceName, optionalResourceFile);

      //If does not exist, then fall back to using resource name as the absolute path
      if (targetFile == null || !targetFile.Exists)
        targetFile = ResolveResourceFile(resourceName);

      //If still does not exist, then we could not open the stream
      TeslaContentException.ThrowIfDoesNotExist(targetFile, nameof(resourceName));

      return targetFile.OpenRead();
    }

    /// <summary>
    /// Queries a resource file representing the resource.
    /// </summary>
    /// <param name="resourceName">Full resource name (with its extension)</param>
    /// <returns>The located file, or null if it does not exist.</returns>
    public IResourceFile QueryResourceFile(String resourceName)
    {
      return QueryResourceFile(resourceName, null);
    }

    /// <summary>
    /// Queries a resource file representing the resource. The file may be relative to another or an absolute path in
    /// the repositories. This will search all repositories.
    /// </summary>
    /// <param name="resourceName">Resource name (with its extnesion)</param>
    /// <param name="resourceFileRelativeTo">Optional relative resource file/</param>
    /// <returns>The located file, or null if it does not exist.</returns>
    public IResourceFile QueryResourceFile(String resourceName, IResourceFile resourceFileRelativeTo)
    {
      CheckDisposed();

      IResourceFile file = null;

      //Locate absolute file...
      if (resourceFileRelativeTo == null || !resourceFileRelativeTo.Exists)
      {
        file = ResolveResourceFile(resourceName);
      }
      else
      {
        //Or locate relative to optional file...
        file = ResolveResourceFile(resourceName, resourceFileRelativeTo);
      }

      if (file != null && file.Exists)
        return file;

      return null;
    }

    /// <summary>
    /// Adds an additional repository to the content manager. If the repository is not open, a connection to the repository will automatically
    /// be opened. The content manager may or may not manage the repository, where it will close the connection when the manager is disposed. This
    /// repository is only used to resolve a resource if a resource could not be located from the manager's primary resource repository, so it can
    /// be thought of as a fallback resolver.
    /// </summary>
    /// <remarks>This method is not thread-safe.</remarks>
    /// <param name="repository">Additional repository</param>
    /// <param name="manageRepository">True if the current content manager should close the repository's connection when the manager is disposed, false if not.</param>
    /// <exception cref="ObjectDisposedException">Thrown if the content manager has previously been disposed.</exception>
    public void AddAdditionalRepository(IResourceRepository repository, bool manageRepository)
    {
      CheckDisposed();

      if (repository == null || m_additionalRepositories.ContainsKey(repository))
        return;

      if (!repository.IsOpen)
        repository.OpenConnection(ResourceFileMode.Open, ResourceFileShare.Read);

      m_additionalRepositories.Add(repository, manageRepository);
    }

    /// <summary>
    /// Removes the specified resource repository, which should already be contained by the content manager. If the repository is managed by the
    /// content manager, then it will also be closed. The content manager's primary resource repository cannot be removed.
    /// </summary>
    /// <remarks>This method is not thread-safe.</remarks>
    /// <param name="repository">Repository to remove</param>
    /// <returns>True if the repository was removed, false otherwise.</returns>
    /// <exception cref="ObjectDisposedException">Thrown if the content manager has previously been disposed.</exception>
    public bool RemoveAdditionalRepository(IResourceRepository repository)
    {
      CheckDisposed();

      if (repository == null)
        return false;

      bool ownsIt;
      if (m_additionalRepositories.TryGetValue(repository, out ownsIt))
      {
        if (ownsIt)
          repository.CloseConnection();

        return true;
      }

      return false;
    }

    /// <summary>
    /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
    /// </summary>
    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    private void Dispose(bool disposing)
    {
      if (!m_isDisposed)
      {
        if (disposing)
        {
          try
          {
            Unload();

            if (m_manageRepository)
              m_repository.CloseConnection();

            foreach (KeyValuePair<IResourceRepository, bool> kv in m_additionalRepositories)
            {
              if (kv.Value)
                kv.Key.CloseConnection();
            }
          }
          finally
          {
            m_importers.Clear();
            m_missingContentHandlers.Clear();
            m_additionalRepositories.Clear();
            m_repository = null;
          }
        }

        m_isDisposed = true;
      }
    }

    //Split this out of LoadInternal to try and resolve the full resource name + subresource before we start locking the cache
    private bool PrepareResourceLoading(String rawResourceName, IResourceFile optionalRelativeResource, bool fallbackRelativeLoading, ref ImporterParameters parameters, out String resourceNameWithSubresourceName, out IResourceFile foundFile, out bool paramsFromPool)
    {
      resourceNameWithSubresourceName = null;
      foundFile = null;
      paramsFromPool = false;

      if (String.IsNullOrEmpty(rawResourceName))
        return false;

      //The final identifier is the fully qualified resource name + subresource, the path used to actually resolve the resource file is the cleaned
      //name. The subresource name will exist on the parameters for the importer to use.
      String resourceNameClean;
      paramsFromPool = MassageSubresourceNaming(rawResourceName, ref parameters, out resourceNameWithSubresourceName, out resourceNameClean);

      bool hasRelativeResource = optionalRelativeResource != null && optionalRelativeResource.Exists;

      //If loading relative to file resource, then try and load it. If we can fallback, try and load as an absolute path
      if (hasRelativeResource)
      {
        foundFile = ResolveResourceFile(resourceNameClean, optionalRelativeResource);
        if ((foundFile == null || !foundFile.Exists) && fallbackRelativeLoading)
          foundFile = ResolveResourceFile(resourceNameClean);
      }
      //Otherwise load as absolute path
      else
      {
        foundFile = ResolveResourceFile(resourceNameClean);
      }

      //If the file exists, then it was successfully resolved
      return (foundFile != null && foundFile.Exists);
    }

    private T PrepareAndLoadInternal<T>(String rawResourceName, IResourceFile optionalRelativeResource, bool fallbackRelativeLoading, ImporterParameters parameters) where T : class
    {
      if (String.IsNullOrEmpty(rawResourceName))
        return GetPlaceHolderOrThrow<T>("NULLRESOURCE");

      String resourceNameWithSubresourceName;
      IResourceFile foundFile;
      bool paramsFromPool;

      bool successfullyResolved = PrepareResourceLoading(rawResourceName, optionalRelativeResource, fallbackRelativeLoading, ref parameters, out resourceNameWithSubresourceName, out foundFile, out paramsFromPool);

      try
      {
        if (!successfullyResolved)
          return GetPlaceHolderOrThrow<T>(rawResourceName);

        return LoadInternal<T>(resourceNameWithSubresourceName, foundFile, parameters);
      }
      finally
      {
        if (paramsFromPool)
          Pool<ImporterParameters>.Return(parameters);
      }
    }

    private Task<T> PrepareAndLoadInternalAsync<T>(String rawResourceName, IResourceFile optionalRelativeResource, bool fallbackRelativeLoading, ImporterParameters parameters) where T : class
    {
      if (String.IsNullOrEmpty(rawResourceName))
        return Task<T>.FromResult<T>(GetPlaceHolderOrThrow<T>("NULLRESOURCE"));

      String resourceNameWithSubresourceName;
      IResourceFile foundFile;
      bool paramsFromPool;

      bool successfullyResolved = PrepareResourceLoading(rawResourceName, optionalRelativeResource, fallbackRelativeLoading, ref parameters, out resourceNameWithSubresourceName, out foundFile, out paramsFromPool);

      if (!successfullyResolved)
        return Task<T>.FromResult<T>(GetPlaceHolderOrThrow<T>(rawResourceName));

      //Create the task that will attempt to load the resource
      Task<T> task = Task<T>.Factory.StartNew(() =>
      {
        try
        {
          return LoadInternal<T>(resourceNameWithSubresourceName, foundFile, parameters);
        }
        finally
        {
          if (paramsFromPool)
            Pool<ImporterParameters>.Return(parameters);
        }
      }, CancellationToken.None, TaskCreationOptions.HideScheduler, TaskScheduler.Default);

      return task;
    }

    private T LoadInternal<T>(String resourceNameWithSubresourceName, IResourceFile fileToLoad, ImporterParameters parameters) where T : class
    {
      //Resolved resource file and final name in a Prepare call before this, so if we hit this everything should be legal
      Object obj;
      Type type = typeof(T);

      //Find an importer for the resource
      IResourceImporter importer = m_importers.FindSuitableImporter(fileToLoad.Extension, type);
      if (importer == null)
        return GetPlaceHolderOrThrow<T>(resourceNameWithSubresourceName);

      if (!importer.CanLoadType(type))
        throw new InvalidCastException(StringLocalizer.Instance.GetLocalizedString("TypeMismatch", importer.TargetType.FullName, typeof(T).FullName));

      //Lock on the fully qualified (full path + optional subresource name)
      lock (GetResourceLocker(resourceNameWithSubresourceName))
      {
        //Lock on the loaded resources dictionary to see if we already have it loaded
        lock (m_loadedResources)
        {
          if (m_loadedResources.TryGetValue(resourceNameWithSubresourceName, out obj))
          {
            if (obj is IContentCastable)
              obj = (obj as IContentCastable).CastTo(type, parameters.SubresourceName);

            return obj as T;
          }
        }

        //If we don't have it cached, try and load it
        try
        {
          obj = importer.Load(fileToLoad, this, parameters);
        }
        catch (Exception e)
        {
          EngineLog.LogException(LogLevel.Error, "Failed to load content", e);
          throw CreateContentLoadException(resourceNameWithSubresourceName, e);
        }

        //If we couldn't load it but no exception, either return placeholder content or throw
        if (obj == null)
          return GetPlaceHolderOrThrow<T>(resourceNameWithSubresourceName);

        //Lock again to add the loaded resource to the cache
        lock (m_loadedResources)
          m_loadedResources.Add(resourceNameWithSubresourceName, obj);

        if (obj is INamable)
          (obj as INamable).Name = fileToLoad.Name;

        if (obj is IContentCastable)
          obj = (obj as IContentCastable).CastTo(type, parameters.SubresourceName);

        return obj as T;
      }
    }

    //We support either [filePath].[extesion]::[subresourcename] syntax OR by setting the subresourcename in the importer parameters
    private bool MassageSubresourceNaming(String rawFilePath, ref ImporterParameters importParams, out String filePathWithSubresourceName, out String filePathClean)
    {
      String filePath, subresourceName;
      ContentHelper.ParseSubresourceName(rawFilePath, out filePath, out subresourceName);

      filePathClean = filePath;

      //No subresource name in the raw file path...check import params
      if (String.IsNullOrEmpty(subresourceName))
      {
        if (importParams == null)
        {
          //No parameters so use the non-pooled default None and use the filepath normally
          importParams = ImporterParameters.None;
          filePathWithSubresourceName = filePathClean;
          return false;
        }
        else
        {
          //Import params may have it
          if (!String.IsNullOrEmpty(importParams.SubresourceName))
          {
            filePathWithSubresourceName = String.Concat(filePath, "::", importParams.SubresourceName);
            return false;
          }
          else
          {
            //Else no subresource name so use the filepath normally
            filePathWithSubresourceName = filePath;
            return false;
          }
        }
      }
      else
      {
        //Got a subresource name, set it on the import params
        if (importParams == null)
        {
          //Get a pooled parameter object
          importParams = Pool<ImporterParameters>.Fetch();
          importParams.Reset();
          importParams.SubresourceName = subresourceName;

          filePathWithSubresourceName = rawFilePath;
          return true;
        }
        else
        {
          //Importer parameters exist and may have a name, but since the subresource name in the file path is not null that takes precedence
          filePathWithSubresourceName = rawFilePath;
          importParams.SubresourceName = subresourceName;
          return false;
        }
      }
    }

    private T GetPlaceHolderOrThrow<T>(String resourceName) where T : class
    {
      EngineLog.Log(LogLevel.Warn, StringLocalizer.Instance.GetLocalizedString("ContentCouldNotBeLoaded", resourceName));

      if (m_throwForMissingContent)
        throw CreateContentLoadException(resourceName, null);

      IMissingContentHandler handler = m_missingContentHandlers[typeof(T)];

      if (handler == null)
        return default(T);

      return handler.GetPlaceHolderContent<T>();
    }

    private TeslaContentException CreateContentLoadException(String resourceName, Exception inner)
    {
      if (String.IsNullOrEmpty(resourceName))
        resourceName = "<null>";

      if (inner == null)
        return new TeslaContentException(StringLocalizer.Instance.GetLocalizedString("ContentCouldNotBeLoaded", resourceName));
      else
        return new TeslaContentException(StringLocalizer.Instance.GetLocalizedString("ContentCouldNotBeLoaded", resourceName), inner);
    }

    private Object GetResourceLocker(String resourceName)
    {
      Object obj;
      lock (m_resourceLockers)
      {
        if (!m_resourceLockers.TryGetValue(resourceName, out obj))
        {
          obj = new Object();
          m_resourceLockers.Add(resourceName, obj);
        }
      }

      return obj;
    }

    private IResourceFile ResolveResourceFile(String resourceName)
    {
      IResourceFile file = m_repository.GetResourceFile(resourceName);

      //Look at additional repositories
      if (file == null || !file.Exists)
      {
        foreach (KeyValuePair<IResourceRepository, bool> kv in m_additionalRepositories)
        {
          file = kv.Key.GetResourceFile(resourceName);

          if (file != null && file.Exists)
            break;
        }
      }

      return file;
    }

    private IResourceFile ResolveResourceFile(String resourceName, IResourceFile relativeTo)
    {
      IResourceFile file = m_repository.GetResourceFileRelativeTo(resourceName, relativeTo);

      //Look at additional repositories
      if (file == null || !file.Exists)
      {
        foreach (KeyValuePair<IResourceRepository, bool> kv in m_additionalRepositories)
        {
          file = kv.Key.GetResourceFileRelativeTo(resourceName, relativeTo);

          if (file != null && file.Exists)
            break;
        }
      }

      return file;
    }

    private void CheckDisposed()
    {
      if (m_isDisposed)
        throw new ObjectDisposedException(StringLocalizer.Instance.GetLocalizedString("ObjectDisposedNamed", typeof(ContentManager).Name));
    }

    /// <summary>
    /// Returns an enumerator that iterates through the loaded resources.
    /// </summary>
    /// <returns>An enumerator that can be used to iterate through the loaded resources.</returns>
    public LoadedContentEnumerator GetEnumerator()
    {
      return new LoadedContentEnumerator(this);
    }

    /// <summary>
    /// Returns an enumerator that iterates through the loaded resources.
    /// </summary>
    /// <returns>An enumerator that can be used to iterate through the loaded resources.</returns>
    IEnumerator<Pair<String, Object>> IEnumerable<Pair<String, Object>>.GetEnumerator()
    {
      return new LoadedContentEnumerator(this);
    }

    /// <summary>
    /// Returns an enumerator that iterates through the loaded resources.
    /// </summary>
    /// <returns>An enumerator that can be used to iterate through the loaded resources.</returns>
    IEnumerator IEnumerable.GetEnumerator()
    {
      return new LoadedContentEnumerator(this);
    }

    /// <summary>
    /// Iterates over the loaded resources in a <see cref="ContentManager"/>.
    /// </summary>
    /// <seealso cref="System.Collections.Generic.IEnumerator{Tesla.Pair{System.String, System.Object}}" />
    public struct LoadedContentEnumerator : IEnumerator<Pair<String, Object>>
    {
      private Dictionary<String, Object>.Enumerator m_dictEnumerator;
      private Pair<String, Object> m_current;

      /// <summary>
      /// Gets the current loaded resource.
      /// </summary>
      public Pair<String, Object> Current
      {
        get
        {
          return m_current;
        }
      }

      /// <summary>
      /// Gets the current loaded resource.
      /// </summary>
      Object IEnumerator.Current
      {
        get
        {
          return m_current;
        }
      }

      /// <summary>
      /// Constructs a new instance of the <see cref="LoadedContentEnumerator"/> struct.
      /// </summary>
      /// <param name="contentManager">The content manager.</param>
      internal LoadedContentEnumerator(ContentManager contentManager)
      {
        m_dictEnumerator = contentManager.m_loadedResources.GetEnumerator();
        m_current = new Pair<String, Object>();
      }

      /// <summary>
      /// Advances the enumerator to the next element of the collection.
      /// </summary>
      /// <returns>True if the enumerator was successfully advanced to the next element; false if the enumerator has passed the end of the collection.</returns>
      public bool MoveNext()
      {
        if (m_dictEnumerator.MoveNext())
        {
          KeyValuePair<String, Object> kv = m_dictEnumerator.Current;
          m_current = new Pair<String, Object>(kv.Key, kv.Value);
          return true;
        }

        m_current = new Pair<String, Object>();
        return false;
      }

      /// <summary>
      /// Sets the enumerator to its initial position, which is before the first element in the collection.
      /// </summary>
      void IEnumerator.Reset()
      {
      }

      /// <summary>
      /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
      /// </summary>
      void IDisposable.Dispose()
      {
      }
    }
  }
}
