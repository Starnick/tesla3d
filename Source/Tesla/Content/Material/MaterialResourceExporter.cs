﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.IO;
using System.Text;
using Tesla.Graphics;

namespace Tesla.Content
{
    /// <summary>
    /// A resource exporter that can save <see cref="MaterialScriptCollection"/> and <see cref="Material"/> objects to
    /// the TEM (Tesla Engine Material) script format.
    /// </summary>
    public sealed class MaterialResourceExporter : ResourceExporter<MaterialScriptCollection>
    {
        private Type m_materialType;

        /// <summary>
        /// Constructs a new instance of the <see cref="MaterialResourceExporter"/> class.
        /// </summary>
        public MaterialResourceExporter() 
            : base(".tem") 
        {
            m_materialType = typeof(Material);
        }

        /// <summary>
        /// Returns true if the specified type can be handled by this importer, false otherwise.
        /// </summary>
        /// <param name="contentType">Content type to be loaded.</param>
        /// <returns>True if the type can be loaded by this importer, false otherwise.</returns>
        public override bool CanSaveType(Type contentType)
        {
            return base.CanSaveType(contentType) || m_materialType.IsAssignableFrom(contentType) || contentType.IsAssignableFrom(m_materialType);
        }

        /// <summary>
        /// Writes the specified content to a resource file, as dictated by the user supplied external handler. Use this method
        /// if custom content naming and external writers are required. The savable write flags are also dictated by those on the external
        /// handler.
        /// </summary>
        /// <param name="externalHandler">User-specified external handler</param>
        /// <param name="content">Content to write</param>
        /// <exception cref="ArgumentNullException">Thrown if the external handler is null, its parent resource file, or the content to write.</exception>
        public void Save(IExternalReferenceHandler externalHandler, MaterialScriptCollection content)
        {
            if(externalHandler == null)
                throw new ArgumentNullException("externalHandler");

            ValidateParameters(externalHandler.ParentResourceFile, content);
            Stream output = externalHandler.ParentResourceFile.OpenWrite();

            using(StreamWriter writer = new StreamWriter(output))
            {
                MaterialSerializer serializer = new MaterialSerializer();
                writer.Write(serializer.Serialize(content, externalHandler));
            }

            externalHandler.Flush();
        }

        /// <summary>
        /// Writes the specified content to a resource file.
        /// </summary>
        /// <param name="resourceFile">Resource file to write to</param>
        /// <param name="content">Content to write</param>
        /// <exception cref="ArgumentNullException">Thrown if either the resource file or content are null.</exception>
        public override void Save(IResourceFile resourceFile, MaterialScriptCollection content)
        {
            ValidateParameters(resourceFile, content);

            SavableWriteFlags writeFlags = SavableWriteFlags.OverwriteExistingResource | SavableWriteFlags.UseCompression;
            IExternalReferenceHandler handler = ExternalReferenceHandler.CreateBinaryDefaultHandler(resourceFile, writeFlags);

            Save(handler, content);
        }

        /// <summary>
        /// Writes the specified content to a resource file, as dictated by the user supplied external handler. Use this method
        /// if custom content naming and external writers are required. The savable write flags are also dictated by those on the external
        /// handler.
        /// </summary>
        /// <param name="externalHandler">User-specified external handler</param>
        /// <param name="content">Content to write</param>
        /// <exception cref="ArgumentNullException">Thrown if the external handler is null, its parent resource file, or the content to write.</exception>
        public void Save(IExternalReferenceHandler externalHandler, Material content)
        {
            if(externalHandler == null)
                throw new ArgumentNullException("externalHandler");

            ValidateParameters(externalHandler.ParentResourceFile, content);
            Stream output = externalHandler.ParentResourceFile.OpenWrite();

            using(StreamWriter writer = new StreamWriter(output))
            {
                MaterialSerializer serializer = new MaterialSerializer();
                writer.Write(serializer.Serialize(content, externalHandler));
            }

            externalHandler.Flush();
        }

        /// <summary>
        /// Writes the specified content to a resource file.
        /// </summary>
        /// <param name="resourceFile">Resource file to write to</param>
        /// <param name="content">Content to write</param>
        /// <exception cref="ArgumentNullException">Thrown if either the resource file or content are null.</exception>
        public void Save(IResourceFile resourceFile, Material content)
        {
            ValidateParameters(resourceFile, content);

            SavableWriteFlags writeFlags = SavableWriteFlags.OverwriteExistingResource | SavableWriteFlags.UseCompression;
            IExternalReferenceHandler handler = ExternalReferenceHandler.CreateBinaryDefaultHandler(resourceFile, writeFlags);

            Save(handler, content);
        }

        /// <summary>
        /// Writes the specified content to a stream.
        /// </summary>
        /// <param name="output">Output stream to write to</param>
        /// <param name="content">Content to write</param>
        /// <exception cref="ArgumentNullException">Thrown if output stream/content is null or if the stream is not writeable.</exception>
        public override void Save(Stream output, MaterialScriptCollection content)
        {
            ValidateParameters(output, content);

            using(StreamWriter writer = new StreamWriter(output, Encoding.UTF8, 1024, true))
            {
                MaterialSerializer serializer = new MaterialSerializer();
                writer.Write(serializer.Serialize(content));
            }
        }

        /// <summary>
        /// Writes the specified content to a stream.
        /// </summary>
        /// <param name="output">Output stream to write to</param>
        /// <param name="content">Content to write</param>
        /// <exception cref="ArgumentNullException">Thrown if output stream/content is null or if the stream is not writeable.</exception>
        public void Save(Stream output, Material content)
        {
            ValidateParameters(output, content);

            using(StreamWriter writer = new StreamWriter(output, Encoding.UTF8, 1024, true))
            {
                MaterialSerializer serializer = new MaterialSerializer();
                writer.Write(serializer.Serialize(content));
            }
        }

        private void ValidateParameters(IResourceFile resourceFile, Material content)
        {
            if(resourceFile == null)
                throw new ArgumentNullException("resourceFile");

            if(content == null)
                throw new ArgumentNullException("content");
        }

        private void ValidateParameters(Stream output, Material content)
        {
            if(output == null || !output.CanWrite)
                throw new ArgumentNullException("output", StringLocalizer.Instance.GetLocalizedString("CannotWriteToStream"));

            if(content == null)
                throw new ArgumentNullException("content");
        }
    }
}
