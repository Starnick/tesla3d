﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Text;
using System.Collections.Generic;
using System.IO;
using Tesla.Graphics;

namespace Tesla.Content
{
    /// <summary>
    /// Parser for both the TEM (Tesla Engine Material) and TEMD (Tesla Engine Material Definition) script files.
    /// </summary>
    public sealed class MaterialParser
    {
        private StringTokenizer m_tokenStream;
        
        //Cache for re-using render state and effect instances between materials
        private MaterialParserCache m_cache;
        private ContentManager m_content;
        private MaterialImporterParameters m_matParams;

        //Per material data
        private Dictionary<String, ScriptParameter> m_scriptParameters;
        private Dictionary<String, ComputedScriptParameter> m_computedParameters;
        private Dictionary<String, RenderState> m_renderStates;
        private Dictionary<String, ScriptPass> m_passes;
        private List<ScriptPass> m_passList;
        private Effect m_effect;
        private String m_materialName;
        private IResourceFile m_resourceFile;
        private InheritedValue<ShadowMode> m_shadowMode;
        private InheritedValue<TransparencyMode> m_transparencyMode;
        private bool m_parsingParent; //True when we're currently parsing a parent script, script data will be set to "is inherited"
        private String m_parentInfo; //Name of the closest parent material

        //Error codes
        private const String ERROR_GENERAL = "X000";
        private const String ERROR_SYNTAX = "X010";
        private const String ERROR_PARENT = "X020";
        private const String ERROR_MISSING_NAME = "X030";
        private const String ERROR_FILE_NOT_FOUND = "X040";
        private const String ERROR_TYPE_MISMATCH = "X050";
        private const String ERROR_NOT_DEFINED = "X060";
        private const String ERROR_INVALID = "X070";

        /// <summary>
        /// Gets or sets the material importing parameters.
        /// </summary>
        public MaterialImporterParameters MaterialParameters
        {
            get
            {
                return m_matParams;
            }
            set
            {
                m_matParams = value;
            }
        }

        /// <summary>
        /// Gets or sets the optional resource file representing the material that will be parsed. If set this will
        /// be used to try and load content relative to the material first, then try and load via an absolute path (as defined
        /// by either the file path in the material script or the override texture path importer option).
        /// </summary>
        public IResourceFile MaterialResourceFile
        {
            get
            {
                return m_resourceFile;
            }
            set
            {
                m_resourceFile = value;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MaterialParser"/> class.
        /// </summary>
        public MaterialParser() : this(null) { }

        /// <summary>
        /// Constructs a new instance of the <see cref="MaterialParser"/> class.
        /// </summary>
        /// <param name="cache">A cache for sharing objects between material instances.</param>
        public MaterialParser(MaterialParserCache cache)
        {
            if(cache == null)
                cache = new MaterialParserCache();

            m_cache = cache;
            m_tokenStream = new StringTokenizer();
            m_effect = null;
            m_materialName = "Material";
            m_resourceFile = null;
            m_shadowMode = new InheritedValue<ShadowMode>(ShadowMode.None, false);
            m_transparencyMode = new InheritedValue<TransparencyMode>(TransparencyMode.OneSided, false);
            m_parsingParent = false;
            m_parentInfo = String.Empty;

            m_passes = new Dictionary<String, ScriptPass>();
            m_passList = new List<ScriptPass>();
            m_scriptParameters = new Dictionary<String, ScriptParameter>();
            m_computedParameters = new Dictionary<String, ComputedScriptParameter>();
            m_renderStates = new Dictionary<String, RenderState>();
        }

        #region MaterialDefinition Parsing

        /// <summary>
        /// Parses a material definition collection (which may be just a single material definition) from input text.
        /// </summary>
        /// <param name="matDefScript">Text holding the material definition script(s).</param>
        /// <param name="contentManager">Content manager to load external resources.</param>
        /// <returns>Parsed material definition collection.</returns>
        public MaterialDefinitionScriptCollection ParseDefinitions(String matDefScript, ContentManager contentManager)
        {
            if(String.IsNullOrEmpty(matDefScript) || contentManager == null)
                return null;

            MaterialDefinitionScriptCollection collection = new MaterialDefinitionScriptCollection();

            m_tokenStream.Initialize(matDefScript);
            m_content = contentManager;

            //One dictionary for each mat def in the file
            List<MaterialDefinition> matDefs = new List<MaterialDefinition>();
            List<Dictionary<RenderBucketID, String>> renderBucketToMaterialPathList = new List<Dictionary<RenderBucketID, String>>();
            Dictionary<String, ScriptTextCache> filePathToScriptText = new Dictionary<String, ScriptTextCache>();
            Dictionary<String, Material> filePathToMaterial = new Dictionary<String, Material>();

            bool cloneMaterials = (m_matParams != null) ? !m_matParams.UseSharedInstances : false;

            try
            {
                //Parse ALL of the materials for each material definition as the first step
                while(m_tokenStream.HasNext())
                {
                    Dictionary<RenderBucketID, String> localRenderBucketsToMaterial = new Dictionary<RenderBucketID, String>();
                    MaterialDefinition matDef = ParseSingleDefinition(null, localRenderBucketsToMaterial, filePathToScriptText, filePathToMaterial);
                    if(matDef != null)
                    {
                        matDefs.Add(matDef);
                        renderBucketToMaterialPathList.Add(localRenderBucketsToMaterial);
                    }
                }

                //Now create the materials
                //
                //NOTE:
                //Each unique material is SHARED (e.g. not cloned) between material definitions ONLY in the same file.
                //We may have an importer parameter later on in order to ensure every material is its own instance

                for(int i = 0; i < matDefs.Count; i++)
                {
                    MaterialDefinition matDef = matDefs[i];
                    PopulateMaterialDefinition(matDef, renderBucketToMaterialPathList[i], filePathToScriptText, filePathToMaterial, contentManager, cloneMaterials);

                    if(matDef.Count > 0)
                        collection.Add(matDef);
                }
            }
            finally
            {
                m_tokenStream.Initialize(String.Empty);
                m_content = null;
            }

            return (collection.Count > 0) ? collection : null;
        }

        /// <summary>
        /// Parses a single material definition with the specified name from the input text.
        /// </summary>
        /// <param name="matDefScript">Text holding the material definition script(s).</param>
        /// <param name="matDefName">Name of the material definition to parse.</param>
        /// <param name="contentManager">Content manager to load external resources.</param>
        /// <returns>The parsed definition material, or null if it could not be parsed.</returns>
        public MaterialDefinition ParseDefinition(String matDefScript, String matDefName, ContentManager contentManager)
        {
            if(String.IsNullOrEmpty(matDefScript) || contentManager == null)
                return null;

            m_tokenStream.Initialize(matDefScript);
            m_content = contentManager;

            Dictionary<RenderBucketID, String> renderBucketToMaterialPath = new Dictionary<RenderBucketID, String>();
            Dictionary<String, ScriptTextCache> filePathToScriptText = new Dictionary<String, ScriptTextCache>();
            Dictionary<String, Material> filePathToMaterial = new Dictionary<String, Material>();

            MaterialDefinition matDef = null;

            bool cloneMaterials = (m_matParams != null) ? !m_matParams.UseSharedInstances : false;

            try
            {
                matDef = ParseSingleDefinition(matDefName, renderBucketToMaterialPath, filePathToScriptText, filePathToMaterial);
                
                if(matDef != null)
                    PopulateMaterialDefinition(matDef, renderBucketToMaterialPath, filePathToScriptText, filePathToMaterial, contentManager, cloneMaterials);
            }
            finally
            {
                m_tokenStream.Initialize(String.Empty);
                m_content = null;
            }

            return (matDef != null && matDef.Count > 0) ? matDef : null;
        }

        private MaterialDefinition ParseSingleDefinition(String matchesMatDefName, 
            Dictionary<RenderBucketID, String> renderBucketToMaterialPath, Dictionary<String, ScriptTextCache> filePathToScriptText, 
            Dictionary<String, Material> filePathToMaterial)
        {
            //Yes! Using a goto to avoid recursion, if we're trying to match a material definition name in a script that can contain any number of materials
            //definitions, we're going to keep going to the next material definition and parsing its name to see if it matches, then go to the next one,
            //and keep repeating until we find the specified material definition or we hit end of file, which in that case we return.
            BackToStart:
                String token = m_tokenStream.NextToken();

            if(StringCompare(token, "MaterialDefinition"))
            {
                String matDefName = m_tokenStream.NextToken();
                IsValidIdentifier(matDefName);

                if(!String.IsNullOrEmpty(matchesMatDefName) && !StringCompare(matchesMatDefName, matDefName))
                {
                    //Reached end of file, no material with that name was found
                    if(!GoToNextMaterialDefinition())
                        return null;

                    goto BackToStart;
                }

                //Parse the definition
                ParseOpenBrace();

                while(HasNextStatement())
                    ParseDefinitionStatement(matDefName, renderBucketToMaterialPath, filePathToScriptText);

                ParseCloseBrace();

                return new MaterialDefinition(matDefName);
            }
            else
            {
                ThrowException(ERROR_INVALID, StringLocalizer.Instance.GetLocalizedString("NotAMaterialDefinition"));
            }

            return null;
        }

        private void PopulateMaterialDefinition(MaterialDefinition matDef, Dictionary<RenderBucketID, String> renderBucketToMaterialPath, 
            Dictionary<String, ScriptTextCache> filePathToScriptText, Dictionary<String, Material> filePathToMaterial, ContentManager contentManager, bool cloneMaterials)
        {
            if(matDef == null)
                return;

            //Now go through and create each material as necessary and add it to the material definition
            foreach(KeyValuePair<RenderBucketID, String> kv in renderBucketToMaterialPath)
            {
                //NOTE:
                //The material importer parameters can control if each unique material is SHARED (e.g. not cloned) or not between
                //material definitions in ONLY the same file.
                Material mat;

                //Already exists...just add it to this material def
                if(filePathToMaterial.TryGetValue(kv.Value, out mat))
                {
                    //Might be null...there was an error somewhere
                    if(mat != null)
                    {
                        //First instance of the material in the dictionary should already have been set to whatever material definition first discovered it
                        if(cloneMaterials)
                            matDef.Add(kv.Key, mat.Clone());
                        else
                            matDef.Add(kv.Key, mat);
                    }

                    continue;
                }
                else
                {
                    //The material path may be the keyword "null", so check for it and add a null entry for the material definition
                    if (StringCompare(kv.Value, "null"))
                    {
                        matDef.Add(kv.Key, null);
                        continue;
                    }

                    //Find the material and parse it. We may be referencing multiple materials from the same script file, so cache the text
                    String filePath, matName;
                    ContentHelper.ParseSubresourceName(kv.Value, out filePath, out matName);

                    IResourceFile matDefFile = m_resourceFile;

                    //Read the text in if its the first time
                    ScriptTextCache script;
                    if(!filePathToScriptText.TryGetValue(filePath, out script))
                    {
                        //Allow "StandardLibrary" materials (file paths WITHOUT the .tem extension)
                        if(Path.HasExtension(filePath))
                        {
                            //Try and resolve the resource file relative to the material definition if there is one, otherwise the absolute path will be tried
                            m_resourceFile = contentManager.QueryResourceFile(filePath, matDefFile);
                            if (m_resourceFile == null)
                                m_resourceFile = contentManager.QueryResourceFile(filePath);

                            if (m_resourceFile == null)
                                this.ThrowException(ERROR_FILE_NOT_FOUND, StringLocalizer.Instance.GetLocalizedString("Material_FileNotFound", filePath));

                            using (StreamReader reader = new StreamReader(m_resourceFile.OpenRead(), Encoding.UTF8, true, 1024))
                                script.ScriptText = reader.ReadToEnd();

                            script.NameWithoutExtension = Path.GetFileNameWithoutExtension(filePath);
                        }
                        else
                        {
                            script.ScriptText = StandardMaterialScriptLibrary.GetMaterialScript(filePath);
                            script.NameWithoutExtension = filePath;
                        }

                        //Whether standard lib or a custom material script, this is not an anonymous material
                        script.IsAnonymous = false;

                        filePathToScriptText.Add(filePath, script);
                    }

                    //Parse the material
                    mat = ParseMaterial(script.ScriptText, matName, contentManager);

                    //Ensure the material is valid...
                    if(mat != null && mat.IsValid)
                    {
                        //Set anonymous metadata
                        mat.IsAnonymous = script.IsAnonymous;

                        //If anonymous, don't set a script file name
                        if (!script.IsAnonymous)
                            mat.ScriptFileName = script.NameWithoutExtension;

                        //Add to the material definition
                        matDef.Add(kv.Key, mat);

                        //Add the material to our cache
                        filePathToMaterial.Add(kv.Value, mat);
                    }
                    else
                    {
                        System.Diagnostics.Debug.Assert(false, "Parsed an invalid material."); //Sanity check
                    }

                    //Set the resource file (if we have one) back to the original one we started with
                    m_resourceFile = matDefFile;
                }
            }
        }

        private void ParseDefinitionStatement(String matDefName, Dictionary<RenderBucketID, String> renderBucketToMaterialPath, Dictionary<String, ScriptTextCache> filePathToScriptText)
        {
            String token = m_tokenStream.NextToken();
            RenderBucketID bucketID = RenderBucketID.QueryID(token);

            if(bucketID == RenderBucketID.Invalid)
                ThrowException(ERROR_NOT_DEFINED, StringLocalizer.Instance.GetLocalizedString("Material_InvalidRenderBucketID", token));

            if(renderBucketToMaterialPath.ContainsKey(bucketID))
                ThrowException(ERROR_GENERAL, StringLocalizer.Instance.GetLocalizedString("Material_RenderBucketAlreadyDefined", token));

            bool isAnonMaterial;
            ParseDefinitionSeparator(out isAnonMaterial);

            if(!isAnonMaterial)
            {
                String materialFilePath = m_tokenStream.NextToken();

                renderBucketToMaterialPath.Add(bucketID, materialFilePath);
            }
            else
            {
                //Build up a complete material script from the partial script
                StringBuilder matScriptBuilder = new StringBuilder();
                String matName = String.Format("{0}_{1}", matDefName, token);
                matScriptBuilder.Append("Material ");
                matScriptBuilder.Append(matName);
                matScriptBuilder.Append(" ");

                //If curly brace, then no parent material
                if(!m_tokenStream.HasNextStartsWith("{"))
                {
                    //Otherwise, we need to insert a separator 
                    matScriptBuilder.Append(": ");
                }

                BracePairCounter pairCounter;
                if(!BracePairCounter.FindFirstOpenBrace(m_tokenStream.CurrentPosition, m_tokenStream.Data, out pairCounter))
                    ThrowException(ERROR_SYNTAX, StringLocalizer.Instance.GetLocalizedString("Material_MissingOpenBrace"));

                int endIndex = pairCounter.IndexOfLastClosingBrace();
                if(endIndex == -1)
                    ThrowException(ERROR_SYNTAX, StringLocalizer.Instance.GetLocalizedString("Material_MissingCloseBrace"));

                String partialScript = m_tokenStream.Data.Substring(m_tokenStream.CurrentPosition, (endIndex - m_tokenStream.CurrentPosition) + 1);
                if(String.IsNullOrEmpty(partialScript) || !m_tokenStream.HasNext())
                    ThrowException(ERROR_SYNTAX, StringLocalizer.Instance.GetLocalizedString("Material_InvalidDefinition"));

                matScriptBuilder.Append(partialScript);

                renderBucketToMaterialPath.Add(bucketID, matName);

                //Anonymous materials do not have a script file name, a serializer may group them into a single material script, but we save the metadata
                //now.
                ScriptTextCache cache = new ScriptTextCache(matScriptBuilder.ToString(), String.Empty, isAnonMaterial);
                filePathToScriptText.Add(matName, cache);

                //Put the token stream onto right before the last curly brace and put that as the next token, parsing should continue normally after
                //skipping the partial material script
                m_tokenStream.CurrentPosition = endIndex;
                m_tokenStream.NextToken();
            }
        }

        private void ParseDefinitionSeparator(out bool isAnonymousMaterial)
        {
            String nextToken = m_tokenStream.NextToken();
            isAnonymousMaterial = false;

            if(nextToken.Equals(":"))
            {
                isAnonymousMaterial = false;
            }
            else if(nextToken.Equals("=>"))
            {
                isAnonymousMaterial = true;
            }
            else
            {
                ThrowException(ERROR_SYNTAX, StringLocalizer.Instance.GetLocalizedString("Material_InvalidDefinition"));
            }
        }

        private struct BracePairCounter
        {
            private int m_count;
            private int m_startIndex;
            private String m_target;

            public BracePairCounter(int startIndex, String text)
            {
                m_count = 0;
                m_startIndex = startIndex;
                m_target = text;
            }

            public static bool FindFirstOpenBrace(int startIndex, String text, out BracePairCounter pair)
            {
                for(int i = startIndex; i < text.Length; i++)
                {
                    if(text[i] == '{')
                    {
                        pair = new BracePairCounter(i, text);
                        return true;
                    }
                }

                pair = new BracePairCounter();
                return false;
            }

            public int IndexOfLastClosingBrace()
            {
                for(int i = m_startIndex; i < m_target.Length; i++)
                {
                    char c = m_target[i];

                   if(c == '{')
                    {
                        m_count++;
                    }
                    else if (c == '}')
                    {
                        m_count--;

                        if(m_count == 0)
                        {
                            return i;
                        }
                    }
                }

                return -1;
            }
        }

        #endregion

        #region Material Parsing

        /// <summary>
        /// Parses a material collection (which may be just a single material) from input text.
        /// </summary>
        /// <param name="matScript">Text holding the material script(s).</param>
        /// <param name="contentManager">Content manager to load external resources.</param>
        /// <returns>Parsed material collection, or null if no materials were found.</returns>
        public MaterialScriptCollection ParseMaterials(String matScript, ContentManager contentManager)
        {
            if(String.IsNullOrEmpty(matScript) || contentManager == null)
                return null;

            MaterialScriptCollection collection = new MaterialScriptCollection();

            m_tokenStream.Initialize(matScript);
            m_content = contentManager;

            try
            {
                while(m_tokenStream.HasNext())
                {
                    Material mat = ParseSingleMaterial(null);

                    if(mat != null && mat.IsValid)
                        collection.Add(mat);
                }
            }
            finally
            {
                m_tokenStream.Initialize(String.Empty);
                m_content = null;
                m_materialName = "Material";
            }

            return (collection.Count > 0) ? collection : null;
        }

        /// <summary>
        /// Parses a single material with the specified name from the input text.
        /// </summary>
        /// <param name="matScript">Text holding the material script(s).</param>
        /// <param name="matName">Optional name of the material to parse. If null, then the first material in the script is returned.</param>
        /// <param name="contentManager">Content manager to load external resources.</param>
        /// <returns>The parsed material, or null if it could not be parsed.</returns>
        public Material ParseMaterial(String matScript, String matName, ContentManager contentManager)
        {
            if(String.IsNullOrEmpty(matScript) || contentManager == null)
                return null;

            m_tokenStream.Initialize(matScript);
            m_content = contentManager;

            Material material = null;

            try
            {
                material = ParseSingleMaterial(matName);
            }
            finally
            {
                m_tokenStream.Initialize(String.Empty);
                m_content = null;
                m_materialName = "Material";
            }

            return (material != null && material.IsValid) ? material : null;
        }

        /// <summary>
        /// Parses all the valid names contained in a material (.tem) or material definition (.temd) script.
        /// </summary>
        /// <param name="matScript">Material or material definition script text.</param>
        /// <param name="matNames">List of names.</param>
        /// <returns>True if any names were found, false otherwise.</returns>
        public bool ParseNamesFromScript(String matScript, List<String> matNames)
        {
            if (String.IsNullOrEmpty(matScript) || matNames == null)
                return false;

            m_tokenStream.Initialize(matScript);
            bool foundOne = false;
            bool? isMatDef = null;
            bool? isMat = null;

            try
            {
                while (m_tokenStream.HasNext())
                {
                    String token = m_tokenStream.NextToken();
                    bool haveOne = false;

                    if((isMat ?? true) && StringCompare(token, "Material"))
                    {
                        isMat = true;
                        isMatDef = false;
                        haveOne = true;
                    }
                    else if((isMatDef ?? true) && StringCompare(token, "MaterialDefinition"))
                    {
                        isMat = false;
                        isMatDef = true;
                        haveOne = true;
                    }

                    if(haveOne && m_tokenStream.HasNext())
                    {
                        String matName = m_tokenStream.NextToken();
                        if (IsValidIdentifier(matName, false))
                        {
                            foundOne = true;
                            matNames.Add(matName);
                        }
                    }
                }
            }
            finally
            {
                m_tokenStream.Initialize(String.Empty);
            }

            return foundOne;
        }

        private Material ParseSingleMaterial(String matchesMatName, bool createMaterial = true)
        {
            //Yes! Using a goto to avoid recursion, if we're trying to match a material name in a script that can contain any number of materials,
            //we're going to keep going to the next material and parsing its name to see if it matches, then go to the next material and keep repeating
            //until we find the specified material or we hit end of file, which in that case we return.
            BackToStart:
                String token = m_tokenStream.NextToken();

            if(StringCompare(token, "Material"))
            {
                String matName = m_tokenStream.NextToken();
                IsValidIdentifier(matName);

                if(!String.IsNullOrEmpty(matchesMatName) && !StringCompare(matchesMatName, matName))
                {
                    //Reached end of file, no material with that name was found
                    if(!GoToNextMaterial())
                        return null;

                    goto BackToStart;
                }

                m_materialName = matName;

                String nextToken = m_tokenStream.NextToken();

                if(StringCompare(nextToken, ":"))
                {
                    //When parsing parent info, we first parse the parent material to gather all the information about it, then we parse the child material,
                    //which will override bindings properties from the parent, or add new information.
                    String parentInfo = m_tokenStream.NextToken();

                    //If have parent info, and not currently parsing a parent, then lets log the name
                    if (!m_parsingParent && !String.IsNullOrEmpty(parentInfo))
                        m_parentInfo = parentInfo;

                    ParseParentMaterial(parentInfo);

                    nextToken = m_tokenStream.NextToken();
                    if(StringCompare(nextToken, "{"))
                    {
                        ParseBlocks();
                        ParseCloseBrace();
                    }
                    else
                    {
                        ThrowException(ERROR_SYNTAX, StringLocalizer.Instance.GetLocalizedString("Material_GeneralError", nextToken));
                    }
                }
                else if(StringCompare(nextToken, "{"))
                {
                    ParseBlocks();
                    ParseCloseBrace();
                }
                else
                {
                    ThrowException(ERROR_SYNTAX, StringLocalizer.Instance.GetLocalizedString("Material_GeneralError", nextToken));
                }
            }
            else
            {
                ThrowException(ERROR_SYNTAX, StringLocalizer.Instance.GetLocalizedString("Material_ExpectingKeyword", "Material", token));
            }

            if(createMaterial)
            {
                //Current material is fully parsed, now populate a material object with it and clear the current data
                Material mat = PopulateMaterialFromTransientData();
                ClearTransientData();

                return mat;
            }
            else
            {
                return null;
            }
        }

        private bool GoToNextMaterial()
        {
            while(!m_tokenStream.HasNext("Material", StringComparison.InvariantCultureIgnoreCase))
            {
                m_tokenStream.NextToken();

                //If EOF then return false
                if(!m_tokenStream.HasNext())
                    return false;
            }

            return true;
        }

        private bool GoToNextMaterialDefinition()
        {
            while(!m_tokenStream.HasNext("MaterialDefinition", StringComparison.InvariantCultureIgnoreCase))
            {
                m_tokenStream.NextToken();

                //If EOF then return false
                if(!m_tokenStream.HasNext())
                    return false;
            }

            return true;
        }

        private Material PopulateMaterialFromTransientData()
        {
            if(m_effect == null)
                return null;

            Material mat = new Material((String.IsNullOrEmpty(m_materialName)) ? "Material" : m_materialName, m_effect);

            if (!String.IsNullOrEmpty(m_parentInfo))
                mat.ParentScriptName = m_parentInfo;

            mat.ShadowMode = m_shadowMode;
            mat.TransparencyMode = m_transparencyMode;

            SetupBindings(mat);
            SetupPasses(mat);

            return mat;
        }

        private void SetupBindings(Material mat)
        {
            ImporterParameters texParams = (m_matParams != null) ? m_matParams.TextureParameters : ImporterParameters.None;

            //Make a check if have a texture override path
            bool hasOverridePath = false;
            String overridePath = (m_matParams != null) ? m_matParams.OverrideTexturePath : null;
            if (!String.IsNullOrEmpty(overridePath))
                hasOverridePath = true;

            foreach (KeyValuePair<String, ScriptParameter> kv in m_scriptParameters)
            {
                //Bind the parameter
                Material.ScriptParameterBinding binding;
                if(!mat.SetParameterBinding(kv.Key, kv.Value.Data.DataType, out binding))
                {
                    ThrowException(ERROR_NOT_DEFINED, StringLocalizer.Instance.GetLocalizedString("ParameterDoesNotExist", kv.Key));
                }
                else
                {
                    //Set if inherited
                    binding.IsInherited = kv.Value.IsInherited;

                    //If it is a resource, we deferred loading until the very end. So now load the texture
                    ResourceParameterData resourceData = kv.Value.Data as ResourceParameterData;
                    if(resourceData != null)
                        resourceData.Load(m_content, (!hasOverridePath) ? m_resourceFile : null, texParams);

                    //Set initial data
                    IEffectParameter parameter = binding.Parameter;
                    if(parameter != null)
                        kv.Value.Data.SetData(parameter);
                }
            }

            foreach(KeyValuePair<String, ComputedScriptParameter> kv in m_computedParameters)
            {
                Material.ComputedParameterBinding binding;
                if(!mat.SetComputedParameterBinding(kv.Key, kv.Value.Provider, out binding))
                    ThrowException(ERROR_NOT_DEFINED, StringLocalizer.Instance.GetLocalizedString("ParameterDoesNotExist", kv.Key));

                //Set if inherited
                binding.IsInherited = kv.Value.IsInherited;
            }
        }

        private void SetupPasses(Material mat)
        {
            if(m_passList.Count == 0)
                ThrowException(ERROR_NOT_DEFINED, StringLocalizer.Instance.GetLocalizedString("Material_MustHaveOnePass"));

            //Materials don't really use the "CurrentShaderGroup" bit on the Effect, maybe it'll go away, but for completeness, set it to the first pass shader group
            m_effect.SetCurrentShaderGroup(m_passList[0].ShaderGroupName);

            for(int i = 0; i < m_passList.Count; i++)
            {
                ScriptPass pass = m_passList[i];

                IEffectShaderGroup shaderGroup = mat.Effect.ShaderGroups[pass.ShaderGroupName];
                if(shaderGroup == null)
                    ThrowException(ERROR_NOT_DEFINED, StringLocalizer.Instance.GetLocalizedString("Material_ShaderGroupNotFound", (pass.ShaderGroupName == null) ? "<MISSING-ShaderGroupName>" : pass.ShaderGroupName));

                MaterialPass matPass = mat.Passes.Add(pass.Name, shaderGroup);
                matPass.IsInherited = pass.IsInherited;

                //Set any none-default render states if there are any. By default, each pass is configured to always set the default render states
                if(pass.HasRenderStates)
                {
                    if(!String.IsNullOrEmpty(pass.BlendStateName))
                        matPass.BlendState = GetPredefinedOrCachedBlendState(pass.BlendStateName);

                    if(!String.IsNullOrEmpty(pass.RasterizerStateName))
                        matPass.RasterizerState = GetPredefinedOrCachedRasterizerState(pass.RasterizerStateName);

                    if(!String.IsNullOrEmpty(pass.DepthStencilStateName))
                        matPass.DepthStencilState = GetPredefinedOrCachedDepthStencilState(pass.DepthStencilStateName);
                }
            }
        }

        private void ClearTransientData()
        {
            m_scriptParameters.Clear();
            m_computedParameters.Clear();
            m_renderStates.Clear();
            m_passes.Clear();
            m_passList.Clear();
            m_effect = null;
            m_materialName = "Material";
            m_resourceFile = null;
            m_shadowMode = new InheritedValue<ShadowMode>(ShadowMode.None, false);
            m_transparencyMode = new InheritedValue<TransparencyMode>(TransparencyMode.OneSided, false);
            m_parentInfo = String.Empty;
            m_parsingParent = false;
        }

        private void ParseParentMaterial(String parentMaterialInfo)
        {
            String filePath, matName;
            ContentHelper.ParseSubresourceName(parentMaterialInfo, out filePath, out matName);

            String matScriptText = null;

            //A parent string in the form of "::mat_name" points to a material name in the same script file
            if(String.IsNullOrEmpty(filePath) && !String.IsNullOrEmpty(matName))
            {
                matScriptText = m_tokenStream.Data;
            }
            //A parent string in the form of "mat_file::mat_name" where mat_file does NOT have an extension points
            //to a standard material. mat_name is optional
            else if(!Path.HasExtension(filePath))
            {
                matScriptText = StandardMaterialScriptLibrary.GetMaterialScript(filePath);

                //If standard library material couldnt be found...error
                if(String.IsNullOrEmpty(matScriptText))
                    ThrowException(ERROR_FILE_NOT_FOUND, StringLocalizer.Instance.GetLocalizedString("Material_FileNotFound", filePath));
            }
            //A parent string in form of "mat_file.tem::mat_name" points to a user defined material script file. mat_name
            //is again optional, this is the most common use case I think.
            else
            {
                try
                {
                    using(StreamReader strReader = new StreamReader(m_content.OpenStream(filePath)))
                    {
                        matScriptText = strReader.ReadToEnd();
                    }
                }
                catch(Exception e)
                {
                    ThrowException(ERROR_FILE_NOT_FOUND, StringLocalizer.Instance.GetLocalizedString("Material_FileNotFound", filePath) + " \n " + e.Message);
                }
            }

            if(String.IsNullOrEmpty(matScriptText))
                return; //Maybe emit a warning?

            StringTokenizer parentTokenizer = new StringTokenizer(matScriptText);
            StringTokenizer oldTokenizer = m_tokenStream;
            String oldMatName = m_materialName;
            bool oldParsingParent = m_parsingParent; //Might be recursively parsing a few parents, so after we're done with this we may still be in another parent
            m_tokenStream = parentTokenizer;
            m_parsingParent = true;

            try
            {
                //Recursively parse the material, before we parse each material it's parent will be parsed until we hit the topmost parent.
                ParseSingleMaterial(matName, false);
            }
            finally
            {
                m_parsingParent = oldParsingParent;
                m_tokenStream = oldTokenizer;
                m_materialName = oldMatName;

                //Only a parent material can create the effect, so if we recursively parse parents and still don't have an effect, we didn't find the stuff
                if(m_effect == null)
                    ThrowException(ERROR_NOT_DEFINED, StringLocalizer.Instance.GetLocalizedString("Material_ParentMaterialNotFound", matName, filePath));
            }
        }

        private void ParseBlocks()
        {
            while(HasNextStatement())
            {
                String token = m_tokenStream.NextToken();

                if(StringCompare(token, "Properties"))
                {
                    ParsePropertiesBlock();
                }
                else if(StringCompare(token, "ParameterBindings"))
                {
                    ParseParameterBindingBlock();
                }
                else if(StringCompare(token, "ComputedParameterBindings"))
                {
                    ParseComputedParameterBindingBlock();
                }
                else if(StringCompare(token, "RenderStates"))
                {
                    ParseRenderStateBlock();
                }
                else if(StringCompare(token, "Pass"))
                {
                    ParsePass();
                }
                else
                {
                    ThrowException(ERROR_SYNTAX, StringLocalizer.Instance.GetLocalizedString("Material_BlockKeywordInvalid", token));
                }
            }
        }

        #endregion

        #region Properties Parsing

        //Parses a complete Properties { } block, which includes loading the effect
        private void ParsePropertiesBlock()
        {
            ParseOpenBrace();

            String filePath = String.Empty;

            while(HasNextStatement())
            {
                String token = m_tokenStream.NextToken();
                ParseSeparator();

                if(StringCompare(token, "Effect"))
                {
                    filePath = m_tokenStream.NextToken();
                }
                else if(StringCompare(token, "ShadowMode"))
                {
                    m_shadowMode = new InheritedValue<ShadowMode>(ParseShadowMode(m_tokenStream.NextToken()), m_parsingParent);
                }
                else if(StringCompare(token, "TransparencyMode"))
                {
                    m_transparencyMode = new InheritedValue<TransparencyMode>(ParseTransparencyMode(m_tokenStream.NextToken()), m_parsingParent);
                }
                else
                {
                    ThrowException(ERROR_SYNTAX, StringLocalizer.Instance.GetLocalizedString("Material_GeneralError", token));
                }
            }

            ParseCloseBrace();

            //If already have an effect, inherited from earlier parse pass, spit out an error because the user should know this isnt supported at all
            if(m_effect != null)
                ThrowException(ERROR_INVALID, StringLocalizer.Instance.GetLocalizedString("Material_EffectOverrideNotSupported"));

            if(String.IsNullOrEmpty(filePath))
                ThrowException(ERROR_INVALID, StringLocalizer.Instance.GetLocalizedString("Material_EffectInvalid", filePath));

            //Load effect
            try
            {
                m_effect = m_cache.GetCachedEffect(filePath, m_resourceFile, m_content, (m_matParams != null) ? m_matParams.EffectParameters : null);
            }
            catch(TeslaContentException e)
            {
                ThrowException(ERROR_FILE_NOT_FOUND, StringLocalizer.Instance.GetLocalizedString("Material_FileNotFound", e.Message));
            }
            catch(InvalidCastException e)
            {
                ThrowException(ERROR_TYPE_MISMATCH, StringLocalizer.Instance.GetLocalizedString("Material_CouldNotLoadResource", e.Message));
            }
            catch(Exception e)
            {
                ThrowException(ERROR_GENERAL, StringLocalizer.Instance.GetLocalizedString("Material_GeneralError", e.Message));
            }

            if(m_effect == null)
                ThrowException(ERROR_INVALID, StringLocalizer.Instance.GetLocalizedString("Material_EffectInvalid", filePath));
        }

        private ShadowMode ParseShadowMode(String text)
        {
            ShadowMode mode = ShadowMode.None;

            if(StringCompare(text, "Cast"))
            {
                mode = ShadowMode.Cast;
            }
            else if(StringCompare(text, "Receive"))
            {
                mode = ShadowMode.Receive;
            }
            else if(StringCompare(text, "CastAndReceive"))
            {
                mode = ShadowMode.CastAndReceive;
            }

            return mode;
        }

        private TransparencyMode ParseTransparencyMode(String text)
        {
            TransparencyMode transMode = TransparencyMode.OneSided;

            if(StringCompare(text, "TwoSided"))
                transMode = TransparencyMode.TwoSided;

            return transMode;
        }

        #endregion

        #region Parameter Binding Parsing

        private void ParseParameterBindingBlock()
        {
            ParseOpenBrace();

            while(HasNextStatement())
                ParseParameterBinding();

            ParseCloseBrace();
        }

        private void ParseParameterBinding()
        {
            String token = m_tokenStream.NextToken();

            if(StringCompare(token, "int") || StringCompare(token, "uint"))
            {
                ParseInt(1);
            }
            else if(StringCompare(token, "Int2"))
            {
                ParseInt(2);
            }
            else if(StringCompare(token, "Int3"))
            {
                ParseInt(3);
            }
            else if(StringCompare(token, "Int4"))
            {
                ParseInt(4);
            }
            else if(StringCompare(token, "bool"))
            {
                ParseBool(1);
            }
            else if(StringCompare(token, "Bool2"))
            {
                ParseBool(2);
            }
            else if(StringCompare(token, "Bool3"))
            {
                ParseBool(3);
            }
            else if(StringCompare(token, "Bool4"))
            {
                ParseBool(4);
            }
            else if(StringCompare(token, "float"))
            {
                ParseFloat(1);
            }
            else if(StringCompare(token, "Vector2") || StringCompare(token, "Float2"))
            {
                ParseFloat(2);
            }
            else if(StringCompare(token, "Vector3") || StringCompare(token, "Float3"))
            {
                ParseFloat(3);
            }
            else if(StringCompare(token, "Vector4") || StringCompare(token, "Quaternion") || StringCompare(token, "Float4"))
            {
                ParseFloat(4);
            }
            else if(StringCompare(token, "Matrix"))
            {
                ParseMatrix();
            }
            else if(StringCompare(token, "Color"))
            {
                ParseColor(false);
            }
            else if(StringCompare(token, "ColorBGRA"))
            {
                ParseColor(true);
            }
            else if(StringCompare(token, "Texture1D"))
            {
                ParseTexture(TextureDimension.One, false);
            }
            else if(StringCompare(token, "Texture1DArray"))
            {
                ParseTexture(TextureDimension.One, true);
            }
            else if(StringCompare(token, "Texture2D"))
            {
                ParseTexture(TextureDimension.Two, false);
            }
            else if(StringCompare(token, "Texture2DArray"))
            {
                ParseTexture(TextureDimension.Two, true);
            }
            else if(StringCompare(token, "Texture3D"))
            {
                ParseTexture(TextureDimension.Three, false);
            }
            else if(StringCompare(token, "TextureCube"))
            {
                ParseTexture(TextureDimension.Cube, false);
            }
            else if(StringCompare(token, "SamplerState"))
            {
                ParseSampler();
            }
            else
            {
                ThrowException(ERROR_INVALID, StringLocalizer.Instance.GetLocalizedString("Material_ParameterTypeInvalid"));
            }
        }

        //Parses N ints, in format: x y z w
        private void ParseInt(int numComponents)
        {
            String paramName = m_tokenStream.NextToken();
            IsValidIdentifier(paramName);
            ParseSeparator();

            try
            {
                ScriptParameter scriptParam = new ScriptParameter();

                switch(numComponents)
                {
                    case 1:
                        {
                            int value = m_tokenStream.NextInt();
                            scriptParam = new ScriptParameter(paramName, new ValueParameterData<int>(value), m_parsingParent);
                        }
                        break;
                    case 2:
                        {
                            Int2 v = new Int2(m_tokenStream.NextInt(), m_tokenStream.NextInt());
                            scriptParam = new ScriptParameter(paramName, new ValueParameterData<Int2>(v), m_parsingParent);
                        }
                        break;
                    case 3:
                        {
                            Int3 v = new Int3(m_tokenStream.NextInt(), m_tokenStream.NextInt(), m_tokenStream.NextInt());
                            scriptParam = new ScriptParameter(paramName, new ValueParameterData<Int3>(v), m_parsingParent);
                        }
                        break;
                    case 4:
                        {
                            Int4 v = new Int4(m_tokenStream.NextInt(), m_tokenStream.NextInt(), m_tokenStream.NextInt(), m_tokenStream.NextInt());
                            scriptParam = new ScriptParameter(paramName, new ValueParameterData<Int4>(v), m_parsingParent);
                        }
                        break;
                    default:
                        ThrowException(ERROR_TYPE_MISMATCH, StringLocalizer.Instance.GetLocalizedString("Material_UnableToParseBool"));
                        break;
                }

                //Don't check if already is contained, always overwrite
                m_scriptParameters[paramName] = scriptParam;
            }
            catch(Exception e)
            {
                ThrowException(ERROR_TYPE_MISMATCH, StringLocalizer.Instance.GetLocalizedString("Material_UnableToParseBool") + " \n " + e.Message);
            }
        }

        //Parses N bools, in format: x y z w
        private void ParseBool(int numComponents)
        {
            String paramName = m_tokenStream.NextToken();
            IsValidIdentifier(paramName);
            ParseSeparator();

            try
            {
                ScriptParameter scriptParam = new ScriptParameter();

                switch(numComponents)
                {
                    case 1:
                        {
                            bool value = m_tokenStream.NextBool();
                            scriptParam = new ScriptParameter(paramName, new ValueParameterData<Bool>(value), m_parsingParent);
                        }
                        break;
                    case 2:
                        {
                            Bool2 v = new Bool2(m_tokenStream.NextBool(), m_tokenStream.NextBool());
                            scriptParam = new ScriptParameter(paramName, new ValueParameterData<Bool2>(v), m_parsingParent);
                        }
                        break;
                    case 3:
                        {
                            Bool3 v = new Bool3(m_tokenStream.NextBool(), m_tokenStream.NextBool(), m_tokenStream.NextBool());
                            scriptParam = new ScriptParameter(paramName, new ValueParameterData<Bool3>(v), m_parsingParent);
                        }
                        break;
                    case 4:
                        {
                            Bool4 v = new Bool4(m_tokenStream.NextBool(), m_tokenStream.NextBool(), m_tokenStream.NextBool(), m_tokenStream.NextBool());
                            scriptParam = new ScriptParameter(paramName, new ValueParameterData<Bool4>(v), m_parsingParent);
                        }
                        break;
                    default:
                        ThrowException(ERROR_TYPE_MISMATCH, StringLocalizer.Instance.GetLocalizedString("Material_UnableToParseBool"));
                        break;
                }

                //Don't check if already is contained, always overwrite
                m_scriptParameters[paramName] = scriptParam;
            }
            catch(Exception e)
            {
                ThrowException(ERROR_TYPE_MISMATCH, StringLocalizer.Instance.GetLocalizedString("Material_UnableToParseBool") + " \n " + e.Message);
            }
        }

        //Parses N floats, in format: x y z w
        private void ParseFloat(int numComponents)
        {
            String paramName = m_tokenStream.NextToken();
            IsValidIdentifier(paramName);
            ParseSeparator();

            try
            {
                ScriptParameter scriptParam = new ScriptParameter();

                switch(numComponents)
                {
                    case 1:
                        {
                            float value = m_tokenStream.NextSingle();
                            scriptParam = new ScriptParameter(paramName, new ValueParameterData<float>(value), m_parsingParent);
                        }
                        break;
                    case 2:
                        {
                            Vector2 v = new Vector2(m_tokenStream.NextSingle(), m_tokenStream.NextSingle());
                            scriptParam = new ScriptParameter(paramName, new ValueParameterData<Vector2>(v), m_parsingParent);
                        }
                        break;
                    case 3:
                        {
                            Vector3 v = new Vector3(m_tokenStream.NextSingle(), m_tokenStream.NextSingle(), m_tokenStream.NextSingle());
                            scriptParam = new ScriptParameter(paramName, new ValueParameterData<Vector3>(v), m_parsingParent);
                        }
                        break;
                    case 4:
                        {
                            Vector4 v = new Vector4(m_tokenStream.NextSingle(), m_tokenStream.NextSingle(), m_tokenStream.NextSingle(), m_tokenStream.NextSingle());
                            scriptParam = new ScriptParameter(paramName, new ValueParameterData<Vector4>(v), m_parsingParent);
                        }
                        break;
                    default:
                        ThrowException(ERROR_TYPE_MISMATCH, StringLocalizer.Instance.GetLocalizedString("Material_UnableToParseFloat"));
                        break;
                }

                //Don't check if already is contained, always overwrite
                m_scriptParameters[paramName] = scriptParam;
            }
            catch(Exception e)
            {
                ThrowException(ERROR_TYPE_MISMATCH, StringLocalizer.Instance.GetLocalizedString("Material_UnableToParseFloat") + " \n " + e.Message);
            }
        }

        //Parses a 4x4 row major matrix, in format: row1 row2 row 3 row4 where each row are four floats in the format x y z w
        private void ParseMatrix()
        {
            String paramName = m_tokenStream.NextToken();
            IsValidIdentifier(paramName);
            ParseSeparator();

            try
            {
                Vector4 row1 = new Vector4(m_tokenStream.NextSingle(), m_tokenStream.NextSingle(), m_tokenStream.NextSingle(), m_tokenStream.NextSingle());
                Vector4 row2 = new Vector4(m_tokenStream.NextSingle(), m_tokenStream.NextSingle(), m_tokenStream.NextSingle(), m_tokenStream.NextSingle());
                Vector4 row3 = new Vector4(m_tokenStream.NextSingle(), m_tokenStream.NextSingle(), m_tokenStream.NextSingle(), m_tokenStream.NextSingle());
                Vector4 row4 = new Vector4(m_tokenStream.NextSingle(), m_tokenStream.NextSingle(), m_tokenStream.NextSingle(), m_tokenStream.NextSingle());

                //Don't check if already is contained, always overwrite
                m_scriptParameters[paramName] = new ScriptParameter(paramName, new MatrixParameterData(new Matrix(row1, row2, row3, row4)), m_parsingParent);
            }
            catch(Exception e)
            {
                ThrowException(ERROR_TYPE_MISMATCH, StringLocalizer.Instance.GetLocalizedString("Material_UnableToParseFloat") + " \n " + e.Message);
            }
        }

        //Parses a Color, in either RGBA or BGRA format where each component is an int
        private void ParseColor(bool inBGRAFormat)
        {
            String paramName = m_tokenStream.NextToken();
            IsValidIdentifier(paramName);
            ParseSeparator();

            try
            {
                //If BGRA format, r and b really are b and r respectively
                float r = m_tokenStream.NextInt();
                float g = m_tokenStream.NextInt();
                float b = m_tokenStream.NextInt();
                float a = m_tokenStream.NextInt();

                ScriptParameter scriptParam = 
                    (inBGRAFormat) 
                    ? new ScriptParameter(paramName, new ValueParameterData<ColorBGRA>(new ColorBGRA(r, g, b, a)), m_parsingParent)
                    : new ScriptParameter(paramName, new ValueParameterData<Color>(new Color(r, g, b, a)), m_parsingParent);

                //Don't check if already is contained, always overwrite
                m_scriptParameters[paramName] = scriptParam;
            }
            catch(Exception e)
            {
                ThrowException(ERROR_TYPE_MISMATCH, StringLocalizer.Instance.GetLocalizedString("Material_UnableToParseInt") + " \n " + e.Message);
            }
        }

        //Parses a texture, valid separates are / or \\ but not // since it's the start of a comment.
        private void ParseTexture(TextureDimension texDims, bool isArray)
        {
            String paramName = m_tokenStream.NextToken();
            IsValidIdentifier(paramName);
            ParseSeparator();

            String filePath = m_tokenStream.NextToken();

            //If the token matches the "null" keyword, then we don't try loading it. Set it to null and create the binding.
            if (StringCompare(filePath, "null"))
            {
                filePath = null;
            }
            else
            {
                //Defer texture loading so we don't load up any textures in parents that are overritten
                String overridePath = (m_matParams != null) ? m_matParams.OverrideTexturePath : null;
                if (!String.IsNullOrEmpty(overridePath))
                    filePath = Path.Combine(overridePath, Path.GetFileName(filePath));
            }

            //Don't check if already is contained, always overwrite
            switch(texDims)
            {
                case TextureDimension.One:
                    {
                        if(isArray)
                        {
                            m_scriptParameters[paramName] = new ScriptParameter(paramName, CreateTextureParameterData<Texture1DArray>(filePath), m_parsingParent);
                        }
                        else
                        {
                            m_scriptParameters[paramName] = new ScriptParameter(paramName, CreateTextureParameterData<Texture1D>(filePath), m_parsingParent);
                        }
                    }
                    break;
                case TextureDimension.Two:
                    {
                        if(isArray)
                        {
                            m_scriptParameters[paramName] = new ScriptParameter(paramName, CreateTextureParameterData<Texture2DArray>(filePath), m_parsingParent);
                        }
                        else
                        {
                            m_scriptParameters[paramName] = new ScriptParameter(paramName, CreateTextureParameterData<Texture2D>(filePath), m_parsingParent);
                        }
                    }
                    break;
                case TextureDimension.Three:
                    {
                        if(!isArray)
                        {
                            m_scriptParameters[paramName] = new ScriptParameter(paramName, CreateTextureParameterData<Texture3D>(filePath), m_parsingParent);
                        }
                    }
                    break;
                case TextureDimension.Cube:
                    {
                        if(!isArray)
                        {
                            m_scriptParameters[paramName] = new ScriptParameter(paramName, CreateTextureParameterData<TextureCube>(filePath), m_parsingParent);
                        }
                    }
                    break;
            }
        }

        //Parses a sampler state binding (name only, SS defined in the renderstate block)
        private void ParseSampler()
        {
            String paramName = m_tokenStream.NextToken();
            IsValidIdentifier(paramName);
            ParseSeparator();

            String samplerStateID = m_tokenStream.NextToken();

            //Don't check if already is contained, always overwrite
            m_scriptParameters[paramName] = new ScriptParameter(paramName, new SamplerParameterData(samplerStateID, this), m_parsingParent);
        }

        private ResourceParameterData<T> CreateTextureParameterData<T>(String filePath) where T : Texture
        {
            return new ResourceParameterData<T>(filePath, m_materialName, m_tokenStream.LineNumber.ToString(), m_tokenStream.Column.ToString());
        }

        #endregion

        #region Computed Parameter Parsing

        private void ParseComputedParameterBindingBlock()
        {
            ParseOpenBrace();

            while(HasNextStatement())
                ParseComputedParameterBinding();

            ParseCloseBrace();
        }

        private void ParseComputedParameterBinding()
        {
            String providerName = m_tokenStream.NextToken();
            IComputedParameterProvider provider = ComputedParameter.GetProvider(providerName);

            if(provider == null)
                ThrowException(ERROR_INVALID, StringLocalizer.Instance.GetLocalizedString("Material_ComputedParameterInvalid", providerName));

            ParseSeparator();

            String paramName = m_tokenStream.NextToken();
            IsValidIdentifier(paramName);

            //Don't check if already is contained, always overwrite
            m_computedParameters[paramName] = new ComputedScriptParameter(paramName, provider, m_parsingParent);
        }

        #endregion

        #region RenderState Parsing

        private void ParseRenderStateBlock()
        {
            ParseOpenBrace();

            while(HasNextStatement())
                ParseRenderState();

            ParseCloseBrace();
        }

        private void ParseRenderState()
        {
            String token = m_tokenStream.NextToken();
            IRenderSystem renderSystem = m_content.ServiceProvider.GetService(typeof(IRenderSystem)) as IRenderSystem;

            if(StringCompare(token, "DepthStencilState"))
            {
                ParseDepthStencilState(renderSystem);
            }
            else if(StringCompare(token, "RasterizerState"))
            {
                ParseRasterizerState(renderSystem);
            }
            else if(StringCompare(token, "BlendState"))
            {
                ParseBlendState(renderSystem);
            }
            else if(StringCompare(token, "SamplerState"))
            {
                ParseSamplerState(renderSystem);
            }
            else
            {
                ThrowException(ERROR_INVALID, StringLocalizer.Instance.GetLocalizedString("Material_RenderStateInvalid", token));
            }
        }

        #region DepthStencilState Parsing
        
        private void ParseDepthStencilState(IRenderSystem renderSystem)
        {
            String paramName = m_tokenStream.NextToken();
            IsValidIdentifier(paramName);

            String token = m_tokenStream.NextToken();
            DepthStencilState dss = null;

            //Parse predefined depth stencil states
            if(StringCompare(token, ":"))
            {
                String type = m_tokenStream.NextToken();
                dss = GetPredefinedDepthStencilState(type);

                if(dss == null)
                    ThrowException(ERROR_NOT_DEFINED, StringLocalizer.Instance.GetLocalizedString("Material_PredefinedRenderStateInvalid", type, typeof(DepthStencilState).Name));
            }
            //Parse user defined state
            else if(StringCompare(token, "{"))
            {
                dss = new DepthStencilState(renderSystem);
                while(HasNextStatement())
                    ParseDepthStencilStateProperty(dss);

                ParseCloseBrace();
            }
            else
            {
                ThrowException(ERROR_SYNTAX, StringLocalizer.Instance.GetLocalizedString("Material_MissingRenderStateBody"));
            }

            //Don't check if already is contained, always overwrite
            m_renderStates[paramName] = m_cache.GetCachedRenderState(dss);
        }

        private void ParseDepthStencilStateProperty(DepthStencilState dss)
        {
            String property = m_tokenStream.NextToken();

            if(StringCompare(property, "DepthEnable"))
            {
                ParseSeparator();
                dss.DepthEnable = m_tokenStream.NextBool();
            }
            else if(StringCompare(property, "DepthWriteEnable"))
            {
                ParseSeparator();
                dss.DepthWriteEnable = m_tokenStream.NextBool();
            }
            else if(StringCompare(property, "DepthFunction"))
            {
                ParseSeparator();
                dss.DepthFunction = ParseComparisonFunction();
            }
            else if(StringCompare(property, "StencilEnable"))
            {
                ParseSeparator();
                dss.StencilEnable = m_tokenStream.NextBool();
            }
            else if(StringCompare(property, "ReferenceStencil"))
            {
                ParseSeparator();
                dss.ReferenceStencil = m_tokenStream.NextInt();
            }
            else if(StringCompare(property, "StencilReadMask"))
            {
                ParseSeparator();
                dss.StencilReadMask = m_tokenStream.NextInt();
            }
            else if(StringCompare(property, "StencilWriteMask"))
            {
                ParseSeparator();
                dss.StencilWriteMask = m_tokenStream.NextInt();
            }
            else if(StringCompare(property, "TwoSidedStencilEnable"))
            {
                ParseSeparator();
                dss.TwoSidedStencilEnable = m_tokenStream.NextBool();
            }
            else if(StringCompare(property, "StencilFunction"))
            {
                ParseSeparator();
                dss.StencilFunction = ParseComparisonFunction();
            }
            else if(StringCompare(property, "StencilDepthFail"))
            {
                ParseSeparator();
                dss.StencilDepthFail = ParseStencilOperation();
            }
            else if(StringCompare(property, "StencilFail"))
            {
                ParseSeparator();
                dss.StencilFail = ParseStencilOperation();
            }
            else if(StringCompare(property, "StencilPass"))
            {
                ParseSeparator();
                dss.StencilPass = ParseStencilOperation();
            }
            else if(StringCompare(property, "BackFaceStencilFunction"))
            {
                ParseSeparator();
                dss.BackFaceStencilFunction = ParseComparisonFunction();
            }
            else if(StringCompare(property, "BackFaceStencilDepthFail"))
            {
                ParseSeparator();
                dss.BackFaceStencilDepthFail = ParseStencilOperation();
            }
            else if(StringCompare(property, "BackFaceStencilDepthFail"))
            {
                ParseSeparator();
                dss.BackFaceStencilDepthFail = ParseStencilOperation();
            }
            else if(StringCompare(property, "BackFaceStencilPass"))
            {
                ParseSeparator();
                dss.BackFaceStencilPass = ParseStencilOperation();
            }
            else
            {
                ThrowException(ERROR_INVALID, StringLocalizer.Instance.GetLocalizedString("Material_RenderStatePropertyInvalid", property, typeof(DepthStencilState).Name));
            }
        }

        #endregion

        #region RasterizerState Parsing
        
        private void ParseRasterizerState(IRenderSystem renderSystem)
        {
            String paramName = m_tokenStream.NextToken();
            IsValidIdentifier(paramName);

            String token = m_tokenStream.NextToken();
            RasterizerState rs = null;

            //Parse predefined rasterizer states
            if(StringCompare(token, ":"))
            {
                String type = m_tokenStream.NextToken();
                rs = GetPredefinedRasterizerState(type);

                if(rs == null)
                    ThrowException(ERROR_NOT_DEFINED, StringLocalizer.Instance.GetLocalizedString("Material_PredefinedRenderStateInvalid", type, typeof(RasterizerState).Name));
            }
            //Parse user defined state
            else if(StringCompare(token, "{"))
            {
                rs = new RasterizerState(renderSystem);
                while(HasNextStatement())
                    ParseRasterizerStateProperty(rs);

                ParseCloseBrace();
            }
            else
            {
                ThrowException(ERROR_SYNTAX, StringLocalizer.Instance.GetLocalizedString("Material_MissingRenderStateBody"));
            }

            //Don't check if already is contained, always overwrite
            m_renderStates[paramName] = m_cache.GetCachedRenderState(rs);
        }

        private void ParseRasterizerStateProperty(RasterizerState rs)
        {
            String property = m_tokenStream.NextToken();

            if(StringCompare(property, "Cull"))
            {
                ParseSeparator();
                rs.Cull = ParseCullMode();
            }
            else if(StringCompare(property, "VertexWinding"))
            {
                ParseSeparator();
                rs.VertexWinding = ParseVertexWinding();
            }
            else if(StringCompare(property, "Fill"))
            {
                ParseSeparator();
                rs.Fill = ParseFillMode();
            }
            else if(StringCompare(property, "DepthBias"))
            {
                ParseSeparator();
                rs.DepthBias = m_tokenStream.NextInt();
            }
            else if(StringCompare(property, "DepthBiasClamp"))
            {
                ParseSeparator();
                rs.DepthBiasClamp = m_tokenStream.NextSingle();
            }
            else if(StringCompare(property, "SlopeScaledDepthBias"))
            {
                ParseSeparator();
                rs.SlopeScaledDepthBias = m_tokenStream.NextSingle();
            }
            else if(StringCompare(property, "DepthClipEnable"))
            {
                ParseSeparator();
                rs.DepthClipEnable = m_tokenStream.NextBool();
            }
            else if(StringCompare(property, "MultiSampleEnable"))
            {
                ParseSeparator();
                rs.MultiSampleEnable = m_tokenStream.NextBool();
            }
            else if(StringCompare(property, "AntialiasedLineEnable"))
            {
                ParseSeparator();
                rs.AntialiasedLineEnable = m_tokenStream.NextBool();
            }
            else if(StringCompare(property, "ScissorTestEnable"))
            {
                ParseSeparator();
                rs.ScissorTestEnable = m_tokenStream.NextBool();
            }
            else
            {
                ThrowException(ERROR_INVALID, StringLocalizer.Instance.GetLocalizedString("Material_RenderStatePropertyInvalid", property, typeof(RasterizerState).Name));
            }
        }

        #endregion

        #region BlendState Parsing

        private void ParseBlendState(IRenderSystem renderSystem)
        {
            String paramName = m_tokenStream.NextToken();
            IsValidIdentifier(paramName);

            String token = m_tokenStream.NextToken();
            BlendState bs = null;

            //Parse predefined blend states
            if(StringCompare(token, ":"))
            {
                String type = m_tokenStream.NextToken();
                bs = GetPredefinedBlendState(type);

                if(bs == null)
                    ThrowException(ERROR_NOT_DEFINED, StringLocalizer.Instance.GetLocalizedString("Material_PredefinedRenderStateInvalid", type, typeof(BlendState).Name));
            }
            //Parse user defined state
            else if(StringCompare(token, "{"))
            {
                bs = new BlendState(renderSystem);
                while(HasNextStatement())
                    ParseBlendStateProperty(bs);

                ParseCloseBrace();
            }
            else
            {
                ThrowException(ERROR_SYNTAX, StringLocalizer.Instance.GetLocalizedString("Material_MissingRenderStateBody"));
            }

            //Don't check if already is contained, always overwrite
            m_renderStates[paramName] = m_cache.GetCachedRenderState(bs);
        }

        private void ParseBlendStateProperty(BlendState bs)
        {
            String property = m_tokenStream.NextToken();

            if(StringCompare(property, "AlphaToConverageEnable"))
            {
                ParseSeparator();
                bs.AlphaToCoverageEnable = m_tokenStream.NextBool();
            }
            else if(StringCompare(property, "IndependentBlendEnable"))
            {
                ParseSeparator();
                bs.IndependentBlendEnable = m_tokenStream.NextBool();
            }
            else if(StringCompare(property, "BlendFactor"))
            {
                ParseSeparator();
                bs.BlendFactor = new Color(m_tokenStream.NextInt(), m_tokenStream.NextInt(), m_tokenStream.NextInt(), m_tokenStream.NextInt());
            }
            else if(StringCompare(property, "MultiSampleMask"))
            {
                ParseSeparator();
                bs.MultiSampleMask = m_tokenStream.NextInt();
            }
            else
            {
                int index;
                String propWithoutIndex;
                ParseNameAndIndex(property, out propWithoutIndex, out index);
                bool canSet = index >= 0 && index < bs.RenderTargetBlendCount;

                if(StringCompare(propWithoutIndex, "BlendEnable"))
                {
                    ParseSeparator();
                    bool blendEnable = m_tokenStream.NextBool();

                    if(canSet)
                        bs.SetBlendEnable(index, blendEnable);
                }
                else if(StringCompare(propWithoutIndex, "AlphaBlendFunction"))
                {
                    ParseSeparator();
                    BlendFunction alphaBlendFunc = ParseBlendFunction();

                    if(canSet)
                        bs.SetAlphaBlendFunction(index, alphaBlendFunc);
                }
                else if(StringCompare(propWithoutIndex, "AlphaSourceBlend"))
                {
                    ParseSeparator();
                    Blend alphaSrcBlend = ParseBlend();

                    if(canSet)
                        bs.SetAlphaSourceBlend(index, alphaSrcBlend);
                }
                else if(StringCompare(propWithoutIndex, "AlphaDestinationBlend"))
                {
                    ParseSeparator();
                    Blend alphaDstBlend = ParseBlend();

                    if(canSet)
                        bs.SetAlphaDestinationBlend(index, alphaDstBlend);
                }
                else if(StringCompare(propWithoutIndex, "ColorBlendFunction"))
                {
                    ParseSeparator();
                    BlendFunction colorBlendFunc = ParseBlendFunction();

                    if(canSet)
                        bs.SetColorBlendFunction(index, colorBlendFunc);
                }
                else if(StringCompare(propWithoutIndex, "ColorSourceBlend"))
                {
                    ParseSeparator();
                    Blend colorSrcBlend = ParseBlend();

                    if(canSet)
                        bs.SetColorSourceBlend(index, colorSrcBlend);
                }
                else if(StringCompare(propWithoutIndex, "ColorDestinationBlend"))
                {
                    ParseSeparator();
                    Blend colorDstBlend = ParseBlend();

                    if(canSet)
                        bs.SetColorDestinationBlend(index, colorDstBlend);
                }
                else if(StringCompare(propWithoutIndex, "WriteChannels"))
                {
                    ParseSeparator();
                    ColorWriteChannels writeChannels = ParseColorWriteChannels();

                    if(canSet)
                        bs.SetWriteChannels(index, writeChannels);
                }
                else
                {
                    ThrowException(ERROR_INVALID, StringLocalizer.Instance.GetLocalizedString("Material_RenderStatePropertyInvalid", property, typeof(BlendState).Name));
                }
            }
        }

        #endregion

        #region SamplerState Parsing
        
        private void ParseSamplerState(IRenderSystem renderSystem)
        {
            String paramName = m_tokenStream.NextToken();
            IsValidIdentifier(paramName);

            String token = m_tokenStream.NextToken();
            SamplerState ss = null;

            //Parse predefined blend states
            if(StringCompare(token, ":"))
            {
                String type = m_tokenStream.NextToken();
                ss = GetPredefinedSamplerState(type);

                if(ss == null)
                    ThrowException(ERROR_NOT_DEFINED, StringLocalizer.Instance.GetLocalizedString("Material_PredefinedRenderStateInvalid", type, typeof(BlendState).Name));
            }
            //Parse user defined state
            else if(StringCompare(token, "{"))
            {
                ss = new SamplerState(renderSystem);
                while(HasNextStatement())
                    ParseSamplerStateProperty(ss);

                ParseCloseBrace();
            }
            else
            {
                ThrowException(ERROR_SYNTAX, StringLocalizer.Instance.GetLocalizedString("Material_MissingRenderStateBody"));
            }

            //Don't check if already is contained, always overwrite
            m_renderStates[paramName] = m_cache.GetCachedRenderState(ss);
        }

        private void ParseSamplerStateProperty(SamplerState ss)
        {
            String property = m_tokenStream.NextToken();

            if(StringCompare(property, "AddressU"))
            {
                ParseSeparator();
                ss.AddressU = ParseAddressMode();
            }
            else if(StringCompare(property, "AddressV"))
            {
                ParseSeparator();
                ss.AddressV = ParseAddressMode();
            }
            else if(StringCompare(property, "AddressW"))
            {
                ParseSeparator();
                ss.AddressW = ParseAddressMode();
            }
            else if(StringCompare(property, "Filter"))
            {
                ParseSeparator();
                ss.Filter = ParseTextureFilter();
            }
            else if(StringCompare(property, "MaxAnisotropy"))
            {
                ParseSeparator();
                ss.MaxAnisotropy = m_tokenStream.NextInt();
            }
            else if(StringCompare(property, "MipMapLevelOfDetailBias") || StringCompare(property, "MipMapLoDBias"))
            {
                ParseSeparator();
                ss.MipMapLevelOfDetailBias = m_tokenStream.NextSingle();
            }
            else if(StringCompare(property, "MinMipMapLevel"))
            {
                ParseSeparator();
                ss.MinMipMapLevel = m_tokenStream.NextInt();
            }
            else if(StringCompare(property, "MaxMipMapLevel"))
            {
                ParseSeparator();
                ss.MaxMipMapLevel = m_tokenStream.NextInt();
            }
            else if(StringCompare(property, "BorderColor"))
            {
                ParseSeparator();
                ss.BorderColor = new Color(m_tokenStream.NextInt(), m_tokenStream.NextInt(), m_tokenStream.NextInt(), m_tokenStream.NextInt());
            }
            else
            {
                ThrowException(ERROR_INVALID, StringLocalizer.Instance.GetLocalizedString("Material_RenderStatePropertyInvalid", property, typeof(SamplerState).Name));
            }
        }

        #endregion

        #endregion

        #region Pass Parsing

        private void ParsePass()
        {
            //Pass names WITHOUT a name is legal, instead a number is assigned based on the current # of passes we know about, that way
            //if some passes are named, unnamed passes will have a number that relates to their index in the list
            String passName = null;

            if(m_tokenStream.HasNext("{"))
            {
                passName = m_passList.Count.ToString();
            }
            else
            {
                passName = m_tokenStream.NextToken();
            }

            IsValidIdentifier(passName);

            ParseOpenBrace();

            ScriptPass pass = new ScriptPass();
            pass.Name = passName;

            //If parsing parent, then this will be inherited. Even if we have multiple parents (e.g. A->B->C, B overrides A but relative to C, it still inherits from B).
            pass.IsInherited = m_parsingParent;

            while(HasNextStatement())
            {
                String token = m_tokenStream.NextToken();

                if(StringCompare(token, "ShaderGroup"))
                {
                    ParseSeparator();

                    String shaderGroupName = m_tokenStream.NextToken();
                    IsValidIdentifier(shaderGroupName);

                    pass.ShaderGroupName = shaderGroupName;
                }
                else if(StringCompare(token, "BlendState"))
                {
                    ParseSeparator();

                    String bsName = m_tokenStream.NextToken();
                    IsValidIdentifier(bsName);

                    pass.BlendStateName = bsName;
                }
                else if(StringCompare(token, "RasterizerState"))
                {
                    ParseSeparator();

                    String rsName = m_tokenStream.NextToken();
                    IsValidIdentifier(rsName);

                    pass.RasterizerStateName = rsName;
                }
                else if(StringCompare(token, "DepthStencilState"))
                {
                    ParseSeparator();

                    String dssName = m_tokenStream.NextToken();
                    IsValidIdentifier(dssName);

                    pass.DepthStencilStateName = dssName;
                }
                else
                {
                    ThrowException(ERROR_SYNTAX, StringLocalizer.Instance.GetLocalizedString("Material_PassPropertyInvalid", token));
                }
            }

            ParseCloseBrace();

            //If already contained, then we're overriding a pass defined in the parent rather than adding, so modify the existing pass
            ScriptPass oldPass;
            if(m_passes.TryGetValue(pass.Name, out oldPass))
            {
                //Look up the old pass in the list
                ScriptPass passInList = m_passList[oldPass.Index];

                //Sanity checks
                System.Diagnostics.Debug.Assert(passInList.Name.Equals(pass.Name));
                System.Diagnostics.Debug.Assert(passInList.Index == oldPass.Index);

                //Set if inherited...most likely not, but may have multiple parents
                passInList.IsInherited = pass.IsInherited;

                //Overwrite only the names that aren't null in the pass list.
                if (!String.IsNullOrEmpty(pass.ShaderGroupName))
                    passInList.ShaderGroupName = pass.ShaderGroupName;

                if (!String.IsNullOrEmpty(pass.BlendStateName))
                    passInList.BlendStateName = pass.BlendStateName;

                if (!String.IsNullOrEmpty(pass.DepthStencilStateName))
                    passInList.DepthStencilStateName = pass.DepthStencilStateName;

                if (!String.IsNullOrEmpty(pass.RasterizerStateName))
                    passInList.RasterizerStateName = pass.RasterizerStateName;

                //Set data back to pass list
                m_passList[passInList.Index] = passInList;
                m_passes[passInList.Name] = passInList;
            }
            else
            {
                //Set pass index
                pass.Index = m_passList.Count;

                //Set initial pass data
                m_passList.Add(pass);
                m_passes.Add(pass.Name, pass);
            }
        }

        #endregion

        #region Common utilities

        /// <summary>
        /// Parses : statement syntax, erroring if the token is not equivalent.
        /// </summary>
        private void ParseSeparator()
        {
            if(!m_tokenStream.NextToken().Equals(":"))
                ThrowException(ERROR_SYNTAX, StringLocalizer.Instance.GetLocalizedString("Material_MissingNoColon"));
        }

        /// <summary>
        /// Parses { block syntax, erroring if the token is not equivalent.
        /// </summary>
        private void ParseOpenBrace()
        {
            if(!m_tokenStream.NextToken().Equals("{"))
                ThrowException(ERROR_SYNTAX, StringLocalizer.Instance.GetLocalizedString("Material_MissingOpenBrace"));
        }

        /// <summary>
        /// Parses } block syntax, erroring if the token is not equivalent.
        /// </summary>
        private void ParseCloseBrace()
        {
            if(!m_tokenStream.NextToken().Equals("}"))
                ThrowException(ERROR_SYNTAX, StringLocalizer.Instance.GetLocalizedString("Material_MissingCloseBrace"));
        }

        /// <summary>
        /// Checks if we have a next statement in a { } block, which is a simple check to see
        /// if we have hit the closing curly brace.
        /// </summary>
        /// <returns></returns>
        private bool HasNextStatement()
        {
            return !m_tokenStream.HasNext("}");
        }

        /// <summary>
        /// String comparison helper that uses StringComparison.InvariantCultureIgnoreCase
        /// for comparing tokens to keywords/syntax. Keywords are not case sensitive.
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        private static bool StringCompare(String left, String right)
        {
            return left.Equals(right, StringComparison.InvariantCultureIgnoreCase);
        }

        /// <summary>
        /// String comparison helper that uses StringComparison.InvariantCultureIgnoreCase
        /// to see if the left string starts with the string sequence defined by the right string.
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        private static bool StringStartsWith(String left, String right)
        {
            return left.StartsWith(right, StringComparison.InvariantCultureIgnoreCase);
        }

        /// <summary>
        /// Exception helper that throws an IOException with an error code, error message,
        /// and other details such as the resource name, line number and column number approximately where the error occured.
        /// </summary>
        /// <param name="errorCode"></param>
        /// <param name="errorMsg"></param>
        private void ThrowException(String errorCode, String errorMsg)
        { 
            throw new IOException(StringLocalizer.Instance.GetLocalizedString("Material_ExceptionString", m_materialName, m_tokenStream.LineNumber.ToString(), m_tokenStream.Column.ToString(), errorCode, errorMsg));
        }

        /// <summary>
        /// Checks if an identifier is valid.
        /// </summary>
        /// <param name="name"></param>
        private bool IsValidIdentifier(String name, bool throwException = true)
        {
            //Currently only check if an identifier is a curly brace or a colon, as we sometimes use those to coordinate our parsing, so it's
            //a bad idea to let identifiers use them. Maybe we'll be strict with keywords, but for now just these three.
            if(String.IsNullOrEmpty(name) || name.Equals("{", StringComparison.InvariantCultureIgnoreCase) || name.Equals("}", StringComparison.InvariantCultureIgnoreCase)
                || name.Equals(":", StringComparison.InvariantCultureIgnoreCase))
            {
                if(throwException)
                    ThrowException(ERROR_INVALID, StringLocalizer.Instance.GetLocalizedString("Material_IdentifierInvalid", name));

                return false;
            }

            return true;
        }

        /// <summary>
        /// Parses an integer index value from a string with syntax: value[index] and returns both the index and the value
        /// string minus the indexer.
        /// </summary>
        /// <param name="token"></param>
        /// <param name="name"></param>
        /// <param name="index"></param>
        private void ParseNameAndIndex(String token, out String name, out int index)
        {
            name = token;
            index = 0;

            if(String.IsNullOrEmpty(token))
                return;

            int startBracketIndex = -1;
            int stopBracketIndex = -1;

            for(int i = 0; i < token.Length; i++)
            {
                char tokenChar = token[i];

                if(startBracketIndex == -1)
                {
                    if(tokenChar == '[')
                        startBracketIndex = i;
                }
                else if(stopBracketIndex == -1)
                {
                    if(tokenChar == ']')
                        stopBracketIndex = i;
                }
                else
                {
                    break;
                }
            }

            if(startBracketIndex == -1 || stopBracketIndex == -1)
                return;

            String num = token.Substring(startBracketIndex + 1, (stopBracketIndex - 1) - startBracketIndex);
            name = token.Substring(0, startBracketIndex);
            if(!int.TryParse(num, out index))
                index = 0;
        }

        /// <summary>
        /// Parses a ComparisonFunction enum.
        /// </summary>
        /// <returns>Parsed value</returns>
        private ComparisonFunction ParseComparisonFunction()
        {
            String compFunc = m_tokenStream.NextToken();

            if(StringCompare(compFunc, "Always"))
            {
                return ComparisonFunction.Always;
            }
            else if(StringCompare(compFunc, "Never"))
            {
                return ComparisonFunction.Never;
            }
            else if(StringCompare(compFunc, "Less"))
            {
                return ComparisonFunction.Less;
            }
            else if(StringCompare(compFunc, "LessEqual"))
            {
                return ComparisonFunction.LessEqual;
            }
            else if(StringCompare(compFunc, "Equal"))
            {
                return ComparisonFunction.Equal;
            }
            else if(StringCompare(compFunc, "GreaterEqual"))
            {
                return ComparisonFunction.GreaterEqual;
            }
            else if(StringCompare(compFunc, "Greater"))
            {
                return ComparisonFunction.Greater;
            }
            else if(StringCompare(compFunc, "NotEqual"))
            {
                return ComparisonFunction.NotEqual;
            }
            else
            {
                ThrowException(ERROR_INVALID, StringLocalizer.Instance.GetLocalizedString("Material_EnumInvalid", compFunc, typeof(ComparisonFunction).Name));
            }

            return ComparisonFunction.Always;
        }

        /// <summary>
        /// Parses a StencilOperation enum.
        /// </summary>
        /// <returns>Parsed value</returns>
        private StencilOperation ParseStencilOperation()
        {
            String stencilOp = m_tokenStream.NextToken();

            if(StringCompare(stencilOp, "Keep"))
            {
                return StencilOperation.Keep;
            }
            else if(StringCompare(stencilOp, "Zero"))
            {
                return StencilOperation.Zero;
            }
            else if(StringCompare(stencilOp, "Replace"))
            {
                return StencilOperation.Replace;
            }
            else if(StringCompare(stencilOp, "Increment"))
            {
                return StencilOperation.Increment;
            }
            else if(StringCompare(stencilOp, "Decrement"))
            {
                return StencilOperation.Decrement;
            }
            else if(StringCompare(stencilOp, "IncrementAndClamp"))
            {
                return StencilOperation.IncrementAndClamp;
            }
            else if(StringCompare(stencilOp, "DecrementAndClamp"))
            {
                return StencilOperation.DecrementAndClamp;
            }
            else if(StringCompare(stencilOp, "Invert"))
            {
                return StencilOperation.Invert;
            }
            else
            {
                ThrowException(ERROR_INVALID, StringLocalizer.Instance.GetLocalizedString("Material_EnumInvalid", stencilOp, typeof(StencilOperation).Name));
            }

            return StencilOperation.Keep;
        }

        /// <summary>
        /// Parses a TextureFilter enum.
        /// </summary>
        /// <returns>Parsed value</returns>
        private TextureFilter ParseTextureFilter()
        {
            String filter = m_tokenStream.NextToken();

            if(StringCompare(filter, "Point"))
            {
                return TextureFilter.Point;
            }
            else if(StringCompare(filter, "PointMipLinear"))
            {
                return TextureFilter.PointMipLinear;
            }
            else if(StringCompare(filter, "Linear"))
            {
                return TextureFilter.Linear;
            }
            else if(StringCompare(filter, "LinearMipPoint"))
            {
                return TextureFilter.LinearMipPoint;
            }
            else if(StringCompare(filter, "MinLinearMagPointMipLinear"))
            {
                return TextureFilter.MinLinearMagPointMipLinear;
            }
            else if(StringCompare(filter, "MinLinearMagPointMipPoint"))
            {
                return TextureFilter.MinLinearMagPointMipPoint;
            }
            else if(StringCompare(filter, "MinPointMagLinearMipLinear"))
            {
                return TextureFilter.MinPointMagLinearMipLinear;
            }
            else if(StringCompare(filter, "MinPointMagLinearMipPoint"))
            {
                return TextureFilter.MinPointMagLinearMipPoint;
            }
            else if(StringCompare(filter, "Anisotropic"))
            {
                return TextureFilter.Anisotropic;
            }
            else
            {
                ThrowException(ERROR_INVALID, StringLocalizer.Instance.GetLocalizedString("Material_EnumInvalid", filter, typeof(TextureFilter).Name));
            }

            return TextureFilter.Point;
        }

        /// <summary>
        /// Parses a TextureAddressMode enum.
        /// </summary>
        /// <returns>Parsed value</returns>
        private TextureAddressMode ParseAddressMode()
        {
            String address = m_tokenStream.NextToken();

            if(StringCompare(address, "Clamp"))
            {
                return TextureAddressMode.Clamp;
            }
            else if(StringCompare(address, "Wrap"))
            {
                return TextureAddressMode.Wrap;
            }
            else if(StringCompare(address, "Mirror"))
            {
                return TextureAddressMode.Mirror;
            }
            else
            {
                ThrowException(ERROR_INVALID, StringLocalizer.Instance.GetLocalizedString("Material_EnumInvalid", address, typeof(TextureAddressMode).Name));
            }

            return TextureAddressMode.Clamp;
        }

        /// <summary>
        /// Parses a ColorWriteChannel bitflag enum.
        /// </summary>
        /// <returns>Parsed value</returns>
        private ColorWriteChannels ParseColorWriteChannels()
        {
            String channel = m_tokenStream.NextToken();

            if(StringCompare(channel, "None"))
            {
                return ColorWriteChannels.None;
            }
            else if(StringCompare(channel, "Red"))
            {
                return ColorWriteChannels.Red;
            }
            else if(StringCompare(channel, "Green"))
            {
                return ColorWriteChannels.Green;
            }
            else if(StringCompare(channel, "Blue"))
            {
                return ColorWriteChannels.Blue;
            }
            else if(StringCompare(channel, "Alpha"))
            {
                return ColorWriteChannels.Alpha;
            }
            else if(StringCompare(channel, "All"))
            {
                return ColorWriteChannels.All;
            }
            else
            {
                ThrowException(ERROR_INVALID, StringLocalizer.Instance.GetLocalizedString("Material_EnumInvalid", channel, typeof(ColorWriteChannels).Name));
            }

            return ColorWriteChannels.All;
        }

        /// <summary>
        /// Parses a BlendFunction enum.
        /// </summary>
        /// <returns>Parsed value</returns>
        private BlendFunction ParseBlendFunction()
        {
            String func = m_tokenStream.NextToken();

            if(StringCompare(func, "Add"))
            {
                return BlendFunction.Add;
            }
            else if(StringCompare(func, "Subtract"))
            {
                return BlendFunction.Subtract;
            }
            else if(StringCompare(func, "ReverseSubtract"))
            {
                return BlendFunction.ReverseSubtract;
            }
            else if(StringCompare(func, "Min"))
            {
                return BlendFunction.Minimum;
            }
            else if(StringCompare(func, "Max"))
            {
                return BlendFunction.Maximum;
            }
            else
            {
                ThrowException(ERROR_INVALID, StringLocalizer.Instance.GetLocalizedString("Material_EnumInvalid", func, typeof(BlendFunction).Name));
            }

            return BlendFunction.Add;
        }

        /// <summary>
        /// Parses a Blend enum.
        /// </summary>
        /// <returns>Parsed value</returns>
        private Blend ParseBlend()
        {
            String blend = m_tokenStream.NextToken();

            if(StringCompare(blend, "Zero"))
            {
                return Blend.Zero;
            }
            else if(StringCompare(blend, "One"))
            {
                return Blend.One;
            }
            else if(StringCompare(blend, "SourceColor"))
            {
                return Blend.SourceColor;
            }
            else if(StringCompare(blend, "InverseSourceColor"))
            {
                return Blend.InverseSourceColor;
            }
            else if(StringCompare(blend, "SourceAlpha"))
            {
                return Blend.SourceAlpha;
            }
            else if(StringCompare(blend, "InverseSourceAlpha"))
            {
                return Blend.InverseSourceAlpha;
            }
            else if(StringCompare(blend, "DestinationColor"))
            {
                return Blend.DestinationColor;
            }
            else if(StringCompare(blend, "InverseDestinationColor"))
            {
                return Blend.InverseDestinationColor;
            }
            else if(StringCompare(blend, "DestinationAlpha"))
            {
                return Blend.DestinationAlpha;
            }
            else if(StringCompare(blend, "InverseDestinationAlpha"))
            {
                return Blend.InverseDestinationAlpha;
            }
            else if(StringCompare(blend, "BlendFactor"))
            {
                return Blend.BlendFactor;
            }
            else if(StringCompare(blend, "InverseBlendFactor"))
            {
                return Blend.InverseBlendFactor;
            }
            else if(StringCompare(blend, "SourceAlphaSaturation"))
            {
                return Blend.SourceAlphaSaturation;
            }
            else
            {
                ThrowException(ERROR_INVALID, StringLocalizer.Instance.GetLocalizedString("Material_EnumInvalid", blend, typeof(Blend).Name));
            }

            return Blend.One;
        }

        /// <summary>
        /// Parses a CullMode enum.
        /// </summary>
        /// <returns>Parsed value</returns>
        private CullMode ParseCullMode()
        {
            String cullMode = m_tokenStream.NextToken();

            if(StringCompare(cullMode, "Back"))
            {
                return CullMode.Back;
            }
            else if(StringCompare(cullMode, "Front"))
            {
                return CullMode.Front;
            }
            else if(StringCompare(cullMode, "None"))
            {
                return CullMode.None;
            }
            else
            {
                ThrowException(ERROR_INVALID, StringLocalizer.Instance.GetLocalizedString("Material_EnumInvalid", cullMode, typeof(CullMode).Name));
            }

            return CullMode.Back;
        }

        /// <summary>
        /// Parses a VertexWinding enum.
        /// </summary>
        /// <returns>Parsed value</returns>
        private VertexWinding ParseVertexWinding()
        {
            String vertexWinding = m_tokenStream.NextToken();

            if(StringCompare(vertexWinding, "Clockwise"))
            {
                return VertexWinding.Clockwise;
            }
            else if(StringCompare(vertexWinding, "CounterClockwise"))
            {
                return VertexWinding.CounterClockwise;
            }
            else
            {
                ThrowException(ERROR_INVALID, StringLocalizer.Instance.GetLocalizedString("Material_EnumInvalid", vertexWinding, typeof(VertexWinding).Name));
            }

            return VertexWinding.Clockwise;
        }

        /// <summary>
        /// Parses a FillMode enum.
        /// </summary>
        /// <returns>Parsed value</returns>
        private FillMode ParseFillMode()
        {
            String fillMode = m_tokenStream.NextToken();

            if(StringCompare(fillMode, "Solid"))
            {
                return FillMode.Solid;
            }
            else if(StringCompare(fillMode, "WireFrame"))
            {
                return FillMode.WireFrame;
            }
            else
            {
                ThrowException(ERROR_INVALID, StringLocalizer.Instance.GetLocalizedString("Material_EnumInvalid", fillMode, typeof(FillMode).Name));
            }

            return FillMode.Solid;
        }

        private DepthStencilState GetPredefinedOrCachedDepthStencilState(String name)
        {
            if(String.IsNullOrEmpty(name))
                return null;

            DepthStencilState dss = GetPredefinedDepthStencilState(name);
            if(dss == null)
            {
                RenderState state;
                if(m_renderStates.TryGetValue(name, out state))
                    dss = state as DepthStencilState;
            }

            if(dss == null)
                ThrowException(ERROR_NOT_DEFINED, StringLocalizer.Instance.GetLocalizedString("Material_RenderStateUndefined", name));

            return dss;
        }

        private RasterizerState GetPredefinedOrCachedRasterizerState(String name)
        {
            if(String.IsNullOrEmpty(name))
                return null;

            RasterizerState rs = GetPredefinedRasterizerState(name);
            if(rs == null)
            {
                RenderState state;
                if(m_renderStates.TryGetValue(name, out state))
                    rs = state as RasterizerState;
            }

            if(rs == null)
                ThrowException(ERROR_NOT_DEFINED, StringLocalizer.Instance.GetLocalizedString("Material_RenderStateUndefined", name));

            return rs;
        }

        private BlendState GetPredefinedOrCachedBlendState(String name)
        {
            if(String.IsNullOrEmpty(name))
                return null;

            BlendState bs = GetPredefinedBlendState(name);
            if(bs == null)
            {
                RenderState state;
                if(m_renderStates.TryGetValue(name, out state))
                    bs = state as BlendState;
            }

            if(bs == null)
                ThrowException(ERROR_NOT_DEFINED, StringLocalizer.Instance.GetLocalizedString("Material_RenderStateUndefined", name));

            return bs;
        }

        private SamplerState GetPredefinedOrCachedSamplerState(String name)
        {
            if(String.IsNullOrEmpty(name))
                return null;

            SamplerState ss = GetPredefinedSamplerState(name);
            if(ss == null)
            {
                RenderState state;
                if(m_renderStates.TryGetValue(name, out state))
                    ss = state as SamplerState;
            }

            if(ss == null)
                ThrowException(ERROR_NOT_DEFINED, StringLocalizer.Instance.GetLocalizedString("Material_RenderStateUndefined", name));

            return ss;
        }

        private static DepthStencilState GetPredefinedDepthStencilState(String type)
        {
            DepthStencilState dss = null;

            if(StringCompare(type, "Default"))
            {
                dss = DepthStencilState.Default;
            }
            else if(StringCompare(type, "DepthWriteOff"))
            {
                dss = DepthStencilState.DepthWriteOff;
            }
            else if(StringCompare(type, "None"))
            {
                dss = DepthStencilState.None;
            }

            return dss;
        }

        private static RasterizerState GetPredefinedRasterizerState(String type)
        {
            RasterizerState rs = null;

            if(StringCompare(type, "CullNone"))
            {
                rs = RasterizerState.CullNone;
            }
            else if(StringCompare(type, "CullNoneWireframe"))
            {
                rs = RasterizerState.CullNoneWireframe;
            }
            else if(StringCompare(type, "CullBackClockwiseFront"))
            {
                rs = RasterizerState.CullBackClockwiseFront;
            }
            else if(StringCompare(type, "CullBackCounterClockwiseFront"))
            {
                rs = RasterizerState.CullBackCounterClockwiseFront;
            }

            return rs;
        }

        private static BlendState GetPredefinedBlendState(String type)
        {
            BlendState bs = null;

            if(StringCompare(type, "AdditiveBlend"))
            {
                bs = BlendState.AdditiveBlend;
            }
            else if(StringCompare(type, "Opaque"))
            {
                bs = BlendState.Opaque;
            }
            else if(StringCompare(type, "AlphaBlendNonPremultiplied"))
            {
                bs = BlendState.AlphaBlendNonPremultiplied;
            }
            else if(StringCompare(type, "AlphaBlendPremultiplied"))
            {
                bs = BlendState.AlphaBlendPremultiplied;
            }

            return bs;
        }

        private static SamplerState GetPredefinedSamplerState(String type)
        {
            SamplerState ss = null;

            if(StringCompare(type, "AnisotropicClamp"))
            {
                ss = SamplerState.AnisotropicClamp;
            }
            else if(StringCompare(type, "AnisotropicWrap"))
            {
                ss = SamplerState.AnisotropicWrap;
            }
            else if(StringCompare(type, "LinearClamp"))
            {
                ss = SamplerState.LinearClamp;
            }
            else if(StringCompare(type, "LinearWrap"))
            {
                ss = SamplerState.LinearWrap;
            }
            else if(StringCompare(type, "PointClamp"))
            {
                ss = SamplerState.PointClamp;
            }
            else if(StringCompare(type, "PointWrap"))
            {
                ss = SamplerState.PointWrap;
            }

            return ss;
        }

        #endregion

        #region Structures

        private interface IScriptParameterData
        {
            Type DataType { get; }

            void SetData(IEffectParameter parameter);
        }

        private class MatrixParameterData : IScriptParameterData
        {
            private Matrix m_value;

            public Type DataType {  get { return typeof(Matrix); } }

            public MatrixParameterData(Matrix value)
            {
                m_value = value;
            }

            public void SetData(IEffectParameter parameter)
            {
                parameter.SetValue(m_value);
            }
        }

        private class ValueParameterData<T> : IScriptParameterData where T : unmanaged
        {
            private T m_value;

            public Type DataType {  get { return typeof(T); } }

            public ValueParameterData(T value)
            {
                m_value = value;
            }

            public void SetData(IEffectParameter parameter)
            {
                parameter.SetValue<T>(m_value);
            }
        }

        private abstract class ResourceParameterData : IScriptParameterData
        {
            public abstract Type DataType { get; }

            public abstract void SetData(IEffectParameter parameter);
            public abstract void Load(ContentManager cm, IResourceFile relativeFile, ImporterParameters importParams);
        }

        private class ResourceParameterData<T> : ResourceParameterData where T : class, IShaderResource
        {
            private T m_value;
            private String m_fullPath;
            private String m_matName, m_lineNumber, m_columnNumber; //For error reporting ONLY, because we defer loading, we won't be at the spot where we parsed the texture.

            public override Type DataType {  get { return typeof(T); } }

            public ResourceParameterData(String fullPath, String matName, String lineNum, String columnNum)
            {
                m_value = null;
                m_fullPath = fullPath;
                m_matName = matName;
                m_lineNumber = lineNum;
                m_columnNumber = columnNum;
            }

            public override void SetData(IEffectParameter parameter)
            {
                parameter.SetResource<T>(m_value);
            }

            public override void Load(ContentManager cm, IResourceFile relativeFile, ImporterParameters importParams)
            {
                //If no path, then it may be a null resource binding -- do not try to load it, the default value of null will be fine.
                if (String.IsNullOrEmpty(m_fullPath))
                    return;

                try
                {
                    m_value = cm.LoadOptionalRelativeTo<T>(m_fullPath, relativeFile, importParams);
                }
                catch(TeslaContentException e)
                {
                    ThrowException(ERROR_FILE_NOT_FOUND, StringLocalizer.Instance.GetLocalizedString("Material_FileNotFound", e.Message));
                }
                catch(InvalidCastException e)
                {
                    ThrowException(ERROR_TYPE_MISMATCH, StringLocalizer.Instance.GetLocalizedString("Material_CouldNotLoadResource", e.Message));
                }
                catch(Exception e)
                {
                    ThrowException(ERROR_GENERAL, StringLocalizer.Instance.GetLocalizedString("Material_GeneralError", e.Message));
                }
            }

            private void ThrowException(String errorCode, String msg)
            {
                //Stolen from parser's ThrowException
                throw new IOException(StringLocalizer.Instance.GetLocalizedString("Material_ExceptionString", m_matName, m_lineNumber, m_columnNumber, errorCode, msg));
            }
        }

        private class SamplerParameterData : IScriptParameterData
        {
            private String m_samplerName;
            private MaterialParser m_parser;

            public Type DataType {  get { return typeof(SamplerState); } }

            public SamplerParameterData(String samplerName, MaterialParser parser)
            {
                m_samplerName = samplerName;
                m_parser = parser;
            }

            public void SetData(IEffectParameter parameter)
            {
                SamplerState ss = m_parser.GetPredefinedOrCachedSamplerState(m_samplerName);
                parameter.SetResource<SamplerState>(ss);
            }
        }

        private struct ScriptParameter
        {
            public String Name;
            public IScriptParameterData Data;
            public bool IsInherited;

            public ScriptParameter(String name, IScriptParameterData data, bool isInherited)
            {
                Name = name;
                Data = data;
                IsInherited = isInherited;
            }
        }

        private struct ComputedScriptParameter
        {
            public String Name;
            public IComputedParameterProvider Provider;
            public bool IsInherited;

            public ComputedScriptParameter(String name, IComputedParameterProvider provider, bool isInherited)
            {
                Name = name;
                Provider = provider;
                IsInherited = isInherited;
            }
        }

        private struct ScriptPass
        {
            public String Name;
            public String ShaderGroupName;
            public String BlendStateName;
            public String RasterizerStateName;
            public String DepthStencilStateName;
            public bool IsInherited;
            public int Index;

            public bool HasRenderStates
            {
                get
                {
                    return !String.IsNullOrEmpty(BlendStateName) || !String.IsNullOrEmpty(RasterizerStateName) || !String.IsNullOrEmpty(DepthStencilStateName);
                }
            }
        }

        private struct ScriptTextCache
        {
            public String ScriptText;
            public String NameWithoutExtension;
            public bool IsAnonymous;

            public ScriptTextCache(String text, String name, bool isAnonymous = false)
            {
                ScriptText = text;
                NameWithoutExtension = name;
                IsAnonymous = isAnonymous;
            }
        }

        #endregion
    }
}
