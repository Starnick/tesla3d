﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.IO;
using Tesla.Graphics;

namespace Tesla.Content
{
    public class MaterialExternalWriter : IExternalReferenceWriter
    {
        private Type m_targetType;
        private bool m_writeOutScripts;

        public String ResourceExtension
        {
            get
            {
                return ".tem";
            }
        }

        public Type TargetType
        {
            get
            {
                return m_targetType;
            }
        }

        public MaterialExternalWriter(bool writeOutScripts)
        {
            m_writeOutScripts = writeOutScripts;

            if(m_writeOutScripts)
                m_targetType = typeof(MaterialScriptCollection);
            else
                m_targetType = typeof(Material);
        }

        public void WriteSavable<T>(IResourceFile outputResourceFile, IExternalReferenceHandler externalHandler, T value) where T : class, ISavable
        {
            if(outputResourceFile == null)
                throw new ArgumentNullException("outputResourceFile");

            if(externalHandler == null)
                throw new ArgumentNullException("externalHandler");

            if(value == null)
                throw new ArgumentNullException("savable");

            if(!m_targetType.IsAssignableFrom(value.GetType()))
                throw new InvalidCastException(StringLocalizer.Instance.GetLocalizedString("TypeMismatch", m_targetType.Name, value.GetType().Name));

            if(outputResourceFile.Extension != ResourceExtension)
                throw new ArgumentException(StringLocalizer.Instance.GetLocalizedString("InvalidResourceExtension", ResourceExtension, outputResourceFile.Extension));

            Stream output = outputResourceFile.OpenWrite();

            MaterialSerializer serializer = new MaterialSerializer();
            String scriptText = null;

            if(m_writeOutScripts)
                scriptText = serializer.Serialize(value as MaterialScriptCollection, externalHandler);
            else
                scriptText = serializer.Serialize(value as Material, externalHandler);

            if(!String.IsNullOrEmpty(scriptText))
            {
                using(StreamWriter writer = new StreamWriter(output))
                    writer.Write(scriptText);
            }
            else
            {
                output.Close();
            }
        }
    }
}
