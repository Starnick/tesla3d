﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.IO;
using Tesla.Graphics;

#nullable enable

namespace Tesla.Content
{
  /// <summary>
  /// Represents a thread-safe cache of objects that can be shared between different <see cref="Material"/> instances.
  /// </summary>
  public sealed class MaterialParserCache
  {
    /// <summary>
    /// Given a render state, check to see if there exists an already cached state and return that, or add the specified
    /// state to the cache.
    /// </summary>
    /// <param name="renderState">Render state.</param>
    /// <returns>Cached render state.</returns>
    public RenderState GetCachedRenderState(RenderState renderState)
    {
      if (renderState is null)
        return null!;

      return renderState.RenderSystem.RenderStateCache.GetOrCache(renderState);
    }

    /// <summary>
    /// Given a filename, check to see if an effect already has been loaded and return a cloned instance of that effect. If the filename
    /// has no extension then it is assumed to be a standard effect that is loaded from the render system, if the content manager has one
    /// as a service provider.
    /// </summary>
    /// <param name="fileName">Filename of effect to load from the cache.</param>
    /// <param name="fileRelativeTo">Optional resource file that the effect file is relative to. If null, then the filepath is considered absolute.</param>
    /// <param name="contentManager">Content manager to use to load the effect if not present in the cache.</param>
    /// <param name="importParams">Optional import parameters.</param>
    /// <returns>Cached effect..</returns>
    public Effect? GetCachedEffect(string fileName, IResourceFile fileRelativeTo, ContentManager contentManager, EffectImporterParameters importParams)
    {
      if (String.IsNullOrEmpty(fileName) || contentManager is null)
        return null;

      if (Path.HasExtension(fileName))
      {
        Effect? effect = contentManager.LoadOptionalRelativeTo<Effect>(fileName, fileRelativeTo, importParams);

        if (effect is not null)
          return effect.Clone();

        return null;
      }
      else
      {
        IRenderSystem? renderSystem = contentManager.ServiceProvider.GetService(typeof(IRenderSystem)) as IRenderSystem;
        if (renderSystem is not null)
          return renderSystem.StandardEffects.CreateEffect(fileName);

        return null;
      }
    }
  }
}
