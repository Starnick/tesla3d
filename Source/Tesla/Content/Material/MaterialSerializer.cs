﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Tesla.Graphics;

namespace Tesla.Content
{
    public sealed class MaterialSerializer
    {
        private const int TAB_SPACES = 4;

        private static Dictionary<Type, String> s_typenames;
        private static Dictionary<Type, Func<IEffectParameter, IExternalReferenceHandler, String>> s_typeformatters;

        private StringBuilder m_builder;
        private Dictionary<RenderState, String> m_renderStates;
        private Dictionary<String, (MaterialScriptCollection ScriptCollection, ExternalReference Path)> m_scriptNameToCollection;
        private Dictionary<String, (MaterialScriptCollection ScriptCollection, ExternalReference Path)> m_temp;

        static MaterialSerializer()
        {
            s_typenames = new Dictionary<Type, String>();
            s_typeformatters = new Dictionary<Type, Func<IEffectParameter, IExternalReferenceHandler, String>>();

            InitializeTypeNames();
            InitializeTypeFormatters();
        }

        public MaterialSerializer()
        {
            m_renderStates = new Dictionary<RenderState, String>();
            m_scriptNameToCollection = new Dictionary<String, (MaterialScriptCollection ScriptCollection, ExternalReference Path)>();
            m_temp = new Dictionary<String, (MaterialScriptCollection ScriptCollection, ExternalReference Path)>();
            m_builder = new StringBuilder();

            Reset();
        }

        public String Serialize(MaterialDefinitionScriptCollection materialDefinitions, bool serializeAllAnonymous = false, IExternalReferenceHandler externalHandler = null)
        {
            if(materialDefinitions == null || materialDefinitions.Count == 0)
                return String.Empty;

            //No external writers, so force all to be anonymous...
            if (externalHandler == null)
                serializeAllAnonymous = true;

            //If not forcing all materials to be anonymous, potentially we may have some that need to be written out to their own TEM script files
            if (!serializeAllAnonymous)
            {
                foreach (MaterialDefinition matDef in materialDefinitions)
                    GatherMaterialsInSameScriptFile(matDef);

                SerializeExternalScriptFiles(externalHandler);
            }

            foreach(MaterialDefinition matDef in materialDefinitions)
            {
                SerializeSingleMaterialDefinition(m_builder, 0, matDef, serializeAllAnonymous, externalHandler);
                m_builder.AppendLine();
                m_builder.AppendLine();
            }

            String scriptText = m_builder.ToString();
            Reset();

            return scriptText;
        }

        public String Serialize(MaterialDefinition materialDefinition, bool serializeAllAnonymous = false, IExternalReferenceHandler externalHandler = null)
        {
            if(materialDefinition == null)
                return String.Empty;

            //No external writers, so force all to be anonymous...
            if (externalHandler == null)
                serializeAllAnonymous = true;

            //If not forcing all materials to be anonymous, potentially we may have some that need to be written out to their own TEM script files
            if (!serializeAllAnonymous)
            {
                GatherMaterialsInSameScriptFile(materialDefinition);
                SerializeExternalScriptFiles(externalHandler);
            }

            SerializeSingleMaterialDefinition(m_builder, 0, materialDefinition, serializeAllAnonymous, externalHandler);

            String scriptText = m_builder.ToString();
            Reset();

            return scriptText;
        }

        public String Serialize(Material material, IExternalReferenceHandler externalHandler = null)
        {
            if(material == null)
                return String.Empty;

            SerializeSingleMaterial(m_builder, 0, false, false, material, externalHandler);

            String scriptText = m_builder.ToString();
            Reset();

            return scriptText;
        }

        public String Serialize(MaterialScriptCollection materials, IExternalReferenceHandler externalHandler = null)
        {
            return InternalSerializeMaterials(materials, externalHandler, true);
        }

        private String InternalSerializeMaterials(MaterialScriptCollection materials, IExternalReferenceHandler externalHandler, bool clearCachedScripts)
        {
            if (materials == null || materials.Count == 0)
                return String.Empty;

            foreach (Material child in materials)
            {
                SerializeSingleMaterial(m_builder, 0, false, false, child, externalHandler);
                m_builder.AppendLine();
                m_builder.AppendLine();
            }

            String scriptText = m_builder.ToString();
            Reset(clearCachedScripts);

            return scriptText;
        }

        private void Reset(bool clearCachedScripts = true)
        {
            m_builder.Clear();
            m_renderStates.Clear();

            if (clearCachedScripts)
            {
                m_scriptNameToCollection.Clear();
                m_temp.Clear();
            }
        }

        private void GatherMaterialsInSameScriptFile(MaterialDefinition materialDef)
        {
            foreach(KeyValuePair<RenderBucketID, Material> kv in materialDef)
            {
                if (kv.Value.IsAnonymous)
                    continue;

                (MaterialScriptCollection ScriptCollection, ExternalReference Path) pair;
                if(!m_scriptNameToCollection.TryGetValue(kv.Value.ScriptFileName, out pair))
                {
                    pair.ScriptCollection = new MaterialScriptCollection();
                    pair.Path = null;

                    pair.ScriptCollection.ScriptFileName = kv.Value.ScriptFileName;
                    m_scriptNameToCollection.Add(kv.Value.ScriptFileName, pair);
                }

                //This does NOT account for redundant names, which will be a problem. Not 100% sure I like the idea of modifying the material
                //when trying to serialize it, so just assert and log a warning
                bool isUnique = !pair.ScriptCollection.Contains(kv.Value.Name);

                if (!isUnique)
                {
                    String warnMsg = StringLocalizer.Instance.GetLocalizedString("MaterialScriptNamesMustBeUnique", kv.Value.Name);

                    System.Diagnostics.Debug.Assert(false,  warnMsg);
                    EngineLog.Log(LogLevel.Warn, warnMsg);
                }

                pair.ScriptCollection.Add(kv.Value);
            }
        }

        private void SerializeExternalScriptFiles(IExternalReferenceHandler handler)
        {
            //Gah using var...!!
            foreach (var kv in m_scriptNameToCollection)
            {
                String scriptFileName = kv.Key;
                if (!Path.HasExtension(scriptFileName))
                    scriptFileName = Path.ChangeExtension(scriptFileName, ".tem");

                IResourceFile scriptFile = handler.ParentResourceFile.Repository.GetResourceFileRelativeTo(scriptFileName, handler.ParentResourceFile);

                using (StreamWriter strWriter = new StreamWriter(scriptFile.OpenWrite()))
                    strWriter.Write(InternalSerializeMaterials(kv.Value.ScriptCollection, handler, false));

                ExternalReference extRef = new ExternalReference(typeof(MaterialScriptCollection), scriptFileName);
                m_temp.Add(kv.Key, (kv.Value.ScriptCollection, Path : extRef));
            }

            m_scriptNameToCollection.Clear();
            var temp = m_temp;
            m_temp = m_scriptNameToCollection;
            m_scriptNameToCollection = temp;
        }

        private void SerializeSingleMaterialDefinition(StringBuilder builder, int numSpaces, MaterialDefinition materialDef, bool forceAnonymous, IExternalReferenceHandler externalHandler)
        {
            IndentAndAppend(m_builder, numSpaces, String.Format("MaterialDefinition {0}", materialDef.Name));
            IndentAndAppend(m_builder, numSpaces, "{");

            //Non-anymous materials will go into their own TEM files. All the possible script files should have been processed by now. 
            int index = 0;
            foreach (KeyValuePair<RenderBucketID, Material> kv in materialDef)
            {
                bool isNull = kv.Value == null;
                bool isAnonymous = !isNull && (kv.Value.IsAnonymous || forceAnonymous);

                if(isAnonymous)
                {
                    //Give space between anonymous materials both above and below the declaration.
                    if(index > 0)
                        m_builder.AppendLine();

                    bool hasParent = kv.Value.HasParent;
                    bool forceNoParent = false;

                    if(hasParent)
                    {
                        //Throw exception if parent name starts with "::" because that denotes the parent is inside the same script -- serialize the script as if it does not inherit
                        if (kv.Value.ParentScriptName.StartsWith("::"))
                        {
                            forceNoParent = true;
                            IndentAndAppend(m_builder, numSpaces + TAB_SPACES, String.Format("{0} =>", kv.Key.Name));
                        }
                        else
                        {
                            IndentAndAppend(m_builder, numSpaces + TAB_SPACES, String.Format("{0} => {1}", kv.Key.Name, kv.Value.ParentScriptName));
                        }
                    }
                    else
                    {
                        IndentAndAppend(m_builder, numSpaces + TAB_SPACES, String.Format("{0} =>", kv.Key.Name));
                    }

                    SerializeSingleMaterial(m_builder, numSpaces + TAB_SPACES, true, forceNoParent, kv.Value, externalHandler);

                    //Give extra space below
                    m_builder.AppendLine();
                }
                else if(!isNull)
                {
                    //Query the script file we processed that should hold the material.
                    (MaterialScriptCollection ScriptCollection, ExternalReference Path) matScriptCollection;
                    bool foundScriptCollection = m_scriptNameToCollection.TryGetValue(kv.Value.ScriptFileName, out matScriptCollection);

                    if(!foundScriptCollection)
                    {
                        System.Diagnostics.Debug.Assert(false, "Script File Collection not found for material...");
                        continue;
                    }

                    //Write out external reference for the material. If multiple materials exist in the external reference, then write out the material name as a subresource.
                    bool hasSingleMaterial = matScriptCollection.ScriptCollection.Count == 1;

                    String pathToMaterialFile = (hasSingleMaterial) ? matScriptCollection.Path.ResourcePath : String.Format("{0}::{1}", matScriptCollection.Path.ResourcePath, kv.Value.Name);
                    IndentAndAppend(m_builder, numSpaces + TAB_SPACES, String.Format("{0} : {1}", kv.Key.Name, pathToMaterialFile));
                }
                else
                {
                    //Null material
                    IndentAndAppend(m_builder, numSpaces + TAB_SPACES, String.Format("{0} : null", kv.Key.Name));
                }

                index++;
            }

            IndentAndAppend(m_builder, numSpaces, "}");
        }

        private void SerializeSingleMaterial(StringBuilder builder, int numSpaces, bool isAnonymous, bool forceNoParent, Material material, IExternalReferenceHandler externalHandler)
        {
            if(!material.IsValid)
                return;

            bool hasParent = (!forceNoParent) ? material.HasParent : false;

            if (!isAnonymous)
                IndentAndAppend(builder, numSpaces, (hasParent) ? String.Format("Material {0} : {1}", material.Name, material.ParentScriptName) : String.Format("Material {0}", material.Name));

            IndentAndAppend(builder, numSpaces, "{");

            //Make sure blocks are nicely spaced
            bool prependNewLine = false;

            prependNewLine |= WritePropertiesBlock(builder, numSpaces + TAB_SPACES, false, material, forceNoParent, externalHandler);
            prependNewLine |= WriteParameterBindingBlock(builder, numSpaces + TAB_SPACES, prependNewLine, material, forceNoParent, externalHandler);
            prependNewLine |= WriteComputedParameterBindingBlock(builder, numSpaces + TAB_SPACES, prependNewLine, forceNoParent, material);
            prependNewLine |= WriteRenderStateBlock(builder, numSpaces + TAB_SPACES, prependNewLine, material, forceNoParent);
            prependNewLine |= WritePasses(builder, numSpaces + TAB_SPACES, prependNewLine, material, forceNoParent);

            IndentAndAppend(builder, numSpaces, "}");
        }

        #region Effect Block

        private bool WritePropertiesBlock(StringBuilder builder, int numSpaces, bool prependNewLine, Material material, bool forceNoParent, IExternalReferenceHandler externalHandler)
        {
            bool writeShadowModeOut = !material.ShadowMode.IsInherited && material.ShadowMode != ShadowMode.None;
            bool writeTransparencyOut = !material.TransparencyMode.IsInherited && material.TransparencyMode != TransparencyMode.OneSided;
            bool hasParent = (!forceNoParent) ? material.HasParent : false;

            //Inherits effect from parent. Cannot change it.
            if (hasParent && !writeShadowModeOut && !writeTransparencyOut)
                return false;

            if(prependNewLine)
                builder.AppendLine();

            IndentAndAppend(builder, numSpaces, "Properties");
            IndentAndAppend(builder, numSpaces, "{");

            //If no parent, write out effect
            if (!hasParent)
            {
                Effect effect = material.Effect;
                System.Diagnostics.Debug.Assert(effect != null);

                //If effect came from a standard library, just write out its name. Otherwise come up with a path for it.
                String effectFileName = effect.StandardContentName;
                if (String.IsNullOrEmpty(effectFileName))
                {
                    ExternalReference externalReference = externalHandler.ProcessSavable<Effect>(effect);
                    if (externalReference != ExternalReference.NullReference)
                        effectFileName = externalReference.ResourcePath;
                }

                IndentAndAppend(builder, numSpaces + TAB_SPACES, String.Format("Effect : {0}", effectFileName));
            }

            if(writeShadowModeOut)
                IndentAndAppend(builder, numSpaces + TAB_SPACES, String.Format("ShadowMode : {0}", material.ShadowMode.ToString()));

            if(writeTransparencyOut)
                IndentAndAppend(builder, numSpaces + TAB_SPACES, String.Format("TransparencyMode : {0}", material.TransparencyMode.ToString()));

            IndentAndAppend(builder, numSpaces, "}");

            return true;
        }

        #endregion

        #region ParameterBindings Block

        private bool WriteParameterBindingBlock(StringBuilder builder, int numSpaces, bool prependNewLine, Material mat, bool forceNoParent, IExternalReferenceHandler externalHandler)
        {
            if (!CanWriteParameters(mat, forceNoParent))
                return false;

            if(prependNewLine)
                builder.AppendLine();

            IndentAndAppend(builder, numSpaces, "ParameterBindings");
            IndentAndAppend(builder, numSpaces, "{");

            foreach(Material.ScriptParameterBinding binding in mat.ScriptParameterBindings)
            {
                //Inherited bindings should appear in the parent, so we can safely ignore here. If the binding was inherited, but the value changed at some point
                //it no longer will be inherited.
                bool isInherited = (!forceNoParent) ? binding.IsInherited : false;

                if(binding.IsValid && !isInherited)
                {
                    Type dataType = ResolveParameterType(binding);
                    String typeString = GetTypeName(binding.Parameter, dataType);
                    String valueString = GetValueString(binding.Parameter, externalHandler, dataType);

                    //TODO - If following a resource variable that is null, what do we do? Ignore (no assert) OR do we write out
                    //null?

                    //If anything went wrong, ignore it
                    if (valueString == null || typeString == null)
                    {
                        System.Diagnostics.Debug.Assert(false, "Unable to serialize binding.");
                        continue;
                    }

                    IndentAndAppend(builder, numSpaces + TAB_SPACES, String.Format("{0} {1} : {2}", typeString, binding.Name, valueString));
                }
            }

            IndentAndAppend(builder, numSpaces, "}");

            return true;
        }

        private bool CanWriteParameters(Material mat, bool forceNoParent)
        {
            if (mat.ScriptParameterBindingCount == 0)
                return false;

            int numCanWrite = 0;
            foreach(Material.ScriptParameterBinding binding in mat.ScriptParameterBindings)
            {
                bool isInherited = (!forceNoParent) ? binding.IsInherited : false;

                if (binding.IsValid && !isInherited)
                    numCanWrite++;
            }

            return numCanWrite > 0;
        }

        private Type ResolveParameterType(Material.ScriptParameterBinding binding)
        {
            Type dataType = binding.DataType;

            IEffectParameter eParam = binding.Parameter;

            if (dataType == null)
            {
                switch (eParam.ParameterClass)
                {
                    case EffectParameterClass.Scalar:
                    case EffectParameterClass.Vector:
                        switch (eParam.ParameterType)
                        {
                            case EffectParameterType.Bool:
                                switch (eParam.ColumnCount)
                                {
                                    case 1:
                                        dataType = typeof(bool);
                                        break;
                                    case 2:
                                        dataType = typeof(Bool2);
                                        break;
                                    case 3:
                                        dataType = typeof(Bool3);
                                        break;
                                    case 4:
                                        dataType = typeof(Bool4);
                                        break;
                                }
                                break;
                            case EffectParameterType.Int32:
                                switch (eParam.ColumnCount)
                                {
                                    case 1:
                                        dataType = typeof(int);
                                        break;
                                    case 2:
                                        dataType = typeof(Int2);
                                        break;
                                    case 3:
                                        dataType = typeof(Int3);
                                        break;
                                    case 4:
                                        dataType = typeof(Int4);
                                        break;
                                }
                                break;
                            case EffectParameterType.Single:
                                switch (eParam.ColumnCount)
                                {
                                    case 1:
                                        dataType = typeof(float);
                                        break;
                                    case 2:
                                        dataType = typeof(Vector2);
                                        break;
                                    case 3:
                                        dataType = typeof(Vector3);
                                        break;
                                    case 4:
                                        dataType = typeof(Vector4);
                                        break;
                                }
                                break;
                        }
                        break;
                    case EffectParameterClass.MatrixColumns:
                    case EffectParameterClass.MatrixRows:
                        dataType = typeof(Matrix);
                        break;
                    case EffectParameterClass.Object:
                        switch (eParam.ParameterType)
                        {
                            case EffectParameterType.SamplerState:
                                dataType = typeof(SamplerState);
                                break;
                            case EffectParameterType.Texture1D:
                                dataType = typeof(Texture1D);
                                break;
                            case EffectParameterType.Texture1DArray:
                                dataType = typeof(Texture1DArray);
                                break;
                            case EffectParameterType.Texture2D:
                                dataType = typeof(Texture2D);
                                break;
                            case EffectParameterType.Texture2DArray:
                                dataType = typeof(Texture2DArray);
                                break;
                            case EffectParameterType.Texture3D:
                                dataType = typeof(Texture3D);
                                break;
                            case EffectParameterType.TextureCube:
                                dataType = typeof(TextureCube);
                                break;
                            case EffectParameterType.TextureCubeArray:
                                dataType = typeof(TextureCubeArray);
                                break;
                        }
                        break;
                }
            }

            return dataType;
        }
        
        private String GetTypeName(IEffectParameter eParam, Type dataType)
        {
            //Get the type name
            String typename;
            if (dataType != null && s_typenames.TryGetValue(dataType, out typename))
                return typename;

            return null;
        }
        
        private String GetValueString(IEffectParameter parameter, IExternalReferenceHandler externalHandler, Type dataType)
        {
            //Sampler states get serialized into the RenderStates block, but referenced in this block
            if(dataType == typeof(SamplerState))
                return GetSamplerStateValueString(parameter.GetResource<SamplerState>());

            //Otherwise, get the formatter for the type
            Func<IEffectParameter, IExternalReferenceHandler, String> formatter;
            if (s_typeformatters.TryGetValue(dataType, out formatter))
                return formatter(parameter, externalHandler);

            System.Diagnostics.Debug.Assert(false, "Bad data type.");
            return null;
        }
        
        #endregion

        #region ComputedParameterBindings Block

        private bool WriteComputedParameterBindingBlock(StringBuilder builder, int numSpaces, bool prependNewLine, bool forceNoParent, Material mat)
        {
            if(!CanWriteComputedParameters(mat, forceNoParent))
                return false;

            if (prependNewLine)
                builder.AppendLine();

            IndentAndAppend(builder, numSpaces, "ComputedParameterBindings");
            IndentAndAppend(builder, numSpaces, "{");

            foreach(Material.ComputedParameterBinding binding in mat.ComputedParameterBindings)
            {
                //Inherited bindings should appear in the parent, so we can safely ignore here. If the binding was inherited, but the value changed at some point
                //it no longer will be inherited.
                bool isInherited = (!forceNoParent) ? binding.IsInherited : false;

                if (binding.IsValid && !isInherited)
                    IndentAndAppend(builder, numSpaces + TAB_SPACES, String.Format("{0} : {1}", binding.Provider.ComputedParameterName, binding.Name));
            }

            IndentAndAppend(builder, numSpaces, "}");

            return true;
        }

        private bool CanWriteComputedParameters(Material mat, bool forceNoParent)
        {
            if (mat.ComputedParameterBindingCount == 0)
                return false;

            int numCanWrite = 0;
            foreach(Material.ComputedParameterBinding binding in mat.ComputedParameterBindings)
            {
                bool isInherited = (!forceNoParent) ? binding.IsInherited : false;

                if (binding.IsValid && !isInherited)
                    numCanWrite++;
            }

            return numCanWrite > 0;
        }

        #endregion

        #region RenderState Block

        private bool WriteRenderStateBlock(StringBuilder builder, int numSpaces, bool prependNewLine, Material mat, bool forceNoParent)
        {
            return false;
        }

        #endregion

        #region Pass Block

        private bool WritePasses(StringBuilder builder, int numSpaces, bool prependNewLine, Material mat, bool forceNoParent)
        {
            if (!CanWritePasses(mat, forceNoParent))
                return false;

            bool firstPassProcessed = false;
            foreach (MaterialPass pass in mat.Passes)
            {
                //If inherited pass, it should be in the parent material so skip writing
                bool isInherited = (!forceNoParent) ? pass.IsInherited : false;

                if (isInherited)
                    continue;

                //Every subsequent pass append a new line, but the first pass only do it if requested
                if (!firstPassProcessed)
                {
                    if (prependNewLine)
                        builder.AppendLine();

                    firstPassProcessed = true;
                }
                else
                {
                    builder.AppendLine();
                }

                //Write out the pass
                WritePassBlock(builder, numSpaces, pass, forceNoParent);
            }

            return true;
        }

        private bool CanWritePasses(Material mat, bool forceNoParent)
        {
            if (mat.Passes.Count == 0)
                return false;

            int numCanWrite = 0;
            foreach(MaterialPass pass in mat.Passes)
            {
                bool isInherited = (!forceNoParent) ? pass.IsInherited : false;

                if (!isInherited)
                    numCanWrite++;
            }

            return numCanWrite > 0;
        }

        private void WritePassBlock(StringBuilder builder, int numSpaces, MaterialPass pass, bool forceNoParent)
        {
            System.Diagnostics.Debug.Assert(!pass.IsInherited, "Should not write out inherited passes.");

            String passName = null;

            //If the pass name equals the pass index, then it didn't have a custom name (null/empty names not allowed). This leaves two scenarios:
            //1. If the pass overrides a pass in a parent, it MUST specify the override name (index in this case). Otherwise on re-parse it would be treated as a
            //different pass.
            //2. If the pass is not override, meaning it never inherited, then we can omit the pass name.
            if(pass.Name.Equals(pass.PassIndex.ToString()))
            {
                //If forcing material to not have a parent, then it can never override
                if (!forceNoParent && pass.IsOverride)
                    passName = pass.Name;
            }
            else
            {
                //Has a custom name, so preserve that
                passName = pass.Name;
            }

            IndentAndAppend(builder, numSpaces, (passName == null) ? "Pass" : String.Format("Pass {0}", passName));
            IndentAndAppend(builder, numSpaces, "{");

            //Shadergroup is not optional
            IndentAndAppend(builder, numSpaces + TAB_SPACES, String.Format("ShaderGroup : {0}", pass.ShaderGroup.Name));

            //Look at each render state, if we use it + its not the default state, then we want to write it out.
            if((pass.RenderStatesToApply & EnforcedRenderState.BlendState) == EnforcedRenderState.BlendState)
            {
                BlendState bs = pass.BlendState;
                if(!bs.IsSameState(BlendState.Opaque))
                    IndentAndAppend(builder, numSpaces + TAB_SPACES, GetBlendStateValueString(bs));
            }

            if ((pass.RenderStatesToApply & EnforcedRenderState.DepthStencilState) == EnforcedRenderState.DepthStencilState)
            {
                DepthStencilState dss = pass.DepthStencilState;
                if (!dss.IsSameState(DepthStencilState.Default))
                    IndentAndAppend(builder, numSpaces + TAB_SPACES, GetDepthStencilStateValueString(dss));
            }

            if ((pass.RenderStatesToApply & EnforcedRenderState.RasterizerState) == EnforcedRenderState.RasterizerState)
            {
                RasterizerState rs = pass.RasterizerState;
                if (!rs.IsSameState(RasterizerState.CullBackClockwiseFront))
                    IndentAndAppend(builder, numSpaces + TAB_SPACES, GetRasterizerStateValueString(rs));
            }

            IndentAndAppend(builder, numSpaces, "}");
        }

        #endregion

        #region RenderState serialization

        private String GetSamplerStateValueString(SamplerState ss)
        {
            if(ss == null)
                return null;

            if (ss.IsStandardContent)
                return ss.StandardContentName;

            return null;
        }

        private String GetBlendStateValueString(BlendState bs)
        {
            if (bs == null)
                return null;

            if (bs.IsStandardContent)
                return bs.StandardContentName;

            return null;
        }

        private String GetDepthStencilStateValueString(DepthStencilState dss)
        {
            if (dss == null)
                return null;

            if (dss.IsStandardContent)
                return dss.StandardContentName;

            return null;
        }

        private String GetRasterizerStateValueString(RasterizerState rs)
        {
            if (rs == null)
                return null;

            if (rs.IsStandardContent)
                return rs.StandardContentName;

            return null;
        }

        #endregion

        #region Utilities

        private void IndentAndAppend(StringBuilder builder, int numSpaces, String text, bool appendNewLine = true)
        {
            Indent(builder, numSpaces);

            if(appendNewLine)
                builder.AppendLine(text);
            else
                builder.Append(text);
        }

        private void Indent(StringBuilder builder, int numSpaces)
        {
            for(int i = 0; i < numSpaces; i++)
                builder.Append(' ');
        }

        #endregion

        #region Static Initialization

        private static void InitializeTypeNames()
        {
            s_typenames.Add(typeof(float), "float");
            s_typenames.Add(typeof(Vector2), "Vector2");
            s_typenames.Add(typeof(Vector3), "Vector3");
            s_typenames.Add(typeof(Vector4), "Vector4");
            s_typenames.Add(typeof(Quaternion), "Quaternion");

            s_typenames.Add(typeof(uint), "uint");
            s_typenames.Add(typeof(int), "int");
            s_typenames.Add(typeof(Int2), "Int2");
            s_typenames.Add(typeof(Int3), "Int3");
            s_typenames.Add(typeof(Int4), "Int4");

            s_typenames.Add(typeof(Bool), "bool");
            s_typenames.Add(typeof(Bool2), "Bool2");
            s_typenames.Add(typeof(Bool3), "Bool3");
            s_typenames.Add(typeof(Bool4), "Bool4");

            s_typenames.Add(typeof(Color), "Color");
            s_typenames.Add(typeof(ColorBGRA), "ColorBGRA");

            s_typenames.Add(typeof(Matrix), "Matrix");

            s_typenames.Add(typeof(Texture1D), "Texture1D");
            s_typenames.Add(typeof(Texture1DArray), "Texture1DArray");
            s_typenames.Add(typeof(Texture2D), "Texture2D");
            s_typenames.Add(typeof(Texture2DArray), "Texture2DArray");
            s_typenames.Add(typeof(Texture3D), "Texture3D");
            s_typenames.Add(typeof(TextureCube), "TextureCube");
            s_typenames.Add(typeof(SamplerState), "SamplerState");
        }

        private static void InitializeTypeFormatters()
        {
            //FloatX
            s_typeformatters.Add(typeof(float), (IEffectParameter parameter, IExternalReferenceHandler externalhandler) =>
            {
                return parameter.GetValue<float>().ToString();
            });

            s_typeformatters.Add(typeof(Vector2), (IEffectParameter parameter, IExternalReferenceHandler externalhandler) =>
            {
                Vector2 v2 = parameter.GetValue<Vector2>();
                return String.Format("{0} {1}", v2.X.ToString(), v2.Y.ToString());
            });

            s_typeformatters.Add(typeof(Vector3), (IEffectParameter parameter, IExternalReferenceHandler externalhandler) =>
            {
                Vector3 v3 = parameter.GetValue<Vector3>();
                return String.Format("{0} {1} {2}", v3.X.ToString(), v3.Y.ToString(), v3.Z.ToString());
            });

            s_typeformatters.Add(typeof(Vector4), (IEffectParameter parameter, IExternalReferenceHandler externalhandler) =>
            {
                Vector4 v4 = parameter.GetValue<Vector4>();
                return String.Format("{0} {1} {2} {3}", v4.X.ToString(), v4.Y.ToString(), v4.Z.ToString(), v4.W.ToString());
            });

            s_typeformatters.Add(typeof(Quaternion), (IEffectParameter parameter, IExternalReferenceHandler externalhandler) =>
            {
                Quaternion q = parameter.GetValue<Quaternion>();
                return String.Format("{0} {1} {2} {3}", q.X.ToString(), q.Y.ToString(), q.Z.ToString(), q.W.ToString());
            });


            //IntX
            s_typeformatters.Add(typeof(uint), (IEffectParameter parameter, IExternalReferenceHandler externalhandler) =>
            {
                return parameter.GetValue<uint>().ToString();
            });

            s_typeformatters.Add(typeof(int), (IEffectParameter parameter, IExternalReferenceHandler externalhandler) =>
            {
                return parameter.GetValue<int>().ToString();
            });

            s_typeformatters.Add(typeof(Int2), (IEffectParameter parameter, IExternalReferenceHandler externalhandler) =>
            {
                Int2 i2 = parameter.GetValue<Int2>();
                return String.Format("{0} {1}", i2.X.ToString(), i2.Y.ToString());
            });

            s_typeformatters.Add(typeof(Int3), (IEffectParameter parameter, IExternalReferenceHandler externalhandler) =>
            {
                Int3 i3 = parameter.GetValue<Int3>();
                return String.Format("{0} {1} {2}", i3.X.ToString(), i3.Y.ToString(), i3.Z.ToString());
            });

            s_typeformatters.Add(typeof(Int4), (IEffectParameter parameter, IExternalReferenceHandler externalhandler) =>
            {
                Int4 i4 = parameter.GetValue<Int4>();
                return String.Format("{0} {1} {2} {3}", i4.X.ToString(), i4.Y.ToString(), i4.Z.ToString(), i4.W.ToString());
            });


            //BoolX
            s_typeformatters.Add(typeof(Bool), (IEffectParameter parameter, IExternalReferenceHandler externalhandler) =>
            {
                return parameter.GetValue<Bool>().ToString();
            });

            s_typeformatters.Add(typeof(Bool2), (IEffectParameter parameter, IExternalReferenceHandler externalhandler) =>
            {
                Bool2 b2 = parameter.GetValue<Bool2>();
                return String.Format("{0} {1}", b2.X.ToString(), b2.Y.ToString());
            });

            s_typeformatters.Add(typeof(Bool3), (IEffectParameter parameter, IExternalReferenceHandler externalhandler) =>
            {
                Bool3 b3 = parameter.GetValue<Bool3>();
                return String.Format("{0} {1} {2}", b3.X.ToString(), b3.Y.ToString(), b3.Z.ToString());
            });

            s_typeformatters.Add(typeof(Bool4), (IEffectParameter parameter, IExternalReferenceHandler externalhandler) =>
            {
                Bool4 b4 = parameter.GetValue<Bool4>();
                return String.Format("{0} {1}, {2}, {3}", b4.X.ToString(), b4.Y.ToString(), b4.Z.ToString(), b4.W.ToString());
            });

            
            //Color
            s_typeformatters.Add(typeof(Color), (IEffectParameter parameter, IExternalReferenceHandler externalhandler) =>
            {
                Color c = parameter.GetValue<Color>();
                return String.Format("{0} {1} {2} {3}", c.R.ToString(), c.G.ToString(), c.B.ToString(), c.A.ToString());
            });

            s_typeformatters.Add(typeof(ColorBGRA), (IEffectParameter parameter, IExternalReferenceHandler externalhandler) =>
            {
                ColorBGRA c = parameter.GetValue<ColorBGRA>();
                return String.Format("{0} {1} {2} {3}", c.B.ToString(), c.G.ToString(), c.R.ToString(), c.A.ToString());
            });


            //Matrix
            s_typeformatters.Add(typeof(Matrix), (IEffectParameter parameter, IExternalReferenceHandler externalhandler) =>
            {
                return parameter.GetMatrixValue().ToString();
            });


            //Textures
            Func<IEffectParameter, IExternalReferenceHandler, String> textureFormatter = (IEffectParameter parameter, IExternalReferenceHandler externalHandler) =>
            {
                Texture texObj = parameter.GetResource<Texture>();

                //If the texture object is actually null, then it is still a valid value
                if (texObj == null)
                    return "null";

                //If external handler is null then we cannot write out textures, log an error
                if (externalHandler == null)
                {
                    System.Diagnostics.Debug.Assert(false, String.Format("Parameter '{0}' cannot write out texture.", parameter.Name));
                    EngineLog.Log(LogLevel.Warn, "Provide an IExternalReferenceHandler when serializing materials with textures.");

                    //Null in this case means an error, since we cannot handle textures if they exist
                    return null;
                }

                ExternalReference externalReference = externalHandler.ProcessSavable<Texture>(texObj);
                if (externalReference != ExternalReference.NullReference)
                    return externalReference.ResourcePath;

                //Null in this case also means an error, since we tried to handle the texture object.
                return null;
            };

            s_typeformatters.Add(typeof(Texture1D), textureFormatter);
            s_typeformatters.Add(typeof(Texture1DArray), textureFormatter);
            s_typeformatters.Add(typeof(Texture2D), textureFormatter);
            s_typeformatters.Add(typeof(Texture2DArray), textureFormatter);
            s_typeformatters.Add(typeof(Texture3D), textureFormatter);
            s_typeformatters.Add(typeof(TextureCube), textureFormatter);
        }

        #endregion
    }
}
