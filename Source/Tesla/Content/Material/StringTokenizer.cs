﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Globalization;
using System.Text;

namespace Tesla.Content
{
    /// <summary>
    /// String tokenizer that creates a stream of tokens from an input text source. A token is typically
    /// separated by either a white space, a semi-colon, or a control character (such as a new line). Double slash (//) comments are allowed
    /// in the text, and are skipped over. Text is considered a comment at the start of two forward slash characters,
    /// and end at the following new line character.
    /// </summary>
    public class StringTokenizer
    {
        private const char EOF = (char) 0;
        private StringBuilder m_token;
        private int m_pos;
        private String m_data;
        private String m_currentToken;
        private String m_peekToken;
        private int m_line;
        private int m_col;
        private int m_savedLine;
        private int m_savedCol;
        private char[] m_numDelims = new char[] { 'f' };

        /// <summary>
        /// Gets the current token, this is the result of the last NextToken() call.
        /// </summary>
        public String CurrentToken
        {
            get
            {
                return m_currentToken;
            }
        }

        /// <summary>
        /// Gets or sets the current character position in the string that is being tokenized.
        /// </summary>
        public int CurrentPosition
        {
            get
            {
                return m_pos;
            }
            set
            {
                m_pos = Math.Max(0, value);
                GoToNextToken();
            }
        }

        /// <summary>
        /// Gets the line number of the current token.
        /// </summary>
        public int LineNumber
        {
            get
            {
                return m_savedLine;
            }
        }

        /// <summary>
        /// Gets the starting column value of the current token.
        /// </summary>
        public int Column
        {
            get
            {
                return m_savedCol;
            }
        }

        /// <summary>
        /// Gets the string data that is being parsed.
        /// </summary>
        public String Data
        {
            get
            {
                return m_data;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StringTokenizer"/> class.
        /// </summary>
        public StringTokenizer() : this(String.Empty) { }

        /// <summary>
        /// Constructs a new instance of the <see cref="StringTokenizer"/> class.
        /// </summary>
        /// <param name="text">Input text</param>
        /// <exception cref="System.ArgumentNullException">Thrown if the text is null.</exception>
        public StringTokenizer(String text)
        {
            Initialize(text);
        }

        /// <summary>
        /// Initializes the token stream with new input text.
        /// </summary>
        /// <param name="text">Input text</param>
        public void Initialize(String text)
        {
            if(text == null)
                throw new ArgumentNullException("text", "Text cannot be null");

            m_data = text;
            m_pos = 0;
            m_line = 1;
            m_col = 1;
            m_currentToken = String.Empty;

            if(m_token == null)
                m_token = new StringBuilder();
            else
                m_token.Clear();
        }

        /// <summary>
        /// Checks if we have another token left in the token stream.
        /// </summary>
        /// <returns>True if there is a next token, false otherwise.</returns>
        public bool HasNext()
        {
            //Takes care of first/last cases when we're starting out or after we're done, and will reveal
            //if we do have a token left
            if(String.IsNullOrEmpty(m_currentToken))
                GoToNextToken();

            if(GetCharacter(0) == EOF)
                return false;

            return true;
        }

        /// <summary>
        /// Peeks ahead to the next token and checks if it matches with the string sequence. 
        /// This does not advance the current position, and uses InvariantCultureIgnoreCase
        /// as the string comparison.
        /// </summary>
        /// <param name="token">String sequence to check</param>
        /// <returns>True if the next token matches, false otherwise.</returns>
        public bool HasNext(String token)
        {
            return HasNext(token, StringComparison.InvariantCultureIgnoreCase);
        }

        /// <summary>
        /// Peeks ahead to the next token and checks if it matches with the string sequence. 
        /// This does not advance the current position.
        /// </summary>
        /// <param name="token">String sequence to check</param>
        /// <param name="comparison">Comparison to use</param>
        /// <returns>True if the next token matches, false otherwise.</returns>
        public bool HasNext(String token, StringComparison comparison)
        {
            //If called more than once and NextToken() isn't called, we save the look ahead
            //so we don't have to keep collecting it.
            if(!String.IsNullOrEmpty(m_peekToken))
                return m_peekToken.Equals(token, comparison);

            int lc = m_line;
            int cc = m_col;
            int pos = m_pos;
            int slc = m_savedLine;
            int scc = m_savedCol;
            String sstr = m_currentToken;

            String str = NextToken();
            bool result = false;

            if(!String.IsNullOrEmpty(str))
            {
                result = str.Equals(token, comparison);
                m_peekToken = str;
            }

            m_line = lc;
            m_col = cc;
            m_pos = pos;
            m_savedLine = slc;
            m_savedCol = scc;
            m_currentToken = sstr;

            return result;
        }

        /// <summary>
        /// Peeks ahead to the next token and checks if it starts with the string sequence. 
        /// This does not advance the current position, and uses InvariantCultureIgnoreCase
        /// as the string comparison.
        /// </summary>
        /// <param name="token">String sequence to check</param>
        /// <returns>True if the next token matches, false otherwise.</returns>
        public bool HasNextStartsWith(String token)
        {
            return HasNextStartsWith(token, StringComparison.InvariantCultureIgnoreCase);
        }

        /// <summary>
        /// Peeks ahead to the next token and checks if it starts with the string sequence. 
        /// This does not advance the current position.
        /// </summary>
        /// <param name="token">String sequence to check</param>
        /// <param name="comparison">Comparison to use</param>
        /// <returns>True if the next token matches</returns>
        public bool HasNextStartsWith(String token, StringComparison comparison)
        {
            //If called more than once and NextToken() isn't called, we save the look ahead
            //so we don't have to keep collecting it.
            if(!String.IsNullOrEmpty(m_peekToken))
                return m_peekToken.StartsWith(token, comparison);

            int lc = m_line;
            int cc = m_col;
            int pos = m_pos;
            int slc = m_savedLine;
            int scc = m_savedCol;
            String sstr = m_currentToken;

            String str = NextToken();
            bool result = false;

            if(!String.IsNullOrEmpty(str))
            {
                result = str.StartsWith(token, comparison);
                m_peekToken = str;
            }

            m_line = lc;
            m_col = cc;
            m_pos = pos;
            m_savedLine = slc;
            m_savedCol = scc;
            m_currentToken = sstr;

            return result;
        }

        /// <summary>
        /// Parses the next token as a boolean.
        /// </summary>
        /// <returns>Boolean</returns>
        public bool NextBool()
        {
            return bool.Parse(NextToken());
        }

        /// <summary>
        /// Parses the next token as an integer.
        /// </summary>
        /// <returns>Int</returns>
        public int NextInt()
        {
            String token = NextToken();
            if(token.StartsWith("0x", StringComparison.InvariantCulture))
                return int.Parse(token.Substring(2), NumberStyles.HexNumber);

            return int.Parse(token);
        }

        /// <summary>
        /// Parses the next token as a float.
        /// </summary>
        /// <returns>Float</returns>
        public float NextSingle()
        {
            String str = NextToken();
            return float.Parse(str.TrimEnd(m_numDelims), CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Gets the next token from the text. If reached the end of the text,
        /// an empty string is returned. Whitespaces, separators, comments,
        /// and control characters are ignored.
        /// </summary>
        /// <returns>The token</returns>
        public String NextToken()
        {
            while(true)
            {
                char c = GetCharacter(0);

                switch(c)
                {
                    case EOF:
                        m_currentToken = String.Empty;
                        return m_currentToken;
                    case ' ':
                    case '\t':
                    case ';':
                    case '\r':
                    case '\n':
                        GoToNextToken();
                        break;
                    case '/':
                        if(!GoToEndOfComment())
                            return ReadToken();
                        else
                            GoToNextToken();

                        break;
                    default:
                        return ReadToken();
                }
            }
        }

        /// <summary>
        /// Reads a complete token, returning when we hit
        /// either the end of the text, a separator, a comment,
        /// or a control character.
        /// </summary>
        /// <returns>The token</returns>
        private String ReadToken()
        {
            StartReading();
            Consume();

            while(true)
            {
                char c = GetCharacter(0);

                switch(c)
                {
                    case EOF:
                    case ' ':
                    case '\t':
                    case '\r':
                    case '\n':
                    case ';':
                        GoToNextToken();
                        m_currentToken = m_token.ToString();
                        return m_currentToken;
                    case '/':
                        if(GoToEndOfComment())
                        {
                            GoToNextToken();
                            m_currentToken = m_token.ToString();
                            return m_currentToken;
                        }
                        Consume();
                        break;
                    default:
                        Consume();
                        break;
                }
            }
        }

        /// <summary>
        /// Gets the character at the current position, or
        /// subsequent characters. Character will be located at pos + count
        /// </summary>
        /// <param name="count">Number of characters to skip ahead</param>
        /// <returns>Character at the position</returns>
        private char GetCharacter(int count)
        {
            if(m_pos + count >= m_data.Length)
            {
                return EOF;
            }
            else
            {
                return m_data[m_pos + count];
            }
        }

        /// <summary>
        /// Prepare to start reading a token.
        /// </summary>
        private void StartReading()
        {
            m_token.Clear();
            m_peekToken = null;
            m_savedCol = m_col;
            m_savedLine = m_line;
        }

        /// <summary>
        /// Consumes a single valid character, and adds it to the current token.
        /// </summary>
        private void Consume()
        {
            m_token.Append(m_data[m_pos]); ;
            m_pos++;
            m_col++;
        }

        /// <summary>
        /// Skips an invalid character
        /// </summary>
        private void Skip()
        {
            m_pos++;
        }

        /// <summary>
        /// Skips a number of invalid characters
        /// </summary>
        /// <param name="count">Number of characters to skip</param>
        private void Skip(int count)
        {
            m_pos += count;
        }

        /// <summary>
        /// Go to the beginning of the very next token, skipping over
        /// separators, comments, and control characters.
        /// </summary>
        private void GoToNextToken()
        {
            while(true)
            {
                char c = GetCharacter(0);

                switch(c)
                {
                    case EOF:
                        Skip();
                        return;
                    case ' ':
                    case '\t':
                    case ';':
                        Skip();
                        m_col++;
                        break;
                    case '\r':
                        char peek = GetCharacter(1);
                        if(peek == '\n')
                            Skip();

                        Skip();
                        m_line++;
                        m_col = 1;
                        break;
                    case '\n':
                        Skip();
                        m_line++;
                        m_col = 1;
                        break;
                    case '/':
                        if(GoToEndOfComment())
                            GoToNextToken();

                        return;
                    default:
                        return;
                }
            }
        }

        /// <summary>
        /// Checks if we're at the start of a comment (//), and
        /// if we are skip all the characters until we reach a control character.
        /// </summary>
        /// <returns></returns>
        private bool GoToEndOfComment()
        {
            char dbSlash = GetCharacter(1);

            if(dbSlash == '/')
            {
                Skip(2);
                m_col += 2;
            }
            else
            {
                return false;
            }

            while(true)
            {
                char c = GetCharacter(0);

                if(!(c == '\r' || c == '\n'))
                {
                    Skip();
                    m_col++;
                }
                else
                {
                    return true;
                }
            }
        }
    }
}
