﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesla.Graphics;

namespace Tesla.Content
{
    public sealed class MaterialDefinitionResourceExporter : ResourceExporter<MaterialDefinitionScriptCollection>
    {
        private Type m_matDefType;
        private bool m_serializeAsAnonymous;

        public bool AlwaysSaveAsAnonymousMaterials
        {
            get
            {
                return m_serializeAsAnonymous;
            }
            set
            {
                m_serializeAsAnonymous = value;
            }
        }

        public MaterialDefinitionResourceExporter() 
            : base(".temd")
        {
            m_matDefType = typeof(MaterialDefinition);
            m_serializeAsAnonymous = false;
        }

        public override bool CanSaveType(Type contentType)
        {
            return base.CanSaveType(contentType) || m_matDefType.IsAssignableFrom(contentType) || contentType.IsAssignableFrom(m_matDefType);
        }

        public void Save(IExternalReferenceHandler externalHandler, MaterialDefinitionScriptCollection content)
        {
            if(externalHandler == null)
                throw new ArgumentNullException("externalHandler");

            ValidateParameters(externalHandler.ParentResourceFile, content);
            Stream output = externalHandler.ParentResourceFile.OpenWrite();

            using(StreamWriter writer = new StreamWriter(output))
            {
                MaterialSerializer serializer = new MaterialSerializer();
                writer.Write(serializer.Serialize(content, m_serializeAsAnonymous, externalHandler));
            }

            externalHandler.Flush();
        }

        public void Save(IExternalReferenceHandler externalHandler, MaterialDefinition content)
        {
            if(externalHandler == null)
                throw new ArgumentNullException("externalHandler");

            ValidateParameters(externalHandler.ParentResourceFile, content);
            Stream output = externalHandler.ParentResourceFile.OpenWrite();

            using(StreamWriter writer = new StreamWriter(output))
            {
                MaterialSerializer serializer = new MaterialSerializer();
                writer.Write(serializer.Serialize(content, m_serializeAsAnonymous, externalHandler));
            }

            externalHandler.Flush();
        }

        public void Save(IResourceFile resourceFile, MaterialDefinition content)
        {
            ValidateParameters(resourceFile, content);

            SavableWriteFlags writeFlags = SavableWriteFlags.OverwriteExistingResource | SavableWriteFlags.UseCompression;
            IExternalReferenceHandler handler = ExternalReferenceHandler.CreateBinaryDefaultHandler(resourceFile, writeFlags);

            Save(handler, content);
        }

        public void Save(Stream output, MaterialDefinition content)
        {
            ValidateParameters(output, content);

            using(StreamWriter writer = new StreamWriter(output, Encoding.UTF8, 1024, true))
            {
                MaterialSerializer serializer = new MaterialSerializer();
                writer.Write(serializer.Serialize(content, m_serializeAsAnonymous));
            }
        }

        public override void Save(IResourceFile resourceFile, MaterialDefinitionScriptCollection content)
        {
            ValidateParameters(resourceFile, content);

            SavableWriteFlags writeFlags = SavableWriteFlags.OverwriteExistingResource | SavableWriteFlags.UseCompression;
            IExternalReferenceHandler handler = ExternalReferenceHandler.CreateBinaryDefaultHandler(resourceFile, writeFlags);

            Save(handler, content);
        }

        public override void Save(Stream output, MaterialDefinitionScriptCollection content)
        {
            ValidateParameters(output, content);

            using(StreamWriter writer = new StreamWriter(output, Encoding.UTF8, 1024, true))
            {
                MaterialSerializer serializer = new MaterialSerializer();
                writer.Write(serializer.Serialize(content, m_serializeAsAnonymous));
            }
        }

        private void ValidateParameters(IResourceFile resourceFile, MaterialDefinition content)
        {
            if(resourceFile == null)
                throw new ArgumentNullException("resourceFile");

            if(content == null)
                throw new ArgumentNullException("content");
        }

        private void ValidateParameters(Stream output, MaterialDefinition content)
        {
            if(output == null || !output.CanWrite)
                throw new ArgumentNullException("output", StringLocalizer.Instance.GetLocalizedString("CannotWriteToStream"));

            if(content == null)
                throw new ArgumentNullException("content");
        }
    }
}
