﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.IO;
using System.Text;
using Tesla.Graphics;

namespace Tesla.Content
{
    /// <summary>
    /// A resource importer that can load <see cref="MaterialScriptCollection"/> and <see cref="Material"/> objects from the
    /// TEM (Tesla Engine Material) script format.
    /// </summary>
    public sealed class MaterialResourceImporter : ResourceImporter<MaterialScriptCollection>
    {
        private Type m_materialType;
        private MaterialParserCache m_cache;

        /// <summary>
        /// Gets the parser cache.
        /// </summary>
        public MaterialParserCache ParserCache
        {
            get
            {
                return m_cache;
            }
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="MaterialResourceImporter"/> class.
        /// </summary>
        public MaterialResourceImporter() 
            : this(null)
        {
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="MaterialResourceImporter"/> class.
        /// </summary>
        /// <param name="cache">Optional parser cache to re-use resources between parsers (thread safe).</param>
        public MaterialResourceImporter(MaterialParserCache cache)
            : base(".tem")
        {
            m_materialType = typeof(Material);
            m_cache = (cache == null) ? new MaterialParserCache() : cache;
        }

        /// <summary>
        /// Returns true if the specified type can be handled by this importer, false otherwise.
        /// </summary>
        /// <param name="contentType">Content type to be loaded.</param>
        /// <returns>True if the type can be loaded by this importer, false otherwise.</returns>
        public override bool CanLoadType(Type contentType)
        {
            return base.CanLoadType(contentType) || m_materialType.IsAssignableFrom(contentType) || contentType.IsAssignableFrom(m_materialType);
        }

        /// <summary>
        /// Loads content from the specified resource as the target runtime type.
        /// </summary>
        /// <param name="resourceFile">Resource file to read from</param>
        /// <param name="contentManager">Calling content manager</param>
        /// <param name="parameters">Optional loading parameters</param>
        /// <returns>The loaded object or null if it could not be loaded</returns>
        public override MaterialScriptCollection Load(IResourceFile resourceFile, ContentManager contentManager, ImporterParameters parameters)
        {
            ValidateParameters(resourceFile, contentManager, ref parameters);

            Stream stream = resourceFile.OpenRead();

            String scriptFileName = resourceFile.Name;
            String scriptText = String.Empty;

            using(StreamReader reader = new StreamReader(stream))
                scriptText = reader.ReadToEnd();

            return ParseMaterials(scriptFileName, scriptText, resourceFile, contentManager, parameters);
        }

        /// <summary>
        /// Loads content from the specified stream as the target runtime type.
        /// </summary>
        /// <param name="input">Stream to read from.</param>
        /// <param name="contentManager">Calling content manager.</param>
        /// <param name="parameters">Optional loading parameters.</param>
        /// <returns>The loaded object or null if it could not be loaded.</returns>
        public override MaterialScriptCollection Load(Stream input, ContentManager contentManager, ImporterParameters parameters)
        {
            ValidateParameters(input, contentManager, ref parameters);

            String scriptFileName = String.Empty;
            String scriptText = String.Empty;

            using(StreamReader reader = new StreamReader(input, Encoding.UTF8, true, 1024, true))
                scriptText = reader.ReadToEnd();

            return ParseMaterials(scriptFileName, scriptText, null, contentManager, parameters);
        }

        /// <summary>
        /// Parses the standard material specified at the path (virtual folder path + material script name) and then returns
        /// a script collection of all materials parsed. Normally standard material files only declare one material.
        /// </summary>
        /// <param name="fullName">The full name, the folder path + the name of the material script to return.</param>
        /// <param name="contentManager">Content manager used to load dependent resources.</param>
        /// <returns>The collection of materials defined in the script.</returns>
        public MaterialScriptCollection CreateStandardMaterials(String fullName, ContentManager contentManager)
        {
            return CreateStandardMaterials(fullName, contentManager, null);
        }

        /// <summary>
        /// Parses the standard material specified at the path (virtual folder path + material script name) and then returns
        /// a script collection of all materials parsed. Normally standard material files only declare one material.
        /// </summary>
        /// <param name="fullName">The full name, the folder path + the name of the material script to return.</param>
        /// <param name="contentManager">Content manager used to load dependent resources.</param>
        /// <param name="parameters">Material script importer parameters</param>
        /// <returns>The collection of materials defined in the script.</returns>
        public MaterialScriptCollection CreateStandardMaterials(String fullName, ContentManager contentManager, MaterialImporterParameters parameters)
        {
            String scriptText = StandardMaterialScriptLibrary.GetMaterialScript(fullName);

            if (String.IsNullOrEmpty(scriptText))
                return null;

            MaterialScriptCollection materialScripts = ParseMaterials(fullName, scriptText, null, contentManager, (parameters == null) ? ImporterParameters.None : parameters);

            //Configure each material with a different name and mark everything as inherited
            if(materialScripts != null)
            {
                foreach(Material mat in materialScripts)
                    ConfigureStandardMaterial(mat, null);
            }

            return materialScripts;
        }

        /// <summary>
        /// Parses the standard material specified at the path (virtual folder path + material script name) and then returns
        /// the first material in the script. Normally standard material files only declare one material.
        /// </summary>
        /// <param name="fullName">The full name, the folder path + the name of the material script to return.</param>
        /// <param name="contentManager">Content manager used to load dependent resources.</param>
        /// <param name="matName">Optional name to give the material. If null, then the name will be derived from the standard material name.</param>
        /// <returns>The standard material.</returns>
        public Material CreateStandardMaterial(String fullName, ContentManager contentManager, String matName = null)
        {
            return CreateStandardMaterial(fullName, contentManager, null, matName);
        }

        /// <summary>
        /// Parses the standard material specified at the path (virtual folder path + material script name) and then returns
        /// either the first material in the script or the material named in the importer parameters. Normally standard material files only declare one material.
        /// </summary>
        /// <param name="fullName">The full name, the folder path + the name of the material script to return.</param>
        /// <param name="contentManager">Content manager used to load dependent resources.</param>
        /// <param name="parameters">Material script importer parameters</param>
        /// <param name="matName">Optional name to give the material. If null, then the name will be derived from the standard material name.</param>
        /// <returns>The standard material.</returns>
        public Material CreateStandardMaterial(String fullName, ContentManager contentManager, MaterialImporterParameters parameters, String matName = null)
        {
            String scriptText = StandardMaterialScriptLibrary.GetMaterialScript(fullName);

            if (String.IsNullOrEmpty(scriptText))
                return null;

            MaterialScriptCollection scriptCollection = ParseMaterials(fullName, scriptText, null, contentManager, (parameters == null) ? ImporterParameters.None : parameters);
            if(scriptCollection != null && scriptCollection.Count > 0)
            {
                Material mat = scriptCollection[0];
                ConfigureStandardMaterial(mat, matName);
                scriptCollection.Clear();

                return mat;
            }

            return null;
        }

        private void ConfigureStandardMaterial(Material mat, String matName)
        {
            //Generally, an instance of a standard material should be treated as if it inherited from a parent. So set the parent information and all the bindings as inherit
            mat.ParentScriptName = mat.Name;
            mat.Name = (String.IsNullOrEmpty(matName)) ? String.Format("{0}_Instance", mat.ParentScriptName) : matName;
            mat.ScriptFileName = String.Empty;

            foreach (Material.ScriptParameterBinding binding in mat.ScriptParameterBindings)
                binding.IsInherited = true;

            foreach (Material.ComputedParameterBinding binding in mat.ComputedParameterBindings)
                binding.IsInherited = true;

            foreach (MaterialPass pass in mat.Passes)
                pass.IsInherited = true;
        }

        private MaterialScriptCollection ParseMaterials(String scriptFileName, String scriptText, IResourceFile resourceFile, ContentManager contentManager, ImporterParameters parameters)
        {
            MaterialParser parser = new MaterialParser(m_cache);
            parser.MaterialResourceFile = resourceFile;

            //If we got passed some parameters use them
            MaterialImporterParameters matParams = parameters as MaterialImporterParameters;
            if(matParams != null)
                parser.MaterialParameters = matParams;

            MaterialScriptCollection matColl = null;

            if(!String.IsNullOrEmpty(parameters.SubresourceName))
            {
                Material mat = parser.ParseMaterial(scriptText, parameters.SubresourceName, contentManager);
                if(mat != null)
                {
                    mat.ScriptFileName = scriptFileName;
                    matColl = new MaterialScriptCollection(1);
                    matColl.Add(mat);
                }
            }
            else
            {
                matColl = parser.ParseMaterials(scriptText, contentManager);

                //Since parser isn't aware of the script file name, set them now
                if(matColl != null)
                {
                    for(int i = 0; i < matColl.Count; i++)
                        matColl[i].ScriptFileName = scriptFileName;
                }
            }

            return matColl;
        }
    }
}
