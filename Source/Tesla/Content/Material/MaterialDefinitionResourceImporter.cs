﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.IO;
using System.Text;
using Tesla.Graphics;

namespace Tesla.Content
{
    /// <summary>
    /// A resource importer that can load <see cref="MaterialDefinitionScriptCollection"/> and <see cref="MaterialDefinition"/> objects from the
    /// TEMD (Tesla Engine Material Definition) script format.
    /// </summary>
    public sealed class MaterialDefinitionResourceImporter : ResourceImporter<MaterialDefinitionScriptCollection>
    {
        private Type m_matDefType;
        private MaterialParserCache m_cache;

        /// <summary>
        /// Gets the parser cache.
        /// </summary>
        public MaterialParserCache ParserCache
        {
            get
            {
                return m_cache;
            }
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="MaterialDefinitionResourceImporter"/> class.
        /// </summary>
        public MaterialDefinitionResourceImporter() 
            : this(null) 
        {
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="MaterialDefinitionResourceImporter"/> class.
        /// </summary>
        /// <param name="cache">Optional parser cache to re-use resources between parsers (thread safe).</param>
        public MaterialDefinitionResourceImporter(MaterialParserCache cache)
            : base(".temd")
        {
            m_matDefType = typeof(MaterialDefinition);
            m_cache = (cache == null) ? new MaterialParserCache() : cache;
        }

        /// <summary>
        /// Returns true if the specified type can be handled by this importer, false otherwise.
        /// </summary>
        /// <param name="contentType">Content type to be loaded.</param>
        /// <returns>True if the type can be loaded by this importer, false otherwise.</returns>
        public override bool CanLoadType(Type contentType)
        {
            return base.CanLoadType(contentType) || m_matDefType.IsAssignableFrom(contentType) || contentType.IsAssignableFrom(m_matDefType);
        }

        /// <summary>
        /// Loads content from the specified resource as the target runtime type.
        /// </summary>
        /// <param name="resourceFile">Resource file to read from</param>
        /// <param name="contentManager">Calling content manager</param>
        /// <param name="parameters">Optional loading parameters</param>
        /// <returns>The loaded object or null if it could not be loaded</returns>
        public override MaterialDefinitionScriptCollection Load(IResourceFile resourceFile, ContentManager contentManager, ImporterParameters parameters)
        {
            ValidateParameters(resourceFile, contentManager, ref parameters);

            Stream stream = resourceFile.OpenRead();

            String scriptFileName = resourceFile.Name;
            String scriptText = String.Empty;

            using(StreamReader reader = new StreamReader(stream))
                scriptText = reader.ReadToEnd();

            return ParseDefinitions(scriptFileName, scriptText, resourceFile, contentManager, parameters);
        }

        /// <summary>
        /// Loads content from the specified stream as the target runtime type.
        /// </summary>
        /// <param name="input">Stream to read from.</param>
        /// <param name="contentManager">Calling content manager.</param>
        /// <param name="parameters">Optional loading parameters.</param>
        /// <returns>The loaded object or null if it could not be loaded.</returns>
        public override MaterialDefinitionScriptCollection Load(Stream input, ContentManager contentManager, ImporterParameters parameters)
        {
            ValidateParameters(input, contentManager, ref parameters);

            String scriptFileName = String.Empty;
            String scriptText = String.Empty;

            using(StreamReader reader = new StreamReader(input, Encoding.UTF8, true, 1024, true))
                scriptText = reader.ReadToEnd();

            return ParseDefinitions(scriptFileName, scriptText, null, contentManager, parameters);
        }

        private MaterialDefinitionScriptCollection ParseDefinitions(String scriptFileName, String scriptText, IResourceFile resourceFile, ContentManager contentManager, ImporterParameters parameters)
        {
            MaterialParser parser = new MaterialParser(m_cache);
            parser.MaterialResourceFile = resourceFile;

            //If we got passed some parameters use them
            MaterialImporterParameters matParams = parameters as MaterialImporterParameters;
            if(matParams != null)
                parser.MaterialParameters = matParams;

            MaterialDefinitionScriptCollection matDefColl = null;

            if(!String.IsNullOrEmpty(parameters.SubresourceName))
            {
                MaterialDefinition matDef = parser.ParseDefinition(scriptText, parameters.SubresourceName, contentManager);
                if(matDef != null)
                {
                    matDef.ScriptFileName = scriptFileName;
                    matDefColl = new MaterialDefinitionScriptCollection(1);
                    matDefColl.Add(matDef);
                }
            }
            else
            {
                matDefColl = parser.ParseDefinitions(scriptText, contentManager);

                //Since parser isn't aware of the script file name, set them now
                if(matDefColl != null)
                {
                    for(int i = 0; i < matDefColl.Count; i++)
                        matDefColl[i].ScriptFileName = scriptFileName;
                }
            }

            return matDefColl;
        }
    }
}
