﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Buffers;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text.Json;

#nullable enable

namespace Tesla.Content
{
  /// <summary>
  /// Extension methods for the serialization interfaces (readers/writers/etc).
  /// </summary>
  public static class ReaderWriterExtensions
  {
    /// <summary>
    /// Writes a nullable <see cref="int"/> number, if not defined a null value is written.
    /// </summary>
    /// <param name="writer">UTF-8 Json writer.</param>
    /// <param name="value">Nullable number value.</param>
    public static void WriteNumberValueOrNull(this Utf8JsonWriter writer, int? value)
    {
      if (value is not null)
        writer.WriteNumberValue(value.Value);
      else
        writer.WriteNullValue();
    }

    /// <summary>
    /// Writes a nullable <see cref="long"/> number, if not defined a null value is written.
    /// </summary>
    /// <param name="writer">UTF-8 Json writer.</param>
    /// <param name="value">Nullable number value.</param>
    public static void WriteNumberValueOrNull(this Utf8JsonWriter writer, long? value)
    {
      if (value is not null)
        writer.WriteNumberValue(value.Value);
      else
        writer.WriteNullValue();
    }

    /// <summary>
    /// Writes a nullable <see cref="uint"/> number, if not defined a null value is written.
    /// </summary>
    /// <param name="writer">UTF-8 Json writer.</param>
    /// <param name="value">Nullable number value.</param>
    public static void WriteNumberValueOrNull(this Utf8JsonWriter writer, uint? value)
    {
      if (value is not null)
        writer.WriteNumberValue(value.Value);
      else
        writer.WriteNullValue();
    }

    /// <summary>
    /// Writes a nullable <see cref="ulong"/> number, if not defined a null value is written.
    /// </summary>
    /// <param name="writer">UTF-8 Json writer.</param>
    /// <param name="value">Nullable number value.</param>
    public static void WriteNumberValueOrNull(this Utf8JsonWriter writer, ulong? value)
    {
      if (value is not null)
        writer.WriteNumberValue(value.Value);
      else
        writer.WriteNullValue();
    }

    /// <summary>
    /// Writes a nullable <see cref="float"/> number, if not defined a null value is written.
    /// </summary>
    /// <param name="writer">UTF-8 Json writer.</param>
    /// <param name="value">Nullable number value.</param>
    public static void WriteNumberValueOrNull(this Utf8JsonWriter writer, float? value)
    {
      if (value is not null)
        writer.WriteNumberValue(value.Value);
      else
        writer.WriteNullValue();
    }

    /// <summary>
    /// Writes a nullable <see cref="double"/> number, if not defined a null value is written.
    /// </summary>
    /// <param name="writer">UTF-8 Json writer.</param>
    /// <param name="value">Nullable number value.</param>
    public static void WriteNumberValueOrNull(this Utf8JsonWriter writer, double? value)
    {
      if (value is not null)
        writer.WriteNumberValue(value.Value);
      else
        writer.WriteNullValue();
    }

    /// <summary>
    /// Writes a nullable <see cref="decimal"/> number, if not defined a null value is written.
    /// </summary>
    /// <param name="writer">UTF-8 Json writer.</param>
    /// <param name="value">Nullable number value.</param>
    public static void WriteNumberValueOrNull(this Utf8JsonWriter writer, decimal? value)
    {
      if (value is not null)
        writer.WriteNumberValue(value.Value);
      else
        writer.WriteNullValue();
    }

    /// <summary>
    /// Writes a nullable boolean value, if not defined a null value is written.
    /// </summary>
    /// <param name="writer">UTF-8 Json writer.</param>
    /// <param name="value">Nullable boolean value.</param>
    public static void WriteBooleanValueOrNull(this Utf8JsonWriter writer, bool? value)
    {
      if (value is not null)
        writer.WriteBooleanValue(value.Value);
      else
        writer.WriteNullValue();
    }

    /// <summary>
    /// Writes a string value if it's defined, or the null value if undefined.
    /// </summary>
    /// <param name="writer">UTF-8 Json writer.</param>
    /// <param name="value">String value</param>
    public static void WriteStringValueOrNull(this Utf8JsonWriter writer, string? value)
    {
      if (value is not null)
        writer.WriteStringValue(value);
      else
        writer.WriteNullValue();
    }

    /// <summary>
    /// Reads exactly the number of bytes as specified by the span.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="bytes">Span of bytes.</param>
    public static void ReadExactly(this BinaryReader reader, Span<byte> bytes)
    {
      reader.BaseStream.ReadExactly(bytes);
    }

    /// <summary>
    /// Reads exactly the number of chars as specified by the span.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="chars">Span of chars.</param>
    public static void ReadExactly(this BinaryReader reader, Span<char> chars)
    {
      int numRead = 0;
      int numRequested = chars.Length;
      while (numRead < numRequested)
      {
        int charsRead = reader.Read(chars.Slice(numRead));
        if (charsRead == 0)
          throw new EndOfStreamException(StringLocalizer.Instance.GetLocalizedString("EOFStream"));

        numRead += charsRead;
      }
    }

    /// <summary>
    /// Skips over the requested number of characters in the reader's stream.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="numChars">Number of chars to skip.</param>
    /// <exception cref="System.IO.EndOfStreamException">Thrown if the end of the stream is reached.</exception>
    public static void SkipReadChars(this BinaryReader reader, int numChars)
    {
      if (numChars <= 0)
        return;

      char[] buffer = ArrayPool<char>.Shared.Rent(4096);

      try
      {
        int numRead = 0;
        Span<char> chars = buffer.AsSpan(0, numChars);
        while (numRead < numChars)
        {
          int charsRead = reader.Read(chars.Slice(numRead));
          if (charsRead == 0)
            throw new EndOfStreamException(StringLocalizer.Instance.GetLocalizedString("EOFStream"));

          numRead += charsRead;
        }
      } 
      finally
      {
        if (buffer is not null)
          ArrayPool<char>.Shared.Return(buffer);
      }
    }

    /// <summary>
    /// Skips over the requested number of characters in the reader's stream.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="numChars">Number of chars to skip.</param>
    /// <exception cref="System.IO.EndOfStreamException">Thrown if the end of the stream is reached.</exception>
    public static void SkipReadBytes(this BinaryReader reader, int numBytes)
    {
      reader.BaseStream.Position += numBytes;
    }

    /// <summary>
    /// Skips over a single encoded string in the reader's stream.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <exception cref="System.IO.EndOfStreamException">Thrown if the end of the stream is reached.</exception>
    public static void SkipReadString(this BinaryReader reader)
    {
      // Strings have a length, but in bytes encoded first
      int numBytes = reader.Read7BitEncodedInt();
      reader.BaseStream.Position += numBytes;
    }

    /// <summary>
    /// Reads an array of bytes from the input.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <returns>Array of bytes.</returns>
    public static byte[] ReadByteArray(this IPrimitiveReader reader, string name)
    {
      int count = reader.PeekArrayCount(name);
      byte[] values = (count > 0) ? new byte[count] : Array.Empty<byte>();
      reader.ReadByteArray(name, values);

      return values;
    }

    /// <summary>
    /// Reads a single byte from the input - if it is null, returns the default value.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <param name="defaultValue">Optional default value.</param>
    /// <returns>Byte value</returns>
    public static byte ReadByteOrDefault(this IPrimitiveReader reader, string name, byte defaultValue = default)
    {
      byte? maybeValue = reader.ReadNullableByte(name);
      return maybeValue ?? defaultValue;
    }

    /// <summary>
    /// Tries to read a single byte from the input - if it's null, returns false.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">Read byte value.</param>
    /// <returns>True if the value was non-null, false if it was null.</returns>
    public static bool TryReadByte(this IPrimitiveReader reader, string name, out byte value)
    {
      byte? maybeValue = reader.ReadNullableByte(name);
      value = maybeValue ?? default;
      return maybeValue.HasValue;
    }

    /// <summary>
    /// Reads an array of sbytes from the input.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <returns>Array of sbytes.</returns>
    public static sbyte[] ReadSByteArray(this IPrimitiveReader reader, string name)
    {
      int count = reader.PeekArrayCount(name);
      sbyte[] values = (count > 0) ? new sbyte[count] : Array.Empty<sbyte>();
      reader.ReadSByteArray(name, values);

      return values;
    }

    /// <summary>
    /// Reads a single sbyte from the input - if it is null, returns the default value.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <param name="defaultValue">Optional default value.</param>
    /// <returns>SByte value</returns>
    public static sbyte ReadSByteOrDefault(this IPrimitiveReader reader, string name, sbyte defaultValue = default)
    {
      sbyte? maybeValue = reader.ReadNullableSByte(name);
      return maybeValue ?? defaultValue;
    }

    /// <summary>
    /// Tries to read a single sbyte from the input - if it's null, returns false.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">Read sbyte value.</param>
    /// <returns>True if the value was non-null, false if it was null.</returns>
    public static bool TryReadSByte(this IPrimitiveReader reader, string name, out sbyte value)
    {
      sbyte? maybeValue = reader.ReadNullableSByte(name);
      value = maybeValue ?? default;
      return maybeValue.HasValue;
    }

    /// <summary>
    /// Reads an array of chars from the input.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <returns>Array of chars.</returns>
    public static char[] ReadCharArray(this IPrimitiveReader reader, string name)
    {
      int count = reader.PeekArrayCount(name);
      char[] values = (count > 0) ? new char[count] : Array.Empty<char>();
      reader.ReadCharArray(name, values);

      return values;
    }

    /// <summary>
    /// Reads a single char from the input - if it is null, returns the default value.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <param name="defaultValue">Optional default value.</param>
    /// <returns>Char value</returns>
    public static char ReadCharOrDefault(this IPrimitiveReader reader, string name, char defaultValue = default)
    {
      char? maybeValue = reader.ReadNullableChar(name);
      return maybeValue ?? defaultValue;
    }

    /// <summary>
    /// Tries to read a single char from the input - if it's null, returns false.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">Read char value.</param>
    /// <returns>True if the value was non-null, false if it was null.</returns>
    public static bool TryReadChar(this IPrimitiveReader reader, string name, out char value)
    {
      char? maybeValue = reader.ReadNullableChar(name);
      value = maybeValue ?? default;
      return maybeValue.HasValue;
    }

    /// <summary>
    /// Reads an array of unsigned 16-bit ints from the input.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <returns>Array of uint16s.</returns>
    public static ushort[] ReadUInt16Array(this IPrimitiveReader reader, string name)
    {
      int count = reader.PeekArrayCount(name);
      ushort[] values = (count > 0) ? new ushort[count] : Array.Empty<ushort>();
      reader.ReadUInt16Array(name, values);

      return values;
    }

    /// <summary>
    /// Reads a single unsigned 16-bit int from the input - if it is null, returns the default value.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <param name="defaultValue">Optional default value.</param>
    /// <returns>UInt16 value</returns>
    public static ushort ReadUInt16OrDefault(this IPrimitiveReader reader, string name, ushort defaultValue = default)
    {
      ushort? maybeValue = reader.ReadNullableUInt16(name);
      return maybeValue ?? defaultValue;
    }

    /// <summary>
    /// Tries to read a single unsigned 16-bit int from the input - if it's null, returns false.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">Read uint16 value.</param>
    /// <returns>True if the value was non-null, false if it was null.</returns>
    public static bool TryReadUInt16(this IPrimitiveReader reader, string name, out ushort value)
    {
      ushort? maybeValue = reader.ReadNullableUInt16(name);
      value = maybeValue ?? default;
      return maybeValue.HasValue;
    }

    /// <summary>
    /// Reads an array of unsigned 32-bit ints from the input.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <returns>Array of uint32s.</returns>
    public static uint[] ReadUInt32Array(this IPrimitiveReader reader, string name)
    {
      int count = reader.PeekArrayCount(name);
      uint[] values = (count > 0) ? new uint[count] : Array.Empty<uint>();
      reader.ReadUInt32Array(name, values);
      
      return values;
    }

    /// <summary>
    /// Reads a single unsigned 32-bit int from the input - if it is null, returns the default value.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <param name="defaultValue">Optional default value.</param>
    /// <returns>UInt32 value</returns>
    public static uint ReadUInt32OrDefault(this IPrimitiveReader reader, string name, uint defaultValue = default)
    {
      uint? maybeValue = reader.ReadNullableUInt32(name);
      return maybeValue ?? defaultValue;
    }

    /// <summary>
    /// Tries to read a single unsigned 32-bit int from the input - if it's null, returns false.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">Read uint32 value.</param>
    /// <returns>True if the value was non-null, false if it was null.</returns>
    public static bool TryReadUInt32(this IPrimitiveReader reader, string name, out uint value)
    {
      uint? maybeValue = reader.ReadNullableUInt32(name);
      value = maybeValue ?? default;
      return maybeValue.HasValue;
    }

    /// <summary>
    /// Reads an array of unsigned 64-bit ints from the input.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <returns>Array of uint64s.</returns>
    public static ulong[] ReadUInt64Array(this IPrimitiveReader reader, string name)
    {
      int count = reader.PeekArrayCount(name);
      ulong[] values = (count > 0) ? new ulong[count] : Array.Empty<ulong>();
      reader.ReadUInt64Array(name, values);

      return values;
    }

    /// <summary>
    /// Reads a single unsigned 64-bit int from the input - if it is null, returns the default value.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <param name="defaultValue">Optional default value.</param>
    /// <returns>UInt64 value</returns>
    public static ulong ReadUInt64OrDefault(this IPrimitiveReader reader, string name, ulong defaultValue = default)
    {
      ulong? maybeValue = reader.ReadNullableUInt64(name);
      return maybeValue ?? defaultValue;
    }

    /// <summary>
    /// Tries to read a single unsigned 64-bit int from the input - if it's null, returns false.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">Read uint64 value.</param>
    /// <returns>True if the value was non-null, false if it was null.</returns>
    public static bool TryReadUInt64(this IPrimitiveReader reader, string name, out ulong value)
    {
      ulong? maybeValue = reader.ReadNullableUInt64(name);
      value = maybeValue ?? default;
      return maybeValue.HasValue;
    }

    /// <summary>
    /// Reads an array of 16-bit ints from the input.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <returns>Array of int16s.</returns>
    public static short[] ReadInt16Array(this IPrimitiveReader reader, string name)
    {
      int count = reader.PeekArrayCount(name);
      short[] values = (count > 0) ? new short[count] : Array.Empty<short>();
      reader.ReadInt16Array(name, values);

      return values;
    }

    /// <summary>
    /// Reads a single 16-bit int from the input - if it is null, returns the default value.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <param name="defaultValue">Optional default value.</param>
    /// <returns>Int16 value</returns>
    public static short ReadInt16OrDefault(this IPrimitiveReader reader, string name, short defaultValue = default)
    {
      short? maybeValue = reader.ReadNullableInt16(name);
      return maybeValue ?? defaultValue;
    }

    /// <summary>
    /// Tries to read a single 16-bit int from the input - if it's null, returns false.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">Read int16 value.</param>
    /// <returns>True if the value was non-null, false if it was null.</returns>
    public static bool TryReadInt16(this IPrimitiveReader reader, string name, out short value)
    {
      short? maybeValue = reader.ReadNullableInt16(name);
      value = maybeValue ?? default;
      return maybeValue.HasValue;
    }

    /// <summary>
    /// Reads an array of 32-bit ints from the input.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <returns>Array of int32s.</returns>
    public static int[] ReadInt32Array(this IPrimitiveReader reader, string name)
    {
      int count = reader.PeekArrayCount(name);
      int[] values = (count > 0) ? new int[count] : Array.Empty<int>();
      reader.ReadInt32Array(name, values);

      return values;
    }

    /// <summary>
    /// Reads a single 32-bit int from the input - if it is null, returns the default value.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <param name="defaultValue">Optional default value.</param>
    /// <returns>Int32 value</returns>
    public static int ReadInt32OrDefault(this IPrimitiveReader reader, string name, int defaultValue = default)
    {
      int? maybeValue = reader.ReadNullableInt32(name);
      return maybeValue ?? defaultValue;
    }

    /// <summary>
    /// Tries to read a single 32-bit int from the input - if it's null, returns false.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">Read int32 value.</param>
    /// <returns>True if the value was non-null, false if it was null.</returns>
    public static bool TryReadInt32(this IPrimitiveReader reader, string name, out int value)
    {
      int? maybeValue = reader.ReadNullableInt32(name);
      value = maybeValue ?? default;
      return maybeValue.HasValue;
    }

    /// <summary>
    /// Reads an array of 64-bit ints from the input.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <returns>Array of int64s.</returns>
    public static long[] ReadInt64Array(this IPrimitiveReader reader, string name)
    {
      int count = reader.PeekArrayCount(name);
      long[] values = (count > 0) ? new long[count] : Array.Empty<long>();
      reader.ReadInt64Array(name, values);

      return values;
    }

    /// <summary>
    /// Reads a single 64-bit int from the input - if it is null, returns the default value.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <param name="defaultValue">Optional default value.</param>
    /// <returns>Int64 value</returns>
    public static long ReadInt64OrDefault(this IPrimitiveReader reader, string name, long defaultValue = default)
    {
      long? maybeValue = reader.ReadNullableInt64(name);
      return maybeValue ?? defaultValue;
    }

    /// <summary>
    /// Tries to read a single 64-bit int from the input - if it's null, returns false.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">Read int64 value.</param>
    /// <returns>True if the value was non-null, false if it was null.</returns>
    public static bool TryReadInt64(this IPrimitiveReader reader, string name, out long value)
    {
      long? maybeValue = reader.ReadNullableInt64(name);
      value = maybeValue ?? default;
      return maybeValue.HasValue;
    }

    /// <summary>
    /// Reads an array of halfs from the input.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <returns>Array of halfs.</returns>
    public static Half[] ReadHalfArray(this IPrimitiveReader reader, string name)
    {
      int count = reader.PeekArrayCount(name);
      Half[] values = (count > 0) ? new Half[count] : Array.Empty<Half>();
      reader.ReadHalfArray(name, values);

      return values;
    }

    /// <summary>
    /// Reads a single half from the input - if it is null, returns the default value.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <param name="defaultValue">Optional default value.</param>
    /// <returns>Half value</returns>
    public static Half ReadHalfOrDefault(this IPrimitiveReader reader, string name, Half defaultValue = default)
    {
      Half? maybeValue = reader.ReadNullableHalf(name);
      return maybeValue ?? defaultValue;
    }

    /// <summary>
    /// Tries to read a single half from the input - if it's null, returns false.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">Read half value.</param>
    /// <returns>True if the value was non-null, false if it was null.</returns>
    public static bool TryReadHalf(this IPrimitiveReader reader, string name, out Half value)
    {
      Half? maybeValue = reader.ReadNullableHalf(name);
      value = maybeValue ?? default;
      return maybeValue.HasValue;
    }

    /// <summary>
    /// Reads an array of floats from the input.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <returns>Array of floats.</returns>
    public static float[] ReadSingleArray(this IPrimitiveReader reader, string name)
    {
      int count = reader.PeekArrayCount(name);
      float[] values = (count > 0) ? new float[count] : Array.Empty<float>();
      reader.ReadSingleArray(name, values);

      return values;
    }

    /// <summary>
    /// Reads a single float from the input - if it is null, returns the default value.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <param name="defaultValue">Optional default value.</param>
    /// <returns>Float value</returns>
    public static float ReadSingleOrDefault(this IPrimitiveReader reader, string name, float defaultValue = default)
    {
      float? maybeValue = reader.ReadNullableSingle(name);
      return maybeValue ?? defaultValue;
    }

    /// <summary>
    /// Tries to read a single float from the input - if it's null, returns false.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">Read float value.</param>
    /// <returns>True if the value was non-null, false if it was null.</returns>
    public static bool TryReadSingle(this IPrimitiveReader reader, string name, out float value)
    {
      float? maybeValue = reader.ReadNullableSingle(name);
      value = maybeValue ?? default;
      return maybeValue.HasValue;
    }

    /// <summary>
    /// Reads an array of doubles from the input.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <returns>Array of doubles.</returns>
    public static double[] ReadDoubleArray(this IPrimitiveReader reader, string name)
    {
      int count = reader.PeekArrayCount(name);
      double[] values = (count > 0) ? new double[count] : Array.Empty<double>();
      reader.ReadDoubleArray(name, values);

      return values;
    }

    /// <summary>
    /// Reads a single double from the input - if it is null, returns the default value.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <param name="defaultValue">Optional default value.</param>
    /// <returns>Double value</returns>
    public static double ReadDoubleOrDefault(this IPrimitiveReader reader, string name, double defaultValue = default)
    {
      double? maybeValue = reader.ReadNullableDouble(name);
      return maybeValue ?? defaultValue;
    }

    /// <summary>
    /// Tries to read a single double from the input - if it's null, returns false.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">Read double value.</param>
    /// <returns>True if the value was non-null, false if it was null.</returns>
    public static bool TryReadDouble(this IPrimitiveReader reader, string name, out double value)
    {
      double? maybeValue = reader.ReadNullableDouble(name);
      value = maybeValue ?? default;
      return maybeValue.HasValue;
    }

    /// <summary>
    /// Reads an array of decimals from the input.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <returns>Array of decimals.</returns>
    public static decimal[] ReadDecimalArray(this IPrimitiveReader reader, string name)
    {
      int count = reader.PeekArrayCount(name);
      decimal[] values = (count > 0) ? new decimal[count] : Array.Empty<decimal>();
      reader.ReadDecimalArray(name, values);

      return values;
    }

    /// <summary>
    /// Reads a single decimal from the input - if it is null, returns the default value.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <param name="defaultValue">Optional default value.</param>
    /// <returns>Decimal value</returns>
    public static decimal ReadDecimalOrDefault(this IPrimitiveReader reader, string name, decimal defaultValue = default)
    {
      decimal? maybeValue = reader.ReadNullableDecimal(name);
      return maybeValue ?? defaultValue;
    }

    /// <summary>
    /// Tries to read a single decimal from the input - if it's null, returns false.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">Read decimal value.</param>
    /// <returns>True if the value was non-null, false if it was null.</returns>
    public static bool TryReadDecimal(this IPrimitiveReader reader, string name, out decimal value)
    {
      decimal? maybeValue = reader.ReadNullableDecimal(name);
      value = maybeValue ?? default;
      return maybeValue.HasValue;
    }

    /// <summary>
    /// Reads an array of booleans from the input.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <returns>Array of booleans.</returns>
    public static bool[] ReadBooleanArray(this IPrimitiveReader reader, string name)
    {
      int count = reader.PeekArrayCount(name);
      bool[] values = (count > 0) ? new bool[count] : Array.Empty<bool>();
      reader.ReadBooleanArray(name, values);

      return values;
    }

    /// <summary>
    /// Reads a single boolean from the input - if it is null, returns the default value.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <param name="defaultValue">Optional default value.</param>
    /// <returns>Boolean value</returns>
    public static bool ReadBooleanOrDefault(this IPrimitiveReader reader, string name, bool defaultValue = default)
    {
      bool? maybeValue = reader.ReadNullableBoolean(name);
      return maybeValue ?? defaultValue;
    }

    /// <summary>
    /// Tries to read a single boolean from the input - if it's null, returns false.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">Read boolean value.</param>
    /// <returns>True if the value was non-null, false if it was null.</returns>
    public static bool TryReadBoolean(this IPrimitiveReader reader, string name, out bool value)
    {
      bool? maybeValue = reader.ReadNullableBoolean(name);
      value = maybeValue ?? default;
      return maybeValue.HasValue;
    }

    /// <summary>
    /// Reads an array of strings from the input.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <returns>Array of strings.</returns>
    public static string?[] ReadStringArray(this IPrimitiveReader reader, string name)
    {
      int count = reader.PeekArrayCount(name);
      string?[] values = (count > 0) ? new string?[count] : Array.Empty<string?>();
      reader.ReadStringArray(name, values);

      return values;
    }

    /// <summary>
    /// Reads an array of strings from the input, and enforces none of the array elements are null.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <param name="defaultValue">Optional default value, if not specified then an empty string.</param>
    /// <returns>Array of strings.</returns>
    public static string[] ReadStringArrayOrDefault(this IPrimitiveReader reader, string name, string defaultValue = "")
    {
      int count = reader.PeekArrayCount(name);
      string[] values = (count > 0) ? new string[count] : Array.Empty<string>();
      ReadStringArrayOrDefault(reader, name, values.AsSpan(), defaultValue);

      return values;
    }

    /// <summary>
    /// Reads an array of strings from the input and enforces they are not null values. If the receiving span is smaller than the number of elements that can be read, 
    /// it will read as many as it can and skip the rest. Use <see cref="PeekArrayCount"/> to determine how large the receiving span should be.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Span to hold the array values.</param>
    /// <param name="defaultValue">Optional default value, if not specified then an empty string.</param>
    /// <returns>Number of values read.</returns>
    public static int ReadStringArrayOrDefault(this IPrimitiveReader reader, string name, Span<string> values, string defaultValue = "")
    {
      int numRead = reader.ReadStringArray(name, values!);
      for (int i = 0; i < values.Length; i++)
      {
        if (values[i] is null)
          values[i] = defaultValue;
      }

      return numRead;
    }

    /// <summary>
    /// Reads a string from the input - if it is null, returns the default value.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <param name="defaultValue">Optional default value, if not specified then an empty string.</param>
    /// <returns>String value</returns>
    public static string ReadStringOrDefault(this IPrimitiveReader reader, string name, string defaultValue = "")
    {
      string? maybeValue = reader.ReadString(name);
      return maybeValue ?? defaultValue;
    }

    /// <summary>
    /// Reads a string from the input - if it is null, throws a <see cref="ArgumentNullException"/>.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <returns>String value</returns>
    /// <exception cref="ArgumentNullException">Thrown if the value read from the input is null.</exception>
    public static string ReadStringOrThrow(this IPrimitiveReader reader, string name)
    {
      string? maybeValue = reader.ReadString(name);
      ArgumentNullException.ThrowIfNull(maybeValue, name);
      return maybeValue;
    }

    /// <summary>
    /// Tries to read a string from the input - if it's null, returns false.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">Read string value.</param>
    /// <returns>True if the value was non-null, false if it was null.</returns>
    public static bool TryReadString(this IPrimitiveReader reader, string name, [MaybeNullWhen(false)] out string value)
    {
      string? maybeValue = reader.ReadString(name);
      if (maybeValue is not null)
      {
        value = maybeValue;
        return true;
      } 
      else
      {
        value = null;
        return false;
      }
    }

    /// <summary>
    /// Reads a single <see cref="IPrimitiveValue"/> struct from the input.
    /// </summary>
    /// <typeparam name="T">Primitive struct type.</typeparam>
    /// <param name="name">Name of the value.</param>
    /// <returns>Read <see cref="IPrimitiveValue"/> struct value.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static T Read<T>(this IPrimitiveReader reader, string name) where T : struct, IPrimitiveValue
    {
      reader.Read<T>(name, out T value);
      return value;
    }

    /// <summary>
    /// Reads a single <see cref="IPrimitiveValue"/> struct from the output.
    /// </summary>
    /// <typeparam name="T">Primitive struct type.</typeparam>
    /// <param name="name">Name of the value.</param>
    /// <returns>Read <see cref="IPrimitiveValue"/> struct value.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static T? ReadNullable<T>(this IPrimitiveReader reader, string name) where T : struct, IPrimitiveValue
    {
      reader.ReadNullable<T>(name, out T? value);
      return value;
    }

    /// <summary>
    /// Reads an array of <see cref="IPrimitiveValue"/> structs from the input.
    /// </summary>
    /// <typeparam name="T">Primitive struct type.</typeparam>
    /// <param name="name">Name of the value.</param>
    /// <returns>Array of <see cref="IPrimitiveValue"/> struct values.</returns>
    public static T[] ReadArray<T>(this IPrimitiveReader reader, string name) where T : struct, IPrimitiveValue
    {
      int count = reader.PeekArrayCount(name);
      T[] values = (count > 0) ? new T[count] : Array.Empty<T>();
      reader.ReadArray<T>(name, values);

      return values;
    }

    /// <summary>
    /// Reads an array of <see cref="IPrimitiveValue"/> structs from the input into a generic collection.
    /// </summary>
    /// <typeparam name="T">Primitive struct type.</typeparam>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Collection that holds that read data.</param>
    /// <returns>Number of elements read from the input.</returns>
    /// <exception cref="ArgumentNullException">Thrown if the collection is null.</exception>
    public static int ReadArray<T>(this IPrimitiveReader reader, string name, ICollection<T> values) where T : struct, IPrimitiveValue
    {
      ArgumentNullException.ThrowIfNull(values, nameof(values));

      int count = reader.PeekArrayCount(name);
      if (count == 0)
      {
        reader.ReadArray<T>(name, Span<T>.Empty);
        return 0;
      }

      // If our list type, we can more efficiently read the data
      if (values is IRefList<T> refList)
      {
        Span<T> listSpan = refList.AddRange(count);
        reader.ReadArray<T>(name, listSpan);
        return count;
      }

      // Use a pooled array to read the data, then add to the collection
      using PooledArray<T> temp = new PooledArray<T>(count);
      Span<T> span = temp.Span;
      reader.ReadArray<T>(name, span);

      // Ensure list doesn't resize
      if (values is List<T> listT)
      {
        int requiredCapacity = listT.Count + count;
        if (listT.Capacity < requiredCapacity)
          listT.Capacity = requiredCapacity;
      }

      for (int i = 0; i < span.Length; i++)
        values.Add(span[i]);

      return count;
    }

    /// <summary>
    /// Reads an array of <see cref="ISavable"/> objects from the input into a generic collection.
    /// </summary>
    /// <typeparam name="T">Type of object to read.</typeparam>
    /// <param name="name">Name of the object.</param>
    /// <param name="values">Collection that holds that read data.</param>
    /// <returns>Number of elements read from the input.</returns>
    /// <exception cref="ArgumentNullException">Thrown if the collection is null.</exception>
    public static int ReadSavableArray<T>(this ISavableReader reader, string name, ICollection<T> values) where T : class?, ISavable?
    {
      ArgumentNullException.ThrowIfNull(values, nameof(values));

      int count = reader.PeekArrayCount(name);

      // Used a pooled array to read the data, then add to the collection
      using PooledArray<T> temp = new PooledArray<T>(count);
      Span<T> span = temp.Span;
      reader.ReadSavableArray<T>(name, span);

      // Ensure list doesn't resize
      if (values is List<T> listT)
      {
        int requiredCapacity = listT.Count + count;
        if (listT.Capacity < requiredCapacity)
          listT.Capacity = requiredCapacity;
      }

      for (int i = 0; i < span.Length; i++)
        values.Add(span[i]);

      return count;
    }

    /// <summary>
    /// Writes an array of <see cref="IPrimitiveValue"/> structs to the output from a generic collection.
    /// </summary>
    /// <typeparam name="T">Primitive struct type.</typeparam>
    /// <param name="name">Name of the value.</param>
    /// <param name="values">Collection that holds the data to write.</param>
    /// <exception cref="ArgumentNullException">Thrown if the collection is null.</exception>
    public static void Write<T>(this IPrimitiveWriter writer, string name, IReadOnlyCollection<T> values) where T : struct, IPrimitiveValue
    {
      ArgumentNullException.ThrowIfNull(values, nameof(values));

      if (values.Count == 0)
      {
        writer.Write<T>(name, ReadOnlySpan<T>.Empty);
        return;
      }

      // Our list is the most efficient
      if (values is IReadOnlyRefList<T> refList)
      {
        writer.Write<T>(name, refList.Span);
      }
      // Regular list we can cheat and get the underlying span...
      else if (values is List<T> listT)
      {
        ReadOnlySpan<T> listSpan = CollectionsMarshal.AsSpan(listT);
        writer.Write<T>(name, listSpan);
      }
      else
      {
        // Otherwise have to used intermediate buffer
        using PooledArray<T> temp = new PooledArray<T>(values.Count);
        Span<T> span = temp.Span;

        // If a generic list, can avoid creating an enumerator
        if (values is IReadOnlyList<T> list)
        {
          for (int i = 0; i < span.Length; i++)
            span[i] = list[i];
        }
        else
        {
          int i = 0;
          foreach (T value in values)
          {
            span[i] = value;
            i++;
          }
        }

        writer.Write<T>(name, span);
      }
    }


    /// <summary>
    /// Writes an array of <see cref="ISavable"/> objects to the output from a generic collection.
    /// </summary>
    /// <typeparam name="T">Type of object to read.</typeparam>
    /// <param name="name">Name of the object.</param>
    /// <param name="values">Collection that holds the data to write.</param>
    /// <exception cref="ArgumentNullException">Thrown if the collection is null.</exception>
    public static void WriteSavable<T>(this ISavableWriter writer, string name, IReadOnlyCollection<T> values) where T : class?, ISavable?
    {
      ArgumentNullException.ThrowIfNull(values, nameof(values));

      if (values.Count == 0)
      {
        writer.WriteSavable<T>(name, ReadOnlySpan<T>.Empty);
        return;
      }

      // Regular list we can cheat and get the underlying span...
      if (values is List<T> listT)
      {
        ReadOnlySpan<T> listSpan = CollectionsMarshal.AsSpan(listT);
        writer.WriteSavable<T>(name, listSpan);
      }
      else
      {
        // Otherwise have to used intermediate buffer
        using PooledArray<T> temp = new PooledArray<T>(values.Count);
        Span<T> span = temp.Span;

        // If a generic list, can avoid creating an enumerator
        if (values is IReadOnlyList<T> list)
        {
          for (int i = 0; i < span.Length; i++)
            span[i] = list[i];
        }
        else
        {
          int i = 0;
          foreach (T value in values)
          {
            span[i] = value;
            i++;
          }
        }

        writer.WriteSavable<T>(name, span);
      }
    }

    /// <summary>
    /// Reads a single <see cref="IPrimitiveValue"/> struct from the input - if it is null, returns the default value.
    /// </summary>
    /// <typeparam name="T">Primitive struct type.</typeparam>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <param name="defaultValue">Optional default value.</param>
    /// <returns><see cref="IPrimitiveValue"/> struct value</returns>
    public static T ReadOrDefault<T>(this IPrimitiveReader reader, string name, in T defaultValue = default) where T : struct, IPrimitiveValue
    {
      reader.ReadNullable<T>(name, out T? maybeValue);
      return maybeValue ?? defaultValue;
    }

    /// <summary>
    /// Tries to read a single <see cref="IPrimitiveValue"/> struct from the input - if it's null, returns false.
    /// </summary>
    /// <typeparam name="T">Primitive struct type.</typeparam>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">Read <see cref="IPrimitiveValue"/> struct value.</param>
    /// <returns>True if the value was non-null, false if it was null.</returns>
    public static bool TryRead<T>(this IPrimitiveReader reader, string name, out T value) where T : struct, IPrimitiveValue
    {
      reader.ReadNullable<T>(name, out T? maybeValue);
      value = maybeValue ?? default;
      return maybeValue.HasValue;
    }

    /// <summary>
    /// Reads a single enum from the input - if it is null, returns the default value.
    /// </summary>
    /// <typeparam name="T">Enum type.</typeparam>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <param name="defaultValue">Optional default value.</param>
    /// <returns>Enum value.</returns>
    public static T ReadEnumOrDefault<T>(this IPrimitiveReader reader, string name, T defaultValue = default) where T : struct, Enum
    {
      T? maybeValue = reader.ReadNullableEnum<T>(name);
      return maybeValue ?? defaultValue;
    }

    /// <summary>
    /// Tries to read a single enum from the input - if it's null, returns false.
    /// </summary>
    /// <typeparam name="T">Enum type.</typeparam>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">Read enum value.</param>
    /// <returns>True if the value was non-null, false if it was null.</returns>
    public static bool TryReadEnum<T>(this IPrimitiveReader reader, string name, out T value) where T : struct, Enum
    {
      T? maybeValue = reader.ReadNullableEnum<T>(name);
      value = maybeValue ?? default;
      return maybeValue.HasValue;
    }

    /// <summary>
    /// Peeks ahead and returns the number of elements of the specified type in the blob. Zero is returned if the blob
    /// is null or not a blob. This does not advance the reader.
    /// </summary>
    /// <typeparam name="T">Blittable eleemnt type.</typeparam>
    /// <param name="reader">Reader</param>
    /// <param name="name">Name of the value</param>
    /// <returns>Non-zero if the value is a blob with elements, zero if it is null.</returns>
    public static int PeekBlobCount<T>(this IPrimitiveReader reader, string name) where T : unmanaged
    {
      PrimitiveBlobHeader? maybeBlobHeader = reader.PeekBlob(name);
      return maybeBlobHeader?.GetElementCount<T>() ?? 0;
    }

    /// <summary>
    /// Reads an <see cref="IndexData"/> from a blob.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <param name="allocatorStrategy">Optional memory allocator strategy.</param>
    /// <returns>Index data, if an empty blob then it may not be valid.</returns>
    /// <exception cref="InvalidOperationException">Thrown if the blob is not a proper index element type.</exception>
    public static IndexData ReadIndexDataBlob(this IPrimitiveReader reader, string name, MemoryAllocatorStrategy allocatorStrategy = MemoryAllocatorStrategy.Default)
    {
      IDataBuffer? db;
      if (!reader.TryReadBlob(name, allocatorStrategy, out db))
        return new IndexData();

      if (db is DataBuffer<int> dbInt) 
      {
        return new IndexData(dbInt);
      } 
      else if (db is DataBuffer<ushort> dbShort)
      {
        return new IndexData(dbShort);
      }

      throw new InvalidOperationException(StringLocalizer.Instance.GetLocalizedString("TypeMismatch", db.ElementType.ToString(), typeof(int).ToString()));
    }

    /// <summary>
    /// Writes an <see cref="IndexData"/> as a blob.
    /// </summary>
    /// <param name="writer">Writer.</param>
    /// <param name="name">Name of the value.</param>
    /// <param name="data">Index data to write to a blob.</param>
    public static void WriteIndexDataBlob(this IPrimitiveWriter writer, string name, IndexData data)
    {
      // Write an empty blob if this isn't valid
      if (!data.IsValid)
        writer.WriteBlob<int>(name, ReadOnlySpan<int>.Empty);

      writer.WriteBlob(name, data.UnderlyingDataBuffer);
    }

    /// <summary>
    /// Reads the blob into the specified data buffer.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <param name="db">Data buffer to hold the contents of the blob.</param>
    /// <param name="resizeIfNecessary">True if the buffer can be resized if the blob is larger. If false and the buffer is less than the size of the blob
    /// in bytes, it will read as many bytes as it can and skip the rest.</param>
    /// <returns>Number of values read, as defined by the data buffer's element size.</returns>
    /// <exception cref="ArgumentNullException">Thrown if the data buffer is null.</exception>
    public static int ReadBlob(this IPrimitiveReader reader, string name, IDataBuffer db, bool resizeIfNecessary = true)
    {
      ArgumentNullException.ThrowIfNull(db, nameof(db));

      PrimitiveBlobHeader? maybeBlobHeader = reader.PeekBlob(name);
      if (!maybeBlobHeader.HasValue)
      {
        // Skip over the null blob
        reader.ReadBlob<byte>(name, Span<byte>.Empty);
        return 0;
      }

      int blobSizeInBytes = maybeBlobHeader.Value.BlobSizeInBytes;
      if (resizeIfNecessary && db.SizeInBytes < blobSizeInBytes)
        db.Resize(BufferHelper.ComputeElementCount(blobSizeInBytes, db.ElementSizeInBytes));

      int bytesRead = reader.ReadBlob<byte>(name, db.Bytes);
      return BufferHelper.ComputeElementCount(bytesRead, db.ElementSizeInBytes);
    }

    /// <summary>
    /// Reads the blob into a data buffer that matches the stored type metadata. If the blob is null, this returns an empty (length 0)
    /// data buffer, where the element type is either the specified fallback type or simply <see cref="byte"/>.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <param name="allocatorStrategy">Optional memory allocator strategy.</param>
    /// <param name="fallbackType">Fallback type to use in case the blob is null, otherwise <see cref="byte"/> will be the type since there was no type information to be read.</param>
    /// <returns>Data buffer with the contents of the blob, or null if the blob is null.</returns>
    /// <exception cref="InvalidOperationException">Thrown if the blob type metadata size does not match the current runtime type size.</exception>
    public static IDataBuffer ReadBlob(this IPrimitiveReader reader, string name, MemoryAllocatorStrategy allocatorStrategy = MemoryAllocatorStrategy.Default, Type? fallbackType = null)
    {
      PrimitiveBlobHeader? maybeBlobHeader = reader.PeekBlob(name);
      if (!maybeBlobHeader.HasValue)
      {
        // Skip over the null blob
        reader.ReadBlob<byte>(name, Span<byte>.Empty);
        return DataBuffer.Create(0, fallbackType ?? typeof(byte), allocatorStrategy);
      }

      PrimitiveBlobHeader blobHeader = maybeBlobHeader.Value;
      int actualSizeInBytes = BufferHelper.SizeOf(blobHeader.ElementType);
      if (blobHeader.ElementSizeInBytes != actualSizeInBytes)
        throw new InvalidOperationException(StringLocalizer.Instance.GetLocalizedString("TypeSizeMismatch", actualSizeInBytes.ToString(), blobHeader.ElementSizeInBytes.ToString()));

      IDataBuffer db = DataBuffer.Create(blobHeader.ElementCount, blobHeader.ElementType, allocatorStrategy);
      reader.ReadBlob<byte>(name, db.Bytes);
      return db;
    }

    /// <summary>
    /// Allocates a data buffer of the specified type and reads the blob contents into it. If the blob is null, this returns an empty (length 0)
    /// data buffer. The type does not have to match the blob's type metadata, allowing the caller to reinterpret the data. As many bytes are read
    /// from the blob, and any remaining are skipped.
    /// </summary>
    /// <typeparam name="T">Blittable element type.</typeparam>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <param name="allocator">Optional memory allocator. If null, default memory allocation strategy is used.</param>
    /// <returns>Data buffer with the contents of the blob, if any.</returns>
    public static DataBuffer<T> ReadBlob<T>(this IPrimitiveReader reader, string name, IMemoryAllocator<T>? allocator = null) where T : unmanaged
    {
      int elemCount = reader.PeekBlobCount<T>(name);
      DataBuffer<T> db = DataBuffer.Create<T>(elemCount, allocator);
      reader.ReadBlob<T>(name, db.Span);

      return db;
    }

    /// <summary>
    /// Allocates a data buffer of the specified type and reads the blob contents into it. If the blob is null, this returns an empty (length 0)
    /// data buffer. The type does not have to match the blob's type metadata, allowing the caller to reinterpret the data. As many bytes are read
    /// from the blob, and any remaining are skipped.
    /// </summary>
    /// <typeparam name="T">Blittable element type.</typeparam>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <param name="allocatorStrategy">Memory allocator strategy.</param>
    /// <returns>Data buffer with the contents of the blob, if any.</returns>
    public static DataBuffer<T> ReadBlob<T>(this IPrimitiveReader reader, string name, MemoryAllocatorStrategy allocatorStrategy) where T : unmanaged
    {
      int elemCount = reader.PeekBlobCount<T>(name);
      DataBuffer<T> db = DataBuffer.Create<T>(elemCount, allocatorStrategy);
      reader.ReadBlob<T>(name, db.Span);

      return db;
    }

    /// <summary>
    /// Allocates a <see cref="Memory{T}"/> of the specified type and reads the blob contents into it. If the blob is null, this returns an
    /// empty (length 0) buffer. The type does not have to match the blob's type metadata, allowing the caller to reinterpret the data. As many bytes are read
    /// from the blob, and any remaining are skipped.
    /// </summary>
    /// <typeparam name="T">Blittable element type.</typeparam>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <returns>Buffer with the contents of the blob, if any.</returns>
    public static Memory<T> ReadBlobAsMemory<T>(this IPrimitiveReader reader, string name) where T : unmanaged
    {
      int elemCount = reader.PeekBlobCount<T>(name);
      T[] arr = (elemCount > 0) ? new T[elemCount] : Array.Empty<T>();
      reader.ReadBlob<T>(name, arr.AsSpan());

      return new Memory<T>(arr);
    }

    /// <summary>
    /// Allocates an array of the specified type and reads the blob contents into it. If the blob is null, this returns an
    /// empty (length 0) array. The type does not have to match the blob's type metadata, allowing the caller to reinterpret the data. As many bytes are read
    /// from the blob, and any remaining are skipped.
    /// </summary>
    /// <typeparam name="T">Blittable element type.</typeparam>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <returns>Array with the contents of the blob, if any.</returns>
    public static T[] ReadBlobAsArray<T>(this IPrimitiveReader reader, string name) where T : unmanaged
    {
      int elemCount = reader.PeekBlobCount<T>(name);
      T[] arr = (elemCount > 0) ? new T[elemCount] : Array.Empty<T>();
      reader.ReadBlob<T>(name, arr.AsSpan());

      return arr;
    }

    /// <summary>
    /// Tries to read a blob from the input. If the blob is null or empty, no buffer is allocated and this returns false. If it returns true,
    /// a buffer is allocated holding the contents of the blob.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <param name="db">Allocated data buffer, if the blob is not null.</param>
    /// <returns>True if the blob was not null, false if otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool TryReadBlob(this IPrimitiveReader reader, string name, [MaybeNullWhen(false)] out IDataBuffer db)
    {
      return reader.TryReadBlob(name, MemoryAllocatorStrategy.Default, out db);
    }

    /// <summary>
    /// Tries to read a blob from the input. If the blob is null or empty, no buffer is allocated and this returns false. If it returns true,
    /// a buffer is allocated holding the contents of the blob.
    /// </summary>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <param name="allocatorStrategy">Memory allocator strategy.</param>
    /// <param name="db">Allocated data buffer, if the blob is not null.</param>
    /// <returns>True if the blob was not null, false if otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool TryReadBlob(this IPrimitiveReader reader, string name, MemoryAllocatorStrategy allocatorStrategy, [MaybeNullWhen(false)] out IDataBuffer db)
    {
      PrimitiveBlobHeader? maybeBlobHeader = reader.PeekBlob(name);
      if (!maybeBlobHeader.HasValue)
      {
        // Skip over the null blob
        reader.ReadBlob<byte>(name, Span<byte>.Empty);

        db = null;
        return false;
      }

      PrimitiveBlobHeader blobHeader = maybeBlobHeader.Value;
      int actualSizeInBytes = BufferHelper.SizeOf(blobHeader.ElementType);
      if (blobHeader.ElementSizeInBytes != actualSizeInBytes)
        throw new InvalidOperationException(StringLocalizer.Instance.GetLocalizedString("TypeSizeMismatch", actualSizeInBytes.ToString(), blobHeader.ElementSizeInBytes.ToString()));

      db = DataBuffer.Create(blobHeader.ElementCount, blobHeader.ElementType, allocatorStrategy);
      reader.ReadBlob<byte>(name, db.Bytes);

      return true;
    }

    /// <summary>
    /// Tries to read a blob from the input. If the blob is null or empty, no buffer is allocated and this returns false. If it returns true,
    /// a buffer is allocated of the specified type. The type does not have to match the blob's type metadata, allowing the caller to reinterpret the data. As many bytes are read
    /// from the blob, and any remaining are skipped.
    /// </summary>
    /// <typeparam name="T">Blittable element type.</typeparam>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <param name="db">Allocated data buffer, if the blob is not null.</param>
    /// <returns>True if the blob was not null, false if otherwise.</returns>
    public static bool TryReadBlob<T>(this IPrimitiveReader reader, string name, [MaybeNullWhen(false)] out DataBuffer<T> db) where T : unmanaged
    {
      return reader.TryReadBlob<T>(name, MemoryAllocatorStrategy.Default, out db);
    }

    /// <summary>
    /// Tries to read a blob from the input. If the blob is null or empty, no buffer is allocated and this returns false. If it returns true,
    /// a buffer is allocated of the specified type. The type does not have to match the blob's type metadata, allowing the caller to reinterpret the data. As many bytes are read
    /// from the blob, and any remaining are skipped.
    /// </summary>
    /// <typeparam name="T">Blittable element type.</typeparam>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <param name="allocatorStrategy">Memory allocator strategy.</param>
    /// <param name="db">Allocated data buffer, if the blob is not null.</param>
    /// <returns>True if the blob was not null, false if otherwise.</returns>
    public static bool TryReadBlob<T>(this IPrimitiveReader reader, string name, MemoryAllocatorStrategy allocatorStrategy, [MaybeNullWhen(false)] out DataBuffer<T> db) where T : unmanaged
    {
      int elemCount = reader.PeekBlobCount<T>(name);
      if (elemCount == 0)
      {
        // Skip over the null blob
        reader.ReadBlob<byte>(name, Span<byte>.Empty);

        db = null;
        return false;
      }

      db = DataBuffer.Create<T>(elemCount, allocatorStrategy);
      reader.ReadBlob<T>(name, db.Span);

      return true;
    }

    /// <summary>
    /// Tries to read a blob from the input. If the blob is null or empty, no buffer is allocated and this returns false. If it returns true,
    /// a buffer is allocated of the specified type. The type does not have to match the blob's type metadata, allowing the caller to reinterpret the data. As many bytes are read
    /// from the blob, and any remaining are skipped.
    /// </summary>
    /// <typeparam name="T">Blittable element type.</typeparam>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <param name="allocator">Optional memory allocator. If null, default memory allocation strategy is used.</param>
    /// <param name="db">Allocated data buffer, if the blob is not null.</param>
    /// <returns>True if the blob was not null, false if otherwise.</returns>
    public static bool TryReadBlob<T>(this IPrimitiveReader reader, string name, IMemoryAllocator<T>? allocator, [MaybeNullWhen(false)] out DataBuffer<T> db) where T : unmanaged
    {
      int elemCount = reader.PeekBlobCount<T>(name);
      if (elemCount == 0)
      {
        // Skip over the null blob
        reader.ReadBlob<byte>(name, Span<byte>.Empty);
        db = null;
        return false;
      }

      db = DataBuffer.Create<T>(elemCount, allocator);
      reader.ReadBlob<T>(name, db.Span);

      return true;
    }

    /// <summary>
    /// Gets the persisted version for the specified type. If a value of -1 is returned, this indicates the type did not exist when the data was written.
    /// </summary>
    /// <typeparam name="T">Savable type.</typeparam>
    /// <param name="reader">Reader.</param>
    /// <returns>A version number that is 1 or greater. If the type was not persisted, then -1.</returns>
    public static int GetVersion<T>(this ISavableReader reader) where T : class, ISavable
    {
      return reader.GetVersion(typeof(T));
    }

    /// <summary>
    /// Gets the persisted version for the object. Note this may be dangerous to use in an object that is subclassed, if the base/subclass
    /// version varies. If each type may evolve, it's best to explicitly request the type rather than rely on the instance's type. 
    /// If a value of -1 is returned, this indicates the type did not exist when the data was written.
    /// </summary>
    /// <typeparam name="T">Savable type.</typeparam>
    /// <param name="reader">Reader.</param>
    /// <param name="obj">Savable instance.</param>
    /// <returns>A version number that is 1 or greater. If the type was not persisted, then -1.</returns>
    public static int GetVersion<T>(this ISavableReader reader, T obj) where T : class, ISavable
    {
      ArgumentNullException.ThrowIfNull(obj);

      return reader.GetVersion(obj.GetType());
    }

    /// <summary>
    /// Reads a savable object from the input - if it is null, throws a <see cref="ArgumentNullException"/>.
    /// </summary>
    /// <typeparam name="T">Type of object to read.</typeparam>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the object.</param>
    /// <returns>Read <see cref="ISavable"/> object.</returns>
    /// <exception cref="ArgumentNullException">Thrown if the value read from the input is null.</exception>
    public static T ReadSavableOrThrow<T>(this ISavableReader reader, string name) where T : class?, ISavable?
    {
      T? maybeValue = reader.ReadSavable<T>(name);
      ArgumentNullException.ThrowIfNull(maybeValue, name);

      return maybeValue;
    }

    /// <summary>
    /// Tries to read a savable object from the input - if it's null, returns false.
    /// </summary>
    /// <typeparam name="T">Type of object to read.</typeparam>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">Read <see cref="ISavable"/> object.</param>
    /// <returns>True if the object was non-null, false if it was null.</returns>
    public static bool TryReadSavable<T>(this ISavableReader reader, string name, [NotNullWhen(true)] out T value) where T : class?, ISavable?
    {
      T? maybeValue = reader.ReadSavable<T>(name);
      if (maybeValue is not null)
      {
        value = maybeValue;
        return true;
      }

      value = null!;
      return false;
    }

    /// <summary>
    /// Reads an array of savable objects from the input.
    /// </summary>
    /// <typeparam name="T">Type of object to read</typeparam>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the object.</param>
    /// <returns>Array of <see cref="ISavable"/> objects read.</returns>
    public static T[] ReadSavableArray<T>(this ISavableReader reader, string name) where T : class?, ISavable?
    {
      int count = reader.PeekArrayCount(name);
      T[] values = (count > 0) ? new T[count] : Array.Empty<T>();
      reader.ReadSavableArray<T>(name, values);

      return values;
    }

    /// <summary>
    /// Reads a shared savable object from the input - if it is null, throws a <see cref="ArgumentNullException"/>.
    /// </summary>
    /// <typeparam name="T">Type of object to read.</typeparam>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the object.</param>
    /// <returns>Read <see cref="ISavable"/> object.</returns>
    /// <exception cref="ArgumentNullException">Thrown if the value read from the input is null.</exception>
    public static T ReadSharedSavableOrThrow<T>(this ISavableReader reader, string name) where T : class?, ISavable?
    {
      T? maybeValue = reader.ReadSharedSavable<T>(name);
      ArgumentNullException.ThrowIfNull(maybeValue, name);

      return maybeValue;
    }

    /// <summary>
    /// Tries to read a shared savable object from the input - if it's null, returns false.
    /// </summary>
    /// <typeparam name="T">Type of object to read.</typeparam>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">Read <see cref="ISavable"/> object.</param>
    /// <returns>True if the object was non-null, false if it was null.</returns>
    public static bool TryReadSharedSavable<T>(this ISavableReader reader, string name, [NotNullWhen(true)] out T value) where T : class?, ISavable?
    {
      T? maybeValue = reader.ReadSharedSavable<T>(name);
      if (maybeValue is not null)
      {
        value = maybeValue;
        return true;
      }

      value = null!;
      return false;
    }

    /// <summary>
    /// Reads an array of shared savable objects from the input.
    /// </summary>
    /// <typeparam name="T">Type of object to read.</typeparam>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the object.</param>
    /// <returns>Array of <see cref="ISavable"/> objects read.</returns>
    public static T[] ReadSharedSavableArray<T>(this ISavableReader reader, string name) where T : class?, ISavable?
    {
      int count = reader.PeekArrayCount(name);
      T[] values = (count > 0) ? new T[count] : Array.Empty<T>();
      reader.ReadSharedSavableArray<T>(name, values);

      return values;
    }

    /// <summary>
    /// Reads a savable object that is external from the input, e.g. a separate file - if it is null, throws a <see cref="ArgumentNullException"/>.
    /// </summary>
    /// <typeparam name="T">Type of object to read.</typeparam>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the object.</param>
    /// <returns>Read <see cref="ISavable"/> object.</returns>
    /// <exception cref="ArgumentNullException">Thrown if the value read from the input is null.</exception>
    public static T ReadExternalSavableOrThrow<T>(this ISavableReader reader, string name) where T : class?, ISavable?
    {
      T? maybeValue = reader.ReadExternalSavable<T>(name);
      ArgumentNullException.ThrowIfNull(maybeValue, name);

      return maybeValue;
    }

    /// <summary>
    /// Reads a savable object that is external from the input, e.g. a separate file - if it's null, returns false.
    /// </summary>
    /// <typeparam name="T">Type of object to read.</typeparam>
    /// <param name="reader">Reader.</param>
    /// <param name="name">Name of the value.</param>
    /// <param name="value">Read <see cref="ISavable"/> object.</param>
    /// <returns>True if the object was non-null, false if it was null.</returns>
    public static bool TryReadExternalSavable<T>(this ISavableReader reader, string name, [NotNullWhen(true)] out T value) where T : class?, ISavable?
    {
      T? maybeValue = reader.ReadExternalSavable<T>(name);
      if (maybeValue is not null)
      {
        value = maybeValue;
        return true;
      }

      value = null!;
      return false;
    }

    /// <summary>
    /// Reads an array of savable objects that is external from the input, e.g. a separate file.
    /// </summary>
    /// <typeparam name="T">Type of object to read.</typeparam>
    /// <param name="name">Name of the object.</param>
    /// <returns>Array of <see cref="ISavable"/> objects read.</returns>
    public static T[] ReadExternalSavableArray<T>(this ISavableReader reader, string name) where T : class?, ISavable?
    {
      int count = reader.PeekArrayCount(name);
      T[] values = (count > 0) ? new T[count] : Array.Empty<T>();
      reader.ReadExternalSavableArray<T>(name, values);

      return values;
    }
  }
}
