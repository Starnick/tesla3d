﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.IO;

#nullable enable

namespace Tesla.Content
{
  /// <summary>
  /// A base resource exporter.
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public abstract class ResourceExporter<T> : IResourceExporter where T : class
  {
    private string m_extension;
    private Type m_targetType;

    /// <inheritdoc />
    public string Extension { get { return m_extension; } }

    /// <inheritdoc />
    public Type TargetType { get { return m_targetType; } }

    /// <summary>
    /// Constructs a new instance of the <see cref="ResourceExporter{T}"/> class.
    /// </summary>
    /// <param name="extension">The format extension this exporter saves files as.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the format is null or empty.</exception>
    protected ResourceExporter(string extension)
    {
      ArgumentNullException.ThrowIfNullOrEmpty(extension, nameof(extension));

      m_extension = extension;
      m_targetType = typeof(T);
    }

    /// <summary>
    /// Writes the specified content to a resource file.
    /// </summary>
    /// <param name="resourceFile">Resource file to write to</param>
    /// <param name="content">Content to write</param>
    public abstract void Save(IResourceFile resourceFile, T content);

    /// <summary>
    /// Writes the specified content to a stream.
    /// </summary>
    /// <param name="output">Output stream to write to</param>
    /// <param name="content">Content to write</param>
    public abstract void Save(Stream output, T content);

    /// <inheritdoc />
    /// <exception cref="System.ArgumentNullException">Thrown if the content item is null.</exception>
    /// <exception cref="System.InvalidCastException">Thrown if the type loaded cannot be casted to the target type.</exception>
    void IResourceExporter.Save(IResourceFile resourceFile, object content)
    {
      ArgumentNullException.ThrowIfNull(content, nameof(content));

      if (!CanSaveType(content.GetType()))
        throw new InvalidCastException(StringLocalizer.Instance.GetLocalizedString("TypeMismatch", m_targetType.FullName, typeof(T).FullName));

      Save(resourceFile, (T) content);
    }

    /// <inheritdoc />
    /// <exception cref="System.ArgumentNullException">Thrown if the content item is null.</exception>
    /// <exception cref="System.InvalidCastException">Thrown if the type loaded cannot be casted to the target type.</exception>
    void IResourceExporter.Save(Stream output, object content)
    {
      ArgumentNullException.ThrowIfNull(content, nameof(content));

      if (!CanSaveType(content.GetType()))
        throw new InvalidCastException(StringLocalizer.Instance.GetLocalizedString("TypeMismatch", m_targetType.FullName, typeof(T).FullName));

      Save(output, (T) content);
    }

    /// <summary>
    /// Returns true if the specified type can be handled by this exporter, false otherwise.
    /// </summary>
    /// <param name="contentType">Content type to be saved.</param>
    /// <returns>True if the type can be saved by this exporter, false otherwise.</returns>
    public virtual bool CanSaveType(Type contentType)
    {
      if (contentType is null)
        return false;

      //Most exporters will have a target type that will allow downcasting to more concrete types (e.g. Texture -> Texture2D)
      //but upcasting is also valid (Texture -> ISavable)
      return m_targetType.IsAssignableFrom(contentType) || contentType.IsAssignableFrom(m_targetType);
    }

    /// <summary>
    /// Validates input parameters - whether the resource file and resource to save are not null.
    /// </summary>
    /// <param name="resourceFile">Resource file to be created or to write to</param>
    /// <param name="content">Resource to save</param>
    /// <exception cref="ArgumentNullException">Thrown if either the resource file or content are null.</exception>
    protected void ValidateParameters(IResourceFile resourceFile, T content)
    {
      ArgumentNullException.ThrowIfNull(resourceFile, nameof(resourceFile));
      ArgumentNullException.ThrowIfNull(content, nameof(content));
    }

    /// <summary>
    /// Validates input parameters - whether the output stream or resource to save exist and if the stream is writable.
    /// </summary>
    /// <param name="output">Output stream to write to</param>
    /// <param name="content">Resource to save</param>
    /// <exception cref="ArgumentNullException">Thrown if output stream/content is null or if the stream is not writeable.</exception>
    protected void ValidateParameters(Stream output, T content)
    {
      if (output is null || !output.CanWrite)
        throw new ArgumentNullException(nameof(output), StringLocalizer.Instance.GetLocalizedString("CannotWriteToStream"));

      ArgumentNullException.ThrowIfNull(content, nameof(content));
    }
  }
}
