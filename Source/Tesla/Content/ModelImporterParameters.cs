﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;

namespace Tesla.Content
{
    /// <summary>
    /// Defines a set of common parameters for model importing.
    /// </summary>
    [SavableVersion(1)]
    public class ModelImporterParameters : ImporterParameters
    {
        //Material options
        private Dictionary<String, String> m_materialNameToMaterialFileScriptPathMapping;
        private String m_materialScriptFilePath;
        
        private MaterialImporterParameters m_matParameters;

        /// <summary>
        /// Gets or sets if the vertex winding order should be swapped (counter clockwise vs clockwise). Default is false.
        /// </summary>
        public bool SwapWindingOrder { get; set; }

        /// <summary>
        /// Gets or sets if UV texture coordinates should be flipped (the Y-coordinate). Default is false.
        /// </summary>
        public bool FlipUVs { get; set; }

        /// <summary>
        /// Gets or sets if lights should be imported if they are contained in the model
        /// file. Default is false.
        /// </summary>
        public bool ImportLights { get; set; }

        /// <summary>
        /// Gets or sets the model scale, which is baked into the vertex data. Default is 1.0f.
        /// </summary>
        public float Scale { get; set; }

        /// <summary>
        /// Gets or sets the angle at which the model should be rotated about the x-axis. This
        /// is baked into the vertex data. Default is <see cref="Angle.Zero"/>.
        /// </summary>
        public Angle XAngle { get; set; }

        /// <summary>
        /// Gets or sets the angle at which the model should be rotated about the y-axis. This
        /// is baked into the vertex data. Default is <see cref="Angle.Zero"/>.
        /// </summary>
        public Angle YAngle { get; set; }

        /// <summary>
        /// Gets or sets the angle at which the model should be rotated about the z-axis. This
        /// is baked into the vertex data. Default is <see cref="Angle.Zero"/>.
        /// </summary>
        public Angle ZAngle { get; set; }

        /// <summary>
        /// Gets or sets the options for calculating vertex normals. Default is
        /// <see cref="NormalGeneration.None"/>.
        /// </summary>
        /// <remarks>
        /// If no generation option is set, then existing normals will be preserved.
        /// </remarks>
        public NormalGeneration NormalGenerationOptions { get; set; }

        /// <summary>
        /// Gets or sets the angle used when crease normals are calculated. Default is 66 degrees.
        /// </summary>
        public Angle CreaseAngle { get; set; }

        /// <summary>
        /// Gets or sets the options for calculating the tangent basis at vertices. Default is <see cref="TangentBasisGeneration.None"/>.
        /// </summary>
        /// <remarks>
        /// At least one set of texture coordinates and normals (either existing
        /// or set to be computed) must exist for the tangent basis to be generated.
        /// </remarks>
        public TangentBasisGeneration TangentBasisGenerationOptions { get; set; }

        /// <summary>
        /// Gets or sets the index format the geometry should be converted. Default is <see cref="Tesla.IndexFormat.ThirtyTwoBits"/>.
        /// </summary>
        public IndexFormat IndexFormat { get; set; }

        /// <summary>
        /// Gets or sets if the lit standard materials should be preferred. If false, then the unlit versions will be set, if applicable. By default
        /// this is true.
        /// </summary>
        public bool PreferLitStandardMaterials { get; set; }
        
        /// <summary>
        /// Gets or sets the fully qualified name (optionally with a subresource name) to a resource to a material (or definition) file name that contains names that map
        /// to material names defined in the model file. This is compatible with with the explicit material mapping option (which will take precedence), but can
        /// be an easier and more automatic way to map materials if the material names are known. Default is <see cref="String.Empty"/>.
        /// </summary>
        public String MaterialScriptPath
        {
            get
            {
                return m_materialScriptFilePath;
            }
            set
            {
                if(value == null)
                    value = String.Empty;

                m_materialScriptFilePath = value;
            }
        }

        /// <summary>
        /// Gets the mapping of material names (defined in the model file) to material script file paths. The file path can be a fully qualified name to a resource file (TEM or TEMD file)
        /// and a subresource name. If <see cref="MaterialScriptPath"/> is set, the explicit mapping will take precedence.
        /// </summary>
        /// <remarks>
        /// This will override the materials with the specified names defined in the model file. If the path is to a material script (TEM file) then it will always be set to the
        /// Opaque render stage. It is generally more preferable to map material names to a complete material definition script (TEMD file) since you have more flexibility in configuring
        /// the entire pipeline for the meshes.
        /// </remarks>
        public Dictionary<String, String> MaterialsToMaterialScriptMapping
        {
            get
            {
                return m_materialNameToMaterialFileScriptPathMapping;
            }
        }

        /// <summary>
        /// Gets the importer parameters for importing materials and textures. Even if default materials are used
        /// this is still useful for setting texture paths and image parameters.
        /// </summary>
        public MaterialImporterParameters MaterialParameters
        {
            get
            {
                return m_matParameters;
            }
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="ModelImporterParameters"/> class.
        /// </summary>
        public ModelImporterParameters()
        {
            SwapWindingOrder = false;
            FlipUVs = false;
            ImportLights = false;
            Scale = 1.0f;
            XAngle = Angle.Zero;
            YAngle = Angle.Zero;
            ZAngle = Angle.Zero;

            NormalGenerationOptions = NormalGeneration.None;
            CreaseAngle = Angle.FromDegrees(66.0f);
            TangentBasisGenerationOptions = TangentBasisGeneration.None;
            IndexFormat = Tesla.IndexFormat.ThirtyTwoBits;

            PreferLitStandardMaterials = true;

            m_materialScriptFilePath = String.Empty;
            m_materialNameToMaterialFileScriptPathMapping = new Dictionary<String, String>();
            m_matParameters = new MaterialImporterParameters();
        }

        /// <summary>
        /// Copies the importer parameters from the specified instance.
        /// </summary>
        /// <param name="parameters">Importer parameter instance to copy from.</param>
        public override void Set(ImporterParameters parameters)
        {
            base.Set(parameters);

            ModelImporterParameters modelParams = parameters as ModelImporterParameters;

            if(modelParams == null)
                return;

            SwapWindingOrder = modelParams.SwapWindingOrder;
            FlipUVs = modelParams.FlipUVs;
            ImportLights = modelParams.ImportLights;
            Scale = modelParams.Scale;
            XAngle = modelParams.XAngle;
            YAngle = modelParams.YAngle;
            ZAngle = modelParams.ZAngle;

            NormalGenerationOptions = modelParams.NormalGenerationOptions;
            CreaseAngle = modelParams.CreaseAngle;
            TangentBasisGenerationOptions = modelParams.TangentBasisGenerationOptions;
            IndexFormat = modelParams.IndexFormat;

            PreferLitStandardMaterials = modelParams.PreferLitStandardMaterials;
            m_materialScriptFilePath = modelParams.m_materialScriptFilePath;

            m_materialNameToMaterialFileScriptPathMapping.Clear();
            foreach (KeyValuePair<String, String> kv in modelParams.m_materialNameToMaterialFileScriptPathMapping)
                m_materialNameToMaterialFileScriptPathMapping.Add(kv.Key, kv.Value);

            m_matParameters.Set(modelParams.m_matParameters);
        }

        /// <summary>
        /// Validates parameters.
        /// </summary>
        /// <param name="reason">Reason why the parameters are not valid.</param>
        /// <returns>True if the parameters are valid.</returns>
        public override bool Validate(out String reason)
        {
            reason = String.Empty;

            if(MathHelper.IsNearlyZero(Scale))
            {
                reason = StringLocalizer.Instance.GetLocalizedString("ModelScaleMustBeNonZero");
                return false;
            }

            return true;
        }

        /// <summary>
        /// Writes the object data to the output.
        /// </summary>
        /// <param name="output">Savable writer</param>
        public override void Write(ISavableWriter output)
        {
            base.Write(output);

            output.Write("SwapWindingOrder", SwapWindingOrder);
            output.Write("FlipUVs", FlipUVs);
            output.Write("ImportLights", ImportLights);
            output.Write("Scale", Scale);
            output.Write<Angle>("XAngle", XAngle);
            output.Write<Angle>("YAngle", YAngle);
            output.Write<Angle>("ZAngle", ZAngle);
            output.WriteEnum<NormalGeneration>("NormalGenerationOptions", NormalGenerationOptions);
            output.Write<Angle>("CreaseAngle", CreaseAngle);
            output.WriteEnum<TangentBasisGeneration>("TangentBasisGenerationOptions", TangentBasisGenerationOptions);
            output.WriteEnum<IndexFormat>("IndexFormat", IndexFormat);
            output.Write("PreferLitStandardMaterials", PreferLitStandardMaterials);
            output.Write("MaterialScriptFilePath", MaterialScriptPath);

            output.BeginWriteGroup("MaterialMappings", m_materialNameToMaterialFileScriptPathMapping.Count);

            foreach(KeyValuePair<String, String> kv in m_materialNameToMaterialFileScriptPathMapping)
            {
                output.BeginWriteGroup("Entry");

                output.Write("MaterialName", kv.Key);
                output.Write("ScriptName", kv.Value);

                output.EndWriteGroup();
            }

            output.EndWriteGroup();

            output.WriteSavable<MaterialImporterParameters>("MaterialParameters", m_matParameters);
        }

        /// <summary>
        /// Reads the object data from the input.
        /// </summary>
        /// <param name="input">Savable reader</param>
        public override void Read(ISavableReader input)
        {
            base.Read(input);

            SwapWindingOrder = input.ReadBoolean("SwapWindingOrder");
            FlipUVs = input.ReadBoolean("FlipUVs");
            ImportLights = input.ReadBoolean("ImportLights");
            Scale = input.ReadSingle("Scale");
            XAngle = input.Read<Angle>("XAngle");
            YAngle = input.Read<Angle>("YAngle");
            ZAngle = input.Read<Angle>("ZAngle");
            NormalGenerationOptions = input.ReadEnum<NormalGeneration>("NormalGenerationOptions");
            CreaseAngle = input.Read<Angle>("CreaseAngle");
            TangentBasisGenerationOptions = input.ReadEnum<TangentBasisGeneration>("TangentBasisGenerationOptions");
            IndexFormat = input.ReadEnum<IndexFormat>("IndexFormat");
            PreferLitStandardMaterials = input.ReadBoolean("PreferLitStandardMaterials");
            MaterialScriptPath = input.ReadString("MaterialScriptFilePath");

            int mapCount = input.BeginReadGroup("MaterialMappings");

            for(int i = 0; i < mapCount; i++)
            {
                input.BeginReadGroup("Entry");

                m_materialNameToMaterialFileScriptPathMapping.Add(input.ReadString("MaterialName"), input.ReadString("ScriptName"));

                input.EndReadGroup();
            }

            input.EndReadGroup();

            m_matParameters = input.ReadSavable<MaterialImporterParameters>("MaterialParameters");
        }
    }
}
