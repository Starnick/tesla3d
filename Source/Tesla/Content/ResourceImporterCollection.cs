﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections;
using System.Collections.Generic;

namespace Tesla.Content
{
    /// <summary>
    /// Represents a collection of resource importers that can be queried by extension and target type. The collection
    /// allows for multiple importers to be registered to a format extension, but there can only be one importer per format extension
    /// of any given type.
    /// </summary>
    //[DebuggerDisplay("Count = {Count}")]
    public sealed class ResourceImporterCollection : IEnumerable<IResourceImporter>
    {
        private MultiKeyDictionary<String, Type, IResourceImporter> m_extensionToImporters;

        /// <summary>
        /// Gets a resource importer registered to the extension and target type.
        /// </summary>
        /// <param name="extension">Format extension that the importer handles</param>
        /// <param name="targetType">Target type that the importer handles</param>
        /// <returns>The resource importer, or null if it is not found</returns>
        public IResourceImporter this[String extension, Type targetType]
        {
            get
            {
                if(String.IsNullOrEmpty(extension) || targetType == null)
                    return null;

                IResourceImporter importer;
                if (m_extensionToImporters.TryGetValue(extension, targetType, out importer))
                    return importer;

                return null;
            }
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="ResourceImporterCollection"/> class.
        /// </summary>
        public ResourceImporterCollection()
        {
            m_extensionToImporters = new MultiKeyDictionary<String, Type, IResourceImporter>(StringComparer.InvariantCultureIgnoreCase, null);
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="ResourceImporterCollection"/> class.
        /// </summary>
        /// <param name="importers">Importers to add to the collection</param>
        public ResourceImporterCollection(IEnumerable<IResourceImporter> importers)
        {
            m_extensionToImporters = new MultiKeyDictionary<String, Type, IResourceImporter>(StringComparer.InvariantCultureIgnoreCase, null);

            if (importers != null)
            {
                foreach(IResourceImporter importer in importers)
                    Add(importer);
            }
        }

        /// <summary>
        /// Adds the importer to the collection.
        /// </summary>
        /// <param name="importer">The importer to add</param>
        public void Add(IResourceImporter importer)
        {
            if(importer == null)
                return;

            IEnumerable<String> extensions = importer.Extensions;
            foreach(String ext in extensions)
                m_extensionToImporters.Add(ext, importer.TargetType, importer);
        }

        /// <summary>
        /// Removes an importer of the extension and target type.
        /// </summary>
        /// <param name="extension">Format extension that the importer handles</param>
        /// <param name="targetType">Target type that the importer handles</param>
        /// <returns>True if the importer was removed, false if it could not be found.</returns>
        public bool Remove(String extension, Type targetType)
        {
            if(String.IsNullOrEmpty(extension) || targetType == null)
                return false;

            return m_extensionToImporters.Remove(extension, targetType);
        }

        /// <summary>
        /// Clears the entire collection of importers.
        /// </summary>
        public void Clear()
        {
            m_extensionToImporters.Clear();
        }

        /// <summary>
        /// Clears all importers that are registered to the specified format extension.
        /// </summary>
        /// <param name="extension">Format extension that the importers handle.</param>
        public void Clear(String extension)
        {
            if(String.IsNullOrEmpty(extension))
                return;

            m_extensionToImporters.ClearMajorKeys(extension);
        }

        /// <summary>
        /// Clears all importers that handle the specified target type, this clears the importer from all
        /// format extension buckets.
        /// </summary>
        /// <param name="targetType">Target type that the importer handles</param>
        public void Clear(Type targetType)
        {
            m_extensionToImporters.ClearMinorKeys(targetType);
        }

        /// <summary>
        /// Queries if the collection contains a resource importer that is registered to the format extension and target type.
        /// </summary>
        /// <param name="extension">Format extension that the importer handles</param>
        /// <param name="targetType">Target type that the importer handles</param>
        /// <returns>True if a resource importer was found, false if it could not be.</returns>
        public bool Contains(String extension, Type targetType)
        {
            if(String.IsNullOrEmpty(extension) || targetType == null)
                return false;

            return m_extensionToImporters.ContainsKey(extension, targetType);
        }

        /// <summary>
        /// Queries the number of importers registered to the specified format extension.
        /// </summary>
        /// <param name="extension">Format extension</param>
        /// <returns>The number of importers registered to the extension.</returns>
        public int Count(String extension)
        {
            if(String.IsNullOrEmpty(extension))
                return 0;

            return m_extensionToImporters.QueryMinorKeyCount(extension);
        }

        /// <summary>
        /// Queries the number of importers that can handle the specified target type.
        /// </summary>
        /// <param name="targetType">Target type</param>
        /// <returns>The number of importers that can handle the target type.</returns>
        public int Count(Type targetType)
        {
            return m_extensionToImporters.QueryMajorKeyCount(targetType);
        }

        /// <summary>
        /// Gets the set of all importers contained in the collection.
        /// </summary>
        /// <returns>Set of importers</returns>
        public HashSet<IResourceImporter> GetImporters()
        {
            HashSet<IResourceImporter> importers = new HashSet<IResourceImporter>();

            foreach (KeyValuePair<MultiKey<String, Type>, IResourceImporter> kv in m_extensionToImporters)
                importers.Add(kv.Value);

            return importers;
        }

        /// <summary>
        /// Gets the set of importers registered to the specified format extension.
        /// </summary>
        /// <param name="extension">Format extension</param>
        /// <returns>Set of importers</returns>
        public HashSet<IResourceImporter> GetImporters(String extension)
        {
            HashSet<IResourceImporter> importers = new HashSet<IResourceImporter>();

            if (!String.IsNullOrEmpty(extension))
            {
                MultiKeyDictionary<String, Type, IResourceImporter>.MinorKeyValueEnumerator enumerator = m_extensionToImporters.GetMinorKeyValueEnumerator(extension);
                while (enumerator.MoveNext())
                {
                    KeyValuePair<Type, IResourceImporter> kv = enumerator.Current;
                    importers.Add(kv.Value);
                }
            }

            return importers;
        }

        /// <summary>
        /// Gets the set of importers that handle the specified target type.
        /// </summary>
        /// <param name="targetType">Target type</param>
        /// <returns>Enumerable collection of importers</returns>
        public HashSet<IResourceImporter> GetImporters(Type targetType)
        {
            return GetImporters(targetType, false);
        }

        /// <summary>
        /// Gets the set of importers that handle the specified target type.
        /// </summary>
        /// <param name="targetType">Target type</param>
        /// <param name="getOnlyRegisteredToType">Only return importers specifically registered to the type if true. If false, then importers
        /// that pass the <see cref="IResourceImporter.CanLoadType(Type)"/> check will be returned as well.</param>
        /// <returns>Set of importers</returns>
        public HashSet<IResourceImporter> GetImporters(Type targetType, bool getOnlyRegisteredToType)
        {
            HashSet<IResourceImporter> importers = new HashSet<IResourceImporter>();

            if (targetType != null)
            {
                foreach (KeyValuePair<MultiKey<String, Type>, IResourceImporter> kv in m_extensionToImporters)
                {
                    bool add = kv.Value.TargetType == targetType || (!getOnlyRegisteredToType && kv.Value.CanLoadType(targetType));

                    if(add)
                        importers.Add(kv.Value);
                }
            }

            return importers;
        }

        /// <summary>
        /// Finds a resource importer that can potentially load the format into the specified target type. The target type
        /// does not necessarily have to match the resource importer's, if the type to load can be casted to the importer's target type.
        /// </summary>
        /// <param name="extension">Format extension that the importer handles</param>
        /// <param name="targetType">The type that is expected to be loaded</param>
        /// <returns>A resource importer that can potentially load this type of resource, or null if none could be found.</returns>
        public IResourceImporter FindSuitableImporter(String extension, Type targetType)
        {
            if(String.IsNullOrEmpty(extension) || targetType == null)
                return null;

            //Query if we have a importer registered to the type
            IResourceImporter importer;
            if (m_extensionToImporters.TryGetValue(extension, targetType, out importer))
                return importer;

            //If that wasn't successful, search the collection to see if we can find one that can handle the type (first one)
            MultiKeyDictionary<String, Type, IResourceImporter>.MinorKeyValueEnumerator enumerator = m_extensionToImporters.GetMinorKeyValueEnumerator(extension);
            while(enumerator.MoveNext())
            {
                KeyValuePair<Type, IResourceImporter> kv = enumerator.Current;
                if (kv.Value.CanLoadType(targetType))
                    return kv.Value;
            }

            return null;
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.</returns>
        public Enumerator GetEnumerator()
        {
            return new Enumerator(m_extensionToImporters);
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.</returns>
        IEnumerator<IResourceImporter> IEnumerable<IResourceImporter>.GetEnumerator()
        {
            return new Enumerator(m_extensionToImporters);
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return new Enumerator(m_extensionToImporters);
        }

        public struct Enumerator : IEnumerator<IResourceImporter>
        {
            private MultiKeyDictionary<String, Type, IResourceImporter>.Enumerator m_enumerator;
            private IResourceImporter m_current;

            public IResourceImporter Current
            {
                get
                {
                    return m_current;
                }
            }

            Object IEnumerator.Current
            {
                get
                {
                    return m_current;
                }
            }

            internal Enumerator(MultiKeyDictionary<String, Type, IResourceImporter> collection)
            {
                m_enumerator = collection.GetEnumerator();
                m_current = null;
            }

            public bool MoveNext()
            {
                while(m_enumerator.MoveNext())
                {
                    IResourceImporter importer = m_enumerator.Current.Value;
                    if(!Object.ReferenceEquals(importer, m_current))
                    {
                        m_current = importer;
                        return true;
                    }
                }

                return false;
            }

            void IEnumerator.Reset() { }

            void IDisposable.Dispose() { }
        }
    }
}
