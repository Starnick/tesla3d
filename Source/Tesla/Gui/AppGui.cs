﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System.Runtime.CompilerServices;
using Tesla.Application;
using Tesla.Graphics;
using Tesla.Input;

#nullable enable

namespace Tesla.Gui
{
  /// <summary>
  /// Provides convienent access to a registered <see cref="IGuiSystem"/> service.
  /// </summary>
  public static class AppGui
  {
    /// <summary>
    /// Gets if the gui system is available.
    /// </summary>
    public static bool IsAvailable
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return CoreServices.GuiSystem.Service is not null;
      }
    }

    /// <summary>
    /// Gets if the Gui has mouse focus (e.g. capture mouse to move elements and not respond to game inputs).
    /// </summary>
    public static bool HasMouseFocus
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return (GetInputDeviceFocus() & InputDeviceFocus.Mouse) == InputDeviceFocus.Mouse;
      }
    }

    /// <summary>
    /// Gets if the Gui has keyboard focus (e.g. capture keyboard input and not respond to game inputs).
    /// </summary>
    public static bool HasKeyboardFocus
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return (GetInputDeviceFocus() & InputDeviceFocus.Keyboard) == InputDeviceFocus.Keyboard;
      }
    }

    /// <summary>
    /// Gets the Gui's font manager.
    /// </summary>
    public static IGuiFontManager FontManager
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return CoreServices.GuiSystem.ServiceOrThrow.FontManager;
      }
    }

    /// <inheritdoc cref="IGuiSystem.GetInputDeviceFocus(IWindow)"/>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static InputDeviceFocus GetInputDeviceFocus(IWindow? window = null)
    {
      IGuiSystem? guiSystem = CoreServices.GuiSystem.Service;
      if (guiSystem is null)
        return InputDeviceFocus.None;

      return guiSystem.GetInputDeviceFocus(window);
    }

    /// <inheritdoc cref="IGuiSystem.AddTexture(Texture2D, bool, bool)"/>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static GuiTextureID AddTexture(Texture2D texture, bool isPremultipliedAlpha, bool takeOwnership = true)
    {
      IGuiSystem? guiSystem = CoreServices.GuiSystem.Service;
      if (guiSystem is null)
        return GuiTextureID.Invalid;

      return guiSystem.AddTexture(texture, isPremultipliedAlpha, takeOwnership);
    }

    /// <inheritdoc cref="IGuiSystem.RemoveTexture(GuiTextureID, bool)"/>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Texture2D? GetTexture(GuiTextureID texId)
    {
      return CoreServices.GuiSystem.Service?.GetTexture(texId);
    }

    /// <inheritdoc cref="IGuiSystem.RemoveTexture(GuiTextureID, bool)"/>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Texture2D? RemoveTexture(GuiTextureID texId, bool dispose = true)
    {
      return CoreServices.GuiSystem.Service?.RemoveTexture(texId, dispose);
    }

    /// <inheritdoc cref="IGuiFontManager.PushFont(GuiFont)"/>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void PushFont(GuiFont font)
    {
      CoreServices.GuiSystem.Service?.FontManager.PushFont(font);
    }

    /// <inheritdoc cref="IGuiFontManager.PushFont(GuiFont)"/>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void PushFont(string fontFamily, int fontSize, FontWeight fontWeight = FontWeight.Normal, FontStyle fontStyle = FontStyle.Normal, FontWidth fontWidth = FontWidth.Normal)
    {
      CoreServices.GuiSystem.Service?.FontManager.PushFont(new GuiFont(fontFamily, fontSize, fontWeight, fontStyle, fontWidth));
    }

    /// <inheritdoc cref="IGuiFontManager.PopFont"/>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void PopFont()
    {
      CoreServices.GuiSystem.Service?.FontManager.PopFont();
    }

    /// <summary>
    /// Pops the next N-count of fonts off the font stack.
    /// </summary>
    /// <param name="count">Number of fonts to pop off the stack.</param>
    public static void PopFont(int count)
    {
      IGuiSystem? guiSystem = CoreServices.GuiSystem.Service;
      if (guiSystem is null || count < 1)
        return;

      for (int i = 0; i < count; i++)
        guiSystem.FontManager.PopFont();
    }

    /// <inheritdoc cref="IGuiSystem.StartOrResumeFrame(IWindow)"/>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void StartOrResumeFrame(IWindow window)
    {
      CoreServices.GuiSystem.Service?.StartOrResumeFrame(window);
    }

    
    /// <inheritdoc cref="IGuiSystem.Render(IRenderContext, IWindow)"/>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Render(IRenderContext renderContext, IWindow window)
    {
      CoreServices.GuiSystem.Service?.Render(renderContext, window);
    }
  }
}
