﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.Text;

#nullable enable

namespace Tesla.Gui
{
  /// <summary>
  /// Represents a hash key for a particular combination of font styling/weight/widths used to distinguish different fonts
  /// within a font family.
  /// </summary>
  public struct FontSetKey : IEquatable<FontSetKey>
  {
    /// <inheritdoc cref="GuiFont.Weight"/>
    public readonly FontWeight Weight;

    /// <inheritdoc cref="GuiFont.Style"/>
    public readonly FontStyle Style;

    /// <inheritdoc cref="GuiFont.Width"/>
    public readonly FontWidth Width;

    /// <summary>
    /// Constructs a new instance of the <see cref="FontSetKey"/> struct.
    /// </summary>
    /// <param name="weight">Font weight.</param>
    /// <param name="style">Font style.</param>
    /// <param name="width">Font width.</param>
    public FontSetKey(FontWeight weight, FontStyle style = FontStyle.Normal, FontWidth width = FontWidth.Normal)
    {
      Weight = weight;
      Style = style;
      Width = width;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="FontSetKey"/> struct.
    /// </summary>
    /// <param name="font">Font descriptor containing the styling information.</param>
    public FontSetKey(GuiFont font)
    {
      Weight = font.Weight;
      Style = font.Style;
      Width = font.Width;
    }

    /// <summary>
    /// Tests for inequality between two font styleset keys.
    /// </summary>
    /// <param name="a">First font styleset key.</param>
    /// <param name="b">Second font styleset key.</param>
    /// <returns>True if the two are not equal in value, false if otherwise.</returns>
    public static bool operator !=(FontSetKey a, FontSetKey b)
    {
      return !a.Equals(b);
    }

    /// <summary>
    /// Tests for equality between two font descriptors.
    /// </summary>
    /// <param name="a">First font descriptor.</param>
    /// <param name="b">Second font descriptor.</param>
    /// <returns>True if the two are equal in value, false if otherwise.</returns>
    public static bool operator ==(FontSetKey a, FontSetKey b)
    {
      return a.Equals(b);
    }

    /// <inheritdoc />
    public readonly override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is FontSetKey ss)
        return Equals(ss);

      return false;
    }

    /// <inheritdoc />
    public readonly bool Equals(FontSetKey other)
    {
      return Weight == other.Weight && Style == other.Style && Width == other.Width;
    }

    /// <inheritdoc />
    public readonly override int GetHashCode()
    {
      return HashCode.Combine<FontWeight, FontStyle, FontWidth>(Weight, Style, Width);
    }

    /// <inheritdoc />
    public readonly override string ToString()
    {
      return $"[Weight: {Weight}, Style: {Style}, Width: {Width}]";
    }
  }
}
