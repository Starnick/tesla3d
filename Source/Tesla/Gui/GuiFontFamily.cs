﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections;
using System.Collections.Generic;

#nullable enable

namespace Tesla.Gui
{
  /// <summary>
  /// Font family with associated fonts.
  /// </summary>
  public class GuiFontFamily : IEnumerable<GuiFont>
  {
    private string m_fontFamily;
    private Dictionary<FontSetKey, IReadOnlyList<GuiFont>> m_fonts;
    private int m_count;                                                           

    /// <summary>
    /// Gets the name of the font family.
    /// </summary>
    public string FontFamily { get { return m_fontFamily; } }

    /// <summary>
    /// Gets all associated fonts, by font style.
    /// </summary>
    public IReadOnlyDictionary<FontSetKey, IReadOnlyList<GuiFont>> Fonts { get { return m_fonts; } }

    /// <summary>
    /// Gets the number of associated fonts.
    /// </summary>
    public int Count { get { return m_count; } }

    /// <summary>
    /// Constructs a new instance of the <see cref="GuiFontFamily"/> class.
    /// </summary>
    /// <param name="fontFamily">Name of the fonts.</param>
    /// <param name="fonts">Associated fonts, organized by style as lists of pixel sizes.</param>
    public GuiFontFamily(string fontFamily, IReadOnlyDictionary<FontSetKey, IReadOnlyList<int>> fonts)
    {
      m_fontFamily = fontFamily;
      m_fonts = new Dictionary<FontSetKey, IReadOnlyList<GuiFont>>();
      m_count = 0;

      Comparison<GuiFont> sortFunc = (GuiFont a, GuiFont b) =>
      {
        return a.Size.CompareTo(b.Size);
      };

      foreach (KeyValuePair<FontSetKey, IReadOnlyList<int>> kv in fonts)
      {
        FontSetKey key = kv.Key;
        List<GuiFont> styleFonts = new List<GuiFont>();
        for (int i = 0; i < kv.Value.Count; i++)
          styleFonts.Add(new GuiFont(m_fontFamily, kv.Value[i], key.Weight, key.Style, key.Width));

        // Sort sizes
        styleFonts.Sort(sortFunc);
        m_fonts.Add(kv.Key, styleFonts);
        m_count += styleFonts.Count;
      }
    }

    /// <summary>
    /// Gets whether or not the specified style is available.
    /// </summary>
    /// <param name="fontStyle">Font style to search.</param>
    /// <returns>True if there's at least one font with the specified style, false otherwise.</returns>
    public bool IsStyleAvailable(FontSetKey fontStyle)
    {
      if (m_fonts.TryGetValue(fontStyle, out IReadOnlyList<GuiFont>? styleFonts))
        return styleFonts.Count > 0;

      return false;
    }

    /// <summary>
    /// Gets an enumerator to iterate over the collection.
    /// </summary>
    /// <returns>Font enumerator.</returns>
    public Enumerator GetEnumerator()
    {
      return new Enumerator(m_fonts);
    }

    /// <inheritdoc />
    IEnumerator<GuiFont> IEnumerable<GuiFont>.GetEnumerator()
    {
      return new Enumerator(m_fonts);
    }

    /// <inheritdoc />
    IEnumerator IEnumerable.GetEnumerator()
    {
      return new Enumerator(m_fonts);
    }

    /// <inheritdoc />
    public override string ToString()
    {
      return $"['{FontFamily}', Count = {m_count}]";
    }

    /// <summary>
    /// Font enumerator.
    /// </summary>
    public struct Enumerator : IEnumerator<GuiFont>
    {
      private Dictionary<FontSetKey, IReadOnlyList<GuiFont>>.Enumerator m_enumer;
      private IReadOnlyList<GuiFont>? m_currList;
      private int m_currIndex;

      /// <inheritdoc />
      public GuiFont Current
      {
        get
        {
          if (m_currList is null)
            return new GuiFont();

          return m_currList[m_currIndex];
        }
      }

      /// <inheritdoc />
      object IEnumerator.Current { get { return Current; } }

      internal Enumerator(Dictionary<FontSetKey, IReadOnlyList<GuiFont>> fonts)
      {
        m_enumer = fonts.GetEnumerator();
        m_currList = null;
        m_currIndex = 0;
      }

      /// <inheritdoc />
      public bool MoveNext()
      {
        if (m_currList is null)
        {
          if (!m_enumer.MoveNext())
            return false;

          m_currList = m_enumer.Current.Value;
          m_currIndex = 0;
          return true;
        }

        m_currIndex++;
        if (m_currIndex >= m_currList.Count)
        {
          m_currList = null;
          m_currIndex = 0;

          return MoveNext();
        }

        return true;
      }

      /// <inheritdoc />
      public void Dispose() { }

      /// <inheritdoc />
      public void Reset() { }
    }
  }
}
