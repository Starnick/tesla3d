﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;
using System.Text;

#nullable enable

namespace Tesla.Gui
{
  /// <summary>
  /// Describes a font that should be used with a <see cref="IGuiSystem"/>.
  /// </summary>
  public struct GuiFont : IEquatable<GuiFont>
  {
    /// <summary>
    /// Specifies the family this font is loaded from.
    /// </summary>
    public readonly string Family;

    /// <summary>
    /// Specifies the size of the font in pixels.
    /// </summary>
    public readonly int Size;

    /// <summary>
    /// Specifies the styling of the font (italic, outlines, etc).
    /// </summary>
    public readonly FontStyle Style;

    /// <summary>
    /// Specifies the weight of the font (lighter or bolder).
    /// </summary>
    public readonly FontWeight Weight;

    /// <summary>
    /// Specifies the width of the font (condensed or expanded).
    /// </summary>
    public readonly FontWidth Width;

    /// <summary>
    /// Gets whether or not this font descriptor is valid.
    /// </summary>
    public readonly bool IsValid { get { return !String.IsNullOrEmpty(Family) && Size > 0; } }

    /// <summary>
    /// Gets whether or not this font is the regular style (all styling options set to normal).
    /// </summary>
    public readonly bool IsRegular { get { return Weight == FontWeight.Normal && Style == FontStyle.Normal && Width == FontWidth.Normal; } }

    /// <summary>
    /// Gets whether or not this font is bold in weight (if available).
    /// </summary>
    public readonly bool IsBold { get { return ((int) Weight) >= (int)FontWeight.SemiBold; } }

    /// <summary>
    /// Gets whether or not this font is italicized in style (if available).
    /// </summary>
    public readonly bool IsItalic { get { return ((Style & FontStyle.Italic) == FontStyle.Italic) || ((Style & FontStyle.Oblique) == FontStyle.Oblique); } }

    /// <summary>
    /// Gets whether or not this font is both bold in weight and italicized in style (if available).
    /// </summary>
    public readonly bool IsBoldItalic { get { return IsBold && IsItalic; } }

    /// <summary>
    /// Gets whether or not this font has a thin border outline.
    /// </summary>
    public readonly bool HasOutline { get { return (Style & FontStyle.Outline) == FontStyle.Outline; } }

    /// <summary>
    /// Gets whether or not this font has a thick border outline.
    /// </summary>
    public readonly bool HasThickOutline { get { return (Style & FontStyle.ThickOutline) == FontStyle.ThickOutline; } }

    /// <summary>
    /// Constructs a new instance of the <see cref="GuiFont"/> struct.
    /// </summary>
    /// <param name="family">Font family.</param>
    /// <param name="size">Font size in pixels, by default 12px.</param>
    /// <param name="weight">Font weight, by default <see cref="FontWeight.Normal"/>.</param>
    /// <param name="style">Font style, by default <see cref="FontStyle.Normal"/>.</param>
    /// <param name="width">Font width, by default <see cref="FontWidth.Normal"/>.</param>
    public GuiFont(string family, int size = 12, FontWeight weight = FontWeight.Normal, FontStyle style = FontStyle.Normal, FontWidth width = FontWidth.Normal)
    {
      Family = family;
      Size = size;
      Weight = weight;
      Style = style;
      Width = width;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="GuiFont"/> struct with <see cref="FontWeight.Normal"/> weight.
    /// </summary>
    /// <param name="family">Font family.</param>
    /// <param name="size">Font size in pixels.</param>
    /// <param name="style">Font style.</param>
    /// <param name="width">Font width, by default <see cref="FontWidth.Normal"/>.</param>
    public GuiFont(string family, int size, FontStyle style, FontWidth width = FontWidth.Normal)
    {
      Family = family;
      Size = size;
      Weight = FontWeight.Normal;
      Style = style;
      Width = width;
    }

    /// <summary>
    /// Removes invalid styling options and returns a copy.
    /// </summary>
    /// <returns>Font description with valid styles.</returns>
    public readonly GuiFont RemoveInvalidStyles()
    {
      // If have italic, can't have oblique
      FontStyle style = Style;
      if ((style & FontStyle.Italic) == FontStyle.Italic && (style & FontStyle.Oblique) == FontStyle.Oblique)
        style &= ~FontStyle.Oblique;

      // If have thick outline, can't have outline
      if ((style & FontStyle.ThickOutline) == FontStyle.ThickOutline && (style & FontStyle.Outline) == FontStyle.Outline)
        style &= ~FontStyle.Outline;

      return WithStyle(style);
    }

    /// <summary>
    /// Copies the font descriptor, but with the new size.
    /// </summary>
    /// <param name="size">New font size.</param>
    /// <returns>Font descriptor.</returns>
    public readonly GuiFont AsSize(int size)
    {
      return new GuiFont(Family, size, Weight, Style, Width);
    }

    /// <summary>
    /// Copies the font descriptor, but using the specified weight.
    /// </summary>
    /// <param name="weight">New font weight.</param>
    /// <param name="sizeOverride">Optional size override.</param>
    /// <returns>Font descriptor.</returns>
    public readonly GuiFont WithStyle(FontWeight weight, int? sizeOverride = null)
    {
      return WithStyle(weight, Style, Width, sizeOverride);
    }

    /// <summary>
    /// Copies the font descriptor, but using the specified style.
    /// </summary>
    /// <param name="style">New font style.</param>
    /// <param name="sizeOverride">Optional size override.</param>
    /// <returns>Font descriptor.</returns>
    public readonly GuiFont WithStyle(FontStyle style, int? sizeOverride = null)
    {
      return WithStyle(Weight, style, Width, sizeOverride);
    }

    /// <summary>
    /// Copies the font descriptor, but using the specified weight/style.
    /// </summary>
    /// <param name="weight">New font weight.</param>
    /// <param name="style">New font style.</param>
    /// <param name="sizeOverride">Optional size override.</param>
    /// <returns>Font descriptor.</returns>
    public readonly GuiFont WithStyle(FontWeight weight, FontStyle style, int? sizeOverride = null)
    {
      return WithStyle(weight, style, Width, sizeOverride);
    }


    /// <summary>
    /// Copies the font descriptor, but using the specified weight/style/width.
    /// </summary>
    /// <param name="weight">New font weight.</param>
    /// <param name="style">New font style.</param>
    /// <param name="width">New font width.</param>
    /// <param name="sizeOverride">Optional size override.</param>
    /// <returns>Font descriptor.</returns>
    public readonly GuiFont WithStyle(FontWeight weight, FontStyle style, FontWidth width, int? sizeOverride = null)
    {
      return new GuiFont(Family, sizeOverride.HasValue ? sizeOverride.Value : Size, weight, style, width);
    }

    /// <summary>
    /// Copies the font descriptor, but as the regular weighted/styled font.
    /// </summary>
    /// <param name="sizeOverride">Optional size override.</param>
    /// <returns>Font descriptor.</returns>
    public readonly GuiFont AsRegular(int? sizeOverride = null)
    {
      return WithStyle(FontWeight.Normal, FontStyle.Normal, FontWidth.Normal, sizeOverride);
    }

    /// <summary>
    /// Copies the font descriptor, but with <see cref="FontWeight.Bold"/> weight.
    /// </summary>
    /// <param name="sizeOverride">Optional size override.</param>
    /// <returns>Font descriptor.</returns>
    public readonly GuiFont AsBold(int? sizeOverride = null)
    {
      return WithStyle(FontWeight.Bold, Style, Width, sizeOverride);
    }

    /// <summary>
    /// Copies the font descriptor, but with <see cref="FontStyle.Italic"/> styling.
    /// </summary>
    /// <param name="sizeOverride">Optional size override.</param>
    /// <returns>Font descriptor.</returns>
    public readonly GuiFont AsItalic(int? sizeOverride = null)
    {
      return WithStyle(Weight, FontStyle.Italic, Width, sizeOverride);
    }

    /// <summary>
    /// Copies the font descriptor, but with both <see cref="FontWeight.Bold"/> and <see cref="FontStyle.Italic"/> weight and style.
    /// </summary>
    /// <param name="sizeOverride">Optional size override.</param>
    /// <returns>Font descriptor.</returns>
    public readonly GuiFont AsBoldItalic(int? sizeOverride = null)
    {
      return WithStyle(FontWeight.Bold, FontStyle.Italic, Width, sizeOverride);
    }

    /// <summary>
    /// Copies the font descriptor, adding the <see cref="FontStyle.ThickOutline"/> to the styling.
    /// </summary>
    /// <param name="sizeOverride">Optional size override.</param>
    /// <returns>Font descriptor.</returns>
    public readonly GuiFont AsThickOutline(int? sizeOverride = null)
    {
      return WithStyle(Weight, FontStyle.ThickOutline | Style, Width, sizeOverride);
    }

    /// <summary>
    /// Copies the font descriptor, adding the <see cref="FontStyle.ThickOutline"/> to the styling.
    /// </summary>
    /// <param name="sizeOverride">Optional size override.</param>
    /// <returns>Font descriptor.</returns>
    public readonly GuiFont AsOutline(int? sizeOverride = null)
    {
      return WithStyle(Weight, FontStyle.Outline | Style, Width, sizeOverride);
    }

    /// <summary>
    /// Creates a new bold weighted <see cref="GuiFont"/>.
    /// </summary>
    /// <param name="family">Font family.</param>
    /// <param name="size">Font size in pixels.</param>
    /// <param name="style">Optional font style, default is <see cref="FontStyle.Normal"/>.</param>
    /// <param name="width">Optional font width, default is <see cref="FontWidth.Normal"/>.</param>
    /// <returns>Font descriptor.</returns>
    public static GuiFont Bold(string family, int size, FontStyle style = FontStyle.Normal, FontWidth width = FontWidth.Normal)
    {
      return new GuiFont(family, size, FontWeight.Bold, style, width);
    }

    /// <summary>
    /// Creates a new italic styled <see cref="GuiFont"/>.
    /// </summary>
    /// <param name="family">Font family.</param>
    /// <param name="size">Font size in pixels.</param>
    /// <param name="weight">Optional font weight, default is <see cref="FontWeight.Normal"/>.</param>
    /// <param name="width">Optional font width, default is <see cref="FontWidth.Normal"/>.</param>
    /// <returns>Font descriptor.</returns>
    public static GuiFont Italic(string family, int size, FontWeight weight = FontWeight.Normal, FontWidth width = FontWidth.Normal)
    {
      return new GuiFont(family, size, weight, FontStyle.Italic, width);
    }

    /// <summary>
    /// Creates a new bold weighted and italic styled <see cref="GuiFont"/>.
    /// </summary>
    /// <param name="family">Font family.</param>
    /// <param name="size">Font size in pixels.</param>
    /// <param name="width">Optional font width, default is <see cref="FontWidth.Normal"/>.</param>
    /// <returns>Font descriptor.</returns>
    public static GuiFont BoldItalic(string family, int size, FontWidth width = FontWidth.Normal)
    {
      return new GuiFont(family, size, FontWeight.Bold, FontStyle.Italic, width);
    }

    /// <summary>
    /// Creates a new oblique styled <see cref="GuiFont"/>.
    /// </summary>
    /// <param name="family">Font family.</param>
    /// <param name="size">Font size in pixels.</param>
    /// <param name="weight">Optional font weight, default is <see cref="FontWeight.Normal"/>.</param>
    /// <param name="width">Optional font width, default is <see cref="FontWidth.Normal"/>.</param>
    /// <returns>Font descriptor.</returns>
    public static GuiFont Oblique(string family, int size, FontWeight weight = FontWeight.Normal, FontWidth width = FontWidth.Normal)
    {
      return new GuiFont(family, size, weight, FontStyle.Oblique, width);
    }

    /// <summary>
    /// Creates a new thick outline styled <see cref="GuiFont"/>.
    /// </summary>
    /// <param name="family">Font family.</param>
    /// <param name="size">Font size in pixels.</param>
    /// <param name="weight">Optional font weight, default is <see cref="FontWeight.Normal"/>.</param>
    /// <param name="additionalStyling">Optionally can specify more styling options, default is <see cref="FontStyle.Normal"/>.</param>
    /// <param name="width">Optional font width, default is <see cref="FontWidth.Normal"/>.</param>
    /// <returns>Font descriptor.</returns>
    public static GuiFont ThickOutline(string family, int size, FontWeight weight = FontWeight.Normal, FontStyle additionalStyling = FontStyle.Normal, FontWidth width = FontWidth.Normal)
    {
      return new GuiFont(family, size, weight, FontStyle.ThickOutline | additionalStyling, width);
    }

    /// <summary>
    /// Creates a new outline styled <see cref="GuiFont"/>.
    /// </summary>
    /// <param name="family">Font family.</param>
    /// <param name="size">Font size in pixels.</param>
    /// <param name="weight">Optional font weight, default is <see cref="FontWeight.Normal"/>.</param>
    /// <param name="additionalStyling">Optionally can specify more styling options, default is <see cref="FontStyle.Normal"/>.</param>
    /// <param name="width">Optional font width, default is <see cref="FontWidth.Normal"/>.</param>
    /// <returns>Font descriptor.</returns>
    public static GuiFont Outline(string family, int size, FontWeight weight = FontWeight.Normal, FontStyle additionalStyling = FontStyle.Normal, FontWidth width = FontWidth.Normal)
    {
      return new GuiFont(family, size, weight, FontStyle.Outline | additionalStyling, width);
    }

    /// <summary>
    /// Tests for inequality between two font descriptors.
    /// </summary>
    /// <param name="a">First font descriptor.</param>
    /// <param name="b">Second font descriptor.</param>
    /// <returns>True if the two are not equal in value, false if otherwise.</returns>
    public static bool operator !=(GuiFont a, GuiFont b)
    {
      return !a.Equals(b);
    }

    /// <summary>
    /// Tests for equality between two font descriptors.
    /// </summary>
    /// <param name="a">First font descriptor.</param>
    /// <param name="b">Second font descriptor.</param>
    /// <returns>True if the two are equal in value, false if otherwise.</returns>
    public static bool operator ==(GuiFont a, GuiFont b)
    {
      return a.Equals(b);
    }

    /// <inheritdoc />
    public readonly override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is GuiFont gf)
        return Equals(gf);

      return false;
    }

    /// <inheritdoc />
    public readonly bool Equals(GuiFont other)
    {
      return Family == other.Family && Size == other.Size && Weight == other.Weight && Style == other.Style && Width == other.Width;
    }

    /// <inheritdoc />
    public readonly override int GetHashCode()
    {
      return HashCode.Combine<string, int, FontWeight, FontStyle, FontWidth>(Family, Size, Weight, Style, Width);
    }

    /// <inheritdoc />
    public readonly override string ToString()
    {
      StringBuilder builder = new StringBuilder();
      builder.Append("['");
      builder.Append(Family);
      builder.Append("', ");
      builder.Append(Size.ToString());

      if (Weight != FontWeight.Normal)
      {
        builder.Append(", ");
        builder.Append(Weight.ToString());
      }

      if (Style != FontStyle.Normal)
      {
        builder.Append(", ");
        builder.Append(Style.ToString());
      }

      if (Width != FontWidth.Normal)
      {
        builder.Append(", ");
        builder.Append(Width.ToString());
      }

      builder.Append("]");
      return builder.ToString();
    }
  }
}
