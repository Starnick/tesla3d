﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;

#nullable enable

namespace Tesla.Gui
{
  /// <summary>
  /// Enumerates various font weights.
  /// </summary>
  public enum FontWeight
  {
    /// <summary>
    /// A thin font weight of 100.
    /// </summary>
    Thin = 100,

    /// <summary>
    /// A thin font weight of 200.
    /// </summary>
    ExtraLight = 200,

    /// <summary>
    /// A thin font weight of 300.
    /// </summary>
    Light = 300,

    /// <summary>
    /// A thin font weight of 350.
    /// </summary>
    SemiLight = 350,

    /// <summary>
    /// A slightly thinner font weight than normal, mimics the weight commonly found in books.
    /// </summary>
    Book = 380,

    /// <summary>
    /// A typical font weight of 400, this is the default weight.
    /// </summary>
    Normal = 400,

    /// <summary>
    /// A thicker font weight of 500.
    /// </summary>
    Medium = 500,

    /// <summary>
    /// A thick font weight of 600.
    /// </summary>
    SemiBold = 600,

    /// <summary>
    /// A thick font weight of 700. This is the default weight for a bold font.
    /// </summary>
    Bold = 700,

    /// <summary>
    /// A thick font weight of 800.
    /// </summary>
    ExtraBold = 800,

    /// <summary>
    /// A thick font weight of 900.
    /// </summary>
    Black = 900,

    /// <summary>
    /// A thick font weight of 1000.
    /// </summary>
    ExtraBlack = 1000
  }

  /// <summary>
  /// Enumerates various font widths.
  /// </summary>
  public enum FontWidth
  {
    /// <summary>
    /// A condensed font width of 1. About 50% of normal.
    /// </summary>
    UltraCondensed = 1,

    /// <summary>
    /// A condensed font width of 2. About 62.5% of normal.
    /// </summary>
    ExtraCondensed = 2,

    /// <summary>
    /// A condensed font width of 3. About 75% of normal.
    /// </summary>
    Condensed = 3,

    /// <summary>
    /// A condensed font width of 4. About 87.5% of normal.
    /// </summary>
    SemiCondensed = 4,

    /// <summary>
    /// A normal font width of 5. This is the default width.
    /// </summary>
    Normal = 5,

    /// <summary>
    /// An expanded font width of 6. About 112.5% of normal.
    /// </summary>
    SemiExpanded = 6,

    /// <summary>
    /// An expanded font width of 7. About 125% of normal.
    /// </summary>
    Expanded = 7,

    /// <summary>
    /// An expanded font width of 8. About 150% of normal.
    /// </summary>
    ExtraExpanded = 8,

    /// <summary>
    /// An expanded font width of 9. About 200% of normal.
    /// </summary>
    UltraExpanded = 9
  }

  /// <summary>
  /// Enumerates styling flags for a font. Not all styling flags are compatible with each other.
  /// </summary>
  [Flags]
  public enum FontStyle
  {
    /// <summary>
    /// The normal styling of the font, no other styling specified. This is the default style.
    /// </summary>
    Normal = 0,

    /// <summary>
    /// The font's italic styling, if available. If both this and <see cref="Italic"/> is set, <see cref="Italic"/> is used instead.
    /// </summary>
    Italic = 1,

    /// <summary>
    /// The font's oblique styling, if available. If both this and <see cref="Italic"/> is set, <see cref="Italic"/> is used instead.
    /// </summary>
    Oblique = 2,

    /// <summary>
    /// Specifies the font should be drawn with a thin outline that is appropiate to it's weight. If both this and <see cref="ThickOutline"/> are set, <see cref="ThickOutline"/> is used instead.
    /// </summary>
    Outline = 4,

    /// <summary>
    /// Specifies the font should be drawn with a thick, heavy outline that is appropiate to it's weight. Small sizes or thin weights resemble thin outlines due to their small size.
    /// If both this and <see cref="Outline"/> are set, this overrides.
    /// </summary>
    ThickOutline = 8,

    /// <summary>
    /// DEBUG - Uses the embedded font style of the UI system to make comparisons
    /// </summary>
    Embedded = 16
  }

  /// <summary>
  /// Enumerates the status of font loading.
  /// </summary>
  public enum FontLoadStatus
  {
    /// <summary>
    /// Font is not loaded nor has been scheduled for loading.
    /// </summary>
    NotLoaded = 0,

    /// <summary>
    /// Font is fully loaded.
    /// </summary>
    Loaded = 1,

    /// <summary>
    /// Font was attempted to load but could not be found. A fallback font is used in this case, either the best available from the same font-family, or the system default.
    /// </summary>
    Missing = 2,

    /// <summary>
    /// Font has been scheduled to load on the next frame. The current frame will use the system default if the font is used.
    /// </summary>
    Scheduled = 3
  }
}
