﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using Tesla.Application;
using Tesla.Content;
using Tesla.Graphics;
using Tesla.Input;

#nullable enable

namespace Tesla.Gui
{
  /// <summary>
  /// <para>
  /// Represents a Gui system that is rendered within a <see cref="IWindow"/> using the <see cref="IRenderSystem"/>. This can be an Immediate-Mode Gui (IMGUI) or
  /// a retained Gui. In either case, at the start of a new frame inputs and other state is updated and UI elements are submitted during the normal update/render
  /// methods of the application. Then at the end of the frame, the UI elements are drawn as textured triangles / sprites.
  /// </para>
  /// <para>
  /// Each window represents a single distinct Gui context, so UI elements are submitted to the active context from the last <see cref="StartOrResumeFrame(IWindow)"/>
  /// invocation. At the end of the frame, if the Gui is not drawn, the frame automatically ends.
  /// </para>
  /// </summary>
  public interface IGuiSystem : IUpdatableEngineService, IDisposableEngineService
  {
    /// <summary>
    /// Gets the current <see cref="IGuiSystem"/> registered to the <see cref="Engine.Instance"/>.
    /// </summary>
    /// <exception cref="ArgumentNullException">Thrown if <see cref="Engine.Initialize"/> has yet to be called -or- the current instance is null.</exception>
    public static IGuiSystem Current { get { return CoreServices.GuiSystem.ServiceOrThrow; } }

    /// <summary>
    /// Gets the font manager that allows the system to load fonts to use in the UI.
    /// </summary>
    IGuiFontManager FontManager { get; }

    /// <summary>
    /// Gets the input device focus for the Gui based on the specified window. This alerts other sub systems that the Gui wants to capture
    /// the mouse or key input.
    /// </summary>
    /// <param name="window">Window that the Gui is drawing to. If null, then checks if any windows want device focus.</param>
    /// <returns>Device focus flags.</returns>
    InputDeviceFocus GetInputDeviceFocus(IWindow? window);

    /// <summary>
    /// Adds a texture that can be used by the Gui system. By default the texture is managed by
    /// the Gui until it is explicitly removed or when the system shuts down.
    /// </summary>
    /// <param name="texture">Texture to add.</param>
    /// <param name="isPremultipliedAlpha">Optionally specify if the texture has premultiplied alpha, by default, false.</param>
    /// <param name="takeOwnership">Optionally have the texture's lifetime managed by the system, by default true.</param>
    /// <returns>The texture ID that the Gui sysem will use to reference this texture.</returns>
    GuiTextureID AddTexture(Texture2D texture, bool isPremultipliedAlpha = false, bool takeOwnership = true);

    /// <summary>
    /// Gets a texture that is managed by the Gui system.
    /// </summary>
    /// <param name="texId">ID of a texture managed by the Gui system.</param>
    /// <returns>Texture object, if it exists.</returns>
    Texture2D? GetTexture(GuiTextureID texId);

    /// <summary>
    /// Removes the texture from the Gui's texture management.
    /// </summary>
    /// <param name="texId">Texture ID to remove.</param>
    /// <param name="dispose">Optionally dispose, by default true.</param>
    /// <returns>The texture object, if it was managed by the Gui.</returns>
    Texture2D? RemoveTexture(GuiTextureID texId, bool dispose = true);

    /// <summary>
    /// Applies the window context so that it is the active target to receive UI elements. If the frame has not yet started,
    /// it will start automatically, otherwise it merely resumes. This is safe to call frequently to ensure UI elements are received to their proper window.
    /// </summary>
    /// <param name="window">Window to receive UI elements.</param>
    void StartOrResumeFrame(IWindow window);

    /// <summary>
    /// Renders the contents of the Gui frame to the window. Usually this is the last thing drawn to the backbuffer before it is presented to the screen. If not
    /// called, when the system is next updated (e.g. OnIdle tick) the frame automatically ends.
    /// </summary>
    /// <param name="renderContext">Render context that will draw the UI elements.</param>
    /// <param name="window">Window to receive UI elements.</param>
    void Render(IRenderContext renderContext, IWindow window);
  }

  /// <summary>
  /// Manages fonts for an <see cref="IGuiSystem"/>.
  /// </summary>
  public interface IGuiFontManager
  {
    /// <summary>
    /// Gets the default font description.
    /// </summary>
    GuiFont DefaultFont { get; }

    /// <summary>
    /// Gets all currently loaded fonts, organized by family.
    /// </summary>
    IReadOnlyList<GuiFontFamily> LoadedFonts { get; }

    /// <summary>
    /// Gets the currently active from, manipulated via <see cref="PushFont(GuiFont)"/> and <see cref="PopFont"/>.
    /// </summary>
    GuiFont ActiveFont { get; }

    /// <summary>
    /// Adds a new font path to the manager to load font data from. If the repository is not yet opened, it's connection is managed by the manager.
    /// </summary>
    /// <param name="repo">Resource repository representing the font path.</param>
    void AddFontPath(IResourceRepository repo);

    /// <summary>
    /// Gets all the font paths the manager searches to load font data from.
    /// </summary>
    /// <returns>Font path collection.</returns>
    IEnumerable<IResourceRepository> GetFontPaths();

    /// <summary>
    /// Removes the font path from the manager. If the repository's connection was opened by the manager, this closes the connection.
    /// </summary>
    /// <param name="repo">Resource repository representing the font path.</param>
    void RemoveFontPath(IResourceRepository repo);

    /// <summary>
    /// Queries the current load status of the specified font. If the font is missing, the font rendering may fall back to a simpler style of the font (e.g. requested bold,
    /// but only regular exists) or the system-wide default font family/size. If scheduled to load next frame, the system default is used for this frame.
    /// </summary>
    /// <param name="font">Font description.</param>
    /// <returns>Load status.</returns>
    FontLoadStatus GetFontLoadStatus(GuiFont font);

    /// <summary>
    /// Tries to load the requested font. If there is a current Gui frame, this will be queued to load on the next frame. If a font is currently missing, this
    /// will attempt to reload the font.
    /// </summary>
    /// <param name="font">Font description.</param>
    /// <returns>Load status.</returns>
    FontLoadStatus LoadFont(GuiFont font);

    /// <summary>
    /// Tries to set the default font. The font must be successfully loaded beforehand. If no window is specified, this sets the default system-wide.
    /// </summary>
    /// <param name="font">Font description.</param>
    /// <returns>True if the font was made default, false if otherwise.</returns>
    bool TrySetDefaultFont(GuiFont font, IWindow? window = null);

    /// <summary>
    /// Gets the default font used by the window context. Normally this is <see cref="DefaultFont"/> unless if <see cref="TrySetDefaultFont(GuiFont, IWindow?)"/>
    /// was invoked with the window.
    /// </summary>
    /// <param name="window">Target window context.</param>
    /// <returns>Default font description.</returns>
    GuiFont GetDefaultFont(IWindow window);

    /// <summary>
    /// Pushes the specified font to the Gui font stack and to make active for subsequent UI elements. If the font is invalid or not 
    /// yet loaded, a default font is pushed instead. If the font is not yet loaded or failed to load, a fallback font is used instead. In the best case,
    /// the fallback is a simpler style (e.q. requested bold, but only regular exists). In the worst case, the default font family/size is used.
    /// </summary>
    /// <param name="font">Font description to make active.</param>
    void PushFont(GuiFont font);

    /// <summary>
    /// Pops the current font from the Gui font stack and makes the previous active. When the last font is popped, the default
    /// font is activated.
    /// </summary>
    void PopFont();
  }
}
