﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;
using Tesla.Graphics;

namespace Tesla.Gui
{
  /// <summary>
  /// Strongly types the ID used by a Gui for referencing textures. It is the <see cref="GraphicsResource.ResourceID"/> assigned
  /// to the <see cref="Texture"/> instance.
  /// </summary>
  public struct GuiTextureID : IEquatable<GuiTextureID>, IComparable<GuiTextureID>
  {
    /// <summary>
    /// Gets the invalid texture ID (value of 0).
    /// </summary>
    public static readonly GuiTextureID Invalid = new GuiTextureID(0);

    private int m_id;

    /// <summary>
    /// Constructs a new <see cref="GuiTextureID"/> instance.
    /// </summary>
    /// <param name="id">Resource ID of the texture.</param>
    public GuiTextureID(int id)
    {
      m_id = id;
    }

    /// <inheritdoc />
    public int CompareTo(GuiTextureID other)
    {
      return m_id.CompareTo(other.m_id);
    }

    /// <inheritdoc />
    public bool Equals(GuiTextureID other)
    {
      return m_id == other.m_id;
    }

    /// <inheritdoc />
    public override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is GuiTextureID)
      {
        GuiTextureID other = (GuiTextureID) obj;
        return m_id == other.m_id;
      }

      return false;
    }

    /// <inheritdoc />
    public override int GetHashCode()
    {
      return m_id.GetHashCode();
    }

    /// <inheritdoc />
    public override string ToString()
    {
      return $"ImGuiTexture {m_id.ToString()}";
    }

    /// <summary>
    /// Tests inequality between the two texture IDs.
    /// </summary>
    /// <param name="a">First ID.</param>
    /// <param name="b">Second ID.</param>
    /// <returns>True if not same ID, false otherwise.</returns>

    public static bool operator !=(GuiTextureID a, GuiTextureID b)
    {
      return a.m_id != b.m_id;
    }

    /// <summary>
    /// Tests equality between the two texture IDs.
    /// </summary>
    /// <param name="a">First ID.</param>
    /// <param name="b">Second ID.</param>
    /// <returns>True if same ID, false otherwise.</returns>
    public static bool operator ==(GuiTextureID a, GuiTextureID b)
    {
      return a.m_id == b.m_id;
    }

    /// <summary>
    /// Converts the ID to an IntPtr, which is used by ImGui.
    /// </summary>
    /// <param name="id">ID value.</param>
    public static implicit operator IntPtr(GuiTextureID id)
    {
      return new IntPtr(id.m_id);
    }

    /// <summary>
    /// Converts a resource ID value to the strongly typed texture id.
    /// </summary>
    /// <param name="id">ID value.</param>
    public static implicit operator GuiTextureID(int id)
    {
      return new GuiTextureID(id);
    }

    /// <summary>
    /// Extracts the resource ID value as a strongly typed texture id.
    /// </summary>
    /// <param name="tex">ID value.</param>
    public static implicit operator GuiTextureID(Texture tex)
    {
      return new GuiTextureID(tex?.ResourceID ?? 0);
    }

    /// <summary>
    /// Converts the native pointer value representing the texture id.
    /// </summary>
    /// <param name="id">ID value.</param>
    public static implicit operator GuiTextureID(IntPtr id)
    {
      return new GuiTextureID(id.ToInt32());
    }
  }
}
