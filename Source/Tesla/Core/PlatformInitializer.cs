﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Generic platform initializer that dynamically loads assemblies containing implementations of <see cref="IEngineService"/> and
  /// then registers them to the engine.
  /// </summary>
  public sealed class PlatformInitializer : IPlatformInitializer
  {
    private List<ServiceDescriptor> m_descriptors;

    /// <summary>
    /// Gets the list of descriptors.
    /// </summary>
    public IReadOnlyList<ServiceDescriptor> ServiceDescriptors { get { return m_descriptors; } }

    /// <summary>
    /// Constructs a new instance of the <see cref="PlatformInitializer"/> class.
    /// </summary>
    /// <param name="servicesToLoad">The services to dynamically load and register.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the collection is null.</exception>
    public PlatformInitializer(params ServiceDescriptor[] servicesToLoad)
    {
      if (servicesToLoad is null)
        throw new ArgumentNullException(nameof(servicesToLoad));

      m_descriptors = new List<ServiceDescriptor>(servicesToLoad);
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="PlatformInitializer"/> class.
    /// </summary>
    /// <param name="servicesToLoad">The services to dynamically load and register.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the collection is null.</exception>
    public PlatformInitializer(IEnumerable<ServiceDescriptor> servicesToLoad)
    {
      if (servicesToLoad is null)
        throw new ArgumentNullException(nameof(servicesToLoad));

      m_descriptors = new List<ServiceDescriptor>(servicesToLoad);
    }

    /// <summary>
    /// Adds the service descriptor to the initializer. A previous descriptor of the same interface type is removed if it is present.
    /// </summary>
    /// <param name="descr"></param>
    /// <param name="priority">Priority for the service, lower values are inserted at the beginning of the service list.</param>
    public void AddServiceDescriptor(ServiceDescriptor? descr, int priority = int.MaxValue)
    {
      if (descr is null)
        return;

      int index = RemoveServiceDescriptor(descr.ServiceTypeName);

      if (priority != int.MaxValue)
        index = MathHelper.Clamp(priority, 0, m_descriptors.Count);
      else if (index < 0 || index >= m_descriptors.Count)
        index = m_descriptors.Count;

      if (index == m_descriptors.Count)
        m_descriptors.Add(descr);
      else
        m_descriptors.Insert(index, descr);
    }

    /// <summary>
    /// Tries to remove a service descriptor given the fully qualified service name.
    /// </summary>
    /// <param name="serviceTypeName">Fully qualified service name.</param>
    /// <returns>Index that the service descriptor was located at, or -1 if not found.</returns>
    public int RemoveServiceDescriptor(string serviceTypeName)
    {
      int index = -1;
      for (int i = 0; i < m_descriptors.Count; i++)
      {
        if (m_descriptors[i].ServiceTypeName.Equals(serviceTypeName))
        {
          index = i;
          break;
        }
      }

      if (index >= 0)
        m_descriptors.RemoveAt(index);

      return index;
    }

    /// <summary>
    /// Adds the service descriptor to the initializer.
    /// </summary>
    /// <param name="type">Service descriptor enum.</param>
    /// <param name="priority">Priority for the service, lower values are inserted at the beginning of the service list.</param>
    public void AddServiceDescriptor(SubSystems.Gui type, int priority = int.MaxValue)
    {
      AddServiceDescriptor(Platforms.GetServiceDescriptor(type), priority);
    }

    /// <summary>
    /// Adds the service descriptor to the initializer. A previous descriptor of the same interface type is removed if it is present.
    /// </summary>
    /// <param name="type">Service descriptor enum.</param>
    /// <param name="priority">Priority for the service, lower values are inserted at the beginning of the service list.</param>
    public void AddServiceDescriptor(SubSystems.Graphics type, int priority = int.MaxValue)
    {
      AddServiceDescriptor(Platforms.GetServiceDescriptor(type), priority);
    }

    /// <summary>
    /// Adds the service descriptor to the initializer. A previous descriptor of the same interface type is removed if it is present.
    /// </summary>
    /// <param name="type">Service descriptor enum.</param>
    /// <param name="priority">Priority for the service, lower values are inserted at the beginning of the service list.</param>
    public void AddServiceDescriptor(SubSystems.Audio type, int priority = int.MaxValue)
    {
      AddServiceDescriptor(Platforms.GetServiceDescriptor(type), priority);
    }

    /// <summary>
    /// Adds the service descriptor to the initializer. A previous descriptor of the same interface type is removed if it is present.
    /// </summary>
    /// <param name="type">Service descriptor enum.</param>
    /// <param name="priority">Priority for the service, lower values are inserted at the beginning of the service list.</param>
    public void AddServiceDescriptor(SubSystems.Logging type, int priority = int.MaxValue)
    {
      AddServiceDescriptor(Platforms.GetServiceDescriptor(type), priority);
    }

    /// <summary>
    /// Initializes the platform's services.
    /// </summary>
    /// <param name="engine">Engine instance</param>
    /// <exception cref="TeslaException">Thrown if a service was unable to be loaded or if the type to load is not an engine service.</exception>
    public void Initialize(Engine engine)
    {
      foreach (ServiceDescriptor descriptor in m_descriptors)
      {
        if (descriptor is null)
          continue;

        Assembly assembly = Assembly.LoadFrom(descriptor.AssemblyName);

        Type? type = assembly.GetType(descriptor.TypeName);
        if (type is null)
          ThrowIfBadDescriptor(descriptor);

        Type? serviceType = Type.GetType(descriptor.ServiceTypeName);
        if (serviceType is null)
          ThrowIfBadDescriptor(descriptor);

        if (!typeof(IEngineService).IsAssignableFrom(serviceType))
          throw new TeslaException(StringLocalizer.Instance.GetLocalizedString("NotAnEngineService"));

        IEngineService? service;
        if (descriptor.ConstructorParameters is null || descriptor.ConstructorParameters.Length == 0)
          service = Activator.CreateInstance(type!) as IEngineService;
        else
          service = Activator.CreateInstance(type!, descriptor.ConstructorParameters) as IEngineService;

        if (service is null)
          throw new TeslaException(StringLocalizer.Instance.GetLocalizedString("NotAnEngineService"));

        engine.Services.AddService(serviceType, service);
      }
    }

    private void ThrowIfBadDescriptor(ServiceDescriptor descriptor)
    {
      throw new TeslaException(StringLocalizer.Instance.GetLocalizedString("UnableToLoadServiceDescriptor", descriptor.ServiceTypeName, descriptor.TypeName, descriptor.AssemblyName));
    }
  }
}
