﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Engine root class that manages core engine services such as rendering, windowing, input, etc. The class itself
  /// is a singleton, therefore individual services do not have to be implemented in such a manner.
  /// </summary>
  public sealed class Engine
  {
    private static Engine? s_instance;

    private EngineServiceRegistry m_services;

    /// <summary>
    /// Occurs when the engine is about to be initialized, before any resources have been initialized.
    /// </summary>
    public static event TypedEventHandler<Engine>? Initializing;

    /// <summary>
    /// Occurs when the engine has been initialized, after all resources have been initialized.
    /// </summary>
    public static event TypedEventHandler<Engine>? Initialized;

    /// <summary>
    /// Occurs when the engine is about to be destroyed, before any resource is disposed.
    /// </summary>
    public static event TypedEventHandler<Engine>? Destroying;

    /// <summary>
    /// Occurs when the engine has been destroyed, disposing of all resources.
    /// </summary>
    public static event TypedEventHandler<Engine>? Destroyed;

    /// <summary>
    /// Occurs when an engine service has been added or an existing service replaced.
    /// </summary>
    public static event TypedEventHandler<Engine, EngineServiceChangedEventArgs>? ServiceChanged;

    /// <summary>
    /// Gets the current engine instance.
    /// </summary>
    /// <exception cref="TeslaException">Thrown if <see cref="Engine.Initialize"/> is not yet called.</exception>
    public static Engine Instance
    {
      get
      {
        if (s_instance is null)
          throw new TeslaException(StringLocalizer.Instance.GetLocalizedString("EngineNotInitialized"));

        return s_instance;
      }
    }

    /// <summary>
    /// Gets if the engine has been initialized or not.
    /// </summary>
    public static bool IsInitialized
    {
      get
      {
        return s_instance is not null;
      }
    }

    /// <summary>
    /// Gets the engine service registry. The registry manages engine services, which are registered/queried by a type (usually an interface).
    /// </summary>
    public EngineServiceRegistry Services
    {
      get
      {
        return m_services;
      }
    }

    private Engine()
    {
      m_services = new EngineServiceRegistry(this);
    }

    /// <summary>
    /// Initializes the engine. This should be called on the main thread.
    /// </summary>
    /// <returns>The initialized engine instance.</returns>
    /// <exception cref="TeslaException">Thrown if the engine has already been initialized.</exception>
    public static Engine Initialize()
    {
      Engine instance = InitializeSingleton();

      OnInitializing(instance);
      OnInitialized(instance);

      return instance;
    }

    /// <summary>
    /// Initializes the engine. This takes in a platform plugin that initializes the engine with any number of engine service implementations.
    /// This should be called on the main thread.
    /// </summary>
    /// <param name="platform">Platform plugin</param>
    /// <returns>The initialized engine instance</returns>
    /// <exception cref="TeslaException">Thrown if the engine has already been initialized.</exception>
    /// <exception cref="ArgumentNullException">Thrown if the platform plugin is null.</exception>
    public static Engine Initialize(IPlatformInitializer platform)
    {
      Engine instance = InitializeSingleton();

      OnInitializing(instance);

      platform.Initialize(instance);

      OnInitialized(instance);

      return instance;
    }

    /// <summary>
    /// Destroys the engine and disposes of all resources. Services that are disposable are disposed here. This should be
    /// called on the main thread.
    /// </summary>
    /// <exception cref="TeslaException">Thrown if the engine has not been initialized.</exception>
    public static void Destroy()
    {
      if (s_instance is null)
        throw new TeslaException(StringLocalizer.Instance.GetLocalizedString("EngineNotInitialized"));

      try
      {
        //Before services are disposed, call any pre-destroy handlers
        OnDestroying(s_instance);

        // Destroy loggers last so they capture all our output
        List<ILoggingSystem> loggers = new List<ILoggingSystem>();
        s_instance.Services.RemoveAllAndDispose(loggers);

        //After services are all removed, call any destroy handlers
        OnDestroyed(s_instance);

        foreach (ILoggingSystem logger in loggers)
          logger.Dispose();
      }
      finally
      {
        s_instance = null;
      }
    }

    /// <summary>
    /// Engine per-frame update. All services that are upadatable will be called to have an opportunity to change their state
    /// or poll devices. This should be called on the main thread / update loop.
    /// </summary>
    /// <param name="time">Elapsed time since the last update.</param>
    public void Update(IGameTime time)
    {
      m_services.UpdateServices(time);
    }


    [MemberNotNull(nameof(s_instance))]
    private static Engine InitializeSingleton()
    {
      if (s_instance is not null)
        throw new TeslaException(StringLocalizer.Instance.GetLocalizedString("EngineAlreadyInitialized"));

      s_instance = new Engine();
      return s_instance;
    }

    private static void OnInitializing(Engine engine)
    {
      EngineLog.Log(LogLevel.Info, "Engine Starting Up...");

      Initializing?.Invoke(engine, EventArgs.Empty);
    }

    private static void OnInitialized(Engine engine)
    {
      EngineLog.Log(LogLevel.Info, "...Engine Initialized.");

      Initialized?.Invoke(engine, EventArgs.Empty);
    }

    private static void OnDestroying(Engine engine)
    {
      EngineLog.Log(LogLevel.Info, "Engine Shutting Down...");

      Destroying?.Invoke(engine, EventArgs.Empty);
    }

    private static void OnDestroyed(Engine engine)
    {
      EngineLog.Log(LogLevel.Info, "...Engine Destroyed.");

      Destroyed?.Invoke(engine, EventArgs.Empty);
    }

    internal void NotifyServiceChanged(EngineServiceChangedEventArgs args)
    {
      if (args.ChangeEventType == ServiceChangeEventType.Added)
        EngineLog.Log(LogLevel.Info, $"Added Engine Service: '{args.Service.Name}' [{args.ServiceType.ToString()}]");
      else
        EngineLog.Log(LogLevel.Info, $"Changed Engine Service: '{args.Service.Name}' [{args.ServiceType.ToString()}]");

      ServiceChanged?.Invoke(this, args);
    }
  }
}
