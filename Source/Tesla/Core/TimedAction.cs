﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Delegate for logic that is executed by a TimedAction, used in leui of a subclass.
  /// </summary>
  /// <param name="time">Time elapsed since the last update.</param>
  public delegate void TimedActionDelegate(IGameTime time);

  /// <summary>
  /// Represents logic that should be performed after a specified amount of time has passed.
  /// </summary>
  public class TimedAction
  {
    private TimeSpan m_lastUpdate;
    private TimeSpan m_updateInterval;
    private TimeSpan m_nextUpdate;
    private TimedActionDelegate? m_action;
    private bool m_triggerOnce;
    private bool m_hasTriggered;

    /// <summary>
    /// Gets or sets if the timed action should only be performed once.
    /// </summary>
    public bool TriggerOnce
    {
      get
      {
        return m_triggerOnce;
      }
      set
      {
        m_triggerOnce = value;
      }
    }

    /// <summary>
    /// Gets or sets if the timed action has already been performed once.
    /// </summary>
    public bool HasTriggered
    {
      get
      {
        return m_hasTriggered;
      }
      set
      {
        m_hasTriggered = value;
      }
    }

    /// <summary>
    /// Gets or sets the update interval of the action.
    /// </summary>
    public TimeSpan UpdateInterval
    {
      get
      {
        return m_updateInterval;
      }
      set
      {
        m_updateInterval = value;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="TimedAction"/> class.
    /// </summary>
    /// <param name="updateInterval">The update interval.</param>
    protected TimedAction(TimeSpan updateInterval)
    {
      m_lastUpdate = TimeSpan.Zero;
      m_updateInterval = updateInterval;
      m_nextUpdate = TimeSpan.Zero;

      m_triggerOnce = false;
      m_hasTriggered = false;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="TimedAction"/> class.
    /// </summary>
    /// <param name="updateInterval">Desired update interval that the action will be performed.</param>
    /// <param name="action">Action delegate.</param>
    /// <param name="setTriggered">Optional, set true if the action should already be set into a triggered state.</param>
    /// <exception cref="ArgumentNullException">Thrown if the action delegate is null.</exception>
    public TimedAction(TimeSpan updateInterval, TimedActionDelegate action, bool setTriggered = false)
    {
      ArgumentNullException.ThrowIfNull(action, nameof(action));

      m_lastUpdate = TimeSpan.Zero;
      m_updateInterval = updateInterval;
      m_nextUpdate = TimeSpan.Zero;
      m_action = action;

      m_triggerOnce = false;
      m_hasTriggered = setTriggered;
    }

    /// <summary>
    /// Resets the timed action to allow to be triggered again.
    /// </summary>
    /// <param name="lastUpdate">Time elapsed since the last update.</param>
    public void Reset(TimeSpan lastUpdate)
    {
      m_hasTriggered = false;
      m_lastUpdate = lastUpdate;
      m_nextUpdate = lastUpdate + m_updateInterval;
    }

    /// <summary>
    /// Resets the timed action to allow to be triggered again.
    /// </summary>
    /// <param name="time">Time elapsed since the last update.</param>
    public void Reset(IGameTime time)
    {
      m_hasTriggered = false;
      m_lastUpdate = time.TotalGameTime;
      m_nextUpdate = time.TotalGameTime + m_updateInterval;
    }

    /// <summary>
    /// Checks if the action is ready to be perfoemd, and if so performs it, then schedules the next update.
    /// </summary>
    /// <param name="time">Time elapsed since the last update.</param>
    /// <returns>True if the action has been performed, false otherwise.</returns>
    public bool CheckAndPerform(IGameTime time)
    {
      if (m_triggerOnce && m_hasTriggered)
        return false;

      TimeSpan now = time.TotalGameTime;

      if (now >= m_nextUpdate)
      {
        //Schedule next update
        m_lastUpdate = now;
        m_nextUpdate = now + m_updateInterval;

        Perform(time);

        m_hasTriggered = true;
        return true;
      }

      return false;
    }

    /// <summary>
    /// Performs the action.
    /// </summary>
    /// <param name="time">Time elapsed since the last update.</param>
    protected virtual void Perform(IGameTime time)
    {
      if (m_action is not null)
        m_action(time);
    }
  }
}
