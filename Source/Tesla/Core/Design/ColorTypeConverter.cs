﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.Design.Serialization;
using System.Globalization;
using System.Reflection;

namespace Tesla.Design
{
  public class ColorTypeConverter : MathTypeConverter
  {
    private static String[] s_expectedValues = new String[] { "R", "G", "B", "A" };

    public ColorTypeConverter() : base(true) { }

    public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
    {
      byte[] valueArray = ConvertToValues<byte>(context, culture, value, 4, s_expectedValues);
      if (valueArray is not null)
        return new Color(valueArray[0], valueArray[1], valueArray[2], valueArray[3]);

      return base.ConvertFrom(context, culture, value);
    }

    public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
    {
      ArgumentNullException.ThrowIfNull(destinationType, nameof(destinationType));

      if ((destinationType == typeof(string)) && (value is Color))
      {
        Color c = (Color) value;
        return ConvertFromValues<byte>(context, culture, new byte[] { c.R, c.G, c.B, c.A });
      }

      if ((destinationType == typeof(InstanceDescriptor)) && (value is Color))
      {
        Color c = (Color) value;
        ConstructorInfo cInfo = typeof(Color).GetConstructor(new Type[] { typeof(byte), typeof(byte), typeof(byte), typeof(byte) });
        if (cInfo is not null)
          return new InstanceDescriptor(cInfo, new Object[] { c.R, c.G, c.B, c.A });
      }

      return base.ConvertTo(context, culture, value, destinationType);
    }

    public override object CreateInstance(ITypeDescriptorContext context, IDictionary propertyValues)
    {
      return new Color((byte) propertyValues["R"], (byte) propertyValues["G"], (byte) propertyValues["B"], (byte) propertyValues["A"]);
    }

    protected override PropertyDescriptorCollection GeneratePropertyDescriptors()
    {
      Type type = typeof(Color);

      return new PropertyDescriptorCollection
      (
       new PropertyDescriptor[]
       {
        new FieldPropertyDescriptor(type.GetField("R")),
        new FieldPropertyDescriptor(type.GetField("G")),
        new FieldPropertyDescriptor(type.GetField("B")),
        new FieldPropertyDescriptor(type.GetField("A"))
       }
      );
    }
  }
}
