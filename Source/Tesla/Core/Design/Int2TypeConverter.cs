﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.Design.Serialization;
using System.Globalization;
using System.Reflection;

namespace Tesla.Design
{
  public class Int2TypeConverter : MathTypeConverter
  {
    private static String[] s_expectedValues = new String[] { "X", "Y" };

    public Int2TypeConverter() : base(true) { }

    public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
    {
      int[] valueArray = ConvertToValues<int>(context, culture, value, 2, s_expectedValues);
      if (valueArray is not null)
        return new Int2(valueArray[0], valueArray[1]);

      return base.ConvertFrom(context, culture, value);
    }

    public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
    {
      ArgumentNullException.ThrowIfNull(destinationType, nameof(destinationType));

      if ((destinationType == typeof(string)) && (value is Int2))
      {
        Int2 v = (Int2) value;
        return ConvertFromValues<int>(context, culture, new int[] { v.X, v.Y });
      }

      if ((destinationType == typeof(InstanceDescriptor)) && (value is Int2))
      {
        Int2 v = (Int2) value;
        ConstructorInfo cInfo = typeof(Int2).GetConstructor(new Type[] { typeof(int), typeof(int) });
        if (cInfo is not null)
          return new InstanceDescriptor(cInfo, new Object[] { v.X, v.Y });
      }

      return base.ConvertTo(context, culture, value, destinationType);
    }

    public override object CreateInstance(ITypeDescriptorContext context, IDictionary propertyValues)
    {
      return new Int2((int) propertyValues["X"], (int) propertyValues["Y"]);
    }

    protected override PropertyDescriptorCollection GeneratePropertyDescriptors()
    {
      Type type = typeof(Int2);

      return new PropertyDescriptorCollection
      (
       new PropertyDescriptor[]
       {
        new FieldPropertyDescriptor(type.GetField("X")),
        new FieldPropertyDescriptor(type.GetField("Y"))
       }
      );
    }
  }
}
