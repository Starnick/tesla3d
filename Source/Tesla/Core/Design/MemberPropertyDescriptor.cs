﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.ComponentModel;
using System.Reflection;

namespace Tesla.Design
{
  public abstract class MemberPropertyDescriptor : PropertyDescriptor
  {
    private MemberInfo m_memberInfo;
    private bool m_isReadOnly;

    public override bool IsReadOnly
    {
      get
      {
        return m_isReadOnly;
      }
    }

    public override Type ComponentType
    {
      get
      {
        return m_memberInfo.DeclaringType;
      }
    }

    public MemberPropertyDescriptor(MemberInfo member)
        : this(member, false)
    {
    }

    public MemberPropertyDescriptor(MemberInfo member, bool isReadOnly)
        : base(member.Name, (Attribute[]) member.GetCustomAttributes(typeof(Attribute), true))
    {
      m_memberInfo = member;
      m_isReadOnly = isReadOnly;
    }

    public override bool CanResetValue(object component)
    {
      return false;
    }

    public override void ResetValue(object component)
    {
    }

    public override bool Equals(object obj)
    {
      MemberPropertyDescriptor descr = obj as MemberPropertyDescriptor;
      if (descr is null)
        return false;

      return descr.m_memberInfo.Equals(m_memberInfo);
    }

    public override int GetHashCode()
    {
      return m_memberInfo.GetHashCode();
    }

    public override bool ShouldSerializeValue(object component)
    {
      return true;
    }
  }
}
