﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.Design.Serialization;
using System.Globalization;
using System.Reflection;

namespace Tesla.Design
{
  public class Vector3TypeConverter : MathTypeConverter
  {
    private static String[] s_expectedValues = new String[] { "X", "Y", "Z" };

    public Vector3TypeConverter() : base(true) { }

    public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
    {
      float[] valueArray = ConvertToValues<float>(context, culture, value, 3, s_expectedValues);
      if (valueArray is not null)
        return new Vector3(valueArray[0], valueArray[1], valueArray[2]);

      return base.ConvertFrom(context, culture, value);
    }

    public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
    {
      ArgumentNullException.ThrowIfNull(destinationType, nameof(destinationType));

      if ((destinationType == typeof(string)) && (value is Vector3))
      {
        Vector3 v = (Vector3) value;
        return ConvertFromValues<float>(context, culture, new float[] { v.X, v.Y, v.Z });
      }

      if ((destinationType == typeof(InstanceDescriptor)) && (value is Vector3))
      {
        Vector3 v = (Vector3) value;
        ConstructorInfo cInfo = typeof(Vector3).GetConstructor(new Type[] { typeof(float), typeof(float), typeof(float) });
        if (cInfo is not null)
          return new InstanceDescriptor(cInfo, new Object[] { v.X, v.Y, v.Z });
      }

      return base.ConvertTo(context, culture, value, destinationType);
    }

    public override object CreateInstance(ITypeDescriptorContext context, IDictionary propertyValues)
    {
      return new Vector3((float) propertyValues["X"], (float) propertyValues["Y"], (float) propertyValues["Z"]);
    }

    protected override PropertyDescriptorCollection GeneratePropertyDescriptors()
    {
      Type type = typeof(Vector3);

      return new PropertyDescriptorCollection
      (
       new PropertyDescriptor[]
       {
        new FieldPropertyDescriptor(type.GetField("X")),
        new FieldPropertyDescriptor(type.GetField("Y")),
        new FieldPropertyDescriptor(type.GetField("Z"))
       }
      );
    }
  }
}
