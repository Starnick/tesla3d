﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Reflection;

namespace Tesla.Design
{
  public sealed class FieldPropertyDescriptor : MemberPropertyDescriptor
  {
    private FieldInfo m_fieldInfo;

    public override Type PropertyType
    {
      get
      {
        return m_fieldInfo.FieldType;
      }
    }

    public FieldPropertyDescriptor(FieldInfo fieldInfo)
        : this(fieldInfo, false)
    {
    }

    public FieldPropertyDescriptor(FieldInfo fieldInfo, bool isReadOnly)
        : base(fieldInfo, isReadOnly)
    {
      m_fieldInfo = fieldInfo;
    }

    public override object GetValue(object component)
    {
      return m_fieldInfo.GetValue(component);
    }

    public override void SetValue(object component, object value)
    {
      m_fieldInfo.SetValue(component, value);
      OnValueChanged(component, EventArgs.Empty);
    }
  }
}
