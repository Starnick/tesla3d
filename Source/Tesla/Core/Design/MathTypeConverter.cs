﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.ComponentModel;
using System.ComponentModel.Design.Serialization;
using System.Globalization;

namespace Tesla.Design
{
  public abstract class MathTypeConverter : ExpandableObjectConverter
  {
    private PropertyDescriptorCollection m_propDescriptors;
    private bool m_supportStringConvert;

    protected MathTypeConverter(bool supportsStringConvert)
    {
      m_supportStringConvert = supportsStringConvert;
      m_propDescriptors = GeneratePropertyDescriptors();
    }

    public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
    {
      return (m_supportStringConvert && (sourceType == typeof(String))) || base.CanConvertFrom(context, sourceType);
    }

    public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
    {
      return (destinationType == typeof(InstanceDescriptor)) || base.CanConvertTo(context, destinationType);
    }

    public override bool GetCreateInstanceSupported(ITypeDescriptorContext context)
    {
      return true;
    }

    public override PropertyDescriptorCollection GetProperties(ITypeDescriptorContext context, object value, Attribute[] attributes)
    {
      return m_propDescriptors;
    }

    public override bool GetPropertiesSupported(ITypeDescriptorContext context)
    {
      return true;
    }

    protected abstract PropertyDescriptorCollection GeneratePropertyDescriptors();

    protected String ConvertFromValues<T>(ITypeDescriptorContext context, CultureInfo culture, T[] values)
    {
      if (values is null)
        return String.Empty;

      if (culture is null)
        culture = CultureInfo.CurrentCulture;

      String separator = culture.TextInfo.ListSeparator + " ";
      TypeConverter converter = TypeDescriptor.GetConverter(typeof(T));
      if (converter is null)
        return String.Empty;

      String[] stringArray = new String[values.Length];

      for (int i = 0; i < values.Length; i++)
        stringArray[i] = converter.ConvertToString(context, culture, values[i]);

      return String.Join(separator, stringArray);
    }

    protected T[] ConvertToValues<T>(ITypeDescriptorContext context, CultureInfo culture, object value, int arrayCount, params String[] expectedParams)
    {
      String valueString = value as String;
      if (valueString is null)
        return null;

      valueString = valueString.Trim();

      if (culture is null)
        culture = CultureInfo.CurrentCulture;

      String[] stringArray = valueString.Split(new String[] { culture.TextInfo.ListSeparator }, StringSplitOptions.None);
      T[] valueArray = new T[stringArray.Length];
      TypeConverter converter = TypeDescriptor.GetConverter(typeof(T));

      if (converter is null)
        return null;

      try
      {
        for (int i = 0; i < valueArray.Length; i++)
          valueArray[i] = (T) converter.ConvertFromString(context, culture, stringArray[i]);
      }
      catch (Exception exception)
      {
        throw new ArgumentException(StringLocalizer.Instance.GetLocalizedString("InvalidStringFormat", String.Join(culture.TextInfo.ListSeparator, expectedParams)), exception);
      }

      if (valueArray.Length != arrayCount)
        throw new ArgumentException(StringLocalizer.Instance.GetLocalizedString("InvalidStringFormat", String.Join(culture.TextInfo.ListSeparator, expectedParams)));

      return valueArray;
    }
  }
}
