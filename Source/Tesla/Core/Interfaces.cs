﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Interface for a core engine service, such as rendering, input, or windowing services.
  /// </summary>
  public interface IEngineService
  {
    /// <summary>
    /// Gets the name of the service.
    /// </summary>
    string Name { get; }

    /// <summary>
    /// Initializes the service. This is called by the engine when a service is newly registered.
    /// </summary>
    /// <param name="engine">Engine instance</param>
    void Initialize(Engine engine);
  }

  /// <summary>
  /// Interface for a core engine service that has resources that need to be cleaned up when the engine is destroyed.
  /// </summary>
  public interface IDisposableEngineService : IEngineService, IDisposable
  {
    /// <summary>
    /// Gets whether the service has been disposed or not.
    /// </summary>
    bool IsDisposed { get; }
  }

  /// <summary>
  /// Interface for a core engine service that can have its state updated. For example, an input service would poll the device to synchronize its
  /// internal state.
  /// </summary>
  public interface IUpdatableEngineService : IEngineService
  {
    /// <summary>
    /// Notifies the service to update its internal state.
    /// </summary>
    /// <param name="time">Elapsed time since the last update.</param>
    void Update(IGameTime time);
  }

  /// <summary>
  /// Interface for a core engine service that wants to be notified when another service has been added or replaced. Used in conjunction with
  /// <see cref="RequiredDependencyAttribute"/> and <see cref="OptionalDependencyAttribute"/>.
  /// </summary>
  public interface IEngineServiceDependencyChanged : IEngineService
  {
    /// <summary>
    /// Notifies the service the specified service was added or replaced.
    /// </summary>
    /// <param name="args">Service changed arguments.</param>
    void OnDependencyChanged(EngineServiceChangedEventArgs args);
  }

  /// <summary>
  /// Interface that will register all the necessary services to get fully setup and operational on a given platform. Its a logical container
  /// to easily package together different services together based on platform, which can have several configurations in itself.
  /// </summary>
  public interface IPlatformInitializer
  {
    /// <summary>
    /// Initializes the platform's services.
    /// </summary>
    /// <param name="engine">Engine instance</param>
    void Initialize(Engine engine);
  }

  /// <summary>
  /// Interface for objects that have a name.
  /// </summary>
  public interface INamed
  {
    /// <summary>
    /// Gets the name of the object.
    /// </summary>
    string Name { get; }
  }

  /// <summary>
  /// Interface for objects that has a name and can be renamed.
  /// </summary>
  public interface INamable : INamed
  {
    /// <summary>
    /// Gets or sets the name of the object.
    /// </summary>
    new string Name { get; set; }
  }

  /// <summary>
  /// Interface for objects that have a debug name, e.g. for a graphics debugger. It may not be a
  /// persistable name, like containing IDs that are only valid during the lifetime of the application.
  /// </summary>
  public interface IDebugNamable
  {
    /// <summary>
    /// Gets or sets the debug name of the object.
    /// </summary>
    string DebugName { get; set; }
  }

  /// <summary>
  /// Interface for objects that can have their data cloned deeply.
  /// </summary>
  public interface IDeepCloneable
  {
    /// <summary>
    /// Get a copy of the object.
    /// </summary>
    /// <returns>Cloned copy.</returns>
    IDeepCloneable Clone();
  }

  /// <summary>
  /// Interface for objects that contain a map of "extended properties".
  /// </summary>
  public interface IExtendedProperties
  {
    /// <summary>
    /// Gets the dictionary of extended properties.
    /// </summary>
    ExtendedPropertiesCollection ExtendedProperties { get; }
  }

  /// <summary>
  /// Interface for string localization.
  /// </summary>
  public interface IStringLocalizer
  {

    /// <summary>
    /// Gets a localized string from the supplied resource string.
    /// </summary>
    /// <param name="resourceString">Resource string representing the key for the localized string.</param>
    /// <returns>The localized string if it exists.</returns>
    string GetLocalizedString(string resourceString);

    /// <summary>
    /// Gets a localized string from the supplied resource string and parses an optional set of parameters to replace an expression in the localized string with the argument.
    /// </summary>
    /// <param name="resourceString">Resource string representing the key for the localized string.</param>
    /// <param name="args">Optional set of arguments.</param>
    /// <returns>The localized string if it exists.</returns>
    string GetLocalizedString(string resourceString, params string[] args);
  }

  #region GameTime

  /// <summary>
  /// Interface for a timer used for interpolation during simulation (the "game"). When the timer is updated, it returns a time snapshot - the elapsed
  /// time from the last update call, and the total time the simulation has been running. This does not necessarily reflect real time (wall clock), since
  /// the timer can be paused. 
  /// </summary>
  public interface IGameTimer
  {
    /// <summary>
    /// Gets the game time snapshot.
    /// </summary>
    IGameTime GameTime { get; }

    /// <summary>
    /// Gets the resolution of the timer (inverse of frequency).
    /// </summary>
    double Resolution { get; }

    /// <summary>
    /// Gets the frequency of the timer as ticks per second.
    /// </summary>
    long Frequency { get; }

    /// <summary>
    /// Gets if the timer is currently running.
    /// </summary>
    bool IsRunning { get; }

    /// <summary>
    /// Starts the timer.
    /// </summary>
    void Start();

    /// <summary>
    /// Pauses the timer.
    /// </summary>
    void Pause();

    /// <summary>
    /// Resumes the timer.
    /// </summary>
    void Resume();

    /// <summary>
    /// Resets the timer - the timer will be invalid until it is started again.
    /// </summary>
    void Reset();

    /// <summary>
    /// Advance and update the timer.
    /// </summary>
    /// <returns>Updated game time snapshot.</returns>
    IGameTime Update();
  }

  /// <summary>
  /// Interface for a snapshot of time during simulation. It is managed by a game timer.
  /// </summary>
  public interface IGameTime
  {
    /// <summary>
    /// Gets the elapsed time span since the last snapshot.
    /// </summary>
    TimeSpan ElapsedGameTime { get; }

    /// <summary>
    /// Gets the total time the simulation has been running.
    /// </summary>
    TimeSpan TotalGameTime { get; }

    /// <summary>
    /// Gets the elapsed time since the last snapshot in seconds.
    /// </summary>
    float ElapsedTimeInSeconds { get; }

    /// <summary>
    /// Gets the elapsed time since the last snapshot in seconds, in double precision.
    /// </summary>
    double ElapsedTimeInSecondsDouble { get; }

    /// <summary>
    /// Gets if the game update is running slowly, that is the actual
    /// elapsed time is greater than the target elapsed time.
    /// </summary>
    bool IsRunningSlowly { get; }
  }

  #endregion
}
