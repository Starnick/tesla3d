﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System.Collections.Generic;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Contains various enumerations of different sub-systems for a platform.
  /// </summary>
  public static class SubSystems
  {
    /// <summary>
    /// Enumerates application host types, which manage the message loop and create native windows from an OS.
    /// </summary>
    public enum Application
    {
      /// <summary>
      /// No host.
      /// </summary>
      None = 0,

      /// <summary>
      /// Windows OS that uses WinForms to back windows.
      /// </summary>
      WindowsForms
    }

    /// <summary>
    /// Enumerates the underlying graphics API used by the render system.
    /// </summary>
    public enum Graphics
    {
      /// <summary>
      /// No render system.
      /// </summary>
      None = 0,

      /// <summary>
      /// Direct3D11 render system, which is windows-only.
      /// </summary>
      Direct3D11 = 1
    }

    /// <summary>
    /// Enumerates what sort of input system to use.
    /// </summary>
    public enum Input
    {
      /// <summary>
      /// No input.
      /// </summary>
      None = 0,

      /// <summary>
      /// Polling system where each new update checks what keys/mouse state is current.
      /// </summary>
      Polling = 1,

      /// <summary>
      /// Event-based raw input.
      /// </summary>
      RawInput = 2
    }

    /// <summary>
    /// Enumerates what sort of audio system to use.
    /// </summary>
    public enum Audio
    {
      /// <summary>
      /// None.
      /// </summary>
      None = 0,

      /// <summary>
      /// XAudio2, which is windows-only.
      /// </summary>
      XAudio2 = 1
    }

    /// <summary>
    /// Enumerates what sort of logging system to use.
    /// </summary>
    public enum Logging
    {
      /// <summary>
      /// Null logging.
      /// </summary>
      None = 0,

      /// <summary>
      /// Outputs to a console, if available.
      /// </summary>
      Console = 1,

      /// <summary>
      /// Outputs to a file.
      /// </summary>
      File = 2,

      /// <summary>
      /// Outputs to memory.
      /// </summary>
      InMemory = 3
    }

    /// <summary>
    /// Enumerates what sort of Gui system to use.
    /// </summary>
    public enum Gui
    {
      /// <summary>
      /// No Gui system.
      /// </summary>
      None = 0,

      /// <summary>
      /// Dear ImGui.
      /// </summary>
      ImGui = 1
    }
  }

  /// <summary>
  /// Defines groups of platform initializers.
  /// </summary>
  public static class Platforms
  {
    /// <summary>
    /// Creates a windows platform initializer that defaults to <see cref="SubSystems.Graphics.Direct3D11"/> without any input/audio by default.
    /// </summary>
    /// <param name="graphics">Rendering subsystem type.</param>
    /// <param name="input">Type of input. Mouse/keyboard automatically added based on availability.</param>
    /// <param name="audio">Audio subystem type.</param>
    /// <returns>Platform initializer.</returns>
    public static PlatformInitializer CreateWindows(SubSystems.Graphics graphics = SubSystems.Graphics.Direct3D11, SubSystems.Input input = SubSystems.Input.None, SubSystems.Audio audio = SubSystems.Audio.None)
    {
      return Create(SubSystems.Application.WindowsForms, graphics, input, audio);
    }

    /// <summary>
    /// Creates a windows platform initializer that defaults to <see cref="SubSystems.Graphics.Direct3D11"/> and <see cref="SubSystems.Input.Polling"/> for mouse/keyboard.
    /// </summary>
    /// <param name="graphics">Rendering subsystem type.</param>
    /// <param name="input">Type of input. Mouse/keyboard automatically added based on availability.</param>
    /// <param name="audio">Audio subystem type.</param>
    /// <returns>Platform initializer.</returns>
    public static PlatformInitializer CreateWindowsStandardInput(SubSystems.Graphics graphics = SubSystems.Graphics.Direct3D11, SubSystems.Input input = SubSystems.Input.Polling, SubSystems.Audio audio = SubSystems.Audio.None)
    {
      return Create(SubSystems.Application.WindowsForms, graphics, input, audio);
    }

    /// <summary>
    /// Creates a platform initializer only with a render system. Mostly intended for unit-tests.
    /// </summary>
    /// <param name="graphics">Rendering subsystem type.</param>
    /// <returns>Platform initializer.</returns>
    public static PlatformInitializer CreateOnlyGraphics(SubSystems.Graphics graphics = SubSystems.Graphics.Direct3D11)
    {
      return Create(SubSystems.Application.None, graphics, SubSystems.Input.None, SubSystems.Audio.None);
    }

    /// <summary>
    /// Creates a platform initializer based on the sub-system type choices.
    /// </summary>
    /// <param name="app">Application subystem type. Controls windowing and input, if none then the engine runs in "headless" mode.</param>
    /// <param name="graphics">Rendering subsystem type.</param>
    /// <param name="input">Type of input. Mouse/keyboard automatically added based on availability.</param>
    /// <param name="audio">Audio subystem type.</param>
    /// <returns>Platform initializer.</returns>
    public static PlatformInitializer Create(SubSystems.Application app, SubSystems.Graphics graphics, SubSystems.Input input, SubSystems.Audio audio)
    {
      List<ServiceDescriptor> descriptors = new List<ServiceDescriptor>();

      ServiceDescriptor? descr = GetServiceDescriptor(app);
      if (descr is not null)
        descriptors.Add(descr);

      descr = GetServiceDescriptor(graphics);
      if (descr is not null)
        descriptors.Add(descr);

      descr = GetKeyboardServiceDescriptor(app, input);
      if (descr is not null)
        descriptors.Add(descr);

      descr = GetMouseServiceDescriptor(app, input);
      if (descr is not null)
        descriptors.Add(descr);

      descr = GetServiceDescriptor(audio);
      if (descr is not null)
        descriptors.Add(descr);

      return new PlatformInitializer(descriptors);
    }

    /// <summary>
    /// Gets a service descriptor for the given subsystem type.
    /// </summary>
    /// <param name="type">Subsystem type.</param>
    /// <returns>Service descriptor, or null if type is not specified.</returns>
    public static ServiceDescriptor? GetServiceDescriptor(SubSystems.Application type)
    {
      switch(type)
      {
        case SubSystems.Application.WindowsForms:
          return new ServiceDescriptor("Tesla.Windows.dll", "Tesla.Application.IApplicationHost", "Tesla.Windows.Application.SWFApplicationHost");
        default:
          return null;
      }
    }

    /// <summary>
    /// Gets a service descriptor for the given subsystem type.
    /// </summary>
    /// <param name="type">Subsystem type.</param>
    /// <returns>Service descriptor, or null if type is not specified.</returns>
    public static ServiceDescriptor? GetServiceDescriptor(SubSystems.Graphics type)
    {
      switch (type)
      {
        case SubSystems.Graphics.Direct3D11:
          return new ServiceDescriptor("Tesla.Direct3D11.dll", "Tesla.Graphics.IRenderSystem", "Tesla.Direct3D11.Graphics.D3D11RenderSystem");
        default:
          return null;
      }
    }

    /// <summary>
    /// Gets a service descriptor for the given subsystem type.
    /// </summary>
    /// <param name="type">Subsystem type.</param>
    /// <returns>Service descriptor, or null if type is not specified.</returns>
    public static ServiceDescriptor? GetServiceDescriptor(SubSystems.Audio type)
    {
      switch (type)
      {
        case SubSystems.Audio.XAudio2:
          return new ServiceDescriptor("Tesla.XAudio2.dll", "Tesla.Audio.IAudioSystem", "Tesla.XAudio2.Audio.X2AudioSystem");
        default:
          return null;
      }
    }

    /// <summary>
    /// Gets a service descriptor for the given subsystem type.
    /// </summary>
    /// <param name="type">Subsystem type.</param>
    /// <returns>Service descriptor, or null if type is not specified.</returns>
    public static ServiceDescriptor? GetServiceDescriptor(SubSystems.Logging type)
    {
      switch (type)
      {
        case SubSystems.Logging.None:
          return new ServiceDescriptor("Tesla.dll", "Tesla.ILoggingSystem", "Tesla.NullLoggingSystem");
        case SubSystems.Logging.Console:
          return new ServiceDescriptor("Tesla.dll", "Tesla.ILoggingSystem", "Tesla.ConsoleLoggingSystem");
        case SubSystems.Logging.InMemory:
          return new ServiceDescriptor("Tesla.dll", "Tesla.ILoggingSystem", "Tesla.InMemoryLoggingSystem");
        default:
          return null;
      }
    }

    /// <summary>
    /// Gets a service descriptor for the given subsystem type.
    /// </summary>
    /// <param name="type">Subsystem type.</param>
    /// <returns>Service descriptor, or null if type is not specified.</returns>
    public static ServiceDescriptor? GetServiceDescriptor(SubSystems.Gui type)
    {
      switch (type)
      {
        case SubSystems.Gui.ImGui:
          return new ServiceDescriptor("Tesla.ImGui.dll", "Tesla.Gui.IGuiSystem", "Tesla.Gui.ImGuiSystem");
        default:
          return null;
      }
    }

    /// <summary>
    /// Gets a service descriptor for the given subsystem type for a keyboard service.
    /// </summary>
    /// <param name="appType">App type.</param>
    /// <param name="type">Subsystem type.</param>
    /// <returns>Service descriptor, or null if type is not specified.</returns>
    public static ServiceDescriptor? GetKeyboardServiceDescriptor(SubSystems.Application appType, SubSystems.Input type)
    {
      if (appType != SubSystems.Application.WindowsForms)
        return null;

      switch (type)
      {
        case SubSystems.Input.Polling:
          return new ServiceDescriptor("Tesla.Windows.dll", "Tesla.Input.IKeyboardInputSystem", "Tesla.Windows.Input.Win32KeyboardInputSystem");
        case SubSystems.Input.RawInput:
          return new ServiceDescriptor("Tesla.Windows.dll", "Tesla.Input.IKeyboardInputSystem", "Tesla.Windows.Input.RawKeyboardInputSystem");
        default:
          return null;
      }
    }

    /// <summary>
    /// Gets a service descriptor for the given subsystem type for a mouse service.
    /// </summary>
    /// <param name="appType">App type.</param>
    /// <param name="type">Subsystem type.</param>
    /// <returns>Service descriptor, or null if type is not specified.</returns>
    public static ServiceDescriptor? GetMouseServiceDescriptor(SubSystems.Application appType, SubSystems.Input type)
    {
      if (appType != SubSystems.Application.WindowsForms)
        return null;

      switch (type)
      {
        case SubSystems.Input.Polling:
          return new ServiceDescriptor("Tesla.Windows.dll", "Tesla.Input.IMouseInputSystem", "Tesla.Windows.Input.Win32MouseInputSystem");
        case SubSystems.Input.RawInput:
          return new ServiceDescriptor("Tesla.Windows.dll", "Tesla.Input.IMouseInputSystem", "Tesla.Windows.Input.RawMouseInputSystem");
        default:
          return null;
      }
    }
  }
}
