﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using Tesla.Application;
using Tesla.Audio;
using Tesla.Graphics;
using Tesla.Input;
using Tesla.Gui;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Tracks core engine services for easy access and to ensure any static resources get cleaned up properly as the engine services are
  /// modified or the engine is destroyed.
  /// </summary>
  internal static class CoreServices
  {
    private static readonly ILoggingSystem s_consoleLoggingSystem = new ConsoleLoggingSystem();

    /// <summary>
    /// Gets the logging system or default console logging. Does not throw.
    /// </summary>
    public static ILoggingSystem LoggingSystemOrDefault { get { return LoggingSystem.Service ?? s_consoleLoggingSystem; } }

    /// <summary>
    /// Render system tracker.
    /// </summary>
    public static readonly EngineServiceTracker<IRenderSystem> RenderSystem = new EngineServiceTracker<IRenderSystem>();

    /// <summary>
    /// Logging system tracker.
    /// </summary>
    public static readonly EngineServiceTracker<ILoggingSystem> LoggingSystem = new EngineServiceTracker<ILoggingSystem>();

    /// <summary>
    /// Application host tracker.
    /// </summary>
    public static readonly EngineServiceTracker<IApplicationHost> ApplicationHost = new EngineServiceTracker<IApplicationHost>();

    /// <summary>
    /// Gui system tracker.
    /// </summary>
    public static readonly EngineServiceTracker<IGuiSystem> GuiSystem = new EngineServiceTracker<IGuiSystem>();

    /// <summary>
    /// Audio system tracker.
    /// </summary>
    public static readonly EngineServiceTracker<IAudioSystem> AudioSystem = new EngineServiceTracker<IAudioSystem>();

    /// <summary>
    /// Keyboard input tracker.
    /// </summary>
    public static readonly EngineServiceTracker<IKeyboardInputSystem> KeyboardInputSystem = new EngineServiceTracker<IKeyboardInputSystem>();

    /// <summary>
    /// Mouse input tracker.
    /// </summary>
    public static readonly EngineServiceTracker<IMouseInputSystem> MouseInputSystem = new EngineServiceTracker<IMouseInputSystem>();
  }
}
