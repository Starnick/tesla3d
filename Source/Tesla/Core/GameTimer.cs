﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// A timer used for interpolation during simulation (the "game"). When the timer is updated, it returns a time snapshot - the elapsed
  /// time from the last update call, and the total time the simulation has been running. This does not necessarily reflect real time (wall clock), since
  /// the timer can be paused. 
  /// </summary>
  public sealed class GameTimer : IGameTimer
  {
    private GameTime m_gameTime;

    private long m_startTime;
    private long m_prevTime;

    private bool m_isRunning;
    private bool m_wasPaused;

    private long m_pauseStart;
    private long m_timePaused;

    private long m_frequency;

    /// <summary>
    /// Gets the game time snapshot.
    /// </summary>
    public IGameTime GameTime
    {
      get
      {
        return m_gameTime;
      }
    }

    /// <summary>
    /// Gets the resolution of the timer (inverse of frequency).
    /// </summary>
    public double Resolution
    {
      get
      {
        return 1.0 / (double) m_frequency;
      }
    }

    /// <summary>
    /// Gets the frequency of the timer as ticks per second.
    /// </summary>
    public long Frequency
    {
      get
      {
        return m_frequency;
      }
    }

    /// <summary>
    /// Gets if the timer is currently running.
    /// </summary>
    public bool IsRunning
    {
      get
      {
        return m_isRunning;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="GameTimer"/> class.
    /// </summary>
    public GameTimer()
    {
      m_gameTime = new GameTime(TimeSpan.Zero, TimeSpan.Zero);
      m_frequency = Stopwatch.Frequency;
      Reset();
    }

    /// <summary>
    /// Starts the timer.
    /// </summary>
    public void Start()
    {
      if (!m_isRunning)
      {
        Reset();
        m_prevTime = Stopwatch.GetTimestamp();
        m_startTime = m_prevTime;
        m_isRunning = true;
      }
    }

    /// <summary>
    /// Pauses the timer.
    /// </summary>
    public void Pause()
    {
      if (m_isRunning)
      {
        m_isRunning = false;
        m_wasPaused = false;

        m_pauseStart = Stopwatch.GetTimestamp();
      }
    }

    /// <summary>
    /// Resumes the timer.
    /// </summary>
    public void Resume()
    {
      if (!m_isRunning)
      {
        m_isRunning = true;
        m_wasPaused = true;

        m_timePaused += Stopwatch.GetTimestamp() - m_pauseStart;
        m_pauseStart = 0L;
      }
    }

    /// <summary>
    /// Resets the timer - the timer will be invalid until it is started again.
    /// </summary>
    public void Reset()
    {
      m_isRunning = m_wasPaused = false;
      m_prevTime = m_startTime = m_pauseStart = m_timePaused = 0L;
    }

    /// <summary>
    /// Advance and update the timer.
    /// </summary>
    /// <returns>Updated game time snapshot.</returns>
    public IGameTime Update()
    {
      long currTime = Stopwatch.GetTimestamp();

      if (!m_isRunning)
      {
        m_gameTime.ElapsedGameTime = TimeSpan.Zero;
        return m_gameTime;
      }
      else if (m_wasPaused)
      {
        TimeSpan elapsedTime = CreateTimeSpan(currTime - (m_prevTime + m_timePaused));
        m_timePaused = 0L;
        m_gameTime.ElapsedGameTime = elapsedTime;
        m_gameTime.TotalGameTime += elapsedTime;
        m_prevTime = currTime;
        m_wasPaused = false;
        return m_gameTime;
      }
      else
      {
        TimeSpan elapsedTime = CreateTimeSpan(currTime - m_prevTime);
        m_gameTime.ElapsedGameTime = elapsedTime;
        m_gameTime.TotalGameTime += elapsedTime;

        m_prevTime = currTime;
        return m_gameTime;
      }
    }

    private TimeSpan CreateTimeSpan(long ticks)
    {
      long time = (ticks * 10000000L) / m_frequency;
      return TimeSpan.FromTicks(time);
    }
  }
}
