﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Globalization;
using System.Reflection;
using System.Resources;
using System.Text.RegularExpressions;

namespace Tesla
{
  /// <summary>
  /// Base class for a string localizer. Arguments that can be replaced in the localized string
  /// take the format of <code>%{index} where {index} is a number that matches the index of the argument value</code>.
  /// </summary>
  public abstract class BaseStringLocalizer : IStringLocalizer
  {
    private CultureInfo m_cultureInfo;
    private ResourceManager m_resourceManager;

    /// <summary>
    /// Constructs a new instance of the <see cref="BaseStringLocalizer"/> class.
    /// </summary>
    /// <param name="baseName">Full path to the localizable string resource.</param>
    /// <param name="assembly">Assembly the resource resides in.</param>
    protected BaseStringLocalizer(String baseName, Assembly assembly)
    {
      m_resourceManager = new ResourceManager(baseName, assembly);
      m_cultureInfo = new CultureInfo(CultureInfo.CurrentUICulture.Name);
    }

    /// <summary>
    /// Gets a localized string from the supplied resource string.
    /// </summary>
    /// <param name="resourceString">Resource string representing the key for the localized string.</param>
    /// <returns>The localized string if it exists.</returns>
    public String GetLocalizedString(String resourceString)
    {
      String toReturn = null;

      if (m_resourceManager != null && m_cultureInfo != null)
        toReturn = m_resourceManager.GetString(resourceString, m_cultureInfo);

      if (toReturn == null)
        toReturn = "<-" + resourceString + "->";

      return toReturn;
    }

    /// <summary>
    /// Gets a localized string from the supplied resource string and parses an optional set of parameters to replace an expression in the localized string with the argument.
    /// </summary>
    /// <param name="resourceString">Resource string representing the key for the localized string.</param>
    /// <param name="args">Optional set of arguments.</param>
    /// <returns>The localized string if it exists.</returns>
    public String GetLocalizedString(String resourceString, params String[] args)
    {
      String toReturn = null;

      if (m_resourceManager != null && m_cultureInfo != null)
      {
        toReturn = m_resourceManager.GetString(resourceString, m_cultureInfo);
        toReturn = ParseArguments(toReturn, args);
      }

      if (toReturn == null)
        toReturn = "<-" + resourceString + "->";

      return toReturn;
    }

    /// <summary>
    /// Parses the arguments from the string, replacing them with the corresponding value in the argument list.
    /// </summary>
    /// <param name="value">String containing arguments.</param>
    /// <param name="args">Argument list containing replacement values.</param>
    /// <returns>The string</returns>
    protected virtual String ParseArguments(String value, String[] args)
    {
      if (args == null)
        return value;

      for (int i = 0; i <= args.Length - 1; i++)
      {
        String regex = "%" + (i + 1);
        String arg = args[i];
        value = Regex.Replace(value, regex, arg, RegexOptions.None);
      }

      return value;
    }
  }

  internal sealed class StringLocalizer : BaseStringLocalizer
  {
    private static StringLocalizer s_instance;

    public static StringLocalizer Instance
    {
      get
      {
        if (s_instance == null)
          s_instance = new StringLocalizer();

        return s_instance;
      }
    }

    private StringLocalizer() : base("Tesla.LocalizableStrings", Assembly.GetExecutingAssembly()) { }
  }
}
