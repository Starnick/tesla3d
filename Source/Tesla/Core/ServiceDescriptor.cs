﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Describes an <see cref="IEngineService"/> so that it can be dynamically loaded.
  /// </summary>
  public sealed class ServiceDescriptor
  {
    private string m_assemblyName;
    private string m_serviceTypeName;
    private string m_typeName;
    private object[] m_parameters;

    /// <summary>
    /// Gets the assembly name (including extension).
    /// </summary>
    public string AssemblyName
    {
      get
      {
        return m_assemblyName;
      }
    }

    /// <summary>
    /// Gets the interface type name the service should be registered under.
    /// </summary>
    public string ServiceTypeName
    {
      get
      {
        return m_serviceTypeName;
      }
    }

    /// <summary>
    /// Gets the concrete type name of the service to be instantiated.
    /// </summary>
    public string TypeName
    {
      get
      {
        return m_typeName;
      }
    }

    /// <summary>
    /// Gets any constructor parameters the service requires.
    /// </summary>
    public object[] ConstructorParameters
    {
      get
      {
        return m_parameters;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="ServiceDescriptor"/> class.
    /// </summary>
    /// <param name="assemblyName">Name of the assembly the type is located in.</param>
    /// <param name="serviceTypeName">Name of the interface type the service should be registered under.</param>
    /// <param name="typeName">Name of the concrete type to instantiate.</param>
    public ServiceDescriptor(string assemblyName, string serviceTypeName, string typeName)
    {
      m_assemblyName = assemblyName;
      m_serviceTypeName = serviceTypeName;
      m_typeName = typeName;
      m_parameters = Array.Empty<object>();
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="ServiceDescriptor"/> class.
    /// </summary>
    /// <param name="assemblyName">Name of the assembly the type is located in.</param>
    /// <param name="serviceTypeName">Name of the interface type the service should be registered under.</param>
    /// <param name="typeName">Name of the concrete type to instantiate.</param>
    /// <param name="parameters">Constructor parameters required by the service.</param>
    public ServiceDescriptor(string assemblyName, string serviceTypeName, string typeName, params object[] parameters)
    {
      m_assemblyName = assemblyName;
      m_serviceTypeName = serviceTypeName;
      m_typeName = typeName;
      m_parameters = (parameters is null) ? Array.Empty<object>() : (parameters.Clone() as object[])!;
    }

    /// <inheritdoc />
    public override string ToString()
    {
      return $"'{m_serviceTypeName}' => '{m_typeName}' [{m_assemblyName}]";
    }
  }
}
