﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics;
using System.IO;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Wrapper that treats a data buffer as a .NET stream for reading and writing.
  /// </summary>
  [DebuggerDisplay("{ToString(),raw}")]
  public class DataBufferStream : Stream
  {
    private IReadOnlyDataBuffer m_db;
    private bool m_canWrite;
    private bool m_ownsDataBuffer;
    private long m_byteLength;
    private long m_bytePosition;
    private bool m_isDisposed;

    /// <summary>
    /// Gets if the stream has been disposed.
    /// </summary>
    public bool IsDisposed
    {
      get
      {
        return m_isDisposed;
      }
    }

    /// <summary>
    /// Gets if the stream owns the data buffer. When the stream is closed, the data buffer is disposed.
    /// </summary>
    public bool OwnsDataBuffer
    {
      get
      {
        return m_ownsDataBuffer;
      }
    }

    /// <summary>
    /// Gets the underlying data buffer.
    /// </summary>
    public IReadOnlyDataBuffer UnderlyingDataBuffer
    {
      get
      {
        return m_db;
      }
    }

    /// <summary>
    /// Gets a value indicating whether the current stream supports reading.
    /// </summary>
    public override bool CanRead
    {
      get
      {
        return true;
      }
    }

    /// <summary>
    /// Gets a value indicating whether the current stream supports seeking.
    /// </summary>
    public override bool CanSeek
    {
      get
      {
        return true;
      }
    }

    /// <summary>
    /// Gets a value indicating whether the current stream supports writing.
    /// </summary>
    public override bool CanWrite
    {
      get
      {
        return m_canWrite && m_db is IDataBuffer;
      }
    }

    /// <summary>
    /// Gets the length in bytes of the stream.
    /// </summary>
    public override long Length
    {
      get
      {
        return m_byteLength;
      }
    }

    /// <summary>
    /// Gets or sets the position within the current stream.
    /// </summary>
    public override long Position
    {
      get
      {
        return m_bytePosition;
      }
      set
      {
        Seek(value, SeekOrigin.Begin);
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="DataBufferStream"/> class. This starts off with no length and as data is written, the length
    /// and possibly the stream expands.
    /// </summary>
    /// <param name="minimumCapacity">Minimum capacity of the stream.</param>
    /// <param name="allocatorStrategy">Optional allocator strategy, by default pooling is used.</param>
    public DataBufferStream(long minimumCapacity = 4096, MemoryAllocatorStrategy allocatorStrategy = MemoryAllocatorStrategy.DefaultPooled)
    {
      m_db = DataBuffer.Create<byte>((int) minimumCapacity, MemoryAllocatorStrategy.DefaultPooled);
      m_canWrite = true;
      m_ownsDataBuffer = true;

      m_bytePosition = 0;
      m_byteLength = 0;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="DataBufferStream"/> class. The <see cref="Length"/> of the stream is set to the size of the specified buffer,
    /// essentially making the contents of the buffer available via the stream. If more data is written, the stream will expand.
    /// </summary>
    /// <param name="dataBuffer">Data buffer to treat as a stream.</param>
    /// <param name="writable">Sets if this stream can be written to or is read only.</param>
    /// <param name="ownsDataBuffer">Sets if the stream owns the data buffer, and if so, when the stream is closed/disposed, so is the data buffer.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the data buffer is null.</exception>
    public DataBufferStream(IDataBuffer dataBuffer, bool writable = true, bool ownsDataBuffer = false)
    {
      ArgumentNullException.ThrowIfNull(dataBuffer, nameof(dataBuffer));

      m_db = dataBuffer;
      m_canWrite = writable;
      m_ownsDataBuffer = ownsDataBuffer;

      m_bytePosition = 0;
      m_byteLength = dataBuffer.SizeInBytes;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="DataBufferStream"/> class. The <see cref="Length"/> of the stream is set to the size of the specified buffer,
    /// essentially making the contents of the buffer available via the stream.
    /// </summary>
    /// <param name="dataBuffer">The read-only data buffer.</param>
    /// <param name="ownsDataBuffer">Sets if the stream owns the data buffer, and if so, when the stream is closed/disposed, so is the data buffer.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the data buffer is null.</exception>
    public DataBufferStream(IReadOnlyDataBuffer dataBuffer, bool ownsDataBuffer = false)
    {
      ArgumentNullException.ThrowIfNull(dataBuffer, nameof(dataBuffer));

      m_db = dataBuffer;
      m_canWrite = false;
      m_ownsDataBuffer = ownsDataBuffer;

      m_bytePosition = 0;
      m_byteLength = dataBuffer.SizeInBytes;
    }

    /// <summary>
    /// Creates a memory stream backed by a data buffer that is allocated via <see cref="MemoryAllocatorStrategy.DefaultPooled"/> strategy.
    /// </summary>
    /// <param name="minCapacity">Minimum capacity of the stream.</param>
    /// <param name="setLength">Sets the length to the specified capacity.</param>
    /// <returns>Memory stream.</returns>
    public static DataBufferStream FromPool(long minCapacity = 4096, bool setLength = false)
    {
      DataBufferStream stream = new DataBufferStream(minCapacity);
      if (setLength)
        stream.SetLength(minCapacity);

      return stream;
    }

    /// <summary>
    /// Gets a read-only span of bytes from the start of the stream to it's length.
    /// </summary>
    /// <returns>Read only span of bytes.</returns>
    public ReadOnlySpan<byte> AsReadOnlySpan()
    {
      return AsReadOnlySpan(0, m_byteLength);
    }

    /// <summary>
    /// Gets a read-only span of bytes from the specified start of the stream.
    /// </summary>
    /// <param name="offset">Offset from the start of the stream.</param>
    /// <returns>Read only span of bytes.</returns>
    public ReadOnlySpan<byte> AsReadOnlySpan(long offset)
    {
      return AsReadOnlySpan(offset, m_byteLength - offset);
    }

    /// <summary>
    /// Gets a read-only span of bytes from the specified start of the stream.
    /// </summary>
    /// <param name="offset">Offset from the start of the stream.</param>
    /// <param name="count">Number of bytes in the span.</param>
    /// <returns>Read only span of bytes.</returns>
    public ReadOnlySpan<byte> AsReadOnlySpan(long offset, long count)
    {
      offset = Math.Max(0, offset);
      count = Math.Max(m_byteLength, count);
      return UnderlyingDataBuffer.Bytes.Slice((int) offset, (int) count);
    }

    /// <summary>
    /// Gets a span of bytes from the start of the stream to it's length.
    /// </summary>
    /// <returns>Span of bytes.</returns>
    public Span<byte> AsSpan()
    {
      return AsSpan(0, m_byteLength);
    }

    /// <summary>
    /// Gets a span of bytes from the specified start of the stream.
    /// </summary>
    /// <param name="offset">Offset from the start of the stream.</param>
    /// <returns>Span of bytes.</returns>
    public Span<byte> AsSpan(long offset)
    {
      return AsSpan(offset, m_byteLength - offset);
    }

    /// <summary>
    /// Gets a span of bytes from the specified start of the stream.
    /// </summary>
    /// <param name="offset">Offset from the start of the stream.</param>
    /// <param name="count">Number of bytes in the span.</param>
    /// <returns>Span of bytes.</returns>
    public Span<byte> AsSpan(long offset, long count)
    {
      if (!CanWrite)
        throw new NotSupportedException(StringLocalizer.Instance.GetLocalizedString("CannotResizeDataBufferStream"));

      offset = Math.Max(0, offset);
      count = Math.Max(m_byteLength, count);
      return (m_db as IDataBuffer)!.Bytes.Slice((int) offset, (int) count);
    }

    /// <summary>
    /// Releases the unmanaged resources used by the <see cref="T:System.IO.Stream" /> and optionally releases the managed resources.
    /// </summary>
    /// <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
    protected override void Dispose(bool disposing)
    {
      if (!m_isDisposed)
      {
        base.Dispose(disposing);

        if (disposing)
        {
          if (m_ownsDataBuffer)
            m_db.Dispose();
        }

        m_isDisposed = true;
      }
    }

    /// <summary>
    /// Clears all buffers for this stream and causes any buffered data to be written to the underlying device.
    /// </summary>
    public override void Flush() { }

    /// <summary>
    /// Sets the position within the current stream.
    /// </summary>
    /// <param name="offset">A byte offset relative to the <paramref name="origin" /> parameter.</param>
    /// <param name="origin">A value of type <see cref="T:System.IO.SeekOrigin" /> indicating the reference point used to obtain the new position.</param>
    /// <returns>The new position within the current stream.</returns>
    /// <exception cref="System.InvalidOperationException">Thrown if trying to seek beyond the beginning or end of the stream.</exception>
    public override long Seek(long offset, SeekOrigin origin)
    {
      CheckDisposed();

      long num = 0;
      switch (origin)
      {
        case SeekOrigin.Begin:
          num = offset;
          break;
        case SeekOrigin.Current:
          num = m_bytePosition + offset;
          break;
        case SeekOrigin.End:
          num = m_byteLength - offset;
          break;
      }

      if (num < 0)
        throw new InvalidOperationException(StringLocalizer.Instance.GetLocalizedString("CannotSeekBeyondBeginningOfStream"));

      if (num > m_byteLength)
        throw new InvalidOperationException(StringLocalizer.Instance.GetLocalizedString("CannotSeekBeyondEndOfStream"));

      m_bytePosition = num;
      return m_bytePosition;
    }

    /// <summary>
    /// Resizes the databuffer, if it can be written to.
    /// </summary>
    /// <param name="numBytes">The desired length of the current stream in bytes.</param>
    /// <exception cref="System.NotSupportedException">Thrown if the databuffer is read only.</exception>
    public override void SetLength(long numBytes)
    {
      if (!CanWrite)
        throw new NotSupportedException(StringLocalizer.Instance.GetLocalizedString("CannotResizeDataBufferStream"));

      // If new length is larger than the buffer, resize it
      if (numBytes > m_db.SizeInBytes)
      {
        // Take the largest element count if the # of bytes isn't divisible evenly
        int numElems = BufferHelper.ComputeElementCount((int) numBytes, m_db.ElementSizeInBytes);
        (m_db as IDataBuffer)!.Resize(numElems);
        numBytes = m_db.SizeInBytes;
      }

      m_byteLength = numBytes;

      //Make sure if we're trimming the byte position adjusts accordingly
      m_bytePosition = Math.Min(m_bytePosition, m_byteLength);
    }

    /// <summary>
    /// Reads a sequence of bytes from the current stream and advances the position within the stream by the number of bytes read.
    /// </summary>
    /// <param name="buffer">An array of bytes. When this method returns, the buffer contains the specified byte array with the values between <paramref name="offset" /> and (<paramref name="offset" /> + <paramref name="count" /> - 1) replaced by the bytes read from the current source.</param>
    /// <param name="offset">The zero-based byte offset in <paramref name="buffer" /> at which to begin storing the data read from the current stream.</param>
    /// <param name="count">The maximum number of bytes to be read from the current stream.</param>
    /// <returns>The total number of bytes read into the buffer. This can be less than the number of bytes requested if that many bytes are not currently available, or zero (0) if the end of the stream has been reached.</returns>
    /// <exception cref="System.NotSupportedException">Thrown if reading is not supported.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if any of the arguments are out of range.</exception>
    public override int Read(byte[] buffer, int offset, int count)
    {
      CheckArrayIndex(buffer.Length, offset, 0);

      return Read(buffer.AsSpan(offset, count));
    }

    /// <summary>
    /// Reads a sequence of bytes from the current stream and advances the position within the stream by the number of bytes read.
    /// </summary>
    /// <param name="buffer">Span of bytes to hold what is read from the stream.</param>
    /// <returns>The total number of bytes read into the buffer. This can be less than the number of bytes requested if that many bytes are not currently available, or zero (0) if the end of the stream has been reached.</returns>
    /// <exception cref="System.NotSupportedException">Thrown if reading is not supported.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if any of the arguments are out of range.</exception>
    public override int Read(Span<byte> buffer)
    {
      if (buffer.IsEmpty)
        return 0;

      if (!CanRead)
        throw new NotSupportedException(StringLocalizer.Instance.GetLocalizedString("CannotReadFromStream"));

      //Determine how many bytes to read, if the requested count is too high then we need to cap to available count left or zero
      int count = buffer.Length;
      if (m_bytePosition + count > m_byteLength)
        count = Math.Max(0, (int) (m_byteLength - m_bytePosition));

      if (count > 0)
      {
        m_db.Bytes.Slice((int) m_bytePosition, count).CopyTo(buffer);
        m_bytePosition += count;
      }

      return count;
    }

    /// <summary>
    /// When overridden in a derived class, writes a sequence of bytes to the current stream and advances the current position within this stream by the number of bytes written.
    /// </summary>
    /// <param name="buffer">An array of bytes. This method copies <paramref name="count" /> bytes from <paramref name="buffer" /> to the current stream.</param>
    /// <param name="offset">The zero-based byte offset in <paramref name="buffer" /> at which to begin copying bytes to the current stream.</param>
    /// <param name="count">The number of bytes to be written to the current stream.</param>
    /// <exception cref="System.NotSupportedException">Thrown if writing is not supported.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if any of the arguments are out of range.</exception>
    public override void Write(byte[] buffer, int offset, int count)
    {
      CheckArrayIndex(buffer.Length, offset, count);

      Write(buffer.AsSpan(offset, count));
    }

    /// <summary>
    /// Writes a sequence of bytes to the current stream and advances the current position within this stream by the number of bytes written.
    /// </summary>
    /// <param name="buffer">Span of bytes to write to the stream.</param>
    /// <exception cref="System.NotSupportedException">Thrown if writing is not supported.</exception>
    public override void Write(ReadOnlySpan<byte> buffer)
    {
      if (buffer.IsEmpty)
        return;

      CheckDisposed();

      if (!CanWrite)
        throw new NotSupportedException(StringLocalizer.Instance.GetLocalizedString("CannotResizeDataBufferStream"));

      if (m_bytePosition + buffer.Length > m_db.SizeInBytes)
        EnsureCapacity(GetGrowSize((int) m_bytePosition + buffer.Length));

      Span<byte> dst = (m_db as IDataBuffer)!.Bytes.Slice((int) m_bytePosition, buffer.Length);
      buffer.CopyTo(dst);
      m_bytePosition += buffer.Length;
      m_byteLength += buffer.Length;
    }

    /// <summary>
    /// Writes the entire contents of this stream [0, Length) to the specified stream.
    /// </summary>
    /// <param name="stream">Stream to write to.</param>
    public void WriteTo(Stream stream)
    {
      if (m_byteLength == 0)
        return;

      CheckDisposed();

      stream.Write(AsSpan());
    }

    /// <summary>
    /// Returns a string representation of the stream.
    /// </summary>
    public override string ToString()
    {
      return $"Length = {Length}";
    }

    #region Helpers

    private void CheckDisposed()
    {
      if (m_isDisposed)
        throw new ObjectDisposedException(StringLocalizer.Instance.GetLocalizedString("ObjectDisposed"));
    }

    private void CheckArrayIndex(int length, int index, int count)
    {
      if (count < 0)
        throw new ArgumentOutOfRangeException(nameof(count), StringLocalizer.Instance.GetLocalizedString("CountMustBeGreaterThanOrEqualToZero"));

      if (index < 0)
        throw new ArgumentOutOfRangeException(nameof(index), StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));

      if (index + count > length)
        throw new ArgumentOutOfRangeException($"{nameof(index) + nameof(count)}", StringLocalizer.Instance.GetLocalizedString("IndexAndCountOutOfRange"));
    }

    private int GetGrowSize(int minRequest)
    {
      if (m_db.SizeInBytes == 0)
        return Math.Max(minRequest, 4096);

      return Math.Max(minRequest, m_db.SizeInBytes * 2);
    }

    private void EnsureCapacity(int numBytes)
    {
      if (m_db.SizeInBytes < numBytes)
      {
        int numElems = BufferHelper.ComputeElementCount((int) numBytes, m_db.ElementSizeInBytes);
        (m_db as IDataBuffer)!.Resize(numElems);
      }

    }

    #endregion
  }
}
