﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Index buffer builder that allows for appending data in circumstances where the final size is not known, as data is appended
  /// the buffer grows. When finished the buffer can be claimed and optionally trimmed.
  /// </summary>
  [DebuggerTypeProxy(typeof(IndexDataBuilderDebugView))]
  public class IndexDataBuilder
  {
    private int m_position;
    private int m_count;
    private IndexData m_db;
    private IndexFormat m_format;
    private bool m_autoPromote;
    private MemoryAllocatorStrategy m_allocatorStrategy;

    /// <summary>
    /// Gets or sets the current position pointer of the buffer. If the position
    /// is out of range, it is clamped within the range of [0, Count].
    /// </summary>
    public int Position
    {
      get
      {
        return m_position;
      }
      set
      {
        m_position = MathHelper.Clamp(value, 0, Count);
      }
    }

    /// <summary>
    /// Gets the remaining count in the buffer (Count - Position).
    /// </summary>
    public int RemainingCount
    {
      get
      {
        return m_count - m_position;
      }
    }

    /// <summary>
    /// Gets or sets the number of elements in the buffer.
    /// </summary>
    public int Count
    {
      get
      {
        return m_count;
      }
      set
      {
        if (Capacity < value)
          Capacity = value;

        m_count = MathHelper.Clamp(value, 0, (m_db.IsValid) ? m_db.Length : 0);
        m_position = MathHelper.Clamp(m_position, 0, m_count);
      }
    }

    /// <summary>
    /// Gets the capacity of the buffer. This may be higher than the element count.
    /// </summary>
    public int Capacity
    {
      get
      {
        return (m_db.IsValid) ? m_db.Length : 0;
      }
      set
      {
        ref IndexData db = ref GetOrCreateBuffer();
        db.Resize(value);
        m_count = Math.Min(m_count, db.Length);
        m_position = Math.Min(m_position, db.Length);
      }
    }

    /// <summary>
    /// Gets the size of a single element in the buffer.
    /// </summary>
    public unsafe int ElementSizeInBytes { get { return m_db.ElementSizeInBytes; } }

    /// <summary>
    /// Gets the total size of the buffer in bytes.
    /// </summary>
    public int SizeInBytes { get { return ElementSizeInBytes * m_count; } }

    /// <summary>
    /// Gets the type of the element that the buffer contains.
    /// </summary>
    public Type ElementType { get { return m_db.ElementType; } }

    /// <summary>
    /// Gets or sets the index format of the data. If there is data, it is transformed if the specified format is different. E.g. 16-bit is
    /// promoted to 32-bit, and 32-bit demoted to 16-bit. Use with caution going from 32-bit to 16-bit as if the values in the buffer are greater than
    /// <see cref="ushort.MaxValue"/>, then the values will overflow.
    /// </summary>
    public IndexFormat IndexFormat
    {
      get
      {
        return m_format;
      }
      set
      {
        m_format = value;
        if (m_db.IsValid && m_db.IndexFormat != value)
        {
          if (m_db.Is32Bit)
            m_db.DemoteTo16Bits();
          else
            m_db.PromoteTo32Bits();
        }
      }
    }

    /// <summary>
    /// Gets or sets if the builder automatically promotes 16-bit indices to 32-bit if it detects data being appended exceeds <see cref="ushort.MaxValue"/>.
    /// </summary>
    public bool AutoPromote { get { return m_autoPromote; } set { m_autoPromote = value; } }

    /// <summary>
    /// Gets a span of 32-bit values if the underlying data is 32-bit, starting at the start of the buffer to the element count.
    /// </summary>
    public Span<int> Span32 { get { return (m_db.IsValid && m_db.Is32Bit) ? m_db.Span32.Slice(0, Count) : Span<int>.Empty; } }

    /// <summary>
    /// Gets a span of 16-bit values if the underlying data is 16-bit, starting at the start of the buffer to the element count.
    /// </summary>
    public Span<ushort> Span16 { get { return (m_db.IsValid && !m_db.Is32Bit) ? m_db.Span16.Slice(0, Count) : Span<ushort>.Empty; } }

    /// <summary>
    /// Gets a span of bytes, starting at the start of the buffer to the element count.
    /// </summary>
    public Span<byte> Bytes { get { return (Is32Bit) ? Span32.AsBytes() : Span16.AsBytes(); } }

    /// <summary>
    /// Gets a span over the remaining 32-bit values if the underlying data is 32-bit, starting at the current position of the buffer.
    /// </summary>
    public Span<int> RemainingSpan32 { get { return (m_db.IsValid && m_db.Is32Bit && HasNext) ? m_db.Span32.Slice(m_position, RemainingCount) : Span<int>.Empty; } }

    /// <summary>
    /// Gets a span over the remaining 16-bit values if the underlying data is 16-bit, starting at the current position of the buffer.
    /// </summary>
    public Span<ushort> RemainingSpan16 { get { return (m_db.IsValid && !m_db.Is32Bit && HasNext) ? m_db.Span16.Slice(m_position, RemainingCount) : Span<ushort>.Empty; } }

    /// <summary>
    /// Gets a span over the remaining elements as bytes, starting at the current position of the buffer.
    /// </summary>
    public Span<byte> RemainingBytes { get { return (Is32Bit) ? RemainingSpan32.AsBytes() : RemainingSpan16.AsBytes(); } }

    /// <summary>
    /// True if the data is 32-bit, false if 16-bit.
    /// </summary>
    public bool Is32Bit { get { return (m_db.IsValid) ? m_db.Is32Bit : m_format == IndexFormat.ThirtyTwoBits; } }

    /// <summary>
    /// Gets if the current position is less than the current count. If this is false and
    /// the position pointer is incremented, the buffer may be required to grow in capacity.
    /// </summary>
    public bool HasNext
    {
      get
      {
        return m_position < m_count;
      }
    }

    /// <summary>
    /// Gets or sets the allocator typeused when creating a new buffer.
    /// </summary>
    public MemoryAllocatorStrategy AllocatorStrategy
    {
      get
      {
        return m_allocatorStrategy;
      }
      set
      {
        m_allocatorStrategy = value;
      }
    }

    internal IndexData UnderlyingBuffer
    {
      get
      {
        return m_db;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="IndexDataBuilder"/> class.
    /// </summary>
    /// <param name="initialCapacity">Initial capacity of the buffer.</param>
    /// <param name="format">Optional index format, by default 32-bit.</param>
    /// <param name="allocatorStrategy">Optional memory allocator strategy.</param>
    public IndexDataBuilder(int initialCapacity = 0, IndexFormat format = IndexFormat.ThirtyTwoBits, MemoryAllocatorStrategy allocatorStrategy = MemoryAllocatorStrategy.Default)
    {
      m_allocatorStrategy = allocatorStrategy;
      m_count = 0;
      m_position = 0;
      m_db = (initialCapacity > 0) ? new IndexData(initialCapacity, format, m_allocatorStrategy) : new IndexData();
      m_format = format;
      m_autoPromote = false;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="IndexDataBuilder"/> class that starts off as 16-bit indices and will automatically
    /// expand to 32-bit if an index exceeding <see cref="ushort.MaxValue"/> is appended.
    /// </summary>
    /// <param name="initialCapacity">The initial capacity.</param>
    /// <returns>Builder instance</returns>
    public static IndexDataBuilder CreateInitially16Bit(int initialCapacity = 0)
    {
      IndexDataBuilder builder = new IndexDataBuilder(initialCapacity, IndexFormat.SixteenBits);
      builder.AutoPromote = true;
      return builder;
    }

    /// <summary>
    /// Resets the builder and disposes of the current buffer (if any).
    /// </summary>
    public void Clear()
    {
      if (m_db.IsValid)
        m_db.Dispose();

      m_db = new IndexData();
      m_count = 0;
      m_position = 0;
    }

    /// <summary>
    /// Takes ownership of the specified buffer and disposes of the previous held one. To reclaim ownership, call
    /// <see cref="Claim(bool)"/>.
    /// </summary>
    /// <param name="db">Buffer that should back the builder.</param>
    public void CaptureBuffer(IndexData db)
    {
      if (m_db.IsValid)
        m_db.Dispose();

      m_db = db;
      m_count = 0;
      m_position = 0;
      m_format = db.IndexFormat;
    }

    /// <summary>
    /// Gets the element at the current position index in the buffer, incrementing the position index. This will NOT cause the
    /// buffer to be resized and <see cref="HasNext"/> should be checked before when iterating to avoid having this go out of range.
    /// </summary>
    /// <returns>Value at the current position index in the buffer.</returns>
    /// <exception cref="System.ArgumentOutOfRangeException">Thrown if the current position pointer is at the end of the buffer.</exception>
    public int Get()
    {
      if (m_position >= Count)
        throw new ArgumentOutOfRangeException(nameof(Position), StringLocalizer.Instance.GetLocalizedString("CannotAdvanceBufferBeyondLength"));

      ref IndexData db = ref GetOrCreateBuffer();
      return db[m_position++];
    }

    /// <summary>
    /// Gets the element at the specified index in the buffer, without incrementing the position index. The index is only valid between
    /// [0, Count).
    /// </summary>
    /// <param name="index">Zero-based element index in the buffer at which to write the element to.</param>
    /// <returns>Reference to an element at the specified index.</returns>
    /// <exception cref="System.ArgumentOutOfRangeException">Thrown if the index is out of range of the current buffer allocation.</exception>
    public int Get(int index)
    {
      if ((uint) index >= (uint) Count)
        throw new ArgumentOutOfRangeException(nameof(index), StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));

      ref IndexData db = ref GetOrCreateBuffer();
      return db[index];
    }

    /// <summary>
    /// Sets the element at the current position index in the buffer, incrementing the position index. If the position is at the end,
    /// the count is incremented and capacity is grown as needed.
    /// </summary>
    /// <param name="value">Value to set at the current position index.</param>
    /// <returns>The index the value was placed in.</returns>
    public int Set(int value)
    {
      CheckAutoPromote(value);

      ref IndexData db = ref GetOrCreateBuffer();

      if (m_position + 1 > Capacity)
        EnsureCapacity(GetGrowSize(db.Length));

      int index = m_position;
      m_position++;
      if (m_position > m_count)
        m_count++;

      db[index] = value;
      return index;
    }

    /// <summary>
    /// Sets the element at the specified index in the buffer, without incrementing the position index. The index is only valid between
    /// [0, Count).
    /// </summary>
    /// <param name="index">Zero-based element index in the buffer at which to write the element to.</param>
    /// <param name="value">Value to write.</param>
    /// <exception cref="System.ArgumentOutOfRangeException">Thrown if the index is out of range of the current buffer allocation.</exception>
    public void Set(int index, int value)
    {
      if ((uint) index >= (uint) Count)
        throw new ArgumentOutOfRangeException(nameof(index), StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));

      CheckAutoPromote(value);

      ref IndexData db = ref GetOrCreateBuffer();
      db[index] = value;
    }

    /// <summary>
    /// Gets a copy of the range of elements starting at the specified index. The index and length of the span must be within [0, Count).
    /// </summary>
    /// <param name="index">Index into the buffer to start copying.</param>
    /// <param name="data">Span of data to receive the copy.</param>
    public void CopyRange(int index, Span<int> data)
    {
      if ((uint) (index + data.Length) > Count)
        throw new ArgumentOutOfRangeException(nameof(index), StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));

      if (data.IsEmpty)
        return;

      ref IndexData db = ref GetOrCreateBuffer();
      if (db.Is32Bit)
      {
        db.Span32.Slice(index, data.Length).CopyTo(data);
      } 
      else
      {
        Span<ushort> shortSpan = db.Span16.Slice(index, data.Length);
        for (int i = 0; i < data.Length; i++)
          data[i] = shortSpan[i];
      }
    }

    /// <summary>
    /// Gets a copy of the range of elements starting at the specified index. The index and length of the span must be within [0, Count).
    /// Use with caution going from 32-bit to 16-bit as if the values in the buffer are greater than <see cref="ushort.MaxValue"/>, 
    /// then the values will overflow.
    /// </summary>
    /// <param name="index">Index into the buffer to start copying.</param>
    /// <param name="data">Span of elements to receive the copy.</param>
    public void CopyRange(int index, Span<ushort> data)
    {
      if ((uint) (index + data.Length) > Count)
        throw new ArgumentOutOfRangeException(nameof(index), StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));

      if (data.IsEmpty)
        return;

      ref IndexData db = ref GetOrCreateBuffer();
      if (db.Is32Bit)
      {
        Span<int> shortSpan = db.Span32.Slice(index, data.Length);
        for (int i = 0; i < data.Length; i++)
          data[i] = (ushort) shortSpan[i];
      }
      else
      {
        db.Span16.Slice(index, data.Length).CopyTo(data);
      }
    }

    /// <summary>
    /// Sets the contents of the <see cref="IndexData"/> starting at the current position index buffer, incrementing the position index. If the position is at the end
    /// or the number of elements is greater than capacity, the count is incremented and capacity is grown as needed.
    /// </summary>
    /// <param name="data">Index data</param>
    /// <param name="baseVertexOffset">Optional offset to add to each index value (e.g. merging two sets of indices, one needs to be offsetted).</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if baseVertexOffset is negative.</exception>
    public void SetRange(IndexData data, int baseVertexOffset = 0)
    {
      if (!data.IsValid)
        return;

      if (data.Is32Bit)
        SetRange(data.Span32, baseVertexOffset);
      else
        SetRange(data.Span16, baseVertexOffset);
    }

    /// <summary>
    /// Sets the range of elements starting at the current position index buffer, incrementing the position index. If the position is at the end
    /// or the number of elements is greater than capacity, the count is incremented and capacity is grown as needed.
    /// </summary>
    /// <param name="data">Span of elements</param>
    /// <param name="baseVertexOffset">Optional offset to add to each index value (e.g. merging two sets of indices, one needs to be offsetted).</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if baseVertexOffset is negative.</exception>
    public void SetRange(ReadOnlySpan<int> data, int baseVertexOffset = 0)
    {
      if (data.IsEmpty)
        return;

      if (baseVertexOffset < 0)
        throw new ArgumentOutOfRangeException(nameof(baseVertexOffset));

      CheckAutoPromote(data, baseVertexOffset);

      if (Is32Bit)
      {
        Span<int> span = GetOrExpandRangeAsInt(data.Length);

        if (baseVertexOffset == 0)
        {
          data.CopyTo(span);
        }
        else
        {
          int index = 0;
          foreach (int value in data)
            span[index++] = value + baseVertexOffset;
        }
      }
      else
      {
        Span<ushort> span = GetOrExpandRangeAsShort(data.Length);
        int index = 0;
        foreach (int value in data)
          span[index++] = (ushort) (value + baseVertexOffset);
      }
    }

    /// <summary>
    /// Sets the range of elements starting at the current position index buffer, incrementing the position index. If the position is at the end
    /// or the number of elements is greater than capacity, the count is incremented and capacity is grown as needed.
    /// </summary>
    /// <param name="data">Array of elements</param>
    /// <param name="baseVertexOffset">Optional offset to add to each index value (e.g. merging two sets of indices, one needs to be offsetted).</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if baseVertexOffset is negative.</exception>
    public void SetRange(int[] data, int baseVertexOffset = 0)
    {
      SetRange(new ReadOnlySpan<int>(data), baseVertexOffset);
    }

    /// <summary>
    /// Sets the range of elements starting at the current position index buffer, incrementing the position index. If the position is at the end
    /// or the number of elements is greater than capacity, the count is incremented and capacity is grown as needed.
    /// </summary>
    /// <param name="data">Span of elements</param>
    /// <param name="baseVertexOffset">Optional offset to add to each index value (e.g. merging two sets of indices, one needs to be offsetted).</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if baseVertexOffset is negative.</exception>
    public void SetRange(IEnumerable<int> data, int baseVertexOffset = 0)
    {
      if (data is IReadOnlyDataBuffer<int> db)
      {
        SetRange(db.Span, baseVertexOffset);
        return;
      }
      else if (data is int[] arr)
      {
        SetRange(arr.AsSpan(), baseVertexOffset);
        return;
      }
      else if (data is IReadOnlyRefList<int> rl)
      {
        SetRange(rl.Span, baseVertexOffset);
        return;
      }
      else if (data is List<int> l)
      {
        SetRange(CollectionsMarshal.AsSpan(l), baseVertexOffset);
        return;
      }

      int count = 0;
      if (data is ICollection<int> c)
        count = c.Count;
      else if (data is IReadOnlyCollection<int> rc)
        count = rc.Count;
      else
        count = data.Count();

      if (count == 0)
        return;

      if (baseVertexOffset < 0)
        throw new ArgumentOutOfRangeException(nameof(baseVertexOffset));

      CheckAutoPromote(data);

      if (Is32Bit)
      {
        Span<int> span = GetOrExpandRangeAsInt(count);
        int index = 0;
        foreach (int value in data)
          span[index++] = value + baseVertexOffset;
      } 
      else
      {
        Span<ushort> span = GetOrExpandRangeAsShort(count);
        int index = 0;
        foreach (int value in data)
          span[index++] = (ushort) (value + baseVertexOffset);
      }
    }

    /// <summary>
    /// Sets the range of elements starting at the current position index buffer, incrementing the position index. If the position is at the end
    /// or the number of elements is greater than capacity, the count is incremented and capacity is grown as needed.
    /// </summary>
    /// <param name="data">Span of elements</param>
    /// <param name="baseVertexOffset">Optional offset to add to each index value (e.g. merging two sets of indices, one needs to be offsetted).</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if baseVertexOffset is negative.</exception>
    public void SetRange(ReadOnlySpan<ushort> data, int baseVertexOffset = 0)
    {
      if (data.IsEmpty)
        return;

      if (baseVertexOffset < 0)
        throw new ArgumentOutOfRangeException(nameof(baseVertexOffset));

      CheckAutoPromote(data, baseVertexOffset);

      if (Is32Bit)
      {
        Span<int> span = GetOrExpandRangeAsInt(data.Length);
        int index = 0;
        foreach (ushort value in data)
          span[index++] = value + baseVertexOffset;
      }
      else
      {
        Span<ushort> span = GetOrExpandRangeAsShort(data.Length);

        if (baseVertexOffset == 0)
        {
          data.CopyTo(span);
        }
        else
        {
          int index = 0;
          foreach (ushort value in data)
            span[index++] = (ushort) (value + baseVertexOffset);
        }
      }
    }

    /// <summary>
    /// Sets the range of elements starting at the current position index buffer, incrementing the position index. If the position is at the end
    /// or the number of elements is greater than capacity, the count is incremented and capacity is grown as needed.
    /// </summary>
    /// <param name="data">Array of elements</param>
    /// <param name="baseVertexOffset">Optional offset to add to each index value (e.g. merging two sets of indices, one needs to be offsetted).</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if baseVertexOffset is negative.</exception>
    public void SetRange(ushort[] data, int baseVertexOffset = 0)
    {
      SetRange(new ReadOnlySpan<ushort>(data), baseVertexOffset);
    }

    /// <summary>
    /// Sets the range of elements starting at the current position index buffer, incrementing the position index. If the position is at the end
    /// or the number of elements is greater than capacity, the count is incremented and capacity is grown as needed.
    /// </summary>
    /// <param name="data">Span of elements</param>
    /// <param name="baseVertexOffset">Optional offset to add to each index value (e.g. merging two sets of indices, one needs to be offsetted).</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if baseVertexOffset is negative.</exception>
    public void SetRange(IEnumerable<ushort> data, int baseVertexOffset = 0)
    {
      if (data is IReadOnlyDataBuffer<ushort> db)
      {
        SetRange(db.Span, baseVertexOffset);
        return;
      }
      else if (data is ushort[] arr)
      {
        SetRange(arr.AsSpan(), baseVertexOffset);
        return;
      }
      else if (data is IReadOnlyRefList<ushort> rl)
      {
        SetRange(rl.Span, baseVertexOffset);
        return;
      }
      else if (data is List<ushort> l)
      {
        SetRange(CollectionsMarshal.AsSpan(l), baseVertexOffset);
        return;
      }

      int count = 0;
      if (data is ICollection<ushort> c)
        count = c.Count;
      else if (data is IReadOnlyCollection<ushort> rc)
        count = rc.Count;
      else
        count = data.Count();

      if (count == 0)
        return;

      if (baseVertexOffset < 0)
        throw new ArgumentOutOfRangeException(nameof(baseVertexOffset));

      CheckAutoPromote(data, baseVertexOffset);

      if (Is32Bit)
      {
        Span<int> span = GetOrExpandRangeAsInt(count);
        int index = 0;
        foreach (ushort value in data)
          span[index++] = value + baseVertexOffset;
      }
      else
      {
        Span<ushort> span = GetOrExpandRangeAsShort(count);
        int index = 0;
        foreach (ushort value in data)
          span[index++] = (ushort) (value + baseVertexOffset);
      }
    }

    /// <summary>
    /// Checks if the position pointer can be incremented N more times before it reaches the
    /// current count. If false, the buffer may need to grow it's capacity to accomodate more elements.
    /// </summary>
    /// <param name="numElements">Number of elements requested.</param>
    /// <returns>True if the buffer has enough room to add the next N elements.</returns>
    public bool HasNextFor(int numElements)
    {
      if (numElements <= 0)
        return false;

      return (Position + numElements) <= Count;
    }

    /// <summary>
    /// Claims the underlying data buffer, releasing ownership of it from the builder.
    /// </summary>
    /// <param name="trim">True if the buffer should not have excess capacity, false if it can be larger than the current Count.</param>
    /// <returns>Data buffer</returns>
    public IndexData Claim(bool trim = false)
    {
      IndexData db = GetOrCreateBuffer();
      if (trim && Capacity != Count)
        db.Resize(Count);

      m_db = new IndexData();
      m_count = 0;
      m_position = 0;
      return db;
    }

    /// <summary>
    /// Ensures the buffer has the specified capacity
    /// </summary>
    /// <param name="requestedCapacity">Requested capacity</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the requested capacity is less than zero.</exception>
    public void EnsureCapacity(int requestedCapacity)
    {
      if (requestedCapacity < 0)
        throw new ArgumentOutOfRangeException(nameof(requestedCapacity), StringLocalizer.Instance.GetLocalizedString("MustBeNonNegative"));

      if (Capacity < requestedCapacity)
        Capacity = requestedCapacity;
    }

    private ref IndexData GetOrCreateBuffer()
    {
      if (m_db.IsValid)
        return ref m_db;

      m_db = new IndexData(0, m_format, m_allocatorStrategy);
      return ref m_db;
    }

    private int GetGrowSize(int length)
    {
      if (length == 0)
        return 4;

      return length * 2;
    }

    private Span<int> GetOrExpandRangeAsInt(int count)
    {
      ref IndexData db = ref GetOrCreateBuffer();

      if (m_position + count > Capacity)
        EnsureCapacity(m_position + count);

      int index = m_position;
      m_position += count;
      if (m_position > m_count)
        m_count += count;

      return db.Span32.Slice(index, count);
    }

    private Span<ushort> GetOrExpandRangeAsShort(int count)
    {
      ref IndexData db = ref GetOrCreateBuffer();

      if (m_position + count > Capacity)
        EnsureCapacity(m_position + count);

      int index = m_position;
      m_position += count;
      if (m_position > m_count)
        m_count += count;

      return db.Span16.Slice(index, count);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private void CheckAutoPromote(int value)
    {
      if (IndexFormat == IndexFormat.ThirtyTwoBits || !AutoPromote)
        return;

      if (value >= ushort.MaxValue)
        IndexFormat = IndexFormat.ThirtyTwoBits;
    }

    private void CheckAutoPromote(ReadOnlySpan<int> data, int baseVertexOffset = 0)
    {
      if (IndexFormat == IndexFormat.ThirtyTwoBits || !AutoPromote)
        return;

      for (int i = 0; i < data.Length; i++)
      {
        if ((data[i] + baseVertexOffset) >= ushort.MaxValue)
        {
          IndexFormat = IndexFormat.ThirtyTwoBits;
          break;
        }
      }
    }

    private void CheckAutoPromote(IEnumerable<int> data, int baseVertexOffset = 0)
    {
      if (IndexFormat == IndexFormat.ThirtyTwoBits || !AutoPromote)
        return;

      foreach (int value in data)
      {
        if ((value + baseVertexOffset) >= ushort.MaxValue)
        {
          IndexFormat = IndexFormat.ThirtyTwoBits;
          break;
        }
      }
    }

    private void CheckAutoPromote(ReadOnlySpan<ushort> data, int baseVertexOffset)
    {
      if (baseVertexOffset <= 0 || IndexFormat == IndexFormat.ThirtyTwoBits || !AutoPromote)
        return;

      for (int i = 0; i < data.Length; i++)
      {
        if ((data[i] + baseVertexOffset) >= ushort.MaxValue)
        {
          IndexFormat = IndexFormat.ThirtyTwoBits;
          break;
        }
      }
    }

    private void CheckAutoPromote(IEnumerable<ushort> data, int baseVertexOffset)
    {
      if (IndexFormat == IndexFormat.ThirtyTwoBits || !AutoPromote)
        return;

      foreach (ushort value in data)
      {
        if ((value + baseVertexOffset) >= ushort.MaxValue)
        {
          IndexFormat = IndexFormat.ThirtyTwoBits;
          break;
        }
      }
    }
  }
}
