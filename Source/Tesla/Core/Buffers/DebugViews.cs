﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

#nullable enable

namespace Tesla
{
  // DebugView for DataBuffer
  internal sealed class DataBufferDebugView<T> where T : unmanaged
  {
    private IReadOnlyDataBuffer<T> m_db;

    public ReadOnlySpan<byte> Bytes
    {
      get
      {
        return m_db.Bytes;
      }
    }

    public int Length
    {
      get
      {
        return m_db.Length;
      }
    }

    public int SizeInBytes
    {
      get
      {
        return m_db.SizeInBytes;
      }
    }

    public int ElementSizeInBytes
    {
      get
      {
        return m_db.ElementSizeInBytes;
      }
    }

    public Type ElementType
    {
      get
      {
        return m_db.ElementType;
      }
    }

    public bool IsDisposed
    {
      get
      {
        return m_db.IsDisposed;
      }
    }

    //Hide root will get "Results View" to display as if the original structure had the automatic enumerable expander
    [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
    public DataBufferEnumerable<T> ZZZResultsView
    {
      get
      {
        return new DataBufferEnumerable<T>(m_db);
      }
    }

    public DataBufferDebugView(IReadOnlyDataBuffer<T> db)
    {
      m_db = db;
    }

    //Nested class to get "Results View" for enumerables
    public class DataBufferEnumerable<K> : IEnumerable<K> where K : unmanaged
    {
      [DebuggerBrowsable(DebuggerBrowsableState.Never)]
      private IReadOnlyDataBuffer<K> m_db;

      public DataBufferEnumerable(IReadOnlyDataBuffer<K> db)
      {
        m_db = db;
      }

      IEnumerator IEnumerable.GetEnumerator()
      {
        return m_db.GetEnumerator();
      }

      public IEnumerator<K> GetEnumerator()
      {
        return m_db.GetEnumerator();
      }
    }
  }

  // DebugView for DataBufferBuilder
  internal sealed class DataBufferBuilderDebugView<T> where T : unmanaged
  {
    private DataBufferBuilder<T> m_builder;

    public IMemoryAllocator<T> Allocator
    {
      get
      {
        return m_builder.Allocator;
      }
    }

    public int Position
    {
      get
      {
        return m_builder.Position;
      }
    }

    public int RemainingCount
    {
      get
      {
        return m_builder.RemainingCount;
      }
    }

    public int Count
    {
      get
      {
        return m_builder.Count;
      }
    }

    public int Capacity
    {
      get
      {
        return m_builder.Capacity;
      }
    }

    public bool HasNext
    {
      get
      {
        return m_builder.HasNext;
      }
    }

    public int SizeInBytes
    {
      get
      {
        return m_builder.SizeInBytes;
      }
    }

    public int ElementSizeInBytes
    {
      get
      {
        return m_builder.ElementSizeInBytes;
      }
    }

    public Type ElementType
    {
      get
      {
        return m_builder.ElementType;
      }
    }

    public Span<T> Span
    {
      get
      {
        return m_builder.Span;
      }
    }

    public Span<byte> Bytes
    {
      get
      {
        return m_builder.Bytes;
      }
    }

    public Span<T> RemainingSpan
    {
      get
      {
        return m_builder.RemainingSpan;
      }
    }

    public Span<byte> RemainingBytes
    {
      get
      {
        return m_builder.RemainingBytes;
      }
    }

    public DataBuffer<T>? UnderlyingBuffer
    {
      get
      {
        return m_builder.UnderlyingBuffer;
      }
    }

    //Hide root will get "Results View" to display as if the original structure had the automatic enumerable expander
    [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
    public BuilderEnumerable<T> ZZZResultsView
    {
      get
      {
        return new BuilderEnumerable<T>(m_builder);
      }
    }

    public DataBufferBuilderDebugView(DataBufferBuilder<T> builder)
    {
      m_builder = builder;
    }

    //Nested class to get "Results View" for enumerables
    public class BuilderEnumerable<K> : IEnumerable<K> where K : unmanaged
    {
      [DebuggerBrowsable(DebuggerBrowsableState.Never)]
      private IEnumerable<K> m_values;

      public BuilderEnumerable(DataBufferBuilder<K> builder)
      {
        m_values = Enumerate(builder);
      }

      IEnumerator IEnumerable.GetEnumerator()
      {
        return m_values.GetEnumerator();
      }

      public IEnumerator<K> GetEnumerator()
      {
        return m_values.GetEnumerator();
      }

      private static IEnumerable<K> Enumerate(DataBufferBuilder<K> builder)
      {
        if (builder.UnderlyingBuffer != null)
        {
          DataBuffer<K> db = builder.UnderlyingBuffer;
          for (int i = 0; i < builder.Count; i++)
            yield return db[i];
        }
      }
    }
  }

  // DebugView for IndexData
  internal sealed class IndexDataDebugView
  {
    private IndexData m_db;

    public ReadOnlySpan<byte> Bytes
    {
      get
      {
        return m_db.Bytes;
      }
    }

    public int Length
    {
      get
      {
        return m_db.Length;
      }
    }

    public int SizeInBytes
    {
      get
      {
        return m_db.SizeInBytes;
      }
    }

    public int ElementSizeInBytes
    {
      get
      {
        return m_db.ElementSizeInBytes;
      }
    }

    public Type ElementType
    {
      get
      {
        return m_db.ElementType;
      }
    }

    public IndexFormat IndexFormat
    {
      get
      {
        return m_db.IndexFormat;
      }
    }

    public bool Is32Bit
    {
      get
      {
        return m_db.Is32Bit;
      }
    }

    public bool IsValid
    {
      get
      {
        return m_db.IsValid;
      }
    }

    public bool IsDisposed
    {
      get
      {
        return m_db.IsDisposed;
      }
    }

    //Hide root will get "Results View" to display as if the original structure had the automatic enumerable expander
    [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
    public IndexDataEnumerable ZZZResultsView
    {
      get
      {
        return new IndexDataEnumerable(m_db);
      }
    }

    public IndexDataDebugView(IndexData db)
    {
      m_db = db;
    }

    //Nested class to get "Results View" for enumerables
    public class IndexDataEnumerable : IEnumerable<int>
    {
      [DebuggerBrowsable(DebuggerBrowsableState.Never)]
      private IndexData m_db;

      public IndexDataEnumerable(IndexData db)
      {
        m_db = db;
      }

      IEnumerator IEnumerable.GetEnumerator()
      {
        return m_db.GetEnumerator();
      }

      public IEnumerator<int> GetEnumerator()
      {
        return m_db.GetEnumerator();
      }
    }
  }

  // DebugView for IndexDataBuilder
  internal sealed class IndexDataBuilderDebugView
  {
    private IndexDataBuilder m_builder;

    public MemoryAllocatorStrategy AllocatorStrategy
    {
      get
      {
        return m_builder.AllocatorStrategy;
      }
    }

    public int Position
    {
      get
      {
        return m_builder.Position;
      }
    }

    public int RemainingCount
    {
      get
      {
        return m_builder.RemainingCount;
      }
    }

    public int Count
    {
      get
      {
        return m_builder.Count;
      }
    }

    public int Capacity
    {
      get
      {
        return m_builder.Capacity;
      }
    }

    public bool HasNext
    {
      get
      {
        return m_builder.HasNext;
      }
    }

    public int SizeInBytes
    {
      get
      {
        return m_builder.SizeInBytes;
      }
    }

    public int ElementSizeInBytes
    {
      get
      {
        return m_builder.ElementSizeInBytes;
      }
    }

    public Type ElementType
    {
      get
      {
        return m_builder.ElementType;
      }
    }

    public IndexFormat IndexFormat
    {
      get
      {
        return m_builder.IndexFormat;
      }
    }

    public bool Is32Bit
    {
      get
      {
        return m_builder.Is32Bit;
      }
    }

    public Span<int> Span32
    {
      get
      {
        return m_builder.Span32;
      }
    }

    public Span<ushort> Span16
    {
      get
      {
        return m_builder.Span16;
      }
    }

    public Span<byte> Bytes
    {
      get
      {
        return m_builder.Bytes;
      }
    }

    public Span<int> RemainingSpan32
    {
      get
      {
        return m_builder.RemainingSpan32;
      }
    }

    public Span<ushort> RemainingSpan16
    {
      get
      {
        return m_builder.RemainingSpan16;
      }
    }

    public Span<byte> RemainingBytes
    {
      get
      {
        return m_builder.RemainingBytes;
      }
    }

    public IndexData UnderlyingBuffer
    {
      get
      {
        return m_builder.UnderlyingBuffer;
      }
    }

    //Hide root will get "Results View" to display as if the original structure had the automatic enumerable expander
    [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
    public BuilderEnumerable ZZZResultsView
    {
      get
      {
        return new BuilderEnumerable(m_builder);
      }
    }

    public IndexDataBuilderDebugView(IndexDataBuilder builder)
    {
      m_builder = builder;
    }

    //Nested class to get "Results View" for enumerables
    public class BuilderEnumerable : IEnumerable<int>
    {
      [DebuggerBrowsable(DebuggerBrowsableState.Never)]
      private IEnumerable<int> m_values;

      public BuilderEnumerable(IndexDataBuilder builder)
      {
        m_values = Enumerate(builder);
      }

      IEnumerator IEnumerable.GetEnumerator()
      {
        return m_values.GetEnumerator();
      }

      public IEnumerator<int> GetEnumerator()
      {
        return m_values.GetEnumerator();
      }

      private static IEnumerable<int> Enumerate(IndexDataBuilder builder)
      {
        if (builder.UnderlyingBuffer.IsValid)
        {
          IndexData db = builder.UnderlyingBuffer;
          for (int i = 0; i < builder.Count; i++)
            yield return db[i];
        }
      }
    }
  }

  // DebugView for PooledArray
  internal sealed class PooledArrayDebugView<T>
  {
    private PooledArray<T> m_arr;

    public int Length
    {
      get
      {
        return m_arr.Length;
      }
    }

    public bool DisposeContents
    {
      get
      {
        return m_arr.DisposeContents;
      }
    }

    public bool IsValid
    {
      get
      {
        return m_arr.IsValid;
      }
    }

    //Hide root will get "Results View" to display as if the original structure had the automatic enumerable expander
    [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
    public PooledArrayEnumerable<T> ZZZResultsView
    {
      get
      {
        return new PooledArrayEnumerable<T>(m_arr);
      }
    }

    public PooledArrayDebugView(PooledArray<T> arr)
    {
      m_arr = arr;
    }

    //Nested class to get "Results View" for enumerables
    public class PooledArrayEnumerable<K> : IEnumerable<K>
    {
      [DebuggerBrowsable(DebuggerBrowsableState.Never)]
      private PooledArray<K> m_arr;

      public PooledArrayEnumerable(PooledArray<K> arr)
      {
        m_arr = arr;
      }

      IEnumerator IEnumerable.GetEnumerator()
      {
        return m_arr.GetEnumerator();
      }

      public IEnumerator<K> GetEnumerator()
      {
        return m_arr.GetEnumerator();
      }
    }
  }
}
