﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Buffers;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// A read-only interface for a buffer representing a number of elements with a type that can be treated as a span of bytes.
  /// </summary>
  public interface IReadOnlyDataBuffer : IDisposable, IEnumerable, IDeepCloneable
  {
    /// <summary>
    /// Gets if the buffer has been disposed or not.
    /// </summary>
    bool IsDisposed { get; }

    /// <summary>
    /// Gets the number of elements that the buffer contains.
    /// </summary>
    int Length { get; }

    /// <summary>
    /// Gets the size of a single element in the buffer.
    /// </summary>
    int ElementSizeInBytes { get; }

    /// <summary>
    /// Gets the total size of the buffer in bytes.
    /// </summary>
    int SizeInBytes { get; }

    /// <summary>
    /// Gets the type of the element that the buffer contains.
    /// </summary>
    Type ElementType { get; }

    /// <summary>
    /// Gets the buffer contents as a read-only span of bytes.
    /// </summary>
    ReadOnlySpan<byte> Bytes { get; }

    /// <summary>
    /// Clones the buffer.
    /// </summary>
    /// <returns>A new buffer instance containing a duplicate of the data.</returns>
    new IDataBuffer Clone();

    /// <summary>
    /// Pins the memory and returns a handle to it. Call dispose on the handle to unpin the memory.
    /// </summary>
    /// <returns>A memory handle representing the underlying pinned data.</returns>
    MemoryHandle Pin();
  }

  /// <summary>
  /// A read-only interface for a buffer representing a number of elements of type T.
  /// </summary>
  /// <typeparam name="T">Blittable element type contained in the buffer.</typeparam>
  public interface IReadOnlyDataBuffer<T> : IReadOnlyDataBuffer, IReadOnlyRefList<T> where T : unmanaged
  {
    /// <summary>
    /// Gets the buffer contents as read-only memory block.
    /// </summary>
    ReadOnlyMemory<T> Memory { get; }
  }

  /// <summary>
  /// An interface for a buffer representing a number of elements with a type that can be treated as a span of bytes.
  /// </summary>
  public interface IDataBuffer : IReadOnlyDataBuffer
  {
    /// <summary>
    /// Gets the buffer contents as a span of bytes.
    /// </summary>
    new Span<byte> Bytes { get; }

    /// <summary>
    /// Clears the contents of the buffer, setting to zeroed values.
    /// </summary>
    void Clear();

    /// <summary>
    /// Resizes the buffer. If smaller than the current size, data will be lost. If larger, the new portions of the buffer will be zeroed values.
    /// </summary>
    /// <param name="newSize">The requested new size.</param>
    void Resize(int newSize);
  }

  /// <summary>
  /// A strongly typed buffer for blittable data types that can easily treat the data as a span of bytes and supports allocating different
  /// types of memory blocks.
  /// </summary>
  /// <typeparam name="T">Blittable element type contained in the buffer.</typeparam>
  [DebuggerTypeProxy(typeof(DataBufferDebugView<>))]
  [DebuggerDisplay("{ToString(),raw}")]
  public sealed class DataBuffer<T> : IDataBuffer, IReadOnlyDataBuffer<T> where T : unmanaged
  {
    private IMemoryOwnerEx<T> m_memory;
    private bool m_isMemoryOwned;
    private int m_elemSizeInBytes;
    private Type m_elemType;
    private bool m_isDisposed;

    /// <summary>
    /// Gets if the buffer has been disposed or not.
    /// </summary>
    public bool IsDisposed { get { return m_isDisposed; } }

    /// <summary>
    /// Gets the number of elements that the buffer contains.
    /// </summary>
    public int Length { get { return m_memory.Length; } }

    /// <summary>
    /// Gets the number of elements that the buffer contains.
    /// </summary>
    int IReadOnlyCollection<T>.Count { get { return m_memory.Length; } }

    /// <summary>
    /// Gets the size of a single element in the buffer.
    /// </summary>
    public int ElementSizeInBytes { get { return m_elemSizeInBytes; } }

    /// <summary>
    /// Gets the total size of the buffer in bytes.
    /// </summary>
    public int SizeInBytes { get { return m_memory.Length * m_elemSizeInBytes; } }

    /// <summary>
    /// Gets the type of the element that the buffer contains.
    /// </summary>
    public Type ElementType { get { return m_elemType; } }

    /// <summary>
    /// Gets a reference to the element at the given index.
    /// </summary>
    public ref T this[int index] { [MethodImpl(MethodImplOptions.AggressiveInlining)] get { return ref m_memory[index]; } }

    /// <summary>
    /// Gets a readonly reference to the element at the given index.
    /// </summary>
    ref readonly T IReadOnlyRefList<T>.this[int index] { get { return ref m_memory[index]; } }

    /// <summary>
    /// Gets the element at the given index.
    /// </summary>
    T IReadOnlyList<T>.this[int index] { get { return m_memory[index]; } }

    /// <summary>
    /// Gets a span representing the data buffer.
    /// </summary>
    public Span<T> Span { [MethodImpl(MethodImplOptions.AggressiveInlining)] get { return m_memory.Span; } }

    /// <summary>
    /// Gets a read-only span representing the data buffer.
    /// </summary>
    ReadOnlySpan<T> IReadOnlyRefList<T>.Span { get { return m_memory.Span; } }

    /// <summary>
    /// Gets a memory representing the data buffer.
    /// </summary>
    public Memory<T> Memory { get { return m_memory.Memory; } }

    /// <summary>
    /// Gets a read-only memory representing the data buffer.
    /// </summary>
    ReadOnlyMemory<T> IReadOnlyDataBuffer<T>.Memory { get { return m_memory.Memory; } }

    /// <summary>
    /// Gets the buffer contents as a span of bytes.
    /// </summary>
    public Span<byte> Bytes { [MethodImpl(MethodImplOptions.AggressiveInlining)] get { return MemoryMarshal.AsBytes(m_memory.Span); } }

    /// <summary>
    /// Gets the buffer contents as a read-only span of bytes.
    /// </summary>
    ReadOnlySpan<byte> IReadOnlyDataBuffer.Bytes { get { return Bytes; } }

    /// <summary>
    /// Constructs a new instance of the <see cref="DataBuffer{T}"/> class with the specified length.
    /// </summary>
    /// <param name="length">Length of the buffer.</param>
    /// <param name="allocator">Optional memory allocator. If null, default memory allocation strategy is used.</param>
    public DataBuffer(int length, IMemoryAllocator<T>? allocator = null)
    {
      if (allocator is null)
        allocator = MemoryAllocator<T>.Default;

      m_elemSizeInBytes = BufferHelper.SizeOf<T>();
      m_elemType = typeof(T);
      m_memory = allocator.Allocate(length);
      m_isMemoryOwned = true;
      m_isDisposed = false;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="DataBuffer{T}"/> class with the specified length and allocator strategy.
    /// </summary>
    /// <param name="length">Length of the buffer.</param>
    /// <param name="allocatorStrategy">Memory allocator strategy.</param>
    public DataBuffer(int length, MemoryAllocatorStrategy allocatorStrategy) 
      : this(length, MemoryAllocator<T>.GetAllocator(allocatorStrategy)) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="DataBuffer{T}"/> class copying data from the specified source.
    /// </summary>
    /// <param name="data">Source data to populate the new buffer instance.</param>
    /// <param name="allocator">Optional memory allocator. If null, default memory allocation strategy is used.</param>
    public DataBuffer(ReadOnlySpan<T> data, IMemoryAllocator<T>? allocator = null)
      : this(data.Length, allocator)
    {
      data.CopyTo(Span);
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="DataBuffer{T}"/> class copying data from the specified source.
    /// </summary>
    /// <param name="data">Source data to populate the new buffer instance.</param>
    /// <param name="allocatorStrategy">Memory allocator strategy.</param>
    public DataBuffer(ReadOnlySpan<T> data, MemoryAllocatorStrategy allocatorStrategy)
      : this(data.Length, allocatorStrategy)
    {
      data.CopyTo(Span);
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="DataBuffer{T}"/> class with the specified memory to back it.
    /// </summary>
    /// <param name="owner">Memory block owner.</param>
    /// <param name="takeOwnership">True if the buffer should manage the lifetime of the memory, false if the memory is externally managed.</param>
    public DataBuffer(IMemoryOwnerEx<T> owner, bool takeOwnership = true)
    {
      m_elemSizeInBytes = BufferHelper.SizeOf<T>();
      m_elemType = typeof(T);
      m_memory = owner;
      m_isMemoryOwned = takeOwnership;
      m_isDisposed = false;
    }

    /// <summary>
    /// Clones the buffer.
    /// </summary>
    /// <returns>
    /// A new buffer instance containing a duplicate of the data.
    /// </returns>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    public DataBuffer<T> Clone()
    {
      CheckDisposed();

      return new DataBuffer<T>(m_memory.Clone());
    }

    /// <summary>
    /// Clones the buffer.
    /// </summary>
    /// <returns>
    /// A new buffer instance containing a duplicate of the data.
    /// </returns>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    IDataBuffer IReadOnlyDataBuffer.Clone() { return Clone(); }

    /// <summary>
    /// Clones the buffer.
    /// </summary>
    /// <returns>
    /// A new buffer instance containing a duplicate of the data.
    /// </returns>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    IDeepCloneable IDeepCloneable.Clone() { return Clone(); }

    /// <summary>
    /// Pins the memory and returns a handle to it. Call dispose on the handle to unpin the memory.
    /// </summary>
    /// <returns>A memory handle representing the underlying pinned data.</returns>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    public MemoryHandle Pin()
    {
      CheckDisposed();

      return m_memory.Memory.Pin();
    }

    /// <summary>
    /// Clears the contents of the buffer, setting to zeroed values.
    /// </summary>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    public void Clear()
    {
      CheckDisposed();

      Span.Clear();
    }

    /// <summary>
    /// Resizes the buffer. If smaller than the current size, data will be lost. If larger, the new portions of the buffer will be zeroed values.
    /// </summary>
    /// <param name="newSize">The requested new size.</param>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the new size is negative.</exception>
    public void Resize(int newSize)
    {
      CheckDisposed();

      if (newSize < 0)
        throw new ArgumentOutOfRangeException(nameof(newSize), StringLocalizer.Instance.GetLocalizedString("MustBeNonNegative"));

      if (newSize == m_memory.Length)
        return;

      m_memory.Reallocate(newSize, true);
    }

    /// <summary>
    /// Forms a span slice from the buffer at the specified index.
    /// </summary>
    /// <param name="index">The index at which to begin the slice.</param>
    /// <returns>A span of elements from the start index to the end of the span.</returns>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the index is out of range.</exception>
    public Span<T> Slice(int index)
    {
      CheckDisposed();

      return Span.Slice(index);
    }

    /// <summary>
    /// Forms a span slice from the buffer at the specified index.
    /// </summary>
    /// <param name="index">The index at which to begin the slice.</param>
    /// <param name="count">Number of elements in the slice.</param>
    /// <returns>A span of elements from the start index to the end of the span.</returns>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the index is out of range.</exception>
    public Span<T> Slice(int index, int count)
    {
      CheckDisposed();

      return Span.Slice(index, count);
    }

    /// <summary>
    /// Copies the contents of the collection to this buffer.
    /// </summary>
    /// <param name="data">Data to copy.</param>
    /// <param name="resizeToFit">If the data to copy is larger than the available space, true to resize the buffer, else only copy as many elements that fit.</param>
    /// <returns>The number of elements copied.</returns>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    public int CopyFrom(IEnumerable<T> data, bool resizeToFit = true)
    {
      return CopyFrom(0, data, resizeToFit);
    }

    /// <summary>
    /// Copies the contents of the collection to this buffer.
    /// </summary>
    /// <param name="data">Data to copy.</param>
    /// <param name="count">Requested number of elements to copy from the collection.</param>
    /// <param name="resizeToFit">If the data to copy is larger than the available space, true to resize the buffer, else only copy as many elements that fit.</param>
    /// <returns>The number of elements copied.</returns>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    public int CopyFrom(IEnumerable<T> data, int count, bool resizeToFit = true)
    {
      return CopyFrom(0, data, count, resizeToFit);
    }

    /// <summary>
    /// Copies the contents of the read-only span to this buffer.
    /// </summary>
    /// <param name="data">Data to copy.</param>
    /// <param name="resizeToFit">If the data to copy is larger than the available space, true to resize the buffer, else only copy as many elements that fit.</param>
    /// <returns>The number of elements copied.</returns>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    public int CopyFrom(ReadOnlySpan<T> data, bool resizeToFit = true)
    {
      return CopyFrom(0, data, resizeToFit);
    }

    /// <summary>
    /// Copies the contents of the array to this buffer.
    /// </summary>
    /// <param name="data">Data to copy.</param>
    /// <param name="resizeToFit">If the data to copy is larger than the available space, true to resize the buffer, else only copy as many elements that fit.</param>
    /// <returns>The number of elements copied.</returns>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    public int CopyFrom(T[] data, bool resizeToFit = true)
    {
      return CopyFrom(0, new ReadOnlySpan<T>(data), resizeToFit);
    }

    /// <summary>
    /// Copies the contents of the collection to this buffer.
    /// </summary>
    /// <param name="index">Starting index to copy the data into this buffer.</param>
    /// <param name="data">Data to copy.</param>
    /// <param name="resizeToFit">If the data to copy is larger than the available space, true to resize the buffer, else only copy as many elements that fit.</param>
    /// <returns>The number of elements copied.</returns>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the index or count are negative.</exception>
    public int CopyFrom(int index, IEnumerable<T> data, bool resizeToFit = true)
    {
      if (data is IReadOnlyDataBuffer<T> db)
        return CopyFrom(index, db.Span, resizeToFit);
      else if (data is T[] arr)
        return CopyFrom(index, arr.AsSpan(), resizeToFit);
      else if (data is IReadOnlyRefList<T> rl)
        return CopyFrom(index, rl.Span, resizeToFit);
      else if (data is List<T> l)
        return CopyFrom(index, CollectionsMarshal.AsSpan(l), resizeToFit);
      else if (data is ICollection<T> c)
        return CopyFrom(index, data, c.Count, resizeToFit);
      else if (data is IReadOnlyCollection<T> rc)
        return CopyFrom(index, data, rc.Count, resizeToFit);

      return CopyFrom(index, data, data.Count(), resizeToFit);
    }

    /// <summary>
    /// Copies the contents of the read-only span to this buffer.
    /// </summary>
    /// <param name="index">Starting index to copy the data into this buffer.</param>
    /// <param name="data">Data to copy.</param>
    /// <param name="resizeToFit">If the data to copy is larger than the available space, true to resize the buffer, else only copy as many elements that fit.</param>
    /// <returns>The number of elements copied.</returns>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the index or count are negative.</exception>
    public int CopyFrom(int index, ReadOnlySpan<T> data, bool resizeToFit = true)
    {
      CheckDisposed();

      if (index < 0)
        throw new ArgumentOutOfRangeException(nameof(index), StringLocalizer.Instance.GetLocalizedString("MustBeNonNegative"));

      int count = data.Length;
      if (Length < (index + count))
      {
        if (resizeToFit)
          Resize(index + count);
        else
          count = Length - index;
      }

      if (count > 0)
        data.Slice(0, count).CopyTo(Span.Slice(index, count));

      return count;
    }

    /// <summary>
    /// Copies the contents of the array to this buffer.
    /// </summary>
    /// <param name="index">Starting index to copy the data into this buffer.</param>
    /// <param name="data">Data to copy.</param>
    /// <param name="resizeToFit">If the data to copy is larger than the available space, true to resize the buffer, else only copy as many elements that fit.</param>
    /// <returns>The number of elements copied.</returns>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the index or count are negative.</exception>
    public int CopyFrom(int index, T[] data, bool resizeToFit = true)
    {
      return CopyFrom(index, new ReadOnlySpan<T>(data), resizeToFit);
    }

    /// <summary>
    /// Copies the contents of the collection to this buffer.
    /// </summary>
    /// <param name="index">Starting index to copy the data into this buffer.</param>
    /// <param name="data">Data to copy.</param>
    /// <param name="count">Requested number of elements to copy from the collection.</param>
    /// <param name="resizeToFit">If the data to copy is larger than the available space, true to resize the buffer, else only copy as many elements that fit.</param>
    /// <returns>The number of elements copied.</returns>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the index or count are negative.</exception>
    public int CopyFrom(int index, IEnumerable<T> data, int count, bool resizeToFit = true)
    {
      CheckDisposed();

      if (index < 0)
        throw new ArgumentOutOfRangeException(nameof(index), StringLocalizer.Instance.GetLocalizedString("MustBeNonNegative"));

      if (count < 0)
        throw new ArgumentOutOfRangeException(nameof(count), StringLocalizer.Instance.GetLocalizedString("MustBeNonNegative"));

      if (Length < (index + count))
      {
        if (resizeToFit)
          Resize(index + count);
        else
          count = Length - index;
      }

      if (count > 0)
      {
        Span<T> span = Span;
        int num = 0;
        foreach (T value in data)
        {
          span[index] = value;
          index++;
          num++;

          if (num == count)
            break;
        }
      }

      return count;
    }

    /// <summary>
    /// Determines whether this buffer contains the object.
    /// </summary>
    /// <param name="value">Value that may be in the list.</param>
    /// <returns>True if the value was found, false otherwise.</returns>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    public bool Contains(in T value)
    {
      return IndexOf(value) != -1;
    }

    /// <summary>
    /// Determines whether this buffer contains the object.
    /// </summary>
    /// <param name="value">Value that may be in the list.</param>
    /// <param name="comparer">Optional value comparer.</param>
    /// <returns>True if the value was found, false otherwise.</returns>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    public bool Contains(in T value, RefEqualityComparer<T>? comparer)
    {
      return IndexOf(value, 0, Length, comparer) != -1;
    }

    /// <summary>
    /// Returns the index of a particular value, if it is in the buffer. If not, a value of -1 is returned.
    /// </summary>
    /// <param name="value">Value to search for.</param>
    /// <returns>Returns the index of the value, or -1 if it is not in the buffer.</returns>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    public int IndexOf(in T value)
    {
      return IndexOf(value, 0, Length, null);
    }

    /// <summary>
    /// Returns the index of a particular value, if it is in the buffer. If not, a value of -1 is returned.
    /// </summary>
    /// <param name="value">Value to search for.</param>
    /// <param name="startIndex">Start index to begin searching.</param>
    /// <param name="comparer">Optional value comparer.</param>
    /// <returns>Returns the index of the value, or -1 if it is not in the buffer.</returns>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    public int IndexOf(in T value, int startIndex, RefEqualityComparer<T>? comparer = null)
    {
      return IndexOf(value, startIndex, Length - startIndex, comparer);
    }

    /// <summary>
    /// Returns the index of a particular value, if it is in the buffer. If not, a value of -1 is returned.
    /// </summary>
    /// <param name="value">Value to search for.</param>
    /// <param name="startIndex">Start index to begin searching.</param>
    /// <param name="count">Number of elements to search.</param>
    /// <param name="comparer">Optional value comparer.</param>
    /// <returns>Returns the index of the value, or -1 if it is not in the buffer.</returns>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    public int IndexOf(in T value, int startIndex, int count, RefEqualityComparer<T>? comparer = null)
    {
      CheckDisposed();

      if (!BufferHelper.IsInRange(startIndex, count, Length))
        return -1;

      RefEqualityComparer<T> compr = comparer ?? RefEqualityComparer<T>.Default;
      Span<T> span = Span;
      for (int i = startIndex; i < startIndex + count; i++)
      {
        ref readonly T otherValue = ref span[i];
        if (compr.Equals(in value, in otherValue))
          return i;
      }

      return -1;
    }

    /// <summary>
    /// Copies the contents of the entire list into an array, starting at the specified index of the target array.
    /// </summary>
    /// <param name="array">Destination array to hold the elements.</param>
    /// <param name="arrayIndex">Zero-based index in <paramref name="array" /> at which copying begins.</param>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the index is out of range.</exception>
    void IReadOnlyRefList<T>.CopyTo(T[] array, int arrayIndex)
    {
      CheckDisposed();

      Span.CopyTo(array.AsSpan<T>(arrayIndex));
    }

    /// <summary>
    /// Returns an enumerator that iterates through the contents of the buffer.
    /// </summary>
    /// <returns>Data buffer enumerator</returns>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    public Enumerator GetEnumerator()
    {
      CheckDisposed();

      return new Enumerator(this);
    }

    /// <summary>
    /// Returns an enumerator that iterates through the contents of the buffer.
    /// </summary>
    /// <returns>Data buffer enumerator</returns>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    IEnumerator<T> IEnumerable<T>.GetEnumerator() { return GetEnumerator(); }

    /// <summary>
    /// Returns an enumerator that iterates through a collection.
    /// </summary>
    /// <returns>Data buffer enumerator</returns>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    IEnumerator IEnumerable.GetEnumerator() { return GetEnumerator(); }

    /// <summary>
    /// Returns a string with the name of the type and the number of elements.
    /// </summary>
    /// <returns>String representation</returns>
    public override string ToString()
    {
      return $"Tesla.DataBuffer<{typeof(T).Name}>[{Length}]";
    }

    /// <summary>
    /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
    /// </summary>
    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    private void Dispose(bool disposing)
    {
      if (!m_isDisposed)
      {
        if (disposing)
        {
          if (m_isMemoryOwned)
            m_memory.Dispose();
        }
      }

      m_isDisposed = true;
    }

    private void CheckDisposed()
    {
      if (m_isDisposed)
        throw new ObjectDisposedException(StringLocalizer.Instance.GetLocalizedString("ObjectDisposed"));
    }

    /// <summary>
    /// Performs an explicit conversion from <see cref="DataBuffer{T}"/> to <see cref="ReadOnlySpan{T}"/>.
    /// </summary>
    /// <param name="value">Data buffer</param>
    public static implicit operator ReadOnlySpan<T>(DataBuffer<T> value)
    {
      return value.m_memory.Span;
    }

    /// <summary>
    /// Performs an implicit conversion from <see cref="DataBuffer{T}"/> to <see cref="Span{T}"/>.
    /// </summary>
    /// <param name="value">Data buffer</param>
    public static implicit operator Span<T>(DataBuffer<T> value)
    {
      return value.m_memory.Span;
    }

    #region Enumerator

    /// <summary>
    /// Enumerates elements of a <see cref="DataBuffer{T}"/>.
    /// </summary>
    public struct Enumerator : IEnumerator<T>
    {
      private DataBuffer<T> m_buffer;
      private IMemoryOwnerEx<T> m_memory;
      private int m_index;
      private int m_length;

      /// <summary>
      /// Gets the current value.
      /// </summary>
      public ref T Current { [MethodImpl(MethodImplOptions.AggressiveInlining)] get { return ref m_memory[m_index]; } }

      /// <inheritdoc />
      T IEnumerator<T>.Current { [MethodImpl(MethodImplOptions.AggressiveInlining)] get { return m_memory[m_index]; } }

      /// <inheritdoc />
      object IEnumerator.Current { get { return m_memory[m_index]; } }

      internal Enumerator(DataBuffer<T> buffer)
      {
        m_buffer = buffer;
        m_memory = buffer.m_memory;
        m_index = -1;
        m_length = m_memory.Length;
      }

      /// <inheritdoc />
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public bool MoveNext()
      {
        if (m_index >= m_length || m_buffer.IsDisposed)
          return false;

        m_index++;
        return m_index < m_length;
      }

      /// <inheritdoc />
      public void Reset()
      {
        m_index = -1;
      }

      /// <inheritdoc />
      public void Dispose() { }
    }

    #endregion
  }
}
