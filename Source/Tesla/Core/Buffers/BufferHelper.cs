﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Buffers;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Tesla.Graphics;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Methods for manipulating memory buffiers.
  /// </summary>
  public static class BufferHelper
  {
    /// <summary>
    /// The large object heap threshold in bytes. An object of this size or greater will be allocated on this heap,
    /// which usually is not as compacted as often as other generations.
    /// </summary>
    public static readonly int LOHThreshold = QueryLOHThreshold();

    private static int QueryLOHThreshold()
    {
      IReadOnlyDictionary<string, object> gcConfigs = GC.GetConfigurationVariables();
      if (gcConfigs.TryGetValue("LOHThreshold", out object? threshold) && threshold is int lohThreshold)
        return lohThreshold;
      
      return 85_000;
    }

    /// <summary>
    /// Pins the specified group of buffers.
    /// </summary>
    /// <param name="data">Span of buffers to pin.</param>
    /// <param name="handlesOut">Span containing the pinned handles.</param>
    /// <exception cref="System.ArgumentOutOfRangeException">Thrown if the output handles span is not the same length as the input data.</exception>
    public static void Pin(ReadOnlySpan<IReadOnlyDataBuffer?> data, Span<MemoryHandle> handlesOut)
    {
      if (data.Length != handlesOut.Length)
        throw new ArgumentOutOfRangeException(nameof(handlesOut), StringLocalizer.Instance.GetLocalizedString("LengthMustBeSame"));
      
      for (int i = 0; i < data.Length; i++)
      {
        IReadOnlyDataBuffer? db = data[i];
        handlesOut[i] = (db != null) ? db.Pin() : new MemoryHandle();
      }
    }

    /// <summary>
    /// Pins the specified group of buffers.
    /// </summary>
    /// <param name="data">Span of buffers to pin.</param>
    /// <returns>Array of pinned handles.</returns>
    public static MemoryHandle[] Pin(ReadOnlySpan<IReadOnlyDataBuffer?> data)
    {
      if (data.IsEmpty)
        return Array.Empty<MemoryHandle>();

      MemoryHandle[] handles = new MemoryHandle[data.Length];
      BufferHelper.Pin(data, handles);

      return handles;
    }

    /// <summary>
    /// Pins the specified group of buffers.
    /// </summary>
    /// <param name="data">Array of buffers to pin.</param>
    /// <returns>Array of pinned handles.</returns>
    public static MemoryHandle[] Pin(params IReadOnlyDataBuffer?[]? data)
    {
      if (data is null)
        return Array.Empty<MemoryHandle>();

      MemoryHandle[] handles = new MemoryHandle[data.Length];
      BufferHelper.Pin(data, handles);

      return handles;
    }
    
    /// <summary>
    /// Pins the specified group of buffers. The returned array is pooled.
    /// </summary>
    /// <param name="data">Span of buffers to pin.</param>
    /// <returns>Array of pinned handles.</returns>
    public static PooledArray<MemoryHandle> PinPooled(ReadOnlySpan<IReadOnlyDataBuffer?> data)
    {
      if (data.IsEmpty)
        return default;

      PooledArray<MemoryHandle> handles = new PooledArray<MemoryHandle>(data.Length);
      BufferHelper.Pin(data, handles);

      return handles;
    }

    /// <summary>
    /// Pins the specified group of buffers. The returned array is pooled.
    /// </summary>
    /// <param name="data">Array of buffers to pin.</param>
    /// <returns>Array of pinned handles.</returns>
    public static PooledArray<MemoryHandle> PinPooled(params IReadOnlyDataBuffer?[]? data)
    {
      if (data is null)
        return default;

      PooledArray<MemoryHandle> handles = new PooledArray<MemoryHandle>(data.Length);
      BufferHelper.Pin(data, handles);

      return handles;
    }

    /// <summary>
    /// Unpins the specified group of handles.
    /// </summary>
    /// <param name="handles">Pinned handles to release.</param>
    public static void Unpin(ReadOnlySpan<MemoryHandle> handles)
    {
      for (int i = 0; i < handles.Length; i++)
        handles[i].Dispose();
    }

    /// <summary>
    /// Unpins the specified group of handles.
    /// </summary>
    /// <param name="handles">Pinned handles to release.</param>
    public static void Unpin(params MemoryHandle[] handles)
    {
      BufferHelper.Unpin(new ReadOnlySpan<MemoryHandle>(handles));
    }

    /// <summary>
    /// Maps the specified group of buffers that represent texture data so the mipmaps / slices can be accessed appropiately.
    /// </summary>
    /// <param name="format">Format of the texture data.</param>
    /// <param name="arrayCount">Number of array slices in the texture data.</param>
    /// <param name="mipCount">Number of mipmaps in the texture data.</param>
    /// <param name="width">Width in pixels of the texture.</param>
    /// <param name="height">Height in pixels of the texture..</param>
    /// <param name="data">Group of buffers to map.</param>
    /// <param name="mappedBuffersOut">Span containing the mapped buffers.</param>
    /// <exception cref="System.ArgumentOutOfRangeException">Thrown if the output span is not the same length as the input data.</exception>
    /// <returns>Mapped buffers.</returns>
    public static void Map(SurfaceFormat format, int arrayCount, int mipCount, int width, int height, ReadOnlySpan<IReadOnlyDataBuffer?> data, Span<MappedBuffer> mappedBuffersOut)
    {
      if (data.Length != mappedBuffersOut.Length)
        throw new ArgumentOutOfRangeException(nameof(mappedBuffersOut), StringLocalizer.Instance.GetLocalizedString("LengthMustBeSame"));

      for (int i = 0; i < arrayCount; i++)
      {
        for (int j = 0; j < mipCount; j++)
        {
          int index = Texture.CalculateSubResourceIndex(i, j, mipCount);
          IReadOnlyDataBuffer? db = data[index];
          if (db == null)
          {
            mappedBuffersOut[index] = new MappedBuffer();
            continue;
          }

          int formatSize;
          int widthCount = width;
          int heightCount = height;

          Texture.CalculateMipLevelDimensions(j, ref widthCount, ref heightCount);
          Texture.CalculateRegionSizeInBytes(format, ref widthCount, ref heightCount, out formatSize);

          int rowPitch = widthCount * formatSize;
          int slicePitch = rowPitch * heightCount;
          mappedBuffersOut[index] = new MappedBuffer(db.Pin(), rowPitch, slicePitch);
        }
      }
    }

    /// <summary>
    /// Maps the specified group of buffers that represent texture data so the mipmaps / slices can be accessed appropiately.
    /// </summary>
    /// <param name="format">Format of the texture data.</param>
    /// <param name="arrayCount">Number of array slices in the texture data.</param>
    /// <param name="mipCount">Number of mipmaps in the texture data.</param>
    /// <param name="width">Width in pixels of the texture.</param>
    /// <param name="height">Height in pixels of the texture..</param>
    /// <param name="data">Group of buffers to map.</param>
    /// <returns>Mapped buffers.</returns>
    public static MappedBuffer[] Map(SurfaceFormat format, int arrayCount, int mipCount, int width, int height, ReadOnlySpan<IReadOnlyDataBuffer?> data)
    {
      if (data.IsEmpty)
        return Array.Empty<MappedBuffer>();

      MappedBuffer[] mappedBuffers = new MappedBuffer[data.Length];
      BufferHelper.Map(format, arrayCount, mipCount, width, height, data, mappedBuffers);

      return mappedBuffers;
    }

    /// <summary>
    /// Maps the specified group of buffers that represent texture data so the mipmaps / slices can be accessed appropiately.
    /// </summary>
    /// <param name="format">Format of the texture data.</param>
    /// <param name="arrayCount">Number of array slices in the texture data.</param>
    /// <param name="mipCount">Number of mipmaps in the texture data.</param>
    /// <param name="width">Width in pixels of the texture.</param>
    /// <param name="height">Height in pixels of the texture..</param>
    /// <param name="data">Group of buffers to map.</param>
    /// <returns>Mapped buffers.</returns>
    public static MappedBuffer[] Map(SurfaceFormat format, int arrayCount, int mipCount, int width, int height, params IReadOnlyDataBuffer?[]? data)
    {
      if (data is null)
        return Array.Empty<MappedBuffer>();

      MappedBuffer[] mappedBuffers = new MappedBuffer[data.Length];
      BufferHelper.Map(format, arrayCount, mipCount, width, height, data, mappedBuffers);

      return mappedBuffers;
    }

    /// <summary>
    /// Maps the specified group of buffers that represent texture data so the mipmaps / slices can be accessed appropiately.
    /// The returned array is pooled.
    /// </summary>
    /// <param name="format">Format of the texture data.</param>
    /// <param name="arrayCount">Number of array slices in the texture data.</param>
    /// <param name="mipCount">Number of mipmaps in the texture data.</param>
    /// <param name="width">Width in pixels of the texture.</param>
    /// <param name="height">Height in pixels of the texture..</param>
    /// <param name="data">Group of buffers to map.</param>
    /// <returns>Mapped buffers.</returns>
    public static PooledArray<MappedBuffer> MapPooled(SurfaceFormat format, int arrayCount, int mipCount, int width, int height, ReadOnlySpan<IReadOnlyDataBuffer?> data)
    {
      if (data.IsEmpty)
        return default;

      PooledArray<MappedBuffer> mappedBuffers = new PooledArray<MappedBuffer>(data.Length);
      BufferHelper.Map(format, arrayCount, mipCount, width, height, data, mappedBuffers);

      return mappedBuffers;
    }

    /// <summary>
    /// Maps the specified group of buffers that represent texture data so the mipmaps / slices can be accessed appropiately.
    /// The returned array is pooled.
    /// </summary>
    /// <param name="format">Format of the texture data.</param>
    /// <param name="arrayCount">Number of array slices in the texture data.</param>
    /// <param name="mipCount">Number of mipmaps in the texture data.</param>
    /// <param name="width">Width in pixels of the texture.</param>
    /// <param name="height">Height in pixels of the texture..</param>
    /// <param name="data">Group of buffers to map.</param>
    /// <returns>Mapped buffers.</returns>
    public static PooledArray<MappedBuffer> MapPooled(SurfaceFormat format, int arrayCount, int mipCount, int width, int height, params IReadOnlyDataBuffer?[]? data)
    {
      if (data is null)
        return default;

      PooledArray<MappedBuffer> mappedBuffers = new PooledArray<MappedBuffer>(data.Length);
      BufferHelper.Map(format, arrayCount, mipCount, width, height, data, mappedBuffers);

      return mappedBuffers;
    }

    /// <summary>
    /// Unmaps the specified group of mapped buffers.
    /// </summary>
    /// <param name="mappedBuffers">Mapped buffers.</param>
    public static void Unmap(ReadOnlySpan<MappedBuffer> mappedBuffers)
    {
      for (int i = 0; i < mappedBuffers.Length; i++)
        mappedBuffers[i].Data.Dispose();
    }

    /// <summary>
    /// Unmaps the specified group of mapped buffers.
    /// </summary>
    /// <param name="mappedBuffers">Mapped buffers.</param>
    public static void Unmap(params MappedBuffer[] mappedBuffers)
    {
      BufferHelper.Unmap(new ReadOnlySpan<MappedBuffer>(mappedBuffers));
    }

    /// <summary>
    /// Convienence method to dispose all items in a span.
    /// </summary>
    /// <typeparam name="T">IDisposable type.</typeparam>
    /// <param name="span">Span of disposables.</param>
    public static void DisposeSpan<T>(ReadOnlySpan<T> span) where T : IDisposable?
    {
      if (span.IsEmpty)
        return;

      for (int i = 0; i < span.Length; i++)
      {
        T? disposable = span[i];
        if (disposable is not null)
          disposable.Dispose();
      }
    }

    /// <summary>
    /// Convienence method to dispose all items in the collection
    /// </summary>
    /// <typeparam name="T">IDisposable type.</typeparam>
    /// <param name="collection">Collection of disposables.</param>
    public static void DisposeCollection<T>(IEnumerable<T>? collection) where T : IDisposable?
    {
      if (collection is null)
        return;

      if (collection is IReadOnlyList<T> roList)
      {
        for (int i = 0; i < roList.Count; i++)
        {
          T? disposable = roList[i];
          if (disposable is not null)
            disposable.Dispose();
        }
      } 
      else if (collection is IList<T> list)
      {
        for (int i = 0; i < list.Count; i++)
        {
          T? disposable = list[i];
          if (disposable is not null)
            disposable.Dispose();
        }
      }
      else
      {
        //Otherwise enumerate the collection
        foreach (T? disposable in collection)
        {
          if (disposable is not null)
            disposable.Dispose();
        }
      }
    }

    /// <summary>
    /// Swaps the value between two references.
    /// </summary>
    /// <typeparam name="T">Type of data to swap.</typeparam>
    /// <param name="left">First reference</param>
    /// <param name="right">Second reference</param>
    public static void Swap<T>(ref T left, ref T right)
    {
      T temp = left;
      left = right;
      right = temp;
    }

    /// <summary>
    /// Computes a hash code using the <a href="http://bretm.home.comcast.net/~bretm/hash/6.html">FNV modified algorithm</a>m.
    /// </summary>
    /// <param name="data">Byte data to hash.</param>
    /// <returns>Hash code for the data.</returns>
    public static int ComputeFNVModifiedHashCode(ReadOnlySpan<byte> data)
    {
      if (data.IsEmpty)
        return 0;

      unchecked
      {
        uint p = 16777619;
        uint hash = 2166136261;

        for (int i = 0; i < data.Length; i++)
          hash = (hash ^ data[i]) * p;

        hash += hash << 13;
        hash ^= hash >> 7;
        hash += hash << 3;
        hash ^= hash >> 17;
        hash += hash << 5;

        return (int) hash;
      }
    }

    /// <summary>
    /// Reads a stream until the end is reached into a byte array. Based on
    /// <a href="http://www.yoda.arachsys.com/csharp/readbinary.html">Jon Skeet's implementation</a>.
    /// It is up to the caller to dispose of the stream.
    /// </summary>
    /// <param name="stream">Stream to read all bytes from</param>
    /// <param name="initialLength">Initial buffer length, default is 32K</param>
    /// <returns>The byte array containing all the bytes from the stream</returns>
    public static byte[] ReadStreamFully(Stream stream, int initialLength = 32768)
    {
      if (initialLength < 1)
        initialLength = 32768; //Init to 32K if not a valid initial length

      byte[] buffer = new byte[initialLength];
      int position = 0;
      int chunk;

      while ((chunk = stream.Read(buffer, position, buffer.Length - position)) > 0)
      {
        position += chunk;

        //If we reached the end of the buffer check to see if there's more info
        if (position == buffer.Length)
        {
          int nextByte = stream.ReadByte();

          //If -1 we reached the end of the stream
          if (nextByte == -1)
          {
            return buffer;
          }

          //Not at the end, need to resize the buffer
          byte[] newBuffer = new byte[buffer.Length * 2];
          Array.Copy(buffer, newBuffer, buffer.Length);
          newBuffer[position] = (byte) nextByte;
          buffer = newBuffer;
          position++;
        }
      }

      //Trim the buffer before returning
      byte[] toReturn = new byte[position];
      Array.Copy(buffer, toReturn, position);
      return toReturn;
    }

    /// <summary>
    /// Compares two arrays of bytes for equivalence. 
    /// </summary>
    /// <param name="firstData">First array of data.</param>
    /// <param name="secondData">Second array of data.</param>
    /// <returns>True if both arrays contain the same data, false otherwise.</returns>
    public static bool Compare(byte[]? firstData, byte[]? secondData)
    {
      if (Object.ReferenceEquals(firstData, secondData))
        return true;

      return Compare(firstData ?? ReadOnlySpan<byte>.Empty, secondData ?? ReadOnlySpan<byte>.Empty);
    }

    /// <summary>
    /// Compares two spans of bytes for equivalence. 
    /// </summary>
    /// <param name="firstData">First span of data.</param>
    /// <param name="secondData">Second span of data.</param>
    /// <returns>True if both spans contain the same data, false otherwise.</returns>
    public static bool Compare(ReadOnlySpan<byte> firstData, ReadOnlySpan<byte> secondData)
    {
      if (firstData.Length != secondData.Length)
        return false;

      for (int i = 0; i < firstData.Length; i++)
      {
        if (firstData[i] != secondData[i])
          return false;
      }

      return true;
    }

    /// <summary>
    /// Copies bytes from the source address to the destination address.
    /// </summary>
    /// <param name="dst">Destination memory address.</param>
    /// <param name="src">Source memory address.</param>
    /// <param name="sizeInBytesToCopy">The size in bytes to copy.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void CopyMemory(ref byte dst, ref byte src, int sizeInBytesToCopy)
    {
      Unsafe.CopyBlockUnaligned(ref dst, ref src, (uint) sizeInBytesToCopy);
    }

    /// <summary>
    /// Copies bytes from the source address to the destination address.
    /// </summary>
    /// <param name="dst">Destination memory address.</param>
    /// <param name="src">Source memory address.</param>
    /// <param name="sizeInBytesToCopy">The size in bytes to copy.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static unsafe void CopyMemory(IntPtr dst, IntPtr src, int sizeInBytesToCopy)
    {
      Unsafe.CopyBlockUnaligned((void*) dst, (void*) src, (uint) sizeInBytesToCopy);
    }

    /// <summary>
    /// Reads a value of the specified type from the memory address.
    /// </summary>
    /// <typeparam name="T">Blittable type.</typeparam>
    /// <param name="src">Source memory address.</param>
    /// <returns>Read value</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static T Read<T>(ref byte src) where T : unmanaged
    {
      return Unsafe.ReadUnaligned<T>(ref src);
    }

    /// <summary>
    /// Reads a value of the specified type from the memory address.
    /// </summary>
    /// <typeparam name="T">Blittable type.</typeparam>
    /// <param name="src">Source memory address.</param>
    /// <returns>Read value</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static unsafe T Read<T>(IntPtr src) where T : unmanaged
    {
      return Unsafe.ReadUnaligned<T>((void*) src);
    }

    /// <summary>
    /// Reads a span of values of the specifie type from the memory addres.
    /// </summary>
    /// <typeparam name="T">Blittable type.</typeparam>
    /// <param name="src">Source memory address.</param>
    /// <param name="valuesOut">Span to hold the values.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static unsafe void Read<T>(ref byte src, Span<T> valuesOut) where T : unmanaged
    {
      ref byte dstRef = ref Unsafe.As<T, byte>(ref MemoryMarshal.GetReference(valuesOut));

      Unsafe.CopyBlockUnaligned(ref dstRef, ref src, (uint) (sizeof(T) * valuesOut.Length));
    }

    /// <summary>
    /// Reads a span of values of the specifie type from the memory addres.
    /// </summary>
    /// <typeparam name="T">Blittable type.</typeparam>
    /// <param name="src">Source memory address.</param>
    /// <param name="valuesOut">Span to hold the values.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static unsafe void Read<T>(IntPtr src, Span<T> valuesOut) where T : unmanaged
    {
      ref byte dstRef = ref Unsafe.As<T, byte>(ref MemoryMarshal.GetReference(valuesOut));
      ref byte srcRef = ref Unsafe.AsRef<byte>((void*) src);
      
      Unsafe.CopyBlockUnaligned(ref dstRef, ref srcRef, (uint) (sizeof(T) * valuesOut.Length));
    }

    /// <summary>
    /// Writes a value of the specifie type to the memory address.
    /// </summary>
    /// <typeparam name="T">Blittable type.</typeparam>
    /// <param name="dst">Destination memory address.</param>
    /// <param name="value">Value to write.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Write<T>(ref byte dst, in T value) where T : unmanaged
    {
      Unsafe.WriteUnaligned<T>(ref dst, value);
    }

    /// <summary>
    /// Writes a value of the specifie type to the memory address.
    /// </summary>
    /// <typeparam name="T">Blittable type.</typeparam>
    /// <param name="dst">Destination memory address.</param>
    /// <param name="value">Value to write.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static unsafe void Write<T>(IntPtr dst, in T value) where T : unmanaged
    {
      Unsafe.WriteUnaligned<T>((void*) dst, value);
    }

    /// <summary>
    /// Writes a span of elements to the memory addres.
    /// </summary>
    /// <param name="dst">Destination memory address.</param>typeparam>
    /// <param name="values">Values to write.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static unsafe void Write<T>(ref byte dst, ReadOnlySpan<T> values) where T : unmanaged
    {
      ref byte src = ref Unsafe.As<T, byte>(ref MemoryMarshal.GetReference(values));

      Unsafe.CopyBlockUnaligned(ref dst, ref src, (uint) (sizeof(T) * values.Length));
    }

    /// <summary>
    /// Writes a span of elements to the memory addres.
    /// </summary>
    /// <param name="dst">Destination memory address.</param>typeparam>
    /// <param name="values">Values to write.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static unsafe void Write<T>(IntPtr dst, ReadOnlySpan<T> values) where T : unmanaged
    {
      ref byte srcRef = ref Unsafe.As<T, byte>(ref MemoryMarshal.GetReference(values));
      ref byte dstRef = ref Unsafe.AsRef<byte>((void*) dst);

      Unsafe.CopyBlockUnaligned(ref dstRef, ref srcRef, (uint) (sizeof(T) * values.Length));
    }

    /// <summary>
    /// Returns the size of the blittable type in bytes.
    /// </summary>
    /// <typeparam name="T">Blittable type.</typeparam>
    /// <returns>Size in bytes.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static unsafe int SizeOf<T>() where T : unmanaged
    {
      return sizeof(T);
    }

    /// <summary>
    /// Returns the size of the specified type. This may be dangerous if the type is not blittable or marshallable.
    /// </summary>
    /// <param name="type">Type.</param>
    /// <returns>Size in bytes.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static unsafe int SizeOf(Type type)
    {
      return Marshal.SizeOf(type);
    }

    /// <summary>
    /// Returns the size of the blittable span in bytes.
    /// </summary>
    /// <typeparam name="T">Blittable type.</typeparam>
    /// <param name="span">Span of elements of the type.</param>
    /// <returns>Size in bytes.</returns>

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static unsafe int SizeOf<T>(ReadOnlySpan<T> span) where T : unmanaged
    {
      return sizeof(T) * span.Length;
    }

    /// <summary>
    /// Returns the total size in bytes of the span of buffers.
    /// </summary>
    /// <param name="data">Span of buffers.</param>
    /// <returns>Total size in bytes.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static int SizeOf(ReadOnlySpan<IReadOnlyDataBuffer?> data)
    {
      int totalSize = 0;
      for (int i = 0; i < data.Length; i++)
      {
        IReadOnlyDataBuffer? db = data[i];
        if (db is not null)
          totalSize += db.SizeInBytes;
      }

      return totalSize;
    }

    /// <summary>
    /// Casts a primitive value to an enum value.
    /// </summary>
    /// <typeparam name="TFrom">Primitive type.</typeparam>
    /// <typeparam name="TEnum">Enum type.</typeparam>
    /// <param name="value">Primitive value.</param>
    /// <returns>Enum value.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static unsafe TEnum CastToEnum<TFrom, TEnum>(TFrom value) where TFrom : unmanaged where TEnum : struct, Enum
    {
      return Unsafe.As<TFrom, TEnum>(ref value);
    }

    /// <summary>
    /// Computes the element count given the number of bytes.
    /// </summary>
    /// <typeparam name="T">Blittable type.</typeparam>
    /// <param name="numBytes">Number of bytes.</param>
    /// <returns>Number of elements of type T.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static unsafe int ComputeElementCount<T>(int numBytes) where T : unmanaged
    {
      return (int) MathF.Floor(numBytes / (float) sizeof(T));
    }

    /// <summary>
    /// Computes the element count given the number of bytes and size of a single element.
    /// </summary>
    /// <param name="numBytes">Number of bytes.</param>
    /// <param name="elemSizeInBytes">Element size in bytes.</param>
    /// <returns>Number of elements of the specified size.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static int ComputeElementCount(int numBytes, int elemSizeInBytes)
    {
      if (elemSizeInBytes == 0)
        return 0;

      return (int) MathF.Floor(numBytes / (float) elemSizeInBytes);
    }

    /// <summary>
    /// Computes the element count given the number of bytes and type of the elements.
    /// </summary>
    /// <param name="numBytes">Number of bytes.</param>
    /// <param name="type">Element type.</param>
    /// <returns>Number of elements of the specified type.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static int ComputeElementCount(int numBytes, Type type)
    {
      return (int) MathF.Floor(numBytes / (float) Marshal.SizeOf(type));
    }

    /// <summary>
    /// Constructs a new array of the specified count. If zero, returns the empty array.
    /// </summary>
    /// <typeparam name="T">Element type.</typeparam>
    /// <param name="count">Number of elements.</param>
    /// <returns>Array instance.</returns>
    public static T[] NewArray<T>(int count)
    {
      return (count <= 0) ? Array.Empty<T>() : new T[count];
    }

    /// <summary>
    /// Constructs a span over a single managed object.
    /// </summary>
    /// <typeparam name="T">Object type.</typeparam>
    /// <param name="obj">Managed object.</param>
    /// <returns>Span with the object.</returns>
    public static Span<T> CreateSingleSpan<T>(T obj) where T : class
    {
      return MemoryMarshal.CreateSpan<T>(ref obj, 1);
    }

    /// <summary>
    /// Constructs a readonly span over a single managed ob ject.
    /// </summary>
    /// <typeparam name="T">Object type.</typeparam>
    /// <param name="obj">Managed object.</param>
    /// <returns>Span with the object.</returns>
    public static ReadOnlySpan<T> CreateSingleReadOnlySpan<T>(T obj) where T : class
    {
      return MemoryMarshal.CreateReadOnlySpan<T>(ref obj, 1);
    }

    /// <summary>
    /// Checks if the index-count pair is within the range of [0, totalLength] representing a buffer or collection.
    /// </summary>
    /// <param name="startIndex">Start index in the buffer or collection.</param>
    /// <param name="count">Number of elements to process.</param>
    /// <param name="length">Total length of the buffer or collection.</param>
    /// <returns>True if the parameters are within range, false if exceeds bounds.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool IsInRange(int startIndex, int count, int length)
    {
      if ((uint) startIndex > (uint) length || (uint) count > (uint) (length - startIndex))
        return false;

      return true;
    }

    /// <summary>
    /// Checks if the index is within the range of [0, length] representing a buffer or collection.
    /// </summary>
    /// <param name="index">Index in the buffer or collection.</param>
    /// <param name="length">Total length of the buffer or collection.</param>
    /// <returns>True if the index is within range, false if it exceeds bounds.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool IsInRange(int index, int length)
    {
      if ((uint) index > (uint) length)
        return false;

      return true;
    }
  }
}
