﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Buffers;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Allocates new blocks of memory managed by a memory owner instance.
  /// </summary>
  /// <typeparam name="T">Element type.</typeparam>
  public interface IMemoryAllocator<T> where T : unmanaged
  {
    /// <summary>
    /// Allocates a block of memory as represented by the returned memory owner.
    /// </summary>
    /// <param name="minNumElements">The minimum number of elements the memory owner needs to be able to contain. If zero or less, memory represents a "null pointer" or "zero-lengthed array".</param>
    /// <returns>Object that manages the underlying memory.</returns>
    IMemoryOwnerEx<T> Allocate(int minNumElements);
  }

  /// <summary>
  /// Memory owner with extended operations.
  /// </summary>
  /// <typeparam name="T">Element type.</typeparam>
  public interface IMemoryOwnerEx<T> : IMemoryOwner<T> where T : unmanaged
  {
    /// <summary>
    /// Gets if memory has been disposed or not.
    /// </summary>
    bool IsDisposed { get; }

    /// <summary>
    /// Gets the number of elements contained in the memory block.
    /// </summary>
    int Length { get; }

    /// <summary>
    /// Gets the memory block as a Span.
    /// </summary>
    Span<T> Span { get; }

    /// <summary>
    /// Gets the allocator that allocated this memory.
    /// </summary>
    IMemoryAllocator<T> Allocator { get; }

    /// <summary>
    /// Gets the element at the specified zero-based index.
    /// </summary>
    /// <param name="index">Index of the element.</param>
    /// <returns>Element ref at the index.</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the index is less than zero or greater than than the number of elements in the buffer.</exception>
    ref T this[int index] { get; }

    /// <summary>
    /// Reallocates memory. If the number of elements is less, then the memory is truncated.
    /// Optionally, the owner is able to avoid allocations and re-use larger blocks of memory
    /// unless if trimming is requested.
    /// </summary>
    /// <param name="minNumElements">Minimum number of elements in the memory.</param>
    /// <param name="trimExcess">If true and if possible, ensure the memory returned is the size of the specified number of elements.</param>
    /// <exception cref="System.ObjectDisposedException">Thrown if the buffer has already been disposed.</exception>
    /// <exception cref="System.OutOfMemoryException">Thrown if the buffer cannot be allocated due to insufficient available memory.</exception>
    void Reallocate(int minNumElements, bool trimExcess = false);

    /// <summary>
    /// Clones the block of memory.
    /// </summary>
    /// <returns>New block of memory with contents of this memory copied over.</returns>
    IMemoryOwnerEx<T> Clone();
  }

  /// <summary>
  /// Defines a variety of type-specific memory allocators.
  /// </summary>
  /// <typeparam name="T">Element type.</typeparam>
  public static class MemoryAllocator<T> where T : unmanaged
  {
    /// <summary>
    /// Default memory allocator.
    /// </summary>
    public static IMemoryAllocator<T> Default => Managed;

    /// <summary>
    /// Default pooled memory allocator.
    /// </summary>
    public static IMemoryAllocator<T> DefaultPooled => PooledManaged;

    /// <summary>
    /// Allocator backed by standard .NET arrays.
    /// </summary>

    public static readonly IMemoryAllocator<T> Managed = new ManagedMemoryAllocator<T>();

    /// <summary>
    /// Allocator backed by .NET arrays that are from the Pinned Object Heap (POH).
    /// </summary>
    public static readonly IMemoryAllocator<T> PinnedManaged = new PinnedManagedMemoryAllocator<T>();

    /// <summary>
    /// Allocator backed by .NET arrays that are pooled using the default ArrayPool implementation.
    /// </summary>
    public static readonly IMemoryAllocator<T> PooledManaged = new PooledManagedMemoryAllocator<T>();

    /// <summary>
    /// Allocator backed by unmanaged memory using the NativeMemory malloc. This is always aligned to 16-byte boundaries.
    /// </summary>
    public static readonly IMemoryAllocator<T> Native = new NativeMemoryAllocator<T>();

    /// <summary>
    /// Gets an allocator based on a strategy.
    /// </summary>
    /// <param name="strategy">Allocator strategy type.</param>
    /// <returns>Memory Allocator.</returns>
    public static IMemoryAllocator<T> GetAllocator(MemoryAllocatorStrategy strategy)
    {
      switch (strategy)
      {
        case MemoryAllocatorStrategy.PinnedManaged:
          return PinnedManaged;
        case MemoryAllocatorStrategy.PooledManaged:
          return PooledManaged;
        case MemoryAllocatorStrategy.Native:
          return Native;
        case MemoryAllocatorStrategy.Managed:
          return Managed;
        case MemoryAllocatorStrategy.DefaultPooled:
          return DefaultPooled;
        default:
          return Default;
      }
    }

    /// <summary>
    /// Gets an allocator based on a specified strategy or the best default allocator based on expected buffer size.
    /// </summary>
    /// <param name="totalSizeInBytes">The total size of the buffer in bytes.</param>
    /// <param name="strategy">Optional allocator strategy.</param>
    /// <returns>Memory Allocator.</returns>
    public static IMemoryAllocator<T> GetAllocatorOrDefault(int totalSizeInBytes, MemoryAllocatorStrategy strategy = MemoryAllocatorStrategy.Default)
    {
      MemoryAllocatorStrategy allocType;

      // At some point have a default allocator that automatically switches to account for LOH, for now switch on managed/native allocators.
      // Use native if the requested size will put the managed array on the Large Object Heap.
      if (strategy == MemoryAllocatorStrategy.Default && (totalSizeInBytes >= BufferHelper.LOHThreshold))
        allocType = MemoryAllocatorStrategy.Native;
      else
        allocType = strategy;

      return GetAllocator(allocType);
    }
  }

  /// <summary>
  /// Defines different types of allocators, useful if the type is not known up front but still want to control what
  /// allocator is used. This only is applicable to the built-in memory allocators and not for custom ones.
  /// </summary>
  public enum MemoryAllocatorStrategy
  {
    /// <summary>
    /// Use the default memory allocator, usually backed by managed memory. May be backed by native memory if the size of the buffer
    /// is larger than or equal to the <see cref="BufferHelper.LOHThreshold"/>, usually 85,000 bytes.
    /// </summary>
    Default = 0,

    /// <summary>
    /// Use the default pooled memory allocator, usually backed by managed memory. May be backed by native memory if the size of the buffer
    /// is larger than or equal to the <see cref="BufferHelper.LOHThreshold"/>, usually 85,000 bytes.
    /// </summary>
    /// <remarks>
    /// Note: This is not yet implemented. It will always be the managed pool.
    /// </remarks>
    DefaultPooled = 1,

    /// <summary>
    /// Allocator backed by standard .NET arrays.
    /// </summary>
    Managed = 2,

    /// <summary>
    /// Allocator backed by .NET arrays that are from the Pinned Object Heap (POH).
    /// </summary>
    PinnedManaged = 3,

    /// <summary>
    /// Allocator backed by .NET arrays that are pooled using the default ArrayPool implementation.
    /// </summary>
    PooledManaged = 4,

    /// <summary>
    /// Allocator backed by unmanaged memory using the NativeMemory malloc. This is always aligned to 16-byte boundaries.
    /// </summary>
    Native = 5
  }
}
