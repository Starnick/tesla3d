﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Buffer builder that allows for appending data in circumstances where the final size is not known, as data is appended
  /// the buffer grows. When finished the buffer can be claimed and optionally trimmed.
  /// </summary>
  /// <typeparam name="T">Blittable element type contained in the buffer.</typeparam>
  [DebuggerTypeProxy(typeof(DataBufferBuilderDebugView<>))]
  public class DataBufferBuilder<T> where T : unmanaged
  {
    private int m_position;
    private int m_count;
    private DataBuffer<T>? m_db;
    private IMemoryAllocator<T> m_allocator;

    /// <summary>
    /// Gets or sets the current position pointer of the buffer. If the position
    /// is out of range, it is clamped within the range of [0, Count].
    /// </summary>
    public int Position
    {
      get
      {
        return m_position;
      }
      set
      {
        m_position = MathHelper.Clamp(value, 0, Count);
      }
    }

    /// <summary>
    /// Gets the remaining count in the buffer (Count - Position).
    /// </summary>
    public int RemainingCount
    {
      get
      {
        return m_count - m_position;
      }
    }

    /// <summary>
    /// Gets or sets the number of elements in the buffer.
    /// </summary>
    public int Count 
    {
      get
      { 
        return m_count; 
      }
      set
      {
        if (Capacity < value)
          Capacity = value;

        m_count = MathHelper.Clamp(value, 0, m_db?.Length ?? 0);
        m_position = MathHelper.Clamp(m_position, 0, m_count);
      }
    }

    /// <summary>
    /// Gets the capacity of the buffer. This may be higher than the element count.
    /// </summary>
    public int Capacity
    {
      get
      {
        return (m_db is not null) ? m_db.Length : 0;
      }
      set
      {
        if (m_count == value)
          return;

        DataBuffer<T> db = GetOrCreateBuffer();
        db.Resize(value);
        m_count = Math.Min(m_count, db.Length);
        m_position = Math.Min(m_position, db.Length);
      }
    }

    /// <summary>
    /// Gets the size of a single element in the buffer.
    /// </summary>
    public unsafe int ElementSizeInBytes { get { return (m_db is not null) ? m_db.ElementSizeInBytes : sizeof(T); } }

    /// <summary>
    /// Gets the total size of the buffer in bytes.
    /// </summary>
    public int SizeInBytes { get { return ElementSizeInBytes * m_count; } }

    /// <summary>
    /// Gets the type of the element that the buffer contains.
    /// </summary>
    public Type ElementType { get { return (m_db is not null) ? m_db.ElementType : typeof(T); } }

    /// <summary>
    /// Gets a span of elements, starting at the start of the buffer to the element count.
    /// </summary>
    public Span<T> Span { get { return (m_db is not null) ? m_db.Span.Slice(0, m_count) : Span<T>.Empty; } }

    /// <summary>
    /// Gets a span of elements as bytes, starting at the start of the buffer to the element count.
    /// </summary>
    public Span<byte> Bytes { get { return Span.AsBytes(); } }

    /// <summary>
    /// Gets a span over the remaining elements, starting at the current position of the buffer.
    /// </summary>
    public Span<T> RemainingSpan { get { return (m_db is not null && HasNext) ? m_db.Span.Slice(m_position, RemainingCount) : Span<T>.Empty; } }

    /// <summary>
    /// Gets a span over the remaining elements as bytes, starting at the current position of the buffer.
    /// </summary>
    public Span<byte> RemainingBytes { get { return RemainingSpan.AsBytes(); } }

    /// <summary>
    /// Gets if the current position is less than the current count. If this is false and
    /// the position pointer is incremented, the buffer may be required to grow in capacity.
    /// </summary>
    public bool HasNext
    {
      get
      {
        return m_position < m_count;
      }
    }

    /// <summary>
    /// Gets or sets the allocator used when creating a new buffer.
    /// </summary>
    public IMemoryAllocator<T> Allocator
    {
      get
      {
        return m_allocator;
      }
      set
      {
        m_allocator = value;
      }
    }

    internal DataBuffer<T>? UnderlyingBuffer
    {
      get
      {
        return m_db;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="DataBufferBuilder{T}"/> class.
    /// </summary>
    /// <param name="initialCapacity">Initial capacity of the buffer.</param>
    /// <param name="allocator">Optional memory allocator. If null, default memory allocation strategy is used.</param>
    public DataBufferBuilder(int initialCapacity = 0, IMemoryAllocator<T>? allocator = null)
    {
      m_allocator = allocator ?? MemoryAllocator<T>.GetAllocatorOrDefault(initialCapacity * BufferHelper.SizeOf<T>());
      m_count = 0;
      m_position = 0;
      m_db = (initialCapacity > 0) ? DataBuffer.Create<T>(initialCapacity, m_allocator) : null;
    }

    /// <summary>
    /// Resets the builder and disposes of the current buffer (if any).
    /// </summary>
    public void Clear()
    {
      if (m_db is not null)
        m_db.Dispose();

      m_db = null;
      m_count = 0;
      m_position = 0;
    }

    /// <summary>
    /// Takes ownership of the specified buffer and disposes of the previous held one. To reclaim ownership, call
    /// <see cref="Claim(bool)"/>.
    /// </summary>
    /// <param name="db">Buffer that should back the builder.</param>
    public void CaptureBuffer(DataBuffer<T> db)
    {
      if (m_db is not null)
        m_db.Dispose();

      m_db = db;
      m_count = 0;
      m_position = 0;
    }

    /// <summary>
    /// Gets the element at the current position index in the buffer, incrementing the position index. 
    /// If the position is at the end, a default element is appended and the count incremented.
    /// Capacity is grown as needed.
    /// </summary>
    /// <returns>Reference to an element at the current position index.</returns>
    public ref T Get()
    {
      DataBuffer<T> db = GetOrCreateBuffer();

      if (m_position + 1 > Capacity)
        EnsureCapacity(GetGrowSize(db));

      int index = m_position;
      m_position++;
      if (m_position > m_count)
        m_count++;

      return ref db[index];
    }

    /// <summary>
    /// Gets the element at the specified index in the buffer, without incrementing the position index. The index is only valid between
    /// [0, Count).
    /// </summary>
    /// <param name="index">Zero-based element index in the buffer at which to write the element to.</param>
    /// <returns>Reference to an element at the specified index.</returns>
    /// <exception cref="System.ArgumentOutOfRangeException">Thrown if the index is out of range of the current buffer allocation.</exception>
    public ref T Get(int index)
    {
      if ((uint) index >= (uint) Count)
        throw new ArgumentOutOfRangeException(nameof(index), StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));

      DataBuffer<T> db = GetOrCreateBuffer();
      return ref db[index];
    }

    /// <summary>
    /// Sets the element at the current position index in the buffer, incrementing the position index. If the position is at the end,
    /// the count is incremented and capacity is grown as needed.
    /// </summary>
    /// <param name="value">Value to set at the current position index.</param>
    /// <returns>The index the value was placed in.</returns>
    public int Set(in T value)
    {
      DataBuffer<T> db = GetOrCreateBuffer();

      if (m_position + 1 > Capacity)
        EnsureCapacity(GetGrowSize(db));

      int index = m_position;
      m_position++;
      if (m_position > m_count)
        m_count++;

      db[index] = value;
      return index;
    }

    /// <summary>
    /// Sets the element at the specified index in the buffer, without incrementing the position index. The index is only valid between
    /// [0, Count).
    /// </summary>
    /// <param name="index">Zero-based element index in the buffer at which to write the element to.</param>
    /// <param name="value">Value to write.</param>
    /// <exception cref="System.ArgumentOutOfRangeException">Thrown if the index is out of range of the current buffer allocation.</exception>
    public void Set(int index, in T value)
    {
      if ((uint) index >= (uint) Count)
        throw new ArgumentOutOfRangeException(nameof(index), StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));

      DataBuffer<T> db = GetOrCreateBuffer();
      db[index] = value;
    }

    /// <summary>
    /// Gets the range of elements starting at the current position index in the buffer, incrementing the position index. If the position is at the end
    /// or the number of elements is greater than capacity, the count is incremented and capacity is grown as needed. In this case the returned
    /// elements are default value.
    /// </summary>
    /// <param name="count">Number of elements in the range.</param>
    /// <returns>Range of elements</returns>
    public Span<T> GetRange(int count)
    {
      if (count == 0)
        return Span<T>.Empty;

      DataBuffer<T> db = GetOrCreateBuffer();

      if (m_position + count > Capacity)
        EnsureCapacity(m_position + count);

      int index = m_position;
      m_position += count;
      if (m_position > m_count)
        m_count += count;

      return db.Slice(index, count);
    }

    /// <summary>
    /// Gets a copy of the range of elements starting at the specified index. The index and length of the span must be within [0, Count).
    /// </summary>
    /// <param name="index">Index into the buffer to start copying.</param>
    /// <param name="data">Span of elements to receive the copy.</param>
    public void CopyRange(int index, Span<T> data)
    {
      if ((uint)(index + data.Length) > Count)
        throw new ArgumentOutOfRangeException(nameof(index), StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));

      if (data.IsEmpty)
        return;

      DataBuffer<T> db = GetOrCreateBuffer();
      db.Slice(index, data.Length).CopyTo(data);
    }

    /// <summary>
    /// Sets the range of elements starting at the current position index in the buffer, incrementing the position index. If the position is at the end
    /// or the number of elements is greater than capacity, the count is incremented and capacity is grown as needed.
    /// </summary>
    /// <param name="data">Span of elements</param>
    public void SetRange(ReadOnlySpan<T> data)
    {
      data.CopyTo(GetRange(data.Length));
    }

    /// <summary>
    /// Sets the range of elements starting at the current position index in the buffer, incrementing the position index. If the position is at the end
    /// or the number of elements is greater than capacity, the count is incremented and capacity is grown as needed.
    /// </summary>
    /// <param name="data">Array of elements</param>
    public void SetRange(T[] data)
    {
      SetRange(new ReadOnlySpan<T>(data));
    }

    /// <summary>
    /// Sets the range of elements starting at the current position index in the buffer, incrementing the position index. If the position is at the end
    /// or the number of elements is greater than capacity, the count is incremented and capacity is grown as needed.
    /// </summary>
    /// <param name="data">Span of elements</param>
    public void SetRange(IEnumerable<T> data)
    {
      if (data is IReadOnlyDataBuffer<T> db)
      {
        SetRange(db.Span);
        return;
      }
      else if (data is T[] arr)
      {
        SetRange(arr.AsSpan());
        return;
      }
      else if (data is IReadOnlyRefList<T> rl)
      {
        SetRange(rl.Span);
        return;
      }
      else if (data is List<T> l)
      {
        SetRange(CollectionsMarshal.AsSpan(l));
        return;
      }

      int count = 0;
      if (data is ICollection<T> c)
        count = c.Count;
      else if (data is IReadOnlyCollection<T> rc)
        count = rc.Count;
      else
        count = data.Count();

      if (count == 0)
        return;

      Span<T> span = GetRange(count);
      int index = 0;
      foreach (T value in data)
        span[index++] = value;
    }

    /// <summary>
    /// Checks if the position pointer can be incremented N more times before it reaches the
    /// current count. If false, the buffer may need to grow it's capacity to accomodate more elements.
    /// </summary>
    /// <param name="numElements">Number of elements requested.</param>
    /// <returns>True if the buffer has enough room to add the next N elements.</returns>
    public bool HasNextFor(int numElements)
    {
      if (numElements <= 0)
        return false;

      return (Position + numElements) <= Count;
    }

    /// <summary>
    /// Claims the underlying data buffer, releasing ownership of it from the builder.
    /// </summary>
    /// <param name="trim">True if the buffer should not have excess capacity, false if it can be larger than the current Count.</param>
    /// <returns>Data buffer</returns>
    public DataBuffer<T> Claim(bool trim = false)
    {
      DataBuffer<T> db = GetOrCreateBuffer();
      if (trim && Capacity != Count)
        db.Resize(Count);

      m_db = null;
      m_count = 0;
      m_position = 0;
      return db;
    }

    /// <summary>
    /// Ensures the buffer has the specified capacity
    /// </summary>
    /// <param name="requestedCapacity">Requested capacity</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the requested capacity is less than zero.</exception>
    public void EnsureCapacity(int requestedCapacity)
    {
      if (requestedCapacity < 0)
        throw new ArgumentOutOfRangeException(nameof(requestedCapacity), StringLocalizer.Instance.GetLocalizedString("MustBeNonNegative"));

      if (Capacity < requestedCapacity)
        Capacity = requestedCapacity;
    }

    private DataBuffer<T> GetOrCreateBuffer()
    {
      if (m_db is not null)
        return m_db;

      m_db = DataBuffer.Create<T>(0, m_allocator);
      return m_db;
    }

    private int GetGrowSize(DataBuffer<T> db)
    {
      if (db.Length == 0)
        return 4;

      return db.Length * 2;
    }
  }
}
