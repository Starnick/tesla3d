﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Buffers;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Extension methods for <see cref="ReadOnlySpan{T}"/>, <see cref="Span{T}"/>, <see cref="IntPtr"/>, and <see cref="Array"/> relating to low-level buffer access.
  /// </summary>
  public static class BufferExtensions
  {
    /// <summary>
    /// Gets the total size in bytes of the array.
    /// </summary>
    /// <param name="array">Array of values.</param>
    /// <returns>Size of the array in bytes.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static unsafe int SizeInBytes<T>(this T[] array) where T : unmanaged
    {
      return sizeof(T) * array.Length;
    }

    /// <summary>
    /// Gets the pointer value as an <see cref="IntPtr"/>.
    /// </summary>
    /// <param name="handle">Memory handle</param>
    /// <param name="offset">Optional offset to add to the pointer.</param>
    /// <returns>Pointer to memory block</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static unsafe IntPtr AsIntPointer(this MemoryHandle handle, int offset = 0)
    {
      return new IntPtr((byte*)handle.Pointer + offset);
    }

    /// <summary>
    /// Casts the array to a span of bytes.
    /// </summary>
    /// <param name="array">Array of data.</param>
    /// <returns>Span of bytes</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static unsafe Span<byte> AsBytes<T>(this T[] array) where T : unmanaged
    {
      return MemoryMarshal.AsBytes<T>(new Span<T>(array));
    }

    /// <summary>
    /// Casts the pointer to a span of bytes.
    /// </summary>
    /// <param name="ptr">Pointer to data.</param>
    /// <param name="sizeInBytes">Size of the data in bytes.</param>
    /// <returns>Span of bytes</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static unsafe Span<byte> AsBytes(this IntPtr ptr, int sizeInBytes)
    {
      return new Span<byte>(ptr.ToPointer(), sizeInBytes);
    }

    /// <summary>
    /// Casts the pointer to a span of bytes.
    /// </summary>
    /// <param name="ptr">Pointer to data.</param>
    /// <param name="ptrOffset">Offset to add to the pointer address.</param>
    /// <param name="sizeInBytes">Size of the data in bytes.</param>
    /// <returns>Span of bytes</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static unsafe Span<byte> AsBytes(this IntPtr ptr, int ptrOffset, int sizeInBytes)
    {
      return new Span<byte>((void*) (ptr + ptrOffset), sizeInBytes);
    }


    /// <summary>
    /// Casts the array to a read-only span of bytes.
    /// </summary>
    /// <param name="array">Array of data</param>
    /// <returns>Span of bytes</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static unsafe ReadOnlySpan<byte> AsReadOnlyBytes<T>(this T[] array) where T : unmanaged
    {
      return MemoryMarshal.AsBytes<T>(new ReadOnlySpan<T>(array));
    }

    /// <summary>
    /// Casts the pointer to a read-only span of bytes.
    /// </summary>
    /// <param name="ptr">Pointer to data.</param>
    /// <param name="sizeInBytes">Size of the data in bytes.</param>
    /// <returns>Span of bytes</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static unsafe ReadOnlySpan<byte> AsReadOnlyBytes(this IntPtr ptr, int sizeInBytes)
    {
      return new ReadOnlySpan<byte>(ptr.ToPointer(), sizeInBytes);
    }

    /// <summary>
    /// Casts the pointer to a read-only span of bytes.
    /// </summary>
    /// <param name="ptr">Pointer to data.</param>
    /// <param name="ptrOffset">Offset to add to the pointer address.</param>
    /// <param name="sizeInBytes">Size of the data in bytes.</param>
    /// <returns>Span of bytes</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static unsafe ReadOnlySpan<byte> AsReadOnlyBytes(this IntPtr ptr, int ptrOffset, int sizeInBytes)
    {
      return new ReadOnlySpan<byte>((void*) (ptr + ptrOffset), sizeInBytes);
    }

    /// <summary>
    /// Casts the pointer to a span of elements of the specified type.
    /// </summary>
    /// <typeparam name="T">Element type.</typeparam>
    /// <param name="ptr">Pointer to data.</param>
    /// <param name="elementCount">Number of elements in the span.</param>
    /// <returns>Span of elements</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static unsafe Span<T> AsSpan<T>(this IntPtr ptr, int elementCount) where T : unmanaged
    {
      return new Span<T>(ptr.ToPointer(), elementCount);
    }

    /// <summary>
    /// Casts the pointer to a read-only span of elements of the specified type.
    /// </summary>
    /// <typeparam name="T">Element type.</typeparam>
    /// <param name="ptr">Pointer to data.</param>
    /// <param name="elementCount">Number of elements in the span.</param>
    /// <returns>Span of elements</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static unsafe ReadOnlySpan<T> AsReadOnlySpan<T>(this IntPtr ptr, int elementCount) where T : unmanaged
    {
      return new ReadOnlySpan<T>(ptr.ToPointer(), elementCount);
    }

    /// <summary>
    /// Casts the span to a span of bytes.
    /// </summary>
    /// <typeparam name="T">Element type.</typeparam>
    /// <param name="span">Span to cast.</param>
    /// <returns>Span of bytes</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static unsafe Span<byte> AsBytes<T>(this Span<T> span) where T : unmanaged
    {
      return MemoryMarshal.AsBytes<T>(span);
    }

    /// <summary>
    /// Casts the read-only span to a read-only span of bytes.
    /// </summary>
    /// <typeparam name="T">Element type.</typeparam>
    /// <param name="span">Span to cast.</param>
    /// <returns>Span of bytes</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static unsafe ReadOnlySpan<byte> AsBytes<T>(this ReadOnlySpan<T> span) where T : unmanaged
    {
      return MemoryMarshal.AsBytes<T>(span);
    }

    /// <summary>
    /// Copies the contents of this span into the destination span of bytes.
    /// </summary>
    /// <typeparam name="T">Element type.</typeparam>
    /// <param name="span">Source span.</param>
    /// <param name="dst">Destination span.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void CopyTo<T>(this Span<T> span, Span<byte> dst) where T : unmanaged
    {
      Span<byte> src = MemoryMarshal.AsBytes<T>(span);
      src.CopyTo(dst);
    }

    /// <summary>
    /// Copies the contents of this read-only span into the destination span of bytes.
    /// </summary>
    /// <typeparam name="T">Element type.</typeparam>
    /// <param name="span">Source span.</param>
    /// <param name="dst">Destination span.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void CopyTo<T>(this ReadOnlySpan<T> span, Span<byte> dst) where T : unmanaged
    {
      ReadOnlySpan<byte> src = MemoryMarshal.AsBytes<T>(span);
      src.CopyTo(dst);
    }

    /// <summary>
    /// Copies the contents of this read-only span into the destination span of bytes.
    /// </summary>
    /// <typeparam name="T">Element type.</typeparam>
    /// <param name="span">Source span.</param>
    /// <param name="dst">Destination span.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void CopyTo<T>(this ReadOnlySpan<byte> span, Span<T> dst) where T : unmanaged
    {
      ReadOnlySpan<T> src = MemoryMarshal.Cast<byte, T>(span);
      src.CopyTo(dst);
    }

    /// <summary>
    /// Copies the contents of this span into the destination span of bytes.
    /// </summary>
    /// <typeparam name="T">Element type.</typeparam>
    /// <param name="span">Source span.</param>
    /// <param name="dst">Destination span.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void CopyTo<T>(this Span<byte> span, Span<T> dst) where T : unmanaged
    {
      Span<T> src = MemoryMarshal.Cast<byte, T>(span);
      src.CopyTo(dst);
    }


    /// <summary>
    /// Copies the contents of this span into a memory address.
    /// </summary>
    /// <typeparam name="T">Element type</typeparam>
    /// <param name="span">Source span.</param>
    /// <param name="dst">Destination pointer.</param>
    /// <param name="dstSizeInBytes">Size of the destination in bytes.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void CopyTo<T>(this ReadOnlySpan<T> span, IntPtr dst, int dstSizeInBytes) where T : unmanaged
    {
      ReadOnlySpan<byte> src = MemoryMarshal.AsBytes<T>(span);
      Span<byte> dstSpan = dst.AsBytes(dstSizeInBytes);
      src.CopyTo(dstSpan);
    }

    /// <summary>
    /// Copies the contents of this span into a memory address.
    /// </summary>
    /// <typeparam name="T">Element type</typeparam>
    /// <param name="span">Source span.</param>
    /// <param name="dst">Destination pointer.</param>
    /// <param name="dstSizeInBytes">Size of the destination in bytes.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void CopyTo<T>(this Span<T> span, IntPtr dst, int dstSizeInBytes) where T : unmanaged
    {
      Span<byte> src = MemoryMarshal.AsBytes<T>(span);
      Span<byte> dstSpan = dst.AsBytes(dstSizeInBytes);
      src.CopyTo(dstSpan);
    }

    /// <summary>
    /// Copies the contents of the memory address into this span.
    /// </summary>
    /// <typeparam name="T">Element type</typeparam>
    /// <param name="span">Destination span.</param>
    /// <param name="src">Source pointer.</param>
    /// <param name="srcSizeInBytes">Size of the source in bytes.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void CopyFrom<T>(this Span<T> span, IntPtr src, int srcSizeInBytes) where T : unmanaged
    {
      ReadOnlySpan<byte> srcSpan = src.AsReadOnlyBytes(srcSizeInBytes);
      Span<byte> dst = MemoryMarshal.AsBytes<T>(span);
      srcSpan.CopyTo(dst);
    }

    /// <summary>
    /// Copies the contents of the memory address into this span, starting at the specified index.
    /// </summary>
    /// <typeparam name="T">Element type</typeparam>
    /// <param name="span">Destination span.</param>
    /// <param name="dstIndex">Index into the span at which to start copying to.</param>
    /// <param name="src">Source pointer.</param>
    /// <param name="srcSizeInBytes">Size of the source in bytes.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void CopyFrom<T>(this Span<T> span, int dstIndex, IntPtr src, int srcSizeInBytes) where T : unmanaged
    {
      ReadOnlySpan<byte> srcSpan = src.AsReadOnlyBytes(srcSizeInBytes);
      Span<byte> dst = MemoryMarshal.AsBytes<T>(span.Slice(dstIndex));
      srcSpan.CopyTo(dst);
    }

    /// <summary>
    /// Copies the contents of the read-only byte span into this span.
    /// </summary>
    /// <typeparam name="T">Element type</typeparam>
    /// <param name="span">Destination span.</param>
    /// <param name="src">Source span.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void CopyFrom<T>(this Span<T> span, ReadOnlySpan<byte> src) where T : unmanaged
    {
      Span<byte> dst = MemoryMarshal.AsBytes<T>(span);
      src.CopyTo(dst);
    }

    /// <summary>
    /// Copies the contents of the read-only span of elements into this byte span.
    /// </summary>
    /// <typeparam name="T">Element type</typeparam>
    /// <param name="span">Destination span.</param>
    /// <param name="src">Source span.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void CopyFrom<T>(this Span<byte> span, ReadOnlySpan<T> src) where T : unmanaged
    {
      Span<T> dst = MemoryMarshal.Cast<byte, T>(span);
      src.CopyTo(dst);
    }

    /// <summary>
    /// Reinterprets the data in the specified read-only span to another data type.
    /// </summary>
    /// <typeparam name="TFrom">Source data type.</typeparam>
    /// <typeparam name="TTo">Target data type.</typeparam>
    /// <param name="span">Read-only span to reinterpret.</param>
    /// <returns>Read-only reinterpreted span.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static ReadOnlySpan<TTo> Reinterpret<TFrom, TTo>(this ReadOnlySpan<TFrom> span) where TFrom : unmanaged where TTo : unmanaged
    {
      return MemoryMarshal.Cast<TFrom, TTo>(span);
    }

    /// <summary>
    /// Reinterprets the data in the specified span to another data type.
    /// </summary>
    /// <typeparam name="TFrom">Source data type.</typeparam>
    /// <typeparam name="TTo">Target data type.</typeparam>
    /// <param name="span">Span to reinterpret.</param>
    /// <returns>Reinterpreted span.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Span<TTo> Reinterpret<TFrom, TTo>(this Span<TFrom> span) where TFrom : unmanaged where TTo : unmanaged
    {
      return MemoryMarshal.Cast<TFrom, TTo>(span);
    }

    /// <summary>
    /// Gets a reference to the element at index 0.
    /// </summary>
    /// <typeparam name="T">Element type.</typeparam>
    /// <param name="span">Span from which the reference is retrieved.</param>
    /// <returns>Reference to element at index 0.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static ref T GetRef<T>(this Span<T> span) where T : unmanaged
    {
      return ref MemoryMarshal.GetReference<T>(span);
    }

    /// <summary>
    /// Gets a read-only reference to the element at index 0.
    /// </summary>
    /// <typeparam name="T">Element type.</typeparam>
    /// <param name="span">Read-only Span from which the reference is retrieved.</param>
    /// <returns>Readonly reference to element at index 0.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static ref readonly T GetRef<T>(this ReadOnlySpan<T> span) where T : unmanaged
    {
      return ref MemoryMarshal.GetReference<T>(span);
    }

    /// <summary>
    /// Reinterprets the byte span to the specified data type and gets a reference to it.
    /// </summary>
    /// <typeparam name="T">Element type.</typeparam>
    /// <param name="span">Span of bytes.</param>
    /// <returns>Referene as the specified data type.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static ref T AsRef<T>(this Span<byte> span) where T : unmanaged
    {
      return ref MemoryMarshal.AsRef<T>(span);
    }

    /// <summary>
    /// Reinterprets the read-only byte span to the specified data type and gets a reference to it.
    /// </summary>
    /// <typeparam name="T">Element type.</typeparam>
    /// <param name="span">Read-only span of bytes.</param>
    /// <returns>Read-only referene as the specified data type.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static ref readonly T AsRef<T>(this ReadOnlySpan<byte> span) where T : unmanaged
    {
      return ref MemoryMarshal.AsRef<T>(span);
    }

    /// <summary>
    /// Reads the specified type from the byte span.
    /// </summary>
    /// <typeparam name="T">Element Type.</typeparam>
    /// <param name="span">The span to read from..</param>
    /// <returns>The read value.</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the size of the data exceeds the size of the byte span.</exception>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static unsafe T Read<T>(this Span<byte> span) where T : unmanaged
    {
      int sizeInBytes = sizeof(T);
      if ((uint) sizeInBytes > (uint) span.Length)
        throw new ArgumentOutOfRangeException("Length");

      return Unsafe.ReadUnaligned<T>(ref MemoryMarshal.GetReference(span));
    }

    /// <summary>
    /// Reads the specified type from the read-only byte span.
    /// </summary>
    /// <typeparam name="T">Element Type.</typeparam>
    /// <param name="span">The read-only span to read from.</param>
    /// <returns>The read value.</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the size of the data exceeds the size of the read-only byte span.</exception>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static unsafe T Read<T>(this ReadOnlySpan<byte> span) where T : unmanaged
    {
      int sizeInBytes = sizeof(T);
      if ((uint) sizeInBytes > (uint) span.Length)
        throw new ArgumentOutOfRangeException("Length");

      return Unsafe.ReadUnaligned<T>(ref MemoryMarshal.GetReference(span));
    }

    /// <summary>
    /// Reads the specified number of values from the read-only byte span.
    /// </summary>
    /// <typeparam name="T">Element type.</typeparam>
    /// <param name="span">The read-only span to read from.</param>
    /// <param name="valuesOut">The span to contain the read values. The length determines how many values will be read.</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the size of the data exceeds the size of the read-only byte span.</exception>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static unsafe void Read<T>(this ReadOnlySpan<byte> span, Span<T> valuesOut) where T : unmanaged
    {
      if (valuesOut.IsEmpty)
        return;

      int sizeInBytes = sizeof(T) * valuesOut.Length;
      if ((uint) sizeInBytes > (uint) span.Length)
        throw new ArgumentOutOfRangeException("Length");

      ref byte dst = ref Unsafe.As<T, byte>(ref MemoryMarshal.GetReference(valuesOut));
      ref byte src = ref MemoryMarshal.GetReference(span);
      Unsafe.CopyBlockUnaligned(ref dst, ref src, (uint) sizeInBytes);
    }

    /// <summary>
    /// Reads the specified number of values from the byte span.
    /// </summary>
    /// <typeparam name="T">Element type.</typeparam>
    /// <param name="span">The span to read from.</param>
    /// <param name="valuesOut">The span to contain the read values. The length determines how many values will be read.</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the size of the data exceeds the size of the byte span.</exception>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static unsafe void Read<T>(this Span<byte> span, Span<T> valuesOut) where T : unmanaged
    {
      if (valuesOut.IsEmpty)
        return;

      int sizeInBytes = sizeof(T) * valuesOut.Length;
      if ((uint) sizeInBytes > (uint) span.Length)
        throw new ArgumentOutOfRangeException("Length");

      ref byte dst = ref Unsafe.As<T, byte>(ref MemoryMarshal.GetReference(valuesOut));
      ref byte src = ref MemoryMarshal.GetReference(span);
      Unsafe.CopyBlockUnaligned(ref dst, ref src, (uint) sizeInBytes);
    }

    /// <summary>
    /// Reads the specified type from the read-only byte span and increments the source by sizeof(T) + additionalStepInBytes. If the step size goes beyond
    /// the end of the span, it is set to be empty.
    /// </summary>
    /// <typeparam name="T">Element type.</typeparam>
    /// <param name="span">Source span.</param>
    /// <param name="additionalStepInBytes">Optional additional step size to add to the destination.</param>
    /// <returns>The read value.</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the size of the data exceeds the size of the read-only byte span.</exception>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static unsafe T ReadAndStep<T>(ref this ReadOnlySpan<byte> span, int additionalStepInBytes = 0) where T : unmanaged
    {
      T item = span.Read<T>();
      span = span.TrySlice(sizeof(T) + additionalStepInBytes);
      return item;
    }

    /// <summary>
    /// Reads the specified type from the byte span and increments the source by sizeof(T) + additionalStepInBytes. If the step size goes beyond
    /// the end of the span, it is set to be empty.
    /// </summary>
    /// <typeparam name="T">Element type.</typeparam>
    /// <param name="span">Source span.</param>
    /// <param name="additionalStepInBytes">Optional additional step size to add to the destination.</param>
    /// <returns>The read value.</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the size of the data exceeds the size of the byte span.</exception>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static unsafe T ReadAndStep<T>(ref this Span<byte> span, int additionalStepInBytes = 0) where T : unmanaged
    {
      T item = span.Read<T>();
      span = span.TrySlice(sizeof(T) + additionalStepInBytes);

      return item;
    }

    /// <summary>
    /// Reads the specified values from the read-only byte span and increments the source by (sizeof(T) * valuesOut.Length) + additionalStepInBytes. If the step size goes beyond
    /// the end of the span, it is set to be empty.
    /// </summary>
    /// <typeparam name="T">Element type.</typeparam>
    /// <param name="span">Source span.</param>
    /// <param name="valuesOut">The span to contain the read values. The length determines how many values will be read.</param>
    /// <param name="additionalStepInBytes">Optional additional step size to add to the destination.</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the size of the data exceeds the size of the read-only byte span.</exception>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static unsafe void ReadAndStep<T>(ref this ReadOnlySpan<byte> span, Span<T> valuesOut, int additionalStepInBytes = 0) where T : unmanaged
    {
      span.Read<T>(valuesOut);
      span = span.TrySlice((sizeof(T) * valuesOut.Length) + additionalStepInBytes);
    }

    /// <summary>
    /// Reads the specified values from the byte span and increments the source by (sizeof(T) * valuesOut.Length) + additionalStepInBytes. If the step size goes beyond
    /// the end of the span, it is set to be empty.
    /// </summary>
    /// <typeparam name="T">Element type.</typeparam>
    /// <param name="span">Source span.</param>
    /// <param name="valuesOut">The span to contain the read values. The length determines how many values will be read.</param>
    /// <param name="additionalStepInBytes">Optional additional step size to add to the destination.</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the size of the data exceeds the size of the byte span.</exception>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static unsafe void ReadAndStep<T>(ref this Span<byte> span, Span<T> valuesOut, int additionalStepInBytes = 0) where T : unmanaged
    {
      span.Read<T>(valuesOut);
      span = span.TrySlice((sizeof(T) * valuesOut.Length) + additionalStepInBytes);
    }

    /// <summary>
    /// Writes the value to the destination.
    /// </summary>
    /// <typeparam name="T">Blittable type.</typeparam>
    /// <param name="span">Destination span.</param>
    /// <param name="value">Value to write to the destination.</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if there is not enough space to write the value.</exception>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static unsafe void Write<T>(this Span<byte> span, in T value) where T : unmanaged
    {
      int sizeInBytes = sizeof(T);
      if ((uint) sizeInBytes > (uint) span.Length)
        throw new ArgumentOutOfRangeException("Length");

      Unsafe.WriteUnaligned<T>(ref MemoryMarshal.GetReference(span), value);
    }

    /// <summary>
    /// Writes the value to the destination.
    /// </summary>
    /// <typeparam name="T">Blittable type.</typeparam>
    /// <param name="span">Destination span.</param>
    /// <param name="values">Span of values to write to the destination.</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if there is not enough space to write the values.</exception>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static unsafe void Write<T>(this Span<byte> span, ReadOnlySpan<T> values) where T : unmanaged
    {
      if (values.IsEmpty)
        return;

      int sizeInBytes = sizeof(T) * values.Length;
      if ((uint) sizeInBytes > (uint) span.Length)
        throw new ArgumentOutOfRangeException("Length");

      ref byte src = ref Unsafe.As<T, byte>(ref MemoryMarshal.GetReference(values));
      ref byte dst = ref MemoryMarshal.GetReference(span);
      Unsafe.CopyBlockUnaligned(ref dst, ref src, (uint) sizeInBytes);
    }

    /// <summary>
    /// Writes the value and increments the destination by sizeof(T) + additionalStepInBytes. If the step size goes beyond
    /// the end of the span, it is set to be empty.
    /// </summary>
    /// <typeparam name="T">Blittable type.</typeparam>
    /// <param name="span">Destination span.</param>
    /// <param name="value">Value to write to the destination.</param>
    /// <param name="additionalStepInBytes">Optional additional step size to add to the destination.</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if there is not enough space to write the value.</exception>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static unsafe void WriteAndStep<T>(ref this Span<byte> span, in T value, int additionalStepInBytes = 0) where T : unmanaged
    {
      span.Write<T>(value);
      span = span.TrySlice(sizeof(T) + additionalStepInBytes);
    }

    /// <summary>
    /// Writes the values and increments the destination by (sizeof(T) * values.Length) + additionalStepInBytes. If the step size goes beyond
    /// the end of the span, it is set to be empty.
    /// </summary>
    /// <typeparam name="T">Blittable type.</typeparam>
    /// <param name="span">Destination span.</param>
    /// <param name="values">Span of values to write to the destination.</param>
    /// <param name="additionalStepInBytes">Optional additional step size to add to the destination.</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if there is not enough space to write the values.</exception>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static unsafe void WriteAndStep<T>(ref this Span<byte> span, ReadOnlySpan<T> values, int additionalStepInBytes = 0) where T : unmanaged
    {
      span.Write<T>(values);
      span = span.TrySlice((sizeof(T) * values.Length) + additionalStepInBytes);
    }

    /// <summary>
    /// Tries to slice the span at the starting index. If the slice goes out of range, an empty span is returned.
    /// </summary>
    /// <typeparam name="T">Element type</typeparam>
    /// <param name="span">Span to slice</param>
    /// <param name="start">Starting index</param>
    /// <returns>Span slice, or empty.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Span<T> TrySlice<T>(this Span<T> span, int start)
    {
      if ((uint) start > (uint) span.Length)
        return Span<T>.Empty;

      return span.Slice(start);
    }

    /// <summary>
    /// Tries to slice the span at the starting index and specified length. If the slice goes out of range, an empty span is returned.
    /// </summary>
    /// <typeparam name="T">Element type</typeparam>
    /// <param name="span">Span to slice</param>
    /// <param name="start">Starting index</param>
    /// <param name="length">Number of elements in the slice.</param>
    /// <returns>Span slice, or empty.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Span<T> TrySlice<T>(this Span<T> span, int start, int length)
    {
      if ((uint) start > (uint) span.Length || (uint) length > (uint) (span.Length - start))
        return Span<T>.Empty;

      return span.Slice(start, length);
    }

    /// <summary>
    /// Tries to slice the read-only span at the starting index. If the slice goes out of range, an empty span is returned.
    /// </summary>
    /// <typeparam name="T">Element type</typeparam>
    /// <param name="span">Span to slice</param>
    /// <param name="start">Starting index</param>
    /// <returns>Read-only span slice, or empty.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static ReadOnlySpan<T> TrySlice<T>(this ReadOnlySpan<T> span, int start)
    {
      if ((uint) start > (uint) span.Length)
        return ReadOnlySpan<T>.Empty;

      return span.Slice(start);
    }

    /// <summary>
    /// Tries to slice the read-only span at the starting index and specified length. If the slice goes out of range, an empty span is returned.
    /// </summary>
    /// <typeparam name="T">Element type</typeparam>
    /// <param name="span">Span to slice</param>
    /// <param name="start">Starting index</param>
    /// <param name="length">Number of elements in the slice.</param>
    /// <returns>Read-only span slice, or empty.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static ReadOnlySpan<T> TrySlice<T>(this ReadOnlySpan<T> span, int start, int length)
    {
      if ((uint) start > (uint) span.Length || (uint) length > (uint) (span.Length - start))
        return ReadOnlySpan<T>.Empty;

      return span.Slice(start, length);
    }
  }
}
