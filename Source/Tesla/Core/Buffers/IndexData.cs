﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Buffers;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Unified accessor for index data, which can be either 16-bit or 32-bit integers. The data is backed by an underlying buffer
  /// of the appropiate type, but presented as if the data is always 32-bit indices to make it easier on callers. If a mesh's number of vertices 
  /// is less than or equal to <see cref="ushort.MaxValue"/>, 16-bit indices can be used in order to reduce memory usage.
  /// </summary>
  [DebuggerTypeProxy(typeof(IndexDataDebugView))]
  [DebuggerDisplay("{ToString(),raw}")]
  public struct IndexData : IReadOnlyList<int>, IDisposable
  {
    [DebuggerBrowsable(DebuggerBrowsableState.Never)]
    internal DataBuffer<int>? m_intBuffer;

    [DebuggerBrowsable(DebuggerBrowsableState.Never)]
    private DataBuffer<ushort>? m_shortBuffer;

    /// <summary>
    /// Gets if the underlying data buffer exists.
    /// </summary>
    public readonly bool IsValid { get { return m_intBuffer is not null || m_shortBuffer is not null; } }

    /// <summary>
    /// Gets if the buffer has been disposed or not.
    /// </summary>
    public readonly bool IsDisposed
    {
      get
      {
        if (IsValid)
          return UnderlyingDataBuffer.IsDisposed;

        return true;
      }
    }

    /// <summary>
    /// Gets the number of elements that the buffer contains.
    /// </summary>
    public readonly int Length
    {
      get
      {
        if (IsValid)
          return UnderlyingDataBuffer.Length;

        return 0;
      }
    }

    /// <summary>
    /// Gets the number of elements in the collection.
    /// </summary>
    [DebuggerBrowsable(DebuggerBrowsableState.Never)]
    readonly int IReadOnlyCollection<int>.Count { get { return Length; } }

    /// <summary>
    /// Gets the index format, whether 32-bit or 16-bit.
    /// </summary>
    public readonly IndexFormat IndexFormat { get { return (m_shortBuffer is not null) ? IndexFormat.SixteenBits : IndexFormat.ThirtyTwoBits; } }

    /// <summary>
    /// True if the data is 32-bit, false if 16-bit.
    /// </summary>
    public readonly bool Is32Bit { get { return IndexFormat == IndexFormat.ThirtyTwoBits; } }

    /// <summary>
    /// Gets the total size of the buffer in bytes.
    /// </summary>
    public readonly int SizeInBytes
    {
      get
      {
        if (IsValid)
          return UnderlyingDataBuffer.SizeInBytes;

        return 0;
      }
    }

    /// <summary>
    /// Gets the size of a single element in the buffer.
    /// </summary>
    public readonly int ElementSizeInBytes
    {
      get
      {
        if (IsValid)
          return UnderlyingDataBuffer.ElementSizeInBytes;

        return sizeof(int);
      }
    }

    /// <summary>
    /// Gets the type of the element that the buffer contains.
    /// </summary>
    public readonly Type ElementType
    {
      get
      {
        if (IsValid)
          return UnderlyingDataBuffer.ElementType;

        return typeof(int);
      }
    }

    /// <summary>
    /// Gets the underlying data buffer, which may be null.
    /// </summary>
    public readonly IDataBuffer UnderlyingDataBuffer { get { return (m_shortBuffer is not null) ? m_shortBuffer : m_intBuffer!; } }

    /// <summary>
    /// Gets the underlying buffer as a 32-bit buffer. If the format is 16-bit, this will be null.
    /// </summary>
    public readonly DataBuffer<int> DataBuffer32 { get { return m_intBuffer!; } }

    /// <summary>
    /// Gets the underlying buffer as a 16-bit buffer. If the format is 32-bit, this will be null.
    /// </summary>
    public readonly DataBuffer<ushort> DataBuffer16 { get { return m_shortBuffer!; } }

    /// <summary>
    /// Gets or sets the element at the specified index.
    /// </summary>
    public readonly int this[int index]
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        if (m_intBuffer is not null)
          return m_intBuffer[index];

        if (m_shortBuffer is not null)
          return (int) m_shortBuffer[index];

        return 0;
      }
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      set
      {
        if (m_intBuffer is not null)
          m_intBuffer[index] = value;
        else if (m_shortBuffer is not null)
          m_shortBuffer[index] = (ushort) value;
      }
    }

    /// <summary>
    /// Gets or sets the element at the specified index.
    /// </summary>
    public readonly ushort this[ushort index]
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        if (m_intBuffer is not null)
          return (ushort) m_intBuffer[index];

        if (m_shortBuffer is not null)
          return m_shortBuffer[index];

        return 0;
      }
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      set
      {
        if (m_intBuffer is not null)
          m_intBuffer[index] = value;
        else if (m_shortBuffer is not null)
          m_shortBuffer[index] = value;
      }
    }

    /// <summary>
    /// Gets the buffer contents as a Span of 32-bit integers. If the format is 16-bit, an empty span is returned.
    /// </summary>
    public readonly Span<int> Span32 { get { return (m_intBuffer is not null) ? m_intBuffer.Span : Span<int>.Empty; } }

    /// <summary>
    /// Gets the buffer contents as Span of 16-bit integers. If the format is 32-bit, an empty span is returned.
    /// </summary>
    public readonly Span<ushort> Span16 { get { return (m_shortBuffer is not null) ? m_shortBuffer.Span : Span<ushort>.Empty; } }

    /// <summary>
    /// Gets the buffer contents as a Span of bytes. If the buffer is not valid, an empty span is returned.
    /// </summary>
    public readonly Span<byte> Bytes
    {
      get
      {
        if (IsValid)
          return UnderlyingDataBuffer.Bytes;

        return Span<byte>.Empty;
      }
    }

    /// <summary>
    /// Captures the 32-bit buffer as index data.
    /// </summary>
    /// <param name="data">Buffer to treat as index data.</param>
    public IndexData(DataBuffer<int> data)
    {
      m_intBuffer = data;
      m_shortBuffer = null;
    }

    /// <summary>
    /// Captures the 16-bit buffer as index data.
    /// </summary>
    /// <param name="data">Buffer to treat as index data.</param>
    public IndexData(DataBuffer<ushort> data)
    {
      m_intBuffer = null;
      m_shortBuffer = data;
    }

    /// <summary>
    /// Allocates a new index data buffer of the specified count and format.
    /// </summary>
    /// <param name="count">Size of the index data buffer.</param>
    /// <param name="format">Optional format, defaults to 32-bit.</param>
    /// <param name="allocatorStrategy">Optional memory allocator strategy.</param>
    public IndexData(int count, IndexFormat format = IndexFormat.ThirtyTwoBits, MemoryAllocatorStrategy allocatorStrategy = MemoryAllocatorStrategy.Default)
    {
      if (format == IndexFormat.ThirtyTwoBits)
      {
        m_intBuffer = DataBuffer.Create<int>(count, allocatorStrategy);
        m_shortBuffer = null;
      }
      else
      {
        m_intBuffer = null;
        m_shortBuffer = DataBuffer.Create<ushort>(count, allocatorStrategy);
      }
    }

    /// <summary>
    /// Allocates a new 32-bit index buffer with data copied from the specified read-only span.
    /// </summary>
    /// <param name="buffer">Read-only span with data to copy.</param>
    /// <param name="allocator">Optional memory allocator. If null, default memory allocation strategy is used.</param>
    public IndexData(ReadOnlySpan<int> buffer, IMemoryAllocator<int>? allocator = null)
    {
      m_intBuffer = DataBuffer.Create<int>(buffer, allocator);
      m_shortBuffer = null;
    }

    /// <summary>
    /// Allocates a new 32-bit index buffer with data copied from the specified read-only span.
    /// </summary>
    /// <param name="buffer">Read-only span with data to copy.</param>
    /// <param name="allocatorStrategy">Optional memory allocator strategy.</param>
    public IndexData(ReadOnlySpan<int> buffer, MemoryAllocatorStrategy allocatorStrategy)
    {
      m_intBuffer = DataBuffer.Create<int>(buffer, allocatorStrategy);
      m_shortBuffer = null;
    }

    /// <summary>
    /// Allocates a new 16-bit index buffer with data copied from the specified read-only span.
    /// </summary>
    /// <param name="buffer">Read-only span with data to copy.</param>
    /// <param name="allocator">Optional memory allocator. If null, default memory allocation strategy is used.</param>
    public IndexData(ReadOnlySpan<ushort> buffer, IMemoryAllocator<ushort>? allocator = null)
    {
      m_shortBuffer = DataBuffer.Create<ushort>(buffer, allocator);
      m_intBuffer = null;
    }

    /// <summary>
    /// Allocates a new 16-bit index buffer with data copied from the specified read-only span.
    /// </summary>
    /// <param name="buffer">Read-only span with data to copy.</param>
    /// <param name="allocatorStrategy">Optional memory allocator strategy.</param>
    public IndexData(ReadOnlySpan<ushort> buffer, MemoryAllocatorStrategy allocatorStrategy)
    {
      m_shortBuffer = DataBuffer.Create<ushort>(buffer, allocatorStrategy);
      m_intBuffer = null;
    }

    /// <summary>
    /// Pins the memory and returns a handle to it. Call dispose on the handle to unpin the memory.
    /// </summary>
    /// <returns>A memory handle representing the underlying pinned data.</returns>
    /// <exception cref="NullReferenceException">Throw if the buffer is null.</exception>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    public readonly MemoryHandle Pin()
    {
      CheckIsValid();

      return UnderlyingDataBuffer.Pin();
    }

    /// <summary>
    /// Clones the buffer.
    /// </summary>
    /// <returns>
    /// A new buffer instance containing a duplicate of the data.
    /// </returns>
    /// <exception cref="NullReferenceException">Throw if the buffer is null.</exception>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    public readonly IndexData Clone()
    {
      CheckIsValid();

      if (m_shortBuffer is not null)
        return new IndexData(m_shortBuffer.Clone());
      else if (m_intBuffer is not null)
        return new IndexData(m_intBuffer.Clone());

      return new IndexData();
    }

    /// <summary>
    /// Clears the contents of the buffer, setting to zeroed values.
    /// </summary>
    /// <exception cref="NullReferenceException">Throw if the buffer is null.</exception>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    public readonly void Clear()
    {
      CheckIsValid();

      UnderlyingDataBuffer.Clear();
    }

    /// <summary>
    /// Copies a slice from a <see cref="IndexData"/> beginning at the specified index.
    /// </summary>
    /// <param name="index">The index at which to begin the slice.</param>
    /// <param name="allocatorStrategy">Optional memory allocator strategy.</param>
    /// <returns>Newly allocated buffer containing the contents of the slice of the original buffer</returns>
    /// <exception cref="NullReferenceException">Throw if the buffer is null.</exception>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    public readonly IndexData CopySlice(int index, MemoryAllocatorStrategy allocatorStrategy = MemoryAllocatorStrategy.Default)
    {
      CheckIsValid();

      if (Is32Bit)
        return new IndexData(Span32.Slice(index), allocatorStrategy);
      else
        return new IndexData(Span16.Slice(index), allocatorStrategy);
    }

    /// <summary>
    /// Copies a slice from a <see cref="IndexData"/> beginning at the specified index for a specified length.
    /// </summary>
    /// <param name="index">The index at which to begin the slice.</param>
    /// <param name="count">Number of elements in the slice.</param>
    /// <param name="allocatorStrategy">Optional memory allocator strategy.</param>
    /// <returns>Newly allocated buffer containing the contents of the slice of the original buffer</returns>
    /// <exception cref="NullReferenceException">Throw if the buffer is null.</exception>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    public readonly IndexData CopySlice(int index, int count, MemoryAllocatorStrategy allocatorStrategy = MemoryAllocatorStrategy.Default)
    {
      CheckIsValid();

      if (Is32Bit)
        return new IndexData(Span32.Slice(index, count), allocatorStrategy);
      else
        return new IndexData(Span16.Slice(index, count), allocatorStrategy);
    }

    /// <summary>
    /// Resizes the buffer. If smaller than the current size, data will be lost. If larger, the new portions of the buffer will be zeroed values.
    /// Be careful expanding an existing buffer beyond <see cref="ushort.MaxValue"/> when the format is 16-bits, either the index data needs to
    /// be promoted to 32-bit or the mesh data the index buffer represents needs to be split into several pieces.
    /// </summary>
    /// <param name="newSize">The requested new size.</param>
    /// <exception cref="NullReferenceException">Throw if the buffer is null.</exception>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the new size is negative.</exception>
    public readonly void Resize(int newSize)
    {
      CheckIsValid();

      UnderlyingDataBuffer.Resize(newSize);
    }

    /// <summary>
    /// If the underlying data is 16-bits, promote it to a 32-bit buffer with the same data.
    /// </summary>
    /// <param name="allocator">Optional memory allocator. If null, default memory allocation strategy is used.</param>
    /// <exception cref="NullReferenceException">Throw if the buffer is null.</exception>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    public void PromoteTo32Bits(IMemoryAllocator<int>? allocator = null)
    {
      CheckIsValid();

      if (IndexFormat == IndexFormat.ThirtyTwoBits)
        return;

      DataBuffer<ushort> sbuffer = DataBuffer16;
      m_intBuffer = DataBuffer.Create<int>(sbuffer.Length, allocator);
      m_shortBuffer = null;

      CopyFrom(sbuffer.Span, false);
      sbuffer.Dispose();
    }

    /// <summary>
    /// If the underlying data is 16-bits, promote it to a 32-bit buffer with the same data.
    /// </summary>
    /// <param name="allocator">Memory allocator strategy.</param>
    /// <exception cref="NullReferenceException">Throw if the buffer is null.</exception>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    public void PromoteTo32Bits(MemoryAllocatorStrategy allocatorStrategy)
    {
      PromoteTo32Bits(MemoryAllocator<int>.GetAllocatorOrDefault(Length * sizeof(int), allocatorStrategy));
    }

    /// <summary>
    /// If the underlying data is 32-bits, demote it to a 16-bit buffer with the same data. Use with caution, as if the index values
    /// contained are greater than <see cref="ushort.MaxValue"/> they will overflow.
    /// </summary>
    /// <param name="allocator">Optional memory allocator. If null, default memory allocation strategy is used.</param>
    /// <exception cref="NullReferenceException">Throw if the buffer is null.</exception>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    public void DemoteTo16Bits(IMemoryAllocator<ushort>? allocator = null)
    {
      CheckIsValid();

      if (IndexFormat == IndexFormat.SixteenBits)
        return;

      DataBuffer<int> ibuffer = DataBuffer32;
      m_shortBuffer = DataBuffer.Create<ushort>(ibuffer.Length, allocator);
      m_intBuffer = null;

      CopyFrom(ibuffer.Span, false);
      ibuffer.Dispose();
    }

    /// <summary>
    /// If the underlying data is 32-bits, demote it to a 16-bit buffer with the same data. Use with caution, as if the index values
    /// contained are greater than <see cref="ushort.MaxValue"/> they will overflow.
    /// </summary>
    /// <param name="allocator">Memory allocator strategy.</param>
    /// <exception cref="NullReferenceException">Throw if the buffer is null.</exception>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    public void DemoteTo16Bits(MemoryAllocatorStrategy allocatorStrategy)
    {
      DemoteTo16Bits(MemoryAllocator<ushort>.GetAllocatorOrDefault(Length * sizeof(ushort), allocatorStrategy));
    }

    /// <summary>
    /// Efficiently resizes the buffer and ensures it's 32-bit data.
    /// </summary>
    /// <param name="newSize">The requested new size.</param>
    /// <param name="allocator">Optional memory allocator. If null, default memory allocation strategy is used.</param>
    /// <exception cref="NullReferenceException">Throw if the buffer is null.</exception>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the new size is negative.</exception>
    public void ResizeAndPromoteTo32Bits(int newSize, IMemoryAllocator<int>? allocator = null)
    {
      CheckIsValid();

      if (IndexFormat == IndexFormat.ThirtyTwoBits)
      {
        Resize(newSize);
        return;
      }

      DataBuffer<ushort> sbuffer = DataBuffer16;
      m_intBuffer = DataBuffer.Create<int>(newSize, allocator);
      m_shortBuffer = null;

      CopyFrom(sbuffer.Span, false);
      sbuffer.Dispose();
    }

    /// <summary>
    /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
    /// </summary>
    public void Dispose()
    {
      if (IsValid)
      {
        UnderlyingDataBuffer.Dispose();
        m_shortBuffer = null;
        m_intBuffer = null;
      }
    }

    /// <summary>
    /// Determines whether the value is contained in the buffer.
    /// </summary>
    /// <param name="value">Value to search for.</param>
    /// <exception cref="NullReferenceException">Throw if the buffer is null.</exception>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    public readonly bool Contains(int value)
    {
      return IndexOf(value, 0, Length) != -1;
    }

    /// <summary>
    /// Determines the index of the first occurrence of the value in the buffer, if it exists.
    /// </summary>
    /// <param name="value">Value to search for.</param>
    /// <returns>The zero-based index of the value if found, otherwise -1.</returns>
    /// <exception cref="NullReferenceException">Throw if the buffer is null.</exception>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    public readonly int IndexOf(int value)
    {
      return IndexOf(value, 0, Length);
    }

    /// <summary>
    /// Returns the index of a particular value, if it is in the buffer. If not, a value of -1 is returned.
    /// </summary>
    /// <param name="value">Value to search for.</param>
    /// <param name="startIndex">Start index to begin searching.</param>
    /// <param name="comparer">Optional value comparer.</param>
    /// <returns>Returns the index of the value, or -1 if it is not in the buffer.</returns>
    /// <exception cref="NullReferenceException">Throw if the buffer is null.</exception>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    public readonly int IndexOf(int value, int startIndex)
    {
      return IndexOf(value, startIndex, Length - startIndex);
    }

    /// <summary>
    /// Determines the index of the first occurrence of the value in the buffer, if it exists.
    /// </summary>
    /// <param name="value">Value to search for.</param>
    /// <param name="startIndex">Starting index to begin searching.</param>
    /// <param name="count">Number of elements to search.</param>
    /// <returns>The zero-based index of the value if found, otherwise -1.</returns>
    /// <exception cref="NullReferenceException">Throw if the buffer is null.</exception>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    public readonly int IndexOf(int value, int startIndex, int count)
    {
      CheckIsValid();

      if (m_intBuffer is not null)
        return m_intBuffer.IndexOf(value, startIndex, count);

      if (m_shortBuffer is not null)
        return m_shortBuffer.IndexOf((ushort) value, startIndex, count);

      return -1;
    }

    /// <summary>
    /// Copies the contents of the read-only span to this buffer.
    /// </summary>
    /// <param name="data">Data to copy.</param>
    /// <param name="resizeToFit">If the data to copy is larger than the available space, true to resize the buffer, else only copy as many elements that fit.</param>
    /// <returns>The number of elements copied.</returns>
    /// <exception cref="NullReferenceException">Throw if the buffer is null.</exception>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    public readonly int CopyFrom(ReadOnlySpan<int> data, bool resizeToFit = true)
    {
      return CopyFrom(0, data, resizeToFit);
    }

    /// <summary>
    /// Copies the contents of the array to this buffer.
    /// </summary>
    /// <param name="data">Data to copy.</param>
    /// <param name="resizeToFit">If the data to copy is larger than the available space, true to resize the buffer, else only copy as many elements that fit.</param>
    /// <returns>The number of elements copied.</returns>
    /// <exception cref="NullReferenceException">Throw if the buffer is null.</exception>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    public readonly int CopyFrom(int[] data, bool resizeToFit = true)
    {
      return CopyFrom(0, new ReadOnlySpan<int>(data), resizeToFit);
    }

    /// <summary>
    /// Copies the contents of the collection to this buffer.
    /// </summary>
    /// <param name="data">Data to copy.</param>
    /// <param name="resizeToFit">If the data to copy is larger than the available space, true to resize the buffer, else only copy as many elements that fit.</param>
    /// <returns>The number of elements copied.</returns>
    /// <exception cref="NullReferenceException">Throw if the buffer is null.</exception>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    public readonly int CopyFrom(IEnumerable<int> data, bool resizeToFit = true)
    {
      return CopyFrom(0, data, resizeToFit);
    }

    /// <summary>
    /// Copies the contents of the collection to this buffer.
    /// </summary>
    /// <param name="data">Data to copy.</param>
    /// <param name="count">Requested number of elements to copy from the collection.</param>
    /// <param name="resizeToFit">If the data to copy is larger than the available space, true to resize the buffer, else only copy as many elements that fit.</param>
    /// <returns>The number of elements copied.</returns>
    /// <exception cref="NullReferenceException">Throw if the buffer is null.</exception>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    public readonly int CopyFrom(IEnumerable<int> data, int count, bool resizeToFit = true)
    {
      return CopyFrom(0, data, count, resizeToFit);
    }

    /// <summary>
    /// Copies the contents of the read-only span to this buffer.
    /// </summary>
    /// <param name="index">Starting index to copy the data into this buffer.</param>
    /// <param name="data">Data to copy.</param>
    /// <param name="resizeToFit">If the data to copy is larger than the available space, true to resize the buffer, else only copy as many elements that fit.</param>
    /// <returns>The number of elements copied.</returns>
    /// <exception cref="NullReferenceException">Throw if the buffer is null.</exception>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the index or count are negative.</exception>
    public readonly int CopyFrom(int index, ReadOnlySpan<int> data, bool resizeToFit = true)
    {
      CheckIsValid();
      CheckDisposed();

      if (index < 0)
        throw new ArgumentOutOfRangeException(nameof(index), StringLocalizer.Instance.GetLocalizedString("MustBeNonNegative"));

      // If underlying is int, direct copy
      if (m_intBuffer is not null)
        return m_intBuffer.CopyFrom(index, data, resizeToFit);

      // If underlying is ushort...
      int count = data.Length;
      if (Length < (index + count))
      {
        if (resizeToFit)
          Resize(index + count);
        else
          count = Length - index;
      }

      // Copy ints -> ushorts
      if (count > 0)
      {
        Span<ushort> span = Span16;
        int num = 0;
        foreach (int value in data)
        {
          span[index] = (ushort) value;
          index++;
          num++;

          if (num == count)
            break;
        }
      }

      return count;
    }

    /// <summary>
    /// Copies the contents of the array to this buffer.
    /// </summary>
    /// <param name="index">Starting index to copy the data into this buffer.</param>
    /// <param name="data">Data to copy.</param>
    /// <param name="resizeToFit">If the data to copy is larger than the available space, true to resize the buffer, else only copy as many elements that fit.</param>
    /// <returns>The number of elements copied.</returns>
    /// <exception cref="NullReferenceException">Throw if the buffer is null.</exception>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the index or count are negative.</exception>
    public readonly int CopyFrom(int index, int[] data, bool resizeToFit = true)
    {
      return CopyFrom(index, new ReadOnlySpan<int>(data), resizeToFit);
    }

    /// <summary>
    /// Copies the contents of the collection to this buffer.
    /// </summary>
    /// <param name="index">Starting index to copy the data into this buffer.</param>
    /// <param name="data">Data to copy.</param>
    /// <param name="count">Requested number of elements to copy from the collection.</param>
    /// <param name="resizeToFit">If the data to copy is larger than the available space, true to resize the buffer, else only copy as many elements that fit.</param>
    /// <returns>The number of elements copied.</returns>
    /// <exception cref="NullReferenceException">Throw if the buffer is null.</exception>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the index or count are negative.</exception>
    public readonly int CopyFrom(int index, IEnumerable<int> data, bool resizeToFit = true)
    {
      if (data is IReadOnlyDataBuffer<int> db)
        return CopyFrom(index, db.Span, resizeToFit);
      else if (data is int[] arr)
        return CopyFrom(index, arr.AsSpan(), resizeToFit);
      else if (data is IReadOnlyRefList<int> rl)
        return CopyFrom(index, rl.Span, resizeToFit);
      else if (data is List<int> l)
        return CopyFrom(index, CollectionsMarshal.AsSpan(l), resizeToFit);
      else if (data is ICollection<int> c)
        return CopyFrom(index, data, c.Count, resizeToFit);
      else if (data is IReadOnlyCollection<int> rc)
        return CopyFrom(index, data, rc.Count, resizeToFit);

      return CopyFrom(index, data, data.Count(), resizeToFit);
    }

    /// <summary>
    /// Copies the contents of the collection to this buffer.
    /// </summary>
    /// <param name="index">Starting index to copy the data into this buffer.</param>
    /// <param name="data">Data to copy.</param>
    /// <param name="count">Requested number of elements to copy from the collection.</param>
    /// <param name="resizeToFit">If the data to copy is larger than the available space, true to resize the buffer, else only copy as many elements that fit.</param>
    /// <returns>The number of elements copied.</returns>
    /// <exception cref="NullReferenceException">Throw if the buffer is null.</exception>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the index or count are negative.</exception>
    public readonly int CopyFrom(int index, IEnumerable<int> data, int count, bool resizeToFit = true)
    {
      CheckIsValid();
      CheckDisposed();

      if (index < 0)
        throw new ArgumentOutOfRangeException(nameof(index), StringLocalizer.Instance.GetLocalizedString("MustBeNonNegative"));

      if (count < 0)
        throw new ArgumentOutOfRangeException(nameof(count), StringLocalizer.Instance.GetLocalizedString("MustBeNonNegative"));

      // If underlying is int, direct copy
      if (m_intBuffer is not null)
        return m_intBuffer.CopyFrom(index, data, count, resizeToFit);

      // If underlying is ushort...
      if (Length < (index + count))
      {
        if (resizeToFit)
          Resize(index + count);
        else
          count = Length - index;
      }

      // Copy ints -> ushorts
      if (count > 0)
      {
        Span<ushort> span = Span16;
        int num = 0;
        foreach (int value in data)
        {
          span[index] = (ushort) value;
          index++;
          num++;

          if (num == count)
            break;
        }
      }

      return count;
    }


    /// <summary>
    /// Copies the contents of the read-only span to this buffer.
    /// </summary>
    /// <param name="data">Data to copy.</param>
    /// <param name="resizeToFit">If the data to copy is larger than the available space, true to resize the buffer, else only copy as many elements that fit.</param>
    /// <returns>The number of elements copied.</returns>
    /// <exception cref="NullReferenceException">Throw if the buffer is null.</exception>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    public readonly int CopyFrom(ReadOnlySpan<ushort> data, bool resizeToFit = true)
    {
      return CopyFrom(0, data, resizeToFit);
    }

    /// <summary>
    /// Copies the contents of the array to this buffer.
    /// </summary>
    /// <param name="data">Data to copy.</param>
    /// <param name="resizeToFit">If the data to copy is larger than the available space, true to resize the buffer, else only copy as many elements that fit.</param>
    /// <returns>The number of elements copied.</returns>
    /// <exception cref="NullReferenceException">Throw if the buffer is null.</exception>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    public readonly int CopyFrom(ushort[] data, bool resizeToFit = true)
    {
      return CopyFrom(0, new ReadOnlySpan<ushort>(data), resizeToFit);
    }

    /// <summary>
    /// Copies the contents of the collection to this buffer.
    /// </summary>
    /// <param name="data">Data to copy.</param>
    /// <param name="resizeToFit">If the data to copy is larger than the available space, true to resize the buffer, else only copy as many elements that fit.</param>
    /// <returns>The number of elements copied.</returns>
    /// <exception cref="NullReferenceException">Throw if the buffer is null.</exception>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    public readonly int CopyFrom(IEnumerable<ushort> data, bool resizeToFit = true)
    {
      return CopyFrom(0, data, resizeToFit);
    }

    /// <summary>
    /// Copies the contents of the collection to this buffer.
    /// </summary>
    /// <param name="data">Data to copy.</param>
    /// <param name="count">Requested number of elements to copy from the collection.</param>
    /// <param name="resizeToFit">If the data to copy is larger than the available space, true to resize the buffer, else only copy as many elements that fit.</param>
    /// <returns>The number of elements copied.</returns>
    /// <exception cref="NullReferenceException">Throw if the buffer is null.</exception>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    public readonly int CopyFrom(IEnumerable<ushort> data, int count, bool resizeToFit = true)
    {
      return CopyFrom(0, data, count, resizeToFit);
    }

    /// <summary>
    /// Copies the contents of the read-only span to this buffer.
    /// </summary>
    /// <param name="index">Starting index to copy the data into this buffer.</param>
    /// <param name="data">Data to copy.</param>
    /// <param name="resizeToFit">If the data to copy is larger than the available space, true to resize the buffer, else only copy as many elements that fit.</param>
    /// <returns>The number of elements copied.</returns>
    /// <exception cref="NullReferenceException">Throw if the buffer is null.</exception>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the index or count are negative.</exception>
    public readonly int CopyFrom(int index, ReadOnlySpan<ushort> data, bool resizeToFit = true)
    {
      CheckIsValid();
      CheckDisposed();

      if (index < 0)
        throw new ArgumentOutOfRangeException(nameof(index), StringLocalizer.Instance.GetLocalizedString("MustBeNonNegative"));

      // If underlying is ushort, direct copy
      if (m_shortBuffer is not null)
        return m_shortBuffer.CopyFrom(index, data, resizeToFit);

      // If underlying is int...
      int count = data.Length;
      if (Length < (index + count))
      {
        if (resizeToFit)
          Resize(index + count);
        else
          count = Length - index;
      }

      // Copy ushorts -> ints
      if (count > 0)
      {
        Span<int> span = Span32;
        int num = 0;
        foreach (ushort value in data)
        {
          span[index] = value;
          index++;
          num++;

          if (num == count)
            break;
        }
      }

      return count;
    }

    /// <summary>
    /// Copies the contents of the array to this buffer.
    /// </summary>
    /// <param name="index">Starting index to copy the data into this buffer.</param>
    /// <param name="data">Data to copy.</param>
    /// <param name="resizeToFit">If the data to copy is larger than the available space, true to resize the buffer, else only copy as many elements that fit.</param>
    /// <returns>The number of elements copied.</returns>
    /// <exception cref="NullReferenceException">Throw if the buffer is null.</exception>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the index or count are negative.</exception>
    public readonly int CopyFrom(int index, ushort[] data, bool resizeToFit = true)
    {
      return CopyFrom(index, new ReadOnlySpan<ushort>(data), resizeToFit);
    }

    /// <summary>
    /// Copies the contents of the collection to this buffer.
    /// </summary>
    /// <param name="index">Starting index to copy the data into this buffer.</param>
    /// <param name="data">Data to copy.</param>
    /// <param name="count">Requested number of elements to copy from the collection.</param>
    /// <param name="resizeToFit">If the data to copy is larger than the available space, true to resize the buffer, else only copy as many elements that fit.</param>
    /// <returns>The number of elements copied.</returns>
    /// <exception cref="NullReferenceException">Throw if the buffer is null.</exception>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the index or count are negative.</exception>
    public readonly int CopyFrom(int index, IEnumerable<ushort> data, bool resizeToFit = true)
    {
      if (data is IReadOnlyDataBuffer<ushort> db)
        return CopyFrom(index, db.Span, resizeToFit);
      else if (data is ushort[] arr)
        return CopyFrom(index, arr.AsSpan(), resizeToFit);
      else if (data is IReadOnlyRefList<ushort> rl)
        return CopyFrom(index, rl.Span, resizeToFit);
      else if (data is List<ushort> l)
        return CopyFrom(index, CollectionsMarshal.AsSpan(l), resizeToFit);
      else if (data is ICollection<ushort> c)
        return CopyFrom(index, data, c.Count, resizeToFit);
      else if (data is IReadOnlyCollection<ushort> rc)
        return CopyFrom(index, data, rc.Count, resizeToFit);

      return CopyFrom(index, data, data.Count(), resizeToFit);
    }

    /// <summary>
    /// Copies the contents of the collection to this buffer.
    /// </summary>
    /// <param name="index">Starting index to copy the data into this buffer.</param>
    /// <param name="data">Data to copy.</param>
    /// <param name="count">Requested number of elements to copy from the collection.</param>
    /// <param name="resizeToFit">If the data to copy is larger than the available space, true to resize the buffer, else only copy as many elements that fit.</param>
    /// <returns>The number of elements copied.</returns>
    /// <exception cref="NullReferenceException">Throw if the buffer is null.</exception>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the index or count are negative.</exception>
    public readonly int CopyFrom(int index, IEnumerable<ushort> data, int count, bool resizeToFit = true)
    {
      CheckIsValid();
      CheckDisposed();

      if (index < 0)
        throw new ArgumentOutOfRangeException(nameof(index), StringLocalizer.Instance.GetLocalizedString("MustBeNonNegative"));

      if (count < 0)
        throw new ArgumentOutOfRangeException(nameof(count), StringLocalizer.Instance.GetLocalizedString("MustBeNonNegative"));

      // If underlying is ushort, direct copy
      if (m_shortBuffer is not null)
        return m_shortBuffer.CopyFrom(index, data, count, resizeToFit);

      // If underlying is int...
      if (Length < (index + count))
      {
        if (resizeToFit)
          Resize(index + count);
        else
          count = Length - index;
      }

      // Copy ushorts -> ints
      if (count > 0)
      {
        Span<int> span = Span32;
        int num = 0;
        foreach (ushort value in data)
        {
          span[index] = value;
          index++;
          num++;

          if (num == count)
            break;
        }
      }

      return count;
    }

    /// <summary>
    /// Returns an enumerator that iterates through the contents of the buffer.
    /// </summary>
    /// <returns>Data buffer enumerator</returns>
    /// <exception cref="NullReferenceException">Throw if the buffer is null.</exception>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    public readonly Enumerator GetEnumerator()
    {
      CheckIsValid();
      CheckDisposed();

      return new Enumerator(this);
    }

    /// <summary>
    /// Returns an enumerator that iterates through the contents of the buffer.
    /// </summary>
    /// <returns>Data buffer enumerator</returns>
    /// <exception cref="NullReferenceException">Throw if the buffer is null.</exception>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    readonly IEnumerator<int> IEnumerable<int>.GetEnumerator() { return GetEnumerator(); }

    /// <summary>
    /// Returns an enumerator that iterates through a collection.
    /// </summary>
    /// <returns>Data buffer enumerator</returns>
    /// <exception cref="NullReferenceException">Throw if the buffer is null.</exception>
    /// <exception cref="ObjectDisposedException">Thrown if the buffer has been disposed.</exception>
    readonly IEnumerator IEnumerable.GetEnumerator() { return GetEnumerator(); }

    /// <summary>
    /// Returns a string with the name of the type and the number of elements.
    /// </summary>
    /// <returns>String representation</returns>
    public readonly override string ToString()
    {
      return $"Tesla.IndexData[{Length}]";
    }

    /// <summary>
    /// Wraps a buffer containing 32-bit integers as an <see cref="IndexData"/>.
    /// </summary>
    /// <param name="data">32-bit index buffer</param>
    /// <returns>Index data</returns>
    public static implicit operator IndexData(DataBuffer<int> data)
    {
      return new IndexData(data);
    }

    /// <summary>
    /// Wraps a buffer containing 32-bit integers as an <see cref="IndexData"/>.
    /// </summary>
    /// <param name="data">16-bit index buffer</param>
    /// <returns>Index data</returns>
    public static implicit operator IndexData(DataBuffer<ushort> data)
    {
      return new IndexData(data);
    }

    private readonly void CheckIsValid()
    {
      if (!IsValid)
        throw new NullReferenceException(StringLocalizer.Instance.GetLocalizedString("DataBufferNull"));
    }

    private readonly void CheckDisposed()
    {
      if (IsDisposed)
        throw new ObjectDisposedException(StringLocalizer.Instance.GetLocalizedString("ObjectDisposed"));
    }


    #region Enumerator

    /// <summary>
    /// Enumerates elements of a <see cref="IndexData"/>.
    /// </summary>
    public struct Enumerator : IEnumerator<int>
    {
      private IndexData m_data;
      private int m_index;

      /// <summary>
      /// Gets the current value.
      /// </summary>
      public int Current { [MethodImpl(MethodImplOptions.AggressiveInlining)] get { return m_data[m_index]; } }

      /// <inheritdoc />
      object IEnumerator.Current { get { return m_data[m_index]; } }

      internal Enumerator(IndexData data)
      {
        m_data = data;
        m_index = -1;
      }

      /// <inheritdoc />
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public bool MoveNext()
      {
        int length = m_data.Length;
        if (m_index >= length || m_data.IsDisposed)
          return false;

        m_index++;
        return m_index < length;
      }

      /// <inheritdoc />
      public void Reset()
      {
        m_index = -1;
      }

      /// <inheritdoc />
      public void Dispose() { }
    }

    #endregion
  }
}
