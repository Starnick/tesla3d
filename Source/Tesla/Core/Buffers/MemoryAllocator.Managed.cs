﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Memory allocator that allocates memory backed by .NET arrays.
  /// </summary>
  /// <typeparam name="T">Element type.</typeparam>
  public sealed class ManagedMemoryAllocator<T> : IMemoryAllocator<T> where T : unmanaged
  {
    /// <summary>
    /// Allocates memory as represented by the returned memory owner.
    /// </summary>
    /// <param name="minNumElements">The minimum number of elements the memory owner needs to be able to contain.</param>
    /// <returns>Object that manages the underlying memory.</returns>
    public IMemoryOwnerEx<T> Allocate(int minNumElements)
    {
      return new ArrayMemoryOwner<T>(this, minNumElements, false);
    }
  }

  /// <summary>
  /// Allocates memory backed by .NET arrays that exist on the Pinned Object Heap (POH).
  /// </summary>
  /// <typeparam name="T">Element type.</typeparam>
  public sealed class PinnedManagedMemoryAllocator<T> : IMemoryAllocator<T> where T : unmanaged
  {
    /// <summary>
    /// Allocates memory as represented by the returned memory owner.
    /// </summary>
    /// <param name="minNumElements">The minimum number of elements the memory owner needs to be able to contain.</param>
    /// <returns>Object that manages the underlying memory.</returns>
    public IMemoryOwnerEx<T> Allocate(int minNumElements)
    {
      return new ArrayMemoryOwner<T>(this, minNumElements, true);
    }
  }

  /// <summary>
  /// Represents a memory owner backed by a .NET array.
  /// </summary>
  /// <typeparam name="T">Element type.</typeparam>
  public sealed class ArrayMemoryOwner<T> : IMemoryOwnerEx<T> where T : unmanaged
  {
    private IMemoryAllocator<T> m_allocator;
    private int m_numElements;
    private T[] m_array;
    private bool m_pinned;
    private bool m_isDisposed;

    /// <summary>
    /// Gets if memory has been disposed or not.
    /// </summary>
    public bool IsDisposed { get { return m_isDisposed; } }

    /// <summary>
    /// Gets if the array was allocated on the Pinned Object Heap (POH) or not.
    /// </summary>
    public bool IsPinned { get { return m_pinned; } }

    /// <summary>
    /// Gets the actual length of the array, which might be greater than the number of elements requested.
    /// </summary>
    public int UnderlyingLength { get { return m_array.Length; } }

    /// <summary>
    /// Gets the number of elements contained in the memory block.
    /// </summary>
    public int Length { get { return m_numElements; } }

    /// <summary>
    /// Gets the memory belonging to this owner.
    /// </summary>
    public Memory<T> Memory { get { return new Memory<T>(m_array, 0, m_numElements); } }

    /// <summary>
    /// Gets the span of elements.
    /// </summary>
    public Span<T> Span { get { return new Span<T>(m_array, 0, m_numElements); } }


    /// <summary>
    /// Gets the allocator that allocated this memory.
    /// </summary>
    public IMemoryAllocator<T> Allocator { get { return m_allocator; } }

    /// <summary>
    /// Gets the element at the specified zero-based index.
    /// </summary>
    /// <param name="index">Index of the element.</param>
    /// <returns>Element ref at the index.</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the index is less than zero or greater than than the number of elements in the buffer.</exception>
    public ref T this[int index] { get { return ref m_array[index]; } }


    /// <summary>
    /// Constructs a new instance of the <see cref="ArrayMemoryOwner{T}"/> class.
    /// </summary>
    /// <param name="allocator">Allocator that created this.</param>
    /// <param name="numElements">Number of elements the array should contain.</param>
    /// <param name="pinned">True if the array should be allocated on the POH or not.</param>
    public ArrayMemoryOwner(IMemoryAllocator<T> allocator, int numElements, bool pinned = false)
    {
      m_isDisposed = false;
      m_numElements = Math.Max(0, numElements);
      m_pinned = pinned;
      m_array = (m_numElements > 0) ? GC.AllocateArray<T>(numElements, pinned) : Array.Empty<T>();
      m_allocator = allocator;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="ArrayMemoryOwner{T}"/> class.
    /// </summary>
    /// <param name="data">Array of elements to wrap.</param>
    /// <param name="numElements">Number of elements that should be spannable.</param>
    public ArrayMemoryOwner(T[] data, int numElements)
    {
      m_isDisposed = false;
      m_numElements = Math.Max(0, numElements);
      m_pinned = false;
      m_array = data;
      m_allocator = MemoryAllocator<T>.Managed;
    }

    /// <summary>
    /// Clones the block of memory.
    /// </summary>
    /// <returns>New block of memory with contents of this memory copied over.</returns>
    IMemoryOwnerEx<T> IMemoryOwnerEx<T>.Clone()
    {
      return Clone();
    }

    /// <summary>
    /// Clones the block of memory.
    /// </summary>
    /// <returns>New block of memory with contents of this memory copied over.</returns>
    public ArrayMemoryOwner<T> Clone()
    {
      ArrayMemoryOwner<T> buffer = new ArrayMemoryOwner<T>(m_allocator, m_numElements, m_pinned);
      if (!Span.IsEmpty)
        Span.CopyTo(buffer.Span);

      return buffer;
    }

    /// <summary>
    /// Reallocates memory. If the number of elements is less, then the memory is truncated.
    /// Optionally, the owner is able to avoid allocations and re-use larger blocks of memory
    /// unless if trimming is requested.
    /// </summary>
    /// <param name="minNumElements">Minimum number of elements in the memory.</param>
    /// <param name="trimExcess">If true and if possible, ensure the memory returned is the size of the specified number of elements.</param>
    /// <exception cref="System.ObjectDisposedException">Thrown if the buffer has already been disposed.</exception>
    /// <exception cref="System.OutOfMemoryException">Thrown if the buffer cannot be allocated due to insufficient available memory.</exception>
    public void Reallocate(int minNumElements, bool trimExcess = false)
    {
      if (m_isDisposed)
        throw new ObjectDisposedException(nameof(ArrayMemoryOwner<T>));

      minNumElements = Math.Max(0, minNumElements);

      // Can avoid allocating and re-use the existing array
      if (!trimExcess && minNumElements <= m_numElements)
      {
        m_numElements = minNumElements;
        return;
      }

      // Otherwise allocate a new array and copy to it
      T[] newArray = (minNumElements > 0) ? GC.AllocateArray<T>(minNumElements, m_pinned) : Array.Empty<T>();

      int elemsToCopy = Math.Min(m_numElements, minNumElements);
      if (elemsToCopy > 0)
        Array.Copy(m_array, newArray, elemsToCopy);

      m_numElements = minNumElements;
      m_array = newArray;
    }

    /// <summary>
    /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
    /// </summary>
    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    private void Dispose(bool disposing)
    {
      if (disposing)
        m_array = Array.Empty<T>();

      m_isDisposed = true;
      m_numElements = 0;
    }
  }
}