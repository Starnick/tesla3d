﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Concurrent;
using System.Reflection;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Extension methods for <see cref="Type"/>.
  /// </summary>
  public static class TypeExtensions
  {
    private static ConcurrentDictionary<Type, bool> s_isUnmanaged = new ConcurrentDictionary<Type, bool>();

    /// <summary>
    /// Determines if this type satisfies the "unmanaged" constraint, meaning it's a value type without any managed references.
    /// </summary>
    /// <param name="type">Type to check.</param>
    /// <returns>True if the type is unmanaged, false if it contains references or is a class.</returns>
    public static bool IsUnmanaged(this Type type)
    {
      if (!s_isUnmanaged.TryGetValue(type, out bool isUnmanaged))
      {
        if (!type.IsValueType)
        {
          isUnmanaged = false;
        } 
        else if (type.IsPrimitive || type.IsPointer || type.IsEnum)
        {
          isUnmanaged = true;
        } 
        else
        {
          isUnmanaged = true;

          // Check all fields, if any is not unmanaged then this isn't either
          FieldInfo[] fields = type.GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
          foreach (FieldInfo field in fields)
          {
            if (!field.FieldType.IsUnmanaged())
            {
              isUnmanaged = false;
              break;
            }
          }
        }

        s_isUnmanaged.TryAdd(type, isUnmanaged);
      }

      return isUnmanaged;
    }
  }
}
