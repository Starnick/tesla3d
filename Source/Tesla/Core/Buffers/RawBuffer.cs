﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Buffers;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Buffer representing unmanaged memory whether it's allocated (and managed) by the buffer
  /// or an external user buffer for interop scenarios (e.g. memory from native code). Memory
  /// allocated by this buffer is always aligned to 16-byte boundaries.
  /// </summary>
  /// <typeparam name="T">Element type in the buffer.</typeparam>
  public sealed unsafe class RawBuffer<T> : MemoryManager<T>, IMemoryOwnerEx<T> where T : unmanaged
  {
    private IMemoryAllocator<T> m_allocator;
    private byte* m_buffer;
    private int m_numElements;
    private int m_elemSize;
    private bool m_isDisposed;
    private bool m_ownsBuffer;

    /// <summary>
    /// Gets if the buffer has been disposed or not.
    /// </summary>
    public bool IsDisposed { get { return m_isDisposed; } }

    /// <summary>
    /// Gets if the buffer is owned, meaning the memory is managed and calling dispose will free it.
    /// </summary>
    public bool IsBufferOwned { get { return m_ownsBuffer; } }

    /// <summary>
    /// Gets the buffer pointer.
    /// </summary>
    public IntPtr Pointer { get { return new IntPtr(m_buffer); } }

    /// <summary>
    /// Gets if the buffer is a null pointer and has zero-length.
    /// </summary>
    public bool IsNullPointer { get { return Pointer == IntPtr.Zero; } }

    /// <summary>
    /// Gets the number of elements in the buffer.
    /// </summary>
    public int Length { get { return m_numElements; } }

    /// <summary>
    /// Gets the size of each element in the buffer.
    /// </summary>
    public int ElementSizeInBytes { get { return m_elemSize; } }

    /// <summary>
    /// Gets the total size of the buffer in bytes.
    /// </summary>
    public int SizeInBytes { get { return m_elemSize * m_numElements; } }

    /// <summary>
    /// Gets the Span representing this buffer.
    /// </summary>
    public Span<T> Span { get { return new Span<T>(m_buffer, m_numElements); } }

    /// <summary>
    /// Gets the Span of bytes representing this buffer.
    /// </summary>
    public Span<byte> Bytes { get { return new Span<byte>(m_buffer, SizeInBytes); } }


    /// <summary>
    /// Gets the allocator that allocated this memory.
    /// </summary>
    public IMemoryAllocator<T> Allocator { get { return m_allocator; } }

    /// <summary>
    /// Gets the element at the specified zero-based index.
    /// </summary>
    /// <param name="index">Index of the element.</param>
    /// <returns>Element ref at the index.</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the index is less than zero or greater than than the number of elements in the buffer.</exception>
    public ref T this[int index]
    {
      get
      {
        if ((uint) index >= (uint) m_numElements)
          throw new ArgumentOutOfRangeException(nameof(index));

        return ref Unsafe.AsRef<T>(m_buffer + (index * m_elemSize));
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="RawBuffer{T}" /> class.
    /// </summary>
    /// <param name="numElements">Number of elements the buffer should contain. If zero, the buffer is null unless resized.</param>
    /// <param name="allocator">Optional allocator that created this.</param>
    /// <exception cref="System.OutOfMemoryException">Thrown if the buffer cannot be allocated due to insufficient available memory.</exception>
    public RawBuffer(int numElements, IMemoryAllocator<T>? allocator = null)
    {
      m_allocator = allocator ?? MemoryAllocator<T>.Native;
      m_numElements = Math.Max(0, numElements);
      m_elemSize = Unsafe.SizeOf<T>();

      int sizeInBytes = SizeInBytes;
      m_buffer = (m_numElements > 0) ? (byte*) NativeMemory.AlignedAlloc((nuint) sizeInBytes, 16) : (byte*) IntPtr.Zero.ToPointer();
      m_isDisposed = false;
      m_ownsBuffer = true;

      // Zero out memory
      if (m_numElements > 0)
        Span.Clear();

      AddMemoryPressure(sizeInBytes, m_ownsBuffer);
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="RawBuffer{T}" /> class. This can copy the contents of the user buffer
    /// or wrap it.
    /// </summary>
    /// <param name="userBuffer">External user buffer whose lifetime is not managed by the buffer. May be a null pointer.</param>
    /// <param name="numElements">Number of elements in the user buffer.</param>
    /// <param name="makeCopy">True if the data should be copied or false if the userBuffer should be wrapped.</param>
    /// <param name="allocator">Optional allocator that created this.</param>
    /// <exception cref="System.OutOfMemoryException">Thrown if the buffer cannot be allocated due to insufficient available memory.</exception>
    public RawBuffer(IntPtr userBuffer, int numElements, bool makeCopy = false, IMemoryAllocator<T>? allocator = null)
    {
      // Force to null pointer if the count or pointer are sketchy
      if (numElements <= 0 || userBuffer == IntPtr.Zero)
      {
        userBuffer = IntPtr.Zero;
        numElements = 0;
      }

      m_allocator = allocator ?? MemoryAllocator<T>.Native;
      m_numElements = numElements;
      m_elemSize = Unsafe.SizeOf<T>();
      m_isDisposed = false;

      int sizeInBytes = SizeInBytes;

      if (makeCopy)
      {
        if (userBuffer != IntPtr.Zero)
        {
          m_buffer = (byte*) NativeMemory.AlignedAlloc((nuint) sizeInBytes, 16);

          // Copy data over
          Buffer.MemoryCopy(userBuffer.ToPointer(), m_buffer, sizeInBytes, sizeInBytes);
        }
        else
        {
          m_buffer = (byte*) IntPtr.Zero.ToPointer();
        }

        m_ownsBuffer = true;
      }
      else
      {
        m_buffer = (byte*) userBuffer.ToPointer();
        m_ownsBuffer = false;
      }

      AddMemoryPressure(sizeInBytes, m_ownsBuffer);
    }


    /// <summary>
    /// Constructs a new instance of the <see cref="RawBuffer{T}" /> class. This will always copy the contents of the
    /// user buffer.
    /// </summary>
    /// <param name="userBuffer">Buffer containing data to copy.</param>
    /// <param name="allocator">Optional allocator that created this.</param>
    /// <exception cref="System.OutOfMemoryException">Thrown if the buffer cannot be allocated due to insufficient available memory.</exception>
    public RawBuffer(Memory<T> userBuffer, IMemoryAllocator<T>? allocator = null)
    {
      m_numElements = userBuffer.Length;
      m_elemSize = Unsafe.SizeOf<T>();

      int sizeInBytes = SizeInBytes;
      m_buffer = (m_numElements > 0) ? (byte*) NativeMemory.AlignedAlloc((nuint) sizeInBytes, 16) : (byte*) IntPtr.Zero.ToPointer();
      m_isDisposed = false;
      m_ownsBuffer = true;
      m_allocator = allocator ?? MemoryAllocator<T>.Native;

      // Copy contents over
      if (m_numElements > 0)
        userBuffer.Span.CopyTo(Span);
    }

    /// <summary>
    /// Finalizes an instance of the <see cref="RawBuffer{T}"/> class.
    /// </summary>
    ~RawBuffer()
    {
      Dispose(false);
    }

    /// <summary>
    /// Returns a memory span that wraps the underlying memory buffer.
    /// </summary>
    /// <returns>A memory span that wraps the underlying memory buffer.</returns>
    public override Span<T> GetSpan()
    {
      return new Span<T>(m_buffer, m_numElements);
    }

    /// <summary>
    /// Returns a handle to the memory that has been pinned and whose address can be taken.
    /// </summary>
    /// <param name="elementIndex">The offset to the element in the memory buffer at which the returned <see cref="T:System.Buffers.MemoryHandle">MemoryHandle</see> points.</param>
    /// <returns>A handle to the memory that has been pinned.</returns>
    /// <exception cref="System.ArgumentOutOfRangeException">elementIndex</exception>
    /// <exception cref="System.InvalidOperationException">Thrown if the buffer represents a null pointer.</exception>
    public override MemoryHandle Pin(int elementIndex = 0)
    {
      if ((uint) elementIndex >= (uint) m_numElements)
        throw new ArgumentOutOfRangeException(nameof(elementIndex));

      if (IsNullPointer)
        throw new InvalidOperationException();

      return new MemoryHandle(m_buffer + (elementIndex * m_elemSize), default, this);
    }

    /// <summary>
    /// Unpins pinned memory so that the garbage collector is free to move it.
    /// </summary>
    public override void Unpin() { }

    /// <summary>
    /// Reallocates memory. If the number of elements is less, then the memory is truncated.
    /// Optionally, the owner is able to avoid allocations and re-use larger blocks of memory
    /// unless if trimming is requested.
    /// </summary>
    /// <param name="minNumElements">Minimum number of elements in the memory.</param>
    /// <param name="trimExcess">If true and if possible, ensure the memory returned is the size of the specified number of elements.</param>
    /// <exception cref="System.OutOfMemoryException">Thrown if the buffer cannot be allocated due to insufficient available memory.</exception>
    void IMemoryOwnerEx<T>.Reallocate(int minNumElements, bool trimExcess)
    {
      Resize(minNumElements);
    }

    /// <summary>
    /// Resizes the buffer to the specified number of elements.
    /// </summary>
    /// <param name="numElements">The number of elements that the buffer should be able to hold.</param>
    /// <exception cref="System.ObjectDisposedException">Thrown if the buffer has already been disposed.</exception>
    /// <exception cref="System.OutOfMemoryException">Thrown if the buffer cannot be allocated due to insufficient available memory.</exception>
    public void Resize(int numElements)
    {
      if (m_isDisposed)
        throw new ObjectDisposedException(nameof(RawBuffer<T>));

      RemoveMemoryPressure(SizeInBytes, m_ownsBuffer);

      numElements = Math.Max(0, numElements);

      int newSizeInBytes = numElements * m_elemSize;
      int bytesToCopy = Math.Min(newSizeInBytes, SizeInBytes);
      m_numElements = numElements;

      // Only call realloc on a non-null owned buffer that was previously allocated. No need to free old buffer in either case.
      bool callAlloc = !m_ownsBuffer || IsNullPointer;
      if (callAlloc)
      {
        byte* newBuffer = (m_numElements > 0) ? (byte*) NativeMemory.AlignedAlloc((nuint) newSizeInBytes, 16) : (byte*) IntPtr.Zero.ToPointer();

        if (bytesToCopy > 0)
          Buffer.MemoryCopy(m_buffer, newBuffer, newSizeInBytes, bytesToCopy);

        m_buffer = newBuffer;
        m_ownsBuffer = true;
      }
      else
      {
        if (newSizeInBytes > 0)
        {
          m_buffer = (byte*) NativeMemory.AlignedRealloc(m_buffer, (nuint) newSizeInBytes, 16);
        }
        else
        {
          // Resizing to zero, odd case but turn it into a null pointer
          NativeMemory.AlignedFree(m_buffer);
          m_buffer = (byte*) IntPtr.Zero.ToPointer();
        }
      }

      // Clear remaining part of buffer if it grew
      if (newSizeInBytes > bytesToCopy)
        Unsafe.InitBlock(m_buffer + bytesToCopy, 0, (uint) (newSizeInBytes - bytesToCopy));

      AddMemoryPressure(newSizeInBytes, m_ownsBuffer);
    }

    /// <summary>
    /// Clones the block of memory.
    /// </summary>
    /// <returns>New block of memory with contents of this memory copied over.</returns>
    IMemoryOwnerEx<T> IMemoryOwnerEx<T>.Clone()
    {
      return new RawBuffer<T>(Memory, m_allocator);
    }

    /// <summary>
    /// Clones the buffer.
    /// </summary>
    /// <returns>New buffer with contents copied over.</returns>
    public RawBuffer<T> Clone()
    {
      return new RawBuffer<T>(Memory, m_allocator);
    }

    /// <summary>
    /// Returns the fully qualified type name of this instance.
    /// </summary>
    /// <returns>A <see cref="T:System.String" /> containing a fully qualified type name.</returns>
    public override String ToString()
    {
      return String.Format(CultureInfo.CurrentCulture, "Pointer: {0}, SizeInBytes: {1}, Length: {2}", Pointer.ToString(), SizeInBytes.ToString(), Length.ToString());
    }

    /// <summary>
    /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
    /// </summary>
    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }


    /// <summary>
    /// Releases unmanaged and - optionally - managed resources
    /// </summary>
    /// <param name="disposing">True to release both managed and unmanaged resources; False to release only unmanaged resources.</param>
    protected override void Dispose(bool disposing)
    {
      if (!m_isDisposed)
      {
        if (m_ownsBuffer && !IsNullPointer)
          NativeMemory.AlignedFree(m_buffer);

        RemoveMemoryPressure(SizeInBytes, m_ownsBuffer);
        m_buffer = (byte*) IntPtr.Zero.ToPointer();
        m_isDisposed = true;
      }
    }

    private void AddMemoryPressure(long sizeInBytes, bool isBufferOwned)
    {
      if (isBufferOwned && sizeInBytes > 0)
        GC.AddMemoryPressure(sizeInBytes);
    }

    private void RemoveMemoryPressure(long sizeInBytes, bool isBufferOwned)
    {
      if (isBufferOwned && sizeInBytes > 0)
        GC.RemoveMemoryPressure(sizeInBytes);
    }
  }
}
