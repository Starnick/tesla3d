﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Buffers;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Wraps an array from an <see cref="ArrayPool{T}"/> and treats it as an array of given length (although the underlying
  /// array may in fact be larger), call dispose to have the array returned. Be careful about passing this by-copy and calling 
  /// dispose on multiple copies; there should only be one distinct owner that manages the lifetime of the rented array.
  /// </summary>
  /// <remarks>
  /// The usage is intended for in places where stackalloc may be unsuitable, either due to the size of the necessary memory
  /// or because the element type are reference types.
  /// </remarks>
  /// <typeparam name="T">Type of element in the array.</typeparam>
  [DebuggerTypeProxy(typeof(PooledArrayDebugView<>))]
  [DebuggerDisplay("{ToString(),raw}")]
  public struct PooledArray<T> : IDisposable, IEnumerable<T>
  {
    private ArrayPool<T>? m_pool;
    private T[]? m_array;
    private int m_length;
    private bool m_disposeContents;

    /// <summary>
    /// Gets if the array is valid (after dispose is called, this will be false).
    /// </summary>
    public readonly bool IsValid
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return m_array is not null;
      }
    }

    /// <summary>
    /// Gets the pool the array was borrowed from.
    /// </summary>
    public readonly ArrayPool<T> Pool
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return m_pool ?? ArrayPool<T>.Shared;
      }
    }

    /// <summary>
    /// Gets a span to the array.
    /// </summary>
    public readonly Span<T> Span
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return (m_array is null) ? Span<T>.Empty : new Span<T>(m_array, 0, m_length);
      }
    }

    /// <summary>
    /// Gets a memory to the array.
    /// </summary>
    public readonly Memory<T> Memory
    {
      get
      {
        return (m_array is null) ? Memory<T>.Empty : new Memory<T>(m_array, 0, m_length);
      }
    }

    /// <summary>
    /// Gets the length of the array. Note: This may be less than the actual length of the array due to pooling,
    /// use this value when dealing with the underlying raw array.
    /// </summary>
    public readonly int Length
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return m_length;
      }
    }

    /// <summary>
    /// Gets a reference to the element at the specified index.
    /// </summary>
    public readonly ref T this[int index]
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        if (m_array is null || ((uint) index >= (uint) m_length))
          throw new ArgumentOutOfRangeException(nameof(index));

        return ref m_array[index];
      }
    }

    /// <summary>
    /// Gets or sets if the contents of the array should be disposed, if they implement <see cref="IDisposable"/>, when the array is returned the pool.
    /// By default, this is false.
    /// </summary>
    public bool DisposeContents
    {
      readonly get
      {
        return m_disposeContents;
      }
      set
      {
        m_disposeContents = value;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="PooledArray{T}"/> struct.
    /// </summary>
    /// <param name="count">Length of the array.</param>
    /// <param name="pool">Optional array pool, if null then the default shared pool is used.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public PooledArray(int count, ArrayPool<T>? pool = null)
    {
      m_disposeContents = false;
      m_pool = pool ?? ArrayPool<T>.Shared;
      m_length = Math.Max(0, count);
      m_array = (m_length > 0) ? m_pool.Rent(m_length) : Array.Empty<T>();
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="PooledArray{T}"/> struct.
    /// </summary>
    /// <param name="elem0">First element.</param>
    /// <param name="pool">Optional array pool, if null then the default shared pool is used.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public PooledArray(in T elem0, ArrayPool<T>? pool = null)
    {
      m_disposeContents = false;
      m_pool = pool ?? ArrayPool<T>.Shared;
      m_length = 1;
      m_array = m_pool.Rent(m_length);
      m_array[0] = elem0;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="PooledArray{T}"/> struct.
    /// </summary>
    /// <param name="elem0">First element.</param>
    /// <param name="elem1">Second element.</param>
    /// <param name="pool">Optional array pool, if null then the default shared pool is used.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public PooledArray(in T elem0, in T elem1, ArrayPool<T>? pool = null)
    {
      m_disposeContents = false;
      m_pool = pool ?? ArrayPool<T>.Shared;
      m_length = 2;
      m_array = m_pool.Rent(m_length);
      m_array[0] = elem0;
      m_array[1] = elem1;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="PooledArray{T}"/> struct.
    /// </summary>
    /// <param name="elem0">First element.</param>
    /// <param name="elem1">Second element.</param>
    /// <param name="elem2">Third element.</param>
    /// <param name="pool">Optional array pool, if null then the default shared pool is used.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public PooledArray(in T elem0, in T elem1, in T elem2, ArrayPool<T>? pool = null)
    {
      m_disposeContents = false;
      m_pool = pool ?? ArrayPool<T>.Shared;
      m_length = 3;
      m_array = m_pool.Rent(m_length);
      m_array[0] = elem0;
      m_array[1] = elem1;
      m_array[2] = elem2;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="PooledArray{T}"/> struct.
    /// </summary>
    /// <param name="elem0">First element.</param>
    /// <param name="elem1">Second element.</param>
    /// <param name="elem2">Third element.</param>
    /// <param name="elem3">Fourth element.</param>
    /// <param name="pool">Optional array pool, if null then the default shared pool is used.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public PooledArray(in T elem0, in T elem1, in T elem2, in T elem3, ArrayPool<T>? pool = null) 
    {
      m_disposeContents = false;
      m_pool = pool ?? ArrayPool<T>.Shared;
      m_length = 4;
      m_array = m_pool.Rent(m_length);
      m_array[0] = elem0;
      m_array[1] = elem1;
      m_array[2] = elem2;
      m_array[3] = elem3;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="PooledArray{T}"/> struct.
    /// </summary>
    /// <param name="elem0">First element.</param>
    /// <param name="elem1">Second element.</param>
    /// <param name="elem2">Third element.</param>
    /// <param name="elem3">Fourth element.</param>
    /// <param name="elem4">Fifth element.</param>
    /// <param name="pool">Optional array pool, if null then the default shared pool is used.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public PooledArray(in T elem0, in T elem1, in T elem2, in T elem3, in T elem4, ArrayPool<T>? pool = null)
    {
      m_disposeContents = false;
      m_pool = pool ?? ArrayPool<T>.Shared;
      m_length = 5;
      m_array = m_pool.Rent(m_length);
      m_array[0] = elem0;
      m_array[1] = elem1;
      m_array[2] = elem2;
      m_array[3] = elem3;
      m_array[4] = elem4;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="PooledArray{T}"/> struct.
    /// </summary>
    /// <param name="elem0">First element.</param>
    /// <param name="elem1">Second element.</param>
    /// <param name="elem2">Third element.</param>
    /// <param name="elem3">Fourth element.</param>
    /// <param name="elem4">Fifth element.</param>
    /// <param name="elem5">Sixth element.</param>
    /// <param name="pool">Optional array pool, if null then the default shared pool is used.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public PooledArray(in T elem0, in T elem1, in T elem2, in T elem3, in T elem4, in T elem5, ArrayPool<T>? pool = null)
    {
      m_disposeContents = false;
      m_pool = pool ?? ArrayPool<T>.Shared;
      m_length = 6;
      m_array = m_pool.Rent(m_length);
      m_array[0] = elem0;
      m_array[1] = elem1;
      m_array[2] = elem2;
      m_array[3] = elem3;
      m_array[4] = elem4;
      m_array[5] = elem5;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="PooledArray{T}"/> struct.
    /// </summary>
    /// <param name="elem0">First element.</param>
    /// <param name="elem1">Second element.</param>
    /// <param name="elem2">Third element.</param>
    /// <param name="elem3">Fourth element.</param>
    /// <param name="elem4">Fifth element.</param>
    /// <param name="elem5">Sixth element.</param>
    /// <param name="elem6">Seventh element.</param>
    /// <param name="pool">Optional array pool, if null then the default shared pool is used.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public PooledArray(in T elem0, in T elem1, in T elem2, in T elem3, in T elem4, in T elem5, in T elem6, ArrayPool<T>? pool = null)
    {
      m_disposeContents = false;
      m_pool = pool ?? ArrayPool<T>.Shared;
      m_length = 7;
      m_array = m_pool.Rent(m_length);
      m_array[0] = elem0;
      m_array[1] = elem1;
      m_array[2] = elem2;
      m_array[3] = elem3;
      m_array[4] = elem4;
      m_array[5] = elem5;
      m_array[6] = elem6;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="PooledArray{T}"/> struct.
    /// </summary>
    /// <param name="elem0">First element.</param>
    /// <param name="elem1">Second element.</param>
    /// <param name="elem2">Third element.</param>
    /// <param name="elem3">Fourth element.</param>
    /// <param name="elem4">Fifth element.</param>
    /// <param name="elem5">Sixth element.</param>
    /// <param name="elem6">Seventh element.</param>
    /// <param name="elem7">Eight element.</param>
    /// <param name="pool">Optional array pool, if null then the default shared pool is used.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public PooledArray(in T elem0, in T elem1, in T elem2, in T elem3, in T elem4, in T elem5, in T elem6, in T elem7, ArrayPool<T>? pool = null)
    {
      m_disposeContents = false;
      m_pool = pool ?? ArrayPool<T>.Shared;
      m_length = 8;
      m_array = m_pool.Rent(m_length);
      m_array[0] = elem0;
      m_array[1] = elem1;
      m_array[2] = elem2;
      m_array[3] = elem3;
      m_array[4] = elem4;
      m_array[5] = elem5;
      m_array[6] = elem6;
      m_array[7] = elem7;
    }

    /// <summary>
    /// Performs an explicit conversion from <see cref="PooledArray{T}"/> to <see cref="T[]"/>. If the wrapper is not valid this returns an empty array.
    /// </summary>
    /// <param name="value">Pooled array wrapper.</param>
    /// <returns>The array. This may be larger than the <see cref="Length"/> due to pooling. Use the wrapper's length when using the raw array.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static explicit operator T[](PooledArray<T> value)
    {
      return value.m_array ?? Array.Empty<T>();
    }

    /// <summary>
    /// Performs an implicit conversion from <see cref="PooledArray{T}"/> to <see cref="ReadOnlySpan{T}"/>.
    /// </summary>
    /// <param name="value">Pooled array wrapper.</param>
    /// <returns>The read-only span.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static implicit operator ReadOnlySpan<T>(PooledArray<T> value)
    {
      return (value.m_array is null) ? ReadOnlySpan<T>.Empty : new ReadOnlySpan<T>(value.m_array, 0, value.m_length);
    }

    /// <summary>
    /// Performs an implicit conversion from <see cref="PooledArray{T}"/> to <see cref="Span{T}"/>.
    /// </summary>
    /// <param name="value">Pooled array wrapper.</param>
    /// <returns>The span.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static implicit operator Span<T>(PooledArray<T> value)
    {
      return (value.m_array is null) ? Span<T>.Empty : new Span<T>(value.m_array, 0, value.m_length);
    }

    /// <summary>
    /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
    /// </summary>
    public void Dispose()
    {
      if (m_array is not null && m_pool is not null)
      {
        if (m_disposeContents)
        {
          for (int i = 0; i < m_length; i++)
          {
            T val = m_array[i];
            if (val is IDisposable disp)
              disp.Dispose();
          }
        }

        // Don't bother returning zero arrays
        if (m_array.Length > 0)
          m_pool.Return(m_array, true);

        m_array = null;
        m_length = 0;
      }
    }

    /// <summary>
    /// Resizes the array and copies the contents over.
    /// </summary>
    /// <param name="newSize">Newly requested size. If larger than the current length, default values are set for the new entries.</param>
    public void Resize(int newSize)
    {
      if (newSize == Length)
        return;

      T[] newArray = Pool.Rent(newSize);
      int numToCopy = Math.Min(newSize, m_length);
      if (m_array is not null)
      {
        if (numToCopy > 0)
          Array.Copy(m_array, 0, newArray, 0, numToCopy);

        Pool.Return(m_array);
      }

      // If array grew in size, make sure the new entries are zeroed.
      int numToClear = newSize - m_length;
      if (numToClear > 0)
        Array.Clear(newArray, numToCopy, numToClear);

      m_array = newArray;
      m_length = newSize;
    }

    /// <summary>
    /// Clears the entire array to default values.
    /// </summary>
    public readonly void Clear()
    {
      Clear(0, m_length);
    }

    /// <summary>
    /// Clears the range of values in the array to default values.
    /// </summary>
    /// <param name="index">The starting index of the range of elements to clear.</param>
    /// <param name="count">The number of elements to clear.</param>
    public readonly void Clear(int index, int count)
    {
      if (m_array is null)
        return;

      index = Math.Max(0, index);
      count = MathHelper.Clamp(count, 0, m_length);
      if (count > 0)
        Array.Clear(m_array, index, count);
    }

    /// <summary>
    /// Returns an enumerator that iterates through the contents of the pooled array.
    /// </summary>
    /// <returns>Pooled array enumerator</returns>
    public Enumerator GetEnumerator()
    {
      return new Enumerator(m_array, m_length);
    }

    /// <summary>
    /// Returns an enumerator that iterates through the contents of the pooled array.
    /// </summary>
    /// <returns>Pooled array enumerator</returns>
    IEnumerator<T> IEnumerable<T>.GetEnumerator() { return GetEnumerator(); }

    /// <summary>
    /// Returns an enumerator that iterates through the contents of the pooled array.
    /// </summary>
    /// <returns>Pooled array enumerator</returns>
    IEnumerator IEnumerable.GetEnumerator() { return GetEnumerator(); }

    /// <summary>
    /// Returns a string with the name of the type and the number of elements.
    /// </summary>
    /// <returns>String representation</returns>
    public readonly override string ToString()
    {
      return $"Tesla.PooledArray<{typeof(T).Name}>[{Length}]";
    }

    #region Enumerator

    /// <summary>
    /// Enumerates elements of a <see cref="DataBuffer{T}"/>.
    /// </summary>
    public struct Enumerator : IEnumerator<T>
    {
      private T[] m_arr;
      private int m_length;
      private int m_index;

      /// <summary>
      /// Gets the current value.
      /// </summary>
      public ref T Current { [MethodImpl(MethodImplOptions.AggressiveInlining)] get { return ref m_arr[m_index]; } }

      /// <inheritdoc />
      T IEnumerator<T>.Current { [MethodImpl(MethodImplOptions.AggressiveInlining)] get { return m_arr[m_index]; } }

      /// <inheritdoc />
      object? IEnumerator.Current { get { return m_arr[m_index]; } }

      internal Enumerator(T[]? arr, int length)
      {
        m_arr = arr ?? Array.Empty<T>();
        m_length = length;
        m_index = -1;
      }

      /// <inheritdoc />
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public bool MoveNext()
      {
        if (m_index >= m_length)
          return false;

        m_index++;
        return m_index < m_length;
      }

      /// <inheritdoc />
      public void Reset()
      {
        m_index = -1;
      }

      /// <inheritdoc />
      public void Dispose() { }
    }

    #endregion
  }
}
