﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Reflection;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Contains a collection of helper methods for creating <see cref="DataBuffer{T}"/> instances from various memory sources.
  /// </summary>
  public static class DataBuffer
  {
    /// <summary>
    /// Wraps a raw memory pointer as a <see cref="DataBuffer{T}"/>. This is merely a wrapper, it does not own the lifetime of the memory.
    /// </summary>
    /// <typeparam name="T">Blittable element type contained in the buffer.</typeparam>
    /// <param name="ptr">Raw memory pointer.</param>
    /// <param name="totalSizeInBytes">Total size of the memory in bytes.</param>
    /// <returns>New data buffer instance.</returns>
    public static DataBuffer<T> Wrap<T>(IntPtr ptr, int totalSizeInBytes) where T : unmanaged
    {
      return new DataBuffer<T>(new RawBuffer<T>(ptr, BufferHelper.ComputeElementCount<T>(totalSizeInBytes)));
    }

    /// <summary>
    /// Wraps an array as a <see cref="DataBuffer{T}"/>.
    /// </summary>
    /// <typeparam name="T">Blittable element type contained in the buffer.</typeparam>
    /// <param name="data">Array of elements.</param>
    /// <returns>New data buffer instance.</returns>
    public static DataBuffer<T> Wrap<T>(T[] data) where T : unmanaged
    {
      return new DataBuffer<T>(new ArrayMemoryOwner<T>(data, data.Length));
    }

    /// <summary>
    /// Wraps an array as a <see cref="DataBuffer{T}"/>.
    /// </summary>
    /// <typeparam name="T">Blittable element type contained in the buffer.</typeparam>
    /// <param name="data">Array of elements.</param>
    /// <param name="numElements">Number of elements that should be spannable.</param>
    /// <returns>New data buffer instance.</returns>
    public static DataBuffer<T> Wrap<T>(T[] data, int numElements) where T : unmanaged
    {
      return new DataBuffer<T>(new ArrayMemoryOwner<T>(data, numElements));
    }

    /// <summary>
    /// Creates a concrete instance of <see cref="IDataBuffer"/> with the specified type and size. This does not
    /// validate if the type is unmanaged, it will throw an exception if the type can contain references.
    /// </summary>
    /// <param name="count">Number of elements in the buffer.</param>
    /// <param name="dataType">Blittable element type to be contained in the buffer.</param>
    /// <param name="allocatorStrategy">Optional memory allocator strategy.</param>
    /// <returns>New data buffer instance.</returns>
    /// <exception cref="InvalidOperationException">Thrown if the buffer of the type cannot be constructed, e.g. the data type is not an unmanaged type.</exception>
    public static IDataBuffer Create(int count, Type dataType, MemoryAllocatorStrategy allocatorStrategy = MemoryAllocatorStrategy.Default)
    {
      try
      {
        if (!dataType.IsUnmanaged())
          throw new InvalidOperationException(StringLocalizer.Instance.GetLocalizedString("TypeMustBeUnmanaged", dataType.AssemblyQualifiedName));

        Type dbType = typeof(DataBuffer<>).MakeGenericType(dataType);
        Type memAllocType = typeof(MemoryAllocator<>).MakeGenericType(dataType);
        int totalSizeInBytes = BufferHelper.SizeOf(dataType) * count;
        object? allocator = memAllocType.GetMethod("GetAllocatorOrDefault")?.Invoke(null, new object[] { totalSizeInBytes, allocatorStrategy });
        if (allocator is null)
          throw new InvalidOperationException("Allocator should be exist, else we get ambigious ctors");

        IDataBuffer? db = Activator.CreateInstance(dbType, count, allocator) as IDataBuffer;
        if (db is null)
          throw new InvalidOperationException();

        return db;
      }
      catch(Exception e)
      {
        ILoggingSystem.Current.DefaultLogger.LogException(LogLevel.Debug, e);
        throw e;
      }
    }

    /// <summary>
    /// Creates a new <see cref="DataBuffer{T}"/> instance with an optional memory allocator. If no allocator is given, the default strategy
    /// is to create either a managed or native memory block. If the requested buffer size is <= 85,000 bytes then managed memory is allocated,
    /// otherwise native memory is allocated. This is to avoid allocating the buffer to the large object heap (LOH).
    /// </summary>
    /// <typeparam name="T">Blittable element type contained in the buffer.</typeparam>
    /// <param name="count">Number of elements in the buffer.</param>
    /// <param name="allocator">Optional memory allocator. If null, default memory allocation strategy is used.</param>
    /// <returns>New data buffer instance.</returns>
    public static DataBuffer<T> Create<T>(int count, IMemoryAllocator<T>? allocator = null) where T : unmanaged
    {
      if (allocator is null)
        allocator = MemoryAllocator<T>.GetAllocatorOrDefault(count * BufferHelper.SizeOf<T>());

      return new DataBuffer<T>(count, allocator);
    }

    /// <summary>
    /// Creates a new <see cref="DataBuffer{T}"/> instance with the specified memory allocator strategy.
    /// </summary>
    /// <typeparam name="T">Blittable element type contained in the buffer.</typeparam>
    /// <param name="count">Number of elements in the buffer.</param>
    /// <param name="allocatorStrategy">Memory allocator strategy.</param>
    /// <returns>New data buffer instance.</returns>
    public static DataBuffer<T> Create<T>(int count, MemoryAllocatorStrategy allocatorStrategy) where T : unmanaged
    {
      IMemoryAllocator<T> allocator = MemoryAllocator<T>.GetAllocatorOrDefault(count * BufferHelper.SizeOf<T>(), allocatorStrategy);

      return new DataBuffer<T>(count, allocator);
    }

    /// <summary>
    /// Creates a new <see cref="DataBuffer{T}"/> instance containing a copy of the specified memory. If no allocator is given, the default strategy
    /// is to create either a managed or native memory block. If the requested buffer size is <= 85,000 bytes then managed memory is allocated,
    /// otherwise native memory is allocated. This is to avoid allocating the buffer to the large object heap (LOH).
    /// </summary>
    /// <typeparam name="T">Blittable element type contained in the buffer.</typeparam>
    /// <param name="data">Data to copy.</param>
    /// <param name="allocator">Optional memory allocator. If null, default memory allocation strategy is used.</param>
    /// <returns>New data buffer instance.</returns>
    public static DataBuffer<T> Create<T>(ReadOnlySpan<T> data, IMemoryAllocator<T>? allocator = null) where T : unmanaged
    {
      if (allocator is null)
        allocator = MemoryAllocator<T>.GetAllocatorOrDefault(data.Length * BufferHelper.SizeOf<T>());

      return new DataBuffer<T>(data, allocator);
    }

    /// <summary>
    /// Creates a new <see cref="DataBuffer{T}"/> instance containing a copy of the specified memory.
    /// </summary>
    /// <typeparam name="T">Blittable element type contained in the buffer.</typeparam>
    /// <param name="data">Data to copy.</param>
    /// <param name="allocatorStrategy">Memory allocator strategy.</param>
    /// <returns>New data buffer instance.</returns>
    public static DataBuffer<T> Create<T>(ReadOnlySpan<T> data, MemoryAllocatorStrategy allocatorStrategy) where T : unmanaged
    {
      IMemoryAllocator<T> allocator = MemoryAllocator<T>.GetAllocatorOrDefault(data.Length * BufferHelper.SizeOf<T>(), allocatorStrategy);

      return new DataBuffer<T>(data, allocator);
    }

    /// <summary>
    /// Creates a new <see cref="DataBuffer{T}"/> instance backed by managed memory from a pool.
    /// </summary>
    /// <typeparam name="T">Blittable element type contained in the buffer.</typeparam>
    /// <param name="count">Number of elements in the buffer.</param>
    /// <returns>New data buffer instance.</returns>
    public static DataBuffer<T> CreatePooled<T>(int count) where T : unmanaged
    {
      return Create<T>(count, MemoryAllocator<T>.PooledManaged);
    }

    /// <summary>
    /// Creates a new <see cref="DataBuffer{T}"/> instance backed by managed memory.
    /// </summary>
    /// <typeparam name="T">Blittable element type contained in the buffer.</typeparam>
    /// <param name="count">Number of elements in the buffer.</param>
    /// <returns>New data buffer instance.</returns>
    public static DataBuffer<T> CreateManaged<T>(int count) where T : unmanaged
    {
      return Create<T>(count, MemoryAllocator<T>.Default);
    }

    /// <summary>
    /// Creates a new <see cref="DataBuffer{T}"/> instance backed by native memory.
    /// </summary>
    /// <typeparam name="T">Blittable element type contained in the buffer.</typeparam>
    /// <param name="count">Number of elements in the buffer.</param>
    /// <returns>New data buffer instance.</returns>
    public static DataBuffer<T> CreateNative<T>(int count) where T : unmanaged
    {
      return Create<T>(count, MemoryAllocator<T>.Native);
    }

    /// <summary>
    /// Creates a new <see cref="DataBuffer{T}"/> instance backed by managed memory from the pinned object heap (POH).
    /// </summary>
    /// <typeparam name="T">Blittable element type contained in the buffer.</typeparam>
    /// <param name="count">Number of elements in the buffer.</param>
    /// <returns>New data buffer instance.</returns>
    public static DataBuffer<T> CreatePinned<T>(int count) where T : unmanaged
    {
      return Create<T>(count, MemoryAllocator<T>.PinnedManaged);
    }
  }

  /// <summary>
  /// Extension methods for databuffers.
  /// </summary>
  public static class DataBufferExtensions
  {
    /// <summary>
    /// Copies a slice from a <see cref="IReadOnlyDataBuffer{T}"/> beginning at the specified index.
    /// </summary>
    /// <typeparam name="T">Blittable element type contained in the buffer.</typeparam>
    /// <param name="db">Read-only data buffer.</param>
    /// <param name="index">The index at which to begin the slice.</param>
    /// <param name="allocator">Optional memory allocator for the copied buffer. If null, default memory allocation strategy is used.</param>
    /// <returns>New buffer containing the contents of the slice of the original buffer</returns>
    public static DataBuffer<T> CopySlice<T>(this IReadOnlyDataBuffer<T> db, int index, IMemoryAllocator<T>? allocator = null) where T : unmanaged
    {
      CheckIsDisposed(db);

      return DataBuffer.Create<T>(db.Span.Slice(index), allocator);
    }

    /// <summary>
    /// Copies a slice from a <see cref="IReadOnlyDataBuffer{T}"/> beginning at the specified index for a specified length.
    /// </summary>
    /// <typeparam name="T">Blittable element type contained in the buffer.</typeparam>
    /// <param name="db">Read-only data buffer.</param>
    /// <param name="index">The index at which to begin the slice.</param>
    /// <param name="count">Number of elements in the slice.</param>
    /// <param name="allocator">Optional memory allocator for the copied buffer. If null, default memory allocation strategy is used.</param>
    /// <returns>New buffer containing the contents of the slice of the original buffer</returns>
    public static DataBuffer<T> CopySlice<T>(this IReadOnlyDataBuffer<T> db, int index, int count, IMemoryAllocator<T>? allocator = null) where T : unmanaged
    {
      CheckIsDisposed(db);

      return DataBuffer.Create<T>(db.Span.Slice(index, count), allocator);
    }

    /// <summary>
    /// Reinterprets the buffer's data as the specified data type.
    /// </summary>
    /// <typeparam name="T">Blittable element type to convert to.</typeparam>
    /// <param name="db">Read-only data buffer.</param>
    /// <param name="positionInBytes">Optional offset to add to the start of the buffer that the returned span shall start at.</param>
    /// <returns>Read-only span of elements of the specified type.</returns>
    public static ReadOnlySpan<T> Reinterpret<T>(this IReadOnlyDataBuffer db, int positionInBytes = 0) where T : unmanaged
    {
      CheckIsDisposed(db);

      return db.Bytes.Slice(positionInBytes).Reinterpret<byte, T>();
    }

    /// <summary>
    /// Reinterprets the buffer's data as the specified data type.
    /// </summary>
    /// <typeparam name="T">Blittable element type to convert to.</typeparam>
    /// <param name="db">Data buffer.</param>
    /// <param name="positionInBytes">Optional offset to add to the start of the buffer that the returned span shall start at.</param>
    /// <returns>Span of elements of the specified type.</returns>
    public static Span<T> Reinterpret<T>(this IDataBuffer db, int positionInBytes = 0) where T : unmanaged
    {
      CheckIsDisposed(db);

      return db.Bytes.Slice(positionInBytes).Reinterpret<byte, T>();
    }

    private static void CheckIsDisposed(IReadOnlyDataBuffer db)
    {
      if (db.IsDisposed)
        throw new ObjectDisposedException(StringLocalizer.Instance.GetLocalizedString("ObjectDisposed"));
    }
  }
}
