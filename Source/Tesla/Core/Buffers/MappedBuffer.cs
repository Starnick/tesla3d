﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Buffers;

namespace Tesla
{
  /// <summary>
  /// Represents data that is laid out in a particular way (e.g. a 2D or 3D texture).
  /// </summary>
  public struct MappedBuffer
  {
    /// <summary>
    /// Pointer to the data, starting at row 0, slice 0.
    /// </summary>
    public MemoryHandle Data;

    /// <summary>
    /// Row pitch of the data, the number of bytes to move between rows (e.g. a 2D texture).
    /// </summary>
    public int RowPitch;

    /// <summary>
    /// Slice pitch of the data, the number of bytes to the between depth slices (e.g. a 3D texture).
    /// </summary>
    public int SlicePitch;

    /// <summary>
    /// Gets if the data is null.
    /// </summary>
    public bool IsEmpty
    {
      get
      {
        if (Data.AsIntPointer() == IntPtr.Zero && RowPitch == 0)
          return SlicePitch == 0;

        return false;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="MappedBuffer"/> struct.
    /// </summary>
    /// <param name="data">Buffer data, starting at row 0, slice 0.</param>
    /// <param name="rowPitch">Row pitch of the data, the number of bytes to move to the next row.</param>
    /// <param name="slicePitch">Slice pitch of the data, the number of bytes to the next depth slice.</param>
    public MappedBuffer(MemoryHandle data, int rowPitch = 0, int slicePitch = 0)
    {
      Data = data;
      RowPitch = rowPitch;
      SlicePitch = slicePitch;
    }
  }
}
