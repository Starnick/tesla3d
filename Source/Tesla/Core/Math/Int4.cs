﻿/*
* Copyright (c) 2010-2024 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Tesla.Content;
using Tesla.Design;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Defines a four dimensional vector where each component is a 32-bit integer.
  /// </summary>
  [Serializable]
  [StructLayout(LayoutKind.Sequential, Pack = 4)]
  [TypeConverter(typeof(Int4TypeConverter))]
  public struct Int4 : IEquatable<Int4>, IRefEquatable<Int4>, IFormattable, IPrimitiveValue
  {
    /// <summary>
    /// X component of the vector.
    /// </summary>
    public int X;

    /// <summary>
    /// Y component of the vector.
    /// </summary>
    public int Y;

    /// <summary>
    /// Z component of the vector.
    /// </summary>
    public int Z;

    /// <summary>
    /// W component of the vector.
    /// </summary>
    public int W;

    private static readonly Int4 s_zero = new Int4(0, 0, 0, 0);
    private static readonly Int4 s_one = new Int4(1, 1, 1, 1);
    private static readonly Int4 s_unitX = new Int4(1, 0, 0, 0);
    private static readonly Int4 s_unitY = new Int4(0, 1, 0, 0);
    private static readonly Int4 s_unitZ = new Int4(0, 0, 1, 0);
    private static readonly Int4 s_unitW = new Int4(0, 0, 0, 1);

    private static readonly int s_sizeInBytes = BufferHelper.SizeOf<Int4>();

    /// <summary>
    /// Gets an <see cref="Int4"/> set to (0, 0, 0, 0).
    /// </summary>
    public static ref readonly Int4 Zero
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_zero;
      }
    }

    /// <summary>
    /// Gets an <see cref="Int4"/> set to (1, 1, 1, 1).
    /// </summary>
    public static ref readonly Int4 One
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_one;
      }
    }

    /// <summary>
    /// Gets a unit <see cref="Int4"/> set to (1, 0, 0, 0).
    /// </summary>
    public static ref readonly Int4 UnitX
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_unitX;
      }
    }

    /// <summary>
    /// Gets a unit <see cref="Int4"/> set to (0, 1, 0, 0).
    /// </summary>
    public static ref readonly Int4 UnitY
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_unitY;
      }
    }

    /// <summary>
    /// Gets a unit <see cref="Int4"/> set to (0, 0, 1, 0).
    /// </summary>
    public static ref readonly Int4 UnitZ
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_unitZ;
      }
    }

    /// <summary>
    /// Gets a unit <see cref="Int4"/> set to (0, 0, 0, 1).
    /// </summary>
    public static ref readonly Int4 UnitW
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_unitW;
      }
    }

    /// <summary>
    /// Gets the size of the <see cref="Int4"/> type in bytes.
    /// </summary>
    public static int SizeInBytes
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return s_sizeInBytes;
      }
    }

    /// <summary>
    /// Gets or sets individual components of the vector in the order that the components are declared (XYZW).
    /// </summary>
    /// <param name="index">Zero-based index, in range of [0, 3].</param>
    /// <returns>The value of the specified component.</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the index is out of range [0, 3]</exception>
    public int this[int index]
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      readonly get
      {
        switch (index)
        {
          case 0:
            return X;
          case 1:
            return Y;
          case 2:
            return Z;
          case 3:
            return W;
          default:
            throw new ArgumentOutOfRangeException(nameof(index), StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));
        }
      }
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      set
      {
        switch (index)
        {
          case 0:
            X = value;
            break;
          case 1:
            Y = value;
            break;
          case 2:
            Z = value;
            break;
          case 3:
            W = value;
            break;
          default:
            throw new ArgumentOutOfRangeException(nameof(index), StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));
        }
      }
    }

    /// <summary>
    /// Determines whether this vector is (0, 0, 0, 0).
    /// </summary>
    public readonly bool IsZero
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        ref readonly Int4 zero = ref Int4.Zero;
        return Equals(zero);
      }
    }

    /// <summary>
    /// Determines whether this vector is (1, 1, 1, 1).
    /// </summary>
    public readonly bool IsOne
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        ref readonly Int4 one = ref Int4.One;
        return Equals(one);
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Int4"/> struct.
    /// </summary>
    /// <param name="value">Value to initialize each component to.</param>
    public Int4(int value)
    {
      X = Y = Z = W = value;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Int4"/> struct.
    /// </summary>
    /// <param name="xy">Vector that contains XY components.</param>
    /// <param name="z">Optional Z component, by default zero.</param>
    /// <param name="w">Optional W component, by default zero.</param>
    public Int4(in Int2 xy, int z = 0, int w = 0)
    {
      X = xy.X;
      Y = xy.Y;
      Z = z;
      W = w;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Int4"/> struct.
    /// </summary>
    /// <param name="xyz">Vector that contains XYZ components.</param>
    /// <param name="w">Optional W component, by default zero.</param>
    public Int4(in Int3 xyz, int w = 0)
    {
      X = xyz.X;
      Y = xyz.Y;
      Z = xyz.Z;
      W = w;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Int4"/> struct.
    /// </summary>
    /// <param name="x">X component.</param>
    /// <param name="y">Y component.</param>
    /// <param name="z">Z component.</param>
    /// <param name="w">W component.</param>
    public Int4(int x, int y, int z, int w)
    {
      X = x;
      Y = y;
      Z = z;
      W = w;
    }

    /// <summary>
    /// Adds two vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Sum of the two vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int4 Add(in Int4 a, in Int4 b)
    {
      Int4 result;
      result.X = a.X + b.X;
      result.Y = a.Y + b.Y;
      result.Z = a.Z + b.Z;
      result.W = a.W + b.W;

      return result;
    }

    /// <summary>
    /// Adds two vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <param name="result">Sum of the two vectors</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Add(in Int4 a, in Int4 b, out Int4 result)
    {
      result.X = a.X + b.X;
      result.Y = a.Y + b.Y;
      result.Z = a.Z + b.Z;
      result.W = a.W + b.W;
    }

    /// <summary>
    /// Subtracts vector b from vector a.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Difference of the two vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int4 Subtract(in Int4 a, in Int4 b)
    {
      Int4 result;
      result.X = a.X - b.X;
      result.Y = a.Y - b.Y;
      result.Z = a.Z - b.Z;
      result.W = a.W - b.W;

      return result;
    }

    /// <summary>
    /// Subtracts vector b from vector a.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <param name="result">Difference of the two vectors</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Subtract(in Int4 a, in Int4 b, out Int4 result)
    {
      result.X = a.X - b.X;
      result.Y = a.Y - b.Y;
      result.Z = a.Z - b.Z;
      result.W = a.W - b.W;
    }

    /// <summary>
    /// Divides the components of vector a by those of vector b.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Divisor vector</param>
    /// <returns>Quotient of the two vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int4 Divide(in Int4 a, in Int4 b)
    {
      Int4 result;
      result.X = a.X / b.X;
      result.Y = a.Y / b.Y;
      result.Z = a.Z / b.Z;
      result.W = a.W / b.W;

      return result;
    }

    /// <summary>
    /// Divides the components of vector a by those of vector b.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Divisor vector</param>
    /// <param name="result">Quotient of the two vectors</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Divide(in Int4 a, in Int4 b, out Int4 result)
    {
      result.X = a.X / b.X;
      result.Y = a.Y / b.Y;
      result.Z = a.Z / b.Z;
      result.W = a.W / b.W;
    }

    /// <summary>
    /// Divides the components of one vector by a scalar.
    /// </summary>
    /// <param name="value">First vector</param>
    /// <param name="divisor">Divisor scalar</param>
    /// <returns>Divided vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int4 Divide(in Int4 value, int divisor)
    {
      Int4 result;
      result.X = value.X / divisor;
      result.Y = value.Y / divisor;
      result.Z = value.Z / divisor;
      result.W = value.W / divisor;

      return result;
    }

    /// <summary>
    /// Divides the components of one vector by a scalar.
    /// </summary>
    /// <param name="value">First vector</param>
    /// <param name="divisor">Divisor scalar</param>
    /// <param name="result">Divided Vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Divide(in Int4 value, int divisor, out Int4 result)
    {
      result.X = value.X / divisor;
      result.Y = value.Y / divisor;
      result.Z = value.Z / divisor;
      result.W = value.W / divisor;
    }

    /// <summary>
    /// Multiplies components of a vector by a scalar value.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="scale">Amount to scale</param>
    /// <returns>Scaled vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int4 Multiply(in Int4 value, int scale)
    {
      Int4 result;
      result.X = value.X * scale;
      result.Y = value.Y * scale;
      result.Z = value.Z * scale;
      result.W = value.W * scale;

      return result;
    }

    /// <summary>
    /// Multiplies components of a vector by a scalar value.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="scale">Amount to scale</param>
    /// <param name="result">Scaled vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Multiply(in Int4 value, int scale, out Int4 result)
    {
      result.X = value.X * scale;
      result.Y = value.Y * scale;
      result.Z = value.Z * scale;
      result.W = value.W * scale;
    }

    /// <summary>
    /// Multiplies components of two vectors together.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Multiplied vectorr</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int4 Multiply(in Int4 a, in Int4 b)
    {
      Int4 result;
      result.X = a.X * b.X;
      result.Y = a.Y * b.Y;
      result.Z = a.Z * b.Z;
      result.W = a.W * b.W;

      return result;
    }

    /// <summary>
    /// Multiplies components of two vectors together.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <param name="result">Multiplied vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Multiply(in Int4 a, in Int4 b, out Int4 result)
    {
      result.X = a.X * b.X;
      result.Y = a.Y * b.Y;
      result.Z = a.Z * b.Z;
      result.W = a.W * b.W;
    }

    /// <summary>
    /// Restricts the source vector in the range of the minimum and maximum vectors.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="min">Minimum vector</param>
    /// <param name="max">Maximum vector</param>
    /// <returns>Clamped vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int4 Clamp(in Int4 value, in Int4 min, in Int4 max)
    {
      int x = value.X;
      int y = value.Y;
      int z = value.Z;
      int w = value.W;

      x = (x > max.X) ? max.X : x;
      x = (x < min.X) ? min.X : x;

      y = (y > max.Y) ? max.Y : y;
      y = (y < min.Y) ? min.Y : y;

      z = (z > max.Z) ? max.Z : z;
      z = (z < min.Z) ? min.Z : z;

      w = (w > max.W) ? max.W : w;
      w = (w < min.W) ? min.W : w;

      Int4 result;
      result.X = x;
      result.Y = y;
      result.Z = z;
      result.W = w;

      return result;
    }

    /// <summary>
    /// Restricts the source vector in the range of the minimum and maximum vectors.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="min">Minimum vector</param>
    /// <param name="max">Maximum vector</param>
    /// <param name="result">Clamped vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Clamp(in Int4 value, in Int4 min, in Int4 max, out Int4 result)
    {
      int x = value.X;
      int y = value.Y;
      int z = value.Z;
      int w = value.W;

      x = (x > max.X) ? max.X : x;
      x = (x < min.X) ? min.X : x;

      y = (y > max.Y) ? max.Y : y;
      y = (y < min.Y) ? min.Y : y;

      z = (z > max.Z) ? max.Z : z;
      z = (z < min.Z) ? min.Z : z;

      w = (w > max.W) ? max.W : w;
      w = (w < min.W) ? min.W : w;

      result.X = x;
      result.Y = y;
      result.Z = z;
      result.W = w;
    }

    /// <summary>
    /// Gets the vector that contains the mininum value from each of the components of the 
    /// two supplied vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Minimum vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int4 Min(in Int4 a, in Int4 b)
    {
      Int4 result;
      result.X = (a.X < b.X) ? a.X : b.X;
      result.Y = (a.Y < b.Y) ? a.Y : b.Y;
      result.Z = (a.Z < b.Z) ? a.Z : b.Z;
      result.W = (a.W < b.W) ? a.W : b.W;

      return result;
    }

    /// <summary>
    /// Gets the vector that contains the mininum value from each of the components of the 
    /// two supplied vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <param name="result">Minimum vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Min(in Int4 a, in Int4 b, out Int4 result)
    {
      result.X = (a.X < b.X) ? a.X : b.X;
      result.Y = (a.Y < b.Y) ? a.Y : b.Y;
      result.Z = (a.Z < b.Z) ? a.Z : b.Z;
      result.W = (a.W < b.W) ? a.W : b.W;
    }

    /// <summary>
    /// Gets the vector that contains the maximum value from each of the components of the 
    /// two supplied vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Maximum vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int4 Max(in Int4 a, in Int4 b)
    {
      Int4 result;
      result.X = (a.X > b.X) ? a.X : b.X;
      result.Y = (a.Y > b.Y) ? a.Y : b.Y;
      result.Z = (a.Z > b.Z) ? a.Z : b.Z;
      result.W = (a.W > b.W) ? a.W : b.W;

      return result;
    }

    /// <summary>
    /// Gets the vector that contains the maximum value from each of the components of the 
    /// two supplied vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <param name="result">Maximum vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Max(in Int4 a, in Int4 b, out Int4 result)
    {
      result.X = (a.X > b.X) ? a.X : b.X;
      result.Y = (a.Y > b.Y) ? a.Y : b.Y;
      result.Z = (a.Z > b.Z) ? a.Z : b.Z;
      result.W = (a.W > b.W) ? a.W : b.W;
    }

    /// <summary>
    /// Flips the signs of the components of the specified vector.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <returns>Negated vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int4 Negate(in Int4 value)
    {
      Int4 result;
      result.X = -value.X;
      result.Y = -value.Y;
      result.Z = -value.Z;
      result.W = -value.W;

      return result;
    }

    /// <summary>
    /// Flips the signs of the components of the specified vector.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="result">Negated vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Negate(in Int4 value, out Int4 result)
    {
      result.X = -value.X;
      result.Y = -value.Y;
      result.Z = -value.Z;
      result.W = -value.W;
    }

    /// <summary>
    /// Adds the two vectors together.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Sum of the two vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int4 operator +(Int4 a, Int4 b)
    {
      Int4 result;
      result.X = a.X + b.X;
      result.Y = a.Y + b.Y;
      result.Z = a.Z + b.Z;
      result.W = a.W + b.W;

      return result;
    }

    /// <summary>
    /// Subtracts vector b from vector a.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Difference of the two vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int4 operator -(Int4 a, Int4 b)
    {
      Int4 result;
      result.X = a.X - b.X;
      result.Y = a.Y - b.Y;
      result.Z = a.Z - b.Z;
      result.W = a.W - b.W;

      return result;
    }

    /// <summary>
    /// Flips the signs of the components of the specified vector.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <returns>Negated vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int4 operator -(Int4 value)
    {
      Int4 result;
      result.X = -value.X;
      result.Y = -value.Y;
      result.Z = -value.Z;
      result.W = -value.W;

      return result;
    }

    /// <summary>
    /// Multiplies two vectors together.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Multiplied vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int4 operator *(Int4 a, Int4 b)
    {
      Int4 result;
      result.X = a.X * b.X;
      result.Y = a.Y * b.Y;
      result.Z = a.Z * b.Z;
      result.W = a.W * b.W;

      return result;
    }

    /// <summary>
    /// Multiplies a vector by a scaling factor.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="scale">Amount to scale</param>
    /// <returns>Multiplied vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int4 operator *(Int4 value, int scale)
    {
      Int4 result;
      result.X = value.X * scale;
      result.Y = value.Y * scale;
      result.Z = value.Z * scale;
      result.W = value.W * scale;

      return result;
    }

    /// <summary>
    /// Multiplies a vector by a scaling factor.
    /// </summary>
    /// <param name="scale">Amount to scale</param>
    /// <param name="value">Source vector</param>
    /// <returns>Multiplied vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int4 operator *(int scale, Int4 value)
    {
      Int4 result;
      result.X = value.X * scale;
      result.Y = value.Y * scale;
      result.Z = value.Z * scale;
      result.W = value.W * scale;

      return result;
    }

    /// <summary>
    /// Divides the components of vector a by those of vector b.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Divisor vector</param>
    /// <returns>Quotient of the two vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int4 operator /(Int4 a, Int4 b)
    {
      Int4 result;
      result.X = a.X / b.X;
      result.Y = a.Y / b.Y;
      result.Z = a.Z / b.Z;
      result.W = a.W / b.W;

      return result;
    }

    /// <summary>
    /// Divides a vector by a scalar value.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="divisor">Divisor scalar</param>
    /// <returns>Divided vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int4 operator /(Int4 value, int divisor)
    {
      Int4 result;
      result.X = value.X / divisor;
      result.Y = value.Y / divisor;
      result.Z = value.Z / divisor;
      result.W = value.W / divisor;

      return result;
    }

    /// <summary>
    /// Performs an explicit conversion from <see cref="Int4"/> to <see cref="Vector4"/>.
    /// </summary>
    /// <param name="value">Int4 value</param>
    /// <returns>Converted Vector4</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static explicit operator Vector4(Int4 value)
    {
      Vector4 result;
      result.X = (float) value.X;
      result.Y = (float) value.Y;
      result.Z = (float) value.Z;
      result.W = (float) value.W;

      return result;
    }

    /// <summary>
    /// Performs an explicit conversion from <see cref="Vector4"/> to <see cref="Int4"/>.
    /// </summary>
    /// <param name="value">Vector4 value</param>
    /// <returns>Converted Int4</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static explicit operator Int4(Vector4 value)
    {
      Int4 result;
      result.X = (int) value.X;
      result.Y = (int) value.Y;
      result.Z = (int) value.Z;
      result.W = (int) value.W;

      return result;
    }

    /// <summary>
    /// Implicitly converts from <see cref="Int4"/> to <see cref="System.Numerics.Vector4"/> for interop.
    /// </summary>
    /// <param name="v">Vector to convert.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static implicit operator System.Numerics.Vector4(Int4 v)
    {
      return new System.Numerics.Vector4(v.X, v.Y, v.Z, v.W);
    }

    /// <summary>
    /// Implicitly converts from <see cref="System.Numerics.Vector4"/> to <see cref="Int4"/> for interop.
    /// </summary>
    /// <param name="v">Vector to convert.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static implicit operator Int4(System.Numerics.Vector4 v)
    {
      return new Int4((int) v.X, (int) v.Y, (int) v.Z, (int) v.W);
    }

    /// <summary>
    /// Checks equality between two vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator ==(Int4 a, Int4 b)
    {
      return (a.X == b.X) && (a.Y == b.Y) && (a.Z == b.Z) && (a.W == b.W);
    }

    /// <summary>
    /// Checks inequality between two vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>True if both are not equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator !=(Int4 a, Int4 b)
    {
      return (a.X != b.X) || (a.Y != b.Y) || (a.Z != b.Z) || (a.W != b.W);
    }

    /// <summary>
    /// Flips the signs of the components of the vector.
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void Negate()
    {
      X = -X;
      Y = -Y;
      Z = -Z;
      W = -W;
    }

    /// <summary>
    /// Checks equality between the vector and another vector.
    /// </summary>
    /// <param name="other">Other vector</param>
    /// <returns>True if the vectors are equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    readonly bool IEquatable<Int4>.Equals(Int4 other)
    {
      return (X == other.X) && (Y == other.Y) && (Z == other.Z) && (W == other.W);
    }

    /// <summary>
    /// Checks equality between the vector and another vector.
    /// </summary>
    /// <param name="other">Other vector</param>
    /// <returns>True if the vectors are equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Equals(in Int4 other)
    {
      return (X == other.X) && (Y == other.Y) && (Z == other.Z) && (W == other.W);
    }

    /// <summary>
    /// Tests equality between the vector and XYZW values.
    /// </summary>
    /// <param name="x">X component.</param>
    /// <param name="y">Y component.</param>
    /// <param name="z">Z component.</param>
    /// <param name="w">W component.</param>
    /// <returns>True if components are equal within tolerance, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Equals(float x, float y, float z, float w)
    {
      return (X == x) && (Y == y) && (Z == z) && (W == w);
    }

    /// <summary>
    /// Determines whether the specified <see cref="System.Object" /> is equal to this instance.
    /// </summary>
    /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
    /// <returns>True if the specified <see cref="System.Object" /> is equal to this instance; otherwise, False.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is Int4)
        return Equals((Int4) obj);

      return false;
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
    public readonly override int GetHashCode()
    {
      unchecked
      {
        return X.GetHashCode() + Y.GetHashCode() + Z.GetHashCode() + W.GetHashCode();
      }
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly override string ToString()
    {
      CultureInfo info = CultureInfo.CurrentCulture;
      return String.Format(info, "X: {0} Y: {1} Z: {2} W: {3}",
          new Object[] { X.ToString(info), Y.ToString(info), Z.ToString(info), W.ToString(info) });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="format">The format</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(string? format)
    {
      if (format is null)
        return ToString();

      CultureInfo info = CultureInfo.CurrentCulture;
      return String.Format(info, "X: {0} Y: {1} Z: {2} W: {3}",
          new Object[] { X.ToString(format, info), Y.ToString(format, info), Z.ToString(format, info), W.ToString(format, info) });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="formatProvider">The format provider.</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(IFormatProvider? formatProvider)
    {
      if (formatProvider is null)
        return ToString();

      return String.Format(formatProvider, "X: {0} Y: {1} Z: {2} W: {3}",
          new Object[] { X.ToString(formatProvider), Y.ToString(formatProvider), Z.ToString(formatProvider), W.ToString(formatProvider) });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="format">The format.</param>
    /// <param name="formatProvider">The format provider.</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(string? format, IFormatProvider? formatProvider)
    {
      if (formatProvider is null)
        return ToString();

      if (format is null)
        return ToString(formatProvider);

      return String.Format(formatProvider, "X: {0} Y: {1} Z: {2} W: {3}",
          new Object[] { X.ToString(format, formatProvider), Y.ToString(format, formatProvider), Z.ToString(format, formatProvider), W.ToString(format, formatProvider) });
    }

    /// <summary>
    /// Writes the primitive data to the output.
    /// </summary>
    /// <param name="output">Primitive writer.</param>
    readonly void IPrimitiveValue.Write(IPrimitiveWriter output)
    {
      output.Write("X", X);
      output.Write("Y", Y);
      output.Write("Z", Z);
      output.Write("W", W);
    }

    /// <summary>
    /// Reads the primitive data from the input.
    /// </summary>
    /// <param name="input">Primitive reader.</param>
    void IPrimitiveValue.Read(IPrimitiveReader input)
    {
      X = input.ReadInt32("X");
      Y = input.ReadInt32("Y");
      Z = input.ReadInt32("Z");
      W = input.ReadInt32("W");
    }
  }
}
