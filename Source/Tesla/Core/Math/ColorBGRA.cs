﻿/*
* Copyright (c) 2010-2024 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Tesla.Content;
using Tesla.Design;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Defines a packed 32-bit color using blue, green, red, and alpha components (in BGRA order).
  /// </summary>
  [Serializable]
  [StructLayout(LayoutKind.Sequential, Size = 4)]
  [TypeConverter(typeof(ColorBGRATypeConverter))]
  public struct ColorBGRA : IEquatable<ColorBGRA>, IRefEquatable<ColorBGRA>, IFormattable, IPrimitiveValue
  {
    /// <summary>
    /// Blue component.
    /// </summary>
    public byte B;

    /// <summary>
    /// Green component.
    /// </summary>
    public byte G;

    /// <summary>
    /// Red component.
    /// </summary>
    public byte R;

    /// <summary>
    /// Alpha component.
    /// </summary>
    public byte A;

    private static readonly int s_sizeInBytes = BufferHelper.SizeOf<ColorBGRA>();

    /// <summary>
    /// Gets the size of <see cref="ColorBGRA"/> type in bytes.
    /// </summary>
    public static int SizeInBytes
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return s_sizeInBytes;
      }
    }

    /// <summary>
    /// Gets or sets individual components of the color in the order that the components are declared (BGRA).
    /// </summary>
    /// <param name="index">Zero-based index, in range of [0, 3].</param>
    /// <returns>The value of the specified component.</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the index is out of range [0, 3]</exception>
    public byte this[int index]
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      readonly get
      {
        switch (index)
        {
          case 0:
            return B;
          case 1:
            return G;
          case 2:
            return R;
          case 3:
            return A;
          default:
            throw new ArgumentOutOfRangeException(nameof(index), StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));
        }
      }
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      set
      {
        switch (index)
        {
          case 0:
            B = value;
            break;
          case 1:
            G = value;
            break;
          case 2:
            R = value;
            break;
          case 3:
            A = value;
            break;
          default:
            throw new ArgumentOutOfRangeException(nameof(index), StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));
        }
      }
    }

    /// <summary>
    /// Gets the hue of the color. First component of the Hue-Saturation-Brightness (HSB) representation of the color.
    /// </summary>
    public readonly float Hue
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        if (R == G && G == B)
          return 0.0f;

        float r = ((float) R) / 255.0f;
        float g = ((float) G) / 255.0f;
        float b = ((float) B) / 255.0f;

        float max = r;
        float min = r;
        float delta;
        float hue = 0.0f;

        if (g > max)
          max = g;

        if (b > max)
          max = b;

        if (g < min)
          min = g;

        if (b < min)
          min = b;

        delta = max - min;

        if (r == max)
        {
          hue = (g - b) / delta;
        }
        else if (g == max)
        {
          hue = 2.0f + (b - r) / delta;
        }
        else if (b == max)
        {
          hue = 4.0f + (r - g) / delta;
        }

        hue *= 60.0f;

        if (hue < 0.0f)
          hue += 360.0f;

        return hue;
      }
    }

    /// <summary>
    /// Gets the saturation of the color. Second component of the Hue-Saturation-Brightness (HSB) representation of the color.
    /// </summary>
    public readonly float Saturation
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        float r = ((float) R) / 255.0f;
        float g = ((float) G) / 255.0f;
        float b = ((float) B) / 255.0f;

        float max = r;
        float min = r;
        float l;
        float saturation = 0.0f;

        if (g > max)
          max = g;

        if (b > max)
          max = b;

        if (g < min)
          min = g;

        if (b < min)
          min = b;

        //if max == min, then there is no color and the saturation is zero
        if (max != min)
        {
          l = (max + min) / 2.0f;

          if (l <= 0.5f)
          {
            saturation = (max - min) / (max + min);
          }
          else
          {
            saturation = (max - min) / (2.0f - max - min);
          }
        }

        return saturation;
      }
    }

    /// <summary>
    /// Gets the brightness of the color. Third component of the Hue-Saturation-Brightness (HSB) representation of the color.
    /// </summary>
    public readonly float Brightness
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        float r = ((float) R) / 255.0f;
        float g = ((float) G) / 255.0f;
        float b = ((float) B) / 255.0f;

        float max = r;
        float min = r;

        if (g > max)
          max = g;

        if (b > max)
          max = b;

        if (g < min)
          min = g;

        if (b < min)
          min = b;

        return (max + min) / 2.0f;
      }
    }

    /// <summary>
    /// Gets the color as a packed BGRA value.
    /// </summary>
    public readonly uint PackedValue
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return (uint) (B | (G << 8) | (R << 16) | (A << 24));
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="ColorBGRA"/> struct.
    /// </summary>
    /// <param name="bgra">Packed int containing BGRA values.</param>
    public ColorBGRA(uint bgra)
    {
      UnpackColor(bgra, out B, out G, out R, out A);
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="ColorBGRA"/> struct from BGRA byte values with range 0-255. If
    /// the component values are greater than 255 or less than 0, they will be clamped
    /// to the range 0-255.
    /// </summary>
    /// <param name="blue">Blue component</param>
    /// <param name="green">Green component</param>
    /// <param name="red">Red component</param>
    public ColorBGRA(int blue, int green, int red)
    {
      B = (byte) MathHelper.ClampToByte(blue);
      G = (byte) MathHelper.ClampToByte(green);
      R = (byte) MathHelper.ClampToByte(red);
      A = 255;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="ColorBGRA"/> struct from BGRA byte values with range 0-255. If
    /// the component values are greater than 255 or less than 0, they will be clamped to the range 0-255.
    /// </summary>
    /// <param name="blue">Blue component</param>
    /// <param name="green">Green component</param>
    /// <param name="red">Red component</param>
    /// <param name="alpha">Alpha component</param>
    public ColorBGRA(int blue, int green, int red, int alpha)
    {
      B = (byte) MathHelper.ClampToByte(blue);
      G = (byte) MathHelper.ClampToByte(green);
      R = (byte) MathHelper.ClampToByte(red);
      A = (byte) MathHelper.ClampToByte(alpha);
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="ColorBGRA"/> struct from BGR float values with range 0 to 1.0f. Alpha is set to 1.0f.
    /// </summary>
    /// <param name="blue">Blue component</param>
    /// <param name="green">Green component</param>
    /// <param name="red">Red component</param>
    public ColorBGRA(float blue, float green, float red)
    {
      B = (byte) MathHelper.ClampAndRound(blue * 255.0f, 0.0f, 255.0f);
      G = (byte) MathHelper.ClampAndRound(green * 255.0f, 0.0f, 255.0f);
      R = (byte) MathHelper.ClampAndRound(red * 255.0f, 0.0f, 255.0f);
      A = 255;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="ColorBGRA"/> struct from BGRA float values with range 0 to 1.0f.
    /// </summary>
    /// <param name="blue">Blue component</param>
    /// <param name="green">Green component</param>
    /// <param name="red">Red component</param>
    /// <param name="alpha">Alpha component</param>
    public ColorBGRA(float blue, float green, float red, float alpha)
    {
      B = (byte) MathHelper.ClampAndRound(blue * 255.0f, 0.0f, 255.0f);
      G = (byte) MathHelper.ClampAndRound(green * 255.0f, 0.0f, 255.0f);
      R = (byte) MathHelper.ClampAndRound(red * 255.0f, 0.0f, 255.0f);
      A = (byte) MathHelper.ClampAndRound(alpha * 255.0f, 0.0f, 255.0f);
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="ColorBGRA"/> struct from BGR float values with range 0 to 1.0f.
    /// </summary>
    /// <param name="bgr">Vector3 containing BGR components as XYZ</param>
    public ColorBGRA(in Vector3 bgr)
    {
      B = (byte) MathHelper.ClampAndRound(bgr.Z * 255.0f, 0.0f, 255.0f);
      G = (byte) MathHelper.ClampAndRound(bgr.Y * 255.0f, 0.0f, 255.0f);
      R = (byte) MathHelper.ClampAndRound(bgr.X * 255.0f, 0.0f, 255.0f);
      A = 255;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="ColorBGRA"/> struct from BGRA float values with range 0 to 1.0f.
    /// </summary>
    /// <param name="bgr">Vector3 containing BGR components as XYZ</param>
    /// <param name="alpha">Alpha component</param>
    public ColorBGRA(in Vector3 bgr, float alpha)
    {
      B = (byte) MathHelper.ClampAndRound(bgr.Z * 255.0f, 0.0f, 255.0f);
      G = (byte) MathHelper.ClampAndRound(bgr.Y * 255.0f, 0.0f, 255.0f);
      R = (byte) MathHelper.ClampAndRound(bgr.X * 255.0f, 0.0f, 255.0f);
      A = (byte) MathHelper.ClampAndRound(alpha * 255.0f, 0.0f, 255.0f);
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="ColorBGRA"/> struct from BGRA float values with range 0 to 1.0f.
    /// </summary>
    /// <param name="bgra">Vector4 containing BGRA components as XYZW</param>
    public ColorBGRA(in Vector4 bgra)
    {
      B = (byte) MathHelper.ClampAndRound(bgra.Z * 255.0f, 0.0f, 255.0f);
      G = (byte) MathHelper.ClampAndRound(bgra.Y * 255.0f, 0.0f, 255.0f);
      R = (byte) MathHelper.ClampAndRound(bgra.X * 255.0f, 0.0f, 255.0f);
      A = (byte) MathHelper.ClampAndRound(bgra.W * 255.0f, 0.0f, 255.0f);
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="ColorBGRA"/> struct from an initial BGR color and new alpha value range 0-255.
    /// </summary>
    /// <param name="color">Initial BGR color.</param>
    /// <param name="alpha">Alpha value</param>
    public ColorBGRA(ColorBGRA color, int alpha)
    {
      B = color.B;
      G = color.G;
      R = color.R;
      A = (byte) MathHelper.ClampToByte(alpha);
    }

    //Pack helper
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private static uint PackColor(float b, float g, float r, float a)
    {
      uint n = (uint) MathHelper.ClampAndRound(b * 255.0f, 0.0f, 255.0f);
      uint n1 = ((uint) MathHelper.ClampAndRound(g * 255.0f, 0.0f, 255.0f)) << 8;
      uint n2 = ((uint) MathHelper.ClampAndRound(r * 255.0f, 0.0f, 255.0f)) << 16;
      uint n3 = ((uint) MathHelper.ClampAndRound(a * 255.0f, 0.0f, 255.0f)) << 24;

      return n | n1 | n2 | n3;
    }

    //Unpack helper
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private static void UnpackColor(uint packedValue, out byte b, out byte g, out byte r, out byte a)
    {
      b = (byte) packedValue;
      g = (byte) (packedValue >> 8);
      r = (byte) (packedValue >> 16);
      a = (byte) (packedValue >> 24);
    }

    /// <summary>
    /// Converts a color from a packed BGRA format.
    /// </summary>
    /// <param name="rgba">Packed format, 4 bytes, one for each component in BGRA order.</param>
    /// <returns>Color</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static ColorBGRA FromRGBA(uint rgba)
    {
      byte b, g, r, a;
      UnpackColor(rgba, out b, out g, out r, out a);

      ColorBGRA result;
      result.B = r;
      result.G = g;
      result.R = b;
      result.A = a;

      return result;
    }

    /// <summary>
    /// Adjusts the contrast of the color. If the contrast is 0, the color is 50% gray
    /// and if its 1 the original color is returned.
    /// </summary>
    /// <param name="value">Source color</param>
    /// <param name="contrast">Contrast amount</param>
    /// <returns>Adjusted color</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static ColorBGRA AdjustContrast(ColorBGRA value, float contrast)
    {
      ColorBGRA result;
      result.B = (byte) MathHelper.ClampToByte((int) (((value.B - 128) * contrast) + 128));
      result.G = (byte) MathHelper.ClampToByte((int) (((value.G - 128) * contrast) + 128));
      result.R = (byte) MathHelper.ClampToByte((int) (((value.R - 128) * contrast) + 128));
      result.A = value.A;

      return result;
    }

    /// <summary>
    /// Adjusts the saturation of the color. If the saturation is 0, then the grayscale
    /// color is chosen and if its 1, then the original color is returned.
    /// </summary>
    /// <param name="value">Source color</param>
    /// <param name="saturation">Saturation amount</param>
    /// <returns>Adjusted color</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static ColorBGRA AdjustSaturation(ColorBGRA value, float saturation)
    {
      uint b = value.B;
      uint g = value.G;
      uint r = value.R;

      uint grey = (uint) MathHelper.Clamp((((float) b * .0721f) + ((float) g * .7154f) + ((float) r * .2125f)), 0.0f, 255.0f);

      ColorBGRA result;
      result.B = (byte) MathHelper.ClampToByte((int) (((b - grey) * saturation) + grey));
      result.G = (byte) MathHelper.ClampToByte((int) (((g - grey) * saturation) + grey));
      result.R = (byte) MathHelper.ClampToByte((int) (((r - grey) * saturation) + grey));
      result.A = value.A;

      return result;
    }

    /// <summary>
    /// Clamps the color within range of the min and max values.
    /// </summary>
    /// <param name="value">Source color</param>
    /// <param name="min">Minimum value</param>
    /// <param name="max">Maximum value</param>
    /// <returns>Clamped color</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static ColorBGRA Clamp(ColorBGRA value, ColorBGRA min, ColorBGRA max)
    {
      ColorBGRA result;
      result.B = (byte) MathHelper.Clamp(value.B, min.B, max.B);
      result.G = (byte) MathHelper.Clamp(value.G, min.G, max.G);
      result.R = (byte) MathHelper.Clamp(value.R, min.R, max.R);
      result.A = (byte) MathHelper.Clamp(value.A, min.A, max.A);

      return result;
    }

    /// <summary>
    /// Gets the color that contains the maximum value from each of the components of the 
    /// two supplied colors.
    /// </summary>
    /// <param name="a">First color</param>
    /// <param name="b">Second color</param>
    /// <returns>Maximum color</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static ColorBGRA Max(ColorBGRA a, ColorBGRA b)
    {
      ColorBGRA result;
      result.B = (a.B > b.B) ? a.B : b.B;
      result.G = (a.G > b.G) ? a.G : b.G;
      result.R = (a.R > b.R) ? a.R : b.R;
      result.A = (a.A > b.A) ? a.A : b.A;

      return result;
    }

    /// <summary>
    /// Gets the color that contains the mininum value from each of the components of the 
    /// two supplied colors.
    /// </summary>
    /// <param name="a">First color</param>
    /// <param name="b">Second color</param>
    /// <returns>Minimum color</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static ColorBGRA Min(ColorBGRA a, ColorBGRA b)
    {
      ColorBGRA result;
      result.B = (a.B < b.B) ? a.B : b.B;
      result.G = (a.G < b.G) ? a.G : b.G;
      result.R = (a.R < b.R) ? a.R : b.R;
      result.A = (a.A < b.A) ? a.A : b.A;

      return result;
    }

    /// <summary>
    /// Adds two colors together.
    /// </summary>
    /// <param name="a">First color</param>
    /// <param name="b">Second color</param>
    /// <returns>Sum of the two colors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static ColorBGRA Add(ColorBGRA a, ColorBGRA b)
    {
      ColorBGRA result;
      result.B = (byte) MathHelper.ClampToByte(a.B + b.B);
      result.G = (byte) MathHelper.ClampToByte(a.G + b.G);
      result.R = (byte) MathHelper.ClampToByte(a.R + b.R);
      result.A = (byte) MathHelper.ClampToByte(a.A + b.A);

      return result;
    }

    /// <summary>
    /// Subtracts a color from another.
    /// </summary>
    /// <param name="a">First color</param>
    /// <param name="b">Second color</param>
    /// <returns>Difference of the two colors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static ColorBGRA Subtract(ColorBGRA a, ColorBGRA b)
    {
      ColorBGRA result;
      result.B = (byte) MathHelper.ClampToByte(a.B - b.B);
      result.G = (byte) MathHelper.ClampToByte(a.G - b.G);
      result.R = (byte) MathHelper.ClampToByte(a.R - b.R);
      result.A = (byte) MathHelper.ClampToByte(a.A - b.A);

      return result;
    }

    /// <summary>
    /// Modulates two colors together.
    /// </summary>
    /// <param name="a">First color</param>
    /// <param name="b">Second color</param>
    /// <returns>Modulated color</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static ColorBGRA Modulate(ColorBGRA a, ColorBGRA b)
    {
      ColorBGRA result;
      result.B = (byte) MathHelper.ClampToByte(a.B * b.B);
      result.G = (byte) MathHelper.ClampToByte(a.G * b.G);
      result.R = (byte) MathHelper.ClampToByte(a.R * b.R);
      result.A = (byte) MathHelper.ClampToByte(a.A * b.A);

      return result;
    }

    /// <summary>
    /// Premultiplies the BGR component of a color by its alpha.
    /// </summary>
    /// <param name="color">Non-premultiplied color</param>
    /// <returns>Premultiplied color</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static ColorBGRA PremultiplyAlpha(ColorBGRA color)
    {
      ColorBGRA result;
      result.B = (byte) MathHelper.ClampToByte(color.B * color.A);
      result.G = (byte) MathHelper.ClampToByte(color.G * color.A);
      result.R = (byte) MathHelper.ClampToByte(color.R * color.A);
      result.A = color.A;

      return result;
    }

    /// <summary>
    /// Scales a color by a scaling factor.
    /// </summary>
    /// <param name="value">Source color</param>
    /// <param name="scale">Amount to multiply</param>
    /// <returns>Scaled color</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static ColorBGRA Scale(ColorBGRA value, float scale)
    {
      ColorBGRA result;
      result.B = (byte) MathHelper.ClampToByte((int) (value.B * scale));
      result.G = (byte) MathHelper.ClampToByte((int) (value.G * scale));
      result.R = (byte) MathHelper.ClampToByte((int) (value.R * scale));
      result.A = (byte) MathHelper.ClampToByte((int) (value.A * scale));

      return result;
    }

    /// <summary>
    /// Negates the specified color by subtracting each of its components from 1.0f.
    /// </summary>
    /// <param name="value">Source color</param>
    /// <returns>Negated color</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static ColorBGRA Negate(ColorBGRA value)
    {
      ColorBGRA result;
      result.B = (byte) (255 - value.B);
      result.G = (byte) (255 - value.G);
      result.R = (byte) (255 - value.R);
      result.A = (byte) (255 - value.A);

      return result;
    }

    /// <summary>
    /// Linearly interpolates between two colors.
    /// </summary>
    /// <param name="a">Starting color</param>
    /// <param name="b">Ending color</param>
    /// <param name="percent">Amount to interpolate by</param>
    /// <returns>Interpolated color</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static ColorBGRA Lerp(ColorBGRA a, ColorBGRA b, float percent)
    {
      ColorBGRA result;
      result.B = (byte) MathHelper.ClampToByte(a.B + (int) ((b.B - a.B) * percent));
      result.G = (byte) MathHelper.ClampToByte(a.G + (int) ((b.G - a.G) * percent));
      result.R = (byte) MathHelper.ClampToByte(a.R + (int) ((b.R - a.R) * percent));
      result.A = (byte) MathHelper.ClampToByte(a.A + (int) ((b.A - a.A) * percent));

      return result;
    }

    /// <summary>
    /// Compute a cubic interpolation between two colors.
    /// </summary>
    /// <param name="a">First color</param>
    /// <param name="b">Second color</param>
    /// <param name="wf">Weighting factor (between 0 and 1.0)</param>
    /// <returns>Cubic interpolated color</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static ColorBGRA SmoothStep(ColorBGRA a, ColorBGRA b, float wf)
    {
      float amt = MathHelper.Clamp(wf, 0.0f, 1.0f);
      amt = (amt * amt) * (3.0f - (2.0f * amt));

      ColorBGRA result;
      result.B = (byte) MathHelper.ClampToByte(a.B + (int) ((b.B - a.B) * amt));
      result.G = (byte) MathHelper.ClampToByte(a.G + (int) ((b.G - a.G) * amt));
      result.R = (byte) MathHelper.ClampToByte(a.R + (int) ((b.R - a.R) * amt));
      result.A = (byte) MathHelper.ClampToByte(a.A + (int) ((b.A - a.A) * amt));

      return result;
    }

    /// <summary>
    /// Tests equality between two colors.
    /// </summary>
    /// <param name="a">First color</param>
    /// <param name="b">Second color</param>
    /// <returns>True if components are equal</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator ==(ColorBGRA a, ColorBGRA b)
    {
      return (a.B == b.B) && (a.G == b.G) && (a.R == b.R) && (a.A == b.A);
    }

    /// <summary>
    /// Tests inequality between two colors.
    /// </summary>
    /// <param name="a">First color</param>
    /// <param name="b">Second color</param>
    /// <returns>True if components are not equal</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator !=(ColorBGRA a, ColorBGRA b)
    {
      return (a.B != b.B) || (a.G != b.G) || (a.R != b.R) || (a.A != b.A);
    }

    /// <summary>
    /// Adds the two colors together.
    /// </summary>
    /// <param name="a">First color</param>
    /// <param name="b">Second color</param>
    /// <returns>Sum of the two colors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static ColorBGRA operator +(ColorBGRA a, ColorBGRA b)
    {
      ColorBGRA result;
      result.B = (byte) MathHelper.ClampToByte(a.B + b.B);
      result.G = (byte) MathHelper.ClampToByte(a.G + b.G);
      result.R = (byte) MathHelper.ClampToByte(a.R + b.R);
      result.A = (byte) MathHelper.ClampToByte(a.A + b.A);

      return result;
    }

    /// <summary>
    /// Negates the color (subtracts value from 1.0f) of each color component.
    /// </summary>
    /// <param name="value">Source color</param>
    /// <returns>Difference of the two colors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static ColorBGRA operator -(ColorBGRA value)
    {
      ColorBGRA result;
      result.B = (byte) (255 - value.B);
      result.G = (byte) (255 - value.G);
      result.R = (byte) (255 - value.R);
      result.A = (byte) (255 - value.A);

      return result;
    }

    /// <summary>
    /// Subtracts a color from another.
    /// </summary>
    /// <param name="a">First color</param>
    /// <param name="b">Second color</param>
    /// <returns>Difference of the two colors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static ColorBGRA operator -(ColorBGRA a, ColorBGRA b)
    {
      ColorBGRA result;
      result.B = (byte) MathHelper.ClampToByte(a.B - b.B);
      result.G = (byte) MathHelper.ClampToByte(a.G - b.G);
      result.R = (byte) MathHelper.ClampToByte(a.R - b.R);
      result.A = (byte) MathHelper.ClampToByte(a.A - b.A);

      return result;
    }

    /// <summary>
    /// Modulates two colors together.
    /// </summary>
    /// <param name="a">First color</param>
    /// <param name="b">Second color</param>
    /// <returns>Modulated color</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static ColorBGRA operator *(ColorBGRA a, ColorBGRA b)
    {
      ColorBGRA result;
      result.B = (byte) MathHelper.ClampToByte(a.B * b.B);
      result.G = (byte) MathHelper.ClampToByte(a.G * b.G);
      result.R = (byte) MathHelper.ClampToByte(a.R * b.R);
      result.A = (byte) MathHelper.ClampToByte(a.A * b.A);

      return result;
    }

    /// <summary>
    /// Scales a color by a scaling factor.
    /// </summary>
    /// <param name="value">Source color</param>
    /// <param name="scale">Amount to multiply</param>
    /// <returns>Scaled color</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static ColorBGRA operator *(ColorBGRA value, float scale)
    {
      ColorBGRA result;
      result.B = (byte) MathHelper.ClampToByte((int) (value.B * scale));
      result.G = (byte) MathHelper.ClampToByte((int) (value.G * scale));
      result.R = (byte) MathHelper.ClampToByte((int) (value.R * scale));
      result.A = (byte) MathHelper.ClampToByte((int) (value.A * scale));

      return result;
    }

    /// <summary>
    /// Scales a color by a scaling factor.
    /// </summary>
    /// <param name="scale">Amount to multiply</param>
    /// <param name="value">Source color</param>
    /// <returns>Scaled color</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static ColorBGRA operator *(float scale, ColorBGRA value)
    {
      ColorBGRA result;
      result.B = (byte) MathHelper.ClampToByte((int) (value.B * scale));
      result.G = (byte) MathHelper.ClampToByte((int) (value.G * scale));
      result.R = (byte) MathHelper.ClampToByte((int) (value.R * scale));
      result.A = (byte) MathHelper.ClampToByte((int) (value.A * scale));

      return result;
    }

    /// <summary>
    /// Performs an explicit conversion from <see cref="ColorBGRA"/> to <see cref="uint"/>.
    /// </summary>
    /// <param name="value">Color value.</param>
    /// <returns>RGBA as a packed unsigned integer</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static explicit operator uint(ColorBGRA value)
    {
      return value.PackedValue;
    }

    /// <summary>
    /// Performs an explicit conversion from <see cref="ColorBGRA"/> to <see cref="Vector3"/>.
    /// </summary>
    /// <param name="value">Color value.</param>
    /// <returns>BGR as a vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static explicit operator Vector3(ColorBGRA value)
    {
      return value.ToVector3();
    }

    /// <summary>
    /// Performs an explicit conversion from <see cref="ColorBGRA"/> to <see cref="Vector4"/>.
    /// </summary>
    /// <param name="value">Color value</param>
    /// <returns>BGRA as a vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static explicit operator Vector4(ColorBGRA value)
    {
      return value.ToVector4();
    }

    /// <summary>
    /// Performs an explicit conversion from <see cref="Vector3"/> to <see cref="ColorBGRA"/>.
    /// </summary>
    /// <param name="value">BGR as a vector</param>
    /// <returns>Converted color</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static explicit operator ColorBGRA(in Vector3 value)
    {
      return new ColorBGRA(value);
    }

    /// <summary>
    /// Performs an explicit conversion from <see cref="Vector4"/> to <see cref="ColorBGRA"/>.
    /// </summary>
    /// <param name="value">BGRA as a vector</param>
    /// <returns>Converted color</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static explicit operator ColorBGRA(in Vector4 value)
    {
      return new ColorBGRA(value);
    }

    /// <summary>
    /// Tests equality between this color and another color.
    /// </summary>
    /// <param name="other">Color to test against</param>
    /// <returns>True if the colors are equal</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Equals(ColorBGRA other)
    {
      return (B == other.B) && (G == other.G) && (R == other.R) && (A == other.A);
    }

    /// <summary>
    /// Tests equality between this color and another color.
    /// </summary>
    /// <param name="other">Color to test against</param>
    /// <returns>True if the colors are equal</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    readonly bool IRefEquatable<ColorBGRA>.Equals(in ColorBGRA other)
    {
      return (B == other.B) && (G == other.G) && (R == other.R) && (A == other.A);
    }

    /// <summary>
    /// Tests equality between this color and the supplied object.
    /// </summary>
    /// <param name="obj">Object to compare</param>
    /// <returns>True if object is a color and components are equal</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is ColorBGRA)
        return Equals((ColorBGRA) obj);

      return false;
    }

    /// <summary>
    /// Returns this Color as as 3-component float vector. BGR corresponds to XYZ.
    /// </summary>
    /// <returns>Color as a Vector3</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly Vector3 ToVector3()
    {
      Vector3 result;
      result.X = ((float) B) / 255.0f;
      result.Y = ((float) G) / 255.0f;
      result.Z = ((float) R) / 255.0f;

      return result;
    }

    /// <summary>
    /// Returns this Color as 4-component float vector. BGRA corresponds to XYZW.
    /// </summary>
    /// <returns>Color as a Vector4</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly Vector4 ToVector4()
    {
      Vector4 result;
      result.X = ((float) B) / 255.0f;
      result.Y = ((float) G) / 255.0f;
      result.Z = ((float) R) / 255.0f;
      result.W = ((float) A) / 255.0f;

      return result;
    }

    /// <summary>
    /// Negates this color by subtracting each of its components from 1.0f.
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void Negate()
    {
      B = (byte) (255 - B);
      G = (byte) (255 - G);
      R = (byte) (255 - R);
      A = (byte) (255 - A);
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
    public readonly override int GetHashCode()
    {
      unchecked
      {
        return B.GetHashCode() + G.GetHashCode() + R.GetHashCode() + A.GetHashCode();
      }
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly override string ToString()
    {
      CultureInfo info = CultureInfo.CurrentCulture;
      return String.Format(info, "B: {0} G: {1} R: {2} A: {3}",
          new Object[] { B.ToString(info), G.ToString(info), R.ToString(info), A.ToString(info) });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="format">The format</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(string? format)
    {
      if (format is null)
        return ToString();

      CultureInfo info = CultureInfo.CurrentCulture;
      return String.Format(info, "B: {0} G: {1} R: {2} A: {3}",
          new Object[] { B.ToString(format, info), G.ToString(format, info), R.ToString(format, info), A.ToString(format, info) });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="formatProvider">The format provider.</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(IFormatProvider? formatProvider)
    {
      if (formatProvider is null)
        return ToString();

      return String.Format(formatProvider, "B: {0} G: {1} R: {2} A: {3}",
          new Object[] { B.ToString(formatProvider), G.ToString(formatProvider), R.ToString(formatProvider), A.ToString(formatProvider) });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="format">The format.</param>
    /// <param name="formatProvider">The format provider.</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(string? format, IFormatProvider? formatProvider)
    {
      if (formatProvider is null)
        return ToString();

      if (format is null)
        return ToString(formatProvider);

      return String.Format(formatProvider, "B: {0} G: {1} R: {2} A: {3}",
          new Object[] { B.ToString(format, formatProvider), G.ToString(format, formatProvider), R.ToString(format, formatProvider), A.ToString(format, formatProvider) });
    }

    /// <summary>
    /// Writes the primitive data to the output.
    /// </summary>
    /// <param name="output">Primitive writer.</param>
    readonly void IPrimitiveValue.Write(IPrimitiveWriter output)
    {
      output.Write("B", B);
      output.Write("G", G);
      output.Write("R", R);
      output.Write("A", A);
    }

    /// <summary>
    /// Reads the primitive data from the input.
    /// </summary>
    /// <param name="input">Primitive reader.</param>
    void IPrimitiveValue.Read(IPrimitiveReader input)
    {
      B = input.ReadByte("B");
      G = input.ReadByte("G");
      R = input.ReadByte("R");
      A = input.ReadByte("A");
    }

    #region Predefined Colors

    /// <summary>
    /// Gets a color with the value R:0 G: 0 B: 0 A: 0.
    /// </summary>
    public static ColorBGRA TransparentBlack
    {
      get
      {
        return new ColorBGRA(0, 0, 0, 0);
      }
    }

    /// <summary>
    /// Gets a color with the value R:240 G:248 B:255 A:255.
    /// </summary>
    public static ColorBGRA AliceBlue
    {
      get
      {
        return new ColorBGRA(255, 248, 240, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:250 G:235 B:215 A:255.
    /// </summary>
    public static ColorBGRA AntiqueWhite
    {
      get
      {
        return new ColorBGRA(215, 235, 250, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:0 G:255 B:255 A:255.
    /// </summary>
    public static ColorBGRA Aqua
    {
      get
      {
        return new ColorBGRA(255, 255, 0, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:127 G:255 B:212 A:255.
    /// </summary>
    public static ColorBGRA Aquamarine
    {
      get
      {
        return new ColorBGRA(212, 255, 127, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:240 G:255 B:255 A:255.
    /// </summary>
    public static ColorBGRA Azure
    {
      get
      {
        return new ColorBGRA(255, 255, 240, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:245 G:245 B:220 A:255.
    /// </summary>
    public static ColorBGRA Beige
    {
      get
      {
        return new ColorBGRA(220, 245, 245, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:228 B:196 A:255.
    /// </summary>
    public static ColorBGRA Bisque
    {
      get
      {
        return new ColorBGRA(196, 228, 255, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:0 G:0 B:0 A:255.
    /// </summary>
    public static ColorBGRA Black
    {
      get
      {
        return new ColorBGRA(0, 0, 0, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:235 B:205 A:255.
    /// </summary>
    public static ColorBGRA BlanchedAlmond
    {
      get
      {
        return new ColorBGRA(205, 235, 255, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:0 G:0 B:255 A:255.
    /// </summary>
    public static ColorBGRA Blue
    {
      get
      {
        return new ColorBGRA(255, 0, 0, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:138 G:43 B:226 A:255.
    /// </summary>
    public static ColorBGRA BlueViolet
    {
      get
      {
        return new ColorBGRA(226, 43, 138, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:165 G:42 B:42 A:255.
    /// </summary>
    public static ColorBGRA Brown
    {
      get
      {
        return new ColorBGRA(42, 42, 165, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:222 G:184 B:135 A:255.
    /// </summary>
    public static ColorBGRA BurlyWood
    {
      get
      {
        return new ColorBGRA(135, 184, 222, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:95 G:158 B:160 A:255.
    /// </summary>
    public static ColorBGRA CadetBlue
    {
      get
      {
        return new ColorBGRA(160, 158, 95, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:127 G:255 B:0 A:255.
    /// </summary>
    public static ColorBGRA Chartreuse
    {
      get
      {
        return new ColorBGRA(0, 255, 127, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:210 G:105 B:30 A:255.
    /// </summary>
    public static ColorBGRA Chocolate
    {
      get
      {
        return new ColorBGRA(30, 105, 210, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:127 B:80 A:255.
    /// </summary>
    public static ColorBGRA Coral
    {
      get
      {
        return new ColorBGRA(80, 127, 255, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:100 G:149 B:237 A:255.
    /// </summary>
    public static ColorBGRA CornflowerBlue
    {
      get
      {
        return new ColorBGRA(237, 149, 100, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:248 B:220 A:255.
    /// </summary>
    public static ColorBGRA Cornsilk
    {
      get
      {
        return new ColorBGRA(220, 248, 255, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:220 G:20 B:60 A:255.
    /// </summary>
    public static ColorBGRA Crimson
    {
      get
      {
        return new ColorBGRA(60, 20, 220, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:0 G:255 B:255 A:255.
    /// </summary>
    public static ColorBGRA Cyan
    {
      get
      {
        return new ColorBGRA(255, 255, 0, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:0 G:0 B:139 A:255.
    /// </summary>
    public static ColorBGRA DarkBlue
    {
      get
      {
        return new ColorBGRA(139, 0, 0, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:0 G:139 B:139 A:255.
    /// </summary>
    public static ColorBGRA DarkCyan
    {
      get
      {
        return new ColorBGRA(139, 139, 0, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:184 G:134 B:11 A:255.
    /// </summary>
    public static ColorBGRA DarkGoldenrod
    {
      get
      {
        return new ColorBGRA(11, 134, 184, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:169 G:169 B:169 A:255.
    /// </summary>
    public static ColorBGRA DarkGray
    {
      get
      {
        return new ColorBGRA(169, 169, 169, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:0 G:100 B:0 A:255.
    /// </summary>
    public static ColorBGRA DarkGreen
    {
      get
      {
        return new ColorBGRA(0, 100, 40, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:189 G:183 B:107 A:255.
    /// </summary>
    public static ColorBGRA DarkKhaki
    {
      get
      {
        return new ColorBGRA(107, 183, 189, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:139 G:0 B:139 A:255.
    /// </summary>
    public static ColorBGRA DarkMagenta
    {
      get
      {
        return new ColorBGRA(139, 0, 139, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:85 G:107 B:47 A:255.
    /// </summary>
    public static ColorBGRA DarkOliveGreen
    {
      get
      {
        return new ColorBGRA(47, 107, 85, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:140 B:0 A:255.
    /// </summary>
    public static ColorBGRA DarkOrange
    {
      get
      {
        return new ColorBGRA(0, 140, 255, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:153 G:50 B:204 A:255.
    /// </summary>
    public static ColorBGRA DarkOrchid
    {
      get
      {
        return new ColorBGRA(204, 50, 153, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:139 G:0 B:0 A:255.
    /// </summary>
    public static ColorBGRA DarkRed
    {
      get
      {
        return new ColorBGRA(0, 0, 139, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:233 G:150 B:122 A:255.
    /// </summary>
    public static ColorBGRA DarkSalmon
    {
      get
      {
        return new ColorBGRA(122, 150, 233, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:143 G:188 B:139 A:255.
    /// </summary>
    public static ColorBGRA DarkSeaGreen
    {
      get
      {
        return new ColorBGRA(139, 188, 143, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:72 G:61 B:139 A:255.
    /// </summary>
    public static ColorBGRA DarkSlateBlue
    {
      get
      {
        return new ColorBGRA(139, 61, 72, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:47 G:79 B:79 A:255.
    /// </summary>
    public static ColorBGRA DarkSlateGray
    {
      get
      {
        return new ColorBGRA(79, 79, 47, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:0 G:206 B:209 A:255.
    /// </summary>
    public static ColorBGRA DarkTurquoise
    {
      get
      {
        return new ColorBGRA(209, 206, 0, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:148 G:0 B:211 A:255.
    /// </summary>
    public static ColorBGRA DarkViolet
    {
      get
      {
        return new ColorBGRA(211, 0, 148, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:20 B:147 A:255.
    /// </summary>
    public static ColorBGRA DeepPink
    {
      get
      {
        return new ColorBGRA(147, 20, 255, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:0 G:191 B:255 A:255.
    /// </summary>
    public static ColorBGRA DeepSkyBlue
    {
      get
      {
        return new ColorBGRA(255, 191, 0, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:105 G:105 B:105 A:255.
    /// </summary>
    public static ColorBGRA DimGray
    {
      get
      {
        return new ColorBGRA(105, 105, 105, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:30 G:144 B:255 A:255.
    /// </summary>
    public static ColorBGRA DodgerBlue
    {
      get
      {
        return new ColorBGRA(255, 144, 30, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:178 G:34 B:34 A:255.
    /// </summary>
    public static ColorBGRA Firebrick
    {
      get
      {
        return new ColorBGRA(34, 34, 178, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:250 B:240 A:255.
    /// </summary>
    public static ColorBGRA FloralWhite
    {
      get
      {
        return new ColorBGRA(240, 250, 255, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:34 G:139 B:34 A:255.
    /// </summary>
    public static ColorBGRA ForestGreen
    {
      get
      {
        return new ColorBGRA(34, 139, 34, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:0 B:255 A:255.
    /// </summary>
    public static ColorBGRA Fuchsia
    {
      get
      {
        return new ColorBGRA(255, 0, 255, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:220 G:220 B:220 A:255.
    /// </summary>
    public static ColorBGRA Gainsboro
    {
      get
      {
        return new ColorBGRA(220, 220, 220, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:248 G:248 B:255 A:255.
    /// </summary>
    public static ColorBGRA GhostWhite
    {
      get
      {
        return new ColorBGRA(255, 248, 248, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:215 B:0 A:255.
    /// </summary>
    public static ColorBGRA Gold
    {
      get
      {
        return new ColorBGRA(0, 215, 255, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:218 G:165 B:32 A:255.
    /// </summary>
    public static ColorBGRA Goldenrod
    {
      get
      {
        return new ColorBGRA(32, 165, 218, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:128 G:128 B:128 A:255.
    /// </summary>
    public static ColorBGRA Gray
    {
      get
      {
        return new ColorBGRA(128, 128, 128, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:0 G:128 B:0 A:255.
    /// </summary>
    public static ColorBGRA Green
    {
      get
      {
        return new ColorBGRA(0, 128, 0, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:173 G:255 B:47 A:255.
    /// </summary>
    public static ColorBGRA GreenYellow
    {
      get
      {
        return new ColorBGRA(47, 255, 173, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:240 G:255 B:240 A:255.
    /// </summary>
    public static ColorBGRA Honeydew
    {
      get
      {
        return new ColorBGRA(240, 255, 240, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:105 B:180 A:255.
    /// </summary>
    public static ColorBGRA HotPink
    {
      get
      {
        return new ColorBGRA(180, 105, 255, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:205 G:92 B:92 A:255.
    /// </summary>
    public static ColorBGRA IndianRed
    {
      get
      {
        return new ColorBGRA(92, 92, 205, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:75 G:0 B:130 A:255.
    /// </summary>
    public static ColorBGRA Indigo
    {
      get
      {
        return new ColorBGRA(130, 0, 75, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:255 B:240 A:255.
    /// </summary>
    public static ColorBGRA Ivory
    {
      get
      {
        return new ColorBGRA(240, 255, 255, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:240 G:230 B:140 A:255.
    /// </summary>
    public static ColorBGRA Khaki
    {
      get
      {
        return new ColorBGRA(140, 230, 240, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:230 G:230 B:250 A:255.
    /// </summary>
    public static ColorBGRA Lavender
    {
      get
      {
        return new ColorBGRA(250, 230, 230, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:240 B:245 A:255.
    /// </summary>
    public static ColorBGRA LavenderBlush
    {
      get
      {
        return new ColorBGRA(245, 240, 255, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:124 G:252 B:0 A:255.
    /// </summary>
    public static ColorBGRA LawnGreen
    {
      get
      {
        return new ColorBGRA(0, 252, 124, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:250 B:205 A:255.
    /// </summary>
    public static ColorBGRA LemonChiffon
    {
      get
      {
        return new ColorBGRA(205, 250, 255, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:173 G:216 B:230 A:255.
    /// </summary>
    public static ColorBGRA LightBlue
    {
      get
      {
        return new ColorBGRA(230, 216, 173, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:240 G:128 B:128 A:255.
    /// </summary>
    public static ColorBGRA LightCoral
    {
      get
      {
        return new ColorBGRA(128, 128, 240, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:224 G:255 B:255 A:255.
    /// </summary>
    public static ColorBGRA LightCyan
    {
      get
      {
        return new ColorBGRA(255, 255, 224, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:250 G:250 B:210 A:255.
    /// </summary>
    public static ColorBGRA LightGoldenrodYellow
    {
      get
      {
        return new ColorBGRA(210, 250, 250, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:144 G:238 B:144 A:255.
    /// </summary>
    public static ColorBGRA LightGreen
    {
      get
      {
        return new ColorBGRA(144, 238, 144, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:211 G:211 B:211 A:255.
    /// </summary>
    public static ColorBGRA LightGray
    {
      get
      {
        return new ColorBGRA(211, 211, 211, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:182 B:193 A:255.
    /// </summary>
    public static ColorBGRA LightPink
    {
      get
      {
        return new ColorBGRA(193, 182, 255, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:160 B:122 A:255.
    /// </summary>
    public static ColorBGRA LightSalmon
    {
      get
      {
        return new ColorBGRA(122, 160, 255, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:32 G:178 B:170 A:255.
    /// </summary>
    public static ColorBGRA LightSeaGreen
    {
      get
      {
        return new ColorBGRA(170, 178, 32, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:135 G:206 B:250 A:255.
    /// </summary>
    public static ColorBGRA LightSkyBlue
    {
      get
      {
        return new ColorBGRA(250, 206, 135, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:119 G:136 B:153 A:255.
    /// </summary>
    public static ColorBGRA LightSlateGray
    {
      get
      {
        return new ColorBGRA(153, 136, 119, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:176 G:196 B:222 A:255.
    /// </summary>
    public static ColorBGRA LightSteelBlue
    {
      get
      {
        return new ColorBGRA(222, 196, 176, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:255 B:224 A:255.
    /// </summary>
    public static ColorBGRA LightYellow
    {
      get
      {
        return new ColorBGRA(224, 255, 255, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:0 G:255 B:0 A:255.
    /// </summary>
    public static ColorBGRA Lime
    {
      get
      {
        return new ColorBGRA(0, 255, 0, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:50 G:205 B:50 A:255.
    /// </summary>
    public static ColorBGRA LimeGreen
    {
      get
      {
        return new ColorBGRA(50, 205, 50, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:250 G:240 B:230 A:255.
    /// </summary>
    public static ColorBGRA Linen
    {
      get
      {
        return new ColorBGRA(230, 240, 250, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:0 B:255 A:255.
    /// </summary>
    public static ColorBGRA Magenta
    {
      get
      {
        return new ColorBGRA(255, 0, 255, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:128 G:0 B:0 A:255.
    /// </summary>
    public static ColorBGRA Maroon
    {
      get
      {
        return new ColorBGRA(0, 0, 128, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:102 G:205 B:170 A:255.
    /// </summary>
    public static ColorBGRA MediumAquamarine
    {
      get
      {
        return new ColorBGRA(170, 205, 102, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:0 G:0 B:205 A:255.
    /// </summary>
    public static ColorBGRA MediumBlue
    {
      get
      {
        return new ColorBGRA(205, 0, 0, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:186 G:85 B:211 A:255.
    /// </summary>
    public static ColorBGRA MediumOrchid
    {
      get
      {
        return new ColorBGRA(211, 85, 186, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:147 G:112 B:219 A:255.
    /// </summary>
    public static ColorBGRA MediumPurple
    {
      get
      {
        return new ColorBGRA(219, 112, 147, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:60 G:179 B:113 A:255.
    /// </summary>
    public static ColorBGRA MediumSeaGreen
    {
      get
      {
        return new ColorBGRA(113, 179, 60, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:123 G:104 B:238 A:255.
    /// </summary>
    public static ColorBGRA MediumSlateBlue
    {
      get
      {
        return new ColorBGRA(238, 104, 123, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:0 G:250 B:154 A:255.
    /// </summary>
    public static ColorBGRA MediumSpringGreen
    {
      get
      {
        return new ColorBGRA(154, 250, 0, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:72 G:209 B:204 A:255.
    /// </summary>
    public static ColorBGRA MediumTurquoise
    {
      get
      {
        return new ColorBGRA(204, 209, 72, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:199 G:21 B:133 A:255.
    /// </summary>
    public static ColorBGRA MediumVioletRed
    {
      get
      {
        return new ColorBGRA(133, 21, 199, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:25 G:25 B:112 A:255.
    /// </summary>
    public static ColorBGRA MidnightBlue
    {
      get
      {
        return new ColorBGRA(112, 25, 25, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:245 G:255 B:250 A:255.
    /// </summary>
    public static ColorBGRA MintCream
    {
      get
      {
        return new ColorBGRA(250, 255, 245, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:228 B:225 A:255.
    /// </summary>
    public static ColorBGRA MistyRose
    {
      get
      {
        return new ColorBGRA(225, 228, 255, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:228 B:181 A:255.
    /// </summary>
    public static ColorBGRA Moccasin
    {
      get
      {
        return new ColorBGRA(181, 228, 255, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:222 B:173 A:255.
    /// </summary>
    public static ColorBGRA NavajoWhite
    {
      get
      {
        return new ColorBGRA(173, 222, 255, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:0 G:0 B:128 A:255.
    /// </summary>
    public static ColorBGRA Navy
    {
      get
      {
        return new ColorBGRA(128, 0, 0, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:253 G:245 B:230 A:255.
    /// </summary>
    public static ColorBGRA OldLace
    {
      get
      {
        return new ColorBGRA(230, 245, 253, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:128 G:128 B:0 A:255.
    /// </summary>
    public static ColorBGRA Olive
    {
      get
      {
        return new ColorBGRA(0, 128, 128, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:107 G:142 B:35 A:255.
    /// </summary>
    public static ColorBGRA OliveDrab
    {
      get
      {
        return new ColorBGRA(35, 142, 107, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:165 B:0 A:255.
    /// </summary>
    public static ColorBGRA Orange
    {
      get
      {
        return new ColorBGRA(0, 165, 255, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:69 B:0 A:255.
    /// </summary>
    public static ColorBGRA OrangeRed
    {
      get
      {
        return new ColorBGRA(0, 69, 255, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:218 G:112 B:214 A:255.
    /// </summary>
    public static ColorBGRA Orchid
    {
      get
      {
        return new ColorBGRA(214, 112, 218, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:238 G:232 B:170 A:255.
    /// </summary>
    public static ColorBGRA PaleGoldenrod
    {
      get
      {
        return new ColorBGRA(170, 232, 238, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:152 G:251 B:152 A:255.
    /// </summary>
    public static ColorBGRA PaleGreen
    {
      get
      {
        return new ColorBGRA(152, 251, 152, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:175 G:238 B:238 A:255.
    /// </summary>
    public static ColorBGRA PaleTurquoise
    {
      get
      {
        return new ColorBGRA(238, 238, 175, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:219 G:112 B:147 A:255.
    /// </summary>
    public static ColorBGRA PaleVioletRed
    {
      get
      {
        return new ColorBGRA(147, 112, 219, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:239 B:213 A:255.
    /// </summary>
    public static ColorBGRA PapayaWhip
    {
      get
      {
        return new ColorBGRA(213, 239, 255, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:218 B:185 A:255.
    /// </summary>
    public static ColorBGRA PeachPuff
    {
      get
      {
        return new ColorBGRA(185, 218, 255, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:205 G:133 B:63 A:255.
    /// </summary>
    public static ColorBGRA Peru
    {
      get
      {
        return new ColorBGRA(63, 133, 205, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:192 B:203 A:255.
    /// </summary>
    public static ColorBGRA Pink
    {
      get
      {
        return new ColorBGRA(203, 192, 255, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:221 G:160 B:221 A:255.
    /// </summary>
    public static ColorBGRA Plum
    {
      get
      {
        return new ColorBGRA(221, 160, 221, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:176 G:224 B:230 A:255.
    /// </summary>
    public static ColorBGRA PowderBlue
    {
      get
      {
        return new ColorBGRA(230, 224, 176, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:128 G:0 B:128 A:255.
    /// </summary>
    public static ColorBGRA Purple
    {
      get
      {
        return new ColorBGRA(128, 0, 128, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:0 B:0 A:255.
    /// </summary>
    public static ColorBGRA Red
    {
      get
      {
        return new ColorBGRA(0, 0, 255, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:188 G:143 B:143 A:255.
    /// </summary>
    public static ColorBGRA RosyBrown
    {
      get
      {
        return new ColorBGRA(143, 143, 188, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:65 G:105 B:225 A:255.
    /// </summary>
    public static ColorBGRA RoyalBlue
    {
      get
      {
        return new ColorBGRA(225, 105, 65, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:139 G:69 B:19 A:255.
    /// </summary>
    public static ColorBGRA SaddleBrown
    {
      get
      {
        return new ColorBGRA(19, 69, 139, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:250 G:128 B:114 A:255.
    /// </summary>
    public static ColorBGRA Salmon
    {
      get
      {
        return new ColorBGRA(114, 128, 250, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:244 G:164 B:96 A:255.
    /// </summary>
    public static ColorBGRA SandyBrown
    {
      get
      {
        return new ColorBGRA(96, 164, 244, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:46 G:139 B:87 A:255.
    /// </summary>
    public static ColorBGRA SeaGreen
    {
      get
      {
        return new ColorBGRA(87, 139, 46, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:245 B:238 A:255.
    /// </summary>
    public static ColorBGRA SeaShell
    {
      get
      {
        return new ColorBGRA(238, 245, 255, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:160 G:82 B:45 A:255.
    /// </summary>
    public static ColorBGRA Sienna
    {
      get
      {
        return new ColorBGRA(45, 82, 160, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:192 G:192 B:192 A:255.
    /// </summary>
    public static ColorBGRA Silver
    {
      get
      {
        return new ColorBGRA(192, 192, 192, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:135 G:206 B:235 A:255.
    /// </summary>
    public static ColorBGRA SkyBlue
    {
      get
      {
        return new ColorBGRA(235, 206, 135, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:106 G:90 B:205 A:255.
    /// </summary>
    public static ColorBGRA SlateBlue
    {
      get
      {
        return new ColorBGRA(205, 90, 106, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:112 G:128 B:144 A:255.
    /// </summary>
    public static ColorBGRA SlateGray
    {
      get
      {
        return new ColorBGRA(144, 128, 112, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:250 B:250 A:255.
    /// </summary>
    public static ColorBGRA Snow
    {
      get
      {
        return new ColorBGRA(250, 250, 255, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:0 G:255 B:127 A:255.
    /// </summary>
    public static ColorBGRA SpringGreen
    {
      get
      {
        return new ColorBGRA(127, 255, 0, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:70 G:130 B:180 A:255.
    /// </summary>
    public static ColorBGRA SteelBlue
    {
      get
      {
        return new ColorBGRA(180, 130, 70, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:210 G:180 B:140 A:255.
    /// </summary>
    public static ColorBGRA Tan
    {
      get
      {
        return new ColorBGRA(140, 180, 210, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:0 G:128 B:128 A:255.
    /// </summary>
    public static ColorBGRA Teal
    {
      get
      {
        return new ColorBGRA(128, 128, 0, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:216 G:191 B:216 A:255.
    /// </summary>
    public static ColorBGRA Thistle
    {
      get
      {
        return new ColorBGRA(216, 191, 216, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:99 B:71 A:255.
    /// </summary>
    public static ColorBGRA Tomato
    {
      get
      {
        return new ColorBGRA(71, 99, 255, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:64 G:224 B:208 A:255.
    /// </summary>
    public static ColorBGRA Turquoise
    {
      get
      {
        return new ColorBGRA(208, 224, 64, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:238 G:130 B:238 A:255.
    /// </summary>
    public static ColorBGRA Violet
    {
      get
      {
        return new ColorBGRA(238, 130, 238, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:245 G:222 B:179 A:255.
    /// </summary>
    public static ColorBGRA Wheat
    {
      get
      {
        return new ColorBGRA(179, 222, 245, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:255 B:255 A:255.
    /// </summary>
    public static ColorBGRA White
    {
      get
      {
        return new ColorBGRA(255, 255, 255, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:245 G:245 B:245 A:255.
    /// </summary>
    public static ColorBGRA WhiteSmoke
    {
      get
      {
        return new ColorBGRA(245, 245, 245, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:255 B:0 A:255.
    /// </summary>
    public static ColorBGRA Yellow
    {
      get
      {
        return new ColorBGRA(0, 255, 255, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:154 G:205 B:50 A:255.
    /// </summary>
    public static ColorBGRA YellowGreen
    {
      get
      {
        return new ColorBGRA(50, 205, 154, 255);
      }
    }

    #endregion
  }
}
