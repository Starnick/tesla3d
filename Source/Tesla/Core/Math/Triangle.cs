﻿/*
* Copyright (c) 2010-2024 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Tesla.Content;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Defines a 3-point primitive. The vertex winding is considered to be clockwise ordering of {A, B, C}.
  /// </summary>
  [Serializable]
  [StructLayout(LayoutKind.Sequential, Pack = 4)]
  public struct Triangle : IEquatable<Triangle>, IRefEquatable<Triangle>, IFormattable, IPrimitiveValue
  {
    /// <summary>
    /// First vertex of the triangle.
    /// </summary>
    public Vector3 PointA;

    /// <summary>
    /// Second vertex of the triangle.
    /// </summary>
    public Vector3 PointB;

    /// <summary>
    /// Third vertex of the triangle.
    /// </summary>
    public Vector3 PointC;

    /// <summary>
    /// Gets the normal vector of the triangle. The triangle is considered to be in a clockwise ordering
    /// of {A, B, C}.
    /// </summary>
    public readonly Vector3 Normal
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        Vector3 normal;
        Vector3 edgeCA, edgeBA;
        Vector3.Subtract(PointC, PointA, out edgeCA);
        Vector3.Subtract(PointB, PointA, out edgeBA);

        Vector3.NormalizedCross(edgeCA, edgeBA, out normal);

        return normal;
      }
    }

    /// <summary>
    /// Gets the plane that the triangle resides on.
    /// </summary>
    public readonly Plane Plane
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        Vector3 normal;
        Vector3 edgeCA, edgeBA;
        Vector3.Subtract(PointC, PointA, out edgeCA);
        Vector3.Subtract(PointB, PointA, out edgeBA);

        Vector3.NormalizedCross(edgeCA, edgeBA, out normal);

        Vector3 center;
        Vector3.Add(PointA, PointB, out center);
        Vector3.Add(PointC, center, out center);
        Vector3.Multiply(center, MathHelper.OneThird, out center);

        return new Plane(normal, center);
      }
    }

    /// <summary>
    /// Gets the center of the triangle.
    /// </summary>
    public readonly Vector3 Center
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        Vector3 center;
        Vector3.Add(PointA, PointB, out center);
        Vector3.Add(PointC, center, out center);
        Vector3.Multiply(center, MathHelper.OneThird, out center);

        return center;
      }
    }

    /// <summary>
    /// Gets the edge representing B - A.
    /// </summary>
    public readonly Vector3 EdgeBA
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        Vector3 edgeBA;
        Vector3.Subtract(PointB, PointA, out edgeBA);

        return edgeBA;
      }
    }

    /// <summary>
    /// Gets the edge representing C- A.
    /// </summary>
    public readonly Vector3 EdgeCA
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        Vector3 edgeCA;
        Vector3.Subtract(PointC, PointA, out edgeCA);

        return edgeCA;
      }
    }

    /// <summary>
    /// Gets the edge representing B - C.
    /// </summary>
    public readonly Vector3 EdgeBC
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        Vector3 edgeBC;
        Vector3.Subtract(PointB, PointC, out edgeBC);

        return edgeBC;
      }
    }

    /// <summary>
    /// Gets the area of the triangle.
    /// </summary>
    public readonly float Area
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        float area = 0.5f * ((PointA.X * (PointB.Y - PointC.Y)) + (PointB.X * (PointC.Y - PointA.Y)) + (PointC.X * (PointA.Y - PointB.Y)));
        return MathF.Abs(area);
      }
    }

    /// <summary>
    /// Gets if the triangle is degenerate (normal is zero).
    /// </summary>
    public readonly bool IsDegenerate
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return Normal.IsAlmostZero();
      }
    }

    /// <summary>
    /// Gets whether any of the components of the triangle are NaN (Not A Number).
    /// </summary>
    public readonly bool IsNaN
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return PointA.IsNaN || PointB.IsNaN || PointC.IsNaN;
      }
    }

    /// <summary>
    /// Gets whether any of the components of the triangle are positive or negative infinity.
    /// </summary>
    public readonly bool IsInfinity
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return PointA.IsInfinity || PointB.IsInfinity || PointC.IsInfinity;
      }
    }

    /// <summary>
    /// Gets or sets triangle vertices by a zero-based index, in the order of {A, B, C} where index 0 is Point A.
    /// </summary>
    /// <param name="index">Zero based index, in range of [0, 2].</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the index is out of range.</exception>
    public Vector3 this[int index]
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      readonly get
      {
        switch (index)
        {
          case 0:
            return PointA;
          case 1:
            return PointB;
          case 2:
            return PointC;
          default:
            throw new ArgumentOutOfRangeException(nameof(index), StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));
        }
      }
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      set
      {
        switch (index)
        {
          case 0:
            PointA = value;
            break;
          case 1:
            PointB = value;
            break;
          case 2:
            PointC = value;
            break;
          default:
            throw new ArgumentOutOfRangeException(nameof(index), StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));
        }
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Triangle"/> struct. The vertices are
    /// considered to be in clockwise order of {A, B, C}.
    /// </summary>
    /// <param name="pointA">First vertex</param>
    /// <param name="pointB">Second vertex</param>
    /// <param name="pointC">Third vertex</param>
    public Triangle(in Vector3 pointA, in Vector3 pointB, in Vector3 pointC)
    {
      PointA = pointA;
      PointB = pointB;
      PointC = pointC;
    }

    #region Swap Vertex winding

    /// <summary>
    /// Swaps the vertex winding of the triangle from clockwise {A, B, C} to counter-clockwise {A, C, B}. Effectively,
    /// this swaps vertices B and C, and negates the triangle's normal.
    /// </summary>
    /// <param name="triangle">Triangle to swap vertex order.</param>
    /// <returns>Triangle with swapped vertex order.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Triangle SwapVertexWinding(in Triangle triangle)
    {
      Triangle result = triangle;
      BufferHelper.Swap(ref result.PointB, ref result.PointC);

      return result;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="triangle">Triangle to swap vertex order.</param>
    /// <param name="result">Triangle with swapped vertex order.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void SwapVertexWinding(in Triangle triangle, out Triangle result)
    {
      result = triangle;
      BufferHelper.Swap(ref result.PointB, ref result.PointC);
    }

    #endregion

    #region Transform

    /// <summary>
    /// Transforms the triangle by the given <see cref="Quaternion"/>.
    /// </summary>
    /// <param name="triangle">Triangle to transform.</param>
    /// <param name="rotation">Quaternion rotation.</param>
    /// <returns>Transformed triangle.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Triangle Transform(in Triangle triangle, in Quaternion rotation)
    {
      Triangle result;
      Vector3.Transform(triangle.PointA, rotation, out result.PointA);
      Vector3.Transform(triangle.PointB, rotation, out result.PointB);
      Vector3.Transform(triangle.PointC, rotation, out result.PointC);

      return result;
    }

    /// <summary>
    /// Transforms the triangle by the given <see cref="Quaternion"/>.
    /// </summary>
    /// <param name="triangle">Triangle to transform.</param>
    /// <param name="rotation">Quaternion rotation.</param>
    /// <param name="result">Transformed triangle.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Transform(in Triangle triangle, in Quaternion rotation, out Triangle result)
    {
      Vector3.Transform(triangle.PointA, rotation, out result.PointA);
      Vector3.Transform(triangle.PointB, rotation, out result.PointB);
      Vector3.Transform(triangle.PointC, rotation, out result.PointC);
    }

    /// <summary>
    /// Transforms the triangle by the given <see cref="Matrix"/>.
    /// </summary>
    /// <param name="triangle">Triangle to transform.</param>
    /// <param name="transform">Transformation matrix.</param>
    /// <returns>Transformed triangle.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Triangle Transform(in Triangle triangle, in Matrix transform)
    {
      Triangle result;
      Vector3.Transform(triangle.PointA, transform, out result.PointA);
      Vector3.Transform(triangle.PointB, transform, out result.PointB);
      Vector3.Transform(triangle.PointC, transform, out result.PointC);

      return result;
    }

    /// <summary>
    /// Transforms the triangle by the given <see cref="Matrix"/>.
    /// </summary>
    /// <param name="triangle">Triangle to transform.</param>
    /// <param name="transform">Transformation matrix.</param>
    /// <param name="result">Transformed triangle.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Transform(in Triangle triangle, in Matrix transform, out Triangle result)
    {
      Vector3.Transform(triangle.PointA, transform, out result.PointA);
      Vector3.Transform(triangle.PointB, transform, out result.PointB);
      Vector3.Transform(triangle.PointC, transform, out result.PointC);
    }

    /// <summary>
    /// Translates the triangle by the given translation vector.
    /// </summary>
    /// <param name="triangle">Triangle to transform.</param>
    /// <param name="translation">Translation vector.</param>
    /// <returns>Transformed triangle.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Triangle Transform(in Triangle triangle, in Vector3 translation)
    {
      Triangle result;
      Vector3.Add(triangle.PointA, translation, out result.PointA);
      Vector3.Add(triangle.PointB, translation, out result.PointB);
      Vector3.Add(triangle.PointC, translation, out result.PointC);

      return result;
    }

    /// <summary>
    /// Translates the triangle by the given translation vector.
    /// </summary>
    /// <param name="triangle">Triangle to transform.</param>
    /// <param name="translation">Translation vector.</param>
    /// <param name="result">Transformed triangle.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Transform(in Triangle triangle, in Vector3 translation, out Triangle result)
    {
      Vector3.Add(triangle.PointA, translation, out result.PointA);
      Vector3.Add(triangle.PointB, translation, out result.PointB);
      Vector3.Add(triangle.PointC, translation, out result.PointC);
    }

    /// <summary>
    /// Scales the triangle by the given scaling factor, which is uniform along the X, Y, and Z axes.
    /// </summary>
    /// <param name="triangle">Triangle to transform.</param>
    /// <param name="scale">Scaling factor.</param>
    /// <returns>Transformed triangle.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Triangle Transform(in Triangle triangle, float scale)
    {
      Triangle result;
      Vector3.Multiply(triangle.PointA, scale, out result.PointA);
      Vector3.Multiply(triangle.PointB, scale, out result.PointB);
      Vector3.Multiply(triangle.PointC, scale, out result.PointC);

      return result;
    }

    /// <summary>
    /// Scales the triangle by the given scaling factor, which is uniform along the X, Y, and Z axes.
    /// </summary>
    /// <param name="triangle">Triangle to transform.</param>
    /// <param name="scale">Scaling factor.</param>
    /// <param name="result">Transformed triangle.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Transform(in Triangle triangle, float scale, out Triangle result)
    {
      Vector3.Multiply(triangle.PointA, scale, out result.PointA);
      Vector3.Multiply(triangle.PointB, scale, out result.PointB);
      Vector3.Multiply(triangle.PointC, scale, out result.PointC);
    }

    /// <summary>
    /// Scales the triangle by the given scaling factors along the X, Y, and Z axes.
    /// </summary>
    /// <param name="triangle">Triangle to transform.</param>
    /// <param name="xScale">Scaling factor along the X axis.</param>
    /// <param name="yScale">Scaling factor along the Y axis.</param>
    /// <param name="zScale">Scaling factor along the Z axis.</param>
    /// <returns>Transformed triangle.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Triangle Transform(in Triangle triangle, float xScale, float yScale, float zScale)
    {
      Vector3 scale = new Vector3(xScale, yScale, zScale);

      Triangle result;
      Vector3.Multiply(triangle.PointA, scale, out result.PointA);
      Vector3.Multiply(triangle.PointB, scale, out result.PointB);
      Vector3.Multiply(triangle.PointC, scale, out result.PointC);

      return result;
    }

    /// <summary>
    /// Scales the triangle by the given scaling factors along the X, Y, and Z axes.
    /// </summary>
    /// <param name="triangle">Triangle to transform.</param>
    /// <param name="xScale">Scaling factor along the X axis.</param>
    /// <param name="yScale">Scaling factor along the Y axis.</param>
    /// <param name="zScale">Scaling factor along the Z axis.</param>
    /// <param name="result">Transformed triangle.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Transform(in Triangle triangle, float xScale, float yScale, float zScale, out Triangle result)
    {
      Vector3 scale = new Vector3(xScale, yScale, zScale);

      Vector3.Multiply(triangle.PointA, scale, out result.PointA);
      Vector3.Multiply(triangle.PointB, scale, out result.PointB);
      Vector3.Multiply(triangle.PointC, scale, out result.PointC);
    }

    #endregion

    #region Equality Operators

    /// <summary>
    /// Tests equality between two triangles.
    /// </summary>
    /// <param name="a">First triangle</param>
    /// <param name="b">Second triangle</param>
    /// <returns>True if the points of the two triangles are equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator ==(in Triangle a, in Triangle b)
    {
      return a.PointA.Equals(b.PointA) && a.PointB.Equals(b.PointB) && a.PointC.Equals(b.PointC);
    }

    /// <summary>
    /// Tests inequality between two triangles.
    /// </summary>
    /// <param name="a">First triangle</param>
    /// <param name="b">Second triangle</param>
    /// <returns>True if the points of the two triangles are equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator !=(in Triangle a, in Triangle b)
    {
      return !a.PointA.Equals(b.PointA) || !a.PointB.Equals(b.PointB) || !a.PointC.Equals(b.PointC);
    }

    #endregion

    #region Public Methods

    /// <summary>
    /// Swaps the vertex winding of the triangle from clockwise {A, B, C} to counter-clockwise {A, C, B}. Effectively,
    /// this swaps vertices B and C, and negates the triangle's normal.
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void SwapVertexWinding()
    {
      BufferHelper.Swap(ref PointB, ref PointC);
    }

    /// <summary>
    /// Tests if the point in space is on the triangle.
    /// </summary>
    /// <param name="pt">Point on triangle</param>
    /// <returns>True if the point is in fact on the triangle, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool IsPointOn(in Vector3 pt)
    {
      if (IsDegenerate)
        return false;

      Vector3 v0, v1, v2;

      Vector3.Subtract(PointC, PointA, out v0);
      Vector3.Subtract(PointB, PointA, out v1);
      Vector3.Subtract(pt, PointA, out v2);

      float dot00 = Vector3.Dot(v0, v0);
      float dot01 = Vector3.Dot(v0, v1);
      float dot02 = Vector3.Dot(v0, v2);
      float dot11 = Vector3.Dot(v1, v1);
      float dot12 = Vector3.Dot(v1, v2);

      float invDenom = 1.0f / (dot00 * dot11 - dot01 * dot01);
      float s = (dot11 * dot02 - dot01 * dot12) * invDenom;
      float t = (dot00 * dot12 - dot01 * dot02) * invDenom;

      return (s >= 0.0f) && (t >= 0.0f) && (s + t <= 1.0f);
    }

    #endregion

    #region Intersects

    /// <summary>
    /// Determines whether there is an intersection between this object and a <see cref="Ray"/>.
    /// </summary>
    /// <param name="ray">Ray to test.</param>
    /// <param name="ignoreBackface">True if the test should ignore an intersection if the triangle is a back face, false if otherwise.</param>
    /// <returns>True if an intersection between the two objects occurs, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Intersects(in Ray ray, bool ignoreBackface = false)
    {
      return GeometricToolsHelper.IntersectRayTriangle(this, ray, ignoreBackface, out _);
    }

    /// <summary>
    /// Determines whether there is an intersection between this object and a <see cref="Ray"/>.
    /// </summary>
    /// <param name="ray">Ray to test.</param>
    /// <param name="result">Intersection result between the two objects.</param>
    /// <param name="ignoreBackface">True if the test should ignore an intersection if the triangle is a back face, false if otherwise.</param>
    /// <returns>True if an intersection between the two objects occurs, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Intersects(in Ray ray, out LineIntersectionResult result, bool ignoreBackface = false)
    {
      return GeometricToolsHelper.IntersectRayTriangle(this, ray, ignoreBackface, out result);
    }

    /// <summary>
    /// Determines whether there is an intersection between this object and a <see cref="Segment"/>.
    /// </summary>
    /// <param name="segment">Segment to test.</param>
    /// <param name="ignoreBackface">True if the test should ignore an intersection if the triangle is a back face, false if otherwise.</param>
    /// <returns>True if an intersection between the two objects occurs, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Intersects(in Segment segment, bool ignoreBackface = false)
    {
      return GeometricToolsHelper.IntersectSegmentTriangle(this, segment, ignoreBackface, out _);
    }

    /// <summary>
    /// Determines whether there is an intersection between this object and a <see cref="Segment"/>.
    /// </summary>
    /// <param name="segment">Segment to test.</param>
    /// <param name="result">Intersection result between the two objects.</param>
    /// <param name="ignoreBackface">True if the test should ignore an intersection if the triangle is a back face, false if otherwise.</param>
    /// <returns>True if an intersection between the two objects occurs, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Intersects(in Segment segment, out LineIntersectionResult result, bool ignoreBackface = false)
    {
      return GeometricToolsHelper.IntersectSegmentTriangle(this, segment, ignoreBackface, out result);
    }

    /// <summary>
    /// Determines whether there is an intersection between this object and a <see cref="Plane"/>.
    /// </summary>
    /// <param name="plane">Plane to test.</param>
    /// <returns>True if an intersection between the two objects occurs, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Intersects(in Plane plane)
    {
      return GeometricToolsHelper.IntersectPlaneTriangle(this, plane);
    }

    /// <summary>
    /// Determines whether there is an intersection between this object and a <see cref="Plane"/>.
    /// </summary>
    /// <param name="plane">Plane to test.</param>
    /// <param name="result">Intersection result between the two objects.</param>
    /// <returns>True if an intersection between the two objects occurs, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Intersects(in Plane plane, out Segment result)
    {
      return GeometricToolsHelper.IntersectPlaneTriangle(this, plane, out result);
    }

    /// <summary>
    /// Determines whether there is an intersection between this object and a <see cref="Triangle"/>.
    /// </summary>
    /// <param name="triangle">Triangle to test.</param>
    /// <returns>True if an intersection between the two objects occurs, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Intersects(in Triangle triangle)
    {
      return GeometricToolsHelper.IntersectTriangleTriangle(this, triangle);
    }

    /// <summary>
    /// Determines whether there is an intersection between this object and a <see cref="Triangle"/>.
    /// </summary>
    /// <param name="triangle">Triangle to test.</param>
    /// <param name="result">Intersection result between the two objects.</param>
    /// <returns>True if an intersection between the two objects occurs, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Intersects(in Triangle triangle, out Segment result)
    {
      return GeometricToolsHelper.IntersectTriangleTriangle(this, triangle, out result);
    }

    /// <summary>
    /// Determines whether there is an intersection between this object and an <see cref="Ellipse"/>.
    /// </summary>
    /// <param name="ellipse">Ellipse to test.</param>
    /// <returns>True if an intersection between the two objects occurs, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Intersects(in Ellipse ellipse)
    {
      //TODO
      throw new NotImplementedException();
    }

    /// <summary>
    /// Determines whether there is an intersection between this object and an <see cref="Ellipse"/>.
    /// </summary>
    /// <param name="ellipse">Ellipse to test.</param>
    /// <param name="result">Intersection result between the two objects.</param>
    /// <returns>True if an intersection between the two objects occurs, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Intersects(in Ellipse ellipse, out Segment result)
    {
      //TODO
      throw new NotImplementedException();
    }

    /// <summary>
    /// Determines whether there is an intersection between this object and a <see cref="BoundingVolume"/>.
    /// </summary>
    /// <param name="volume">Bounding volume to test.</param>
    /// <returns>True if an intersection between the two objects occurs, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Intersects([NotNullWhen(true)] BoundingVolume? volume)
    {
      if (volume == null)
        return false;

      return volume.Intersects(this);
    }

    #endregion

    #region Distance To

    /// <summary>
    /// Determines the distance between this object and a point.
    /// </summary>
    /// <param name="point">Point to test.</param>
    /// <returns>Distance between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float DistanceTo(in Vector3 point)
    {
      GeometricToolsHelper.DistancePointTriangle(this, point, out Vector3 ptOnTriangle, out float sqrDist);

      return MathF.Sqrt(sqrDist);
    }

    /// <summary>
    /// Determines the distance between this object and a <see cref="Ray"/>.
    /// </summary>
    /// <param name="ray">Ray to test.</param>
    /// <returns>Distance between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float DistanceTo(in Ray ray)
    {
      GeometricToolsHelper.DistanceRayTriangle(this, ray, out Vector3 ptOnTriangle, out Vector3 ptOnRay, out float sqrDist);

      return MathF.Sqrt(sqrDist);
    }

    /// <summary>
    /// Determines the distance between this object and a <see cref="Segment"/>.
    /// </summary>
    /// <param name="segment">Segment to test.</param>
    /// <returns>Distance between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float DistanceTo(in Segment segment)
    {
      GeometricToolsHelper.DistanceSegmentTriangle(this, segment, out Vector3 ptOnTriangle, out Vector3 ptOnSegment, out float sqrDist);

      return MathF.Sqrt(sqrDist);
    }

    /// <summary>
    /// Determines the distance between this object and a <see cref="Plane"/>.
    /// </summary>
    /// <param name="plane">Plane to test.</param>
    /// <returns>Distance between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float DistanceTo(in Plane plane)
    {
      GeometricToolsHelper.DistancePlaneTriangle(this, plane, out Vector3 ptOnTriangle, out Vector3 ptOnPlane, out float sqrDist);

      return MathF.Sqrt(sqrDist);
    }

    /// <summary>
    /// Determines the distance between this object and a <see cref="Triangle"/>.
    /// </summary>
    /// <param name="triangle">Triangle to test.</param>
    /// <returns>Distance between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float DistanceTo(in Triangle triangle)
    {
      GeometricToolsHelper.DistanceTriangleTriangle(this, triangle, out Vector3 ptOnFirst, out Vector3 ptOnSecond, out float sqrDist);

      return MathF.Sqrt(sqrDist);
    }

    /// <summary>
    /// Determines the distance between this object and an <see cref="Ellipse"/>.
    /// </summary>
    /// <param name="ellipse">Ellipse to test.</param>
    /// <returns>Distance between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float DistanceTo(in Ellipse ellipse)
    {
      //TODO
      throw new NotImplementedException();
    }

    #endregion

    #region Distance Squared To

    /// <summary>
    /// Determines the distance squared between this object and a point.
    /// </summary>
    /// <param name="point">Point to test.</param>
    /// <returns>Distance squared between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float DistanceSquaredTo(in Vector3 point)
    {
      GeometricToolsHelper.DistancePointTriangle(this, point, out Vector3 ptOnTriangle, out float sqrDist);

      return sqrDist;
    }

    /// <summary>
    /// Determines the distance squared between this object and a <see cref="Ray"/>.
    /// </summary>
    /// <param name="ray">Ray to test.</param>
    /// <returns>Distance squared between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float DistanceSquaredTo(in Ray ray)
    {
      GeometricToolsHelper.DistanceRayTriangle(this, ray, out Vector3 ptOnTriangle, out Vector3 ptOnRay, out float sqrDist);

      return sqrDist;
    }

    /// <summary>
    /// Determines the distance squared between this object and a <see cref="Segment"/>.
    /// </summary>
    /// <param name="segment">Segment to test.</param>
    /// <returns>Distance squared between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float DistanceSquaredTo(in Segment segment)
    {
      GeometricToolsHelper.DistanceSegmentTriangle(this, segment, out Vector3 ptOnTriangle, out Vector3 ptOnSegment, out float sqrDist);

      return sqrDist;
    }

    /// <summary>
    /// Determines the distance squared between this object and a <see cref="Plane"/>.
    /// </summary>
    /// <param name="plane">Plane to test.</param>
    /// <returns>Distance squared between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float DistanceSquaredTo(in Plane plane)
    {
      GeometricToolsHelper.DistancePlaneTriangle(this, plane, out Vector3 ptOnTriangle, out Vector3 ptOnPlane, out float sqrDist);

      return sqrDist;
    }

    /// <summary>
    /// Determines the distance squared between this object and a <see cref="Triangle"/>.
    /// </summary>
    /// <param name="triangle">Triangle to test.</param>
    /// <returns>Distance squared between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float DistanceSquaredTo(in Triangle triangle)
    {
      GeometricToolsHelper.DistanceTriangleTriangle(this, triangle, out Vector3 ptOnFirst, out Vector3 ptOnSecond, out float sqrDist);

      return sqrDist;
    }

    /// <summary>
    /// Determines the distance squared between this object and an <see cref="Ellipse"/>.
    /// </summary>
    /// <param name="ellipse">Ellipse to test.</param>
    /// <returns>Distance squared between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float DistanceSquaredTo(in Ellipse ellipse)
    {
      //TODO
      throw new NotImplementedException();
    }

    #endregion

    #region Closest Point / Approach Segment

    /// <summary>
    /// Determines the closest point on this object from the point.
    /// </summary>
    /// <param name="point">Point to test.</param>
    /// <returns>Closest point on this object.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly Vector3 ClosestPointTo(in Vector3 point)
    {
      GeometricToolsHelper.DistancePointTriangle(this, point, out Vector3 ptOnTriangle, out float sqrDist);

      return ptOnTriangle;
    }

    /// <summary>
    /// Determines the closest point on this object from the point.
    /// </summary>
    /// <param name="point">Point to test.</param>
    /// <param name="result">Closest point on this object.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly void ClosestPointTo(in Vector3 point, out Vector3 result)
    {
      GeometricToolsHelper.DistancePointTriangle(this, point, out result, out float sqrDist);
    }

    /// <summary>
    /// Determines the closest point on this object from the point.
    /// </summary>
    /// <param name="point">Point to test.</param>
    /// <param name="result">Closest point on this object.</param>
    /// <param name="squaredDistance">Squared distance between the two objects.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly void ClosestPointTo(in Vector3 point, out Vector3 result, out float squaredDistance)
    {
      GeometricToolsHelper.DistancePointTriangle(this, point, out result, out squaredDistance);
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Ray"/>.
    /// </summary>
    /// <param name="ray">Ray to test</param>
    /// <returns>Closest approach segment between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly Segment ClosestApproachSegment(in Ray ray)
    {
      Segment result;
      GeometricToolsHelper.DistanceRayTriangle(this, ray, out result.StartPoint, out result.EndPoint, out float sqrDist);

      return result;
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Ray"/>.
    /// </summary>
    /// <param name="ray">Ray to test</param>
    /// <param name="result">Closest approach segment between the two objects.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly void ClosestApproachSegment(in Ray ray, out Segment result)
    {
      GeometricToolsHelper.DistanceRayTriangle(this, ray, out result.StartPoint, out result.EndPoint, out float sqrDist);
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Ray"/>.
    /// </summary>
    /// <param name="ray">Ray to test</param>
    /// <param name="result">Closest approach segment between the two objects.</param>
    /// <param name="squaredDistance">Squared distance between the two objects.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly void ClosestApproachSegment(in Ray ray, out Segment result, out float squaredDistance)
    {
      GeometricToolsHelper.DistanceRayTriangle(this, ray, out result.StartPoint, out result.EndPoint, out squaredDistance);
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Segment"/>.
    /// </summary>
    /// <param name="segment">Segment to test</param>
    /// <returns>Closest approach segment between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly Segment ClosestApproachSegment(in Segment segment)
    {
      Segment result;
      GeometricToolsHelper.DistanceSegmentTriangle(this, segment, out result.StartPoint, out result.EndPoint, out float sqrDist);

      return result;
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Segment"/>.
    /// </summary>
    /// <param name="segment">Segment to test</param>
    /// <param name="result">Closest approach segment between the two objects.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly void ClosestApproachSegment(in Segment segment, out Segment result)
    {
      GeometricToolsHelper.DistanceSegmentTriangle(this, segment, out result.StartPoint, out result.EndPoint, out float sqrDist);
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Segment"/>.
    /// </summary>
    /// <param name="segment">Segment to test</param>
    /// <param name="result">Closest approach segment between the two objects.</param>
    /// <param name="squaredDistance">Squared distance between the two objects.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly void ClosestApproachSegment(in Segment segment, out Segment result, out float squaredDistance)
    {
      GeometricToolsHelper.DistanceSegmentTriangle(this, segment, out result.StartPoint, out result.EndPoint, out squaredDistance);
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Plane"/>.
    /// </summary>
    /// <param name="plane">Plane to test</param>
    /// <returns>Closest approach segment between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly Segment ClosestApproachSegment(in Plane plane)
    {
      Segment result;
      GeometricToolsHelper.DistancePlaneTriangle(this, plane, out result.StartPoint, out result.EndPoint, out float sqrDist);

      return result;
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Plane"/>.
    /// </summary>
    /// <param name="plane">Plane to test</param>
    /// <param name="result">Closest approach segment between the two objects.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly void ClosestApproachSegment(in Plane plane, out Segment result)
    {
      GeometricToolsHelper.DistancePlaneTriangle(this, plane, out result.StartPoint, out result.EndPoint, out float sqrDist);
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Plane"/>.
    /// </summary>
    /// <param name="plane">Plane to test</param>
    /// <param name="result">Closest approach segment between the two objects.</param>
    /// <param name="squaredDistance">Squared distance between the two objects.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly void ClosestApproachSegment(in Plane plane, out Segment result, out float squaredDistance)
    {
      GeometricToolsHelper.DistancePlaneTriangle(this, plane, out result.StartPoint, out result.EndPoint, out squaredDistance);
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Triangle"/>.
    /// </summary>
    /// <param name="triangle">Triangle to test</param>
    /// <returns>Closest approach segment between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly Segment ClosestApproachSegment(in Triangle triangle)
    {
      Segment result;
      GeometricToolsHelper.DistanceTriangleTriangle(this, triangle, out result.StartPoint, out result.EndPoint, out float sqrDist);

      return result;
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Triangle"/>.
    /// </summary>
    /// <param name="triangle">Triangle to test</param>
    /// <param name="result">Closest approach segment between the two objects.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly void ClosestApproachSegment(in Triangle triangle, out Segment result)
    {
      GeometricToolsHelper.DistanceTriangleTriangle(this, triangle, out result.StartPoint, out result.EndPoint, out float sqrDist);
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Triangle"/>.
    /// </summary>
    /// <param name="triangle">Triangle to test</param>
    /// <param name="result">Closest approach segment between the two objects.</param>
    /// <param name="squaredDistance">Squared distance between the two objects.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly void ClosestApproachSegment(in Triangle triangle, out Segment result, float squaredDistance)
    {
      GeometricToolsHelper.DistanceTriangleTriangle(this, triangle, out result.StartPoint, out result.EndPoint, out squaredDistance);
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Ellipse"/>.
    /// </summary>
    /// <param name="ellipse">Ellipse to test</param>
    /// <returns>Closest approach segment between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly Segment ClosestApproachSegment(in Ellipse ellipse)
    {
      //TODO
      throw new NotImplementedException();
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Ellipse"/>.
    /// </summary>
    /// <param name="ellipse">Ellipse to test</param>
    /// <param name="result">Closest approach segment between the two objects.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly void ClosestApproachSegment(in Ellipse ellipse, out Segment result)
    {
      //TODO
      throw new NotImplementedException();
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Ellipse"/>.
    /// </summary>
    /// <param name="ellipse">Ellipse to test</param>
    /// <param name="result">Closest approach segment between the two objects.</param>
    /// <param name="squaredDistance">Squared distance between the two objects.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly void ClosestApproachSegment(in Ellipse ellipse, out Segment result, out float squaredDistance)
    {
      //TODO
      throw new NotImplementedException();
    }

    #endregion

    #region Equality

    /// <summary>
    /// Indicates whether this instance and a specified object are equal.
    /// </summary>
    /// <param name="obj">Another object to compare to.</param>
    /// <returns>True if <paramref name="obj" /> and this instance are the same type and represent the same value; otherwise, false.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is Triangle)
        return Equals((Triangle) obj);

      return false;
    }

    /// <summary>
    /// Tests equality between the triangle and another triangle.
    /// </summary>
    /// <param name="other">Other triangle</param>
    /// <returns>True if the points of the two triangles are equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    readonly bool IEquatable<Triangle>.Equals(Triangle other)
    {
      return PointA.Equals(other.PointA) && PointB.Equals(other.PointB) && PointC.Equals(other.PointC);
    }

    /// <summary>
    /// Tests equality between the triangle and another triangle.
    /// </summary>
    /// <param name="other">Other triangle</param>
    /// <returns>True if the points of the two triangles are equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Equals(in Triangle other)
    {
      return PointA.Equals(other.PointA) && PointB.Equals(other.PointB) && PointC.Equals(other.PointC);
    }

    /// <summary>
    /// Tests equality between the triangle and another triangle within the specified tolerance.
    /// </summary>
    /// <param name="other">Other triangle</param>
    /// <param name="tolerance">Tolerance</param>
    /// <returns>True if the points of the two triangles are equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Equals(in Triangle other, float tolerance)
    {
      return PointA.Equals(other.PointA, tolerance) && PointB.Equals(other.PointB, tolerance) && PointC.Equals(other.PointC, tolerance);
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.</returns>
    public readonly override int GetHashCode()
    {
      unchecked
      {
        return PointA.GetHashCode() + PointB.GetHashCode() + PointC.GetHashCode();
      }
    }

    #endregion

    #region ToString

    /// <summary>
    /// Returns the fully qualified type name of this instance.
    /// </summary>
    /// <returns>A <see cref="T:System.String" /> containing a fully qualified type name.</returns>
    public readonly override string ToString()
    {
      CultureInfo info = CultureInfo.CurrentCulture;
      return String.Format(info, "A: ({0}), B: ({1}), C: ({2}), Normal: ({3})",
          new Object[] { PointA.ToString(info), PointB.ToString(info), PointC.ToString(info), Normal.ToString() });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="format">The format</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(string? format)
    {
      if (format is null)
        return ToString();

      CultureInfo info = CultureInfo.CurrentCulture;
      return String.Format(info, "A: ({0}), B: ({1}), C: ({2}), Normal: ({3})",
          new Object[] { PointA.ToString(format, info), PointB.ToString(format, info), PointC.ToString(format, info), Normal.ToString(format, info) });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="formatProvider">The format provider.</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(IFormatProvider? formatProvider)
    {
      if (formatProvider is null)
        return ToString();

      return String.Format(formatProvider, "A: ({0}), B: ({1}), C: ({2}), Normal: ({3})",
          new Object[] { PointA.ToString(formatProvider), PointB.ToString(formatProvider), PointC.ToString(formatProvider), Normal.ToString(formatProvider) });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="format">The format.</param>
    /// <param name="formatProvider">The format provider.</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(string? format, IFormatProvider? formatProvider)
    {
      if (formatProvider is null)
        return ToString();

      if (format is null)
        return ToString(formatProvider);

      return String.Format(formatProvider, "A: ({0}), B: ({1}), C: ({2}), Normal: ({3})",
          new Object[] { PointA.ToString(format, formatProvider), PointB.ToString(format, formatProvider), PointC.ToString(format, formatProvider),
                    Normal.ToString(format, formatProvider) });
    }

    #endregion

    #region IPrimitiveValue

    /// <summary>
    /// Writes the primitive data to the output.
    /// </summary>
    /// <param name="output">Primitive writer.</param>
    readonly void IPrimitiveValue.Write(IPrimitiveWriter output)
    {
      output.Write<Vector3>("PointA", PointA);
      output.Write<Vector3>("PointB", PointB);
      output.Write<Vector3>("PointC", PointC);
    }

    /// <summary>
    /// Reads the primitive data from the input.
    /// </summary>
    /// <param name="input">Primitive reader.</param>
    void IPrimitiveValue.Read(IPrimitiveReader input)
    {
      input.Read<Vector3>("PointA", out PointA);
      input.Read<Vector3>("PointB", out PointB);
      input.Read<Vector3>("PointC", out PointC);
    }

    #endregion
  }
}
