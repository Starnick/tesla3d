﻿/*
* Copyright (c) 2010-2024 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections;
using System.Collections.Generic;

namespace Tesla
{
  /// <summary>
  /// Helper for clipping polygons.
  /// </summary>
  public static class PolygonHelper
  {
    public static ContainmentType Clip3DPolygon(IReadOnlyRefList<Vector3> subjectPoly, in Plane plane, RefList<Vector3> outputPoly, RefList<float> tempList = null)
    {
      if (subjectPoly.Count < 3)
        return ContainmentType.Outside;

      if (tempList is null)
        tempList = new RefList<float>();
      else
        tempList.Clear();

      outputPoly.Clear();

      float tolerance = MathHelper.ZeroTolerance;

      //First check if all points are on one side of the plane or not
      bool allIn = true;
      bool allOut = true;

      int numVertices = subjectPoly.Count;
      if (tempList.Capacity < numVertices)
        tempList.Capacity = numVertices;

      for (int i = 0; i < numVertices; i++)
      {
        ref readonly Vector3 pt = ref subjectPoly[i];
        float dist = plane.SignedDistanceTo(pt);

        tempList.Add(dist);

        if (dist < -tolerance)
          allIn = false;

        if (dist >= tolerance)
          allOut = false;
      }

      //Check if can early out
      if (allIn)
      {
        outputPoly.AddRange(subjectPoly);
        return ContainmentType.Inside;
      }

      if (allOut)
        return ContainmentType.Outside;

      //Clip polygon against the plane
      bool wasClipped = false;

      Vector3 v1 = subjectPoly[0];
      float dist1 = tempList[0];
      float dist2;
      bool isInside = dist1 >= 0.0;

      for (int i = 1; i <= numVertices; i++)
      {
        //Look at edge defined by v1-v2
        int index = i % numVertices;
        ref readonly Vector3 v2 = ref subjectPoly[index];
        dist2 = tempList[index];

        //Both in
        if (isInside && (dist2 >= 0.0f))
        {
          outputPoly.Add(v2);
        }
        //Coming in
        else if (!isInside && (dist2 >= tolerance))
        {
          wasClipped = true;
          isInside = true;

          //Interpolate
          Vector3 v;
          float d = dist1 / (dist1 - dist2);

          //Numerical robustness: If d == 1, then v == v2
          if (!MathHelper.IsNearlyOne(d))
          {
            Vector3.Lerp(v1, v2, d, out v);

            outputPoly.Add(v);
          }

          outputPoly.Add(v2);
        }
        //Going out
        else if (isInside && (dist2 < -tolerance))
        {
          wasClipped = true;
          isInside = false;

          //Interpolate
          Vector3 v;
          float d = dist1 / (dist1 - dist2);

          //Numerical robustness: If d == 0, then v == v1. If this segment is going out
          //we should have already added v1 since it should have been detected as being inside.
          if (!MathHelper.IsNearlyZero(d))
          {
            Vector3.Lerp(v1, v2, d, out v);

            outputPoly.Add(v);
          }

        }
        //Both outside
        else
        {
          wasClipped = true;
        }

        v1 = v2;
        dist1 = dist2;
      }

      //Return clip results
      if (!wasClipped)
      {
        outputPoly.AddRange(subjectPoly);
        return ContainmentType.Inside;
      }

      if (outputPoly.Count < 3)
      {
        outputPoly.Clear();
        return ContainmentType.Outside;
      }

      return ContainmentType.Intersects;
    }

    /// <summary>
    /// Clips the subject 2D polygon with another on the XY plane. Any winding order is accepted, the polygons are assumed to be connected
    /// without having the first and last points be equal.
    /// </summary>
    /// <param name="subjectPoly">Polygon to be clipped.</param>
    /// <param name="clipPoly">Polygon used to clip.</param>
    /// <param name="outputPoly">Resulting polygon.</param>
    /// <param name="tempList">Optional temporary list to use to avoid memory allocation, may be null.</param>
    /// <returns>True if the polygon was clipped successfully, false if they do not intersect.</returns>
    public static bool Clip2DPolygonXY(IReadOnlyRefList<Vector3> subjectPoly, IReadOnlyRefList<Vector3> clipPoly, RefList<Vector3> outputPoly, RefList<Vector3> tempList = null)
    {
      return Clip2DPolygon(subjectPoly, clipPoly, outputPoly, tempList, CoordinatePair.XY);
    }

    /// <summary>
    /// Clips the subject 2D polygon with another on the XZ plane. Any winding order is accepted, the polygons are assumed to be connected
    /// without having the first and last points be equal.
    /// </summary>
    /// <param name="subjectPoly">Polygon to be clipped.</param>
    /// <param name="clipPoly">Polygon used to clip.</param>
    /// <param name="outputPoly">Resulting polygon.</param>
    /// <param name="tempList">Optional temporary list to use to avoid memory allocation, may be null.</param>
    /// <returns>True if the polygon was clipped successfully, false if they do not intersect.</returns>
    public static bool Clip2DPolygonXZ(IReadOnlyRefList<Vector3> subjectPoly, IReadOnlyRefList<Vector3> clipPoly, RefList<Vector3> outputPoly, RefList<Vector3> tempList = null)
    {
      return Clip2DPolygon(subjectPoly, clipPoly, outputPoly, tempList, CoordinatePair.XZ);
    }

    /// <summary>
    /// Clips the subject 2D polygon with another on the ZY plane. Any winding order is accepted, the polygons are assumed to be connected
    /// without having the first and last points be equal.
    /// </summary>
    /// <param name="subjectPoly">Polygon to be clipped.</param>
    /// <param name="clipPoly">Polygon used to clip.</param>
    /// <param name="outputPoly">Resulting polygon.</param>
    /// <param name="tempList">Optional temporary list to use to avoid memory allocation, may be null.</param>
    /// <returns>True if the polygon was clipped successfully, false if they do not intersect.</returns>
    public static bool Clip2DPolygonZY(IReadOnlyRefList<Vector3> subjectPoly, IReadOnlyRefList<Vector3> clipPoly, RefList<Vector3> outputPoly, RefList<Vector3> tempList = null)
    {
      return Clip2DPolygon(subjectPoly, clipPoly, outputPoly, tempList, CoordinatePair.ZY);
    }

    //SutherlandHodgman clipping algorithm
    private static bool Clip2DPolygon(IReadOnlyRefList<Vector3> subjectPoly, IReadOnlyRefList<Vector3> clipPoly, RefList<Vector3> outputPoly, RefList<Vector3> tempList = null, CoordinatePair pair = CoordinatePair.XY)
    {
      if (subjectPoly.Count < 3 || clipPoly.Count < 3)
        return false;

      if (tempList is null)
        tempList = new RefList<Vector3>();
      else
        tempList.Clear();

      outputPoly.Clear();
      outputPoly.AddRange(subjectPoly);

      if (!IsClockwise(subjectPoly, pair))
        outputPoly.Reverse();

      SegmentEnumerator segEnumerator = new SegmentEnumerator(clipPoly, pair, IsClockwise(clipPoly, pair));

      while (segEnumerator.MoveNext())
      {
        Segment clipEdge;
        segEnumerator.GetCurrent(out clipEdge);

        tempList.Clear();
        tempList.AddRange(outputPoly);
        outputPoly.Clear();

        if (tempList.Count == 0)
          break;

        ref readonly Vector3 sPrime = ref tempList[tempList.Count - 1];
        Vector3 S;
        GetCoordinatePair(pair, sPrime, out S);

        for (int j = 0; j < tempList.Count; j++)
        {
          ref readonly Vector3 ePrime = ref tempList[j];
          Vector3 E;
          GetCoordinatePair(pair, ePrime, out E);

          if (IsInside(clipEdge, E))
          {
            if (!IsInside(clipEdge, S))
            {
              Segment polyEdge = new Segment(S, E);

              Vector3? interPt;
              LineIntersect(polyEdge, clipEdge, out interPt);

              if (interPt is not null)
              {
                Vector3 iPt = interPt.Value;
                ConvertCoordinatePair(pair, iPt, ePrime, out iPt);
                outputPoly.Add(iPt);
              }
              else
              {
                //Sanity check
                System.Diagnostics.Debug.Assert(false, "Line segments should have intersected!");
              }
            }

            outputPoly.Add(ePrime);
          }
          else if (IsInside(clipEdge, S))
          {
            Segment polyEdge;
            polyEdge.StartPoint = S;
            polyEdge.EndPoint = E;

            Vector3? interPt;
            LineIntersect(polyEdge, clipEdge, out interPt);

            if (interPt is not null)
            {
              Vector3 iPt = interPt.Value;
              ConvertCoordinatePair(pair, iPt, ePrime, out iPt);
              outputPoly.Add(iPt);
            }
            else
            {
              //Sanity check
              System.Diagnostics.Debug.Assert(false, "Line segments should have intersected!");
            }
          }

          S = E;
        }
      }

      return outputPoly.Count > 0;
    }

    /// <summary>
    /// Clips the subject 2D polygon with another. Any winding order is accepted, the polygons are assumed to be connected
    /// without having the first and last points be equal.
    /// </summary>
    /// <param name="subjectPoly">Polygon to be clipped.</param>
    /// <param name="clipPoly">Polygon used to clip.</param>
    /// <param name="outputPoly">Resulting polygon.</param>
    /// <param name="tempList">Optional temporary list to use to avoid memory allocation, may be null.</param>
    /// <returns>True if the polygon was clipped successfully, false if they do not intersect.</returns>
    public static bool Clip2DPolygon(IReadOnlyRefList<Vector2> subjectPoly, IReadOnlyRefList<Vector2> clipPoly, RefList<Vector2> outputPoly, RefList<Vector2> tempList = null)
    {
      if (subjectPoly.Count < 3 || clipPoly.Count < 3)
        return false;

      if (tempList is null)
        tempList = new RefList<Vector2>();
      else
        tempList.Clear();

      outputPoly.Clear();
      outputPoly.AddRange(subjectPoly);

      if (!IsClockwise(subjectPoly))
        outputPoly.Reverse();

      Segment2DEnumerator segEnumerator = new Segment2DEnumerator(clipPoly, IsClockwise(clipPoly));

      while (segEnumerator.MoveNext())
      {
        Segment clipEdge;
        segEnumerator.GetCurrent(out clipEdge);

        tempList.Clear();
        tempList.AddRange(outputPoly);
        outputPoly.Clear();

        if (tempList.Count == 0)
          break;

        Vector2 S = tempList[tempList.Count - 1];

        for (int j = 0; j < tempList.Count; j++)
        {
          ref readonly Vector2 E = ref tempList[j];

          if (IsInside(clipEdge, E))
          {
            if (!IsInside(clipEdge, S))
            {
              Segment polyEdge = new Segment(new Vector3(S, 0.0f), new Vector3(E, 0.0f));

              Vector3? interPt;
              LineIntersect(polyEdge, clipEdge, out interPt);

              if (interPt is not null)
              {
                Vector3 iPt = interPt.Value;
                outputPoly.Add(new Vector2(iPt.X, iPt.Y));
              }
              else
              {
                //Sanity check
                System.Diagnostics.Debug.Assert(false, "Line segments should have intersected!");
              }
            }

            outputPoly.Add(E);
          }
          else if (IsInside(clipEdge, S))
          {
            Segment polyEdge;
            polyEdge.StartPoint = new Vector3(S, 0.0f);
            polyEdge.EndPoint = new Vector3(E, 0.0f);

            Vector3? interPt;
            LineIntersect(polyEdge, clipEdge, out interPt);

            if (interPt is not null)
            {
              Vector3 iPt = interPt.Value;
              outputPoly.Add(new Vector2(iPt.X, iPt.Y));
            }
            else
            {
              //Sanity check
              System.Diagnostics.Debug.Assert(false, "Line segments should have intersected!");
            }
          }

          S = E;
        }
      }

      return outputPoly.Count > 0;
    }

    private static void LineIntersect(in Segment line1, in Segment line2, out Vector3? result)
    {
      Vector3 direction1 = line1.EndPoint - line1.StartPoint;
      Vector3 direction2 = line2.EndPoint - line2.StartPoint;
      float dotPerp = (direction1.X * direction2.Y) - (direction1.Y * direction2.X);

      // If it's 0, it means the lines are parallel so have infinite intersection points
      if (MathHelper.IsNearlyZero(dotPerp))
      {
        result = null;
        return;
      }

      Vector3 c = line2.StartPoint - line1.StartPoint;
      float t = (c.X * direction2.Y - c.Y * direction2.X) / dotPerp;

      //	Return the intersection point on the infinite line
      result = line1.StartPoint + (t * direction1);
    }

    private static bool IsInside(in Segment edge, in Vector2 pt)
    {
      bool? isLeft = IsLeftOf(edge, pt);

      //Collinear considered to be inside
      if (isLeft is null)
        return true;

      return !isLeft.Value;
    }

    private static bool IsInside(in Segment edge, in Vector3 pt)
    {
      bool? isLeft = IsLeftOf(edge, pt);

      //Collinear considered to be inside
      if (isLeft is null)
        return true;

      return !isLeft.Value;
    }

    private static bool? IsLeftOf(in Segment edge, in Vector2 pt)
    {
      Vector3 temp1 = edge.EndPoint - edge.StartPoint;
      Vector3 temp2 = new Vector3(pt, 0.0f) - edge.EndPoint;

      float side = (temp1.X * temp2.Y) - (temp1.Y * temp2.X); //dot product of perpendicular

      if (side < 0)
        return false;
      else if (side > 0)
        return true;

      //colinear point
      return null;
    }

    private static bool? IsLeftOf(in Segment edge, in Vector3 pt)
    {
      Vector3 temp1, temp2;
      Vector3.Subtract(edge.EndPoint, edge.StartPoint, out temp1);
      Vector3.Subtract(pt, edge.EndPoint, out temp2);

      float side = (temp1.X * temp2.Y) - (temp1.Y * temp2.X); //dot product of perpendicular

      if (side < 0)
        return false;
      else if (side > 0)
        return true;

      //colinear point
      return null;
    }

    private static void ConvertCoordinatePair(CoordinatePair pair, in Vector3 v, in Vector3 reference, out Vector3 result)
    {
      switch (pair)
      {
        case CoordinatePair.XZ:
          result = new Vector3(v.X, reference.Y, v.Y);
          break;
        case CoordinatePair.ZY:
          result = new Vector3(reference.X, v.Y, v.X);
          break;
        case CoordinatePair.XY:
        default:
          result = new Vector3(v.X, v.Y, reference.Z);
          break;
      }
    }

    private static void GetCoordinatePair(CoordinatePair pair, in Vector3 v, out Vector3 result)
    {
      switch (pair)
      {
        case CoordinatePair.XZ:
          result = new Vector3(v.X, v.Z, 0.0f);
          break;
        case CoordinatePair.ZY:
          result = new Vector3(v.Z, v.Y, 0.0f);
          break;
        case CoordinatePair.XY:
        default:
          result = new Vector3(v.X, v.Y, 0.0f);
          break;
      }
    }

    private static void GetCoordinatePair(CoordinatePair pair, in Vector3 v, out Vector2 result)
    {
      switch (pair)
      {
        case CoordinatePair.XZ:
          result = new Vector2(v.X, v.Z);
          break;
        case CoordinatePair.ZY:
          result = new Vector2(v.Z, v.Y);
          break;
        case CoordinatePair.XY:
        default:
          result = new Vector2(v.X, v.Y);
          break;
      }
    }

    private static bool IsClockwise(IReadOnlyList<Vector3> polygon, CoordinatePair coordinates)
    {
      Vector3 pt0 = polygon[0];
      Vector3 pt1 = polygon[1];

      Segment edge;
      GetCoordinatePair(coordinates, pt0, out edge.StartPoint);
      GetCoordinatePair(coordinates, pt1, out edge.EndPoint);

      for (int i = 2; i < polygon.Count; i++)
      {
        Vector3 pt = polygon[i];
        GetCoordinatePair(coordinates, pt, out pt);

        bool? isLeft = IsLeftOf(edge, pt);
        if (isLeft is not null)
          return !isLeft.Value;
      }

      throw new ArgumentException("All the points in the polygon are colinear");
    }

    private static bool IsClockwise(IReadOnlyList<Vector2> polygon)
    {
      Segment edge = new Segment(new Vector3(polygon[0], 0.0f), new Vector3(polygon[1], 0.0f));

      for (int i = 2; i < polygon.Count; i++)
      {
        Vector2 pt = polygon[i];

        bool? isLeft = IsLeftOf(edge, pt);
        if (isLeft is not null)
          return !isLeft.Value;
      }

      throw new ArgumentException("All the points in the polygon are colinear");
    }

    private enum CoordinatePair
    {
      XY = 0,
      XZ = 1,
      ZY = 2
    }

    #region 3D segment enumerator

    private struct SegmentEnumerator : IEnumerator<Segment>
    {
      private IReadOnlyList<Vector3> m_pts;
      private CoordinatePair m_coordinatePair;
      private bool m_clockWise;
      private int m_currIndex;
      private bool m_inreset;

      public Segment Current
      {
        get
        {
          Segment curr;
          GetCurrent(out curr);

          return curr;
        }
      }

      object IEnumerator.Current
      {
        get
        {
          return Current;
        }
      }

      public SegmentEnumerator(IReadOnlyList<Vector3> pts, CoordinatePair coordinatePair, bool isClockwise)
      {
        m_pts = pts;
        m_coordinatePair = coordinatePair;
        m_clockWise = isClockwise;
        m_currIndex = (m_clockWise) ? -1 : m_pts.Count;
        m_inreset = true;
      }

      public void GetCurrent(out Segment current)
      {
        if (m_inreset)
        {
          current = new Segment();
          return;
        }

        int lastIndex = (m_clockWise) ? m_pts.Count - 1 : 0;
        Vector3 pt0, pt1, p0, p1;
        if (m_currIndex == lastIndex)
        {
          if (m_clockWise)
          {
            p0 = m_pts[lastIndex];
            p1 = m_pts[0];
          }
          else
          {
            p0 = m_pts[lastIndex];
            p1 = m_pts[m_pts.Count - 1];
          }
        }
        else
        {
          int delta = (m_clockWise) ? 1 : -1;
          p0 = m_pts[m_currIndex];
          p1 = m_pts[m_currIndex + delta];
        }

        GetCoordinatePair(m_coordinatePair, p0, out pt0);
        GetCoordinatePair(m_coordinatePair, p1, out pt1);

        current = new Segment(pt0, pt1);
      }

      public void Dispose()
      {
      }

      public bool MoveNext()
      {
        if (m_clockWise)
        {
          if (m_currIndex < m_pts.Count)
          {
            m_inreset = false;
            m_currIndex++;
            return true;
          }
          else
          {
            m_currIndex = m_pts.Count - 1;
          }
        }
        else
        {
          if (m_currIndex > 0)
          {
            m_inreset = false;
            m_currIndex--;
            return true;
          }
          else
          {
            m_currIndex = 0;
          }
        }

        return false;
      }

      public void Reset()
      {
        m_currIndex = (m_clockWise) ? -1 : m_pts.Count;
        m_inreset = true;
      }
    }

    #endregion

    #region 2D segment enumerator

    private struct Segment2DEnumerator : IEnumerator<Segment>
    {
      private IReadOnlyList<Vector2> m_pts;
      private bool m_clockWise;
      private int m_currIndex;
      private bool m_inreset;

      public Segment Current
      {
        get
        {
          Segment current;
          GetCurrent(out current);

          return current;
        }
      }

      object IEnumerator.Current
      {
        get
        {
          return Current;
        }
      }

      public Segment2DEnumerator(IReadOnlyList<Vector2> pts, bool isClockwise)
      {
        m_pts = pts;
        m_clockWise = isClockwise;
        m_currIndex = (m_clockWise) ? -1 : m_pts.Count;
        m_inreset = true;
      }

      public void GetCurrent(out Segment current)
      {
        if (m_inreset)
        {
          current = new Segment();
          return;
        }

        int lastIndex = (m_clockWise) ? m_pts.Count - 1 : 0;
        Vector3 pt0, pt1;
        if (m_currIndex == lastIndex)
        {
          if (m_clockWise)
          {
            pt0 = new Vector3(m_pts[lastIndex], 0.0f);
            pt1 = new Vector3(m_pts[0], 0.0f);
          }
          else
          {
            pt0 = new Vector3(m_pts[lastIndex], 0.0f);
            pt1 = new Vector3(m_pts[m_pts.Count - 1], 0.0f);
          }
        }
        else
        {
          int delta = (m_clockWise) ? 1 : -1;
          pt0 = new Vector3(m_pts[m_currIndex], 0.0f);
          pt1 = new Vector3(m_pts[m_currIndex + delta], 0.0f);
        }

        current = new Segment(pt0, pt1);
      }

      public void Dispose()
      {
      }

      public bool MoveNext()
      {
        if (m_clockWise)
        {
          if (m_currIndex < m_pts.Count)
          {
            m_inreset = false;
            m_currIndex++;
            return true;
          }
          else
          {
            m_currIndex = m_pts.Count - 1;
          }
        }
        else
        {
          if (m_currIndex > 0)
          {
            m_inreset = false;
            m_currIndex--;
            return true;
          }
          else
          {
            m_currIndex = 0;
          }
        }

        return false;
      }

      public void Reset()
      {
        m_currIndex = (m_clockWise) ? -1 : m_pts.Count;
        m_inreset = true;
      }
    }

    #endregion
  }
}
