﻿/*
* Copyright (c) 2010-2024 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Tesla.Content;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Defines a 3D unbounded line in space that has an origin, and a direction.
  /// </summary>
  [Serializable]
  [StructLayout(LayoutKind.Sequential, Pack = 4)]
  public struct Ray : IEquatable<Ray>, IRefEquatable<Ray>, IFormattable, IPrimitiveValue
  {
    /// <summary>
    /// Origin of the ray.
    /// </summary>
    public Vector3 Origin;

    /// <summary>
    /// Direction of the ray.
    /// </summary>
    public Vector3 Direction;

    private static readonly int s_sizeInBytes = BufferHelper.SizeOf<Ray>();

    /// <summary>
    /// Gets the size of the <see cref="Ray"/> type in bytes.
    /// </summary>
    public static int SizeInBytes
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return s_sizeInBytes;
      }
    }

    /// <summary>
    /// Gets if the ray is degenerate (normal is zero).
    /// </summary>
    public bool IsDegenerate
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return Direction.IsAlmostZero();
      }
    }

    /// <summary>
    /// Gets whether any of the components of the ray are NaN (Not A Number).
    /// </summary>
    public bool IsNaN
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return Origin.IsNaN || Direction.IsNaN;
      }
    }

    /// <summary>
    /// Gets whether any of the components of the ray are positive or negative infinity.
    /// </summary>
    public bool IsInfinity
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return Origin.IsInfinity || Direction.IsInfinity;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Ray"/> struct.
    /// </summary>
    /// <param name="origin">Origin point of the ray.</param>
    /// <param name="direction">Direction of the ray.</param>
    public Ray(in Vector3 origin, in Vector3 direction)
    {
      Origin = origin;
      Direction = direction;
      Direction.Normalize();
    }

    #region Normalize

    /// <summary>
    /// Normalizes the ray.
    /// </summary>
    /// <param name="ray">Ray to normalize.</param>
    /// <returns>Normalized ray.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Ray Normalize(in Ray ray)
    {
      Ray result;
      result.Origin = ray.Origin;
      Vector3.Normalize(ray.Direction, out result.Direction);

      return result;
    }

    /// <summary>
    /// Normalizes the ray.
    /// </summary>
    /// <param name="ray">Ray to normalize.</param>
    /// <param name="result">Normalized ray.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Normalize(in Ray ray, out Ray result)
    {
      result.Origin = ray.Origin;
      Vector3.Normalize(ray.Direction, out result.Direction);
    }

    #endregion

    #region Negate

    /// <summary>
    /// Negates the ray.
    /// </summary>
    /// <param name="ray">Ray to negate.</param>
    /// <returns>Reversed ray.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Ray Negate(in Ray ray)
    {
      Ray result;
      result.Origin = ray.Origin;
      Vector3.Negate(ray.Direction, out result.Direction);

      return result;
    }

    /// <summary>
    /// Negates the ray.
    /// </summary>
    /// <param name="ray">Ray to negate.</param>
    /// <param name="result">Reversed ray.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Negate(in Ray ray, out Ray result)
    {
      result.Origin = ray.Origin;
      Vector3.Negate(ray.Direction, out result.Direction);
    }

    #endregion

    #region From Methods

    /// <summary>
    /// Creates a ray from an origin point and a target point.
    /// </summary>
    /// <param name="origin">Origin of the ray</param>
    /// <param name="target">Target of the ray (target - origin defines direction of the ray)</param>
    /// <returns>The resulting ray</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Ray FromOriginTarget(in Vector3 origin, in Vector3 target)
    {
      Ray result;
      result.Origin = origin;
      Vector3.Subtract(target, origin, out result.Direction);
      result.Direction.Normalize();

      return result;
    }

    /// <summary>
    /// Creates a ray from an origin point and a target point.
    /// </summary>
    /// <param name="origin">Origin of the ray</param>
    /// <param name="target">Target of the ray (target - origin defines direction of the ray)</param>
    /// <param name="result">The resulting ray</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void FromOriginTarget(in Vector3 origin, in Vector3 target, out Ray result)
    {
      result.Origin = origin;
      Vector3.Subtract(target, origin, out result.Direction);
      result.Direction.Normalize();
    }

    /// <summary>
    /// Creates a ray from a line segment.
    /// </summary>
    /// <param name="line">Line segment</param>
    /// <returns>The resulting ray</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Ray FromSegment(in Segment line)
    {
      Ray ray;
      Ray.FromOriginTarget(line.StartPoint, line.EndPoint, out ray);
      return ray;
    }

    /// <summary>
    /// Creates a ray from a line segment.
    /// </summary>
    /// <param name="line">Line segment</param>
    /// <param name="result">The resulting ray</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void FromSegment(in Segment line, out Ray result)
    {
      Ray.FromOriginTarget(line.StartPoint, line.EndPoint, out result);
    }

    #endregion

    #region Transform

    /// <summary>
    /// Transforms a ray by a SRT transformation matrix.
    /// </summary>
    /// <param name="ray">Ray to transform</param>
    /// <param name="transform">Transformation matrix</param>
    /// <returns>Transformed ray</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Ray Transform(in Ray ray, in Matrix transform)
    {
      Ray result;
      Vector3.Transform(ray.Origin, transform, out result.Origin);
      Vector3.TransformNormal(ray.Direction, transform, out result.Direction);

      result.Direction.Normalize();

      return result;
    }

    /// <summary>
    /// Transforms a ray by a SRT transformation matrix.
    /// </summary>
    /// <param name="ray">Ray to transform</param>
    /// <param name="transform">Transformation matrix</param>
    /// <param name="result">Transformed ray</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Transform(in Ray ray, in Matrix transform, out Ray result)
    {
      Vector3.Transform(ray.Origin, transform, out result.Origin);
      Vector3.TransformNormal(ray.Direction, transform, out result.Direction);

      result.Direction.Normalize();
    }

    /// <summary>
    /// Transforms a ray by a rotation.
    /// </summary>
    /// <param name="ray">Ray to be transformed.</param>
    /// <param name="rotation">Quaternion rotation.</param>
    /// <returns>Transformed ray.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Ray Transform(in Ray ray, in Quaternion rotation)
    {
      Ray result;
      result.Origin = ray.Origin;
      Vector3.Transform(ray.Direction, rotation, out result.Direction);

      result.Direction.Normalize();

      return result;
    }

    /// <summary>
    /// Transforms a ray by a rotation.
    /// </summary>
    /// <param name="ray">Ray to be transformed.</param>
    /// <param name="rotation">Quaternion rotation.</param>
    /// <param name="result">Transformed ray.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Transform(in Ray ray, in Quaternion rotation, out Ray result)
    {
      result.Origin = ray.Origin;
      Vector3.Transform(ray.Direction, rotation, out result.Direction);

      result.Direction.Normalize();
    }

    /// <summary>
    /// Transforms a ray by a translation vector.
    /// </summary>
    /// <param name="ray">Ray to be transformed.</param>
    /// <param name="translation">Translation vector.</param>
    /// <returns>Transformed ray.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Ray Transform(in Ray ray, in Vector3 translation)
    {
      Ray result;
      Vector3.Add(ray.Origin, translation, out result.Origin);
      result.Direction = ray.Direction;

      return result;
    }

    /// <summary>
    /// Transforms a ray by a translation vector.
    /// </summary>
    /// <param name="ray">Ray to be transformed.</param>
    /// <param name="translation">Translation vector.</param>
    /// <param name="result">Transformed ray.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Transform(in Ray ray, in Vector3 translation, out Ray result)
    {
      Vector3.Add(ray.Origin, translation, out result.Origin);
      result.Direction = ray.Direction;
    }

    #endregion

    #region Equality Operators

    /// <summary>
    /// Tests equality between two rays.
    /// </summary>
    /// <param name="a">First ray</param>
    /// <param name="b">Second ray</param>
    /// <returns>True if the rays are equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator ==(in Ray a, in Ray b)
    {
      return a.Origin.Equals(b.Origin) && a.Direction.Equals(b.Direction);
    }

    /// <summary>
    /// Tests inequality between two rays.
    /// </summary>
    /// <param name="a">First ray</param>
    /// <param name="b">Second ray</param>
    /// <returns>True if the rays are not equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator !=(in Ray a, in Ray b)
    {
      return !a.Origin.Equals(b.Origin) || !a.Direction.Equals(b.Direction);
    }

    #endregion

    #region Public Methods

    /// <summary>
    /// Normalizes the ray's direction.
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void Normalize()
    {
      Direction.Normalize();
    }

    /// <summary>
    /// Reverses the ray's direction.
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void Negate()
    {
      Direction.Negate();
    }

    /// <summary>
    /// Tests if the ray is perpendicular to the specified ray. This assumes both rays are normalized.
    /// </summary>
    /// <param name="ray">Ray to test against</param>
    /// <returns>True if the rays are perpendicular to each other, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool IsPerpendicularTo(in Ray ray)
    {
      float dot = Vector3.Dot(Direction, ray.Direction);

      return MathHelper.IsEqual(dot, 0.0f);
    }

    /// <summary>
    /// Tests if the ray is parallel to the specified ray.
    /// </summary>
    /// <param name="ray">Ray to test against</param>
    /// <returns>True if the rays are parallel to each other, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool IsParallelTo(in Ray ray)
    {
      Vector3.NormalizedCross(Direction, ray.Direction, out Vector3 cross);

      return cross.IsAlmostZero();
    }

    #endregion

    #region Intersects

    /// <summary>
    /// Determines whether there is an apparent 2D (XY only) intersection between this object and a <see cref="Ray"/>.
    /// </summary>
    /// <param name="ray">Ray to test.</param>
    /// <returns>True if an intersection between the two objects occurs, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool IntersectsXY(in Ray ray)
    {
      return GeometricToolsHelper.IntersectRayRayXY(this, ray);
    }

    /// <summary>
    /// Determines whether there is an apparent 2D (XY only) intersection between this object and a <see cref="Ray"/>.
    /// </summary>
    /// <param name="ray">Ray to test.</param>
    /// <param name="result">Intersection result between the two objects.</param>
    /// <returns>True if an intersection between the two objects occurs, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool IntersectsXY(in Ray ray, out Vector3 result)
    {
      return GeometricToolsHelper.IntersectRayRayXY(this, ray, out result, out float ray0Param, out float ray1Param);
    }

    /// <summary>
    /// Determines whether there is an apparent 2D (XY only) intersection between this object and a <see cref="Segment"/>.
    /// </summary>
    /// <param name="segment">Segment to test.</param>
    /// <returns>True if an intersection between the two objects occurs, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool IntersectsXY(in Segment segment)
    {
      return GeometricToolsHelper.IntersectRaySegmentXY(this, segment);
    }

    /// <summary>
    /// Determines whether there is an apparent 2D (XY only) intersection between this object and a <see cref="Segment"/>.
    /// </summary>
    /// <param name="segment">Segment to test.</param>
    /// <param name="result">Intersection result between the two objects.</param>
    /// <returns>True if an intersection between the two objects occurs, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool IntersectsXY(in Segment segment, out Vector3 result)
    {
      return GeometricToolsHelper.IntersectRaySegmentXY(this, segment, out result, out float rayParam, out float segParam);
    }

    /// <summary>
    /// Determines whether there is an intersection between this object and a <see cref="Plane"/>.
    /// </summary>
    /// <param name="plane">Plane to test.</param>
    /// <returns>True if an intersection between the two objects occurs, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Intersects(in Plane plane)
    {
      return GeometricToolsHelper.IntersectRayPlane(this, plane);
    }

    /// <summary>
    /// Determines whether there is an intersection between this object and a <see cref="Plane"/>.
    /// </summary>
    /// <param name="plane">Plane to test.</param>
    /// <param name="result">Intersection result between the two objects.</param>
    /// <returns>True if an intersection between the two objects occurs, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Intersects(in Plane plane, out LineIntersectionResult result)
    {
      return GeometricToolsHelper.IntersectRayPlane(this, plane, out result);
    }

    /// <summary>
    /// Determines whether there is an intersection between this object and a <see cref="Triangle"/>.
    /// </summary>
    /// <param name="triangle">Triangle to test.</param>
    /// <param name="ignoreBackface">True if the test should ignore an intersection if the triangle is a back face, false if otherwise.</param>
    /// <returns>True if an intersection between the two objects occurs, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Intersects(in Triangle triangle, bool ignoreBackface = false)
    {
      return GeometricToolsHelper.IntersectRayTriangle(triangle, this, ignoreBackface, out _);
    }

    /// <summary>
    /// Determines whether there is an intersection between this object and a <see cref="Triangle"/>.
    /// </summary>
    /// <param name="triangle">Triangle to test.</param>
    /// <param name="result">Intersection result between the two objects.</param>
    /// <param name="ignoreBackface">True if the test should ignore an intersection if the triangle is a back face, false if otherwise.</param>
    /// <returns>True if an intersection between the two objects occurs, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Intersects(in Triangle triangle, out LineIntersectionResult result, bool ignoreBackface = false)
    {
      return GeometricToolsHelper.IntersectRayTriangle(triangle, this, ignoreBackface, out result);
    }

    /// <summary>
    /// Determines whether there is an intersection between this object and an <see cref="Ellipse"/>.
    /// </summary>
    /// <param name="ellipse">Ellipse to test.</param>
    /// <returns>True if an intersection between the two objects occurs, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Intersects(in Ellipse ellipse)
    {
      //TODO
      throw new NotImplementedException();
    }

    /// <summary>
    /// Determines whether there is an intersection between this object and an <see cref="Ellipse"/>.
    /// </summary>
    /// <param name="ellipse">Ellipse to test.</param>
    /// <param name="result">Intersection result between the two objects.</param>
    /// <returns>True if an intersection between the two objects occurs, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Intersects(in Ellipse ellipse, out LineIntersectionResult result)
    {
      //TODO
      throw new NotImplementedException();
    }

    /// <summary>
    /// Determines whether there is an intersection between this object and a <see cref="BoundingVolume"/>.
    /// </summary>
    /// <param name="volume">Bounding volume to test.</param>
    /// <returns>True if an intersection between the two objects occurs, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Intersects([NotNullWhen(true)] BoundingVolume? volume)
    {
      if (volume is null)
        return false;

      return volume.Intersects(this);
    }

    /// <summary>
    /// Determines whether there is an intersection between this object and a <see cref="BoundingVolume"/>.
    /// </summary>
    /// <param name="volume">Bounding volume to test.</param>
    /// <param name="result">Bounding intersection result.</param>
    /// <returns>True if an intersection between the two objects occurs, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Intersects([NotNullWhen(true)] BoundingVolume? volume, out BoundingIntersectionResult result)
    {
      if (volume is null)
      {
        result = new BoundingIntersectionResult();
        return false;
      }

      return volume.Intersects(this, out result);
    }

    #endregion

    #region Distance/Point To/From

    /// <summary>
    /// Gets the point along the ray that is the distance from the origin.
    /// </summary>
    /// <param name="distance">Distance of the point from the origin.</param>
    /// <returns>The point along the ray.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly Vector3 PointAtDistance(float distance)
    {
      Vector3 result;
      Vector3.Multiply(Direction, distance, out result);
      Vector3.Add(Origin, result, out result);

      return result;
    }

    /// <summary>
    /// Gets the point along the ray that is the distance from the origin.
    /// </summary>
    /// <param name="distance">Distance of the point from the origin.</param>
    /// <param name="result">The point along the ray.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly void PointAtDistance(float distance, out Vector3 result)
    {
      Vector3.Multiply(Direction, distance, out result);
      Vector3.Add(Origin, result, out result);
    }

    /// <summary>
    /// Gets the distance of the point (or closest on the ray) from the origin.
    /// </summary>
    /// <param name="point">Point along or near the ray.</param>
    /// <returns>The distance from the point (or point closest on the ray) to the origin.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float DistanceAtPoint(in Vector3 point)
    {
      Vector3 v;
      Vector3.Subtract(point, Origin, out v);

      float dot = Vector3.Dot(Direction, v);
      float dotDir = Vector3.Dot(Direction, Direction);

      if (MathHelper.IsEqual(dotDir, MathHelper.ZeroTolerance))
        return 0.0f;

      return dot / dotDir;
    }

    #endregion

    #region Distance To

    /// <summary>
    /// Determines the distance between this object and a point.
    /// </summary>
    /// <param name="point">Point to test.</param>
    /// <returns>Distance between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float DistanceTo(in Vector3 point)
    {
      GeometricToolsHelper.DistancePointRay(point, this, out Vector3 ptOnRay, out float rayParam, out float sqrDist);

      return MathF.Sqrt(sqrDist);
    }

    /// <summary>
    /// Determines the distance between this object and a <see cref="Ray"/>.
    /// </summary>
    /// <param name="ray">Ray to test.</param>
    /// <returns>Distance between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float DistanceTo(in Ray ray)
    {
      GeometricToolsHelper.DistanceRayRay(this, ray, out float ray0Param, out float ray1Param, out float sqrDist);

      return MathF.Sqrt(sqrDist);
    }

    /// <summary>
    /// Determines the distance between this object and a <see cref="Segment"/>.
    /// </summary>
    /// <param name="segment">Segment to test.</param>
    /// <returns>Distance between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float DistanceTo(in Segment segment)
    {
      GeometricToolsHelper.DistanceRaySegment(this, segment, out float rayParam, out float segParam, out float sqrDist);

      return MathF.Sqrt(sqrDist);
    }

    /// <summary>
    /// Determines the distance between this object and a <see cref="Plane"/>.
    /// </summary>
    /// <param name="plane">Plane to test.</param>
    /// <returns>Distance between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float DistanceTo(in Plane plane)
    {
      GeometricToolsHelper.DistanceRayPlane(plane, this, out Vector3 ptOnPlane, out Vector3 ptOnRay, out float sqrDist);

      return MathF.Sqrt(sqrDist);
    }

    /// <summary>
    /// Determines the distance between this object and a <see cref="Triangle"/>.
    /// </summary>
    /// <param name="triangle">Triangle to test.</param>
    /// <returns>Distance between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float DistanceTo(in Triangle triangle)
    {
      GeometricToolsHelper.DistanceRayTriangle(triangle, this, out Vector3 ptOnTriangle, out Vector3 ptOnRay, out float sqrDist);

      return MathF.Sqrt(sqrDist);
    }

    /// <summary>
    /// Determines the distance between this object and an <see cref="Ellipse"/>.
    /// </summary>
    /// <param name="ellipse">Ellipse to test.</param>
    /// <returns>Distance between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float DistanceTo(in Ellipse ellipse)
    {
      //TODO
      throw new NotImplementedException();
    }

    #endregion

    #region Distance Squared To

    /// <summary>
    /// Determines the distance squared between this object and a point.
    /// </summary>
    /// <param name="point">Point to test.</param>
    /// <returns>Distance squared between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float DistanceSquaredTo(in Vector3 point)
    {
      GeometricToolsHelper.DistancePointRay(point, this, out Vector3 ptOnRay, out float rayParam, out float sqrDist);

      return sqrDist;
    }

    /// <summary>
    /// Determines the distance squared between this object and a <see cref="Ray"/>.
    /// </summary>
    /// <param name="ray">Ray to test.</param>
    /// <returns>Distance squared between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float DistanceSquaredTo(in Ray ray)
    {
      GeometricToolsHelper.DistanceRayRay(this, ray, out float ray0Param, out float ray1Param, out float sqrDist);

      return sqrDist;
    }

    /// <summary>
    /// Determines the distance squared between this object and a <see cref="Segment"/>.
    /// </summary>
    /// <param name="segment">Segment to test.</param>
    /// <returns>Distance squared between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float DistanceSquaredTo(in Segment segment)
    {
      GeometricToolsHelper.DistanceRaySegment(this, segment, out float rayParam, out float segParam, out float sqrDist);

      return sqrDist;
    }

    /// <summary>
    /// Determines the distance squared between this object and a <see cref="Plane"/>.
    /// </summary>
    /// <param name="plane">Plane to test.</param>
    /// <returns>Distance squared between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float DistanceSquaredTo(in Plane plane)
    {
      GeometricToolsHelper.DistanceRayPlane(plane, this, out Vector3 ptOnPlane, out Vector3 ptOnRay, out float sqrDist);

      return sqrDist;
    }

    /// <summary>
    /// Determines the distance squared between this object and a <see cref="Triangle"/>.
    /// </summary>
    /// <param name="triangle">Triangle to test.</param>
    /// <returns>Distance squared between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float DistanceSquaredTo(in Triangle triangle)
    {
      GeometricToolsHelper.DistanceRayTriangle(triangle, this, out Vector3 ptOnTriangle, out Vector3 ptOnRay, out float sqrDist);

      return sqrDist;
    }

    /// <summary>
    /// Determines the distance squared between this object and an <see cref="Ellipse"/>.
    /// </summary>
    /// <param name="ellipse">Ellipse to test.</param>
    /// <returns>Distance squared between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float DistanceSquaredTo(in Ellipse ellipse)
    {
      //TODO
      throw new NotImplementedException();
    }

    #endregion

    #region Closest Point / Approach Segment

    /// <summary>
    /// Determines the closest point on this object from the point.
    /// </summary>
    /// <param name="point">Point to test.</param>
    /// <returns>Closest point on this object.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly Vector3 ClosestPointTo(in Vector3 point)
    {
      GeometricToolsHelper.DistancePointRay(point, this, out Vector3 ptOnRay, out float rayParam, out float sqrDist);

      return ptOnRay;
    }

    /// <summary>
    /// Determines the closest point on this object from the point.
    /// </summary>
    /// <param name="point">Point to test.</param>
    /// <param name="result">Closest point on this object.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly void ClosestPointTo(in Vector3 point, out Vector3 result)
    {
      GeometricToolsHelper.DistancePointRay(point, this, out result, out float rayParam, out float sqrDist);
    }

    /// <summary>
    /// Determines the closest point on this object from the point.
    /// </summary>
    /// <param name="point">Point to test.</param>
    /// <param name="result">Closest point on this object.</param>
    /// <param name="squaredDistance">Squared distance between the two objects.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly void ClosestPointTo(in Vector3 point, out Vector3 result, out float squaredDistance)
    {
      GeometricToolsHelper.DistancePointRay(point, this, out result, out float rayParam, out squaredDistance);
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Ray"/>.
    /// </summary>
    /// <param name="ray">Ray to test</param>
    /// <returns>Closest approach segment between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly Segment ClosestApproachSegment(in Ray ray)
    {
      float ray0Param, ray1Param, sqrDist;
      GeometricToolsHelper.DistanceRayRay(this, ray, out ray0Param, out ray1Param, out sqrDist);

      Segment result;
      Vector3.Multiply(Direction, ray0Param, out result.StartPoint);
      Vector3.Add(result.StartPoint, Origin, out result.StartPoint);

      Vector3.Multiply(ray.Direction, ray1Param, out result.EndPoint);
      Vector3.Multiply(result.EndPoint, ray.Origin, out result.EndPoint);

      return result;
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Ray"/>.
    /// </summary>
    /// <param name="ray">Ray to test</param>
    /// <param name="result">Closest approach segment between the two objects.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly void ClosestApproachSegment(in Ray ray, out Segment result)
    {
      float ray0Param, ray1Param, sqrDist;
      GeometricToolsHelper.DistanceRayRay(this, ray, out ray0Param, out ray1Param, out sqrDist);

      Vector3.Multiply(Direction, ray0Param, out result.StartPoint);
      Vector3.Add(result.StartPoint, Origin, out result.StartPoint);

      Vector3.Multiply(ray.Direction, ray1Param, out result.EndPoint);
      Vector3.Multiply(result.EndPoint, ray.Origin, out result.EndPoint);
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Ray"/>.
    /// </summary>
    /// <param name="ray">Ray to test</param>
    /// <param name="result">Closest approach segment between the two objects.</param>
    /// <param name="squaredDistance">Squared distance between the two objects.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly void ClosestApproachSegment(in Ray ray, out Segment result, out float squaredDistance)
    {
      float ray0Param, ray1Param;
      GeometricToolsHelper.DistanceRayRay(this, ray, out ray0Param, out ray1Param, out squaredDistance);

      Vector3.Multiply(Direction, ray0Param, out result.StartPoint);
      Vector3.Add(result.StartPoint, Origin, out result.StartPoint);

      Vector3.Multiply(ray.Direction, ray1Param, out result.EndPoint);
      Vector3.Multiply(result.EndPoint, ray.Origin, out result.EndPoint);
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Segment"/>.
    /// </summary>
    /// <param name="segment">Segment to test</param>
    /// <returns>Closest approach segment between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly Segment ClosestApproachSegment(in Segment segment)
    {
      float rayParam, segParam, sqrDist;
      GeometricToolsHelper.DistanceRaySegment(this, segment, out rayParam, out segParam, out sqrDist);

      Segment result;
      Vector3.Multiply(Direction, rayParam, out result.StartPoint);
      Vector3.Add(result.StartPoint, Origin, out result.StartPoint);

      //Calculate point along segment
      Vector3.NormalizedSubtract(segment.EndPoint, segment.StartPoint, out Vector3 dir);

      Vector3.Multiply(dir, segParam, out dir);
      Vector3.Add(segment.StartPoint, dir, out result.EndPoint);

      return result;
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Segment"/>.
    /// </summary>
    /// <param name="segment">Segment to test</param>
    /// <param name="result">Closest approach segment between the two objects.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly void ClosestApproachSegment(in Segment segment, out Segment result)
    {
      float rayParam, segParam, sqrDist;
      GeometricToolsHelper.DistanceRaySegment(this, segment, out rayParam, out segParam, out sqrDist);

      Vector3.Multiply(Direction, rayParam, out result.StartPoint);
      Vector3.Add(result.StartPoint, Origin, out result.StartPoint);

      //Calculate point along segment
      Vector3.NormalizedSubtract(segment.EndPoint, segment.StartPoint, out Vector3 dir);

      Vector3.Multiply(dir, segParam, out dir);
      Vector3.Add(segment.StartPoint, dir, out result.EndPoint);
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Segment"/>.
    /// </summary>
    /// <param name="segment">Segment to test</param>
    /// <param name="result">Closest approach segment between the two objects.</param>
    /// <param name="squaredDistance">Squared distance between the two objects.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly void ClosestApproachSegment(in Segment segment, out Segment result, out float squaredDistance)
    {
      float rayParam, segParam;
      GeometricToolsHelper.DistanceRaySegment(this, segment, out rayParam, out segParam, out squaredDistance);

      Vector3.Multiply(Direction, rayParam, out result.StartPoint);
      Vector3.Add(result.StartPoint, Origin, out result.StartPoint);

      //Calculate point along segment
      Vector3.NormalizedSubtract(segment.EndPoint, segment.StartPoint, out Vector3 dir);

      Vector3.Multiply(dir, segParam, out dir);
      Vector3.Add(segment.StartPoint, dir, out result.EndPoint);
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Plane"/>.
    /// </summary>
    /// <param name="plane">Plane to test</param>
    /// <returns>Closest approach segment between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly Segment ClosestApproachSegment(in Plane plane)
    {
      Segment result;
      GeometricToolsHelper.DistanceRayPlane(plane, this, out result.EndPoint, out result.StartPoint, out float sqrDist);

      return result;
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Plane"/>.
    /// </summary>
    /// <param name="plane">Plane to test</param>
    /// <param name="result">Closest approach segment between the two objects.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly void ClosestApproachSegment(in Plane plane, out Segment result)
    {
      GeometricToolsHelper.DistanceRayPlane(plane, this, out result.EndPoint, out result.StartPoint, out float sqrDist);
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Plane"/>.
    /// </summary>
    /// <param name="plane">Plane to test</param>
    /// <param name="result">Closest approach segment between the two objects.</param>
    /// <param name="squaredDistance">Squared distance between the two objects.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly void ClosestApproachSegment(in Plane plane, out Segment result, out float squaredDistance)
    {
      GeometricToolsHelper.DistanceRayPlane(plane, this, out result.EndPoint, out result.StartPoint, out squaredDistance);
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Triangle"/>.
    /// </summary>
    /// <param name="triangle">Triangle to test</param>
    /// <returns>Closest approach segment between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly Segment ClosestApproachSegment(in Triangle triangle)
    {
      Segment result;
      GeometricToolsHelper.DistanceRayTriangle(triangle, this, out result.EndPoint, out result.StartPoint, out float sqrDist);

      return result;
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Triangle"/>.
    /// </summary>
    /// <param name="triangle">Triangle to test</param>
    /// <param name="result">Closest approach segment between the two objects.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly void ClosestApproachSegment(in Triangle triangle, out Segment result)
    {
      GeometricToolsHelper.DistanceRayTriangle(triangle, this, out result.EndPoint, out result.StartPoint, out float sqrDist);
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Triangle"/>.
    /// </summary>
    /// <param name="triangle">Triangle to test</param>
    /// <param name="result">Closest approach segment between the two objects.</param>
    /// <param name="squaredDistance">Squared distance between the two objects.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly void ClosestApproachSegment(in Triangle triangle, out Segment result, float squaredDistance)
    {
      GeometricToolsHelper.DistanceRayTriangle(triangle, this, out result.EndPoint, out result.StartPoint, out squaredDistance);
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Ellipse"/>.
    /// </summary>
    /// <param name="ellipse">Ellipse to test</param>
    /// <returns>Closest approach segment between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly Segment ClosestApproachSegment(in Ellipse ellipse)
    {
      //TODO
      throw new NotImplementedException();
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Ellipse"/>.
    /// </summary>
    /// <param name="ellipse">Ellipse to test</param>
    /// <param name="result">Closest approach segment between the two objects.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly void ClosestApproachSegment(in Ellipse ellipse, out Segment result)
    {
      //TODO
      throw new NotImplementedException();
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Ellipse"/>.
    /// </summary>
    /// <param name="ellipse">Ellipse to test</param>
    /// <param name="result">Closest approach segment between the two objects.</param>
    /// <param name="squaredDistance">Squared distance between the two objects.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly void ClosestApproachSegment(in Ellipse ellipse, out Segment result, out float squaredDistance)
    {
      //TODO
      throw new NotImplementedException();
    }

    #endregion

    #region Equality

    /// <summary>
    /// Indicates whether this instance and a specified object are equal.
    /// </summary>
    /// <param name="obj">Another object to compare to.</param>
    /// <returns>
    /// true if <paramref name="obj" /> and this instance are the same type and represent the same value; otherwise, false.
    /// </returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is Ray)
        return Equals((Ray) obj);

      return false;
    }

    /// <summary>
    /// Tests equality between the ray and another ray.
    /// </summary>
    /// <param name="other">Other ray to test</param>
    /// <returns>True if the rays are equal, false otherwise</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    readonly bool IEquatable<Ray>.Equals(Ray other)
    {
      return Origin.Equals(other.Origin) && Direction.Equals(other.Direction);
    }

    /// <summary>
    /// Tests equality between the ray and another ray.
    /// </summary>
    /// <param name="other">Other ray to test</param>
    /// <returns>True if the rays are equal, false otherwise</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Equals(in Ray other)
    {
      return Origin.Equals(other.Origin) && Direction.Equals(other.Direction);
    }

    /// <summary>
    /// Tests equality between the ray and another ray.
    /// </summary>
    /// <param name="other">Other ray to test</param>
    /// <param name="tolerance">Tolerance</param>
    /// <returns>True if the rays are equal, false otherwise</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Equals(in Ray other, float tolerance)
    {
      return Origin.Equals(other.Origin, tolerance) && Direction.Equals(other.Direction, tolerance);
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
    public readonly override int GetHashCode()
    {
      unchecked
      {
        return Origin.GetHashCode() + Direction.GetHashCode();
      }
    }

    #endregion

    #region ToString

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly override string ToString()
    {
      CultureInfo info = CultureInfo.CurrentCulture;
      return String.Format(info, "Origin: ({0}), Direction: ({1})",
          new Object[] { Origin.ToString(info), Direction.ToString(info) });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="format">The format</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(string? format)
    {
      if (format is null)
        return ToString();

      CultureInfo info = CultureInfo.CurrentCulture;
      return String.Format(info, "Origin: ({0}), Direction: ({1})",
          new Object[] { Origin.ToString(format, info), Direction.ToString(format, info) });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="formatProvider">The format provider.</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(IFormatProvider? formatProvider)
    {
      if (formatProvider is null)
        return ToString();

      return String.Format(formatProvider, "Origin: ({0}), Direction: ({1})",
          new Object[] { Origin.ToString(formatProvider), Direction.ToString(formatProvider) });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="format">The format.</param>
    /// <param name="formatProvider">The format provider.</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(string? format, IFormatProvider? formatProvider)
    {
      if (formatProvider is null)
        return ToString();

      if (format is null)
        return ToString(formatProvider);

      return String.Format(formatProvider, "Origin: ({0}), Direction: ({1})",
          new Object[] { Origin.ToString(format, formatProvider), Direction.ToString(format, formatProvider) });
    }

    #endregion

    #region IPrimitiveValue

    /// <summary>
    /// Writes the primitive data to the output.
    /// </summary>
    /// <param name="output">Primitive writer.</param>
    readonly void IPrimitiveValue.Write(IPrimitiveWriter output)
    {
      output.Write<Vector3>("Origin", Origin);
      output.Write<Vector3>("Direction", Direction);
    }

    /// <summary>
    /// Reads the primitive data from the input.
    /// </summary>
    /// <param name="input">Primitive reader.</param>
    void IPrimitiveValue.Read(IPrimitiveReader input)
    {
      input.Read<Vector3>("Origin", out Origin);
      input.Read<Vector3>("Direction", out Direction);
    }

    #endregion
  }
}
