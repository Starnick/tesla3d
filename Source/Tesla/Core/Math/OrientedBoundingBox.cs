﻿/*
* Copyright (c) 2010-2024 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Tesla.Content;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Represents a Bounding Box with arbitrary orientation defined by three axes. The box is defined by extents from its center along the three axes.
  /// </summary>
  [SavableVersion(1)]
  public sealed class OrientedBoundingBox : BoundingVolume
  {
    private Vector3 m_center;
    private Triad m_axes;
    private Vector3 m_extents;

    /// <summary>
    /// Gets or sets the center of the bounding volume.
    /// </summary>
    public override Vector3 Center
    {
      get
      {
        return m_center;
      }
      set
      {
        m_center = value;
        UpdateCorners = true;
      }
    }

    /// <summary>
    /// Gets or sets the X-Axis of the bounding box.
    /// </summary>
    public Vector3 XAxis
    {
      get
      {
        return m_axes.XAxis;
      }
      set
      {
        m_axes.XAxis = value;
        UpdateCorners = true;
      }
    }

    /// <summary>
    /// Gets or sets the Y-Axis of the bounding box.
    /// </summary>
    public Vector3 YAxis
    {
      get
      {
        return m_axes.YAxis;
      }
      set
      {
        m_axes.YAxis = value;
        UpdateCorners = true;
      }
    }

    /// <summary>
    /// Gets or sets the Z-Axis of the bounding box.
    /// </summary>
    public Vector3 ZAxis
    {
      get
      {
        return m_axes.ZAxis;
      }
      set
      {
        m_axes.ZAxis = value;
        UpdateCorners = true;
      }
    }

    /// <summary>
    /// Gets or sets the axes of the bounding box;
    /// </summary>
    public Triad Axes
    {
      get
      {
        return m_axes;
      }
      set
      {
        m_axes = Axes;
        UpdateCorners = true;
      }
    }

    /// <summary>
    /// Gets or sets the extents of the bounding box (half-lengths along each axis from the center).
    /// </summary>
    public Vector3 Extents
    {
      get
      {
        return m_extents;
      }
      set
      {
        m_extents = value;
        UpdateCorners = true;
      }
    }

    /// <summary>
    /// Gets the volume of the bounding volume.
    /// </summary>
    public override float Volume { get { return (m_extents.X * m_extents.Y * m_extents.Z) * 2.0f; } }

    /// <summary>
    /// Gets the bounding type.
    /// </summary>
    public override BoundingType BoundingType { get { return BoundingType.OrientedBoundingBox; } }

    /// <summary>
    /// Constructs a new instance of the <see cref="OrientedBoundingBox"/> class.
    /// </summary>
    public OrientedBoundingBox()
    {
      m_center = Vector3.Zero;
      m_axes = Triad.UnitAxes;
      m_extents = Vector3.Zero;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="OrientedBoundingBox"/> class.
    /// </summary>
    /// <param name="center">Center of the bounding box.</param>
    /// <param name="extents">Extents of the bounding box.</param>
    public OrientedBoundingBox(in Vector3 center, in Vector3 extents)
    {
      m_center = center;
      m_axes = Triad.UnitAxes;
      m_extents = extents;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="OrientedBoundingBox"/> class.
    /// </summary>
    /// <param name="center">Center of the bounding box.</param>
    /// <param name="xAxis">X-Axis of the bounding box.</param>
    /// <param name="yAxis">Y-Axis of the bounding box.</param>
    /// <param name="zAxis">Z-Axis of the bounding box.</param>
    /// <param name="extents">Extents of the bounding box.</param>
    public OrientedBoundingBox(in Vector3 center, in Vector3 xAxis, in Vector3 yAxis, in Vector3 zAxis, in Vector3 extents)
    {
      m_center = center;
      m_axes.XAxis = xAxis;
      m_axes.YAxis = yAxis;
      m_axes.ZAxis = zAxis;
      m_extents = extents;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="OrientedBoundingBox"/> class.
    /// </summary>
    /// <param name="center">Center of the bounding box.</param>
    /// <param name="axes">Axes of the bounding box.</param>
    /// <param name="extents">Extents of the bounding box.</param>
    public OrientedBoundingBox(in Vector3 center, in Triad axes, in Vector3 extents)
    {
      m_center = center;
      m_axes = axes;
      m_extents = extents;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="OrientedBoundingBox"/> class.
    /// </summary>
    /// <param name="orientedBoundingBoxData">Oriented bounding box data.</param>
    public OrientedBoundingBox(in OrientedBoundingBox.Data orientedBoundingBoxData)
    {
      m_center = orientedBoundingBoxData.Center;
      m_axes = orientedBoundingBoxData.Axes;
      m_extents = orientedBoundingBoxData.Extents;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="OrientedBoundingBox"/> class.
    /// </summary>
    /// <param name="orientedBoundingBox">Oriented bounding box to copy from.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the oriented bounding box is null.</exception>
    public OrientedBoundingBox(OrientedBoundingBox orientedBoundingBox)
    {
      ArgumentNullException.ThrowIfNull(orientedBoundingBox, nameof(orientedBoundingBox));

      m_center = orientedBoundingBox.m_center;
      m_axes = orientedBoundingBox.m_axes;
      m_extents = orientedBoundingBox.m_extents;
    }

    /// <summary>
    /// Sets the bounding volume by either copying the specified bounding volume if its the specified type, or computing a volume required to fully contain it.
    /// </summary>
    /// <param name="volume">Bounding volume to copy from</param>
    public override void Set(BoundingVolume? volume)
    {
      if (volume is null)
        return;

      switch (volume.BoundingType)
      {
        case BoundingType.AxisAlignedBoundingBox:
          {
            BoundingBox other = (volume as BoundingBox)!;
            m_center = other.Center;
            m_axes = Triad.UnitAxes;
            m_extents = other.Extents;
          }
          break;
        case BoundingType.Sphere:
          {
            BoundingSphere other = (volume as BoundingSphere)!;
            float radius = other.Radius;
            m_center = other.Center;
            m_extents = new Vector3(radius, radius, radius);
          }
          break;
        case BoundingType.Capsule:
          {
            BoundingCapsule other = (volume as BoundingCapsule)!;
            Segment centerLine = other.CenterLine;

            GeometricToolsHelper.BoxFromCapsule(centerLine, other.Radius, out m_center, out m_axes.XAxis, out m_axes.YAxis, out m_axes.ZAxis, out m_extents);
          }
          break;
        case BoundingType.OrientedBoundingBox:
          {
            OrientedBoundingBox other = (volume as OrientedBoundingBox)!;
            m_center = other.m_center;
            m_axes = other.m_axes;
            m_extents = other.m_extents;
          }
          break;
        case BoundingType.Frustum:
        case BoundingType.Mesh:
        default:
          {
            ComputeFromPoints(volume.Corners, null);
          }
          break;
      }

      UpdateCorners = true;
    }

    /// <summary>
    /// Sets the bounding box from the specified data.
    /// </summary>
    /// <param name="orientedBoundingBoxData">Oriented bounding box data.</param>
    public void Set(in OrientedBoundingBox.Data orientedBoundingBoxData)
    {
      m_center = orientedBoundingBoxData.Center;
      m_axes = orientedBoundingBoxData.Axes;
      m_extents = orientedBoundingBoxData.Extents;
      UpdateCorners = true;
    }

    /// <summary>
    /// Sets the bounding box from the specified data.
    /// </summary>
    /// <param name="center">Center of the bounding box.</param>
    /// <param name="xAxis">X-Axis of the bounding box.</param>
    /// <param name="yAxis">Y-Axis of the bounding box.</param>
    /// <param name="zAxis">Z-Axis of the bounding box.</param>
    /// <param name="extents">Extents of the bounding box.</param>
    public void Set(in Vector3 center, in Vector3 xAxis, in Vector3 yAxis, in Vector3 zAxis, in Vector3 extents)
    {
      m_center = center;
      m_axes.XAxis = xAxis;
      m_axes.YAxis = yAxis;
      m_axes.ZAxis = zAxis;
      m_extents = extents;
      UpdateCorners = true;
    }

    /// <summary>
    /// Sets the bounding box from the specified data.
    /// </summary>
    /// <param name="center">Center of the bounding box.</param>
    /// <param name="axes">Axes of the bounding box.</param>
    /// <param name="extents">Extents of the bounding box.</param>
    public void Set(in Vector3 center, in Triad axes, in Vector3 extents)
    {
      m_center = center;
      m_axes = axes;
      m_extents = extents;
      UpdateCorners = true;
    }

    /// <summary>
    /// Creates a copy of the bounding volume and returns a new instance.
    /// </summary>
    /// <returns>Copied bounding volume</returns>
    public override BoundingVolume Clone()
    {
      return new OrientedBoundingBox(this);
    }

    /// <summary>
    /// Computes the distance from a given point to the edge of the volume.
    /// </summary>
    /// <param name="point">Point in space</param>
    /// <returns>Distance from the point to the edge of the volume.</returns>
    public override float DistanceTo(in Vector3 point)
    {
      float sqrDist;
      GeometricToolsHelper.DistancePointBox(point, m_center, m_axes.XAxis, m_axes.YAxis, m_axes.ZAxis, m_extents, out sqrDist);

      return (float) Math.Sqrt(sqrDist);
    }

    /// <summary>
    /// Computes the distance squared from a given point to the edge of the volume.
    /// </summary>
    /// <param name="point">Point in space</param>
    /// <returns>Distanced squared from the point to the edge of the volume.</returns>
    public override float DistanceSquaredTo(in Vector3 point)
    {
      float sqrDist;
      GeometricToolsHelper.DistancePointBox(point, m_center, m_axes.XAxis, m_axes.YAxis, m_axes.ZAxis, m_extents, out sqrDist);

      return sqrDist;
    }

    /// <summary>
    /// Computes the closest point on the volume from the given point in space.
    /// </summary>
    /// <param name="point">Point in space</param>
    /// <param name="result">Closest point on the edge of the volume.</param>
    public override void ClosestPointTo(in Vector3 point, out Vector3 result)
    {
      GeometricToolsHelper.ClosestPointToBox(point, m_center, m_axes.XAxis, m_axes.YAxis, m_axes.ZAxis, m_extents, out result);
    }

    /// <summary>
    /// Computes a minimum bounding volume that encloses the specified points in space.
    /// </summary>
    /// <param name="points">Points in space</param>
    /// <param name="subMeshRange">Optional range inside the buffer that represents the mesh to compute bounds for. Note: the base vertex offset will be ignored.</param>
    public override void ComputeFromPoints(ReadOnlySpan<Vector3> points, in SubMeshRange? subMeshRange = null)
    {
      OrientedBoundingBox.Data data;
      OrientedBoundingBox.Data.FromPoints(points, subMeshRange, out data);

      m_center = data.Center;
      m_axes = data.Axes;
      m_extents = data.Extents;
      UpdateCorners = true;
    }

    /// <summary>
    /// Computes a minimum bounding volume that encloses the specified indexed points in space.
    /// </summary>
    /// <param name="points">Points in space.</param>
    /// <param name="indices">Point indices denoting location in the point buffer.</param>
    /// <param name="subMeshRange">Optional range inside the buffer that represents the mesh to compute bounds for.</param>
    public override void ComputeFromIndexedPoints(ReadOnlySpan<Vector3> points, IndexData indices, in SubMeshRange? subMeshRange = null)
    {
      OrientedBoundingBox.Data data;
      OrientedBoundingBox.Data.FromIndexedPoints(points, indices, subMeshRange, out data);

      m_center = data.Center;
      m_axes = data.Axes;
      m_extents = data.Extents;
      UpdateCorners = true;
    }

    /// <summary>
    /// Determines if the specified point is contained inside the bounding volume.
    /// </summary>
    /// <param name="point">Point to test against.</param>
    /// <returns>Type of containment</returns>
    public override ContainmentType Contains(in Vector3 point)
    {
      Vector3 diff;
      Vector3.Subtract(point, m_center, out diff);

      for (int i = 0; i < 3; i++)
      {
        Vector3 axis;
        m_axes.GetAxis(i, out axis);

        float coeff = Vector3.Dot(diff, axis);

        //If projected dist <= extent on each axis then it's inside
        if (Math.Abs(coeff) > m_extents[i])
          return ContainmentType.Outside;
      }

      return ContainmentType.Inside;
    }

    /// <summary>
    /// Determines if the specified segment line is contained inside the bounding volume.
    /// </summary>
    /// <param name="line">Segment to test against.</param>
    /// <returns>Type of containment.</returns>
    public override ContainmentType Contains(in Segment line)
    {
      //If both end points are inside bounding box, then it is wholly contained
      if (Contains(line.StartPoint) == ContainmentType.Inside && Contains(line.EndPoint) == ContainmentType.Inside)
        return ContainmentType.Inside;

      if (GeometricToolsHelper.IntersectSegmentBox(line, m_center, m_axes.XAxis, m_axes.YAxis, m_axes.ZAxis, m_extents))
        return ContainmentType.Intersects;

      return ContainmentType.Outside;
    }

    /// <summary>
    /// Determines if the specified triangle is contained inside the bounding volume.
    /// </summary>
    /// <param name="triangle">Triangle to test against.</param>
    /// <returns>Type of containment.</returns>
    public override ContainmentType Contains(in Triangle triangle)
    {
      //If all three vertices are inside bounding box, then it is wholly contained
      if (Contains(triangle.PointA) == ContainmentType.Inside && Contains(triangle.PointB) == ContainmentType.Inside &&
          Contains(triangle.PointC) == ContainmentType.Inside)
        return ContainmentType.Inside;

      if (GeometricToolsHelper.IntersectTriangleBox(triangle, m_center, m_axes.XAxis, m_axes.YAxis, m_axes.ZAxis, m_extents))
        return ContainmentType.Intersects;

      return ContainmentType.Outside;
    }

    /// <summary>
    /// Determines if the specified ellipse intersects with the bounding volume.
    /// </summary>
    /// <param name="ellipse">Ellipse to test against.</param>
    /// <returns>True if the bounding volume intersects with the ellipse, false otherwise.</returns>
    public override ContainmentType Contains(in Ellipse ellipse)
    {
      //TODO
      throw new NotImplementedException();
    }

    /// <summary>
    /// Determines if the specified bounding volume is contained inside the bounding volume.
    /// </summary>
    /// <param name="volume">Bounding volume to test against.</param>
    /// <returns>Type of containment.</returns>
    public override ContainmentType Contains(BoundingVolume? volume)
    {
      if (volume is null)
        return ContainmentType.Outside;

      switch (volume.BoundingType)
      {
        case BoundingType.AxisAlignedBoundingBox:
          {
            BoundingBox other = (volume as BoundingBox)!;
            Vector3 boxCenter = other.Center;
            Vector3 boxExtents = other.Extents;

            Vector3 xAxis = Vector3.UnitX;
            Vector3 yAxis = Vector3.UnitY;
            Vector3 zAxis = Vector3.UnitZ;

            return GeometricToolsHelper.BoxContainsBox(m_center, m_axes.XAxis, m_axes.YAxis, m_axes.ZAxis, m_extents,
                   boxCenter, xAxis, yAxis, zAxis, boxExtents);
          }
        case BoundingType.Sphere:
          {
            BoundingSphere other = (volume as BoundingSphere)!;
            Vector3 sphereCenter = other.Center;
            float sphereRadius = other.Radius;

            return GeometricToolsHelper.BoxContainsSphere(m_center, m_axes.XAxis, m_axes.YAxis, m_axes.ZAxis, m_extents, sphereCenter, sphereRadius);
          }
        case BoundingType.Capsule:
          {
            BoundingCapsule other = (volume as BoundingCapsule)!;
            Segment centerLine = other.CenterLine;
            float capsuleRadius = other.Radius;

            return GeometricToolsHelper.BoxContainsCapsule(m_center, m_axes.XAxis, m_axes.YAxis, m_axes.ZAxis, m_extents, centerLine, capsuleRadius);
          }
        case BoundingType.OrientedBoundingBox:
          {
            OrientedBoundingBox other = (volume as OrientedBoundingBox)!;

            return GeometricToolsHelper.BoxContainsBox(m_center, m_axes.XAxis, m_axes.YAxis, m_axes.ZAxis, m_extents,
                    other.m_center, other.m_axes.XAxis, other.m_axes.YAxis, other.m_axes.ZAxis, other.m_extents);
          }
        case BoundingType.Frustum:
        case BoundingType.Mesh:
        default:
          {
            return Contains(volume.Corners);
          }
      }
    }

    /// <summary>
    /// Tests if the specified ray intersects with the bounding volume.
    /// </summary>
    /// <param name="ray">Ray to test against.</param>
    /// <returns>True if the bounding volume intersects with the ray, false otherwise.</returns>
    public override bool Intersects(in Ray ray)
    {
      return GeometricToolsHelper.IntersectRayBox(ray, m_center, m_axes.XAxis, m_axes.YAxis, m_axes.ZAxis, m_extents);
    }

    /// <summary>
    /// Determines if the specified ray intersects with the bounding volume.
    /// </summary>
    /// <param name="ray">Ray to test against.</param>
    /// <param name="result">Bounding intersection result.</param>
    /// <returns>True if the bounding volume intersects with the ray, false otherwise.</returns>
    public override bool Intersects(in Ray ray, out BoundingIntersectionResult result)
    {
      return GeometricToolsHelper.IntersectRayBox(ray, m_center, m_axes.XAxis, m_axes.YAxis, m_axes.ZAxis, m_extents, out result);
    }

    /// <summary>
    /// Determines if the specified segment line intersects with the bounding volume.
    /// </summary>
    /// <param name="line">Segment line to test against.</param>
    /// <returns>True if the bounding volume intersects with the segment line, false otherweise.</returns>
    public override bool Intersects(in Segment line)
    {
      return GeometricToolsHelper.IntersectSegmentBox(line, m_center, m_axes.XAxis, m_axes.YAxis, m_axes.ZAxis, m_extents);
    }

    /// <summary>
    /// Determines if the specified segment line intersects with the bounding volume.
    /// </summary>
    /// <param name="line">Segment line to test against.</param>
    /// <param name="result">Bounding intersection result.</param>
    /// <returns>True if the bounding volume intersects with the segment line, false otherweise.</returns>
    public override bool Intersects(in Segment line, out BoundingIntersectionResult result)
    {
      return GeometricToolsHelper.IntersectSegmentBox(line, m_center, m_axes.XAxis, m_axes.YAxis, m_axes.ZAxis, m_extents, out result);
    }

    /// <summary>
    /// Tests if the specified plane intersects with the bounding volume.
    /// </summary>
    /// <param name="plane">Plane to test against.</param>
    /// <returns>Type of plane intersection.</returns>
    public override PlaneIntersectionType Intersects(in Plane plane)
    {
      float xRadius = Vector3.Dot(plane.Normal, m_axes.XAxis) * m_extents.X;
      float yRadius = Vector3.Dot(plane.Normal, m_axes.YAxis) * m_extents.Y;
      float zRadius = Vector3.Dot(plane.Normal, m_axes.ZAxis) * m_extents.Z;

      float radius = Math.Abs(xRadius) + Math.Abs(yRadius) + Math.Abs(zRadius);
      float signedDistance = plane.SignedDistanceTo(m_center);

      if (signedDistance > radius)
      {
        return PlaneIntersectionType.Front;
      }
      else if (signedDistance < -radius)
      {
        return PlaneIntersectionType.Back;
      }
      else
      {
        return PlaneIntersectionType.Intersects;
      }
    }

    /// <summary>
    /// Determines if the specified bounding volume intersects with the bounding volume.
    /// </summary>
    /// <param name="volume">Other bounding volume to test against.</param>
    /// <returns> True if the two volumes intersect with one another, false otherwise.</returns>
    public override bool Intersects(BoundingVolume? volume)
    {
      if (volume is null)
        return false;

      switch (volume.BoundingType)
      {
        case BoundingType.AxisAlignedBoundingBox:
          {
            BoundingBox other = (volume as BoundingBox)!;
            Vector3 boxCenter = other.Center;
            Vector3 boxExtents = other.Extents;

            Vector3 unitXAxis = Vector3.UnitX;
            Vector3 unitYAxis = Vector3.UnitY;
            Vector3 unitZAxis = Vector3.UnitZ;

            return GeometricToolsHelper.IntersectBoxBox(m_center, m_axes.XAxis, m_axes.YAxis, m_axes.ZAxis, m_extents,
                    boxCenter, unitXAxis, unitYAxis, unitZAxis, boxExtents);
          }
        case BoundingType.Sphere:
          {
            BoundingSphere other = (volume as BoundingSphere)!;
            Vector3 sphereCenter = other.Center;
            float sphereRadius = other.Radius;

            return GeometricToolsHelper.IntersectSphereBox(sphereCenter, sphereRadius, m_center, m_axes.XAxis, m_axes.YAxis, m_axes.ZAxis, m_extents);
          }
        case BoundingType.Capsule:
          {
            BoundingCapsule other = (volume as BoundingCapsule)!;
            Segment centerLine = other.CenterLine;
            float capsuleRadius = other.Radius;

            return GeometricToolsHelper.IntersectCapsuleBox(centerLine, capsuleRadius, m_center, m_axes.XAxis, m_axes.YAxis, m_axes.ZAxis, m_extents);
          }
        case BoundingType.OrientedBoundingBox:
          {
            OrientedBoundingBox other = (volume as OrientedBoundingBox)!;

            return GeometricToolsHelper.IntersectBoxBox(m_center, m_axes.XAxis, m_axes.YAxis, m_axes.ZAxis, m_extents,
                    other.m_center, other.m_axes.XAxis, other.m_axes.YAxis, other.m_axes.ZAxis, other.m_extents);
          }
        case BoundingType.Frustum:
        case BoundingType.Mesh:
          {
            return volume.Intersects(this);
          }
        default:
          return IntersectsGeneral(volume);
      }
    }

    /// <summary>
    /// Merges the bounding volume with the specified bounding volume, resulting in a volume that encloses both.
    /// </summary>
    /// <param name="volume">Bounding volume to merge with.</param>
    public override void Merge(BoundingVolume? volume)
    {
      if (volume is null)
        return;

      //Treat every other volume as an OBB and merge with that
      OrientedBoundingBox.Data b0;
      b0.Center = m_center;
      b0.Axes = m_axes;
      b0.Extents = m_extents;

      OrientedBoundingBox.Data b1, merged;

      switch (volume.BoundingType)
      {
        case BoundingType.AxisAlignedBoundingBox:
          {
            BoundingBox other = (volume as BoundingBox)!;

            b1.Center = other.Center;
            b1.Axes = Triad.UnitAxes;
            b1.Extents = other.Extents;
          }
          break;
        case BoundingType.Sphere:
          {
            BoundingSphere other = (volume as BoundingSphere)!;
            float sphereRadius = other.Radius;

            b1.Center = other.Center;
            b1.Axes = Triad.UnitAxes;
            b1.Extents = new Vector3(sphereRadius, sphereRadius, sphereRadius);
          }
          break;
        case BoundingType.Capsule:
          {
            BoundingCapsule other = (volume as BoundingCapsule)!;
            Segment centerLine = other.CenterLine;
            float capsuleRadius = other.Radius;

            Vector3 segCenter, segDir;
            float segExtent;
            GeometricToolsHelper.CalculateSegmentProperties(centerLine, out segCenter, out segDir, out segExtent);

            b1.Center = segCenter;
            Triad.FromYComplementBasis(segDir, out b1.Axes);
            b1.Extents = new Vector3(capsuleRadius, capsuleRadius, segExtent + capsuleRadius);
          }
          break;
        case BoundingType.OrientedBoundingBox:
          {
            OrientedBoundingBox other = (volume as OrientedBoundingBox)!;

            b1.Center = other.m_center;
            b1.Axes = other.m_axes;
            b1.Extents = other.m_extents;
          }
          break;
        case BoundingType.Frustum:
        case BoundingType.Mesh:
        default:
          {
            OrientedBoundingBox.Data.FromPoints(volume.Corners, out b1);
          }
          break;
      }

      //We get a better fit by recomputing an OBB using the corners. Might be something wrong in GaussPointsFit...
      GeometricToolsHelper.MergeBoxBoxUsingCorners(b0, b1, out merged);

      m_center = merged.Center;
      m_axes = merged.Axes;
      m_extents = merged.Extents;
      UpdateCorners = true;
    }

    /// <summary>
    /// Transforms the bounding volume by a Scale-Rotation-Translation (SRT) transform.
    /// </summary>
    /// <param name="scale">Scaling</param>
    /// <param name="rotation">Rotation</param>
    /// <param name="translation">Translation</param>
    public override void Transform(in Vector3 scale, in Quaternion rotation, in Vector3 translation)
    {
      //Transform center by the SRT
      Vector3 center;
      Vector3.Multiply(m_center, scale, out center);
      Vector3.Transform(center, rotation, out center);
      Vector3.Add(translation, center, out center);

      m_center = center;

      //Scale extents
      m_extents.X = Math.Abs(Math.Abs(scale.X) * m_extents.X);
      m_extents.Y = Math.Abs(Math.Abs(scale.Y) * m_extents.Y);
      m_extents.Z = Math.Abs(Math.Abs(scale.Z) * m_extents.Z);

      //Transform axes
      Triad.Transform(m_axes, rotation, out m_axes);

      UpdateCorners = true;
    }

    /// <summary>
    /// Tests equality between the bounding volume and the other bounding volume.
    /// </summary>
    /// <param name="other">Other bounding volume</param>
    /// <param name="tolerance">Tolerance</param>
    /// <returns>True if the volume is of same type and same size/shape, false otherwise.</returns>
    public override bool Equals([NotNullWhen(true)] BoundingVolume? other, float tolerance)
    {
      OrientedBoundingBox? obb = other as OrientedBoundingBox;
      if (obb is not null)
        return m_center.Equals(obb.m_center, tolerance) && m_axes.Equals(obb.m_axes, tolerance) && m_extents.Equals(obb.m_extents, tolerance);

      return false;
    }

    /// <summary>
    /// Reads the object data from the input.
    /// </summary>
    /// <param name="input">Savable reader</param>
    public override void Read(ISavableReader input)
    {
      input.Read<Vector3>("Center", out m_center);
      input.Read<Triad>("Axes", out m_axes);
      input.Read<Vector3>("Extents", out m_extents);
      UpdateCorners = true;
    }

    /// <summary>
    /// Writes the object data to the output.
    /// </summary>
    /// <param name="output">Savable writer</param>
    public override void Write(ISavableWriter output)
    {
      output.Write<Vector3>("Center", m_center);
      output.Write<Triad>("Axes", m_axes);
      output.Write<Vector3>("Extents", m_extents);
    }

    /// <summary>
    /// Computes the corners that represent the extremal points of this bounding volume.
    /// </summary>
    /// <param name="corners">Databuffer to contain the points, length equal to the corner count.</param>
    /// <exception cref="System.ArgumentOutOfRangeException">Thrown if the corner buffer does not have enough space.</exception>
    protected override void ComputeCorners(Span<Vector3> corners)
    {
      if (corners.Length < CornerCount)
        throw new ArgumentOutOfRangeException(StringLocalizer.Instance.GetLocalizedString("NotEnoughSpaceForBoundingCorners", CornerCount.ToString()));

      Vector3 exAxis;
      Vector3 eyAxis;
      Vector3 ezAxis;

      Vector3.Multiply(m_axes.XAxis, m_extents.X, out exAxis);
      Vector3.Multiply(m_axes.YAxis, m_extents.Y, out eyAxis);
      Vector3.Multiply(m_axes.ZAxis, m_extents.Z, out ezAxis);

      ref Vector3 temp = ref corners[0];
      Vector3.Subtract(m_center, exAxis, out temp);
      Vector3.Subtract(temp, eyAxis, out temp);
      Vector3.Subtract(temp, ezAxis, out temp);

      temp = ref corners[1];
      Vector3.Add(m_center, exAxis, out temp);
      Vector3.Subtract(temp, eyAxis, out temp);
      Vector3.Subtract(temp, ezAxis, out temp);

      temp = ref corners[2];
      Vector3.Add(m_center, exAxis, out temp);
      Vector3.Add(temp, eyAxis, out temp);
      Vector3.Subtract(temp, ezAxis, out temp);

      temp = ref corners[3];
      Vector3.Subtract(m_center, exAxis, out temp);
      Vector3.Add(temp, eyAxis, out temp);
      Vector3.Subtract(temp, ezAxis, out temp);

      temp = ref corners[4];
      Vector3.Add(m_center, exAxis, out temp);
      Vector3.Subtract(temp, eyAxis, out temp);
      Vector3.Add(temp, ezAxis, out temp);

      temp = ref corners[5];
      Vector3.Subtract(m_center, exAxis, out temp);
      Vector3.Subtract(temp, eyAxis, out temp);
      Vector3.Add(temp, ezAxis, out temp);

      temp = ref corners[6];
      Vector3.Add(m_center, exAxis, out temp);
      Vector3.Add(temp, eyAxis, out temp);
      Vector3.Add(temp, ezAxis, out temp);

      temp = ref corners[7];
      Vector3.Subtract(m_center, exAxis, out temp);
      Vector3.Add(temp, eyAxis, out temp);
      Vector3.Add(temp, ezAxis, out temp);
    }

    #region OrientedBoundingBox Data

    /// <summary>
    /// Represents data for a <see cref="OrientedBoundingBox"/>.
    /// </summary>
    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 4)]
    public struct Data : IEquatable<OrientedBoundingBox.Data>, IRefEquatable<OrientedBoundingBox.Data>, IFormattable, IPrimitiveValue
    {
      /// <summary>
      /// Center of the bounding box.
      /// </summary>
      public Vector3 Center;

      /// <summary>
      /// Axes of the bounding box.
      /// </summary>
      public Triad Axes;

      /// <summary>
      /// Extents of the bounding box (half-lengths along each axis from the center).
      /// </summary>
      public Vector3 Extents;

      /// <summary>
      /// Constructs a new instance of the <see cref="OrientedBoundingBox.Data"/> struct.
      /// </summary>
      /// <param name="center">Center of the bounding box.</param>
      /// <param name="xAxis">X-Axis of the bounding box.</param>
      /// <param name="yAxis">Y-Axis of the bounding box.</param>
      /// <param name="zAxis">Z-Axis of the bounding box.</param>
      /// <param name="extents">Extents of the bounding box.</param>
      public Data(in Vector3 center, in Vector3 xAxis, in Vector3 yAxis, in Vector3 zAxis, in Vector3 extents)
      {
        Center = center;
        Axes.XAxis = xAxis;
        Axes.YAxis = yAxis;
        Axes.ZAxis = zAxis;
        Extents = extents;
      }

      /// <summary>
      /// Constructs a new instance of the <see cref="OrientedBoundingBox.Data"/> struct.
      /// </summary>
      /// <param name="center">Center of the bounding box.</param>
      /// <param name="axes">Axes of the bounding box.</param>
      /// <param name="extents">Extents of the bounding box.</param>
      public Data(in Vector3 center, in Triad axes, in Vector3 extents)
      {
        Center = center;
        Axes = axes;
        Extents = extents;
      }

      /// <summary>
      /// Constructs a new instance of the <see cref="OrientedBoundingBox.Data"/> struct.
      /// </summary>
      /// <param name="box">Oriented bounding box to copy from.</param>
      public Data(OrientedBoundingBox box)
      {
        ArgumentNullException.ThrowIfNull(box, nameof(box));

        Center = box.m_center;
        Axes = box.m_axes;
        Extents = box.m_extents;
      }

      /// <summary>
      /// Computes a minimum bounding volume that encloses the specified points in space.
      /// </summary>
      /// <param name="points">Points in space</param>
      /// <param name="subMeshRange">Optional range inside the buffer that represents the mesh to compute bounds for. Note: the base vertex offset will be ignored.</param>
      /// <returns>Computed oriented bounding box.</returns>
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static OrientedBoundingBox.Data FromPoints(ReadOnlySpan<Vector3> points, in SubMeshRange? subMeshRange = null)
      {
        OrientedBoundingBox.Data result;
        FromPoints(points, null, out result);

        return result;
      }

      /// <summary>
      /// Computes a minimum bounding volume that encloses the specified points in space.
      /// </summary>
      /// <param name="points">Points in space</param>
      /// <param name="result">Computed oriented bounding box.</param>
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static void FromPoints(ReadOnlySpan<Vector3> points, out OrientedBoundingBox.Data result)
      {
        FromPoints(points, null, out result);
      }

      /// <summary>
      /// Computes a minimum bounding volume that encloses the specified points in space.
      /// </summary>
      /// <param name="points">Points in space</param>
      /// <param name="subMeshRange">Optional range inside the buffer that represents the mesh to compute bounds for. Note: the base vertex offset will be ignored.</param>
      /// <param name="result">Computed oriented bounding box.</param>
      public static void FromPoints(ReadOnlySpan<Vector3> points, in SubMeshRange? subMeshRange, out OrientedBoundingBox.Data result)
      {
        int offset;
        int count;

        if (!BoundingVolume.ExtractSubMeshRange(points, subMeshRange, out offset, out count))
        {
          result = new OrientedBoundingBox.Data();
          return;
        }

        ref readonly Vector3 firstPt = ref points[offset];

        //Trivial case
        if (count == 1)
        {
          result.Center = firstPt;
          result.Axes = Triad.UnitAxes;
          result.Extents = Vector3.Zero;
          return;
        }

        GeometricToolsHelper.GaussPointsFit(points.Slice(offset, count), out result);

        Vector3 diff, pMin, pMax;
        Vector3.Subtract(firstPt, result.Center, out diff);

        pMin.X = Vector3.Dot(diff, result.Axes.XAxis);
        pMin.Y = Vector3.Dot(diff, result.Axes.YAxis);
        pMin.Z = Vector3.Dot(diff, result.Axes.ZAxis);
        pMax = pMin;

        int upperBoundExclusive = offset + count;
        for (int i = offset + 1; i < upperBoundExclusive; i++)
        {
          ref readonly Vector3 pt = ref points[i];
          Vector3.Subtract(pt, result.Center, out diff);
          for (int j = 0; j < 3; j++)
          {
            Vector3 axis;
            result.Axes.GetAxis(j, out axis);

            float dot = Vector3.Dot(diff, axis);

            if (dot < pMin[j])
            {
              pMin[j] = dot;
            }
            else if (dot > pMax[j])
            {
              pMax[j] = dot;
            }
          }
        }

        //Calculate new center
        for (int j = 0; j < 3; j++)
        {
          Vector3 temp;
          Vector3 axis = result.Axes[j];
          Vector3.Multiply(axis, 0.5f * (pMin[j] + pMax[j]), out temp);
          Vector3.Add(result.Center, temp, out result.Center);
        }

        result.Extents.X = 0.5f * (pMax[0] - pMin[0]);
        result.Extents.Y = 0.5f * (pMax[1] - pMin[1]);
        result.Extents.Z = 0.5f * (pMax[2] - pMin[2]);
      }

      /// <summary>
      /// Computes a minimum bounding volume that encloses the specified indexed points in space.
      /// </summary>
      /// <param name="points">Points in space.</param>
      /// <param name="indices">Point indices denoting location in the point buffer.</param>
      /// <param name="subMeshRange">Optional range in the index buffer.</param>
      /// <returns>Computed oriented bounding box.</returns>
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static OrientedBoundingBox.Data FromIndexedPoints(ReadOnlySpan<Vector3> points, IndexData indices, in SubMeshRange? subMeshRange = null)
      {
        OrientedBoundingBox.Data result;
        FromIndexedPoints(points, indices, subMeshRange, out result);

        return result;
      }

      /// <summary>
      /// Computes a minimum bounding volume that encloses the specified indexed points in space.
      /// </summary>
      /// <param name="points">Points in space.</param>
      /// <param name="indices">Point indices denoting location in the point buffer.</param>
      /// <param name="result">Computed oriented bounding box.</param>
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static void FromIndexedPoints(ReadOnlySpan<Vector3> points, IndexData indices, out OrientedBoundingBox.Data result)
      {
        FromIndexedPoints(points, indices, null, out result);
      }

      /// <summary>
      /// Computes a minimum bounding volume that encloses the specified indexed points in space.
      /// </summary>
      /// <param name="points">Points in space.</param>
      /// <param name="indices">Point indices denoting location in the point buffer.</param>
      /// <param name="subMeshRange">Optional range in the index buffer.</param>
      /// <param name="result">Computed oriented bounding box.</param>
      public static void FromIndexedPoints(ReadOnlySpan<Vector3> points, IndexData indices, in SubMeshRange? subMeshRange, out OrientedBoundingBox.Data result)
      {
        int offset;
        int count;
        int baseVertexOffset;

        if (!BoundingVolume.ExtractSubMeshRange(points, indices, subMeshRange, out offset, out count, out baseVertexOffset))
        {
          result = new OrientedBoundingBox.Data();
          return;
        }

        int index = indices[offset];
        ref readonly Vector3 firstPt = ref points[index + baseVertexOffset];

        //Trivial case
        if (count == 1)
        {
          result.Center = firstPt;
          result.Axes = Triad.UnitAxes;
          result.Extents = Vector3.Zero;
          return;
        }

        GeometricToolsHelper.GaussPointsFit(points, indices, offset, count, baseVertexOffset, out result);

        Vector3 diff, pMin, pMax;
        Vector3.Subtract(firstPt, result.Center, out diff);

        pMin.X = Vector3.Dot(diff, result.Axes.XAxis);
        pMin.Y = Vector3.Dot(diff, result.Axes.YAxis);
        pMin.Z = Vector3.Dot(diff, result.Axes.ZAxis);
        pMax = pMin;

        int upperBoundExclusive = offset + count;
        for (int i = offset + 1; i < upperBoundExclusive; i++)
        {
          index = indices[i];
          ref readonly Vector3 pt = ref points[index + baseVertexOffset];
          Vector3.Subtract(pt, result.Center, out diff);
          for (int j = 0; j < 3; j++)
          {
            Vector3 axis;
            result.Axes.GetAxis(j, out axis);

            float dot = Vector3.Dot(diff, axis);

            if (dot < pMin[j])
            {
              pMin[j] = dot;
            }
            else if (dot > pMax[j])
            {
              pMax[j] = dot;
            }
          }
        }

        //Calculate new center
        for (int j = 0; j < 3; j++)
        {
          Vector3 temp;
          Vector3 axis = result.Axes[j];
          Vector3.Multiply(axis, 0.5f * (pMin[j] + pMax[j]), out temp);
          Vector3.Add(result.Center, temp, out result.Center);
        }

        result.Extents.X = 0.5f * (pMax[0] - pMin[0]);
        result.Extents.Y = 0.5f * (pMax[1] - pMin[1]);
        result.Extents.Z = 0.5f * (pMax[2] - pMin[2]);
      }

      #region OBB Calculations

      //TODO - Eberly's Min box implementation using Convex Hulls
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      internal static unsafe void ComputeMinBox(Vector3* points, int numPoints, IndexData? indices, out OrientedBoundingBox.Data result)
      {
        throw new NotImplementedException();
      }

      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      internal static void ComputeCorners(in OrientedBoundingBox.Data box, Span<Vector3> points)
      {
        Vector3 exAxis;
        Vector3 eyAxis;
        Vector3 ezAxis;

        Vector3.Multiply(box.Axes.XAxis, box.Extents.X, out exAxis);
        Vector3.Multiply(box.Axes.YAxis, box.Extents.Y, out eyAxis);
        Vector3.Multiply(box.Axes.ZAxis, box.Extents.Z, out ezAxis);

        Vector3 temp;

        Vector3.Subtract(box.Center, exAxis, out temp);
        Vector3.Subtract(temp, eyAxis, out temp);
        Vector3.Subtract(temp, ezAxis, out temp);
        points[0] = temp;

        Vector3.Add(box.Center, exAxis, out temp);
        Vector3.Subtract(temp, eyAxis, out temp);
        Vector3.Subtract(temp, ezAxis, out temp);
        points[1] = temp;

        Vector3.Add(box.Center, exAxis, out temp);
        Vector3.Add(temp, eyAxis, out temp);
        Vector3.Subtract(temp, ezAxis, out temp);
        points[2] = temp;

        Vector3.Subtract(box.Center, exAxis, out temp);
        Vector3.Add(temp, eyAxis, out temp);
        Vector3.Subtract(temp, ezAxis, out temp);
        points[3] = temp;

        Vector3.Add(box.Center, exAxis, out temp);
        Vector3.Subtract(temp, eyAxis, out temp);
        Vector3.Add(temp, ezAxis, out temp);
        points[4] = temp;

        Vector3.Subtract(box.Center, exAxis, out temp);
        Vector3.Subtract(temp, eyAxis, out temp);
        Vector3.Add(temp, ezAxis, out temp);
        points[5] = temp;

        Vector3.Add(box.Center, exAxis, out temp);
        Vector3.Add(temp, eyAxis, out temp);
        Vector3.Add(temp, ezAxis, out temp);
        points[6] = temp;

        Vector3.Subtract(box.Center, exAxis, out temp);
        Vector3.Add(temp, eyAxis, out temp);
        Vector3.Add(temp, ezAxis, out temp);
        points[7] = temp;
      }

      #endregion

      /// <summary>
      /// Determines whether the specified <see cref="System.Object" /> is equal to this instance.
      /// </summary>
      /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
      /// <returns>True if the specified <see cref="System.Object" /> is equal to this instance; otherwise, false.</returns>
      public override bool Equals([NotNullWhen(true)] object? obj)
      {
        if (obj is OrientedBoundingBox.Data)
          return Equals((OrientedBoundingBox.Data) obj);

        return false;
      }

      /// <summary>
      /// Tests equality between this oriented bounding box and another.
      /// </summary>
      /// <param name="other">Oriented bounding box</param>
      /// <returns>True if equal, false otherwise.</returns>
      bool IEquatable<OrientedBoundingBox.Data>.Equals(OrientedBoundingBox.Data other)
      {
        return Center.Equals(other.Center) && Axes.Equals(other.Axes) && Extents.Equals(other.Extents);
      }

      /// <summary>
      /// Tests equality between this oriented bounding box and another.
      /// </summary>
      /// <param name="other">Oriented bounding box</param>
      /// <returns>True if equal, false otherwise.</returns>
      public bool Equals(in OrientedBoundingBox.Data other)
      {
        return Center.Equals(other.Center) && Axes.Equals(other.Axes) && Extents.Equals(other.Extents);
      }

      /// <summary>
      /// Tests equality between this oriented bounding box and another.
      /// </summary>
      /// <param name="other">Oriented bounding box</param>
      /// <param name="tolerance">Tolerance</param>
      /// <returns>True if equal, false otherwise.</returns>
      public bool Equals(in OrientedBoundingBox.Data other, float tolerance)
      {
        return Center.Equals(other.Center, tolerance) && Axes.Equals(other.Axes, tolerance) && Extents.Equals(other.Extents, tolerance);
      }

      /// <summary>
      /// Returns a hash code for this instance.
      /// </summary>
      /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.</returns>
      public override int GetHashCode()
      {
        unchecked
        {
          return Center.GetHashCode() + Axes.GetHashCode() + Extents.GetHashCode();
        }
      }

      /// <summary>
      /// Returns a <see cref="System.String" /> that represents this instance.
      /// </summary>
      /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
      public override string ToString()
      {
        CultureInfo info = CultureInfo.CurrentCulture;
        return String.Format(info, "Center: {0}, {1}, Extents: {2}",
            new Object[] { Center.ToString(info), Axes.ToString(info), Extents.ToString(info) });
      }

      /// <summary>
      /// Returns a <see cref="System.String" /> that represents this instance.
      /// </summary>
      /// <param name="format">The format</param>
      /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
      public string ToString(string? format)
      {
        if (format is null)
          return ToString();

        CultureInfo info = CultureInfo.CurrentCulture;
        return String.Format(info, "Center: {0}, {1}, Extents: {2}",
            new Object[] { Center.ToString(format, info), Axes.ToString(format, info), Extents.ToString(format, info) });
      }

      /// <summary>
      /// Returns a <see cref="System.String" /> that represents this instance.
      /// </summary>
      /// <param name="formatProvider">The format provider.</param>
      /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
      public string ToString(IFormatProvider? formatProvider)
      {
        if (formatProvider is null)
          return ToString();

        return String.Format(formatProvider, "Center: {0}, {1}, Extents: {2}",
            new Object[] { Center.ToString(formatProvider), Axes.ToString(formatProvider), Extents.ToString(formatProvider) });
      }

      /// <summary>
      /// Returns a <see cref="System.String" /> that represents this instance.
      /// </summary>
      /// <param name="format">The format.</param>
      /// <param name="formatProvider">The format provider.</param>
      /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
      public string ToString(string? format, IFormatProvider? formatProvider)
      {
        if (formatProvider is null)
          return ToString();

        if (format is null)
          return ToString(formatProvider);

        return String.Format(formatProvider, "Center: {0}, {1}, Extents: {2}",
            new Object[] { Center.ToString(format, formatProvider), Axes.ToString(format, formatProvider), Extents.ToString(format, formatProvider) });
      }

      /// <summary>
      /// Writes the primitive data to the output.
      /// </summary>
      /// <param name="output">Primitive writer.</param>
      readonly void IPrimitiveValue.Write(IPrimitiveWriter output)
      {
        output.Write<Vector3>("Center", Center);
        output.Write<Triad>("Axes", Axes);
        output.Write<Vector3>("Extents", Extents);
      }

      /// <summary>
      /// Reads the primitive data from the input.
      /// </summary>
      /// <param name="input">Primitive reader.</param>
      void IPrimitiveValue.Read(IPrimitiveReader input)
      {
        input.Read<Vector3>("Center", out Center);
        input.Read<Triad>("Axes", out Axes);
        input.Read<Vector3>("Extents", out Extents);
      }
    }

    #endregion
  }
}
