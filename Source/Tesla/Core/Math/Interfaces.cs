﻿/*
* Copyright (c) 2010-2024 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System.Collections.Generic;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Represents an object that can participate in picking operations.
  /// </summary>
  public interface IPickable
  {
    /// <summary>
    /// Gets the absolute world bounding of the object.
    /// </summary>
    BoundingVolume WorldBounding { get; }

    /// <summary>
    /// Performs a ray-mesh intersection test.
    /// </summary>
    /// <param name="ray">Ray to test against.</param>
    /// <param name="results">List of results to add to.</param>
    /// <param name="ignoreBackfaces">True if backfaces (relative to the pick ray) should be ignored, false if they should be considered a result.</param>
    /// <returns>True if an intersection occured and results were added to the output list, false if no intersection occured.</returns>
    bool IntersectsMesh(in Ray ray, IRefList<Pair<LineIntersectionResult, Triangle?>> results, bool ignoreBackfaces);
  }
}
