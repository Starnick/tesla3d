﻿/*
* Copyright (c) 2010-2024 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Content;
using System.Globalization;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.CompilerServices;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Defines a range of vertices or indexed vertices that make up a mesh. 
  /// </summary>
  public struct SubMeshRange : IEquatable<SubMeshRange>, IRefEquatable<SubMeshRange>, IPrimitiveValue
  {
    private int m_offset;
    private int m_count;
    private int m_baseVertexOffset;

    /// <summary>
    /// Gets the offset in the vertex/index buffer to start reading at.
    /// </summary>
    public readonly int Offset
    {
      get
      {
        return m_offset;
      }
    }

    /// <summary>
    /// Gets the number of vertices/indices to read.
    /// </summary>
    public readonly int Count
    {
      get
      {
        return m_count;
      }
    }

    /// <summary>
    /// Gets the base vertex offset at which to start reading vertices from (indexed meshes only). This is added
    /// to the vertex index in the index buffer, allowing multiple meshes to be contained in a single buffer.
    /// </summary>
    public readonly int BaseVertexOffset
    {
      get
      {
        return m_baseVertexOffset;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="SubMeshRange"/> struct.
    /// </summary>
    /// <param name="offset">Offset in the buffer to start reading at.</param>
    /// <param name="count">Number of elements to read.</param>
    public SubMeshRange(int offset, int count)
    {
      m_offset = offset;
      m_count = count;
      m_baseVertexOffset = 0;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="SubMeshRange"/> struct.
    /// </summary>
    /// <param name="offset">Offset in the buffer to start reading at.</param>
    /// <param name="count">Number of elements to read.</param>
    /// <param name="baseVertexOffset">Base vertex offset to add to each index (Only for indexed meshes).</param>
    public SubMeshRange(int offset, int count, int baseVertexOffset)
    {
      m_offset = offset;
      m_count = count;
      m_baseVertexOffset = baseVertexOffset;
    }

    /// <summary>
    /// Tests equality between two submesh ranges.
    /// </summary>
    /// <param name="a">First submesh range</param>
    /// <param name="b">Second submesh range</param>
    /// <returns>True if both submesh ranges are equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator ==(SubMeshRange a, SubMeshRange b)
    {
      return a.m_offset == b.m_offset && a.m_count == b.m_count && a.m_baseVertexOffset == b.m_baseVertexOffset;
    }

    /// <summary>
    /// Tests inequality between two submesh ranges.
    /// </summary>
    /// <param name="a">First submesh range</param>
    /// <param name="b">Second submesh range</param>
    /// <returns>True if both submesh ranges are not equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator !=(SubMeshRange a, SubMeshRange b)
    {
      return a.m_offset != b.m_offset || a.m_count != b.m_count || a.m_baseVertexOffset != b.m_baseVertexOffset;
    }

    /// <summary>
    /// Tests equality between the submeshrange and another submeshrange.
    /// </summary>
    /// <param name="other">Submeshrange to test against</param>
    /// <returns>True if components are equal, false otherwise.</returns>
    readonly bool IEquatable<SubMeshRange>.Equals(SubMeshRange other)
    {
      return m_offset == other.m_offset && m_count == other.m_count && m_baseVertexOffset == other.m_baseVertexOffset;
    }

    /// <summary>
    /// Tests equality between the submeshrange and another submeshrange.
    /// </summary>
    /// <param name="other">Submeshrange to test against</param>
    /// <returns>True if components are equal, false otherwise.</returns>
    public readonly bool Equals(in SubMeshRange other)
    {
      return m_offset == other.m_offset && m_count == other.m_count && m_baseVertexOffset == other.m_baseVertexOffset;
    }

    /// <summary>
    /// Determines whether the specified <see cref="System.Object" /> is equal to this instance.
    /// </summary>
    /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
    /// <returns>True if the specified <see cref="System.Object" /> is equal to this instance; otherwise, false.</returns>
    public readonly override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is SubMeshRange)
        return Equals((SubMeshRange) obj);

      return false;
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
    public readonly override int GetHashCode()
    {
      return base.GetHashCode();
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly override string ToString()
    {
      CultureInfo info = CultureInfo.CurrentCulture;
      return String.Format(info, "Offset: {0}, Count: {1}, BaseVertexOffset: {2}",
          new Object[] { Offset.ToString(info), Count.ToString(info), BaseVertexOffset.ToString(info) });
    }

    /// <summary>
    /// Writes the primitive data to the output.
    /// </summary>
    /// <param name="output">Primitive writer.</param>
    public readonly void Write(IPrimitiveWriter output)
    {
      output.Write("Offset", m_offset);
      output.Write("Count", m_count);
      output.Write("BaseVertexOffset", m_baseVertexOffset);
    }

    /// <summary>
    /// Reads the primitive data from the input.
    /// </summary>
    /// <param name="input">Primitive reader.</param>
    public void Read(IPrimitiveReader input)
    {
      m_offset = input.ReadInt32("Offset");
      m_count = input.ReadInt32("Count");
      m_baseVertexOffset = input.ReadInt32("BaseVertexOffset");
    }
  }
}
