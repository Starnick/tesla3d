﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using Tesla.Content;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// The Transform class represents a 3D SRT (scaling/rotation/translation) matrix. 
  /// Generally this class is used to represent a world matrix and has functions to set
  /// each transform part, compute the SRT matrix, and do parent-child transform combination.
  /// </summary>
  [SavableVersion(1)]
  public sealed class Transform : IEquatable<Transform>, IFormattable, ISavable, IDeepCloneable
  {
    private Vector3 m_scale;
    private Quaternion m_rotation;
    private Vector3 m_translation;
    private Matrix m_cachedMatrix;
    private bool m_cacheRefresh;

    /// <summary>
    /// Gets or sets the scaling vector.
    /// </summary>
    public Vector3 Scale
    {
      get
      {
        return m_scale;
      }
      set
      {
        m_scale = value;
        m_cacheRefresh = true;
      }
    }

    /// <summary>
    /// Gets or sets the rotation quaternion.
    /// </summary>
    public Quaternion Rotation
    {
      get
      {
        return m_rotation;
      }
      set
      {
        m_rotation = value;
        m_cacheRefresh = true;
      }
    }

    /// <summary>
    /// Gets or sets the translation vector.
    /// </summary>
    public Vector3 Translation
    {
      get
      {
        return m_translation;
      }
      set
      {
        m_translation = value;
        m_cacheRefresh = true;
      }
    }

    /// <summary>
    /// Gets the computed SRT matrix.
    /// </summary>
    public ref readonly Matrix Matrix
    {
      get
      {
        if (m_cacheRefresh)
          ComputeMatrix();
        
        return ref m_cachedMatrix;
      }
    }

    /// <summary>
    /// Constructs a new instance of a Transform with unit scaling, no translation, and an identity rotation quaternion.
    /// </summary>
    public Transform()
    {
      m_scale = Vector3.One;
      m_rotation = Quaternion.Identity;
      m_translation = Vector3.Zero;
      m_cachedMatrix = Matrix.Identity;
      m_cacheRefresh = false;
    }

    /// <summary>
    /// Constructs a new instance of a Transform from the components of the supplied prototype.
    /// </summary>
    /// <param name="prototype">Transform to copy from</param>
    public Transform(Transform prototype)
    {
      m_scale = prototype.m_scale;
      m_rotation = prototype.m_rotation;
      m_translation = prototype.m_translation;
    }

    /// <summary>
    /// Constructs a new instance of a Transform with the supplied components.
    /// </summary>
    /// <param name="scale">Scaling vector</param>
    /// <param name="rotation">Rotation quaternion</param>
    /// <param name="translation">Translation vector</param>
    public Transform(in Vector3 scale, in Quaternion rotation, in Vector3 translation)
    {
      m_scale = scale;
      m_rotation = rotation;
      m_translation = translation;
    }

    /// <summary>
    /// Copies the transform data into a new instance.
    /// </summary>
    /// <returns>Cloned transform.</returns>
    public Transform Clone()
    {
      return new Transform(this);
    }

    /// <summary>
    /// Get a copy of the object.
    /// </summary>
    /// <returns>Cloned copy.</returns>
    IDeepCloneable IDeepCloneable.Clone()
    {
      return new Transform(this);
    }

    /// <summary>
    /// Tests equality between two transforms.
    /// </summary>
    /// <param name="a">First transform</param>
    /// <param name="b">Second transform</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    public static bool operator ==(Transform? a, Transform? b)
    {
      if (Object.ReferenceEquals(a, null) && Object.ReferenceEquals(b, null))
        return true;

      if (Object.ReferenceEquals(a, null) || Object.ReferenceEquals(b, null))
        return false;

      return a.m_scale.Equals(b.m_scale) && a.m_rotation.Equals(b.m_rotation) && a.m_translation.Equals(b.m_translation);
    }

    /// <summary>
    /// Tests inequality between two transforms.
    /// </summary>
    /// <param name="a">First transform</param>
    /// <param name="b">Second transform</param>
    /// <returns>True if both are not equal, false otherwise.</returns>
    public static bool operator !=(Transform? a, Transform? b)
    {
      if (Object.ReferenceEquals(a, null) && Object.ReferenceEquals(b, null))
        return false;

      if (Object.ReferenceEquals(a, null) || Object.ReferenceEquals(b, null))
        return true;

      return !a.m_scale.Equals(b.m_scale) || !a.m_rotation.Equals(b.m_rotation) || !a.m_translation.Equals(b.m_translation);
    }

    /// <summary>
    /// Tests equality between this transform and another.
    /// </summary>
    /// <param name="other">Other transform to compare against.</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    public bool Equals([NotNullWhen(true)] Transform? other)
    {
      if (other is null)
        return false;

      return m_scale.Equals(other.m_scale) && m_rotation.Equals(other.m_rotation) && m_translation.Equals(other.m_translation);
    }

    /// <summary>
    /// Tests equality between this transform and another.
    /// </summary>
    /// <param name="other">Other transform to compare against.</param>
    /// <param name="tolerance">Tolerance</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    public bool Equals([NotNullWhen(true)] Transform? other, float tolerance)
    {
      if (other is null)
        return false;

      return m_scale.Equals(other.m_scale, tolerance) && m_rotation.Equals(other.m_rotation, tolerance) && m_translation.Equals(other.m_translation, tolerance);
    }

    /// <summary>
    /// Determines whether the specified <see cref="T:System.Object" /> is equal to the current <see cref="T:System.Object" />.
    /// </summary>
    /// <param name="obj">The object to compare with the current object.</param>
    /// <returns>True if the specified object  is equal to the current object; otherwise, false. </returns>
    public override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is Transform other)
        return m_scale.Equals(other.m_scale) && m_rotation.Equals(other.m_rotation) && m_translation.Equals(other.m_translation);

      return false;
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.  </returns>
    public override int GetHashCode()
    {
      unchecked
      {
        return m_scale.GetHashCode() + m_rotation.GetHashCode() + m_translation.GetHashCode();
      }
    }

    /// <summary>
    /// Returns a string that represents the current object.
    /// </summary>
    /// <returns>A string that represents the current object. </returns>
    public override String ToString()
    {
      CultureInfo info = CultureInfo.CurrentCulture;
      return String.Format(info, "Scale: {0}, Rotation: {1}, Translation: {2}",
          new Object[] { m_scale.ToString(), m_rotation.ToString(), m_translation.ToString() });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="format">The format</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public String ToString(String? format)
    {
      if (format is null)
        return ToString();

      CultureInfo info = CultureInfo.CurrentCulture;
      return String.Format(info, "Scale: {0}, Rotation: {1}, Translation: {2}",
          new Object[] { m_scale.ToString(format, info), m_rotation.ToString(format, info), m_translation.ToString(format, info) });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="formatProvider">The format provider.</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public String ToString(IFormatProvider? formatProvider)
    {
      if (formatProvider is null)
        return ToString();

      return String.Format(formatProvider, "Scale: {0}, Rotation: {1}, Translation: {2}",
          new Object[] { m_scale.ToString(formatProvider), m_rotation.ToString(formatProvider), m_translation.ToString(formatProvider) });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="format">The format.</param>
    /// <param name="formatProvider">The format provider.</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public String ToString(String? format, IFormatProvider? formatProvider)
    {
      if (formatProvider is null)
        return ToString();

      if (format is null)
        return ToString(formatProvider);

      return String.Format(formatProvider, "Scale: {0}, Rotation: {1}, Translation: {2}",
          new Object[] { m_scale.ToString(format, formatProvider), m_rotation.ToString(format, formatProvider), m_translation.ToString(format, formatProvider) });
    }

    /// <summary>
    /// Sets the transform with the store from the supplied transform.
    /// </summary>
    /// <param name="transform">Transform to copy from</param>
    public void Set(Transform transform)
    {
      m_scale = transform.m_scale;
      m_rotation = transform.m_rotation;
      m_translation = transform.m_translation;
      m_cacheRefresh = true;
    }

    /// <summary>
    /// Sets the transform from a (S)cale-(R)otation-(T)ranslation matrix.
    /// </summary>
    /// <param name="matrix">Matrix to decompose the scale/rotation/translation components from.</param>
    public void Set(in Matrix matrix)
    {
      matrix.Decompose(out m_scale, out m_rotation, out m_translation);
      m_cacheRefresh = true;
    }

    /// <summary>
    /// Sets the transform with the supplied components.
    /// </summary>
    /// <param name="scale">Scaling vector</param>
    /// <param name="rotation">Rotation quaternion</param>
    /// <param name="translation">Translation vector</param>
    public void Set(in Vector3 scale, in Quaternion rotation, in Vector3 translation)
    {
      m_scale = scale;
      m_rotation = rotation;
      m_translation = translation;
      m_cacheRefresh = true;
    }

    /// <summary>
    /// Sets the transform's translation vector from the supplied coordinates.
    /// </summary>
    /// <param name="x">X coordinate</param>
    /// <param name="y">Y coordinate</param>
    /// <param name="z">Z coordinate</param>
    public void SetTranslation(float x, float y, float z)
    {
      m_translation.X = x;
      m_translation.Y = y;
      m_translation.Z = z;
      m_cacheRefresh = true;
    }

    /// <summary>
    /// Sets the transform's translation vector from the supplied coordinates.
    /// </summary>
    /// <param name="translation">Translation vector</param>
    public void SetTranslation(in Vector3 translation)
    {
      m_translation = translation;
      m_cacheRefresh = true;
    }

    /// <summary>
    /// Sets the transform's scaling vector with the supplied scaling factors for each axis.
    /// </summary>
    /// <param name="x">Scaling on x axis</param>
    /// <param name="y">Scaling on y axis</param>
    /// <param name="z">Scaling on z axis</param>
    public void SetScale(float x, float y, float z)
    {
      m_scale.X = x;
      m_scale.Y = y;
      m_scale.Z = z;
      m_cacheRefresh = true;
    }

    /// <summary>
    /// Sets the transform's scaling vector with a single uniform index.
    /// </summary>
    /// <param name="scale">Uniform scaling index</param>
    public void SetScale(float scale)
    {
      m_scale.X = scale;
      m_scale.Y = scale;
      m_scale.Z = scale;
      m_cacheRefresh = true;
    }

    /// <summary>
    /// Sets the transform's scaling vector with the supplied scaling factors for each axis.
    /// </summary>
    /// <param name="scale">Scaling vector</param>
    public void SetScale(in Vector3 scale)
    {
      m_scale = scale;
      m_cacheRefresh = true;
    }

    /// <summary>
    /// Sets the rotation quaternion from a rotation matrix. The matrix *must* represent a rotation!
    /// This method does NOT check if if the matrix is a valid rotation matrix.
    /// </summary>
    /// <param name="rotMatrix">Rotation matrix</param>
    public void SetRotation(in Matrix rotMatrix)
    {
      Quaternion.FromRotationMatrix(rotMatrix, out m_rotation);
      m_cacheRefresh = true;
    }

    /// <summary>
    /// Sets the rotation quaternion.
    /// </summary>
    /// <param name="rotation">Rotation.</param>
    public void SetRotation(in Quaternion rotation)
    {
      m_rotation = rotation;
      m_cacheRefresh = true;
    }

    /// <summary>
    /// Sets the SRT components of the transform.
    /// </summary>
    /// <param name="scale">Scaling vector.</param>
    /// <param name="rotation">Rotation.</param>
    /// <param name="translation">Translation vector.</param>
    public void SetComponents(in Vector3 scale, in Quaternion rotation, in Vector3 translation)
    {
      m_scale = scale;
      m_rotation = rotation;
      m_translation = translation;
    }

    /// <summary>
    /// Sets the transform to its identity state - scaling vector is all 1's, translation is all 0's, and
    /// the rotation quaternion is the identity.
    /// </summary>
    public void SetIdentity()
    {
      m_scale = Vector3.One;
      m_rotation = Quaternion.Identity;
      m_translation = Vector3.Zero;
      m_cachedMatrix = Matrix.Identity;
      m_cacheRefresh = false;
    }

    /// <summary>
    /// Returns the calculated row vector of the 3x3 rotation matrix (represented by the quaternion).
    /// Row 0 is Right, row 1 is Up, and row 2 is Forward.
    /// </summary>
    /// <param name="i">Row index, must be between 0 and 2</param>
    /// <returns>Row vector of the rotation matrix</returns>
    /// <exception cref="TeslaException">Throws an exception if index is not in range</exception>
    public Vector3 GetRotationVector(int i)
    {
      if (i > 2 || i < 0)
        throw new TeslaException("GetRotationVector3 index must be between 0 and 2.");

      Vector3 row;
      Quaternion.GetRotationVector(m_rotation, i, out row);
      return row;
    }

    /// <summary>
    /// Gets the scaling vector.
    /// </summary>
    /// <param name="scale">Scaling vector.</param>
    public void GetScale(out Vector3 scale)
    {
      scale = m_scale;
    }

    /// <summary>
    /// Gets the rotation.
    /// </summary>
    /// <param name="rotation">Rotation.</param>
    public void GetRotation(out Quaternion rotation)
    {
      rotation = m_rotation;
    }

    /// <summary>
    /// Gets the translation vector.
    /// </summary>
    /// <param name="translation">Translation vector.</param>
    public void GetTranslation(out Vector3 translation)
    {
      translation = m_translation;
    }

    /// <summary>
    /// Gets the SRT components of the transform.
    /// </summary>
    /// <param name="scale">Scaling vector.</param>
    /// <param name="rotation">Rotation.</param>
    /// <param name="translation">Translation vector.</param>
    public void GetComponents(out Vector3 scale, out Quaternion rotation, out Vector3 translation)
    {
      scale = m_scale;
      rotation = m_rotation;
      translation = m_translation;
    }

    /// <summary>
    /// Combines this transform with a transform that represents its "parent".
    /// </summary>
    /// <param name="parent">Parent transform</param>
    public void CombineWithParent(Transform parent)
    {
      //Multiply scaling
      Vector3.Multiply(parent.m_scale, m_scale, out m_scale);

      //Multiply rotation
      Quaternion.Multiply(parent.m_rotation, m_rotation, out m_rotation);

      //Combine translation
      Vector3.Multiply(m_translation, parent.m_scale, out m_translation);
      Vector3.Transform(m_translation, parent.m_rotation, out m_translation);
      Vector3.Add(m_translation, parent.m_translation, out m_translation);
      m_cacheRefresh = true;
    }

    /// <summary>
    /// Interpolates between two transforms, setting the result to this transform. Slerp is applied
    /// to the rotations and Lerp to the translation/scale.
    /// </summary>
    /// <param name="start">Starting transform</param>
    /// <param name="end">Ending transform</param>
    /// <param name="percent">Percent to interpolate between the two transforms, must be between 0 and 1</param>
    public void InterpolateTransforms(Transform start, Transform end, float percent)
    {
      Quaternion.Slerp(start.m_rotation, end.m_rotation, percent, out m_rotation);
      Vector3.Lerp(start.m_scale, end.m_scale, percent, out m_scale);
      Vector3.Lerp(start.m_translation, end.m_translation, percent, out m_translation);
    }

    /// <summary>
    /// Transforms the supplied vector.
    /// </summary>
    /// <param name="v">Vector3 to be transformed</param>
    /// <returns>Transformed vector</returns>
    public Vector3 TransformVector(in Vector3 v)
    {
      Vector3 result;

      Vector3.Multiply(v, m_scale, out result);
      Vector3.Transform(result, m_rotation, out result);
      Vector3.Add(result, m_translation, out result);

      return result;
    }

    /// <summary>
    /// Transforms the supplied vector.
    /// </summary>
    /// <param name="v">Vector3 to be transformed</param>
    /// <param name="result">Existing Vector3 to hold result</param>
    public void TransformVector(in Vector3 v, out Vector3 result)
    {
      Vector3.Multiply(v, m_scale, out result);
      Vector3.Transform(result, m_rotation, out result);
      Vector3.Add(result, m_translation, out result);
    }

    /// <summary>
    /// Reads the object data from the input.
    /// </summary>
    /// <param name="input">Savable reader</param>
    public void Read(ISavableReader input)
    {
      input.Read<Vector3>("Scale", out m_scale);
      input.Read<Quaternion>("Rotation", out m_rotation);
      input.Read<Vector3>("Translation", out m_translation);

      //Cause matrix to be computed and cached
      ComputeMatrix();
    }

    /// <summary>
    /// Writes the object data to the output.
    /// </summary>
    /// <param name="output">Savable writer</param>
    public void Write(ISavableWriter output)
    {
      output.Write("Scale", m_scale);
      output.Write("Rotation", m_rotation);
      output.Write("Translation", m_translation);
    }

    private void ComputeMatrix()
    {
      Matrix scaleM;
      Matrix rotationM;
      Matrix translationM;

      Matrix.FromScale(m_scale, out scaleM);
      Matrix.FromQuaternion(m_rotation, out rotationM);
      Matrix.FromTranslation(m_translation, out translationM);

      //((Scale * Rotation) * Translation)
      Matrix.Multiply(scaleM, rotationM, out m_cachedMatrix);
      Matrix.Multiply(m_cachedMatrix, translationM, out m_cachedMatrix);
      m_cacheRefresh = false;
    }
  }
}
