﻿/*
* Copyright (c) 2010-2024 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System.Collections.Generic;

namespace Tesla
{
  /// <summary>
  /// A picking query result for a single object, which contains two components: a bounding intersection and a variable number of primitive intersections. Depending on
  /// the picking query, a result may just contain the bounding intersection result.
  /// </summary>
  public sealed class PickResult : ReadOnlyRefList<Pair<LineIntersectionResult, Triangle?>>
  {
    private static Comparer s_comparer = new Comparer();

    private IPickable m_target;
    private BoundingIntersectionResult m_boundingResult;

    /// <summary>
    /// Gets the target of the result.
    /// </summary>
    public IPickable Target
    {
      get
      {
        return m_target;
      }
    }

    /// <summary>
    /// Gets the bounding intersection result.
    /// </summary>
    public ref readonly BoundingIntersectionResult BoundingIntersection
    {
      get
      {
        return ref m_boundingResult;
      }
    }

    /// <summary>
    /// Gets the closest primitive intersection.
    /// </summary>
    public ref readonly Pair<LineIntersectionResult, Triangle?> ClosestIntersection
    {
      get
      {
        return ref m_list[0];
      }
    }

    /// <summary>
    /// Gets the farthest primitive intersection.
    /// </summary>
    public ref readonly Pair<LineIntersectionResult, Triangle?> FarthestIntersection
    {
      get
      {
        return ref m_list[m_list.Count - 1];
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="PickResult"/> class.
    /// </summary>
    public PickResult() : this(null, new BoundingIntersectionResult(), null) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="PickResult"/> class.
    /// </summary>
    /// <param name="target">The picking target.</param>
    /// <param name="boundingResult">Bounding intersection result.</param>
    public PickResult(IPickable target, in BoundingIntersectionResult boundingResult) : this(target, boundingResult, null) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="PickResult"/> class.
    /// </summary>
    /// <param name="target">The picking target.</param>
    /// <param name="boundingResult">Bounding intersection result.</param>
    /// <param name="results">Optional list of primitive intersections.</param>
    public PickResult(IPickable target, in BoundingIntersectionResult boundingResult, IEnumerable<Pair<LineIntersectionResult, Triangle?>> results)
        : base(new RefList<Pair<LineIntersectionResult, Triangle?>>())
    {
      m_target = target;
      m_boundingResult = boundingResult;

      if (results != null)
      {
        GetRefList().AddRange(results);
        Sort();
      }
    }

    //To get at the internal ref list -- we always initialize it to a RefList
    internal RefList<Pair<LineIntersectionResult, Triangle?>> GetRefList()
    {
      return m_list as RefList<Pair<LineIntersectionResult, Triangle?>>;
    }

    //When recycling pick result
    internal void Set(IPickable target, in BoundingIntersectionResult boundingResult)
    {
      m_target = target;
      m_boundingResult = boundingResult;
    }

    //When recycling pick result
    internal void Clear()
    {
      m_boundingResult = new BoundingIntersectionResult();
      m_target = null;
      m_list.Clear();
    }

    //When recycling pick result
    internal void Sort()
    {
      GetRefList().Sort(s_comparer);
    }

    /// <summary>
    /// Gets the bounding intersection result.
    /// </summary>
    /// <param name="result">Bounding intersection result.</param>
    public void GetBoundingIntersection(out BoundingIntersectionResult result)
    {
      result = m_boundingResult;
    }

    /// <summary>
    /// Gets the closest primitive intersection.
    /// </summary>
    /// <param name="result">Closest primitive intersection result.</param>
    /// <returns>True if the intersection exists, false if not.</returns>
    public bool GetClosestIntersection(out LineIntersectionResult result)
    {
      Triangle? triangle;
      return GetIntersection(0, out result, out triangle);
    }

    /// <summary>
    /// Gets the closest primitive intersection.
    /// </summary>
    /// <param name="result">Closest primitive intersection result.</param>
    /// <param name="triangle">Optional triangle that was picked.</param>
    /// <returns>True if the intersection exists, false if not.</returns>
    public bool GetClosestIntersection(out LineIntersectionResult result, out Triangle? triangle)
    {
      return GetIntersection(0, out result, out triangle);
    }

    /// <summary>
    /// Gets the farthest primitive intersection.
    /// </summary>
    /// <param name="result">Farthest primitive intersection result.</param>
    /// <returns>True if the intersection exists, false if not.</returns>
    public bool GetFarthestIntersection(out LineIntersectionResult result)
    {
      Triangle? triangle;
      return GetIntersection(Count - 1, out result, out triangle);
    }

    /// <summary>
    /// Gets the farthest primitive intersection.
    /// </summary>
    /// <param name="result">Farthest primitive intersection result.</param>
    /// <param name="triangle">Optional triangle that was picked.</param>
    /// <returns>True if the intersection exists, false if not.</returns>
    public bool GetFarthestIntersection(out LineIntersectionResult result, out Triangle? triangle)
    {
      return GetIntersection(Count - 1, out result, out triangle);
    }

    /// <summary>
    /// Gets the primitive intersection at the specified index.
    /// </summary>
    /// <param name="index">Zero-based index of the primitive intersection.</param>
    /// <param name="result">Farthest primitive intersection result.</param>
    /// <returns>True if the intersection exists, false if not.</returns>
    public bool GetIntersection(int index, out LineIntersectionResult result)
    {
      Triangle? triangle;
      return GetIntersection(index, out result, out triangle);
    }

    /// <summary>
    /// Gets the primitive intersection at the specified index.
    /// </summary>
    /// <param name="index">Zero-based index of the primitive intersection.</param>
    /// <param name="result">Farthest primitive intersection result.</param>
    /// <param name="triangle">Optional triangle that was picked.</param>
    /// <returns>True if the intersection exists, false if not.</returns>
    public bool GetIntersection(int index, out LineIntersectionResult result, out Triangle? triangle)
    {
      if (index < 0 || index >= Count)
      {
        result = new LineIntersectionResult();
        triangle = null;
        return false;
      }

      ref readonly Pair<LineIntersectionResult, Triangle?> pair = ref m_list[index];

      result = pair.First;
      triangle = pair.Second;

      return true;
    }

    /// <summary>
    /// Returns an enumerator that iterates through the pick results.
    /// </summary>
    /// <returns>An enumerator that can be used to iterate through the pick results.</returns>
    public new RefList<Pair<LineIntersectionResult, Triangle?>>.Enumerator GetEnumerator()
    {
      return GetRefList().GetEnumerator();
    }

    //For sorting the primitive intersection results
    private sealed class Comparer : IRefComparer<Pair<LineIntersectionResult, Triangle?>>
    {
      public int Compare(in Pair<LineIntersectionResult, Triangle?> x, in Pair<LineIntersectionResult, Triangle?> y)
      {
        float distx = x.First.Distance;
        float disty = y.First.Distance;

        if (MathHelper.IsEqual(distx, disty, MathHelper.ZeroTolerance))
          return 0;

        if (distx < disty)
          return -1;
        else
          return 1;
      }
    }
  }
}
