﻿/*
* Copyright (c) 2010-2024 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;
using Tesla.Content;

#nullable enable

namespace Tesla
{
  [SavableVersion(1)]
  public sealed class BoundingMesh : BoundingVolume
  {
    public override Vector3 Center
    {
      get
      {
        throw new NotImplementedException();
      }
      set
      {
        throw new NotImplementedException();
      }
    }

    public override float Volume { get { throw new NotImplementedException(); } }

    public override int CornerCount { get { throw new NotImplementedException(); } }

    public override BoundingType BoundingType { get { throw new NotImplementedException(); } }

    public override void Set(BoundingVolume? volume)
    {
      throw new NotImplementedException();
    }

    public override BoundingVolume Clone()
    {
      throw new NotImplementedException();
    }

    public override float DistanceTo(in Vector3 point)
    {
      throw new NotImplementedException();
    }

    public override float DistanceSquaredTo(in Vector3 point)
    {
      throw new NotImplementedException();
    }

    public override void ClosestPointTo(in Vector3 point, out Vector3 result)
    {
      throw new NotImplementedException();
    }

    public override void ComputeFromPoints(ReadOnlySpan<Vector3> points, in SubMeshRange? subMeshRange = null)
    {
      throw new NotImplementedException();
    }

    public override void ComputeFromIndexedPoints(ReadOnlySpan<Vector3> points, IndexData indices, in SubMeshRange? subMeshRange = null)
    {
      throw new NotImplementedException();
    }

    public override ContainmentType Contains(in Vector3 point)
    {
      throw new NotImplementedException();
    }

    public override ContainmentType Contains(in Segment line)
    {
      throw new NotImplementedException();
    }

    public override ContainmentType Contains(in Triangle triangle)
    {
      throw new NotImplementedException();
    }

    public override ContainmentType Contains(in Ellipse ellipse)
    {
      throw new NotImplementedException();
    }

    public override ContainmentType Contains(BoundingVolume? volume)
    {
      throw new NotImplementedException();
    }

    public override bool Intersects(in Ray ray)
    {
      throw new NotImplementedException();
    }

    public override bool Intersects(in Ray ray, out BoundingIntersectionResult result)
    {
      throw new NotImplementedException();
    }

    public override bool Intersects(in Segment line)
    {
      throw new NotImplementedException();
    }

    public override bool Intersects(in Segment line, out BoundingIntersectionResult result)
    {
      throw new NotImplementedException();
    }

    public override PlaneIntersectionType Intersects(in Plane plane)
    {
      throw new NotImplementedException();
    }

    public override bool Intersects(BoundingVolume? volume)
    {
      throw new NotImplementedException();
    }

    public override void Merge(BoundingVolume? volume)
    {
      throw new NotImplementedException();
    }

    public override void Transform(in Vector3 scale, in Quaternion rotation, in Vector3 translation)
    {
      throw new NotImplementedException();
    }

    public override bool Equals([NotNullWhen(true)] BoundingVolume? other, float tolerance)
    {
      throw new NotImplementedException();
    }

    public override void Read(ISavableReader input)
    {
      throw new NotImplementedException();
    }

    public override void Write(ISavableWriter output)
    {
      throw new NotImplementedException();
    }

    protected override void ComputeCorners(Span<Vector3> corners)
    {
      throw new NotImplementedException();
    }
  }
}
