﻿/*
* Copyright (c) 2010-2024 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Tesla.Content;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Defines a boolean value that is stored in 4 bytes rather than one byte like the standard .NET bool type.
  /// </summary>
  [Serializable]
  [StructLayout(LayoutKind.Sequential, Size = 4)]
  public struct Bool : IEquatable<Bool>, IRefEquatable<Bool>, IPrimitiveValue
  {
    private int m_boolValue;

    private static readonly Bool s_false = new Bool(false);
    private static readonly Bool s_true = new Bool(true);
    private static readonly int s_sizeInBytes = BufferHelper.SizeOf<Bool>();

    /// <summary>
    /// Gets a <see cref="Bool"/> set to false.
    /// </summary>
    public static Bool False
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return s_false;
      }
    }

    /// <summary>
    /// Gets a <see cref="Bool"/> set to true.
    /// </summary>
    public static Bool True
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return s_true;
      }
    }

    /// <summary>
    /// Gets the size of the <see cref="Bool"/> type in bytes.
    /// </summary>
    public static int SizeInBytes
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return s_sizeInBytes;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Bool"/> struct.
    /// </summary>
    /// <param name="boolValue">Boolean value</param>
    public Bool(bool boolValue)
    {
      m_boolValue = boolValue ? 1 : 0;
    }

    /// <summary>
    /// Checks equality between two <see cref="Bool"/>.
    /// </summary>
    /// <param name="a">First <see cref="Bool"/>.</param>
    /// <param name="b">Second <see cref="Bool"/>.</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator ==(Bool a, Bool b)
    {
      return a.m_boolValue == b.m_boolValue;
    }

    /// <summary>
    /// Checks inequality between two <see cref="Bool"/>.
    /// </summary>
    /// <param name="a">First <see cref="Bool"/>.</param>
    /// <param name="b">Second <see cref="Bool"/>.</param>
    /// <returns>True if both are not equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator !=(Bool a, Bool b)
    {
      return a.m_boolValue != b.m_boolValue;
    }

    /// <summary>
    /// Implicit conversion from .NET <see cref="Boolean"/> to <see cref="Bool"/> type.
    /// </summary>
    /// <param name="boolValue">.NET <see cref="Boolean"/> type.</param>
    /// <returns>4 byte <see cref="Bool"/></returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static implicit operator Bool(bool boolValue)
    {
      return new Bool(boolValue);
    }

    /// <summary>
    /// Implicit conversion from 4 byte <see cref="Bool"/> to .NET <see cref="Boolean"/> type.
    /// </summary>
    /// <param name="value">4 byte <see cref="Bool"/></param>
    /// <returns>1 byte .NET <see cref="Boolean"/></returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static implicit operator bool(Bool value)
    {
      return value.m_boolValue != 0;
    }

    /// <summary>
    /// Checks equality with another <see cref="Bool"/>.
    /// </summary>
    /// <param name="other">Other <see cref="Bool"/></param>
    /// <returns>True if both are equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Equals(Bool other)
    {
      return m_boolValue == other.m_boolValue;
    }

    /// <summary>
    /// Checks equality with another <see cref="Bool"/>.
    /// </summary>
    /// <param name="other">Other <see cref="Bool"/></param>
    /// <returns>True if both are equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    readonly bool IRefEquatable<Bool>.Equals(in Bool other)
    {
      return m_boolValue == other.m_boolValue;
    }

    /// <summary>
    /// Determines whether the specified <see cref="System.Object" /> is equal to this instance.
    /// </summary>
    /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
    /// <returns>True if the specified <see cref="System.Object" /> is equal to this instance; otherwise, False.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is Bool)
        return m_boolValue == ((Bool)obj).m_boolValue;

      return false;
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
    public readonly override int GetHashCode()
    {
      unchecked
      {
        return m_boolValue;
      }
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly override string ToString()
    {
      CultureInfo info = CultureInfo.CurrentCulture;
      return String.Format(info, "{0}",
          new Object[] { m_boolValue != 0 });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="formatProvider">The format provider.</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(IFormatProvider? formatProvider)
    {
      if (formatProvider is null)
        return ToString();

      return String.Format(formatProvider, "{0}", new Object[] { (m_boolValue != 0).ToString(formatProvider) });
    }

    /// <summary>
    /// Writes the primitive data to the output.
    /// </summary>
    /// <param name="output">Primitive writer.</param>
    readonly void IPrimitiveValue.Write(IPrimitiveWriter output)
    {
      output.Write("BoolValue", (m_boolValue > 0) ? true : false);
    }

    /// <summary>
    /// Reads the primitive data from the input.
    /// </summary>
    /// <param name="input">Primitive reader.</param>
    void IPrimitiveValue.Read(IPrimitiveReader input)
    {
      m_boolValue = input.ReadBoolean("BoolValue") ? 1 : 0;
    }
  }
}
