﻿/*
* Copyright (c) 2010-2024 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Tesla.Content;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Defines a 4x4 row-vector Matrix. Memory layout is row major.
  /// </summary>
  /// <remarks>
  /// Matrix functions exhibit the following behaviors by default:
  /// <list type="bullet">
  /// <item>
  /// <description>Right handedness conventions used. By default, +X to the right (row 1), +Y up (row 2), and -Z (row 3).</description>
  /// </item>
  /// <item>
  /// <description>Matrix multiplication order is local space on the left and world target on the right 
  /// (e.g. SRT - scale * rotation * translation, or WVP - world * view * projection)</description>
  /// </item>
  /// <item>
  /// <description>The vector is the left operand of the transform, as represented by the transform methods in the Vector2/3/4 structures.</description>
  /// </item>
  /// <item>
  /// <description>Vector transforms are dot products between the vector and the column elements of the matrix.</description>
  /// </item>
  /// </list>
  /// </remarks>
  [Serializable]
  [StructLayout(LayoutKind.Sequential, Pack = 4)]
  public struct Matrix : IEquatable<Matrix>, IRefEquatable<Matrix>, IFormattable, IPrimitiveValue
  {
    /// <summary>
    /// Value at row 1, column 1 of the matrix
    /// </summary>
    public float M11;

    /// <summary>
    /// Value at row 1, column 2 of the matrix
    /// </summary>
    public float M12;

    /// <summary>
    /// Value at row 1, column 3 of the matrix
    /// </summary>
    public float M13;

    /// <summary>
    /// Value at row 1, column 4 of the matrix
    /// </summary>
    public float M14;

    /// <summary>
    /// Value at row 2, column 1 of the matrix
    /// </summary>
    public float M21;

    /// <summary>
    /// Value at row 2, column 2 of the matrix
    /// </summary>
    public float M22;

    /// <summary>
    /// Value at row 2, column 3 of the matrix
    /// </summary>
    public float M23;

    /// <summary>
    /// Value at row 2, column 4 of the matrix
    /// </summary>
    public float M24;

    /// <summary>
    /// Value at row 3, column 1 of the matrix
    /// </summary>
    public float M31;

    /// <summary>
    /// Value at row 3, column 2 of the matrix
    /// </summary>
    public float M32;

    /// <summary>
    /// Value at row 3, column 3 of the matrix
    /// </summary>
    public float M33;

    /// <summary>
    /// Value at row 3, column 4 of the matrix
    /// </summary>
    public float M34;

    /// <summary>
    /// Value at row 4, column 1 of the matrix
    /// </summary>
    public float M41;

    /// <summary>
    /// Value at row 4, column 2 of the matrix
    /// </summary>
    public float M42;

    /// <summary>
    /// Value at row 4, column 3 of the matrix
    /// </summary>
    public float M43;

    /// <summary>
    /// Value at row 4, column 4 of the matrix
    /// </summary>
    public float M44;

    private static readonly Matrix s_identity = new Matrix(1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f,
        0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f);

    private static readonly int s_sizeInBytes = BufferHelper.SizeOf<Matrix>();

    /// <summary>
    /// Get the identity matrix.
    /// </summary>
    public static ref readonly Matrix Identity
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_identity;
      }
    }

    /// <summary>
    /// Get the size of the Matrix structure in bytes.
    /// </summary>
    public static int SizeInBytes
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return s_sizeInBytes;
      }
    }

    /// <summary>
    /// Gets the axes that represent the orientation of the matrix.
    /// </summary>
    public readonly Triad Axes
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        Triad axes;
        axes.XAxis = Right;
        axes.YAxis = Up;
        axes.ZAxis = Backward;

        return axes;
      }
    }

    /// <summary>
    /// Gets or sets the translation vector (M41, M42, M43) of the matrix.
    /// </summary>
    public Vector3 Translation
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      readonly get
      {
        Vector3 v;
        v.X = M41;
        v.Y = M42;
        v.Z = M43;
        return v;
      }
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      set
      {
        M41 = value.X;
        M42 = value.Y;
        M43 = value.Z;
      }
    }

    /// <summary>
    /// Gets or sets the up vector (M21, M22, M23) of the matrix.
    /// </summary>
    public Vector3 Up
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      readonly get
      {
        Vector3 v;
        v.X = M21;
        v.Y = M22;
        v.Z = M23;
        return v;
      }
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      set
      {
        M21 = value.X;
        M22 = value.Y;
        M23 = value.Z;
      }
    }

    /// <summary>
    /// Gets or sets the down vector of the matrix (negation of Up vector).
    /// Setting will negate component values.
    /// </summary>
    public Vector3 Down
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      readonly get
      {
        Vector3 v;
        v.X = -M21;
        v.Y = -M22;
        v.Z = -M23;
        return v;
      }
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      set
      {
        M21 = -value.X;
        M22 = -value.Y;
        M23 = -value.Z;
      }
    }

    /// <summary>
    /// Gets or sets the backward vector (M31, M32, M33) of the matrix.
    /// </summary>
    public Vector3 Backward
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      readonly get
      {
        Vector3 v;
        v.X = M31;
        v.Y = M32;
        v.Z = M33;
        return v;
      }
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      set
      {
        M31 = value.X;
        M32 = value.Y;
        M33 = value.Z;
      }
    }

    /// <summary>
    /// Gets or sets the forward vector of the matrix (negation of backward vector).
    /// </summary>
    public Vector3 Forward
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      readonly get
      {
        Vector3 v;
        v.X = -M31;
        v.Y = -M32;
        v.Z = -M33;
        return v;
      }
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      set
      {
        M31 = -value.X;
        M32 = -value.Y;
        M33 = -value.Z;
      }
    }

    /// <summary>
    /// Gets or sets the right vector (M11, M12, M13) of the matrix.
    /// </summary>
    public Vector3 Right
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      readonly get
      {
        Vector3 v;
        v.X = M11;
        v.Y = M12;
        v.Z = M13;
        return v;
      }
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      set
      {
        M11 = value.X;
        M12 = value.Y;
        M13 = value.Z;
      }
    }

    /// <summary>
    /// Gets or sets the left vector of the matrix (negation of right vector).
    /// </summary>
    public Vector3 Left
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      readonly get
      {
        Vector3 v;
        v.X = -M11;
        v.Y = -M12;
        v.Z = -M13;
        return v;
      }
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      set
      {
        M11 = -value.X;
        M12 = -value.Y;
        M13 = -value.Z;
      }
    }

    /// <summary>
    /// Gets or sets the scale of the matrix (M11, M22, M33).
    /// </summary>
    public Vector3 Scale
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      readonly get
      {
        Vector3 v;
        v.X = M11;
        v.Y = M22;
        v.Z = M33;
        return v;
      }
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      set
      {
        M11 = value.X;
        M22 = value.Y;
        M33 = value.Z;
      }
    }

    /// <summary>
    /// Gets or sets the first row of the matrix (M11, M12, M13, M14).
    /// </summary>
    public Vector4 Row1
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      readonly get
      {
        Vector4 row;
        row.X = M11;
        row.Y = M12;
        row.Z = M13;
        row.W = M14;

        return row;
      }
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      set
      {
        M11 = value.X;
        M12 = value.Y;
        M13 = value.Z;
        M14 = value.W;
      }
    }

    /// <summary>
    /// Gets or sets the second row of the matrix (M21, M22, M23, M24).
    /// </summary>
    public Vector4 Row2
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      readonly get
      {
        Vector4 row;
        row.X = M21;
        row.Y = M22;
        row.Z = M23;
        row.W = M24;

        return row;
      }
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      set
      {
        M21 = value.X;
        M22 = value.Y;
        M23 = value.Z;
        M24 = value.W;
      }
    }

    /// <summary>
    /// Gets or sets the third row of the matrix (M31, M32, M33, M34).
    /// </summary>
    public Vector4 Row3
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      readonly get
      {
        Vector4 row;
        row.X = M31;
        row.Y = M32;
        row.Z = M33;
        row.W = M34;

        return row;
      }
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      set
      {
        M31 = value.X;
        M32 = value.Y;
        M33 = value.Z;
        M34 = value.W;
      }
    }

    /// <summary>
    /// Gets or sets the first row of the matrix (M41, M42, M43, M44).
    /// </summary>
    public Vector4 Row4
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      readonly get
      {
        Vector4 row;
        row.X = M41;
        row.Y = M42;
        row.Z = M43;
        row.W = M44;

        return row;
      }
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      set
      {
        M41 = value.X;
        M42 = value.Y;
        M43 = value.Z;
        M44 = value.W;
      }
    }

    /// <summary>
    /// Gets or sets the first column of the matrix (M11, M21, M31, M41).
    /// </summary>
    public Vector4 Column1
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      readonly get
      {
        Vector4 col;
        col.X = M11;
        col.Y = M21;
        col.Z = M31;
        col.W = M41;

        return col;
      }
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      set
      {
        M11 = value.X;
        M21 = value.Y;
        M31 = value.Z;
        M41 = value.W;
      }
    }

    /// <summary>
    /// Gets or sets the second column of the matrix (M12, M22, M32, M42).
    /// </summary>
    public Vector4 Column2
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      readonly get
      {
        Vector4 col;
        col.X = M12;
        col.Y = M22;
        col.Z = M32;
        col.W = M42;

        return col;
      }
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      set
      {
        M12 = value.X;
        M22 = value.Y;
        M32 = value.Z;
        M42 = value.W;
      }
    }

    /// <summary>
    /// Gets or sets the third column of the matrix (M13, M23, M33, M43).
    /// </summary>
    public Vector4 Column3
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      readonly get
      {
        Vector4 col;
        col.X = M13;
        col.Y = M23;
        col.Z = M33;
        col.W = M43;

        return col;
      }
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      set
      {
        M13 = value.X;
        M23 = value.Y;
        M33 = value.Z;
        M43 = value.W;
      }
    }

    /// <summary>
    /// Gets or sets the fourth column of the matrix (M14, M24, M34, M44).
    /// </summary>
    public Vector4 Column4
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      readonly get
      {
        Vector4 col;
        col.X = M14;
        col.Y = M24;
        col.Z = M34;
        col.W = M44;

        return col;
      }
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      set
      {
        M14 = value.X;
        M24 = value.Y;
        M34 = value.Z;
        M44 = value.W;
      }
    }

    /// <summary>
    /// Gets if this matrix is equal to the identity matrix.
    /// </summary>
    public readonly bool IsIdentity
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return (MathF.Abs(M11 - s_identity.M11) <= MathHelper.ZeroTolerance) &&
                (MathF.Abs(M12 - s_identity.M12) <= MathHelper.ZeroTolerance) &&
                (MathF.Abs(M13 - s_identity.M13) <= MathHelper.ZeroTolerance) &&
                (MathF.Abs(M14 - s_identity.M14) <= MathHelper.ZeroTolerance) &&

                (MathF.Abs(M21 - s_identity.M21) <= MathHelper.ZeroTolerance) &&
                (MathF.Abs(M22 - s_identity.M22) <= MathHelper.ZeroTolerance) &&
                (MathF.Abs(M23 - s_identity.M23) <= MathHelper.ZeroTolerance) &&
                (MathF.Abs(M24 - s_identity.M24) <= MathHelper.ZeroTolerance) &&

                (MathF.Abs(M31 - s_identity.M31) <= MathHelper.ZeroTolerance) &&
                (MathF.Abs(M32 - s_identity.M32) <= MathHelper.ZeroTolerance) &&
                (MathF.Abs(M33 - s_identity.M33) <= MathHelper.ZeroTolerance) &&
                (MathF.Abs(M34 - s_identity.M34) <= MathHelper.ZeroTolerance) &&

                (MathF.Abs(M41 - s_identity.M41) <= MathHelper.ZeroTolerance) &&
                (MathF.Abs(M42 - s_identity.M42) <= MathHelper.ZeroTolerance) &&
                (MathF.Abs(M43 - s_identity.M43) <= MathHelper.ZeroTolerance) &&
                (MathF.Abs(M44 - s_identity.M44) <= MathHelper.ZeroTolerance);
      }
    }

    /// <summary>
    /// Gets whether any of the components of the matrix are NaN (Not A Number).
    /// </summary>
    public readonly bool IsNaN
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return float.IsNaN(M11) || float.IsNaN(M12) || float.IsNaN(M13) || float.IsNaN(M14) ||
            float.IsNaN(M21) || float.IsNaN(M22) || float.IsNaN(M23) || float.IsNaN(M24) ||
            float.IsNaN(M31) || float.IsNaN(M32) || float.IsNaN(M33) || float.IsNaN(M34) ||
            float.IsNaN(M41) || float.IsNaN(M42) || float.IsNaN(M43) || float.IsNaN(M44);
      }
    }

    /// <summary>
    /// Gets whether any of the components of the matrix are positive or negative infinity.
    /// </summary>
    public readonly bool IsInfinity
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return float.IsInfinity(M11) || float.IsInfinity(M12) || float.IsInfinity(M13) || float.IsInfinity(M14) ||
               float.IsInfinity(M21) || float.IsInfinity(M22) || float.IsInfinity(M23) || float.IsInfinity(M24) ||
               float.IsInfinity(M31) || float.IsInfinity(M32) || float.IsInfinity(M33) || float.IsInfinity(M34) ||
               float.IsInfinity(M41) || float.IsInfinity(M42) || float.IsInfinity(M43) || float.IsInfinity(M44);
      }
    }

    /// <summary>
    /// Gets or sets the matrix component at the specified index. Indices follow memory layout (first row accessed by first four indices, second row
    /// by the next four, etc).
    /// </summary>
    /// <param name="index">Zero-based index, in range of [0, 15].</param>
    /// <returns>The value at the index</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the index is out of range [0, 15]</exception>
    public float this[int index]
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      readonly get
      {
        switch (index)
        {
          case 0:
            return M11;
          case 1:
            return M12;
          case 2:
            return M13;
          case 3:
            return M14;
          case 4:
            return M21;
          case 5:
            return M22;
          case 6:
            return M23;
          case 7:
            return M24;
          case 8:
            return M31;
          case 9:
            return M32;
          case 10:
            return M33;
          case 11:
            return M34;
          case 12:
            return M41;
          case 13:
            return M42;
          case 14:
            return M43;
          case 15:
            return M44;
          default:
            throw new ArgumentOutOfRangeException(nameof(index), StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));
        }
      }
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      set
      {
        switch (index)
        {
          case 0:
            M11 = value;
            break;
          case 1:
            M12 = value;
            break;
          case 2:
            M13 = value;
            break;
          case 3:
            M14 = value;
            break;
          case 4:
            M21 = value;
            break;
          case 5:
            M22 = value;
            break;
          case 6:
            M23 = value;
            break;
          case 7:
            M24 = value;
            break;
          case 8:
            M31 = value;
            break;
          case 9:
            M32 = value;
            break;
          case 10:
            M33 = value;
            break;
          case 11:
            M34 = value;
            break;
          case 12:
            M41 = value;
            break;
          case 13:
            M42 = value;
            break;
          case 14:
            M43 = value;
            break;
          case 15:
            M44 = value;
            break;
          default:
            throw new ArgumentOutOfRangeException(nameof(index), StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));
        }
      }
    }

    /// <summary>
    /// Gets or sets an individual component based on its index
    /// in the matrix. The indices are zero-based, so
    /// the first element is located at (0,0) and the last element
    /// at (3,3). 
    /// </summary>
    /// <param name="row">Zero-based row index valid between [0, 3]</param>
    /// <param name="column">Zerp-based column index valid between [0, 3]</param>
    /// <returns>The matrix value</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if either index is out of range [0, 3]</exception>
    public float this[int row, int column]
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      readonly get
      {
        if (row < 0 || row > 3)
          throw new ArgumentOutOfRangeException(nameof(row), StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));

        if (column < 0 || column > 3)
          throw new ArgumentOutOfRangeException(nameof(column), StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));

        return this[(row * 4) + column];
      }
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      set
      {
        if (row < 0 || row > 3)
          throw new ArgumentOutOfRangeException(nameof(row), StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));

        if (column < 0 || column > 3)
          throw new ArgumentOutOfRangeException(nameof(column), StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));

        this[(row * 4) + column] = value;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Matrix"/> struct.
    /// </summary>
    /// <param name="value">The value assigned to all matrix components</param>
    public Matrix(float value)
    {
      M11 = M12 = M13 = M14 = M21 = M22 = M23 = M24 = M31 = M32 = M33 = M34 = M41 = M42 = M43 = M44 = value;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Matrix"/> struct.
    /// </summary>
    /// <param name="row1">First row</param>
    /// <param name="row2">Second row</param>
    /// <param name="row3">Third row</param>
    /// <param name="row4">Fourth row</param>
    public Matrix(in Vector4 row1, in Vector4 row2, in Vector4 row3, in Vector4 row4)
    {
      M11 = row1.X;
      M12 = row1.Y;
      M13 = row1.Z;
      M14 = row1.W;

      M21 = row2.X;
      M22 = row2.Y;
      M23 = row2.Z;
      M24 = row3.W;

      M31 = row3.X;
      M32 = row3.Y;
      M33 = row3.Z;
      M34 = row3.W;

      M41 = row4.X;
      M42 = row4.Y;
      M43 = row4.Z;
      M44 = row4.W;
    }


    /// <summary>
    /// Constructs a new instance of the <see cref="Matrix"/> struct.
    /// </summary>
    /// <param name="m11">Element at row 1, column 1</param>
    /// <param name="m12">Element at row 1, column 2</param>
    /// <param name="m13">Element at row 1, column 3</param>
    /// <param name="m14">Element at row 1, column 4</param>
    /// <param name="m21">Element at row 2, column 1</param>
    /// <param name="m22">Element at row 2, column 2</param>
    /// <param name="m23">Element at row 2, column 3</param>
    /// <param name="m24">Element at row 2, column 4</param>
    /// <param name="m31">Element at row 3, column 1</param>
    /// <param name="m32">Element at row 3, column 2</param>
    /// <param name="m33">Element at row 3, column 3</param>
    /// <param name="m34">Element at row 3, column 4</param>
    /// <param name="m41">Element at row 4, column 1</param>
    /// <param name="m42">Element at row 4, column 2</param>
    /// <param name="m43">Element at row 4, column 3</param>
    /// <param name="m44">Element at row 4, column 4</param>
    public Matrix(float m11, float m12, float m13, float m14, float m21, float m22, float m23, float m24,
        float m31, float m32, float m33, float m34, float m41, float m42, float m43, float m44)
    {
      M11 = m11;
      M12 = m12;
      M13 = m13;
      M14 = m14;

      M21 = m21;
      M22 = m22;
      M23 = m23;
      M24 = m24;

      M31 = m31;
      M32 = m32;
      M33 = m33;
      M34 = m34;

      M41 = m41;
      M42 = m42;
      M43 = m43;
      M44 = m44;
    }

    /// <summary>
    /// Adds two matrices.
    /// </summary>
    /// <param name="a">First matrix</param>
    /// <param name="b">Second matrix</param>
    /// <returns>Sum of the two matrices</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix Add(in Matrix a, in Matrix b)
    {
      Matrix.Add(a, b, out Matrix result);

      return result;
    }

    /// <summary>
    /// Adds two matrices.
    /// </summary>
    /// <param name="a">First matrix</param>
    /// <param name="b">Second matrix</param>
    /// <param name="result">Sum of the two matrices</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Add(in Matrix a, in Matrix b, out Matrix result)
    {
      result.M11 = a.M11 + b.M11;
      result.M12 = a.M12 + b.M12;
      result.M13 = a.M13 + b.M13;
      result.M14 = a.M14 + b.M14;

      result.M21 = a.M21 + b.M21;
      result.M22 = a.M22 + b.M22;
      result.M23 = a.M23 + b.M23;
      result.M24 = a.M24 + b.M24;

      result.M31 = a.M31 + b.M31;
      result.M32 = a.M32 + b.M32;
      result.M33 = a.M33 + b.M33;
      result.M34 = a.M34 + b.M34;

      result.M41 = a.M41 + b.M41;
      result.M42 = a.M42 + b.M42;
      result.M43 = a.M43 + b.M43;
      result.M44 = a.M44 + b.M44;
    }

    /// <summary>
    /// Subtracts matrix b from matrix a.
    /// </summary>
    /// <param name="a">First matrix</param>
    /// <param name="b">Second matrix</param>
    /// <returns>Difference of the two matrices</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix Subtract(in Matrix a, in Matrix b)
    {
      Matrix.Subtract(a, b, out Matrix result);

      return result;
    }

    /// <summary>
    /// Subtracts matrix b from matrix a.
    /// </summary>
    /// <param name="a">First matrix</param>
    /// <param name="b">Second matrix</param>
    /// <param name="result">Difference of the two matrices</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Subtract(in Matrix a, in Matrix b, out Matrix result)
    {
      result.M11 = a.M11 - b.M11;
      result.M12 = a.M12 - b.M12;
      result.M13 = a.M13 - b.M13;
      result.M14 = a.M14 - b.M14;

      result.M21 = a.M21 - b.M21;
      result.M22 = a.M22 - b.M22;
      result.M23 = a.M23 - b.M23;
      result.M24 = a.M24 - b.M24;

      result.M31 = a.M31 - b.M31;
      result.M32 = a.M32 - b.M32;
      result.M33 = a.M33 - b.M33;
      result.M34 = a.M34 - b.M34;

      result.M41 = a.M41 - b.M41;
      result.M42 = a.M42 - b.M42;
      result.M43 = a.M43 - b.M43;
      result.M44 = a.M44 - b.M44;
    }

    /// <summary>
    /// Multiplies two matrices.
    /// </summary>
    /// <param name="a">First matrix</param>
    /// <param name="b">Second matrix</param>
    /// <returns>Product of the two matrices</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix Multiply(in Matrix a, in Matrix b)
    {
      Matrix.Multiply(a, b, out Matrix result);

      return result;
    }

    /// <summary>
    /// Multiplies two matrices.
    /// </summary>
    /// <param name="a">First matrix</param>
    /// <param name="b">Second matrix</param>
    /// <param name="result">Product of the two matrices</param>
    public static void Multiply(in Matrix a, in Matrix b, out Matrix result)
    {
      float m11 = (((a.M11 * b.M11) + (a.M12 * b.M21)) + (a.M13 * b.M31)) + (a.M14 * b.M41);
      float m12 = (((a.M11 * b.M12) + (a.M12 * b.M22)) + (a.M13 * b.M32)) + (a.M14 * b.M42);
      float m13 = (((a.M11 * b.M13) + (a.M12 * b.M23)) + (a.M13 * b.M33)) + (a.M14 * b.M43);
      float m14 = (((a.M11 * b.M14) + (a.M12 * b.M24)) + (a.M13 * b.M34)) + (a.M14 * b.M44);

      float m21 = (((a.M21 * b.M11) + (a.M22 * b.M21)) + (a.M23 * b.M31)) + (a.M24 * b.M41);
      float m22 = (((a.M21 * b.M12) + (a.M22 * b.M22)) + (a.M23 * b.M32)) + (a.M24 * b.M42);
      float m23 = (((a.M21 * b.M13) + (a.M22 * b.M23)) + (a.M23 * b.M33)) + (a.M24 * b.M43);
      float m24 = (((a.M21 * b.M14) + (a.M22 * b.M24)) + (a.M23 * b.M34)) + (a.M24 * b.M44);

      float m31 = (((a.M31 * b.M11) + (a.M32 * b.M21)) + (a.M33 * b.M31)) + (a.M34 * b.M41);
      float m32 = (((a.M31 * b.M12) + (a.M32 * b.M22)) + (a.M33 * b.M32)) + (a.M34 * b.M42);
      float m33 = (((a.M31 * b.M13) + (a.M32 * b.M23)) + (a.M33 * b.M33)) + (a.M34 * b.M43);
      float m34 = (((a.M31 * b.M14) + (a.M32 * b.M24)) + (a.M33 * b.M34)) + (a.M34 * b.M44);

      float m41 = (((a.M41 * b.M11) + (a.M42 * b.M21)) + (a.M43 * b.M31)) + (a.M44 * b.M41);
      float m42 = (((a.M41 * b.M12) + (a.M42 * b.M22)) + (a.M43 * b.M32)) + (a.M44 * b.M42);
      float m43 = (((a.M41 * b.M13) + (a.M42 * b.M23)) + (a.M43 * b.M33)) + (a.M44 * b.M43);
      float m44 = (((a.M41 * b.M14) + (a.M42 * b.M24)) + (a.M43 * b.M34)) + (a.M44 * b.M44);

      result.M11 = m11;
      result.M12 = m12;
      result.M13 = m13;
      result.M14 = m14;

      result.M21 = m21;
      result.M22 = m22;
      result.M23 = m23;
      result.M24 = m24;

      result.M31 = m31;
      result.M32 = m32;
      result.M33 = m33;
      result.M34 = m34;

      result.M41 = m41;
      result.M42 = m42;
      result.M43 = m43;
      result.M44 = m44;
    }

    /// <summary>
    /// Multiplies a matrix by a scalar value.
    /// </summary>
    /// <param name="value">Source matrix</param>
    /// <param name="scale">Amount to scale</param>
    /// <returns>Multiplied matrix</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix Multiply(in Matrix value, float scale)
    {
      Matrix.Multiply(value, scale, out Matrix result);

      return result;
    }

    /// <summary>
    /// Multiplies a matrix by a scalar value.
    /// </summary>
    /// <param name="value">Source matrix</param>
    /// <param name="scale">Amount to scale</param>
    /// <param name="result">Multiplied matrix</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Multiply(in Matrix value, float scale, out Matrix result)
    {
      result.M11 = value.M11 * scale;
      result.M12 = value.M12 * scale;
      result.M13 = value.M13 * scale;
      result.M14 = value.M14 * scale;

      result.M21 = value.M21 * scale;
      result.M22 = value.M22 * scale;
      result.M23 = value.M23 * scale;
      result.M24 = value.M24 * scale;

      result.M31 = value.M31 * scale;
      result.M32 = value.M32 * scale;
      result.M33 = value.M33 * scale;
      result.M34 = value.M34 * scale;

      result.M41 = value.M41 * scale;
      result.M42 = value.M42 * scale;
      result.M43 = value.M43 * scale;
      result.M44 = value.M44 * scale;
    }

    /// <summary>
    /// Divides the a matrice's components by the components of another.
    /// </summary>
    /// <param name="a">Source matrix</param>
    /// <param name="b">Divisor matrix</param>
    /// <returns>Quotient of the two matrices</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix Divide(in Matrix a, in Matrix b)
    {
      Matrix.Divide(a, b, out Matrix result);

      return result;
    }

    /// <summary>
    /// Divides the a matrice's components by the components of another.
    /// </summary>
    /// <param name="a">Source matrix</param>
    /// <param name="b">Divisor matrix</param>
    /// <param name="result">Quotient of the two matrices</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Divide(in Matrix a, in Matrix b, out Matrix result)
    {
      result.M11 = a.M11 / b.M11;
      result.M12 = a.M12 / b.M12;
      result.M13 = a.M13 / b.M13;
      result.M14 = a.M14 / b.M14;

      result.M21 = a.M21 / b.M21;
      result.M22 = a.M22 / b.M22;
      result.M23 = a.M23 / b.M23;
      result.M24 = a.M24 / b.M24;

      result.M31 = a.M31 / b.M31;
      result.M32 = a.M32 / b.M32;
      result.M33 = a.M33 / b.M33;
      result.M34 = a.M34 / b.M34;

      result.M41 = a.M41 / b.M41;
      result.M42 = a.M42 / b.M42;
      result.M43 = a.M43 / b.M43;
      result.M44 = a.M44 / b.M44;
    }

    /// <summary>
    /// Divides the a matrice's components by a scalar value.
    /// </summary>
    /// <param name="value">Source matrix</param>
    /// <param name="divisor">Divisor scalar</param>
    /// <returns>Divided matrix</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix Divide(in Matrix value, float divisor)
    {
      Matrix.Divide(value, divisor, out Matrix result);

      return result;
    }

    /// <summary>
    /// Divides the a matrice's components by a scalar value.
    /// </summary>
    /// <param name="value">Source matrix</param>
    /// <param name="divisor">Divisor scalar</param>
    /// <param name="result">Divided matrix</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Divide(in Matrix value, float divisor, out Matrix result)
    {
      float invScale = 1.0f / divisor;
      result.M11 = value.M11 * invScale;
      result.M12 = value.M12 * invScale;
      result.M13 = value.M13 * invScale;
      result.M14 = value.M14 * invScale;

      result.M21 = value.M21 * invScale;
      result.M22 = value.M22 * invScale;
      result.M23 = value.M23 * invScale;
      result.M24 = value.M24 * invScale;

      result.M31 = value.M31 * invScale;
      result.M32 = value.M32 * invScale;
      result.M33 = value.M33 * invScale;
      result.M34 = value.M34 * invScale;

      result.M41 = value.M41 * invScale;
      result.M42 = value.M42 * invScale;
      result.M43 = value.M43 * invScale;
      result.M44 = value.M44 * invScale;
    }

    /// <summary>
    /// Computes the inverse of the matrix.
    /// </summary>
    /// <param name="matrix">Source matrix</param>
    /// <returns>Inverse of the matrix</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix Invert(in Matrix matrix)
    {
      Matrix.Invert(matrix, out Matrix result);

      return result;
    }

    /// <summary>
    /// Compute the inverse of the specified matrix.
    /// </summary>
    /// <param name="matrix">Source matrix</param>
    /// <param name="result">Inverted matrixt</param>
    public static void Invert(in Matrix matrix, out Matrix result)
    {
      float m11 = matrix.M11;
      float m12 = matrix.M12;
      float m13 = matrix.M13;
      float m14 = matrix.M14;
      float m21 = matrix.M21;
      float m22 = matrix.M22;
      float m23 = matrix.M23;
      float m24 = matrix.M24;
      float m31 = matrix.M31;
      float m32 = matrix.M32;
      float m33 = matrix.M33;
      float m34 = matrix.M34;
      float m41 = matrix.M41;
      float m42 = matrix.M42;
      float m43 = matrix.M43;
      float m44 = matrix.M44;

      float h1 = (m33 * m44) - (m34 * m43);
      float h2 = (m32 * m44) - (m34 * m42);
      float h3 = (m32 * m43) - (m33 * m42);
      float h4 = (m31 * m44) - (m34 * m41);
      float h5 = (m31 * m43) - (m33 * m41);
      float h6 = (m31 * m42) - (m32 * m41);

      float e1 = ((m22 * h1) - (m23 * h2)) + (m24 * h3);
      float e2 = -(((m21 * h1) - (m23 * h4)) + (m24 * h5));
      float e3 = ((m21 * h2) - (m22 * h4)) + (m24 * h6);
      float e4 = -(((m21 * h3) - (m22 * h5)) + (m23 * h6));
      float invDet = 1.0f / ((((m11 * e1) + (m12 * e2)) + (m13 * e3)) + (m14 * e4));

      result.M11 = e1 * invDet;
      result.M21 = e2 * invDet;
      result.M31 = e3 * invDet;
      result.M41 = e4 * invDet;

      result.M12 = -(((m12 * h1) - (m13 * h2)) + (m14 * h3)) * invDet;
      result.M22 = (((m11 * h1) - (m13 * h4)) + (m14 * h5)) * invDet;
      result.M32 = -(((m11 * h2) - (m12 * h4)) + (m14 * h6)) * invDet;
      result.M42 = (((m11 * h3) - (m12 * h5)) + (m13 * h6)) * invDet;

      float h7 = (m23 * m44) - (m24 * m43);
      float h8 = (m22 * m44) - (m24 * m42);
      float h9 = (m22 * m43) - (m23 * m42);
      float h10 = (m21 * m44) - (m24 * m41);
      float h11 = (m21 * m43) - (m23 * m41);
      float h12 = (m21 * m42) - (m22 * m41);

      result.M13 = (((m12 * h7) - (m13 * h8)) + (m14 * h9)) * invDet;
      result.M23 = -(((m11 * h7) - (m13 * h10)) + (m14 * h11)) * invDet;
      result.M33 = (((m11 * h8) - (m12 * h10)) + (m14 * h12)) * invDet;
      result.M43 = -(((m11 * h9) - (m12 * h11)) + (m13 * h12)) * invDet;

      float h13 = (m23 * m34) - (m24 * m33);
      float h14 = (m22 * m34) - (m24 * m32);
      float h15 = (m22 * m33) - (m23 * m32);
      float h16 = (m21 * m34) - (m24 * m31);
      float h17 = (m21 * m33) - (m23 * m31);
      float h18 = (m21 * m32) - (m22 * m31);

      result.M14 = -(((m12 * h13) - (m13 * h14)) + (m14 * h15)) * invDet;
      result.M24 = (((m11 * h13) - (m13 * h16)) + (m14 * h17)) * invDet;
      result.M34 = -(((m11 * h14) - (m12 * h16)) + (m14 * h18)) * invDet;
      result.M44 = (((m11 * h15) - (m12 * h17)) + (m13 * h18)) * invDet;
    }

    /// <summary>
    /// Linearly interpolates between the two specified matrices.
    /// </summary>
    /// <param name="a">Starting matrix</param>
    /// <param name="b">Ending matrix</param>
    /// <param name="percent">Percent to interpolate by</param>
    /// <returns>Linearly interpolated matrix</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix Lerp(in Matrix a, in Matrix b, float percent)
    {
      Matrix.Lerp(a, b, percent, out Matrix result);

      return result;
    }

    /// <summary>
    /// Linearly interpolates between the two specified matrices.
    /// </summary>
    /// <param name="a">Starting matrix</param>
    /// <param name="b">Ending matrix</param>
    /// <param name="percent">Percent to interpolate by</param>
    /// <param name="result">Linearly interpolated matrix</param>
    public static void Lerp(in Matrix a, in Matrix b, float percent, out Matrix result)
    {
      result.M11 = a.M11 + ((b.M11 - a.M11) * percent);
      result.M12 = a.M12 + ((b.M12 - a.M12) * percent);
      result.M13 = a.M13 + ((b.M13 - a.M13) * percent);
      result.M14 = a.M14 + ((b.M14 - a.M14) * percent);

      result.M21 = a.M21 + ((b.M21 - a.M21) * percent);
      result.M22 = a.M22 + ((b.M22 - a.M22) * percent);
      result.M23 = a.M23 + ((b.M23 - a.M23) * percent);
      result.M24 = a.M24 + ((b.M24 - a.M24) * percent);

      result.M31 = a.M31 + ((b.M31 - a.M31) * percent);
      result.M32 = a.M32 + ((b.M32 - a.M32) * percent);
      result.M33 = a.M33 + ((b.M33 - a.M33) * percent);
      result.M34 = a.M34 + ((b.M34 - a.M34) * percent);

      result.M41 = a.M41 + ((b.M41 - a.M41) * percent);
      result.M42 = a.M42 + ((b.M42 - a.M42) * percent);
      result.M43 = a.M43 + ((b.M43 - a.M43) * percent);
      result.M44 = a.M44 + ((b.M44 - a.M44) * percent);
    }

    /// <summary>
    /// Creates a view matrix with specified eye position, a target, and the up vector in the world.
    /// </summary>
    /// <param name="eyePosition">Position of the camera</param>
    /// <param name="target">Camera's target</param>
    /// <param name="up">Up vector of camera</param>
    /// <returns>View matrix</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix CreateViewMatrix(in Vector3 eyePosition, in Vector3 target, in Vector3 up)
    {
      Matrix.CreateViewMatrix(eyePosition, target, up, out Matrix result);

      return result;
    }

    /// <summary>
    /// Creates a view matrix with specified eye position, a target, and the up vector in the world.
    /// </summary>
    /// <param name="position">Position of the camera</param>
    /// <param name="target">Camera's target</param>
    /// <param name="up">Up vector of camera</param>
    /// <param name="result">View matrix</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void CreateViewMatrix(in Vector3 position, in Vector3 target, in Vector3 up, out Matrix result)
    {
      Vector3.NormalizedSubtract(position, target, out Vector3 direction);
      Vector3.NormalizedCross(up, direction, out Vector3 left);
      Vector3.NormalizedCross(direction, left, out Vector3 newUp);

      result.M11 = left.X;
      result.M12 = newUp.X;
      result.M13 = direction.X;
      result.M14 = 0.0f;

      result.M21 = left.Y;
      result.M22 = newUp.Y;
      result.M23 = direction.Y;
      result.M24 = 0.0f;

      result.M31 = left.Z;
      result.M32 = newUp.Z;
      result.M33 = direction.Z;
      result.M34 = 0.0f;

      result.M41 = -Vector3.Dot(left, position);
      result.M42 = -Vector3.Dot(newUp, position);
      result.M43 = -Vector3.Dot(direction, position);
      result.M44 = 1.0f;
    }

    /// <summary>
    /// Creates an orthographic projection matrix.
    /// </summary>
    /// <param name="width">Width of view volume</param>
    /// <param name="height">Height of view volume</param>
    /// <param name="zNearPlane">Minimum z value of the view volume</param>
    /// <param name="zFarPlane">Maximum z value of the view volume</param>
    /// <returns>Orthographic projection matrix</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix CreateOrthographicMatrix(float width, float height, float zNearPlane, float zFarPlane)
    {
      Matrix.CreateOrthographicMatrix(width, height, zNearPlane, zFarPlane, out Matrix result);

      return result;
    }

    /// <summary>
    /// Creates an orthographic projection matrix.
    /// </summary>
    /// <param name="width">Width of view volume</param>
    /// <param name="height">Height of view volume</param>
    /// <param name="zNearPlane">Minimum z value of the view volume</param>
    /// <param name="zFarPlane">Maximum z value of the view volume</param>
    /// <param name="result">Orthographic projection matrix</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void CreateOrthographicMatrix(float width, float height, float zNearPlane, float zFarPlane, out Matrix result)
    {
      result.M11 = 2.0f / width;
      result.M12 = 0.0f;
      result.M13 = 0.0f;
      result.M14 = 0.0f;

      result.M21 = 0.0f;
      result.M22 = 2.0f / height;
      result.M23 = 0.0f;
      result.M24 = 0.0f;

      result.M31 = 0.0f;
      result.M32 = 0.0f;
      result.M33 = 1.0f / (zNearPlane - zFarPlane);
      result.M34 = 0.0f;

      result.M41 = 0.0f;
      result.M42 = 0.0f;
      result.M43 = zNearPlane / (zNearPlane - zFarPlane);
      result.M44 = 1.0f;
    }

    /// <summary>
    /// Creates a custom orthographic projection matrix.
    /// </summary>
    /// <param name="left">Minimum x value of view volume</param>
    /// <param name="right">Maximum x value of view volume</param>
    /// <param name="bottom">Minimum y value of view volume</param>
    /// <param name="top">Maximum y value of view volume</param>
    /// <param name="zNearPlane">Minimum z value of view volume</param>
    /// <param name="zFarPlane">Maximum z value of view volume</param>
    /// <returns>Orthographic projection matrix</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix CreateOrthographicMatrix(float left, float right, float bottom, float top, float zNearPlane, float zFarPlane)
    {
      Matrix.CreateOrthographicMatrix(left, right, bottom, top, zNearPlane, zFarPlane, out Matrix result);

      return result;
    }

    /// <summary>
    /// Creates a custom orthographic projection matrix.
    /// </summary>
    /// <param name="left">Minimum x value of view volume</param>
    /// <param name="right">Maximum x value of view volume</param>
    /// <param name="bottom">Minimum y value of view volume</param>
    /// <param name="top">Maximum y value of view volume</param>
    /// <param name="zNearPlane">Minimum z value of view volume</param>
    /// <param name="zFarPlane">Maximum z value of view volume</param>
    /// <param name="result">Orthographic projection matrix</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void CreateOrthographicMatrix(float left, float right, float bottom, float top, float zNearPlane, float zFarPlane, out Matrix result)
    {
      result.M11 = 2.0f / (right - left);
      result.M12 = 0.0f;
      result.M13 = 0.0f;
      result.M14 = 0.0f;

      result.M21 = 0.0f;
      result.M22 = 2.0f / (top - bottom);
      result.M23 = 0.0f;
      result.M24 = 0.0f;

      result.M31 = 0.0f;
      result.M32 = 0.0f;
      result.M33 = 1.0f / (zNearPlane - zFarPlane);
      result.M34 = 0.0f;

      result.M41 = (left + right) / (left - right);
      result.M42 = (top + bottom) / (bottom - top);
      result.M43 = zNearPlane / (zNearPlane - zFarPlane);
      result.M44 = 1.0f;
    }

    /// <summary>
    /// Creates a perspective projection matrix.
    /// </summary>
    /// <param name="width">Width of the view volume at the near view plane</param>
    /// <param name="height">Height of the view volume at the near view plane</param>
    /// <param name="nearPlaneDistance">Distance to the near view plane</param>
    /// <param name="farPlaneDistance">Distance to the far view plane</param>
    /// <returns>Perspective projection matrix</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the near/far planes are not greater than zero or if the near plane is greater than or equal to the far plane.</exception>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix CreatePerspectiveMatrix(float width, float height, float nearPlaneDistance, float farPlaneDistance)
    {
      Matrix.CreatePerspectiveMatrix(width, height, nearPlaneDistance, farPlaneDistance, out Matrix result);

      return result;
    }

    /// <summary>
    /// Creates a perspective projection matrix.
    /// </summary>
    /// <param name="width">Width of the view volume at the near view plane</param>
    /// <param name="height">Height of the view volume at the near view plane</param>
    /// <param name="nearPlaneDistance">Distance to the near view plane</param>
    /// <param name="farPlaneDistance">Distance to the far view plane</param>
    /// <param name="result">Perspective projection matrix</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the near/far planes are not greater than zero or if the near plane is greater than or equal to the far plane.</exception>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void CreatePerspectiveMatrix(float width, float height, float nearPlaneDistance, float farPlaneDistance, out Matrix result)
    {
      ArgumentOutOfRangeException.ThrowIfLessThanOrEqual(nearPlaneDistance, 0.0f, nameof(nearPlaneDistance));
      ArgumentOutOfRangeException.ThrowIfLessThanOrEqual(farPlaneDistance, 0.0f, nameof(farPlaneDistance));

      if (nearPlaneDistance >= farPlaneDistance)
        throw new ArgumentOutOfRangeException(nameof(nearPlaneDistance), StringLocalizer.Instance.GetLocalizedString("NearPlaneDistanceLargerThanFarPlane"));
      

      result.M11 = (2.0f * nearPlaneDistance) / width;
      result.M12 = 0.0f;
      result.M13 = 0.0f;
      result.M14 = 0.0f;

      result.M21 = 0.0f;
      result.M22 = (2.0f * nearPlaneDistance) / height;
      result.M23 = 0.0f;
      result.M24 = 0.0f;

      result.M31 = 0.0f;
      result.M32 = 0.0f;
      result.M33 = farPlaneDistance / (nearPlaneDistance - farPlaneDistance);
      result.M34 = -1.0f;

      result.M41 = 0.0f;
      result.M42 = 0.0f;
      result.M43 = (nearPlaneDistance * farPlaneDistance) / (nearPlaneDistance - farPlaneDistance);
      result.M44 = 0.0f;
    }

    /// <summary>
    /// Creates a perspective projection matrix based on a field of view.
    /// </summary>
    /// <param name="fieldOfView">Field of view angle, in the range of [0 and Pi)</param>
    /// <param name="aspectRatio">Aspect ratio, defined as the view space's width divided by height</param>
    /// <param name="nearPlaneDistance">Distance to the near plane</param>
    /// <param name="farPlaneDistance">Distance to the far plane</param>
    /// <returns>Perspective projection matrix</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the Field of View is not within range, if the near/far planes are not greater than zero, 
    /// or if the near plane is greater than or equal to the far plane.</exception>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix CreatePerspectiveFOVMatrix(Angle fieldOfView, float aspectRatio, float nearPlaneDistance, float farPlaneDistance)
    {
      Matrix.CreatePerspectiveFOVMatrix(fieldOfView, aspectRatio, nearPlaneDistance, farPlaneDistance, out Matrix result);

      return result;
    }

    /// <summary>
    /// Creates a perspective projection matrix based on a field of view.
    /// </summary>
    /// <param name="fieldOfView">Field of view angle, in the range of [0 and Pi)</param>
    /// <param name="aspectRatio">Aspect ratio, defined as the view space's width divided by height</param>
    /// <param name="nearPlaneDistance">Distance to the near plane</param>
    /// <param name="farPlaneDistance">Distance to the far plane</param>
    /// <param name="result">Perspective projection matrix</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the Field of View is not within range, if the near/far planes/aspectRatio are not greater than zero, 
    /// or if the near plane is greater than or equal to the far plane.</exception>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void CreatePerspectiveFOVMatrix(Angle fieldOfView, float aspectRatio, float nearPlaneDistance, float farPlaneDistance, out Matrix result)
    {
      if ((fieldOfView.Radians <= 0.0f) || (fieldOfView.Radians >= MathHelper.Pi))
        throw new ArgumentOutOfRangeException(nameof(fieldOfView), StringLocalizer.Instance.GetLocalizedString("MustBeZeroToPIRange"));

      ArgumentOutOfRangeException.ThrowIfLessThanOrEqual(aspectRatio, 0.0f, nameof(aspectRatio));
      ArgumentOutOfRangeException.ThrowIfLessThanOrEqual(nearPlaneDistance, 0.0f, nameof(nearPlaneDistance));
      ArgumentOutOfRangeException.ThrowIfLessThanOrEqual(farPlaneDistance, 0.0f, nameof(farPlaneDistance));

      if (nearPlaneDistance >= farPlaneDistance)
        throw new ArgumentOutOfRangeException(nameof(nearPlaneDistance), StringLocalizer.Instance.GetLocalizedString("NearPlaneDistanceLargerThanFarPlane"));
      

      float h = 1.0f / MathF.Tan(fieldOfView.Radians * 0.5f);
      float w = h / aspectRatio;

      result.M11 = w;
      result.M12 = 0.0f;
      result.M13 = 0.0f;
      result.M14 = 0.0f;

      result.M21 = 0.0f;
      result.M22 = h;
      result.M23 = 0.0f;
      result.M24 = 0.0f;

      result.M31 = 0.0f;
      result.M32 = 0.0f;
      result.M33 = farPlaneDistance / (nearPlaneDistance - farPlaneDistance);
      result.M34 = -1.0f;

      result.M41 = 0.0f;
      result.M42 = 0.0f;
      result.M43 = (nearPlaneDistance * farPlaneDistance) / (nearPlaneDistance - farPlaneDistance);
      result.M44 = 0f;
    }

    /// <summary>
    /// Creates a perspective projection matrix based on a field of view that is infinite, meaning there is no far plane.
    /// </summary>
    /// <param name="fieldOfView">Field of view angle, in the range of [0 and Pi)</param>
    /// <param name="aspectRatio">Aspect ratio, defined as the view space's width divided by height</param>
    /// <param name="nearPlaneDistance">Distance to the near plane</param>
    /// <returns>Perspective projection matrix</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the Field of View is not within range, if the near plane/aspectRatio are not greater than zero.</exception>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix CreateInfinitePerspectiveFOVMatrix(Angle fieldOfView, float aspectRatio, float nearPlaneDistance)
    {
      Matrix.CreateInfinitePerspectiveFOVMatrix(fieldOfView, aspectRatio, nearPlaneDistance, out Matrix result);

      return result;
    }

    /// <summary>
    /// Creates a perspective projection matrix based on a field of view that is infinite, meaning there is no far plane.
    /// </summary>
    /// <param name="fieldOfView">Field of view angle, in the range of [0 and Pi)</param>
    /// <param name="aspectRatio">Aspect ratio, defined as the view space's width divided by height</param>
    /// <param name="nearPlaneDistance">Distance to the near plane</param>
    /// <param name="result">Perspective projection matrix</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the Field of View is not within range, if the near plane/aspectRatio are not greater than zero.</exception>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void CreateInfinitePerspectiveFOVMatrix(Angle fieldOfView, float aspectRatio, float nearPlaneDistance, out Matrix result)
    {
      if ((fieldOfView.Radians <= 0.0f) || (fieldOfView.Radians >= MathHelper.Pi))
        throw new ArgumentOutOfRangeException(nameof(fieldOfView), StringLocalizer.Instance.GetLocalizedString("MustBeZeroToPIRange"));

      ArgumentOutOfRangeException.ThrowIfLessThanOrEqual(aspectRatio, 0.0f, nameof(aspectRatio));
      ArgumentOutOfRangeException.ThrowIfLessThanOrEqual(nearPlaneDistance, 0.0f, nameof(nearPlaneDistance));

      float e = MathHelper.TightZeroTolerance;
      float h = 1.0f / MathF.Tan(fieldOfView.Radians * 0.5f);

      result.M11 = h / aspectRatio;
      result.M12 = 0.0f;
      result.M13 = 0.0f;
      result.M14 = 0.0f;

      result.M21 = 0.0f;
      result.M22 = h;
      result.M23 = 0.0f;
      result.M24 = 0.0f;

      result.M31 = 0.0f;
      result.M32 = 0.0f;
      result.M33 = e;
      result.M34 = -1.0f; // +1 for LH, we deal with RH only

      result.M41 = 0.0f;
      result.M42 = 0.0f;
      result.M43 = nearPlaneDistance * (1.0f - e);
      result.M44 = 0f;
    }

    /// <summary>
    /// Creates a custom perspective projection matrix.
    /// </summary>
    /// <param name="left">Minimum x value of the view volume at the near view plane</param>
    /// <param name="right">Maximum x value of the view volume at the near view plane</param>
    /// <param name="bottom">Minimum y value of the view volume at the near view plane</param>
    /// <param name="top">Maximum y value of the view volume at the near view plane</param>
    /// <param name="nearPlaneDistance">Distance to the near view plane</param>
    /// <param name="farPlaneDistance">Distance to the far view plane</param>
    /// <returns>Perspective projection matrix</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the near/far planes are not greater than zero or if the near plane is greater than or equal to the far plane.</exception>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix CreatePerspectiveMatrix(float left, float right, float bottom, float top, float nearPlaneDistance, float farPlaneDistance)
    {
      Matrix.CreatePerspectiveMatrix(left, right, bottom, top, nearPlaneDistance, farPlaneDistance, out Matrix result);

      return result;
    }

    /// <summary>
    /// Creates a custom perspective projection matrix.
    /// </summary>
    /// <param name="left">Minimum x value of the view volume at the near view plane</param>
    /// <param name="right">Maximum x value of the view volume at the near view plane</param>
    /// <param name="bottom">Minimum y value of the view volume at the near view plane</param>
    /// <param name="top">Maximum y value of the view volume at the near view plane</param>
    /// <param name="nearPlaneDistance">Distance to the near view plane</param>
    /// <param name="farPlaneDistance">Distance to the far view plane</param>
    /// <param name="result">Perspective projection matrix</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the near/far planes are not greater than zero or if the near plane is greater than or equal to the far plane.</exception>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void CreatePerspectiveMatrix(float left, float right, float bottom, float top, float nearPlaneDistance, float farPlaneDistance, out Matrix result)
    {
      ArgumentOutOfRangeException.ThrowIfLessThanOrEqual(nearPlaneDistance, 0.0f, nameof(nearPlaneDistance));
      ArgumentOutOfRangeException.ThrowIfLessThanOrEqual(farPlaneDistance, 0.0f, nameof(farPlaneDistance));

      if (nearPlaneDistance >= farPlaneDistance)
        throw new ArgumentOutOfRangeException(nameof(nearPlaneDistance), StringLocalizer.Instance.GetLocalizedString("NearPlaneDistanceLargerThanFarPlane"));

      result.M11 = (2.0f * nearPlaneDistance) / (right - left);
      result.M12 = 0.0f;
      result.M13 = 0.0f;
      result.M14 = 0.0f;

      result.M21 = 0.0f;
      result.M22 = (2.0f * nearPlaneDistance) / (top - bottom);
      result.M23 = 0.0f;
      result.M24 = 0.0f;

      result.M31 = (left + right) / (right - left);
      result.M32 = (top + bottom) / (top - bottom);
      result.M33 = farPlaneDistance / (nearPlaneDistance - farPlaneDistance);
      result.M34 = -1.0f;

      result.M41 = 0.0f;
      result.M42 = 0.0f;
      result.M43 = (nearPlaneDistance * farPlaneDistance) / (nearPlaneDistance - farPlaneDistance);
      result.M44 = 0.0f;
    }

    /// <summary>
    /// Creates a matrix that can reflect vectors about a plane. The plane is normalized before the matrix is computed.
    /// </summary>
    /// <param name="plane">Plane on which the reflection occurs.</param>
    /// <returns>Reflection matrix</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix CreateReflectionMatrix(in Plane plane)
    {
      Matrix.CreateReflectionMatrix(plane, out Matrix result);

      return result;
    }

    /// <summary>
    /// Creates a matrix that can reflect vectors about a plane. The plane is normalized before the matrix is computed.
    /// </summary>
    /// <param name="plane">Plane on which the reflection occurs.</param>
    /// <param name="result">Reflection matrix</param>
    public static void CreateReflectionMatrix(in Plane plane, out Matrix result)
    {
      Plane plane2 = plane;
      plane2.Normalize();

      float x = plane2.Normal.X;
      float y = plane2.Normal.Y;
      float z = plane2.Normal.Z;

      float x2 = x * -2.0f;
      float y2 = y * -2.0f;
      float z2 = z * -2.0f;

      result.M11 = (x2 * x) + 1.0f;
      result.M12 = y2 * x;
      result.M13 = z2 * x;
      result.M14 = 0.0f;

      result.M21 = x2 * y;
      result.M22 = (y2 * y) + 1.0f;
      result.M23 = (z2 * y);
      result.M24 = 0.0f;

      result.M31 = x2 * z;
      result.M32 = y2 * z;
      result.M33 = (z2 * z) + 1.0f;
      result.M34 = 0.0f;

      result.M41 = x2 * plane2.D;
      result.M42 = y2 * plane2.D;
      result.M43 = z2 * plane2.D;
      result.M44 = 1.0f;
    }

    /// <summary>
    /// Creates a matrix that flattens geometry into a shadow. The light is assumed to be a directional (parallel) light.
    /// </summary>
    /// <param name="lightDir">Light direction</param>
    /// <param name="plane">The plane onto which the shadow is projected on. The plane is automatically normalized.</param>
    /// <returns>Shadow matrix</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix CreateShadowMatrix(in Vector3 lightDir, in Plane plane)
    {
      CreateShadowMatrix(lightDir, plane, false, out Matrix result);

      return result;
    }

    /// <summary>
    /// Creates a matrix that flattens geometry into a shadow.
    /// </summary>
    /// <param name="lightDir">Light direction</param>
    /// <param name="plane">The plane onto which the shadow is projected on. The plane is automatically normalized.</param>
    /// <param name="isPointLight">True if the light is a point light, false if it is a directional (parallel) light.</param>
    /// <returns>Shadow matrix</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix CreateShadowMatrix(in Vector3 lightDir, in Plane plane, bool isPointLight)
    {
      CreateShadowMatrix(lightDir, plane, isPointLight, out Matrix result);

      return result;
    }

    /// <summary>
    /// Creates a matrix that flattens geometry into a shadow. The light is assumed to be a directional (parallel) light.
    /// </summary>
    /// <param name="lightDir">Light direction</param>
    /// <param name="plane">The plane onto which the shadow is projected on. The plane is automatically normalized.</param>
    /// <param name="result">Shadow matrix</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void CreateShadowMatrix(in Vector3 lightDir, in Plane plane, out Matrix result)
    {
      CreateShadowMatrix(lightDir, plane, false, out result);
    }

    /// <summary>
    /// Creates a matrix that flattens geometry into a shadow.
    /// </summary>
    /// <param name="lightDir">Light direction</param>
    /// <param name="plane">The plane onto which the shadow is projected on. The plane is automatically normalized.</param>
    /// <param name="isPointLight">True if the light is a point light, false if it is a directional (parallel) light.</param>
    /// <param name="result">Shadow matrix</param>
    public static void CreateShadowMatrix(in Vector3 lightDir, in Plane plane, bool isPointLight, out Matrix result)
    {
      Plane plane2 = plane;
      plane2.Normalize();

      Vector4 light = new Vector4(lightDir, (isPointLight) ? 1 : 0);

      float dot = (plane2.Normal.X * light.X) + (plane2.Normal.Y * light.Y) + (plane2.Normal.Z * light.Z) + (plane2.D * light.W);
      float x = -plane2.Normal.X;
      float y = -plane2.Normal.Y;
      float z = -plane2.Normal.Z;
      float d = -plane2.D;

      result.M11 = (x * light.X) + dot;
      result.M21 = y * light.X;
      result.M31 = z * light.X;
      result.M41 = d * light.X;

      result.M12 = x * light.Y;
      result.M22 = (y * light.Y) + dot;
      result.M32 = z * light.Y;
      result.M42 = d * light.Y;

      result.M13 = x * light.Z;
      result.M23 = y * light.Z;
      result.M33 = (z * light.Z) + dot;
      result.M43 = d * light.Z;

      result.M14 = x * light.W;
      result.M24 = y * light.W;
      result.M34 = z * light.W;
      result.M44 = (d * light.W) + dot;
    }

    /// <summary>
    /// Creates a spherical billboard that rotates around an object position that is aligned towards the camera.
    /// </summary>
    /// <param name="objectPosition">Position of the object which the billboard will rotate around</param>
    /// <param name="cameraPosition">Position of the camera</param>
    /// <param name="cameraUpVector">Up vector of the camera</param>
    /// <param name="cameraForwardVector">Optional forward vector (direction) of the camera</param>
    /// <returns>Billboard matrix</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix CreateBillboardMatrix(in Vector3 objectPosition, in Vector3 cameraPosition, in Vector3 cameraUpVector, in Vector3? cameraForwardVector)
    {
      Matrix.CreateBillboardMatrix(objectPosition, cameraPosition, cameraUpVector, cameraForwardVector, out Matrix result);

      return result;
    }

    /// <summary>
    /// Creates a spherical billboard that rotates around an object position that is aligned towards the camera.
    /// </summary>
    /// <param name="objectPosition">Position of the object which the billboard will rotate around</param>
    /// <param name="cameraPosition">Position of the camera</param>
    /// <param name="cameraUpVector">Up vector of the camera</param>
    /// <param name="cameraForwardVector">Optional forward vector (direction) of the camera</param>
    /// <param name="result">Billboard matrix</param>
    public static void CreateBillboardMatrix(in Vector3 objectPosition, in Vector3 cameraPosition, in Vector3 cameraUpVector, in Vector3? cameraForwardVector, out Matrix result)
    {
      Vector3 viewDir;
      Vector3.Subtract(objectPosition, cameraPosition, out viewDir);
      float diffLengthSquared = viewDir.LengthSquared();

      if (MathHelper.IsNearlyZero(diffLengthSquared))
      {
        viewDir = cameraForwardVector.HasValue ? -cameraForwardVector.Value : Vector3.Forward;
      }
      else
      {
        Vector3.Multiply(viewDir, 1.0f / MathF.Sqrt(diffLengthSquared), out viewDir);
      }

      Vector3 rightDir;
      Vector3 billboardUp;
      Vector3.NormalizedCross(cameraUpVector, viewDir, out rightDir);
      Vector3.NormalizedCross(viewDir, rightDir, out billboardUp);

      result.M11 = rightDir.X;
      result.M12 = rightDir.Y;
      result.M13 = rightDir.Z;
      result.M14 = 0.0f;

      result.M21 = billboardUp.X;
      result.M22 = billboardUp.Y;
      result.M23 = billboardUp.Z;
      result.M24 = 0.0f;

      result.M31 = viewDir.X;
      result.M32 = viewDir.Y;
      result.M33 = viewDir.Z;
      result.M34 = 0.0f;

      result.M41 = objectPosition.X;
      result.M42 = objectPosition.Y;
      result.M43 = objectPosition.Z;
      result.M44 = 1.0f;
    }

    /// <summary>
    /// Creates a cylindrical billboard that rotates around a specified axis.
    /// </summary>
    /// <param name="objectPosition">Position of the object the billboard will rotate around.</param>
    /// <param name="cameraPosition">Position of the camera.</param>
    /// <param name="rotationAxis">Axis to rotate the billboard around.</param>
    /// <param name="cameraForwardVector">Optional forward vector of the camera.</param>
    /// <param name="objectForwardVector">Optional forward vector of the object.</param>
    /// <returns>Constrained billboard matrix</returns>
    /// <remarks>
    /// This method computes the facing direction of the billboard from the object position and the camera position. If the two positions
    /// are too close, the matrix will not be accurate. To accound for this, supply the optional camera forward vector which would be used in this scenario.
    /// </remarks>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix CreateConstrainedBillboardMatrix(in Vector3 objectPosition, in Vector3 cameraPosition, in Vector3 rotationAxis, in Vector3? cameraForwardVector, in Vector3? objectForwardVector)
    {
      Matrix.CreateConstrainedBillboardMatrix(objectPosition, cameraPosition, rotationAxis, cameraForwardVector, objectForwardVector, out Matrix result);

      return result;
    }

    /// <summary>
    /// Creates a cylindrical billboard that rotates around a specified axis.
    /// </summary>
    /// <param name="objectPosition">Position of the object the billboard will rotate around.</param>
    /// <param name="cameraPosition">Position of the camera.</param>
    /// <param name="rotationAxis">Axis to rotate the billboard around.</param>
    /// <param name="cameraForwardVector">Optional forward vector of the camera.</param>
    /// <param name="objectForwardVector">Optional forward vector of the object.</param>
    /// <param name="result">Constrained billboard matrix</param>
    /// <remarks>
    /// This method computes the facing direction of the billboard from the object position and the camera position. If the two positions
    /// are too close, the matrix will not be accurate. To accound for this, supply the optional camera forward vector which would be used in this scenario.
    /// </remarks>
    public static void CreateConstrainedBillboardMatrix(in Vector3 objectPosition, in Vector3 cameraPosition, in Vector3 rotationAxis, in Vector3? cameraForwardVector, in Vector3? objectForwardVector, out Matrix result)
    {
      Vector3 viewDir;
      Vector3.Subtract(objectPosition, cameraPosition, out viewDir);
      float diffLengthSquared = viewDir.LengthSquared();

      if (MathHelper.IsNearlyZero(diffLengthSquared))
      {
        viewDir = cameraForwardVector.HasValue ? -cameraForwardVector.Value : Vector3.Forward;
      }
      else
      {
        Vector3.Multiply(viewDir, 1.0f / MathF.Sqrt(diffLengthSquared), out viewDir);
      }

      float tolerance = 1.0f - MathHelper.ZeroTolerance;

      Vector3 billboardForward;
      Vector3 billboardRight;

      float dot = Vector3.Dot(rotationAxis, viewDir);

      //If viewDir is parallel to rotationAxis, choose a more suitable forward
      if (MathF.Abs(dot) >= tolerance)
      {
        if (objectForwardVector.HasValue)
        {
          billboardForward = objectForwardVector.Value;
          dot = Vector3.Dot(rotationAxis, billboardForward);

          //Fallback if objForward is parallel
          if (MathF.Abs(dot) >= tolerance)
          {
            Vector3 forward = Vector3.Forward;
            dot = Vector3.Dot(rotationAxis, forward);
            billboardForward = (MathF.Abs(dot) > tolerance) ? Vector3.Right : Vector3.Forward;
          }
        }
        else
        {
          Vector3 forward = Vector3.Forward;
          dot = Vector3.Dot(rotationAxis, forward);
          billboardForward = (MathF.Abs(dot) > tolerance) ? Vector3.Right : Vector3.Forward;
        }

        //Compute orthonormal basis with chosen forward
        Vector3.NormalizedCross(rotationAxis, billboardForward, out billboardRight);
        Vector3.NormalizedCross(billboardRight, rotationAxis, out billboardForward);
      }
      else
      {
        //Compute orthonormal basis with viewDir
        Vector3.NormalizedCross(rotationAxis, viewDir, out billboardRight);
        Vector3.NormalizedCross(billboardRight, rotationAxis, out billboardForward);
      }

      result.M11 = billboardRight.X;
      result.M12 = billboardRight.Y;
      result.M13 = billboardRight.Z;
      result.M14 = 0.0f;

      result.M21 = rotationAxis.X;
      result.M22 = rotationAxis.Y;
      result.M23 = rotationAxis.Z;
      result.M24 = 0.0f;

      result.M31 = billboardForward.X;
      result.M32 = billboardForward.Y;
      result.M33 = billboardForward.Z;
      result.M34 = 0.0f;

      result.M41 = objectPosition.X;
      result.M42 = objectPosition.Y;
      result.M43 = objectPosition.Z;
      result.M44 = 1.0f;
    }

    /// <summary>
    /// Creates a SRT (Scale-Rotation-Translation) matrix.
    /// </summary>
    /// <param name="scaling">Uniform scaling along X, Y, Z axes</param>
    /// <param name="rotation">Rotation quaternion</param>
    /// <param name="translation">Translation vector</param>
    /// <returns>SRT (Scale-Rotation-Translation) matrix</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix CreateTransformationMatrix(float scaling, in Quaternion rotation, in Vector3 translation)
    {
      Matrix.CreateTransformationMatrix(scaling, rotation, translation, out Matrix result);

      return result;
    }

    /// <summary>
    /// Creates a SRT (Scale-Rotation-Translation) matrix.
    /// </summary>
    /// <param name="scaling">Uniform scaling along X, Y, Z axes</param>
    /// <param name="rotation">Rotation quaternion</param>
    /// <param name="translation">Translation vector</param>
    /// <param name="result">SRT (Scale-Rotation-Translation) matrix</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void CreateTransformationMatrix(float scaling, in Quaternion rotation, in Vector3 translation, out Matrix result)
    {
      Matrix.FromScale(scaling, out Matrix scaleMatrix);
      Matrix.FromQuaternion(rotation, out Matrix rotMatrix);
      Matrix.FromTranslation(translation, out Matrix transMatrix);

      Matrix.Multiply(scaleMatrix, rotMatrix, out result);
      Matrix.Multiply(result, transMatrix, out result);
    }

    /// <summary>
    /// Creates a SRT (Scale-Rotation-Translation) matrix.
    /// </summary>
    /// <param name="scaleX">Scaling along X axis</param>
    /// <param name="scaleY">Scaling along Y axis</param>
    /// <param name="scaleZ">Scaling along Z axis</param>
    /// <param name="rotation">Rotation quaternion</param>
    /// <param name="translation">Translation vector</param>
    /// <returns>SRT (Scale-Rotation-Translation) matrix</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix CreateTransformationMatrix(float scaleX, float scaleY, float scaleZ, in Quaternion rotation, in Vector3 translation)
    {
      Matrix.CreateTransformationMatrix(scaleX, scaleY, scaleZ, rotation, translation, out Matrix result);

      return result;
    }

    /// <summary>
    /// Creates a SRT (Scale-Rotation-Translation) matrix.
    /// </summary>
    /// <param name="scaleX">Scaling along X axis</param>
    /// <param name="scaleY">Scaling along Y axis</param>
    /// <param name="scaleZ">Scaling along Z axis</param>
    /// <param name="rotation">Rotation quaternion</param>
    /// <param name="translation">Translation vector</param>
    /// <param name="result">SRT (Scale-Rotation-Translation) matrix</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void CreateTransformationMatrix(float scaleX, float scaleY, float scaleZ, in Quaternion rotation, in Vector3 translation, out Matrix result)
    {
      Matrix.FromScale(scaleX, scaleY, scaleZ, out Matrix scaleMatrix);
      Matrix.FromQuaternion(rotation, out Matrix rotMatrix);
      Matrix.FromTranslation(translation, out Matrix transMatrix);

      Matrix.Multiply(scaleMatrix, rotMatrix, out result);
      Matrix.Multiply(result, transMatrix, out result);
    }

    /// <summary>
    /// Creates a SRT (Scale-Rotation-Translation) matrix, where the rotation is about a point.
    /// </summary>
    /// <param name="scaling">Uniform scaling along X, Y, Z axes</param>
    /// <param name="rotationCenter">Center of the rotation</param>
    /// <param name="rotation">Rotation quaternion</param>
    /// <param name="translation">Translation vector</param>
    /// <returns>SRT (Scale-Rotation-Translation) matrix</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix CreateTransformationMatrix(float scaling, in Vector3 rotationCenter, in Quaternion rotation, in Vector3 translation)
    {
      Matrix.CreateTransformationMatrix(scaling, rotationCenter, rotation, translation, out Matrix result);

      return result;
    }

    /// <summary>
    /// Creates a SRT (Scale-Rotation-Translation) matrix, where the rotation is about a point.
    /// </summary>
    /// <param name="scaling">Uniform scaling along X, Y, Z axes</param>
    /// <param name="rotationCenter">Center of the rotation</param>
    /// <param name="rotation">Rotation quaternion</param>
    /// <param name="translation">Translation vector</param>
    /// <param name="result">SRT (Scale-Rotation-Translation) matrix</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void CreateTransformationMatrix(float scaling, in Vector3 rotationCenter, in Quaternion rotation, in Vector3 translation, out Matrix result)
    {
      Matrix.FromScale(scaling, out Matrix scaleMatrix);
      Matrix.FromQuaternion(rotation, out Matrix rotMatrix);

      Vector3.Negate(rotationCenter, out Vector3 negRotCenter);
      Matrix.FromTranslation(negRotCenter, out Matrix negTransRotCenterMatrix);

      Matrix.FromTranslation(rotationCenter, out Matrix transRotCenterMatrix);

      Matrix.FromTranslation(translation, out Matrix transMatrix);

      Matrix.Multiply(scaleMatrix, negTransRotCenterMatrix, out result);
      Matrix.Multiply(result, rotMatrix, out result);
      Matrix.Multiply(result, transRotCenterMatrix, out result);
      Matrix.Multiply(result, transMatrix, out result);
    }

    /// <summary>
    /// Creates a SRT (Scale-Rotation-Translation) matrix, where the rotation is about a point.
    /// </summary>
    /// <param name="scaleX">Scaling along X axis</param>
    /// <param name="scaleY">Scaling along Y axis</param>
    /// <param name="scaleZ">Scaling along Z axis</param>
    /// <param name="rotationCenter">Center of the rotation</param>
    /// <param name="rotation">Rotation quaternion</param>
    /// <param name="translation">Translation vector</param>
    /// <returns>SRT (Scale-Rotation-Translation) matrix</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix CreateTransformationMatrix(float scaleX, float scaleY, float scaleZ, in Vector3 rotationCenter, in Quaternion rotation, in Vector3 translation)
    {
      Matrix.CreateTransformationMatrix(scaleX, scaleY, scaleZ, rotationCenter, rotation, translation, out Matrix result);

      return result;
    }

    /// <summary>
    /// Creates a SRT (Scale-Rotation-Translation) matrix, where the rotation is about a point.
    /// </summary>
    /// <param name="scaleX">Scaling along X axis</param>
    /// <param name="scaleY">Scaling along Y axis</param>
    /// <param name="scaleZ">Scaling along Z axis</param>
    /// <param name="rotationCenter">Center of the rotation</param>
    /// <param name="rotation">Rotation quaternion</param>
    /// <param name="translation">Translation vector</param>
    /// <param name="result">SRT (Scale-Rotation-Translation) matrix</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void CreateTransformationMatrix(float scaleX, float scaleY, float scaleZ, in Vector3 rotationCenter, in Quaternion rotation, in Vector3 translation, out Matrix result)
    {
      Matrix.FromScale(scaleX, scaleY, scaleZ, out Matrix scaleMatrix);
      Matrix.FromQuaternion(rotation, out Matrix rotMatrix);

      Vector3.Negate(rotationCenter, out Vector3 negRotCenter);
      Matrix.FromTranslation(negRotCenter, out Matrix negTransRotCenterMatrix);

      Matrix.FromTranslation(rotationCenter, out Matrix transRotCenterMatrix);

      Matrix.FromTranslation(translation, out Matrix transMatrix);

      Matrix.Multiply(scaleMatrix, negTransRotCenterMatrix, out result);
      Matrix.Multiply(result, rotMatrix, out result);
      Matrix.Multiply(result, transRotCenterMatrix, out result);
      Matrix.Multiply(result, transMatrix, out result);
    }

    /// <summary>
    /// Creates a RT (Rotation-Translation) matrix.
    /// </summary>
    /// <param name="rotation">Rotation quaternion</param>
    /// <param name="translation">Translation vector</param>
    /// <returns>RT (Rotation-Translation) matrix</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix CreateTransformationMatrix(in Quaternion rotation, in Vector3 translation)
    {
      Matrix.CreateTransformationMatrix(rotation, translation, out Matrix result);

      return result;
    }

    /// <summary>
    /// Creates a RT (Rotation-Translation) matrix.
    /// </summary>
    /// <param name="rotation">Rotation quaternion</param>
    /// <param name="translation">Translation vector</param>
    /// <param name="result">RT (Rotation-Translation) matrix</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void CreateTransformationMatrix(in Quaternion rotation, in Vector3 translation, out Matrix result)
    {
      Matrix.FromQuaternion(rotation, out Matrix rotMatrix);
      Matrix.FromTranslation(translation, out Matrix transMatrix);

      Matrix.Multiply(rotMatrix, transMatrix, out result);
    }

    /// <summary>
    /// Creates a RT (Rotation-Translation) matrix, where the rotation is about a point.
    /// </summary>
    /// <param name="rotationCenter">Center of the rotation</param>
    /// <param name="rotation">Rotation quaternion</param>
    /// <param name="translation">Translation vector</param>
    /// <returns>RT (Rotation-Translation) matrix</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix CreateTransformationMatrix(in Vector3 rotationCenter, in Quaternion rotation, in Vector3 translation)
    {
      Matrix.CreateTransformationMatrix(rotationCenter, rotation, translation, out Matrix result);

      return result;
    }

    /// <summary>
    /// Creates a RT (Rotation-Translation) matrix, where the rotation is about a point.
    /// </summary>
    /// <param name="rotationCenter">Center of the rotation</param>
    /// <param name="rotation">Rotation quaternion</param>
    /// <param name="translation">Translation vector</param>
    /// <param name="result">RT (Rotation-Translation) matrix</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void CreateTransformationMatrix(in Vector3 rotationCenter, in Quaternion rotation, in Vector3 translation, out Matrix result)
    {
      Matrix.FromQuaternion(rotation, out Matrix rotMatrix);

      Vector3.Negate(rotationCenter, out Vector3 negRotCenter);
      Matrix.FromTranslation(negRotCenter, out Matrix negTransRotCenterMatrix);

      Matrix.FromTranslation(rotationCenter, out Matrix transRotCenterMatrix);

      Matrix.FromTranslation(translation, out Matrix transMatrix);

      Matrix.Multiply(rotMatrix, negTransRotCenterMatrix, out result);
      Matrix.Multiply(result, transRotCenterMatrix, out result);
      Matrix.Multiply(result, transMatrix, out result);
    }

    /// <summary>
    /// Creates a rotation matrix where the object is facing the target along its z axis.
    /// If your object's "forward" facing is down -Z axis, then the object will correctly face the target.
    /// </summary>
    /// <param name="position">Position of object</param>
    /// <param name="target">Position of target</param>
    /// <param name="worldUp">World up vector</param>
    /// <returns>Rotation matrix</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix LookAt(in Vector3 position, in Vector3 target, in Vector3 worldUp)
    {
      Matrix.LookAt(position, target, worldUp, out Matrix result);

      return result;
    }

    /// <summary>
    /// Creates a rotation matrix where the object is facing the target along its z axis.
    /// If your object's "forward" facing is down -Z axis, then the object will correctly face the target.
    /// </summary>
    /// <param name="position">Position of object</param>
    /// <param name="target">Position of target</param>
    /// <param name="worldUp">World up vector</param>
    /// <param name="result">Rotation matrix</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void LookAt(in Vector3 position, in Vector3 target, in Vector3 worldUp, out Matrix result)
    {
      Vector3 zAxis;
      Vector3 xAxis;
      Vector3 yAxis;

      Vector3.NormalizedSubtract(position, target, out zAxis);
      Vector3.NormalizedCross(worldUp, zAxis, out xAxis);
      Vector3.NormalizedCross(zAxis, xAxis, out yAxis);

      Matrix.FromAxes(xAxis, yAxis, zAxis, out result);
    }

    /// <summary>
    /// Creates a rotation matrix from 3 orthogonal axes.
    /// </summary>
    /// <param name="xAxis">X axis (first row, right)</param>
    /// <param name="yAxis">Y axis (second row, up)</param>
    /// <param name="zAxis">Z axis (third row, backward)</param>
    /// <returns>Rotation matrix</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix FromAxes(in Vector3 xAxis, in Vector3 yAxis, in Vector3 zAxis)
    {
      Matrix.FromAxes(xAxis, yAxis, zAxis, out Matrix result);

      return result;
    }

    /// <summary>
    /// Creates a rotation matrix from 3 orthogonal axes.
    /// </summary>
    /// <param name="xAxis">X axis (first row, right)</param>
    /// <param name="yAxis">Y axis (second row, up)</param>
    /// <param name="zAxis">Z axis (third row, backward)</param>
    /// <param name="result">Rotation matrix</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void FromAxes(in Vector3 xAxis, in Vector3 yAxis, in Vector3 zAxis, out Matrix result)
    {
      //Set x axis to xAxis (first row)
      result.M11 = xAxis.X;
      result.M12 = xAxis.Y;
      result.M13 = xAxis.Z;
      result.M14 = 0.0f;

      //Set y axis to yAxis (second row)
      result.M21 = yAxis.X;
      result.M22 = yAxis.Y;
      result.M23 = yAxis.Z;
      result.M24 = 0.0f;

      //Set z axis to zAxis (third row)
      result.M31 = zAxis.X;
      result.M32 = zAxis.Y;
      result.M33 = zAxis.Z;
      result.M34 = 0.0f;

      result.M41 = 0.0f;
      result.M42 = 0.0f;
      result.M43 = 0.0f;
      result.M44 = 1.0f;
    }

    /// <summary>
    /// Transforms the specified matrix by a quaternion rotation.
    /// </summary>
    /// <param name="value">Source matrix</param>
    /// <param name="rot">Quaternion rotation</param>
    /// <returns>Transformed matrix</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix Transform(in Matrix value, in Quaternion rot)
    {
      Matrix.Transform(value, rot, out Matrix result);

      return result;
    }

    /// <summary>
    /// Transforms the specified matrix by a quaternion rotation.
    /// </summary>
    /// <param name="value">Source matrix</param>
    /// <param name="rot">Quaternion rotation</param>
    /// <param name="result">Transformed</param>
    public static void Transform(in Matrix value, in Quaternion rot, out Matrix result)
    {
      float x2 = rot.X + rot.X;
      float y2 = rot.Y + rot.Y;
      float z2 = rot.Z + rot.Z;

      float wx2 = rot.W * x2;
      float wy2 = rot.W * y2;
      float wz2 = rot.W * z2;

      float xx2 = rot.X * x2;
      float xy2 = rot.X * y2;
      float xz2 = rot.X * z2;

      float yy2 = rot.Y * y2;
      float yz2 = rot.Y * z2;

      float zz2 = rot.Z * z2;

      float h1 = (1f - yy2) - zz2;
      float h2 = xy2 - wz2;
      float h3 = xz2 + wy2;
      float h4 = xy2 + wz2;
      float h5 = (1f - xx2) - zz2;
      float h6 = yz2 - wx2;
      float h7 = xz2 - wy2;
      float h8 = yz2 + wx2;
      float h9 = (1f - xx2) - yy2;

      result.M11 = ((value.M11 * h1) + (value.M12 * h2)) + (value.M13 * h3);
      result.M12 = ((value.M11 * h4) + (value.M12 * h5)) + (value.M13 * h6);
      result.M13 = ((value.M11 * h7) + (value.M12 * h8)) + (value.M13 * h9);
      result.M14 = value.M14;

      result.M21 = ((value.M21 * h1) + (value.M22 * h2)) + (value.M23 * h3);
      result.M22 = ((value.M21 * h4) + (value.M22 * h5)) + (value.M23 * h6);
      result.M23 = ((value.M21 * h7) + (value.M22 * h8)) + (value.M23 * h9);
      result.M24 = value.M24;

      result.M31 = ((value.M31 * h1) + (value.M32 * h2)) + (value.M33 * h3);
      result.M32 = ((value.M31 * h4) + (value.M32 * h5)) + (value.M33 * h6);
      result.M33 = ((value.M31 * h7) + (value.M32 * h8)) + (value.M33 * h9);
      result.M34 = value.M34;

      result.M41 = ((value.M41 * h1) + (value.M42 * h2)) + (value.M43 * h3);
      result.M42 = ((value.M41 * h4) + (value.M42 * h5)) + (value.M43 * h6);
      result.M43 = ((value.M41 * h7) + (value.M42 * h8)) + (value.M43 * h9);
      result.M44 = value.M44;
    }

    /// <summary>
    /// Swaps the rows and columns of the specified matrix.
    /// </summary>
    /// <param name="value">Source matrix</param>
    /// <returns>Transposed matrix</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix Transpose(in Matrix value)
    {
      Matrix.Transpose(value, out Matrix result);

      return result;
    }

    /// <summary>
    /// Swaps the rows and columns of the specified matrix.
    /// </summary>
    /// <param name="value">Source matrix</param>
    /// <param name="result">Transposed matrix</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Transpose(in Matrix value, out Matrix result)
    {
      float m11 = value.M11;
      float m12 = value.M12;
      float m13 = value.M13;
      float m14 = value.M14;
      float m21 = value.M21;
      float m22 = value.M22;
      float m23 = value.M23;
      float m24 = value.M24;
      float m31 = value.M31;
      float m32 = value.M32;
      float m33 = value.M33;
      float m34 = value.M34;
      float m41 = value.M41;
      float m42 = value.M42;
      float m43 = value.M43;
      float m44 = value.M44;

      result.M11 = m11;
      result.M12 = m21;
      result.M13 = m31;
      result.M14 = m41;

      result.M21 = m12;
      result.M22 = m22;
      result.M23 = m32;
      result.M24 = m42;

      result.M31 = m13;
      result.M32 = m23;
      result.M33 = m33;
      result.M34 = m43;

      result.M41 = m14;
      result.M42 = m24;
      result.M43 = m34;
      result.M44 = m44;
    }

    /// <summary>
    /// Flips the signs of each matrix element.
    /// </summary>
    /// <param name="value">Source matrix</param>
    /// <returns>Negated matrix</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix Negate(in Matrix value)
    {
      Matrix.Negate(value, out Matrix result);

      return result;
    }

    /// <summary>
    /// Flips the signs of each matrix element.
    /// </summary>
    /// <param name="value">Source matrix</param>
    /// <param name="result">Negated matrix</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Negate(in Matrix value, out Matrix result)
    {
      result.M11 = -value.M11;
      result.M12 = -value.M12;
      result.M13 = -value.M13;
      result.M14 = -value.M14;

      result.M21 = -value.M21;
      result.M22 = -value.M22;
      result.M23 = -value.M23;
      result.M24 = -value.M24;

      result.M31 = -value.M31;
      result.M32 = -value.M32;
      result.M33 = -value.M33;
      result.M34 = -value.M34;

      result.M41 = -value.M41;
      result.M42 = -value.M42;
      result.M43 = -value.M43;
      result.M44 = -value.M44;
    }

    /// <summary>
    /// Creates a matrix that represents a rotation around the x-axis.
    /// </summary>
    /// <param name="angle">Angle to rotate</param>
    /// <returns>Rotation matrix</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix FromRotationX(Angle angle)
    {
      Matrix.FromRotationX(angle, out Matrix result);

      return result;
    }

    /// <summary>
    /// Creates a matrix that represents a rotation around the x-axis.
    /// </summary>
    /// <param name="angle">Angle to rotate</param>
    /// <param name="result">Rotation matrix</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void FromRotationX(Angle angle, out Matrix result)
    {
      float cos = angle.Cos;
      float sin = angle.Sin;

      result.M11 = 1.0f;
      result.M12 = 0.0f;
      result.M13 = 0.0f;
      result.M14 = 0.0f;

      result.M21 = 0.0f;
      result.M22 = cos;
      result.M23 = sin;
      result.M24 = 0.0f;

      result.M31 = 0.0f;
      result.M32 = -sin;
      result.M33 = cos;
      result.M34 = 0.0f;

      result.M41 = 0.0f;
      result.M42 = 0.0f;
      result.M43 = 0.0f;
      result.M44 = 1.0f;
    }

    /// <summary>
    /// Creates a matrix that represents a rotation around the y-axis.
    /// </summary>
    /// <param name="angle">Angle to rotate</param>
    /// <returns>Rotation matrix</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix FromRotationY(Angle angle)
    {
      Matrix.FromRotationY(angle, out Matrix result);

      return result;
    }

    /// <summary>
    /// Creates a matrix that represents a rotation around the y-axis.
    /// </summary>
    /// <param name="angle">Angle to rotates</param>
    /// <param name="result">Rotation matrix</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void FromRotationY(Angle angle, out Matrix result)
    {
      float cos = angle.Cos;
      float sin = angle.Sin;

      result.M11 = cos;
      result.M12 = 0.0f;
      result.M13 = -sin;
      result.M14 = 0.0f;

      result.M21 = 0.0f;
      result.M22 = 1.0f;
      result.M23 = 0.0f;
      result.M24 = 0.0f;

      result.M31 = sin;
      result.M32 = 0.0f;
      result.M33 = cos;
      result.M34 = 0.0f;

      result.M41 = 0.0f;
      result.M42 = 0.0f;
      result.M43 = 0.0f;
      result.M44 = 1.0f;
    }

    /// <summary>
    /// Creates a matrix that represents a rotation around the z-axis.
    /// </summary>
    /// <param name="angle">Angle to rotate</param>
    /// <returns>Rotation matrix</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix FromRotationZ(Angle angle)
    {
      Matrix.FromRotationZ(angle, out Matrix result);

      return result;
    }

    /// <summary>
    /// Creates a matrix that represents a rotation around the z-axis.
    /// </summary>
    /// <param name="angle">Angle to rotate</param>
    /// <param name="result">Rotation matrix</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void FromRotationZ(Angle angle, out Matrix result)
    {
      float cos = angle.Cos;
      float sin = angle.Sin;

      result.M11 = cos;
      result.M12 = sin;
      result.M13 = 0.0f;
      result.M14 = 0.0f;

      result.M21 = -sin;
      result.M22 = cos;
      result.M23 = 0.0f;
      result.M24 = 0.0f;

      result.M31 = 0.0f;
      result.M32 = 0.0f;
      result.M33 = 1.0f;
      result.M34 = 0.0f;

      result.M41 = 0.0f;
      result.M42 = 0.0f;
      result.M43 = 0.0f;
      result.M44 = 1.0f;
    }

    /// <summary>
    /// Creates a scaling matrix from the specified scale vector.
    /// </summary>
    /// <param name="scale">Scaling vector where each component corresponds to each axis to scale on</param>
    /// <returns>Scaling matrix</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix FromScale(in Vector3 scale)
    {
      Matrix.FromScale(scale, out Matrix result);

      return result;
    }

    /// <summary>
    /// Creates a scaling matrix from the specified scale vector.
    /// </summary>
    /// <param name="scale">Scaling vector where each component corresponds to each axis to scale on</param>
    /// <param name="result">Scaling matrix</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void FromScale(in Vector3 scale, out Matrix result)
    {
      result.M11 = scale.X;
      result.M12 = 0.0f;
      result.M13 = 0.0f;
      result.M14 = 0.0f;

      result.M21 = 0.0f;
      result.M22 = scale.Y;
      result.M23 = 0.0f;
      result.M24 = 0.0f;

      result.M31 = 0.0f;
      result.M32 = 0.0f;
      result.M33 = scale.Z;
      result.M34 = 0.0f;

      result.M41 = 0.0f;
      result.M42 = 0.0f;
      result.M43 = 0.0f;
      result.M44 = 1.0f;
    }

    /// <summary>
    /// Creates a scaling matrix from the specified scaling value for uniform scale.
    /// </summary>
    /// <param name="scale">Scaling value for x,y,z axes</param>
    /// <returns>Scaling matrix</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix FromScale(float scale)
    {
      Matrix.FromScale(scale, out Matrix result);

      return result;
    }

    /// <summary>
    /// Creates a scaling matrix from the specified scaling value for uniform scale.
    /// </summary>
    /// <param name="scale">Scaling value for x,y,z axes</param>
    /// <param name="result">Scaling matrix</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void FromScale(float scale, out Matrix result)
    {
      result.M11 = scale;
      result.M12 = 0.0f;
      result.M13 = 0.0f;
      result.M14 = 0.0f;

      result.M21 = 0.0f;
      result.M22 = scale;
      result.M23 = 0.0f;
      result.M24 = 0.0f;

      result.M31 = 0.0f;
      result.M32 = 0.0f;
      result.M33 = scale;
      result.M34 = 0.0f;

      result.M41 = 0.0f;
      result.M42 = 0.0f;
      result.M43 = 0.0f;
      result.M44 = 1.0f;
    }

    /// <summary>
    /// Creates a scale matrix from the specified scale values.
    /// </summary>
    /// <param name="x">Amount to scale along x axis</param>
    /// <param name="y">Amount to scale along y axis</param>
    /// <param name="z">Amount to scale along z axis</param>
    /// <returns>Scaling matrix</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix FromScale(float x, float y, float z)
    {
      Matrix.FromScale(x, y, z, out Matrix result);

      return result;
    }

    /// <summary>
    /// Creates a scale matrix from the specified scale values.
    /// </summary>
    /// <param name="x">Amount to scale along x axis</param>
    /// <param name="y">Amount to scale along y axis</param>
    /// <param name="z">Amount to scale along z axis</param>
    /// <param name="result">Scaling matrix</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void FromScale(float x, float y, float z, out Matrix result)
    {
      result.M11 = x;
      result.M12 = 0.0f;
      result.M13 = 0.0f;
      result.M14 = 0.0f;

      result.M21 = 0.0f;
      result.M22 = y;
      result.M23 = 0.0f;
      result.M24 = 0.0f;

      result.M31 = 0.0f;
      result.M32 = 0.0f;
      result.M33 = z;
      result.M34 = 0.0f;

      result.M41 = 0.0f;
      result.M42 = 0.0f;
      result.M43 = 0.0f;
      result.M44 = 1.0f;
    }

    /// <summary>
    /// Creates a translation matrix from the specified translation vector.
    /// </summary>
    /// <param name="translation">Translation vector</param>
    /// <returns>Translation matrix</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix FromTranslation(in Vector3 translation)
    {
      Matrix.FromTranslation(translation, out Matrix result);

      return result;
    }

    /// <summary>
    /// Creates a translation matrix from the specified translation vector.
    /// </summary>
    /// <param name="translation">Translation vector</param>
    /// <param name="result">Translation matrix</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void FromTranslation(in Vector3 translation, out Matrix result)
    {
      result.M11 = 1.0f;
      result.M12 = 0.0f;
      result.M13 = 0.0f;
      result.M14 = 0.0f;

      result.M21 = 0.0f;
      result.M22 = 1.0f;
      result.M23 = 0.0f;
      result.M24 = 0.0f;

      result.M31 = 0.0f;
      result.M32 = 0.0f;
      result.M33 = 1.0f;
      result.M34 = 0.0f;

      result.M41 = translation.X;
      result.M42 = translation.Y;
      result.M43 = translation.Z;
      result.M44 = 1.0f;
    }

    /// <summary>
    /// Creates a translation matrix from the specified translation values.
    /// </summary>
    /// <param name="x">Amount to translate along x axis</param>
    /// <param name="y">Amount to translate along y axis</param>
    /// <param name="z">Amount to translate along z axis</param>
    /// <returns>Translation matrix</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix FromTranslation(float x, float y, float z)
    {
      Matrix.FromTranslation(x, y, z, out Matrix result);

      return result;
    }

    /// <summary>
    /// Creates a translation matrix from the the specified translation values.
    /// </summary>
    /// <param name="x">Amount to translate along x axis</param>
    /// <param name="y">Amount to translate along y axis</param>
    /// <param name="z">Amount to translate along z axis</param>
    /// <param name="result">Translation matrix</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void FromTranslation(float x, float y, float z, out Matrix result)
    {
      result.M11 = 1.0f;
      result.M12 = 0.0f;
      result.M13 = 0.0f;
      result.M14 = 0.0f;

      result.M21 = 0.0f;
      result.M22 = 1.0f;
      result.M23 = 0.0f;
      result.M24 = 0.0f;

      result.M31 = 0.0f;
      result.M32 = 0.0f;
      result.M33 = 1.0f;
      result.M34 = 0.0f;

      result.M41 = x;
      result.M42 = y;
      result.M43 = z;
      result.M44 = 1.0f;
    }

    /// <summary>
    /// Creates a transform matrix where the translation component maps back to the given point.
    /// </summary>
    /// <param name="fixedPoint">Fixed point in space.</param>
    /// <param name="rotation">Rotation matrix, translation components will be ignored.</param>
    /// <returns>Resulting transform matrix.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix FromFixedPoint(in Vector3 fixedPoint, in Matrix rotation)
    {
      Matrix.FromFixedPoint(fixedPoint, rotation, out Matrix transform);

      return transform;
    }

    /// <summary>
    /// Creates a transform matrix where the translation component maps back to the given point.
    /// </summary>
    /// <param name="fixedPoint">Fixed point in space.</param>
    /// <param name="rotation">Rotation matrix, translation components will be ignored.</param>
    /// <param name="transform">Resulting transform matrix.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void FromFixedPoint(in Vector3 fixedPoint, in Matrix rotation, out Matrix transform)
    {
      // Compute fixedPoint - rotation * fixedPointDirectionToZero 
      float dirX = fixedPoint.X;
      float dirY = fixedPoint.Y;
      float dirZ = fixedPoint.Z;

      transform = rotation;

      transform.M41 = fixedPoint.X - (rotation.M11 * dirX + rotation.M21 * dirY + rotation.M31 * dirZ);
      transform.M42 = fixedPoint.Y - (rotation.M12 * dirX + rotation.M22 * dirY + rotation.M32 * dirZ);
      transform.M43 = fixedPoint.Z - (rotation.M13 * dirX + rotation.M23 * dirY + rotation.M33 * dirZ);
    }

    /// <summary>
    /// Creates a rotation matrix from the specified quaternion.
    /// </summary>
    /// <param name="rot">Rotation quaternion</param>
    /// <returns>Rotation matrix</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix FromQuaternion(in Quaternion rot)
    {
      Matrix.FromQuaternion(rot, out Matrix result);

      return result;
    }

    /// <summary>
    /// Creates a rotation matrix from the specified quaternion.
    /// </summary>
    /// <param name="rot">Rotation quaternion</param>
    /// <param name="result">Rotation matrix</param>
    public static void FromQuaternion(in Quaternion rot, out Matrix result)
    {
      float x = rot.X;
      float y = rot.Y;
      float z = rot.Z;
      float w = rot.W;

      float xx = x * x;
      float xy = x * y;
      float xw = x * w;

      float yy = y * y;
      float yz = y * z;
      float yw = y * w;

      float zx = z * x;
      float zz = z * z;
      float zw = z * w;

      result.M11 = 1.0f - (2.0f * (yy + zz));
      result.M12 = 2.0f * (xy + zw);
      result.M13 = 2.0f * (zx - yw);
      result.M14 = 0.0f;

      result.M21 = 2.0f * (xy - zw);
      result.M22 = 1.0f - (2.0f * (xx + zz));
      result.M23 = 2.0f * (yz + xw);
      result.M24 = 0.0f;

      result.M31 = 2.0f * (zx + yw);
      result.M32 = 2.0f * (yz - xw);
      result.M33 = 1.0f - (2.0f * (xx + yy));
      result.M34 = 0.0f;

      result.M41 = 0.0f;
      result.M42 = 0.0f;
      result.M43 = 0.0f;
      result.M44 = 1.0f;
    }

    /// <summary>
    /// Creates a rotation matrix from a specified angle in radians and
    /// axis to rotate about.
    /// </summary>
    /// <param name="angle">Angle to rotate</param>
    /// <param name="axis">Axis to rotate about</param>
    /// <returns>Rotation matrix</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix FromAngleAxis(Angle angle, in Vector3 axis)
    {
      Matrix.FromAngleAxis(angle, axis, out Matrix result);

      return result;
    }

    /// <summary>
    /// Creates a rotation matrix from a specified angle in radians and
    /// axis to rotate about.
    /// </summary>
    /// <param name="angle">Angle to rotate</param>
    /// <param name="axis">Axis to rotate about</param>
    /// <param name="result">Rotation matrix</param>
    public static void FromAngleAxis(Angle angle, in Vector3 axis, out Matrix result)
    {
      float x = axis.X;
      float y = axis.Y;
      float z = axis.Z;

      float sin = angle.Sin;
      float cos = angle.Cos;

      float xx = x * x;
      float yy = y * y;
      float zz = z * z;
      float xy = x * y;
      float xz = x * z;
      float yz = y * z;

      result.M11 = xx + (cos * (1.0f - xx));
      result.M12 = (xy - (cos * xy)) + (sin * z);
      result.M13 = (xz - (cos * xz)) - (sin * y);
      result.M14 = 0.0f;

      result.M21 = (xy - (cos * xy)) - (sin * z);
      result.M22 = yy + (cos * (1.0f - yy));
      result.M23 = (yz - (cos * yz)) + (sin * x);
      result.M24 = 0.0f;

      result.M31 = (xz - (cos * xz)) + (sin * y);
      result.M32 = (yz - (cos * yz)) - (sin * x);
      result.M33 = zz + (cos * (1.0f - zz));

      result.M34 = 0.0f;
      result.M41 = 0.0f;
      result.M42 = 0.0f;
      result.M43 = 0.0f;
      result.M44 = 1.0f;
    }

    /// <summary>
    /// Creates a rotation matrix from the corresponding yaw, pitch, and roll
    /// angles. The order of the angles are applied in that order.
    /// </summary>
    /// <param name="yaw">Angle to rotate about the y-axis</param>
    /// <param name="pitch">Angle to rotate about the x-axis</param>
    /// <param name="roll">Angle to rotate about the z-axis</param>
    /// <returns>Rotation matrix</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix FromEulerAngles(Angle yaw, Angle pitch, Angle roll)
    {
      Matrix.FromEulerAngles(yaw, pitch, roll, out Matrix result);

      return result;
    }

    /// <summary>
    /// Creates a rotation matrix from the corresponding yaw, pitch, and roll
    /// angles. The order of the angles are applied in that order.
    /// </summary>
    /// <param name="yaw">Angle to rotate about the y-axis</param>
    /// <param name="pitch">Angle to rotate about the x-axis</param>
    /// <param name="roll">Angle to rotate about the z-axis</param>
    /// <param name="result">Rotation matrix</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void FromEulerAngles(Angle yaw, Angle pitch, Angle roll, out Matrix result)
    {
      Quaternion.FromEulerAngles(yaw, pitch, roll, out Quaternion quaternion);
      Matrix.FromQuaternion(quaternion, out result);
    }

    /// <summary>
    /// Orthogonalizes the matrix.
    /// </summary>
    /// <param name="value">Matrix to orthogonalize</param>
    /// <returns>Orthogonalized matrix</returns>
    /// <remarks>
    /// Orthogonalization is the process of making the axes (rows) of the matrix orthogonal to each other. Thus any
    /// row in the matrix will be orthogonal to any other row in the Matrix. The method uses the modified Gram-Schmidt process,
    /// which results in the matrix to be numerically unstable where the numerical stability decreases in the order that the rows
    /// are pr ocessed. The first row is the most stable, the fourth row the least.
    /// </remarks>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix Orthogonalize(in Matrix value)
    {
      Matrix.Orthogonalize(value, out Matrix result);

      return result;
    }

    /// <summary>
    /// Orthogonalizes the matrix.
    /// </summary>
    /// <param name="value">Matrix to orthogonalize</param>
    /// <param name="result">Orthogonalized matrix</param>
    /// <remarks>
    /// Orthogonalization is the process of making the axes (rows) of the matrix orthogonal to each other. Thus any
    /// row in the matrix will be orthogonal to any other row in the Matrix. The method uses the modified Gram-Schmidt process,
    /// which results in the matrix to be numerically unstable where the numerical stability decreases in the order that the rows
    /// are pr ocessed. The first row is the most stable, the fourth row the least.
    /// </remarks>
    public static void Orthogonalize(in Matrix value, out Matrix result)
    {
      result = value;
      result.Row2 -= (Vector4.Dot(result.Row1, result.Row2) / Vector4.Dot(result.Row1, result.Row1)) * result.Row1;

      result.Row3 -= (Vector4.Dot(result.Row1, result.Row3) / Vector4.Dot(result.Row1, result.Row1)) * result.Row1;
      result.Row3 -= (Vector4.Dot(result.Row2, result.Row3) / Vector4.Dot(result.Row2, result.Row2)) * result.Row2;

      result.Row4 -= (Vector4.Dot(result.Row1, result.Row4) / Vector4.Dot(result.Row1, result.Row1)) * result.Row1;
      result.Row4 -= (Vector4.Dot(result.Row2, result.Row4) / Vector4.Dot(result.Row2, result.Row2)) * result.Row2;
      result.Row4 -= (Vector4.Dot(result.Row3, result.Row4) / Vector4.Dot(result.Row3, result.Row3)) * result.Row3;
    }

    /// <summary>
    /// Tests equality between two matrices.
    /// </summary>
    /// <param name="a">First matrix</param>
    /// <param name="b">Second matrix</param>
    /// <returns>True if components are equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator ==(in Matrix a, in Matrix b)
    {
      return a.Equals(b);
    }

    /// <summary>
    /// Tests inequality between two matrices.
    /// </summary>
    /// <param name="a">First matrix</param>
    /// <param name="b">Second matrix</param>
    /// <returns>True if components are not equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator !=(in Matrix a, in Matrix b)
    {
      return !a.Equals(b);
    }

    /// <summary>
    /// Adds two matrices.
    /// </summary>
    /// <param name="a">First matrix</param>
    /// <param name="b">Second matrix</param>
    /// <returns>Sum of the two matrices</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix operator +(in Matrix a, in Matrix b)
    {
      Matrix.Add(a, b, out Matrix result);

      return result;
    }

    /// <summary>
    /// Flips the signs of each matrix element.
    /// </summary>
    /// <param name="value">Source matrix</param>
    /// <returns>Negated matrix</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix operator -(in Matrix value)
    {
      Matrix.Negate(value, out Matrix result);

      return result;
    }

    /// <summary>
    /// Subtracts matrix b from matrix a.
    /// </summary>
    /// <param name="a">First matrix</param>
    /// <param name="b">Second matrix</param>
    /// <returns>Difference of the two matrices</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix operator -(in Matrix a, in Matrix b)
    {
      Matrix.Subtract(a, b, out Matrix result);

      return result;
    }

    /// <summary>
    /// Multiplies two matrices.
    /// </summary>
    /// <param name="a">First matrix</param>
    /// <param name="b">Second matrix</param>
    /// <returns>Product of the two matrices</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix operator *(in Matrix a, in Matrix b)
    {
      Matrix.Multiply(a, b, out Matrix result);

      return result;
    }

    /// <summary>
    /// Multiplies a matrix by a scalar value.
    /// </summary>
    /// <param name="value">Source matrix</param>
    /// <param name="scale">Amount to scale</param>
    /// <returns>Multiplied matrix</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix operator *(in Matrix value, float scale)
    {
      Matrix.Multiply(value, scale, out Matrix result);

      return result;
    }

    /// <summary>
    /// Multiplies a matrix by a scalar value.
    /// </summary>
    /// <param name="scale">Amount to scale</param>
    /// <param name="value">Source matrix</param>
    /// <returns>Multiplied matrix</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix operator *(float scale, in Matrix value)
    {
      Matrix.Multiply(value, scale, out Matrix result);

      return result;
    }

    /// <summary>
    /// Divides the a matrice's components by the components of another.
    /// </summary>
    /// <param name="a">Source matrix</param>
    /// <param name="b">Divisor matrix</param>
    /// <returns>Quotient of the two matrices</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix operator /(in Matrix a, in Matrix b)
    {
      Matrix.Divide(a, b, out Matrix result);

      return result;
    }

    /// <summary>
    /// Divides the a matrice's components by a scalar value.
    /// </summary>
    /// <param name="value">Source matrix</param>
    /// <param name="divisor">Divisor scalar</param>
    /// <returns>Divided matrix</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Matrix operator /(in Matrix value, float divisor)
    {
      Matrix.Divide(value, divisor, out Matrix result);

      return result;
    }

    /// <summary>
    /// Computes the determinant of the matrix.
    /// </summary>
    /// <returns>Determinant</returns>
    public readonly float Determinant()
    {
      float m11 = M11;
      float m12 = M12;
      float m13 = M13;
      float m14 = M14;
      float m21 = M21;
      float m22 = M22;
      float m23 = M23;
      float m24 = M24;
      float m31 = M31;
      float m32 = M32;
      float m33 = M33;
      float m34 = M34;
      float m41 = M41;
      float m42 = M42;
      float m43 = M43;
      float m44 = M44;

      float h1 = (m33 * m44) - (m34 * m43);
      float h2 = (m32 * m44) - (m34 * m42);
      float h3 = (m32 * m43) - (m33 * m42);
      float h4 = (m31 * m44) - (m34 * m41);
      float h5 = (m31 * m43) - (m33 * m41);
      float h6 = (m31 * m42) - (m32 * m41);
      return ((((m11 * (((m22 * h1) - (m23 * h2)) + (m24 * h3))) - (m12 * (((m21 * h1) - (m23 * h4)) + (m24 * h5)))) + (m13 * (((m21 * h2) - (m22 * h4)) + (m24 * h6)))) - (m14 * (((m21 * h3) - (m22 * h5)) + (m23 * h6))));
    }

    /// <summary>
    /// Inverts the matrix.
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void Invert()
    {
      Matrix.Invert(this, out this);
    }

    /// <summary>
    /// Flips the signs of each matrix element.
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void Negate()
    {
      Matrix.Negate(this, out this);
    }

    /// <summary>
    /// Swaps the rows and columns of the matrix.
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void Transpose()
    {
      Matrix.Transpose(this, out this);
    }

    /// <summary>
    /// Orthogonalizes the matrix.
    /// </summary>
    /// <remarks>
    /// Orthogonalization is the process of making the axes (rows) of the matrix orthogonal to each other. Thus any
    /// row in the matrix will be orthogonal to any other row in the Matrix. The method uses the modified Gram-Schmidt process,
    /// which results in the matrix to be numerically unstable where the numerical stability decreases in the order that the rows
    /// are pr ocessed. The first row is the most stable, the fourth row the least.
    /// </remarks>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void Orthogonalize()
    {
      Matrix.Orthogonalize(this, out this);
    }

    /// <summary>
    /// Decomposes a 3D rotation matrix into euler angles.
    /// </summary>
    /// <param name="yaw">Angle to rotate about the y-axis</param>
    /// <param name="pitch">Angle to rotate about the x-axis</param>
    /// <param name="roll">Angle to rotate about the z-axis</param>
    public readonly void ToEulerAngles(out Angle yaw, out Angle pitch, out Angle roll)
    {
      //North pole singularity
      if (M21 > 0.998)
      {
        yaw = new Angle(MathF.Atan2(M13, M33));
        pitch = new Angle(0.0f);
        roll = new Angle(MathHelper.PiOverTwo);
      }
      //South pole singularity
      else if (M21 < -0.998)
      {
        yaw = new Angle(MathF.Atan2(M13, M33));
        pitch = new Angle(0.0f);
        roll = new Angle(-MathHelper.PiOverTwo);
      }
      else
      {
        yaw = new Angle(MathF.Atan2(-M31, M11));
        pitch = new Angle(MathF.Atan2(-M23, M22));
        roll = new Angle(MathF.Asin(M21));
      }

      if (MathF.Abs(yaw.Radians) <= MathHelper.ZeroTolerance)
        yaw = new Angle(0.0f);

      if (MathF.Abs(pitch.Radians) <= MathHelper.ZeroTolerance)
        pitch = new Angle(0.0f);

      if (MathF.Abs(roll.Radians) <= MathHelper.ZeroTolerance)
        roll = new Angle(0.0f);
    }

    /// <summary>
    /// Decomposes a 3D scale/rotation/translation (SRT) matrix into its
    /// scaling, rotation, and translation components.
    /// </summary>
    /// <param name="scale">Decomposed scale</param>
    /// <param name="rotation">Decomposed rotation</param>
    /// <param name="translation">Decomposed translation</param>
    /// <returns>Returns true if rotation was derived, if false the identity quaternion is returned</returns>
    public readonly bool Decompose(out Vector3 scale, out Quaternion rotation, out Vector3 translation)
    {
      float m11 = M11;
      float m12 = M12;
      float m13 = M13;
      float m14 = M14;
      float m21 = M21;
      float m22 = M22;
      float m23 = M23;
      float m24 = M24;
      float m31 = M31;
      float m32 = M32;
      float m33 = M33;
      float m34 = M34;
      float m41 = M41;
      float m42 = M42;
      float m43 = M43;
      float m44 = M44;

      //Flag for if the operation was a success - only returns false if we can't get a rotation
      bool flag = true;

      //Determine which axis to start with dealing with negative scaling
      int index1;
      int index2;
      int index3;

      //Setup a canonical basis
      Triad canonicalBasis = new Triad(Vector3.UnitX, Vector3.UnitY, Vector3.UnitZ);

      //Setup a basis for the rotation matrix
      Vector3 basisX;
      basisX.X = m11;
      basisX.Y = m12;
      basisX.Z = m13;

      Vector3 basisY;
      basisY.X = m21;
      basisY.Y = m22;
      basisY.Z = m23;

      Vector3 basisZ;
      basisZ.X = m31;
      basisZ.Y = m32;
      basisZ.Z = m33;

      Triad rotBasis = new Triad(basisX, basisY, basisZ);

      //Get the translation component
      translation.X = m41;
      translation.Y = m42;
      translation.Z = m43;

      //Get scaling component
      float scaleX = basisX.Length();
      float scaleY = basisY.Length();
      float scaleZ = basisZ.Length();

      if (scaleX < scaleY)
      {
        if (scaleY < scaleZ)
        {
          index1 = 2;
          index2 = 1;
          index3 = 0;
        }
        else
        {
          index1 = 1;
          if (scaleX < scaleZ)
          {
            index2 = 2;
            index3 = 0;
          }
          else
          {
            index2 = 0;
            index3 = 2;
          }
        }
      }
      else if (scaleX < scaleZ)
      {
        index1 = 2;
        index2 = 0;
        index3 = 1;
      }
      else
      {
        index1 = 0;
        if (scaleY < scaleZ)
        {
          index2 = 2;
          index3 = 1;
        }
        else
        {
          index2 = 1;
          index3 = 2;
        }
      }

      float temp = 0;
      switch (index1)
      {
        case 0:
          temp = scaleX;
          break;
        case 1:
          temp = scaleY;
          break;
        case 2:
          temp = scaleZ;
          break;
      }

      bool cb = false;
      if (temp < MathHelper.ZeroTolerance)
      {
        cb = true;
        rotBasis[index1] = canonicalBasis[index1];
      }

      rotBasis[index1] = Vector3.Normalize(rotBasis[index1]);

      temp = 0;
      switch (index2)
      {
        case 0:
          temp = scaleX;
          break;
        case 1:
          temp = scaleY;
          break;
        case 2:
          temp = scaleZ;
          break;
      }

      if (temp < MathHelper.ZeroTolerance)
      {
        int index4;
        float absX = MathF.Abs(rotBasis[index1].X);
        float absY = MathF.Abs(rotBasis[index1].Y);
        float absZ = MathF.Abs(rotBasis[index1].Z);

        if (absX < absY)
        {
          if (absY < absZ)
          {
            index4 = 0;
          }
          else if (absX < absZ)
          {
            index4 = 0;
          }
          else
          {
            index4 = 2;
          }
        }
        else if (absX < absZ)
        {
          index4 = 1;
        }
        else if (absY < absZ)
        {
          index4 = 1;
        }
        else
        {
          index4 = 2;
        }
        if (cb)
        {
          rotBasis[index4] = Vector3.Cross(rotBasis[index2], rotBasis[index1]);
        }
      }

      rotBasis[index2] = Vector3.Normalize(rotBasis[index2]);

      temp = 0;
      switch (index3)
      {
        case 0:
          temp = scaleX;
          break;
        case 1:
          temp = scaleY;
          break;
        case 2:
          temp = scaleZ;
          break;
      }

      if (temp < MathHelper.ZeroTolerance)
        rotBasis[index2] = Vector3.Cross(rotBasis[index3], rotBasis[index1]);

      rotBasis[index3] = Vector3.Normalize(rotBasis[index3]);

      float det = rotBasis.ComputeDeterminant();
      if (det < 0.0f)
      {
        switch (index1)
        {
          case 0:
            scaleX = -scaleX;
            break;
          case 1:
            scaleY = -scaleY;
            break;
          case 2:
            scaleZ = -scaleZ;
            break;
        }
        rotBasis[index1] = -rotBasis[index1];
        det = -det;
      }

      det--;
      det *= det;

      //Grab rotation
      if (MathHelper.ZeroTolerance < det)
      {
        rotation = Quaternion.Identity;
        flag = false;
      }
      else
      {
        Matrix m = Matrix.Identity;
        m.M11 = rotBasis.XAxis.X;
        m.M12 = rotBasis.XAxis.Y;
        m.M13 = rotBasis.XAxis.Z;
        m.M21 = rotBasis.YAxis.X;
        m.M22 = rotBasis.YAxis.Y;
        m.M23 = rotBasis.YAxis.Z;
        m.M31 = rotBasis.ZAxis.X;
        m.M32 = rotBasis.ZAxis.Y;
        m.M33 = rotBasis.ZAxis.Z;
        Quaternion.FromRotationMatrix(m, out rotation);
      }

      //Grab scaling
      scale.X = scaleX;
      scale.Y = scaleY;
      scale.Z = scaleZ;

      return flag;
    }

    /// <summary>
    /// Gets the matrix as a 16-length span of floats, every four values is a row in the matrix.
    /// </summary>
    /// <returns>Span of matrix components.</returns>
    public Span<float> AsSpan()
    {
      return new Span<Matrix>(ref Unsafe.AsRef(ref this)).Reinterpret<Matrix, float>();
    }

    /// <summary>
    /// Gets the matrix as a 16-length readonly span of floats, every four values is a row in the matrix.
    /// </summary>
    /// <returns>Readonly span of matrix components.</returns>
    public readonly ReadOnlySpan<float> AsReadOnlySpan()
    {
      return new ReadOnlySpan<Matrix>(ref Unsafe.AsRef(in this)).Reinterpret<Matrix, float>();
    }

    /// <summary>
    /// Tests equality between the matrix and another mat rix.
    /// </summary>
    /// <param name="other">Matrix to compare</param>
    /// <returns>True if components are equal, false otherwise.</returns>
    readonly bool IEquatable<Matrix>.Equals(Matrix other)
    {
      return (MathF.Abs(M11 - other.M11) <= MathHelper.ZeroTolerance) &&
             (MathF.Abs(M12 - other.M12) <= MathHelper.ZeroTolerance) &&
             (MathF.Abs(M13 - other.M13) <= MathHelper.ZeroTolerance) &&
             (MathF.Abs(M14 - other.M14) <= MathHelper.ZeroTolerance) &&

             (MathF.Abs(M21 - other.M21) <= MathHelper.ZeroTolerance) &&
             (MathF.Abs(M22 - other.M22) <= MathHelper.ZeroTolerance) &&
             (MathF.Abs(M23 - other.M23) <= MathHelper.ZeroTolerance) &&
             (MathF.Abs(M24 - other.M24) <= MathHelper.ZeroTolerance) &&

             (MathF.Abs(M31 - other.M31) <= MathHelper.ZeroTolerance) &&
             (MathF.Abs(M32 - other.M32) <= MathHelper.ZeroTolerance) &&
             (MathF.Abs(M33 - other.M33) <= MathHelper.ZeroTolerance) &&
             (MathF.Abs(M34 - other.M34) <= MathHelper.ZeroTolerance) &&

             (MathF.Abs(M41 - other.M41) <= MathHelper.ZeroTolerance) &&
             (MathF.Abs(M42 - other.M42) <= MathHelper.ZeroTolerance) &&
             (MathF.Abs(M43 - other.M43) <= MathHelper.ZeroTolerance) &&
             (MathF.Abs(M44 - other.M44) <= MathHelper.ZeroTolerance);
    }

    /// <summary>
    /// Tests equality between the matrix and another mat rix.
    /// </summary>
    /// <param name="other">Matrix to compare</param>
    /// <returns>True if components are equal, false otherwise.</returns>
    public readonly bool Equals(in Matrix other)
    {
      return (MathF.Abs(M11 - other.M11) <= MathHelper.ZeroTolerance) &&
             (MathF.Abs(M12 - other.M12) <= MathHelper.ZeroTolerance) &&
             (MathF.Abs(M13 - other.M13) <= MathHelper.ZeroTolerance) &&
             (MathF.Abs(M14 - other.M14) <= MathHelper.ZeroTolerance) &&

             (MathF.Abs(M21 - other.M21) <= MathHelper.ZeroTolerance) &&
             (MathF.Abs(M22 - other.M22) <= MathHelper.ZeroTolerance) &&
             (MathF.Abs(M23 - other.M23) <= MathHelper.ZeroTolerance) &&
             (MathF.Abs(M24 - other.M24) <= MathHelper.ZeroTolerance) &&

             (MathF.Abs(M31 - other.M31) <= MathHelper.ZeroTolerance) &&
             (MathF.Abs(M32 - other.M32) <= MathHelper.ZeroTolerance) &&
             (MathF.Abs(M33 - other.M33) <= MathHelper.ZeroTolerance) &&
             (MathF.Abs(M34 - other.M34) <= MathHelper.ZeroTolerance) &&

             (MathF.Abs(M41 - other.M41) <= MathHelper.ZeroTolerance) &&
             (MathF.Abs(M42 - other.M42) <= MathHelper.ZeroTolerance) &&
             (MathF.Abs(M43 - other.M43) <= MathHelper.ZeroTolerance) &&
             (MathF.Abs(M44 - other.M44) <= MathHelper.ZeroTolerance);
    }

    /// <summary>
    /// Tests equality between the matrix and another matrix.
    /// </summary>
    /// <param name="other">Matrix to compare</param>
    /// <param name="tolerance">Tolerance</param>
    /// <returns>True if components are equal within tolerance, false otherwise.</returns>
    public readonly bool Equals(in Matrix other, float tolerance)
    {
      return (MathF.Abs(M11 - other.M11) <= tolerance) &&
             (MathF.Abs(M12 - other.M12) <= tolerance) &&
             (MathF.Abs(M13 - other.M13) <= tolerance) &&
             (MathF.Abs(M14 - other.M14) <= tolerance) &&

             (MathF.Abs(M21 - other.M21) <= tolerance) &&
             (MathF.Abs(M22 - other.M22) <= tolerance) &&
             (MathF.Abs(M23 - other.M23) <= tolerance) &&
             (MathF.Abs(M24 - other.M24) <= tolerance) &&

             (MathF.Abs(M31 - other.M31) <= tolerance) &&
             (MathF.Abs(M32 - other.M32) <= tolerance) &&
             (MathF.Abs(M33 - other.M33) <= tolerance) &&
             (MathF.Abs(M34 - other.M34) <= tolerance) &&

             (MathF.Abs(M41 - other.M41) <= tolerance) &&
             (MathF.Abs(M42 - other.M42) <= tolerance) &&
             (MathF.Abs(M43 - other.M43) <= tolerance) &&
             (MathF.Abs(M44 - other.M44) <= tolerance);
    }

    /// <summary>
    /// Determines whether the specified <see cref="System.Object" /> is equal to this instance.
    /// </summary>
    /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
    /// <returns>True if the specified <see cref="System.Object" /> is equal to this instance; otherwise, false.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is Matrix)
        return Equals((Matrix)obj);

      return false;
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
    public readonly override int GetHashCode()
    {
      unchecked
      {
        return M11.GetHashCode() + M12.GetHashCode() + M13.GetHashCode() + M14.GetHashCode() +
               M21.GetHashCode() + M22.GetHashCode() + M23.GetHashCode() + M24.GetHashCode() +
               M31.GetHashCode() + M32.GetHashCode() + M33.GetHashCode() + M34.GetHashCode() +
               M41.GetHashCode() + M42.GetHashCode() + M43.GetHashCode() + M44.GetHashCode();
      }
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly override string ToString()
    {
      CultureInfo info = CultureInfo.CurrentCulture;
      Object[] args = new object[] { M11.ToString(info), M12.ToString(info), M13.ToString(info), M14.ToString(info),
                M21.ToString(info), M22.ToString(info), M23.ToString(info), M24.ToString(info),
                M31.ToString(info), M32.ToString(info), M33.ToString(info), M34.ToString(info),
                M41.ToString(info), M42.ToString(info), M43.ToString(info), M44.ToString(info) };
      return String.Format(info, "[M11: {0} M12: {1} M13: {2} M14: {3}] [M21: {4} M22: {5} M23: {6} M24: {7}] [M31: {8} M32: {9} M33: {10} M34: {11}] [M41: {12} M42: {13} M43: {14} M44: {15}]", args);
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="format">The format</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(string? format)
    {
      if (format is null)
        return ToString();

      CultureInfo info = CultureInfo.CurrentCulture;
      Object[] args = new object[] { M11.ToString(format, info), M12.ToString(format, info), M13.ToString(format, info), M14.ToString(format, info),
                M21.ToString(format, info), M22.ToString(format, info), M23.ToString(format, info), M24.ToString(format, info),
                M31.ToString(format, info), M32.ToString(format, info), M33.ToString(format, info), M34.ToString(format, info),
                M41.ToString(format, info), M42.ToString(format, info), M43.ToString(format, info), M44.ToString(format, info) };
      return String.Format(info, "[M11: {0} M12: {1} M13: {2} M14: {3}] [M21: {4} M22: {5} M23: {6} M24: {7}] [M31: {8} M32: {9} M33: {10} M34: {11}] [M41: {12} M42: {13} M43: {14} M44: {15}]", args);
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="formatProvider">The format provider.</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(IFormatProvider? formatProvider)
    {
      if (formatProvider is null)
        return ToString();

      Object[] args = new object[] { M11.ToString(formatProvider), M12.ToString(formatProvider), M13.ToString(formatProvider), M14.ToString(formatProvider),
                M21.ToString(formatProvider), M22.ToString(formatProvider), M23.ToString(formatProvider), M24.ToString(formatProvider),
                M31.ToString(formatProvider), M32.ToString(formatProvider), M33.ToString(formatProvider), M34.ToString(formatProvider),
                M41.ToString(formatProvider), M42.ToString(formatProvider), M43.ToString(formatProvider), M44.ToString(formatProvider) };
      return String.Format(formatProvider, "[M11: {0} M12: {1} M13: {2} M14: {3}] [M21: {4} M22: {5} M23: {6} M24: {7}] [M31: {8} M32: {9} M33: {10} M34: {11}] [M41: {12} M42: {13} M43: {14} M44: {15}]", args);
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="format">The format.</param>
    /// <param name="formatProvider">The format provider.</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(string? format, IFormatProvider? formatProvider)
    {
      if (formatProvider is null)
        return ToString();

      if (format is null)
        return ToString(formatProvider);

      Object[] args = new object[] { M11.ToString(format, formatProvider), M12.ToString(format, formatProvider), M13.ToString(format, formatProvider), M14.ToString(format, formatProvider),
                M21.ToString(format, formatProvider), M22.ToString(format, formatProvider), M23.ToString(format, formatProvider), M24.ToString(format, formatProvider),
                M31.ToString(format, formatProvider), M32.ToString(format, formatProvider), M33.ToString(format, formatProvider), M34.ToString(format, formatProvider),
                M41.ToString(format, formatProvider), M42.ToString(format, formatProvider), M43.ToString(format, formatProvider), M44.ToString(format, formatProvider) };
      return String.Format(formatProvider, "[M11: {0} M12: {1} M13: {2} M14: {3}] [M21: {4} M22: {5} M23: {6} M24: {7}] [M31: {8} M32: {9} M33: {10} M34: {11}] [M41: {12} M42: {13} M43: {14} M44: {15}]", args);
    }

    /// <summary>
    /// Writes the primitive data to the output.
    /// </summary>
    /// <param name="output">Primitive writer.</param>
    readonly void IPrimitiveValue.Write(IPrimitiveWriter output)
    {
      Vector4 row1 = new Vector4(M11, M12, M13, M14);
      Vector4 row2 = new Vector4(M21, M22, M23, M24);
      Vector4 row3 = new Vector4(M31, M32, M33, M34);
      Vector4 row4 = new Vector4(M41, M42, M43, M44);

      output.Write<Vector4>("Row1", row1);
      output.Write<Vector4>("Row2", row2);
      output.Write<Vector4>("Row3", row3);
      output.Write<Vector4>("Row4", row4);
    }

    /// <summary>
    /// Reads the primitive data from the input.
    /// </summary>
    /// <param name="input">Primitive reader.</param>
    void IPrimitiveValue.Read(IPrimitiveReader input)
    {
      Vector4 row1, row2, row3, row4;

      input.Read<Vector4>("Row1", out row1);
      input.Read<Vector4>("Row2", out row2);
      input.Read<Vector4>("Row3", out row3);
      input.Read<Vector4>("Row4", out row4);

      M11 = row1.X;
      M12 = row1.Y;
      M13 = row1.Z;
      M13 = row1.W;

      M21 = row2.X;
      M22 = row2.Y;
      M23 = row2.Z;
      M24 = row2.W;

      M31 = row3.X;
      M32 = row3.Y;
      M33 = row3.Z;
      M34 = row3.W;

      M41 = row4.X;
      M42 = row4.Y;
      M43 = row4.Z;
      M44 = row4.W;
    }
  }
}
