﻿/*
* Copyright (c) 2010-2024 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Tesla.Content;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Defines an infinite plane at an origin with a normal. The origin of the plane is represented by a distance value from zero, along the normal vector.
  /// </summary>
  [Serializable]
  [StructLayout(LayoutKind.Sequential)]
  public struct Plane : IEquatable<Plane>, IRefEquatable<Plane>, IFormattable, IPrimitiveValue
  {
    /// <summary>
    /// The normal vector of the plane.
    /// </summary>
    public Vector3 Normal;

    /// <summary>
    /// The plane constant, which is the (negative) distance from the origin (0, 0, 0) to the origin of the plane along its normal.
    /// </summary>
    public float D;

    private static int s_sizeInBytes = BufferHelper.SizeOf<Plane>();
    private static Plane s_unitX = new Plane(Vector3.UnitX, 0.0f);
    private static Plane s_unitY = new Plane(Vector3.UnitY, 0.0f);
    private static Plane s_unitZ = new Plane(Vector3.UnitZ, 0.0f);

    /// <summary>
    /// Gets the size of Plane structure in bytes.
    /// </summary>
    public static int SizeInBytes
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return s_sizeInBytes;
      }
    }

    /// <summary>
    /// Gets a unit <see cref="Plane"/> that has its origin at zero and normal set to (1, 0, 0).
    /// </summary>
    public static ref readonly Plane UnitX
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_unitX;
      }
    }

    /// <summary>
    /// Gets a unit <see cref="Plane"/> that has its origin at zero and normal set to (0, 1, 0).
    /// </summary>
    public static ref readonly Plane UnitY
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_unitY;
      }
    }

    /// <summary>
    /// Gets a unit <see cref="Plane"/> that has its origin at zero and normal set to (0, 0, 1).
    /// </summary>
    public static ref readonly Plane UnitZ
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_unitZ;
      }
    }

    /// <summary>
    /// Gets or sets the plane's origin.
    /// </summary>
    public Vector3 Origin
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      readonly get
      {
        Vector3.Multiply(Normal, -D, out Vector3 origin);
        return origin;
      }
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      set
      {
        D = -Vector3.Dot(Normal, value);
      }
    }


    /// <summary>
    /// Gets if the plane is degenerate (normal is zero).
    /// </summary>
    public readonly bool IsDegenerate
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return Normal.IsAlmostZero();
      }
    }

    /// <summary>
    /// Gets whether any of the components of the plane are NaN (Not A Number).
    /// </summary>
    public readonly bool IsNaN
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return float.IsNaN(D) || Normal.IsNaN;
      }
    }

    /// <summary>
    /// Gets whether any of the components of the plane are positive or negative infinity.
    /// </summary>
    public readonly bool IsInfinity
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return float.IsNegativeInfinity(D) || float.IsPositiveInfinity(D) || Normal.IsInfinity;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Plane"/> struct.
    /// </summary>
    /// <param name="x">X component of the plane normal</param>
    /// <param name="y">Y component of the plane normal</param>
    /// <param name="z">Z component of the plane normal</param>
    /// <param name="d">Plane constant, (negative) distance from origin (0, 0, 0) to plane origin.</param>
    public Plane(float x, float y, float z, float d)
    {
      Normal = new Vector3(x, y, z);
      D = d;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Plane"/> struct.
    /// </summary>
    /// <param name="normal">Plane normal</param>
    /// <param name="d">Plane constant, (negative) distance from origin (0, 0, 0) to plane origin.</param>
    public Plane(in Vector3 normal, float d)
    {
      Normal = normal;
      D = d;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Plane"/> struct.
    /// </summary>
    /// <param name="plane">XYZ contains the plane normal vector, and W contains the plane constant.</param>
    public Plane(in Vector4 plane)
    {
      Normal = new Vector3(plane.X, plane.Y, plane.Z);
      D = plane.W;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Plane"/> struct.
    /// </summary>
    /// <param name="normal">Plane normal.</param>
    /// <param name="origin">Plane origin.</param>
    public Plane(in Vector3 normal, in Vector3 origin)
    {
      Normal = normal;
      D = -Vector3.Dot(normal, origin);
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Plane"/> struct from three points.
    /// </summary>
    /// <param name="p1">First position</param>
    /// <param name="p2">Second position</param>
    /// <param name="p3">Third position</param>
    public Plane(in Vector3 p1, in Vector3 p2, in Vector3 p3)
    {
      Vector3 v1, v2;

      //Compute first vector
      v1.X = p2.X - p1.X;
      v1.Y = p2.Y - p1.Y;
      v1.Z = p2.Z - p1.Z;

      //Compute second vector
      v2.X = p3.X - p1.X;
      v2.Y = p3.Y - p1.Y;
      v2.Z = p3.Z - p1.Z;

      //Take cross product
      Vector3.NormalizedCross(v1, v2, out Normal);

      D = -Vector3.Dot(Normal, p1);
    }

    #region Public static methods

    /// <summary>
    /// Compute the dot product between the two specified planes (4D dot product where normals are XYZ and D is W).
    /// </summary>
    /// <param name="a">First plane</param>
    /// <param name="b">Second plane</param>
    /// <returns>Dot product</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float Dot(in Plane a, in Plane b)
    {
      return (a.Normal.X * b.Normal.X) + (a.Normal.Y * b.Normal.Y) + (a.Normal.Z * b.Normal.Z) + (a.D * b.D);
    }

    /// <summary>
    /// Compute the dot product between the specified plane's normal and vector plus the plane's constant.
    /// </summary>
    /// <param name="plane">Plane</param>
    /// <param name="value">Vector3</param>
    /// <returns>Dot product</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float DotCoordinate(in Plane plane, in Vector3 value)
    {
      return (plane.Normal.X * value.X) + (plane.Normal.Y * value.Y) + (plane.Normal.Z * value.Z) + plane.D;
    }

    /// <summary>
    /// Compute the dot product between the specified plane's normal and vector.
    /// </summary>
    /// <param name="plane">Plane</param>
    /// <param name="value">Vector3</param>
    /// <returns>Dot product</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float DotNormal(in Plane plane, in Vector3 value)
    {
      return (plane.Normal.X * value.X) + (plane.Normal.Y * value.Y) + (plane.Normal.Z * value.Z);
    }

    /// <summary>
    /// Compute the dot product between the two specified plane's normals.
    /// </summary>
    /// <param name="a">First plane</param>
    /// <param name="b">Second plane</param>
    /// <returns>Dot product</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float DotNormal(in Plane a, in Plane b)
    {
      return (a.Normal.X * b.Normal.X) + (a.Normal.Y * b.Normal.Y) + (a.Normal.Z * b.Normal.Z);
    }

    /// <summary>
    /// Normalizes the plane's normal to be unit length.
    /// </summary>
    /// <param name="plane">Plane to normalize.</param>
    /// <returns>Normalized plane.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Plane Normalize(in Plane plane)
    {
      Plane result = plane;
      result.Normalize();

      return result;
    }

    /// <summary>
    /// Normalizes the plane's normal to be unit length.
    /// </summary>
    /// <param name="plane">Plane to normalize.</param>
    /// <param name="result">Normalized plane.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Normalize(in Plane plane, out Plane result)
    {
      result = plane;
      result.Normalize();
    }

    /// <summary>
    /// Reverses the plane's normal so it is pointing in the opposite direction.
    /// </summary>
    /// <param name="plane">Plane to reverse.</param>
    /// <returns>Reversed plane.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Plane Negate(in Plane plane)
    {
      Plane result = plane;
      result.Negate();

      return result;
    }

    /// <summary>
    /// Reverses the plane's normal so it is pointing in the opposite direction.
    /// </summary>
    /// <param name="plane">Plane to reverse</param>
    /// <param name="result">Reversed plane.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Negate(in Plane plane, out Plane result)
    {
      result = plane;
      result.Negate();
    }

    #endregion

    #region Transform

    /// <summary>
    /// Transforms the plane by the given <see cref="Quaternion"/>.
    /// </summary>
    /// <param name="plane">Plane to transform.</param>
    /// <param name="rotation">Quaternion rotation.</param>
    /// <returns>Transformed plane.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Plane Transform(in Plane plane, in Quaternion rotation)
    {
      Plane result;
      result.D = plane.D;
      Vector3.Transform(plane.Normal, rotation, out result.Normal);

      return result;
    }

    /// <summary>
    /// Transforms the plane by the given <see cref="Quaternion"/>.
    /// </summary>
    /// <param name="plane">Plane to transform.</param>
    /// <param name="rotation">Quaternion rotation.</param>
    /// <param name="result">Transformed plane.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Transform(in Plane plane, in Quaternion rotation, out Plane result)
    {
      result.D = plane.D;
      Vector3.Transform(plane.Normal, rotation, out result.Normal);
    }

    /// <summary>
    /// Transforms the plane by the given <see cref="Matrix"/>.
    /// </summary>
    /// <param name="plane">Plane to transform.</param>
    /// <param name="transform">Transformation matrix.</param>
    /// <returns>Transformed plane.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Plane Transform(in Plane plane, in Matrix transform)
    {
      Vector3 planeNormal = plane.Normal;
      Vector3 planeOrigin = plane.Origin;

      Vector3.Transform(planeOrigin, transform, out planeOrigin);
      Vector3.TransformNormal(plane.Normal, transform, out planeNormal);

      return new Plane(planeNormal, planeOrigin);
    }

    /// <summary>
    /// Transforms the plane by the given <see cref="Matrix"/>.
    /// </summary>
    /// <param name="plane">Plane to transform.</param>
    /// <param name="transform">Transformation matrix.</param>
    /// <param name="result">Transformed plane.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Transform(in Plane plane, in Matrix transform, out Plane result)
    {
      Vector3 planeNormal = plane.Normal;
      Vector3 planeOrigin = plane.Origin;

      Vector3.Transform(planeOrigin, transform, out planeOrigin);
      Vector3.TransformNormal(plane.Normal, transform, out planeNormal);

      result = new Plane(planeNormal, planeOrigin);
    }

    /// <summary>
    /// Translates the plane by the given translation vector.
    /// </summary>
    /// <param name="plane">Plane to transform.</param>
    /// <param name="translation">Translation vector.</param>
    /// <returns>Transformed plane.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Plane Transform(in Plane plane, in Vector3 translation)
    {
      Vector3 planeOrigin = plane.Origin;
      Vector3.Add(planeOrigin, translation, out planeOrigin);

      return new Plane(plane.Normal, planeOrigin);
    }

    /// <summary>
    /// Translates the plane by the given translation vector.
    /// </summary>
    /// <param name="plane">Plane to transform.</param>
    /// <param name="translation">Translation vector.</param>
    /// <param name="result">Transformed plane.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Transform(in Plane plane, in Vector3 translation, out Plane result)
    {
      Vector3 planeOrigin = plane.Origin;
      Vector3.Add(planeOrigin, translation, out planeOrigin);

      result = new Plane(plane.Normal, planeOrigin);
    }

    #endregion

    #region Equality Operators

    /// <summary>
    /// Checks equality between two planes.
    /// </summary>
    /// <param name="a">First plane</param>
    /// <param name="b">Second plane</param>
    /// <returns>True if the planes are equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator ==(Plane a, Plane b)
    {
      return a.Normal.Equals(b.Normal) && (MathF.Abs(a.D - b.D) <= MathHelper.ZeroTolerance);
    }

    /// <summary>
    /// Checks inequality between two planes.
    /// </summary>
    /// <param name="a">First plane</param>
    /// <param name="b">Second plane</param>
    /// <returns>True if the planes are not equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator !=(Plane a, Plane b)
    {
      return !a.Normal.Equals(b.Normal) || !(MathF.Abs(a.D - b.D) <= MathHelper.ZeroTolerance);
    }

    #endregion

    #region Public methods

    /// <summary>
    /// Reverses the plane's normal so it is pointing in the opposite direction.
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void Negate()
    {
      Normal.Negate();
      D = -D;
    }

    /// <summary>
    /// Normalizes the plane's normal to be unit length.
    /// </summary>
    public void Normalize()
    {
      float lengthSquared = (Normal.X * Normal.X) + (Normal.Y * Normal.Y) + (Normal.Z * Normal.Z);
      if (lengthSquared != 0.0f)
      {
        float invLength = 1.0f / MathF.Sqrt(lengthSquared);
        Vector3.Multiply(Normal, invLength, out Normal);
        D *= invLength;
      }
    }

    /// <summary>
    /// Determines which side of the plane the point lies on.
    /// </summary>
    /// <param name="point">Point to test.</param>
    /// <returns>Which side of the plane the point lies on.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public PlaneIntersectionType WhichSide(in Vector3 point)
    {
      float dot = Vector3.Dot(Normal, point);
      float distance = dot + D;

      if (distance > 0.0f)
        return PlaneIntersectionType.Front;
      else if (distance < 0.0f)
        return PlaneIntersectionType.Back;

      return PlaneIntersectionType.Intersects;
    }

    /// <summary>
    /// Determines the signed distance between this object and a point. If negative, the point is on the
    /// back of the plane, if positive then on the front.
    /// </summary>
    /// <param name="point">Point to test.</param>
    /// <returns>Distance between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public float SignedDistanceTo(in Vector3 point)
    {
      float dot = Vector3.Dot(Normal, point);
      return dot + D;
    }

    #endregion

    #region Intersects

    /// <summary>
    /// Determines whether there is an intersection between this object and a <see cref="Ray"/>.
    /// </summary>
    /// <param name="ray">Ray to test.</param>
    /// <returns>True if an intersection between the two objects occurs, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Intersects(in Ray ray)
    {
      return GeometricToolsHelper.IntersectRayPlane(ray, this);
    }

    /// <summary>
    /// Determines whether there is an intersection between this object and a <see cref="Ray"/>.
    /// </summary>
    /// <param name="ray">Ray to test.</param>
    /// <param name="result">Intersection result between the two objects.</param>
    /// <returns>True if an intersection between the two objects occurs, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Intersects(in Ray ray, out LineIntersectionResult result)
    {
      return GeometricToolsHelper.IntersectRayPlane(ray, this, out result);
    }

    /// <summary>
    /// Determines whether there is an intersection between this object and a <see cref="Segment"/>.
    /// </summary>
    /// <param name="segment">Segment to test.</param>
    /// <returns>True if an intersection between the two objects occurs, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Intersects(in Segment segment)
    {
      return GeometricToolsHelper.IntersectSegmentPlane(segment, this);
    }

    /// <summary>
    /// Determines whether there is an intersection between this object and a <see cref="Segment"/>.
    /// </summary>
    /// <param name="segment">Segment to test.</param>
    /// <param name="result">Intersection result between the two objects.</param>
    /// <returns>True if an intersection between the two objects occurs, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Intersects(in Segment segment, out LineIntersectionResult result)
    {
      return GeometricToolsHelper.IntersectSegmentPlane(segment, this, out result);
    }

    /// <summary>
    /// Determines whether there is an intersection between this object and a <see cref="Plane"/>.
    /// </summary>
    /// <param name="plane">Plane to test.</param>
    /// <returns>True if an intersection between the two objects occurs, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Intersects(in Plane plane)
    {
      return GeometricToolsHelper.IntersectPlanePlane(this, plane);
    }

    /// <summary>
    /// Determines whether there is an intersection between this object and a <see cref="Plane"/>.
    /// </summary>
    /// <param name="plane">Plane to test.</param>
    /// <param name="result">Intersection result between the two objects.</param>
    /// <returns>True if an intersection between the two objects occurs, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Intersects(in Plane plane, out Ray result)
    {
      return GeometricToolsHelper.IntersectPlanePlane(this, plane, out result);
    }

    /// <summary>
    /// Determines whether there is an intersection between this object and a <see cref="Triangle"/>.
    /// </summary>
    /// <param name="triangle">Triangle to test.</param>
    /// <returns>True if an intersection between the two objects occurs, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Intersects(in Triangle triangle)
    {
      return GeometricToolsHelper.IntersectPlaneTriangle(triangle, this);
    }

    /// <summary>
    /// Determines whether there is an intersection between this object and a <see cref="Triangle"/>.
    /// </summary>
    /// <param name="triangle">Triangle to test.</param>
    /// <param name="result">Intersection result between the two objects.</param>
    /// <returns>True if an intersection between the two objects occurs, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Intersects(in Triangle triangle, out Segment result)
    {
      return GeometricToolsHelper.IntersectPlaneTriangle(triangle, this, out result);
    }

    /// <summary>
    /// Determines whether there is an intersection between this object and an <see cref="Ellipse"/>.
    /// </summary>
    /// <param name="ellipse">Ellipse to test.</param>
    /// <returns>True if an intersection between the two objects occurs, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Intersects(in Ellipse ellipse)
    {
      //TODO
      throw new NotImplementedException();
    }

    /// <summary>
    /// Determines whether there is an intersection between this object and an <see cref="Ellipse"/>.
    /// </summary>
    /// <param name="ellipse">Ellipse to test.</param>
    /// <param name="result">Intersection result between the two objects.</param>
    /// <returns>True if an intersection between the two objects occurs, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Intersects(in Ellipse ellipse, out Segment result)
    {
      //TODO
      throw new NotImplementedException();
    }

    /// <summary>
    /// Determines whether there is an intersection between this object and a <see cref="BoundingVolume"/>.
    /// </summary>
    /// <param name="volume">Bounding volume to test.</param>
    /// <returns>Type of plane intersection.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly PlaneIntersectionType Intersects(BoundingVolume? volume)
    {
      if (volume is null)
        return PlaneIntersectionType.Front;

      return volume.Intersects(this);
    }

    #endregion

    #region Distance To

    /// <summary>
    /// Determines the distance between this object and a point.
    /// </summary>
    /// <param name="point">Point to test.</param>
    /// <returns>Distance between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float DistanceTo(in Vector3 point)
    {
      GeometricToolsHelper.DistancePointPlane(this, point, out Vector3 ptOnPlane, out float sqrDist);

      return MathF.Sqrt(sqrDist);
    }

    /// <summary>
    /// Determines the distance between this object and a <see cref="Ray"/>.
    /// </summary>
    /// <param name="ray">Ray to test.</param>
    /// <returns>Distance between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float DistanceTo(in Ray ray)
    {
      GeometricToolsHelper.DistanceRayPlane(this, ray, out Vector3 ptOnPlane, out Vector3 ptOnRay, out float sqrDist);

      return MathF.Sqrt(sqrDist);
    }

    /// <summary>
    /// Determines the distance between this object and a <see cref="Segment"/>.
    /// </summary>
    /// <param name="segment">Segment to test.</param>
    /// <returns>Distance between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float DistanceTo(in Segment segment)
    {
      GeometricToolsHelper.DistanceSegmentPlane(this, segment, out Vector3 ptOnPlane, out Vector3 ptOnSegment, out float sqrDist);

      return MathF.Sqrt(sqrDist);
    }


    /// <summary>
    /// Determines the distance between this object and a <see cref="Plane"/>.
    /// </summary>
    /// <param name="plane">Plane to test.</param>
    /// <returns>Distance between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float DistanceTo(in Plane plane)
    {
      float dist = 0.0f;

      //Most likely the planes will intersect...if not, then they are parallel or coplanar so take distance between origins
      if (!GeometricToolsHelper.IntersectPlanePlane(this, plane))
        dist = Vector3.Distance(Origin, plane.Origin);

      return dist;
    }

    /// <summary>
    /// Determines the distance between this object and a <see cref="Triangle"/>.
    /// </summary>
    /// <param name="triangle">Triangle to test.</param>
    /// <returns>Distance between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float DistanceTo(in Triangle triangle)
    {
      GeometricToolsHelper.DistancePlaneTriangle(triangle, this, out Vector3 ptOnTriangle, out Vector3 ptOnPlane, out float sqrDist);

      return MathF.Sqrt(sqrDist);
    }

    /// <summary>
    /// Determines the distance between this object and an <see cref="Ellipse"/>.
    /// </summary>
    /// <param name="ellipse">Ellipse to test.</param>
    /// <returns>Distance between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float DistanceTo(in Ellipse ellipse)
    {
      //TODO
      throw new NotImplementedException();
    }

    #endregion

    #region Distance Squared To

    /// <summary>
    /// Determines the distance squared between this object and a point.
    /// </summary>
    /// <param name="point">Point to test.</param>
    /// <returns>Distance squared between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float DistanceSquaredTo(in Vector3 point)
    {
      GeometricToolsHelper.DistancePointPlane(this, point, out Vector3 ptOnPlane, out float sqrDist);

      return sqrDist;
    }

    /// <summary>
    /// Determines the distance squared between this object and a <see cref="Ray"/>.
    /// </summary>
    /// <param name="ray">Ray to test.</param>
    /// <returns>Distance squared between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float DistanceSquaredTo(in Ray ray)
    {
      GeometricToolsHelper.DistanceRayPlane(this, ray, out Vector3 ptOnPlane, out Vector3 ptOnRay, out float sqrDist);

      return sqrDist;
    }

    /// <summary>
    /// Determines the distance squared between this object and a <see cref="Segment"/>.
    /// </summary>
    /// <param name="segment">Segment to test.</param>
    /// <returns>Distance squared between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float DistanceSquaredTo(in Segment segment)
    {
      GeometricToolsHelper.DistanceSegmentPlane(this, segment, out Vector3 ptOnPlane, out Vector3 ptOnSegment, out float sqrDist);

      return sqrDist;
    }

    /// <summary>
    /// Determines the distance squared between this object and a <see cref="Plane"/>.
    /// </summary>
    /// <param name="plane">Plane to test.</param>
    /// <returns>Distance squared between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float DistanceSquaredTo(in Plane plane)
    {
      float sqrDist = 0.0f;

      //Most likely the planes will intersect...if not, then they are parallel or coplanar so take distance between origins
      if (!GeometricToolsHelper.IntersectPlanePlane(this, plane))
        sqrDist = Vector3.DistanceSquared(Origin, plane.Origin);

      return sqrDist;
    }

    /// <summary>
    /// Determines the distance squared between this object and a <see cref="Triangle"/>.
    /// </summary>
    /// <param name="triangle">Triangle to test.</param>
    /// <returns>Distance squared between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float DistanceSquaredTo(in Triangle triangle)
    {
      GeometricToolsHelper.DistancePlaneTriangle(triangle, this, out Vector3 ptOnTriangle, out Vector3 ptOnPlane, out float sqrDist);

      return sqrDist;
    }

    /// <summary>
    /// Determines the distance squared between this object and an <see cref="Ellipse"/>.
    /// </summary>
    /// <param name="ellipse">Ellipse to test.</param>
    /// <returns>Distance squared between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float DistanceSquaredTo(in Ellipse ellipse)
    {
      //TODO
      throw new NotImplementedException();
    }

    #endregion

    #region Closest Point / Approach Segment

    /// <summary>
    /// Determines the closest point on this object from the point.
    /// </summary>
    /// <param name="point">Point to test.</param>
    /// <returns>Closest point on this object.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly Vector3 ClosestPointTo(in Vector3 point)
    {
      GeometricToolsHelper.DistancePointPlane(this, point, out Vector3 ptOnPlane, out float sqrDist);

      return ptOnPlane;
    }

    /// <summary>
    /// Determines the closest point on this object from the point.
    /// </summary>
    /// <param name="point">Point to test.</param>
    /// <param name="result">Closest point on this object.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly void ClosestPointTo(in Vector3 point, out Vector3 result)
    {
      GeometricToolsHelper.DistancePointPlane(this, point, out result, out float sqrDist);
    }

    /// <summary>
    /// Determines the closest point on this object from the point.
    /// </summary>
    /// <param name="point">Point to test.</param>
    /// <param name="result">Closest point on this object.</param>
    /// <param name="squaredDistance">Squared distance between the two objects.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly void ClosestPointTo(in Vector3 point, out Vector3 result, out float squaredDistance)
    {
      GeometricToolsHelper.DistancePointPlane(this, point, out result, out squaredDistance);
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Ray"/>.
    /// </summary>
    /// <param name="ray">Ray to test</param>
    /// <returns>Closest approach segment between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly Segment ClosestApproachSegment(in Ray ray)
    {
      Segment result;
      GeometricToolsHelper.DistanceRayPlane(this, ray, out result.StartPoint, out result.EndPoint, out float sqrDist);

      return result;
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Ray"/>.
    /// </summary>
    /// <param name="ray">Ray to test</param>
    /// <param name="result">Closest approach segment between the two objects.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly void ClosestApproachSegment(in Ray ray, out Segment result)
    {
      GeometricToolsHelper.DistanceRayPlane(this, ray, out result.StartPoint, out result.EndPoint, out float sqrDist);
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Ray"/>.
    /// </summary>
    /// <param name="ray">Ray to test</param>
    /// <param name="result">Closest approach segment between the two objects.</param>
    /// <param name="squaredDistance">Squared distance between the two objects.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly void ClosestApproachSegment(in Ray ray, out Segment result, out float squaredDistance)
    {
      GeometricToolsHelper.DistanceRayPlane(this, ray, out result.StartPoint, out result.EndPoint, out squaredDistance);
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Segment"/>.
    /// </summary>
    /// <param name="segment">Segment to test</param>
    /// <returns>Closest approach segment between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly Segment ClosestApproachSegment(in Segment segment)
    {
      Segment result;
      GeometricToolsHelper.DistanceSegmentPlane(this, segment, out result.StartPoint, out result.EndPoint, out float sqrDist);

      return result;
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Segment"/>.
    /// </summary>
    /// <param name="segment">Segment to test</param>
    /// <param name="result">Closest approach segment between the two objects.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly void ClosestApproachSegment(in Segment segment, out Segment result)
    {
      GeometricToolsHelper.DistanceSegmentPlane(this, segment, out result.StartPoint, out result.EndPoint, out float sqrDist);
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Segment"/>.
    /// </summary>
    /// <param name="segment">Segment to test</param>
    /// <param name="result">Closest approach segment between the two objects.</param>
    /// <param name="squaredDistance">Squared distance between the two objects.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly void ClosestApproachSegment(in Segment segment, out Segment result, out float squaredDistance)
    {
      GeometricToolsHelper.DistanceSegmentPlane(this, segment, out result.StartPoint, out result.EndPoint, out squaredDistance);
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Plane"/>.
    /// </summary>
    /// <param name="plane">Plane to test</param>
    /// <returns>Closest approach segment between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public Segment ClosestApproachSegment(in Plane plane)
    {
      Segment result;
      if (GeometricToolsHelper.IntersectPlanePlane(this, plane, out Ray intr))
      {
        result.StartPoint = intr.Origin;
        result.EndPoint = intr.Origin;
      }
      else
      {
        result.StartPoint = Origin;
        result.EndPoint = plane.Origin;
      }

      return result;
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Plane"/>.
    /// </summary>
    /// <param name="plane">Plane to test</param>
    /// <param name="result">Closest approach segment between the two objects.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly void ClosestApproachSegment(in Plane plane, out Segment result)
    {
      if (GeometricToolsHelper.IntersectPlanePlane(this, plane, out Ray intr))
      {
        result.StartPoint = intr.Origin;
        result.EndPoint = intr.Origin;
      }
      else
      {
        result.StartPoint = Origin;
        result.EndPoint = plane.Origin;
      }
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Plane"/>.
    /// </summary>
    /// <param name="plane">Plane to test</param>
    /// <param name="result">Closest approach segment between the two objects.</param>
    /// <param name="squaredDistance">Squared distance between the two objects.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly void ClosestApproachSegment(in Plane plane, out Segment result, out float squaredDistance)
    {
      if (GeometricToolsHelper.IntersectPlanePlane(this, plane, out Ray intr))
      {
        result.StartPoint = intr.Origin;
        result.EndPoint = intr.Origin;
        squaredDistance = 0.0f;
      }
      else
      {
        result.StartPoint = Origin;
        result.EndPoint = plane.Origin;
        squaredDistance = result.Length * result.Length;
      }
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Triangle"/>.
    /// </summary>
    /// <param name="triangle">Triangle to test</param>
    /// <returns>Closest approach segment between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly Segment ClosestApproachSegment(in Triangle triangle)
    {
      Segment result;
      GeometricToolsHelper.DistancePlaneTriangle(triangle, this, out result.EndPoint, out result.StartPoint, out float sqrDist);

      return result;
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Triangle"/>.
    /// </summary>
    /// <param name="triangle">Triangle to test</param>
    /// <param name="result">Closest approach segment between the two objects.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly void ClosestApproachSegment(in Triangle triangle, out Segment result)
    {
      GeometricToolsHelper.DistancePlaneTriangle(triangle, this, out result.EndPoint, out result.StartPoint, out float sqrDist);
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Triangle"/>.
    /// </summary>
    /// <param name="triangle">Triangle to test</param>
    /// <param name="result">Closest approach segment between the two objects.</param>
    /// <param name="squaredDistance">Squared distance between the two objects.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly void ClosestApproachSegment(in Triangle triangle, out Segment result, float squaredDistance)
    {
      GeometricToolsHelper.DistancePlaneTriangle(triangle, this, out result.EndPoint, out result.StartPoint, out squaredDistance);
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Ellipse"/>.
    /// </summary>
    /// <param name="ellipse">Ellipse to test</param>
    /// <returns>Closest approach segment between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly Segment ClosestApproachSegment(in Ellipse ellipse)
    {
      //TODO
      throw new NotImplementedException();
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Ellipse"/>.
    /// </summary>
    /// <param name="ellipse">Ellipse to test</param>
    /// <param name="result">Closest approach segment between the two objects.</param>
    /// <param name="squaredDistance">Squared distance between the two objects.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly void ClosestApproachSegment(in Ellipse ellipse, out Segment result, out float squaredDistance)
    {
      //TODO
      throw new NotImplementedException();
    }

    #endregion

    #region Equality

    /// <summary>
    /// Checks equality between the plane and another plane.
    /// </summary>
    /// <param name="other">Other plane</param>
    /// <returns>True if the planes are equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    readonly bool IEquatable<Plane>.Equals(Plane other)
    {
      return Normal.Equals(other.Normal) && (MathF.Abs(D - other.D) <= MathHelper.ZeroTolerance);
    }

    /// <summary>
    /// Checks equality between the plane and another plane.
    /// </summary>
    /// <param name="other">Other plane</param>
    /// <returns>True if the planes are equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Equals(in Plane other)
    {
      return Normal.Equals(other.Normal) && (MathF.Abs(D - other.D) <= MathHelper.ZeroTolerance);
    }

    /// <summary>
    /// Checks inequality between the plane and another plane.
    /// </summary>
    /// <param name="other">Other plane</param>
    /// <param name="tolerance">Tolerance</param>
    /// <returns>True if the planes are equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Equals(in Plane other, float tolerance)
    {
      return Normal.Equals(other.Normal, tolerance) && (MathF.Abs(D - other.D) <= tolerance);
    }

    /// <summary>
    /// Determines whether the specified <see cref="System.Object" /> is equal to this instance.
    /// </summary>
    /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
    /// <returns>True if the specified <see cref="System.Object" /> is equal to this instance; otherwise, False.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is Plane)
        return Equals((Plane)obj);

      return false;
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
    public readonly override int GetHashCode()
    {
      unchecked
      {
        return Normal.GetHashCode() + D.GetHashCode();
      }
    }

    #endregion

    #region ToString

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly override string ToString()
    {
      CultureInfo info = CultureInfo.CurrentCulture;
      return String.Format(info, "Normal: {0}, D: {1}, Origin: {2}",
          new Object[] { Normal.ToString(info), D.ToString(info), Origin.ToString(info) });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="format">The format</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(string? format)
    {
      if (format is null)
        return ToString();

      CultureInfo info = CultureInfo.CurrentCulture;
      return String.Format(info, "Normal: {0}, D: {1}, Origin: {2}",
          new Object[] { Normal.ToString(format, info), D.ToString(format, info), Origin.ToString(format, info) });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="formatProvider">The format provider.</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(IFormatProvider? formatProvider)
    {
      if (formatProvider is null)
        return ToString();

      return String.Format(formatProvider, "Normal: {0}, D: {1}, Origin: {2}",
          new Object[] { Normal.ToString(formatProvider), D.ToString(formatProvider), Origin.ToString(formatProvider) });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="format">The format.</param>
    /// <param name="formatProvider">The format provider.</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(string? format, IFormatProvider? formatProvider)
    {
      if (formatProvider is null)
        return ToString();

      if (format is null)
        return ToString(formatProvider);

      return String.Format(formatProvider, "Normal: {0}, D: {1}, Origin: {2}",
          new Object[] { Normal.ToString(format, formatProvider), D.ToString(format, formatProvider), Origin.ToString(format, formatProvider) });
    }

    #endregion

    #region IPrimitiveValue

    /// <summary>
    /// Writes the primitive data to the output.
    /// </summary>
    /// <param name="output">Primitive writer.</param>
    readonly void IPrimitiveValue.Write(IPrimitiveWriter output)
    {
      output.Write<Vector3>("Normal", Normal);
      output.Write("D", D);
    }

    /// <summary>
    /// Reads the primitive data from the input.
    /// </summary>
    /// <param name="input">Primitive reader.</param>
    void IPrimitiveValue.Read(IPrimitiveReader input)
    {
      input.Read<Vector3>("Normal", out Normal);
      D = input.ReadSingle("D");
    }

    #endregion
  }
}
