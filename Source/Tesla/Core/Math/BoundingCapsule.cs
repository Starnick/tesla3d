﻿/*
* Copyright (c) 2010-2024 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Tesla.Content;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Represents a Bounding Capsule that is a cylinder capped with two half spheres. The capsule is defined by a center line segment and a radius.
  /// </summary>
  [SavableVersion(1)]
  public sealed class BoundingCapsule : BoundingVolume
  {
    private Segment m_centerLine;
    private float m_radius;

    /// <summary>
    /// Gets or sets the center of the bounding volume.
    /// </summary>
    public override Vector3 Center
    {
      get
      {
        return m_centerLine.Center;
      }
      set
      {
        m_centerLine.Center = value;
        UpdateCorners = true;
      }
    }

    /// <summary>
    /// Gets or sets the center line start point of the bounding capsule.
    /// </summary>
    public Vector3 StartPoint
    {
      get
      {
        return m_centerLine.StartPoint;
      }
      set
      {
        m_centerLine.StartPoint = value;
        UpdateCorners = true;
      }
    }

    /// <summary>
    /// Gets or sets the center line end point of the bounding capsule.
    /// </summary>
    public Vector3 EndPoint
    {
      get
      {
        return m_centerLine.EndPoint;
      }
      set
      {
        m_centerLine.EndPoint = value;
        UpdateCorners = true;
      }
    }

    /// <summary>
    /// Gets or sets the center line segment of the bounding capsule.
    /// </summary>
    public Segment CenterLine
    {
      get
      {
        return m_centerLine;
      }
      set
      {
        m_centerLine = value;
        UpdateCorners = true;
      }
    }

    /// <summary>
    /// Gets or sets the radius of the bounding capsule.
    /// </summary>
    public float Radius
    {
      get
      {
        return m_radius;
      }
      set
      {
        m_radius = value;
        UpdateCorners = true;
      }
    }

    /// <summary>
    /// Gets the volume of the bounding volume.
    /// </summary>
    public override float Volume { get { return MathHelper.Pi * (m_radius * m_radius) * ((MathHelper.FourThirds * m_radius) + m_centerLine.Length); } }

    /// <summary>
    /// Gets the bounding type.
    /// </summary>
    public override BoundingType BoundingType { get { return BoundingType.Capsule; } }

    /// <summary>
    /// Initializes a new instance of the <see cref="BoundingCapsule"/> class.
    /// </summary>
    public BoundingCapsule()
    {
      m_centerLine.StartPoint = Vector3.Zero;
      m_centerLine.EndPoint = Vector3.Zero;
      m_radius = 0.0f;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="BoundingCapsule"/> class.
    /// </summary>
    /// <param name="startPoint">Center line start point of the bounding capsule.</param>
    /// <param name="endPoint">Center line end point of the bounding capsule.</param>
    /// <param name="radius">Radius of the bounding capsule.</param>
    public BoundingCapsule(in Vector3 startPoint, in Vector3 endPoint, float radius)
    {
      m_centerLine.StartPoint = startPoint;
      m_centerLine.EndPoint = endPoint;
      m_radius = radius;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="BoundingCapsule"/> class.
    /// </summary>
    /// <param name="centerLine">Center line segment of the bounding capsule</param>
    /// <param name="radius">Radius of the bounding capsule.</param>
    public BoundingCapsule(in Segment centerLine, float radius)
    {
      m_centerLine = centerLine;
      m_radius = radius;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="BoundingCapsule"/> class.
    /// </summary>
    /// <param name="boundingCapsuleData">Bounding capsule data.</param>
    public BoundingCapsule(in BoundingCapsule.Data boundingCapsuleData)
    {
      m_centerLine = boundingCapsuleData.CenterLine;
      m_radius = boundingCapsuleData.Radius;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="BoundingCapsule"/> class.
    /// </summary>
    /// <param name="boundingCapsule">Bounding capsule to copy from.</param>
    /// <exception cref="ArgumentNullException">Thrown if the bounding capsule is null.</exception>
    public BoundingCapsule(BoundingCapsule boundingCapsule)
    {
      ArgumentNullException.ThrowIfNull(boundingCapsule, nameof(boundingCapsule));

      m_centerLine = boundingCapsule.m_centerLine;
      m_radius = boundingCapsule.m_radius;
    }

    /// <summary>
    /// Sets the bounding volume by either copying the specified bounding volume if its the specified type, or computing a volume required to fully contain it.
    /// </summary>
    /// <param name="volume">Bounding volume to copy from</param>
    public override void Set(BoundingVolume? volume)
    {
      if (volume is null)
        return;

      switch (volume.BoundingType)
      {
        case BoundingType.AxisAlignedBoundingBox:
          {
            BoundingBox other = (volume as BoundingBox)!;
            Vector3 center = other.Center;
            Vector3 extents = other.Extents;
            Triad axes = Triad.UnitAxes;

            GeometricToolsHelper.CapsuleFromBox(center, axes.XAxis, axes.YAxis, axes.ZAxis, extents, out m_centerLine, out m_radius);
          }
          break;
        case BoundingType.Sphere:
          {
            //Sphere is just a degenerate capsule
            BoundingSphere other = (volume as BoundingSphere)!;
            Vector3 center = other.Center;
            m_centerLine.StartPoint = center;
            m_centerLine.EndPoint = center;
            m_radius = other.Radius;
          }
          break;
        case BoundingType.Capsule:
          {
            BoundingCapsule other = (volume as BoundingCapsule)!;
            m_centerLine = other.m_centerLine;
            m_radius = other.m_radius;
          }
          break;
        case BoundingType.OrientedBoundingBox:
          {
            OrientedBoundingBox other = (volume as OrientedBoundingBox)!;
            Vector3 center = other.Center;
            Vector3 extents = other.Extents;
            Triad axes = other.Axes;

            GeometricToolsHelper.CapsuleFromBox(center, axes.XAxis, axes.YAxis, axes.ZAxis, extents, out m_centerLine, out m_radius);
          }
          break;
        case BoundingType.Frustum:
        case BoundingType.Mesh:
        default:
          {
            ComputeFromPoints(volume.Corners, null);
          }
          break;
      }

      UpdateCorners = true;
    }

    /// <summary>
    /// Creates a copy of the bounding volume and returns a new instance.
    /// </summary>
    /// <returns>Copied bounding volume</returns>
    public override BoundingVolume Clone()
    {
      return new BoundingCapsule(this);
    }

    /// <summary>
    /// Computes the distance from a given point to the edge of the volume.
    /// </summary>
    /// <param name="point">Point in space</param>
    /// <returns>Distance from the point to the edge of the volume.</returns>
    public override float DistanceTo(in Vector3 point)
    {
      float distSquared, segParameter;
      Vector3 ptOnCenterLine;
      GeometricToolsHelper.DistancePointSegment(point, m_centerLine, out ptOnCenterLine, out segParameter, out distSquared);

      return MathF.Max(0.0f, MathF.Sqrt(distSquared) - m_radius);
    }

    /// <summary>
    /// Computes the distance squared from a given point to the edge of the volume.
    /// </summary>
    /// <param name="point">Point in space</param>
    /// <returns>Distanced squared from the point to the edge of the volume.</returns>
    public override float DistanceSquaredTo(in Vector3 point)
    {
      float distSquared, segParameter;
      Vector3 ptOnCenterLine;
      GeometricToolsHelper.DistancePointSegment(point, m_centerLine, out ptOnCenterLine, out segParameter, out distSquared);

      return MathF.Max(0.0f, distSquared - (m_radius * m_radius));
    }

    /// <summary>
    /// Computes the closest point on the volume from the given point in space.
    /// </summary>
    /// <param name="point">Point in space</param>
    /// <param name="result">Closest point on the edge of the volume.</param>
    public override void ClosestPointTo(in Vector3 point, out Vector3 result)
    {
      float distSquared, segParameter;
      Vector3 ptOnCenterLine;
      GeometricToolsHelper.DistancePointSegment(point, m_centerLine, out ptOnCenterLine, out segParameter, out distSquared);

      //Essentially becomes like bounding sphere, but with a point along the center line that the input point is closest to
      Vector3 v;
      Vector3.Subtract(point, ptOnCenterLine, out v);
      v.Normalize();

      Vector3.Multiply(v, m_radius, out v);
      Vector3.Add(ptOnCenterLine, v, out result);
    }

    /// <summary>
    /// Computes a minimum bounding volume that encloses the specified points in space.
    /// </summary>
    /// <param name="points">Points in space</param>
    /// <param name="subMeshRange">Optional range inside the buffer that represents the mesh to compute bounds for. Note: the base vertex offset will be ignored.</param>

    public override void ComputeFromPoints(ReadOnlySpan<Vector3> points, in SubMeshRange? subMeshRange = null)
    {
      BoundingCapsule.Data data;
      BoundingCapsule.Data.FromPoints(points, subMeshRange, out data);

      m_centerLine = data.CenterLine;
      m_radius = data.Radius;
      UpdateCorners = true;
    }

    /// <summary>
    /// Computes a minimum bounding volume that encloses the specified indexed points in space.
    /// </summary>
    /// <param name="points">Points in space.</param>
    /// <param name="indices">Point indices denoting location in the point buffer.</param>
    /// <param name="subMeshRange">Optional range inside the buffer that represents the mesh to compute bounds for.</param>
    public override void ComputeFromIndexedPoints(ReadOnlySpan<Vector3> points, IndexData indices, in SubMeshRange? subMeshRange = null)
    {
      BoundingCapsule.Data data;
      BoundingCapsule.Data.FromIndexedPoints(points, indices, subMeshRange, out data);

      m_centerLine = data.CenterLine;
      m_radius = data.Radius;
      UpdateCorners = true;
    }

    /// <summary>
    /// Determines if the specified point is contained inside the bounding volume.
    /// </summary>
    /// <param name="point">Point to test against.</param>
    /// <returns>Type of containment</returns>
    public override ContainmentType Contains(in Vector3 point)
    {
      float distSquared, segParameter;
      Vector3 ptOnCenterLine;
      GeometricToolsHelper.DistancePointSegment(point, m_centerLine, out ptOnCenterLine, out segParameter, out distSquared);

      if (distSquared <= (m_radius * m_radius))
        return ContainmentType.Inside;

      return ContainmentType.Outside;
    }

    /// <summary>
    /// Determines if the specified segment line is contained inside the bounding volume.
    /// </summary>
    /// <param name="line">Segment to test against.</param>
    /// <returns>Type of containment.</returns>
    public override ContainmentType Contains(in Segment line)
    {
      float distStart, distEnd, segParameter;
      Vector3 ptOnCenterLine;
      GeometricToolsHelper.DistancePointSegment(line.StartPoint, m_centerLine, out ptOnCenterLine, out segParameter, out distStart);
      GeometricToolsHelper.DistancePointSegment(line.EndPoint, m_centerLine, out ptOnCenterLine, out segParameter, out distEnd);

      float radiusSquared = m_radius * m_radius;

      if (distStart <= radiusSquared && distEnd <= radiusSquared)
        return ContainmentType.Inside;

      if (GeometricToolsHelper.IntersectSegmentCapsule(line, m_centerLine, m_radius))
        return ContainmentType.Intersects;

      return ContainmentType.Outside;
    }

    /// <summary>
    /// Determines if the specified triangle is contained inside the bounding volume.
    /// </summary>
    /// <param name="triangle">Triangle to test against.</param>
    /// <returns>Type of containment.</returns>
    public override ContainmentType Contains(in Triangle triangle)
    {
      float distA, distB, distC, segParameter;
      Vector3 ptOnCenterLine;
      GeometricToolsHelper.DistancePointSegment(triangle.PointA, m_centerLine, out ptOnCenterLine, out segParameter, out distA);
      GeometricToolsHelper.DistancePointSegment(triangle.PointB, m_centerLine, out ptOnCenterLine, out segParameter, out distB);
      GeometricToolsHelper.DistancePointSegment(triangle.PointC, m_centerLine, out ptOnCenterLine, out segParameter, out distC);

      float radiusSquared = m_radius * m_radius;

      if (distA <= radiusSquared && distB <= radiusSquared && distC <= radiusSquared)
        return ContainmentType.Inside;

      if (GeometricToolsHelper.IntersectTriangleCapsule(triangle, m_centerLine, m_radius))
        return ContainmentType.Intersects;

      return ContainmentType.Outside;
    }

    /// <summary>
    /// Determines if the specified ellipse intersects with the bounding volume.
    /// </summary>
    /// <param name="ellipse">Ellipse to test against.</param>
    /// <returns>True if the bounding volume intersects with the ellipse, false otherwise.</returns>
    public override ContainmentType Contains(in Ellipse ellipse)
    {
      //TODO
      throw new NotImplementedException();
    }

    /// <summary>
    /// Determines if the specified bounding volume is contained inside the bounding volume.
    /// </summary>
    /// <param name="volume">Bounding volume to test against.</param>
    /// <returns>Type of containment.</returns>
    public override ContainmentType Contains(BoundingVolume? volume)
    {
      if (volume is null)
        return ContainmentType.Outside;

      switch (volume.BoundingType)
      {
        case BoundingType.AxisAlignedBoundingBox:
          {
            BoundingBox other = (volume as BoundingBox)!;
            Vector3 boxCenter = other.Center;
            Vector3 boxExtents = other.Extents;

            return GeometricToolsHelper.CapsuleContainsAABB(m_centerLine, m_radius, boxCenter, boxExtents);
          }
        case BoundingType.Sphere:
          {
            BoundingSphere other = (volume as BoundingSphere)!;
            Vector3 sphereCenter = other.Center;
            float sphereRadius = other.Radius;

            return GeometricToolsHelper.CapsuleContainsSphere(m_centerLine, m_radius, sphereCenter, sphereRadius);
          }
        case BoundingType.Capsule:
          {
            BoundingCapsule other = (volume as BoundingCapsule)!;

            return GeometricToolsHelper.CapsuleContainsCapsule(m_centerLine, m_radius, other.m_centerLine, other.m_radius);
          }
        case BoundingType.OrientedBoundingBox:
          {
            OrientedBoundingBox other = (volume as OrientedBoundingBox)!;
            Vector3 boxCenter = other.Center;
            Triad boxAxes = other.Axes;
            Vector3 boxExtents = other.Extents;

            return GeometricToolsHelper.CapsuleContainsBox(m_centerLine, m_radius, boxCenter, boxAxes.XAxis, boxAxes.YAxis, boxAxes.ZAxis, boxExtents);
          }
        case BoundingType.Frustum:
        case BoundingType.Mesh:
        default:
          {
            return Contains(volume.Corners);
          }
      }
    }

    /// <summary>
    /// Tests if the specified ray intersects with the bounding volume.
    /// </summary>
    /// <param name="ray">Ray to test against.</param>
    /// <returns>True if the bounding volume intersects with the ray, false otherwise.</returns>
    public override bool Intersects(in Ray ray)
    {
      return GeometricToolsHelper.IntersectRayCapsule(ray, m_centerLine, m_radius);
    }

    /// <summary>
    /// Determines if the specified ray intersects with the bounding volume.
    /// </summary>
    /// <param name="ray">Ray to test against.</param>
    /// <param name="result">Bounding intersection result.</param>
    /// <returns>True if the bounding volume intersects with the ray, false otherwise.</returns>
    public override bool Intersects(in Ray ray, out BoundingIntersectionResult result)
    {
      return GeometricToolsHelper.IntersectRayCapsule(ray, m_centerLine, m_radius, out result);
    }

    /// <summary>
    /// Determines if the specified segment line intersects with the bounding volume.
    /// </summary>
    /// <param name="line">Segment line to test against.</param>
    /// <returns>True if the bounding volume intersects with the segment line, false otherweise.</returns>
    public override bool Intersects(in Segment line)
    {
      return GeometricToolsHelper.IntersectSegmentCapsule(line, m_centerLine, m_radius);
    }

    /// <summary>
    /// Determines if the specified segment line intersects with the bounding volume.
    /// </summary>
    /// <param name="line">Segment line to test against.</param>
    /// <param name="result">Bounding intersection result.</param>
    /// <returns>True if the bounding volume intersects with the segment line, false otherweise.</returns>
    public override bool Intersects(in Segment line, out BoundingIntersectionResult result)
    {
      return GeometricToolsHelper.IntersectSegmentCapsule(line, m_centerLine, m_radius, out result);
    }

    /// <summary>
    /// Tests if the specified plane intersects with the bounding volume.
    /// </summary>
    /// <param name="plane">Plane to test against.</param>
    /// <returns>Type of plane intersection.</returns>
    public override PlaneIntersectionType Intersects(in Plane plane)
    {
      float pDist = plane.SignedDistanceTo(m_centerLine.EndPoint);
      float nDist = plane.SignedDistanceTo(m_centerLine.StartPoint);

      if ((pDist * nDist) <= 0.0f)
      {
        //Capsule end points on opposite sides of the plane
        return PlaneIntersectionType.Intersects;
      }

      //Endpoints on same side, but may still overlap
      if (MathF.Abs(pDist) <= m_radius || MathF.Abs(nDist) <= m_radius)
      {
        return PlaneIntersectionType.Intersects;
      }
      //No overlap, so front or back. This is signed distance, so use the Bounding sphere test? > radius == front, doesn't
      //matter which since both are on the same side
      else if (pDist > m_radius)
      {
        return PlaneIntersectionType.Front;
      }
      else
      {
        return PlaneIntersectionType.Back;
      }
    }

    /// <summary>
    /// Determines if the specified bounding volume intersects with the bounding volume.
    /// </summary>
    /// <param name="volume">Other bounding volume to test against.</param>
    /// <returns>True if the two volumes intersect with one another, false otherwise.</returns>
    public override bool Intersects(BoundingVolume? volume)
    {
      if (volume is null)
        return false;

      switch (volume.BoundingType)
      {
        case BoundingType.AxisAlignedBoundingBox:
          {
            BoundingBox other = (volume as BoundingBox)!;
            Vector3 boxCenter = other.Center;
            Vector3 boxExtents = other.Extents;

            return GeometricToolsHelper.IntersectCapsuleAABB(m_centerLine, m_radius, boxCenter, boxExtents);
          }
        case BoundingType.Sphere:
          {
            BoundingSphere other = (volume as BoundingSphere)!;
            Vector3 sphereCenter = other.Center;
            float sphereRadius = other.Radius;

            return GeometricToolsHelper.IntersectCapsuleSphere(m_centerLine, m_radius, sphereCenter, sphereRadius);
          }
        case BoundingType.Capsule:
          {
            BoundingCapsule other = (volume as BoundingCapsule)!;

            return GeometricToolsHelper.IntersectCapsuleCapsule(m_centerLine, m_radius, other.m_centerLine, other.m_radius);
          }
        case BoundingType.OrientedBoundingBox:
          {
            OrientedBoundingBox other = (volume as OrientedBoundingBox)!;
            Vector3 boxCenter = other.Center;
            Triad boxAxes = other.Axes;
            Vector3 boxExtents = other.Extents;

            return GeometricToolsHelper.IntersectCapsuleBox(m_centerLine, m_radius, boxCenter, boxAxes.XAxis, boxAxes.YAxis, boxAxes.ZAxis, boxExtents);
          }
        case BoundingType.Frustum:
        case BoundingType.Mesh:
          {
            return volume.Intersects(this);
          }
        default:
          return IntersectsGeneral(volume);
      }
    }

    /// <summary>
    /// Merges the bounding volume with the specified bounding volume, resulting in a volume that encloses both.
    /// </summary>
    /// <param name="volume">Bounding volume to merge with.</param>
    public override void Merge(BoundingVolume? volume)
    {
      if (volume is null)
        return;

      //Treat capsule as an OBB, treat the others as OBB and merge, then extract capsule from merged OBB
      Vector3 segCenter, segDir;
      float segExtent;
      GeometricToolsHelper.CalculateSegmentProperties(m_centerLine, out segCenter, out segDir, out segExtent);

      OrientedBoundingBox.Data b0;
      b0.Center = segCenter;
      Triad.FromZComplementBasis(segDir, out b0.Axes);
      b0.Extents = new Vector3(m_radius, m_radius, segExtent + m_radius);

      OrientedBoundingBox.Data b1, merged;

      switch (volume.BoundingType)
      {
        case BoundingType.AxisAlignedBoundingBox:
          {
            BoundingBox other = (volume as BoundingBox)!;

            b1.Center = other.Center;
            b1.Axes = Triad.UnitAxes;
            b1.Extents = other.Extents;
          }
          break;
        case BoundingType.Sphere:
          {
            BoundingSphere other = (volume as BoundingSphere)!;
            float sphereRadius = other.Radius;

            b1.Center = other.Center;
            b1.Axes = Triad.UnitAxes;
            b1.Extents = new Vector3(sphereRadius, sphereRadius, sphereRadius);
          }
          break;
        case BoundingType.Capsule:
          {
            BoundingCapsule other = (volume as BoundingCapsule)!;
            Segment centerLine = other.CenterLine;
            float capsuleRadius = other.Radius;

            GeometricToolsHelper.CalculateSegmentProperties(centerLine, out segCenter, out segDir, out segExtent);

            b1.Center = segCenter;
            Triad.FromZComplementBasis(segDir, out b1.Axes);
            b1.Extents = new Vector3(capsuleRadius, capsuleRadius, segExtent + capsuleRadius);
          }
          break;
        case BoundingType.OrientedBoundingBox:
          {
            OrientedBoundingBox other = (volume as OrientedBoundingBox)!;

            b1.Center = other.Center;
            b1.Axes = other.Axes;
            b1.Extents = other.Extents;
          }
          break;
        case BoundingType.Frustum:
        case BoundingType.Mesh:
        default:
          {
            OrientedBoundingBox.Data.FromPoints(volume.Corners, out b1);
          }
          break;
      }

      GeometricToolsHelper.MergeBoxBoxUsingCorners(b0, b1, out merged);
      GeometricToolsHelper.CapsuleFromBox(merged.Center, merged.Axes.XAxis, merged.Axes.YAxis, merged.Axes.ZAxis, merged.Extents, out m_centerLine, out m_radius);
      UpdateCorners = true;
    }

    /// <summary>
    /// Transforms the bounding volume by a Scale-Rotation-Translation (SRT) transform.
    /// </summary>
    /// <param name="scale">Scaling</param>
    /// <param name="rotation">Rotation</param>
    /// <param name="translation">Translation</param>
    public override void Transform(in Vector3 scale, in Quaternion rotation, in Vector3 translation)
    {
      //Scale centerline extent and radius
      float maxScale = MathF.Max(MathF.Abs(scale.X), MathF.Max(MathF.Abs(scale.Y), MathF.Abs(scale.Z)));
      if (!MathHelper.IsEqual(maxScale, 1.0f))
      {
        Segment.Transform(m_centerLine, maxScale, out m_centerLine);
        m_radius = m_radius * maxScale;
      }

      //Transform centerline by Rotation-Translation
      Matrix rotTranslate, temp;
      Matrix.FromQuaternion(rotation, out rotTranslate);
      Matrix.FromTranslation(translation, out temp);
      Matrix.Multiply(rotTranslate, temp, out rotTranslate);

      Segment.Transform(m_centerLine, rotTranslate, out m_centerLine);

      UpdateCorners = true;
    }

    /// <summary>
    /// Tests equality between the bounding volume and the other bounding volume.
    /// </summary>
    /// <param name="other">Other bounding volume</param>
    /// <param name="tolerance">Tolerance</param>
    /// <returns>True if the volume is of same type and same size/shape, false otherwise.</returns>
    public override bool Equals([NotNullWhen(true)] BoundingVolume? other, float tolerance)
    {
      BoundingCapsule? capsule = other as BoundingCapsule;
      if (capsule is not null)
        return m_centerLine.Equals(capsule.m_centerLine, tolerance) && MathHelper.IsEqual(m_radius, capsule.m_radius, tolerance);

      return false;
    }

    /// <summary>
    /// Reads the object data from the input.
    /// </summary>
    /// <param name="input">Savable reader</param>
    public override void Read(ISavableReader input)
    {
      input.Read<Segment>("CenterLine", out m_centerLine);
      m_radius = input.ReadSingle("Radius");
      UpdateCorners = true;
    }

    /// <summary>
    /// Writes the object data to the output.
    /// </summary>
    /// <param name="output">Savable writer</param>
    public override void Write(ISavableWriter output)
    {
      output.Write<Segment>("CenterLine", m_centerLine);
      output.Write("Radius", m_radius);
    }

    /// <summary>
    /// Computes the corners that represent the extremal points of this bounding volume.
    /// </summary>
    /// <param name="corners">Databuffer to contain the points, length equal to the corner count.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the corner buffer is null.</exception>
    /// <exception cref="System.ArgumentOutOfRangeException">Thrown if the corner buffer does not have enough space.</exception>
    protected override void ComputeCorners(Span<Vector3> corners)
    {
      if (corners.Length < CornerCount)
        throw new ArgumentOutOfRangeException(StringLocalizer.Instance.GetLocalizedString("NotEnoughSpaceForBoundingCorners", CornerCount.ToString()));

      //Axes of the capsule
      Vector3 xAxis, yAxis, zAxis;

      if (m_centerLine.IsDegenerate)
      {
        zAxis = Vector3.UnitZ;
      }
      else
      {
        Vector3.Subtract(m_centerLine.EndPoint, m_centerLine.StartPoint, out zAxis);
      }

      Vector3.ComplementBasis(ref zAxis, out xAxis, out yAxis);

      Vector3.Multiply(xAxis, m_radius, out xAxis);
      Vector3.Multiply(yAxis, m_radius, out yAxis);
      Vector3.Multiply(zAxis, m_radius, out zAxis);

      //Sphere at start point (-R along Z axis)
      ref Vector3 temp = ref corners[0];
      Vector3.Subtract(m_centerLine.StartPoint, xAxis, out temp);
      Vector3.Add(temp, yAxis, out temp);
      Vector3.Subtract(temp, zAxis, out temp);

      temp = ref corners[1];
      Vector3.Add(m_centerLine.StartPoint, xAxis, out temp);
      Vector3.Add(temp, yAxis, out temp);
      Vector3.Subtract(temp, zAxis, out temp);

      temp = ref corners[2];
      Vector3.Add(m_centerLine.StartPoint, xAxis, out temp);
      Vector3.Subtract(temp, yAxis, out temp);
      Vector3.Subtract(temp, zAxis, out temp);

      temp = ref corners[3];
      Vector3.Subtract(m_centerLine.StartPoint, xAxis, out temp);
      Vector3.Subtract(temp, yAxis, out temp);
      Vector3.Subtract(temp, zAxis, out temp);

      //Sphere at end point (+R along Z axis)
      temp = ref corners[4];
      Vector3.Subtract(m_centerLine.EndPoint, xAxis, out temp);
      Vector3.Add(temp, yAxis, out temp);
      Vector3.Add(temp, zAxis, out temp);

      temp = ref corners[5];
      Vector3.Add(m_centerLine.EndPoint, xAxis, out temp);
      Vector3.Add(temp, yAxis, out temp);
      Vector3.Add(temp, zAxis, out temp);

      temp = ref corners[6];
      Vector3.Add(m_centerLine.EndPoint, xAxis, out temp);
      Vector3.Subtract(temp, yAxis, out temp);
      Vector3.Add(temp, zAxis, out temp);

      temp = ref corners[7];
      Vector3.Subtract(m_centerLine.EndPoint, xAxis, out temp);
      Vector3.Subtract(temp, yAxis, out temp);
      Vector3.Add(temp, zAxis, out temp);
    }

    #region BoundingCapsule Data

    /// <summary>
    /// Represents data for a <see cref="BoundingCapsule"/>.
    /// </summary>
    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 4)]
    public struct Data : IEquatable<BoundingCapsule.Data>, IRefEquatable<BoundingCapsule.Data>, IFormattable, IPrimitiveValue
    {
      /// <summary>
      /// Center line segment of the bounding capsule.
      /// </summary>
      public Segment CenterLine;

      /// <summary>
      /// Radius of the bounding capsule.
      /// </summary>
      public float Radius;

      /// <summary>
      /// Constructs a new instance of the <see cref="BoundingCapsule.Data"/> struct.
      /// </summary>
      /// <param name="centerLine">Center line segment of the bounding capsule.</param>
      /// <param name="radius">Radius of the bounding capsule.</param>
      public Data(in Segment centerLine, float radius)
      {
        CenterLine = centerLine;
        Radius = radius;
      }

      /// <summary>
      /// Constructs a new instance of the <see cref="BoundingCapsule.Data"/> struct.
      /// </summary>
      /// <param name="capsule">Bounding capsule to copy from.</param>
      public Data(BoundingCapsule capsule)
      {
        ArgumentNullException.ThrowIfNull(capsule, nameof(capsule));

        CenterLine = capsule.m_centerLine;
        Radius = capsule.m_radius;
      }

      /// <summary>
      /// Computes a minimum bounding volume that encloses the specified points in space.
      /// </summary>
      /// <param name="points">Points in space</param>
      /// <param name="subMeshRange">Optional range inside the buffer that represents the mesh to compute bounds for. Note: the base vertex offset will be ignored.</param>
      /// <returns>Computed bounding capsule.</returns>
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static BoundingCapsule.Data FromPoints(ReadOnlySpan<Vector3> points, in SubMeshRange? subMeshRange = null)
      {
        BoundingCapsule.Data result;
        FromPoints(points, subMeshRange, out result);

        return result;
      }

      /// <summary>
      /// Computes a minimum bounding volume that encloses the specified points in space.
      /// </summary>
      /// <param name="points">Points in space</param>
      /// <param name="result">Computed bounding capsule</param>
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static void FromPoints(ReadOnlySpan<Vector3> points, out BoundingCapsule.Data result)
      {
        FromPoints(points, null, out result);
      }

      /// <summary>
      /// Computes a minimum bounding volume that encloses the specified points in space.
      /// </summary>
      /// <param name="points">Points in space</param>
      /// <param name="subMeshRange">Optional range inside the buffer that represents the mesh to compute bounds for. Note: the base vertex offset will be ignored.</param>
      /// <param name="result">Computed bounding capsule</param>
      public static void FromPoints(ReadOnlySpan<Vector3> points, in SubMeshRange? subMeshRange, out BoundingCapsule.Data result)
      {
        int offset = 0;
        int count = 0;

        if (!BoundingVolume.ExtractSubMeshRange(points, subMeshRange, out offset, out count))
        {
          result.CenterLine = new Segment(Vector3.Zero, Vector3.Zero);
          result.Radius = 0.0f;

          return;
        }

        //Trivial case
        if (count == 1)
        {
          ref readonly Vector3 pt = ref points[offset];
          result.CenterLine = new Segment(pt, pt);
          result.Radius = 0.0f;
          return;
        }

        Vector3 lineOrigin, lineDir;
        GeometricToolsHelper.OrthogonalLineFit(points.Slice(offset, count), out lineOrigin, out lineDir);

        int upperBoundExclusive = offset + count;
        float maxRadiusSqr = 0.0f;
        for (int i = offset; i < upperBoundExclusive; i++)
        {
          ref readonly Vector3 pt = ref points[i];
          float rSqr, lineParameter;
          Vector3 ptOnLine;
          GeometricToolsHelper.DistancePointLine(pt, lineOrigin, lineDir, out ptOnLine, out lineParameter, out rSqr);

          if (rSqr > maxRadiusSqr)
            maxRadiusSqr = rSqr;
        }

        Vector3 U, V, W;
        W = lineDir;

        Vector3.ComplementBasis(ref W, out U, out V);

        float minValue = float.MaxValue;
        float maxValue = float.MinValue;
        for (int i = offset; i < upperBoundExclusive; i++)
        {
          ref readonly Vector3 pt = ref points[i];
          Vector3 diff;
          Vector3.Subtract(pt, lineOrigin, out diff);

          float UdDiff = Vector3.Dot(U, diff);
          float VdDiff = Vector3.Dot(V, diff);
          float WdDiff = Vector3.Dot(W, diff);

          float discr = maxRadiusSqr - ((UdDiff * UdDiff) + (VdDiff * VdDiff));
          float radical = MathF.Sqrt(MathF.Abs(discr));

          float test = WdDiff + radical;
          if (test < minValue)
            minValue = test;

          test = WdDiff - radical;
          if (test > maxValue)
            maxValue = test;
        }

        Vector3 center;
        Vector3.Multiply(lineDir, 0.5f * (minValue + maxValue), out center);
        Vector3.Add(center, lineOrigin, out center);

        //max > min, is a capsule, otherwise degenerates into a sphere
        float extent = (maxValue > minValue) ? (0.5f * (maxValue - minValue)) : 0.0f;

        result.Radius = MathF.Sqrt(maxRadiusSqr);
        Segment.FromCenterExtent(center, lineDir, extent, out result.CenterLine);
      }

      /// <summary>
      /// Computes a minimum bounding volume that encloses the specified indexed points in space.
      /// </summary>
      /// <param name="points">Points in space.</param>
      /// <param name="indices">Point indices denoting location in the point buffer.</param>
      /// <param name="subMeshRange">Optional range inside the buffer that represents the mesh to compute bounds for.</param>
      /// <returns>Computed bounding capsule.</returns>
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static BoundingCapsule.Data FromIndexedPoints(ReadOnlySpan<Vector3> points, IndexData indices, in SubMeshRange? subMeshRange = null)
      {
        BoundingCapsule.Data result;
        FromIndexedPoints(points, indices, subMeshRange, out result);

        return result;
      }

      /// <summary>
      /// Computes a minimum bounding volume that encloses the specified indexed points in space.
      /// </summary>
      /// <param name="points">Points in space.</param>
      /// <param name="indices">Point indices denoting location in the point buffer.</param>
      /// <param name="result">Computed bounding capsule.</param>
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static void FromIndexedPoints(ReadOnlySpan<Vector3> points, IndexData indices, out BoundingCapsule.Data result)
      {
        FromIndexedPoints(points, indices, null, out result);
      }

      /// <summary>
      /// Computes a minimum bounding volume that encloses the specified indexed points in space.
      /// </summary>
      /// <param name="points">Points in space.</param>
      /// <param name="subMeshRange">Optional range inside the buffer that represents the mesh to compute bounds for.</param>
      /// <param name="indices">Point indices denoting location in the point buffer.</param>
      /// <param name="result">Computed bounding capsule.</param>
      public static void FromIndexedPoints(ReadOnlySpan<Vector3> points, IndexData indices, in SubMeshRange? subMeshRange, out BoundingCapsule.Data result)
      {
        int offset = 0;
        int count = 0;
        int baseVertexOffset = 0;

        if (!BoundingVolume.ExtractSubMeshRange(points, indices, subMeshRange, out offset, out count, out baseVertexOffset))
        {
          result.CenterLine = new Segment(Vector3.Zero, Vector3.Zero);
          result.Radius = 0.0f;

          return;
        }

        //Trivial case
        if (count == 1)
        {
          ref readonly Vector3 pt = ref points[indices[offset] + baseVertexOffset];
          result.CenterLine = new Segment(pt, pt);
          result.Radius = 0.0f;
          return;
        }

        Vector3 lineOrigin, lineDir;
        GeometricToolsHelper.OrthogonalLineFit(points, indices, offset, count, baseVertexOffset, out lineOrigin, out lineDir);

        int upperBoundExclusive = offset + count;
        float maxRadiusSqr = 0.0f;
        for (int i = offset; i < upperBoundExclusive; i++)
        {
          int index = indices[i];
          ref readonly Vector3 pt = ref points[index + baseVertexOffset];
          float rSqr, lineParameter;
          Vector3 ptOnLine;
          GeometricToolsHelper.DistancePointLine(pt, lineOrigin, lineDir, out ptOnLine, out lineParameter, out rSqr);

          if (rSqr > maxRadiusSqr)
            maxRadiusSqr = rSqr;
        }

        Vector3 U, V, W;
        W = lineDir;

        Vector3.ComplementBasis(ref W, out U, out V);

        float minValue = float.MaxValue;
        float maxValue = float.MinValue;
        for (int i = offset; i < upperBoundExclusive; i++)
        {
          int index = indices[i];
          ref readonly Vector3 pt = ref points[index + baseVertexOffset];
          Vector3 diff;
          Vector3.Subtract(pt, lineOrigin, out diff);

          float UdDiff = Vector3.Dot(U, diff);
          float VdDiff = Vector3.Dot(V, diff);
          float WdDiff = Vector3.Dot(W, diff);

          float discr = maxRadiusSqr - ((UdDiff * UdDiff) + (VdDiff * VdDiff));
          float radical = MathF.Sqrt(MathF.Abs(discr));

          float test = WdDiff + radical;
          if (test < minValue)
            minValue = test;

          test = WdDiff - radical;
          if (test > maxValue)
            maxValue = test;
        }

        Vector3 center;
        Vector3.Multiply(lineDir, 0.5f * (minValue + maxValue), out center);
        Vector3.Add(center, lineOrigin, out center);

        //max > min, is a capsule, otherwise degenerates into a sphere
        float extent = (maxValue > minValue) ? (0.5f * (maxValue - minValue)) : 0.0f;

        result.Radius = MathF.Sqrt(maxRadiusSqr);
        Segment.FromCenterExtent(center, lineDir, extent, out result.CenterLine);
      }

      /// <summary>
      /// Determines whether the specified <see cref="System.Object" /> is equal to this instance.
      /// </summary>
      /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
      /// <returns>True if the specified <see cref="System.Object" /> is equal to this instance; otherwise, false.</returns>
      public readonly override bool Equals([NotNullWhen(true)] object? obj)
      {
        if (obj is BoundingCapsule.Data)
          return Equals((BoundingCapsule.Data)obj);

        return false;
      }

      /// <summary>
      /// Tests equality between this bounding capsule and another.
      /// </summary>
      /// <param name="other">Bounding capsule</param>
      /// <returns>True if equal, false otherwise.</returns>
      readonly bool IEquatable<BoundingCapsule.Data>.Equals(BoundingCapsule.Data other)
      {
        return CenterLine.Equals(other.CenterLine) && MathHelper.IsEqual(Radius, other.Radius);
      }

      /// <summary>
      /// Tests equality between this bounding capsule and another.
      /// </summary>
      /// <param name="other">Bounding capsule</param>
      /// <returns>True if equal, false otherwise.</returns>
      public readonly bool Equals(in BoundingCapsule.Data other)
      {
        return CenterLine.Equals(other.CenterLine) && MathHelper.IsEqual(Radius, other.Radius);
      }

      /// <summary>
      /// Tests equality between this bounding capsule and another.
      /// </summary>
      /// <param name="other">Bounding capsule</param>
      /// <param name="tolerance">Tolerance</param>
      /// <returns>True if equal, false otherwise.</returns>
      public readonly bool Equals(in BoundingCapsule.Data other, float tolerance)
      {
        return CenterLine.Equals(other.CenterLine, tolerance) && MathHelper.IsEqual(Radius, other.Radius, tolerance);
      }

      /// <summary>
      /// Returns a hash code for this instance.
      /// </summary>
      /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.</returns>
      public readonly override int GetHashCode()
      {
        unchecked
        {
          return CenterLine.GetHashCode() + Radius.GetHashCode();
        }
      }

      /// <summary>
      /// Returns a <see cref="System.String" /> that represents this instance.
      /// </summary>
      /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
      public readonly override string ToString()
      {
        CultureInfo info = CultureInfo.CurrentCulture;
        return String.Format(info, "CenterLine: {0}, Radius: {1}",
            new Object[] { CenterLine.ToString(info), Radius.ToString(info) });
      }

      /// <summary>
      /// Returns a <see cref="System.String" /> that represents this instance.
      /// </summary>
      /// <param name="format">The format</param>
      /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
      public readonly string ToString(string? format)
      {
        if (format is null)
          return ToString();

        CultureInfo info = CultureInfo.CurrentCulture;
        return String.Format(info, "CenterLine: {0}, Radius: {1}",
            new Object[] { CenterLine.ToString(format, info), Radius.ToString(format, info) });
      }

      /// <summary>
      /// Returns a <see cref="System.String" /> that represents this instance.
      /// </summary>
      /// <param name="formatProvider">The format provider.</param>
      /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
      public readonly string ToString(IFormatProvider? formatProvider)
      {
        if (formatProvider is null)
          return ToString();

        return String.Format(formatProvider, "CenterLine: {0}, Radius: {1}",
            new Object[] { CenterLine.ToString(formatProvider), Radius.ToString(formatProvider) });
      }

      /// <summary>
      /// Returns a <see cref="System.String" /> that represents this instance.
      /// </summary>
      /// <param name="format">The format.</param>
      /// <param name="formatProvider">The format provider.</param>
      /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
      public readonly string ToString(string? format, IFormatProvider? formatProvider)
      {
        if (formatProvider is null)
          return ToString();

        if (format is null)
          return ToString(formatProvider);

        return String.Format(formatProvider, "CenterLine: {0}, Radius: {1}",
            new Object[] { CenterLine.ToString(format, formatProvider), Radius.ToString(format, formatProvider) });
      }

      /// <summary>
      /// Writes the primitive data to the output.
      /// </summary>
      /// <param name="output">Primitive writer.</param>
      readonly void IPrimitiveValue.Write(IPrimitiveWriter output)
      {
        output.Write<Segment>("CenterLine", CenterLine);
        output.Write("Radius", Radius);
      }

      /// <summary>
      /// Reads the primitive data from the input.
      /// </summary>
      /// <param name="input">Primitive reader.</param>
      void IPrimitiveValue.Read(IPrimitiveReader input)
      {
        input.Read<Segment>("CenterLine", out CenterLine);
        Radius = input.ReadSingle("Radius");
      }

    }

    #endregion
  }
}
