﻿/*
* Copyright (c) 2010-2024 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Tesla.Content;
using Tesla.Design;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Defines a three dimensional vector where each component is a 32-bit float.
  /// </summary>
  [Serializable]
  [StructLayout(LayoutKind.Sequential, Pack = 4)]
  [TypeConverter(typeof(Vector3TypeConverter))]
  public struct Vector3 : IEquatable<Vector3>, IRefEquatable<Vector3>, IFormattable, IPrimitiveValue
  {
    /// <summary>
    /// X component of the vector.
    /// </summary>
    public float X;

    /// <summary>
    /// Y component of the vector.
    /// </summary>
    public float Y;

    /// <summary>
    /// Z component of the vector.
    /// </summary>
    public float Z;

    private static readonly Vector3 s_zero = new Vector3(0f, 0f, 0f);
    private static readonly Vector3 s_one = new Vector3(1f, 1f, 1f);
    private static readonly Vector3 s_unitX = new Vector3(1f, 0f, 0f);
    private static readonly Vector3 s_unitY = new Vector3(0f, 1f, 0f);
    private static readonly Vector3 s_unitZ = new Vector3(0f, 0f, 1f);

    private static readonly Vector3 s_up = new Vector3(0f, 1f, 0f);
    private static readonly Vector3 s_down = new Vector3(0f, -1f, 0f);
    private static readonly Vector3 s_left = new Vector3(-1f, 0f, 0f);
    private static readonly Vector3 s_right = new Vector3(1f, 0f, 0f);
    private static readonly Vector3 s_forward = new Vector3(0f, 0f, -1f);
    private static readonly Vector3 s_backward = new Vector3(0f, 0f, 1f);

    private static readonly int s_sizeInbytes = BufferHelper.SizeOf<Vector3>();

    /// <summary>
    /// Gets a <see cref="Vector3"/> set to (0, 0, 0).
    /// </summary>
    public static ref readonly Vector3 Zero
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_zero;
      }
    }

    /// <summary>
    /// Gets a <see cref="Vector3"/> set to (1, 1, 1).
    /// </summary>
    public static ref readonly Vector3 One
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_one;
      }
    }

    /// <summary>
    /// Gets a unit <see cref="Vector3"/> set to (1, 0, 0).
    /// </summary>
    public static ref readonly Vector3 UnitX
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_unitX;
      }
    }

    /// <summary>
    /// Gets a unit <see cref="Vector3"/> set to (0, 1, 0).
    /// </summary>
    public static ref readonly Vector3 UnitY
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_unitY;
      }
    }

    /// <summary>
    /// Gets a unit <see cref="Vector3"/> set to (0, 0, 1).
    /// </summary>
    public static ref readonly Vector3 UnitZ
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_unitZ;
      }
    }

    /// <summary>
    /// Gets the <see cref="Vector3"/> set to (0, 1, 0) designating
    /// "up" in the right-handed coordinate system.
    /// </summary>
    public static ref readonly Vector3 Up
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_up;
      }
    }

    /// <summary>
    /// Gets the <see cref="Vector3"/> set to (0, -1, 0) designating
    /// "down" in the right-handed coordinate system.
    /// </summary>
    public static ref readonly Vector3 Down
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_down;
      }
    }

    /// <summary>
    /// Gets the <see cref="Vector3"/> set to (-1, 0, 0) designating
    /// "left" in the right-handed coordinate system.
    /// </summary>
    public static ref readonly Vector3 Left
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_left;
      }
    }

    /// <summary>
    /// Gets the <see cref="Vector3"/> set to (1, 0, 0) designating
    /// "right" in the right-handed coordinate system.
    /// </summary>
    public static ref readonly Vector3 Right
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_right;
      }
    }

    /// <summary>
    /// Gets the <see cref="Vector3"/> set to (0, 0, -1) designating
    /// "forward" in the right-handed coordinate system.
    /// </summary>
    public static ref readonly Vector3 Forward
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_forward;
      }
    }

    /// <summary>
    /// Gets the <see cref="Vector3"/> set to (0, 0, 1) designating
    /// "backward" in the right-handed coordinate system.
    /// </summary>
    public static ref readonly Vector3 Backward
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_backward;
      }
    }

    /// <summary>
    /// Gets the size of the <see cref="Vector3"/> type in bytes.
    /// </summary>
    public static int SizeInBytes
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return s_sizeInbytes;
      }
    }

    /// <summary>
    /// Gets whether the vector is normalized or not.
    /// </summary>
    public bool IsNormalized
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return MathF.Abs(((X * X) + (Y * Y) + (Z * Z)) - 1.0f) <= MathHelper.ZeroTolerance;
      }
    }

    /// <summary>
    /// Gets whether any of the components of the vector are NaN (Not A Number).
    /// </summary>
    public bool IsNaN
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return float.IsNaN(X) || float.IsNaN(Y) || float.IsNaN(Z);
      }
    }

    /// <summary>
    /// Gets whether any of the components of the vector are positive or negative infinity.
    /// </summary>
    public bool IsInfinity
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return float.IsInfinity(X) || float.IsInfinity(Y) || float.IsInfinity(Z);
      }
    }

    /// <summary>
    /// Gets or sets individual components of the vector in the order that the components are declared (XYZ).
    /// </summary>
    /// <param name="index">Zero-based index.</param>
    /// <returns>The value of the specified component.</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the index is out of range [0, 2]</exception>
    public float this[int index]
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        switch (index)
        {
          case 0:
            return X;
          case 1:
            return Y;
          case 2:
            return Z;
          default:
            throw new ArgumentOutOfRangeException(nameof(index), StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));
        }
      }
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      set
      {
        switch (index)
        {
          case 0:
            X = value;
            break;
          case 1:
            Y = value;
            break;
          case 2:
            Z = value;
            break;
          default:
            throw new ArgumentOutOfRangeException(nameof(index), StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));
        }
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Vector3"/> struct.
    /// </summary>
    /// <param name="value">Value to initialize each component to.</param>
    public Vector3(float value)
    {
      X = Y = Z = value;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Vector3"/> struct.
    /// </summary>
    /// <param name="x">X component.</param>
    /// <param name="y">Y component.</param>
    /// <param name="z">Z component.</param>
    public Vector3(float x, float y, float z)
    {
      X = x;
      Y = y;
      Z = z;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Vector3"/> struct.
    /// </summary>
    /// <param name="xy">Vector containing XY components.</param>
    /// <param name="z">Optional Z component, by default zero.</param>
    public Vector3(in Vector2 xy, float z = 0)
    {
      X = xy.X;
      Y = xy.Y;
      Z = z;
    }


    /// <summary>
    /// Constructs a new instance of the <see cref="Vector3"/> struct.
    /// </summary>
    /// <param name="xyz">Convert from <see cref="Vector4"/>, only taking the XYZ values.</param>
    public Vector3(in Vector4 xyz)
    {
      X = xyz.X;
      Y = xyz.Y;
      Z = xyz.Z;
    }


    /// <summary>
    /// Compute the acute angle between two vectors in the range of [0, PI / 2]. Assumes that both vectors are already normalized.
    /// </summary>
    /// <param name="a">First unit vector</param>
    /// <param name="b">Second unit vector</param>
    /// <returns>Acute angle between the vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Angle AcuteAngleBetween(in Vector3 a, in Vector3 b)
    {
      //Acos(dotProduct)
      Angle angle = new Angle(MathF.Acos(MathHelper.Clamp((a.X * b.X) + (a.Y * b.Y) + (a.Z * b.Z), -1.0f, 1.0f)));

      if (angle.Radians > MathHelper.PiOverTwo)
        angle.Radians = MathHelper.Pi - angle.Radians;

      return angle;
    }

    /// <summary>
    /// Compute the angle between two vectors in the range of [0, PI]. Assumes that both vectors are already normalized.
    /// </summary>
    /// <param name="a">First unit vector</param>
    /// <param name="b">Second unit vector</param>
    /// <returns>Angle between the vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Angle AngleBetween(in Vector3 a, in Vector3 b)
    {
      //Acos(dotProduct)
      return new Angle(MathF.Acos(MathHelper.Clamp((a.X * b.X) + (a.Y * b.Y) + (a.Z * b.Z), -1.0f, 1.0f)));
    }

    /// <summary>
    /// Computes a signed angle between two vectors in the range of [-PI, PI]. This uses a normal to the plane that the source/destination vectors both lie 
    /// on in order to determine orientation. The signed angle is found by taking the cross product of the two vectors [source X dest] and 
    /// computing the dot product with the orientation vector. Assumes that all three vectors are already normalized.
    /// </summary>
    /// <param name="source">Start unit vector</param>
    /// <param name="dest">Destination unit vector</param>
    /// <param name="planeNormal">Normal of the plane that the source and destination vectors both lie upon.</param>
    /// <returns>Signed angle between the vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Angle SignedAngleBetween(in Vector3 source, in Vector3 dest, in Vector3 planeNormal)
    {
      //Acos(dotProduct)
      float angleBetween = MathF.Acos(MathHelper.Clamp((source.X * dest.X) + (source.Y * dest.Y) + (source.Z * dest.Z), -1.0f, 1.0f));

      Vector3 cross;
      Vector3.Cross(source, dest, out cross);

      float dot = Vector3.Dot(cross, planeNormal);

      //Cross product should either be pointing in same direction as planeNormal or in opposite. If opposite, it will be negative
      return new Angle((dot < 0.0f) ? -angleBetween : angleBetween);
    }

    /// <summary>
    /// Computes a signed acute angle between two vectors in the range of [-PI / 2, PI / 2]. This uses a normal to the plane that the source/destination vectors both lie 
    /// on in order to determine orientation. The signed angle is found by taking the cross product of the two vectors [source X dest] and 
    /// computing the dot product with the orientation vector. Assumes that all three vectors are already normalized.
    /// </summary>
    /// <param name="source">Start unit vector</param>
    /// <param name="dest">Destination unit vector</param>
    /// <param name="planeNormal">Normal of the plane that the source and destination vectors both lie upon.</param>
    /// <returns>Signed angle between the vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Angle SignedAcuteAngleBetween(in Vector3 source, in Vector3 dest, in Vector3 planeNormal)
    {
      //Acos(dotProduct)
      float angleBetween = MathF.Acos(MathHelper.Clamp((source.X * dest.X) + (source.Y * dest.Y) + (source.Z * dest.Z), -1.0f, 1.0f));

      if (angleBetween > MathHelper.PiOverTwo)
        angleBetween = MathHelper.Pi - angleBetween;
      
      Vector3 cross;
      Vector3.Cross(source, dest, out cross);

      float dot = Vector3.Dot(cross, planeNormal);

      //Cross product should either be pointing in same direction as planeNormal or in opposite. If opposite, it will be negative
      return new Angle((dot < 0.0f) ? -angleBetween : angleBetween);
    }

    /// <summary>
    /// Adds two vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Sum of the two vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector3 Add(in Vector3 a, in Vector3 b)
    {
      Vector3 result;
      result.X = a.X + b.X;
      result.Y = a.Y + b.Y;
      result.Z = a.Z + b.Z;

      return result;
    }

    /// <summary>
    /// Adds two vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <param name="result">Sum of the two vectors</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Add(in Vector3 a, in Vector3 b, out Vector3 result)
    {
      result.X = a.X + b.X;
      result.Y = a.Y + b.Y;
      result.Z = a.Z + b.Z;
    }

    /// <summary>
    /// Subtracts vector b from vector a.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Difference of the two vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector3 Subtract(in Vector3 a, in Vector3 b)
    {
      Vector3 result;
      result.X = a.X - b.X;
      result.Y = a.Y - b.Y;
      result.Z = a.Z - b.Z;

      return result;
    }

    /// <summary>
    /// Subtracts vector b from vector a.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <param name="result">Difference of the two vectors</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Subtract(in Vector3 a, in Vector3 b, out Vector3 result)
    {
      result.X = a.X - b.X;
      result.Y = a.Y - b.Y;
      result.Z = a.Z - b.Z;
    }

    /// <summary>
    /// Multiplies components of two vectors together.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Multiplied vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector3 Multiply(in Vector3 a, in Vector3 b)
    {
      Vector3 result;
      result.X = a.X * b.X;
      result.Y = a.Y * b.Y;
      result.Z = a.Z * b.Z;

      return result;
    }

    /// <summary>
    /// Multiplies components of two vectors together.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <param name="result">Multiplied vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Multiply(in Vector3 a, in Vector3 b, out Vector3 result)
    {
      result.X = a.X * b.X;
      result.Y = a.Y * b.Y;
      result.Z = a.Z * b.Z;
    }

    /// <summary>
    /// Multiplies components of a vector by a scalar value.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="scale">Amount to scale</param>
    /// <returns>Scaled vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector3 Multiply(in Vector3 value, float scale)
    {
      Vector3 result;
      result.X = value.X * scale;
      result.Y = value.Y * scale;
      result.Z = value.Z * scale;

      return result;
    }

    /// <summary>
    /// Multiplies components of a vector by a scalar value.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="scale">Amount to scale</param>
    /// <param name="result">Scaled vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Multiply(in Vector3 value, float scale, out Vector3 result)
    {
      result.X = value.X * scale;
      result.Y = value.Y * scale;
      result.Z = value.Z * scale;
    }

    /// <summary>
    /// Multiplies the matrix and the vector. The vector is treated
    /// as a column vector, so the multiplication is M*v.
    /// </summary>
    /// <param name="m">Matrix to multiply.</param>
    /// <param name="value">Vector to multiply.</param>
    /// <returns>Resulting vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector3 Multiply(in Matrix m, in Vector3 value)
    {
      float x = (m.M11 * value.X) + (m.M12 * value.Y) + (m.M13 * value.Z);
      float y = (m.M21 * value.X) + (m.M22 * value.Y) + (m.M23 * value.Z);
      float z = (m.M31 * value.X) + (m.M32 * value.Y) + (m.M33 * value.Z);

      Vector3 result;
      result.X = x;
      result.Y = y;
      result.Z = z;

      return result;
    }

    /// <summary>
    /// Multiplies the matrix and the vector. The vector is treated
    /// as a column vector, so the multiplication is M*v.
    /// </summary>
    /// <param name="m">Matrix to multiply.</param>
    /// <param name="value">Vector to multiply.</param>
    /// <param name="result">Resulting vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Multiply(in Matrix m, in Vector3 value, out Vector3 result)
    {
      float x = (m.M11 * value.X) + (m.M12 * value.Y) + (m.M13 * value.Z);
      float y = (m.M21 * value.X) + (m.M22 * value.Y) + (m.M23 * value.Z);
      float z = (m.M31 * value.X) + (m.M32 * value.Y) + (m.M33 * value.Z);

      result.X = x;
      result.Y = y;
      result.Z = z;
    }

    /// <summary>
    /// Divides the components of vector a by those of vector b.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Divisor vector</param>
    /// <returns>Quotient of the two vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector3 Divide(in Vector3 a, in Vector3 b)
    {
      Vector3 result;
      result.X = a.X / b.X;
      result.Y = a.Y / b.Y;
      result.Z = a.Z / b.Z;

      return result;
    }

    /// <summary>
    /// Divides the components of vector a by those of vector b.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Divisor vector</param>
    /// <param name="result">Quotient of the two vectors</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Divide(in Vector3 a, in Vector3 b, out Vector3 result)
    {
      result.X = a.X / b.X;
      result.Y = a.Y / b.Y;
      result.Z = a.Z / b.Z;
    }

    /// <summary>
    /// Divides the components of a vector by a scalar.
    /// </summary>
    /// <param name="value">First vector</param>
    /// <param name="divisor">Divisor scalar</param>
    /// <returns>Divided vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector3 Divide(in Vector3 value, float divisor)
    {
      float invDiv = 1.0f / divisor;
      Vector3 result;
      result.X = value.X * invDiv;
      result.Y = value.Y * invDiv;
      result.Z = value.Z * invDiv;

      return result;
    }

    /// <summary>
    /// Divides the components of a vector by a scalar.
    /// </summary>
    /// <param name="value">First vector</param>
    /// <param name="divisor">Divisor scalar</param>
    /// <param name="result">Divided vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Divide(in Vector3 value, float divisor, out Vector3 result)
    {
      float invDiv = 1.0f / divisor;
      result.X = value.X * invDiv;
      result.Y = value.Y * invDiv;
      result.Z = value.Z * invDiv;
    }

    /// <summary>
    /// Flips the signs of the components of the specified vector.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <returns>Negated vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector3 Negate(in Vector3 value)
    {
      Vector3 result;
      result.X = -value.X;
      result.Y = -value.Y;
      result.Z = -value.Z;

      return result;
    }

    /// <summary>
    /// Flips the signs of the components of the specified vector.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="result">Negated vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Negate(in Vector3 value, out Vector3 result)
    {
      result.X = -value.X;
      result.Y = -value.Y;
      result.Z = -value.Z;
    }

    /// <summary>
    /// Compute the dot product between two vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Dot product</returns>
    /// <remarks>
    /// <para>The dot product is also known as the inner product, where it is equal to:</para>
    /// <para>Dot = Length(Vector1) * Length(Vector2) * Cos(theta)</para>
    /// <para>where theta is the angle between the two vectors. When the two vectors are unit vectors, their length is equal to
    /// one, therefore in this case the dot product is equal to the cosine of theta.</para>
    /// <para>When vectors a and b are unit vectors, then the dot product (not accounting for floating point error) can mean the following:</para>
    /// <list type="bullet">
    /// <item>
    /// <description>If dot &gt; 0, the angle between the two vectors is less than 90 degrees.</description>
    /// </item>
    /// <item>
    /// <description>If dot &lt; 0, the angle between the two vectors is greater than 90 degrees.</description>
    /// </item>
    /// <item>
    /// <description>If dot == 0, the angle between the two vectors is 90 degrees (vectors are orthogonal).</description>
    /// </item>
    /// <item>
    /// <description>If dot == 1, the angle between the two vectors is 0 degrees (vectors are parallel and point in the same direction).</description>
    /// </item>
    /// <item>
    /// <description>If dot == -1, the angle between the two vectors is 180 degrees (vectors are parallel and point in opposite directions).</description>
    /// </item>
    /// </list>
    /// </remarks>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float Dot(in Vector3 a, in Vector3 b)
    {
      return (a.X * b.X) + (a.Y * b.Y) + (a.Z * b.Z);
    }

    /// <summary>
    /// Compute the cross product between two vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Cross product</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector3 Cross(in Vector3 a, in Vector3 b)
    {
      float x = (a.Y * b.Z) - (a.Z * b.Y);
      float y = (a.Z * b.X) - (a.X * b.Z);
      float z = (a.X * b.Y) - (a.Y * b.X);

      Vector3 result;
      result.X = x;
      result.Y = y;
      result.Z = z;

      return result;
    }

    /// <summary>
    /// Compute the cross product between two vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <param name="result">Cross product</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Cross(in Vector3 a, in Vector3 b, out Vector3 result)
    {
      float x = (a.Y * b.Z) - (a.Z * b.Y);
      float y = (a.Z * b.X) - (a.X * b.Z);
      float z = (a.X * b.Y) - (a.Y * b.X);

      result.X = x;
      result.Y = y;
      result.Z = z;
    }

    /// <summary>
    /// Compute the cross product between two vectors and normalize the result.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Normalized cross product</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector3 NormalizedCross(in Vector3 a, in Vector3 b)
    {
      float x = (a.Y * b.Z) - (a.Z * b.Y);
      float y = (a.Z * b.X) - (a.X * b.Z);
      float z = (a.X * b.Y) - (a.Y * b.X);

      Vector3 result;
      result.X = x;
      result.Y = y;
      result.Z = z;
      result.Normalize();

      return result;
    }

    /// <summary>
    /// Compute the cross product between two vectors and normalize the result.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <param name="result">Normalized cross product</param>
    /// <returns>The magnitude (length) of the vector.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float NormalizedCross(in Vector3 a, in Vector3 b, out Vector3 result)
    {
      float x = (a.Y * b.Z) - (a.Z * b.Y);
      float y = (a.Z * b.X) - (a.X * b.Z);
      float z = (a.X * b.Y) - (a.Y * b.X);

      result.X = x;
      result.Y = y;
      result.Z = z;

      return result.Normalize();
    }

    /// <summary>
    /// Subtracts vector b from a and normalizes the result.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector.</param>
    /// <returns>Difference of the two vectors, normalized.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector3 NormalizedSubtract(in Vector3 a, in Vector3 b)
    {
      Vector3 result;
      Vector3.Subtract(a, b, out result);
      result.Normalize();

      return result;
    }

    /// <summary>
    /// Subtracts vector b from a and normalizes the result.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector.</param>
    /// <param name="result">Difference of the two vectors, normalized.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void NormalizedSubtract(in Vector3 a, in Vector3 b, out Vector3 result)
    {
      Vector3.Subtract(a, b, out result);
      result.Normalize();
    }

    /// <summary>
    /// Normalize the source vector to a unit vector, which results in a vector with a magnitude of 1 but
    /// preserves the direction the vector was pointing in.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <returns>Normalized unit vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector3 Normalize(in Vector3 value)
    {
      float lengthSquared = (value.X * value.X) + (value.Y * value.Y) + (value.Z * value.Z);

      Vector3 result = value;

      if (lengthSquared > MathHelper.ZeroTolerance)
      {
        float magnitnude = MathF.Sqrt(lengthSquared);

        float invLength = 1.0f / magnitnude;
        result.X *= invLength;
        result.Y *= invLength;
        result.Z *= invLength;
      }

      return result;
    }

    /// <summary>
    /// Normalize the source vector to a unit vector, which results in a vector with a magnitude of 1 but
    /// preserves the direction the vector was pointing in.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="result">Normalized unit vector</param>
    /// <returns>The magnitude (length) of the vector.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float Normalize(in Vector3 value, out Vector3 result)
    {
      float lengthSquared = (value.X * value.X) + (value.Y * value.Y) + (value.Z * value.Z);

      result = value;

      if (lengthSquared > MathHelper.ZeroTolerance)
      {
        float magnitnude = MathF.Sqrt(lengthSquared);

        float invLength = 1.0f / magnitnude;
        result.X *= invLength;
        result.Y *= invLength;
        result.Z *= invLength;

        return magnitnude;
      }

      return 0.0f;
    }

    /// <summary>
    /// Compute the distance between two vectors.
    /// </summary>
    /// <param name="start">First vector</param>
    /// <param name="end">Second vector</param>
    /// <returns>Distance between the vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float Distance(in Vector3 start, in Vector3 end)
    {
      float dx = start.X - end.X;
      float dy = start.Y - end.Y;
      float dz = start.Z - end.Z;
      float distanceSquared = (dx * dx) + (dy * dy) + (dz * dz);

      return MathF.Sqrt(distanceSquared);
    }

    /// <summary>
    /// Compute the XY distance between two vectors.
    /// </summary>
    /// <param name="start">First vector</param>
    /// <param name="end">Second vector</param>
    /// <returns>Distance between the vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float DistanceXY(in Vector3 start, in Vector3 end)
    {
      float dx = start.X - end.X;
      float dy = start.Y - end.Y;
      float distanceSquared = (dx * dx) + (dy * dy);

      return MathF.Sqrt(distanceSquared);
    }

    /// <summary>
    /// Compute the distance squared between two vectors.
    /// </summary>
    /// <param name="start">First vector</param>
    /// <param name="end">Second vector</param>
    /// <returns>Distance squared between the vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float DistanceSquared(in Vector3 start, in Vector3 end)
    {
      float dx = start.X - end.X;
      float dy = start.Y - end.Y;
      float dz = start.Z - end.Z;

      return (dx * dx) + (dy * dy) + (dz * dz);
    }


    /// <summary>
    /// Compute the XY distance squared between two vectors.
    /// </summary>
    /// <param name="start">First vector</param>
    /// <param name="end">Second vector</param>
    /// <returns>Distance squared between the vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float DistanceSquaredXY(in Vector3 start, in Vector3 end)
    {
      float dx = start.X - end.X;
      float dy = start.Y - end.Y;

      return (dx * dx) + (dy * dy);
    }
    /// <summary>
    /// Gets the vector that contains the maximum value from each of the components of the 
    /// two supplied vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Maximum vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector3 Max(in Vector3 a, in Vector3 b)
    {
      Vector3 result;
      result.X = (a.X > b.X) ? a.X : b.X;
      result.Y = (a.Y > b.Y) ? a.Y : b.Y;
      result.Z = (a.Z > b.Z) ? a.Z : b.Z;

      return result;
    }

    /// <summary>
    /// Gets the vector that contains the maximum value from each of the components of the 
    /// two supplied vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <param name="result">Maximum vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Max(in Vector3 a, in Vector3 b, out Vector3 result)
    {
      result.X = (a.X > b.X) ? a.X : b.X;
      result.Y = (a.Y > b.Y) ? a.Y : b.Y;
      result.Z = (a.Z > b.Z) ? a.Z : b.Z;
    }

    /// <summary>
    /// Gets the vector that contains the mininum value from each of the components of the 
    /// two supplied vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Minimum vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector3 Min(in Vector3 a, in Vector3 b)
    {
      Vector3 result;
      result.X = (a.X < b.X) ? a.X : b.X;
      result.Y = (a.Y < b.Y) ? a.Y : b.Y;
      result.Z = (a.Z < b.Z) ? a.Z : b.Z;

      return result;
    }

    /// <summary>
    /// Gets the vector that contains the mininum value from each of the components of the 
    /// two supplied vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <param name="result">Minimum vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Min(in Vector3 a, in Vector3 b, out Vector3 result)
    {
      result.X = (a.X < b.X) ? a.X : b.X;
      result.Y = (a.Y < b.Y) ? a.Y : b.Y;
      result.Z = (a.Z < b.Z) ? a.Z : b.Z;
    }

    /// <summary>
    /// Restricts the source vector in the range of the minimum and maximum vectors.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="min">Minimum vector</param>
    /// <param name="max">Maximum vector</param>
    /// <returns>Clamped vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector3 Clamp(in Vector3 value, in Vector3 min, in Vector3 max)
    {
      float x = value.X;
      float y = value.Y;
      float z = value.Z;

      x = (x > max.X) ? max.X : x;
      x = (x < min.X) ? min.X : x;

      y = (y > max.Y) ? max.Y : y;
      y = (y < min.Y) ? min.Y : y;

      z = (z > max.Z) ? max.Z : z;
      z = (z < min.Z) ? min.Z : z;

      Vector3 result;
      result.X = x;
      result.Y = y;
      result.Z = z;

      return result;
    }

    /// <summary>
    /// Restricts the source vector in the range of the minimum and maximum vectors.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="min">Minimum vector</param>
    /// <param name="max">Maximum vector</param>
    /// <param name="result">Clamped vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Clamp(in Vector3 value, in Vector3 min, in Vector3 max, out Vector3 result)
    {
      float x = value.X;
      float y = value.Y;
      float z = value.Z;

      x = (x > max.X) ? max.X : x;
      x = (x < min.X) ? min.X : x;

      y = (y > max.Y) ? max.Y : y;
      y = (y < min.Y) ? min.Y : y;

      z = (z > max.Z) ? max.Z : z;
      z = (z < min.Z) ? min.Z : z;

      result.X = x;
      result.Y = y;
      result.Z = z;
    }

    /// <summary>
    /// Computes an orthonormal basis from a single vector, which may or may not be normalized.
    /// </summary>
    /// <param name="zAxis">Z axis to form an orthonormal basis to. The vector does not need to be normalized, but will be normalized after the method returns.</param>
    /// <param name="xAxis">X axis orthogonal to Z and Y axes.</param>
    /// <param name="yAxis">Y axis orthogonal to X and Z axes.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void ComplementBasis(ref Vector3 zAxis, out Vector3 xAxis, out Vector3 yAxis)
    {
      if (MathF.Abs(zAxis.X) >= MathF.Abs(zAxis.Y))
      {
        //X or Z of zAxis is largest magnitude component, swap them
        float invLength = 1.0f / MathF.Sqrt(zAxis.X * zAxis.X + zAxis.Z * zAxis.Z);

        xAxis.X = -zAxis.Z * invLength;
        xAxis.Y = 0.0f;
        xAxis.Z = zAxis.X * invLength;

        yAxis.X = zAxis.Y * xAxis.Z;
        yAxis.Y = (zAxis.Z * xAxis.X) - (zAxis.X * xAxis.Z);
        yAxis.Z = -zAxis.Y * xAxis.X;
      }
      else
      {
        //Y or Z is the largest magnitude component, swap them
        float invLength = 1.0f / MathF.Sqrt(zAxis.Y * zAxis.Y + zAxis.Z * zAxis.Z);

        xAxis.X = 0.0f;
        xAxis.Y = zAxis.Z * invLength;
        xAxis.Z = -zAxis.Y * invLength;

        yAxis.X = (zAxis.Y * xAxis.Z) - (zAxis.Z * xAxis.Y);
        yAxis.Y = -zAxis.X * xAxis.Z;
        yAxis.Z = zAxis.X * xAxis.Y;
      }

      //Ensure ALL axes are normalized
      xAxis.Normalize();
      yAxis.Normalize();
      zAxis.Normalize();
    }

    /// <summary>
    /// Computes an orthonormal basis from a single vector, which may or may not be normalized.
    /// </summary>
    /// <param name="zAxis">Z axis to form an orthonormal basis to.</param>
    /// <param name="xAxis">X axis orthogonal to Z and Y axes.</param>
    /// <param name="yAxis">Y axis orthogonal to X and Z axes.</param>
    /// <param name="normalizedZAxis">The input Z axis, normalized.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void ComplementBasis(in Vector3 zAxis, out Vector3 xAxis, out Vector3 yAxis, out Vector3 normalizedZAxis)
    {
      normalizedZAxis = zAxis;

      Vector3.ComplementBasis(ref normalizedZAxis, out xAxis, out yAxis);
    }

    /// <summary>
    /// Returns a <see cref="Vector3"/> containing the 3D Cartesian coordinates of a point specified
    /// in barycentric coordinates relative to a 3D triangle.
    /// </summary>
    /// <param name="a">Vector containing 3D cartesian coordinates corresponding to triangle's first vertex</param>
    /// <param name="b">Vector containing 3D cartesian coordinates corresponding to triangle's second vertex</param>
    /// <param name="c">Vector containing 3D cartesian coordinates corresponding to triangle's third vertex</param>
    /// <param name="s">Barycentric coordinate s that is the weighting factor toward the second vertex</param>
    /// <param name="t">Barycentric coordinate t that is the weighting factor toward the third vertex</param>
    /// <returns>Barycentric coordinates</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector3 Barycentric(in Vector3 a, in Vector3 b, in Vector3 c, float s, float t)
    {
      Vector3 result;
      result.X = (a.X + (s * (b.X - a.X))) + (t * (c.X - a.X));
      result.Y = (a.Y + (s * (b.Y - a.Y))) + (t * (c.Y - a.Y));
      result.Z = (a.Z + (s * (b.Z - a.Z))) + (t * (c.Z - a.Z));

      return result;
    }

    /// <summary>
    /// Returns a <see cref="Vector3"/> containing the 3D Cartesian coordinates of a point specified
    /// in barycentric coordinates relative to a 3D triangle.
    /// </summary>
    /// <param name="a">Vector containing 3D cartesian coordinates corresponding to triangle's first vertex</param>
    /// <param name="b">Vector containing 3D cartesian coordinates corresponding to triangle's second vertex</param>
    /// <param name="c">Vector containing 3D cartesian coordinates corresponding to triangle's third vertex</param>
    /// <param name="s">Barycentric coordinate s that is the weighting factor toward the second vertex</param>
    /// <param name="t">Barycentric coordinate t that is the weighting factor toward the third vertex</param>
    /// <param name="result">Barycentric coordinates</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Barycentric(in Vector3 a, in Vector3 b, in Vector3 c, float s, float t, out Vector3 result)
    {
      result.X = (a.X + (s * (b.X - a.X))) + (t * (c.X - a.X));
      result.Y = (a.Y + (s * (b.Y - a.Y))) + (t * (c.Y - a.Y));
      result.Z = (a.Z + (s * (b.Z - a.Z))) + (t * (c.Z - a.Z));
    }

    /// <summary>
    /// Returns a <see cref="Vector3"/> containing the 3D Cartesian coordinates of a point specified in barycentric coordinates relative to a 3D triangle.
    /// </summary>
    /// <param name="pt">Point on or near the triangle to find the barycentric coordinates of.</param>
    /// <param name="a">Vector containing 3D cartesian coordinates corresponding to triangle's first vertex</param>
    /// <param name="b">Vector containing 3D cartesian coordinates corresponding to triangle's second vertex</param>
    /// <param name="c">Vector containing 3D cartesian coordinates corresponding to triangle's third vertex</param>
    /// <returns>Barycentric coordinates of the point.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector3 Barycentric(in Vector3 pt, in Vector3 a, in Vector3 b, in Vector3 c)
    {
      Vector3 edgeBA, edgeCA, edgePtA;
      Vector3.Subtract(b, a, out edgeBA);
      Vector3.Subtract(c, a, out edgeCA);
      Vector3.Subtract(pt, a, out edgePtA);

      float d00 = Vector3.Dot(edgeBA, edgeBA);
      float d01 = Vector3.Dot(edgeBA, edgeCA);
      float d11 = Vector3.Dot(edgeCA, edgeCA);
      float d20 = Vector3.Dot(edgePtA, edgeBA);
      float d21 = Vector3.Dot(edgePtA, edgeCA);

      float invDenom = 1.0f / ((d00 * d11) - (d01 * d01));

      float v = (d11 * d20 - d01 * d21) * invDenom;
      float w = (d00 * d21 - d01 * d20) * invDenom;
      float u = 1.0f - v - w;

      return new Vector3(u, v, w);
    }

    /// <summary>
    /// Returns a <see cref="Vector3"/> containing the 3D Cartesian coordinates of a point specified in barycentric coordinates relative to a 3D triangle.
    /// </summary>
    /// <param name="pt">Point on or near the triangle to find the barycentric coordinates of.</param>
    /// <param name="a">Vector containing 3D cartesian coordinates corresponding to triangle's first vertex</param>
    /// <param name="b">Vector containing 3D cartesian coordinates corresponding to triangle's second vertex</param>
    /// <param name="c">Vector containing 3D cartesian coordinates corresponding to triangle's third vertex</param>
    /// <param name="result">Barycentric coordinates of the point.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Barycentric(in Vector3 pt, in Vector3 a, in Vector3 b, in Vector3 c, out Vector3 result)
    {
      Vector3 edgeBA, edgeCA, edgePtA;
      Vector3.Subtract(b, a, out edgeBA);
      Vector3.Subtract(c, a, out edgeCA);
      Vector3.Subtract(pt, a, out edgePtA);

      float d00 = Vector3.Dot(edgeBA, edgeBA);
      float d01 = Vector3.Dot(edgeBA, edgeCA);
      float d11 = Vector3.Dot(edgeCA, edgeCA);
      float d20 = Vector3.Dot(edgePtA, edgeBA);
      float d21 = Vector3.Dot(edgePtA, edgeCA);

      float invDenom = 1.0f / ((d00 * d11) - (d01 * d01));

      float v = ((d11 * d20) - (d01 * d21)) * invDenom;
      float w = ((d00 * d21) - (d01 * d20)) * invDenom;
      float u = 1.0f - v - w;

      result.X = u;
      result.Y = v;
      result.Z = w;
    }

    /// <summary>
    /// Compute Catmull-Rom interpolation using the the specified positions.
    /// </summary>
    /// <param name="a">First position</param>
    /// <param name="b">Second position</param>
    /// <param name="c">Third position</param>
    /// <param name="d">Fourth position</param>
    /// <param name="wf">Weighting factor</param>
    /// <returns>Catmull-Rom interpolated vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector3 CatmullRom(in Vector3 a, in Vector3 b, in Vector3 c, in Vector3 d, float wf)
    {
      Vector3 result;
      float wfSquared = wf * wf;
      float wfCubed = wf * wfSquared;
      result.X = 0.5f * ((((2.0f * b.X) + ((-a.X + c.X) * wf)) + (((((2.0f * a.X) - (5.0f * b.X)) + (4.0f * c.X)) - d.X) * wfSquared)) + ((((-a.X + (3.0f * b.X)) - (3.0f * c.X)) + d.X) * wfCubed));
      result.Y = 0.5f * ((((2.0f * b.Y) + ((-a.Y + c.Y) * wf)) + (((((2.0f * a.Y) - (5.0f * b.Y)) + (4.0f * c.Y)) - d.Y) * wfSquared)) + ((((-a.Y + (3.0f * b.Y)) - (3.0f * c.Y)) + d.Y) * wfCubed));
      result.Z = 0.5f * ((((2.0f * b.Z) + ((-a.Z + c.Z) * wf)) + (((((2.0f * a.Z) - (5.0f * b.Z)) + (4.0f * c.Z)) - d.Z) * wfSquared)) + ((((-a.Z + (3.0f * b.Z)) - (3.0f * c.Z)) + d.Z) * wfCubed));

      return result;
    }

    /// <summary>
    /// Compute Catmull-Rom interpolation.
    /// </summary>
    /// <param name="a">First position</param>
    /// <param name="b">Second position</param>
    /// <param name="c">Third position</param>
    /// <param name="d">Fourth position</param>
    /// <param name="wf">Weighting factor</param>
    /// <param name="result">Catmull-Rom interpolated vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void CatmullRom(in Vector3 a, in Vector3 b, in Vector3 c, in Vector3 d, float wf, out Vector3 result)
    {
      float wfSquared = wf * wf;
      float wfCubed = wf * wfSquared;
      result.X = 0.5f * ((((2.0f * b.X) + ((-a.X + c.X) * wf)) + (((((2.0f * a.X) - (5.0f * b.X)) + (4.0f * c.X)) - d.X) * wfSquared)) + ((((-a.X + (3.0f * b.X)) - (3.0f * c.X)) + d.X) * wfCubed));
      result.Y = 0.5f * ((((2.0f * b.Y) + ((-a.Y + c.Y) * wf)) + (((((2.0f * a.Y) - (5.0f * b.Y)) + (4.0f * c.Y)) - d.Y) * wfSquared)) + ((((-a.Y + (3.0f * b.Y)) - (3.0f * c.Y)) + d.Y) * wfCubed));
      result.Z = 0.5f * ((((2.0f * b.Z) + ((-a.Z + c.Z) * wf)) + (((((2.0f * a.Z) - (5.0f * b.Z)) + (4.0f * c.Z)) - d.Z) * wfSquared)) + ((((-a.Z + (3.0f * b.Z)) - (3.0f * c.Z)) + d.Z) * wfCubed));
    }

    /// <summary>
    /// Compute a Hermite spline interpolation.
    /// </summary>
    /// <param name="a">First position</param>
    /// <param name="tangentA">First vector's tangent</param>
    /// <param name="b">Second position</param>
    /// <param name="tangentB">Second vector's tangent</param>
    /// <param name="wf">Weighting factor</param>
    /// <returns>Hermite interpolated vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector3 Hermite(in Vector3 a, in Vector3 tangentA, in Vector3 b, in Vector3 tangentB, float wf)
    {
      float wfSquared = wf * wf;
      float wfCubed = wfSquared * wf;
      float h1 = ((2.0f * wfCubed) - (3.0f * wfSquared)) + 1.0f;
      float h2 = (-2.0f * wfCubed) + (3.0f * wfSquared);
      float h3 = (wfCubed - (2.0f * wfSquared)) + wf;
      float h4 = wfCubed - wfSquared;

      Vector3 result;
      result.X = (((a.X * h1) + (b.X * h2)) + (tangentA.X * h3)) + (tangentB.X * h4);
      result.Y = (((a.Y * h1) + (b.Y * h2)) + (tangentA.Y * h3)) + (tangentB.Y * h4);
      result.Z = (((a.Z * h1) + (b.Z * h2)) + (tangentA.Z * h3)) + (tangentB.Z * h4);

      return result;
    }

    /// <summary>
    /// Compute a Hermite spline interpolation.
    /// </summary>
    /// <param name="a">First position</param>
    /// <param name="tangentA">First vector's tangent</param>
    /// <param name="b">Second position</param>
    /// <param name="tangentB">Second vector's tangent</param>
    /// <param name="wf">Weighting factor</param>
    /// <param name="result">Hermite interpolated vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Hermite(in Vector3 a, in Vector3 tangentA, in Vector3 b, in Vector3 tangentB, float wf, out Vector3 result)
    {
      float wfSquared = wf * wf;
      float wfCubed = wfSquared * wf;
      float h1 = ((2.0f * wfCubed) - (3.0f * wfSquared)) + 1.0f;
      float h2 = (-2.0f * wfCubed) + (3.0f * wfSquared);
      float h3 = (wfCubed - (2.0f * wfSquared)) + wf;
      float h4 = wfCubed - wfSquared;

      result.X = (((a.X * h1) + (b.X * h2)) + (tangentA.X * h3)) + (tangentB.X * h4);
      result.Y = (((a.Y * h1) + (b.Y * h2)) + (tangentA.Y * h3)) + (tangentB.Y * h4);
      result.Z = (((a.Z * h1) + (b.Z * h2)) + (tangentA.Z * h3)) + (tangentB.Z * h4);
    }

    /// <summary>
    /// Compute a cubic interpolation between two vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <param name="wf">Weighting factor (between 0 and 1.0)</param>
    /// <returns>Cubic interpolated vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector3 SmoothStep(in Vector3 a, in Vector3 b, float wf)
    {
      Vector3 result;
      float amt = MathHelper.Clamp(wf, 0.0f, 1.0f);
      amt = (amt * amt) * (3.0f - (2.0f * amt));
      result.X = a.X + ((b.X - a.X) * amt);
      result.Y = a.Y + ((b.Y - a.Y) * amt);
      result.Z = a.Z + ((b.Z - a.Z) * amt);

      return result;
    }

    /// <summary>
    /// Compute a cubic interpolation between two vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <param name="wf">Weighting factor (between 0 and 1.0)</param>
    /// <param name="result">Cubic interpolated vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void SmoothStep(in Vector3 a, in Vector3 b, float wf, out Vector3 result)
    {
      float amt = MathHelper.Clamp(wf, 0.0f, 1.0f);
      amt = (amt * amt) * (3.0f - (2.0f * amt));
      result.X = a.X + ((b.X - a.X) * amt);
      result.Y = a.Y + ((b.Y - a.Y) * amt);
      result.Z = a.Z + ((b.Z - a.Z) * amt);
    }

    /// <summary>
    /// Linearly interpolates between two vectors.
    /// </summary>
    /// <param name="a">Starting vector</param>
    /// <param name="b">Ending vector</param>
    /// <param name="percent">Amount to interpolate by</param>
    /// <returns>Linear interpolated vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector3 Lerp(in Vector3 a, in Vector3 b, float percent)
    {
      // Better for floating point precision than a + (b - a) * percent;
      float oneMinusPercent = 1.0f - percent;

      Vector3 result;
      result.X = a.X * oneMinusPercent + b.X * percent;
      result.Y = a.Y * oneMinusPercent + b.Y * percent;
      result.Z = a.Z * oneMinusPercent + b.Z * percent;

      return result;
    }

    /// <summary>
    /// Linearly interpolates between two vectors.
    /// </summary>
    /// <param name="a">Starting vector</param>
    /// <param name="b">Ending vector</param>
    /// <param name="percent">Amount to interpolate by</param>
    /// <param name="result">Linear interpolated vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Lerp(in Vector3 a, in Vector3 b, float percent, out Vector3 result)
    {
      // Better for floating point precision than a + (b - a) * percent;
      float oneMinusPercent = 1.0f - percent;

      result.X = a.X * oneMinusPercent + b.X * percent;
      result.Y = a.Y * oneMinusPercent + b.Y * percent;
      result.Z = a.Z * oneMinusPercent + b.Z * percent;
    }

    /// <summary>
    /// Compute the reflection vector off a surface with the specified normal.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="normal">Surface normal (unit vector)</param>
    /// <returns>Reflected vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector3 Reflect(in Vector3 value, in Vector3 normal)
    {
      Vector3 result;
      float dot = (value.X * normal.X) + (value.Y * normal.Y) + (value.Z * normal.Z);
      result.X = value.X - ((2.0f * dot) * normal.X);
      result.Y = value.Y - ((2.0f * dot) * normal.Y);
      result.Z = value.Z - ((2.0f * dot) * normal.Z);

      return result;
    }

    /// <summary>
    /// Compute the reflection vector off a surface with the specified normal.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="normal">Surface normal (unit vector)</param>
    /// <param name="result">Reflected vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Reflect(in Vector3 value, in Vector3 normal, out Vector3 result)
    {
      float dot = (value.X * normal.X) + (value.Y * normal.Y) + (value.Z * normal.Z);
      result.X = value.X - ((2.0f * dot) * normal.X);
      result.Y = value.Y - ((2.0f * dot) * normal.Y);
      result.Z = value.Z - ((2.0f * dot) * normal.Z);
    }

    /// <summary>
    /// Transfroms the specified vector by the given <see cref="Matrix"/>. The vector is treated
    /// as a row vector, so the multiplication is v*M.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="m">Transformation matrix</param>
    /// <returns>Transformed vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector3 Transform(in Vector3 value, in Matrix m)
    {
      float x = (value.X * m.M11) + (value.Y * m.M21) + (value.Z * m.M31) + m.M41;
      float y = (value.X * m.M12) + (value.Y * m.M22) + (value.Z * m.M32) + m.M42;
      float z = (value.X * m.M13) + (value.Y * m.M23) + (value.Z * m.M33) + m.M43;

      Vector3 result;
      result.X = x;
      result.Y = y;
      result.Z = z;

      return result;
    }

    /// <summary>
    /// Transfroms the specified vector by the given <see cref="Matrix"/>. The vector is treated
    /// as a row vector, so the multiplication is v*M.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="m">Transformation matrix</param>
    /// <param name="result">Transformed vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Transform(in Vector3 value, in Matrix m, out Vector3 result)
    {
      float x = (value.X * m.M11) + (value.Y * m.M21) + (value.Z * m.M31) + m.M41;
      float y = (value.X * m.M12) + (value.Y * m.M22) + (value.Z * m.M32) + m.M42;
      float z = (value.X * m.M13) + (value.Y * m.M23) + (value.Z * m.M33) + m.M43;

      result.X = x;
      result.Y = y;
      result.Z = z;
    }

    /// <summary>
    /// Transforms an span of vectors by the given <see cref="Matrix"/>. The vector is treated
    /// as a row vector, so the multiplication is v*M.
    /// </summary>
    /// <param name="source">Span of vectors to be transformed</param>
    /// <param name="m">Transformation matrix</param>
    /// <param name="destination">Span to store transformed vectors, this may be the same as the source span</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the destination span is smaller than the source span.</exception>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Transform(ReadOnlySpan<Vector3> source, in Matrix m, Span<Vector3> destination)
    {
      if (destination.Length < source.Length)
        throw new ArgumentOutOfRangeException(nameof(destination), StringLocalizer.Instance.GetLocalizedString("DestinationArraySmallerThanSource"));

      for (int i = 0; i < source.Length; i++)
        Transform(source[i], m, out destination[i]);
    }

    /// <summary>
    /// Transfroms the specified vector by the given <see cref="Quaternion"/>.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="q">Quaternion rotation</param>
    /// <returns>Transformed vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector3 Transform(in Vector3 value, in Quaternion q)
    {
      float x2 = q.X + q.X;
      float y2 = q.Y + q.Y;
      float z2 = q.Z + q.Z;

      float wx2 = q.W * x2;
      float wy2 = q.W * y2;
      float wz2 = q.W * z2;

      float xx2 = q.X * x2;
      float xy2 = q.X * y2;
      float xz2 = q.X * z2;

      float yy2 = q.Y * y2;
      float yz2 = q.Y * z2;

      float zz2 = q.Z * z2;

      float x = ((value.X * ((1.0f - yy2) - zz2)) + (value.Y * (xy2 - wz2))) + (value.Z * (xz2 + wy2));
      float y = ((value.X * (xy2 + wz2)) + (value.Y * ((1.0f - xx2) - zz2))) + (value.Z * (yz2 - wx2));
      float z = ((value.X * (xz2 - wy2)) + (value.Y * (yz2 + wx2))) + (value.Z * ((1.0f - xx2) - yy2));

      Vector3 result;
      result.X = x;
      result.Y = y;
      result.Z = z;

      return result;
    }

    /// <summary>
    /// Transfroms the specified vector by the given <see cref="Quaternion"/>.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="q">Quaternion rotation</param>
    /// <param name="result">Transformed vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Transform(in Vector3 value, in Quaternion q, out Vector3 result)
    {
      float x2 = q.X + q.X;
      float y2 = q.Y + q.Y;
      float z2 = q.Z + q.Z;

      float wx2 = q.W * x2;
      float wy2 = q.W * y2;
      float wz2 = q.W * z2;

      float xx2 = q.X * x2;
      float xy2 = q.X * y2;
      float xz2 = q.X * z2;

      float yy2 = q.Y * y2;
      float yz2 = q.Y * z2;

      float zz2 = q.Z * z2;

      float x = ((value.X * ((1.0f - yy2) - zz2)) + (value.Y * (xy2 - wz2))) + (value.Z * (xz2 + wy2));
      float y = ((value.X * (xy2 + wz2)) + (value.Y * ((1.0f - xx2) - zz2))) + (value.Z * (yz2 - wx2));
      float z = ((value.X * (xz2 - wy2)) + (value.Y * (yz2 + wx2))) + (value.Z * ((1.0f - xx2) - yy2));

      result.X = x;
      result.Y = y;
      result.Z = z;
    }

    /// <summary>
    /// Transforms an span of vectors by the given <see cref="Quaternion"/>.
    /// </summary>
    /// <param name="source">Span of vectors to be transformed</param>
    /// <param name="q">Quaternion rotation</param>
    /// <param name="destination">Span to store transformed vectors, this may be the same as the source span</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the destination span is smaller than the source span.</exception>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Transform(ReadOnlySpan<Vector3> source, in Quaternion q, Span<Vector3> destination)
    {
      if (destination.Length < source.Length)
        throw new ArgumentOutOfRangeException(nameof(destination), StringLocalizer.Instance.GetLocalizedString("DestinationArraySmallerThanSource"));

      for (int i = 0; i < source.Length; i++)
        Transform(source[i], q, out destination[i]);
    }

    /// <summary>
    /// Performs a normal transformation using the given <see cref="Matrix"/>. The vector is treated
    /// as a row vector, so the multiplication is v*M.
    /// </summary>
    /// <param name="normal">Normal vector to transform</param>
    /// <param name="m">Transformation matrix</param>
    /// <returns>Transformed normal</returns>
    /// <remarks>A normal transform performs the transformation with the assumption that the w component is zero. This causes the fourth
    /// row and fourth column of the matrix to be unused. The end result is a vector that is not translated, but is rotated/scaled. This is preferred for
    /// normalized vectors that act as normals and only represent directions.</remarks>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector3 TransformNormal(in Vector3 normal, in Matrix m)
    {
      float x = (normal.X * m.M11) + (normal.Y * m.M21) + (normal.Z * m.M31);
      float y = (normal.X * m.M12) + (normal.Y * m.M22) + (normal.Z * m.M32);
      float z = (normal.X * m.M13) + (normal.Y * m.M23) + (normal.Z * m.M33);

      Vector3 result;
      result.X = x;
      result.Y = y;
      result.Z = z;

      return result;
    }

    /// <summary>
    /// Performs a normal transformation using the given <see cref="Matrix"/>. The vector is treated
    /// as a row vector, so the multiplication is v*M.
    /// </summary>
    /// <param name="normal">Normal vector to transform</param>
    /// <param name="m">Transformation matrix</param>
    /// <param name="result">Transformed normal</param>
    /// <remarks>A normal transform performs the transformation with the assumption that the w component is zero. This causes the fourth
    /// row and fourth column of the matrix to be unused. The end result is a vector that is not translated, but is rotated/scaled. This is preferred for
    /// normalized vectors that act as normals and only represent directions.</remarks>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void TransformNormal(in Vector3 normal, in Matrix m, out Vector3 result)
    {
      float x = (normal.X * m.M11) + (normal.Y * m.M21) + (normal.Z * m.M31);
      float y = (normal.X * m.M12) + (normal.Y * m.M22) + (normal.Z * m.M32);
      float z = (normal.X * m.M13) + (normal.Y * m.M23) + (normal.Z * m.M33);

      result.X = x;
      result.Y = y;
      result.Z = z;
    }

    /// <summary>
    /// Performs a normal transformation on an span of normal vectors by the given <see cref="Matrix"/>. The vector is treated
    /// as a row vector, so the multiplication is v*M.
    /// </summary>
    /// <param name="source">Span of normal vectors to be transformed</param>
    /// <param name="m">Transformation matrix</param>
    /// <param name="destination">Span to store transformed normal vectors, this may be the same as the source span</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the destination span is smaller than the source span.</exception>
    /// <remarks>A normal transform performs the transformation with the assumption that the w component is zero. This causes the fourth
    /// row and fourth column of the matrix to be unused. The end result is a vector that is not translated, but is rotated/scaled. This is preferred for
    /// normalized vectors that act as normals and only represent directions.</remarks>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void TransformNormal(ReadOnlySpan<Vector3> source, in Matrix m, Span<Vector3> destination)
    {
      if (destination.Length < source.Length)
        throw new ArgumentOutOfRangeException(nameof(destination), StringLocalizer.Instance.GetLocalizedString("DestinationArraySmallerThanSource"));

      for (int i = 0; i < source.Length; i++)
        TransformNormal(source[i], m, out destination[i]);
    }

    /// <summary>
    /// Performs a coordinate transformation using the given <see cref="Matrix"/>. The vector is treated
    /// as a row vector, so the multiplication is v*M.
    /// </summary>
    /// <param name="coordinate">Coordinate vector to transform</param>
    /// <param name="m">Transformation matrix</param>
    /// <returns>Transformed coordinate</returns>
    /// <remarks>A coordinate transform performs the transformation with the assumption that the w component is one. The four dimensional vector
    /// obtained from the transformation operation has each component in the vector divided by the w component. This forces the w component to
    /// be one and therefore makes the vector homogeneous. The homogeneous vector is often preferred when working with coordinates as the w component can be safely
    /// ignored.</remarks>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector3 TransformCoordinate(in Vector3 coordinate, in Matrix m)
    {
      float x = (coordinate.X * m.M11) + (coordinate.Y * m.M21) + (coordinate.Z * m.M31) + m.M41;
      float y = (coordinate.X * m.M12) + (coordinate.Y * m.M22) + (coordinate.Z * m.M32) + m.M42;
      float z = (coordinate.X * m.M13) + (coordinate.Y * m.M23) + (coordinate.Z * m.M33) + m.M43;
      float w = 1.0f / ((coordinate.X * m.M14) + (coordinate.Y * m.M24) + (coordinate.Z * m.M34) + m.M44);

      Vector3 result;
      result.X = x * w;
      result.Y = y * w;
      result.Z = z * w;

      return result;
    }

    /// <summary>
    /// Performs a coordinate transformation using the given <see cref="Matrix"/>. The vector is treated
    /// as a row vector, so the multiplication is v*M.
    /// </summary>
    /// <param name="coordinate">Coordinate vector to transform</param>
    /// <param name="m">Transformation matrix</param>
    /// <param name="result">Transformed coordinate</param>
    /// <remarks>A coordinate transform performs the transformation with the assumption that the w component is one. The four dimensional vector
    /// obtained from the transformation operation has each component in the vector divided by the w component. This forces the w component to
    /// be one and therefore makes the vector homogeneous. The homogeneous vector is often preferred when working with coordinates as the w component can be safely
    /// ignored.</remarks>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void TransformCoordinate(in Vector3 coordinate, in Matrix m, out Vector3 result)
    {
      float x = (coordinate.X * m.M11) + (coordinate.Y * m.M21) + (coordinate.Z * m.M31) + m.M41;
      float y = (coordinate.X * m.M12) + (coordinate.Y * m.M22) + (coordinate.Z * m.M32) + m.M42;
      float z = (coordinate.X * m.M13) + (coordinate.Y * m.M23) + (coordinate.Z * m.M33) + m.M43;
      float w = 1.0f / ((coordinate.X * m.M14) + (coordinate.Y * m.M24) + (coordinate.Z * m.M34) + m.M44);

      result.X = x * w;
      result.Y = y * w;
      result.Z = z * w;
    }

    /// <summary>
    /// Performs a coordinate transformation on an span of coordinate vectors by the given <see cref="Matrix"/>. The vector is treated
    /// as a row vector, so the multiplication is v*M.
    /// </summary>
    /// <param name="source">Span of coordinate vectors to be transformed</param>
    /// <param name="m">Transformation matrix</param>
    /// <param name="destination">Span to store transformed coordinate vectors, this may be the same as the source span</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the destination span is smaller than the source span.</exception>
    /// <remarks>A coordinate transform performs the transformation with the assumption that the w component is one. The four dimensional vector
    /// obtained from the transformation operation has each component in the vector divided by the w component. This forces the w component to
    /// be one and therefore makes the vector homogeneous. The homogeneous vector is often preferred when working with coordinates as the w component can be safely
    /// ignored.</remarks>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void TransformCoordinate(ReadOnlySpan<Vector3> source, in Matrix m, Span<Vector3> destination)
    {
      if (destination.Length < source.Length)
        throw new ArgumentOutOfRangeException(nameof(destination), StringLocalizer.Instance.GetLocalizedString("DestinationArraySmallerThanSource"));

      for (int i = 0; i < source.Length; i++)
        TransformCoordinate(source[i], m, out destination[i]);
    }

    /// <summary>
    /// Orthogonalizes an span of vectors using the modified Gram-Schmidt process. Source and destination should not be the same spans.
    /// </summary>
    /// <param name="source">Span of coordinate vectors to be transformed</param>
    /// <param name="destination">Span to store transformed coordinate vectors</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the destination span is smaller than the source span.</exception>
    /// <remarks>Orthonormalization is the process of making all vectors orthogonal to each other. This means that any given vector will be orthogonal to any other 
    /// given vector in the list. Because this method uses the modified Gram-Schmidt process, the resulting vectors tend to be numerically unstable. 
    /// The numeric stability decreases according to the vectors position in the span, so that the first vector is the most stable and the last vector is the least stable.</remarks>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Orthogonalize(ReadOnlySpan<Vector3> source, Span<Vector3> destination)
    {
      if (destination.Length < source.Length)
        throw new ArgumentOutOfRangeException(nameof(destination), StringLocalizer.Instance.GetLocalizedString("DestinationArraySmallerThanSource"));

      for (int i = 0; i < source.Length; i++)
      {
        Vector3 curr = source[i];
        for (int j = 0; j < i; j++)
        {
          Vector3 next = destination[j];
          float dotuv = Vector3.Dot(next, curr);
          float dotuu = Vector3.Dot(next, next);

          Vector3.Multiply(next, dotuv / dotuu, out next);
          Vector3.Subtract(curr, next, out curr);
        }

        destination[i] = curr;
      }
    }

    /// <summary>
    /// Orthonormalizes an span of vectors using the modified Gram-Schmidt process. Source and destination should not be the same spans.
    /// </summary>
    /// <param name="source">Span of coordinate vectors to be transformed</param>
    /// <param name="destination">Span to store transformed coordinate vectors</param>
    /// <exception cref="ArgumentNullException">Thrown if either the source or destination span is null.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the destination span is smaller than the source span.</exception>
    /// <remarks>Orthonormalization is the process of making all vectors orthogonal to each other and making all vectors of unit length. This means
    /// that any given vector will be orthogonal to any other given vector in the list. Because this
    /// method uses the modified Gram-Schmidt process, the resulting vectors tend to be numerically unstable. The numeric stability decreases according to the vectors position in
    /// the span, so that the first vector is the most stable and the last vector is the least stable.</remarks>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Orthonormalize(ReadOnlySpan<Vector3> source, Span<Vector3> destination)
    {
      if (destination.Length < source.Length)
        throw new ArgumentOutOfRangeException(nameof(destination), StringLocalizer.Instance.GetLocalizedString("DestinationArraySmallerThanSource"));

      for (int i = 0; i < source.Length; i++)
      {
        Vector3 curr = source[i];
        for (int j = 0; j < i; j++)
        {
          Vector3 next = destination[j];
          float dotuv = Vector3.Dot(next, curr);

          Vector3.Multiply(next, dotuv, out next);
          Vector3.Subtract(curr, next, out curr);
        }

        curr.Normalize();
        destination[i] = curr;
      }
    }

    /// <summary>
    /// Implicitly converts from <see cref="Vector3"/> to <see cref="System.Numerics.Vector3"/> for interop.
    /// </summary>
    /// <param name="v">Vector to convert.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static implicit operator System.Numerics.Vector3(Vector3 v)
    {
      return new System.Numerics.Vector3(v.X, v.Y, v.Z);
    }

    /// <summary>
    /// Implicitly converts from <see cref="System.Numerics.Vector3"/> to <see cref="Vector3"/> for interop.
    /// </summary>
    /// <param name="v">Vector to convert.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static implicit operator Vector3(System.Numerics.Vector3 v)
    {
      return new Vector3(v.X, v.Y, v.Z);
    }

    /// <summary>
    /// Tests equality between two vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>True if components are equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator ==(Vector3 a, Vector3 b)
    {
      return (MathF.Abs(a.X - b.X) <= MathHelper.ZeroTolerance) && (MathF.Abs(a.Y - b.Y) <= MathHelper.ZeroTolerance) &&
          (MathF.Abs(a.Z - b.Z) <= MathHelper.ZeroTolerance);
    }

    /// <summary>
    /// Tests inequality between two vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>True if components are not equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator !=(Vector3 a, Vector3 b)
    {
      return (MathF.Abs(a.X - b.X) > MathHelper.ZeroTolerance) || (MathF.Abs(a.Y - b.Y) > MathHelper.ZeroTolerance) ||
          (MathF.Abs(a.Z - b.Z) > MathHelper.ZeroTolerance);
    }

    /// <summary>
    /// Adds the two vectors together.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Sum of the two vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector3 operator +(Vector3 a, Vector3 b)
    {
      Vector3 result;
      result.X = a.X + b.X;
      result.Y = a.Y + b.Y;
      result.Z = a.Z + b.Z;

      return result;
    }

    /// <summary>
    /// Flips the signs of the components of the specified vector.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <returns>Negated vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector3 operator -(Vector3 value)
    {
      Vector3 result;
      result.X = -value.X;
      result.Y = -value.Y;
      result.Z = -value.Z;

      return result;
    }

    /// <summary>
    /// Subtracts vector b from vector a.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Difference of the two vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector3 operator -(Vector3 a, Vector3 b)
    {
      Vector3 result;
      result.X = a.X - b.X;
      result.Y = a.Y - b.Y;
      result.Z = a.Z - b.Z;

      return result;
    }

    /// <summary>
    /// Multiplies two vectors together.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Multiplied vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector3 operator *(Vector3 a, Vector3 b)
    {
      Vector3 result;
      result.X = a.X * b.X;
      result.Y = a.Y * b.Y;
      result.Z = a.Z * b.Z;

      return result;
    }

    /// <summary>
    /// Multiplies a vector by a scaling factor.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="scale">Amount to scale</param>
    /// <returns>Multiplied vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector3 operator *(Vector3 value, float scale)
    {
      Vector3 result;
      result.X = value.X * scale;
      result.Y = value.Y * scale;
      result.Z = value.Z * scale;

      return result;
    }

    /// <summary>
    /// Multiplies a vector by a scaling factor.
    /// </summary>
    /// <param name="scale">Amount to scale</param>
    /// <param name="value">Source vector</param>
    /// <returns>Multiplied vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector3 operator *(float scale, Vector3 value)
    {
      Vector3 result;
      result.X = value.X * scale;
      result.Y = value.Y * scale;
      result.Z = value.Z * scale;

      return result;
    }

    /// <summary>
    /// Divides the components of vector a by those of vector b.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Divisor vector</param>
    /// <returns>Quotient of the two vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector3 operator /(Vector3 a, Vector3 b)
    {
      Vector3 result;
      result.X = a.X / b.X;
      result.Y = a.Y / b.Y;
      result.Z = a.Z / b.Z;

      return result;
    }

    /// <summary>
    /// Divides a vector by a scalar value.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="divisor">Divisor scalar</param>
    /// <returns>Divided vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector3 operator /(Vector3 value, float divisor)
    {
      Vector3 result;
      float invDivisor = 1.0f / divisor;
      result.X = value.X * invDivisor;
      result.Y = value.Y * invDivisor;
      result.Z = value.Z * invDivisor;

      return result;
    }

    /// <summary>
    /// Flips the signs of the components of the vector.
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void Negate()
    {
      X = -X;
      Y = -Y;
      Z = -Z;
    }

    /// <summary>
    /// Compute the length (magnitude) of the vector.
    /// </summary>
    /// <returns>Length</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float Length()
    {
      float lengthSquared = (X * X) + (Y * Y) + (Z * Z);
      return MathF.Sqrt(lengthSquared);
    }

    /// <summary>
    /// Compute the length (magnitude) squared of the vector.
    /// </summary>
    /// <returns>Length squared</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float LengthSquared()
    {
      return (X * X) + (Y * Y) + (Z * Z);
    }

    /// <summary>
    /// Normalize the vector to a unit vector, which results in a vector with a magnitude of 1 but
    /// preserves the direction the vector was pointing in.
    /// </summary>
    /// <returns>The magnitude (length) of the vector.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public float Normalize()
    {
      float lengthSquared = (X * X) + (Y * Y) + (Z * Z);

      if (lengthSquared > MathHelper.ZeroTolerance)
      {
        float magnitnude = MathF.Sqrt(lengthSquared);

        float invLength = 1.0f / magnitnude;
        X *= invLength;
        Y *= invLength;
        Z *= invLength;

        return magnitnude;
      }

      return 0.0f;
    }

    /// <summary>
    /// Tests equality between the vector and another vector.
    /// </summary>
    /// <param name="other">Vector to test against</param>
    /// <returns>True if components are equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    readonly bool IEquatable<Vector3>.Equals(Vector3 other)
    {
      return (MathF.Abs(X - other.X) <= MathHelper.ZeroTolerance) && (MathF.Abs(Y - other.Y) <= MathHelper.ZeroTolerance) &&
          (MathF.Abs(Z - other.Z) <= MathHelper.ZeroTolerance);
    }

    /// <summary>
    /// Tests equality between the vector and another vector.
    /// </summary>
    /// <param name="other">Vector to test against</param>
    /// <returns>True if components are equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Equals(in Vector3 other)
    {
      return (MathF.Abs(X - other.X) <= MathHelper.ZeroTolerance) && (MathF.Abs(Y - other.Y) <= MathHelper.ZeroTolerance) &&
          (MathF.Abs(Z - other.Z) <= MathHelper.ZeroTolerance);
    }

    /// <summary>
    /// Tests equality between the vector and another vector.
    /// </summary>
    /// <param name="other">Vector to test against</param>
    /// <param name="tolerance">Tolerance</param>
    /// <returns>True if components are equal within tolerance, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Equals(in Vector3 other, float tolerance)
    {
      return (MathF.Abs(X - other.X) <= tolerance) && (MathF.Abs(Y - other.Y) <= tolerance) &&
          (MathF.Abs(Z - other.Z) <= tolerance);
    }

    /// <summary>
    /// Tests equality between the vector and XYZ values.
    /// </summary>
    /// <param name="x">X component.</param>
    /// <param name="y">Y component.</param>
    /// <param name="z">Z component.</param>
    /// <param name="tolerance">Optional tolerance, defaults to <see cref="MathHelper.ZeroTolerance"/>.</param>
    /// <returns>True if components are equal within tolerance, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Equals(float x, float y, float z, float tolerance = MathHelper.ZeroTolerance)
    {
      return (MathF.Abs(X - x) <= tolerance) && (MathF.Abs(Y - y) <= tolerance) &&
          (MathF.Abs(Z - z) <= tolerance);
    }

    /// <summary>
    /// Determines whether the specified <see cref="System.Object" /> is equal to this instance.
    /// </summary>
    /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
    /// <returns>True if the specified <see cref="System.Object" /> is equal to this instance; otherwise, false.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is Vector3)
        return Equals((Vector3)obj);

      return false;
    }

    /// <summary>
    /// Determines whether this vector is nearly (0, 0, 0).
    /// </summary>
    /// <param name="tol">Optional tolerance.</param>
    /// <returns>True if this vector is almost zero, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool IsAlmostZero(float tol = MathHelper.ZeroTolerance)
    {
      ref readonly Vector3 zero = ref Vector3.Zero;
      return Equals(zero, tol);
    }

    /// <summary>
    /// Determines whether this vector is nearly (1, 1, 1).
    /// </summary>
    /// <param name="tol">Optional tolerance.</param>
    /// <returns>True if this vector is almost zero, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool IsAlmostOne(float tol = MathHelper.ZeroTolerance)
    {
      ref readonly Vector3 one = ref Vector3.One;
      return Equals(one, tol);
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
    public readonly override int GetHashCode()
    {
      unchecked
      {
        return X.GetHashCode() + Y.GetHashCode() + Z.GetHashCode();
      }
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly override String ToString()
    {
      CultureInfo info = CultureInfo.CurrentCulture;
      return String.Format(info, "X: {0} Y: {1} Z: {2}",
          new Object[] { X.ToString(info), Y.ToString(info), Z.ToString(info) });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="format">The format</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(string? format)
    {
      if (format is null)
        return ToString();

      CultureInfo info = CultureInfo.CurrentCulture;
      return String.Format(info, "X: {0} Y: {1} Z: {2}",
          new Object[] { X.ToString(format, info), Y.ToString(format, info), Z.ToString(format, info) });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="formatProvider">The format provider.</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(IFormatProvider? formatProvider)
    {
      if (formatProvider is null)
        return ToString();

      return String.Format(formatProvider, "X: {0} Y: {1} Z: {2}",
          new Object[] { X.ToString(formatProvider), Y.ToString(formatProvider), Z.ToString(formatProvider) });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="format">The format.</param>
    /// <param name="formatProvider">The format provider.</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(string? format, IFormatProvider? formatProvider)
    {
      if (formatProvider is null)
        return ToString();

      if (format is null)
        return ToString(formatProvider);

      return String.Format(formatProvider, "X: {0} Y: {1} Z: {2}",
          new Object[] { X.ToString(format, formatProvider), Y.ToString(format, formatProvider), Z.ToString(format, formatProvider) });
    }

    /// <summary>
    /// Writes the primitive data to the output.
    /// </summary>
    /// <param name="output">Primitive writer.</param>
    readonly void IPrimitiveValue.Write(IPrimitiveWriter output)
    {
      output.Write("X", X);
      output.Write("Y", Y);
      output.Write("Z", Z);
    }

    /// <summary>
    /// Reads the primitive data from the input.
    /// </summary>
    /// <param name="input">Primitive reader.</param>
    void IPrimitiveValue.Read(IPrimitiveReader input)
    {
      X = input.ReadSingle("X");
      Y = input.ReadSingle("Y");
      Z = input.ReadSingle("Z");
    }
  }
}
