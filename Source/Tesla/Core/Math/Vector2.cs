﻿/*
* Copyright (c) 2010-2024 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Numerics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Tesla.Content;
using Tesla.Design;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Defines a two dimensional vector of 32-bit floats.
  /// </summary>
  [Serializable]
  [StructLayout(LayoutKind.Sequential, Pack = 4)]
  [TypeConverter(typeof(Vector2TypeConverter))]
  public struct Vector2 : IEquatable<Vector2>, IRefEquatable<Vector2>, IFormattable, IPrimitiveValue
  {
    /// <summary>
    /// X component of the vector.
    /// </summary>
    public float X;

    /// <summary>
    /// Y component of the vector.
    /// </summary>
    public float Y;

    private static readonly Vector2 s_zero = new Vector2(0f, 0f);
    private static readonly Vector2 s_one = new Vector2(1f, 1f);
    private static readonly Vector2 s_unitX = new Vector2(1f, 0f);
    private static readonly Vector2 s_unitY = new Vector2(0f, 1f);

    private static readonly int s_sizeInbytes = BufferHelper.SizeOf<Vector2>();

    /// <summary>
    /// Gets a <see cref="Vector2"/> set to (0, 0).
    /// </summary>
    public static ref readonly Vector2 Zero
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_zero;
      }
    }

    /// <summary>
    /// Gets a <see cref="Vector2"/> set to (1, 1).
    /// </summary>
    public static ref readonly Vector2 One
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_one;
      }
    }

    /// <summary>
    /// Gets a unit <see cref="Vector2"/> set to (1, 0).
    /// </summary>
    public static ref readonly Vector2 UnitX
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_unitX;
      }
    }

    /// <summary>
    /// Gets a unit <see cref="Vector2"/> set to (0, 1).
    /// </summary>
    public static ref readonly Vector2 UnitY
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_unitY;
      }
    }

    /// <summary>
    /// Gets the size of the <see cref="Vector2"/> type in bytes.
    /// </summary>
    public static int SizeInBytes
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return s_sizeInbytes;
      }
    }

    /// <summary>
    /// Gets whether the vector is normalized or not.
    /// </summary>
    public readonly bool IsNormalized
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return MathF.Abs(((X * X) + (Y * Y)) - 1.0f) <= MathHelper.ZeroTolerance;
      }
    }

    /// <summary>
    /// Gets whether any of the components of the vector are NaN (Not A Number).
    /// </summary>
    public readonly bool IsNaN
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return float.IsNaN(X) || float.IsNaN(Y);
      }
    }

    /// <summary>
    /// Gets whether any of the components of the vector are positive or negative infinity.
    /// </summary>
    public readonly bool IsInfinity
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return float.IsInfinity(X) || float.IsInfinity(Y);
      }
    }

    /// <summary>
    /// Gets or sets individual components of the vector in the order that the components are declared (XY).
    /// </summary>
    /// <param name="index">Zero-based index.</param>
    /// <returns>The value of the specified component.</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the index is out of range [0, 1]</exception>
    public float this[int index]
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      readonly get
      {
        switch (index)
        {
          case 0:
            return X;
          case 1:
            return Y;
          default:
            throw new ArgumentOutOfRangeException(nameof(index), StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));
        }
      }
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      set
      {
        switch (index)
        {
          case 0:
            X = value;
            break;
          case 1:
            Y = value;
            break;
          default:
            throw new ArgumentOutOfRangeException(nameof(index), StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));
        }
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Vector2"/> struct.
    /// </summary>
    /// <param name="value">Value to initialize each component to.</param>
    public Vector2(float value)
    {
      X = Y = value;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Vector2"/> struct.
    /// </summary>
    /// <param name="x">X component.</param>
    /// <param name="y">Y component.</param>
    public Vector2(float x, float y)
    {
      X = x;
      Y = y;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Vector2"/> struct.
    /// </summary>
    /// <param name="xy">Convert from <see cref="Vector3"/>, only taking the XY values.</param>
    public Vector2(in Vector3 xy)
    {
      X = xy.X;
      Y = xy.Y;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Vector2"/> struct.
    /// </summary>
    /// <param name="xy">Convert from <see cref="Vector4"/>, only taking the XY values.</param>
    public Vector2(in Vector4 xy)
    {
      X = xy.X;
      Y = xy.Y;
    }

    /// <summary>
    /// Compute the acute angle between two vectors in the range of [0, PI / 2]. Assumes that both vectors are already normalized.
    /// </summary>
    /// <param name="a">First unit vector</param>
    /// <param name="b">Second unit vector</param>
    /// <returns>Acute angle between the vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Angle AcuteAngleBetween(in Vector2 a, in Vector2 b)
    {
      //Acos(dotProduct)
      Angle angle = new Angle(MathF.Acos(MathHelper.Clamp((a.X * b.X) + (a.Y * b.Y), -1.0f, 1.0f)));

      if (angle.Radians > MathHelper.PiOverTwo)
        angle.Radians = MathHelper.Pi - angle.Radians;

      return angle;
    }

    /// <summary>
    /// Compute the angle between two vectors in the range of [0, PI]. Assumes that both vectors are already normalized.
    /// </summary>
    /// <param name="a">First unit vector</param>
    /// <param name="b">Second unit vector</param>
    /// <returns>Angle between the vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Angle AngleBetween(in Vector2 a, in Vector2 b)
    {
      //Acos(dotProduct)
      return new Angle(MathF.Acos(MathHelper.Clamp((a.X * b.X) + (a.Y * b.Y), -1.0f, 1.0f)));
    }

    /// <summary>
    /// Computes a signed angle between two vectors in the range of [-PI, PI]. The 3D case would use a plane normal to determine orientation, but since 2D vectors are always on the XY
    /// plane, the orientation vector can be either positive/negative UnitZ (0, 0, 1) or (0, 0, -1). The signed angle is found by taking the cross product of
    /// the two vectors [source X dest] and computing the dot product with the orientation vector. Assumes that the two vectors are already normalized.
    /// </summary>
    /// <param name="source">Start unit vector</param>
    /// <param name="dest">Destination unit vector</param>
    /// <param name="planeNormalUnitZ">True if the plane normal should be UnitZ (0, 0, 1), false if the opposite direction (0, 0, -1). By default this is true.</param>
    /// <returns>Signed angle between the vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Angle SignedAngleBetween(in Vector2 source, in Vector2 dest, bool planeNormalUnitZ = true)
    {
      //Acos(dotProduct)
      float angleBetween = MathF.Acos(MathHelper.Clamp((source.X * dest.X) + (source.Y * dest.Y), -1.0f, 1.0f));

      Vector3 source3D = new Vector3(source, 0.0f);
      Vector3 dest3D = new Vector3(dest, 0.0f);
      Vector3 planeNormal = (planeNormalUnitZ) ? new Vector3(0, 0, 1) : new Vector3(0, 0, -1);

      Vector3 cross;
      Vector3.Cross(source3D, dest3D, out cross);

      float dot = Vector3.Dot(cross, planeNormal);

      //Cross product should either be pointing in same direction as planeNormal or in opposite. If opposite, it will be negative
      return new Angle((dot < 0.0f) ? -angleBetween : angleBetween);
    }

    /// <summary>
    /// Computes a signed acute angle between two vectors in the range of [-PI / 2, PI / 2]. The 3D case would use a plane normal to determine orientation, but since 2D vectors are always on the XY
    /// plane, the orientation vector can be either positive/negative UnitZ (0, 0, 1) or (0, 0, -1). The signed angle is found by taking the cross product of
    /// the two vectors [source X dest] and computing the dot product with the orientation vector. Assumes that the two vectors are already normalized.
    /// </summary>
    /// <param name="source">Start unit vector</param>
    /// <param name="dest">Destination unit vector</param>
    /// <param name="planeNormalUnitZ">True if the plane normal should be UnitZ (0, 0, 1), false if the opposite direction (0, 0, -1). By default this is true.</param>
    /// <returns>Signed angle between the vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Angle SignedAcuteAngleBetween(in Vector2 source, in Vector2 dest, bool planeNormalUnitZ = true)
    {
      //Acos(dotProduct)
      float angleBetween = MathF.Acos(MathHelper.Clamp((source.X * dest.X) + (source.Y * dest.Y), -1.0f, 1.0f));

      if (angleBetween > MathHelper.PiOverTwo)
        angleBetween = MathHelper.Pi - angleBetween;

      Vector3 source3D = new Vector3(source, 0.0f);
      Vector3 dest3D = new Vector3(dest, 0.0f);
      Vector3 planeNormal = (planeNormalUnitZ) ? new Vector3(0, 0, 1) : new Vector3(0, 0, -1);

      Vector3 cross;
      Vector3.Cross(source3D, dest3D, out cross);

      float dot = Vector3.Dot(cross, planeNormal);

      //Cross product should either be pointing in same direction as planeNormal or in opposite. If opposite, it will be negative
      return new Angle((dot < 0.0f) ? -angleBetween : angleBetween);
    }

    /// <summary>
    /// Adds two vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Sum of the two vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector2 Add(in Vector2 a, in Vector2 b)
    {
      Vector2 result;
      result.X = a.X + b.X;
      result.Y = a.Y + b.Y;

      return result;
    }

    /// <summary>
    /// Adds two vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <param name="result">Sum of the two vectors</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Add(in Vector2 a, in Vector2 b, out Vector2 result)
    {
      result.X = a.X + b.X;
      result.Y = a.Y + b.Y;
    }

    /// <summary>
    /// Subtracts vector b from vector a.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Difference of the two vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector2 Subtract(in Vector2 a, in Vector2 b)
    {
      Vector2 result;
      result.X = a.X - b.X;
      result.Y = a.Y - b.Y;

      return result;
    }

    /// <summary>
    /// Subtracts vector b from vector a.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <param name="result">Difference of the two vectors</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Subtract(in Vector2 a, in Vector2 b, out Vector2 result)
    {
      result.X = a.X - b.X;
      result.Y = a.Y - b.Y;
    }

    /// <summary>
    /// Multiplies components of two vectors together.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Multiplied vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector2 Multiply(in Vector2 a, in Vector2 b)
    {
      Vector2 result;
      result.X = a.X * b.X;
      result.Y = a.Y * b.Y;

      return result;
    }

    /// <summary>
    /// Multiplies components of two vectors together.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <param name="result">Multiplied vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Multiply(in Vector2 a, in Vector2 b, out Vector2 result)
    {
      result.X = a.X * b.X;
      result.Y = a.Y * b.Y;
    }

    /// <summary>
    /// Multiplies components of a vector by a scalar value.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="scale">Amount to scale</param>
    /// <returns>Scaled vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector2 Multiply(in Vector2 value, float scale)
    {
      Vector2 result;
      result.X = value.X * scale;
      result.Y = value.Y * scale;

      return result;
    }

    /// <summary>
    /// Multiplies components of a vector by a scalar value.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="scale">Amount to scale</param>
    /// <param name="result">Scaled vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Multiply(in Vector2 value, float scale, out Vector2 result)
    {
      result.X = value.X * scale;
      result.Y = value.Y * scale;
    }

    /// <summary>
    /// Multiplies the matrix and the vector. The vector is treated
    /// as a column vector, so the multiplication is M*v.
    /// </summary>
    /// <param name="m">Matrix to multiply.</param>
    /// <param name="value">Vector to multiply.</param>
    /// <returns>Resulting vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector2 Multiply(in Matrix m, in Vector2 value)
    {
      float x = (m.M11 * value.X) + (m.M12 * value.Y);
      float y = (m.M21 * value.X) + (m.M22 * value.Y);

      Vector2 result;
      result.X = x;
      result.Y = y;

      return result;
    }

    /// <summary>
    /// Multiplies the matrix and the vector. The vector is treated
    /// as a column vector, so the multiplication is M*v.
    /// </summary>
    /// <param name="m">Matrix to multiply.</param>
    /// <param name="value">Vector to multiply.</param>
    /// <param name="result">Resulting vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Multiply(in Matrix m, in Vector2 value, out Vector2 result)
    {
      float x = (m.M11 * value.X) + (m.M12 * value.Y);
      float y = (m.M21 * value.X) + (m.M22 * value.Y);

      result.X = x;
      result.Y = y;
    }

    /// <summary>
    /// Divides the components of vector a by those of vector b.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Divisor vector</param>
    /// <returns>Quotient of the two vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector2 Divide(in Vector2 a, in Vector2 b)
    {
      Vector2 result;
      result.X = a.X / b.X;
      result.Y = a.Y / b.Y;

      return result;
    }

    /// <summary>
    /// Divides the components of vector a by those of vector b.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Divisor vector</param>
    /// <param name="result">Quotient of the two vectors</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Divide(in Vector2 a, in Vector2 b, out Vector2 result)
    {
      result.X = a.X / b.X;
      result.Y = a.Y / b.Y;
    }

    /// <summary>
    /// Divides the components of a vector by a scalar.
    /// </summary>
    /// <param name="value">First vector</param>
    /// <param name="divisor">Divisor scalar</param>
    /// <returns>Divided vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector2 Divide(in Vector2 value, float divisor)
    {
      float invDiv = 1.0f / divisor;
      Vector2 result;
      result.X = value.X * invDiv;
      result.Y = value.Y * invDiv;

      return result;
    }

    /// <summary>
    /// Divides the components of a vector by a scalar.
    /// </summary>
    /// <param name="value">First vector</param>
    /// <param name="divisor">Divisor scalar</param>
    /// <param name="result">Divided vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Divide(in Vector2 value, float divisor, out Vector2 result)
    {
      float invDiv = 1.0f / divisor;
      result.X = value.X * invDiv;
      result.Y = value.Y * invDiv;
    }

    /// <summary>
    /// Flips the signs of the components of the specified vector.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <returns>Negated vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector2 Negate(in Vector2 value)
    {
      Vector2 result;
      result.X = -value.X;
      result.Y = -value.Y;

      return result;
    }

    /// <summary>
    /// Flips the signs of the components of the specified vector.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="result">Negated vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Negate(in Vector2 value, out Vector2 result)
    {
      result.X = -value.X;
      result.Y = -value.Y;
    }

    /// <summary>
    /// Compute the dot product between two vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Dot product</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float Dot(in Vector2 a, in Vector2 b)
    {
      return (a.X * b.X) + (a.Y * b.Y);
    }

    /// <summary>
    /// Normalize the source vector to a unit vector, which results in a vector with a magnitude of 1 but
    /// preserves the direction the vector was pointing in.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <returns>Normalized unit vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector2 Normalize(in Vector2 value)
    {
      float lengthSquared = (value.X * value.X) + (value.Y * value.Y);

      Vector2 result = value;

      if (lengthSquared > MathHelper.ZeroTolerance)
      {
        float invLength = 1.0f / MathF.Sqrt(lengthSquared);
        result.X *= invLength;
        result.Y *= invLength;
      }

      return result;
    }

    /// <summary>
    /// Normalize the source vector to a unit vector, which results in a vector with a magnitude of 1 but
    /// preserves the direction the vector was pointing in.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="result">Normalized unit vector</param>
    /// <returns>The magnitude (length) of the vector.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float Normalize(in Vector2 value, out Vector2 result)
    {
      float lengthSquared = (value.X * value.X) + (value.Y * value.Y);

      result = value;

      if (lengthSquared > MathHelper.ZeroTolerance)
      {
        float magnitnude = MathF.Sqrt(lengthSquared);

        float invLength = 1.0f / magnitnude;
        result.X *= invLength;
        result.Y *= invLength;

        return magnitnude;
      }

      return 0.0f;
    }

    /// <summary>
    /// Compute the distance between two vectors.
    /// </summary>
    /// <param name="start">First vector</param>
    /// <param name="end">Second vector</param>
    /// <returns>Distance between the vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float Distance(in Vector2 start, in Vector2 end)
    {
      float dx = start.X - end.X;
      float dy = start.Y - end.Y;
      float distanceSquared = (dx * dx) + (dy * dy);

      return MathF.Sqrt(distanceSquared);
    }

    /// <summary>
    /// Compute the distance squared between two vectors.
    /// </summary>
    /// <param name="start">First vector</param>
    /// <param name="end">Second vector</param>
    /// <returns>Distance squared between the vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float DistanceSquared(in Vector2 start, in Vector2 end)
    {
      float dx = start.X - end.X;
      float dy = start.Y - end.Y;

      return (dx * dx) + (dy * dy);
    }

    /// <summary>
    /// Gets the vector that contains the maximum value from each of the components of the 
    /// two supplied vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Maximum vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector2 Max(in Vector2 a, in Vector2 b)
    {
      Vector2 result;
      result.X = (a.X > b.X) ? a.X : b.X;
      result.Y = (a.Y > b.Y) ? a.Y : b.Y;

      return result;
    }

    /// <summary>
    /// Gets the vector that contains the maximum value from each of the components of the 
    /// two supplied vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <param name="result">Maximum vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Max(in Vector2 a, in Vector2 b, out Vector2 result)
    {
      result.X = (a.X > b.X) ? a.X : b.X;
      result.Y = (a.Y > b.Y) ? a.Y : b.Y;
    }

    /// <summary>
    /// Gets the vector that contains the mininum value from each of the components of the 
    /// two supplied vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Minimum vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector2 Min(in Vector2 a, in Vector2 b)
    {
      Vector2 result;
      result.X = (a.X < b.X) ? a.X : b.X;
      result.Y = (a.Y < b.Y) ? a.Y : b.Y;

      return result;
    }

    /// <summary>
    /// Gets the vector that contains the mininum value from each of the components of the 
    /// two supplied vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <param name="result">Minimum vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Min(in Vector2 a, in Vector2 b, out Vector2 result)
    {
      result.X = (a.X < b.X) ? a.X : b.X;
      result.Y = (a.Y < b.Y) ? a.Y : b.Y;
    }

    /// <summary>
    /// Restricts the source vector in the range of the minimum and maximum vectors.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="min">Minimum vector</param>
    /// <param name="max">Maximum vector</param>
    /// <returns>Clamped vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector2 Clamp(in Vector2 value, in Vector2 min, in Vector2 max)
    {
      float x = value.X;
      float y = value.Y;

      x = (x > max.X) ? max.X : x;
      x = (x < min.X) ? min.X : x;

      y = (y > max.Y) ? max.Y : y;
      y = (y < min.Y) ? min.Y : y;

      Vector2 result;
      result.X = x;
      result.Y = y;

      return result;
    }

    /// <summary>
    /// Restricts the source vector in the range of the minimum and maximum vectors.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="min">Minimum vector</param>
    /// <param name="max">Maximum vector</param>
    /// <param name="result">Clamped vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Clamp(in Vector2 value, in Vector2 min, in Vector2 max, out Vector2 result)
    {
      float x = value.X;
      float y = value.Y;

      x = (x > max.X) ? max.X : x;
      x = (x < min.X) ? min.X : x;

      y = (y > max.Y) ? max.Y : y;
      y = (y < min.Y) ? min.Y : y;

      result.X = x;
      result.Y = y;
    }

    /// <summary>
    /// Returns a <see cref="Vector2"/> containing the 2D Cartesian coordinates of a point specified
    /// in barycentric coordinates relative to a 2D triangle.
    /// </summary>
    /// <param name="a">Vector containing 2D cartesian coordinates corresponding to triangle's first vertex</param>
    /// <param name="b">Vector containing 2D cartesian coordinates corresponding to triangle's second vertex</param>
    /// <param name="c">Vector containing 2D cartesian coordinates corresponding to triangle's third vertex</param>
    /// <param name="s">Barycentric coordinate s that is the weighting factor toward the second vertex</param>
    /// <param name="t">Barycentric coordinate t that is the weighting factor toward the third vertex</param>
    /// <returns>Barycentric coordinates</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector2 Barycentric(in Vector2 a, in Vector2 b, in Vector2 c, float s, float t)
    {
      Vector2 result;
      result.X = (a.X + (s * (b.X - a.X))) + (t * (c.X - a.X));
      result.Y = (a.Y + (s * (b.Y - a.Y))) + (t * (c.Y - a.Y));
      return result;
    }

    /// <summary>
    /// Returns a <see cref="Vector2"/> containing the 2D Cartesian coordinates of a point specified
    /// in barycentric coordinates relative to a 2D triangle.
    /// </summary>
    /// <param name="a">Vector containing 2D cartesian coordinates corresponding to triangle's first vertex</param>
    /// <param name="b">Vector containing 2D cartesian coordinates corresponding to triangle's second vertex</param>
    /// <param name="c">Vector containing 2D cartesian coordinates corresponding to triangle's third vertex</param>
    /// <param name="s">Barycentric coordinate s that is the weighting factor toward the second vertex</param>
    /// <param name="t">Barycentric coordinate t that is the weighting factor toward the third vertex</param>
    /// <param name="result">Barycentric coordinates</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Barycentric(in Vector2 a, in Vector2 b, in Vector2 c, float s, float t, out Vector2 result)
    {
      result.X = (a.X + (s * (b.X - a.X))) + (t * (c.X - a.X));
      result.Y = (a.Y + (s * (b.Y - a.Y))) + (t * (c.Y - a.Y));
    }

    /// <summary>
    /// Compute Catmull-Rom interpolation using the the specified positions.
    /// </summary>
    /// <param name="a">First position</param>
    /// <param name="b">Second position</param>
    /// <param name="c">Third position</param>
    /// <param name="d">Fourth position</param>
    /// <param name="wf">Weighting factor</param>
    /// <returns>Catmull-Rom interpolated vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector2 CatmullRom(in Vector2 a, in Vector2 b, in Vector2 c, in Vector2 d, float wf)
    {
      Vector2 result;
      float wfSquared = wf * wf;
      float wfCubed = wf * wfSquared;
      result.X = 0.5f * ((((2.0f * b.X) + ((-a.X + c.X) * wf)) + (((((2.0f * a.X) - (5.0f * b.X)) + (4.0f * c.X)) - d.X) * wfSquared)) + ((((-a.X + (3.0f * b.X)) - (3.0f * c.X)) + d.X) * wfCubed));
      result.Y = 0.5f * ((((2.0f * b.Y) + ((-a.Y + c.Y) * wf)) + (((((2.0f * a.Y) - (5.0f * b.Y)) + (4.0f * c.Y)) - d.Y) * wfSquared)) + ((((-a.Y + (3.0f * b.Y)) - (3.0f * c.Y)) + d.Y) * wfCubed));

      return result;
    }

    /// <summary>
    /// Compute Catmull-Rom interpolation.
    /// </summary>
    /// <param name="a">First position</param>
    /// <param name="b">Second position</param>
    /// <param name="c">Third position</param>
    /// <param name="d">Fourth position</param>
    /// <param name="wf">Weighting factor</param>
    /// <param name="result">Catmull-Rom interpolated vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void CatmullRom(in Vector2 a, in Vector2 b, in Vector2 c, in Vector2 d, float wf, out Vector2 result)
    {
      float wfSquared = wf * wf;
      float wfCubed = wf * wfSquared;
      result.X = 0.5f * ((((2.0f * b.X) + ((-a.X + c.X) * wf)) + (((((2.0f * a.X) - (5.0f * b.X)) + (4.0f * c.X)) - d.X) * wfSquared)) + ((((-a.X + (3.0f * b.X)) - (3.0f * c.X)) + d.X) * wfCubed));
      result.Y = 0.5f * ((((2.0f * b.Y) + ((-a.Y + c.Y) * wf)) + (((((2.0f * a.Y) - (5.0f * b.Y)) + (4.0f * c.Y)) - d.Y) * wfSquared)) + ((((-a.Y + (3.0f * b.Y)) - (3.0f * c.Y)) + d.Y) * wfCubed));
    }

    /// <summary>
    /// Compute a Hermite spline interpolation.
    /// </summary>
    /// <param name="a">First position</param>
    /// <param name="tangentA">First vector's tangent</param>
    /// <param name="b">Second position</param>
    /// <param name="tangentB">Second vector's tangent</param>
    /// <param name="wf">Weighting factor</param>
    /// <returns>Hermite interpolated vecto</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector2 Hermite(in Vector2 a, in Vector2 tangentA, in Vector2 b, in Vector2 tangentB, float wf)
    {
      float wfSquared = wf * wf;
      float wfCubed = wfSquared * wf;
      float h1 = ((2.0f * wfCubed) - (3.0f * wfSquared)) + 1.0f;
      float h2 = (-2.0f * wfCubed) + (3.0f * wfSquared);
      float h3 = (wfCubed - (2.0f * wfSquared)) + wf;
      float h4 = wfCubed - wfSquared;

      Vector2 result;
      result.X = (((a.X * h1) + (b.X * h2)) + (tangentA.X * h3)) + (tangentB.X * h4);
      result.Y = (((a.Y * h1) + (b.Y * h2)) + (tangentA.Y * h3)) + (tangentB.Y * h4);

      return result;
    }

    /// <summary>
    /// Compute a Hermite spline interpolation.
    /// </summary>
    /// <param name="a">First position</param>
    /// <param name="tangentA">First vector's tangent</param>
    /// <param name="b">Second position</param>
    /// <param name="tangentB">Second vector's tangent</param>
    /// <param name="wf">Weighting factor</param>
    /// <param name="result">Hermite interpolated vecto</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Hermite(in Vector2 a, in Vector2 tangentA, in Vector2 b, in Vector2 tangentB, float wf, out Vector2 result)
    {
      float wfSquared = wf * wf;
      float wfCubed = wfSquared * wf;
      float h1 = ((2.0f * wfCubed) - (3.0f * wfSquared)) + 1.0f;
      float h2 = (-2.0f * wfCubed) + (3.0f * wfSquared);
      float h3 = (wfCubed - (2.0f * wfSquared)) + wf;
      float h4 = wfCubed - wfSquared;

      result.X = (((a.X * h1) + (b.X * h2)) + (tangentA.X * h3)) + (tangentB.X * h4);
      result.Y = (((a.Y * h1) + (b.Y * h2)) + (tangentA.Y * h3)) + (tangentB.Y * h4);
    }

    /// <summary>
    /// Compute a cubic interpolation between two vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <param name="wf">Weighting factor (between 0 and 1.0)</param>
    /// <returns>Cubic interpolated vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector2 SmoothStep(in Vector2 a, in Vector2 b, float wf)
    {
      Vector2 result;
      float amt = MathHelper.Clamp(wf, 0.0f, 1.0f);
      amt = (amt * amt) * (3.0f - (2.0f * amt));
      result.X = a.X + ((b.X - a.X) * amt);
      result.Y = a.Y + ((b.Y - a.Y) * amt);

      return result;
    }

    /// <summary>
    /// Compute a cubic interpolation between two vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <param name="wf">Weighting factor (between 0 and 1.0)</param>
    /// <param name="result">Cubic interpolated vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void SmoothStep(in Vector2 a, in Vector2 b, float wf, out Vector2 result)
    {
      float amt = MathHelper.Clamp(wf, 0.0f, 1.0f);
      amt = (amt * amt) * (3.0f - (2.0f * amt));
      result.X = a.X + ((b.X - a.X) * amt);
      result.Y = a.Y + ((b.Y - a.Y) * amt);
    }

    /// <summary>
    /// Linearly interpolates between two vectors.
    /// </summary>
    /// <param name="a">Starting vector</param>
    /// <param name="b">Ending vector</param>
    /// <param name="percent">Amount to interpolate by</param>
    /// <returns>Linear interpolated vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector2 Lerp(in Vector2 a, in Vector2 b, float percent)
    {
      // Better for floating point precision than a + (b - a) * percent;
      float oneMinusPercent = 1.0f - percent;

      Vector2 result;
      result.X = a.X * oneMinusPercent + b.X * percent;
      result.Y = a.Y * oneMinusPercent + b.Y * percent;

      return result;
    }

    /// <summary>
    /// Linearly interpolates between two vectors.
    /// </summary>
    /// <param name="a">Starting vector</param>
    /// <param name="b">Ending vector</param>
    /// <param name="percent">Amount to interpolate by</param>
    /// <param name="result">Linear interpolated vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Lerp(in Vector2 a, in Vector2 b, float percent, out Vector2 result)
    {
      // Better for floating point precision than a + (b - a) * percent;
      float oneMinusPercent = 1.0f - percent;

      result.X = a.X * oneMinusPercent + b.X * percent;
      result.Y = a.Y * oneMinusPercent + b.Y * percent;
    }

    /// <summary>
    /// Compute the reflection vector off a surface with the specified normal.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="normal">Surface normal (unit vector)</param>
    /// <returns>Reflected vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector2 Reflect(in Vector2 value, in Vector2 normal)
    {
      Vector2 result;
      float dot = (value.X * normal.X) + (value.Y * normal.Y);
      result.X = value.X - ((2.0f * dot) * normal.X);
      result.Y = value.Y - ((2.0f * dot) * normal.Y);

      return result;
    }

    /// <summary>
    /// Compute the reflection vector off a surface with the specified normal.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="normal">Surface normal (unit vector)</param>
    /// <param name="result">Reflected vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Reflect(in Vector2 value, in Vector2 normal, out Vector2 result)
    {
      float dot = (value.X * normal.X) + (value.Y * normal.Y);
      result.X = value.X - ((2.0f * dot) * normal.X);
      result.Y = value.Y - ((2.0f * dot) * normal.Y);
    }

    /// <summary>
    /// Transfroms the specified vector by the given <see cref="Matrix"/>.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="m">Transformation matrix</param>
    /// <returns>Transformed vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector2 Transform(in Vector2 value, in Matrix m)
    {
      float x = (value.X * m.M11) + (value.Y * m.M21) + m.M41;
      float y = (value.X * m.M12) + (value.Y * m.M22) + m.M42;

      Vector2 result;
      result.X = x;
      result.Y = y;

      return result;
    }

    /// <summary>
    /// Transfroms the specified vector by the given <see cref="Matrix"/>. The vector is treated
    /// as a row vector, so the multiplication is v*M.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="m">Transformation matrix</param>
    /// <param name="result">Transformed vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Transform(in Vector2 value, in Matrix m, out Vector2 result)
    {
      float x = (value.X * m.M11) + (value.Y * m.M21) + m.M41;
      float y = (value.X * m.M12) + (value.Y * m.M22) + m.M42;

      result.X = x;
      result.Y = y;
    }

    /// <summary>
    /// Transforms a span of vectors by the given <see cref="Matrix"/>. The vector is treated
    /// as a row vector, so the multiplication is v*M.
    /// </summary>
    /// <param name="source">Span of vectors to be transformed</param>
    /// <param name="m">Transformation matrix</param>
    /// <param name="destination">Span to store transformed vectors, this may be the same as the source span</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the destination span is smaller than the source span.</exception>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Transform(ReadOnlySpan<Vector2> source, in Matrix m, Span<Vector2> destination)
    {
      if (destination.Length < source.Length)
        throw new ArgumentOutOfRangeException(nameof(destination), StringLocalizer.Instance.GetLocalizedString("DestinationArraySmallerThanSource"));

      for (int i = 0; i < source.Length; i++)
        Transform(source[i], m, out destination[i]);
    }

    /// <summary>
    /// Transfroms the specified vector by the given <see cref="Quaternion"/>.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="q">Quaternion rotation</param>
    /// <returns>Transformed vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector2 Transform(in Vector2 value, in Quaternion q)
    {
      float x2 = q.X + q.X;
      float y2 = q.Y + q.Y;
      float z2 = q.Z + q.Z;

      float wz2 = q.W * z2;

      float xx2 = q.X * x2;
      float xy2 = q.X * y2;

      float yy2 = q.Y * y2;
      float zz2 = q.Z * z2;

      float x = (value.X * ((1.0f - yy2) - zz2)) + (value.Y * (xy2 - wz2));
      float y = (value.X * (xx2 + wz2)) + (value.Y * ((1.0f - xx2) - zz2));

      Vector2 result;
      result.X = x;
      result.Y = y;

      return result;
    }

    /// <summary>
    /// Transfroms the specified vector by the given <see cref="Quaternion"/>.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="q">Quaternion rotation</param>
    /// <param name="result">Transformed vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Transform(in Vector2 value, in Quaternion q, out Vector2 result)
    {
      float x2 = q.X + q.X;
      float y2 = q.Y + q.Y;
      float z2 = q.Z + q.Z;

      float wz2 = q.W * z2;

      float xx2 = q.X * x2;
      float xy2 = q.X * y2;

      float yy2 = q.Y * y2;
      float zz2 = q.Z * z2;

      float x = (value.X * ((1.0f - yy2) - zz2)) + (value.Y * (xy2 - wz2));
      float y = (value.X * (xx2 + wz2)) + (value.Y * ((1.0f - xx2) - zz2));

      result.X = x;
      result.Y = y;
    }

    /// <summary>
    /// Transforms a span of vectors by the given <see cref="Quaternion"/>.
    /// </summary>
    /// <param name="source">Span of vectors to be transformed</param>
    /// <param name="q">Quaternion rotation</param>
    /// <param name="destination">Span to store transformed vectors, this may be the same as the source span</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the destination span is smaller than the source span.</exception>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Transform(ReadOnlySpan<Vector2> source, in Quaternion q, Span<Vector2> destination)
    {
      if (destination.Length < source.Length)
        throw new ArgumentOutOfRangeException(nameof(destination), StringLocalizer.Instance.GetLocalizedString("DestinationArraySmallerThanSource"));

      for (int i = 0; i < source.Length; i++)
        Transform(source[i], q, out destination[i]);
    }

    /// <summary>
    /// Performs a normal transformation using the given <see cref="Matrix"/>. The vector is treated
    /// as a row vector, so the multiplication is v*M.
    /// </summary>
    /// <param name="normal">Normal vector to transform</param>
    /// <param name="m">Transformation matrix</param>
    /// <returns>Transformed normal</returns>
    /// <remarks>A normal transform performs the transformation with the assumption that the w component is zero. This causes the fourth
    /// row and fourth column of the matrix to be unused. The end result is a vector that is not translated, but is rotated/scaled. This is preferred for
    /// normalized vectors that act as normals and only represent directions.</remarks>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector2 TransformNormal(in Vector2 normal, in Matrix m)
    {
      float x = (normal.X * m.M11) + (normal.Y * m.M21);
      float y = (normal.X * m.M12) + (normal.Y * m.M22);

      Vector2 result;
      result.X = x;
      result.Y = y;

      return result;
    }

    /// <summary>
    /// Performs a normal transformation using the given <see cref="Matrix"/>. The vector is treated
    /// as a row vector, so the multiplication is v*M.
    /// </summary>
    /// <param name="normal">Normal vector to transform</param>
    /// <param name="m">Transformation matrix</param>
    /// <param name="result">Transformed normal</param>
    /// <remarks>A normal transform performs the transformation with the assumption that the w component is zero. This causes the fourth
    /// row and fourth column of the matrix to be unused. The end result is a vector that is not translated, but is rotated/scaled. This is preferred for
    /// normalized vectors that act as normals and only represent directions.</remarks>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void TransformNormal(in Vector2 normal, in Matrix m, out Vector2 result)
    {
      float x = (normal.X * m.M11) + (normal.Y * m.M21);
      float y = (normal.X * m.M12) + (normal.Y * m.M22);

      result.X = x;
      result.Y = y;
    }

    /// <summary>
    /// Performs a normal transformation on a span of normal vectors by the given <see cref="Matrix"/>. The vector is treated
    /// as a row vector, so the multiplication is v*M.
    /// </summary>
    /// <param name="source">Span of normal vectors to be transformed</param>
    /// <param name="m">Transformation matrix</param>
    /// <param name="destination">Span to store transformed normal vectors, this may be the same as the source span</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the destination span is smaller than the source span.</exception>
    /// <remarks>A normal transform performs the transformation with the assumption that the w component is zero. This causes the fourth
    /// row and fourth column of the matrix to be unused. The end result is a vector that is not translated, but is rotated/scaled. This is preferred for
    /// normalized vectors that act as normals and only represent directions.</remarks>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void TransformNormal(ReadOnlySpan<Vector2> source, in Matrix m, Span<Vector2> destination)
    {
      if (destination.Length < source.Length)
        throw new ArgumentOutOfRangeException(nameof(destination), StringLocalizer.Instance.GetLocalizedString("DestinationArraySmallerThanSource"));

      for (int i = 0; i < source.Length; i++)
        TransformNormal(source[i], m, out destination[i]);
    }

    /// <summary>
    /// Performs a coordinate transformation using the given <see cref="Matrix"/>.
    /// </summary>
    /// <param name="coordinate">Coordinate vector to transform</param>
    /// <param name="m">Transformation matrix</param>
    /// <returns>Transformed coordinate</returns>
    /// <remarks>A coordinate transform performs the transformation with the assumption that the w component is one. The four dimensional vector
    /// obtained from the transformation operation has each component in the vector divided by the w component. This forces the w component to
    /// be one and therefore makes the vector homogeneous. The homogeneous vector is often preferred when working with coordinates as the w component can be safely
    /// ignored.</remarks>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector2 TransformCoordinate(in Vector2 coordinate, in Matrix m)
    {
      float x = (coordinate.X * m.M11) + (coordinate.Y * m.M21) + m.M41;
      float y = (coordinate.X * m.M12) + (coordinate.Y * m.M22) + m.M42;
      float w = 1.0f / ((coordinate.X * m.M14) + (coordinate.Y * m.M24) + m.M44);

      Vector2 result;
      result.X = x * w;
      result.Y = y * w;

      return result;
    }

    /// <summary>
    /// Performs a coordinate transformation using the given <see cref="Matrix"/>.
    /// </summary>
    /// <param name="coordinate">Coordinate vector to transform</param>
    /// <param name="m">Transformation matrix</param>
    /// <param name="result">Transformed coordinate</param>
    /// <remarks>A coordinate transform performs the transformation with the assumption that the w component is one. The four dimensional vector
    /// obtained from the transformation operation has each component in the vector divided by the w component. This forces the w component to
    /// be one and therefore makes the vector homogeneous. The homogeneous vector is often preferred when working with coordinates as the w component can be safely
    /// ignored.</remarks>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void TransformCoordinate(in Vector2 coordinate, in Matrix m, out Vector2 result)
    {
      float x = (coordinate.X * m.M11) + (coordinate.Y * m.M21) + m.M41;
      float y = (coordinate.X * m.M12) + (coordinate.Y * m.M22) + m.M42;
      float w = 1.0f / ((coordinate.X * m.M14) + (coordinate.Y * m.M24) + m.M44);

      result.X = x * w;
      result.Y = y * w;
    }

    /// <summary>
    /// Performs a coordinate transformation on a span of coordinate vectors by the given <see cref="Matrix"/>.
    /// </summary>
    /// <param name="source">Span of coordinate vectors to be transformed</param>
    /// <param name="m">Transformation matrix</param>
    /// <param name="destination">Span to store transformed coordinate vectors, this may be the same as the source span</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the destination span is smaller than the source span.</exception>
    /// <remarks>A coordinate transform performs the transformation with the assumption that the w component is one. The four dimensional vector
    /// obtained from the transformation operation has each component in the vector divided by the w component. This forces the w component to
    /// be one and therefore makes the vector homogeneous. The homogeneous vector is often preferred when working with coordinates as the w component can be safely
    /// ignored.</remarks>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void TransformCoordinate(ReadOnlySpan<Vector2> source, in Matrix m, Span<Vector2> destination)
    {
      if (destination.Length < source.Length)
        throw new ArgumentOutOfRangeException(nameof(destination), StringLocalizer.Instance.GetLocalizedString("DestinationArraySmallerThanSource"));

      for (int i = 0; i < source.Length; i++)
        TransformCoordinate(source[i], m, out destination[i]);
    }

    /// <summary>
    /// Orthogonalizes a span of vectors using the modified Gram-Schmidt process. Source and destination should not be the same spans.
    /// </summary>
    /// <param name="source">Span of coordinate vectors to be transformed</param>
    /// <param name="destination">Span to store transformed coordinate vectors</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the destination span is smaller than the source span.</exception>
    /// <remarks>Orthonormalization is the process of making all vectors orthogonal to each other. This means that any given vector will be orthogonal to any other 
    /// given vector in the list. Because this method uses the modified Gram-Schmidt process, the resulting vectors tend to be numerically unstable. 
    /// The numeric stability decreases according to the vectors position in the span, so that the first vector is the most stable and the last vector is the least stable.</remarks>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Orthogonalize(ReadOnlySpan<Vector2> source, Span<Vector2> destination)
    {
      if (destination.Length < source.Length)
        throw new ArgumentOutOfRangeException(nameof(destination), StringLocalizer.Instance.GetLocalizedString("DestinationArraySmallerThanSource"));

      for (int i = 0; i < source.Length; i++)
      {
        Vector2 curr = source[i];
        for (int j = 0; j < i; j++)
        {
          ref Vector2 next = ref destination[j];
          float dotuv = Vector2.Dot(next, curr);
          float dotuu = Vector2.Dot(next, next);

          Vector2.Multiply(next, dotuv / dotuu, out next);
          Vector2.Subtract(curr, next, out curr);
        }

        destination[i] = curr;
      }
    }

    /// <summary>
    /// Orthonormalizes a span of vectors using the modified Gram-Schmidt process. Source and destination should not be the same spans.
    /// </summary>
    /// <param name="destination">Span to store transformed coordinate vectors</param>
    /// <param name="source">Span of coordinate vectors to be transformed</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the destination span is smaller than the source span.</exception>
    /// <remarks>Orthonormalization is the process of making all vectors orthogonal to each other and making all vectors of unit length. This means
    /// that any given vector will be orthogonal to any other given vector in the list. Because this
    /// method uses the modified Gram-Schmidt process, the resulting vectors tend to be numerically unstable. The numeric stability decreases according to the vectors position in
    /// the span, so that the first vector is the most stable and the last vector is the least stable.</remarks>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Orthonormalize(ReadOnlySpan<Vector2> source, Span<Vector2> destination)
    {
      if (destination.Length < source.Length)
        throw new ArgumentOutOfRangeException(nameof(destination), StringLocalizer.Instance.GetLocalizedString("DestinationArraySmallerThanSource"));

      for (int i = 0; i < source.Length; i++)
      {
        Vector2 curr = source[i];
        for (int j = 0; j < i; j++)
        {
          ref Vector2 next = ref destination[j];
          float dotuv = Vector2.Dot(next, curr);

          Vector2.Multiply(next, dotuv, out next);
          Vector2.Subtract(curr, next, out curr);
        }

        curr.Normalize();
        destination[i] = curr;
      }
    }

    /// <summary>
    /// Implicitly converts from <see cref="Vector2"/> to <see cref="System.Numerics.Vector2"/> for interop.
    /// </summary>
    /// <param name="v">Vector to convert.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static implicit operator System.Numerics.Vector2(Vector2 v)
    {
      return new System.Numerics.Vector2(v.X, v.Y);
    }

    /// <summary>
    /// Implicitly converts from <see cref="System.Numerics.Vector2"/> to <see cref="Vector2"/> for interop.
    /// </summary>
    /// <param name="v">Vector to convert.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static implicit operator Vector2(System.Numerics.Vector2 v)
    {
      return new Vector2(v.X, v.Y);
    }

    /// <summary>
    /// Tests equality between two vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>True if components are equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator ==(Vector2 a, Vector2 b)
    {
      return (MathF.Abs(a.X - b.X) <= MathHelper.ZeroTolerance) && (MathF.Abs(a.Y - b.Y) <= MathHelper.ZeroTolerance);
    }

    /// <summary>
    /// Tests inequality between two vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>True if components are not equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator !=(Vector2 a, Vector2 b)
    {
      return (MathF.Abs(a.X - b.X) > MathHelper.ZeroTolerance) || (MathF.Abs(a.Y - b.Y) > MathHelper.ZeroTolerance);
    }

    /// <summary>
    /// Adds the two vectors together.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Sum of the two vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector2 operator +(Vector2 a, Vector2 b)
    {
      Vector2 result;
      result.X = a.X + b.X;
      result.Y = a.Y + b.Y;

      return result;
    }

    /// <summary>
    /// Flips the signs of the components of the specified vector.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <returns>Negated vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector2 operator -(Vector2 value)
    {
      Vector2 result;
      result.X = -value.X;
      result.Y = -value.Y;

      return result;
    }

    /// <summary>
    /// Subtracts vector b from vector a.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Difference of the two vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector2 operator -(Vector2 a, Vector2 b)
    {
      Vector2 result;
      result.X = a.X - b.X;
      result.Y = a.Y - b.Y;

      return result;
    }

    /// <summary>
    /// Multiplies two vectors together.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Multiplied vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector2 operator *(Vector2 a, Vector2 b)
    {
      Vector2 result;
      result.X = a.X * b.X;
      result.Y = a.Y * b.Y;

      return result;
    }

    /// <summary>
    /// Multiplies a vector by a scaling factor.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="scale">Amount to scale</param>
    /// <returns>Multiplied vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector2 operator *(Vector2 value, float scale)
    {
      Vector2 result;
      result.X = value.X * scale;
      result.Y = value.Y * scale;

      return result;
    }

    /// <summary>
    /// Multiplies a vector by a scaling factor.
    /// </summary>
    /// <param name="scale">Amount to scale</param>
    /// <param name="value">Source vector</param>
    /// <returns>Multiplied vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector2 operator *(float scale, Vector2 value)
    {
      Vector2 result;
      result.X = value.X * scale;
      result.Y = value.Y * scale;

      return result;
    }

    /// <summary>
    /// Divides the components of vector a by those of vector b.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Divisor vector</param>
    /// <returns>Quotient of the two vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector2 operator /(Vector2 a, Vector2 b)
    {
      Vector2 result;
      result.X = a.X / b.X;
      result.Y = a.Y / b.Y;

      return result;
    }

    /// <summary>
    /// Divides a vector by a scalar value.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="divisor">Divisor scalar</param>
    /// <returns>Divided vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector2 operator /(Vector2 value, float divisor)
    {
      Vector2 result;
      float invDivisor = 1.0f / divisor;
      result.X = value.X * invDivisor;
      result.Y = value.Y * invDivisor;

      return result;
    }

    /// <summary>
    /// Flips the signs of the components of the vector.
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void Negate()
    {
      X = -X;
      Y = -Y;
    }

    /// <summary>
    /// Compute the length (magnitude) of the vector.
    /// </summary>
    /// <returns>Length</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float Length()
    {
      float lengthSquared = (X * X) + (Y * Y);
      return MathF.Sqrt(lengthSquared);
    }

    /// <summary>
    /// Compute the length (magnitude) squared of the vector.
    /// </summary>
    /// <returns>Length squared</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float LengthSquared()
    {
      return (X * X) + (Y * Y);
    }

    /// <summary>
    /// Normalize the vector to a unit vector, which results in a vector with a magnitude of 1 but
    /// preserves the direction the vector was pointing in.
    /// </summary>
    /// <returns>The magnitude (length) of the vector.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public float Normalize()
    {
      float lengthSquared = (X * X) + (Y * Y);

      if (lengthSquared > MathHelper.ZeroTolerance)
      {
        float magnitnude = MathF.Sqrt(lengthSquared);

        float invLength = 1.0f / MathF.Sqrt(lengthSquared);
        X *= invLength;
        Y *= invLength;

        return magnitnude;
      }

      return 0.0f;
    }

    /// <summary>
    /// Tests equality between the vector and another vector.
    /// </summary>
    /// <param name="other">Vector to test against</param>
    /// <returns>True if components are equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    readonly bool IEquatable<Vector2>.Equals(Vector2 other)
    {
      return (MathF.Abs(other.X - X) <= MathHelper.ZeroTolerance) && (MathF.Abs(other.Y - Y) <= MathHelper.ZeroTolerance);
    }

    /// <summary>
    /// Tests equality between the vector and another vector.
    /// </summary>
    /// <param name="other">Vector to test against</param>
    /// <returns>True if components are equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Equals(in Vector2 other)
    {
      return (MathF.Abs(other.X - X) <= MathHelper.ZeroTolerance) && (MathF.Abs(other.Y - Y) <= MathHelper.ZeroTolerance);
    }

    /// <summary>
    /// Tests equality between the vector and another vector.
    /// </summary>
    /// <param name="other">Vector to test against</param>
    /// <param name="tolerance">Tolerance</param>
    /// <returns>True if components are equal within tolerance, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Equals(in Vector2 other, float tolerance)
    {
      return (MathF.Abs(other.X - X) <= tolerance) && (MathF.Abs(other.Y - Y) <= tolerance);
    }

    /// <summary>
    /// Tests equality between the vector and XY values.
    /// </summary>
    /// <param name="x">X component.</param>
    /// <param name="y">Y component.</param>
    /// <param name="tolerance">Optional tolerance, defaults to <see cref="MathHelper.ZeroTolerance"/>.</param>
    /// <returns>True if components are equal within tolerance, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Equals(float x, float y, float tolerance = MathHelper.ZeroTolerance)
    {
      return (MathF.Abs(X - x) <= tolerance) && (MathF.Abs(Y - y) <= tolerance);
    }

    /// <summary>
    /// Determines whether the specified <see cref="System.Object" /> is equal to this instance.
    /// </summary>
    /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
    /// <returns>True if the specified <see cref="System.Object" /> is equal to this instance; otherwise, false.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is Vector2)
        return Equals((Vector2) obj);

      return false;
    }

    /// <summary>
    /// Determines whether this vector is nearly (0, 0).
    /// </summary>
    /// <param name="tol">Optional tolerance.</param>
    /// <returns>True if this vector is almost zero, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool IsAlmostZero(float tol = MathHelper.ZeroTolerance)
    {
      ref readonly Vector2 zero = ref Vector2.Zero;
      return Equals(zero, tol);
    }

    /// <summary>
    /// Determines whether this vector is nearly (1, 1).
    /// </summary>
    /// <param name="tol">Optional tolerance.</param>
    /// <returns>True if this vector is almost zero, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool IsAlmostOne(float tol = MathHelper.ZeroTolerance)
    {
      ref readonly Vector2 one = ref Vector2.One;
      return Equals(one, tol);
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
    public readonly override int GetHashCode()
    {
      unchecked
      {
        return X.GetHashCode() + Y.GetHashCode();
      }
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly override string ToString()
    {
      CultureInfo info = CultureInfo.CurrentCulture;
      return String.Format(info, "X: {0} Y: {1}",
          new Object[] { X.ToString(info), Y.ToString(info) });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="format">The format</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(String? format)
    {
      if (format is null)
        return ToString();

      CultureInfo info = CultureInfo.CurrentCulture;
      return String.Format(info, "X: {0} Y: {1}",
          new Object[] { X.ToString(format, info), Y.ToString(format, info) });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="formatProvider">The format provider.</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(IFormatProvider? formatProvider)
    {
      if (formatProvider is null)
        return ToString();

      return String.Format(formatProvider, "X: {0} Y: {1}",
          new Object[] { X.ToString(formatProvider), Y.ToString(formatProvider) });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="format">The format.</param>
    /// <param name="formatProvider">The format provider.</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(string? format, IFormatProvider? formatProvider)
    {
      if (formatProvider is null)
        return ToString();

      if (format is null)
        return ToString(formatProvider);

      return String.Format(formatProvider, "X: {0} Y: {1}",
          new Object[] { X.ToString(format, formatProvider), Y.ToString(format, formatProvider) });
    }

    /// <summary>
    /// Writes the primitive data to the output.
    /// </summary>
    /// <param name="output">Primitive writer.</param>
    readonly void IPrimitiveValue.Write(IPrimitiveWriter output)
    {
      output.Write("X", X);
      output.Write("Y", Y);
    }

    /// <summary>
    /// Reads the primitive data from the input.
    /// </summary>
    /// <param name="input">Primitive reader.</param>
    void IPrimitiveValue.Read(IPrimitiveReader input)
    {
      X = input.ReadSingle("X");
      Y = input.ReadSingle("Y");
    }
  }
}