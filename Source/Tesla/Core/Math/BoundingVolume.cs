﻿/*
* Copyright (c) 2010-2024 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Tesla.Content;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Base class for all bounding volumes, which are simple logical 3D shapes that encapsulates some volume. Bounding volumes
  /// are used to represent a more complex 3D mesh, offering a variety of containment and intersection methods for collision detection/response,
  /// picking, or culling.
  /// </summary>
  [SavableVersion(1)]
  public abstract class BoundingVolume : IEquatable<BoundingVolume>, ISavable, IDeepCloneable, IPickable
  {
    private bool m_updateCorners;
    private Vector3[]? m_corners;

    /// <summary>
    /// Gets or sets the center of the bounding volume.
    /// </summary>
    public abstract Vector3 Center { get; set; }

    /// <summary>
    /// Gets the volume of the bounding volume.
    /// </summary>
    public abstract float Volume { get; }

    /// <summary>
    /// Gets the bounding type.
    /// </summary>
    public abstract BoundingType BoundingType { get; }

    /// <summary>
    /// Gets the number of corners.
    /// </summary>
    public virtual int CornerCount { get { return 8; } }

    /// <summary>
    /// Gets if the volume is minimum, where it has no volume.
    /// </summary>
    public bool IsMinimumBoundingVolume { get { return MathHelper.IsNearlyZero(Volume); } }

    /// <summary>
    /// Gets the buffer corners that represent the extremal points of this bounding volume.
    /// </summary>
    public ReadOnlySpan<Vector3> Corners
    {
      get
      {
        if (m_corners is null)
        {
          m_corners = new Vector3[CornerCount];
          m_updateCorners = true;
        }

        if (m_updateCorners)
        {
          ComputeCorners(m_corners);
          m_updateCorners = false;
        }

        return m_corners;
      }
    }

    /// <summary>
    /// Gets or sets whether or not corners of the bounding volume should be computed.
    /// </summary>
    protected bool UpdateCorners
    {
      get
      {
        return m_updateCorners;
      }
      set
      {
        m_updateCorners = value;
      }
    }

    /// <summary>
    /// For IPickable, always returns this bounding volume.
    /// </summary>
    BoundingVolume IPickable.WorldBounding { get { return this; } }

    /// <summary>
    /// Constructs a new instance of the <see cref="BoundingVolume"/> class.
    /// </summary>
    public BoundingVolume()
    {
      m_updateCorners = true;
    }

    #region Copy methods

    /// <summary>
    /// Sets the bounding volume by either copying the specified bounding volume if its the specified type, or computing a volume required to fully contain it.
    /// </summary>
    /// <param name="volume">Bounding volume to copy from</param>
    public abstract void Set(BoundingVolume? volume);

    /// <summary>
    /// Creates a copy of the bounding volume and returns a new instance.
    /// </summary>
    /// <returns>Copied bounding volume</returns>
    public abstract BoundingVolume Clone();

    /// <summary>
    /// Get a copy of the object.
    /// </summary>
    /// <returns>Cloned copy.</returns>
    IDeepCloneable IDeepCloneable.Clone()
    {
      return Clone();
    }

    #endregion

    #region DistanceTo methods

    /// <summary>
    /// Computes the distance from the center of the bounding volume to the point.
    /// </summary>
    /// <param name="point">Point in the same space as the bounding volume</param>
    /// <returns>Distance to the center of the bounding volume.</returns>
    public float DistanceToCenter(in Vector3 point)
    {
      Vector3 center = Center;

      return Vector3.Distance(center, point);
    }

    /// <summary>
    /// Computes the distance squared from the center of the bounding volume to the point.
    /// </summary>
    /// <param name="point">Point in the same space as the bounding volume</param>
    /// <returns>Distance squared to the center of the bounding volume.</returns>
    public float DistanceSquaredToCenter(in Vector3 point)
    {
      Vector3 center = Center;

      return Vector3.Distance(center, point);
    }

    /// <summary>
    /// Computes the closest point on the volume from the given point in space.
    /// </summary>
    /// <param name="point">Point in space</param>
    /// <returns>Closest point on the edge of the volume.</returns>
    public Vector3 ClosestPointTo(in Vector3 point)
    {
      Vector3 result;
      ClosestPointTo(point, out result);

      return result;
    }

    /// <summary>
    /// Computes the distance from a given point to the edge of the volume.
    /// </summary>
    /// <param name="point">Point in space</param>
    /// <returns>Distance from the point to the edge of the volume.</returns>
    public abstract float DistanceTo(in Vector3 point);

    /// <summary>
    /// Computes the distance squared from a given point to the edge of the volume.
    /// </summary>
    /// <param name="point">Point in space</param>
    /// <returns>Distanced squared from the point to the edge of the volume.</returns>
    public abstract float DistanceSquaredTo(in Vector3 point);

    /// <summary>
    /// Computes the closest point on the volume from the given point in space.
    /// </summary>
    /// <param name="point">Point in space</param>
    /// <param name="result">Closest point on the edge of the volume.</param>
    public abstract void ClosestPointTo(in Vector3 point, out Vector3 result);

    #endregion

    #region ComputeFromPoints methods

    /// <summary>
    /// Computes a minimum bounding volume that encloses the specified points in space.
    /// </summary>
    /// <param name="points">Points in space</param>
    /// <param name="subMeshRange">Optional range inside the buffer that represents the mesh to compute bounds for. Note: the base vertex offset will be ignored.</param>
    public abstract void ComputeFromPoints(ReadOnlySpan<Vector3> points, in SubMeshRange? subMeshRange = null);

    /// <summary>
    /// Computes a minimum bounding volume that encloses the specified indexed points in space.
    /// </summary>
    /// <param name="points">Points in space.</param>
    /// <param name="indices">Point indices denoting location in the point buffer.</param>
    /// <param name="subMeshRange">Optional range inside the buffer that represents the mesh to compute bounds for.</param>
    public abstract void ComputeFromIndexedPoints(ReadOnlySpan<Vector3> points, IndexData indices, in SubMeshRange? subMeshRange = null);

    #endregion

    #region Containment methods

    /// <summary>
    /// Determines if the specified set of points are contained inside the bounding volume.
    /// </summary>
    /// <param name="points">Set of points to test against.</param>
    /// <returns>Type of containment</returns>
    public ContainmentType Contains(ReadOnlySpan<Vector3> points)
    {
      if (points.IsEmpty)
        return ContainmentType.Outside;

      bool outside = false, inside = false, intersects = false;

      for (int i = 0; i < points.Length; i++)
      {
        ref readonly Vector3 pt = ref points[i];
        switch (Contains(pt))
        {
          case ContainmentType.Outside:
            outside = true;
            break;
          case ContainmentType.Inside:
            inside = true;
            break;
          case ContainmentType.Intersects:
            intersects = true;
            break;
        }
      }

      if ((outside && inside) || intersects)
      {
        return ContainmentType.Intersects;
      }
      else if (inside && !outside)
      {
        return ContainmentType.Inside;
      }
      else
      {
        return ContainmentType.Outside;
      }
    }

    /// <summary>
    /// Determines if the specified point is contained inside the bounding volume.
    /// </summary>
    /// <param name="point">Point to test against.</param>
    /// <returns>Type of containment</returns>
    public abstract ContainmentType Contains(in Vector3 point);

    /// <summary>
    /// Determines if the specified segment line is contained inside the bounding volume.
    /// </summary>
    /// <param name="line">Segment to test against.</param>
    /// <returns>Type of containment.</returns>
    public abstract ContainmentType Contains(in Segment line);

    /// <summary>
    /// Determines if the specified triangle is contained inside the bounding volume.
    /// </summary>
    /// <param name="triangle">Triangle to test against.</param>
    /// <returns>Type of containment.</returns>
    public abstract ContainmentType Contains(in Triangle triangle);

    /// <summary>
    /// Determines if the specified ellipse is contained inside the bounding volume.
    /// </summary>
    /// <param name="ellipse">Ellipse to test against.</param>
    /// <returns>Type of containment.</returns>
    public abstract ContainmentType Contains(in Ellipse ellipse);

    /// <summary>
    /// Determines if the specified bounding volume is contained inside the bounding volume.
    /// </summary>
    /// <param name="volume">Bounding volume to test against.</param>
    /// <returns>Type of containment.</returns>
    public abstract ContainmentType Contains(BoundingVolume? volume);

    #endregion

    #region Intersection methods

    /// <summary>
    /// Determines if the specified triangle intersects with the bounding volume.
    /// </summary>
    /// <param name="triangle">Triangle to test against.</param>
    /// <returns>True if the bounding volume intersects with the triangle, false otherwise.</returns>
    public bool Intersects(in Triangle triangle)
    {
      return Contains(triangle) == ContainmentType.Intersects;
    }

    /// <summary>
    /// Determines if the specified ellipse intersects with the bounding volume.
    /// </summary>
    /// <param name="ellipse">Ellipse to test against.</param>
    /// <returns>True if the bounding volume intersects with the ellipse, false otherwise.</returns>
    public bool Intersects(in Ellipse ellipse)
    {
      return Contains(ellipse) == ContainmentType.Intersects;
    }

    /// <summary>
    /// Determines if the specified ray intersects with the bounding volume.
    /// </summary>
    /// <param name="ray">Ray to test against.</param>
    /// <returns>True if the bounding volume intersects with the ray, false otherwise.</returns>
    public abstract bool Intersects(in Ray ray);

    /// <summary>
    /// Determines if the specified ray intersects with the bounding volume.
    /// </summary>
    /// <param name="ray">Ray to test against.</param>
    /// <param name="result">Bounding intersection result.</param>
    /// <returns>True if the bounding volume intersects with the ray, false otherwise.</returns>
    public abstract bool Intersects(in Ray ray, out BoundingIntersectionResult result);

    /// <summary>
    /// Determines if the specified segment line intersects with the bounding volume.
    /// </summary>
    /// <param name="line">Segment line to test against.</param>
    /// <returns>True if the bounding volume intersects with the segment line, false otherweise.</returns>
    public abstract bool Intersects(in Segment line);

    /// <summary>
    /// Determines if the specified segment line intersects with the bounding volume.
    /// </summary>
    /// <param name="line">Segment line to test against.</param>
    /// <param name="result">Bounding intersection result.</param>
    /// <returns>True if the bounding volume intersects with the segment line, false otherweise.</returns>
    public abstract bool Intersects(in Segment line, out BoundingIntersectionResult result);

    /// <summary>
    /// Determines if the specified plane intersects with the bounding volume.
    /// </summary>
    /// <param name="plane">Plane to test against.</param>
    /// <returns>Type of plane intersection.</returns>
    public abstract PlaneIntersectionType Intersects(in Plane plane);

    /// <summary>
    /// Determines if the specified bounding volume intersects with the bounding volume.
    /// </summary>
    /// <param name="volume">Other bounding volume to test against.</param>
    /// <returns>True if the two volumes intersect with one another, false otherwise.</returns>
    public abstract bool Intersects(BoundingVolume? volume);

    #endregion

    #region Merge methods

    /// <summary>
    /// Creates a new instance of the bounding volume and merges it with the specified bounding volume, resulting in a volume
    /// that encloses both.
    /// </summary>
    /// <param name="volume">Bounding volume to merge with.</param>
    /// <returns>New bounding volume instance that encloses both volumes.</returns>
    public BoundingVolume MergeCopy(BoundingVolume? volume)
    {
      BoundingVolume bv = Clone();
      if (volume is not null)
        bv.Merge(volume);

      return bv;
    }

    /// <summary>
    /// Merges the bounding volume with the specified bounding volume, resulting in a volume that encloses both.
    /// </summary>
    /// <param name="volume">Bounding volume to merge with.</param>
    public abstract void Merge(BoundingVolume? volume);

    #endregion

    #region Transform methods

    /// <summary>
    /// Creates a new instance of the bounding volume and transforms it by the Scale-Rotation-Translation (SRT) transform.
    /// </summary>
    /// <param name="transform">SRT transform.</param>
    /// <returns>New transformed bounding volume instance.</returns>
    public BoundingVolume TransformCopy(Transform transform)
    {
      BoundingVolume bv = Clone();
      bv.Transform(transform);

      return bv;
    }

    /// <summary>
    /// Creates a new instance of the bounding volume and transforms it by the Scale-Rotation-Translation (SRT) transform.
    /// </summary>
    /// <param name="scale">Scaling</param>
    /// <param name="rotation">Rotation</param>
    /// <param name="translation">Translation</param>
    /// <returns>New transformed bounding volume instance.</returns>
    public BoundingVolume TransformCopy(in Vector3 scale, in Quaternion rotation, in Vector3 translation)
    {
      BoundingVolume bv = Clone();

      bv.Transform(scale, rotation, translation);

      return bv;
    }

    /// <summary>
    /// Transforms the bounding volume by a Scale-Rotation-Translation (SRT) transform.
    /// </summary>
    /// <param name="transform">SRT transform.</param>
    public void Transform(Transform transform)
    {
      if (transform is null)
        return;

      Vector3 scale = transform.Scale;
      Quaternion rot = transform.Rotation;
      Vector3 trans = transform.Translation;

      Transform(scale, rot, trans);
    }

    /// <summary>
    /// Transforms the bounding volume by a Scale-Rotation-Translation (SRT) transform.
    /// </summary>
    /// <param name="scale">Scaling</param>
    /// <param name="rotation">Rotation</param>
    /// <param name="translation">Translation</param>
    public abstract void Transform(in Vector3 scale, in Quaternion rotation, in Vector3 translation);

    #endregion

    #region IEquatable methods

    /// <summary>
    /// Determines whether the specified <see cref="T:System.Object" /> is equal to the current <see cref="T:System.Object" />.
    /// </summary>
    /// <param name="obj">The object to compare with the current object.</param>
    /// <returns>True if the specified object  is equal to the current object; otherwise, false.</returns>
    public override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is BoundingVolume bv)
        return Equals(bv, MathHelper.ZeroTolerance);

      return false;
    }

    /// <summary>
    /// Tests equality between the bounding volume and the other bounding volume.
    /// </summary>
    /// <param name="other">Other bounding volume</param>
    /// <returns>True if the volume is of same type and same size/shape, false otherwise.</returns>
    public bool Equals([NotNullWhen(true)] BoundingVolume? other)
    {
      return Equals(other, MathHelper.ZeroTolerance);
    }

    /// <summary>
    /// Tests equality between the bounding volume and the other bounding volume.
    /// </summary>
    /// <param name="other">Other bounding volume</param>
    /// <param name="tolerance">Tolerance</param>
    /// <returns>True if the volume is of same type and same size/shape, false otherwise.</returns>
    public abstract bool Equals([NotNullWhen(true)] BoundingVolume? other, float tolerance);

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.</returns>
    public override int GetHashCode()
    {
      unchecked
      {
        return BoundingType.GetHashCode() + Volume.GetHashCode() + Center.GetHashCode();
      }
    }

    #endregion

    #region ISavable methods

    /// <summary>
    /// Reads the object data from the input.
    /// </summary>
    /// <param name="input">Savable reader</param>
    public abstract void Read(ISavableReader input);

    /// <summary>
    /// Writes the object data to the output.
    /// </summary>
    /// <param name="output">Savable writer</param>
    public abstract void Write(ISavableWriter output);

    #endregion

    #region Protected Methods

    /// <summary>
    /// Checks if the mesh range and buffer are valid.
    /// </summary>
    /// <param name="points">Buffer of vertices</param>
    /// <param name="subMeshRange">Optional range in the buffer.</param>
    /// <returns>True if the range and buffer are valid, false if otherwise.</returns>
    protected static bool IsValidRange(ReadOnlySpan<Vector3> points, in SubMeshRange? subMeshRange)
    {
      if (points.IsEmpty)
        return false;

      if (subMeshRange.HasValue)
      {
        SubMeshRange range = subMeshRange.Value;
        return range.Count > 0 && range.Offset >= 0 && ((range.Offset + range.Count) <= points.Length);
      }
      else
      {
        return points.Length > 0;
      }
    }

    /// <summary>
    /// Checks if the mesh range and buffers are valid.
    /// </summary>
    /// <param name="points">Buffer of vertices</param>
    /// <param name="indices">Buffer of indices</param>
    /// <param name="subMeshRange">Optional range in the index buffer.</param>
    /// <returns>True if the range and buffers are valid, false if otherwise.</returns>
    protected static bool IsValidRange(ReadOnlySpan<Vector3> points, IndexData indices, in SubMeshRange? subMeshRange)
    {
      if (points.IsEmpty || !indices.IsValid)
        return false;

      if (subMeshRange.HasValue)
      {
        SubMeshRange range = subMeshRange.Value;
        return range.Count > 0 && range.Offset >= 0 && range.BaseVertexOffset >= 0 && range.BaseVertexOffset < points.Length
            && ((range.Offset + range.Count) <= indices.Length);
      }
      else
      {
        return points.Length > 0 && indices.Length > 0;
      }
    }

    /// <summary>
    /// Extracts the valid range in the buffer at which to compute a bounding volume from.
    /// </summary>
    /// <param name="points">Buffer of vertices</param>
    /// <param name="subMeshRange">Optional range in the buffer.</param>
    /// <param name="offset">Offset from the start of the buffer.</param>
    /// <param name="count">Number of positions to read.</param>
    /// <returns>True if the range is valid, false if otherwise.</returns>
    protected static bool ExtractSubMeshRange(ReadOnlySpan<Vector3> points, in SubMeshRange? subMeshRange, out int offset, out int count)
    {
      offset = 0;
      count = 0;

      if (!IsValidRange(points, subMeshRange))
        return false;

      if (subMeshRange.HasValue)
      {
        SubMeshRange range = subMeshRange.Value;
        offset = range.Offset;
        count = range.Count;
      }
      else
      {
        count = points.Length;
      }

      return true;
    }

    /// <summary>
    /// Extracts the valid range in the buffer at which to compute a bounding volume from.
    /// </summary>
    /// <param name="points">Buffer of vertices</param>
    /// <param name="indices">Buffer of indices</param>
    /// <param name="subMeshRange">Optional range in the buffer.</param>
    /// <param name="offset">Offset from the start of the buffer.</param>
    /// <param name="count">Number of positions to read.</param>
    /// <param name="baseVertexOffset">Offset to add to the index, if applicable.</param>
    /// <returns>True if the range is valid, false if otherwise.</returns>
    protected static bool ExtractSubMeshRange(ReadOnlySpan<Vector3> points, IndexData indices, in SubMeshRange? subMeshRange, out int offset, out int count, out int baseVertexOffset)
    {
      offset = 0;
      count = 0;
      baseVertexOffset = 0;

      if (!IsValidRange(points, indices, subMeshRange))
        return false;

      if (subMeshRange.HasValue)
      {
        SubMeshRange range = subMeshRange.Value;
        offset = range.Offset;
        baseVertexOffset = range.BaseVertexOffset;
        count = range.Count;
      }
      else
      {
        count = indices.Length;
      }

      return true;
    }

    /// <summary>
    /// Determines if the specified unknown bounding volume is contained within the bounding volume. This is intended only as a 
    /// finaly, fool-proof measure as it tests corner containment.
    /// </summary>
    /// <param name="volume">Unknown other bounding volume to test against.</param>
    /// <returns>Type of containment.</returns>
    protected ContainmentType ContainsGeneral(BoundingVolume? volume)
    {
      if (volume is null)
        return ContainmentType.Outside;

      return Contains(volume.Corners);
    }

    /// <summary>
    /// Determines if the specified unknown bounding volume intersects with the bounding volume. This
    /// is intended only as a final, fool-proof measure as it tests corner containment.
    /// </summary>
    /// <param name="volume">Unknown other bounding volume to test against.</param>
    /// <returns>True if the two volumes intersect with one another, false otherwise.</returns>
    protected bool IntersectsGeneral(BoundingVolume? volume)
    {
      if (volume is null)
        return false;

      ReadOnlySpan<Vector3> pts = volume.Corners;
      for (int i = 0; i < pts.Length; i++)
      {
        ref readonly Vector3 pt = ref pts[i];
        if (Contains(pt) != ContainmentType.Outside)
          return true;
      }

      return false;
    }

    /// <summary>
    /// Computes the corners that represent the extremal points of this bounding volume.
    /// </summary>
    /// <param name="corners">Databuffer to contain the points, length equal to the corner count.</param>
    protected abstract void ComputeCorners(Span<Vector3> corners);

    /// <summary>
    /// Performs a ray-mesh intersection test.
    /// </summary>
    /// <param name="ray">Ray to test against.</param>
    /// <param name="results">List of results to add to.</param>
    /// <param name="ignoreBackfaces">True if backfaces (relative to the pick ray) should be ignored, false if they should be considered a result.</param>
    /// <returns>True if an intersection occured and results were added to the output list, false if no intersection occured.</returns>
    bool IPickable.IntersectsMesh(in Ray ray, IRefList<Pair<LineIntersectionResult, Triangle?>> results, bool ignoreBackfaces)
    {
      return false;
    }

    #endregion
  }
}