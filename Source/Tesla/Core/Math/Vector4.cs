﻿/*
* Copyright (c) 2010-2024 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Tesla.Content;
using Tesla.Design;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Defines a four dimensional vector where each component is a 32-bit float.
  /// </summary>
  [Serializable]
  [StructLayout(LayoutKind.Sequential, Pack = 4)]
  [TypeConverter(typeof(Vector4TypeConverter))]
  public struct Vector4 : IEquatable<Vector4>, IRefEquatable<Vector4>, IFormattable, IPrimitiveValue
  {
    /// <summary>
    /// X component of the vector.
    /// </summary>
    public float X;

    /// <summary>
    /// Y component of the vector.
    /// </summary>
    public float Y;

    /// <summary>
    /// Z component of the vector.
    /// </summary>
    public float Z;

    /// <summary>
    /// W component of the vector.
    /// </summary>
    public float W;

    private static readonly Vector4 s_zero = new Vector4(0f, 0f, 0f, 0f);
    private static readonly Vector4 s_one = new Vector4(1f, 1f, 1f, 1f);
    private static readonly Vector4 s_unitX = new Vector4(1f, 0f, 0f, 0f);
    private static readonly Vector4 s_unitY = new Vector4(0f, 1f, 0f, 0f);
    private static readonly Vector4 s_unitZ = new Vector4(0f, 0f, 1f, 0f);
    private static readonly Vector4 s_unitW = new Vector4(0f, 0f, 0f, 1f);

    private static readonly int s_sizeInbytes = BufferHelper.SizeOf<Vector4>();

    /// <summary>
    /// Gets a <see cref="Vector4"/> set to (0, 0, 0, 0).
    /// </summary>
    public static ref readonly Vector4 Zero
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_zero;
      }
    }

    /// <summary>
    /// Gets a <see cref="Vector4"/> set to (1, 1, 1, 1).
    /// </summary>
    public static ref readonly Vector4 One
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_one;
      }
    }

    /// <summary>
    /// Gets a unit <see cref="Vector4"/> set to (1, 0, 0, 0).
    /// </summary>
    public static ref readonly Vector4 UnitX
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_unitX;
      }
    }

    /// <summary>
    /// Gets a unit <see cref="Vector4"/> set to (0, 1, 0, 0).
    /// </summary>
    public static ref readonly Vector4 UnitY
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_unitY;
      }
    }

    /// <summary>
    /// Gets a unit <see cref="Vector4"/> set to (0, 0, 1, 0).
    /// </summary>
    public static ref readonly Vector4 UnitZ
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_unitZ;
      }
    }

    /// <summary>
    /// Gets a unit <see cref="Vector4"/> set to (0, 0, 0, 1).
    /// </summary>
    public static ref readonly Vector4 UnitW
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_unitW;
      }
    }

    /// <summary>
    /// Gets the size of the <see cref="Vector4"/> type in bytes.
    /// </summary>
    public static int SizeInBytes
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return s_sizeInbytes;
      }
    }

    /// <summary>
    /// Gets whether the vector is normalized or not.
    /// </summary>
    public readonly bool IsNormalized
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return MathF.Abs(((X * X) + (Y * Y) + (Z * Z) + (W * W)) - 1.0f) <= MathHelper.ZeroTolerance;
      }
    }

    /// <summary>
    /// Gets whether any of the components of the vector are NaN (Not A Number).
    /// </summary>
    public readonly bool IsNaN
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return float.IsNaN(X) || float.IsNaN(Y) || float.IsNaN(Z) || float.IsNaN(W);
      }
    }

    /// <summary>
    /// Gets whether any of the components of the vector are positive or negative infinity.
    /// </summary>
    public readonly bool IsInfinity
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return float.IsInfinity(X) || float.IsInfinity(Y) || float.IsInfinity(Z) || float.IsInfinity(W);
      }
    }

    /// <summary>
    /// Gets or sets individual components of the vector in the order that the components are declared (XYZW).
    /// </summary>
    /// <param name="index">Zero-based index.</param>
    /// <returns>The value of the specified component.</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the index is out of range [0, 3]</exception>
    public float this[int index]
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        switch (index)
        {
          case 0:
            return X;
          case 1:
            return Y;
          case 2:
            return Z;
          case 3:
            return W;
          default:
            throw new ArgumentOutOfRangeException(nameof(index), StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));
        }
      }
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      set
      {
        switch (index)
        {
          case 0:
            X = value;
            break;
          case 1:
            Y = value;
            break;
          case 2:
            Z = value;
            break;
          case 3:
            W = value;
            break;
          default:
            throw new ArgumentOutOfRangeException(nameof(index), StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));
        }
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Vector4"/> struct.
    /// </summary>
    /// <param name="value">Value to initialize each component to.</param>
    public Vector4(float value)
    {
      X = Y = Z = W = value;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Vector4"/> struct.
    /// </summary>
    /// <param name="value">Vector that contains XY components.</param>
    /// <param name="z">Optional Z component, by default zero.</param>
    /// <param name="w">Optional W component, by default zero.</param>
    public Vector4(in Vector2 value, float z = 0, float w = 0)
    {
      X = value.X;
      Y = value.Y;
      Z = z;
      W = w;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Vector4"/> struct.
    /// </summary>
    /// <param name="value">Vector that contains XYZ components.</param>
    /// <param name="w">Optional W component, by default zero.</param>
    public Vector4(in Vector3 value, float w = 0)
    {
      X = value.X;
      Y = value.Y;
      Z = value.Z;
      W = w;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Vector4"/> struct.
    /// </summary>
    /// <param name="x">X component</param>
    /// <param name="y">Y component</param>
    /// <param name="z">Z component</param>
    /// <param name="w">W component</param>
    public Vector4(float x, float y, float z, float w)
    {
      X = x;
      Y = y;
      Z = z;
      W = w;
    }

    /// <summary>
    /// Adds two vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Sum of the two vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector4 Add(in Vector4 a, in Vector4 b)
    {
      Vector4 result;
      result.X = a.X + b.X;
      result.Y = a.Y + b.Y;
      result.Z = a.Z + b.Z;
      result.W = a.W + b.W;

      return result;
    }

    /// <summary>
    /// Adds two vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <param name="result">Sum of the two vectors</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Add(in Vector4 a, in Vector4 b, out Vector4 result)
    {
      result.X = a.X + b.X;
      result.Y = a.Y + b.Y;
      result.Z = a.Z + b.Z;
      result.W = a.W + b.W;
    }

    /// <summary>
    /// Subtracts vector b from vector a.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Difference of the two vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector4 Subtract(in Vector4 a, in Vector4 b)
    {
      Vector4 result;
      result.X = a.X - b.X;
      result.Y = a.Y - b.Y;
      result.Z = a.Z - b.Z;
      result.W = a.W - b.W;

      return result;
    }

    /// <summary>
    /// Subtracts vector b from vector a.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <param name="result">Difference of the two vectors</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Subtract(in Vector4 a, in Vector4 b, out Vector4 result)
    {
      result.X = a.X - b.X;
      result.Y = a.Y - b.Y;
      result.Z = a.Z - b.Z;
      result.W = a.W - b.W;
    }

    /// <summary>
    /// Multiplies components of two vectors together.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Multiplied vectorr</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector4 Multiply(in Vector4 a, in Vector4 b)
    {
      Vector4 result;
      result.X = a.X * b.X;
      result.Y = a.Y * b.Y;
      result.Z = a.Z * b.Z;
      result.W = a.W * b.W;

      return result;
    }

    /// <summary>
    /// Multiplies components of two vectors together.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <param name="result">Multiplied vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Multiply(in Vector4 a, in Vector4 b, out Vector4 result)
    {
      result.X = a.X * b.X;
      result.Y = a.Y * b.Y;
      result.Z = a.Z * b.Z;
      result.W = a.W * b.W;
    }

    /// <summary>
    /// Multiplies components of a vector by a scalar value.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="scale">Amount to scale</param>
    /// <returns>Scaled vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector4 Multiply(in Vector4 value, float scale)
    {
      Vector4 result;
      result.X = value.X * scale;
      result.Y = value.Y * scale;
      result.Z = value.Z * scale;
      result.W = value.W * scale;

      return result;
    }

    /// <summary>
    /// Multiplies components of a vector by a scalar value.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="scale">Amount to scale</param>
    /// <param name="result">Scaled vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Multiply(in Vector4 value, float scale, out Vector4 result)
    {
      result.X = value.X * scale;
      result.Y = value.Y * scale;
      result.Z = value.Z * scale;
      result.W = value.W * scale;
    }

    /// <summary>
    /// Multiplies the matrix and the vector. The vector is treated
    /// as a column vector, so the multiplication is M*v.
    /// </summary>
    /// <param name="m">Matrix to multiply.</param>
    /// <param name="value">Vector to multiply.</param>
    /// <returns>Resulting vector.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector4 Multiply(in Matrix m, in Vector4 value)
    {
      float x = (m.M11 * value.X) + (m.M12 * value.Y) + (m.M13 * value.Z) + (m.M14 * value.W);
      float y = (m.M21 * value.X) + (m.M22 * value.Y) + (m.M23 * value.Z) + (m.M24 * value.W);
      float z = (m.M31 * value.X) + (m.M32 * value.Y) + (m.M33 * value.Z) + (m.M34 * value.W);
      float w = (m.M41 * value.X) + (m.M42 * value.Y) + (m.M43 * value.Z) + (m.M44 * value.W);

      Vector4 result;
      result.X = x;
      result.Y = y;
      result.Z = z;
      result.W = w;

      return result;
    }

    /// <summary>
    /// Multiplies the matrix and the vector. The vector is treated
    /// as a column vector, so the multiplication is M*v.
    /// </summary>
    /// <param name="m">Matrix to multiply.</param>
    /// <param name="value">Vector to multiply.</param>
    /// <param name="result">Resulting vector.</param>
    public static void Multiply(in Matrix m, in Vector4 value, out Vector4 result)
    {
      float x = (m.M11 * value.X) + (m.M12 * value.Y) + (m.M13 * value.Z) + (m.M14 * value.W);
      float y = (m.M21 * value.X) + (m.M22 * value.Y) + (m.M23 * value.Z) + (m.M24 * value.W);
      float z = (m.M31 * value.X) + (m.M32 * value.Y) + (m.M33 * value.Z) + (m.M34 * value.W);
      float w = (m.M41 * value.X) + (m.M42 * value.Y) + (m.M43 * value.Z) + (m.M44 * value.W);

      result.X = x;
      result.Y = y;
      result.Z = z;
      result.W = w;
    }

    /// <summary>
    /// Divides the components of vector a by those of vector b.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Divisor vector</param>
    /// <returns>Quotient of the two vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector4 Divide(in Vector4 a, in Vector4 b)
    {
      Vector4 result;
      result.X = a.X / b.X;
      result.Y = a.Y / b.Y;
      result.Z = a.Z / b.Z;
      result.W = a.W / b.W;

      return result;
    }

    /// <summary>
    /// Divides the components of vector a by those of vector b.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Divisor vector</param>
    /// <param name="result">Quotient of the two vectors</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Divide(in Vector4 a, in Vector4 b, out Vector4 result)
    {
      result.X = a.X / b.X;
      result.Y = a.Y / b.Y;
      result.Z = a.Z / b.Z;
      result.W = a.W / b.W;
    }

    /// <summary>
    /// Divides the components of one vector by a scalar.
    /// </summary>
    /// <param name="value">First vector</param>
    /// <param name="divisor">Divisor scalar</param>
    /// <returns>Divided vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector4 Divide(in Vector4 value, float divisor)
    {
      float invDiv = 1.0f / divisor;
      Vector4 result;
      result.X = value.X * invDiv;
      result.Y = value.Y * invDiv;
      result.Z = value.Z * invDiv;
      result.W = value.W * invDiv;

      return result;
    }

    /// <summary>
    /// Divides the components of one vector by a scalar.
    /// </summary>
    /// <param name="value">First vector</param>
    /// <param name="divisor">Divisor scalar</param>
    /// <param name="result">Divided Vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Divide(in Vector4 value, float divisor, out Vector4 result)
    {
      float invDiv = 1.0f / divisor;
      result.X = value.X * invDiv;
      result.Y = value.Y * invDiv;
      result.Z = value.Z * invDiv;
      result.W = value.W * invDiv;
    }

    /// <summary>
    /// Flips the signs of the components of the specified vector.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <returns>Negated vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector4 Negate(in Vector4 value)
    {
      Vector4 result;
      result.X = -value.X;
      result.Y = -value.Y;
      result.Z = -value.Z;
      result.W = -value.W;

      return result;
    }

    /// <summary>
    /// Flips the signs of the components of the specified vector.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="result">Negated vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Negate(in Vector4 value, out Vector4 result)
    {
      result.X = -value.X;
      result.Y = -value.Y;
      result.Z = -value.Z;
      result.W = -value.W;
    }

    /// <summary>
    /// Compute the dot product between two vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Dot product</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float Dot(in Vector4 a, in Vector4 b)
    {
      return (a.X * b.X) + (a.Y * b.Y) + (a.Z * b.Z) + (a.W * b.W);
    }

    /// <summary>
    /// Normalize the source vector to a unit vector, which results in a vector with a magnitude of 1 but
    /// preserves the direction the vector was pointing in.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <returns>Normalized unit vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector4 Normalize(in Vector4 value)
    {
      float lengthSquared = (value.X * value.X) + (value.Y * value.Y) + (value.Z * value.Z) + (value.W * value.W);

      Vector4 result = value;

      if (lengthSquared > MathHelper.ZeroTolerance)
      {
        float invLength = 1.0f / MathF.Sqrt(lengthSquared);
        result.X *= invLength;
        result.Y *= invLength;
        result.Z *= invLength;
        result.W *= invLength;
      }

      return result;
    }

    /// <summary>
    /// Normalize the source vector to a unit vector, which results in a vector with a magnitude of 1 but
    /// preserves the direction the vector was pointing in.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="result">Normalized unit vector</param>
    /// <returns>The magnitude (length) of the vector.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float Normalize(in Vector4 value, out Vector4 result)
    {
      float lengthSquared = (value.X * value.X) + (value.Y * value.Y) + (value.Z * value.Z) + (value.W * value.W);

      result = value;

      if (lengthSquared > MathHelper.ZeroTolerance)
      {
        float magnitnude = MathF.Sqrt(lengthSquared);

        float invLength = 1.0f / magnitnude;
        result.X *= invLength;
        result.Y *= invLength;
        result.Z *= invLength;
        result.W *= invLength;

        return magnitnude;
      }

      return 0.0f;
    }

    /// <summary>
    /// Compute the distance between two vectors.
    /// </summary>
    /// <param name="start">First vector</param>
    /// <param name="end">Second vector</param>
    /// <returns>Distance between the vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float Distance(in Vector4 start, in Vector4 end)
    {
      float dx = start.X - end.X;
      float dy = start.Y - end.Y;
      float dz = start.Z - end.Z;
      float dw = start.W - end.W;
      float distanceSquared = (dx * dx) + (dy * dy) + (dz * dz) + (dw * dw);

      return MathF.Sqrt(distanceSquared);
    }

    /// <summary>
    /// Compute the distance squared between two vectors.
    /// </summary>
    /// <param name="start">First vector</param>
    /// <param name="end">Second vector</param>
    /// <returns>Distance squared between the vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float DistanceSquared(in Vector4 start, in Vector4 end)
    {
      float dx = start.X - end.X;
      float dy = start.Y - end.Y;
      float dz = start.Z - end.Z;
      float dw = start.W - end.W;

      return (dx * dx) + (dy * dy) + (dz * dz) + (dw * dw);
    }

    /// <summary>
    /// Gets the vector that contains the maximum value from each of the components of the 
    /// two supplied vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Maximum vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector4 Max(in Vector4 a, in Vector4 b)
    {
      Vector4 result;
      result.X = (a.X > b.X) ? a.X : b.X;
      result.Y = (a.Y > b.Y) ? a.Y : b.Y;
      result.Z = (a.Z > b.Z) ? a.Z : b.Z;
      result.W = (a.W > b.W) ? a.W : b.W;

      return result;
    }

    /// <summary>
    /// Gets the vector that contains the maximum value from each of the components of the 
    /// two supplied vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <param name="result">Maximum vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Max(in Vector4 a, in Vector4 b, out Vector4 result)
    {
      result.X = (a.X > b.X) ? a.X : b.X;
      result.Y = (a.Y > b.Y) ? a.Y : b.Y;
      result.Z = (a.Z > b.Z) ? a.Z : b.Z;
      result.W = (a.W > b.W) ? a.W : b.W;
    }

    /// <summary>
    /// Gets the vector that contains the mininum value from each of the components of the 
    /// two supplied vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Minimum vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector4 Min(in Vector4 a, in Vector4 b)
    {
      Vector4 result;
      result.X = (a.X < b.X) ? a.X : b.X;
      result.Y = (a.Y < b.Y) ? a.Y : b.Y;
      result.Z = (a.Z < b.Z) ? a.Z : b.Z;
      result.W = (a.W < b.W) ? a.W : b.W;

      return result;
    }

    /// <summary>
    /// Gets the vector that contains the mininum value from each of the components of the 
    /// two supplied vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <param name="result">Minimum vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Min(in Vector4 a, in Vector4 b, out Vector4 result)
    {
      result.X = (a.X < b.X) ? a.X : b.X;
      result.Y = (a.Y < b.Y) ? a.Y : b.Y;
      result.Z = (a.Z < b.Z) ? a.Z : b.Z;
      result.W = (a.W < b.W) ? a.W : b.W;
    }

    /// <summary>
    /// Restricts the source vector in the range of the minimum and maximum vectors.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="min">Minimum vector</param>
    /// <param name="max">Maximum vector</param>
    /// <returns>Clamped vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector4 Clamp(in Vector4 value, in Vector4 min, in Vector4 max)
    {
      float x = value.X;
      float y = value.Y;
      float z = value.Z;
      float w = value.W;

      x = (x > max.X) ? max.X : x;
      x = (x < min.X) ? min.X : x;

      y = (y > max.Y) ? max.Y : y;
      y = (y < min.Y) ? min.Y : y;

      z = (z > max.Z) ? max.Z : z;
      z = (z < min.Z) ? min.Z : z;

      w = (w > max.W) ? max.W : w;
      w = (w < min.W) ? min.W : w;

      Vector4 result;
      result.X = x;
      result.Y = y;
      result.Z = z;
      result.W = w;

      return result;
    }

    /// <summary>
    /// Restricts the source vector in the range of the minimum and maximum vectors.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="min">Minimum vector</param>
    /// <param name="max">Maximum vector</param>
    /// <param name="result">Clamped vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Clamp(in Vector4 value, in Vector4 min, in Vector4 max, out Vector4 result)
    {
      float x = value.X;
      float y = value.Y;
      float z = value.Z;
      float w = value.W;

      x = (x > max.X) ? max.X : x;
      x = (x < min.X) ? min.X : x;

      y = (y > max.Y) ? max.Y : y;
      y = (y < min.Y) ? min.Y : y;

      z = (z > max.Z) ? max.Z : z;
      z = (z < min.Z) ? min.Z : z;

      w = (w > max.W) ? max.W : w;
      w = (w < min.W) ? min.W : w;

      result.X = x;
      result.Y = y;
      result.Z = z;
      result.W = w;
    }

    /// <summary>
    /// Returns a Vector4 containing the 4D Cartesian coordinates of a point specified
    ///  in barycentric coordinates relative to a 4D triangle.
    /// </summary>
    /// <param name="a">Vector4 containing 4D cartesian coordinates corresponding to triangle's first vertex</param>
    /// <param name="b">Vector4 containing 4D cartesian coordinates corresponding to triangle's second vertex</param>
    /// <param name="c">Vector4 containing 4D cartesian coordinates corresponding to triangle's third vertex</param>
    /// <param name="s">Barycentric coordinate s that is the weighting factor toward the second vertex</param>
    /// <param name="t">Barycentric coordinate t that is the weighting factor toward the third vertex</param>
    /// <returns>Barycentric coordinates</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector4 Barycentric(in Vector4 a, in Vector4 b, in Vector4 c, float s, float t)
    {
      Vector4 result;
      result.X = (a.X + (s * (b.X - a.X))) + (t * (c.X - a.X));
      result.Y = (a.Y + (s * (b.Y - a.Y))) + (t * (c.Y - a.Y));
      result.Z = (a.Z + (s * (b.Z - a.Z))) + (t * (c.Z - a.Z));
      result.W = (a.W + (s * (b.W - a.W))) + (t * (c.W - a.W));

      return result;
    }

    /// <summary>
    /// Returns a Vector4 containing the 3D Cartesian coordinates of a point specified
    ///  in barycentric coordinates relative to a 3D triangle.
    /// </summary>
    /// <param name="a">Vector4 containing 3D cartesian coordinates corresponding to triangle's first vertex</param>
    /// <param name="b">Vector4 containing 3D cartesian coordinates corresponding to triangle's second vertex</param>
    /// <param name="c">Vector4 containing 3D cartesian coordinates corresponding to triangle's third vertex</param>
    /// <param name="s">Barycentric coordinate s that is the weighting factor toward the second vertex</param>
    /// <param name="t">Barycentric coordinate t that is the weighting factor toward the third vertex</param>
    /// <param name="result">Barycentric coordinates</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Barycentric(in Vector4 a, in Vector4 b, in Vector4 c, float s, float t, out Vector4 result)
    {
      result.X = (a.X + (s * (b.X - a.X))) + (t * (c.X - a.X));
      result.Y = (a.Y + (s * (b.Y - a.Y))) + (t * (c.Y - a.Y));
      result.Z = (a.Z + (s * (b.Z - a.Z))) + (t * (c.Z - a.Z));
      result.W = (a.W + (s * (b.W - a.W))) + (t * (c.W - a.W));
    }

    /// <summary>
    /// Compute Catmull-Rom interpolation using the the specified positions.
    /// </summary>
    /// <param name="a">First position</param>
    /// <param name="b">Second position</param>
    /// <param name="c">Third position</param>
    /// <param name="d">Fourth position</param>
    /// <param name="wf">Weighting factor</param>
    /// <returns>Catmull-Rom interpolated vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector4 CatmullRom(in Vector4 a, in Vector4 b, in Vector4 c, in Vector4 d, float wf)
    {
      Vector4 result;
      float wfSquared = wf * wf;
      float wfCubed = wf * wfSquared;
      result.X = 0.5f * ((((2.0f * b.X) + ((-a.X + c.X) * wf)) + (((((2.0f * a.X) - (5.0f * b.X)) + (4.0f * c.X)) - d.X) * wfSquared)) + ((((-a.X + (3.0f * b.X)) - (3.0f * c.X)) + d.X) * wfCubed));
      result.Y = 0.5f * ((((2.0f * b.Y) + ((-a.Y + c.Y) * wf)) + (((((2.0f * a.Y) - (5.0f * b.Y)) + (4.0f * c.Y)) - d.Y) * wfSquared)) + ((((-a.Y + (3.0f * b.Y)) - (3.0f * c.Y)) + d.Y) * wfCubed));
      result.Z = 0.5f * ((((2.0f * b.Z) + ((-a.Z + c.Z) * wf)) + (((((2.0f * a.Z) - (5.0f * b.Z)) + (4.0f * c.Z)) - d.Z) * wfSquared)) + ((((-a.Z + (3.0f * b.Z)) - (3.0f * c.Z)) + d.Z) * wfCubed));
      result.W = 0.5f * ((((2.0f * b.W) + ((-a.W + c.W) * wf)) + (((((2.0f * a.W) - (5.0f * b.W)) + (4.0f * c.W)) - d.W) * wfSquared)) + ((((-a.W + (3.0f * b.W)) - (3.0f * c.W)) + d.W) * wfCubed));

      return result;
    }

    /// <summary>
    /// Compute Catmull-Rom interpolation.
    /// </summary>
    /// <param name="a">First position</param>
    /// <param name="b">Second position</param>
    /// <param name="c">Third position</param>
    /// <param name="d">Fourth position</param>
    /// <param name="wf">Weighting factor</param>
    /// <param name="result">Catmull-Rom interpolated vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void CatmullRom(in Vector4 a, in Vector4 b, in Vector4 c, in Vector4 d, float wf, out Vector4 result)
    {
      float wfSquared = wf * wf;
      float wfCubed = wf * wfSquared;
      result.X = 0.5f * ((((2.0f * b.X) + ((-a.X + c.X) * wf)) + (((((2.0f * a.X) - (5.0f * b.X)) + (4.0f * c.X)) - d.X) * wfSquared)) + ((((-a.X + (3.0f * b.X)) - (3.0f * c.X)) + d.X) * wfCubed));
      result.Y = 0.5f * ((((2.0f * b.Y) + ((-a.Y + c.Y) * wf)) + (((((2.0f * a.Y) - (5.0f * b.Y)) + (4.0f * c.Y)) - d.Y) * wfSquared)) + ((((-a.Y + (3.0f * b.Y)) - (3.0f * c.Y)) + d.Y) * wfCubed));
      result.Z = 0.5f * ((((2.0f * b.Z) + ((-a.Z + c.Z) * wf)) + (((((2.0f * a.Z) - (5.0f * b.Z)) + (4.0f * c.Z)) - d.Z) * wfSquared)) + ((((-a.Z + (3.0f * b.Z)) - (3.0f * c.Z)) + d.Z) * wfCubed));
      result.W = 0.5f * ((((2.0f * b.W) + ((-a.W + c.W) * wf)) + (((((2.0f * a.W) - (5.0f * b.W)) + (4.0f * c.W)) - d.W) * wfSquared)) + ((((-a.W + (3.0f * b.W)) - (3.0f * c.W)) + d.W) * wfCubed));
    }

    /// <summary>
    /// Compute a Hermite spline interpolation.
    /// </summary>
    /// <param name="a">First position</param>
    /// <param name="tangentA">First vector's tangent</param>
    /// <param name="b">Second position</param>
    /// <param name="tangentB">Second vector's tangent</param>
    /// <param name="wf">Weighting factor</param>
    /// <returns>Hermite interpolated vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector4 Hermite(in Vector4 a, in Vector4 tangentA, in Vector4 b, in Vector4 tangentB, float wf)
    {
      float wfSquared = wf * wf;
      float wfCubed = wfSquared * wf;
      float h1 = ((2.0f * wfCubed) - (3.0f * wfSquared)) + 1.0f;
      float h2 = (-2.0f * wfCubed) + (3.0f * wfSquared);
      float h3 = (wfCubed - (2.0f * wfSquared)) + wf;
      float h4 = wfCubed - wfSquared;

      Vector4 result;
      result.X = (((a.X * h1) + (b.X * h2)) + (tangentA.X * h3)) + (tangentB.X * h4);
      result.Y = (((a.Y * h1) + (b.Y * h2)) + (tangentA.Y * h3)) + (tangentB.Y * h4);
      result.Z = (((a.Z * h1) + (b.Z * h2)) + (tangentA.Z * h3)) + (tangentB.Z * h4);
      result.W = (((a.W * h1) + (b.W * h2)) + (tangentA.W * h3)) + (tangentB.W * h4);

      return result;
    }

    /// <summary>
    /// Compute a Hermite spline interpolation.
    /// </summary>
    /// <param name="a">First position</param>
    /// <param name="tangentA">First vector's tangent</param>
    /// <param name="b">Second position</param>
    /// <param name="tangentB">Second vector's tangent</param>
    /// <param name="wf">Weighting factor</param>
    /// <param name="result">Hermite interpolated vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Hermite(in Vector4 a, in Vector4 tangentA, in Vector4 b, in Vector4 tangentB, float wf, out Vector4 result)
    {
      float wfSquared = wf * wf;
      float wfCubed = wfSquared * wf;
      float h1 = ((2.0f * wfCubed) - (3.0f * wfSquared)) + 1.0f;
      float h2 = (-2.0f * wfCubed) + (3.0f * wfSquared);
      float h3 = (wfCubed - (2.0f * wfSquared)) + wf;
      float h4 = wfCubed - wfSquared;

      result.X = (((a.X * h1) + (b.X * h2)) + (tangentA.X * h3)) + (tangentB.X * h4);
      result.Y = (((a.Y * h1) + (b.Y * h2)) + (tangentA.Y * h3)) + (tangentB.Y * h4);
      result.Z = (((a.Z * h1) + (b.Z * h2)) + (tangentA.Z * h3)) + (tangentB.Z * h4);
      result.W = (((a.W * h1) + (b.W * h2)) + (tangentA.W * h3)) + (tangentB.W * h4);
    }

    /// <summary>
    /// Compute a cubic interpolation between two vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <param name="wf">Weighting factor (between 0 and 1.0)</param>
    /// <returns>Cubic interpolated vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector4 SmoothStep(in Vector4 a, in Vector4 b, float wf)
    {
      Vector4 result;
      float amt = MathHelper.Clamp(wf, 0.0f, 1.0f);
      amt = (amt * amt) * (3.0f - (2.0f * amt));
      result.X = a.X + ((b.X - a.X) * amt);
      result.Y = a.Y + ((b.Y - a.Y) * amt);
      result.Z = a.Z + ((b.Z - a.Z) * amt);
      result.W = a.W + ((b.W - a.W) * amt);

      return result;
    }

    /// <summary>
    /// Compute a cubic interpolation between two vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <param name="wf">Weighting factor (between 0 and 1.0)</param>
    /// <param name="result">Cubic interpolated vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void SmoothStep(in Vector4 a, in Vector4 b, float wf, out Vector4 result)
    {
      float amt = MathHelper.Clamp(wf, 0.0f, 1.0f);
      amt = (amt * amt) * (3.0f - (2.0f * amt));
      result.X = a.X + ((b.X - a.X) * amt);
      result.Y = a.Y + ((b.Y - a.Y) * amt);
      result.Z = a.Z + ((b.Z - a.Z) * amt);
      result.W = a.W + ((b.W - a.W) * amt);
    }

    /// <summary>
    /// Linearly interpolates between two vectors.
    /// </summary>
    /// <param name="a">Starting vector</param>
    /// <param name="b">Ending vector</param>
    /// <param name="percent">Amount to interpolate by</param>
    /// <returns>Linear interpolated vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector4 Lerp(in Vector4 a, in Vector4 b, float percent)
    {
      // Better for floating point precision than a + (b - a) * percent;
      float oneMinusPercent = 1.0f - percent;

      Vector4 result;
      result.X = a.X * oneMinusPercent + b.X * percent;
      result.Y = a.Y * oneMinusPercent + b.Y * percent;
      result.Z = a.Z * oneMinusPercent + b.Z * percent;
      result.W = a.W * oneMinusPercent + b.W * percent;

      return result;
    }

    /// <summary>
    /// Linearly interpolates between two vectors.
    /// </summary>
    /// <param name="a">Starting vector</param>
    /// <param name="b">Ending vector</param>
    /// <param name="percent">Amount to interpolate by</param>
    /// <param name="result">Linear interpolated vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Lerp(in Vector4 a, in Vector4 b, float percent, out Vector4 result)
    {
      // Better for floating point precision than a + (b - a) * percent;
      float oneMinusPercent = 1.0f - percent;

      result.X = a.X * oneMinusPercent + b.X * percent;
      result.Y = a.Y * oneMinusPercent + b.Y * percent;
      result.Z = a.Z * oneMinusPercent + b.Z * percent;
      result.W = a.W * oneMinusPercent + b.W * percent;
    }

    /// <summary>
    /// Transforms the specified vector by the given <see cref="Matrix"/>.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="m">Transformation matrix</param>
    /// <returns>Transformed vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector4 Transform(in Vector4 value, in Matrix m)
    {
      float x = (value.X * m.M11) + (value.Y * m.M21) + (value.Z * m.M31) + (value.W * m.M41);
      float y = (value.X * m.M12) + (value.Y * m.M22) + (value.Z * m.M32) + (value.W * m.M42);
      float z = (value.X * m.M13) + (value.Y * m.M23) + (value.Z * m.M33) + (value.W * m.M43);
      float w = (value.X * m.M14) + (value.Y * m.M24) + (value.Z * m.M34) + (value.W * m.M44);

      Vector4 result;
      result.X = x;
      result.Y = y;
      result.Z = z;
      result.W = w;

      return result;
    }

    /// <summary>
    /// Transforms the specified vector by the given <see cref="Matrix"/>. The vector is treated
    /// as a row vector, so the multiplication is v*M.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="m">Transformation matrix</param>
    /// <param name="result">Transformed vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Transform(in Vector4 value, in Matrix m, out Vector4 result)
    {
      float x = (value.X * m.M11) + (value.Y * m.M21) + (value.Z * m.M31) + (value.W * m.M41);
      float y = (value.X * m.M12) + (value.Y * m.M22) + (value.Z * m.M32) + (value.W * m.M42);
      float z = (value.X * m.M13) + (value.Y * m.M23) + (value.Z * m.M33) + (value.W * m.M43);
      float w = (value.X * m.M14) + (value.Y * m.M24) + (value.Z * m.M34) + (value.W * m.M44);

      result.X = x;
      result.Y = y;
      result.Z = z;
      result.W = w;
    }

    /// <summary>
    /// Transforms an span of vectors by the given <see cref="Matrix"/>. The vector is treated
    /// as a row vector, so the multiplication is v*M.
    /// </summary>
    /// <param name="source">Span of vectors to be transformed</param>
    /// <param name="m">Transformation matrix</param>
    /// <param name="destination">Span to store transformed vectors, this may be the same as the source span</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the destination span is smaller than the source span.</exception>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Transform(ReadOnlySpan<Vector4> source, in Matrix m, Span<Vector4> destination)
    {
      if (destination.Length < source.Length)
        throw new ArgumentOutOfRangeException(nameof(destination), StringLocalizer.Instance.GetLocalizedString("DestinationArraySmallerThanSource"));

      for (int i = 0; i < source.Length; i++)
        Transform(source[i], m, out destination[i]);
    }

    /// <summary>
    /// Transforms the specified vector by the given <see cref="Quaternion"/>.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="q">Quaternion rotation</param>
    /// <returns>Transformed vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector4 Transform(in Vector4 value, in Quaternion q)
    {
      float x2 = q.X + q.X;
      float y2 = q.Y + q.Y;
      float z2 = q.Z + q.Z;

      float wx2 = q.W * x2;
      float wy2 = q.W * y2;
      float wz2 = q.W * z2;

      float xx2 = q.X * x2;
      float xy2 = q.X * y2;
      float xz2 = q.X * z2;

      float yy2 = q.Y * y2;
      float yz2 = q.Y * z2;

      float zz2 = q.Z * z2;

      float x = ((value.X * ((1.0f - yy2) - zz2)) + (value.Y * (xy2 - wz2))) + (value.Z * (xz2 + wy2));
      float y = ((value.X * (xy2 + wz2)) + (value.Y * ((1.0f - xx2) - zz2))) + (value.Z * (yz2 - wx2));
      float z = ((value.X * (xz2 - wy2)) + (value.Y * (yz2 + wx2))) + (value.Z * ((1.0f - xx2) - yy2));

      Vector4 result;
      result.X = x;
      result.Y = y;
      result.Z = z;
      result.W = value.W;

      return result;
    }

    /// <summary>
    /// Transforms the specified vector by the given <see cref="Quaternion"/>.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="q">Quaternion rotation</param>
    /// <param name="result">Transformed vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Transform(in Vector4 value, in Quaternion q, out Vector4 result)
    {
      float x2 = q.X + q.X;
      float y2 = q.Y + q.Y;
      float z2 = q.Z + q.Z;

      float wx2 = q.W * x2;
      float wy2 = q.W * y2;
      float wz2 = q.W * z2;

      float xx2 = q.X * x2;
      float xy2 = q.X * y2;
      float xz2 = q.X * z2;

      float yy2 = q.Y * y2;
      float yz2 = q.Y * z2;

      float zz2 = q.Z * z2;

      float x = ((value.X * ((1.0f - yy2) - zz2)) + (value.Y * (xy2 - wz2))) + (value.Z * (xz2 + wy2));
      float y = ((value.X * (xy2 + wz2)) + (value.Y * ((1.0f - xx2) - zz2))) + (value.Z * (yz2 - wx2));
      float z = ((value.X * (xz2 - wy2)) + (value.Y * (yz2 + wx2))) + (value.Z * ((1.0f - xx2) - yy2));

      result.X = x;
      result.Y = y;
      result.Z = z;
      result.W = value.W;
    }

    /// <summary>
    /// Transforms an span of vectors by the given <see cref="Quaternion"/>. The vector is treated
    /// as a row vector, so the multiplication is v*M.
    /// </summary>
    /// <param name="source">Span of vectors to be transformed</param>
    /// <param name="q">Quaternion rotation</param>
    /// <param name="destination">Span to store transformed vectors, this may be the same as the source span</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the destination span is smaller than the source span.</exception>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Transform(ReadOnlySpan<Vector4> source, in Quaternion q, Span<Vector4> destination)
    {
      if (destination.Length < source.Length)
        throw new ArgumentOutOfRangeException(nameof(destination), StringLocalizer.Instance.GetLocalizedString("DestinationArraySmallerThanSource"));

      for (int i = 0; i < source.Length; i++)
        Transform(source[i], q, out destination[i]);
    }

    /// <summary>
    /// Implicitly converts from <see cref="Vector4"/> to <see cref="System.Numerics.Vector4"/> for interop.
    /// </summary>
    /// <param name="v">Vector to convert.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static implicit operator System.Numerics.Vector4(Vector4 v)
    {
      return new System.Numerics.Vector4(v.X, v.Y, v.Z, v.W);
    }

    /// <summary>
    /// Implicitly converts from <see cref="System.Numerics.Vector4"/> to <see cref="Vector4"/> for interop.
    /// </summary>
    /// <param name="v">Vector to convert.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static implicit operator Vector4(System.Numerics.Vector4 v)
    {
      return new Vector4(v.X, v.Y, v.Z, v.W);
    }

    /// <summary>
    /// Tests equality between two vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>True if components are equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator ==(Vector4 a, Vector4 b)
    {
      return (MathF.Abs(a.X - b.X) <= MathHelper.ZeroTolerance) && (MathF.Abs(a.Y - b.Y) <= MathHelper.ZeroTolerance) &&
          (MathF.Abs(a.Z - b.Z) <= MathHelper.ZeroTolerance) && (MathF.Abs(a.W - b.W) <= MathHelper.ZeroTolerance);
    }

    /// <summary>
    /// Tests inequality between two vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>True if components are not equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator !=(Vector4 a, Vector4 b)
    {
      return (MathF.Abs(a.X - b.X) > MathHelper.ZeroTolerance) || (MathF.Abs(a.Y - b.Y) > MathHelper.ZeroTolerance) ||
          (MathF.Abs(a.Z - b.Z) > MathHelper.ZeroTolerance) || (MathF.Abs(a.W - b.W) > MathHelper.ZeroTolerance);
    }

    /// <summary>
    /// Adds the two vectors together.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Sum of the two vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector4 operator +(Vector4 a, Vector4 b)
    {
      Vector4 result;
      result.X = a.X + b.X;
      result.Y = a.Y + b.Y;
      result.Z = a.Z + b.Z;
      result.W = a.W + b.W;

      return result;
    }

    /// <summary>
    /// Flips the signs of the components of the specified vector.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <returns>Negated vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector4 operator -(Vector4 value)
    {
      Vector4 result;
      result.X = -value.X;
      result.Y = -value.Y;
      result.Z = -value.Z;
      result.W = -value.W;

      return result;
    }

    /// <summary>
    /// Subtracts vector b from vector a.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Difference of the two vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector4 operator -(Vector4 a, Vector4 b)
    {
      Vector4 result;
      result.X = a.X - b.X;
      result.Y = a.Y - b.Y;
      result.Z = a.Z - b.Z;
      result.W = a.W - b.W;

      return result;
    }

    /// <summary>
    /// Multiplies two vectors together.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Multiplied vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector4 operator *(Vector4 a, Vector4 b)
    {
      Vector4 result;
      result.X = a.X * b.X;
      result.Y = a.Y * b.Y;
      result.Z = a.Z * b.Z;
      result.W = a.W * b.W;

      return result;
    }

    /// <summary>
    /// Multiplies a vector by a scaling factor.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="scale">Amount to scale</param>
    /// <returns>Multiplied vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector4 operator *(Vector4 value, float scale)
    {
      Vector4 result;
      result.X = value.X * scale;
      result.Y = value.Y * scale;
      result.Z = value.Z * scale;
      result.W = value.W * scale;

      return result;
    }

    /// <summary>
    /// Multiplies a vector by a scaling factor.
    /// </summary>
    /// <param name="scale">Amount to scale</param>
    /// <param name="value">Source vector</param>
    /// <returns>Multiplied vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector4 operator *(float scale, Vector4 value)
    {
      Vector4 result;
      result.X = value.X * scale;
      result.Y = value.Y * scale;
      result.Z = value.Z * scale;
      result.W = value.W * scale;

      return result;
    }

    /// <summary>
    /// Divides the components of vector a by those of vector b.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Divisor vector</param>
    /// <returns>Quotient of the two vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector4 operator /(Vector4 a, Vector4 b)
    {
      Vector4 result;
      result.X = a.X / b.X;
      result.Y = a.Y / b.Y;
      result.Z = a.Z / b.Z;
      result.W = a.W / b.W;

      return result;
    }

    /// <summary>
    /// Divides a vector by a scalar value.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="divisor">Divisor scalar</param>
    /// <returns>Divided vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector4 operator /(Vector4 value, float divisor)
    {
      Vector4 result;
      float invDivisor = 1.0f / divisor;
      result.X = value.X * invDivisor;
      result.Y = value.Y * invDivisor;
      result.Z = value.Z * invDivisor;
      result.W = value.W * invDivisor;

      return result;
    }

    /// <summary>
    /// Flips the signs of the components of the vector.
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void Negate()
    {
      X = -X;
      Y = -Y;
      Z = -Z;
      W = -W;
    }

    /// <summary>
    /// Compute the length (magnitude) of the vector.
    /// </summary>
    /// <returns>Length</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float Length()
    {
      float lengthSquared = (X * X) + (Y * Y) + (Z * Z) + (W * W);
      return MathF.Sqrt(lengthSquared);
    }

    /// <summary>
    /// Compute the length (magnitude) squared of the vector.
    /// </summary>
    /// <returns>Length squared</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float LengthSquared()
    {
      return (X * X) + (Y * Y) + (Z * Z) + (W * W);
    }

    /// <summary>
    /// Normalize the vector to a unit vector, which results in a vector with a magnitude of 1 but
    /// preserves the direction the vector was pointing in.
    /// </summary>
    /// <returns>The magnitude (length) of the vector.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public float Normalize()
    {
      float lengthSquared = (X * X) + (Y * Y) + (Z * Z) + (W * W);

      if (lengthSquared > MathHelper.ZeroTolerance)
      {
        float magnitnude = MathF.Sqrt(lengthSquared);

        float invLength = 1.0f / magnitnude;
        X *= invLength;
        Y *= invLength;
        Z *= invLength;
        W *= invLength;

        return magnitnude;
      }

      return 0.0f;
    }

    /// <summary>
    /// Tests equality between the vector and another vector.
    /// </summary>
    /// <param name="other">Vector to test against</param>
    /// <returns>True if components are equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    readonly bool IEquatable<Vector4>.Equals(Vector4 other)
    {
      return (MathF.Abs(X - other.X) <= MathHelper.ZeroTolerance) && (MathF.Abs(Y - other.Y) <= MathHelper.ZeroTolerance) &&
          (MathF.Abs(Z - other.Z) <= MathHelper.ZeroTolerance) && (MathF.Abs(W - other.W) <= MathHelper.ZeroTolerance);
    }

    /// <summary>
    /// Tests equality between the vector and another vector.
    /// </summary>
    /// <param name="other">Vector to test against</param>
    /// <returns>True if components are equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Equals(in Vector4 other)
    {
      return (MathF.Abs(X - other.X) <= MathHelper.ZeroTolerance) && (MathF.Abs(Y - other.Y) <= MathHelper.ZeroTolerance) &&
          (MathF.Abs(Z - other.Z) <= MathHelper.ZeroTolerance) && (MathF.Abs(W - other.W) <= MathHelper.ZeroTolerance);
    }

    /// <summary>
    /// Tests equality between the vector and another vector.
    /// </summary>
    /// <param name="other">Vector to test against</param>
    /// <param name="tolerance">Tolerance</param>
    /// <returns>True if components are equal within tolerance, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Equals(in Vector4 other, float tolerance)
    {
      return (MathF.Abs(X - other.X) <= tolerance) && (MathF.Abs(Y - other.Y) <= tolerance) &&
          (MathF.Abs(Z - other.Z) <= tolerance) && (MathF.Abs(W - other.W) <= tolerance);
    }

    /// <summary>
    /// Tests equality between the vector and XYZW values.
    /// </summary>
    /// <param name="x">X component.</param>
    /// <param name="y">Y component.</param>
    /// <param name="z">Z component.</param>
    /// <param name="w">W component.</param>
    /// <param name="tolerance">Optional tolerance, defaults to <see cref="MathHelper.ZeroTolerance"/>.</param>
    /// <returns>True if components are equal within tolerance, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Equals(float x, float y, float z, float w, float tolerance = MathHelper.ZeroTolerance)
    {
      return (MathF.Abs(X - x) <= tolerance) && (MathF.Abs(Y - y) <= tolerance) &&
          (MathF.Abs(Z - z) <= tolerance) && (MathF.Abs(W - w) <= tolerance);
    }

    /// <summary>
    /// Determines whether the specified <see cref="System.Object" /> is equal to this instance.
    /// </summary>
    /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
    /// <returns>True if the specified <see cref="System.Object" /> is equal to this instance; otherwise, false.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is Vector4)
        return Equals((Vector4)obj);

      return false;
    }

    /// <summary>
    /// Determines whether this vector is nearly (0, 0, 0, 0).
    /// </summary>
    /// <param name="tol">Optional tolerance.</param>
    /// <returns>True if this vector is almost zero, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool IsAlmostZero(float tol = MathHelper.ZeroTolerance)
    {
      ref readonly Vector4 zero = ref Vector4.Zero;
      return Equals(zero, tol);
    }

    /// <summary>
    /// Determines whether this vector is nearly (1, 1, 1, 1).
    /// </summary>
    /// <param name="tol">Optional tolerance.</param>
    /// <returns>True if this vector is almost zero, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool IsAlmostOne(float tol = MathHelper.ZeroTolerance)
    {
      ref readonly Vector4 one = ref Vector4.One;
      return Equals(one, tol);
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
    public readonly override int GetHashCode()
    {
      unchecked
      {
        return X.GetHashCode() + Y.GetHashCode() + Z.GetHashCode() + W.GetHashCode();
      }
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly override string ToString()
    {
      CultureInfo info = CultureInfo.CurrentCulture;
      return String.Format(info, "X: {0} Y: {1} Z: {2} W: {3}",
          new Object[] { X.ToString(info), Y.ToString(info), Z.ToString(info), W.ToString(info) });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="format">The format</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(string? format)
    {
      if (format is null)
        return ToString();

      CultureInfo info = CultureInfo.CurrentCulture;
      return String.Format(info, "X: {0} Y: {1} Z: {2} W: {3}",
          new Object[] { X.ToString(format, info), Y.ToString(format, info), Z.ToString(format, info), W.ToString(format, info) });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="formatProvider">The format provider.</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(IFormatProvider? formatProvider)
    {
      if (formatProvider is null)
        return ToString();

      return String.Format(formatProvider, "X: {0} Y: {1} Z: {2} W: {3}",
          new Object[] { X.ToString(formatProvider), Y.ToString(formatProvider), Z.ToString(formatProvider), W.ToString(formatProvider) });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="format">The format.</param>
    /// <param name="formatProvider">The format provider.</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(string? format, IFormatProvider? formatProvider)
    {
      if (formatProvider is null)
        return ToString();

      if (format is null)
        return ToString(formatProvider);

      return String.Format(formatProvider, "X: {0} Y: {1} Z: {2} W: {3}",
          new Object[] { X.ToString(format, formatProvider), Y.ToString(format, formatProvider), Z.ToString(format, formatProvider), W.ToString(format, formatProvider) });
    }

    /// <summary>
    /// Writes the primitive data to the output.
    /// </summary>
    /// <param name="output">Primitive writer.</param>
    readonly void IPrimitiveValue.Write(IPrimitiveWriter output)
    {
      output.Write("X", X);
      output.Write("Y", Y);
      output.Write("Z", Z);
      output.Write("W", W);
    }

    /// <summary>
    /// Reads the primitive data from the input.
    /// </summary>
    /// <param name="input">Primitive reader.</param>
    void IPrimitiveValue.Read(IPrimitiveReader input)
    {
      X = input.ReadSingle("X");
      Y = input.ReadSingle("Y");
      Z = input.ReadSingle("Z");
      W = input.ReadSingle("W");
    }
  }
}
