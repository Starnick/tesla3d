﻿/*
* Copyright (c) 2010-2024 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Tesla.Content;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Represents an Axis-Aligned Bounding Box. The box is defined by extents from its center along the XYZ standard axes.
  /// </summary>
  [SavableVersion(1)]
  public sealed class BoundingBox : BoundingVolume
  {
    private Vector3 m_center;
    private Vector3 m_extents;

    /// <summary>
    /// Gets or sets the center of the bounding volume.
    /// </summary>
    public override Vector3 Center
    {
      get
      {
        return m_center;
      }
      set
      {
        m_center = value;
        UpdateCorners = true;
      }
    }

    /// <summary>
    /// Gets or sets the extents of the bounding box (half-lengths along each axis from the center).
    /// </summary>
    public Vector3 Extents
    {
      get
      {
        return m_extents;
      }
      set
      {
        m_extents = value;
        UpdateCorners = true;
      }
    }

    /// <summary>
    /// Gets the maximum point of the bounding box.
    /// </summary>
    public Vector3 Max { get { return new Vector3(m_center.X + m_extents.X, m_center.Y + m_extents.Y, m_center.Z + m_extents.Z); } }

    /// <summary>
    /// Gets the minimum point of the bounding box.
    /// </summary>
    public Vector3 Min { get { return new Vector3(m_center.X - m_extents.X, m_center.Y - m_extents.Y, m_center.Z - m_extents.Z); } }

    /// <summary>
    /// Gets the volume of the bounding volume.
    /// </summary>
    public override float Volume { get { return (m_extents.X * m_extents.Y * m_extents.Z) * 2.0f; } }

    /// <summary>
    /// Gets the bounding type.
    /// </summary>
    public override BoundingType BoundingType { get { return BoundingType.AxisAlignedBoundingBox; } }

    /// <summary>
    /// Constructs a new instance of the <see cref="BoundingBox"/> class.
    /// </summary>
    public BoundingBox()
    {
      m_center = Vector3.Zero;
      m_extents = Vector3.Zero;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="BoundingBox"/> class.
    /// </summary>
    /// <param name="center">Center of the bounding box.</param>
    /// <param name="extents">Extents of the bounding box.</param>
    public BoundingBox(in Vector3 center, in Vector3 extents)
    {
      m_center = center;
      m_extents = extents;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="BoundingBox"/> class.
    /// </summary>
    /// <param name="boundingBoxData">Bounding box data.</param>
    public BoundingBox(in BoundingBox.Data boundingBoxData)
    {
      m_center = boundingBoxData.Center;
      m_extents = boundingBoxData.Extents;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="BoundingBox"/> class.
    /// </summary>
    /// <param name="boundingBox">Bounding box to copy from.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the bounding box is null.</exception>
    public BoundingBox(BoundingBox? boundingBox)
    {
      ArgumentNullException.ThrowIfNull(boundingBox, nameof(boundingBox));

      m_center = boundingBox.Center;
      m_extents = boundingBox.Extents;
    }

    /// <summary>
    /// Extends the bounding volume to include the specified point.
    /// </summary>
    /// <param name="point">Point to contain.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void Extend(in Vector3 point)
    {
      Extend(new ReadOnlySpan<Vector3>(in point));
    }

    /// <summary>
    /// Extends the bounding volume to include the specified points.
    /// </summary>
    /// <param name="points">Points to contain.</param>
    public void Extend(ReadOnlySpan<Vector3> points)
    {
      if (points.IsEmpty)
        return;

      Vector3 max = new Vector3(m_center.X + m_extents.X, m_center.Y + m_extents.Y, m_center.Z + m_extents.Z);
      Vector3 min = new Vector3(m_center.X - m_extents.X, m_center.Y - m_extents.Y, m_center.Z - m_extents.Z);

      for (int i = 0; i < points.Length; i++)
      {
        ref readonly Vector3 pt = ref points[i];
        Vector3.Min(min, pt, out min);
        Vector3.Max(max, pt, out max);
      }

      //Compute center from min-max
      Vector3.Add(max, min, out m_center);
      Vector3.Multiply(m_center, 0.5f, out m_center);

      //Compute extents from min-max
      Vector3.Subtract(max, m_center, out m_extents);
      UpdateCorners = true;
    }

    /// <summary>
    /// Sets the bounding volume by either copying the specified bounding volume if its the specified type, or computing a volume required to fully contain it.
    /// </summary>
    /// <param name="volume">Bounding volume to copy from</param>
    public override void Set(BoundingVolume? volume)
    {
      if (volume is null)
        return;

      switch (volume.BoundingType)
      {
        case BoundingType.AxisAlignedBoundingBox:
          {
            BoundingBox other = (volume as BoundingBox)!;
            m_center = other.m_center;
            m_extents = other.m_extents;
          }
          break;
        case BoundingType.Sphere:
          {
            BoundingSphere other = (volume as BoundingSphere)!;
            float radius = other.Radius;
            m_center = other.Center;
            m_extents = new Vector3(radius, radius, radius);
          }
          break;
        case BoundingType.Capsule:
        case BoundingType.OrientedBoundingBox:
        case BoundingType.Frustum:
        case BoundingType.Mesh:
        default:
          {
            ComputeFromPoints(volume.Corners, null);
          }
          break;
      }

      UpdateCorners = true;
    }

    /// <summary>
    /// Sets the bounding box from the specified data.
    /// </summary>
    /// <param name="boundingBoxData">Bounding box data to set.</param>
    public void Set(in BoundingBox.Data boundingBoxData)
    {
      m_center = boundingBoxData.Center;
      m_extents = boundingBoxData.Extents;
      UpdateCorners = true;
    }

    /// <summary>
    /// Sets the bounding box from the specified data.
    /// </summary>
    /// <param name="center">Center of the bounding box.</param>
    /// <param name="extents">Extents of the bounding box.</param>
    public void Set(in Vector3 center, in Vector3 extents)
    {
      m_center = center;
      m_extents = extents;
      UpdateCorners = true;
    }

    /// <summary>
    /// Creates a copy of the bounding volume and returns a new instance.
    /// </summary>
    /// <returns>Copied bounding volume</returns>
    public override BoundingVolume Clone()
    {
      return new BoundingBox(this);
    }

    /// <summary>
    /// Computes the distance from a given point to the edge of the volume.
    /// </summary>
    /// <param name="point">Point in space</param>
    /// <returns>Distance from the point to the edge of the volume.</returns>
    public override float DistanceTo(in Vector3 point)
    {
      float distSqr;
      GeometricToolsHelper.DistancePointAABB(point, m_center, m_extents, out distSqr);

      return MathF.Sqrt(distSqr);
    }

    /// <summary>
    /// Computes the distance squared from a given point to the edge of the volume.
    /// </summary>
    /// <param name="point">Point in space</param>
    /// <returns>Distanced squared from the point to the edge of the volume.</returns>
    public override float DistanceSquaredTo(in Vector3 point)
    {
      float distSqr;
      GeometricToolsHelper.DistancePointAABB(point, m_center, m_extents, out distSqr);

      return distSqr;
    }

    /// <summary>
    /// Computes the closest point on the volume from the given point in space.
    /// </summary>
    /// <param name="point">Point in space</param>
    /// <param name="result">Closest point on the edge of the volume.</param>
    public override void ClosestPointTo(in Vector3 point, out Vector3 result)
    {
      Vector3 max = new Vector3(m_center.X + m_extents.X, m_center.Y + m_extents.Y, m_center.Z + m_extents.Z);
      Vector3 min = new Vector3(m_center.X - m_extents.X, m_center.Y - m_extents.Y, m_center.Z - m_extents.Z);

      result = point;

      for (int i = 0; i < 3; i++)
      {
        float v = point[i];
        v = MathF.Max(v, min[i]);
        v = MathF.Min(v, max[i]);
        result[i] = v;
      }
    }

    /// <summary>
    /// Computes a minimum bounding volume that encloses the specified points in space.
    /// </summary>
    /// <param name="points">Points in space</param>
    /// <param name="subMeshRange">Optional range inside the buffer that represents the mesh to compute bounds for. Note: the base vertex offset will be ignored.</param>
    public override void ComputeFromPoints(ReadOnlySpan<Vector3> points, in SubMeshRange? subMeshRange = null)
    {
      BoundingBox.Data data;
      BoundingBox.Data.FromPoints(points, subMeshRange, out data);

      m_center = data.Center;
      m_extents = data.Extents;
      UpdateCorners = true;
    }

    /// <summary>
    /// Computes a minimum bounding volume that encloses the specified indexed points in space.
    /// </summary>
    /// <param name="points">Points in space.</param>
    /// <param name="indices">Point indices denoting location in the point buffer.</param>
    /// <param name="subMeshRange">Optional range inside the buffer that represents the mesh to compute bounds for.</param>
    public override void ComputeFromIndexedPoints(ReadOnlySpan<Vector3> points, IndexData indices, in SubMeshRange? subMeshRange = null)
    {
      BoundingBox.Data data;
      BoundingBox.Data.FromIndexedPoints(points, indices, subMeshRange, out data);

      m_center = data.Center;
      m_extents = data.Extents;
      UpdateCorners = true;
    }

    /// <summary>
    /// Determines if the specified point is contained inside the bounding volume.
    /// </summary>
    /// <param name="point">Point to test against.</param>
    /// <returns>Type of containment</returns>
    public override ContainmentType Contains(in Vector3 point)
    {
      Vector3 diff;
      Vector3.Subtract(point, m_center, out diff);

      if ((MathF.Abs(diff.X) <= m_extents.X) && (MathF.Abs(diff.Y) <= m_extents.Y) && (MathF.Abs(diff.Z) <= m_extents.Z))
        return ContainmentType.Inside;

      return ContainmentType.Outside;
    }

    /// <summary>
    /// Determines if the specified segment line is contained inside the bounding volume.
    /// </summary>
    /// <param name="line">Segment to test against.</param>
    /// <returns>Type of containment.</returns>
    public override ContainmentType Contains(in Segment line)
    {
      Vector3 startDiff, endDiff;
      Vector3.Subtract(line.StartPoint, m_center, out startDiff);
      Vector3.Subtract(line.EndPoint, m_center, out endDiff);

      //If both end points are inside the bounding box, then it is wholly contained
      if (((MathF.Abs(startDiff.X) <= m_extents.X) && (MathF.Abs(startDiff.Y) <= m_extents.Y) && (MathF.Abs(startDiff.Z) <= m_extents.Z)) &&
          ((MathF.Abs(endDiff.X) <= m_extents.X) && (MathF.Abs(endDiff.Y) <= m_extents.Y) && (MathF.Abs(endDiff.Z) <= m_extents.Z)))
        return ContainmentType.Inside;

      Vector3 xAxis = Vector3.UnitX;
      Vector3 yAxis = Vector3.UnitY;
      Vector3 zAxis = Vector3.UnitZ;

      if (GeometricToolsHelper.IntersectSegmentAABB(line, m_center, m_extents))
        return ContainmentType.Intersects;

      return ContainmentType.Outside;
    }

    /// <summary>
    /// Determines if the specified triangle is contained inside the bounding volume.
    /// </summary>
    /// <param name="triangle">Triangle to test against.</param>
    /// <returns>Type of containment.</returns>
    public override ContainmentType Contains(in Triangle triangle)
    {
      Vector3 ptADiff, ptBDiff, ptCDiff;
      Vector3.Subtract(triangle.PointA, m_center, out ptADiff);
      Vector3.Subtract(triangle.PointB, m_center, out ptBDiff);
      Vector3.Subtract(triangle.PointC, m_center, out ptCDiff);

      //If all three vertices are inside the bounding box, then it is wholly contained
      if (((MathF.Abs(ptADiff.X) <= m_extents.X) && (MathF.Abs(ptADiff.Y) <= m_extents.Y) && (MathF.Abs(ptADiff.Z) <= m_extents.Z)) &&
          ((MathF.Abs(ptBDiff.X) <= m_extents.X) && (MathF.Abs(ptBDiff.Y) <= m_extents.Y) && (MathF.Abs(ptBDiff.Z) <= m_extents.Z)) &&
          ((MathF.Abs(ptCDiff.X) <= m_extents.X) && (MathF.Abs(ptCDiff.Y) <= m_extents.Y) && (MathF.Abs(ptCDiff.Z) <= m_extents.Z)))
        return ContainmentType.Inside;

      Vector3 xAxis = Vector3.UnitX;
      Vector3 yAxis = Vector3.UnitY;
      Vector3 zAxis = Vector3.UnitZ;

      if (GeometricToolsHelper.IntersectTriangleBox(triangle, m_center, xAxis, yAxis, zAxis, m_extents))
        return ContainmentType.Intersects;

      return ContainmentType.Outside;
    }

    /// <summary>
    /// Determines if the specified ellipse intersects with the bounding volume.
    /// </summary>
    /// <param name="ellipse">Ellipse to test against.</param>
    /// <returns>True if the bounding volume intersects with the ellipse, false otherwise.</returns>
    public override ContainmentType Contains(in Ellipse ellipse)
    {
      //TODO
      throw new NotImplementedException();
    }

    /// <summary>
    /// Determines if the specified bounding volume is contained inside the bounding volume.
    /// </summary>
    /// <param name="volume">Bounding volume to test against.</param>
    /// <returns>Type of containment.</returns>
    public override ContainmentType Contains(BoundingVolume? volume)
    {
      if (volume is null)
        return ContainmentType.Outside;

      switch (volume.BoundingType)
      {
        case BoundingType.AxisAlignedBoundingBox:
          {
            BoundingBox other = (volume as BoundingBox)!;
            return GeometricToolsHelper.AABBContainsAABB(ref m_center, ref m_extents, ref other.m_center, ref other.m_extents);
          }
        case BoundingType.Sphere:
          {
            BoundingSphere other = (volume as BoundingSphere)!;
            Vector3 sphereCenter = other.Center;
            float sphereRadius = other.Radius;

            return GeometricToolsHelper.AABBContainsSphere(m_center, m_extents, sphereCenter, sphereRadius);
          }
        case BoundingType.Capsule:
          {
            BoundingCapsule other = (volume as BoundingCapsule)!;
            Segment centerLine = other.CenterLine;
            float capsuleRadius = other.Radius;

            return GeometricToolsHelper.AABBContainsCapsule(m_center, m_extents, centerLine, capsuleRadius);
          }
        case BoundingType.OrientedBoundingBox:
          {
            OrientedBoundingBox other = (volume as OrientedBoundingBox)!;
            Vector3 boxCenter = other.Center;
            Triad boxAxes = other.Axes;
            Vector3 boxExtents = other.Extents;

            Vector3 xAxis = Vector3.UnitX;
            Vector3 yAxis = Vector3.UnitY;
            Vector3 zAxis = Vector3.UnitZ;

            return GeometricToolsHelper.BoxContainsBox(m_center, xAxis, yAxis, zAxis, m_extents,
                    boxCenter, boxAxes.XAxis, boxAxes.YAxis, boxAxes.ZAxis, boxExtents);
          }
        case BoundingType.Frustum:
        case BoundingType.Mesh:
        default:
          {
            return Contains(volume.Corners);
          }
      }
    }

    /// <summary>
    /// Tests if the specified ray intersects with the bounding volume.
    /// </summary>
    /// <param name="ray">Ray to test against.</param>
    /// <returns>True if the bounding volume intersects with the ray, false otherwise.</returns>
    public override bool Intersects(in Ray ray)
    {
      return GeometricToolsHelper.IntersectRayAABB(ray, m_center, m_extents);
    }

    /// <summary>
    /// Determines if the specified ray intersects with the bounding volume.
    /// </summary>
    /// <param name="ray">Ray to test against.</param>
    /// <param name="result">Bounding intersection result.</param>
    /// <returns>True if the bounding volume intersects with the ray, false otherwise.</returns>
    public override bool Intersects(in Ray ray, out BoundingIntersectionResult result)
    {
      return GeometricToolsHelper.IntersectRayAABB(ray, m_center, m_extents, out result);
    }

    /// <summary>
    /// Determines if the specified segment line intersects with the bounding volume.
    /// </summary>
    /// <param name="line">Segment line to test against.</param>
    /// <returns>True if the bounding volume intersects with the segment line, false otherweise.</returns>
    public override bool Intersects(in Segment line)
    {
      return GeometricToolsHelper.IntersectSegmentAABB(line, m_center, m_extents);
    }

    /// <summary>
    /// Determines if the specified segment line intersects with the bounding volume.
    /// </summary>
    /// <param name="line">Segment line to test against.</param>
    /// <param name="result">Bounding intersection result.</param>
    /// <returns>True if the bounding volume intersects with the segment line, false otherweise.</returns>
    public override bool Intersects(in Segment line, out BoundingIntersectionResult result)
    {
      return GeometricToolsHelper.IntersectSegmentAABB(line, m_center, m_extents, out result);
    }

    /// <summary>
    /// Tests if the specified plane intersects with the bounding volume.
    /// </summary>
    /// <param name="plane">Plane to test against.</param>
    /// <returns>Type of plane intersection.</returns>
    public override PlaneIntersectionType Intersects(in Plane plane)
    {
      //Plane-AABB intersection 
      //Real-Time Collision Detection by Christer Ericson

      Vector3 max = new Vector3(m_center.X + m_extents.X, m_center.Y + m_extents.Y, m_center.Z + m_extents.Z);
      Vector3 min = new Vector3(m_center.X - m_extents.X, m_center.Y - m_extents.Y, m_center.Z - m_extents.Z);

      Vector3 n = max;
      Vector3 p = min;

      if (plane.Normal.X >= 0.0f)
      {
        p.X = max.X;
        n.X = min.X;
      }

      if (plane.Normal.Y >= 0.0f)
      {
        p.Y = max.Y;
        n.Y = min.Y;
      }

      if (plane.Normal.Z >= 0.0f)
      {
        p.Z = max.Z;
        n.Z = min.Z;
      }

      if (plane.WhichSide(n) == PlaneIntersectionType.Front)
        return PlaneIntersectionType.Front;

      if (plane.WhichSide(p) == PlaneIntersectionType.Back)
        return PlaneIntersectionType.Back;

      return PlaneIntersectionType.Intersects;
    }

    /// <summary>
    /// Determines if the specified bounding volume intersects with the bounding volume.
    /// </summary>
    /// <param name="volume">Other bounding volume to test against.</param>
    /// <returns>True if the two volumes intersect with one another, false otherwise.</returns>
    public override bool Intersects(BoundingVolume? volume)
    {
      if (volume is null)
        return false;

      switch (volume.BoundingType)
      {
        case BoundingType.AxisAlignedBoundingBox:
          {
            BoundingBox other = (volume as BoundingBox)!;

            return GeometricToolsHelper.IntersectAABBAABB(m_center, m_extents, other.m_center, other.m_extents);
          }
        case BoundingType.Sphere:
          {
            BoundingSphere other = (volume as BoundingSphere)!;
            Vector3 sphereCenter = other.Center;
            float sphereRadius = other.Radius;

            return GeometricToolsHelper.IntersectSphereAABB(sphereCenter, sphereRadius, m_center, m_extents);
          }
        case BoundingType.Capsule:
          {
            BoundingCapsule other = (volume as BoundingCapsule)!;
            Segment centerLine = other.CenterLine;
            float capsuleRadius = other.Radius;

            return GeometricToolsHelper.IntersectCapsuleAABB(centerLine, capsuleRadius, m_center, m_extents);
          }
        case BoundingType.OrientedBoundingBox:
          {
            OrientedBoundingBox other = (volume as OrientedBoundingBox)!;
            Vector3 boxCenter = other.Center;
            Triad boxAxes = other.Axes;
            Vector3 boxExtents = other.Extents;

            Vector3 unitXAxis = Vector3.UnitX;
            Vector3 unitYAxis = Vector3.UnitY;
            Vector3 unitZAxis = Vector3.UnitZ;

            return GeometricToolsHelper.IntersectBoxBox(m_center, unitXAxis, unitYAxis, unitZAxis, m_extents,
                     boxCenter, boxAxes.XAxis, boxAxes.YAxis, boxAxes.ZAxis, boxExtents);
          }
        case BoundingType.Frustum:
          {
            ContainmentType contain = GeometricToolsHelper.FrustumContainsAABB((volume as BoundingFrustum)!, m_center, m_extents);

            return contain != ContainmentType.Outside;
          }
        case BoundingType.Mesh:
        default:
          return IntersectsGeneral(volume);
      }
    }

    /// <summary>
    /// Merges the bounding volume with the specified bounding volume, resulting in a volume that encloses both.
    /// </summary>
    /// <param name="volume">Bounding volume to merge with.</param>
    public override void Merge(BoundingVolume? volume)
    {
      if (volume is null)
        return;

      switch (volume.BoundingType)
      {
        case BoundingType.AxisAlignedBoundingBox:
          {
            BoundingBox other = (volume as BoundingBox)!;

            BoundingBox.Data b0;
            b0.Center = m_center;
            b0.Extents = m_extents;

            BoundingBox.Data b1;
            b1.Center = other.m_center;
            b1.Extents = other.m_extents;

            BoundingBox.Data merged;
            GeometricToolsHelper.MergeAABBABB(b0, b1, out merged);

            m_center = merged.Center;
            m_extents = merged.Extents;
          }
          break;
        case BoundingType.Sphere:
          {
            BoundingSphere other = (volume as BoundingSphere)!;
            float sphereRadius = other.Radius;

            BoundingBox.Data b0;
            b0.Center = m_center;
            b0.Extents = m_extents;

            BoundingBox.Data b1;
            b1.Center = other.Center;
            b1.Extents = new Vector3(sphereRadius, sphereRadius, sphereRadius);

            BoundingBox.Data merged;
            GeometricToolsHelper.MergeAABBABB(b0, b1, out merged);

            m_center = merged.Center;
            m_extents = merged.Extents;
          }
          break;
        case BoundingType.Capsule:
          {
            BoundingCapsule other = (volume as BoundingCapsule)!;
            Segment centerLine = other.CenterLine;
            float capsuleRadius = other.Radius;

            BoundingBox.Data b0;
            b0.Center = m_center;
            b0.Extents = m_extents;

            //Treat capsule as an OBB
            Vector3 segCenter, segDir;
            float segExtent;
            GeometricToolsHelper.CalculateSegmentProperties(centerLine, out segCenter, out segDir, out segExtent);

            OrientedBoundingBox.Data b1;
            b1.Center = segCenter;
            Triad.FromZComplementBasis(segDir, out b1.Axes);
            b1.Extents = new Vector3(capsuleRadius, capsuleRadius, segExtent + capsuleRadius);

            BoundingBox.Data merged;
            GeometricToolsHelper.MergeAABBBox(b0, b1, out merged);

            m_center = merged.Center;
            m_extents = merged.Extents;
          }
          break;
        case BoundingType.OrientedBoundingBox:
          {
            OrientedBoundingBox other = (volume as OrientedBoundingBox)!;

            BoundingBox.Data b0;
            b0.Center = m_center;
            b0.Extents = m_extents;

            OrientedBoundingBox.Data b1;
            b1.Center = other.Center;
            b1.Axes = other.Axes;
            b1.Extents = other.Extents;

            BoundingBox.Data merged;
            GeometricToolsHelper.MergeAABBBox(b0, b1, out merged);

            m_center = merged.Center;
            m_extents = merged.Extents;
          }
          break;
        case BoundingType.Frustum:
        case BoundingType.Mesh:
        default:
          {
            Extend(volume.Corners);
          }
          break;
      }

      UpdateCorners = true;
    }

    /// <summary>
    /// Transforms the bounding volume by a Scale-Rotation-Translation (SRT) transform.
    /// </summary>
    /// <param name="scale">Scaling</param>
    /// <param name="rotation">Rotation</param>
    /// <param name="translation">Translation</param>
    public override void Transform(in Vector3 scale, in Quaternion rotation, in Vector3 translation)
    {
      //Transform center by the SRT
      Vector3 center;
      Vector3.Multiply(m_center, scale, out center);
      Vector3.Transform(center, rotation, out center);
      Vector3.Add(translation, center, out center);

      //Scale extents first
      Vector3 extents;
      extents.X = MathF.Abs(MathF.Abs(scale.X) * m_extents.X);
      extents.Y = MathF.Abs(MathF.Abs(scale.Y) * m_extents.Y);
      extents.Z = MathF.Abs(MathF.Abs(scale.Z) * m_extents.Z);

      //Then transform extents by rotation
      Matrix rotMat;
      Matrix.FromQuaternion(rotation, out rotMat);

      rotMat.M11 = MathF.Abs(rotMat.M11);
      rotMat.M12 = MathF.Abs(rotMat.M12);
      rotMat.M13 = MathF.Abs(rotMat.M13);
      rotMat.M21 = MathF.Abs(rotMat.M21);
      rotMat.M22 = MathF.Abs(rotMat.M22);
      rotMat.M23 = MathF.Abs(rotMat.M23);
      rotMat.M31 = MathF.Abs(rotMat.M31);
      rotMat.M32 = MathF.Abs(rotMat.M32);
      rotMat.M33 = MathF.Abs(rotMat.M33);

      //We don't care about the last row (translation), so pointless to do extra adds
      Vector3.TransformNormal(extents, rotMat, out extents);

      m_center = center;
      m_extents = extents;
      UpdateCorners = true;
    }

    /// <summary>
    /// Tests equality between the bounding volume and the other bounding volume.
    /// </summary>
    /// <param name="other">Other bounding volume</param>
    /// <param name="tolerance">Tolerance</param>
    /// <returns>True if the volume is of same type and same size/shape, false otherwise.</returns>
    public override bool Equals([NotNullWhen(true)] BoundingVolume? other, float tolerance)
    {
      BoundingBox? bb = other as BoundingBox;
      if (bb is not null)
        return m_center.Equals(bb.m_center, tolerance) && m_extents.Equals(bb.m_extents, tolerance);

      return false;
    }

    /// <summary>
    /// Reads the object data from the input.
    /// </summary>
    /// <param name="input">Savable reader</param>
    public override void Read(ISavableReader input)
    {
      input.Read<Vector3>("Center", out m_center);
      input.Read<Vector3>("Extents", out m_extents);
      UpdateCorners = true;
    }

    /// <summary>
    /// Writes the object data to the output.
    /// </summary>
    /// <param name="output">Savable writer</param>
    public override void Write(ISavableWriter output)
    {
      output.Write<Vector3>("Center", m_center);
      output.Write<Vector3>("Extents", m_extents);
    }

    /// <summary>
    /// Computes the corners that represent the extremal points of this bounding volume.
    /// </summary>
    /// <param name="corners">Databuffer to contain the points, length equal to the corner count.</param>
    /// <exception cref="System.ArgumentOutOfRangeException">Thrown if the corner buffer does not have enough space.</exception>
    protected override void ComputeCorners(Span<Vector3> corners)
    {
      if (corners.Length < CornerCount)
        throw new ArgumentOutOfRangeException(StringLocalizer.Instance.GetLocalizedString("NotEnoughSpaceForBoundingCorners", CornerCount.ToString()));

      Vector3 max = new Vector3(m_center.X + m_extents.X, m_center.Y + m_extents.Y, m_center.Z + m_extents.Z);
      Vector3 min = new Vector3(m_center.X - m_extents.X, m_center.Y - m_extents.Y, m_center.Z - m_extents.Z);

      corners[0] =new Vector3(min.X, max.Y, max.Z);
      corners[1] = new Vector3(max.X, max.Y, max.Z);
      corners[2] = new Vector3(max.X, min.Y, max.Z);
      corners[3] = new Vector3(min.X, min.Y, max.Z);
      corners[4] = new Vector3(min.X, max.Y, min.Z);
      corners[5] = new Vector3(max.X, max.Y, min.Z);
      corners[6] = new Vector3(max.X, min.Y, min.Z);
      corners[7] = new Vector3(min.X, min.Y, min.Z);
    }

    #region BoundingBox Data

    /// <summary>
    /// Represents data for a <see cref="BoundingBox"/>.
    /// </summary>
    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 4)]
    public struct Data : IEquatable<BoundingBox.Data>, IRefEquatable<BoundingBox.Data>, IFormattable, IPrimitiveValue
    {
      /// <summary>
      /// Center of the bounding box.
      /// </summary>
      public Vector3 Center;

      /// <summary>
      /// Extents of the bounding box (half-lengths along each axis from the center).
      /// </summary>
      public Vector3 Extents;

      /// <summary>
      /// Constructs a new instance of the <see cref="BoundingBox.Data"/> struct.
      /// </summary>
      /// <param name="center">Center of the bounding box.</param>
      /// <param name="extents">Extents of the bounding box.</param>
      public Data(in Vector3 center, in Vector3 extents)
      {
        Center = center;
        Extents = extents;
      }

      /// <summary>
      /// Constructs a new instance of the <see cref="BoundingBox.Data"/> struct.
      /// </summary>
      /// <param name="box">Bounding box to copy from.</param>
      public Data(BoundingBox? box)
      {
        ArgumentNullException.ThrowIfNull(box, nameof(box));

        Center = box.m_center;
        Extents = box.m_extents;
      }

      /// <summary>
      /// Determines if the AABB intersects with another.
      /// </summary>
      /// <param name="box">AABB to test against.</param>
      /// <returns>True if the objects intersect, false otherwise.</returns>
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public readonly bool Intersects(in BoundingBox.Data box)
      {
        return GeometricToolsHelper.IntersectAABBAABB(Center, Extents, box.Center, box.Extents);
      }

      /// <summary>
      /// Determines if the AABB intersects with a sphere.
      /// </summary>
      /// <param name="sphere">Sphere to test against</param>
      /// <returns>True if the objects intersect, false otherwise.</returns>
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public readonly bool Intersects(in BoundingSphere.Data sphere)
      {
        return GeometricToolsHelper.IntersectSphereAABB(sphere.Center, sphere.Radius, Center, Extents);
      }

      /// <summary>
      /// Computes a minimum bounding volume that encloses the specified points in space.
      /// </summary>
      /// <param name="points">Points in space</param>
      /// <param name="subMeshRange">Optional range inside the buffer that represents the mesh to compute bounds for. Note: the base vertex offset will be ignored.</param>
      /// <returns>Computed bounding box</returns>
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static BoundingBox.Data FromPoints(ReadOnlySpan<Vector3> points, in SubMeshRange? subMeshRange = null)
      {
        BoundingBox.Data result;
        FromPoints(points, subMeshRange, out result);

        return result;
      }

      /// <summary>
      /// Computes a minimum bounding volume that encloses the specified points in space.
      /// </summary>
      /// <param name="points">Points in space</param>
      /// <param name="result">Computed bounding box</param>
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static void FromPoints(ReadOnlySpan<Vector3> points, out BoundingBox.Data result)
      {
        FromPoints(points, null, out result);
      }

      /// <summary>
      /// Computes a minimum bounding volume that encloses the specified points in space.
      /// </summary>
      /// <param name="points">Points in space</param>
      /// <param name="subMeshRange">Optional range inside the buffer that represents the mesh to compute bounds for. Note: the base vertex offset will be ignored.</param>
      /// <param name="result">Computed bounding box</param>
      public static void FromPoints(ReadOnlySpan<Vector3> points, in SubMeshRange? subMeshRange, out BoundingBox.Data result)
      {
        int offset = 0;
        int count = 0;

        if (!BoundingVolume.ExtractSubMeshRange(points, subMeshRange, out offset, out count))
        {
          result.Center = Vector3.Zero;
          result.Extents = Vector3.Zero;

          return;
        }

        //Trivial case
        if (count == 1)
        {
          result.Center = points[offset];
          result.Extents = Vector3.Zero;
          return;
        }

        Vector3 min = new Vector3(float.MaxValue);
        Vector3 max = new Vector3(float.MinValue);

        int upperBoundExclusive = offset + count;
        for (int i = offset; i < upperBoundExclusive; i++)
        {
          ref readonly Vector3 pt = ref points[i];
          Vector3.Min(min, pt, out min);
          Vector3.Max(max, pt, out max);
        }

        //Compute center from min-max
        Vector3.Add(max, min, out result.Center);
        Vector3.Multiply(result.Center, 0.5f, out result.Center);

        //Compute extents from min-max
        Vector3.Subtract(max, result.Center, out result.Extents);
      }

      /// <summary>
      /// Computes a minimum bounding volume that encloses the specified indexed points in space.
      /// </summary>
      /// <param name="points">Points in space.</param>
      /// <param name="indices">Point indices denoting location in the point buffer.</param>
      /// <param name="subMeshRange">Optional range inside the buffer that represents the mesh to compute bounds for.</param>
      /// <returns>Computed bounding box</returns>
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static BoundingBox.Data FromIndexedPoints(ReadOnlySpan<Vector3> points, IndexData indices, in SubMeshRange? subMeshRange = null)
      {
        BoundingBox.Data result;
        FromIndexedPoints(points, indices, subMeshRange, out result);

        return result;
      }

      /// Computes a minimum bounding volume that encloses the specified indexed points in space.
      /// </summary>
      /// <param name="points">Points in space.</param>
      /// <param name="indices">Point indices denoting location in the point buffer.</param>
      /// <param name="result">Computed bounding box</param>
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static void FromIndexedPoints(ReadOnlySpan<Vector3> points, IndexData indices, out BoundingBox.Data result)
      {
        FromIndexedPoints(points, indices, null, out result);
      }

      /// <summary>
      /// Computes a minimum bounding volume that encloses the specified indexed points in space.
      /// </summary>
      /// <param name="points">Points in space.</param>
      /// <param name="indices">Point indices denoting location in the point buffer.</param>
      /// <param name="subMeshRange">Optional range inside the buffer that represents the mesh to compute bounds for.</param>
      /// <param name="result">Computed bounding box</param>
      public static void FromIndexedPoints(ReadOnlySpan<Vector3> points, IndexData indices, in SubMeshRange? subMeshRange, out BoundingBox.Data result)
      {
        int offset = 0;
        int count = 0;
        int baseVertexOffset = 0;

        if (!BoundingVolume.ExtractSubMeshRange(points, indices, subMeshRange, out offset, out count, out baseVertexOffset))
        {
          result.Center = Vector3.Zero;
          result.Extents = Vector3.Zero;

          return;
        }

        //Trivial case
        if (count == 1)
        {
          result.Center = points[indices[offset] + baseVertexOffset];
          result.Extents = Vector3.Zero;
        }

        Vector3 min = new Vector3(float.MaxValue);
        Vector3 max = new Vector3(float.MinValue);

        int upperBoundExclusive = offset + count;
        for (int i = offset; i < upperBoundExclusive; i++)
        {
          int index = indices[i];
          ref readonly Vector3 pt = ref points[index + baseVertexOffset];
          Vector3.Min(min, pt, out min);
          Vector3.Max(max, pt, out max);
        }

        //Compute center from min-max
        Vector3.Add(max, min, out result.Center);
        Vector3.Multiply(result.Center, 0.5f, out result.Center);

        //Compute extents from min-max
        Vector3.Subtract(max, result.Center, out result.Extents);
      }

      /// <summary>
      /// Determines whether the specified <see cref="System.Object" /> is equal to this instance.
      /// </summary>
      /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
      /// <returns>True if the specified <see cref="System.Object" /> is equal to this instance; otherwise, false.</returns>
      public readonly override bool Equals([NotNullWhen(true)] object? obj)
      {
        if (obj is BoundingBox.Data)
          return Equals((BoundingBox.Data)obj);

        return false;
      }

      /// <summary>
      /// Tests equality between this bounding box and another.
      /// </summary>
      /// <param name="other">Bounding box</param>
      /// <returns>True if equal, false otherwise.</returns>
      readonly bool IEquatable<BoundingBox.Data>.Equals(BoundingBox.Data other)
      {
        return Center.Equals(other.Center) && Extents.Equals(other.Extents);
      }

      /// <summary>
      /// Tests equality between this bounding box and another.
      /// </summary>
      /// <param name="other">Bounding box</param>
      /// <returns>True if equal, false otherwise.</returns>
      public readonly bool Equals(in BoundingBox.Data other)
      {
        return Center.Equals(other.Center) && Extents.Equals(other.Extents);
      }

      /// <summary>
      /// Tests equality between this bounding box and another.
      /// </summary>
      /// <param name="other">Bounding box</param>
      /// <param name="tolerance">Tolerance</param>
      /// <returns>True if equal, false otherwise.</returns>
      public readonly bool Equals(in BoundingBox.Data other, float tolerance)
      {
        return Center.Equals(other.Center, tolerance) && Extents.Equals(other.Extents, tolerance);
      }

      /// <summary>
      /// Returns a hash code for this instance.
      /// </summary>
      /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.</returns>
      public override int GetHashCode()
      {
        unchecked
        {
          return Center.GetHashCode() + Extents.GetHashCode();
        }
      }

      /// <summary>
      /// Returns a <see cref="System.String" /> that represents this instance.
      /// </summary>
      /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
      public readonly override string ToString()
      {
        CultureInfo info = CultureInfo.CurrentCulture;
        return String.Format(info, "Center: {0}, Extents: {1}",
            new Object[] { Center.ToString(info), Extents.ToString(info) });
      }

      /// <summary>
      /// Returns a <see cref="System.String" /> that represents this instance.
      /// </summary>
      /// <param name="format">The format</param>
      /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
      public readonly string ToString(string? format)
      {
        if (format is null)
          return ToString();

        CultureInfo info = CultureInfo.CurrentCulture;
        return String.Format(info, "Center: {0}, Extents: {1}",
            new Object[] { Center.ToString(format, info), Extents.ToString(format, info) });
      }

      /// <summary>
      /// Returns a <see cref="System.String" /> that represents this instance.
      /// </summary>
      /// <param name="formatProvider">The format provider.</param>
      /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
      public readonly string ToString(IFormatProvider? formatProvider)
      {
        if (formatProvider is null)
          return ToString();

        return String.Format(formatProvider, "Center: {0}, Extents: {1}",
            new Object[] { Center.ToString(formatProvider), Extents.ToString(formatProvider) });
      }

      /// <summary>
      /// Returns a <see cref="System.String" /> that represents this instance.
      /// </summary>
      /// <param name="format">The format.</param>
      /// <param name="formatProvider">The format provider.</param>
      /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
      public readonly string ToString(string? format, IFormatProvider? formatProvider)
      {
        if (formatProvider is null)
          return ToString();

        if (format is null)
          return ToString(formatProvider);

        return String.Format(formatProvider, "Center: {0}, Extents: {1}",
            new Object[] { Center.ToString(format, formatProvider), Extents.ToString(format, formatProvider) });
      }

      /// <summary>
      /// Writes the primitive data to the output.
      /// </summary>
      /// <param name="output">Primitive writer.</param>
      readonly void IPrimitiveValue.Write(IPrimitiveWriter output)
      {
        output.Write<Vector3>("Center", Center);
        output.Write<Vector3>("Extents", Extents);
      }

      /// <summary>
      /// Reads the primitive data from the input.
      /// </summary>
      /// <param name="input">Primitive reader.</param>
      void IPrimitiveValue.Read(IPrimitiveReader input)
      {
        input.Read<Vector3>("Center", out Center);
        input.Read<Vector3>("Extents", out Extents);
      }
    }

    #endregion
  }
}
