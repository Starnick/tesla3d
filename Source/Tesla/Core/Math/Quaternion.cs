﻿/*
* Copyright (c) 2010-2024 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Tesla.Content;
using Tesla.Design;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Defines a 4 dimensional vector that represents a rotation, where the XYZ components is the axis that an object is rotated about 
  /// by the angle theta. The W component is equal to cos(theta/2.0).
  /// </summary>
  [Serializable]
  [StructLayout(LayoutKind.Sequential, Pack = 4)]
  [TypeConverter(typeof(QuaternionTypeConverter))]
  public struct Quaternion : IEquatable<Quaternion>, IRefEquatable<Quaternion>, IFormattable, IPrimitiveValue
  {
    /// <summary>
    /// X component of the vector part of the quaternion.
    /// </summary>
    public float X;

    /// <summary>
    /// Y component of the vector part of the quaternion.
    /// </summary>
    public float Y;

    /// <summary>
    /// Z component of the vector part of the quaternion.
    /// </summary>
    public float Z;

    /// <summary>
    /// Rotation component of the quaternion.
    /// </summary>
    public float W;

    private static readonly Quaternion s_identity = new Quaternion(0.0f, 0.0f, 0.0f, 1.0f);
    private static readonly Quaternion s_one = new Quaternion(1.0f, 1.0f, 1.0f, 1.0f);

    private static readonly int s_sizeInbytes = BufferHelper.SizeOf<Quaternion>();

    /// <summary>
    /// Gets the identity <see cref="Quaternion"/> set to (0, 0, 0, 1).
    /// </summary>
    public static ref readonly Quaternion Identity
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_identity;
      }
    }

    /// <summary>
    /// Gets the <see cref="Quaternion"/> set to (1, 1, 1, 1).
    /// </summary>
    public static ref readonly Quaternion One
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_one;
      }
    }

    /// <summary>
    /// Gets the size of the <see cref="Quaternion"/> type in bytes.
    /// </summary>
    public static int SizeInBytes
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return s_sizeInbytes;
      }
    }

    /// <summary>
    /// Gets if the quaternion is equal to the identity quaternion.
    /// </summary>
    public readonly bool IsIdentity
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        ref readonly Quaternion identity = ref Quaternion.Identity;
        return this.Equals(identity);
      }
    }

    /// <summary>
    /// Gets if the quaternion is normalized.
    /// </summary>
    public readonly bool IsNormalized
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return MathF.Abs(((X * X) + (Y * Y) + (Z * Z) + (W * W)) - 1.0f) <= MathHelper.ZeroTolerance;
      }
    }

    /// <summary>
    /// Gets whether any of the components of the quaternion are NaN (Not A Number).
    /// </summary>
    public readonly bool IsNaN
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return float.IsNaN(X) || float.IsNaN(Y) || float.IsNaN(Z) || float.IsNaN(W);
      }
    }

    /// <summary>
    /// Gets whether any of the components of the quaternion are positive or negative infinity.
    /// </summary>
    public readonly bool IsInfinity
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return float.IsNegativeInfinity(X) || float.IsPositiveInfinity(X) || float.IsNegativeInfinity(Y) || float.IsPositiveInfinity(Y) ||
            float.IsNegativeInfinity(Z) || float.IsPositiveInfinity(Z) || float.IsNegativeInfinity(W) || float.IsPositiveInfinity(W);
      }
    }

    /// <summary>
    /// Gets the angle of the quaternion.
    /// </summary>
    public readonly Angle Angle
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        float length = (X * X) + (Y * Y) + (Z * Z);

        if (length < MathHelper.ZeroTolerance)
          return Angle.Zero;

        return Angle.FromRadians(2.0f * MathF.Acos(MathHelper.Clamp(W, -1.0f, 1.0f)));
      }
    }

    /// <summary>
    /// Gets the axis of the quaternion.
    /// </summary>
    public readonly Vector3 Axis
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        float length = (X * X) + (Y * Y) + (Z * Z);

        if (length < MathHelper.ZeroTolerance)
          return Vector3.UnitX;

        float invLength = 1.0f / length;

        Vector3 result;
        result.X = X * invLength;
        result.Y = Y * invLength;
        result.Z = Z * invLength;

        return result;
      }
    }

    /// <summary>
    /// Gets or sets individual components of the quaternion in the order that the components are declared.
    /// </summary>
    /// <param name="index">Zero-based index, in range of [0, 3].</param>
    /// <returns>The value of the specified component.</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the index is out of range [0, 3]</exception>
    public float this[int index]
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      readonly get
      {
        switch (index)
        {
          case 0:
            return X;
          case 1:
            return Y;
          case 2:
            return Z;
          case 3:
            return W;
          default:
            throw new ArgumentOutOfRangeException(nameof(index), StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));
        }
      }
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      set
      {
        switch (index)
        {
          case 0:
            X = value;
            break;
          case 1:
            Y = value;
            break;
          case 2:
            Z = value;
            break;
          case 3:
            W = value;
            break;
          default:
            throw new ArgumentOutOfRangeException(nameof(index), StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));
        }
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Quaternion"/> struct.
    /// </summary>
    /// <param name="xyz">Vector part</param>
    /// <param name="w">Rotation component</param>
    public Quaternion(in Vector3 xyz, float w)
    {
      X = xyz.X;
      Y = xyz.Y;
      Z = xyz.Z;
      W = w;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Quaternion"/> struct.
    /// </summary>
    /// <param name="xy">Vector part containing XY components</param>
    /// <param name="z">Z component of the vector part</param>
    /// <param name="w">Rotation component</param>
    public Quaternion(in Vector3 xy, float z, float w)
    {
      X = xy.X;
      Y = xy.Y;
      Z = z;
      W = w;
    }

    /// <summary>
    /// Constructs a new Quaternion.
    /// </summary>
    /// <param name="x">X component of the vector part</param>
    /// <param name="y">Y component of the vector part</param>
    /// <param name="z">Z component of the vector part</param>
    /// <param name="w">Rotation component</param>
    public Quaternion(float x, float y, float z, float w)
    {
      X = x;
      Y = y;
      Z = z;
      W = w;
    }

    /// <summary>
    /// Adds two quaternions.
    /// </summary>
    /// <param name="a">First quaternion</param>
    /// <param name="b">Second quaternion</param>
    /// <returns>Sum of the two quaternions</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Quaternion Add(in Quaternion a, in Quaternion b)
    {
      Quaternion result;
      result.X = a.X + b.X;
      result.Y = a.Y + b.Y;
      result.Z = a.Z + b.Z;
      result.W = a.W + b.W;

      return result;
    }

    /// <summary>
    /// Adds two quaternions.
    /// </summary>
    /// <param name="a">First quaternion</param>
    /// <param name="b">Second quaternion</param>
    /// <param name="result">Sum of the two quaternions</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Add(in Quaternion a, in Quaternion b, out Quaternion result)
    {
      result.X = a.X + b.X;
      result.Y = a.Y + b.Y;
      result.Z = a.Z + b.Z;
      result.W = a.W + b.W;
    }

    /// <summary>
    /// Subtracts quaternion b from quaternion a.
    /// </summary>
    /// <param name="a">First quaternion</param>
    /// <param name="b">Second quaternion</param>
    /// <returns>Difference of the two quaternions</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Quaternion Subtract(in Quaternion a, in Quaternion b)
    {
      Quaternion result;
      result.X = a.X - b.X;
      result.Y = a.Y - b.Y;
      result.Z = a.Z - b.Z;
      result.W = a.W - b.W;

      return result;
    }

    /// <summary>
    /// Subtracts quaternion b from quaternion a.
    /// </summary>
    /// <param name="a">First quaternion</param>
    /// <param name="b">Second quaternion</param>
    /// <param name="result">Difference of the two quaternions</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Subtract(in Quaternion a, in Quaternion b, out Quaternion result)
    {
      result.X = a.X - b.X;
      result.Y = a.Y - b.Y;
      result.Z = a.Z - b.Z;
      result.W = a.W - b.W;
    }

    /// <summary>
    /// Multiplies two quaternions together.
    /// </summary>
    /// <param name="a">First quaternion</param>
    /// <param name="b">Second quaternion</param>
    /// <returns>Product of the two quaternions</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Quaternion Multiply(in Quaternion a, in Quaternion b)
    {
      float x1 = a.X;
      float y1 = a.Y;
      float z1 = a.Z;
      float w1 = a.W;

      float x2 = b.X;
      float y2 = b.Y;
      float z2 = b.Z;
      float w2 = b.W;

      float a1 = (y1 * z2) - (z1 * y2);
      float a2 = (z1 * x2) - (x1 * z2);
      float a3 = (x1 * y2) - (y1 * x2);
      float a4 = ((x1 * x2) + (y1 * y2)) + (z1 * z2);

      Quaternion result;
      result.X = ((x1 * w2) + (x2 * w1)) + a1;
      result.Y = ((y1 * w2) + (y2 * w1)) + a2;
      result.Z = ((z1 * w2) + (z2 * w1)) + a3;
      result.W = (w1 * w2) - a4;

      return result;
    }

    /// <summary>
    /// Multiplies two quaternions together.
    /// </summary>
    /// <param name="a">First quaternion</param>
    /// <param name="b">Second quaternion</param>
    /// <param name="result">Product of the two quaternions</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Multiply(in Quaternion a, in Quaternion b, out Quaternion result)
    {
      float x1 = a.X;
      float y1 = a.Y;
      float z1 = a.Z;
      float w1 = a.W;

      float x2 = b.X;
      float y2 = b.Y;
      float z2 = b.Z;
      float w2 = b.W;

      float a1 = (y1 * z2) - (z1 * y2);
      float a2 = (z1 * x2) - (x1 * z2);
      float a3 = (x1 * y2) - (y1 * x2);
      float a4 = ((x1 * x2) + (y1 * y2)) + (z1 * z2);

      result.X = ((x1 * w2) + (x2 * w1)) + a1;
      result.Y = ((y1 * w2) + (y2 * w1)) + a2;
      result.Z = ((z1 * w2) + (z2 * w1)) + a3;
      result.W = (w1 * w2) - a4;
    }

    /// <summary>
    /// Multiplies a quaternion by a scalar value.
    /// </summary>
    /// <param name="value">Source quaternion</param>
    /// <param name="scale">Amount to scale</param>
    /// <returns>Scaled quaternion</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Quaternion Multiply(in Quaternion value, float scale)
    {
      Quaternion result;
      result.X = value.X * scale;
      result.Y = value.Y * scale;
      result.Z = value.Z * scale;
      result.W = value.W * scale;

      return result;
    }

    /// <summary>
    /// Multiplies a quaternion by a scalar value.
    /// </summary>
    /// <param name="value">Source quaternion</param>
    /// <param name="scale">Amount to scale</param>
    /// <param name="result">Scaled quaternion</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Multiply(in Quaternion value, float scale, out Quaternion result)
    {
      result.X = value.X * scale;
      result.Y = value.Y * scale;
      result.Z = value.Z * scale;
      result.W = value.W * scale;
    }

    /// <summary>
    /// Divides quaternion a by quaternion b.
    /// </summary>
    /// <param name="a">First quaternion</param>
    /// <param name="b">Divisor quaternion</param>
    /// <returns>Quotient of the two quaternions</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Quaternion Divide(in Quaternion a, in Quaternion b)
    {
      float x1 = a.X;
      float y1 = a.Y;
      float z1 = a.Z;
      float w1 = a.W;

      float x2 = b.X;
      float y2 = b.Y;
      float z2 = b.Z;
      float w2 = b.W;

      float num = (((x2 * x2) + (y2 * y2) + (z2 * z2) + (w2 * w2)));
      float invNum = 1.0f / num;

      float m1 = -x2 * invNum;
      float m2 = -y2 * invNum;
      float m3 = -z2 * invNum;
      float m4 = -w2 * invNum;

      float a1 = (y1 * m3) - (z1 * m2);
      float a2 = (z1 * m1) - (x1 * m3);
      float a3 = (x1 * m2) - (y1 * m1);
      float a4 = ((x1 * m1) + (y1 * m2) + (z1 * m3));

      Quaternion result;
      result.X = ((x1 * m4) + (m1 * w1)) + a1;
      result.Y = ((y1 * m4) + (m2 * w1)) + a2;
      result.Z = ((z1 * m4) + (m3 * w1)) + a3;
      result.W = (w1 * m4) - a4;

      return result;
    }

    /// <summary>
    /// Divides quaternion a by quaternion b.
    /// </summary>
    /// <param name="a">First quaternion</param>
    /// <param name="b">Divisor quaternion</param>
    /// <param name="result">Quotient of the two quaternions</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Divide(in Quaternion a, in Quaternion b, out Quaternion result)
    {
      float x1 = a.X;
      float y1 = a.Y;
      float z1 = a.Z;
      float w1 = a.W;

      float x2 = b.X;
      float y2 = b.Y;
      float z2 = b.Z;
      float w2 = b.W;

      float num = (((x2 * x2) + (y2 * y2) + (z2 * z2) + (w2 * w2)));
      float invNum = 1.0f / num;

      float m1 = -x2 * invNum;
      float m2 = -y2 * invNum;
      float m3 = -z2 * invNum;
      float m4 = -w2 * invNum;

      float a1 = (y1 * m3) - (z1 * m2);
      float a2 = (z1 * m1) - (x1 * m3);
      float a3 = (x1 * m2) - (y1 * m1);
      float a4 = ((x1 * m1) + (y1 * m2) + (z1 * m3));

      result.X = ((x1 * m4) + (m1 * w1)) + a1;
      result.Y = ((y1 * m4) + (m2 * w1)) + a2;
      result.Z = ((z1 * m4) + (m3 * w1)) + a3;
      result.W = (w1 * m4) - a4;
    }

    /// <summary>
    /// Reverses the direction of the quaternion where it is facing in the opposite direction.
    /// </summary>
    /// <param name="value">Source quaternion</param>
    /// <returns>Negated quaterion</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Quaternion Negate(in Quaternion value)
    {
      Quaternion result;
      result.X = -value.X;
      result.Y = -value.Y;
      result.Z = -value.Z;
      result.W = -value.W;

      return result;
    }

    /// <summary>
    /// Reverses the direction of the quaternion where it is facing in the opposite direction.
    /// </summary>
    /// <param name="value">Source quaternion</param>
    /// <param name="result">Negated quaterion</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Negate(in Quaternion value, out Quaternion result)
    {
      result.X = -value.X;
      result.Y = -value.Y;
      result.Z = -value.Z;
      result.W = -value.W;
    }

    /// <summary>
    /// Conjugates the quaternion.
    /// </summary>
    /// <param name="value">Source quaternion</param>
    /// <returns>Conjugated quaternion</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Quaternion Conjugate(in Quaternion value)
    {
      Quaternion result;
      result.X = -value.X;
      result.Y = -value.Y;
      result.Z = -value.Z;
      result.W = value.W;
      return result;
    }

    /// <summary>
    /// Conjugates the quaternion.
    /// </summary>
    /// <param name="value">Source quaternion</param>
    /// <param name="result">Conjugated quaternion</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Conjugate(in Quaternion value, out Quaternion result)
    {
      result.X = -value.X;
      result.Y = -value.Y;
      result.Z = -value.Z;
      result.W = value.W;
    }

    /// <summary>
    /// Computes the dot product of two quaternions.
    /// </summary>
    /// <param name="a">First quaternion</param>
    /// <param name="b">Second quaternion</param>
    /// <returns>Dot product</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float Dot(in Quaternion a, in Quaternion b)
    {
      return (a.X * b.X) + (a.Y * b.Y) + (a.Z * b.Z) + (a.W * b.W);
    }

    /// <summary>
    /// Gets the row of a 3x3 rotation matrix as a vector from the quaternion as a normalized rotation vector. The quaternion
    /// is normalized if need be.
    /// </summary>
    /// <param name="rot">Quaternion to get the row vector from</param>
    /// <param name="i">Index of row vector to compute, must be in the range [0, 2]</param>
    /// <returns>Rotation vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector3 GetRotationVector(in Quaternion rot, int i)
    {
      float norm = (rot.W * rot.W) + (rot.X * rot.X) + (rot.Y * rot.Y) + (rot.Z * rot.Z);

      if (norm != 1.0f)
        norm = 1.0f / MathF.Sqrt(norm);

      float xnorm = rot.X * norm;
      float ynorm = rot.Y * norm;
      float znorm = rot.Z * norm;
      float wnorm = rot.W * norm;

      float xx = rot.X * xnorm;
      float xy = rot.X * ynorm;
      float xw = rot.X * wnorm;

      float yy = rot.Y * ynorm;
      float yz = rot.Y * znorm;
      float yw = rot.Y * wnorm;

      float zx = rot.Z * xnorm;
      float zz = rot.Z * znorm;
      float zw = rot.Z * wnorm;

      Vector3 result;

      switch (i)
      {
        case 0:
          result.X = 1.0f - 2.0f * (yy + zz);
          result.Y = 2.0f * (xy + zw);
          result.Z = 2.0f * (zx - yw);
          break;
        case 1:
          result.X = 2.0f * (xy - zw);
          result.Y = 1.0f - 2.0f * (xx + zz);
          result.Z = 2.0f * (yz + xw);
          break;
        case 2:
          result.X = 2.0f * (zx + yw);
          result.Y = 2.0f * (yz - xw);
          result.Z = 1.0f - 2.0f * (xx + yy);
          break;
        default:
          result = Vector3.Zero;
          break;
      }

      return result;
    }

    /// <summary>
    /// Gets the row of a 3x3 rotation matrix as a vector from the quaternion as a normalized rotation vector. The quaternion
    /// is normalized if need be.
    /// </summary>
    /// <param name="rot">Quaternion to get the row vector from</param>
    /// <param name="i">Index of row vector to compute, must be in the range [0, 2]</param>
    /// <param name="result">Rotation vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void GetRotationVector(in Quaternion rot, int i, out Vector3 result)
    {
      float norm = (rot.W * rot.W) + (rot.X * rot.X) + (rot.Y * rot.Y) + (rot.Z * rot.Z);

      if (norm != 1.0f)
        norm = 1.0f / MathF.Sqrt(norm);

      float xnorm = rot.X * norm;
      float ynorm = rot.Y * norm;
      float znorm = rot.Z * norm;
      float wnorm = rot.W * norm;

      float xx = rot.X * xnorm;
      float xy = rot.X * ynorm;
      float xw = rot.X * wnorm;

      float yy = rot.Y * ynorm;
      float yz = rot.Y * znorm;
      float yw = rot.Y * wnorm;

      float zx = rot.Z * xnorm;
      float zz = rot.Z * znorm;
      float zw = rot.Z * wnorm;

      switch (i)
      {
        case 0:
          result.X = 1.0f - 2.0f * (yy + zz);
          result.Y = 2.0f * (xy + zw);
          result.Z = 2.0f * (zx - yw);
          break;
        case 1:
          result.X = 2.0f * (xy - zw);
          result.Y = 1.0f - 2.0f * (xx + zz);
          result.Z = 2.0f * (yz + xw);
          break;
        case 2:
          result.X = 2.0f * (zx + yw);
          result.Y = 2.0f * (yz - xw);
          result.Z = 1.0f - 2.0f * (xx + yy);
          break;
        default:
          result = Vector3.Zero;
          break;
      }
    }

    /// <summary>
    /// Conjugates and renormalizes the quaternion.
    /// </summary>
    /// <param name="value">Source quaternion</param>
    /// <returns>Inverted quaternion</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Quaternion Invert(in Quaternion value)
    {
      float lengthSquared = (value.X * value.X) + (value.Y * value.Y) + (value.Z * value.Z) + (value.W * value.W);

      Quaternion result = value;

      if (lengthSquared != 0.0f)
      {
        float invLengthSquared = 1.0f / lengthSquared;

        result.X = -value.X * invLengthSquared;
        result.Y = -value.Y * invLengthSquared;
        result.Z = -value.Z * invLengthSquared;
        result.W *= invLengthSquared;
      }

      return result;
    }

    /// <summary>
    /// Conjugates and renormalizes the quaternion.
    /// </summary>
    /// <param name="value">Source quaternion</param>
    /// <param name="result">Inverted quaternion</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Invert(in Quaternion value, out Quaternion result)
    {
      float lengthSquared = (value.X * value.X) + (value.Y * value.Y) + (value.Z * value.Z) + (value.W * value.W);

      result = value;

      if (lengthSquared != 0.0f)
      {
        float invLengthSquared = 1.0f / lengthSquared;

        result.X = -value.X * invLengthSquared;
        result.Y = -value.Y * invLengthSquared;
        result.Z = -value.Z * invLengthSquared;
        result.W *= invLengthSquared;
      }
    }

    /// <summary>
    /// Normalizes the quaternion to a unit quaternion, which results in a quaternion with a magnitude of 1.
    /// </summary>
    /// <param name="value">Source quaternion</param>
    /// <returns>Normalized quaternion</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Quaternion Normalize(in Quaternion value)
    {
      float lengthSquared = (value.X * value.X) + (value.Y * value.Y) + (value.Z * value.Z) + (value.W * value.W);

      Quaternion result = value;

      if (lengthSquared != 0.0f)
      {
        float invLength = 1.0f / MathF.Sqrt(lengthSquared);

        result.X *= invLength;
        result.Y *= invLength;
        result.Z *= invLength;
        result.W *= invLength;
      }

      return value;
    }

    /// <summary>
    /// Normalizes the quaternion to a unit quaternion, which results in a quaternion with a magnitude of 1.
    /// </summary>
    /// <param name="value">Source quaternion</param>
    /// <param name="result">Normalized quaternion</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Normalize(ref Quaternion value, out Quaternion result)
    {
      float lengthSquared = (value.X * value.X) + (value.Y * value.Y) + (value.Z * value.Z) + (value.W * value.W);

      result = value;

      if (lengthSquared != 0.0f)
      {
        float invLength = 1.0f / MathF.Sqrt(lengthSquared);

        result.X *= invLength;
        result.Y *= invLength;
        result.Z *= invLength;
        result.W *= invLength;
      }
    }

    /// <summary>
    /// Linearly interpolates between two quaternions.
    /// </summary>
    /// <param name="start">Starting quaternion</param>
    /// <param name="end">Ending quaternion</param>
    /// <param name="percent">Percent to interpolate between the two quaternions (between 0.0 and 1.0)</param>
    /// <returns>Linearly interpolated quaternion</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Quaternion Lerp(in Quaternion start, in Quaternion end, float percent)
    {
      percent = MathHelper.Clamp(percent, 0.0f, 1.0f);
      float percent2 = 1.0f - percent;

      float dot = (start.X * end.X) + (start.Y * end.Y) + (start.Z * end.Z) + (start.W * end.W);
      Quaternion result;

      if (dot >= 0.0f)
      {
        result.X = (percent2 * start.X) + (percent * end.X);
        result.Y = (percent2 * start.Y) + (percent * end.Y);
        result.Z = (percent2 * start.Z) + (percent * end.Z);
        result.W = (percent2 * start.W) + (percent * end.W);
      }
      else
      {
        result.X = (percent2 * start.X) - (percent * end.X);
        result.Y = (percent2 * start.Y) - (percent * end.Y);
        result.Z = (percent2 * start.Z) - (percent * end.Z);
        result.W = (percent2 * start.W) - (percent * end.W);
      }

      result.Normalize();

      return result;
    }

    /// <summary>
    /// Linearly interpolates between two quaternions.
    /// </summary>
    /// <param name="start">Starting quaternion</param>
    /// <param name="end">Ending quaternion</param>
    /// <param name="percent">Percent to interpolate between the two quaternions (between 0.0 and 1.0)</param>
    /// <param name="result">Linearly interpolated quaternion</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Lerp(in Quaternion start, in Quaternion end, float percent, out Quaternion result)
    {
      percent = MathHelper.Clamp(percent, 0.0f, 1.0f);
      float percent2 = 1.0f - percent;

      float dot = (start.X * end.X) + (start.Y * end.Y) + (start.Z * end.Z) + (start.W * end.W);

      if (dot >= 0.0f)
      {
        result.X = (percent2 * start.X) + (percent * end.X);
        result.Y = (percent2 * start.Y) + (percent * end.Y);
        result.Z = (percent2 * start.Z) + (percent * end.Z);
        result.W = (percent2 * start.W) + (percent * end.W);
      }
      else
      {
        result.X = (percent2 * start.X) - (percent * end.X);
        result.Y = (percent2 * start.Y) - (percent * end.Y);
        result.Z = (percent2 * start.Z) - (percent * end.Z);
        result.W = (percent2 * start.W) - (percent * end.W);
      }

      result.Normalize();
    }

    /// <summary>
    /// Interpolates between two quaternions using spherical linear interpolation.
    /// </summary>
    /// <param name="start">Starting quaternion</param>
    /// <param name="end">Ending quaternion</param>
    /// <param name="percent">Percent to interpolate between the two quaternions (between 0.0 and 1.0)</param>
    /// <returns>Spherical linear interpolated quaternion</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Quaternion Slerp(in Quaternion start, in Quaternion end, float percent)
    {
      float opposite, inverse, dot;
      percent = MathHelper.Clamp(percent, 0.0f, 1.0f);

      dot = Quaternion.Dot(start, end);

      if (MathF.Abs(dot) > 1.0f - MathHelper.ZeroTolerance)
      {
        inverse = 1.0f - percent;
        opposite = percent * MathF.Sign(dot);
      }
      else
      {
        float acos = MathF.Acos(MathF.Abs(dot));
        float invSin = 1.0f / MathF.Sin(acos);

        inverse = MathF.Sin((1.0f - percent) * acos) * invSin;
        opposite = MathF.Sin(percent * acos) * invSin * MathF.Sign(dot);
      }

      Quaternion result;
      result.X = (inverse * start.X) + (opposite * end.X);
      result.Y = (inverse * start.Y) + (opposite * end.Y);
      result.Z = (inverse * start.Z) + (opposite * end.Z);
      result.W = (inverse * start.W) + (opposite * end.W);

      return result;
    }

    /// <summary>
    /// Interpolates between two quaternions using spherical linear interpolation.
    /// </summary>
    /// <param name="start">Starting quaternion</param>
    /// <param name="end">Ending quaternion</param>
    /// <param name="percent">Percent to interpolate between the two quaternions (between 0.0 and 1.0)</param>
    /// <param name="result">Spherical linear interpolated quaternion</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Slerp(in Quaternion start, in Quaternion end, float percent, out Quaternion result)
    {
      float opposite, inverse, dot;
      percent = MathHelper.Clamp(percent, 0.0f, 1.0f);

      dot = Quaternion.Dot(start, end);

      if (MathF.Abs(dot) > 1.0f - MathHelper.ZeroTolerance)
      {
        inverse = 1.0f - percent;
        opposite = percent * MathF.Sign(dot);
      }
      else
      {
        float acos = MathF.Acos(MathF.Abs(dot));
        float invSin = 1.0f / MathF.Sin(acos);

        inverse = MathF.Sin((1.0f - percent) * acos) * invSin;
        opposite = MathF.Sin(percent * acos) * invSin * MathF.Sign(dot);
      }

      result.X = (inverse * start.X) + (opposite * end.X);
      result.Y = (inverse * start.Y) + (opposite * end.Y);
      result.Z = (inverse * start.Z) + (opposite * end.Z);
      result.W = (inverse * start.W) + (opposite * end.W);
    }

    /// <summary>
    /// Creates a quaternion from an axis and an angle to rotate about that axis. The axis is assumed to be normalized unit vector.
    /// </summary>
    /// <param name="angle">Angle to rotate</param>
    /// <param name="axis">Axis to rotate about</param>
    /// <returns>Quaternion rotation</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Quaternion FromAngleAxis(Angle angle, in Vector3 axis)
    {
      Angle halfAngle = angle * 0.5f;
      float sin = halfAngle.Sin;
      float cos = halfAngle.Cos;

      Quaternion result;
      result.X = axis.X * sin;
      result.Y = axis.Y * sin;
      result.Z = axis.Z * sin;
      result.W = cos;

      return result;
    }

    /// <summary>
    /// Creates a quaternion from an axis and an angle to rotate about that axis. The axis is assumed to be normalized unit vector.
    /// </summary>
    /// <param name="angle">Angle to rotate</param>
    /// <param name="axis">Axis to rotate about</param>
    /// <param name="result">Quaternion rotation</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void FromAngleAxis(Angle angle, in Vector3 axis, out Quaternion result)
    {
      Angle halfAngle = angle * 0.5f;
      float sin = halfAngle.Sin;
      float cos = halfAngle.Cos;

      result.X = axis.X * sin;
      result.Y = axis.Y * sin;
      result.Z = axis.Z * sin;
      result.W = cos;
    }

    /// <summary>
    /// Creates a quaternion from a matrix that contains a rotation.
    /// </summary>
    /// <param name="m">Source matrix</param>
    /// <returns>Quaternion rotation</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Quaternion FromRotationMatrix(in Matrix m)
    {
      Quaternion.FromRotationMatrix(m, out Quaternion result);

      return result;
    }

    /// <summary>
    /// Creates a quaternion from a matrix that contains a rotation.
    /// </summary>
    /// <param name="m">Source matrix</param>
    /// <param name="result">Quaternion rotation</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void FromRotationMatrix(in Matrix m, out Quaternion result)
    {
      float m11 = m.M11;
      float m12 = m.M12;
      float m13 = m.M13;

      float m21 = m.M21;
      float m22 = m.M22;
      float m23 = m.M23;

      float m31 = m.M31;
      float m32 = m.M32;
      float m33 = m.M33;

      float sqrt, half;
      float trace = m11 + m22 + m33;
      if (trace > 0.0f)
      {
        sqrt = MathF.Sqrt(trace + 1.0f);
        half = 0.5f * sqrt;
        sqrt = 0.5f / sqrt;

        result.X = (m23 - m32) * sqrt;
        result.Y = (m31 - m13) * sqrt;
        result.Z = (m12 - m21) * sqrt;
        result.W = half;

      }
      else if ((m11 >= m22) && (m11 >= m33))
      {
        sqrt = MathF.Sqrt(((1.0f + m11) - m22) - m33);
        half = 0.5f / sqrt;

        result.X = 0.5f * sqrt;
        result.Y = (m12 + m21) * half;
        result.Z = (m13 + m31) * half;
        result.W = (m23 - m32) * half;

      }
      else if (m22 > m33)
      {

        sqrt = MathF.Sqrt(((1.0f + m22) - m11) - m33);
        half = 0.5f / sqrt;

        result.X = (m21 + m12) * half;
        result.Y = 0.5f * sqrt;
        result.Z = (m32 + m23) * half;
        result.W = (m31 - m13) * half;

      }
      else
      {
        sqrt = MathF.Sqrt(((1.0f + m33) - m11) - m22);
        half = 0.5f / sqrt;

        result.X = (m31 + m13) * half;
        result.Y = (m32 + m23) * half;
        result.Z = 0.5f * sqrt;
        result.W = (m12 - m21) * half;
      }
    }

    /// <summary>
    /// Creates a quaternion from euler angles yaw, pitch, roll.
    /// </summary>
    /// <param name="yaw">Rotation about y-axis</param>
    /// <param name="pitch">Rotation about x-axis</param>
    /// <param name="roll">Rotation about z-axis</param>
    /// <returns>Quaternion rotation</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Quaternion FromEulerAngles(Angle yaw, Angle pitch, Angle roll)
    {
      Angle angle = roll * 0.5f;
      float rollSin = angle.Sin;
      float rollCos = angle.Cos;

      angle = pitch * 0.5f;
      float pitchSin = angle.Sin;
      float pitchCos = angle.Cos;

      angle = yaw * 0.5f;
      float yawSin = angle.Sin;
      float yawCos = angle.Cos;

      float yawCosXpitchSin = yawCos * pitchSin;
      float yawSinXpitchCos = yawSin * pitchCos;
      float yawCosXpitchCos = yawCos * pitchCos;
      float yawSinXpitchSin = yawSin * pitchSin;

      Quaternion result;
      result.X = (yawCosXpitchSin * rollCos) + (yawSinXpitchCos * rollSin);
      result.Y = (yawSinXpitchCos * rollCos) - (yawCosXpitchSin * rollSin);
      result.Z = (yawCosXpitchCos * rollSin) - (yawSinXpitchSin * rollCos);
      result.W = (yawCosXpitchCos * rollCos) + (yawSinXpitchSin * rollSin);

      return result;
    }

    /// <summary>
    /// Creates a quaternion from euler angles yaw, pitch, and roll.
    /// </summary>
    /// <param name="yaw">Rotation about y-axis</param>
    /// <param name="pitch">Rotation about x-axis</param>
    /// <param name="roll">Rotation about z-axis</param>
    /// <param name="result">Quaternion rotation</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void FromEulerAngles(Angle yaw, Angle pitch, Angle roll, out Quaternion result)
    {
      Angle angle = roll * 0.5f;
      float rollSin = angle.Sin;
      float rollCos = angle.Cos;

      angle = pitch * 0.5f;
      float pitchSin = angle.Sin;
      float pitchCos = angle.Cos;

      angle = yaw * 0.5f;
      float yawSin = angle.Sin;
      float yawCos = angle.Cos;

      float yawCosXpitchSin = yawCos * pitchSin;
      float yawSinXpitchCos = yawSin * pitchCos;
      float yawCosXpitchCos = yawCos * pitchCos;
      float yawSinXpitchSin = yawSin * pitchSin;

      result.X = (yawCosXpitchSin * rollCos) + (yawSinXpitchCos * rollSin);
      result.Y = (yawSinXpitchCos * rollCos) - (yawCosXpitchSin * rollSin);
      result.Z = (yawCosXpitchCos * rollSin) - (yawSinXpitchSin * rollCos);
      result.W = (yawCosXpitchCos * rollCos) + (yawSinXpitchSin * rollSin);
    }

    /// <summary>
    /// Creates a quaternion that represents a rotation formed by three axes. It is assumed 
    /// the axes are orthogonal to one another and represent a proper right-handed system.
    /// </summary>
    /// <param name="xAxis">X axis of the coordinate system (right)</param>
    /// <param name="yAxis">Y axis of the coordinate system (up)</param>
    /// <param name="zAxis">Z axis of the coordinate system (backward)</param>
    /// <returns>Quaternion rotation</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Quaternion FromAxes(in Vector3 xAxis, in Vector3 yAxis, in Vector3 zAxis)
    {
      Matrix m = new Matrix(xAxis.X, xAxis.Y, xAxis.Z, 0, yAxis.X, yAxis.Y, yAxis.Z, 0, zAxis.X, zAxis.Y, zAxis.Z, 0, 0, 0, 0, 1);
      Quaternion.FromRotationMatrix(m, out Quaternion result);

      return result;
    }

    /// <summary>
    /// Creates a quaternion that represents a rotation formed by three axes. It is assumed 
    /// the axes are orthogonal to one another and represent a proper right-handed system.
    /// </summary>
    /// <param name="xAxis">X axis of the coordinate system (right)</param>
    /// <param name="yAxis">Y axis of the coordinate system (up)</param>
    /// <param name="zAxis">Z axis of the coordinate system (backward)</param>
    /// <param name="result">Quaternion rotation</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void FromAxes(in Vector3 xAxis, in Vector3 yAxis, in Vector3 zAxis, out Quaternion result)
    {
      Matrix m = new Matrix(xAxis.X, xAxis.Y, xAxis.Z, 0, yAxis.X, yAxis.Y, yAxis.Z, 0, zAxis.X, zAxis.Y, zAxis.Z, 0, 0, 0, 0, 1);
      Quaternion.FromRotationMatrix(m, out result);
    }

    /// <summary>
    /// Gets the axes that represents this rotation. The axes are orthogonal to one another and represent a proper right-handed system.
    /// </summary>
    /// <param name="rot">Quaternion rotation.</param>
    /// <param name="xAxis">X axis of the coordinate system (right)</param>
    /// <param name="yAxis">Y axis of the coordinate system (up)</param>
    /// <param name="zAxis">Z axis of the coordinate system (backward)</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void ToAxes(in Quaternion rot, out Vector3 xAxis, out Vector3 yAxis, out Vector3 zAxis)
    {
      Matrix.FromQuaternion(rot, out Matrix m);

      xAxis = m.Right;
      yAxis = m.Up;
      zAxis = m.Backward;
    }

    /// <summary>
    /// Creates a rotation quaternion where the object is facing the target along its z axis.
    /// If your object's "forward" facing is down -Z axis, then the object will correctly face the target.
    /// </summary>
    /// <param name="position">Position of object</param>
    /// <param name="target">Position of target</param>
    /// <param name="worldUp">World up vector</param>
    /// <returns>Quaternion rotation</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Quaternion LookAt(in Vector3 position, in Vector3 target, in Vector3 worldUp)
    {
      Vector3 zAxis;
      Vector3 xAxis;
      Vector3 yAxis;

      Vector3.NormalizedSubtract(position, target, out zAxis);
      Vector3.NormalizedCross(worldUp, zAxis, out xAxis);
      Vector3.NormalizedCross(zAxis, xAxis, out yAxis);

      Quaternion.FromAxes(xAxis, yAxis, zAxis, out Quaternion result);

      return result;
    }

    /// <summary>
    /// Creates a rotation quaternion where the object is facing the target along its z axis.
    /// If your object's "forward" facing is down -Z axis, then the object will correctly face the target.
    /// </summary>
    /// <param name="position">Position of object</param>
    /// <param name="target">Position of target</param>
    /// <param name="worldUp">World up vector</param>
    /// <param name="result">Quaternion rotation</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void LookAt(in Vector3 position, in Vector3 target, in Vector3 worldUp, out Quaternion result)
    {
      Vector3 zAxis;
      Vector3 xAxis;
      Vector3 yAxis;

      Vector3.NormalizedSubtract(position, target, out zAxis);
      Vector3.NormalizedCross(worldUp, zAxis, out xAxis);
      Vector3.NormalizedCross(zAxis, xAxis, out yAxis);

      Quaternion.FromAxes(xAxis, yAxis, zAxis, out result);
    }

    /// <summary>
    /// Create a rotation quaternion which rotates from the start direction to the end direction. Both directions assumed normalized.
    /// </summary>
    /// <param name="fromDirection">Starting direction, normalized.</param>
    /// <param name="toDirection">Ending direction, normalized.</param>
    /// <returns>Rotation between the two vectors.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Quaternion FromToRotation(in Vector3 fromDirection, in Vector3 toDirection)
    {
      FromToRotation(fromDirection, toDirection, out Quaternion result);
      return result;
    }

    /// <summary>
    /// Create a rotation quaternion which rotates from the start direction to the end direction. Both directions assumed normalized.
    /// </summary>
    /// <param name="fromDirection">Starting direction, normalized.</param>
    /// <param name="toDirection">Ending direction, normalized.</param>
    /// <param name="result">Rotation between the two vectors.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void FromToRotation(in Vector3 fromDirection, in Vector3 toDirection, out Quaternion result)
    {
      float dot = Vector3.Dot(fromDirection, toDirection);

      // Parallel
      if (MathHelper.IsEqual(dot, 1.0f))
      {
        result = Quaternion.Identity;
        return;
      } 
      // Opposite
      else if (MathHelper.IsEqual(dot, -1.0f))
      {
        // Find an orthogonal vector to fromDir
        Vector3 ortho = Vector3.Cross(fromDirection, Vector3.UnitX);
        // If collinear, use another axis
        if (ortho.LengthSquared() < MathHelper.ZeroTolerance)
          ortho = Vector3.Cross(fromDirection, Vector3.UnitY);

        ortho.Normalize();

        // Get 180 degree rotation around the ortho vector
        result.X = ortho.X;
        result.Y = ortho.Y;
        result.Z = ortho.Z;
        result.W = 0.0f;
      }
      else
      {
        Vector3 perp = Vector3.Cross(fromDirection, toDirection);
        float sqrt = MathF.Sqrt((1.0f + dot) * 2.0f);
        float invSqrt = 1.0f / sqrt;
        result.X = perp.X * invSqrt;
        result.Y = perp.Y * invSqrt;
        result.Z = perp.Z * invSqrt;
        result.W = sqrt * 0.5f;
      }
    }

    /// <summary>
    /// Tests equality between two quaternions.
    /// </summary>
    /// <param name="a">First quaternion</param>
    /// <param name="b">Second quaternion</param>
    /// <returns>True if components are equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator ==(Quaternion a, Quaternion b)
    {
      return (MathF.Abs(a.X - b.X) <= MathHelper.ZeroTolerance) && (MathF.Abs(a.Y - b.Y) <= MathHelper.ZeroTolerance) &&
          (MathF.Abs(a.Z - b.Z) <= MathHelper.ZeroTolerance) && (MathF.Abs(a.W - b.W) <= MathHelper.ZeroTolerance);
    }

    /// <summary>
    /// Tests inequality between two quaternions
    /// </summary>
    /// <param name="a">First quaternion</param>
    /// <param name="b">Second quaternion</param>
    /// <returns>True if components are not equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator !=(Quaternion a, Quaternion b)
    {
      return (MathF.Abs(a.X - b.X) > MathHelper.ZeroTolerance) || (MathF.Abs(a.Y - b.Y) > MathHelper.ZeroTolerance) ||
          (MathF.Abs(a.Z - b.Z) > MathHelper.ZeroTolerance) || (MathF.Abs(a.W - b.W) > MathHelper.ZeroTolerance);
    }

    /// <summary>
    /// Adds two quaternions.
    /// </summary>
    /// <param name="a">First quaternion</param>
    /// <param name="b">Second quaternion</param>
    /// <returns>Sum of the two quaternions</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Quaternion operator +(Quaternion a, Quaternion b)
    {
      Quaternion result;
      result.X = a.X + b.X;
      result.Y = a.Y + b.Y;
      result.Z = a.Z + b.Z;
      result.W = a.W + b.W;

      return result;
    }

    /// <summary>
    /// Reverses the direction of the quaternion where it is facing in the opposite direction.
    /// </summary>
    /// <param name="value">Source quaternion</param>
    /// <returns>Negated quaterion</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Quaternion operator -(Quaternion value)
    {
      Quaternion result;
      result.X = -value.X;
      result.Y = -value.Y;
      result.Z = -value.Z;
      result.W = -value.W;

      return result;
    }

    /// <summary>
    /// Subtracts quaternion b from quaternion a.
    /// </summary>
    /// <param name="a">First quaternion</param>
    /// <param name="b">Second quaternion</param>
    /// <returns>Difference of the two quaternions</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Quaternion operator -(Quaternion a, Quaternion b)
    {
      Quaternion result;
      result.X = a.X - b.X;
      result.Y = a.Y - b.Y;
      result.Z = a.Z - b.Z;
      result.W = a.W - b.W;

      return result;
    }

    /// <summary>
    /// Multiplies two quaternions together.
    /// </summary>
    /// <param name="a">First quaternion</param>
    /// <param name="b">Second quaternion</param>
    /// <returns>Multiplied quaternion</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Quaternion operator *(in Quaternion a, in Quaternion b)
    {
      float x1 = a.X;
      float y1 = a.Y;
      float z1 = a.Z;
      float w1 = a.W;

      float x2 = b.X;
      float y2 = b.Y;
      float z2 = b.Z;
      float w2 = b.W;

      float a1 = (y1 * z2) - (z1 * y2);
      float a2 = (z1 * x2) - (x1 * z2);
      float a3 = (x1 * y2) - (y1 * x2);
      float a4 = ((x1 * x2) + (y1 * y2)) + (z1 * z2);

      Quaternion result;
      result.X = ((x1 * w2) + (x2 * w1)) + a1;
      result.Y = ((y1 * w2) + (y2 * w1)) + a2;
      result.Z = ((z1 * w2) + (z2 * w1)) + a3;
      result.W = (w1 * w2) - a4;

      return result;
    }

    /// <summary>
    /// Multiplies a quaternion by a scalar value.
    /// </summary>
    /// <param name="value">Source quaternion</param>
    /// <param name="scale">Amount to scale</param>
    /// <returns>Scaled quaternion</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Quaternion operator *(Quaternion value, float scale)
    {
      Quaternion result;
      result.X = value.X * scale;
      result.Y = value.Y * scale;
      result.Z = value.Z * scale;
      result.W = value.W * scale;

      return result;
    }

    /// <summary>
    /// Multiplies a quaternion by a scalar value.
    /// </summary>
    /// <param name="scale">Amount to scale</param>
    /// <param name="value">Source quaternion</param>
    /// <returns>Scaled quaternion</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Quaternion operator *(float scale, Quaternion value)
    {
      Quaternion result;
      result.X = value.X * scale;
      result.Y = value.Y * scale;
      result.Z = value.Z * scale;
      result.W = value.W * scale;

      return result;
    }

    /// <summary>
    /// Divides quaternion a by quaternion b.
    /// </summary>
    /// <param name="a">First quaternion</param>
    /// <param name="b">Divisor quaternion</param>
    /// <returns>Quotient of the two quaternions</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Quaternion operator /(in Quaternion a, in Quaternion b)
    {
      float x1 = a.X;
      float y1 = a.Y;
      float z1 = a.Z;
      float w1 = a.W;

      float x2 = b.X;
      float y2 = b.Y;
      float z2 = b.Z;
      float w2 = b.W;

      float num = (((x2 * x2) + (y2 * y2) + (z2 * z2) + (w2 * w2)));
      float invNum = 1.0f / num;

      float m1 = -x2 * invNum;
      float m2 = -y2 * invNum;
      float m3 = -z2 * invNum;
      float m4 = -w2 * invNum;

      float a1 = (y1 * m3) - (z1 * m2);
      float a2 = (z1 * m1) - (x1 * m3);
      float a3 = (x1 * m2) - (y1 * m1);
      float a4 = ((x1 * m1) + (y1 * m2) + (z1 * m3));

      Quaternion result;
      result.X = ((x1 * m4) + (m1 * w1)) + a1;
      result.Y = ((y1 * m4) + (m2 * w1)) + a2;
      result.Z = ((z1 * m4) + (m3 * w1)) + a3;
      result.W = (w1 * m4) - a4;

      return result;
    }

    /// <summary>
    /// Decomposes a quaternion into euler angles.
    /// </summary>
    /// <param name="yaw">Rotation about y-axis.</param>
    /// <param name="pitch">Rotation about x-axis.</param>
    /// <param name="roll">Rotation about z-axis.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly void ToEulerAngles(out Angle yaw, out Angle pitch, out Angle roll)
    {
      float test = (X * Y) + (Z * W);
      //North pole singularity
      if (test > 0.499f)
      {
        yaw = new Angle(2.0f * MathF.Atan2(X, W));
        pitch = new Angle(0.0f);
        roll = Angle.PiOverTwo;
      }
      //South pole singularity
      else if (test < -0.499f)
      {
        yaw = new Angle(-2.0f * MathF.Atan2(X, W));
        pitch = new Angle(0.0f);
        roll = -Angle.PiOverTwo;
      }
      else
      {
        float xx = X * X;
        float yy = Y * Y;
        float zz = Z * Z;

        yaw = new Angle(MathF.Atan2((2.0f * Y * W) - (2.0f * X * Z), 1.0f - (2.0f * yy) - (2.0f * zz)));
        pitch = new Angle(MathF.Atan2((2.0f * X * W) - (2.0f * Y * Z), 1.0f - (2.0f * xx) - (2.0f * zz)));
        roll = new Angle(MathF.Asin(2.0f * test));
      }

      if (MathF.Abs(yaw.Radians) <= MathHelper.ZeroTolerance)
        yaw = new Angle(0.0f);

      if (MathF.Abs(pitch.Radians) <= MathHelper.ZeroTolerance)
        pitch = new Angle(0.0f);

      if (MathF.Abs(roll.Radians) <= MathHelper.ZeroTolerance)
        roll = new Angle(0.0f);
    }

    /// <summary>
    /// Reverses the direction of the quaternion where it is facing in the opposite direction.
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void Negate()
    {
      X = -X;
      Y = -Y;
      Z = -Z;
      W = -W;
    }

    /// <summary>
    /// Conjugates the quaternion.
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void Conjugate()
    {
      X = -X;
      Y = -Y;
      Z = -Z;
    }

    /// <summary>
    /// Compute the length (magnitude) of the quaternion.
    /// </summary>
    /// <returns>Length</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float Length()
    {
      float lengthSquared = (X * X) + (Y * Y) + (Z * Z) + (W * W);
      return MathF.Sqrt(lengthSquared);
    }

    /// <summary>
    /// Compute the length (magnitude) squared of the quaternion.
    /// </summary>
    /// <returns>Length squared</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float LengthSquared()
    {
      return (X * X) + (Y * Y) + (Z * Z) + (W * W);
    }

    /// <summary>
    /// Conjugates and renormalizes the quaternion.
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void Invert()
    {
      float lengthSquared = (X * X) + (Y * Y) + (Z * Z) + (W * W);

      if (lengthSquared != 0.0f)
      {
        float invSqrt = 1.0f / lengthSquared;
        X = -X * invSqrt;
        Y = -Y * invSqrt;
        Z = -Z * invSqrt;
        W *= invSqrt;
      }
    }

    /// <summary>
    /// Normalizes the quaternion to a unit quaternion, which results in a quaternion with a magnitude of 1.
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void Normalize()
    {
      float lengthSquared = (X * X) + (Y * Y) + (Z * Z) + (W * W);

      if (lengthSquared != 0.0f)
      {
        float invLength = 1.0f / MathF.Sqrt(lengthSquared);
        X *= invLength;
        Y *= invLength;
        Z *= invLength;
        W *= invLength;
      }
    }

    /// <summary>
    /// Tests equality between the quaternion and another quaternion.
    /// </summary>
    /// <param name="other">Quaternion to compare</param>
    /// <returns>True if components are equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    bool IEquatable<Quaternion>.Equals(Quaternion other)
    {
      return (MathF.Abs(X - other.X) <= MathHelper.ZeroTolerance) && (MathF.Abs(Y - other.Y) <= MathHelper.ZeroTolerance) &&
          (MathF.Abs(Z - other.Z) <= MathHelper.ZeroTolerance) && (MathF.Abs(W - other.W) <= MathHelper.ZeroTolerance);
    }

    /// <summary>
    /// Tests equality between the quaternion and another quaternion.
    /// </summary>
    /// <param name="other">Quaternion to compare</param>
    /// <returns>True if components are equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Equals(in Quaternion other)
    {
      return (MathF.Abs(X - other.X) <= MathHelper.ZeroTolerance) && (MathF.Abs(Y - other.Y) <= MathHelper.ZeroTolerance) &&
          (MathF.Abs(Z - other.Z) <= MathHelper.ZeroTolerance) && (MathF.Abs(W - other.W) <= MathHelper.ZeroTolerance);
    }

    /// <summary>
    /// Tests equality between the quaternion and another quaternion.
    /// </summary>
    /// <param name="other">Quaternion to compare</param>
    /// <param name="tolerance">Tolerance</param>
    /// <returns>True if components are equal within tolerance, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Equals(in Quaternion other, float tolerance)
    {
      return (MathF.Abs(X - other.X) <= tolerance) && (MathF.Abs(Y - other.Y) <= tolerance) &&
          (MathF.Abs(Z - other.Z) <= tolerance) && (MathF.Abs(W - other.W) <= tolerance);
    }

    /// <summary>
    /// Determines whether the specified <see cref="System.Object" /> is equal to this instance.
    /// </summary>
    /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
    /// <returns>
    /// true if the specified <see cref="System.Object" /> is equal to this instance; otherwise, false.
    /// </returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is Quaternion)
        return Equals((Quaternion) obj);

      return false;
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
    public readonly override int GetHashCode()
    {
      unchecked
      {
        return X.GetHashCode() + Y.GetHashCode() + Z.GetHashCode() + W.GetHashCode();
      }
    }

    /// <summary>
    /// Returns the fully qualified type name of this instance.
    /// </summary>
    /// <returns>A <see cref="T:System.String" /> containing a fully qualified type name. </returns>
    public readonly override string ToString()
    {
      CultureInfo info = CultureInfo.CurrentCulture;
      return String.Format(info, "X: {0} Y: {1} Z: {2} W: {3}",
          new Object[] { X.ToString(info), Y.ToString(info), Z.ToString(info), W.ToString(info) });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="format">The format</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(string? format)
    {
      if (format is null)
        return ToString();

      CultureInfo info = CultureInfo.CurrentCulture;
      return String.Format(info, "X: {0} Y: {1} Z: {2} W: {3}",
          new Object[] { X.ToString(format, info), Y.ToString(format, info), Z.ToString(format, info), W.ToString(format, info) });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="formatProvider">The format provider.</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(IFormatProvider? formatProvider)
    {
      if (formatProvider is null)
        return ToString();

      return String.Format(formatProvider, "X: {0} Y: {1} Z: {2} W: {3}",
          new Object[] { X.ToString(formatProvider), Y.ToString(formatProvider), Z.ToString(formatProvider), W.ToString(formatProvider) });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="format">The format.</param>
    /// <param name="formatProvider">The format provider.</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(string? format, IFormatProvider? formatProvider)
    {
      if (formatProvider is null)
        return ToString();

      if (format is null)
        return ToString(formatProvider);

      return String.Format(formatProvider, "X: {0} Y: {1} Z: {2} W: {3}",
          new Object[] { X.ToString(format, formatProvider), Y.ToString(format, formatProvider), Z.ToString(format, formatProvider), W.ToString(format, formatProvider) });
    }

    /// <summary>
    /// Writes the primitive data to the output.
    /// </summary>
    /// <param name="output">Primitive writer.</param>
    readonly void IPrimitiveValue.Write(IPrimitiveWriter output)
    {
      output.Write("X", X);
      output.Write("Y", Y);
      output.Write("Z", Z);
      output.Write("W", W);
    }

    /// <summary>
    /// Reads the primitive data from the input.
    /// </summary>
    /// <param name="input">Primitive reader.</param>
    void IPrimitiveValue.Read(IPrimitiveReader input)
    {
      X = input.ReadSingle("X");
      Y = input.ReadSingle("Y");
      Z = input.ReadSingle("Z");
      W = input.ReadSingle("W");
    }
  }
}
