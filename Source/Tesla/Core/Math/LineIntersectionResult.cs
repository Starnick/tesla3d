﻿/*
* Copyright (c) 2010-2024 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Tesla.Content;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Represents an intersection result from a line-object, ray-object, or segment-object test. The object may be a simple bounding volume
  /// or a complex triangle mesh.
  /// </summary>
  [Serializable]
  [StructLayout(LayoutKind.Sequential, Pack = 4)]
  public struct LineIntersectionResult : IEquatable<LineIntersectionResult>, IRefEquatable<LineIntersectionResult>, IComparable<LineIntersectionResult>, IFormattable, IPrimitiveValue
  {
    private Vector3 m_point;
    private float m_distance;
    private Vector3? m_normal;

    /// <summary>
    /// Gets the point of intersection.
    /// </summary>
    public readonly Vector3 Point
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return m_point;
      }
    }

    /// <summary>
    /// Gets the distance from the point of intersection to line origin (if segment, usually the Start point). This is used to sort
    /// intersections records.
    /// </summary>
    public readonly float Distance
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return m_distance;
      }
    }

    /// <summary>
    /// Gets the optional normal vector at the point of intersection.
    /// </summary>
    public readonly Vector3? Normal
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return m_normal;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="LineIntersectionResult"/> struct.
    /// </summary>
    /// <param name="point">Point of intersection</param>
    /// <param name="distance">Distance from the ray/line's origin.</param>
    public LineIntersectionResult(in Vector3 point, float distance)
    {
      m_point = point;
      m_distance = distance;

      m_normal = null;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="LineIntersectionResult"/> struct.
    /// </summary>
    /// <param name="point">Point of intersection</param>
    /// <param name="distance">Distance from the ray/line's origin.</param>
    /// <param name="normal">Normal vector at the point of intersection.</param>
    public LineIntersectionResult(in Vector3 point, float distance, in Vector3 normal)
    {
      m_point = point;
      m_distance = distance;

      m_normal = normal;
    }

    /// <summary>
    /// Tests if the first intersection result's distance is greater than the second.
    /// </summary>
    /// <param name="a">First intersection result</param>
    /// <param name="b">Second intersection result</param>
    /// <returns>True if the first distance is greater. false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator >(in LineIntersectionResult a, in LineIntersectionResult b)
    {
      return a.m_distance > b.m_distance;
    }

    /// <summary>
    /// Tests if the first intersection result's distance is greater than or equal to the second.
    /// </summary>
    /// <param name="a">First intersection result</param>
    /// <param name="b">Second intersection result</param>
    /// <returns>True if the first distance is greater than or equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator >=(in LineIntersectionResult a, in LineIntersectionResult b)
    {
      return a.m_distance >= b.m_distance;
    }

    /// <summary>
    /// Tests if the first intersection result's distance is less than the second.
    /// </summary>
    /// <param name="a">First intersection result</param>
    /// <param name="b">Second intersection result</param>
    /// <returns>True if the first distance is less than, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator <(in LineIntersectionResult a, in LineIntersectionResult b)
    {
      return a.m_distance < b.m_distance;
    }

    /// <summary>
    /// Tests if the first intersection result's distance is less than or equal to the second.
    /// </summary>
    /// <param name="a">First intersection result</param>
    /// <param name="b">Second intersection result</param>
    /// <returns>True if the first distance is less than or equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator <=(in LineIntersectionResult a, in LineIntersectionResult b)
    {
      return a.m_distance <= b.m_distance;
    }

    /// <summary>
    /// Tests equality between two intersection results.
    /// </summary>
    /// <param name="a">First intersection</param>
    /// <param name="b">Second intersection</param>
    /// <returns>True if the two intersections are equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator ==(in LineIntersectionResult a, in LineIntersectionResult b)
    {
      return a.Equals(b, MathHelper.ZeroTolerance);
    }

    /// <summary>
    /// Tests inequality between two intersection results.
    /// </summary>
    /// <param name="a">First intersection</param>
    /// <param name="b">Second intersection</param>
    /// <returns>True if the two intersections are not equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator !=(in LineIntersectionResult a, in LineIntersectionResult b)
    {
      return !a.Equals(b, MathHelper.ZeroTolerance);
    }

    /// <summary>
    /// Compares the current object with another object of the same type.
    /// </summary>
    /// <param name="other">An object to compare with this object.</param>
    /// <returns>
    /// A 32-bit signed integer that indicates the relative order of the objects being compared. The return value has the following meanings: 
    /// Value Meaning Less than zero This object is less than the <paramref name="other" /> parameter.Zero This object is equal to <paramref name="other" />. 
    /// Greater than zero This object is greater than <paramref name="other" />.
    /// </returns>
    public readonly int CompareTo(LineIntersectionResult other)
    {
      if (MathHelper.IsEqual(m_distance, other.m_distance))
        return 0;

      if (m_distance < other.m_distance)
        return -1;
      else
        return 1;
    }

    /// <summary>
    /// Writes the primitive data to the output.
    /// </summary>
    /// <param name="output">Primitive writer.</param>
    public readonly void Write(IPrimitiveWriter output)
    {
      output.Write<Vector3>("Point", m_point);
      output.Write("Distance", m_distance);
      output.Write<Vector3>("Normal", m_normal);
    }

    /// <summary>
    /// Reads the primitive data from the input.
    /// </summary>
    /// <param name="input">Primitive reader.</param>
    public void Read(IPrimitiveReader input)
    {
      input.Read<Vector3>("Point", out m_point);
      m_distance = input.ReadSingle("Distance");
      input.ReadNullable<Vector3>("Normal", out m_normal);
    }

    /// <summary>
    /// Indicates whether this instance and a specified object are equal.
    /// </summary>
    /// <param name="obj">Another object to compare to.</param>
    /// <returns>True if <paramref name="obj" /> and this instance are the same type and represent the same value; otherwise, false.</returns>
    public readonly override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is LineIntersectionResult)
      {
        LineIntersectionResult result = (LineIntersectionResult) obj;
        return Equals(result, MathHelper.ZeroTolerance);
      }

      return false;
    }

    /// <summary>
    /// Tests equality between the intersection result and another.
    /// </summary>
    /// <param name="other">Other result to test</param>
    /// <returns>True if the results are equal, false otherwise.</returns>
    readonly bool IEquatable<LineIntersectionResult>.Equals(LineIntersectionResult other)
    {
      return Equals(other, MathHelper.ZeroTolerance);
    }

    /// <summary>
    /// Tests equality between the intersection result and another.
    /// </summary>
    /// <param name="other">Other result to test</param>
    /// <returns>True if the results are equal, false otherwise.</returns>
    public readonly bool Equals(in LineIntersectionResult other)
    {
      return Equals(other, MathHelper.ZeroTolerance);
    }

    /// <summary>
    /// Tests equality between the intersection result and another.
    /// </summary>
    /// <param name="other">Other result to test</param>
    /// <param name="tolerance">Tolerance</param>
    /// <returns>True if the results are equal, false otherwise.</returns>
    public readonly bool Equals(in LineIntersectionResult other, float tolerance)
    {
      if (!m_point.Equals(other.m_point, tolerance) || !MathHelper.IsEqual(m_distance, other.m_distance, tolerance))
        return false;

      bool normalsEqual = false;

      if (m_normal.HasValue && other.m_normal.HasValue)
      {
        Vector3 n = other.m_normal.Value;
        normalsEqual = m_normal.Value.Equals(n, tolerance);
      }
      else
      {
        normalsEqual = !m_normal.HasValue && !other.m_normal.HasValue;
      }

      return normalsEqual;
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
    public readonly override int GetHashCode()
    {
      unchecked
      {
        int normalHash = (m_normal.HasValue) ? m_normal.Value.GetHashCode() : 0;

        return m_point.GetHashCode() + m_distance.GetHashCode() + normalHash;
      }
    }

    /// <summary>
    /// Returns the fully qualified type name of this instance.
    /// </summary>
    /// <returns>A <see cref="T:System.String" /> containing a fully qualified type name.</returns>
    public readonly override string ToString()
    {
      CultureInfo info = CultureInfo.CurrentCulture;
      return String.Format(info, "Point: {0} Distance: {1} Normal: {2}",
          new Object[] { m_point.ToString(info), m_distance.ToString(info), (m_normal.HasValue) ? m_normal.Value.ToString(info) : "null" });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="format">The format</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(string? format)
    {
      if (format is null)
        return ToString();

      CultureInfo info = CultureInfo.CurrentCulture;
      return String.Format(info, "Point: {0} Distance: {1} Normal: {2}",
          new Object[] { m_point.ToString(format, info), m_distance.ToString(format, info), (m_normal.HasValue) ? m_normal.Value.ToString(format, info) : "null" });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="formatProvider">The format provider.</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(IFormatProvider? formatProvider)
    {
      if (formatProvider is null)
        return ToString();

      return String.Format(formatProvider, "Point: {0} Distance: {1} Normal: {2}",
          new Object[] { m_point.ToString(formatProvider), m_distance.ToString(formatProvider), (m_normal.HasValue) ? m_normal.Value.ToString(formatProvider) : "null" });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="format">The format.</param>
    /// <param name="formatProvider">The format provider.</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(string? format, IFormatProvider? formatProvider)
    {
      if (formatProvider is null)
        return ToString();

      if (format is null)
        return ToString(formatProvider);

      return String.Format(formatProvider, "Point: {0} Distance: {1} Normal: {2}",
          new Object[] { m_point.ToString(format, formatProvider), m_distance.ToString(format, formatProvider), (m_normal.HasValue) ? m_normal.Value.ToString(format, formatProvider) : "null" });
    }
  }
}
