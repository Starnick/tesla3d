﻿/*
* Copyright (c) 2010-2024 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Tesla.Content;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Defines a finite line segment that has a start point and an end point.
  /// </summary>
  [Serializable]
  [StructLayout(LayoutKind.Sequential)]
  public struct Segment : IEquatable<Segment>, IRefEquatable<Segment>, IFormattable, IPrimitiveValue
  {
    /// <summary>
    /// Start point of the line segment.
    /// </summary>
    public Vector3 StartPoint;

    /// <summary>
    /// End point of the line segment.
    /// </summary>
    public Vector3 EndPoint;

    private static readonly int s_sizeInBytes = BufferHelper.SizeOf<Segment>();
    private static readonly Segment s_zero = new Segment(Vector3.Zero, Vector3.Zero);

    /// <summary>
    /// Gets the size of the <see cref="Segment"/> type in bytes.
    /// </summary>
    public static int SizeInBytes
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return s_sizeInBytes;
      }
    }

    /// <summary>
    /// Gets the degenerate segment where both endpoints are at the origin.
    /// </summary>
    public static ref readonly Segment Zero
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_zero;
      }
    }

    /// <summary>
    /// Gets the length of the line segment.
    /// </summary>
    public readonly float Length
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        float length = Vector3.Distance(StartPoint, EndPoint);

        return length;
      }
    }

    /// <summary>
    /// Gets the half-length (length from the center to each end point) of the line segment.
    /// </summary>
    public readonly float Extent
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        float length = Vector3.Distance(StartPoint, EndPoint);

        return length * 0.5f;
      }
    }

    /// <summary>
    /// Gets or sets the center point of the line segment.
    /// </summary>
    public Vector3 Center
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      readonly get
      {
        Vector3 dir;
        Vector3.Subtract(EndPoint, StartPoint, out dir);

        float halfLength = dir.Length() * 0.5f;
        dir.Normalize();

        Vector3 center;
        Vector3.Multiply(dir, halfLength, out dir);
        Vector3.Add(dir, StartPoint, out center);

        return center;
      }
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      set
      {
        Vector3 dir;
        Vector3.Subtract(EndPoint, StartPoint, out dir);

        float halfLength = dir.Length() * 0.5f;
        dir.Normalize();

        Vector3 temp;
        Vector3.Multiply(dir, -halfLength, out temp);
        Vector3.Add(temp, value, out StartPoint);

        Vector3.Multiply(dir, halfLength, out temp);
        Vector3.Add(temp, value, out EndPoint);
      }
    }

    /// <summary>
    /// Gets the direction of the line segment.
    /// </summary>
    public readonly Vector3 Direction
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        Vector3.NormalizedSubtract(EndPoint, StartPoint, out Vector3 dir);

        return dir;
      }
    }

    /// <summary>
    /// Gets if the line segment is degenerate (a point).
    /// </summary>
    public readonly bool IsDegenerate
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return MathHelper.IsEqual(Length, 0.0f, MathHelper.ZeroTolerance);
      }
    }

    /// <summary>
    /// Gets whether any of the components of the segment are NaN (Not A Number).
    /// </summary>
    public readonly bool IsNaN
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return StartPoint.IsNaN || EndPoint.IsNaN;
      }
    }

    /// <summary>
    /// Gets whether any of the components of the segment are positive or negative infinity.
    /// </summary>
    public readonly bool IsInfinity
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return StartPoint.IsInfinity || EndPoint.IsInfinity;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Segment"/> struct.
    /// </summary>
    /// <param name="start">Start point of the line.</param>
    /// <param name="end">End point of the line.</param>
    public Segment(in Vector3 start, in Vector3 end)
    {
      StartPoint = start;
      EndPoint = end;
    }

    #region From methods

    /// <summary>
    /// Creates a segment from a center-extent representation. The extent is the half-length positive and negative distance along the direction
    /// vector from the center to each end point. 
    /// </summary>
    /// <param name="center">Center of the segment.</param>
    /// <param name="direction">Direction of the segment.</param>
    /// <param name="extent">Half-length extent of the segment.</param>
    /// <returns>Segment</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Segment FromCenterExtent(in Vector3 center, in Vector3 direction, float extent)
    {
      Segment result;
      Vector3 temp;
      Vector3.Multiply(direction, -extent, out temp);
      Vector3.Add(temp, center, out result.StartPoint);

      Vector3.Multiply(direction, extent, out temp);
      Vector3.Add(temp, center, out result.EndPoint);

      return result;
    }

    /// <summary>
    /// Creates a segment from a center-extent representation. The extent is the half-length positive and negative distance along the direction
    /// vector from the center to each end point. 
    /// </summary>
    /// <param name="center">Center of the segment.</param>
    /// <param name="direction">Direction of the segment.</param>
    /// <param name="extent">Half-length extent of the segment.</param>
    /// <param name="result">Segment</param>
    public static void FromCenterExtent(in Vector3 center, in Vector3 direction, float extent, out Segment result)
    {
      Vector3 temp;
      Vector3.Multiply(direction, -extent, out temp);
      Vector3.Add(temp, center, out result.StartPoint);

      Vector3.Multiply(direction, extent, out temp);
      Vector3.Add(temp, center, out result.EndPoint);
    }

    #endregion

    #region Transform

    /// <summary>
    /// Transforms the segment's end points by the given <see cref="Matrix"/>.
    /// </summary>
    /// <param name="segment">Segment to transform.</param>
    /// <param name="m">Transformation matrix.</param>
    /// <returns>Transformed segment.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Segment Transform(in Segment segment, in Matrix m)
    {
      Segment result;
      Vector3.Transform(segment.StartPoint, m, out result.StartPoint);
      Vector3.Transform(segment.EndPoint, m, out result.EndPoint);

      return result;
    }

    /// <summary>
    /// Transforms the segment's end points by the given <see cref="Matrix"/>.
    /// </summary>
    /// <param name="segment">Segment to transform.</param>
    /// <param name="m">Transformation matrix.</param>
    /// <param name="result">Transformed segment.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Transform(in Segment segment, in Matrix m, out Segment result)
    {
      Vector3.Transform(segment.StartPoint, m, out result.StartPoint);
      Vector3.Transform(segment.EndPoint, m, out result.EndPoint);
    }

    /// <summary>
    /// Transforms the segment's end points by the given <see cref="Quaternion"/>.
    /// </summary>
    /// <param name="segment">Segment to transform.</param>
    /// <param name="rotation">Quaternion rotation.</param>
    /// <returns>Transformed segment.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Segment Transform(in Segment segment, in Quaternion rotation)
    {
      Segment result;
      Vector3.Transform(segment.StartPoint, rotation, out result.StartPoint);
      Vector3.Transform(segment.EndPoint, rotation, out result.EndPoint);

      return result;
    }

    /// <summary>
    /// Transforms the segment's end points by the given <see cref="Quaternion"/>.
    /// </summary>
    /// <param name="segment">Segment to transform.</param>
    /// <param name="rotation">Quaternion rotation.</param>
    /// <param name="result">Transformed segment.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Transform(in Segment segment, in Quaternion rotation, out Segment result)
    {
      Vector3.Transform(segment.StartPoint, rotation, out result.StartPoint);
      Vector3.Transform(segment.EndPoint, rotation, out result.EndPoint);
    }

    /// <summary>
    /// Scales the segment by the given scaling factor. This will scale the extents (half-lengths from the center
    /// to the end points) and then recompute the end points using the scaled extent and the direction of the
    /// original segment.
    /// </summary>
    /// <param name="segment">Segment to transform.</param>
    /// <param name="scale">Scaling factor.</param>
    /// <returns>Transformed segment.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Segment Transform(in Segment segment, float scale)
    {
      GeometricToolsHelper.CalculateSegmentProperties(segment, out Vector3 center, out Vector3 dir, out float extent);

      extent *= scale;

      Segment result;
      Vector3.Multiply(dir, -extent, out result.StartPoint);
      Vector3.Add(result.StartPoint, center, out result.StartPoint);

      Vector3.Multiply(dir, extent, out result.EndPoint);
      Vector3.Add(result.EndPoint, center, out result.EndPoint);

      return result;
    }

    /// <summary>
    /// Scales the segment by the given scaling factor. This will scale the extents (half-lengths from the center
    /// to the end points) and then recompute the end points using the scaled extent and the direction of the
    /// original segment.
    /// </summary>
    /// <param name="segment">Segment to transform.</param>
    /// <param name="scale">Scaling factor.</param>
    /// <param name="result">Transformed segment.</param>
    public static void Transform(in Segment segment, float scale, out Segment result)
    {
      GeometricToolsHelper.CalculateSegmentProperties(segment, out Vector3 center, out Vector3 dir, out float extent);

      extent *= scale;

      Vector3.Multiply(dir, -extent, out result.StartPoint);
      Vector3.Add(result.StartPoint, center, out result.StartPoint);

      Vector3.Multiply(dir, extent, out result.EndPoint);
      Vector3.Add(result.EndPoint, center, out result.EndPoint);
    }

    /// <summary>
    /// Translates the segment by the given translation vector.
    /// </summary>
    /// <param name="segment">Segment to transform.</param>
    /// <param name="translation">Translation vector.</param>
    /// <returns>Transformed segment.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Segment Transform(in Segment segment, in Vector3 translation)
    {
      Segment result;
      Vector3.Add(segment.StartPoint, translation, out result.StartPoint);
      Vector3.Add(segment.EndPoint, translation, out result.EndPoint);

      return result;
    }

    /// <summary>
    /// Translates the segment by the given translation vector.
    /// </summary>
    /// <param name="segment">Segment to transform.</param>
    /// <param name="translation">Translation vector.</param>
    /// <param name="result">Transformed segment.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Transform(in Segment segment, in Vector3 translation, out Segment result)
    {
      Vector3.Add(segment.StartPoint, translation, out result.StartPoint);
      Vector3.Add(segment.EndPoint, translation, out result.EndPoint);
    }

    #endregion

    #region Equality Operators

    /// <summary>
    /// Tests equality between two lines.
    /// </summary>
    /// <param name="a">First line</param>
    /// <param name="b">Second line</param>
    /// <returns>True if both lines are equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator ==(in Segment a, in Segment b)
    {
      return a.Equals(b);
    }

    /// <summary>
    /// Tests inequality between two lines.
    /// </summary>
    /// <param name="a">First line</param>
    /// <param name="b">Second line</param>
    /// <returns>True if both lines are not equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator !=(in Segment a, in Segment b)
    {
      return !a.Equals(b);
    }

    #endregion

    #region Public methods

    /// <summary>
    /// Tests if the line is perpendicular to the specified line.
    /// </summary>
    /// <param name="line">Line to test against.</param>
    /// <returns>True if the lines are perpendicular, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool IsPerpendicularTo(in Segment line)
    {
      Vector3.NormalizedSubtract(EndPoint, StartPoint, out Vector3 v0);
      Vector3.NormalizedSubtract(line.EndPoint, line.StartPoint, out Vector3 v1);

      float dot = Vector3.Dot(v0, v1);

      return MathHelper.IsEqual(dot, 0.0f);
    }

    /// <summary>
    /// Tests if the line is parallel to the specified line.
    /// </summary>
    /// <param name="line">Line to test against.</param>
    /// <returns>True if the lines are parallel, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool IsParallelTo(in Segment line)
    {
      Vector3.Subtract(EndPoint, StartPoint, out Vector3 v0);
      Vector3.Subtract(line.EndPoint, line.StartPoint, out Vector3 v1);
      Vector3.NormalizedCross(v0, v1, out Vector3 cross);

      return cross.IsAlmostZero();
    }

    #endregion

    #region Intersects

    /// <summary>
    /// Determines whether there is an apparent 2D (XY only) intersection between this object and a <see cref="Ray"/>.
    /// </summary>
    /// <param name="ray">Ray to test.</param>
    /// <returns>True if an intersection between the two objects occurs, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool IntersectsXY(in Ray ray)
    {
      return GeometricToolsHelper.IntersectRaySegmentXY(ray, this);
    }

    /// <summary>
    /// Determines whether there is an apparent 2D (XY only) intersection between this object and a <see cref="Ray"/>.
    /// </summary>
    /// <param name="ray">Ray to test.</param>
    /// <param name="result">Intersection result between the two objects.</param>
    /// <returns>True if an intersection between the two objects occurs, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool IntersectsXY(in Ray ray, out Vector3 result)
    {
      return GeometricToolsHelper.IntersectRaySegmentXY(ray, this, out result, out float rayParam, out float segParam);
    }

    /// <summary>
    /// Determines whether there is an apparent 2D (XY only) intersection between this object and a <see cref="Segment"/>.
    /// </summary>
    /// <param name="segment">Segment to test.</param>
    /// <returns>True if an intersection between the two objects occurs, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool IntersectsXY(in Segment segment)
    {
      return GeometricToolsHelper.IntersectSegmentSegmentXY(this, segment);
    }

    /// <summary>
    /// Determines whether there is an apparent 2D (XY only) intersection between this object and a <see cref="Segment"/>.
    /// </summary>
    /// <param name="segment">Segment to test.</param>
    /// <param name="result">Intersection result between the two objects.</param>
    /// <returns>True if an intersection between the two objects occurs, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool IntersectsXY(in Segment segment, out Vector3 result)
    {
      return GeometricToolsHelper.IntersectSegmentSegmentXY(this, segment, out result, out float seg0Param, out float seg1Param);
    }

    /// <summary>
    /// Determines whether there is an intersection between this object and a <see cref="Plane"/>.
    /// </summary>
    /// <param name="plane">Plane to test.</param>
    /// <returns>True if an intersection between the two objects occurs, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Intersects(in Plane plane)
    {
      return GeometricToolsHelper.IntersectSegmentPlane(this, plane);
    }

    /// <summary>
    /// Determines whether there is an intersection between this object and a <see cref="Plane"/>.
    /// </summary>
    /// <param name="plane">Plane to test.</param>
    /// <param name="result">Intersection result between the two objects.</param>
    /// <returns>True if an intersection between the two objects occurs, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Intersects(in Plane plane, out LineIntersectionResult result)
    {
      return GeometricToolsHelper.IntersectSegmentPlane(this, plane, out result);
    }

    /// <summary>
    /// Determines whether there is an intersection between this object and a <see cref="Triangle"/>.
    /// </summary>
    /// <param name="triangle">Triangle to test.</param>
    /// <param name="ignoreBackface">True if the test should ignore an intersection if the triangle is a back face, false if otherwise.</param>
    /// <returns>True if an intersection between the two objects occurs, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Intersects(in Triangle triangle, bool ignoreBackface = false)
    {
      return GeometricToolsHelper.IntersectSegmentTriangle(triangle, this, ignoreBackface, out LineIntersectionResult result);
    }

    /// <summary>
    /// Determines whether there is an intersection between this object and a <see cref="Triangle"/>.
    /// </summary>
    /// <param name="triangle">Triangle to test.</param>
    /// <param name="result">Intersection result between the two objects.</param>
    /// <param name="ignoreBackface">True if the test should ignore an intersection if the triangle is a back face, false if otherwise.</param>
    /// <returns>True if an intersection between the two objects occurs, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Intersects(in Triangle triangle, out LineIntersectionResult result, bool ignoreBackface = false)
    {
      return GeometricToolsHelper.IntersectSegmentTriangle(triangle, this, ignoreBackface, out result);
    }

    /// <summary>
    /// Determines whether there is an intersection between this object and an <see cref="Ellipse"/>.
    /// </summary>
    /// <param name="ellipse">Ellipse to test.</param>
    /// <returns>True if an intersection between the two objects occurs, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Intersects(in Ellipse ellipse)
    {
      //TODO
      throw new NotImplementedException();
    }

    /// <summary>
    /// Determines whether there is an intersection between this object and an <see cref="Ellipse"/>.
    /// </summary>
    /// <param name="ellipse">Ellipse to test.</param>
    /// <param name="result">Intersection result between the two objects.</param>
    /// <returns>True if an intersection between the two objects occurs, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Intersects(in Ellipse ellipse, out LineIntersectionResult result)
    {
      //TODO
      throw new NotImplementedException();
    }

    /// <summary>
    /// Determines whether there is an intersection between this object and a <see cref="BoundingVolume"/>.
    /// </summary>
    /// <param name="volume">Bounding volume to test.</param>
    /// <returns>True if an intersection between the two objects occurs, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Intersects([NotNullWhen(true)] BoundingVolume? volume)
    {
      if (volume is null)
        return false;

      return volume.Intersects(this);
    }

    /// <summary>
    /// Determines whether there is an intersection between this object and a <see cref="BoundingVolume"/>.
    /// </summary>
    /// <param name="volume">Bounding volume to test.</param>
    /// <param name="result">Bounding intersection result.</param>
    /// <returns>True if an intersection between the two objects occurs, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Intersects([NotNullWhen(true)] BoundingVolume? volume, out BoundingIntersectionResult result)
    {
      if (volume is null)
      {
        result = new BoundingIntersectionResult();
        return false;
      }

      return volume.Intersects(this, out result);
    }

    #endregion

    #region Distance/Point To/From

    /// <summary>
    /// Gets the point along the segment that is the distance from the start point.
    /// </summary>
    /// <param name="distance">Distance of the point from the start point.</param>
    /// <returns>The point along the segment.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly Vector3 PointAtDistance(float distance)
    {
      PointAtDistance(distance, out Vector3 result);

      return result;
    }

    /// <summary>
    /// Gets the point along the segment that is the distance from the start point.
    /// </summary>
    /// <param name="distance">Distance of the point from the start point.</param>
    /// <param name="result">The point along the segment.</param>
    public readonly void PointAtDistance(float distance, out Vector3 result)
    {
      Vector3.NormalizedSubtract(EndPoint, StartPoint, out Vector3 dir);

      Vector3.Multiply(dir, distance, out result);
      Vector3.Add(StartPoint, result, out result);
    }

    /// <summary>
    /// Gets the distance of the point (or closest on the segment) from the start point.
    /// </summary>
    /// <param name="point">Point along or near the segment.</param>
    /// <returns>The distance from the point (or point closest on the segment) to the start point.</returns>
    public readonly float DistanceAtPoint(in Vector3 point)
    {
      Vector3 v;
      Vector3.Subtract(point, StartPoint, out v);

      Vector3.NormalizedSubtract(EndPoint, StartPoint, out Vector3 dir);

      float dot = Vector3.Dot(dir, v);
      float dotDir = Vector3.Dot(dir, dir);

      if (MathHelper.IsEqual(dotDir, MathHelper.ZeroTolerance))
        return 0.0f;

      return dot / dotDir;
    }

    #endregion

    #region Distance To

    /// <summary>
    /// Determines the distance between this object and a point.
    /// </summary>
    /// <param name="point">Point to test.</param>
    /// <returns>Distance between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float DistanceTo(in Vector3 point)
    {
      GeometricToolsHelper.DistancePointSegment(point, this, out Vector3 ptOnSegment, out float segParam, out float sqrDist);

      return MathF.Sqrt(sqrDist);
    }

    /// <summary>
    /// Determines the distance between this object and a <see cref="Ray"/>.
    /// </summary>
    /// <param name="ray">Ray to test.</param>
    /// <returns>Distance between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float DistanceTo(in Ray ray)
    {
      GeometricToolsHelper.DistanceRaySegment(ray, this, out float rayParam, out float segParam, out float sqrDist);

      return MathF.Sqrt(sqrDist);
    }

    /// <summary>
    /// Determines the distance between this object and a <see cref="Segment"/>.
    /// </summary>
    /// <param name="segment">Segment to test.</param>
    /// <returns>Distance between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float DistanceTo(in Segment segment)
    {
      GeometricToolsHelper.DistanceSegmentSegment(this, segment, out float seg0Param, out float seg1Param, out float sqrDist);

      return MathF.Sqrt(sqrDist);
    }

    /// <summary>
    /// Determines the distance between this object and a <see cref="Plane"/>.
    /// </summary>
    /// <param name="plane">Plane to test.</param>
    /// <returns>Distance between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float DistanceTo(in Plane plane)
    {
      GeometricToolsHelper.DistanceSegmentPlane(plane, this, out Vector3 ptOnPlane, out Vector3 ptOnSegment, out float sqrDist);

      return MathF.Sqrt(sqrDist);
    }

    /// <summary>
    /// Determines the distance between this object and a <see cref="Triangle"/>.
    /// </summary>
    /// <param name="triangle">Triangle to test.</param>
    /// <returns>Distance between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float DistanceTo(in Triangle triangle)
    {
      GeometricToolsHelper.DistanceSegmentTriangle(triangle, this, out Vector3 ptOnTriangle, out Vector3 ptOnSegment, out float sqrDist);

      return MathF.Sqrt(sqrDist);
    }

    /// <summary>
    /// Determines the distance between this object and an <see cref="Ellipse"/>.
    /// </summary>
    /// <param name="ellipse">Ellipse to test.</param>
    /// <returns>Distance between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float DistanceTo(in Ellipse ellipse)
    {
      //TODO
      throw new NotImplementedException();
    }

    #endregion

    #region Distance Squared To

    /// <summary>
    /// Determines the distance squared between this object and a point.
    /// </summary>
    /// <param name="point">Point to test.</param>
    /// <returns>Distance squared between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float DistanceSquaredTo(in Vector3 point)
    {
      GeometricToolsHelper.DistancePointSegment(point, this, out Vector3 ptOnSegment, out float segParam, out float sqrDist);

      return sqrDist;
    }

    /// <summary>
    /// Determines the distance squared between this object and a <see cref="Ray"/>.
    /// </summary>
    /// <param name="ray">Ray to test.</param>
    /// <returns>Distance squared between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float DistanceSquaredTo(in Ray ray)
    {
      GeometricToolsHelper.DistanceRaySegment(ray, this, out float rayParam, out float segParam, out float sqrDist);

      return sqrDist;
    }

    /// <summary>
    /// Determines the distance squared between this object and a <see cref="Segment"/>.
    /// </summary>
    /// <param name="segment">Segment to test.</param>
    /// <returns>Distance squared between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float DistanceSquaredTo(in Segment segment)
    {
      GeometricToolsHelper.DistanceSegmentSegment(this, segment, out float seg0Param, out float seg1Param, out float sqrDist);

      return sqrDist;
    }

    /// <summary>
    /// Determines the distance squared between this object and a <see cref="Plane"/>.
    /// </summary>
    /// <param name="plane">Plane to test.</param>
    /// <returns>Distance squared between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float DistanceSquaredTo(in Plane plane)
    {
      GeometricToolsHelper.DistanceSegmentPlane(plane, this, out Vector3 ptOnPlane, out Vector3 ptOnSegment, out float sqrDist);

      return sqrDist;
    }

    /// <summary>
    /// Determines the distance squared between this object and a <see cref="Triangle"/>.
    /// </summary>
    /// <param name="triangle">Triangle to test.</param>
    /// <returns>Distance squared between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float DistanceSquaredTo(in Triangle triangle)
    {
      GeometricToolsHelper.DistanceSegmentTriangle(triangle, this, out Vector3 ptOnTriangle, out Vector3 ptOnSegment, out float sqrDist);

      return sqrDist;
    }

    /// <summary>
    /// Determines the distance squared between this object and an <see cref="Ellipse"/>.
    /// </summary>
    /// <param name="ellipse">Ellipse to test.</param>
    /// <returns>Distance squared between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float DistanceSquaredTo(in Ellipse ellipse)
    {
      // TODO
      throw new NotImplementedException();
    }

    #endregion

    #region Closest Point / Approach Segment

    /// <summary>
    /// Determines the closest point on this object from the point.
    /// </summary>
    /// <param name="point">Point to test.</param>
    /// <returns>Closest point on this object.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly Vector3 ClosestPointTo(in Vector3 point)
    {
      GeometricToolsHelper.DistancePointSegment(point, this, out Vector3 ptOnSegment, out float segParam, out float sqrDist);

      return ptOnSegment;
    }

    /// <summary>
    /// Determines the closest point on this object from the point.
    /// </summary>
    /// <param name="point">Point to test.</param>
    /// <param name="result">Closest point on this object.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly void ClosestPointTo(in Vector3 point, out Vector3 result)
    {
      GeometricToolsHelper.DistancePointSegment(point, this, out result, out float segParam, out float sqrDist);
    }

    /// <summary>
    /// Determines the closest point on this object from the point.
    /// </summary>
    /// <param name="point">Point to test.</param>
    /// <param name="result">Closest point on this object.</param>
    /// <param name="squaredDistance">Squared distance between the two objects.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly void ClosestPointTo(in Vector3 point, out Vector3 result, out float squaredDistance)
    {
      GeometricToolsHelper.DistancePointSegment(point, this, out result, out float segParam, out squaredDistance);
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Ray"/>.
    /// </summary>
    /// <param name="ray">Ray to test</param>
    /// <returns>Closest approach segment between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly Segment ClosestApproachSegment(in Ray ray)
    {
      ClosestApproachSegment(ray, out Segment result, out float squaredDistance);

      return result;
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Ray"/>.
    /// </summary>
    /// <param name="ray">Ray to test</param>
    /// <param name="result">Closest approach segment between the two objects.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly void ClosestApproachSegment(in Ray ray, out Segment result)
    {
      ClosestApproachSegment(ray, out result, out float squaredDistance);
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Ray"/>.
    /// </summary>
    /// <param name="ray">Ray to test</param>
    /// <param name="result">Closest approach segment between the two objects.</param>
    /// <param name="squaredDistance">Squared distance between the two objects.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly void ClosestApproachSegment(in Ray ray, out Segment result, out float squaredDistance)
    {
      float rayParam, segParam;
      GeometricToolsHelper.DistanceRaySegment(ray, this, out rayParam, out segParam, out squaredDistance);

      //Calculate point along segment;
      Vector3.NormalizedSubtract(EndPoint, StartPoint, out Vector3 dir);

      Vector3.Multiply(dir, segParam, out result.StartPoint);
      Vector3.Add(result.StartPoint, StartPoint, out result.StartPoint);

      //Calculate point along ray
      Vector3.Multiply(ray.Direction, rayParam, out result.EndPoint);
      Vector3.Add(result.EndPoint, ray.Origin, out result.EndPoint);
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Segment"/>.
    /// </summary>
    /// <param name="segment">Segment to test</param>
    /// <returns>Closest approach segment between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly Segment ClosestApproachSegment(in Segment segment)
    {
      ClosestApproachSegment(segment, out Segment result, out float squaredDistance);

      return result;
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Segment"/>.
    /// </summary>
    /// <param name="segment">Segment to test</param>
    /// <param name="result">Closest approach segment between the two objects.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly void ClosestApproachSegment(in Segment segment, out Segment result)
    {
      ClosestApproachSegment(segment, out result, out float squaredDistance);
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Segment"/>.
    /// </summary>
    /// <param name="segment">Segment to test</param>
    /// <param name="result">Closest approach segment between the two objects.</param>
    /// <param name="squaredDistance">Squared distance between the two objects.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly void ClosestApproachSegment(in Segment segment, out Segment result, out float squaredDistance)
    {
      float seg0Param, seg1Param;
      GeometricToolsHelper.DistanceSegmentSegment(this, segment, out seg0Param, out seg1Param, out squaredDistance);

      //Calculate point along first segment
      Vector3.NormalizedSubtract(EndPoint, StartPoint, out Vector3 dir);

      Vector3.Multiply(dir, seg0Param, out result.StartPoint);
      Vector3.Add(result.StartPoint, StartPoint, out result.StartPoint);

      //Calculate point along second segment
      Vector3.NormalizedSubtract(segment.EndPoint, segment.StartPoint, out dir);

      Vector3.Multiply(dir, seg1Param, out dir);
      Vector3.Add(segment.StartPoint, dir, out result.EndPoint);
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Plane"/>.
    /// </summary>
    /// <param name="plane">Plane to test</param>
    /// <returns>Closest approach segment between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly Segment ClosestApproachSegment(in Plane plane)
    {
      Segment result;
      GeometricToolsHelper.DistanceSegmentPlane(plane, this, out result.EndPoint, out result.StartPoint, out float sqrDist);

      return result;
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Plane"/>.
    /// </summary>
    /// <param name="plane">Plane to test</param>
    /// <param name="result">Closest approach segment between the two objects.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly void ClosestApproachSegment(in Plane plane, out Segment result)
    {
      GeometricToolsHelper.DistanceSegmentPlane(plane, this, out result.EndPoint, out result.StartPoint, out float squaredDistance);
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Plane"/>.
    /// </summary>
    /// <param name="plane">Plane to test</param>
    /// <param name="result">Closest approach segment between the two objects.</param>
    /// <param name="squaredDistance">Squared distance between the two objects.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly void ClosestApproachSegment(in Plane plane, out Segment result, out float squaredDistance)
    {
      GeometricToolsHelper.DistanceSegmentPlane(plane, this, out result.EndPoint, out result.StartPoint, out squaredDistance);
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Triangle"/>.
    /// </summary>
    /// <param name="triangle">Triangle to test</param>
    /// <returns>Closest approach segment between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly Segment ClosestApproachSegment(in Triangle triangle)
    {
      Segment result;
      GeometricToolsHelper.DistanceSegmentTriangle(triangle, this, out result.EndPoint, out result.StartPoint, out float sqrDist);

      return result;
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Triangle"/>.
    /// </summary>
    /// <param name="triangle">Triangle to test</param>
    /// <param name="result">Closest approach segment between the two objects.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly void ClosestApproachSegment(in Triangle triangle, out Segment result)
    {
      GeometricToolsHelper.DistanceSegmentTriangle(triangle, this, out result.EndPoint, out result.StartPoint, out float squaredDistance);
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Triangle"/>.
    /// </summary>
    /// <param name="triangle">Triangle to test</param>
    /// <param name="result">Closest approach segment between the two objects.</param>
    /// <param name="squaredDistance">Squared distance between the two objects.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly void ClosestApproachSegment(in Triangle triangle, out Segment result, float squaredDistance)
    {
      GeometricToolsHelper.DistanceSegmentTriangle(triangle, this, out result.EndPoint, out result.StartPoint, out squaredDistance);
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Ellipse"/>.
    /// </summary>
    /// <param name="ellipse">Ellipse to test</param>
    /// <returns>Closest approach segment between the two objects.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly Segment ClosestApproachSegment(in Ellipse ellipse)
    {
      ClosestApproachSegment(ellipse, out Segment result, out float squaredDistance);

      return result;
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Ellipse"/>.
    /// </summary>
    /// <param name="ellipse">Ellipse to test</param>
    /// <param name="result">Closest approach segment between the two objects.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly void ClosestApproachSegment(in Ellipse ellipse, out Segment result)
    {
      ClosestApproachSegment(ellipse, out result, out float squaredDistance);
    }

    /// <summary>
    /// Determines the closest approach segment between this object and a <see cref="Ellipse"/>.
    /// </summary>
    /// <param name="ellipse">Ellipse to test</param>
    /// <param name="result">Closest approach segment between the two objects.</param>
    /// <param name="squaredDistance">Squared distance between the two objects.</param>
    public readonly void ClosestApproachSegment(in Ellipse ellipse, out Segment result, out float squaredDistance)
    {
      //TODO
      throw new NotImplementedException();
    }

    #endregion

    #region Equality

    /// <summary>
    /// Indicates whether this instance and a specified object are equal.
    /// </summary>
    /// <param name="obj">Another object to compare to.</param>
    /// <returns>True if <paramref name="obj" /> and this instance are the same type and represent the same value; otherwise, false.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is Segment)
        return Equals((Segment) obj);

      return false;
    }

    /// <summary>
    /// Tests equality between the line and another line.
    /// </summary>
    /// <param name="other">Other line to test against</param>
    /// <returns>True if the lines are equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    readonly bool IEquatable<Segment>.Equals(Segment other)
    {
      return StartPoint.Equals(other.StartPoint, MathHelper.ZeroTolerance) && EndPoint.Equals(other.EndPoint, MathHelper.ZeroTolerance);
    }

    /// <summary>
    /// Tests equality between the line and another line.
    /// </summary>
    /// <param name="other">Other line to test against</param>
    /// <returns>True if the lines are equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Equals(in Segment other)
    {
      return StartPoint.Equals(other.StartPoint, MathHelper.ZeroTolerance) && EndPoint.Equals(other.EndPoint, MathHelper.ZeroTolerance);
    }

    /// <summary>
    /// Tests equality between the line and another line.
    /// </summary>
    /// <param name="other">Other line to test against</param>
    /// <param name="tolerance">Tolerance</param>
    /// <returns>True if the lines are equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Equals(in Segment other, float tolerance)
    {
      return StartPoint.Equals(other.StartPoint, tolerance) && EndPoint.Equals(other.EndPoint, tolerance);
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
    public readonly override int GetHashCode()
    {
      unchecked
      {
        return StartPoint.GetHashCode() + EndPoint.GetHashCode();
      }
    }

    #endregion

    #region ToString

    /// <summary>
    /// Returns the fully qualified type name of this instance.
    /// </summary>
    /// <returns>A <see cref="T:System.String" /> containing a fully qualified type name.</returns>
    public readonly override string ToString()
    {
      CultureInfo info = CultureInfo.CurrentCulture;
      return String.Format(info, "Start: ({0}), End: ({1}), Length: {2}",
          new Object[] { StartPoint.ToString(), EndPoint.ToString(), Length.ToString() });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="format">The format</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(string? format)
    {
      if (format is null)
        return ToString();

      CultureInfo info = CultureInfo.CurrentCulture;
      return String.Format(info, "Start: ({0}), End: ({1}), Length: {2}",
          new Object[] { StartPoint.ToString(format, info), EndPoint.ToString(format, info), Length.ToString(format, info) });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="formatProvider">The format provider.</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(IFormatProvider? formatProvider)
    {
      if (formatProvider is null)
        return ToString();

      return String.Format(formatProvider, "Start: ({0}), End: ({1}), Length: {2}",
          new Object[] { StartPoint.ToString(formatProvider), EndPoint.ToString(formatProvider), Length.ToString(formatProvider) });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="format">The format.</param>
    /// <param name="formatProvider">The format provider.</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(string? format, IFormatProvider? formatProvider)
    {
      if (formatProvider is null)
        return ToString();

      if (format is null)
        return ToString(formatProvider);

      return String.Format(formatProvider, "Start: ({0}), End: ({1}), Length: {2}",
          new Object[] { StartPoint.ToString(format, formatProvider), EndPoint.ToString(format, formatProvider), Length.ToString(format, formatProvider) });
    }

    #endregion

    #region IPrimitiveValue

    /// <summary>
    /// Writes the primitive data to the output.
    /// </summary>
    /// <param name="output">Primitive writer.</param>
    readonly void IPrimitiveValue.Write(IPrimitiveWriter output)
    {
      output.Write<Vector3>("Start", StartPoint);
      output.Write<Vector3>("End", EndPoint);
    }

    /// <summary>
    /// Reads the primitive data from the input.
    /// </summary>
    /// <param name="input">Primitive reader.</param>
    void IPrimitiveValue.Read(IPrimitiveReader input)
    {
      input.Read<Vector3>("Start", out StartPoint);
      input.Read<Vector3>("End", out EndPoint);
    }

    #endregion
  }
}
