﻿/*
* Copyright (c) 2010-2024 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Tesla.Content;
using Tesla.Design;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Defines a four dimensional vector where each component is a 32-bit integer.
  /// </summary>
  [Serializable]
  [StructLayout(LayoutKind.Sequential, Pack = 4)]
  [TypeConverter(typeof(Int2TypeConverter))]
  public struct Int2 : IEquatable<Int2>, IRefEquatable<Int2>, IFormattable, IPrimitiveValue
  {
    /// <summary>
    /// X component of the vector.
    /// </summary>
    public int X;

    /// <summary>
    /// Y component of the vector.
    /// </summary>
    public int Y;

    private static readonly Int2 s_zero = new Int2(0, 0);
    private static readonly Int2 s_one = new Int2(1, 1);
    private static readonly Int2 s_unitX = new Int2(1, 0);
    private static readonly Int2 s_unitY = new Int2(0, 1);

    private static readonly int s_sizeInBytes = BufferHelper.SizeOf<Int2>();

    /// <summary>
    /// Gets an <see cref="Int2"/> set to (0, 0).
    /// </summary>
    public static ref readonly Int2 Zero
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_zero;
      }
    }

    /// <summary>
    /// Gets an <see cref="Int2"/> set to (1, 1).
    /// </summary>
    public static ref readonly Int2 One
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_one;
      }
    }

    /// <summary>
    /// Gets a unit <see cref="Int2"/> set to (1, 0).
    /// </summary>
    public static ref readonly Int2 UnitX
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_unitX;
      }
    }

    /// <summary>
    /// Gets a unit <see cref="Int2"/> set to (0, 1).
    /// </summary>
    public static ref readonly Int2 UnitY
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_unitY;
      }
    }

    /// <summary>
    /// Gets the size of the <see cref="Int2"/> type in bytes.
    /// </summary>
    public static int SizeInBytes
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return s_sizeInBytes;
      }
    }

    /// <summary>
    /// Gets or sets individual components of the vector in the order that the components are declared (XY).
    /// </summary>
    /// <param name="index">Zero-based index, in range of [0, 1].</param>
    /// <returns>The value of the specified component.</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the index is out of range [0, 1]</exception>
    public int this[int index]
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      readonly get
      {
        switch (index)
        {
          case 0:
            return X;
          case 1:
            return Y;
          default:
            throw new ArgumentOutOfRangeException(nameof(index), StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));
        }
      }
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      set
      {
        switch (index)
        {
          case 0:
            X = value;
            break;
          case 1:
            Y = value;
            break;
          default:
            throw new ArgumentOutOfRangeException(nameof(index), StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));
        }
      }
    }

    /// <summary>
    /// Determines whether this vector is (0, 0).
    /// </summary>
    public readonly bool IsZero
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        ref readonly Int2 zero = ref Int2.Zero;
        return Equals(zero);
      }
    }

    /// <summary>
    /// Determines whether this vector is (1, 1).
    /// </summary>
    public readonly bool IsOne
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        ref readonly Int2 one = ref Int2.One;
        return Equals(one);
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Int2"/> struct.
    /// </summary>
    /// <param name="value">Value to initialize each component to.</param>
    public Int2(int value)
    {
      X = Y = value;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Int2"/> struct.
    /// </summary>
    /// <param name="x">X component.</param>
    /// <param name="y">Y component.</param>
    public Int2(int x, int y)
    {
      X = x;
      Y = y;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Int2"/> struct.
    /// </summary>
    /// <param name="xy">Convert from <see cref="Int3"/>, only taking the XY values.</param>
    public Int2(in Int3 xy)
    {
      X = xy.X;
      Y = xy.Y;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Int2"/> struct.
    /// </summary>
    /// <param name="xy">Convert from <see cref="Int4"/>, only taking the XY values.</param>
    public Int2(in Int4 xy)
    {
      X = xy.X;
      Y = xy.Y;
    }

    /// <summary>
    /// Adds two vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Sum of the two vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int2 Add(in Int2 a, in Int2 b)
    {
      Int2 result;
      result.X = a.X + b.X;
      result.Y = a.Y + b.Y;

      return result;
    }

    /// <summary>
    /// Adds two vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <param name="result">Sum of the two vectors</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Add(in Int2 a, in Int2 b, out Int2 result)
    {
      result.X = a.X + b.X;
      result.Y = a.Y + b.Y;
    }

    /// <summary>
    /// Subtracts vector b from vector a.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Difference of the two vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int2 Subtract(in Int2 a, in Int2 b)
    {
      Int2 result;
      result.X = a.X - b.X;
      result.Y = a.Y - b.Y;

      return result;
    }

    /// <summary>
    /// Subtracts vector b from vector a.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <param name="result">Difference of the two vectors</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Subtract(in Int2 a, in Int2 b, out Int2 result)
    {
      result.X = a.X - b.X;
      result.Y = a.Y - b.Y;
    }

    /// <summary>
    /// Divides the components of vector a by those of vector b.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Divisor vector</param>
    /// <returns>Quotient of the two vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int2 Divide(in Int2 a, in Int2 b)
    {
      Int2 result;
      result.X = a.X / b.X;
      result.Y = a.Y / b.Y;

      return result;
    }

    /// <summary>
    /// Divides the components of vector a by those of vector b.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Divisor vector</param>
    /// <param name="result">Quotient of the two vectors</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Divide(in Int2 a, in Int2 b, out Int2 result)
    {
      result.X = a.X / b.X;
      result.Y = a.Y / b.Y;
    }

    /// <summary>
    /// Divides the components of one vector by a scalar.
    /// </summary>
    /// <param name="value">First vector</param>
    /// <param name="divisor">Divisor scalar</param>
    /// <returns>Divided vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int2 Divide(in Int2 value, int divisor)
    {
      Int2 result;
      result.X = value.X / divisor;
      result.Y = value.Y / divisor;

      return result;
    }

    /// <summary>
    /// Divides the components of one vector by a scalar.
    /// </summary>
    /// <param name="value">First vector</param>
    /// <param name="divisor">Divisor scalar</param>
    /// <param name="result">Divided Vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Divide(in Int2 value, int divisor, out Int2 result)
    {
      result.X = value.X / divisor;
      result.Y = value.Y / divisor;
    }

    /// <summary>
    /// Multiplies components of a vector by a scalar value.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="scale">Amount to scale</param>
    /// <returns>Scaled vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int2 Multiply(in Int2 value, int scale)
    {
      Int2 result;
      result.X = value.X * scale;
      result.Y = value.Y * scale;

      return result;
    }

    /// <summary>
    /// Multiplies components of a vector by a scalar value.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="scale">Amount to scale</param>
    /// <param name="result">Scaled vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Multiply(in Int2 value, int scale, out Int2 result)
    {
      result.X = value.X * scale;
      result.Y = value.Y * scale;
    }

    /// <summary>
    /// Multiplies components of two vectors together.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Multiplied vectorr</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int2 Multiply(in Int2 a, in Int2 b)
    {
      Int2 result;
      result.X = a.X * b.X;
      result.Y = a.Y * b.Y;

      return result;
    }

    /// <summary>
    /// Multiplies components of two vectors together.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <param name="result">Multiplied vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Multiply(in Int2 a, in Int2 b, out Int2 result)
    {
      result.X = a.X * b.X;
      result.Y = a.Y * b.Y;
    }

    /// <summary>
    /// Restricts the source vector in the range of the minimum and maximum vectors.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="min">Minimum vector</param>
    /// <param name="max">Maximum vector</param>
    /// <returns>Clamped vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int2 Clamp(in Int2 value, in Int2 min, in Int2 max)
    {
      int x = value.X;
      int y = value.Y;

      x = (x > max.X) ? max.X : x;
      x = (x < min.X) ? min.X : x;

      y = (y > max.Y) ? max.Y : y;
      y = (y < min.Y) ? min.Y : y;

      Int2 result;
      result.X = x;
      result.Y = y;

      return result;
    }

    /// <summary>
    /// Restricts the source vector in the range of the minimum and maximum vectors.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="min">Minimum vector</param>
    /// <param name="max">Maximum vector</param>
    /// <param name="result">Clamped vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Clamp(in Int2 value, in Int2 min, in Int2 max, out Int2 result)
    {
      int x = value.X;
      int y = value.Y;

      x = (x > max.X) ? max.X : x;
      x = (x < min.X) ? min.X : x;

      y = (y > max.Y) ? max.Y : y;
      y = (y < min.Y) ? min.Y : y;

      result.X = x;
      result.Y = y;
    }

    /// <summary>
    /// Gets the vector that contains the mininum value from each of the components of the 
    /// two supplied vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Minimum vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int2 Min(in Int2 a, in Int2 b)
    {
      Int2 result;
      result.X = (a.X < b.X) ? a.X : b.X;
      result.Y = (a.Y < b.Y) ? a.Y : b.Y;

      return result;
    }

    /// <summary>
    /// Gets the vector that contains the mininum value from each of the components of the 
    /// two supplied vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <param name="result">Minimum vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Min(in Int2 a, in Int2 b, out Int2 result)
    {
      result.X = (a.X < b.X) ? a.X : b.X;
      result.Y = (a.Y < b.Y) ? a.Y : b.Y;
    }

    /// <summary>
    /// Gets the vector that contains the maximum value from each of the components of the 
    /// two supplied vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Maximum vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int2 Max(in Int2 a, in Int2 b)
    {
      Int2 result;
      result.X = (a.X > b.X) ? a.X : b.X;
      result.Y = (a.Y > b.Y) ? a.Y : b.Y;

      return result;
    }

    /// <summary>
    /// Gets the vector that contains the maximum value from each of the components of the 
    /// two supplied vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <param name="result">Maximum vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Max(in Int2 a, in Int2 b, out Int2 result)
    {
      result.X = (a.X > b.X) ? a.X : b.X;
      result.Y = (a.Y > b.Y) ? a.Y : b.Y;
    }

    /// <summary>
    /// Flips the signs of the components of the specified vector.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <returns>Negated vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int2 Negate(in Int2 value)
    {
      Int2 result;
      result.X = -value.X;
      result.Y = -value.Y;

      return result;
    }

    /// <summary>
    /// Flips the signs of the components of the specified vector.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="result">Negated vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Negate(in Int2 value, out Int2 result)
    {
      result.X = -value.X;
      result.Y = -value.Y;
    }

    /// <summary>
    /// Adds the two vectors together.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Sum of the two vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int2 operator +(Int2 a, Int2 b)
    {
      Int2 result;
      result.X = a.X + b.X;
      result.Y = a.Y + b.Y;

      return result;
    }

    /// <summary>
    /// Subtracts vector b from vector a.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Difference of the two vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int2 operator -(Int2 a, Int2 b)
    {
      Int2 result;
      result.X = a.X - b.X;
      result.Y = a.Y - b.Y;

      return result;
    }

    /// <summary>
    /// Flips the signs of the components of the specified vector.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <returns>Negated vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int2 operator -(Int2 value)
    {
      Int2 result;
      result.X = -value.X;
      result.Y = -value.Y;

      return result;
    }

    /// <summary>
    /// Multiplies two vectors together.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Multiplied vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int2 operator *(Int2 a, Int2 b)
    {
      Int2 result;
      result.X = a.X * b.X;
      result.Y = a.Y * b.Y;

      return result;
    }

    /// <summary>
    /// Multiplies a vector by a scaling factor.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="scale">Amount to scale</param>
    /// <returns>Multiplied vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int2 operator *(Int2 value, int scale)
    {
      Int2 result;
      result.X = value.X * scale;
      result.Y = value.Y * scale;

      return result;
    }

    /// <summary>
    /// Multiplies a vector by a scaling factor.
    /// </summary>
    /// <param name="scale">Amount to scale</param>
    /// <param name="value">Source vector</param>
    /// <returns>Multiplied vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int2 operator *(int scale, Int2 value)
    {
      Int2 result;
      result.X = value.X * scale;
      result.Y = value.Y * scale;

      return result;
    }

    /// <summary>
    /// Divides the components of vector a by those of vector b.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Divisor vector</param>
    /// <returns>Quotient of the two vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int2 operator /(Int2 a, Int2 b)
    {
      Int2 result;
      result.X = a.X / b.X;
      result.Y = a.Y / b.Y;

      return result;
    }

    /// <summary>
    /// Divides a vector by a scalar value.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="divisor">Divisor scalar</param>
    /// <returns>Divided vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int2 operator /(Int2 value, int divisor)
    {
      Int2 result;
      result.X = value.X / divisor;
      result.Y = value.Y / divisor;

      return result;
    }

    /// <summary>
    /// Performs an explicit conversion from <see cref="Int2"/> to <see cref="Vector2"/>.
    /// </summary>
    /// <param name="value"><see cref="Int2"/> value</param>
    /// <returns>Converted <see cref="Vector2"/></returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static explicit operator Vector2(Int2 value)
    {
      Vector2 result;
      result.X = (float) value.X;
      result.Y = (float) value.Y;

      return result;
    }

    /// <summary>
    /// Performs an explicit conversion from <see cref="Vector2"/> to <see cref="Int2"/>.
    /// </summary>
    /// <param name="value"><see cref="Vector2"/> value</param>
    /// <returns>Converted <see cref="Int2"/></returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static explicit operator Int2(Vector2 value)
    {
      Int2 result;
      result.X = (int) value.X;
      result.Y = (int) value.Y;

      return result;
    }

    /// <summary>
    /// Implicitly converts from <see cref="Int2"/> to <see cref="System.Numerics.Vector2"/> for interop.
    /// </summary>
    /// <param name="v">Vector to convert.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static implicit operator System.Numerics.Vector2(Int2 v)
    {
      return new System.Numerics.Vector2(v.X, v.Y);
    }

    /// <summary>
    /// Implicitly converts from <see cref="System.Numerics.Vector2"/> to <see cref="Int2"/> for interop.
    /// </summary>
    /// <param name="v">Vector to convert.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static implicit operator Int2(System.Numerics.Vector2 v)
    {
      return new Int2((int) v.X, (int) v.Y);
    }

    /// <summary>
    /// Checks equality between two vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator ==(Int2 a, Int2 b)
    {
      return (a.X == b.X) && (a.Y == b.Y);
    }

    /// <summary>
    /// Checks inequality between two vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>True if both are not equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator !=(Int2 a, Int2 b)
    {
      return (a.X != b.X) || (a.Y != b.Y);
    }

    /// <summary>
    /// Flips the signs of the components of the vector.
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void Negate()
    {
      X = -X;
      Y = -Y;
    }

    /// <summary>
    /// Checks equality between the vector and another vector.
    /// </summary>
    /// <param name="other">Other vector</param>
    /// <returns>True if the vectors are equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    readonly bool IEquatable<Int2>.Equals(Int2 other)
    {
      return (X == other.X) && (Y == other.Y);
    }

    /// <summary>
    /// Checks equality between the vector and another vector.
    /// </summary>
    /// <param name="other">Other vector</param>
    /// <returns>True if the vectors are equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Equals(in Int2 other)
    {
      return (X == other.X) && (Y == other.Y);
    }

    /// <summary>
    /// Tests equality between the vector and XY values.
    /// </summary>
    /// <param name="x">X component.</param>
    /// <param name="y">Y component.</param>
    /// <returns>True if components are equal within tolerance, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Equals(float x, float y)
    {
      return (X == x) && (Y == y);
    }

    /// <summary>
    /// Determines whether the specified <see cref="System.Object" /> is equal to this instance.
    /// </summary>
    /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
    /// <returns>True if the specified <see cref="System.Object" /> is equal to this instance; otherwise, False.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is Int2)
        return Equals((Int2) obj);

      return false;
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
    public readonly override int GetHashCode()
    {
      unchecked
      {
        return X.GetHashCode() + Y.GetHashCode();
      }
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly override string ToString()
    {
      CultureInfo info = CultureInfo.CurrentCulture;
      return String.Format(info, "X: {0} Y: {1}",
          new Object[] { X.ToString(info), Y.ToString(info) });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="format">The format</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(string? format)
    {
      if (format is null)
        return ToString();

      CultureInfo info = CultureInfo.CurrentCulture;
      return String.Format(info, "X: {0} Y: {1}",
          new Object[] { X.ToString(format, info), Y.ToString(format, info) });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="formatProvider">The format provider.</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(IFormatProvider? formatProvider)
    {
      if (formatProvider is null)
        return ToString();

      return String.Format(formatProvider, "X: {0} Y: {1}",
          new Object[] { X.ToString(formatProvider), Y.ToString(formatProvider) });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="format">The format.</param>
    /// <param name="formatProvider">The format provider.</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(string? format, IFormatProvider? formatProvider)
    {
      if (formatProvider is null)
        return ToString();

      if (format is null)
        return ToString(formatProvider);

      return String.Format(formatProvider, "X: {0} Y: {1}",
          new Object[] { X.ToString(format, formatProvider), Y.ToString(format, formatProvider) });
    }

    /// <summary>
    /// Writes the primitive data to the output.
    /// </summary>
    /// <param name="output">Primitive writer.</param>
    readonly void IPrimitiveValue.Write(IPrimitiveWriter output)
    {
      output.Write("X", X);
      output.Write("Y", Y);
    }

    /// <summary>
    /// Reads the primitive data from the input.
    /// </summary>
    /// <param name="input">Primitive reader.</param>
    void IPrimitiveValue.Read(IPrimitiveReader input)
    {
      X = input.ReadInt32("X");
      Y = input.ReadInt32("Y");
    }
  }
}
