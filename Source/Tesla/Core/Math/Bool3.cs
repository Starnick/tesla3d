﻿/*
* Copyright (c) 2010-2024 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Tesla.Content;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Defines a three dimensional vector where each component is a 32-bit boolean.
  /// </summary>
  [Serializable]
  [StructLayout(LayoutKind.Sequential, Pack = 4)]
  public struct Bool3 : IEquatable<Bool3>, IRefEquatable<Bool3>, IPrimitiveValue
  {
    /// <summary>
    /// X component of the vector.
    /// </summary>
    public Bool X;

    /// <summary>
    /// Y component of the vector.
    /// </summary>
    public Bool Y;

    /// <summary>
    /// Z component of the vector.
    /// </summary>
    public Bool Z;

    private static readonly Bool3 s_false = new Bool3(false);
    private static readonly Bool3 s_true = new Bool3(true);
    private static readonly Bool3 s_unitX = new Bool3(true, false, false);
    private static readonly Bool3 s_unitY = new Bool3(false, true, false);
    private static readonly Bool3 s_unitZ = new Bool3(false, false, true);

    private static readonly int s_sizeInBytes = BufferHelper.SizeOf<Bool3>();

    /// <summary>
    /// Gets a <see cref="Bool3"/> set to (false, false, false).
    /// </summary>
    public static ref readonly Bool3 False
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_false;
      }
    }

    /// <summary>
    /// Gets a <see cref="Bool3"/> set to (true, true, true).
    /// </summary>
    public static ref readonly Bool3 True
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_true;
      }
    }

    /// <summary>
    /// Gets a unit <see cref="Bool3"/> set to (true, false, false).
    /// </summary>
    public static ref readonly Bool3 UnitX
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_unitX;
      }
    }

    /// <summary>
    /// Gets a <see cref="Bool3"/> set to (false, true, false).
    /// </summary>
    public static ref readonly Bool3 UnitY
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_unitY;
      }
    }

    /// <summary>
    /// Gets a unit <see cref="Bool3"/> set to (false, false, true).
    /// </summary>
    public static ref readonly Bool3 UnitZ
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_unitZ;
      }
    }

    /// <summary>
    /// Gets the size of the <see cref="Bool3"/> type in bytes.
    /// </summary>
    public static int SizeInBytes
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return s_sizeInBytes;
      }
    }

    /// <summary>
    /// Gets or sets individual components of the vector in the order that the components are declared (XYZ).
    /// </summary>
    /// <param name="index">Zero-based index, in range of [0, 2].</param>
    /// <returns>The value of the specified component.</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the index is out of range [0, 2]</exception>
    public Bool this[int index]
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      readonly get
      {
        switch (index)
        {
          case 0:
            return X;
          case 1:
            return Y;
          case 2:
            return Z;
          default:
            throw new ArgumentOutOfRangeException(nameof(index), StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));
        }
      }
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      set
      {
        switch (index)
        {
          case 0:
            X = value;
            break;
          case 1:
            Y = value;
            break;
          case 2:
            Z = value;
            break;
          default:
            throw new ArgumentOutOfRangeException(nameof(index), StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));
        }
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Bool3"/> struct.
    /// </summary>
    /// <param name="value">Value to initialize each component to</param>
    public Bool3(Bool value)
    {
      X = Y = Z = value;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Bool3"/> struct.
    /// </summary>
    /// <param name="xy">Vector containing XY components.</param>
    /// <param name="z">Z component</param>
    public Bool3(in Bool2 xy, Bool z)
    {
      X = xy.X;
      Y = xy.Y;
      Z = z;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Bool3"/> struct.
    /// </summary>
    /// <param name="x">X component</param>
    /// <param name="y">Y component</param>
    /// <param name="z">Z component</param>
    public Bool3(Bool x, Bool y, Bool z)
    {
      X = x;
      Y = y;
      Z = z;
    }

    /// <summary>
    /// Checks equality between two vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator ==(Bool3 a, Bool3 b)
    {
      return (a.X == b.X) && (a.Y == b.Y) && (a.Z == b.Z);
    }

    /// <summary>
    /// Checks inequality between two vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>True if both are not equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator !=(Bool3 a, Bool3 b)
    {
      return (a.X != b.X) || (a.Y != b.Y) || (a.Z != b.Z);
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
    public readonly override int GetHashCode()
    {
      unchecked
      {
        return X.GetHashCode() + Y.GetHashCode() + Z.GetHashCode();
      }
    }

    /// <summary>
    /// Checks inequality between the vector and another vector.
    /// </summary>
    /// <param name="other">Other vector</param>
    /// <returns>True if the vectors are equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    readonly bool IEquatable<Bool3>.Equals(Bool3 other)
    {
      return (X == other.X) && (Y == other.Y) && (Z == other.Z);
    }

    /// <summary>
    /// Checks inequality between the vector and another vector.
    /// </summary>
    /// <param name="other">Other vector</param>
    /// <returns>True if the vectors are equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Equals(in Bool3 other)
    {
      return (X == other.X) && (Y == other.Y) && (Z == other.Z);
    }

    /// <summary>
    /// Determines whether the specified <see cref="System.Object" /> is equal to this instance.
    /// </summary>
    /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
    /// <returns>True if the specified <see cref="System.Object" /> is equal to this instance; otherwise, False.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is Bool3)
        return Equals((Bool3)obj);

      return false;
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly override string ToString()
    {
      CultureInfo info = CultureInfo.CurrentCulture;
      return String.Format(info, "X: {0} Y: {1} Z: {2}",
          new Object[] { ((bool)X).ToString(info), ((bool)Y).ToString(info), ((bool)Z).ToString(info) });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="formatProvider">The format provider.</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(IFormatProvider? formatProvider)
    {
      if (formatProvider is null)
        return ToString();

      return String.Format(formatProvider, "X: {0} Y: {1} Z: {2}",
          new Object[] { ((bool)X).ToString(formatProvider), ((bool)Y).ToString(formatProvider), ((bool)Z).ToString(formatProvider) });
    }

    /// <summary>
    /// Writes the primitive data to the output.
    /// </summary>
    /// <param name="output">Primitive writer.</param>
    readonly void IPrimitiveValue.Write(IPrimitiveWriter output)
    {
      //Writing out actual bools on purpose to avoid excessive grouping
      output.Write("X", (bool) X);
      output.Write("Y", (bool) Y);
      output.Write("Z", (bool) Z);
    }

    /// <summary>
    /// Reads the primitive data from the input.
    /// </summary>
    /// <param name="input">Primitive reader.</param>
    void IPrimitiveValue.Read(IPrimitiveReader input)
    {
      X = input.ReadBoolean("X");
      Y = input.ReadBoolean("Y");
      Z = input.ReadBoolean("Z");
    }
  }
}
