﻿/*
* Copyright (c) 2010-2024 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Runtime.CompilerServices;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Utility class that contains math constants and useful math functions.
  /// </summary>
  public static class MathHelper
  {
    /// <summary>
    /// Represents the math constant e.
    /// </summary>
    public const float E = MathF.E;

    /// <summary>
    /// Represents the math constant log base ten of e.
    /// </summary>
    public const float Log10E = (float)0.43429448190325182d;

    /// <summary>
    /// Represents the log base two of e.
    /// </summary>
    public const float Log2E = (float)1.4426950408889634d;

    /// <summary>
    /// Represents Pi/2 (90 degrees).
    /// </summary>
    public const float PiOverTwo = MathF.PI / 2.0f;

    /// <summary>
    /// Represents Pi/4 (45 degrees).
    /// </summary>
    public const float PiOverFour = MathF.PI / 4.0f;

    /// <summary>
    /// Represents 3Pi/4 (135 degrees).
    /// </summary>
    public const float ThreePiOverFour = (3.0f * MathF.PI) / 4.0f;

    /// <summary>
    /// Represents the value of pi (180 degrees).
    /// </summary>
    public const float Pi = MathF.PI;

    /// <summary>
    /// Represents 2Pi (360 degrees), otherwise known as <see cref="MathHelper.Tau"/>.
    /// </summary>
    public const float TwoPi = MathF.Tau;

    /// <summary>
    /// Represents 2PI (360 degrees), otherwise known as <see cref="MathHelper.TwoPi"/>.
    /// </summary>
    public const float Tau = MathF.Tau;

    /// <summary>
    /// Value to multiply degrees by to obtain radians.
    /// </summary>
    public const float DegreesToRadians = MathF.PI / 180.0f;

    /// <summary>
    /// Value to multiply radians by to obtain degrees.
    /// </summary>
    public const float RadiansToDegrees = 180.0f / MathF.PI;

    /// <summary>
    /// Represents Pi^2
    /// </summary>
    public const float PiSquared = MathF.PI * MathF.PI;

    /// <summary>
    /// One thirds constant (1/3).
    /// </summary>
    public const float OneThird = 1.0f / 3.0f;

    /// <summary>
    /// Two thirds constant (2/3).
    /// </summary>
    public const float TwoThird = 2.0f / 3.0f;

    /// <summary>
    /// Four thirds constant (4/3).
    /// </summary>
    public const float FourThirds = 2.0f / 3.0f;

    /// <summary>
    /// Represents a "close to zero" value.
    /// </summary>
    public const float ZeroTolerance = 1E-06f;

    /// <summary>
    /// Represents a very tight "close to zero" value.
    /// </summary>
    public const float TightZeroTolerance = 1E-12f;

    /// <summary>
    /// Represents a "close to zero" value, the smallest float value possible.
    /// </summary>
    public const float Epsilon = float.Epsilon;

    /// <summary>
    /// Converts an angle in radians to the corresponding angle in degrees.
    /// </summary>
    /// <param name="radians">Angle in radians</param>
    /// <returns>Angle in degrees</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float ToDegrees(float radians)
    {
      return radians * RadiansToDegrees;
    }

    /// <summary>
    /// Converts an angle in degrees to the corresponding angle in radians.
    /// </summary>
    /// <param name="degrees">Angle in degrees</param>
    /// <returns>Angle in radians</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float ToRadians(float degrees)
    {
      return degrees * DegreesToRadians;
    }

    /// <summary>
    /// Given two values, returns the min and max values.
    /// </summary>
    /// <param name="a">First value</param>
    /// <param name="b">Second value</param>
    /// <param name="min">Smaller value</param>
    /// <param name="max">Larger value</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void MinMax(int a, int b, out int min, out int max)
    {
      if (a > b)
      {
        min = b;
        max = a;
      }
      else
      {
        min = a;
        max = b;
      }
    }

    /// <summary>
    /// Given two values, returns the min and max values.
    /// </summary>
    /// <param name="a">First value</param>
    /// <param name="b">Second value</param>
    /// <param name="min">Smaller value</param>
    /// <param name="max">Larger value</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void MinMax(float a, float b, out float min, out float max)
    {
      if (a > b)
      {
        min = b;
        max = a;
      }
      else
      {
        min = a;
        max = b;
      }
    }

    /// <summary>
    /// Reduces an angle in radians to the range of pi to -pi.
    /// </summary>
    /// <param name="angle">Angle in radians</param>
    /// <returns>Reduced angle</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float WrapAngle(float angle)
    {
      angle = MathF.IEEERemainder(angle, MathHelper.TwoPi);

      if (angle <= -Pi)
      {
        angle += TwoPi;
        return angle;
      }

      if (angle > Pi)
        angle -= TwoPi;

      return angle;
    }

    /// <summary>
    /// Clamps a value within the specified range.
    /// </summary>
    /// <param name="value">Source value</param>
    /// <param name="min">Minimum value</param>
    /// <param name="max">Maximum value</param>
    /// <returns>Clamped value</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static int Clamp(int value, int min, int max)
    {
      value = (value > max) ? max : value;
      value = (value < min) ? min : value;
      return value;
    }

    /// <summary>
    /// Clamps a value within the specified range.
    /// </summary>
    /// <param name="value">Source value</param>
    /// <param name="min">Minimum value</param>
    /// <param name="max">Maximum value</param>
    /// <returns>Clamped value</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float Clamp(float value, float min, float max)
    {
      value = (value > max) ? max : value;
      value = (value < min) ? min : value;
      return value;
    }

    /// <summary>
    /// Checks if a value is within the specified range, inclusive of the min/max
    /// </summary>
    /// <param name="value">Source value</param>
    /// <param name="min">Minimum value</param>
    /// <param name="max">Maximum value</param>
    /// <param name="tol">Optional tolerance to handle when the value is very close to the min or max values.</param>
    /// <returns>True if the value is within the interval false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool InInterval(float value, float min, float max, float tol = MathHelper.ZeroTolerance)
    {
      if ((value - tol) >= max || (value + tol) <= min)
        return false;

      return true;
    }

    /// <summary>
    /// Checks if a value is within the specified range, exclusive of the min/max.
    /// </summary>
    /// <param name="value">Source value</param>
    /// <param name="min">Minimum value</param>
    /// <param name="max">Maximum value</param>
    /// <param name="tol">Optional tolerance to handle when the value is very close to the min or max values.</param>
    /// <returns>True if the value is within the interval false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool InIntervalExclusive(float value, float min, float max, float tol = MathHelper.ZeroTolerance)
    {
      if ((value - tol) > max || (value + tol) < min)
        return false;

      return true;
    }

    /// <summary>
    /// Clamps an integer to the byte range 0-255.
    /// </summary>
    /// <param name="value">Integer to be clamped</param>
    /// <returns>Clamped value</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static int ClampToByte(int value)
    {
      if (value < 0)
        return 0;
      else if (value > 255)
        return 255;

      return value;
    }

    /// <summary>
    /// Clamps and rounds a value within the specified range.
    /// </summary>
    /// <param name="value">Source value</param>
    /// <param name="min">Minimum value</param>
    /// <param name="max">Maximum value</param>
    /// <returns>Clamped and rounded value</returns>
    public static float ClampAndRound(float value, float min, float max)
    {
      if (float.IsNaN(value))
        return 0.0f;

      if (float.IsInfinity(value))
        return float.IsNegativeInfinity(value) ? min : max;

      if (value < min)
        return min;

      if (value > max)
        return max;

      return MathF.Round(value);
    }

    /// <summary>
    /// Computes the absolute value between a and b.
    /// </summary>
    /// <param name="a">First value</param>
    /// <param name="b">Second value</param>
    /// <returns>Distance between the two</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float Distance(float a, float b)
    {
      return MathF.Abs(a - b);
    }

    /// <summary>
    /// Linearly interpolates between two values.
    /// </summary>
    /// <param name="a">Starting value</param>
    /// <param name="b">Ending value</param>
    /// <param name="percent">Amount to interpolate</param>
    /// <returns>Interpolated value</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float Lerp(float a, float b, float percent)
    {
      // Better for floating point precision than a + (b - a) * percent;
      return a * (1.0f - percent) + b * percent;
    }

    /// <summary>
    /// Interpolates between two values using a cubic equation.
    /// </summary>
    /// <param name="a">Starting value</param>
    /// <param name="b">Ending value</param>
    /// <param name="percent">Amount to interpolate</param>
    /// <returns>Interpolated value</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float SmoothStep(float a, float b, float percent)
    {
      float clamped = Clamp(percent, 0.0f, 1.0f);
      return Lerp(a, b, (clamped * clamped) * (3.0f - (2.0f * clamped)));
    }

    /// <summary>
    /// Checks if the value is nearly zero.
    /// </summary>
    /// <param name="a">Value to check</param>
    /// <returns>True if the value is zero or nearly zero, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool IsNearlyZero(float a)
    {
      return MathF.Abs(a) < ZeroTolerance;
    }

    /// <summary>
    /// Checks if the value is nearly one.
    /// </summary>
    /// <param name="a">Value to check.</param>
    /// <returns>True if the value is one or nearly one, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool IsNearlyOne(float a)
    {
      return MathF.Abs(a - 1.0f) < ZeroTolerance;
    }

    /// <summary>
    /// Checks if a - b are within an epsilon value.
    /// </summary>
    /// <param name="a">First value</param>
    /// <param name="b">Second value</param>
    /// <returns>True if within range</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool WithinEpsilon(float a, float b)
    {
      float diff = a - b;
      return (ZeroTolerance <= diff) && (diff <= ZeroTolerance);
    }

    /// <summary>
    /// Checks if a - b are within an epsilon value.
    /// </summary>
    /// <param name="a">First value</param>
    /// <param name="b">Second value</param>
    /// <param name="epsilon">Epsilon value.</param>
    /// <returns>True if within range</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool WithinEpsilon(float a, float b, float epsilon)
    {
      float diff = a - b;
      return (epsilon <= diff) && (diff <= epsilon);
    }

    /// <summary>
    /// Checks equality between a and b using ZeroTolerance.
    /// </summary>
    /// <param name="a">First value</param>
    /// <param name="b">Second value</param>
    /// <returns>True if a is nearly equal to b</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool IsEqual(float a, float b)
    {
      return MathF.Abs(a - b) <= ZeroTolerance;
    }

    /// <summary>
    /// Checks equality between a and b using the specified tolerance
    /// </summary>
    /// <param name="a">First value</param>
    /// <param name="b">Second value</param>
    /// <param name="tolerance">Tolerance that a and b should be within</param>
    /// <returns>True if a is nearly equal to b</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool IsEqual(float a, float b, float tolerance)
    {
      return MathF.Abs(a - b) <= tolerance;
    }

    /// <summary>
    /// Divides two numbers safely. If cannot divide, false is returned and result is set to zero.
    /// </summary>
    /// <param name="numerator">Numerator value</param>
    /// <param name="denominator">Denominator value</param>
    /// <param name="result">Quotient result</param>
    /// <returns>True if division occured, false if could not divide.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool SafeDivide(float numerator, float denominator, out float result)
    {
      if (MathF.Abs(denominator) <= (MathF.Abs(numerator) * 1E-14))
      {
        result = 0.0f;
        return false;
      }

      result = numerator / denominator;
      return true;
    }

    /// <summary>
    /// Divides two numbers safely. If cannot divide, false is returned and result is set to the default quotient value.
    /// </summary>
    /// <param name="numerator">Numerator value</param>
    /// <param name="denominator">Denominator value</param>
    /// <param name="defaultQuotient">Default quotient value that is returned if division fails.</param>
    /// <param name="result">Quotient result</param>
    /// <returns>True if division occured, false if could not divide.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool SafeDivide(float numerator, float denominator, float defaultQuotient, out float result)
    {
      if (MathF.Abs(denominator) <= (MathF.Abs(numerator) * 1E-14))
      {
        result = defaultQuotient;
        return false;
      }

      result = numerator / denominator;
      return true;
    }

    /// <summary>
    /// Solves a 2x2 linear system of equations. The equations look like: [xCoff1(X) + yCoff1(Y) = eq1RightValue]
    /// and [xCoff2(X) + yCoff1(Y) = eq2RightValue].
    /// </summary>
    /// <param name="xCoff1">Coefficient of the X variable of the first equation.</param>
    /// <param name="yCoff1">Coefficient of the Y variable of the first equation.</param>
    /// <param name="eq1RightValue">Equation 1's right hand value.</param>
    /// <param name="xCoff2">Coefficient of the X variable of the second equation.</param>
    /// <param name="yCoff2">Coefficient of the Y variable of the second equation.</param>
    /// <param name="eq2RightValue">Equation 2's right hand value.</param>
    /// <param name="xSolution">X variable solution.</param>
    /// <param name="ySolution">Y variable solution.</param>
    /// <returns>True if the equation could be solved, false otherwise.</returns>
    public static bool Solve2x2System(float xCoff1, float yCoff1, float eq1RightValue, float xCoff2, float yCoff2, float eq2RightValue, out float xSolution, out float ySolution)
    {
      float denom = (xCoff1 * yCoff2) - (yCoff1 * xCoff2);

      if (MathF.Abs(denom) > 0.0)
      {
        xSolution = ((yCoff2 * eq1RightValue) - (yCoff1 * eq2RightValue)) / denom;
        ySolution = ((xCoff1 * eq2RightValue) - (xCoff2 * eq1RightValue)) / denom;
        return true;
      }

      xSolution = ySolution = 0.0f;
      return false;
    }

    /// <summary>
    /// Handles a zero-based cyclic axis indexer, forcing positive and negative indices to { 0, 1 } e.g. modulo 2.
    /// </summary>
    /// <param name="axis">Zero based axis index.</param>
    /// <returns>Axis index</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static int Cyclic2DAxis(int axis)
    {
      return Math.Abs(axis) % 2;
    }

    /// <summary>
    /// Handles a zero-based cyclic axis indexer, forcing positive and negative indices to { 0, 1, 2 }, e.g. modulo 3.
    /// </summary>
    /// <param name="axis">Zero based axis index.</param>
    /// <returns>Axis index</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static int Cyclic3DAxis(int axis)
    {
      return Math.Abs(axis) % 3;
    }

    /// <summary>
    /// Handles a zero-based cyclic axis indexer, forcing positive and negative indices to { 0, 1, 2 , 3}, e.g. modulo 4.
    /// </summary>
    /// <param name="axis">Zero based axis index.</param>
    /// <returns>Axis index</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static int Cyclic4DAxis(int axis)
    {
      return Math.Abs(axis) % 4;
    }

    /// <summary>
    /// Solves the quadratic equation where a is the quadratic term, b the linear term, and c the constant term. A*x^2 + B*X + C = 0.
    /// </summary>
    /// <param name="a">Quadratic term</param>
    /// <param name="b">Linear term</param>
    /// <param name="c">Constant term</param>
    /// <param name="result1">First solution</param>
    /// <param name="result2">Second solution</param>
    /// <returns>True if a real solution was found, false if imaginary.</returns>
    public static bool SolveQuadraticEquation(float a, float b, float c, out float result1, out float result2)
    {
      float sqrtpart = b * b - 4 * a * c;

      //Two real solutions
      if (sqrtpart > 0)
      {
        result1 = (-b + MathF.Sqrt(sqrtpart)) / (2 * a);
        result2 = (-b - MathF.Sqrt(sqrtpart)) / (2 * a);

        return true;
      }
      //Two imaginary solutions
      else if (sqrtpart < 0)
      {
        sqrtpart = -sqrtpart;
        result1 = result2 = 0;
        result2 = result1;

        return false;
      }
      //One real solution
      else
      {
        result1 = (-b + MathF.Sqrt(sqrtpart)) / (2 * a);
        result2 = result1;

        return true;
      }
    }
  }
}
