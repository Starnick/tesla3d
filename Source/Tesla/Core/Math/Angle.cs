﻿/*
* Copyright (c) 2010-2024 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Tesla.Content;
using Tesla.Design;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Defines an unit independent angle.
  /// </summary>
  [Serializable]
  [StructLayout(LayoutKind.Sequential, Size = 4)]
  [TypeConverter(typeof(AngleTypeConverter))]
  public struct Angle : IComparable<Angle>, IEquatable<Angle>, IRefEquatable<Angle>, IFormattable, IPrimitiveValue
  {
    private static readonly Angle s_zero = new Angle(0.0f);
    private static readonly Angle s_piOverFour = new Angle(MathHelper.PiOverFour);
    private static readonly Angle s_piOverTwo = new Angle(MathHelper.PiOverTwo);
    private static readonly Angle s_pi = new Angle(MathHelper.Pi);
    private static readonly Angle s_threePiOverFour = new Angle(MathHelper.ThreePiOverFour);
    private static readonly Angle s_twoPi = new Angle(MathHelper.TwoPi);

    private static readonly int s_sizeInBytes = BufferHelper.SizeOf<Angle>();

    //Underlying value stored in radians
    private float m_radians;

    /// <summary>
    /// Gets an <see cref="Angle"/> that is 0°.
    /// </summary>
    public static Angle Zero
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return s_zero;
      }
    }

    /// <summary>
    /// Gets an <see cref="Angle"/> that is 45°.
    /// </summary>
    public static Angle PiOverFour
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return s_piOverFour;
      }
    }

    /// <summary>
    /// Gets an <see cref="Angle"/> that is 90°.
    /// </summary>
    public static Angle PiOverTwo
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return s_piOverTwo;
      }
    }

    /// <summary>
    /// Gets an <see cref="Angle"/> that is 135°.
    /// </summary>
    public static Angle ThreePiOverFour
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return s_threePiOverFour;
      }
    }

    /// <summary>
    /// Gets an <see cref="Angle"/> that is 180°.
    /// </summary>
    public static Angle Pi
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return s_pi;
      }
    }

    /// <summary>
    /// Gets an <see cref="Angle"/> that is 360°.
    /// </summary>
    public static Angle TwoPi
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return s_twoPi;
      }
    }

    /// <summary>
    /// Gets the size of the <see cref="Angle"/> type in bytes.
    /// </summary>
    public static int SizeInBytes
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return s_sizeInBytes;
      }
    }

    /// <summary>
    /// Gets or sets the angle, in radians.
    /// </summary>
    public float Radians
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      readonly get
      {
        return m_radians;
      }
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      set
      {
        m_radians = value;
      }
    }

    /// <summary>
    /// Gets or sets the angle, in degrees.
    /// </summary>
    public float Degrees
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      readonly get
      {
        return m_radians * MathHelper.RadiansToDegrees;
      }
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      set
      {
        m_radians = MathHelper.DegreesToRadians * value;
      }
    }

    /// <summary>
    /// Gets the sine of the angle.
    /// </summary>
    public readonly float Sin
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return MathF.Sin(m_radians);
      }
    }

    /// <summary>
    /// Gets the cosine of the angle.
    /// </summary>
    public readonly float Cos
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return MathF.Cos(m_radians);
      }
    }

    /// <summary>
    /// Gets the tangent of the angle.
    /// </summary>
    public readonly float Tan
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return MathF.Tan(m_radians);
      }
    }

    /// <summary>
    /// Gets the angle which completes the full circle (360°) with the same sweep direction.
    /// </summary>
    public readonly Angle ForwardSweepToFullCircle
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        Angle result;
        if (m_radians > 0.0f)
          result.m_radians = MathHelper.TwoPi - m_radians;
        else
          result.m_radians = -MathHelper.TwoPi - m_radians;

        return result;
      }
    }

    /// <summary>
    /// Gets the angle which completes the full circle (360°) with the opposite sweep direction.
    /// </summary>
    public readonly Angle ReverseSweepToFullCircle
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        Angle result;
        if (m_radians > 0.0)
          result.m_radians = m_radians - MathHelper.TwoPi;
        else
          result.m_radians = m_radians + MathHelper.TwoPi;

        return result;
      }
    }

    /// <summary>
    /// Gets the angle that complements this angle, where both angles add to 90°.
    /// </summary>
    public readonly Angle Complement
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        Angle result;
        result.m_radians = MathHelper.PiOverTwo - m_radians;

        return result;
      }
    }

    /// <summary>
    /// Gets the angle that supplements this angle, where both angles add to 180°.
    /// </summary>
    public readonly Angle Supplement
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        Angle result;
        result.m_radians = MathHelper.Pi - m_radians;

        return result;
      }
    }

    /// <summary>
    /// Gets if the angle is Not a Number (NaN) or not.
    /// </summary>
    public readonly bool IsNaN
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return float.IsNaN(m_radians);
      }
    }

    /// <summary>
    /// Gets whether the angle is positive or negative infinity.
    /// </summary>
    public readonly bool IsInfinity
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return float.IsInfinity(m_radians);
      }
    }

    /// <summary>
    /// Gets if the angle is a full circle, that is if it is 360°.
    /// </summary>
    public readonly bool IsFullCircle
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        float radians = MathF.Abs(m_radians);
        return MathHelper.IsEqual(radians, MathHelper.TwoPi);
      }
    }

    /// <summary>
    /// Gets if the angle is zero, that is if it is 0°.
    /// </summary>
    public readonly bool IsZeroAngle
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        float radians = MathF.Abs(m_radians);
        return MathHelper.IsEqual(radians, 0.0f);
      }
    }

    /// <summary>
    /// Gets if the angle is an acute angle, where it is less than 90° but greater than 0°.
    /// </summary>
    public readonly bool IsAcuteAngle
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        float radians = MathF.Abs(m_radians);
        return radians > 0.0f && radians < MathHelper.PiOverTwo;
      }
    }

    /// <summary>
    /// Gets if the angle is an obtuse angle, where it is is gerater than 90° but less than 180°.
    /// </summary>
    public readonly bool IsObtuseAngle
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        float radians = MathF.Abs(m_radians);
        return radians > MathHelper.PiOverTwo && radians < MathHelper.Pi;
      }
    }

    /// <summary>
    /// Gets if the angle is a right angle, where it is 90°.
    /// </summary>
    public readonly bool IsRightAngle
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        float radians = MathF.Abs(m_radians);
        return MathHelper.IsEqual(radians, MathHelper.PiOverTwo);
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Angle"/> struct.
    /// </summary>
    /// <param name="radians">The angle, in radians.</param>
    public Angle(float radians)
    {
      m_radians = radians;
    }

    /// <summary>
    /// Returns the angle whose cosine is the specified number.
    /// </summary>
    /// <param name="num">Cosine of the angle where the value must be greater than or equal to -1, but less than or equal to 1.</param>
    /// <returns>The angle in the range [0, π] or NaN if the number is out of range. </returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Angle Asin(float num)
    {
      Angle result;
      result.m_radians = MathF.Asin(num);

      return result;
    }

    /// <summary>
    /// Returns the angle whose sine is the specified number.
    /// </summary>
    /// <param name="num">Sine of the angle where the value must be greater than or equal to -1, but less than or equal to 1.</param>
    /// <returns>The angle in the range [-π/2, π/2] or NaN if the number is out of range.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Angle Acos(float num)
    {
      Angle result;
      result.m_radians = MathF.Acos(num);

      return result;
    }

    /// <summary>
    /// Returns the angle whose tangent is the specified number.
    /// </summary>
    /// <param name="num">The tangent</param>
    /// <returns>The angle in the range [-π/2, π/2] or NaN if the number is out of range.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Angle Atan(float num)
    {
      Angle result;
      result.m_radians = MathF.Atan(num);

      return result;
    }

    /// <summary>
    /// Returns the angle whose tangent is the quotient of two coordinates of a cartesian point.
    /// </summary>
    /// <param name="y">Y coordinate of the point</param>
    /// <param name="x">X coordinate of the point</param>
    /// <returns>The angle in the range [-π, π] or NaN if the coordinates are out of range.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Angle Atan2(float y, float x)
    {
      Angle result;
      result.m_radians = MathF.Atan2(y, x);

      return result;
    }

    /// <summary>
    /// Adds two angles together.
    /// </summary>
    /// <param name="a">First angle</param>
    /// <param name="b">Second angle</param>
    /// <returns>Sum of the two angles</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Angle Add(Angle a, Angle b)
    {
      Angle result;
      result.m_radians = a.m_radians + b.m_radians;

      return result;
    }

    /// <summary>
    /// Subtracts angle b from angle a.
    /// </summary>
    /// <param name="a">First angle</param>
    /// <param name="b">Second angle</param>
    /// <returns>Difference of the two angles</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Angle Subtract(Angle a, Angle b)
    {
      Angle result;
      result.m_radians = a.m_radians - b.m_radians;

      return result;
    }

    /// <summary>
    /// Multiplies two angles together.
    /// </summary>
    /// <param name="a">First angle</param>
    /// <param name="b">Second angle</param>
    /// <returns>Product of the two angles</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Angle Multiply(Angle a, Angle b)
    {
      Angle result;
      result.m_radians = a.m_radians * b.m_radians;

      return result;
    }

    /// <summary>
    /// Multiplies the angle by a scalar value.
    /// </summary>
    /// <param name="value">Angle</param>
    /// <param name="scale">Scalar</param>
    /// <returns>Multiplied angle</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Angle Multiply(Angle value, float scale)
    {
      Angle result;
      result.m_radians = value.m_radians * scale;

      return result;
    }

    /// <summary>
    /// Divides angle a by angle b.
    /// </summary>
    /// <param name="a">First angle</param>
    /// <param name="b">Divisor angle</param>
    /// <returns>Quotient of the two angles</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Angle Divide(Angle a, Angle b)
    {
      Angle result;
      result.m_radians = a.m_radians / b.m_radians;

      return result;
    }

    /// <summary>
    /// Divides an angle by a divisor.
    /// </summary>
    /// <param name="value">Angle</param>
    /// <param name="divisor">Divisor</param>
    /// <returns>Divided angle</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Angle Divide(Angle value, float divisor)
    {
      Angle result;
      result.m_radians = value.m_radians / divisor;

      return result;
    }

    /// <summary>
    /// Constructs a new <see cref="Angle"/> from an angle in degrees.
    /// </summary>
    /// <param name="angleInDegrees">Angle in degrees</param>
    /// <returns>The angle</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Angle FromDegrees(float angleInDegrees)
    {
      Angle result;
      result.m_radians = MathHelper.DegreesToRadians * angleInDegrees;

      return result;
    }

    /// <summary>
    /// Constructs a new <see cref="Angle"/> from an angle in radians.
    /// </summary>
    /// <param name="angleInRadians">Angle in radians</param>
    /// <returns>The angle</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Angle FromRadians(float angleInRadians)
    {
      Angle result;
      result.m_radians = angleInRadians;

      return result;
    }

    /// <summary>
    /// Tests if the angle is within the specified sweep.
    /// </summary>
    /// <param name="value">Source angle</param>
    /// <param name="startAngle">Starting angle of the sweep.</param>
    /// <param name="sweepAngle">Sweep angle</param>
    /// <returns>True if the angle is within the sweep, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool IsInSweep(Angle value, Angle startAngle, Angle sweepAngle)
    {
      float diffAngle = value.m_radians - startAngle.m_radians;
      float sweepRads = sweepAngle.m_radians;

      if (diffAngle < 0.0)
      {
        diffAngle = -diffAngle;
        sweepRads = -sweepRads;
      }

      if (diffAngle >= -MathHelper.ZeroTolerance && diffAngle <= (MathHelper.ZeroTolerance + sweepRads))
        return true;

      Angle test;
      test.m_radians = diffAngle;
      test.WrapToPositive();

      return test.m_radians <= (MathHelper.ZeroTolerance + value.m_radians) || test.m_radians >= (MathHelper.TwoPi - MathHelper.ZeroTolerance);
    }

    /// <summary>
    /// Returns the larger of the two angles.
    /// </summary>
    /// <param name="a">First angle</param>
    /// <param name="b">Second angle</param>
    /// <returns>Larger angle</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Angle Max(Angle a, Angle b)
    {
      Angle result;
      result.m_radians = (a.m_radians > b.m_radians) ? a.m_radians : b.m_radians;

      return result;
    }

    /// <summary>
    /// Returns the smaller of the two angles.
    /// </summary>
    /// <param name="a">First angle</param>
    /// <param name="b">Second angle</param>
    /// <returns>Smaller angle</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Angle Min(Angle a, Angle b)
    {
      Angle result;
      result.m_radians = (a.m_radians < b.m_radians) ? a.m_radians : b.m_radians;

      return result;
    }

    /// <summary>
    /// Restricts the source angle in the range of the minimum and maximum angles.
    /// </summary>
    /// <param name="value">Source angle</param>
    /// <param name="min">Minimum angle</param>
    /// <param name="max">Maximum angle</param>
    /// <returns>Clamped angle</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Angle Clamp(Angle value, Angle min, Angle max)
    {
      float rads = value.m_radians;

      rads = (rads > max.m_radians) ? max.m_radians : rads;
      rads = (rads < min.m_radians) ? min.m_radians : rads;

      Angle result;
      result.m_radians = rads;

      return result;
    }

    /// <summary>
    /// Flips the sign of the angle.
    /// </summary>
    /// <param name="value">Source angle</param>
    /// <returns>Negated angle</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Angle Negate(Angle value)
    {
      Angle result;
      result.m_radians = -value.m_radians;

      return result;
    }

    /// <summary>
    /// Wraps the angle to be in the range of [0, 2π).
    /// </summary>
    /// <param name="value">Source angle</param>
    /// <returns>Angle in [0, 2π) range</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Angle WrapToPositive(Angle value)
    {
      Angle result;
      result.m_radians = value.m_radians % MathHelper.TwoPi;

      if (result.m_radians < 0.0f)
        result.m_radians += MathHelper.TwoPi;

      return result;
    }

    /// <summary>
    /// Wraps the angle to be in the range of [π, -π], hence about zero.
    /// </summary>
    /// <param name="value">Source angle</param>
    /// <returns>Angle in [π, -π] range</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Angle WrapAroundZero(Angle value)
    {
      Angle result;
      result.m_radians = MathHelper.WrapAngle(value.m_radians);

      return result;
    }

    /// <summary>
    /// Linearly interpolates from one angle to another but will wrap around 360 degrees.
    /// </summary>
    /// <param name="fromAngle">Starting angle.</param>
    /// <param name="toAngle">Ending angle.</param>
    /// <param name="percent">Weight from start to end, in range of [0, 1].</param>
    /// <returns>Linearly interpolated angle between the start and end angles.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Angle Lerp(Angle fromAngle, Angle toAngle, float percent)
    {
      float diff = (toAngle.Radians - fromAngle.Radians) % MathHelper.TwoPi;
      float dist = ((2.0f * diff) % MathHelper.TwoPi) - diff;
      return Angle.FromRadians(fromAngle.Radians + dist * percent);
    }


    /// <summary>
    /// Performs an explicit cast from a number to an angle.
    /// </summary>
    /// <param name="value">Value, in radians</param>
    /// <returns>Value as an angle</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static explicit operator Angle(double value)
    {
      Angle result;
      result.m_radians = (float)value;

      return result;
    }

    /// <summary>
    /// Performs an explicit cast from an angle to a number.
    /// </summary>
    /// <param name="value">Angle value</param>
    /// <returns>Value a double number, in radians</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static explicit operator double(Angle value)
    {
      return (double)value.m_radians;
    }

    /// <summary>
    /// Performs an explicit cast from a number to an angle.
    /// </summary>
    /// <param name="value">Value, in radians</param>
    /// <returns>Value as an angle</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static explicit operator Angle(float value)
    {
      Angle result;
      result.m_radians = value;

      return result;
    }

    /// <summary>
    /// Performs an explicit cast from an angle to a number.
    /// </summary>
    /// <param name="value">Angle value</param>
    /// <returns>Value as a floating number, in radians.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static explicit operator float(Angle value)
    {
      return value.m_radians;
    }

    /// <summary>
    /// Adds two angles together.
    /// </summary>
    /// <param name="a">First angle</param>
    /// <param name="b">Second angle</param>
    /// <returns>Sum of the two angles</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Angle operator +(Angle a, Angle b)
    {
      Angle result;
      result.m_radians = a.m_radians + b.m_radians;

      return result;
    }

    /// <summary>
    /// Subtracts angle b from angle a.
    /// </summary>
    /// <param name="a">First angle</param>
    /// <param name="b">Second angle</param>
    /// <returns>Difference of the two angles</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Angle operator -(Angle a, Angle b)
    {
      Angle result;
      result.m_radians = a.m_radians - b.m_radians;

      return result;
    }

    /// <summary>
    /// Multiplies two angles together.
    /// </summary>
    /// <param name="a">First angle</param>
    /// <param name="b">Second angle</param>
    /// <returns>Product of the two angles</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Angle operator *(Angle a, Angle b)
    {
      Angle result;
      result.m_radians = a.m_radians * b.m_radians;

      return result;
    }

    /// <summary>
    /// Multiplies the angle by a scalar.
    /// </summary>
    /// <param name="value">Angle</param>
    /// <param name="scale">Scalar</param>
    /// <returns>Multiplied angle</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Angle operator *(Angle value, float scale)
    {
      Angle result;
      result.m_radians = value.m_radians * scale;

      return result;
    }

    /// <summary>
    /// Multiplies the angle by a scalar.
    /// </summary>
    /// <param name="scale">Scalar</param>
    /// <param name="value">Angle</param>
    /// <returns>Multiplied angle</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Angle operator *(float scale, Angle value)
    {
      Angle result;
      result.m_radians = value.m_radians * scale;

      return result;
    }

    /// <summary>
    /// Divides angle a by angle b.
    /// </summary>
    /// <param name="a">First angle</param>
    /// <param name="b">Divisor angle</param>
    /// <returns>Quotient of the two angles</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Angle operator /(Angle a, Angle b)
    {
      Angle result;
      result.m_radians = a.m_radians / b.m_radians;

      return result;
    }

    /// <summary>
    /// Divides angle by a divisor.
    /// </summary>
    /// <param name="value">Angle</param>
    /// <param name="divisor">Divisor</param>
    /// <returns>Divided angle</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Angle operator /(Angle value, float divisor)
    {
      Angle result;
      result.m_radians = value.m_radians / divisor;

      return result;
    }

    /// <summary>
    /// Flips the sign of the angle.
    /// </summary>
    /// <param name="value">Source angle</param>
    /// <returns>Negated angle</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Angle operator -(Angle value)
    {
      Angle result;
      result.m_radians = -value.m_radians;

      return result;
    }

    /// <summary>
    /// Tests if angle a is greater than b.
    /// </summary>
    /// <param name="a">First angle</param>
    /// <param name="b">Second angle</param>
    /// <returns>True if a is greater than b, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator >(Angle a, Angle b)
    {
      return a.m_radians > b.m_radians;
    }

    /// <summary>
    /// Tests if angle a is less than b.
    /// </summary>
    /// <param name="a">First angle</param>
    /// <param name="b">Second angle</param>
    /// <returns>True if a is less than b, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator <(Angle a, Angle b)
    {
      return a.m_radians < b.m_radians;
    }

    /// <summary>
    /// Tests if angle a is greater than or equal to b.
    /// </summary>
    /// <param name="a">First angle</param>
    /// <param name="b">Second angle</param>
    /// <returns>True if a is greater than or equal to b, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator >=(Angle a, Angle b)
    {
      return a.m_radians >= b.m_radians;
    }

    /// <summary>
    /// Tests if angle a is less than or equal to b.
    /// </summary>
    /// <param name="a">First angle</param>
    /// <param name="b">Second angle</param>
    /// <returns>True if a is less than or equal to b, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator <=(Angle a, Angle b)
    {
      return a.m_radians <= b.m_radians;
    }

    /// <summary>
    /// Tests equality between two angles.
    /// </summary>
    /// <param name="a">First angle</param>
    /// <param name="b">Second angle</param>
    /// <returns>True if both angles are equal, false otherwise</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator ==(Angle a, Angle b)
    {
      return MathHelper.IsEqual(a.m_radians, b.m_radians);
    }

    /// <summary>
    /// Tests inequality between two angles.
    /// </summary>
    /// <param name="a">First angle</param>
    /// <param name="b">Second angle</param>
    /// <returns>True if both angles are not equal, false otherwise</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator !=(Angle a, Angle b)
    {
      return !MathHelper.IsEqual(a.m_radians, b.m_radians);
    }

    /// <summary>
    /// Tests if the angle is within the specified sweep.
    /// </summary>
    /// <param name="startAngle">Starting angle of the sweep.</param>
    /// <param name="sweepAngle">Sweep angle</param>
    /// <returns>True if the angle is within the sweep, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool IsInSweep(Angle startAngle, Angle sweepAngle)
    {
      return Angle.IsInSweep(this, startAngle, sweepAngle);
    }

    /// <summary>
    /// Flips the sign of the angle.
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void Negate()
    {
      m_radians = -m_radians;
    }

    /// <summary>
    /// Wraps the angle to be in the range of [0, 2π).
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void WrapToPositive()
    {
      m_radians = m_radians % MathHelper.TwoPi;

      if (m_radians < 0.0f)
        m_radians += MathHelper.TwoPi;
    }

    /// <summary>
    /// Wraps the angle to be in the range of [π, -π], hence about zero.
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void WrapAroundZero()
    {
      m_radians = MathHelper.WrapAngle(m_radians);
    }

    /// <summary>
    /// Tests equality between the angle and another angle.
    /// </summary>
    /// <param name="other">Angle to test against</param>
    /// <returns>True if angles are equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    readonly bool IEquatable<Angle>.Equals(Angle other)
    {
      return MathHelper.IsEqual(other.m_radians, m_radians);
    }

    /// <summary>
    /// Tests equality between the angle and another angle.
    /// </summary>
    /// <param name="other">Angle to test against</param>
    /// <returns>True if angles are equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Equals(in Angle other)
    {
      return MathHelper.IsEqual(other.m_radians, m_radians);
    }

    /// <summary>
    /// Tests equality between the angle and another angle.
    /// </summary>
    /// <param name="other">Angle to test against</param>
    /// <param name="tolerance">Tolerance</param>
    /// <returns>True if angles are equal within tolerance, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Equals(Angle other, float tolerance)
    {
      return MathHelper.IsEqual(other.m_radians, m_radians, tolerance);
    }

    /// <summary>
    /// Determines whether the specified <see cref="System.Object" /> is equal to this instance.
    /// </summary>
    /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
    /// <returns>True if the specified <see cref="System.Object" /> is equal to this instance; otherwise, false.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is Angle)
        return MathHelper.IsEqual(((Angle)obj).m_radians, m_radians);

      return false;
    }

    /// <summary>
    /// Compares the current object with another object of the same type.
    /// </summary>
    /// <param name="other">An object to compare with this object.</param>
    /// <returns>A 32-bit signed integer that indicates the relative order of the objects being compared. The return value has the following meanings: Value Meaning Less than zero 
    /// This object is less than the <paramref name="other" /> parameter.Zero This object is equal to <paramref name="other" />. 
    /// Greater than zero This object is greater than <paramref name="other" />.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly int CompareTo(Angle other)
    {
      if (m_radians > other.m_radians)
        return 1;

      if (m_radians < other.m_radians)
        return -1;

      return 0;
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
    public readonly override int GetHashCode()
    {
      unchecked
      {
        return m_radians.GetHashCode();
      }
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly override string ToString()
    {
      CultureInfo info = CultureInfo.CurrentCulture;
      return String.Format(info, "Radians: {0} Degrees: {1}",
          new Object[] { Radians.ToString(), Degrees.ToString() });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="format">The format</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(string? format)
    {
      if (format is null)
        return ToString();

      CultureInfo info = CultureInfo.CurrentCulture;
      return String.Format(info, "Radians: {0} Degrees: {1}",
          new Object[] { Radians.ToString(format), Degrees.ToString(format) });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="formatProvider">The format provider.</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(IFormatProvider? formatProvider)
    {
      if (formatProvider is null)
        return ToString();

      return String.Format(formatProvider, "Radians: {0} Degrees: {1}",
          new Object[] { Radians.ToString(formatProvider), Degrees.ToString(formatProvider) });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="format">The format.</param>
    /// <param name="formatProvider">The format provider.</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(string? format, IFormatProvider? formatProvider)
    {
      if (formatProvider is null)
        return ToString();

      if (format is null)
        return ToString(formatProvider);

      return String.Format(formatProvider, "Radians: {0} Degrees: {1}",
          new Object[] { Radians.ToString(format, formatProvider), Degrees.ToString(format, formatProvider) });
    }

    /// <summary>
    /// Writes the primitive data to the output.
    /// </summary>
    /// <param name="output">Primitive writer.</param>
    readonly void IPrimitiveValue.Write(IPrimitiveWriter output)
    {
      output.Write("Radians", m_radians);
    }

    /// <summary>
    /// Reads the primitive data from the input.
    /// </summary>
    /// <param name="input">Primitive reader.</param>
    void IPrimitiveValue.Read(IPrimitiveReader input)
    {
      m_radians = input.ReadSingle("Radians");
    }
  }
}
