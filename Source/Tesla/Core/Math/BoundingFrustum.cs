﻿/*
* Copyright (c) 2010-2024 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using Tesla.Content;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Represents an orthogonal view frustum. The frustum is defined by six planes (left, right, top, bottom, near, and far in that order) and looks
  /// like a pyramid with its top sliced off. A frustum is generally represents the viewable volume of a camera system and is used in culling and other
  /// intersection tests to query what is visible or not in a scene.
  /// </summary>
  [SavableVersion(1)]
  public sealed class BoundingFrustum : BoundingVolume
  {
    //Made nullable because to calculate we're using the corners, which can be expensive to calculate...so only calculate when
    //needed and cache
    private Vector3? m_center;
    private Matrix m_viewProjMatrix;
    private Plane[] m_planes;

    /// <summary>
    /// Left frustum plane.
    /// </summary>
    public ref readonly Plane Left { get { return ref m_planes[0]; } }

    /// <summary>
    /// Right frustum plane.
    /// </summary>
    public ref readonly Plane Right { get { return ref m_planes[1]; } }

    /// <summary>
    /// Top frustum plane.
    /// </summary>
    public ref readonly Plane Top { get { return ref m_planes[2]; } }

    /// <summary>
    /// Bottom frustum plane.
    /// </summary>
    public ref readonly Plane Bottom { get { return ref m_planes[3]; } }

    /// <summary>
    /// Near frustum plane.
    /// </summary>
    public ref readonly Plane Near { get { return ref m_planes[4]; } }

    /// <summary>
    /// Far frustum plane.
    /// </summary>
    public ref readonly Plane Far { get { return ref m_planes[5]; } }

    /// <summary>
    /// Gets the number of frustum planes.
    /// </summary>
    public int PlaneCount { get { return 6; } }

    /// <summary>
    /// Gets an individual frustum plane.
    /// </summary>
    /// <param name="plane">Plane to retrieve.</param>
    /// <returns>Specified frustum plane.</returns>
    public ref readonly Plane this[FrustumPlane plane] { get { return ref m_planes[(int)plane]; } }

    /// <summary>
    /// Gets or sets the view-projection matrix that represents this bounding frustm.
    /// </summary>
    public Matrix ViewProjectionMatrix
    {
      get
      {
        return m_viewProjMatrix;
      }
      set
      {
        Set(value);
      }
    }

    /// <summary>
    /// Gets or sets the center of the bounding volume.
    /// </summary>
    public override Vector3 Center
    {
      get
      {
        if (!m_center.HasValue || UpdateCorners)
          ComputeCenter();

        return m_center.Value;
      }
      set
      {
        Vector3 center = Center; //Force a calculation

        if (center.Equals(value))
          return;

        //Determine delta translation
        Vector3 delta;
        Vector3.Subtract(value, center, out delta);

        //Transform
        Vector3 scale = Vector3.One;
        Quaternion rot = Quaternion.Identity;
        Transform(scale, rot, delta);

        m_center = null; //Defer calculations until needed
      }
    }

    /// <summary>
    /// Gets the volume of the bounding volume.
    /// </summary>
    public override float Volume { get { return ComputeVolume(Vector3.Distance(m_planes[4].Origin, m_planes[5].Origin), Corners); } }

    /// <summary>
    /// Gets the bounding type.
    /// </summary>
    public override BoundingType BoundingType { get { return BoundingType.Frustum; } }

    /// <summary>
    /// Constructs a new instance of the <see cref="BoundingFrustum"/> class.
    /// </summary>
    public BoundingFrustum()
    {
      m_center = null; //Defer calculations
      m_viewProjMatrix = Matrix.Identity;
      m_planes = new Plane[PlaneCount];
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="BoundingFrustum"/> class.
    /// </summary>
    /// <param name="viewProjMatrix">View-Projection matrix to construct the frustum from.</param>
    public BoundingFrustum(in Matrix viewProjMatrix)
    {
      m_center = null; //Defer calculations
      m_viewProjMatrix = Matrix.Identity;
      m_planes = new Plane[PlaneCount];
      Set(viewProjMatrix);
    }

    /// <summary>
    /// Gets the specified frustum plane.
    /// </summary>
    /// <param name="plane">Type of frustum plane to get.</param>
    /// <param name="result">Frustum plane.</param>
    public void GetPlane(FrustumPlane plane, out Plane result)
    {
      System.Diagnostics.Debug.Assert((int)plane < m_planes.Length);

      result = m_planes[(int)plane];
    }

    /// <summary>
    /// Gets the origin and normal of the plane so that it is aligned to the constrained side of the frustum. This is mostly for
    /// convienence for visualization purposes.
    /// </summary>
    /// <param name="plane">Type of frustum plane to get.</param>
    /// <param name="origin">Origin of the plane on the frustum.</param>
    /// <param name="normal">Normal of the plane.</param>
    public void GetPlaneOriginOnFrustum(FrustumPlane plane, out Vector3 origin, out Vector3 normal)
    {
      System.Diagnostics.Debug.Assert((int)plane < m_planes.Length);

      origin = Vector3.Zero;
      normal = Vector3.Zero;

      ReadOnlySpan<Vector3> corners = Corners;

      Plane p = m_planes[(int)plane];
      normal = p.Normal;

      //Calculate center of the side using corner points
      Vector3 pt0 = Vector3.Zero;
      Vector3 pt1 = Vector3.Zero;
      Vector3 pt2 = Vector3.Zero;
      Vector3 pt3 = Vector3.Zero;

      switch (plane)
      {
        case FrustumPlane.Near:
          pt0 = corners[0]; pt1 = corners[1]; pt2 = corners[2]; pt3 = corners[3];
          break;
        case FrustumPlane.Far:
          pt0 = corners[4]; pt1 = corners[5]; pt2 = corners[6]; pt3 = corners[7];
          break;
        case FrustumPlane.Left:
          pt0 = corners[7]; pt1 = corners[6]; pt2 = corners[1]; pt3 = corners[0];
          break;
        case FrustumPlane.Right:
          pt0 = corners[3]; pt1 = corners[2]; pt2 = corners[5]; pt3 = corners[4];
          break;
        case FrustumPlane.Top:
          pt0 = corners[1]; pt1 = corners[6]; pt2 = corners[5]; pt3 = corners[2];
          break;
        case FrustumPlane.Bottom:
          pt0 = corners[4]; pt1 = corners[7]; pt2 = corners[0]; pt3 = corners[3];
          break;
      }

      //Average the 4 points
      Vector3.Add(pt0, pt1, out origin);
      Vector3.Add(origin, pt2, out origin);
      Vector3.Add(origin, pt3, out origin);
      Vector3.Multiply(origin, 0.25f, out origin);
    }

    /// <summary>
    /// Sets the bounding frustum from the specified view-projection matrix.
    /// </summary>
    /// <param name="viewProjMatrix">View-Projection matrix to construct the frustum from.</param>
    public void Set(in Matrix viewProjMatrix)
    {
      m_viewProjMatrix = viewProjMatrix;

      ExtractPlanes(viewProjMatrix, m_planes);

      UpdateCorners = true;
    }

    /// <summary>
    /// Sets the bounding volume by either copying the specified bounding volume if its the specified type. For bounding frustums, computing a new frustum to fit the volume
    /// source is not supported.
    /// </summary>
    /// <param name="volume">Bounding volume to copy from</param>
    public override void Set(BoundingVolume? volume)
    {
      if (volume is BoundingFrustum bf)
        Set(bf.m_viewProjMatrix);
      else
        Debug.Assert(false, "Cannot set Bounding Frustum from another volume source!");
    }

    /// <summary>
    /// Creates a copy of the bounding volume and returns a new instance.
    /// </summary>
    /// <returns>Copied bounding volume</returns>
    public override BoundingVolume Clone()
    {
      return new BoundingFrustum(m_viewProjMatrix);
    }

    /// <summary>
    /// Computes the distance from a given point to the edge of the volume.
    /// </summary>
    /// <param name="point">Point in space</param>
    /// <returns>Distance from the point to the edge of the volume.</returns>
    public override float DistanceTo(in Vector3 point)
    {
      float minDist = float.MaxValue;
      Vector3 pointOnVolume;

      for (int i = 0; i < 6; i++)
      {
        ref readonly Plane p = ref m_planes[i];
        p.ClosestPointTo(point, out pointOnVolume);

        if (Contains(pointOnVolume) == ContainmentType.Outside)
          continue;

        float dist = Vector3.DistanceSquared(point, pointOnVolume);

        if (dist < minDist)
          minDist = dist;
      }

      return MathF.Sqrt(minDist);
    }

    /// <summary>
    /// Computes the distance squared from a given point to the edge of the volume.
    /// </summary>
    /// <param name="point">Point in space</param>
    /// <returns>Distanced squared from the point to the edge of the volume.</returns>
    public override float DistanceSquaredTo(in Vector3 point)
    {
      float minDist = float.MaxValue;
      Vector3 pointOnVolume;

      for (int i = 0; i < 6; i++)
      {
        ref readonly Plane p = ref m_planes[i];
        p.ClosestPointTo(point, out pointOnVolume);

        if (Contains(pointOnVolume) == ContainmentType.Outside)
          continue;

        float dist = Vector3.DistanceSquared(point, pointOnVolume);

        if (dist < minDist)
          minDist = dist;
      }

      return minDist;
    }

    /// <summary>
    /// Computes the closest point on the volume from the given point in space.
    /// </summary>
    /// <param name="point">Point in space</param>
    /// <param name="pointOnVolume">Closest point on the edge of the volume.</param>
    public override void ClosestPointTo(in Vector3 point, out Vector3 pointOnVolume)
    {
      float minDist = float.MaxValue;
      Vector3 closestPt = new Vector3();

      for (int i = 0; i < 6; i++)
      {
        ref readonly Plane p = ref m_planes[i];
        p.ClosestPointTo(point, out pointOnVolume);

        if (Contains(pointOnVolume) == ContainmentType.Outside)
          continue;

        float dist = Vector3.DistanceSquared(point, pointOnVolume);

        if (dist < minDist)
        {
          minDist = dist;
          closestPt = pointOnVolume;
        }
      }

      pointOnVolume = closestPt;
    }

    /// <summary>
    /// NOT SUPPORTED.
    /// </summary>
    /// <param name="points">Points in space</param>
    /// <param name="subMeshRange">Optional range inside the buffer that represents the mesh to compute bounds for. Note: the base vertex offset will be ignored.</param>
    public override void ComputeFromPoints(ReadOnlySpan<Vector3> points, in SubMeshRange? subMeshRange = null)
    {
      Debug.Assert(false, "Cannot compute a BoundingFrustum from points!");
    }

    /// <summary>
    /// NOT SUPPORTED.
    /// </summary>
    /// <param name="points">Points in space.</param>
    /// <param name="indices">Point indices denoting location in the point buffer.</param>
    /// <param name="subMeshRange">Optional range inside the buffer that represents the mesh to compute bounds for.</param>
    public override void ComputeFromIndexedPoints(ReadOnlySpan<Vector3> points, IndexData indices, in SubMeshRange? subMeshRange = null)
    {
      Debug.Assert(false, "Cannot compute a BoundingFrustum from points!");
    }

    /// <summary>
    /// Determines if the specified point is contained inside the bounding volume.
    /// </summary>
    /// <param name="point">Point to test against.</param>
    /// <returns>Type of containment</returns>
    public override ContainmentType Contains(in Vector3 point)
    {
      PlaneIntersectionType result = PlaneIntersectionType.Front;

      for (int i = 0; i < 6; i++)
      {
        ref readonly Plane p = ref m_planes[i];

        switch (p.WhichSide(point))
        {
          case PlaneIntersectionType.Back:
            return ContainmentType.Outside; //If on negative side, then it's outside (planes pointing inward). Don't need to check the rest.
          case PlaneIntersectionType.Intersects:
            result = PlaneIntersectionType.Intersects; //If intersecting, it may still be outside the frustum so we need to continue checking the rest
            break;
        }
      }

      return (result == PlaneIntersectionType.Intersects) ? ContainmentType.Intersects : ContainmentType.Inside;
    }

    /// <summary>
    /// Determines if the specified segment line is contained inside the bounding volume.
    /// </summary>
    /// <param name="line">Segment to test against.</param>
    /// <returns>Type of containment.</returns>
    public override ContainmentType Contains(in Segment line)
    {
      ContainmentType startType = Contains(line.StartPoint);
      ContainmentType endType = Contains(line.EndPoint);

      if (startType == ContainmentType.Outside && endType == ContainmentType.Outside)
        return ContainmentType.Outside;

      if (startType == ContainmentType.Inside && endType == ContainmentType.Inside)
        return ContainmentType.Inside;

      return ContainmentType.Intersects;
    }

    /// <summary>
    /// Determines if the specified triangle is contained inside the bounding volume.
    /// </summary>
    /// <param name="triangle">Triangle to test against.</param>
    /// <returns>Type of containment.</returns>
    public override ContainmentType Contains(in Triangle triangle)
    {
      ContainmentType aType = Contains(triangle.PointA);
      ContainmentType bType = Contains(triangle.PointB);
      ContainmentType cType = Contains(triangle.PointC);

      //Triangle is completely contained inside
      if (aType == ContainmentType.Inside && bType == ContainmentType.Inside && cType == ContainmentType.Inside)
        return ContainmentType.Inside;

      //Still need to check each edge (line segment) to see if it intersects, we can have all three vertices outside and the triangle may still intersect
      Segment edgeBA = new Segment(triangle.PointB, triangle.PointA);
      Segment edgeBC = new Segment(triangle.PointB, triangle.PointC);
      Segment edgeCA = new Segment(triangle.PointC, triangle.PointA);

      if (Intersects(edgeBA) || Intersects(edgeBC) || Intersects(edgeCA))
        return ContainmentType.Intersects;

      return ContainmentType.Outside;
    }

    /// <summary>
    /// Determines if the specified ellipse intersects with the bounding volume.
    /// </summary>
    /// <param name="ellipse">Ellipse to test against.</param>
    /// <returns>True if the bounding volume intersects with the ellipse, false otherwise.</returns>
    public override ContainmentType Contains(in Ellipse ellipse)
    {
      //TODO
      throw new NotImplementedException();
    }

    /// <summary>
    /// Determines if the specified bounding volume is contained inside the bounding volume.
    /// </summary>
    /// <param name="volume">Bounding volume to test against.</param>
    /// <returns>Type of containment.</returns>
    public override ContainmentType Contains(BoundingVolume? volume)
    {
      if (volume is null)
        return ContainmentType.Outside;

      switch (volume.BoundingType)
      {
        case BoundingType.AxisAlignedBoundingBox:
          {
            BoundingBox other = (volume as BoundingBox)!;
            Vector3 center = other.Center;
            Vector3 extents = other.Extents;

            return GeometricToolsHelper.FrustumContainsAABB(this, center, extents);
          }
        case BoundingType.Sphere:
        case BoundingType.Capsule:
        case BoundingType.OrientedBoundingBox:
          {
            bool intersects = false;
            for (int i = 0; i < 6; i++)
            {
              ref readonly Plane p = ref m_planes[i];
              switch (volume.Intersects(p))
              {
                case PlaneIntersectionType.Back:
                  return ContainmentType.Outside;
                case PlaneIntersectionType.Intersects:
                  intersects = true;
                  break;
              }
            }

            if (intersects)
              return ContainmentType.Intersects;

            return ContainmentType.Inside;
          }
        case BoundingType.Frustum:
        case BoundingType.Mesh:
        default:
          {
            return Contains(volume.Corners);
          }
      }
    }

    /// <summary>
    /// Tests if the specified ray intersects with the bounding volume.
    /// </summary>
    /// <param name="ray">Ray to test against.</param>
    /// <returns>True if the bounding volume intersects with the ray, false otherwise.</returns>
    public override bool Intersects(in Ray ray)
    {
      if (Contains(ray.Origin) == ContainmentType.Inside)
        return true;

      for (int i = 0; i < 6; i++)
      {
        ref readonly Plane p = ref m_planes[i];

        LineIntersectionResult temp;
        if (p.Intersects(ray, out temp))
        {
          Vector3 pt = temp.Point;
          if (Contains(pt) != ContainmentType.Outside)
            return true;
        }
      }

      return false;
    }

    /// <summary>
    /// Determines if the specified ray intersects with the bounding volume.
    /// </summary>
    /// <param name="ray">Ray to test against.</param>
    /// <param name="result">Bounding intersection result.</param>
    /// <returns>True if the bounding volume intersects with the ray, false otherwise.</returns>
    public override bool Intersects(in Ray ray, out BoundingIntersectionResult result)
    {
      LineIntersectionResult? first = null;
      LineIntersectionResult? second = null;

      for (int i = 0; i < 6; i++)
      {
        ref readonly Plane p = ref m_planes[i];

        LineIntersectionResult temp;
        if (p.Intersects(ray, out temp))
        {
          Vector3 pt = temp.Point;
          if (first is null)
          {
            if (Contains(pt) != ContainmentType.Outside)
              first = temp;

            continue;
          }

          if (second is null && Contains(pt) != ContainmentType.Outside)
          {
            second = temp;
            break;
          }
        }
      }

      BoundingIntersectionResult.FromResults(first, second, out result);

      return result.IntersectionCount > 0;
    }

    /// <summary>
    /// Determines if the specified segment line intersects with the bounding volume.
    /// </summary>
    /// <param name="line">Segment line to test against.</param>
    /// <returns>True if the bounding volume intersects with the segment line, false otherweise.</returns>
    public override bool Intersects(in Segment line)
    {
      for (int i = 0; i < 6; i++)
      {
        ref readonly Plane p = ref m_planes[i];

        LineIntersectionResult temp;
        if (p.Intersects(line, out temp))
        {
          Vector3 pt = temp.Point;
          if (Contains(pt) != ContainmentType.Outside)
            return true;
        }
      }

      return false;
    }

    /// <summary>
    /// Determines if the specified segment line intersects with the bounding volume.
    /// </summary>
    /// <param name="line">Segment line to test against.</param>
    /// <param name="result">Bounding intersection result.</param>
    /// <returns>True if the bounding volume intersects with the segment line, false otherweise.</returns>
    public override bool Intersects(in Segment line, out BoundingIntersectionResult result)
    {
      LineIntersectionResult? first = null;
      LineIntersectionResult? second = null;

      for (int i = 0; i < 6; i++)
      {
        ref readonly Plane p = ref m_planes[i];

        LineIntersectionResult temp;
        if (p.Intersects(line, out temp))
        {
          Vector3 pt = temp.Point;
          if (first is null)
          {
            if (Contains(pt) != ContainmentType.Outside)
              first = temp;

            continue;
          }

          if (second is null && Contains(pt) != ContainmentType.Outside)
          {
            second = temp;
            break;
          }
        }
      }

      BoundingIntersectionResult.FromResults(first, second, out result);

      return result.IntersectionCount > 0;
    }

    /// <summary>
    /// Tests if the specified plane intersects with the bounding volume.
    /// </summary>
    /// <param name="plane">Plane to test against.</param>
    /// <returns>Type of plane intersection.</returns>
    public override PlaneIntersectionType Intersects(in Plane plane)
    {
      ReadOnlySpan<Vector3> corners = Corners;

      PlaneIntersectionType interType = plane.WhichSide(corners[0]);

      for (int i = 1; i < 6; i++)
      {
        ref readonly Vector3 pt = ref corners[i];

        if (plane.WhichSide(pt) != interType)
          return PlaneIntersectionType.Intersects;
      }

      return interType;
    }

    /// <summary>
    /// Determines if the specified bounding volume intersects with the bounding volume.
    /// </summary>
    /// <param name="volume">Other bounding volume to test against.</param>
    /// <returns>True if the two volumes intersect with one another, false otherwise.</returns>
    public override bool Intersects(BoundingVolume? volume)
    {
      if (volume is null)
        return false;

      switch (volume.BoundingType)
      {
        case BoundingType.AxisAlignedBoundingBox:
          {
            BoundingBox other = (volume as BoundingBox)!;
            Vector3 center = other.Center;
            Vector3 extents = other.Extents;

            ContainmentType contains = GeometricToolsHelper.FrustumContainsAABB(this, center, extents);
            return contains != ContainmentType.Outside;
          }
        case BoundingType.Sphere:
        case BoundingType.Capsule:
        case BoundingType.OrientedBoundingBox:
          {
            for (int i = 0; i < 6; i++)
            {
              Plane p = m_planes[i];
              if (volume.Intersects(p) == PlaneIntersectionType.Back)
                return false;
            }

            return true;
          }
        case BoundingType.Frustum:
        case BoundingType.Mesh:
        default:
          {
            return IntersectsGeneral(volume);
          }
      }
    }

    /// <summary>
    /// NOT SUPPORTED.
    /// </summary>
    /// <param name="volume">Bounding volume to merge with.</param>
    public override void Merge(BoundingVolume? volume)
    {
      Debug.Assert(false, "Cannot merge a BoundingFrustum with other bounding volumes!");
    }

    /// <summary>
    /// Transforms the bounding volume by a Scale-Rotation-Translation (SRT) transform.
    /// </summary>
    /// <param name="scale">Scaling</param>
    /// <param name="rotation">Rotation</param>
    /// <param name="translation">Translation</param>
    public override void Transform(in Vector3 scale, in Quaternion rotation, in Vector3 translation)
    {
      //Is this kosher??
      Matrix transform;
      Matrix.CreateTransformationMatrix(scale.X, scale.Y, scale.Z, rotation, translation, out transform);

      Matrix.Multiply(transform, m_viewProjMatrix, out m_viewProjMatrix);
      Set(m_viewProjMatrix);
    }

    /// <summary>
    /// Tests equality between the bounding volume and the other bounding volume.
    /// </summary>
    /// <param name="other">Other bounding volume</param>
    /// <param name="tolerance">Tolerance</param>
    /// <returns>True if the volume is of same type and same size/shape, false otherwise.</returns>
    public override bool Equals([NotNullWhen(true)] BoundingVolume? other, float tolerance)
    {
      BoundingFrustum? bf = other as BoundingFrustum;

      if (bf is not null)
        return m_viewProjMatrix.Equals(bf.m_viewProjMatrix, tolerance); //If ViewProjMatrices equal, then rest are

      return false;
    }

    /// <summary>
    /// Reads the object data from the input.
    /// </summary>
    /// <param name="input">Savable reader</param>
    public override void Read(ISavableReader input)
    {
      m_center = input.Read<Vector3>("Center"); //When written, should have been calculated
      input.Read<Matrix>("ViewProjectionMatrix", out m_viewProjMatrix);
      input.Read<Plane>("Left", out m_planes[0]);
      input.Read<Plane>("Right", out m_planes[1]);
      input.Read<Plane>("Top", out m_planes[2]);
      input.Read<Plane>("Bottom", out m_planes[3]);
      input.Read<Plane>("Near", out m_planes[4]);
      input.Read<Plane>("Far", out m_planes[5]);

      UpdateCorners = true;
    }

    /// <summary>
    /// Writes the object data to the output.
    /// </summary>
    /// <param name="output">Savable writer</param>
    public override void Write(ISavableWriter output)
    {
      output.Write<Vector3>("Center", Center); //Force a calculation
      output.Write<Matrix>("ViewProjectionMatrix", m_viewProjMatrix);
      output.Write<Plane>("Left", m_planes[0]);
      output.Write<Plane>("Right", m_planes[1]);
      output.Write<Plane>("Top", m_planes[2]);
      output.Write<Plane>("Bottom", m_planes[3]);
      output.Write<Plane>("Near", m_planes[4]);
      output.Write<Plane>("Far", m_planes[5]);
    }

    /// <summary>
    /// Computes the corners that represent the extremal points of this bounding volume.
    /// </summary>
    /// <param name="corners">Databuffer to contain the points, length equal to the corner count.</param>
    protected override void ComputeCorners(Span<Vector3> corners)
    {
      if (corners.Length < CornerCount)
        throw new ArgumentOutOfRangeException(StringLocalizer.Instance.GetLocalizedString("NotEnoughSpaceForBoundingCorners", CornerCount.ToString()));

      //Four vertices on near plane, clockwise from lower left (along near normal)
      ref Vector3 temp = ref corners[0];
      GetPlaneIntersectionPoint(m_planes[4], m_planes[3], m_planes[0], out temp); //Near, Bottom, Left

      temp = ref corners[1];
      GetPlaneIntersectionPoint(m_planes[4], m_planes[2], m_planes[0], out temp); //Near, Top, Left

      temp = ref corners[2];
      GetPlaneIntersectionPoint(m_planes[4], m_planes[2], m_planes[1], out temp); //Near, Top, Right

      temp = ref corners[3];
      GetPlaneIntersectionPoint(m_planes[4], m_planes[3], m_planes[1], out temp); //Near, Bottom, Right

      //Four vertices on far plane, counterclockwise from lower right (along near normal)
      temp = ref corners[4];
      GetPlaneIntersectionPoint(m_planes[5], m_planes[3], m_planes[1], out temp); //Far, Bottom, Right

      temp = ref corners[5];
      GetPlaneIntersectionPoint(m_planes[5], m_planes[2], m_planes[1], out temp); //Far, Top, Right

      temp = ref corners[6];
      GetPlaneIntersectionPoint(m_planes[5], m_planes[2], m_planes[0], out temp); //Far, Top, Left

      temp = ref corners[7];
      GetPlaneIntersectionPoint(m_planes[5], m_planes[3], m_planes[0], out temp); //Far, Bottom, Left
    }

    [MemberNotNull(nameof(m_center))]
    private void ComputeCenter()
    {
      ReadOnlySpan<Vector3> corners = Corners;

      Vector3 center = Vector3.Zero;
      for (int i = 0; i < corners.Length; i++)
      {
        ref readonly Vector3 pt = ref corners[i];
        Vector3.Add(center, pt, out center);
      }

      Vector3.Multiply(center, 1.0f / (float)corners.Length, out center);

      m_center = center;
    }

    private static void ExtractPlanes(in Matrix viewProjMatrix, Plane[] planes)
    {
      Plane left = new Plane(viewProjMatrix.M14 + viewProjMatrix.M11,
                        viewProjMatrix.M24 + viewProjMatrix.M21,
                        viewProjMatrix.M34 + viewProjMatrix.M31,
                        viewProjMatrix.M44 + viewProjMatrix.M41);
      left.Normalize();
      planes[0] = left;

      Plane right = new Plane(viewProjMatrix.M14 - viewProjMatrix.M11,
                         viewProjMatrix.M24 - viewProjMatrix.M21,
                         viewProjMatrix.M34 - viewProjMatrix.M31,
                         viewProjMatrix.M44 - viewProjMatrix.M41);
      right.Normalize();
      planes[1] = right;

      Plane top = new Plane(viewProjMatrix.M14 - viewProjMatrix.M12,
                       viewProjMatrix.M24 - viewProjMatrix.M22,
                       viewProjMatrix.M34 - viewProjMatrix.M32,
                       viewProjMatrix.M44 - viewProjMatrix.M42);
      top.Normalize();
      planes[2] = top;

      Plane bottom = new Plane(viewProjMatrix.M14 + viewProjMatrix.M12,
                      viewProjMatrix.M24 + viewProjMatrix.M22,
                      viewProjMatrix.M34 + viewProjMatrix.M32,
                      viewProjMatrix.M44 + viewProjMatrix.M42);
      bottom.Normalize();
      planes[3] = bottom;

      Plane near = new Plane(viewProjMatrix.M13, viewProjMatrix.M23, viewProjMatrix.M33, viewProjMatrix.M43);
      near.Normalize();
      planes[4] = near;

      Plane far = new Plane(viewProjMatrix.M14 - viewProjMatrix.M13,
                       viewProjMatrix.M24 - viewProjMatrix.M23,
                       viewProjMatrix.M34 - viewProjMatrix.M33,
                       viewProjMatrix.M44 - viewProjMatrix.M43);
      far.Normalize();
      planes[5] = far;
    }

    private static void GetPlaneIntersectionPoint(in Plane p1, in Plane p2, in Plane p3, out Vector3 result)
    {
      Vector3 p2Xp3;
      Vector3 p3Xp1;
      Vector3 p1Xp2;

      Vector3.Cross(p2.Normal, p3.Normal, out p2Xp3);
      Vector3.Cross(p3.Normal, p1.Normal, out p3Xp1);
      Vector3.Cross(p1.Normal, p2.Normal, out p1Xp2);

      float p1Dot = Vector3.Dot(p1.Normal, p2Xp3);
      float p2Dot = Vector3.Dot(p2.Normal, p3Xp1);
      float p3Dot = Vector3.Dot(p3.Normal, p1Xp2);

      Vector3.Multiply(p2Xp3, -p1.D / p1Dot, out p2Xp3);
      Vector3.Multiply(p3Xp1, -p2.D / p2Dot, out p3Xp1);
      Vector3.Multiply(p1Xp2, -p3.D / p3Dot, out p1Xp2);

      Vector3.Add(p2Xp3, p3Xp1, out result);
      Vector3.Add(result, p1Xp2, out result);
    }

    private static float ComputeVolume(float height, ReadOnlySpan<Vector3> points)
    {
      float l = Vector3.Distance(points[0], points[1]);
      float w = Vector3.Distance(points[0], points[3]);

      float areaNear = l * w;

      l = Vector3.Distance(points[4], points[5]);
      w = Vector3.Distance(points[4], points[7]);

      float areaFar = l * w;

      //Orthographic so just a box
      if (MathHelper.IsEqual(areaNear, areaFar))
        return l * w * height;

      //Perspective so an orthogonal frustum
      return MathHelper.OneThird * height * (areaNear + areaFar + MathF.Sqrt(areaNear * areaFar));
    }
  }
}