﻿/*
* Copyright (c) 2010-2024 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System.Runtime.CompilerServices;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Generalizes arcs, circles, and ellipses geometry.
  /// </summary>
  public struct Ellipse
  {
    /// <summary>
    /// Center of the ellipse.
    /// </summary>
    public Vector3 Center;

    /// <summary>
    /// 0 degree axis.
    /// </summary>
    public Vector3 Axis0;

    /// <summary>
    /// 90 degree axis.
    /// </summary>
    public Vector3 Axis90;

    /// <summary>
    /// Radius along the 0 degree axis.
    /// </summary>
    public float Radius0;

    /// <summary>
    /// Radius along the 90 degree axis.
    /// </summary>
    public float Radius90;

    /// <summary>
    /// Start angle of arc.
    /// </summary>
    public Angle StartAngle;

    /// <summary>
    /// Sweep angle of arc.
    /// </summary>
    public Angle SweepAngle;

    private static readonly int s_sizeInBytes = BufferHelper.SizeOf<Ellipse>();
    private static readonly Ellipse s_unitCircle = new Ellipse(Vector3.Zero, Vector3.UnitX, Vector3.UnitY, 1.0f, 1.0f, Angle.Zero, Angle.TwoPi);

    /// <summary>
    /// Gets the size of the <see cref="Ellipse"/> type in bytes.
    /// </summary>
    public static int SizeInBytes
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return s_sizeInBytes;
      }
    }

    /// <summary>
    /// Gets the unit circle where both radii are one and the sweep makes a complete circle.
    /// </summary>
    public static ref readonly Ellipse UnitCircle
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_unitCircle;
      }
    }

    /// <summary>
    /// Gets the normal of the ellipse.
    /// </summary>
    public readonly Vector3 Normal
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        Vector3.NormalizedCross(Axis0, Axis90, out Vector3 normal);

        return normal;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Ellipse"/> struct.
    /// </summary>
    /// <param name="center">Center of the arc</param>
    /// <param name="axis0">0 degree axis of the arc</param>
    /// <param name="axis90">90 degree axis of the arc.</param>
    /// <param name="radius0">Radius of the ellipse along the 0 degree axis.</param>
    /// <param name="radius90">Radius of the ellipse along the 90 degree axis.</param>
    /// <param name="startAngle">Starting angle of the arc.</param>
    /// <param name="sweepAngle">Amount the arc sweeps from the starting point.</param>
    public Ellipse(Vector3 center, Vector3 axis0, Vector3 axis90, float radius0, float radius90, Angle startAngle, Angle sweepAngle)
    {
      Center = center;
      Axis0 = axis0;
      Axis90 = axis90;
      Radius0 = radius0;
      Radius90 = radius90;
      StartAngle = startAngle;
      SweepAngle = sweepAngle;
    }
  }
}
