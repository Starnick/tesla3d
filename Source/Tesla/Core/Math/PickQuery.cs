﻿/*
* Copyright (c) 2010-2024 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System.Collections;
using System.Collections.Generic;
using Tesla.Utilities;

namespace Tesla
{
  /// <summary>
  /// Query object for finding and sorting pick intersections with objects. Both bounding and primitive (usually triangle) picking is supported.
  /// </summary>
  public class PickQuery : IReadOnlyList<PickResult>
  {
    private static IComparer<PickResult> s_boundingPickDistanceComparer = new BoundingPickComparer();
    private static IComparer<PickResult> s_primitivePickDistanceComparer = new PrimitivePickComparer();

    private Ray m_ray;
    private PickingOptions m_options;
    private List<PickResult> m_results;
    private bool m_sorted;

    /// <summary>
    /// Gets a comparer that sorts based on the distance of bounding intersections. Smallest distance is the first result.
    /// </summary>
    public static IComparer<PickResult> BoundingPickDistanceComparer { get { return s_boundingPickDistanceComparer; } }

    /// <summary>
    /// Gets a comparer that sorts absed on distance of primitive intersections (and if equal, falls back to bounding intersection). Smallest distance is the first result.
    /// </summary>
    public static IComparer<PickResult> PrimitivePickDistanceComparer { get { return s_primitivePickDistanceComparer; } }

    /// <summary>
    /// Gets or sets the pick ray used in the query.
    /// </summary>
    public ref Ray PickRay
    {
      get
      {
        return ref m_ray;
      }
    }

    /// <summary>
    /// Gets or sets picking options.
    /// </summary>
    public PickingOptions Options
    {
      get
      {
        return m_options;
      }
      set
      {
        m_options = value;
      }
    }

    /// <summary>
    /// Gets the number of results in the query.
    /// </summary>
    public int Count
    {
      get
      {
        return m_results.Count;
      }
    }

    /// <summary>
    /// Gets if the query results have been sorted.
    /// </summary>
    public bool IsSorted
    {
      get
      {
        return m_sorted;
      }
    }

    /// <summary>
    /// Gets the <see cref="PickResult"/> at the specified index.
    /// </summary>
    public PickResult this[int index]
    {
      get
      {
        if (index < 0 || index >= m_results.Count)
          return null;

        return m_results[index];
      }
    }

    /// <summary>
    /// Gets the closest pick, if it exists (null may be returned).
    /// </summary>
    public PickResult ClosestPick
    {
      get
      {
        if (m_results.Count > 0)
          return m_results[0];

        return null;
      }
    }

    /// <summary>
    /// Gets the farthest pick, if it exists (null may be returned).
    /// </summary>
    public PickResult FarthestPick
    {
      get
      {
        if (m_results.Count > 0)
          return m_results[m_results.Count - 1];

        return null;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="PickQuery"/> class.
    /// </summary>
    public PickQuery()
    {
      m_results = new List<PickResult>();
      m_sorted = false;
      m_options = PickingOptions.None;
    }

    /// <summary>
    /// Tests the <see cref="IPickable"/> object against the query, if it successfully passes the intersection tests then it is added to the results.
    /// </summary>
    /// <param name="pickable">Object to pick.</param>
    /// <returns>True if the object has been picked, false if it fails the query tests.</returns>
    public bool AddPick(IPickable pickable)
    {
      if (pickable is null)
        return false;

      BoundingVolume bv = pickable.WorldBounding;
      BoundingIntersectionResult bvResult;

      //If doesn't intersect with bounding...no pick
      if (bv != null && bv.Intersects(m_ray, out bvResult))
      {
        PickResult result = Pool<PickResult>.Fetch();

        bool doPrimPick = (m_options & PickingOptions.PrimitivePick) == PickingOptions.PrimitivePick;

        //If no primitive picking just set the result and return true, otherwise
        //do primitive picking, if that succeeds set the result and return true
        if (doPrimPick)
        {
          bool ignoreBackfaces = (m_options & PickingOptions.IgnoreBackfaces) == PickingOptions.IgnoreBackfaces;

          if (pickable.IntersectsMesh(m_ray, result.GetRefList(), ignoreBackfaces))
          {
            result.Set(pickable, bvResult);
            result.Sort(); //Make sure prim picks are sorted
            m_results.Add(result);
            return true;
          }
        }
        else
        {
          result.Set(pickable, bvResult);
          m_results.Add(result);
          return true;
        }

        //If haven't returned, then return back to pool
        result.Clear();
        Pool<PickResult>.Return(result);
      }

      return false;
    }

    /// <summary>
    /// Sorts the pick results contained in the query. If primitive picking is enabled, sorting is based on pick distances of the primitives, otherwise the distances
    /// of the bounding picks will be used.
    /// </summary>
    public void Sort()
    {
      if (m_sorted)
        return;

      IComparer<PickResult> comparer = ((m_options & PickingOptions.PrimitivePick) == PickingOptions.PrimitivePick) ? s_primitivePickDistanceComparer : s_boundingPickDistanceComparer;
      m_results.Sort(comparer);
      m_sorted = true;
    }

    /// <summary>
    /// Sorts the pick results contained in the query using a custom comparer.
    /// </summary>
    /// <param name="comparer">The comparer.</param>
    public void Sort(IComparer<PickResult> comparer)
    {
      if (m_sorted)
        return;

      if (comparer is null)
        Sort();

      m_results.Sort(comparer);
      m_sorted = true;
    }

    /// <summary>
    /// Clears the query of all results.
    /// </summary>
    public void Clear()
    {
      for (int i = 0; i < m_results.Count; i++)
      {
        PickResult result = m_results[i];
        result.Clear();
      }

      Pool<PickResult>.Return(m_results);
      m_results.Clear();
      m_sorted = false;
    }

    /// <summary>
    /// Returns an enumerator that iterates through the collection.
    /// </summary>
    /// <returns>An enumerator that can be used to iterate through the collection.</returns>
    public List<PickResult>.Enumerator GetEnumerator()
    {
      return m_results.GetEnumerator();
    }

    /// <summary>
    /// Returns an enumerator that iterates through the collection.
    /// </summary>
    /// <returns>An enumerator that can be used to iterate through the collection.</returns>
    IEnumerator<PickResult> IEnumerable<PickResult>.GetEnumerator()
    {
      return m_results.GetEnumerator();
    }

    /// <summary>
    /// Returns an enumerator that iterates through the collection.
    /// </summary>
    /// <returns>An enumerator that can be used to iterate through the collection.</returns>
    IEnumerator IEnumerable.GetEnumerator()
    {
      return m_results.GetEnumerator();
    }

    //Sorts based on bounding intersections
    private sealed class BoundingPickComparer : IComparer<PickResult>
    {
      public int Compare(PickResult x, PickResult y)
      {
        ref readonly BoundingIntersectionResult xBoundsResult = ref x.BoundingIntersection;
        ref readonly BoundingIntersectionResult yBoundsResult = ref y.BoundingIntersection;

        if (xBoundsResult.IsValid && yBoundsResult.IsValid)
        {
          LineIntersectionResult xClosest, yClosest;
          xBoundsResult.GetIntersection(0, out xClosest);
          yBoundsResult.GetIntersection(0, out yClosest);

          float dist1 = xClosest.Distance;
          float dist2 = yClosest.Distance;

          if (MathHelper.IsEqual(dist1, dist2, MathHelper.ZeroTolerance))
            return 0;

          if (dist1 < dist2)
            return -1;
          else
            return 1;
        }
        else if (xBoundsResult.IsValid)
        {
          return -1;
        }
        else if (yBoundsResult.IsValid)
        {
          return 1;
        }

        return 0;
      }
    }

    //Sorts based on the closest primitive intersection, or falls back to bounding pick comparison
    private sealed class PrimitivePickComparer : IComparer<PickResult>
    {
      public int Compare(PickResult x, PickResult y)
      {
        //If both have primitive pick results, compare, otherwise fall back to bounding checks
        if (x.Count > 0 && y.Count > 0)
        {
          float dist1 = x.ClosestIntersection.First.Distance;
          float dist2 = y.ClosestIntersection.First.Distance;

          if (MathHelper.IsEqual(dist1, dist2, MathHelper.ZeroTolerance))
            return 0;

          if (dist1 < dist2)
            return -1;
          else
            return 1;
        }

        return s_boundingPickDistanceComparer.Compare(x, y);
      }
    }
  }
}
