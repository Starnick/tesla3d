﻿/*
* Copyright (c) 2010-2024 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Buffers;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Tesla.Content;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Represents a Bounding Sphere. The sphere is defined by a center point and a radius.
  /// </summary>
  [SavableVersion(1)]
  public sealed class BoundingSphere : BoundingVolume
  {
    private Vector3 m_center;
    private float m_radius;

    /// <summary>
    /// Gets or sets the center of the bounding volume.
    /// </summary>
    public override Vector3 Center
    {
      get
      {
        return m_center;
      }
      set
      {
        m_center = value;
        UpdateCorners = true;
      }
    }

    /// <summary>
    /// Gets or sets the radius of the bounding sphere.
    /// </summary>
    public float Radius
    {
      get
      {
        return m_radius;
      }
      set
      {
        m_radius = value;
        UpdateCorners = true;
      }
    }

    /// <summary>
    /// Gets the volume of the bounding volume.
    /// </summary>
    public override float Volume { get { return 4.0f * MathHelper.OneThird * MathHelper.Pi * m_radius * m_radius * m_radius; } }

    /// <summary>
    /// Gets the bounding type.
    /// </summary>
    public override BoundingType BoundingType { get { return BoundingType.Sphere; } }

    /// <summary>
    /// Constructs a new instance of the <see cref="BoundingSphere"/> class.
    /// </summary>
    public BoundingSphere()
    {
      m_center = Vector3.Zero;
      m_radius = 0.0f;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="BoundingSphere"/> class.
    /// </summary>
    /// <param name="center">Center of the bounding sphere.</param>
    /// <param name="radius">Radius of the bounding sphere.</param>
    public BoundingSphere(in Vector3 center, float radius)
    {
      m_center = center;
      m_radius = radius;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="BoundingSphere"/> class.
    /// </summary>
    /// <param name="boundingSphereData">Bounding sphere data.</param>
    public BoundingSphere(in BoundingSphere.Data boundingSphereData)
    {
      m_center = boundingSphereData.Center;
      m_radius = boundingSphereData.Radius;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="BoundingSphere"/> class.
    /// </summary>
    /// <param name="boundingSphere">Bounding sphere.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the bounding sphere is null</exception>
    public BoundingSphere(BoundingSphere boundingSphere)
    {
      ArgumentNullException.ThrowIfNull(boundingSphere, nameof(boundingSphere));

      m_center = boundingSphere.Center;
      m_radius = boundingSphere.Radius;
    }

    /// <summary>
    /// Extends the bounding volume to include the specified point.
    /// </summary>
    /// <param name="point">Point to contain.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void Extend(in Vector3 point)
    {
      Extend(new ReadOnlySpan<Vector3>(point));
    }

    /// <summary>
    /// Extends the bounding volume to include the specified points.
    /// </summary>
    /// <param name="points">Points to contain.</param>
    public void Extend(ReadOnlySpan<Vector3> points)
    {
      if (points.IsEmpty)
        return;

      //Average center and points
      int numPts = points.Length;
      for (int i = 0; i < numPts; i++)
      {
        ref readonly Vector3 pt = ref points[i];
        Vector3.Add(m_center, pt, out m_center);
      }

      Vector3.Multiply(m_center, 1.0f / (float) numPts, out m_center);

      //Get max radius
      float radiusSqr = m_radius * m_radius;

      for (int i = 0; i < numPts; i++)
      {
        ref readonly Vector3 pt = ref points[i];
        Vector3 diff;
        Vector3.Subtract(pt, m_center, out diff);
        float testRadius = diff.LengthSquared();
        if (testRadius > radiusSqr)
          radiusSqr = testRadius;
      }

      m_radius = MathF.Sqrt(radiusSqr);
      UpdateCorners = true;
    }

    /// <summary>
    /// Sets the bounding volume by either copying the specified bounding volume if its the specified type, or computing a volume required to fully contain it.
    /// </summary>
    /// <param name="volume">Bounding volume to copy from</param>
    public override void Set(BoundingVolume? volume)
    {
      if (volume is null)
        return;

      switch (volume.BoundingType)
      {
        case BoundingType.AxisAlignedBoundingBox:
          {
            BoundingBox other = (volume as BoundingBox)!;
            m_center = other.Center;
            m_radius = other.Extents.Length();
          }
          break;
        case BoundingType.Sphere:
          {
            BoundingSphere other = (volume as BoundingSphere)!;
            m_center = other.m_center;
            m_radius = other.m_radius;
          }
          break;
        case BoundingType.Capsule:
          {
            BoundingCapsule other = (volume as BoundingCapsule)!;
            m_center = other.Center;
            m_radius = other.CenterLine.Extent + other.Radius;
          }
          break;
        case BoundingType.OrientedBoundingBox:
          {
            OrientedBoundingBox other = (volume as OrientedBoundingBox)!;
            m_center = other.Center;

            Vector3 extents = other.Extents;
            Triad axes = other.Axes;

            Vector3.Multiply(axes.XAxis, extents.X, out axes.XAxis);
            Vector3.Multiply(axes.YAxis, extents.Y, out axes.YAxis);
            Vector3.Multiply(axes.ZAxis, extents.Z, out axes.ZAxis);

            Vector3 diagonal;
            Vector3.Add(axes.XAxis, axes.YAxis, out diagonal);
            Vector3.Add(diagonal, axes.ZAxis, out diagonal);

            m_radius = diagonal.Length();
          }
          break;
        case BoundingType.Frustum:
        case BoundingType.Mesh:
        default:
          {
            ComputeFromPoints(volume.Corners, null);
          }
          break;
      }

      UpdateCorners = true;
    }

    /// <summary>
    /// Sets the bounding sphere from the specified data.
    /// </summary>
    /// <param name="boundingSphereData">Bounding sphere data.</param>
    public void Set(in BoundingSphere.Data boundingSphereData)
    {
      m_center = boundingSphereData.Center;
      m_radius = boundingSphereData.Radius;
      UpdateCorners = true;
    }

    /// <summary>
    /// Sets the bounding sphere from the specified data.
    /// </summary>
    /// <param name="center">Center of the bounding sphere.</param>
    /// <param name="radius">Radius of the bounding sphere.</param>
    public void Set(in Vector3 center, float radius)
    {
      m_center = center;
      m_radius = radius;
      UpdateCorners = true;
    }

    /// <summary>
    /// Creates a copy of the bounding volume and returns a new instance.
    /// </summary>
    /// <returns>Copied bounding volume</returns>
    public override BoundingVolume Clone()
    {
      return new BoundingSphere(this);
    }

    /// <summary>
    /// Computes the distance from a given point to the edge of the volume.
    /// </summary>
    /// <param name="point">Point in space</param>
    /// <returns>Distance from the point to the edge of the volume.</returns>
    public override float DistanceTo(in Vector3 point)
    {
      float dist = Vector3.Distance(m_center, point);

      return MathF.Max(0.0f, dist - m_radius);
    }

    /// <summary>
    /// Computes the distance squared from a given point to the edge of the volume.
    /// </summary>
    /// <param name="point">Point in space</param>
    /// <returns>Distanced squared from the point to the edge of the volume.</returns>
    public override float DistanceSquaredTo(in Vector3 point)
    {
      float dist = Vector3.Distance(m_center, point);
      dist = MathF.Max(0.0f, dist - m_radius);

      return dist * dist;
    }

    /// <summary>
    /// Computes the closest point on the volume from the given point in space.
    /// </summary>
    /// <param name="point">Point in space</param>
    /// <param name="result">Closest point on the edge of the volume.</param>
    public override void ClosestPointTo(in Vector3 point, out Vector3 result)
    {
      Vector3 v;
      Vector3.Subtract(point, m_center, out v);
      v.Normalize();

      Vector3.Multiply(v, m_radius, out v);
      Vector3.Add(m_center, v, out result);
    }

    /// <summary>
    /// Computes a minimum bounding volume that encloses the specified points in space.
    /// </summary>
    /// <param name="points">Points in space</param>
    /// <param name="subMeshRange">Optional range inside the buffer that represents the mesh to compute bounds for. Note: the base vertex offset will be ignored.</param>
    public override void ComputeFromPoints(ReadOnlySpan<Vector3> points, in SubMeshRange? subMeshRange = null)
    {
      //TODO - Take a second look at Min sphere computation
      BoundingSphere.Data data;
      BoundingSphere.Data.FromAveragePoints(points, subMeshRange, out data);

      m_center = data.Center;
      m_radius = data.Radius;
      UpdateCorners = true;
    }

    /// <summary>
    /// Computes a minimum bounding volume that encloses the specified indexed points in space.
    /// </summary>
    /// <param name="points">Points in space.</param>
    /// <param name="indices">Point indices denoting location in the point buffer.</param>
    /// <param name="subMeshRange">Optional range inside the buffer that represents the mesh to compute bounds for.</param>
    public override void ComputeFromIndexedPoints(ReadOnlySpan<Vector3> points, IndexData indices, in SubMeshRange? subMeshRange = null)
    {
      //TODO - Take a second look at Min sphere computation
      BoundingSphere.Data data;
      BoundingSphere.Data.FromAverageIndexedPoints(points, indices, subMeshRange, out data);

      m_center = data.Center;
      m_radius = data.Radius;
      UpdateCorners = true;
    }

    /// <summary>
    /// Determines if the specified point is contained inside the bounding volume.
    /// </summary>
    /// <param name="point">Point to test against.</param>
    /// <returns>Type of containment</returns>
    public override ContainmentType Contains(in Vector3 point)
    {
      float distSquared = Vector3.DistanceSquared(m_center, point);

      if (distSquared <= m_radius * m_radius)
        return ContainmentType.Inside;

      return ContainmentType.Outside;
    }

    /// <summary>
    /// Determines if the specified segment line is contained inside the bounding volume.
    /// </summary>
    /// <param name="line">Segment to test against.</param>
    /// <returns>Type of containment.</returns>
    public override ContainmentType Contains(in Segment line)
    {
      float distStart = Vector3.DistanceSquared(m_center, line.StartPoint);
      float distEnd = Vector3.DistanceSquared(m_center, line.EndPoint);

      float radiusSquared = m_radius * m_radius;

      if (distStart <= radiusSquared && distEnd <= radiusSquared)
        return ContainmentType.Inside;

      if (GeometricToolsHelper.IntersectSegmentSphere(line, m_center, m_radius))
        return ContainmentType.Intersects;

      return ContainmentType.Outside;
    }

    /// <summary>
    /// Determines if the specified triangle is contained inside the bounding volume.
    /// </summary>
    /// <param name="triangle">Triangle to test against.</param>
    /// <returns>Type of containment.</returns>
    public override ContainmentType Contains(in Triangle triangle)
    {
      float distA = Vector3.DistanceSquared(m_center, triangle.PointA);
      float distB = Vector3.DistanceSquared(m_center, triangle.PointB);
      float distC = Vector3.DistanceSquared(m_center, triangle.PointC);

      float radiusSquared = m_radius * m_radius;

      if (distA <= radiusSquared && distB <= radiusSquared && distC <= radiusSquared)
        return ContainmentType.Inside;

      if (GeometricToolsHelper.IntersectTriangleSphere(triangle, m_center, m_radius))
        return ContainmentType.Intersects;

      return ContainmentType.Outside;
    }

    /// <summary>
    /// Determines if the specified ellipse intersects with the bounding volume.
    /// </summary>
    /// <param name="ellipse">Ellipse to test against.</param>
    /// <returns>True if the bounding volume intersects with the ellipse, false otherwise.</returns>
    public override ContainmentType Contains(in Ellipse ellipse)
    {
      //TODO
      throw new NotImplementedException();
    }

    /// <summary>
    /// Determines if the specified bounding volume is contained inside the bounding volume.
    /// </summary>
    /// <param name="volume">Bounding volume to test against.</param>
    /// <returns>Type of containment.</returns>
    public override ContainmentType Contains(BoundingVolume? volume)
    {
      if (volume is null)
        return ContainmentType.Outside;

      switch (volume.BoundingType)
      {
        case BoundingType.AxisAlignedBoundingBox:
          {
            BoundingBox other = (volume as BoundingBox)!;
            Vector3 boxCenter = other.Center;
            Vector3 boxExtents = other.Extents;

            return GeometricToolsHelper.SphereContainsAABB(m_center, m_radius, boxCenter, boxExtents);
          }
        case BoundingType.Sphere:
          {
            BoundingSphere other = (volume as BoundingSphere)!;

            return GeometricToolsHelper.SphereContainsSphere(m_center, m_radius, other.m_center, other.m_radius);
          }
        case BoundingType.Capsule:
          {
            BoundingCapsule other = (volume as BoundingCapsule)!;
            Segment centerLine = other.CenterLine;
            float capsuleRadius = other.Radius;

            return GeometricToolsHelper.SphereContainsCapsule(m_center, m_radius, centerLine, capsuleRadius);
          }
        case BoundingType.OrientedBoundingBox:
          {
            OrientedBoundingBox other = (volume as OrientedBoundingBox)!;
            Vector3 boxCenter = other.Center;
            Triad boxAxes = other.Axes;
            Vector3 boxExtents = other.Extents;

            return GeometricToolsHelper.SphereContainsBox(m_center, m_radius, boxCenter, boxAxes.XAxis, boxAxes.YAxis, boxAxes.ZAxis, boxExtents);
          }
        case BoundingType.Frustum:
        case BoundingType.Mesh:
        default:
          {
            return Contains(volume.Corners);
          }
      }
    }

    /// <summary>
    /// Tests if the specified ray intersects with the bounding volume.
    /// </summary>
    /// <param name="ray">Ray to test against.</param>
    /// <returns>True if the bounding volume intersects with the ray, false otherwise.</returns>
    public override bool Intersects(in Ray ray)
    {
      return GeometricToolsHelper.IntersectRaySphere(ray, m_center, m_radius);
    }

    /// <summary>
    /// Determines if the specified ray intersects with the bounding volume.
    /// </summary>
    /// <param name="ray">Ray to test against.</param>
    /// <param name="result">Bounding intersection result.</param>
    /// <returns>True if the bounding volume intersects with the ray, false otherwise.</returns>
    public override bool Intersects(in Ray ray, out BoundingIntersectionResult result)
    {
      return GeometricToolsHelper.IntersectRaySphere(ray, m_center, m_radius, out result);
    }

    /// <summary>
    /// Determines if the specified segment line intersects with the bounding volume.
    /// </summary>
    /// <param name="line">Segment line to test against.</param>
    /// <returns>True if the bounding volume intersects with the segment line, false otherweise.</returns>
    public override bool Intersects(in Segment line)
    {
      return GeometricToolsHelper.IntersectSegmentSphere(line, m_center, m_radius);
    }

    /// <summary>
    /// Determines if the specified segment line intersects with the bounding volume.
    /// </summary>
    /// <param name="line">Segment line to test against.</param>
    /// <param name="result">Bounding intersection result.</param>
    /// <returns>True if the bounding volume intersects with the segment line, false otherweise.</returns>
    public override bool Intersects(in Segment line, out BoundingIntersectionResult result)
    {
      return GeometricToolsHelper.IntersectSegmentSphere(line, m_center, m_radius, out result);
    }

    /// <summary>
    /// Tests if the specified plane intersects with the bounding volume.
    /// </summary>
    /// <param name="plane">Plane to test against.</param>
    /// <returns>Type of plane intersection.</returns>
    public override PlaneIntersectionType Intersects(in Plane plane)
    {
      float signedDistance = plane.SignedDistanceTo(m_center);

      if (signedDistance > m_radius)
      {
        return PlaneIntersectionType.Front;
      }
      else if (signedDistance < -m_radius)
      {
        return PlaneIntersectionType.Back;
      }
      else
      {
        return PlaneIntersectionType.Intersects;
      }
    }

    /// <summary>
    /// Determines if the specified bounding volume intersects with the bounding volume.
    /// </summary>
    /// <param name="volume">Other bounding volume to test against.</param>
    /// <returns>True if the two volumes intersect with one another, false otherwise.</returns>
    public override bool Intersects(BoundingVolume? volume)
    {
      if (volume is null)
        return false;

      switch (volume.BoundingType)
      {
        case BoundingType.AxisAlignedBoundingBox:
          {
            BoundingBox other = (volume as BoundingBox)!;
            Vector3 boxCenter = other.Center;
            Vector3 boxExtents = other.Extents;

            return GeometricToolsHelper.IntersectSphereAABB(m_center, m_radius, boxCenter, boxExtents);
          }
        case BoundingType.Sphere:
          {
            BoundingSphere other = (volume as BoundingSphere)!;

            return GeometricToolsHelper.IntersectSphereSphere(m_center, m_radius, other.m_center, other.m_radius);
          }
        case BoundingType.Capsule:
          {
            BoundingCapsule other = (volume as BoundingCapsule)!;
            Segment centerLine = other.CenterLine;
            float capsuleRadius = other.Radius;

            return GeometricToolsHelper.IntersectCapsuleSphere(centerLine, capsuleRadius, m_center, m_radius);
          }
        case BoundingType.OrientedBoundingBox:
          {
            OrientedBoundingBox other = (volume as OrientedBoundingBox)!;
            Vector3 boxCenter = other.Center;
            Triad boxAxes = other.Axes;
            Vector3 boxExtents = other.Extents;

            return GeometricToolsHelper.IntersectSphereBox(m_center, m_radius, boxCenter, boxAxes.XAxis, boxAxes.YAxis, boxAxes.ZAxis, boxExtents);
          }
        case BoundingType.Frustum:
        case BoundingType.Mesh:
          {
            return volume.Intersects(this);
          }
        default:
          return IntersectsGeneral(volume);
      }
    }

    /// <summary>
    /// Merges the bounding volume with the specified bounding volume, resulting in a volume that encloses both.
    /// </summary>
    /// <param name="volume">Bounding volume to merge with.</param>
    public override void Merge(BoundingVolume? volume)
    {
      if (volume is null)
        return;

      BoundingSphere.Data s0;
      s0.Center = m_center;
      s0.Radius = m_radius;

      switch (volume.BoundingType)
      {
        case BoundingType.AxisAlignedBoundingBox:
          {
            BoundingBox other = (volume as BoundingBox)!;

            BoundingSphere.Data s1;
            s1.Center = other.Center;
            s1.Radius = other.Extents.Length();

            BoundingSphere.Data merged;
            GeometricToolsHelper.MergeSphereSphere(s0, s1, out merged);

            m_center = merged.Center;
            m_radius = merged.Radius;
          }
          break;
        case BoundingType.Sphere:
          {
            BoundingSphere other = (volume as BoundingSphere)!;

            BoundingSphere.Data s1;
            s1.Center = other.Center;
            s1.Radius = other.Radius;

            BoundingSphere.Data merged;
            GeometricToolsHelper.MergeSphereSphere(s0, s1, out merged);

            m_center = merged.Center;
            m_radius = merged.Radius;
          }
          break;
        case BoundingType.Capsule:
          {
            BoundingCapsule other = (volume as BoundingCapsule)!;

            //Treat capsule as a sphere
            BoundingSphere.Data b1;
            b1.Center = other.Center;
            b1.Radius = other.CenterLine.Extent + other.Radius;

            BoundingSphere.Data b0;
            b0.Center = m_center;
            b0.Radius = m_radius;

            BoundingSphere.Data merged;

            GeometricToolsHelper.MergeSphereSphere(b0, b1, out merged);

            m_center = merged.Center;
            m_radius = merged.Radius;
          }
          break;
        case BoundingType.OrientedBoundingBox:
          {
            OrientedBoundingBox other = (volume as OrientedBoundingBox)!;

            OrientedBoundingBox.Data b1;
            b1.Center = other.Center;
            b1.Axes = other.Axes;
            b1.Extents = other.Extents;

            BoundingSphere.Data merged;
            GeometricToolsHelper.MergeSphereBox(s0, b1, out merged);

            m_center = merged.Center;
            m_radius = merged.Radius;
          }
          break;
        case BoundingType.Frustum:
        case BoundingType.Mesh:
        default:
          {
            BoundingSphere.Data s1;
            BoundingSphere.Data.FromPoints(volume.Corners, out s1);

            BoundingSphere.Data merged;
            GeometricToolsHelper.MergeSphereSphere(s0, s1, out merged);

            m_center = merged.Center;
            m_radius = merged.Radius;
          }
          break;
      }

      UpdateCorners = true;
    }

    /// <summary>
    /// Transforms the bounding volume by a Scale-Rotation-Translation (SRT) transform.
    /// </summary>
    /// <param name="scale">Scaling</param>
    /// <param name="rotation">Rotation</param>
    /// <param name="translation">Translation</param>
    public override void Transform(in Vector3 scale, in Quaternion rotation, in Vector3 translation)
    {
      Vector3.Transform(m_center, rotation, out m_center);
      Vector3.Add(m_center, translation, out m_center);

      float maxScale = MathF.Max(MathF.Abs(scale.X), MathF.Max(MathF.Abs(scale.Y), MathF.Abs(scale.Z)));

      m_radius = m_radius * maxScale;
      UpdateCorners = true;
    }

    /// <summary>
    /// Tests equality between the bounding volume and the other bounding volume.
    /// </summary>
    /// <param name="other">Other bounding volume</param>
    /// <param name="tolerance">Tolerance</param>
    /// <returns>True if the volume is of same type and same size/shape, false otherwise.</returns>
    public override bool Equals([NotNullWhen(true)] BoundingVolume? other, float tolerance)
    {
      BoundingSphere? bs = other as BoundingSphere;
      if (bs is not null)
        return m_center.Equals(bs.m_center, tolerance) && MathHelper.IsEqual(m_radius, bs.m_radius, tolerance);

      return false;
    }

    /// <summary>
    /// Reads the object data from the input.
    /// </summary>
    /// <param name="input">Savable reader</param>
    public override void Read(ISavableReader input)
    {
      input.Read<Vector3>("Center", out m_center);
      m_radius = input.ReadSingle("Radius");
      UpdateCorners = true;
    }

    /// <summary>
    /// Writes the object data to the output.
    /// </summary>
    /// <param name="output">Savable writer</param>
    public override void Write(ISavableWriter output)
    {
      output.Write<Vector3>("Center", m_center);
      output.Write("Radius", m_radius);
    }

    /// <summary>
    /// Computes the corners that represent the extremal points of this bounding volume.
    /// </summary>
    /// <param name="corners">Databuffer to contain the points, length equal to the corner count.</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the corner buffer is null.</exception>
    /// <exception cref="System.ArgumentOutOfRangeException">Thrown if the corner buffer does not have enough space.</exception>
    protected override void ComputeCorners(Span<Vector3> corners)
    {
      if (corners.Length < CornerCount)
        throw new ArgumentOutOfRangeException(StringLocalizer.Instance.GetLocalizedString("NotEnoughSpaceForBoundingCorners", CornerCount.ToString()));

      Vector3 max = new Vector3(m_center.X + m_radius, m_center.Y + m_radius, m_center.Z + m_radius);
      Vector3 min = new Vector3(m_center.X - m_radius, m_center.Y - m_radius, m_center.Z - m_radius);

      corners[0] = new Vector3(min.X, max.Y, max.Z);
      corners[1] = new Vector3(max.X, max.Y, max.Z);
      corners[2] = new Vector3(max.X, min.Y, max.Z);
      corners[3] = new Vector3(min.X, min.Y, max.Z);
      corners[4] = new Vector3(min.X, max.Y, min.Z);
      corners[5] = new Vector3(max.X, max.Y, min.Z);
      corners[6] = new Vector3(max.X, min.Y, min.Z);
      corners[7] = new Vector3(min.X, min.Y, min.Z);
    }

    #region BoundingSphere Data

    /// <summary>
    /// Represents data for a <see cref="BoundingSphere"/>.
    /// </summary>
    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 4)]
    public struct Data : IEquatable<BoundingSphere.Data>, IRefEquatable<BoundingSphere.Data>, IFormattable, IPrimitiveValue
    {
      /// <summary>
      /// Center of the bounding sphere.
      /// </summary>
      public Vector3 Center;

      /// <summary>
      /// Radius of the bounding sphere.
      /// </summary>
      public float Radius;

      /// <summary>
      /// Constructs a new instance of the <see cref="BoundingSphere.Data"/> struct.
      /// </summary>
      /// <param name="center">Center of the bounding sphere.</param>
      /// <param name="radius">Radius of the bounding sphere.</param>
      public Data(in Vector3 center, float radius)
      {
        Center = center;
        Radius = radius;
      }

      /// <summary>
      /// Constructs a new instance of the <see cref="BoundingSphere.Data"/> struct.
      /// </summary>
      /// <param name="sphere">Bounding sphere to copy from</param>
      public Data(BoundingSphere sphere)
      {
        ArgumentNullException.ThrowIfNull(sphere, nameof(sphere));

        Center = sphere.m_center;
        Radius = sphere.m_radius;
      }

      /// <summary>
      /// Determines if the AABB intersects with another.
      /// </summary>
      /// <param name="box">AABB to test against.</param>
      /// <returns>True if the objects intersect, false otherwise.</returns>
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public readonly bool Intersects(in BoundingBox.Data box)
      {
        return GeometricToolsHelper.IntersectSphereAABB(Center, Radius, box.Center, box.Extents);
      }

      /// <summary>
      /// Determines if the AABB intersects with a sphere.
      /// </summary>
      /// <param name="sphere">Sphere to test against</param>
      /// <returns>True if the objects intersect, false otherwise.</returns>
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public readonly bool Intersects(in BoundingSphere.Data sphere)
      {
        return GeometricToolsHelper.IntersectSphereSphere(Center, Radius, sphere.Center, Radius);
      }

      /// <summary>
      /// Computes a bounding volume that encloses the specified points in space by averaging the points.
      /// </summary>
      /// <param name="points">Points in space.</param>
      /// <param name="subMeshRange">Optional range inside the buffer that represents the mesh to compute bounds for. Note: the base vertex offset will be ignored.</param>
      /// <returns>Computed bounding sphere.</returns>
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static BoundingSphere.Data FromAveragePoints(ReadOnlySpan<Vector3> points, in SubMeshRange? subMeshRange = null)
      {
        BoundingSphere.Data result;
        FromAveragePoints(points, subMeshRange, out result);

        return result;
      }

      /// <summary>
      /// Computes a bounding volume that encloses the specified points in space by averaging the points.
      /// </summary>
      /// <param name="points">Points in space.</param>
      /// <param name="result">Computed bounding sphere.</param>
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static void FromAveragePoints(ReadOnlySpan<Vector3> points, out BoundingSphere.Data result)
      {
        FromAveragePoints(points, null, out result);
      }

      /// <summary>
      /// Computes a bounding volume that encloses the specified points in space by averaging the points.
      /// </summary>
      /// <param name="points">Points in space.</param>
      /// <param name="subMeshRange">Optional range inside the buffer that represents the mesh to compute bounds for. Note: the base vertex offset will be ignored.</param>
      /// <param name="result">Computed bounding sphere.</param>
      public static void FromAveragePoints(ReadOnlySpan<Vector3> points, in SubMeshRange? subMeshRange, out BoundingSphere.Data result)
      {
        int offset;
        int count;

        if (!BoundingVolume.ExtractSubMeshRange(points, subMeshRange, out offset, out count))
        {
          result = new BoundingSphere.Data();
          return;
        }

        result.Center = points[offset];

        int upperBoundExclusive = offset + count;
        for (int i = offset + 1; i < upperBoundExclusive; i++)
        {
          ref readonly Vector3 pt = ref points[i];
          Vector3.Add(result.Center, pt, out result.Center);
        }

        Vector3.Multiply(result.Center, 1.0f / (float) count, out result.Center);

        float radiusSqr = 0.0f;
        for (int i = offset; i < upperBoundExclusive; i++)
        {
          Vector3 diff;
          ref readonly Vector3 pt = ref points[i];

          Vector3.Subtract(pt, result.Center, out diff);
          float radiusTest = diff.LengthSquared();
          if (radiusTest > radiusSqr)
            radiusSqr = radiusTest;
        }

        result.Radius = MathF.Sqrt(radiusSqr);
      }

      /// <summary>
      /// Computes a bounding volume that encloses the specified indexed points in space by averaging the points.
      /// </summary>
      /// <param name="points">Points in space.</param>
      /// <param name="indices">Point indices denoting location in the point buffer.</param>
      /// <param name="subMeshRange">Optional range inside the buffer that represents the mesh to compute bounds for.</param>
      /// <returns>Computed bounding sphere.</returns>
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static BoundingSphere.Data FromAverageIndexedPoints(ReadOnlySpan<Vector3> points, IndexData indices, in SubMeshRange? subMeshRange = null)
      {
        BoundingSphere.Data result;
        FromAverageIndexedPoints(points, indices, subMeshRange, out result);

        return result;
      }

      /// <summary>
      /// Computes a bounding volume that encloses the specified indexed points in space by averaging the points.
      /// </summary>
      /// <param name="points">Points in space.</param>
      /// <param name="indices">Point indices denoting location in the point buffer.</param>
      /// <param name="result">Computed bounding sphere.</param>
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static void FromAverageIndexedPoints(ReadOnlySpan<Vector3> points, IndexData indices, out BoundingSphere.Data result)
      {
        FromAverageIndexedPoints(points, indices, null, out result);
      }

      /// <summary>
      /// Computes a bounding volume that encloses the specified indexed points in space by averaging the points.
      /// </summary>
      /// <param name="points">Points in space.</param>
      /// <param name="indices">Point indices denoting location in the point buffer.</param
      /// <param name="subMeshRange">Optional range inside the buffer that represents the mesh to compute bounds for.</param>
      /// <param name="result">Computed bounding sphere.</param>
      public static void FromAverageIndexedPoints(ReadOnlySpan<Vector3> points, IndexData indices, in SubMeshRange? subMeshRange, out BoundingSphere.Data result)
      {
        int offset;
        int count;
        int baseVertexOffset;

        if (!BoundingVolume.ExtractSubMeshRange(points, indices, subMeshRange, out offset, out count, out baseVertexOffset))
        {
          result = new BoundingSphere.Data();
          return;
        }

        int index = indices[offset];
        result.Center = points[index + baseVertexOffset];

        int upperBoundExclusive = offset + count;
        for (int i = offset + 1; i < upperBoundExclusive; i++)
        {
          index = indices[i];
          ref readonly Vector3 pt = ref points[index + baseVertexOffset];
          Vector3.Add(result.Center, pt, out result.Center);
        }

        Vector3.Multiply(result.Center, 1.0f / (float) count, out result.Center);

        float radiusSqr = 0.0f;
        for (int i = offset; i < upperBoundExclusive; i++)
        {
          Vector3 diff;

          index = indices[i];
          ref readonly Vector3 pt = ref points[index + baseVertexOffset];

          Vector3.Subtract(pt, result.Center, out diff);
          float radiusTest = diff.LengthSquared();
          if (radiusTest > radiusSqr)
            radiusSqr = radiusTest;
        }

        result.Radius = MathF.Sqrt(radiusSqr);
      }

      /// <summary>
      /// Computes a minimum bounding volume that encloses the specified points in space.
      /// </summary>
      /// <param name="points">Points in space.</param>
      /// <param name="subMeshRange">Optional range inside the buffer that represents the mesh to compute bounds for. Note: the base vertex offset will be ignored.</param>
      /// <returns>Computed bounding sphere.</returns>
      public static BoundingSphere.Data FromPoints(ReadOnlySpan<Vector3> points, in SubMeshRange? subMeshRange = null)
      {
        BoundingSphere.Data result;
        FromPoints(points, subMeshRange, out result);

        return result;
      }

      /// <summary>
      /// Computes a minimum bounding volume that encloses the specified points in space.
      /// </summary>
      /// <param name="points">Points in space.</param>
      /// <param name="result">Computed bounding sphere.</param>
      public static void FromPoints(ReadOnlySpan<Vector3> points, out BoundingSphere.Data result)
      {
        FromPoints(points, null, out result);
      }

      /// <summary>
      /// Computes a minimum bounding volume that encloses the specified points in space.
      /// </summary>
      /// <param name="points">Points in space.</param>
      /// <param name="subMeshRange">Optional range inside the buffer that represents the mesh to compute bounds for. Note: the base vertex offset will be ignored.</param>
      /// <param name="result">Computed bounding sphere.</param>
      public static void FromPoints(ReadOnlySpan<Vector3> points, in SubMeshRange? subMeshRange, out BoundingSphere.Data result)
      {
        int offset;
        int count;

        if (!BoundingVolume.ExtractSubMeshRange(points, subMeshRange, out offset, out count))
        {
          result = new BoundingSphere.Data();
          return;
        }

        ComputeMinSphere(points.Slice(offset, count), null, 0, out result);
      }

      /// <summary>
      /// Computes a minimum bounding volume that encloses the specified indexed points in space.
      /// </summary>
      /// <param name="points">Points in space.</param>
      /// <param name="indices">Point indices denoting location in the point buffer.</param>
      /// <param name="subMeshRange">Optional range inside the buffer that represents the mesh to compute bounds for.</param>
      /// <returns>Computed bounding sphere.</returns>
      public static BoundingSphere.Data FromIndexedPoints(ReadOnlySpan<Vector3> points, IndexData indices, in SubMeshRange? subMeshRange = null)
      {
        BoundingSphere.Data result;
        FromIndexedPoints(points, indices, subMeshRange, out result);

        return result;
      }

      /// <summary>
      /// Computes a minimum bounding volume that encloses the specified indexed points in space.
      /// </summary>
      /// <param name="points">Points in space.</param>
      /// <param name="indices">Point indices denoting location in the point buffer.</param>
      /// <param name="result">Computed bounding sphere.</param>
      public static void FromIndexedPoints(ReadOnlySpan<Vector3> points, IndexData indices, out BoundingSphere.Data result)
      {
        FromIndexedPoints(points, indices, null, out result);
      }

      /// <summary>
      /// Computes a minimum bounding volume that encloses the specified indexed points in space.
      /// </summary>
      /// <param name="points">Points in space.</param>
      /// <param name="indices">Point indices denoting location in the point buffer.</param>
      /// <param name="subMeshRange">Optional range inside the buffer that represents the mesh to compute bounds for.</param>
      /// <param name="result">Computed bounding sphere.</param>
      public static void FromIndexedPoints(ReadOnlySpan<Vector3> points, IndexData indices, in SubMeshRange? subMeshRange, out BoundingSphere.Data result)
      {
        int offset;
        int count;
        int baseVertexOffset;

        if (!BoundingVolume.ExtractSubMeshRange(points, indices, subMeshRange, out offset, out count, out baseVertexOffset))
        {
          result = new BoundingSphere.Data();
          return;
        }

        ComputeMinSphere(points.Slice(offset, count), indices, baseVertexOffset, out result);
      }

      #region Sphere Calculations

      internal static void ComputeMinSphere(ReadOnlySpan<Vector3> points, IndexData? indices, int baseVertexOffset, out BoundingSphere.Data result)
      {
        result = new BoundingSphere.Data();

        PooledArray<Vector3> pooledPts = default;
        try
        {
          // Take a copy of the points in either form and use the Welzl algorithm to find the minimum bounding sphere. A copy is
          // necessary as the points are re-ordered
          if (indices is null)
          {
            int numPoints = points.Length;

            pooledPts = new PooledArray<Vector3>(numPoints);
            points.CopyTo(pooledPts);

            CalculateWelzl(pooledPts, numPoints, 0, 0, ref result);
          }
          else
          {
            IndexData indicesToCopy = indices.Value;
            int numPoints = indicesToCopy.Length;

            pooledPts = new PooledArray<Vector3>(numPoints);

            Span<Vector3> ptsCopy = pooledPts;
            for (int i = 0; i < numPoints; i++)
              ptsCopy[i] = points[indicesToCopy[i] + baseVertexOffset];

            CalculateWelzl(pooledPts, numPoints, 0, 0, ref result);
          }
        }
        finally
        {
          pooledPts.Dispose();
        }
      }

      private static void CalculateWelzl(Span<Vector3> points, int numPoints, int supportCount, int index, ref BoundingSphere.Data result)
      {
        switch (index)
        {
          case 0:
            result.Center = Vector3.Zero;
            result.Radius = 0.0f;
            break;
          case 1:
            ExactSphere1(points[index - 1], MathHelper.ZeroTolerance, out result);
            break;
          case 2:
            ExactSphere2(points[index - 1], points[index - 2], out result);
            break;
          case 3:
            ExactSphere3(points[index - 1], points[index - 2], points[index - 3], out result);
            break;
          case 4:
            ExactSphere4(points[index - 1], points[index - 2], points[index - 3], points[index - 4], out result);
            return;
        }

        for (int i = 0; i < numPoints; i++)
        {
          float distSqr = Vector3.DistanceSquared(points[i + index], result.Center);

          if ((distSqr - (result.Radius * result.Radius)) > 0.0f)
          {
            for (int j = i; j > 0; j--)
            {
              //Swap
              Vector3 tmp = points[j];
              points[j] = points[j - 1];
              points[j - 1] = tmp;
            }

            CalculateWelzl(points, i, supportCount + 1, index + 1, ref result);
          }
        }
      }

      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      private static void ExactSphere1(in Vector3 P, float radius, out BoundingSphere.Data result)
      {
        result.Center = P;
        result.Radius = radius;
      }

      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      private static void ExactSphere2(in Vector3 P0, in Vector3 P1, out BoundingSphere.Data result)
      {
        Vector3.Add(P0, P1, out result.Center);
        Vector3.Multiply(result.Center, 0.5f, out result.Center);

        Vector3 diff;
        Vector3.Subtract(P1, P0, out diff);

        result.Radius = 0.25f * diff.LengthSquared();
      }

      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      private static void ExactSphere3(in Vector3 P0, in Vector3 P1, in Vector3 P2, out BoundingSphere.Data result)
      {
        Vector3 A, B;
        Vector3.Subtract(P0, P2, out A);
        Vector3.Subtract(P1, P2, out B);

        float AdA = Vector3.Dot(A, A);
        float AdB = Vector3.Dot(A, B);
        float BdB = Vector3.Dot(B, B);

        float det = (AdA * BdB) - (AdB * AdB);

        if (MathF.Abs(det) > 0.0f)
        {
          float m00, m01, m10, m11, d0, d1;
          if (AdA >= BdB)
          {
            m00 = 1.0f;
            m01 = AdB / AdA;
            m10 = m01;
            m11 = BdB / AdA;
            d0 = 0.5f;
            d1 = 0.5f * m11;
          }
          else
          {
            m00 = AdA / BdB;
            m01 = AdB / BdB;
            m10 = m01;
            m11 = 1.0f;
            d0 = 0.5f * m00;
            d1 = 0.5f;
          }

          //Compute center using barycentric coordinates of the inscribed triangle
          float invDet = 1.0f / ((m00 * m11) - (m01 * m10));
          float u0 = invDet * ((m11 * d0) - (m01 * d1));
          float u1 = invDet * ((m00 * d1) - (m10 * d0));
          float u2 = 1.0f - u0 - u1;

          Vector3 rV, tmp;
          Vector3.Multiply(P0, u0, out result.Center);
          Vector3.Multiply(P1, u1, out tmp);
          Vector3.Add(result.Center, tmp, out result.Center);
          Vector3.Multiply(P2, u2, out tmp);
          Vector3.Add(result.Center, tmp, out result.Center);

          Vector3.Multiply(A, u0, out rV);
          Vector3.Multiply(B, u1, out tmp);
          Vector3.Add(rV, tmp, out rV);

          result.Radius = rV.LengthSquared();
        }
        else
        {
          result.Center = Vector3.Zero;
          result.Radius = 0.0f;
        }
      }

      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      private static void ExactSphere4(in Vector3 P0, in Vector3 P1, in Vector3 P2, in Vector3 P3, out BoundingSphere.Data result)
      {
        Vector3 A, B, C, D;
        Matrix M;

        Vector3.Subtract(P0, P3, out A);
        Vector3.Subtract(P1, P3, out B);
        Vector3.Subtract(P2, P3, out C);

        Matrix.FromAxes(A, B, C, out M);

        D.X = Vector3.Dot(A, A);
        D.Y = Vector3.Dot(B, B);
        D.Z = Vector3.Dot(C, C);

        Vector3.Multiply(D, 0.5f, out D);

        if (MathF.Abs(M.Determinant()) > MathHelper.ZeroTolerance)
        {
          Matrix invM;
          Matrix.Invert(M, out invM);

          Vector3 V, U;
          Vector3.Multiply(invM, D, out V); // invM * D (opposite of eberly, invM is row major so transpose)
          Vector3.TransformNormal(V, invM, out U); //V * invM

          float U3 = 1.0f - U[0] - U[1] - U[2];

          Vector3 rV, tmp;

          //Calculate center
          Vector3.Multiply(P0, U[0], out result.Center);
          Vector3.Multiply(P1, U[1], out tmp);
          Vector3.Add(result.Center, tmp, out result.Center);
          Vector3.Multiply(P2, U[2], out tmp);
          Vector3.Add(result.Center, tmp, out result.Center);
          Vector3.Multiply(P3, U3, out tmp);
          Vector3.Add(result.Center, tmp, out result.Center);

          //calculate radius
          Vector3.Multiply(A, U[0], out rV);
          Vector3.Multiply(B, U[1], out tmp);
          Vector3.Add(rV, tmp, out rV);
          Vector3.Multiply(C, U[2], out tmp);
          Vector3.Add(rV, tmp, out rV);

          result.Radius = rV.LengthSquared();
        }
        else
        {
          result.Center = Vector3.Zero;
          result.Radius = 0.0f;
        }
      }

      #endregion

      /// <summary>
      /// Determines whether the specified <see cref="System.Object" /> is equal to this instance.
      /// </summary>
      /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
      /// <returns>True if the specified <see cref="System.Object" /> is equal to this instance; otherwise, false.</returns>
      public readonly override bool Equals([NotNullWhen(true)] object? obj)
      {
        if (obj is BoundingBox.Data)
          return Equals((BoundingBox.Data) obj);

        return false;
      }

      /// <summary>
      /// Tests equality between this bounding sphere and another.
      /// </summary>
      /// <param name="other">Bounding sphere</param>
      /// <returns>True if equal, false otherwise.</returns>
      readonly bool IEquatable<BoundingSphere.Data>.Equals(BoundingSphere.Data other)
      {
        return Center.Equals(other.Center) & MathHelper.IsEqual(Radius, other.Radius);
      }

      /// <summary>
      /// Tests equality between this bounding sphere and another.
      /// </summary>
      /// <param name="other">Bounding sphere</param>
      /// <returns>True if equal, false otherwise.</returns>
      public readonly bool Equals(in BoundingSphere.Data other)
      {
        return Center.Equals(other.Center) & MathHelper.IsEqual(Radius, other.Radius);
      }

      /// <summary>
      /// Tests equality between this bounding sphere and another.
      /// </summary>
      /// <param name="other">Bounding sphere</param>
      /// <param name="tolerance">Tolerance</param>
      /// <returns>True if equal, false otherwise.</returns>
      public readonly bool Equals(in BoundingSphere.Data other, float tolerance)
      {
        return Center.Equals(other.Center, tolerance) && MathHelper.IsEqual(Radius, other.Radius, tolerance);
      }

      /// <summary>
      /// Returns a hash code for this instance.
      /// </summary>
      /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.</returns>
      public readonly override int GetHashCode()
      {
        unchecked
        {
          return Center.GetHashCode() + Radius.GetHashCode();
        }
      }

      /// <summary>
      /// Returns a <see cref="System.String" /> that represents this instance.
      /// </summary>
      /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
      public readonly override string ToString()
      {
        CultureInfo info = CultureInfo.CurrentCulture;
        return String.Format(info, "Center: {0}, Radius: {1}",
            new Object[] { Center.ToString(info), Radius.ToString(info) });
      }

      /// <summary>
      /// Returns a <see cref="System.String" /> that represents this instance.
      /// </summary>
      /// <param name="format">The format</param>
      /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
      public readonly string ToString(string? format)
      {
        if (format is null)
          return ToString();

        CultureInfo info = CultureInfo.CurrentCulture;
        return String.Format(info, "Center: {0}, Radius: {1}",
            new Object[] { Center.ToString(format, info), Radius.ToString(format, info) });
      }

      /// <summary>
      /// Returns a <see cref="System.String" /> that represents this instance.
      /// </summary>
      /// <param name="formatProvider">The format provider.</param>
      /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
      public readonly string ToString(IFormatProvider? formatProvider)
      {
        if (formatProvider is null)
          return ToString();

        return String.Format(formatProvider, "Center: {0}, Radius: {1}",
            new Object[] { Center.ToString(formatProvider), Radius.ToString(formatProvider) });
      }

      /// <summary>
      /// Returns a <see cref="System.String" /> that represents this instance.
      /// </summary>
      /// <param name="format">The format.</param>
      /// <param name="formatProvider">The format provider.</param>
      /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
      public readonly string ToString(string? format, IFormatProvider? formatProvider)
      {
        if (formatProvider is null)
          return ToString();

        if (format is null)
          return ToString(formatProvider);

        return String.Format(formatProvider, "Center: {0}, Radius: {1}",
            new Object[] { Center.ToString(format, formatProvider), Radius.ToString(format, formatProvider) });
      }

      /// <summary>
      /// Writes the primitive data to the output.
      /// </summary>
      /// <param name="output">Primitive writer.</param>
      readonly void IPrimitiveValue.Write(IPrimitiveWriter output)
      {
        output.Write<Vector3>("Center", Center);
        output.Write("Radius", Radius);
      }

      /// <summary>
      /// Reads the primitive data from the input.
      /// </summary>
      /// <param name="input">Primitive reader.</param>
      void IPrimitiveValue.Read(IPrimitiveReader input)
      {
        input.Read<Vector3>("Center", out Center);
        Radius = input.ReadSingle("Radius");
      }
    }

    #endregion
  }
}
