﻿/*
* Copyright (c) 2010-2024 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Tesla.Content;
using Tesla.Design;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Defines a four dimensional vector where each component is a 32-bit integer.
  /// </summary>
  [Serializable]
  [StructLayout(LayoutKind.Sequential, Pack = 4)]
  [TypeConverter(typeof(Int3TypeConverter))]
  public struct Int3 : IEquatable<Int3>, IRefEquatable<Int3>, IFormattable, IPrimitiveValue
  {
    /// <summary>
    /// X component of the vector.
    /// </summary>
    public int X;

    /// <summary>
    /// Y component of the vector.
    /// </summary>
    public int Y;

    /// <summary>
    /// Z component of the vector.
    /// </summary>
    public int Z;

    private static readonly Int3 s_zero = new Int3(0, 0, 0);
    private static readonly Int3 s_one = new Int3(1, 1, 1);
    private static readonly Int3 s_unitX = new Int3(1, 0, 0);
    private static readonly Int3 s_unitY = new Int3(0, 1, 0);
    private static readonly Int3 s_unitZ = new Int3(0, 0, 1);

    private static readonly int s_sizeInBytes = BufferHelper.SizeOf<Int3>();

    /// <summary>
    /// Gets an <see cref="Int3"/> set to (0, 0, 0).
    /// </summary>
    public static ref readonly Int3 Zero
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_zero;
      }
    }

    /// <summary>
    /// Gets an <see cref="Int3"/> set to (1, 1, 1).
    /// </summary>
    public static ref readonly Int3 One
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_one;
      }
    }

    /// <summary>
    /// Gets a unit <see cref="Int3"/> set to (1, 0, 0).
    /// </summary>
    public static ref readonly Int3 UnitX
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_unitX;
      }
    }

    /// <summary>
    /// Gets a unit <see cref="Int3"/> set to (0, 1, 0).
    /// </summary>
    public static ref readonly Int3 UnitY
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_unitY;
      }
    }

    /// <summary>
    /// Gets a unit <see cref="Int3"/> set to (0, 0, 1).
    /// </summary>
    public static ref readonly Int3 UnitZ
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_unitZ;
      }
    }

    /// <summary>
    /// Gets the size of the <see cref="Int3"/> type in bytes.
    /// </summary>
    public static int SizeInBytes
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return s_sizeInBytes;
      }
    }

    /// <summary>
    /// Gets or sets individual components of the vector in the order that the components are declared (XYZ).
    /// </summary>
    /// <param name="index">Zero-based index, in range of [0, 2].</param>
    /// <returns>The value of the specified component.</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the index is out of range [0, 2]</exception>
    public int this[int index]
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      readonly get
      {
        switch (index)
        {
          case 0:
            return X;
          case 1:
            return Y;
          case 2:
            return Z;
          default:
            throw new ArgumentOutOfRangeException(nameof(index), StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));
        }
      }
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      set
      {
        switch (index)
        {
          case 0:
            X = value;
            break;
          case 1:
            Y = value;
            break;
          case 2:
            Z = value;
            break;
          default:
            throw new ArgumentOutOfRangeException(nameof(index), StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));
        }
      }
    }

    /// <summary>
    /// Determines whether this vector is (0, 0, 0).
    /// </summary>
    public readonly bool IsZero
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        ref readonly Int3 zero = ref Int3.Zero;
        return Equals(zero);
      }
    }

    /// <summary>
    /// Determines whether this vector is (1, 1, 1).
    /// </summary>
    public readonly bool IsOne
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        ref readonly Int3 one = ref Int3.One;
        return Equals(one);
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Int3"/> struct.
    /// </summary>
    /// <param name="value">Value to initialize each component to.</param>
    public Int3(int value)
    {
      X = Y = Z = value;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Int3"/> struct.
    /// </summary>
    /// <param name="xy">Vector that contains XY components.</param>
    /// <param name="z">Optional Z component, by default zero.</param>
    public Int3(in Int2 xy, int z = 0)
    {
      X = xy.X;
      Y = xy.Y;
      Z = z;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Int3"/> struct.
    /// </summary>
    /// <param name="xyz">Convert from <see cref="Int4"/>, only taking the XYZ values.</param>
    public Int3(in Int4 xyz)
    {
      X = xyz.X;
      Y = xyz.Y;
      Z = xyz.Z;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Int3"/> struct.
    /// </summary>
    /// <param name="x">X component.</param>
    /// <param name="y">Y component.</param>
    /// <param name="z">Z component.</param>
    public Int3(int x, int y, int z)
    {
      X = x;
      Y = y;
      Z = z;
    }

    /// <summary>
    /// Adds two vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Sum of the two vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int3 Add(in Int3 a, in Int3 b)
    {
      Int3 result;
      result.X = a.X + b.X;
      result.Y = a.Y + b.Y;
      result.Z = a.Z + b.Z;

      return result;
    }

    /// <summary>
    /// Adds two vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <param name="result">Sum of the two vectors</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Add(in Int3 a, in Int3 b, out Int3 result)
    {
      result.X = a.X + b.X;
      result.Y = a.Y + b.Y;
      result.Z = a.Z + b.Z;
    }

    /// <summary>
    /// Subtracts vector b from vector a.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Difference of the two vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int3 Subtract(in Int3 a, in Int3 b)
    {
      Int3 result;
      result.X = a.X - b.X;
      result.Y = a.Y - b.Y;
      result.Z = a.Z - b.Z;

      return result;
    }

    /// <summary>
    /// Subtracts vector b from vector a.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <param name="result">Difference of the two vectors</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Subtract(in Int3 a, in Int3 b, out Int3 result)
    {
      result.X = a.X - b.X;
      result.Y = a.Y - b.Y;
      result.Z = a.Z - b.Z;
    }

    /// <summary>
    /// Divides the components of vector a by those of vector b.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Divisor vector</param>
    /// <returns>Quotient of the two vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int3 Divide(in Int3 a, in Int3 b)
    {
      Int3 result;
      result.X = a.X / b.X;
      result.Y = a.Y / b.Y;
      result.Z = a.Z / b.Z;

      return result;
    }

    /// <summary>
    /// Divides the components of vector a by those of vector b.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Divisor vector</param>
    /// <param name="result">Quotient of the two vectors</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Divide(in Int3 a, in Int3 b, out Int3 result)
    {
      result.X = a.X / b.X;
      result.Y = a.Y / b.Y;
      result.Z = a.Z / b.Z;
    }

    /// <summary>
    /// Divides the components of one vector by a scalar.
    /// </summary>
    /// <param name="value">First vector</param>
    /// <param name="divisor">Divisor scalar</param>
    /// <returns>Divided vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int3 Divide(in Int3 value, int divisor)
    {
      Int3 result;
      result.X = value.X / divisor;
      result.Y = value.Y / divisor;
      result.Z = value.Z / divisor;

      return result;
    }

    /// <summary>
    /// Divides the components of one vector by a scalar.
    /// </summary>
    /// <param name="value">First vector</param>
    /// <param name="divisor">Divisor scalar</param>
    /// <param name="result">Divided Vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Divide(in Int3 value, int divisor, out Int3 result)
    {
      result.X = value.X / divisor;
      result.Y = value.Y / divisor;
      result.Z = value.Z / divisor;
    }

    /// <summary>
    /// Multiplies components of a vector by a scalar value.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="scale">Amount to scale</param>
    /// <returns>Scaled vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int3 Multiply(in Int3 value, int scale)
    {
      Int3 result;
      result.X = value.X * scale;
      result.Y = value.Y * scale;
      result.Z = value.Z * scale;

      return result;
    }

    /// <summary>
    /// Multiplies components of a vector by a scalar value.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="scale">Amount to scale</param>
    /// <param name="result">Scaled vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Multiply(in Int3 value, int scale, out Int3 result)
    {
      result.X = value.X * scale;
      result.Y = value.Y * scale;
      result.Z = value.Z * scale;
    }

    /// <summary>
    /// Multiplies components of two vectors together.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Multiplied vectorr</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int3 Multiply(in Int3 a, in Int3 b)
    {
      Int3 result;
      result.X = a.X * b.X;
      result.Y = a.Y * b.Y;
      result.Z = a.Z * b.Z;

      return result;
    }

    /// <summary>
    /// Multiplies components of two vectors together.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <param name="result">Multiplied vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Multiply(in Int3 a, in Int3 b, out Int3 result)
    {
      result.X = a.X * b.X;
      result.Y = a.Y * b.Y;
      result.Z = a.Z * b.Z;
    }

    /// <summary>
    /// Restricts the source vector in the range of the minimum and maximum vectors.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="min">Minimum vector</param>
    /// <param name="max">Maximum vector</param>
    /// <returns>Clamped vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int3 Clamp(in Int3 value, in Int3 min, in Int3 max)
    {
      int x = value.X;
      int y = value.Y;
      int z = value.Z;

      x = (x > max.X) ? max.X : x;
      x = (x < min.X) ? min.X : x;

      y = (y > max.Y) ? max.Y : y;
      y = (y < min.Y) ? min.Y : y;

      z = (z > max.Z) ? max.Z : z;
      z = (z < min.Z) ? min.Z : z;

      Int3 result;
      result.X = x;
      result.Y = y;
      result.Z = z;

      return result;
    }

    /// <summary>
    /// Restricts the source vector in the range of the minimum and maximum vectors.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="min">Minimum vector</param>
    /// <param name="max">Maximum vector</param>
    /// <param name="result">Clamped vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Clamp(in Int3 value, in Int3 min, in Int3 max, out Int3 result)
    {
      int x = value.X;
      int y = value.Y;
      int z = value.Z;

      x = (x > max.X) ? max.X : x;
      x = (x < min.X) ? min.X : x;

      y = (y > max.Y) ? max.Y : y;
      y = (y < min.Y) ? min.Y : y;

      z = (z > max.Z) ? max.Z : z;
      z = (z < min.Z) ? min.Z : z;

      result.X = x;
      result.Y = y;
      result.Z = z;
    }

    /// <summary>
    /// Gets the vector that contains the mininum value from each of the components of the 
    /// two supplied vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Minimum vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int3 Min(in Int3 a, in Int3 b)
    {
      Int3 result;
      result.X = (a.X < b.X) ? a.X : b.X;
      result.Y = (a.Y < b.Y) ? a.Y : b.Y;
      result.Z = (a.Z < b.Z) ? a.Z : b.Z;

      return result;
    }

    /// <summary>
    /// Gets the vector that contains the mininum value from each of the components of the 
    /// two supplied vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <param name="result">Minimum vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Min(in Int3 a, in Int3 b, out Int3 result)
    {
      result.X = (a.X < b.X) ? a.X : b.X;
      result.Y = (a.Y < b.Y) ? a.Y : b.Y;
      result.Z = (a.Z < b.Z) ? a.Z : b.Z;
    }

    /// <summary>
    /// Gets the vector that contains the maximum value from each of the components of the 
    /// two supplied vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Maximum vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int3 Max(in Int3 a, in Int3 b)
    {
      Int3 result;
      result.X = (a.X > b.X) ? a.X : b.X;
      result.Y = (a.Y > b.Y) ? a.Y : b.Y;
      result.Z = (a.Z > b.Z) ? a.Z : b.Z;

      return result;
    }

    /// <summary>
    /// Gets the vector that contains the maximum value from each of the components of the 
    /// two supplied vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <param name="result">Maximum vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Max(in Int3 a, in Int3 b, out Int3 result)
    {
      result.X = (a.X > b.X) ? a.X : b.X;
      result.Y = (a.Y > b.Y) ? a.Y : b.Y;
      result.Z = (a.Z > b.Z) ? a.Z : b.Z;
    }

    /// <summary>
    /// Flips the signs of the components of the specified vector.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <returns>Negated vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int3 Negate(in Int3 value)
    {
      Int3 result;
      result.X = -value.X;
      result.Y = -value.Y;
      result.Z = -value.Z;

      return result;
    }

    /// <summary>
    /// Flips the signs of the components of the specified vector.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="result">Negated vector</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Negate(in Int3 value, out Int3 result)
    {
      result.X = -value.X;
      result.Y = -value.Y;
      result.Z = -value.Z;
    }

    /// <summary>
    /// Adds the two vectors together.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Sum of the two vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int3 operator +(Int3 a, Int3 b)
    {
      Int3 result;
      result.X = a.X + b.X;
      result.Y = a.Y + b.Y;
      result.Z = a.Z + b.Z;

      return result;
    }

    /// <summary>
    /// Subtracts vector b from vector a.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Difference of the two vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int3 operator -(Int3 a, Int3 b)
    {
      Int3 result;
      result.X = a.X - b.X;
      result.Y = a.Y - b.Y;
      result.Z = a.Z - b.Z;

      return result;
    }

    /// <summary>
    /// Flips the signs of the components of the specified vector.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <returns>Negated vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int3 operator -(Int3 value)
    {
      Int3 result;
      result.X = -value.X;
      result.Y = -value.Y;
      result.Z = -value.Z;

      return result;
    }

    /// <summary>
    /// Multiplies two vectors together.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>Multiplied vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int3 operator *(Int3 a, Int3 b)
    {
      Int3 result;
      result.X = a.X * b.X;
      result.Y = a.Y * b.Y;
      result.Z = a.Z * b.Z;

      return result;
    }

    /// <summary>
    /// Multiplies a vector by a scaling factor.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="scale">Amount to scale</param>
    /// <returns>Multiplied vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int3 operator *(Int3 value, int scale)
    {
      Int3 result;
      result.X = value.X * scale;
      result.Y = value.Y * scale;
      result.Z = value.Z * scale;

      return result;
    }

    /// <summary>
    /// Multiplies a vector by a scaling factor.
    /// </summary>
    /// <param name="scale">Amount to scale</param>
    /// <param name="value">Source vector</param>
    /// <returns>Multiplied vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int3 operator *(int scale, Int3 value)
    {
      Int3 result;
      result.X = value.X * scale;
      result.Y = value.Y * scale;
      result.Z = value.Z * scale;

      return result;
    }

    /// <summary>
    /// Divides the components of vector a by those of vector b.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Divisor vector</param>
    /// <returns>Quotient of the two vectors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int3 operator /(Int3 a, Int3 b)
    {
      Int3 result;
      result.X = a.X / b.X;
      result.Y = a.Y / b.Y;
      result.Z = a.Z / b.Z;

      return result;
    }

    /// <summary>
    /// Divides a vector by a scalar value.
    /// </summary>
    /// <param name="value">Source vector</param>
    /// <param name="divisor">Divisor scalar</param>
    /// <returns>Divided vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Int3 operator /(Int3 value, int divisor)
    {
      Int3 result;
      result.X = value.X / divisor;
      result.Y = value.Y / divisor;
      result.Z = value.Z / divisor;

      return result;
    }

    /// <summary>
    /// Performs an explicit conversion from <see cref="Int3"/> to <see cref="Vector3"/>.
    /// </summary>
    /// <param name="value"><see cref="Int3"/> value</param>
    /// <returns>Converted <see cref="Vector3"/></returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static explicit operator Vector3(Int3 value)
    {
      Vector3 result;
      result.X = (float) value.X;
      result.Y = (float) value.Y;
      result.Z = (float) value.Z;

      return result;
    }

    /// <summary>
    /// Performs an explicit conversion from <see cref="Vector3"/> to <see cref="Int3"/>.
    /// </summary>
    /// <param name="value"><see cref="Vector3"/> value</param>
    /// <returns>Converted<see cref="Int3"/></returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static explicit operator Int3(Vector3 value)
    {
      Int3 result;
      result.X = (int) value.X;
      result.Y = (int) value.Y;
      result.Z = (int) value.Z;

      return result;
    }

    /// <summary>
    /// Implicitly converts from <see cref="Int3"/> to <see cref="System.Numerics.Vector3"/> for interop.
    /// </summary>
    /// <param name="v">Vector to convert.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static implicit operator System.Numerics.Vector3(Int3 v)
    {
      return new System.Numerics.Vector3(v.X, v.Y, v.Z);
    }

    /// <summary>
    /// Implicitly converts from <see cref="System.Numerics.Vector3"/> to <see cref="Int3"/> for interop.
    /// </summary>
    /// <param name="v">Vector to convert.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static implicit operator Int3(System.Numerics.Vector3 v)
    {
      return new Int3((int) v.X, (int) v.Y, (int) v.Z);
    }

    /// <summary>
    /// Checks equality between two vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator ==(Int3 a, Int3 b)
    {
      return (a.X == b.X) && (a.Y == b.Y) && (a.Z == b.Z);
    }

    /// <summary>
    /// Checks inequality between two vectors.
    /// </summary>
    /// <param name="a">First vector</param>
    /// <param name="b">Second vector</param>
    /// <returns>True if both are not equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator !=(Int3 a, Int3 b)
    {
      return (a.X != b.X) || (a.Y != b.Y) || (a.Z != b.Z);
    }

    /// <summary>
    /// Flips the signs of the components of the vector.
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void Negate()
    {
      X = -X;
      Y = -Y;
      Z = -Z;
    }

    /// <summary>
    /// Checks equality between the vector and another vector.
    /// </summary>
    /// <param name="other">Other vector</param>
    /// <returns>True if the vectors are equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    readonly bool IEquatable<Int3>.Equals(Int3 other)
    {
      return (X == other.X) && (Y == other.Y) && (Z == other.Z);
    }

    /// <summary>
    /// Checks equality between the vector and another vector.
    /// </summary>
    /// <param name="other">Other vector</param>
    /// <returns>True if the vectors are equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Equals(in Int3 other)
    {
      return (X == other.X) && (Y == other.Y) && (Z == other.Z);
    }

    /// <summary>
    /// Tests equality between the vector and XYZ values.
    /// </summary>
    /// <param name="x">X component.</param>
    /// <param name="y">Y component.</param>
    /// <param name="z">Z component.</param>
    /// <returns>True if components are equal within tolerance, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Equals(float x, float y, float z)
    {
      return (X == x) && (Y == y) && (Z == z);
    }

    /// <summary>
    /// Determines whether the specified <see cref="System.Object" /> is equal to this instance.
    /// </summary>
    /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
    /// <returns>True if the specified <see cref="System.Object" /> is equal to this instance; otherwise, False.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is Int3)
        return Equals((Int3) obj);

      return false;
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
    public readonly override int GetHashCode()
    {
      unchecked
      {
        return X.GetHashCode() + Y.GetHashCode() + Z.GetHashCode();
      }
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly override string ToString()
    {
      CultureInfo info = CultureInfo.CurrentCulture;
      return String.Format(info, "X: {0} Y: {1} Z: {2}",
          new Object[] { X.ToString(info), Y.ToString(info), Z.ToString(info) });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="format">The format</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(string? format)
    {
      if (format is null)
        return ToString();

      CultureInfo info = CultureInfo.CurrentCulture;
      return String.Format(info, "X: {0} Y: {1} Z: {2}",
          new Object[] { X.ToString(format, info), Y.ToString(format, info), Z.ToString(format, info) });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="formatProvider">The format provider.</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(IFormatProvider? formatProvider)
    {
      if (formatProvider is null)
        return ToString();

      return String.Format(formatProvider, "X: {0} Y: {1} Z: {2}",
          new Object[] { X.ToString(formatProvider), Y.ToString(formatProvider), Z.ToString(formatProvider) });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="format">The format.</param>
    /// <param name="formatProvider">The format provider.</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(String? format, IFormatProvider? formatProvider)
    {
      if (formatProvider is null)
        return ToString();

      if (format is null)
        return ToString(formatProvider);

      return String.Format(formatProvider, "X: {0} Y: {1} Z: {2}",
          new Object[] { X.ToString(format, formatProvider), Y.ToString(format, formatProvider), Z.ToString(format, formatProvider) });
    }

    /// <summary>
    /// Writes the primitive data to the output.
    /// </summary>
    /// <param name="output">Primitive writer.</param>
    readonly void IPrimitiveValue.Write(IPrimitiveWriter output)
    {
      output.Write("X", X);
      output.Write("Y", Y);
      output.Write("Z", Z);
    }

    /// <summary>
    /// Reads the primitive data from the input.
    /// </summary>
    /// <param name="input">Primitive reader.</param>
    void IPrimitiveValue.Read(IPrimitiveReader input)
    {
      X = input.ReadInt32("X");
      Y = input.ReadInt32("Y");
      Z = input.ReadInt32("Z");
    }
  }
}
