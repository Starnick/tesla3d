﻿/*
* Copyright (c) 2010-2024 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;

namespace Tesla
{
  /// <summary>
  /// Option flags for pick queries.
  /// </summary>
  [Flags]
  public enum PickingOptions
  {
    /// <summary>
    /// No options.
    /// </summary>
    None = 0,

    /// <summary>
    /// Do primitive picking if bounding check succeeds.
    /// </summary>
    PrimitivePick = 1,

    /// <summary>
    /// Back faces from primitive pick tests are not added to the result list.
    /// </summary>
    IgnoreBackfaces = 2
  }

  /// <summary>
  /// Enumerates the six planes that make up a bounding frustum.
  /// </summary>
  public enum FrustumPlane
  {
    /// <summary>
    /// Left frustum plane.
    /// </summary>
    Left = 0,

    /// <summary>
    /// Right frustum plane.
    /// </summary>
    Right = 1,

    /// <summary>
    /// Top frustum plane.
    /// </summary>
    Top = 2,

    /// <summary>
    /// Bottom frustum plane.
    /// </summary>
    Bottom = 3,

    /// <summary>
    /// Near frustum plane.
    /// </summary>
    Near = 4,

    /// <summary>
    /// Far frustum plane.
    /// </summary>
    Far = 5
  }

  /// <summary>
  /// Enumerates the intersection types between a plane and bounding volume.
  /// </summary>
  public enum PlaneIntersectionType
  {
    /// <summary>
    /// No intersection and object is in the positive half space of the plane. This is on the same side as the direction which the plane's normal vector is pointing in.
    /// </summary>
    Front = 0,
    /// <summary>
    /// No intersection and object is in the negative half space of the plane. This is opposite of the plane's Normal vector.
    /// </summary>
    Back = 1,
    /// <summary>
    /// The plane and the object intersect.
    /// </summary>
    Intersects = 2
  }

  /// <summary>
  /// Enumerates containment types between bounding volumes.
  /// </summary>
  public enum ContainmentType
  {
    /// <summary>
    /// The two bounding volumes do not overlap one another at all.
    /// </summary>
    Outside = 0,

    /// <summary>
    /// One bounding volume is completely inside the other.
    /// </summary>
    Inside = 1,

    /// <summary>
    /// The two bounding volumes partially overlap or touch.
    /// </summary>
    Intersects = 2
  }

  /// <summary>
  /// Enumerates supported bounding volume types.
  /// </summary>
  public enum BoundingType
  {
    /// <summary>
    /// Sphere bounding volume, defined by a point and radius.
    /// </summary>
    Sphere = 0,

    /// <summary>
    /// Axis aligned bounding box with extents along the X, Y, Z axes.
    /// </summary>
    AxisAlignedBoundingBox = 1,

    /// <summary>
    /// Oriented bounding box, with extents along a rotated frame.
    /// </summary>
    OrientedBoundingBox = 2,

    /// <summary>
    /// Capsule bounding volume that is defined by the sweep of a sphere of a certain radius from one point to another point.
    /// </summary>
    Capsule = 3,

    /// <summary>
    /// A frustum defined by six intersecting planes.
    /// </summary>
    Frustum = 4,

    /// <summary>
    /// Simple convex mesh shape.
    /// </summary>
    Mesh = 5
  }
}
