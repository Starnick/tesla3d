﻿/*
* Copyright (c) 2010-2024 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Tesla.Content;
using Tesla.Design;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Defines a packed 32-bit color using red, green, blue, and alpha components (in RGBA order).
  /// </summary>
  [Serializable]
  [StructLayout(LayoutKind.Sequential, Size = 4)]
  [TypeConverter(typeof(ColorTypeConverter))]
  public struct Color : IEquatable<Color>, IRefEquatable<Color>, IFormattable, IPrimitiveValue
  {
    /// <summary>
    /// Red component.
    /// </summary>
    public byte R;

    /// <summary>
    /// Green component.
    /// </summary>
    public byte G;

    /// <summary>
    /// Blue component.
    /// </summary>
    public byte B;

    /// <summary>
    /// Alpha component.
    /// </summary>
    public byte A;

    private static readonly int s_sizeInBytes = BufferHelper.SizeOf<Color>();

    /// <summary>
    /// Gets the size of <see cref="Color"/> type in bytes.
    /// </summary>
    public static int SizeInBytes
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return s_sizeInBytes;
      }
    }

    /// <summary>
    /// Gets or sets individual components of the color in the order that the components are declared (RGBA).
    /// </summary>
    /// <param name="index">Zero-based index, in range of [0, 3].</param>
    /// <returns>The value of the specified component.</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the index is out of range [0, 3]</exception>
    public byte this[int index]
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      readonly get
      {
        switch (index)
        {
          case 0:
            return R;
          case 1:
            return G;
          case 2:
            return B;
          case 3:
            return A;
          default:
            throw new ArgumentOutOfRangeException(nameof(index), StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));
        }
      }
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      set
      {
        switch (index)
        {
          case 0:
            R = value;
            break;
          case 1:
            G = value;
            break;
          case 2:
            B = value;
            break;
          case 3:
            A = value;
            break;
          default:
            throw new ArgumentOutOfRangeException(nameof(index), StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));
        }
      }
    }

    /// <summary>
    /// Gets the hue of the color. First component of the Hue-Saturation-Brightness (HSB) representation of the color.
    /// </summary>
    public readonly float Hue
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        if (R == G && G == B)
          return 0.0f;

        float r = ((float)R) / 255.0f;
        float g = ((float)G) / 255.0f;
        float b = ((float)B) / 255.0f;

        float max = r;
        float min = r;
        float delta;
        float hue = 0.0f;

        if (g > max)
          max = g;

        if (b > max)
          max = b;

        if (g < min)
          min = g;

        if (b < min)
          min = b;

        delta = max - min;

        if (r == max)
        {
          hue = (g - b) / delta;
        }
        else if (g == max)
        {
          hue = 2.0f + (b - r) / delta;
        }
        else if (b == max)
        {
          hue = 4.0f + (r - g) / delta;
        }

        hue *= 60.0f;

        if (hue < 0.0f)
          hue += 360.0f;

        return hue;
      }
    }

    /// <summary>
    /// Gets the saturation of the color. Second component of the Hue-Saturation-Brightness (HSB) representation of the color.
    /// </summary>
    public readonly float Saturation
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        float r = ((float)R) / 255.0f;
        float g = ((float)G) / 255.0f;
        float b = ((float)B) / 255.0f;

        float max = r;
        float min = r;
        float l;
        float saturation = 0.0f;

        if (g > max)
          max = g;

        if (b > max)
          max = b;

        if (g < min)
          min = g;

        if (b < min)
          min = b;

        //if max == min, then there is no color and the saturation is zero
        if (max != min)
        {
          l = (max + min) / 2.0f;

          if (l <= 0.5f)
          {
            saturation = (max - min) / (max + min);
          }
          else
          {
            saturation = (max - min) / (2.0f - max - min);
          }
        }

        return saturation;
      }
    }

    /// <summary>
    /// Gets the brightness of the color. Third component of the Hue-Saturation-Brightness (HSB) representation of the color.
    /// </summary>
    public readonly float Brightness
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        float r = ((float)R) / 255.0f;
        float g = ((float)G) / 255.0f;
        float b = ((float)B) / 255.0f;

        float max = r;
        float min = r;

        if (g > max)
          max = g;

        if (b > max)
          max = b;

        if (g < min)
          min = g;

        if (b < min)
          min = b;

        return (max + min) / 2.0f;
      }
    }

    /// <summary>
    /// Gets the color as a packed RGBA value.
    /// </summary>
    public readonly uint PackedValue
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return (uint)(R | (G << 8) | (B << 16) | (A << 24));
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Color"/> struct.
    /// </summary>
    /// <param name="rgba">Packed int containing RGBA values.</param>
    public Color(uint rgba)
    {
      UnpackColor(rgba, out R, out G, out B, out A);
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Color"/> struct from RGBA byte values with range 0-255. If
    /// the component values are greater than 255 or less than 0, they will be clamped
    /// to the range 0-255.
    /// </summary>
    /// <param name="red">Red component</param>
    /// <param name="green">Green component</param>
    /// <param name="blue">Blue component</param>
    public Color(int red, int green, int blue)
    {
      R = (byte)MathHelper.ClampToByte(red);
      G = (byte)MathHelper.ClampToByte(green);
      B = (byte)MathHelper.ClampToByte(blue);
      A = 255;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Color"/> struct from RGBA byte values with range 0-255. If
    /// the component values are greater than 255 or less than 0, they will be clamped to the range 0-255.
    /// </summary>
    /// <param name="red">Red component</param>
    /// <param name="green">Green component</param>
    /// <param name="blue">Blue component</param>
    /// <param name="alpha">Alpha component</param>
    public Color(int red, int green, int blue, int alpha)
    {
      R = (byte)MathHelper.ClampToByte(red);
      G = (byte)MathHelper.ClampToByte(green);
      B = (byte)MathHelper.ClampToByte(blue);
      A = (byte)MathHelper.ClampToByte(alpha);
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Color"/> struct from RGB float values with range 0 to 1.0f. Alpha is set to 1.0f.
    /// </summary>
    /// <param name="red">Red component</param>
    /// <param name="green">Green component</param>
    /// <param name="blue">Blue component</param>
    public Color(float red, float green, float blue)
    {
      R = (byte)MathHelper.ClampAndRound(red * 255.0f, 0.0f, 255.0f);
      G = (byte)MathHelper.ClampAndRound(green * 255.0f, 0.0f, 255.0f);
      B = (byte)MathHelper.ClampAndRound(blue * 255.0f, 0.0f, 255.0f);
      A = 255;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Color"/> struct from RGBA float values with range 0 to 1.0f.
    /// </summary>
    /// <param name="red">Red component</param>
    /// <param name="green">Green component</param>
    /// <param name="blue">Blue component</param>
    /// <param name="alpha">Alpha component</param>
    public Color(float red, float green, float blue, float alpha)
    {
      R = (byte)MathHelper.ClampAndRound(red * 255.0f, 0.0f, 255.0f);
      G = (byte)MathHelper.ClampAndRound(green * 255.0f, 0.0f, 255.0f);
      B = (byte)MathHelper.ClampAndRound(blue * 255.0f, 0.0f, 255.0f);
      A = (byte)MathHelper.ClampAndRound(alpha * 255.0f, 0.0f, 255.0f);
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Color"/> struct from RGB float values with range 0 to 1.0f.
    /// </summary>
    /// <param name="rgb">Vector3 containing RGB components as XYZ</param>
    public Color(in Vector3 rgb)
    {
      R = (byte)MathHelper.ClampAndRound(rgb.X * 255.0f, 0.0f, 255.0f);
      G = (byte)MathHelper.ClampAndRound(rgb.Y * 255.0f, 0.0f, 255.0f);
      B = (byte)MathHelper.ClampAndRound(rgb.Z * 255.0f, 0.0f, 255.0f);
      A = 255;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Color"/> struct from RGBA float values with range 0 to 1.0f.
    /// </summary>
    /// <param name="rgb">Vector3 containing RGB components as XYZ</param>
    /// <param name="alpha">Alpha component</param>
    public Color(in Vector3 rgb, float alpha)
    {
      R = (byte)MathHelper.ClampAndRound(rgb.X * 255.0f, 0.0f, 255.0f);
      G = (byte)MathHelper.ClampAndRound(rgb.Y * 255.0f, 0.0f, 255.0f);
      B = (byte)MathHelper.ClampAndRound(rgb.Z * 255.0f, 0.0f, 255.0f);
      A = (byte)MathHelper.ClampAndRound(alpha * 255.0f, 0.0f, 255.0f);
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Color"/> struct from RGBA float values with range 0 to 1.0f.
    /// </summary>
    /// <param name="rgba">Vector4 containing RGBA components as XYZW</param>
    public Color(in Vector4 rgba)
    {
      R = (byte)MathHelper.ClampAndRound(rgba.X * 255.0f, 0.0f, 255.0f);
      G = (byte)MathHelper.ClampAndRound(rgba.Y * 255.0f, 0.0f, 255.0f);
      B = (byte)MathHelper.ClampAndRound(rgba.Z * 255.0f, 0.0f, 255.0f);
      A = (byte)MathHelper.ClampAndRound(rgba.W * 255.0f, 0.0f, 255.0f);
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Color"/> struct from an initial RGB color and new alpha value range 0-255.
    /// </summary>
    /// <param name="color">Initial RGB color.</param>
    /// <param name="alpha">Alpha value</param>
    public Color(Color color, int alpha)
    {
      R = color.R;
      G = color.G;
      B = color.B;
      A = (byte) MathHelper.ClampToByte(alpha);
    }

    //Pack helper
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private static uint PackColor(float r, float g, float b, float a)
    {
      uint n = (uint)MathHelper.ClampAndRound(r * 255.0f, 0.0f, 255.0f);
      uint n1 = ((uint)MathHelper.ClampAndRound(g * 255.0f, 0.0f, 255.0f)) << 8;
      uint n2 = ((uint)MathHelper.ClampAndRound(b * 255.0f, 0.0f, 255.0f)) << 16;
      uint n3 = ((uint)MathHelper.ClampAndRound(a * 255.0f, 0.0f, 255.0f)) << 24;

      return n | n1 | n2 | n3;
    }

    //Unpack helper
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private static void UnpackColor(uint packedValue, out byte r, out byte g, out byte b, out byte a)
    {
      r = (byte)packedValue;
      g = (byte)(packedValue >> 8);
      b = (byte)(packedValue >> 16);
      a = (byte)(packedValue >> 24);
    }

    /// <summary>
    /// Converts a color from a packed BGRA format.
    /// </summary>
    /// <param name="bgra">Packed format, 4 bytes, one for each component in BGRA order.</param>
    /// <returns>Color</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Color FromBGRA(uint bgra)
    {
      byte r, g, b, a;
      UnpackColor(bgra, out r, out g, out b, out a);

      Color result;
      result.R = b;
      result.G = g;
      result.B = r;
      result.A = a;

      return result;
    }

    /// <summary>
    /// Adjusts the contrast of the color. If the contrast is 0, the color is 50% gray
    /// and if its 1 the original color is returned.
    /// </summary>
    /// <param name="value">Source color</param>
    /// <param name="contrast">Contrast amount</param>
    /// <returns>Adjusted color</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Color AdjustContrast(Color value, float contrast)
    {
      Color result;
      result.R = (byte)MathHelper.ClampToByte((int)(((value.R - 128) * contrast) + 128));
      result.G = (byte)MathHelper.ClampToByte((int)(((value.G - 128) * contrast) + 128));
      result.B = (byte)MathHelper.ClampToByte((int)(((value.B - 128) * contrast) + 128));
      result.A = value.A;

      return result;
    }

    /// <summary>
    /// Adjusts the saturation of the color. If the saturation is 0, then the grayscale
    /// color is chosen and if its 1, then the original color is returned.
    /// </summary>
    /// <param name="value">Source color</param>
    /// <param name="saturation">Saturation amount</param>
    /// <returns>Adjusted color</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Color AdjustSaturation(Color value, float saturation)
    {
      uint r = value.R;
      uint g = value.G;
      uint b = value.B;

      uint grey = (uint)MathHelper.Clamp((((float)r * .2125f) + ((float)g * .7154f) + ((float)b * .0721f)), 0.0f, 255.0f);

      Color result;
      result.R = (byte)MathHelper.ClampToByte((int)(((r - grey) * saturation) + grey));
      result.G = (byte)MathHelper.ClampToByte((int)(((g - grey) * saturation) + grey));
      result.B = (byte)MathHelper.ClampToByte((int)(((b - grey) * saturation) + grey));
      result.A = value.A;

      return result;
    }

    /// <summary>
    /// Clamps the color within range of the min and max values.
    /// </summary>
    /// <param name="value">Source color</param>
    /// <param name="min">Minimum value</param>
    /// <param name="max">Maximum value</param>
    /// <returns>Clamped color</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Color Clamp(Color value, Color min, Color max)
    {
      Color result;
      result.R = (byte)MathHelper.Clamp(value.R, min.R, max.R);
      result.G = (byte)MathHelper.Clamp(value.G, min.G, max.G);
      result.B = (byte)MathHelper.Clamp(value.B, min.B, max.B);
      result.A = (byte)MathHelper.Clamp(value.A, min.A, max.A);

      return result;
    }

    /// <summary>
    /// Gets the color that contains the maximum value from each of the components of the 
    /// two supplied colors.
    /// </summary>
    /// <param name="a">First color</param>
    /// <param name="b">Second color</param>
    /// <returns>Maximum color</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Color Max(Color a, Color b)
    {
      Color result;
      result.R = (a.R > b.R) ? a.R : b.R;
      result.G = (a.G > b.G) ? a.G : b.G;
      result.B = (a.B > b.B) ? a.B : b.B;
      result.A = (a.A > b.A) ? a.A : b.A;

      return result;
    }

    /// <summary>
    /// Gets the color that contains the mininum value from each of the components of the 
    /// two supplied colors.
    /// </summary>
    /// <param name="a">First color</param>
    /// <param name="b">Second color</param>
    /// <returns>Minimum color</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Color Min(Color a, Color b)
    {
      Color result;
      result.R = (a.R < b.R) ? a.R : b.R;
      result.G = (a.G < b.G) ? a.G : b.G;
      result.B = (a.B < b.B) ? a.B : b.B;
      result.A = (a.A < b.A) ? a.A : b.A;

      return result;
    }

    /// <summary>
    /// Adds two colors together.
    /// </summary>
    /// <param name="a">First color</param>
    /// <param name="b">Second color</param>
    /// <returns>Sum of the two colors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Color Add(Color a, Color b)
    {
      Color result;
      result.R = (byte)MathHelper.ClampToByte(a.R + b.R);
      result.G = (byte)MathHelper.ClampToByte(a.G + b.G);
      result.B = (byte)MathHelper.ClampToByte(a.B + b.B);
      result.A = (byte)MathHelper.ClampToByte(a.A + b.A);

      return result;
    }

    /// <summary>
    /// Subtracts a color from another.
    /// </summary>
    /// <param name="a">First color</param>
    /// <param name="b">Second color</param>
    /// <returns>Difference of the two colors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Color Subtract(Color a, Color b)
    {
      Color result;
      result.R = (byte)MathHelper.ClampToByte(a.R - b.R);
      result.G = (byte)MathHelper.ClampToByte(a.G - b.G);
      result.B = (byte)MathHelper.ClampToByte(a.B - b.B);
      result.A = (byte)MathHelper.ClampToByte(a.A - b.A);

      return result;
    }

    /// <summary>
    /// Modulates two colors together.
    /// </summary>
    /// <param name="a">First color</param>
    /// <param name="b">Second color</param>
    /// <returns>Modulated color</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Color Modulate(Color a, Color b)
    {
      Color result;
      result.R = (byte)MathHelper.ClampToByte(a.R * b.R);
      result.G = (byte)MathHelper.ClampToByte(a.G * b.G);
      result.B = (byte)MathHelper.ClampToByte(a.B * b.B);
      result.A = (byte)MathHelper.ClampToByte(a.A * b.A);

      return result;
    }

    /// <summary>
    /// Premultiplies the RGB component of a color by its alpha.
    /// </summary>
    /// <param name="color">Non-premultiplied color</param>
    /// <returns>Premultiplied color</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Color PremultiplyAlpha(Color color)
    {
      Color result;
      result.R = (byte)MathHelper.ClampToByte(color.R * color.A);
      result.G = (byte)MathHelper.ClampToByte(color.G * color.A);
      result.B = (byte)MathHelper.ClampToByte(color.B * color.A);
      result.A = color.A;

      return result;
    }

    /// <summary>
    /// Scales a color by a scaling factor.
    /// </summary>
    /// <param name="value">Source color</param>
    /// <param name="scale">Amount to multiply</param>
    /// <returns>Scaled color</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Color Scale(Color value, float scale)
    {
      Color result;
      result.R = (byte)MathHelper.ClampToByte((int)(value.R * scale));
      result.G = (byte)MathHelper.ClampToByte((int)(value.G * scale));
      result.B = (byte)MathHelper.ClampToByte((int)(value.B * scale));
      result.A = (byte)MathHelper.ClampToByte((int)(value.A * scale));

      return result;
    }

    /// <summary>
    /// Negates the specified color by subtracting each of its components from 1.0f.
    /// </summary>
    /// <param name="value">Source color</param>
    /// <returns>Negated color</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Color Negate(Color value)
    {
      Color result;
      result.R = (byte) (255 - value.R);
      result.G = (byte) (255 - value.G);
      result.B = (byte) (255 - value.B);
      result.A = (byte) (255 - value.A);

      return result;
    }

    /// <summary>
    /// Linearly interpolates between two colors.
    /// </summary>
    /// <param name="a">Starting color</param>
    /// <param name="b">Ending color</param>
    /// <param name="percent">Amount to interpolate by</param>
    /// <returns>Interpolated color</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Color Lerp(Color a, Color b, float percent)
    {
      Color result;
      result.R = (byte)MathHelper.ClampToByte(a.R + (int)((b.R - a.R) * percent));
      result.G = (byte)MathHelper.ClampToByte(a.G + (int)((b.G - a.G) * percent));
      result.B = (byte)MathHelper.ClampToByte(a.B + (int)((b.B - a.B) * percent));
      result.A = (byte)MathHelper.ClampToByte(a.A + (int)((b.A - a.A) * percent));

      return result;
    }

    /// <summary>
    /// Compute a cubic interpolation between two colors.
    /// </summary>
    /// <param name="a">First color</param>
    /// <param name="b">Second color</param>
    /// <param name="wf">Weighting factor (between 0 and 1.0)</param>
    /// <returns>Cubic interpolated color</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Color SmoothStep(Color a, Color b, float wf)
    {
      float amt = MathHelper.Clamp(wf, 0.0f, 1.0f);
      amt = (amt * amt) * (3.0f - (2.0f * amt));

      Color result;
      result.R = (byte)MathHelper.ClampToByte(a.R + (int)((b.R - a.R) * amt));
      result.G = (byte)MathHelper.ClampToByte(a.G + (int)((b.G - a.G) * amt));
      result.B = (byte)MathHelper.ClampToByte(a.B + (int)((b.B - a.B) * amt));
      result.A = (byte)MathHelper.ClampToByte(a.A + (int)((b.A - a.A) * amt));

      return result;
    }

    /// <summary>
    /// Tests equality between two colors.
    /// </summary>
    /// <param name="a">First color</param>
    /// <param name="b">Second color</param>
    /// <returns>True if components are equal</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator ==(Color a, Color b)
    {
      return (a.R == b.R) && (a.G == b.G) && (a.B == b.B) && (a.A == b.A);
    }

    /// <summary>
    /// Tests inequality between two colors.
    /// </summary>
    /// <param name="a">First color</param>
    /// <param name="b">Second color</param>
    /// <returns>True if components are not equal</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator !=(Color a, Color b)
    {
      return (a.R != b.R) || (a.G != b.G) || (a.B != b.B) || (a.A != b.A);
    }

    /// <summary>
    /// Adds the two colors together.
    /// </summary>
    /// <param name="a">First color</param>
    /// <param name="b">Second color</param>
    /// <returns>Sum of the two colors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Color operator +(Color a, Color b)
    {
      Color result;
      result.R = (byte) MathHelper.ClampToByte(a.R + b.R);
      result.G = (byte) MathHelper.ClampToByte(a.G + b.G);
      result.B = (byte) MathHelper.ClampToByte(a.B + b.B);
      result.A = (byte) MathHelper.ClampToByte(a.A + b.A);

      return result;
    }

    /// <summary>
    /// Negates the color (subtracts value from 1.0f) of each color component.
    /// </summary>
    /// <param name="value">Source color</param>
    /// <returns>Difference of the two colors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Color operator -(Color value)
    {
      Color result;
      result.R = (byte) (255 - value.R);
      result.G = (byte) (255 - value.G);
      result.B = (byte) (255 - value.B);
      result.A = (byte) (255 - value.A);

      return result;
    }

    /// <summary>
    /// Subtracts a color from another.
    /// </summary>
    /// <param name="a">First color</param>
    /// <param name="b">Second color</param>
    /// <returns>Difference of the two colors</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Color operator -(Color a, Color b)
    {
      Color result;
      result.R = (byte) MathHelper.ClampToByte(a.R - b.R);
      result.G = (byte) MathHelper.ClampToByte(a.G - b.G);
      result.B = (byte) MathHelper.ClampToByte(a.B - b.B);
      result.A = (byte) MathHelper.ClampToByte(a.A - b.A);

      return result;
    }

    /// <summary>
    /// Modulates two colors together.
    /// </summary>
    /// <param name="a">First color</param>
    /// <param name="b">Second color</param>
    /// <returns>Modulated color</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Color operator *(Color a, Color b)
    {
      Color result;
      result.R = (byte) MathHelper.ClampToByte(a.R * b.R);
      result.G = (byte) MathHelper.ClampToByte(a.G * b.G);
      result.B = (byte) MathHelper.ClampToByte(a.B * b.B);
      result.A = (byte) MathHelper.ClampToByte(a.A * b.A);

      return result;
    }

    /// <summary>
    /// Scales a color by a scaling factor.
    /// </summary>
    /// <param name="value">Source color</param>
    /// <param name="scale">Amount to multiply</param>
    /// <returns>Scaled color</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Color operator *(Color value, float scale)
    {
      Color result;
      result.R = (byte) MathHelper.ClampToByte((int) (value.R * scale));
      result.G = (byte) MathHelper.ClampToByte((int) (value.G * scale));
      result.B = (byte) MathHelper.ClampToByte((int) (value.B * scale));
      result.A = (byte) MathHelper.ClampToByte((int) (value.A * scale));

      return result;
    }

    /// <summary>
    /// Scales a color by a scaling factor.
    /// </summary>
    /// <param name="scale">Amount to multiply</param>
    /// <param name="value">Source color</param>
    /// <returns>Scaled color</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Color operator *(float scale, Color value)
    {
      Color result;
      result.R = (byte) MathHelper.ClampToByte((int) (value.R * scale));
      result.G = (byte) MathHelper.ClampToByte((int) (value.G * scale));
      result.B = (byte) MathHelper.ClampToByte((int) (value.B * scale));
      result.A = (byte) MathHelper.ClampToByte((int) (value.A * scale));

      return result;
    }

    /// <summary>
    /// Performs an explicit conversion from <see cref="Color"/> to <see cref="uint"/>.
    /// </summary>
    /// <param name="value">Color value.</param>
    /// <returns>RGBA as a packed unsigned integer</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static explicit operator uint(Color value)
    {
      return value.PackedValue;
    }

    /// <summary>
    /// Performs an explicit conversion from <see cref="Color"/> to <see cref="Vector3"/>.
    /// </summary>
    /// <param name="value">Color value.</param>
    /// <returns>RGB as a vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static explicit operator Vector3(Color value)
    {
      return value.ToVector3();
    }

    /// <summary>
    /// Performs an explicit conversion from <see cref="Color"/> to <see cref="Vector4"/>.
    /// </summary>
    /// <param name="value">Color value</param>
    /// <returns>RGBA as a vector</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static explicit operator Vector4(Color value)
    {
      return value.ToVector4();
    }

    /// <summary>
    /// Performs an explicit conversion from <see cref="Vector3"/> to <see cref="Color"/>.
    /// </summary>
    /// <param name="value">RGB as a vector</param>
    /// <returns>Converted color</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static explicit operator Color(in Vector3 value)
    {
      return new Color(value);
    }

    /// <summary>
    /// Performs an explicit conversion from <see cref="Vector4"/> to <see cref="Color"/>.
    /// </summary>
    /// <param name="value">RGBA as a vector</param>
    /// <returns>Converted color</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static explicit operator Color(in Vector4 value)
    {
      return new Color(value);
    }

    /// <summary>
    /// Tests equality between this color and another color.
    /// </summary>
    /// <param name="other">Color to test against</param>
    /// <returns>True if the colors are equal</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Equals(Color other)
    {
      return (R == other.R) && (G == other.G) && (B == other.B) && (A == other.A);
    }

    /// <summary>
    /// Tests equality between this color and another color.
    /// </summary>
    /// <param name="other">Color to test against</param>
    /// <returns>True if the colors are equal</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    readonly bool IRefEquatable<Color>.Equals(in Color other)
    {
      return (R == other.R) && (G == other.G) && (B == other.B) && (A == other.A);
    }

    /// <summary>
    /// Tests equality between this color and the supplied object.
    /// </summary>
    /// <param name="obj">Object to compare</param>
    /// <returns>True if object is a color and components are equal</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is Color)
        return Equals((Color)obj);

      return false;
    }

    /// <summary>
    /// Returns this Color as as 3-component float vector. RGB corresponds to XYZ.
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly Vector3 ToVector3()
    {
      Vector3 result;
      result.X = ((float)R) / 255.0f;
      result.Y = ((float)G) / 255.0f;
      result.Z = ((float)B) / 255.0f;

      return result;
    }

    /// <summary>
    /// Returns this Color as 4-component float vector. RGBA corresponds to XYZW.
    /// </summary>
    /// <returns>Color as Vector4</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly Vector4 ToVector4()
    {
      Vector4 result;
      result.X = ((float) R) / 255.0f;
      result.Y = ((float) G) / 255.0f;
      result.Z = ((float) B) / 255.0f;
      result.W = ((float) A) / 255.0f;

      return result;
    }

    /// <summary>
    /// Negates this color by subtracting each of its components from 1.0f.
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void Negate()
    {
      R = (byte)(255 - R);
      G = (byte)(255 - G);
      B = (byte)(255 - B);
      A = (byte)(255 - A);
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
    public readonly override int GetHashCode()
    {
      unchecked
      {
        return R.GetHashCode() + G.GetHashCode() + B.GetHashCode() + A.GetHashCode();
      }
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly override string ToString()
    {
      CultureInfo info = CultureInfo.CurrentCulture;
      return String.Format(info, "R: {0} G: {1} B: {2} A: {3}",
          new Object[] { R.ToString(info), G.ToString(info), B.ToString(info), A.ToString(info) });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="format">The format</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(string? format)
    {
      if (format is null)
        return ToString();

      CultureInfo info = CultureInfo.CurrentCulture;
      return String.Format(info, "R: {0} G: {1} B: {2} A: {3}",
          new Object[] { R.ToString(format, info), G.ToString(format, info), B.ToString(format, info), A.ToString(format, info) });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="formatProvider">The format provider.</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(IFormatProvider? formatProvider)
    {
      if (formatProvider is null)
        return ToString();

      return String.Format(formatProvider, "R: {0} G: {1} B: {2} A: {3}",
          new Object[] { R.ToString(formatProvider), G.ToString(formatProvider), B.ToString(formatProvider), A.ToString(formatProvider) });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="format">The format.</param>
    /// <param name="formatProvider">The format provider.</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(string? format, IFormatProvider? formatProvider)
    {
      if (formatProvider is null)
        return ToString();

      if (format is null)
        return ToString(formatProvider);

      return String.Format(formatProvider, "R: {0} G: {1} B: {2} A: {3}",
          new Object[] { R.ToString(format, formatProvider), G.ToString(format, formatProvider), B.ToString(format, formatProvider), A.ToString(format, formatProvider) });
    }

    /// <summary>
    /// Writes the primitive data to the output.
    /// </summary>
    /// <param name="output">Primitive writer.</param>
    readonly void IPrimitiveValue.Write(IPrimitiveWriter output)
    {
      output.Write("R", R);
      output.Write("G", G);
      output.Write("B", B);
      output.Write("A", A);
    }

    /// <summary>
    /// Reads the primitive data from the input.
    /// </summary>
    /// <param name="input">Primitive reader.</param>
    void IPrimitiveValue.Read(IPrimitiveReader input)
    {
      R = input.ReadByte("R");
      G = input.ReadByte("G");
      B = input.ReadByte("B");
      A = input.ReadByte("A");
    }

    #region Predefined Colors

    /// <summary>
    /// Gets a color with the value R:0 G: 0 B: 0 A: 0.
    /// </summary>
    public static Color TransparentBlack
    {
      get
      {
        return new Color(0, 0, 0, 0);
      }
    }

    /// <summary>
    /// Gets a color with the value R:240 G:248 B:255 A:255.
    /// </summary>
    public static Color AliceBlue
    {
      get
      {
        return new Color(240, 248, 255, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:250 G:235 B:215 A:255.
    /// </summary>
    public static Color AntiqueWhite
    {
      get
      {
        return new Color(250, 235, 215, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:0 G:255 B:255 A:255.
    /// </summary>
    public static Color Aqua
    {
      get
      {
        return new Color(0, 255, 255, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:127 G:255 B:212 A:255.
    /// </summary>
    public static Color Aquamarine
    {
      get
      {
        return new Color(127, 255, 212, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:240 G:255 B:255 A:255.
    /// </summary>
    public static Color Azure
    {
      get
      {
        return new Color(240, 255, 255, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:245 G:245 B:220 A:255.
    /// </summary>
    public static Color Beige
    {
      get
      {
        return new Color(245, 245, 220, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:228 B:196 A:255.
    /// </summary>
    public static Color Bisque
    {
      get
      {
        return new Color(255, 228, 196, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:0 G:0 B:0 A:255.
    /// </summary>
    public static Color Black
    {
      get
      {
        return new Color(0, 0, 0, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:235 B:205 A:255.
    /// </summary>
    public static Color BlanchedAlmond
    {
      get
      {
        return new Color(255, 235, 205, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:0 G:0 B:255 A:255.
    /// </summary>
    public static Color Blue
    {
      get
      {
        return new Color(0, 0, 255, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:138 G:43 B:226 A:255.
    /// </summary>
    public static Color BlueViolet
    {
      get
      {
        return new Color(138, 43, 226, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:165 G:42 B:42 A:255.
    /// </summary>
    public static Color Brown
    {
      get
      {
        return new Color(165, 42, 42, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:222 G:184 B:135 A:255.
    /// </summary>
    public static Color BurlyWood
    {
      get
      {
        return new Color(222, 184, 135, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:95 G:158 B:160 A:255.
    /// </summary>
    public static Color CadetBlue
    {
      get
      {
        return new Color(95, 158, 160, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:127 G:255 B:0 A:255.
    /// </summary>
    public static Color Chartreuse
    {
      get
      {
        return new Color(127, 255, 0, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:210 G:105 B:30 A:255.
    /// </summary>
    public static Color Chocolate
    {
      get
      {
        return new Color(210, 105, 30, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:127 B:80 A:255.
    /// </summary>
    public static Color Coral
    {
      get
      {
        return new Color(255, 127, 80, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:100 G:149 B:237 A:255.
    /// </summary>
    public static Color CornflowerBlue
    {
      get
      {
        return new Color(100, 149, 237, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:248 B:220 A:255.
    /// </summary>
    public static Color Cornsilk
    {
      get
      {
        return new Color(255, 248, 220, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:220 G:20 B:60 A:255.
    /// </summary>
    public static Color Crimson
    {
      get
      {
        return new Color(220, 20, 60, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:0 G:255 B:255 A:255.
    /// </summary>
    public static Color Cyan
    {
      get
      {
        return new Color(0, 255, 255, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:0 G:0 B:139 A:255.
    /// </summary>
    public static Color DarkBlue
    {
      get
      {
        return new Color(0, 0, 139, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:0 G:139 B:139 A:255.
    /// </summary>
    public static Color DarkCyan
    {
      get
      {
        return new Color(0, 139, 139, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:184 G:134 B:11 A:255.
    /// </summary>
    public static Color DarkGoldenrod
    {
      get
      {
        return new Color(184, 134, 11, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:169 G:169 B:169 A:255.
    /// </summary>
    public static Color DarkGray
    {
      get
      {
        return new Color(169, 169, 169, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:0 G:100 B:0 A:255.
    /// </summary>
    public static Color DarkGreen
    {
      get
      {
        return new Color(40, 100, 0, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:189 G:183 B:107 A:255.
    /// </summary>
    public static Color DarkKhaki
    {
      get
      {
        return new Color(189, 183, 107, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:139 G:0 B:139 A:255.
    /// </summary>
    public static Color DarkMagenta
    {
      get
      {
        return new Color(139, 0, 139, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:85 G:107 B:47 A:255.
    /// </summary>
    public static Color DarkOliveGreen
    {
      get
      {
        return new Color(85, 107, 47, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:140 B:0 A:255.
    /// </summary>
    public static Color DarkOrange
    {
      get
      {
        return new Color(255, 140, 0, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:153 G:50 B:204 A:255.
    /// </summary>
    public static Color DarkOrchid
    {
      get
      {
        return new Color(153, 50, 204, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:139 G:0 B:0 A:255.
    /// </summary>
    public static Color DarkRed
    {
      get
      {
        return new Color(139, 0, 0, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:233 G:150 B:122 A:255.
    /// </summary>
    public static Color DarkSalmon
    {
      get
      {
        return new Color(233, 150, 122, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:143 G:188 B:139 A:255.
    /// </summary>
    public static Color DarkSeaGreen
    {
      get
      {
        return new Color(143, 188, 139, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:72 G:61 B:139 A:255.
    /// </summary>
    public static Color DarkSlateBlue
    {
      get
      {
        return new Color(72, 61, 139, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:47 G:79 B:79 A:255.
    /// </summary>
    public static Color DarkSlateGray
    {
      get
      {
        return new Color(47, 79, 79, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:0 G:206 B:209 A:255.
    /// </summary>
    public static Color DarkTurquoise
    {
      get
      {
        return new Color(0, 206, 209, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:148 G:0 B:211 A:255.
    /// </summary>
    public static Color DarkViolet
    {
      get
      {
        return new Color(148, 0, 211, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:20 B:147 A:255.
    /// </summary>
    public static Color DeepPink
    {
      get
      {
        return new Color(255, 20, 147, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:0 G:191 B:255 A:255.
    /// </summary>
    public static Color DeepSkyBlue
    {
      get
      {
        return new Color(0, 191, 255, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:105 G:105 B:105 A:255.
    /// </summary>
    public static Color DimGray
    {
      get
      {
        return new Color(105, 105, 105, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:30 G:144 B:255 A:255.
    /// </summary>
    public static Color DodgerBlue
    {
      get
      {
        return new Color(30, 144, 255, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:178 G:34 B:34 A:255.
    /// </summary>
    public static Color Firebrick
    {
      get
      {
        return new Color(178, 34, 34, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:250 B:240 A:255.
    /// </summary>
    public static Color FloralWhite
    {
      get
      {
        return new Color(255, 250, 240, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:34 G:139 B:34 A:255.
    /// </summary>
    public static Color ForestGreen
    {
      get
      {
        return new Color(34, 139, 34, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:0 B:255 A:255.
    /// </summary>
    public static Color Fuchsia
    {
      get
      {
        return new Color(255, 0, 255, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:220 G:220 B:220 A:255.
    /// </summary>
    public static Color Gainsboro
    {
      get
      {
        return new Color(220, 220, 220, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:248 G:248 B:255 A:255.
    /// </summary>
    public static Color GhostWhite
    {
      get
      {
        return new Color(248, 248, 255, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:215 B:0 A:255.
    /// </summary>
    public static Color Gold
    {
      get
      {
        return new Color(255, 215, 0, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:218 G:165 B:32 A:255.
    /// </summary>
    public static Color Goldenrod
    {
      get
      {
        return new Color(218, 165, 32, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:128 G:128 B:128 A:255.
    /// </summary>
    public static Color Gray
    {
      get
      {
        return new Color(128, 128, 128, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:0 G:128 B:0 A:255.
    /// </summary>
    public static Color Green
    {
      get
      {
        return new Color(0, 128, 0, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:173 G:255 B:47 A:255.
    /// </summary>
    public static Color GreenYellow
    {
      get
      {
        return new Color(173, 255, 47, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:240 G:255 B:240 A:255.
    /// </summary>
    public static Color Honeydew
    {
      get
      {
        return new Color(240, 255, 240, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:105 B:180 A:255.
    /// </summary>
    public static Color HotPink
    {
      get
      {
        return new Color(255, 105, 180, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:205 G:92 B:92 A:255.
    /// </summary>
    public static Color IndianRed
    {
      get
      {
        return new Color(205, 92, 92, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:75 G:0 B:130 A:255.
    /// </summary>
    public static Color Indigo
    {
      get
      {
        return new Color(75, 0, 130, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:255 B:240 A:255.
    /// </summary>
    public static Color Ivory
    {
      get
      {
        return new Color(255, 255, 240, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:240 G:230 B:140 A:255.
    /// </summary>
    public static Color Khaki
    {
      get
      {
        return new Color(240, 230, 140, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:230 G:230 B:250 A:255.
    /// </summary>
    public static Color Lavender
    {
      get
      {
        return new Color(230, 230, 250, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:240 B:245 A:255.
    /// </summary>
    public static Color LavenderBlush
    {
      get
      {
        return new Color(255, 240, 245, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:124 G:252 B:0 A:255.
    /// </summary>
    public static Color LawnGreen
    {
      get
      {
        return new Color(124, 252, 0, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:250 B:205 A:255.
    /// </summary>
    public static Color LemonChiffon
    {
      get
      {
        return new Color(255, 250, 205, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:173 G:216 B:230 A:255.
    /// </summary>
    public static Color LightBlue
    {
      get
      {
        return new Color(173, 216, 230, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:240 G:128 B:128 A:255.
    /// </summary>
    public static Color LightCoral
    {
      get
      {
        return new Color(240, 128, 128, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:224 G:255 B:255 A:255.
    /// </summary>
    public static Color LightCyan
    {
      get
      {
        return new Color(224, 255, 255, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:250 G:250 B:210 A:255.
    /// </summary>
    public static Color LightGoldenrodYellow
    {
      get
      {
        return new Color(250, 250, 210, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:144 G:238 B:144 A:255.
    /// </summary>
    public static Color LightGreen
    {
      get
      {
        return new Color(144, 238, 144, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:211 G:211 B:211 A:255.
    /// </summary>
    public static Color LightGray
    {
      get
      {
        return new Color(211, 211, 211, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:182 B:193 A:255.
    /// </summary>
    public static Color LightPink
    {
      get
      {
        return new Color(255, 182, 193, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:160 B:122 A:255.
    /// </summary>
    public static Color LightSalmon
    {
      get
      {
        return new Color(255, 160, 122, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:32 G:178 B:170 A:255.
    /// </summary>
    public static Color LightSeaGreen
    {
      get
      {
        return new Color(32, 178, 170, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:135 G:206 B:250 A:255.
    /// </summary>
    public static Color LightSkyBlue
    {
      get
      {
        return new Color(135, 206, 250, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:119 G:136 B:153 A:255.
    /// </summary>
    public static Color LightSlateGray
    {
      get
      {
        return new Color(119, 136, 153, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:176 G:196 B:222 A:255.
    /// </summary>
    public static Color LightSteelBlue
    {
      get
      {
        return new Color(176, 196, 222, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:255 B:224 A:255.
    /// </summary>
    public static Color LightYellow
    {
      get
      {
        return new Color(255, 255, 224, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:0 G:255 B:0 A:255.
    /// </summary>
    public static Color Lime
    {
      get
      {
        return new Color(0, 255, 0, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:50 G:205 B:50 A:255.
    /// </summary>
    public static Color LimeGreen
    {
      get
      {
        return new Color(50, 205, 50, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:250 G:240 B:230 A:255.
    /// </summary>
    public static Color Linen
    {
      get
      {
        return new Color(250, 240, 230, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:0 B:255 A:255.
    /// </summary>
    public static Color Magenta
    {
      get
      {
        return new Color(255, 0, 255, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:128 G:0 B:0 A:255.
    /// </summary>
    public static Color Maroon
    {
      get
      {
        return new Color(128, 0, 0, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:102 G:205 B:170 A:255.
    /// </summary>
    public static Color MediumAquamarine
    {
      get
      {
        return new Color(102, 205, 170, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:0 G:0 B:205 A:255.
    /// </summary>
    public static Color MediumBlue
    {
      get
      {
        return new Color(0, 0, 205, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:186 G:85 B:211 A:255.
    /// </summary>
    public static Color MediumOrchid
    {
      get
      {
        return new Color(186, 85, 211, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:147 G:112 B:219 A:255.
    /// </summary>
    public static Color MediumPurple
    {
      get
      {
        return new Color(147, 112, 219, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:60 G:179 B:113 A:255.
    /// </summary>
    public static Color MediumSeaGreen
    {
      get
      {
        return new Color(60, 179, 113, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:123 G:104 B:238 A:255.
    /// </summary>
    public static Color MediumSlateBlue
    {
      get
      {
        return new Color(123, 104, 238, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:0 G:250 B:154 A:255.
    /// </summary>
    public static Color MediumSpringGreen
    {
      get
      {
        return new Color(0, 250, 154, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:72 G:209 B:204 A:255.
    /// </summary>
    public static Color MediumTurquoise
    {
      get
      {
        return new Color(72, 209, 204, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:199 G:21 B:133 A:255.
    /// </summary>
    public static Color MediumVioletRed
    {
      get
      {
        return new Color(199, 21, 133, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:25 G:25 B:112 A:255.
    /// </summary>
    public static Color MidnightBlue
    {
      get
      {
        return new Color(25, 25, 112, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:245 G:255 B:250 A:255.
    /// </summary>
    public static Color MintCream
    {
      get
      {
        return new Color(245, 255, 250, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:228 B:225 A:255.
    /// </summary>
    public static Color MistyRose
    {
      get
      {
        return new Color(255, 228, 225, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:228 B:181 A:255.
    /// </summary>
    public static Color Moccasin
    {
      get
      {
        return new Color(255, 228, 181, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:222 B:173 A:255.
    /// </summary>
    public static Color NavajoWhite
    {
      get
      {
        return new Color(255, 222, 173, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:0 G:0 B:128 A:255.
    /// </summary>
    public static Color Navy
    {
      get
      {
        return new Color(0, 0, 128, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:253 G:245 B:230 A:255.
    /// </summary>
    public static Color OldLace
    {
      get
      {
        return new Color(253, 245, 230, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:128 G:128 B:0 A:255.
    /// </summary>
    public static Color Olive
    {
      get
      {
        return new Color(128, 128, 0, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:107 G:142 B:35 A:255.
    /// </summary>
    public static Color OliveDrab
    {
      get
      {
        return new Color(107, 142, 35, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:165 B:0 A:255.
    /// </summary>
    public static Color Orange
    {
      get
      {
        return new Color(255, 165, 0, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:69 B:0 A:255.
    /// </summary>
    public static Color OrangeRed
    {
      get
      {
        return new Color(255, 69, 0, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:218 G:112 B:214 A:255.
    /// </summary>
    public static Color Orchid
    {
      get
      {
        return new Color(218, 112, 214, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:238 G:232 B:170 A:255.
    /// </summary>
    public static Color PaleGoldenrod
    {
      get
      {
        return new Color(238, 232, 170, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:152 G:251 B:152 A:255.
    /// </summary>
    public static Color PaleGreen
    {
      get
      {
        return new Color(152, 251, 152, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:175 G:238 B:238 A:255.
    /// </summary>
    public static Color PaleTurquoise
    {
      get
      {
        return new Color(175, 238, 238, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:219 G:112 B:147 A:255.
    /// </summary>
    public static Color PaleVioletRed
    {
      get
      {
        return new Color(219, 112, 147, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:239 B:213 A:255.
    /// </summary>
    public static Color PapayaWhip
    {
      get
      {
        return new Color(255, 239, 213, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:218 B:185 A:255.
    /// </summary>
    public static Color PeachPuff
    {
      get
      {
        return new Color(255, 218, 185, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:205 G:133 B:63 A:255.
    /// </summary>
    public static Color Peru
    {
      get
      {
        return new Color(205, 133, 63, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:192 B:203 A:255.
    /// </summary>
    public static Color Pink
    {
      get
      {
        return new Color(255, 192, 203, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:221 G:160 B:221 A:255.
    /// </summary>
    public static Color Plum
    {
      get
      {
        return new Color(221, 160, 221, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:176 G:224 B:230 A:255.
    /// </summary>
    public static Color PowderBlue
    {
      get
      {
        return new Color(176, 224, 230, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:128 G:0 B:128 A:255.
    /// </summary>
    public static Color Purple
    {
      get
      {
        return new Color(128, 0, 128, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:0 B:0 A:255.
    /// </summary>
    public static Color Red
    {
      get
      {
        return new Color(255, 0, 0, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:188 G:143 B:143 A:255.
    /// </summary>
    public static Color RosyBrown
    {
      get
      {
        return new Color(188, 143, 143, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:65 G:105 B:225 A:255.
    /// </summary>
    public static Color RoyalBlue
    {
      get
      {
        return new Color(65, 105, 225, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:139 G:69 B:19 A:255.
    /// </summary>
    public static Color SaddleBrown
    {
      get
      {
        return new Color(139, 69, 19, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:250 G:128 B:114 A:255.
    /// </summary>
    public static Color Salmon
    {
      get
      {
        return new Color(250, 128, 114, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:244 G:164 B:96 A:255.
    /// </summary>
    public static Color SandyBrown
    {
      get
      {
        return new Color(244, 164, 96, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:46 G:139 B:87 A:255.
    /// </summary>
    public static Color SeaGreen
    {
      get
      {
        return new Color(46, 139, 87, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:245 B:238 A:255.
    /// </summary>
    public static Color SeaShell
    {
      get
      {
        return new Color(255, 245, 238, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:160 G:82 B:45 A:255.
    /// </summary>
    public static Color Sienna
    {
      get
      {
        return new Color(160, 82, 45, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:192 G:192 B:192 A:255.
    /// </summary>
    public static Color Silver
    {
      get
      {
        return new Color(192, 192, 192, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:135 G:206 B:235 A:255.
    /// </summary>
    public static Color SkyBlue
    {
      get
      {
        return new Color(135, 206, 235, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:106 G:90 B:205 A:255.
    /// </summary>
    public static Color SlateBlue
    {
      get
      {
        return new Color(106, 90, 205, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:112 G:128 B:144 A:255.
    /// </summary>
    public static Color SlateGray
    {
      get
      {
        return new Color(112, 128, 144, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:250 B:250 A:255.
    /// </summary>
    public static Color Snow
    {
      get
      {
        return new Color(255, 250, 250, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:0 G:255 B:127 A:255.
    /// </summary>
    public static Color SpringGreen
    {
      get
      {
        return new Color(0, 255, 127, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:70 G:130 B:180 A:255.
    /// </summary>
    public static Color SteelBlue
    {
      get
      {
        return new Color(70, 130, 180, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:210 G:180 B:140 A:255.
    /// </summary>
    public static Color Tan
    {
      get
      {
        return new Color(210, 180, 140, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:0 G:128 B:128 A:255.
    /// </summary>
    public static Color Teal
    {
      get
      {
        return new Color(0, 128, 128, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:216 G:191 B:216 A:255.
    /// </summary>
    public static Color Thistle
    {
      get
      {
        return new Color(216, 191, 216, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:99 B:71 A:255.
    /// </summary>
    public static Color Tomato
    {
      get
      {
        return new Color(255, 99, 71, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:64 G:224 B:208 A:255.
    /// </summary>
    public static Color Turquoise
    {
      get
      {
        return new Color(64, 224, 208, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:238 G:130 B:238 A:255.
    /// </summary>
    public static Color Violet
    {
      get
      {
        return new Color(238, 130, 238, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:245 G:222 B:179 A:255.
    /// </summary>
    public static Color Wheat
    {
      get
      {
        return new Color(245, 222, 179, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:255 B:255 A:255.
    /// </summary>
    public static Color White
    {
      get
      {
        return new Color(255, 255, 255, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:245 G:245 B:245 A:255.
    /// </summary>
    public static Color WhiteSmoke
    {
      get
      {
        return new Color(245, 245, 245, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:255 G:255 B:0 A:255.
    /// </summary>
    public static Color Yellow
    {
      get
      {
        return new Color(255, 255, 0, 255);
      }
    }

    /// <summary>
    /// Gets a color with the value R:154 G:205 B:50 A:255.
    /// </summary>
    public static Color YellowGreen
    {
      get
      {
        return new Color(154, 205, 50, 255);
      }
    }

    #endregion
  }
}
