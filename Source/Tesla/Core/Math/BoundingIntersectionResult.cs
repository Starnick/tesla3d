﻿/*
* Copyright (c) 2010-2024 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Tesla.Content;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Represents an intersection result between a bounding volume and line, ray, or segment.
  /// </summary>
  /// <remarks>
  /// Three possible results can occur:
  /// <list type="number">
  /// <item>
  /// <description>No intersection (e.g. linea misses the volume completely)</description>
  /// </item>
  /// <item>
  /// <description>One point intersection (e.g. line origin is inside and it exits, or the line touches at exactly one point externally)</description>
  /// </item>
  /// <item>
  /// <description>Two point intersection (e.g. line enters and exits the volume</description>
  /// </item>
  /// </list>
  /// </remarks>
  [Serializable]
  [StructLayout(LayoutKind.Sequential)]
  public struct BoundingIntersectionResult : IEquatable<BoundingIntersectionResult>, IRefEquatable<BoundingIntersectionResult>, IPrimitiveValue
  {
    private int m_count;
    private LineIntersectionResult? m_closest;
    private LineIntersectionResult? m_farthest;

    /// <summary>
    /// Gets the number of intersections in the result (0, 1, or 2).
    /// </summary>
    public readonly int IntersectionCount
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return m_count;
      }
    }

    /// <summary>
    /// Gets if the intersection is result (has at least one valid result).
    /// </summary>
    public readonly bool IsValid
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return m_count > 0;
      }
    }

    /// <summary>
    /// Gets the closest intersection record, if it exists.
    /// </summary>
    public readonly LineIntersectionResult? ClosestIntersection
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return m_closest;
      }
    }

    /// <summary>
    /// Gets the farthest intersection record, if it is exists.
    /// </summary>
    public readonly LineIntersectionResult? FarthestIntersection
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return m_farthest;
      }
    }

    /// <summary>
    /// Gets the intersection record by index. 0 = Closest, 1 = Farthest.
    /// </summary>
    /// <param name="index">Zero-based index.</param>
    /// <returns>The corresponding intersection record.</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the index is less than zero or equal to or greater than the intersection count.</exception>
    public readonly LineIntersectionResult this[int index]
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        if (index < 0 || index >= m_count)
          throw new ArgumentOutOfRangeException("index");

        return (index == 0) ? m_closest!.Value : m_farthest!.Value;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="BoundingIntersectionResult"/> struct.
    /// </summary>
    /// <param name="record">An intersection record</param>
    public BoundingIntersectionResult(in LineIntersectionResult record)
    {
      m_closest = record;
      m_farthest = null;
      m_count = 1;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="BoundingIntersectionResult"/> struct.
    /// </summary>
    /// <param name="first">First intersection record</param>
    /// <param name="second">Second intersection record</param>
    public BoundingIntersectionResult(in LineIntersectionResult first, in LineIntersectionResult second)
    {
      if (first.Distance <= second.Distance)
      {
        m_closest = first;
        m_farthest = second;
      }
      else
      {
        m_closest = second;
        m_farthest = first;
      }

      m_count = 2;
    }

    /// <summary>
    /// Constructs a bounding intersection result from potentially two line intersection results.
    /// </summary>
    /// <param name="first">First result which may be null.</param>
    /// <param name="second">Second result which may be null.</param>
    /// <returns>Bounding intersection result.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static BoundingIntersectionResult FromResults(in LineIntersectionResult? first, in LineIntersectionResult? second)
    {
      if (first is not null && second is not null)
      {
        return new BoundingIntersectionResult(first.Value, second.Value);
      } 
      else if (first is not null && second is null)
      {
        return new BoundingIntersectionResult(first.Value);
      }
      else if (first is null && second is not null)
      {
        return new BoundingIntersectionResult(second.Value);
      }

      return new BoundingIntersectionResult();
    }

    /// <summary>
    /// Constructs a bounding intersection result from potentially two line intersection results.
    /// </summary>
    /// <param name="first">First result which may be null.</param>
    /// <param name="second">Second result which may be null.</param>
    /// <param name="result">Bounding intersection result</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void FromResults(in LineIntersectionResult? first, in LineIntersectionResult? second, out BoundingIntersectionResult result)
    {
      if (first is not null && second is not null)
      {
        result = new BoundingIntersectionResult(first.Value, second.Value);
        return;
      }
      else if (first is not null && second is null)
      {
        result = new BoundingIntersectionResult(first.Value);
        return;
      }
      else if (first is null && second is not null)
      {
        result = new BoundingIntersectionResult(second.Value);
        return;
      }
      else
      {
        result = new BoundingIntersectionResult();
        return;
      }
    }

    /// <summary>
    /// Tests equality between two bounding intersection results.
    /// </summary>
    /// <param name="a">First bounding intersection result</param>
    /// <param name="b">Second bounding intersection result</param>
    /// <returns>True if the results are equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator ==(in BoundingIntersectionResult a, in BoundingIntersectionResult b)
    {
      return a.Equals(b, MathHelper.ZeroTolerance);
    }

    /// <summary>
    /// Tests inequality between two bounding intersection results.
    /// </summary>
    /// <param name="a">First bounding intersection result</param>
    /// <param name="b">Second bounding intersection result</param>
    /// <returns>True if the results are not equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator !=(in BoundingIntersectionResult a, in BoundingIntersectionResult b)
    {
      return !a.Equals(b, MathHelper.ZeroTolerance);
    }

    /// <summary>
    /// Gets the intersection record by index. 0 = Closest, 1 = Farthest.
    /// </summary>
    /// <param name="index">Zero-based index.</param>
    /// <returns>The corresponding intersection record.</returns>
    /// <param name="result">Intersection record.</param>
    /// <returns>True if the index was valid and the record is valid, false if otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool GetIntersection(int index, out LineIntersectionResult result)
    {
      if (index < 0 || index >= m_count)
      {
        result = new LineIntersectionResult();
        return false;
      }

      result = (index == 0) ? m_closest!.Value : m_farthest!.Value;

      return true;
    }

    /// <summary>
    /// Indicates whether this instance and a specified object are equal.
    /// </summary>
    /// <param name="obj">Another object to compare to.</param>
    /// <returns>True if <paramref name="obj" /> and this instance are the same type and represent the same value; otherwise, false.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is BoundingIntersectionResult)
        return Equals(obj, MathHelper.ZeroTolerance);

      return false;
    }

    /// <summary>
    /// Tests equality between the bounding intersection result and another.
    /// </summary>
    /// <param name="other">Other result to compare to</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    readonly bool IEquatable<BoundingIntersectionResult>.Equals(BoundingIntersectionResult other)
    {
      return Equals(other, MathHelper.ZeroTolerance);
    }

    /// <summary>
    /// Tests equality between the bounding intersection result and another.
    /// </summary>
    /// <param name="other">Other result to compare to</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Equals(in BoundingIntersectionResult other)
    {
      return Equals(other, MathHelper.ZeroTolerance);
    }

    /// <summary>
    /// Tests equality between the bounding intersection result and another.
    /// </summary>
    /// <param name="other">Other result to compare to</param>
    /// <param name="tolerance">Tolerance</param>
    /// <returns>True if both are equal, false otherwise.</returns>
    public readonly bool Equals(in BoundingIntersectionResult other, float tolerance)
    {
      if (m_count != other.m_count)
        return false;

      bool closestEqual = false;
      bool farthestEqual = false;

      if (m_closest.HasValue && other.m_closest.HasValue)
      {
        LineIntersectionResult cR = other.m_closest.Value;
        closestEqual = m_closest.Value.Equals(cR, tolerance);
      }
      else
      {
        closestEqual = !m_closest.HasValue && !other.m_closest.HasValue;
      }

      if (m_farthest.HasValue && other.m_farthest.HasValue)
      {
        LineIntersectionResult fR = other.m_farthest.Value;
        farthestEqual = m_farthest.Value.Equals(fR, tolerance);
      }
      else
      {
        farthestEqual = !m_farthest.HasValue && !other.m_farthest.HasValue;
      }

      return closestEqual && farthestEqual;
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.</returns>
    public readonly override int GetHashCode()
    {
      unchecked
      {
        int closestHash = (m_closest.HasValue) ? m_closest.Value.GetHashCode() : 0;
        int farthestHash = (m_farthest.HasValue) ? m_farthest.Value.GetHashCode() : 0;

        return m_count.GetHashCode() + closestHash + farthestHash;
      }
    }

    /// <summary>
    /// Returns the fully qualified type name of this instance.
    /// </summary>
    /// <returns>A <see cref="T:System.String" /> containing a fully qualified type name.</returns>
    public readonly override string ToString()
    {
      CultureInfo info = CultureInfo.CurrentCulture;
      return String.Format(info, "{{Count: {0} HasClosest: {1} HasFarthest: {2}}}",
          new Object[] { m_count.ToString(info), m_closest.HasValue.ToString(info), m_farthest.HasValue.ToString(info) });
    }

    /// <summary>
    /// Writes the primitive data to the output.
    /// </summary>
    /// <param name="output">Primitive writer.</param>
    public readonly void Write(IPrimitiveWriter output)
    {
      output.Write("Count", m_count);
      output.Write<LineIntersectionResult>("Closest", m_closest);
      output.Write<LineIntersectionResult>("Farthest", m_farthest);
    }

    /// <summary>
    /// Reads the primitive data from the input.
    /// </summary>
    /// <param name="input">Primitive reader.</param>
    public void Read(IPrimitiveReader input)
    {
      m_count = input.ReadInt32("Count");
      input.ReadNullable<LineIntersectionResult>("Closest", out m_closest);
      input.ReadNullable<LineIntersectionResult>("Farthest", out m_farthest);
    }
  }
}
