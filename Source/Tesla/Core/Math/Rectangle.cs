﻿/*
* Copyright (c) 2010-2024 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Tesla.Content;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Defines a 2D rectangle in a plane where the positive X axis is right and positive Y axis is down.
  /// </summary>
  [Serializable]
  [StructLayout(LayoutKind.Sequential)]
  public struct Rectangle : IEquatable<Rectangle>, IRefEquatable<Rectangle>, IPrimitiveValue
  {
    /// <summary>
    /// Top left X coordinate of the rectangle.
    /// </summary>
    public int X;

    /// <summary>
    /// Top left Y coordinate of the rectangle.
    /// </summary>
    public int Y;

    /// <summary>
    /// Width of the rectangle.
    /// </summary>
    public int Width;

    /// <summary>
    /// Height of the rectangle
    /// </summary>
    public int Height;

    private static readonly Rectangle s_empty = new Rectangle();
    private static readonly int s_sizeInBytes = BufferHelper.SizeOf<Rectangle>();

    /// <summary>
    /// Gets the empty rectangle, which is a rectangle that has no area.
    /// </summary>
    public static ref readonly Rectangle Empty
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_empty;
      }
    }

    /// <summary>
    /// Gets the size of a rectangle type in bytes.
    /// </summary>
    public static int SizeInBytes
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return s_sizeInBytes;
      }
    }

    /// <summary>
    /// Gets the center of the rectangle.
    /// </summary>
    public readonly Int2 Center
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        Int2 center;
        center.X = X + (Width / 2);
        center.Y = Y + (Height / 2);

        return center;
      }
    }

    /// <summary>
    /// Gets the top left point of the rectangle.
    /// </summary>
    public readonly Int2 TopLeftPoint
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        Int2 topLeft;
        topLeft.X = X;
        topLeft.Y = Y;

        return topLeft;
      }
    }

    /// <summary>
    /// Gets the top right point of the rectangle.
    /// </summary>
    public readonly Int2 TopRightPoint
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        Int2 topRight;
        topRight.X = X + Width;
        topRight.Y = Y;

        return topRight;
      }
    }

    /// <summary>
    /// Gets the bottom left point of the rectangle.
    /// </summary>
    public readonly Int2 BottomLeftPoint
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        Int2 bottomLeft;
        bottomLeft.X = X;
        bottomLeft.Y = Y + Height;

        return bottomLeft;
      }
    }

    /// <summary>
    /// Gets the bottom right point of the rectangle.
    /// </summary>
    public readonly Int2 BottomRightPoint
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        Int2 bottomRight;
        bottomRight.X = X + Width;
        bottomRight.Y = Y + Height;

        return bottomRight;
      }
    }

    /// <summary>
    /// Gets the left-most X coordinate.
    /// </summary>
    public int Left
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      readonly get
      {
        return X;
      }
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      set
      {
        X = value;
      }
    }

    /// <summary>
    /// Gets or sets the right-most X coordinate (Left + Width).
    /// </summary>
    public int Right
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      readonly get
      {
        return X + Width;
      }
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      set
      {
        Width = value - X;
      }
    }

    /// <summary>
    /// Gets or sets the top-most Y coordinate.
    /// </summary>
    public int Top
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      readonly get
      {
        return Y;
      }
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      set
      {
        Y = value;
      }
    }

    /// <summary>
    /// Gets or sets the bottom-most Y coordinate (Top + Height).
    /// </summary>
    public int Bottom
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      readonly get
      {
        return Y + Height;
      }
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      set
      {
        Height = value - Y;
      }
    }

    /// <summary>
    /// Gets if t he current rectangle is the empty rectangle, where the top left coordinate and width/height are all zero, and thus
    /// define a rectangle that has no area.
    /// </summary>
    public readonly bool IsEmpty
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return X == 0 && Y == 0 && Width == 0 && Height == 0;
      }
    }

    /// <summary>
    /// Gets whether any of the components of the rectangle are NaN (Not A Number).
    /// </summary>
    public readonly bool IsNaN
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return float.IsNaN(X) || float.IsNaN(Y) || float.IsNaN(Width) || float.IsNaN(Height);
      }
    }

    /// <summary>
    /// Gets whether any of the components of the rectangle are positive or negative infinity.
    /// </summary>
    public readonly bool IsInfinity
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return float.IsNegativeInfinity(X) || float.IsPositiveInfinity(X) || float.IsNegativeInfinity(Y) || float.IsPositiveInfinity(Y) ||
            float.IsNegativeInfinity(Width) || float.IsPositiveInfinity(Width) || float.IsNegativeInfinity(Height) || float.IsNegativeInfinity(Height);
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Rectangle"/> struct.
    /// </summary>
    /// <param name="x">The X coordinate of the top left point of the rectangle.</param>
    /// <param name="y">The Y coordinate of the top left point of the rectangle.</param>
    /// <param name="width">The width of the rectangle.</param>
    /// <param name="height">The height of the rectangle.</param>
    public Rectangle(int x, int y, int width, int height)
    {
      X = x;
      Y = y;
      Width = width;
      Height = height;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Rectangle"/> struct.
    /// </summary>
    /// <param name="topLeft">The top left point of the rectangle</param>
    /// <param name="width">The width of the rectangle.</param>
    /// <param name="height">The height of the rectangle.</param>
    public Rectangle(in Int2 topLeft, int width, int height)
    {
      X = topLeft.X;
      Y = topLeft.Y;
      Width = width;
      Height = height;
    }


    /// <summary>
    /// Constructs a new instance of the <see cref="Rectangle"/> struct.
    /// </summary>
    /// <param name="topLeft">The top left point of the rectangle</param>
    /// <param name="widthHeight">The width (X) and height (Y) of the rectangle.</param>
    public Rectangle(in Int2 topLeft, in Int2 widthHeight)
    {
      X = topLeft.X;
      Y = topLeft.Y;
      Width = widthHeight.X;
      Height = widthHeight.Y;
    }

    /// <summary>
    /// Creates a rectangle from two points that represent the top-left corner and the bottom-right corner.
    /// </summary>
    /// <param name="topLeft">X contains the left most value. Y contains the top most value.</param>
    /// <param name="bottomRight">X contains the right most value. Y contains the bottom most value.</param>
    /// <returns>Resulting rectangle</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Rectangle FromPoints(in Int2 topLeft, in Int2 bottomRight)
    {
      return new Rectangle(topLeft.X, topLeft.Y, bottomRight.X - topLeft.X, bottomRight.Y - topLeft.Y);
    }

    /// <summary>
    /// Creates a rectangle from two points that represent the top-left corner and the bottom-right corner.
    /// </summary>
    /// <param name="topLeft">X contains the left most value. Y contains the top most value.</param>
    /// <param name="bottomRight">X contains the right most value. Y contains the bottom most value.</param>
    /// <param name="result">Resulting rectangle</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void FromPoints(in Int2 topLeft, in Int2 bottomRight, out Rectangle result)
    {
      result = new Rectangle(topLeft.X, topLeft.Y, bottomRight.X - topLeft.X, bottomRight.Y - topLeft.Y);
    }

    /// <summary>
    /// Creates a rectangle from extent values rather than XY, Width, Height. Width and height are calculated
    /// from right-left and bottom-height respectively.
    /// </summary>
    /// <param name="left">Left most X value</param>
    /// <param name="top">Top most Y value</param>
    /// <param name="right">Right most X value</param>
    /// <param name="bottom">Bottom most Y value</param>
    /// <returns>Resulting rectangle</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Rectangle FromExtents(int left, int top, int right, int bottom)
    {
      return new Rectangle(left, top, right - left, bottom - top);
    }

    /// <summary>
    /// Creates a rectangle from extent values rather than XY, Width, Height. Width and height are calculated
    /// from right-left and bottom-height respectively.
    /// </summary>
    /// <param name="left">Left most X value</param>
    /// <param name="top">Top most Y value</param>
    /// <param name="right">Right most X value</param>
    /// <param name="bottom">Bottom most Y value</param>
    /// <param name="result">Resulting rectangle</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void FromExtents(int left, int top, int right, int bottom, out Rectangle result)
    {
      result = new Rectangle(left, top, right - left, bottom - top);
    }

    /// <summary>
    /// Creates a rectangle that represents the intersection (overlap) of two rectangles. This may return an empty triangle.
    /// </summary>
    /// <param name="a">First rectangle.</param>
    /// <param name="b">Second rectangle.</param>
    /// <returns>The rectangle that represents the intersection.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Rectangle Intersect(in Rectangle a, in Rectangle b)
    {
      int x = (a.X > b.X) ? a.X : b.X;
      int y = (a.Y > b.Y) ? a.Y : b.Y;

      int right1 = a.Right;
      int right2 = b.Right;

      int bot1 = a.Bottom;
      int bot2 = b.Bottom;

      int right = (right1 < right2) ? right1 : right2;
      int bottom = (bot1 < bot2) ? bot1 : bot2;

      if ((right > x) && (bottom > y))
        return new Rectangle(x, y, right - x, bottom - y);
      
      return Empty;
    }

    /// <summary>
    /// Creates a rectangle that represents the intersection (overlap) of two rectangles. This may return an empty triangle.
    /// </summary>
    /// <param name="a">First rectangle.</param>
    /// <param name="b">Second rectangle.</param>
    /// <param name="result">The rectangle that represents the intersection.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Intersect(in Rectangle a, in Rectangle b, out Rectangle result)
    {
      int x = (a.X > b.X) ? a.X : b.X;
      int y = (a.Y > b.Y) ? a.Y : b.Y;

      int right1 = a.Right;
      int right2 = b.Right;

      int bot1 = a.Bottom;
      int bot2 = b.Bottom;

      int right = (right1 < right2) ? right1 : right2;
      int bottom = (bot1 < bot2) ? bot1 : bot2;

      if ((right > x) && (bottom > y))
      {
        result = new Rectangle(x, y, right - x, bottom - y);
      }
      else
      {
        result = Empty;
      }
    }

    /// <summary>
    /// Creates a rectangle that is the union of the two rectangles.
    /// </summary>
    /// <param name="a">First rectangle.</param>
    /// <param name="b">Second rectangle.</param>
    /// <returns>The union of the two rectangles.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Rectangle Union(in Rectangle a, in Rectangle b)
    {
      int x = (a.X < b.X) ? a.X : b.X;
      int y = (a.Y < b.Y) ? a.Y : b.Y;

      int right1 = a.Right;
      int right2 = b.Right;

      int bot1 = a.Bottom;
      int bot2 = b.Bottom;

      int right = (right1 > right2) ? right1 : right2;
      int bot = (bot1 > bot2) ? bot1 : bot2;

      return new Rectangle(x, y, right - x, bot - y);
    }

    /// <summary>
    /// Creates a rectangle that is the union of the two rectangles.
    /// </summary>
    /// <param name="a">First rectangle.</param>
    /// <param name="b">Second rectangle.</param>
    /// <param name="result">The union of the two rectangles</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Union(in Rectangle a, in Rectangle b, out Rectangle result)
    {
      int x = (a.X < b.X) ? a.X : b.X;
      int y = (a.Y < b.Y) ? a.Y : b.Y;
      int right1 = a.Right;
      int right2 = b.Right;

      int bot1 = a.Bottom;
      int bot2 = b.Bottom;

      int right = (right1 > right2) ? right1 : right2;
      int bot = (bot1 > bot2) ? bot1 : bot2;

      result = new Rectangle(x, y, right - x, bot - y);
    }

    /// <summary>
    /// Tests equality between two rectangles.
    /// </summary>
    /// <param name="a">First rectangle</param>
    /// <param name="b">Second rectangle</param>
    /// <returns>True if equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator ==(Rectangle a, Rectangle b)
    {
      return (a.X == b.X) && (a.Y == b.Y) && (a.Width == b.Width) && (a.Height == b.Height);
    }

    /// <summary>
    /// Tests inequality between two rectangles.
    /// </summary>
    /// <param name="a">First rectangle</param>
    /// <param name="b">Second rectangle</param>
    /// <returns>True if not equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator !=(Rectangle a, Rectangle b)
    {
      return (a.X != b.X) || (a.Y != b.Y) || (a.Width != b.Width) || (a.Height != b.Height);
    }

    /// <summary>
    /// Translates the rectangle's top left location by the supplied amount.
    /// </summary>
    /// <param name="amt">Amount to translate along X,Y axis.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void Translate(in Int2 amt)
    {
      X += amt.X;
      Y += amt.Y;
    }

    /// <summary>
    /// Translates the rectangle's top left location by the supplied
    /// amount.
    /// </summary>
    /// <param name="xAmt">Amount to translate along X axis.</param>
    /// <param name="yAmt">Amount to translate along Y axis.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void Translate(int xAmt, int yAmt)
    {
      X += xAmt;
      Y += yAmt;
    }

    /// <summary>
    /// Extends the rectangle along each axis by the supplied amount. The x value pushes the left/right sides outward, and the y value pushes the top/bottom sides outward.
    /// </summary>
    /// <param name="amt">Amount to extend along X, Y axis</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void Inflate(in Int2 amt)
    {
      X -= amt.X;
      Y -= amt.Y;

      Width += amt.X * 2;
      Height += amt.Y * 2;
    }

    /// <summary>
    /// Extends the rectangle along each axis by the supplied amount. The x value pushes the left/right sides outward, and the y value pushes the top/bottom sides outward.
    /// </summary>
    /// <param name="xAmt">Amount to extend along X axis</param>
    /// <param name="yAmt">Amount to extend along Y axis</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void Inflate(int xAmt, int yAmt)
    {
      X -= xAmt;
      Y -= yAmt;

      Width += xAmt * 2;
      Height += yAmt * 2;
    }

    /// <summary>
    /// Extends the rectangle along each axis by the supplied amount. The x value pushes the left/right sides outward, and the y value pushes the top/bottom sides outward.
    /// </summary>
    /// <param name="amt">Amount to extend along X, Y axis</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void Inflate(int amt)
    {
      X -= amt;
      Y -= amt;

      Width += amt * 2;
      Height += amt * 2;
    }

    /// <summary>
    /// Tests if the point is inside the rectangle.
    /// </summary>
    /// <param name="pt">Point to test</param>
    /// <returns>True if the point is contained by the rectangle, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Contains(in Int2 pt)
    {
      return (X <= pt.X) && (pt.X < (X + Width)) && (Y <= pt.Y) && (pt.Y < (Y + Height));
    }

    /// <summary>
    /// Tests of the XY point is inside the rectangle.
    /// </summary>
    /// <param name="x">X coordinate of the point</param>
    /// <param name="y">Y coordinate of the point</param>
    /// <returns>True if the point is contained by the rectangle, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Contains(int x, int y)
    {
      return (X <= x) && (x < (X + Width)) && (Y <= y) && (y < (Y + Height));
    }

    /// <summary>
    /// Tests if the specified rectangle is contained inside the current rectangle.
    /// </summary>
    /// <param name="rect">Rectangle to test</param>
    /// <returns>True if the rectangle is contained by the rectangle, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Contains(in Rectangle rect)
    {
      return (X <= rect.X) && ((rect.X + rect.Width) <= (X + Width)) && (Y <= rect.Y) && ((rect.Y + rect.Height) <= (Y + Height));
    }

    /// <summary>
    /// Tests if the specified rectangle intersects with the current rectangle.
    /// </summary>
    /// <param name="rect">Rectangle to test</param>
    /// <returns>True if the rectangle intersects with the current rectangle, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Intersects(in Rectangle rect)
    {
      return (rect.X < (X + Width)) && (X < (rect.X + rect.Width)) && (rect.Y < (Y + Height)) && (Y < (rect.Y + rect.Height));
    }

    /// <summary>
    /// Determines whether the specified <see cref="System.Object" /> is equal to this instance.
    /// </summary>
    /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
    /// <returns>True if the specified <see cref="System.Object" /> is equal to this instance; otherwise, false.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is Rectangle)
        return Equals((Rectangle) obj);

      return false;
    }

    /// <summary>
    /// Tests equality between the rectangle and another rectangle.
    /// </summary>
    /// <param name="other">Rectangle to test</param>
    /// <returns>True if equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    readonly bool IEquatable<Rectangle>.Equals(Rectangle other)
    {
      return (X == other.X) && (Y == other.Y) && (Width == other.Width) && (Height == other.Height);
    }

    /// <summary>
    /// Tests equality between the rectangle and another rectangle.
    /// </summary>
    /// <param name="other">Rectangle to test</param>
    /// <returns>True if equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Equals(in Rectangle other)
    {
      return (X == other.X) && (Y == other.Y) && (Width == other.Width) && (Height == other.Height);
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.</returns>
    public readonly override int GetHashCode()
    {
      unchecked
      {
        return X.GetHashCode() + Y.GetHashCode() + Width.GetHashCode() + Height.GetHashCode();
      }
    }

    /// <summary>
    /// Returns the fully qualified type name of this instance.
    /// </summary>
    /// <returns>A <see cref="T:System.String" /> containing a fully qualified type name.</returns>
    public readonly override string ToString()
    {
      CultureInfo info = CultureInfo.CurrentCulture;
      return String.Format(info, "X:{0} Y:{1} Width:{2} Height:{3}",
          new Object[] { X.ToString(info), Y.ToString(info), Width.ToString(info), Height.ToString(info) });
    }

    /// <summary>
    /// Writes the primitive data to the output.
    /// </summary>
    /// <param name="output">Primitive writer.</param>
    readonly void IPrimitiveValue.Write(IPrimitiveWriter output)
    {
      output.Write("X", X);
      output.Write("Y", Y);
      output.Write("Width", Width);
      output.Write("Height", Height);
    }

    /// <summary>
    /// Reads the primitive data from the input.
    /// </summary>
    /// <param name="input">Primitive reader.</param>
    void IPrimitiveValue.Read(IPrimitiveReader input)
    {
      X = input.ReadInt32("X");
      Y = input.ReadInt32("Y");
      Width = input.ReadInt32("Width");
      Height = input.ReadInt32("Height");
    }
  }
}
