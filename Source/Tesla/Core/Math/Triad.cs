﻿/*
* Copyright (c) 2010-2024 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Tesla.Content;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Defines a grouping of three orthonormal axes that make a coordinate system.
  /// </summary>
  [Serializable]
  [StructLayout(LayoutKind.Sequential, Pack = 4)]
  public struct Triad : IEquatable<Triad>, IRefEquatable<Triad>, IFormattable, IPrimitiveValue
  {
    /// <summary>
    /// X-Axis of the coordinate system.
    /// </summary>
    public Vector3 XAxis;

    /// <summary>
    /// Y-Axis of the coordinate system.
    /// </summary>
    public Vector3 YAxis;

    /// <summary>
    /// Z-Axis of the coordinate system.
    /// </summary>
    public Vector3 ZAxis;

    private static readonly Triad s_unitAxes = new Triad(Vector3.UnitX, Vector3.UnitY, Vector3.UnitZ);
    private static readonly Triad s_unitAxesNegativeZ = new Triad(Vector3.UnitX, Vector3.UnitY, new Vector3(0, 0, -1));

    private static readonly int s_sizeInbytes = BufferHelper.SizeOf<Triad>();

    /// <summary>
    /// Gets the standard or canonical basis where the XAxis = {1, 0, 0}, YAxis = {0, 1, 0}, ZAxis = {0, 0, 1}.
    /// </summary>
    public static ref readonly Triad UnitAxes
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_unitAxes;
      }
    }

    /// <summary>
    /// Gets the standard Right, Up, Forward axes in a +Y up right handed coordinate system, where +X is Right, +Y is Up, and -Z is forward.
    /// </summary>
    public static ref readonly Triad UnitAxesNegativeZ
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return ref s_unitAxesNegativeZ;
      }
    }

    /// <summary>
    /// Gets the size of the <see cref="Triad"/> type in bytes.
    /// </summary>
    public static int SizeInBytes
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return s_sizeInbytes;
      }
    }

    /// <summary>
    /// Gets or sets individual axes of the triad in the order of {X, Y, Z} axes.
    /// </summary>
    /// <param name="index">Zero-based index, in range of [0, 2].</param>
    /// <returns>The value of the specified axis.</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the index is out of range [0, 2]</exception>
    public Vector3 this[int index]
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      readonly get
      {
        switch (index)
        {
          case 0:
            return XAxis;
          case 1:
            return YAxis;
          case 2:
            return ZAxis;
          default:
            throw new ArgumentOutOfRangeException(nameof(index), StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));
        }
      }
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      set
      {
        switch (index)
        {
          case 0:
            XAxis = value;
            break;
          case 1:
            YAxis = value;
            break;
          case 2:
            ZAxis = value;
            break;
          default:
            throw new ArgumentOutOfRangeException(nameof(index), StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));
        }
      }
    }

    /// <summary>
    /// Gets whether the axes are normalized.
    /// </summary>
    public readonly bool IsNormalized
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return XAxis.IsNormalized && YAxis.IsNormalized && ZAxis.IsNormalized;
      }
    }

    /// <summary>
    /// Gets whether any of the axes are degenerate (equal to zero).
    /// </summary>
    public readonly bool IsDegenerate
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return XAxis.IsAlmostZero() || YAxis.IsAlmostZero() || ZAxis.IsAlmostZero();
      }
    }

    /// <summary>
    /// Gets whether any of the components of the axes are NaN (Not A Number).
    /// </summary>
    public readonly bool IsNaN
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return XAxis.IsNaN || YAxis.IsNaN || ZAxis.IsNaN;
      }
    }

    /// <summary>
    /// Gets whether any of the components of the axes are positive or negative infinity.
    /// </summary>
    public readonly bool IsInfinity
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        return XAxis.IsInfinity || YAxis.IsInfinity || ZAxis.IsInfinity;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Triad"/> struct.
    /// </summary>
    /// <param name="xAxis">X-Axis of the coordinate system.</param>
    /// <param name="yAxis">Y-Axis of the coordinate system.</param>
    /// <param name="zAxis">Z-Axis of the coordinate system.</param>
    public Triad(in Vector3 xAxis, in Vector3 yAxis, in Vector3 zAxis)
    {
      XAxis = xAxis;
      YAxis = yAxis;
      ZAxis = zAxis;
    }

    /// <summary>
    /// Creates a triad from a set of three axes.
    /// </summary>
    /// <param name="xAxis">X-Axis of the coordinate system.</param>
    /// <param name="yAxis">Y-Axis of the coordinate system.</param>
    /// <param name="zAxis">Z-Axis of the coordinate system.</param>
    /// <returns>The triad.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Triad FromAxes(in Vector3 xAxis, in Vector3 yAxis, in Vector3 zAxis)
    {
      Triad triad;
      triad.XAxis = xAxis;
      triad.YAxis = yAxis;
      triad.ZAxis = zAxis;

      return triad;
    }

    /// <summary>
    /// Creates a triad from a set of three axes.
    /// </summary>
    /// <param name="xAxis">X-Axis of the coordinate system.</param>
    /// <param name="yAxis">Y-Axis of the coordinate system.</param>
    /// <param name="zAxis">Z-Axis of the coordinate system.</param>
    /// <param name="triad">The triad.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void FromAxes(in Vector3 xAxis, in Vector3 yAxis, in Vector3 zAxis, out Triad triad)
    {
      triad.XAxis = xAxis;
      triad.YAxis = yAxis;
      triad.ZAxis = zAxis;
    }

    /// <summary>
    /// Computes an orthonormal basis from a single vector.
    /// </summary>
    /// <param name="zAxis">Z axis to form an orthonormal basis to.</param>
    /// <returns>Orthonormal basis</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Triad FromZComplementBasis(in Vector3 zAxis)
    {
      Triad result;
      result.ZAxis = zAxis;
      Vector3.ComplementBasis(ref result.ZAxis, out result.XAxis, out result.YAxis);

      return result;
    }

    /// <summary>
    /// Computes an orthonormal basis from a single vector.
    /// </summary>
    /// <param name="zAxis">Z axis to form an orthonormal basis to.</param>
    /// <param name="result">Orthonormal basis</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void FromZComplementBasis(in Vector3 zAxis, out Triad result)
    {
      result.ZAxis = zAxis;
      Vector3.ComplementBasis(ref result.ZAxis, out result.XAxis, out result.YAxis);
    }

    /// <summary>
    /// Computes an orthonormal basis from a single vector.
    /// </summary>
    /// <param name="yAxis">Y axis to form an orthonormal basis to.</param>
    /// <returns>Orthonormal basis</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Triad FromYComplementBasis(in Vector3 yAxis)
    {
      Triad result;
      result.YAxis = yAxis;
      Vector3.ComplementBasis(ref result.YAxis, out result.XAxis, out result.ZAxis);

      return result;
    }

    /// <summary>
    /// Computes an orthonormal basis from a single vector.
    /// </summary>
    /// <param name="yAxis">Y axis to form an orthonormal basis to.</param>
    /// <param name="result">Orthonormal basis</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void FromYComplementBasis(in Vector3 yAxis, out Triad result)
    {
      result.YAxis = yAxis;
      Vector3.ComplementBasis(ref result.YAxis, out result.XAxis, out result.ZAxis);
    }

    /// <summary>
    /// Transfroms the specified axes by the given <see cref="Quaternion"/>.
    /// </summary>
    /// <param name="axes">Axes to transform.</param>
    /// <param name="rotation">Rotation quaternion.</param>
    /// <returns>Transformed axes.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Triad Transform(in Triad axes, in Quaternion rotation)
    {
      Triad result;
      Vector3.Transform(axes.XAxis, rotation, out result.XAxis);
      Vector3.Transform(axes.YAxis, rotation, out result.YAxis);
      Vector3.Transform(axes.ZAxis, rotation, out result.ZAxis);

      return result;
    }

    /// <summary>
    /// Transfroms the specified axes by the given <see cref="Quaternion"/>.
    /// </summary>
    /// <param name="axes">Axes to transform.</param>
    /// <param name="rotation">Rotation quaternion.</param>
    /// <param name="result">Transformed axes.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Transform(in Triad axes, in Quaternion rotation, out Triad result)
    {
      Vector3.Transform(axes.XAxis, rotation, out result.XAxis);
      Vector3.Transform(axes.YAxis, rotation, out result.YAxis);
      Vector3.Transform(axes.ZAxis, rotation, out result.ZAxis);
    }

    /// <summary>
    /// Transforms the specified axes by the given <see cref="Matrix"/>. This does not use the fourth
    /// row and column, meaning the translation component is ignored. If scaling, then axes will need to be
    /// renormalized.
    /// </summary>
    /// <param name="axes">Axes to transform.</param>
    /// <param name="scaleRotation">Scale-Rotation matrix.</param>
    /// <returns>Transformed axes.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Triad Transform(in Triad axes, in Matrix scaleRotation)
    {
      Triad result;
      Vector3.TransformNormal(axes.XAxis, scaleRotation, out result.XAxis);
      Vector3.TransformNormal(axes.YAxis, scaleRotation, out result.YAxis);
      Vector3.TransformNormal(axes.ZAxis, scaleRotation, out result.ZAxis);

      return result;
    }

    /// <summary>
    /// Transforms the specified axes by the given <see cref="Matrix"/>. This does not use the fourth
    /// row and column, meaning the translation component is ignored. If scaling, then axes will need to be
    /// renormalized.
    /// </summary>
    /// <param name="axes">Axes to transform.</param>
    /// <param name="scaleRotation">Scale-Rotation matrix.</param>
    /// <param name="result">Transformed axes.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Transform(in Triad axes, in Matrix scaleRotation, out Triad result)
    {
      Vector3.TransformNormal(axes.XAxis, scaleRotation, out result.XAxis);
      Vector3.TransformNormal(axes.YAxis, scaleRotation, out result.YAxis);
      Vector3.TransformNormal(axes.ZAxis, scaleRotation, out result.ZAxis);
    }

    /// <summary>
    /// Normalizes the axes.
    /// </summary>
    /// <param name="axes">Axes to normalize.</param>
    /// <returns>Normalized axes.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Triad Normalize(in Triad axes)
    {
      Triad result;
      Vector3.Normalize(axes.XAxis, out result.XAxis);
      Vector3.Normalize(axes.YAxis, out result.YAxis);
      Vector3.Normalize(axes.ZAxis, out result.ZAxis);

      return result;
    }

    /// <summary>
    /// Normalizes the axes.
    /// </summary>
    /// <param name="axes">Axes to normalize.</param>
    /// <param name="result">Normalized axes.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Normalize(in Triad axes, out Triad result)
    {
      Vector3.Normalize(axes.XAxis, out result.XAxis);
      Vector3.Normalize(axes.YAxis, out result.YAxis);
      Vector3.Normalize(axes.ZAxis, out result.ZAxis);
    }

    /// <summary>
    /// Tests equality between two triads.
    /// </summary>
    /// <param name="a">First triad</param>
    /// <param name="b">Second triad</param>
    /// <returns>True if the points of the two triad are equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator ==(in Triad a, in Triad b)
    {
      return a.XAxis.Equals(b.XAxis) && a.Equals(b.YAxis) && a.Equals(b.ZAxis);
    }

    /// <summary>
    /// Tests inequality between two triads.
    /// </summary>
    /// <param name="a">First triad</param>
    /// <param name="b">Second triad</param>
    /// <returns>True if the points of the two triads are equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator !=(in Triad a, in Triad b)
    {
      return !a.XAxis.Equals(b.XAxis) || !a.Equals(b.YAxis) || !a.Equals(b.ZAxis);
    }

    /// <summary>
    /// Normalizes the axes.
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void Normalize()
    {
      XAxis.Normalize();
      YAxis.Normalize();
      ZAxis.Normalize();
    }

    /// <summary>
    /// Get individual axes of the triad in the order of {X, Y, Z} axes.
    /// </summary>
    /// <param name="index">Zero-based index.</param>
    /// <param name="axis">The value of the specified axis.</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the index is out of range [0, 2]</exception>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly void GetAxis(int index, out Vector3 axis)
    {
      switch (index)
      {
        case 0:
          axis = XAxis;
          break;
        case 1:
          axis = YAxis;
          break;
        case 2:
          axis = ZAxis;
          break;
        default:
          throw new ArgumentOutOfRangeException(StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));
      }
    }

    /// <summary>
    /// Set individual axes of the triad in the order of {X, Y, Z} axes.
    /// </summary>
    /// <param name="index">Zero-based index.</param>
    /// <param name="axis">The value of the specified axis to set.</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the index is out of range [0, 2]</exception>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void SetAxis(int index, in Vector3 axis)
    {
      switch (index)
      {
        case 0:
          XAxis = axis;
          break;
        case 1:
          YAxis = axis;
          break;
        case 2:
          ZAxis = axis;
          break;
        default:
          throw new ArgumentOutOfRangeException(StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));
      }
    }

    /// <summary>
    /// Computes the determinant of the basis.
    /// </summary>
    /// <returns>Determinant</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly float ComputeDeterminant()
    {
      float m11 = XAxis.X;
      float m12 = XAxis.Y;
      float m13 = XAxis.Z;
      float m21 = YAxis.X;
      float m22 = YAxis.Y;
      float m23 = YAxis.Z;
      float m31 = ZAxis.X;
      float m32 = ZAxis.Y;
      float m33 = ZAxis.Z;

      float h1 = m33;
      float h2 = m32;
      float h4 = m31;
      return ((((m11 * (((m22 * h1) - (m23 * h2)))) - (m12 * (((m21 * h1) - (m23 * h4))))) + (m13 * (((m21 * h2) - (m22 * h4))))));
    }

    /// <summary>
    /// Determines whether the specified <see cref="System.Object" /> is equal to this instance.
    /// </summary>
    /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
    /// <returns>True if the specified <see cref="System.Object" /> is equal to this instance; otherwise, false.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is Triad)
        return Equals((Triad) obj);

      return false;
    }

    /// <summary>
    /// Tests equality between this triad and another.
    /// </summary>
    /// <param name="other">triad</param>
    /// <returns>True if equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    readonly bool IEquatable<Triad>.Equals(Triad other)
    {
      return XAxis.Equals(other.XAxis) && YAxis.Equals(other.YAxis) && ZAxis.Equals(other.ZAxis);
    }

    /// <summary>
    /// Tests equality between this triad and another.
    /// </summary>
    /// <param name="other">triad</param>
    /// <returns>True if equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Equals(in Triad other)
    {
      return XAxis.Equals(other.XAxis) && YAxis.Equals(other.YAxis) && ZAxis.Equals(other.ZAxis);
    }

    /// <summary>
    /// Tests equality between this triad and another.
    /// </summary>
    /// <param name="other">triad</param>
    /// <param name="tolerance">Tolerance</param>
    /// <returns>True if equal, false otherwise.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Equals(in Triad other, float tolerance)
    {
      return XAxis.Equals(other.XAxis, tolerance) && YAxis.Equals(other.YAxis, tolerance) && ZAxis.Equals(other.ZAxis, tolerance);
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
    public readonly override int GetHashCode()
    {
      unchecked
      {
        return XAxis.GetHashCode() + YAxis.GetHashCode() + ZAxis.GetHashCode();
      }
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly override string ToString()
    {
      CultureInfo info = CultureInfo.CurrentCulture;
      return String.Format(info, "XAxis: [{0}], YAxis: [{1}], ZAxis: [{2}]",
          new Object[] { XAxis.ToString(info), YAxis.ToString(info), ZAxis.ToString(info) });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="format">The format</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(string? format)
    {
      if (format is null)
        return ToString();

      CultureInfo info = CultureInfo.CurrentCulture;
      return String.Format(info, "XAxis: [{0}], YAxis: [{1}], ZAxis: [{2}]",
          new Object[] { XAxis.ToString(format, info), YAxis.ToString(format, info), ZAxis.ToString(format, info) });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="formatProvider">The format provider.</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(IFormatProvider? formatProvider)
    {
      if (formatProvider is null)
        return ToString();

      return String.Format(formatProvider, "XAxis: [{0}], YAxis: [{1}], ZAxis: [{2}]",
          new Object[] { XAxis.ToString(formatProvider), YAxis.ToString(formatProvider), ZAxis.ToString(formatProvider) });
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <param name="format">The format.</param>
    /// <param name="formatProvider">The format provider.</param>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly string ToString(string? format, IFormatProvider? formatProvider)
    {
      if (formatProvider is null)
        return ToString();

      if (format is null)
        return ToString(formatProvider);

      return String.Format(formatProvider, "XAxis: [{0}], YAxis: [{1}], ZAxis: [{2}]",
          new Object[] { XAxis.ToString(format, formatProvider), YAxis.ToString(format, formatProvider), ZAxis.ToString(format, formatProvider) });
    }

    /// <summary>
    /// Writes the primitive data to the output.
    /// </summary>
    /// <param name="output">Primitive writer.</param>
    readonly void IPrimitiveValue.Write(IPrimitiveWriter output)
    {
      output.Write<Vector3>("XAxis", XAxis);
      output.Write<Vector3>("YAxis", YAxis);
      output.Write<Vector3>("ZAxis", ZAxis);
    }

    /// <summary>
    /// Reads the primitive data from the input.
    /// </summary>
    /// <param name="input">Primitive reader.</param>
    void IPrimitiveValue.Read(IPrimitiveReader input)
    {
      input.Read<Vector3>("XAxis", out XAxis);
      input.Read<Vector3>("YAxis", out YAxis);
      input.Read<Vector3>("ZAxis", out ZAxis);
    }
  }
}
