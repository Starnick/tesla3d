/*
* Copyright (c) 2010-2017 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;

namespace Tesla
{
    /// <summary>
    /// Provides implementations of common Array functions using readonly-ref arguments to avoid unnecessary copying. If a type implements <see cref="IComparable{T}"/> or <see cref="IEquatable{T}"/>, then
    /// those types are also handled implicitly.
    /// </summary>
    public static class RefArray
    {
        /// <summary>
        /// Searches the entire sorted <see cref="Array"/> for an element that corresponds to the
        /// specified value using the specified comparer, and returns the zero-based index of the element in the array, if it exists.
        /// </summary>
        /// <typeparam name="T">The type of the elements of the array.</typeparam>
        /// <param name="array">The zero-based array to search.</param> 
        /// <param name="value">An object to locate in the <see cref="Array"/>.</param>
        /// <returns>The zero-based index of <paramref name="value"/> in the sorted <see cref="Array"/>, if it is found, otherwise, a negative
        /// number that is the bitwise complement of the index of the next element that is larger than <paramref name="value"/> or if there is none, the bitwise complement of the index of the last element plus 1.</returns>
        /// <exception cref="ArgumentNullException">Thrown if <paramref name="array"/> is null.</exception>
        /// <exception cref="InvalidOperationException">Thrown if the type cannot be compared, either a default comparer was not found or the type does not implement comparable interfaces.</exception>
        public static int BinarySearch<T>(T[] array, in T value) where T : struct
        {
            return BinarySearch<T>(array, 0, (array != null) ? array.Length : 0, in value, null);
        }

        /// <summary>
        /// Searches the entire sorted <see cref="Array"/> for an element that corresponds to the
        /// specified value using the specified comparer, and returns the zero-based index of the element in the array, if it exists.
        /// </summary>
        /// <typeparam name="T">The type of the elements of the array.</typeparam>
        /// <param name="array">The zero-based array to search.</param>
        /// <param name="value">An object to locate in the <see cref="Array"/>.</param>
        /// <param name="comparer">The comparer to use, or null to use the default comparer for the type.</param>
        /// <returns>The zero-based index of <paramref name="value"/> in the sorted <see cref="Array"/>, if it is found, otherwise, a negative
        /// number that is the bitwise complement of the index of the next element that is larger than <paramref name="value"/> or if there is none, the bitwise complement of the index of the last element plus 1.</returns>
        /// <exception cref="ArgumentNullException">Thrown if <paramref name="array"/> is null.</exception>
        /// <exception cref="InvalidOperationException">Thrown if the type cannot be compared, either a default comparer was not found or the type does not implement comparable interfaces.</exception>
        public static int BinarySearch<T>(T[] array, in T value, IRefComparer<T> comparer) where T : struct
        {
            return BinarySearch<T>(array, 0, (array != null) ? array.Length : 0, in value, comparer);
        }

        /// <summary>
        /// Searches the entire sorted <see cref="Array"/> for an element that corresponds to the
        /// specified value using the specified comparer, and returns the zero-based index of the element in the array, if it exists.
        /// </summary>
        /// <typeparam name="T">The type of the elements of the array.</typeparam>
        /// <param name="array">The zero-based array to search.</param>
        /// <param name="value">An object to locate in the <see cref="Array"/>.</param>
        /// <param name="compareFunc">The comparison function to use.</param>
        /// <returns>The zero-based index of <paramref name="value"/> in the sorted <see cref="Array"/>, if it is found, otherwise, a negative
        /// number that is the bitwise complement of the index of the next element that is larger than <paramref name="value"/> or if there is none, the bitwise complement of the index of the last element plus 1.</returns>
        /// <exception cref="ArgumentNullException">Thrown if <paramref name="array"/> is null -or- <paramref name="compareFunc"/> is null.</exception>
        /// <exception cref="InvalidOperationException">Thrown if the type cannot be compared, either a default comparer was not found or the type does not implement comparable interfaces.</exception>
        public static int BinarySearch<T>(T[] array, in T value, RefComparison<T> compareFunc) where T : struct
        {
            return BinarySearch<T>(array, 0, (array != null) ? array.Length : 0, in value, RefComparer<T>.Create(compareFunc));
        }

        /// <summary>
        /// Searches a range of elements in the sorted <see cref="Array"/> for an element that corresponds to the
        /// specified value using the specified comparer, and returns the zero-based index of the element in the array, if it exists.
        /// </summary>
        /// <typeparam name="T">The type of the elements of the array.</typeparam>
        /// <param name="array">The zero-based array to search.</param>
        /// <param name="index">Zero-based starting index of the range to search.</param>
        /// <param name="count">Number of elements to search.</param>
        /// <param name="value">An object to locate in the <see cref="Array"/>.</param>
        /// <returns>The zero-based index of <paramref name="value"/> in the sorted <see cref="Array"/>, if it is found, otherwise, a negative
        /// number that is the bitwise complement of the index of the next element that is larger than <paramref name="value"/> or if there is none, the bitwise complement of the index of the last element plus 1.</returns>
        /// <exception cref="ArgumentNullException">Thrown if <paramref name="array"/> is null.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown if <paramref name="index"/> is less than zero -or- <paramref name="count"/> is less than zero.</exception>
        /// <exception cref="ArgumentException">Thrown if <paramref name="index"/> and <paramref name="count"/> do not denote a valid range.</exception>
        /// <exception cref="InvalidOperationException">Thrown if the type cannot be compared, either a default comparer was not found or the type does not implement comparable interfaces.</exception>
        public static int BinarySearch<T>(T[] array, int index, int count, in T value) where T : struct
        {
            return BinarySearch<T>(array, index, count, in value, null);
        }

        /// <summary>
        /// Searches a range of elements in the sorted <see cref="Array"/> for an element that corresponds to the
        /// specified value using the specified comparer, and returns the zero-based index of the element in the array, if it exists.
        /// </summary>
        /// <typeparam name="T">The type of the elements of the array.</typeparam>
        /// <param name="array">The zero-based array to search.</param>
        /// <param name="index">Zero-based starting index of the range to search.</param>
        /// <param name="count">Number of elements to search.</param>
        /// <param name="value">An object to locate in the <see cref="Array"/>.</param>
        /// <param name="comparer">The comparer to use, or null to use the default comparer for the type.</param>
        /// <returns>The zero-based index of <paramref name="value"/> in the sorted <see cref="Array"/>, if it is found, otherwise, a negative
        /// number that is the bitwise complement of the index of the next element that is larger than <paramref name="value"/> or if there is none, the bitwise complement of the index of the last element plus 1.</returns>
        /// <exception cref="ArgumentNullException">Thrown if <paramref name="array"/> is null.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown if <paramref name="index"/> is less than zero -or- <paramref name="count"/> is less than zero.</exception>
        /// <exception cref="ArgumentException">Thrown if <paramref name="index"/> and <paramref name="count"/> do not denote a valid range.</exception>
        /// <exception cref="InvalidOperationException">Thrown if the type cannot be compared, either a default comparer was not found or the type does not implement comparable interfaces.</exception>
        public static int BinarySearch<T>(T[] array, int index, int count, in T value, IRefComparer<T> comparer) where T : struct
        {
            int platformResult;
            if (TryPlatformBinarySearch<T>(array, index, count, in value, comparer, out platformResult))
                return platformResult;

            if (array == null)
                throw new ArgumentNullException("array");

            if (index < 0 || count < 0)
                throw new ArgumentOutOfRangeException((index < 0) ? "index" : "count", StringLocalizer.Instance.GetLocalizedString("LengthMustBePositive"));

            if ((array.Length - index) < count)
                throw new ArgumentException(StringLocalizer.Instance.GetLocalizedString("IndexAndCountOutOfRange"));

            return RefArraySortHelper<T>.Default.BinarySearch(array, index, count, in value, comparer);
        }

        /// <summary>
        /// Searches for the specified item and returns the index of the first occurrence within the entire <see cref="Array"/>.
        /// </summary>
        /// <typeparam name="T">The type of the elements of the array.</typeparam>
        /// <param name="array">The zero-based array to search.</param>
        /// <param name="value">The item to locate in <paramref name="array"/>.</param>
        /// <returns>Zero-based index of the first occurrence of <paramref name="value"/> within the entire array, if found, otherwise, -1.</returns>
        /// <exception cref="ArgumentNullException">Thrown if <paramref name="array"/> is null.</exception>
        public static int IndexOf<T>(T[] array, in T value) where T : struct
        {
            return IndexOf<T>(array, in value, 0, (array != null) ? array.Length : 0);
        }

        /// <summary>
        /// Searches for the specified item and returns the index of the first occurrence within the range from the starting index to the last element.
        /// </summary>
        /// <typeparam name="T">The type of the elements of the array.</typeparam>
        /// <param name="array">The zero-based array to search.</param>
        /// <param name="value">The item to locate in <paramref name="array"/>.</param>
        /// <param name="startIndex">Zero-based starting index of the search. Zero is valid in an empty array.</param>
        /// <returns>Zero-based index of the first occurrence of <paramref name="value"/> within the entire array, if found, otherwise, -1.</returns>
        /// <exception cref="ArgumentNullException">Thrown if <paramref name="array"/> is null.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown if <paramref name="startIndex"/> is outside the range of valid indices for <paramref name="array"/>.</exception>
        public static int IndexOf<T>(T[] array, in T value, int startIndex) where T : struct
        {
            return IndexOf<T>(array, in value, startIndex, (array != null) ? array.Length : 0);
        }

        /// <summary>
        /// Searches for the specified item and returns the index of the first occurrence within the specified range of elements.
        /// </summary>
        /// <typeparam name="T">The type of the elements of the array.</typeparam>
        /// <param name="array">The zero-based array to search.</param>
        /// <param name="value">The item to locate in <paramref name="array"/>.</param>
        /// <param name="startIndex">Zero-based starting index of the search. Zero is valid in an empty array.</param>
        /// <param name="count">Number of elements to search.</param>
        /// <returns>Zero-based index of the first occurrence of <paramref name="value"/> within the entire array, if found, otherwise, -1.</returns>
        /// <exception cref="ArgumentNullException">Thrown if <paramref name="array"/> is null.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown if <paramref name="startIndex"/> is outside the range of valid indices for <paramref name="array"/>.</exception>
        public static int IndexOf<T>(T[] array, in T value, int startIndex, int count) where T : struct
        {
            if (array == null)
                throw new ArgumentNullException("array");

            int length = array.Length;

            if ((uint)startIndex > (uint) length)
                throw new ArgumentOutOfRangeException("startIndex", StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));

            if (count < 0 || startIndex > (length - count))
                throw new ArgumentOutOfRangeException("count", StringLocalizer.Instance.GetLocalizedString("IndexAndCountOutOfRange"));

            return RefEqualityComparer<T>.Default.IndexOf(array, value, startIndex, count);
        }

        /// <summary>
        /// Searches for the specified item and returns the index of the last occurrence within the entire <see cref="Array"/>.
        /// </summary>
        /// <typeparam name="T">The type of the elements of the array.</typeparam>
        /// <param name="array">The zero-based array to backward search.</param>
        /// <param name="value">The item to locate in <paramref name="array"/>.</param>
        /// <returns>Zero-based index of the last occurrence of <paramref name="value"/> within the entire array, if found, otherwise, -1.</returns>
        /// <exception cref="ArgumentNullException">Thrown if <paramref name="array"/> is null.</exception>
        public static int LastIndexOf<T>(T[] array, in T value) where T : struct
        {
            int startIndex = 0;
            int count = 0;

            if(array != null)
            {
                startIndex = array.Length - 1;
                count = array.Length;
            }

            return LastIndexOf<T>(array, in value, startIndex, count);
        }

        /// <summary>
        /// Searches for the specified item and returns the index of the last occurrence within the range from the starting index to the last element.
        /// </summary>
        /// <typeparam name="T">The type of the elements of the array.</typeparam>
        /// <param name="array">The zero-based array to backward search.</param>
        /// <param name="value">The item to locate in <paramref name="array"/>.</param>
        /// <param name="startIndex">Zero-based starting index of the backward search. Zero is valid in an empty array.</param>
        /// <returns>Zero-based index of the last occurrence of <paramref name="value"/> within the entire array, if found, otherwise, -1.</returns>
        /// <exception cref="ArgumentNullException">Thrown if <paramref name="array"/> is null.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown if <paramref name="startIndex"/> is outside the range of valid indices for <paramref name="array"/>.</exception>
        public static int LastIndexOf<T>(T[] array, in T value, int startIndex) where T : struct
        {
            int count = 0;

            //If a non-zero array...otherwise if array is empty and startIndex is zero, need to pass zero as count
            if (array != null && array.Length > 0)
                count = startIndex + 1;

            return LastIndexOf<T>(array, in value, startIndex, count);
        }

        /// <summary>
        /// Searches for the specified item and returns the index of the last occurrence within the specified range of elements.
        /// </summary>
        /// <typeparam name="T">The type of the elements of the array.</typeparam>
        /// <param name="array">The zero-based array to backward search.</param>
        /// <param name="value">The item to locate in <paramref name="array"/>.</param>
        /// <param name="startIndex">Zero-based starting index of the backward search. Zero is valid in an empty array.</param>
        /// <param name="count">Number of elements to search.</param>
        /// <returns>Zero-based index of the last occurrence of <paramref name="value"/> within the entire array, if found, otherwise, -1.</returns>
        /// <exception cref="ArgumentNullException">Thrown if <paramref name="array"/> is null.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown if <paramref name="startIndex"/> is outside the range of valid indices for <paramref name="array"/>.</exception>
        public static int LastIndexOf<T>(T[] array, in T value, int startIndex, int count) where T : struct
        {
            if (array == null)
                throw new ArgumentNullException("array");

            int length = array.Length;

            if (length == 0)
            {
                //Special case for 0 length array, accept -1 and 0 as a valid startIndex
                if (startIndex != -1 && startIndex != 0)
                    throw new ArgumentOutOfRangeException("startIndex", StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));

                //Only zero is a valid value for count if array is empty
                if (count != 0)
                    throw new ArgumentOutOfRangeException("count", StringLocalizer.Instance.GetLocalizedString("IndexAndCountOutOfRange"));

                return -1;
            }

            //Make sure we're not out of range
            if (startIndex < 0 || startIndex >= length)
                throw new ArgumentOutOfRangeException("startIndex", StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));

            //Will catch when startIndex == MAXINT, so MAXINT - 0 + 1 == -1
            if (count < 0 || ((startIndex - count) + 1) < 0)
                throw new ArgumentOutOfRangeException("count", StringLocalizer.Instance.GetLocalizedString("IndexAndCountOutOfRange"));

            return RefEqualityComparer<T>.Default.LastIndexOf(array, in value, startIndex, count);
        }

        /// <summary>
        /// Sorts the elements in the entire <see cref="Array"/> using the default comparison for the element type.
        /// </summary>
        /// <typeparam name="T">The type of the elements of the array.</typeparam>
        /// <param name="array">The zero-based array to sort.</param>
        /// <exception cref="ArgumentNullException">Thrown if <paramref name="array"/> is null.</exception>
        /// <exception cref="InvalidOperationException">Thrown if the type cannot be compared, either a default comparer was not found or the type does not implement comparable interfaces.</exception>
        public static void Sort<T>(T[] array) where T : struct
        {
            Sort<T>(array, 0, (array != null) ? array.Length : 0, null);
        }

        /// <summary>
        /// Sorts the elements in the entire <see cref="Array"/> using the default comparison for the element type.
        /// </summary>
        /// <typeparam name="T">The type of the elements of the array.</typeparam>
        /// <param name="array">The zero-based array to sort.</param>
        /// <param name="comparer">The comparer to use, or null to use the default comparer for the type.</param>
        /// <exception cref="ArgumentNullException">Thrown if <paramref name="array"/> is null.</exception>
        /// <exception cref="InvalidOperationException">Thrown if the type cannot be compared, either a default comparer was not found or the type does not implement comparable interfaces.</exception>
        public static void Sort<T>(T[] array, IRefComparer<T> comparer) where T : struct
        {
            Sort<T>(array, 0, (array != null) ? array.Length : 0, comparer);
        }

        /// <summary>
        /// Sorts the elements in the entire <see cref="Array"/> using the default comparison for the element type.
        /// </summary>
        /// <typeparam name="T">The type of the elements of the array.</typeparam>
        /// <param name="array">The zero-based array to sort.</param>
        /// <param name="compareFunc">The comparison function to use.</param>
        /// <exception cref="ArgumentNullException">Thrown if <paramref name="array"/> is null -or- <paramref name="compareFunc"/> is null.</exception>
        /// <exception cref="InvalidOperationException">Thrown if the type cannot be compared, either a default comparer was not found or the type does not implement comparable interfaces.</exception>
        public static void Sort<T>(T[] array, RefComparison<T> compareFunc) where T : struct
        {
            if (compareFunc == null)
                throw new ArgumentNullException("compareFunc");

            Sort<T>(array, 0, (array != null) ? array.Length : 0, RefComparer<T>.Create(compareFunc));
        }

        /// <summary>
        /// Sorts a range of elements in the <see cref="Array"/> using the default comparison for the element type.
        /// </summary>
        /// <typeparam name="T">The type of the elements of the array.</typeparam>
        /// <param name="array">The zero-based array to sort.</param>
        /// <param name="index">Zero-based starting index of the range to sort.</param>
        /// <param name="count">Number of elements to sort.</param>
        /// <exception cref="ArgumentNullException">Thrown if <paramref name="array"/> is null.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown if <paramref name="index"/> is less than zero -or- <paramref name="count"/> is less than zero.</exception>
        /// <exception cref="ArgumentException">Thrown if <paramref name="index"/> and <paramref name="count"/> do not denote a valid range.</exception>
        /// <exception cref="InvalidOperationException">Thrown if the type cannot be compared, either a default comparer was not found or the type does not implement comparable interfaces.</exception>
        public static void Sort<T>(T[] array, int index, int count) where T : struct
        {
            Sort<T>(array, index, count, null);
        }

        /// <summary>
        /// Sorts a range of elements in the <see cref="Array"/> using the default comparison for the element type.
        /// </summary>
        /// <typeparam name="T">The type of the elements of the array.</typeparam>
        /// <param name="array">The zero-based array to sort.</param>
        /// <param name="index">Zero-based starting index of the range to sort.</param>
        /// <param name="count">Number of elements to sort.</param>
        /// <param name="comparer">The comparer to use, or null to use the default comparer for the type.</param>
        /// <exception cref="ArgumentNullException">Thrown if <paramref name="array"/> is null.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown if <paramref name="index"/> is less than zero -or- <paramref name="count"/> is less than zero.</exception>
        /// <exception cref="ArgumentException">Thrown if <paramref name="index"/> and <paramref name="count"/> do not denote a valid range.</exception>
        /// <exception cref="InvalidOperationException">Thrown if the type cannot be compared, either a default comparer was not found or the type does not implement comparable interfaces.</exception>
        public static void Sort<T>(T[] array, int index, int count, IRefComparer<T> comparer) where T : struct
        {
            //First try and see if we can do the platform sort.
            if (TryPlatformSort<T>(array, index, count, comparer))
                return;

            //Already sorted!
            if (count <= 1)
                return;

            if (array == null)
                throw new ArgumentNullException("array");

            if (index < 0 || count < 0)
                throw new ArgumentOutOfRangeException((index < 0) ? "index" : "count", StringLocalizer.Instance.GetLocalizedString("LengthMustBePositive"));

            if ((array.Length - index) < count)
                throw new ArgumentException(StringLocalizer.Instance.GetLocalizedString("IndexAndCountOutOfRange"));

            RefArraySortHelper<T>.Default.Sort(array, index, count, comparer);
        }

        private static bool TryPlatformSort<T>(T[] array, int index, int count, IRefComparer<T> comparer) where T : struct
        {
            //If null...look at default comparer, if it's a wrapper then the type doesn't implement IRefComparable<T>
            if(comparer == null)
            {
                if (RefComparer<T>.Default.IsWrappedComparer)
                {
                    //Pass in null because the default wrapper will try and use it anyways (or a similar one, just rely on platform)
                    Array.Sort<T>(array, index, count, null);
                    return true;
                }
            }
            else
            {
                //If not null...check to see if its a wrapper. If the comparer is the default for the type, just rely on platform's default. Otherwise, pass in
                //the underlying comparer.
                RefComparer<T> baseClass = comparer as RefComparer<T>;
                if (baseClass != null && baseClass.IsWrappedComparer)
                {
                    Array.Sort<T>(array, index, count, (comparer == RefComparer<T>.Default) ? null : baseClass.WrappedComparer);
                    return true;
                }
            }

            return false;
        }

        private static bool TryPlatformBinarySearch<T>(T[] array, int index, int count, in T value, IRefComparer<T> comparer, out int result) where T : struct
        {
            result = -1;

            //If null...look at default comparer, if it's a wrapper then the type doesn't implement IRefComparable<T>
            if (comparer == null)
            {
                if (RefComparer<T>.Default.IsWrappedComparer)
                {
                    //Pass in null because the default wrapper will try and use it anyways (or a similar one, just rely on platform)
                    result = Array.BinarySearch<T>(array, index, count, value, null);
                    return true;
                }
            }
            else
            {
                //If not null...check to see if its a wrapper. If the comparer is the default for the type, just rely on platform's default. Otherwise, pass in
                //the underlying comparer.
                RefComparer<T> baseClass = comparer as RefComparer<T>;
                if (baseClass != null && baseClass.IsWrappedComparer)
                {
                    result = Array.BinarySearch<T>(array, index, count, value, (comparer == RefComparer<T>.Default) ? null : baseClass.WrappedComparer);
                    return true;
                }
            }

            return false;
        }

        //Figure out later
        public static ref T First<T>(this IRefList<T> list) where T : struct
        {
            if(list.Count == 0)
                throw new ArgumentOutOfRangeException("index", StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));

            return ref list[0];
        }

        public static ref T Last<T>(this IRefList<T> list) where T : struct
        {
            int index = list.Count - 1;

            if (index < 0)
                throw new ArgumentOutOfRangeException("index", StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));

            return ref list[index];
        }

        public static ref readonly T First<T>(this IReadOnlyRefList<T> list) where T : struct
        {
            if (list.Count == 0)
                throw new ArgumentOutOfRangeException("index", StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));

            return ref list[0];
        }

        public static ref readonly T Last<T>(this IReadOnlyRefList<T> list) where T : struct
        {
            int index = list.Count - 1;

            if(index < 0)
                throw new ArgumentOutOfRangeException("index", StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));

            return ref list[index];
        }
    }
}
