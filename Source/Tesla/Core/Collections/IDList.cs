﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections;
using System.Collections.Generic;

namespace Tesla
{
    /// <summary>
    /// Delegate for fixing up ID's for an object contained in an <see cref="IDList{T}"/>.
    /// </summary>
    /// <typeparam name="T">Type of value</typeparam>
    /// <param name="oldID">The old identifier.</param>
    /// <param name="newID">The new identifier.</param>
    /// <param name="value">The value associated with the ID.</param>
    public delegate void IDChanged<T>(int oldID, int newID, ref T value);

    /// <summary>
    /// A specialized list where the index can safely be used to reference the value, even if objects are removed (the list
    /// can become sparse, with holes). The collection can be compacted and the IDs will be re-assigned (with a callback)
    /// </summary>
    /// <typeparam name="T">Type of value that will be stored in the collection.</typeparam>
    public class IDList<T> : ICollection<T>, IReadOnlyCollection<T>
    {
        private static Pair<bool, T>[] s_emptyList = new Pair<bool, T>[0];

        private Pair<bool,T>[] m_list; 
        private int m_version;
        private int m_count;
        private int m_lastFreeIndex;
        private List<int> m_freeIndices;

        /// <summary>
        /// Gets or sets a value by an ID. If the ID is not within the range of [0, UpperBoundExclusive) then an exception
        /// is thrown when setting a value.
        /// </summary>
        /// <exception cref="ArgumentOutOfRangeException">Thrown if setting an ID that is out of range.</exception>
        public T this[int id]
        {
            get
            {
                if (IsInRange(id))
                {
                    Pair<bool, T> value = m_list[id];
                    if (value.First)
                        return value.Second;
                }

                return default(T);
            }

            set
            {
                if (IsInRange(id))
                {
                    //Are we overwriting, or adding?
                    if (!m_list[id].First)
                        m_count++;

                    m_list[id] = new Pair<bool, T>(true, value);
                }
                else
                {
                    throw new ArgumentOutOfRangeException("id");
                }
            }
        }

        /// <summary>
        /// Gets the number of currently valid objects in the list.
        /// </summary>
        public int Count
        {
            get
            {
                return m_count;
            }
        }

        /// <summary>
        /// Gets if the collection is read only.
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the current upper bound (exclusive) range of IDs. This corresponds to the last free index in the underlying list
        /// or even the array length. As new objects are added and there are no free slots to be filled, this uppder bound value will
        /// continue to grow larger. If the list is compacted or cleared it might shrink.
        /// </summary>
        public int UpperBoundExclusive
        {
            get
            {
                if (m_lastFreeIndex == 0)
                {
                    //Sanity check
                    System.Diagnostics.Debug.Assert(m_count == 0, "Free index is zero, but we have a positive count");
                    return 0;
                }

                int upperBoundsExclusive = m_lastFreeIndex + 1;
                if (upperBoundsExclusive >= m_list.Length)
                    upperBoundsExclusive = m_list.Length;

                return upperBoundsExclusive;
            }
        }
        /// <summary>
        /// Constructs a new <see cref="IDList"/>.
        /// </summary>
        public IDList()
            : this(0)
        {
        }

        /// <summary>
        /// Constructs a new <see cref="IDList"/>.
        /// </summary>
        /// <param name="initialCapacity">Initial capacity of the list.</param>
        public IDList(int initialCapacity)
        {
            m_freeIndices = new List<int>();
            m_list = (initialCapacity <= 0) ? s_emptyList : new Pair<bool, T>[initialCapacity];
            m_count = 0;
            m_lastFreeIndex = 0;
            m_version = 0;   
        }

        /// <summary>
        /// Constructs a new <see cref="IDList"/>.
        /// </summary>
        /// <param name="values">Range of values to be added.</param>
        public IDList(IEnumerable<T> values)
            : this(0)
        {
            AddRange(values);
        }

        /// <summary>
        /// Adds an item to the list.
        /// </summary>
        /// <param name="item">Item to add.</param>
        /// <returns>ID to reference the value.</returns>
        public int Add(T item)
        {
            if (item == null)
                return -1;

            int index = GetNextFreeIndex();
            m_list[index] = new Pair<bool, T>(true, item);
            m_count++;
            m_version++;

            return index;
        }

        /// <summary>
        /// Adds an item to the list.
        /// </summary>
        /// <param name="item">Item to add.</param>
        /// <returns>ID to reference the value.</returns>
        void ICollection<T>.Add(T item)
        {
            Add(item);
        }

        /// <summary>
        /// Adds a range of values to the list.
        /// </summary>
        /// <param name="values">Items to add.</param>
        public void AddRange(IEnumerable<T> values)
        {
            if(values == null)
                return;

            if (values is ICollection<T>)
                EnsureCapacity((values as ICollection<T>).Count);

            T[] array = values as T[];
            if(array != null)
            {
                for(int i = 0; i < array.Length; i++)
                {
                    T v = array[i];
                    if(v != null)
                        Add(v);
                }

                return;
            }

            List<T> list = values as List<T>;
            if(list != null)
            {
                for(int i = 0; i < list.Count; i++)
                {
                    T v = list[i];
                    if(v != null)
                        Add(v);
                }

                return;
            }

            foreach(T v in values)
                Add(v);
        }

        /// <summary>
        /// Queries a value by ID, returning true or false if the value is present.
        /// </summary>
        /// <param name="id">ID of the value.</param>
        /// <param name="value">Value returned.</param>
        /// <returns>True if the value is present, false if not.</returns>
        public bool TryGetValue(int id, out T value)
        {
            if(!IsInRange(id))
            {
                value = default(T);
                return false;
            }

            Pair<bool, T> entry = m_list[id];

            if(!entry.First)
            {
                value = default(T);
                return false;
            }

            value = entry.Second;
            return true;
        }

        /// <summary>
        /// Clears the list.
        /// </summary>
        public void Clear()
        {
            Array.Clear(m_list, 0, UpperBoundExclusive);
            m_count = 0;
            m_freeIndices.Clear();
            m_lastFreeIndex = 0;
            m_version++;
        }

        /// <summary>
        /// Determines if the value is present in the collection.
        /// </summary>
        /// <param name="item"></param>
        /// <returns>True if the value was found in the collection, false if it was not found.</returns>
        public bool Contains(T item)
        {
            return LookupID(item, null) >= 0;
        }

        /// <summary>
        /// Determines if the value is present in the collection.
        /// </summary>
        /// <param name="item"></param>
        /// <param name="comparer">Optional equality comparer. If null, the default comparer is used.</param>
        /// <returns>True if the value was found in the collection, false if it was not found.</returns>
        public bool Contains(T item, IEqualityComparer<T> comparer)
        {
            return LookupID(item, comparer) >= 0;
        }

        /// <summary>
        /// Determines if the ID references a value in the collection.
        /// </summary>
        /// <param name="id">ID to search for.</param>
        /// <returns>True if there is an object associated with the ID, false if not.</returns>
        public bool ContainsID(int id)
        {
            if(!IsInRange(id))
                return false;

            return m_list[id].First;
        }

        /// <summary>
        /// Copies the values in the list to the array (indices / IDs are not preserved).
        /// </summary>
        /// <param name="array">Array to copy to.</param>
        public void CopyTo(T[] array)
        {
            CopyTo(array, 0);
        }

        /// <summary>
        /// Copies the values in the list to the array (indices / IDs are not preserved).
        /// </summary>
        /// <param name="array">Array to copy to.</param>
        /// <param name="arrayIndex">Starting index in array at which to start writing at.</param>
        public void CopyTo(T[] array, int arrayIndex)
        {
            if(array == null || arrayIndex < 0 && arrayIndex >= array.Length)
                return;

            foreach(T val in this)
            {
                if (arrayIndex >= array.Length)
                    break;

                array[arrayIndex] = val;
                arrayIndex++;
            }
        }

        /// <summary>
        /// Looks up the ID associated with the value (the first instance).
        /// </summary>
        /// <param name="item">Value to search for.</param>
        /// <param name="comparer">Optional equality comparer, if null the default comparer is used.</param>
        /// <returns>The ID, or -1 if it was not found.</returns>
        public int LookupID(T item, IEqualityComparer<T> comparer = null)
        {
            if (comparer == null)
                comparer = EqualityComparer<T>.Default;

            int count = UpperBoundExclusive;
            for (int i = 0; i < count; i++)
            {
                Pair<bool, T> itemInList = m_list[i];
                if (itemInList.First && comparer.Equals(itemInList.Second, item))
                    return i;
            }

            return -1;
        }

        /// <summary>
        /// Removes the value from the list.
        /// </summary>
        /// <param name="item">Value to remove.</param>
        /// <returns>True if the value was removed, false otherwise.</returns>
        public bool Remove(T item)
        {
            int id = LookupID(item);

            if(id >= 0)
            {
                RemoveByID(id);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Removes the value from the list.
        /// </summary>
        /// <param name="item">Value to remove.</param>
        /// <param name="comparer">Optional equality comparer, if null the default comparer is used.</param>
        /// <returns>True if the value was removed, false otherwise.</returns>
        public bool Remove(T item, IEqualityComparer<T> comparer)
        {
            int id = LookupID(item, comparer);

            if (id >= 0)
            {
                RemoveByID(id);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Removes the value associated with the specified ID from the list.
        /// </summary>
        /// <param name="id">ID of the value to remove</param>
        /// <returns>True if the value was removed, false if it was not found.</returns>
        public bool RemoveByID(int id)
        {
            if (!IsInRange(id))
                return false;

            //Check to make sure there was something at the index, if not then it's a hole and do nothing
            if (!m_list[id].First)
                return false;

            m_list[id] = new Pair<bool, T>(false, default(T));
            m_count--;
            m_version++;

            //If count is zero, then the list is entirely empty.
            if (m_count == 0)
            {
                m_lastFreeIndex = 0;
                m_freeIndices.Clear();
            }
            //If ID is the upper bound, then we can contract the bound (guarantee that there are no ID's greater than this)
            else if(m_lastFreeIndex == id)
            {
                m_lastFreeIndex--;
            }
            else
            {
                m_freeIndices.Insert(0, id);
            }

            return true;
        }

        /// <summary>
        /// Compacts the list, where all objects will have IDs in range of [0, Count]. Some objects may or may not be moved,
        /// so not all IDs may be changed.
        /// </summary>
        /// <param name="idChangedCallback">Optional callback to notify if the ID of an object has changed.</param>
        /// <param name="trimToSize">Trim the list to the number of elements in the collection.</param>
        public void Compact(IDChanged<T> idChangedCallback, bool trimToSize = false)
        {
            Pair<bool, T>[] array = (trimToSize) ? new Pair<bool, T>[m_count] : m_list;

            int prevIndex = 0;
            for (int i = 0; i < m_count; i++)
            {
                if(array[i].First)
                {
                    prevIndex = i + 1;
                    continue;
                }

                T value;
                int index = GetNextValidValue(prevIndex, out value);
                if (index < 0)
                    break;

                array[i] = new Pair<bool, T>(true, value);
                prevIndex = index + 1;

                if (index != i && idChangedCallback != null)
                    idChangedCallback(index, i, ref value);
            }

            m_lastFreeIndex = m_count + 1;
            m_freeIndices.Clear();
            m_version++;

            if (trimToSize)
                m_list = array;
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>An enumerator that can be used to iterate through the collection.</returns>
        public IEnumerator<T> GetEnumerator()
        {
            return new IDListEnumerator(this);
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>An enumerator that can be used to iterate through the collection.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return new IDListEnumerator(this);
        }

        private int GetNextFreeIndex()
        {
            //Look at free indices, remove the last one to avoid copying
            if(m_freeIndices.Count > 0)
            {
                int indexToRemove = m_freeIndices.Count - 1;
                int index = m_freeIndices[indexToRemove];
                m_freeIndices.RemoveAt(indexToRemove);

                return index;
            }

            //If no free indices, look at the upper bound - if hit the end of the array, resize
            if (m_lastFreeIndex == m_list.Length)
                EnsureCapacity(Math.Max(4, m_list.Length * 2));

            int indexToReturn = m_lastFreeIndex;
            m_lastFreeIndex++;

            return indexToReturn;
        }

        private int GetNextValidValue(int startIndex, out T value)
        {
            value = default(T);
            int index = startIndex;
            int indexToReturn = -1;
            bool isValid = false;
            int upperBoundExclusive = UpperBoundExclusive;
            while (!isValid && index < upperBoundExclusive)
            {
                Pair<bool, T> entry = m_list[index];
                if (entry.First)
                {
                    isValid = true;
                    value = entry.Second;
                    indexToReturn = index;
                }

                index++;
            }

            return indexToReturn;
        }

        private bool IsInRange(int index)
        {
            return index >= 0 && index < UpperBoundExclusive;
        }

        private void EnsureCapacity(int minRequested)
        {
            if (minRequested <= m_list.Length)
                return;

            Pair<bool, T>[] newList = new Pair<bool, T>[minRequested];
            if (m_count > 0)
                Array.Copy(m_list, newList, UpperBoundExclusive);

            m_list = newList;
        }

        /// <summary>
        /// Enumerator for iterating through an <see cref="IDList"/>.
        /// </summary>
        public struct IDListEnumerator : IEnumerator<T>
        {
            private IDList<T> m_list;
            private T m_current;
            private int m_currentIndex;
            private int m_version;

            /// <summary>
            /// Gets the current value.
            /// </summary>
            public T Current
            {
                get
                {
                    return m_current;
                }
            }

            /// <summary>
            /// Gets the current value.
            /// </summary>
            object IEnumerator.Current
            {
                get
                {
                    return m_current;
                }
            }

            /// <summary>
            /// Constructs a new instance of the <see cref="IDListEnumerator"/> struct.
            /// </summary>
            /// <param name="list">ID list to iterate over.</param>
            internal IDListEnumerator(IDList<T> list)
            {
                m_list = list;
                m_current = default(T);
                m_currentIndex = 0;
                m_version = list.m_version;
            }

            /// <summary>
            /// Advances the enumerator to the next element of the collection.
            /// </summary>
            /// <returns>True if the enumerator was successfully advanced to the next element; false if the enumerator has passed the end of the collection.</returns>
            public bool MoveNext()
            {
                ThrowIfChanged();

                bool isValid = false;
                int upperBoundExclusive = m_list.UpperBoundExclusive;
                while(!isValid && m_currentIndex < upperBoundExclusive)
                {
                    Pair<bool, T> entry = m_list.m_list[m_currentIndex];
                    if (entry.First)
                    {
                        isValid = true;
                        m_current = entry.Second;
                    }

                    m_currentIndex++;
                }

                if (!isValid)
                    m_current = default(T);

                return isValid;
            }

            /// <summary>
            /// Sets the enumerator to its initial position, which is before the first element in the collection.
            /// </summary>
            public void Reset()
            {
                ThrowIfChanged();

                m_currentIndex = 0;
                m_current = default(T);
            }

            /// <summary>
            /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
            /// </summary>
            public void Dispose() { }

            private void ThrowIfChanged()
            {
                if (m_version != m_list.m_version)
                    throw new InvalidOperationException(StringLocalizer.Instance.GetLocalizedString("CollectionModifiedDuringEnumeration"));
            }
        }
    }
}
