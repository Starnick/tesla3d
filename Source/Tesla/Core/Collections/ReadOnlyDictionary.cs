﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace Tesla
{
    /// <summary>
    /// A read-only dictionary that can wrap modifiable dictionaries or simply contain a non-modifiable dictionary of Key-Value pairs.
    /// </summary>
    /// <typeparam name="K">Key type</typeparam>
    /// <typeparam name="V">Value type</typeparam>
    [DebuggerTypeProxy(typeof(DebugView_ReadOnlyDictionary<,>)), DebuggerDisplay("Count = {Count}")]
    public class ReadOnlyDictionary<K, V> : IReadOnlyDictionary<K, V>
    {
        /// <summary>
        /// The underlying dictionary.
        /// </summary>
        protected IDictionary<K, V> m_dictionary;

        /// <summary>
        /// Gets the number of entries in the dictionary.
        /// </summary>
        public int Count
        {
            get
            {
                return m_dictionary.Count;
            }
        }

        /// <summary>
        /// Gets the value specified by its key.
        /// </summary>
        /// <param name="key">Key</param>
        /// <returns>Value</returns>
        public V this[K key]
        {
            get
            {
                return m_dictionary[key];
            }
        }

        /// <summary>
        /// Gets the keys of the dictionary.
        /// </summary>
        public IEnumerable<K> Keys
        {
            get
            {
                return m_dictionary.Keys;
            }
        }

        /// <summary>
        /// Gets the values of the dictionary.
        /// </summary>
        public IEnumerable<V> Values
        {
            get
            {
                return m_dictionary.Values;
            }
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="ReadOnlyDictionary{K, V}"/> class. This effectively creates a read-only
        /// wrapper of the dictionary.
        /// </summary>
        /// <param name="elements">The element dictionary to hold.</param>
        /// <exception cref="System.ArgumentNullException">Thrown if the dictionary is null.</exception>
        public ReadOnlyDictionary(IDictionary<K, V> elements)
        {
            if(elements == null)
                throw new ArgumentNullException("elements");

            m_dictionary = elements;
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="ReadOnlyDictionary{K, V}"/> class. This copies elements from the enumerable,
        /// rather than wrappering a collection.
        /// </summary>
        /// <param name="elements">The element key value pairs to copy.</param>
        /// <exception cref="System.ArgumentNullException">Thrown if the enumerable is null.</exception>
        public ReadOnlyDictionary(IEnumerable<KeyValuePair<K, V>> elements)
        {
            if(elements == null)
                throw new ArgumentNullException("elements");


            m_dictionary = new Dictionary<K, V>();
            foreach(KeyValuePair<K, V> kv in elements)
            {
                m_dictionary.Add(kv);
            }
        }

        /// <summary>
        /// Determines whether the dictionary contains an element associated with the specified key.
        /// </summary>
        /// <param name="key">The key to search for.</param>
        /// <returns>True if the dictionary contains the key.</returns>
        /// <exception cref="ArgumentNullException">Thrown if the key is null.</exception>
        public bool ContainsKey(K key)
        {
            return m_dictionary.ContainsKey(key);
        }

        /// <summary>
        /// Tries to get an element associated with the specified key.
        /// </summary>
        /// <param name="key">The key whose value to get</param>
        /// <param name="value">The value associated with the key, if it exists.</param>
        /// <returns>True if the key was found, false otherwise.</returns>
        /// <exception cref="ArgumentNullException">Thrown if the key is null.</exception>
        public bool TryGetValue(K key, out V value)
        {
            return m_dictionary.TryGetValue(key, out value);
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.</returns>
        public IEnumerator<KeyValuePair<K, V>> GetEnumerator()
        {
            return m_dictionary.GetEnumerator();
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return m_dictionary.GetEnumerator();
        }
    }
}
