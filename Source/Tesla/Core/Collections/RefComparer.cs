/*
* Copyright (c) 2010-2017 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;

namespace Tesla
{
    /// <summary>
    /// Provides a base class implementation of <see cref="IRefComparable{T}"/>.
    /// </summary>
    /// <typeparam name="T">The type of object to compare.</typeparam>
    public abstract class RefComparer<T> : IRefComparer<T> where T : struct
    {
        private static volatile RefComparer<T> s_defaultComparer;

        /// <summary>
        /// Returns a default sort order comparer for the type specified by the generic argument.
        /// </summary>
        public static RefComparer<T> Default
        {
            get
            {
                RefComparer<T> comparer = s_defaultComparer;
                if (comparer == null)
                {
                    comparer = CreateComparer();
                    s_defaultComparer = comparer;
                }

                return comparer;
            }
        }

        /// <summary>
        /// If this <see cref="RefComparer{T}"/> is a wrapper around an <see cref="IComparer{T}"/>, get the instance.
        /// </summary>
        public virtual IComparer<T> WrappedComparer
        {
            get
            {
                return null;
            }
        }

        /// <summary>
        /// Queries if this <see cref="RefComparer{T}"/> wraps an <see cref="IComparer{T}"/>.
        /// </summary>
        public bool IsWrappedComparer
        {
            get
            {
                return WrappedComparer != null;
            }
        }

        /// <summary>
        /// Creates a comparer by using the specified comparison.
        /// </summary>
        /// <param name="compareFunc">The comparison function.</param>
        /// <returns>A comparer that uses the function.</returns>
        public static RefComparer<T> Create(RefComparison<T> compareFunc)
        {
            if (compareFunc == null)
                throw new ArgumentNullException("compareFunc");

            return new RefComparisonComparer<T>(compareFunc);
        }

        /// <summary>
        /// Creates an adapter to an existing <see cref="IEqualityComparer{T}"/>.
        /// </summary>
        /// <param name="comparer">Comparer to wrap.</param>
        /// <returns>Wrapped comparer.</returns>
        /// <exception cref="ArgumentNullException">Thrown if <paramref name="comparer"/>.</exception>
        public static RefComparer<T> CreateAdapter(IComparer<T> comparer)
        {
            if (comparer == null)
                throw new ArgumentNullException("comparer");

            return new RefUnknownComparer<T>(comparer);
        }

        /// <summary>
        /// Compares two objects and returns value indicating whether one is less than, equal to, or greater than the other.
        /// </summary>
        /// <param name="x">The first object to compare.</param>
        /// <param name="y">The second object to compare.</param>
        /// <returns>A signed integer that indicates the relative values of <paramref name="x" /> and <paramref name="y" />. A value less than zero means <paramref name="x" /> is less than <paramref name="y" />,
        /// a value of zero means they are equal, and a value greater than zer means <paramref name="x" /> is greater than <paramref name="y" />.</returns>
        public abstract int Compare(in T x, in T y);

        private static RefComparer<T> CreateComparer()
        {
            Type type = typeof(T);
            
            //If the type is a IRefComparable<T>...
            if(typeof(IRefComparable<T>).IsAssignableFrom(type))
            {
                Type comparerType = typeof(RefGenericComparer<>);
                comparerType = comparerType.MakeGenericType(type);

                return (RefComparer<T>) Activator.CreateInstance(comparerType);
            }

            //If the type i s a IComparable<T>...
            if(typeof(IComparable<T>).IsAssignableFrom(type))
            {
                Type comparerType = typeof(RefGenericComparerAdapter<>);
                comparerType = comparerType.MakeGenericType(type);

                return (RefComparer<T>) Activator.CreateInstance(comparerType);
            }
            
            //Handle types that aren't either interface, maybe it implements non-generic IComparable, or might result in
            //an error...
            return new RefUnknownComparer<T>();
        }
    }

    //Comparer for most IRefComparable<T> structs
    internal sealed class RefGenericComparer<T> : RefComparer<T> where T : struct, IRefComparable<T>
    {
        public override int Compare(in T x, in T y)
        {
            return x.CompareTo(in y);
        }
    }

    //Comparer for structs that only implement IComparable<T>
    internal sealed class RefGenericComparerAdapter<T> : RefComparer<T> where T : struct, IComparable<T>
    {
        private IComparer<T> m_defaultComparer;

        public override IComparer<T> WrappedComparer
        {
            get
            {
                return m_defaultComparer;
            }
        }

        public RefGenericComparerAdapter()
        {
            //We get the default comparer for the type to show that we're an adapter, but otherwise we don't actually use it because we can directly call CompareTo
            m_defaultComparer = Comparer<T>.Default;
        }

        public override int Compare(in T x, in T y)
        {
            return x.CompareTo(y);
        }
    }

    //Comparer for structs that do not implement either...maybe the non-generic IComparable
    internal sealed class RefUnknownComparer<T> : RefComparer<T> where T : struct
    {
        private IComparer<T> m_comparer;

        public override IComparer<T> WrappedComparer
        {
            get
            {
                return m_comparer;
            }
        }

        public RefUnknownComparer()
        {
            m_comparer = Comparer<T>.Default;
        }

        public RefUnknownComparer(IComparer<T> comparer)
        {
            m_comparer = comparer;
        }

        public override int Compare(in T x, in T y)
        {
            return m_comparer.Compare(x, y);
        }
    }

    //Comparer that utilizes a delegate function to do the comparison.
    internal sealed class RefComparisonComparer<T> : RefComparer<T> where T : struct
    {
        private readonly RefComparison<T> m_compareFunc;

        public RefComparisonComparer(RefComparison<T> compareFunc)
        {
            m_compareFunc = compareFunc;
        }

        public override int Compare(in T x, in T y)
        {
            return m_compareFunc(in x, in y);
        }
    }
}
