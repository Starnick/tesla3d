﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Globalization;
using System.Runtime.InteropServices;

namespace Tesla
{
    /// <summary>
    /// A lightweight two-tuple that pairs two objects together.
    /// </summary>
    /// <typeparam name="T1">Type of the first object in the pair.</typeparam>
    /// <typeparam name="T2">Type of the second object in the pair.</typeparam>
    [Serializable]
    [StructLayout(LayoutKind.Sequential)]
    public struct Pair<T1, T2>
    {
        /// <summary>
        /// The first object in the pair.
        /// </summary>
        public T1 First;

        /// <summary>
        /// The second object in the pair.
        /// </summary>
        public T2 Second;

        /// <summary>
        /// Constructs a new instance of the <see cref="Pair{T1, T2}"/> struct.
        /// </summary>
        /// <param name="first">The first object in the pair.</param>
        /// <param name="second">The second object in the pair.</param>
        public Pair(in T1 first, in T2 second)
        {
            First = first;
            Second = second;
        }

        /// <summary>
        /// Returns the fully qualified type name of this instance.
        /// </summary>
        /// <returns>A <see cref="T:System.String" /> containing a fully qualified type name.</returns>
        public override String ToString()
        {
            CultureInfo info = CultureInfo.CurrentCulture;
            return String.Format(info, "{0}, {1}",
                new Object[] { (First != null) ? First.ToString() : "null", (Second != null) ? Second.ToString() : "null" });
        }
    }
}
