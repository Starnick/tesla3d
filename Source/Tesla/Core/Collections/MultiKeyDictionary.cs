﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Tesla
{
    /// <summary>
    /// Represents a two-dimensional key that has a "Major" and a "Minor" component.
    /// </summary>
    /// <typeparam name="TMajorKey">Major key type.</typeparam>
    /// <typeparam name="TMinorKey">Minor key type.</typeparam>
    [Serializable, StructLayout(LayoutKind.Sequential)]
    public struct MultiKey<TMajorKey, TMinorKey>
    {
        private TMajorKey m_major;
        private TMinorKey m_minor;
        
        /// <summary>
        /// Gets the major key component.
        /// </summary>
        public TMajorKey Major { get { return m_major; } }

        /// <summary>
        /// Gets the minor key component.
        /// </summary>
        public TMinorKey Minor {  get { return m_minor; } }

        /// <summary>
        /// Constructs a new instance of the <see cref="MultiKey{TMajorKey, TMinorKey}"/> struct.
        /// </summary>
        /// <param name="majorKey">Major key component.</param>
        /// <param name="minorKey">Minor key component.</param>
        public MultiKey(TMajorKey majorKey, TMinorKey minorKey)
        {
            m_major = majorKey;
            m_minor = minorKey;
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
        public override int GetHashCode()
        {
            unchecked
            {
                int majorHash = (m_major != null) ? m_major.GetHashCode() : 0;
                int minorHash = (m_minor != null) ? m_minor.GetHashCode() : 0;

                return ((majorHash << 5) + majorHash) ^ minorHash;
            }
        }

        /// <summary>
        /// Indicates whether this instance and a specified object are equal.
        /// </summary>
        /// <param name="obj">The object to compare with the current instance.</param>
        /// <returns>True if <paramref name="obj" /> and this instance are the same type and represent the same value; otherwise, false.</returns>
        public override bool Equals(Object obj)
        {
            if(obj is MultiKey<TMajorKey, TMinorKey>)
            {
                MultiKey<TMajorKey, TMinorKey> other = (MultiKey<TMajorKey, TMinorKey>)obj;
                return Object.Equals(m_major, other.m_major) && Object.Equals(m_minor, other.m_minor);
            }

            return false;
        }

        /// <summary>
        /// Returns the fully qualified type name of this instance.
        /// </summary>
        /// <returns>A <see cref="T:System.String" /> containing a fully qualified type name.</returns>
        public override String ToString()
        {
            String majorString = (m_major != null) ? m_major.ToString() : "NULL";
            String minorString = (m_minor != null) ? m_minor.ToString() : "NULL";

            return String.Format("[{0},{1}]", majorString, minorString);
        }
    }

    /// <summary>
    /// Represents a collection of two-dimensional keys and values.
    /// </summary>
    /// <remarks>
    /// This is similar to <see cref="Dictionary{TKey, TValue}"/> and thus inherits its properties (e.g. O(1) for lookup) and can be thought
    /// of as a "dictionary of dictionaries". It is compatible with the .NET interfaces by using <see cref="MultiKey{TMajorKey, TMinorKey}"/> as the
    /// singular key to map data. An example of usage would be for a 2D tile that has the XY integer coordinates as the key.
    /// </remarks>
    /// <typeparam name="TMajorKey">Major key type.</typeparam>
    /// <typeparam name="TMinorKey">Minor key type.</typeparam>
    /// <typeparam name="TValue">Data value type.</typeparam>
    [DebuggerTypeProxy(typeof(MultiKeyDictionary<,,>.DebugView)), DebuggerDisplay("Count = {Count}")]
    public class MultiKeyDictionary<TMajorKey, TMinorKey, TValue> :
        IDictionary<MultiKey<TMajorKey, TMinorKey>, TValue>, IReadOnlyDictionary<MultiKey<TMajorKey, TMinorKey>, TValue>
    {
        private Dictionary<TMajorKey, Dictionary<TMinorKey, TValue>> m_table;
        private int m_version;
        private int m_totalValueCount;
        private IEqualityComparer<TMinorKey> m_minorComparer;

        private Queue<Dictionary<TMinorKey, TValue>> m_freeSubDictionaries;
        private List<TMajorKey> m_tempMajorKeyList;
        private KeyCollection m_keys;
        private ValueCollection m_values;

        /// <summary>
        /// Gets or sets the value associated with the specified key.
        /// </summary>
        /// <param name="key">The key of the value to get or set.</param>
        /// <exception cref="System.Collections.Generic.KeyNotFoundException">Thrown if trying to get a key that is not in the dictionary.</exception>
        public TValue this[MultiKey<TMajorKey, TMinorKey> key]
        {
            get
            {
                Dictionary<TMinorKey, TValue> subDictionary;
                if(m_table.TryGetValue(key.Major, out subDictionary))
                    return subDictionary[key.Minor];

                throw new KeyNotFoundException();
            }

            set
            {
                Insert(key, ref value, false);
            }
        }

        /// <summary>
        /// Gets or sets the value associated with the specified key.
        /// </summary>
        /// <param name="majorKey">Major key component.</param>
        /// <param name="minorKey">Minor key component.</param>
        /// <exception cref="System.Collections.Generic.KeyNotFoundException">Thrown if trying to get a key that is not in the dictionary.</exception>
        public TValue this[TMajorKey majorKey, TMinorKey minorKey]
        {
            get
            {
                Dictionary<TMinorKey, TValue> subDictionary;
                if (m_table.TryGetValue(majorKey, out subDictionary))
                    return subDictionary[minorKey];

                throw new KeyNotFoundException();
            }
            set
            {
                Insert(new MultiKey<TMajorKey, TMinorKey>(majorKey, minorKey), ref value, false);
            }
        }

        /// <summary>
        /// Gets the number of elements contained in the dictionary.
        /// </summary>
        public int Count
        {
            get
            {
                return m_totalValueCount;
            }
        }

        /// <summary>
        /// Gets the number of major keys present in the dictionary.
        /// </summary>
        public int TotalMajorKeyCount
        {
            get
            {
                return m_table.Count;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the dictionary is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the comperer used for major key equality.
        /// </summary>
        public IEqualityComparer<TMajorKey> MajorKeyComparer
        {
            get
            {
                return m_table.Comparer;
            }
        }

        /// <summary>
        /// Gets the comparer used for minor key equality.
        /// </summary>
        public IEqualityComparer<TMinorKey> MinorKeyComparer
        {
            get
            {
                return m_minorComparer;
            }
        }

        /// <summary>
        /// Gets a collection containing the keys of the dictionary.
        /// </summary>
        public KeyCollection Keys
        {
            get
            {
                return SafeGetKeys();
            }
        }

        /// <summary>
        /// Gets a collection containing the values of the dictionary.
        /// </summary>
        public ValueCollection Values
        {
            get
            {
                return SafeGetValues();
            }
        }

        /// <summary>
        /// Gets a collection containing the keys of the dictionary.
        /// </summary>
        ICollection<MultiKey<TMajorKey, TMinorKey>> IDictionary<MultiKey<TMajorKey, TMinorKey>, TValue>.Keys
        {
            get
            {
                return SafeGetKeys();
            }
        }

        /// <summary>
        /// Gets a collection containing the values of the dictionary.
        /// </summary>
        ICollection<TValue> IDictionary<MultiKey<TMajorKey, TMinorKey>, TValue>.Values
        {
            get
            {
                return SafeGetValues();
            }
        }

        /// <summary>
        /// Gets a collection containing the keys of the dictionary.
        /// </summary>
        IEnumerable<MultiKey<TMajorKey, TMinorKey>> IReadOnlyDictionary<MultiKey<TMajorKey, TMinorKey>, TValue>.Keys
        {
            get
            {
                return SafeGetKeys();
            }
        }

        /// <summary>
        /// Gets a collection containing the values of the dictionary.
        /// </summary>
        IEnumerable<TValue> IReadOnlyDictionary<MultiKey<TMajorKey, TMinorKey>, TValue>.Values
        {
            get
            {
                return SafeGetValues();
            }
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="MultiKeyDictionary{TMajorKey, TMinorKey, TValue}"/> class.
        /// </summary>
        public MultiKeyDictionary() : this(0, null, null) { }

        /// <summary>
        /// Constructs a new instance of the <see cref="MultiKeyDictionary{TMajorKey, TMinorKey, TValue}"/> class.
        /// </summary>
        /// <param name="capacity">Initial capacity of the dictionary.</param>
        public MultiKeyDictionary(int capacity) : this(capacity, null, null) { }

        /// <summary>
        /// Constructs a new instance of the <see cref="MultiKeyDictionary{TMajorKey, TMinorKey, TValue}"/> class.
        /// </summary>
        /// <param name="majorComparer">Equality comparer for major keys. If null, <see cref="EqualityComparer{TMajorKey}.Default"/> is used.</param>
        /// <param name="minorComparer">Equality comparer for minor keys. If null, <see cref="EqualityComparer{TMinorKey}.Default"/> is used. </param>
        public MultiKeyDictionary(IEqualityComparer<TMajorKey> majorComparer, IEqualityComparer<TMinorKey> minorComparer) : this(0, majorComparer, minorComparer) { }

        /// <summary>
        /// Constructs a new instance of the <see cref="MultiKeyDictionary{TMajorKey, TMinorKey, TValue}"/> class.
        /// </summary>
        /// <param name="capacity">Initial capacity of the dictionary.</param>
        /// <param name="majorComparer">Equality comparer for major keys. If null, <see cref="EqualityComparer{TMajorKey}.Default"/> is used.</param>
        /// <param name="minorComparer">Equality comparer for minor keys. If null, <see cref="EqualityComparer{TMinorKey}.Default"/> is used. </param>
        public MultiKeyDictionary(int capacity, IEqualityComparer<TMajorKey> majorComparer, IEqualityComparer<TMinorKey> minorComparer)
        {
            m_table = new Dictionary<TMajorKey, Dictionary<TMinorKey, TValue>>(0, majorComparer);
            m_freeSubDictionaries = new Queue<Dictionary<TMinorKey, TValue>>(0);
            m_minorComparer = minorComparer ?? EqualityComparer<TMinorKey>.Default;
            m_version = 0;
            m_totalValueCount = 0;
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="MultiKeyDictionary{TMajorKey, TMinorKey, TValue}"/> class.
        /// </summary>
        /// <param name="dictionary">Existing dictionary of key-value pairs to populate this collection with.</param>
        public MultiKeyDictionary(IDictionary<MultiKey<TMajorKey, TMinorKey>, TValue> dictionary) : this(dictionary, null, null) { }

        /// <summary>
        /// Constructs a new instance of the <see cref="MultiKeyDictionary{TMajorKey, TMinorKey, TValue}"/> class.
        /// </summary>
        /// <param name="dictionary">Existing dictionary of key-value pairs to populate this collection with.</param>
        /// <param name="majorComparer">Equality comparer for major keys. If null, <see cref="EqualityComparer{TMajorKey}.Default"/> is used.</param>
        /// <param name="minorComparer">Equality comparer for minor keys. If null, <see cref="EqualityComparer{TMinorKey}.Default"/> is used. </param>
        public MultiKeyDictionary(IDictionary<MultiKey<TMajorKey, TMinorKey>, TValue> dictionary, IEqualityComparer<TMajorKey> majorComparer, IEqualityComparer<TMinorKey> minorComparer)
        {
            m_table = new Dictionary<TMajorKey, Dictionary<TMinorKey, TValue>>(GetCountFromSeedDictionary(dictionary), majorComparer);
            m_freeSubDictionaries = new Queue<Dictionary<TMinorKey, TValue>>(0);
            m_minorComparer = minorComparer ?? EqualityComparer<TMinorKey>.Default;
            
            foreach(KeyValuePair<MultiKey<TMajorKey, TMinorKey>, TValue> kv in dictionary)
            {
                TValue val = kv.Value;
                Insert(kv.Key, ref val, true);
            }

            m_version = 0;
        }

        /// <summary>
        /// Adds the key-value pair to the dictionary.
        /// </summary>
        /// <param name="majorKey">Major key component.</param>
        /// <param name="minorKey">Minor key component.</param>
        /// <param name="value">Value to add.</param>
        public void Add(TMajorKey majorKey, TMinorKey minorKey, TValue value)
        {
            Insert(new MultiKey<TMajorKey, TMinorKey>(majorKey, minorKey), ref value, true);
        }

        /// <summary>
        /// Adds the key-value pair to the dictionary.
        /// </summary>
        /// <param name="key">Key representing the value.</param>
        /// <param name="value">Value to add.</param>
        public void Add(MultiKey<TMajorKey, TMinorKey> key, TValue value)
        {
            Insert(key, ref value, true);
        }

        /// <summary>
        /// Clears the dictionary of values.
        /// </summary>
        public void Clear()
        {
            foreach (KeyValuePair<TMajorKey, Dictionary<TMinorKey, TValue>> kv in m_table)
            {
                kv.Value.Clear();
                FreeSubDictionary(kv.Value);
            }

            m_totalValueCount = 0;
            m_table.Clear();

            m_version++;
        }

        /// <summary>
        /// Clears the dictionary of values associated with the major key.
        /// </summary>
        /// <param name="majorKey">Major key.</param>
        public void ClearMajorKeys(TMajorKey majorKey)
        {
            if (majorKey == null)
                return;

            Dictionary<TMinorKey, TValue> subDict;
            if(m_table.TryGetValue(majorKey, out subDict))
            {
                subDict.Clear();
                FreeSubDictionary(subDict);

                m_table.Remove(majorKey);
            }
        }

        /// <summary>
        /// Clears the dictionary of values associated with the minor key.
        /// </summary>
        /// <param name="minorKey">Minor key.</param>
        public void ClearMinorKeys(TMinorKey minorKey)
        {
            if (minorKey == null)
                return;

            //If we remove minor keys and the subdictionary count goes to zero, we need to remove the subdictionary
            foreach (KeyValuePair<TMajorKey, Dictionary<TMinorKey, TValue>> kv in m_table)
            {
                if(kv.Value.Remove(minorKey) && kv.Value.Count == 0)
                {
                    if (m_tempMajorKeyList == null)
                        m_tempMajorKeyList = new List<TMajorKey>(1);

                    m_tempMajorKeyList.Add(kv.Key);
                    FreeSubDictionary(kv.Value);
                }
            }

            if (m_tempMajorKeyList != null)
            {
                for (int i = 0; i < m_tempMajorKeyList.Count; i++)
                    m_table.Remove(m_tempMajorKeyList[i]);

                m_tempMajorKeyList.Clear();
            }
        }

        /// <summary>
        /// Determines if the major/minor keys are contained in the dictionary.
        /// </summary>
        /// <param name="majorKey">Major key component.</param>
        /// <param name="minorKey">Minor key component.</param>
        /// <returns>True if the key was contained, false if not.</returns>
        public bool ContainsKey(TMajorKey majorKey, TMinorKey minorKey)
        {
            Dictionary<TMinorKey, TValue> subDictionary;
            if (m_table.TryGetValue(majorKey, out subDictionary))
                return subDictionary.ContainsKey(minorKey);

            return false;
        }

        /// <summary>
        /// Determines if the key is contained in the dictionary.
        /// </summary>
        /// <param name="key">Key to check.</param>
        /// <returns>True if the key was contained, false if not.</returns>
        public bool ContainsKey(MultiKey<TMajorKey, TMinorKey> key)
        {
            Dictionary<TMinorKey, TValue> subDictionary;
            if(m_table.TryGetValue(key.Major, out subDictionary))
                return subDictionary.ContainsKey(key.Minor);

            return false;
        }

        /// <summary>
        /// Determines if the value is contained in the dictionary.
        /// </summary>
        /// <param name="value">Value to check.</param>
        /// <returns>True if the value was contained, false if not.</returns>
        public bool ContainsValue(TValue value)
        {
            foreach(KeyValuePair<TMajorKey, Dictionary<TMinorKey, TValue>> kv in m_table)
            {
                if (kv.Value.ContainsValue(value))
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Removes the value with the specified major/minor keys from the dictionary.
        /// </summary>
        /// <param name="majorKey">Major key component.</param>
        /// <param name="minorKey">Minor key component.</param>
        /// <returns>True if the key was found and the value removed, false if it was not.</returns>
        public bool Remove(TMajorKey majorKey, TMinorKey minorKey)
        {
            return Remove(new MultiKey<TMajorKey, TMinorKey>(majorKey, minorKey));
        }

        /// <summary>
        /// Removes the value with the specified key from the dictionary.
        /// </summary>
        /// <param name="key">Key of the value to remove.</param>
        /// <returns>True if the key was found and the value removed, false if it was not.</returns>
        public bool Remove(MultiKey<TMajorKey, TMinorKey> key)
        {
            Dictionary<TMinorKey, TValue> subDictionary;
            if(m_table.TryGetValue(key.Major, out subDictionary))
            {
                if(subDictionary.Remove(key.Minor))
                {
                    m_totalValueCount--;

                    //If no more values, remove from the major key table, but put it back into our pool for future use
                    if(subDictionary.Count == 0)
                    {
                        m_table.Remove(key.Major);
                        FreeSubDictionary(subDictionary);
                    }

                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Gets the value associated with the specified major/minor keys.
        /// </summary>
        /// <param name="majorKey">Major key component.</param>
        /// <param name="minorKey">Minor key component.</param>
        /// <param name="value">Value contained in the dictionary, if not found then default.</param>
        /// <returns>True if the key was found in the dictionary, false if it was not.</returns>
        public bool TryGetValue(TMajorKey majorKey, TMinorKey minorKey, out TValue value)
        {
            Dictionary<TMinorKey, TValue> subDictionary;
            if (m_table.TryGetValue(majorKey, out subDictionary))
                return subDictionary.TryGetValue(minorKey, out value);

            value = default(TValue);
            return false;
        }

        /// <summary>
        /// Gets the value associated with the specified key.
        /// </summary>
        /// <param name="key">Key of the value.</param>
        /// <param name="value">Value contained in the dictionary, if not found then default.</param>
        /// <returns>True if the key was found in the dictionary, false if it was not.</returns>
        public bool TryGetValue(MultiKey<TMajorKey, TMinorKey> key, out TValue value)
        {
            Dictionary<TMinorKey, TValue> subDictionary;
            if (m_table.TryGetValue(key.Major, out subDictionary))
                return subDictionary.TryGetValue(key.Minor, out value);

            value = default(TValue);
            return false;
        }

        /// <summary>
        /// Queries for the total count of values associated with the minor key.
        /// </summary>
        /// <param name="minorKey">Minor key to query with.</param>
        /// <returns>The number of values associated with the minor key.</returns>
        public int QueryMajorKeyCount(TMinorKey minorKey)
        {
            if (m_table.Count == 0 || minorKey == null)
                return 0;

            int totalCount = 0;
            foreach (KeyValuePair<TMajorKey, Dictionary<TMinorKey, TValue>> kv in m_table)
            {
                if (kv.Value.ContainsKey(minorKey))
                    totalCount++;
            }

            return totalCount;
        }

        /// <summary>
        /// Queries for the total count of values associated with the major key.
        /// </summary>
        /// <param name="majorKey">Major key to query with.</param>
        /// <returns>The number of values associated with the major key.</returns>
        public int QueryMinorKeyCount(TMajorKey majorKey)
        {
            if (m_table.Count == 0 || majorKey == null)
                return 0;

            Dictionary<TMinorKey, TValue> subdict;
            if (m_table.TryGetValue(majorKey, out subdict))
                return subdict.Count;

            return 0;
        }

        /// <summary>
        /// Queries all values in the dictionary that match the specified major key.
        /// </summary>
        /// <param name="majorKey">Major key to query with.</param>
        /// <param name="keyValuePairs">List of key-value pairs that match the major key. If null is passed, a list will be created.</param>
        /// <returns>True if at least one value is found with the matching major key, false if no values are returned.</returns>
        public bool QueryValuesWithMajorKey(TMajorKey majorKey, ref List<KeyValuePair<MultiKey<TMajorKey, TMinorKey>, TValue>> keyValuePairs)
        {
            if (m_table.Count == 0 || majorKey == null)
                return false;

            if (keyValuePairs == null)
                keyValuePairs = new List<KeyValuePair<MultiKey<TMajorKey, TMinorKey>, TValue>>();

            MinorKeyValueEnumerator enumerator = GetMinorKeyValueEnumerator(majorKey);
            bool foundOne = false;

            while(enumerator.MoveNext())
            {
                foundOne = true;
                KeyValuePair<TMinorKey, TValue> kv = enumerator.Current;
                keyValuePairs.Add(new KeyValuePair<MultiKey<TMajorKey, TMinorKey>, TValue>(new MultiKey<TMajorKey, TMinorKey>(majorKey, kv.Key), kv.Value));
            }

            return foundOne;
        }

        /// <summary>
        /// Queries all values in the dictionary that match the specified minor key.
        /// </summary>
        /// <param name="minorKey">Minor key to query with.</param>
        /// <param name="keyValuePairs">List of key-value pairs that match the minor key. If null is passed, a list will be created.</param>
        /// <returns>True if at least one value is found with the matching minor key, false if no values are returned.</returns>
        public bool QueryValuesWithMinorKey(TMinorKey minorKey, ref List<KeyValuePair<MultiKey<TMajorKey, TMinorKey>, TValue>> keyValuePairs)
        {
            if (m_table.Count == 0 || minorKey == null)
                return false;

            if (keyValuePairs == null)
                keyValuePairs = new List<KeyValuePair<MultiKey<TMajorKey, TMinorKey>, TValue>>();

            MajorKeyValueEnumerator enumerator = GetMajorKeyValueEnumerator(minorKey);
            bool foundOne = false;

            while(enumerator.MoveNext())
            {
                foundOne = true;
                KeyValuePair<TMajorKey, TValue> kv = enumerator.Current;
                keyValuePairs.Add(new KeyValuePair<MultiKey<TMajorKey, TMinorKey>, TValue>(new MultiKey<TMajorKey, TMinorKey>(kv.Key, minorKey), kv.Value));
            }

            return foundOne;
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>An enumerator that can be used to iterate through the collection.</returns>
        public Enumerator GetEnumerator()
        {
            return new Enumerator(this);
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection, filtering on the specified minor key.
        /// </summary>
        /// <param name="minorKey">Minor key to query with.</param>
        /// <returns>An enumerator that iterates over values that have the associated minor key.</returns>
        public MajorKeyValueEnumerator GetMajorKeyValueEnumerator(TMinorKey minorKey)
        {
            return new MajorKeyValueEnumerator(minorKey, this);
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection, filtering on the specified major key.
        /// </summary>
        /// <param name="majorKey">Major key to query with.</param>
        /// <returns>An enumerator that iterates over values that have the associated major key.</returns>
        public MinorKeyValueEnumerator GetMinorKeyValueEnumerator(TMajorKey majorKey)
        {
            return new MinorKeyValueEnumerator(majorKey, this);
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>An enumerator that can be used to iterate through the collection.</returns>
        IEnumerator<KeyValuePair<MultiKey<TMajorKey, TMinorKey>, TValue>> IEnumerable<KeyValuePair<MultiKey<TMajorKey, TMinorKey>, TValue>>.GetEnumerator()
        {
            return new Enumerator(this);
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>An enumerator that can be used to iterate through the collection.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return new Enumerator(this);
        }

        #region Collections explicit implementation

        /// <summary>
        /// Adds the specified item to the collection.
        /// </summary>
        /// <param name="item">Dictionary item to add.</param>
        void ICollection<KeyValuePair<MultiKey<TMajorKey, TMinorKey>, TValue>>.Add(KeyValuePair<MultiKey<TMajorKey, TMinorKey>, TValue> item)
        {
            TValue val = item.Value;
            Insert(item.Key, ref val, true);
        }

        /// <summary>
        /// Copies the contents of the collection to the array.
        /// </summary>
        /// <param name="array">Array of dictionary items.</param>
        /// <param name="arrayIndex">Starting position to write to the array.</param>
        /// <exception cref="ArgumentNullException">Thrown if the array is null.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown if the index is out of range.</exception>
        /// <exception cref="ArgumentException">Thrown if there is not enough space in the array to write data to.</exception>
        void ICollection<KeyValuePair<MultiKey<TMajorKey, TMinorKey>, TValue>>.CopyTo(KeyValuePair<MultiKey<TMajorKey, TMinorKey>, TValue>[] array, int arrayIndex)
        {
            if (array == null)
                throw new ArgumentNullException("array");

            if (arrayIndex < 0 || arrayIndex > array.Length)
                throw new ArgumentOutOfRangeException("arrayIndex", StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));

            if ((array.Length - arrayIndex) < m_totalValueCount)
                throw new ArgumentException(StringLocalizer.Instance.GetLocalizedString("WriteBuffeOverflow"));

            int currentIndex = arrayIndex;

            foreach(KeyValuePair<TMajorKey, Dictionary<TMinorKey, TValue>> kv in m_table)
            {
                Dictionary<TMinorKey, TValue> subDictionary = kv.Value;
                foreach(KeyValuePair<TMinorKey, TValue> subKv in subDictionary)
                    array[currentIndex++] = new KeyValuePair<MultiKey<TMajorKey, TMinorKey>, TValue>(new MultiKey<TMajorKey, TMinorKey>(kv.Key, subKv.Key), subKv.Value);
            }
        }

        /// <summary>
        /// Determines if the item is contained in the collection.
        /// </summary>
        /// <param name="item">Dictionary item to check.</param>
        /// <returns>True if the item is contained, false otherwise.</returns>
        bool ICollection<KeyValuePair<MultiKey<TMajorKey, TMinorKey>, TValue>>.Contains(KeyValuePair<MultiKey<TMajorKey, TMinorKey>, TValue> item)
        {
            Dictionary<TMinorKey, TValue> subDictionary;
            if (m_table.TryGetValue(item.Key.Major, out subDictionary))
            {
                TValue value;
                if(subDictionary.TryGetValue(item.Key.Minor, out value))
                {
                    return EqualityComparer<TValue>.Default.Equals(value, item.Value);
                }
            }

            return false;
        }

        /// <summary>
        /// Removes an item from the collection.
        /// </summary>
        /// <param name="item">Dictionary item to remove.</param>
        /// <returns>True if the item was successfully removed, false otherwise.</returns>
        bool ICollection<KeyValuePair<MultiKey<TMajorKey, TMinorKey>, TValue>>.Remove(KeyValuePair<MultiKey<TMajorKey, TMinorKey>, TValue> item)
        {
            return Remove(item.Key);
        }

        #endregion

        private void Insert(MultiKey<TMajorKey, TMinorKey> key, ref TValue value, bool add)
        {
            Dictionary<TMinorKey, TValue> subDictionary;
            m_table.TryGetValue(key.Major, out subDictionary);

            //Always add the dictionary for any inserts if it hasn't been created yet
            if (subDictionary == null)
            {
                subDictionary = GetNextFreeSubDictionary();
                m_table.Add(key.Major, subDictionary);
            }

            int prevCount = subDictionary.Count;

            //Follow Dictionary conventions, if a key is present on an add operation an exception occurs,
            //but if not then we want to either add or replace
            if (add)
            {
                subDictionary.Add(key.Minor, value);
            }
            else
            {
                subDictionary[key.Minor] = value;
            }

            //Increment if we added the value
            if (prevCount != subDictionary.Count)
                m_totalValueCount++;

            m_version++;
        }

        private int GetCountFromSeedDictionary(IDictionary<MultiKey<TMajorKey, TMinorKey>, TValue> dict)
        {
            MultiKeyDictionary<TMajorKey, TMinorKey, TValue> mkDict = dict as MultiKeyDictionary<TMajorKey, TMinorKey, TValue>;
            if (mkDict != null)
                return mkDict.TotalMajorKeyCount;

            return (dict != null) ? dict.Count : 0;
        }

        private Dictionary<TMinorKey, TValue> GetNextFreeSubDictionary()
        {
            if (m_freeSubDictionaries.Count > 0)
                return m_freeSubDictionaries.Dequeue();

            return new Dictionary<TMinorKey, TValue>(1, m_minorComparer);
        }

        private void FreeSubDictionary(Dictionary<TMinorKey, TValue> subDictionary)
        {
            m_freeSubDictionaries.Enqueue(subDictionary);
        }

        private KeyCollection SafeGetKeys()
        {
            if (m_keys == null)
                m_keys = new KeyCollection(this);

            return m_keys;
        }

        private ValueCollection SafeGetValues()
        {
            if (m_values == null)
                m_values = new ValueCollection(this);

            return m_values;
        }

        #region Enumerator

        /// <summary>
        /// Enumerator for <see cref="MultiKeyDictionary{TMajorKey, TMinorKey, TValue}"/> that enumerates all
        /// values in the dictionary associated with a certain minor key.
        /// </summary>
        [Serializable, StructLayout(LayoutKind.Sequential)]
        public struct MajorKeyValueEnumerator : IEnumerator<KeyValuePair<TMajorKey, TValue>>
        {
            private MultiKeyDictionary<TMajorKey, TMinorKey, TValue> m_dictionary;
            private Enumerator m_enumerator;
            private KeyValuePair<TMajorKey, TValue> m_current;
            private TMinorKey m_minorKey;

            /// <summary>
            /// Gets the current enumerated key-value pair.
            /// </summary>
            public KeyValuePair<TMajorKey, TValue> Current
            {
                get
                {
                    return m_current;
                }
            }

            /// <summary>
            /// Gets the current enumerated key-value pair.
            /// </summary>
            object IEnumerator.Current
            {
                get
                {
                    return m_current;
                }
            }

            internal MajorKeyValueEnumerator(TMinorKey minorKey, MultiKeyDictionary<TMajorKey, TMinorKey, TValue> mkDictionary)
            {
                m_dictionary = mkDictionary;
                m_enumerator = mkDictionary.GetEnumerator();
                m_current = new KeyValuePair<TMajorKey, TValue>();
                m_minorKey = minorKey;
            }

            /// <summary>
            /// Advances the enumerator to the next element of the collection.
            /// </summary>
            /// <returns>True if the enumerator was successfully advanced to the next element; false if the enumerator has passed the end of the collection.</returns>
            public bool MoveNext()
            {
                bool endOfEnumerator = false;
                while((endOfEnumerator = m_enumerator.MoveNext()) == true)
                {
                    KeyValuePair<MultiKey<TMajorKey, TMinorKey>, TValue> kv = m_enumerator.Current;
                    if(m_dictionary.m_minorComparer.Equals(kv.Key.Minor, m_minorKey))
                    {
                        m_current = new KeyValuePair<TMajorKey, TValue>(kv.Key.Major, kv.Value);
                        return true;
                    }
                }

                if (endOfEnumerator)
                    m_current = new KeyValuePair<TMajorKey, TValue>();

                return false;
            }

            /// <summary>
            /// Sets the enumerator to its initial position, which is before the first element in the collection.
            /// </summary>
            public void Reset()
            {
                m_enumerator.Reset();
                m_current = new KeyValuePair<TMajorKey, TValue>();
            }

            /// <summary>
            /// Not used.
            /// </summary>
            void IDisposable.Dispose() { }
        }

        /// <summary>
        /// Enumerator for <see cref="MultiKeyDictionary{TMajorKey, TMinorKey, TValue}"/> that enumerates all
        /// values in the dictionary associated with a certain major key.
        /// </summary>
        [Serializable, StructLayout(LayoutKind.Sequential)]
        public struct MinorKeyValueEnumerator : IEnumerator<KeyValuePair<TMinorKey, TValue>>
        {
            private MultiKeyDictionary<TMajorKey, TMinorKey, TValue> m_dictionary;
            private Dictionary<TMinorKey, TValue>.Enumerator m_subDictEnumerator;
            private bool m_isValid;
            private TMajorKey m_majorKey;

            /// <summary>
            /// Gets the current enumerated key-value pair.
            /// </summary>
            public KeyValuePair<TMinorKey, TValue> Current
            {
                get
                {
                    if (!m_isValid)
                        return new KeyValuePair<TMinorKey, TValue>();

                    return m_subDictEnumerator.Current;
                }
            }

            /// <summary>
            /// Gets the current enumerated key-value pair.
            /// </summary>
            object IEnumerator.Current
            {
                get
                {
                    if (!m_isValid)
                        return new KeyValuePair<TMinorKey, TValue>();

                    return m_subDictEnumerator.Current;
                }
            }

            internal MinorKeyValueEnumerator(TMajorKey majorKey, MultiKeyDictionary<TMajorKey, TMinorKey, TValue> mkDictionary)
            {
                m_majorKey = majorKey;
                m_dictionary = mkDictionary;

                Dictionary<TMinorKey, TValue> subDict;
                m_isValid = mkDictionary.m_table.TryGetValue(majorKey, out subDict);

                if (m_isValid)
                    m_subDictEnumerator = subDict.GetEnumerator();
                else
                    m_subDictEnumerator = new Dictionary<TMinorKey, TValue>.Enumerator();
            }

            /// <summary>
            /// Advances the enumerator to the next element of the collection.
            /// </summary>
            /// <returns>True if the enumerator was successfully advanced to the next element; false if the enumerator has passed the end of the collection.</returns>
            public bool MoveNext()
            {
                if (!m_isValid)
                    return false;

                return m_subDictEnumerator.MoveNext();
            }

            /// <summary>
            /// Sets the enumerator to its initial position, which is before the first element in the collection.
            /// </summary>
            public void Reset()
            {
                if (!m_isValid)
                    return;

                Dictionary<TMinorKey, TValue> subDict;
                m_isValid = m_dictionary.m_table.TryGetValue(m_majorKey, out subDict);

                if (m_isValid)
                    m_subDictEnumerator = subDict.GetEnumerator();
                else
                    m_subDictEnumerator = new Dictionary<TMinorKey, TValue>.Enumerator();
            }

            /// <summary>
            /// Not used.
            /// </summary>
            void IDisposable.Dispose() { }
        }

        /// <summary>
        /// Enumerator for <see cref="MultiKeyDictionary{TMajorKey, TMinorKey, TValue}"/>.
        /// </summary>
        [Serializable, StructLayout(LayoutKind.Sequential)]
        public struct Enumerator : IEnumerator<KeyValuePair<MultiKey<TMajorKey, TMinorKey>, TValue>>
        {
            private MultiKeyDictionary<TMajorKey, TMinorKey, TValue> m_dictionary;
            private Dictionary<TMajorKey, Dictionary<TMinorKey, TValue>>.Enumerator m_tableEnumerator;
            private Dictionary<TMinorKey, TValue>.Enumerator m_currentSubDictEnumerator;
            private bool m_hasTableEnum, m_hasCurrentSubDictEnum;

            private int m_version;
            private KeyValuePair<MultiKey<TMajorKey, TMinorKey>, TValue> m_current;

            /// <summary>
            /// Gets the current enumerated key-value pair.
            /// </summary>
            public KeyValuePair<MultiKey<TMajorKey, TMinorKey>, TValue> Current
            {
                get
                {
                    return m_current;
                }
            }

            /// <summary>
            /// Gets the current enumerated key-value pair.
            /// </summary>
            Object IEnumerator.Current
            {
                get
                {
                    return m_current;
                }
            }

            internal Enumerator(MultiKeyDictionary<TMajorKey, TMinorKey, TValue> mkDictionary)
            {
                m_dictionary = mkDictionary;
                m_tableEnumerator = mkDictionary.m_table.GetEnumerator();
                m_currentSubDictEnumerator = new Dictionary<TMinorKey, TValue>.Enumerator();
                m_hasTableEnum = true;
                m_hasCurrentSubDictEnum = false;
                m_version = mkDictionary.m_version;

                m_current = new KeyValuePair<MultiKey<TMajorKey, TMinorKey>, TValue>();
            }

            /// <summary>
            /// Advances the enumerator to the next element of the collection.
            /// </summary>
            /// <returns>True if the enumerator was successfully advanced to the next element; false if the enumerator has passed the end of the collection.</returns>
            public bool MoveNext()
            {
                ThrowIfChanged();

                //If table enumerator null, we hit the end of the enumeration and provide an early out. Reset needs to be called
                if (!m_hasTableEnum)
                    return false;

                //If subdict enumerator null already, we are at the start of the enumeration
                if (!m_hasCurrentSubDictEnum)
                    return AdvanceToNextSubDictionaryEnumerator();

                //Else we have a valid subdict enumerator
                if(m_currentSubDictEnumerator.MoveNext())
                {
                    SetCurrent();
                    return true;
                }
                else
                {
                    //End of the sub dict collection, take a look at the next one, if applicable.
                    m_currentSubDictEnumerator = new Dictionary<TMinorKey, TValue>.Enumerator();
                    m_hasCurrentSubDictEnum = false;
                    return AdvanceToNextSubDictionaryEnumerator();
                }
            }

            /// <summary>
            /// Sets the enumerator to its initial position, which is before the first element in the collection.
            /// </summary>
            public void Reset()
            {
                ThrowIfChanged();

                m_tableEnumerator = m_dictionary.m_table.GetEnumerator();
                m_currentSubDictEnumerator = new Dictionary<TMinorKey, TValue>.Enumerator();
                m_hasTableEnum = true;
                m_hasCurrentSubDictEnum = false;

                m_current = new KeyValuePair<MultiKey<TMajorKey, TMinorKey>, TValue>();
            }

            /// <summary>
            /// Not used.
            /// </summary>
            void IDisposable.Dispose() { }

            private void ThrowIfChanged()
            {
                if (m_version != m_dictionary.m_version)
                    throw new InvalidOperationException(StringLocalizer.Instance.GetLocalizedString("CollectionModifiedDuringEnumeration"));
            }

            private bool AdvanceToNextSubDictionaryEnumerator()
            {
                while (m_hasTableEnum && !m_hasCurrentSubDictEnum)
                {
                    if (m_tableEnumerator.MoveNext())
                    {
                        //If we found a sub dictionary, take a look
                        Dictionary<TMinorKey, TValue>.Enumerator subDictEnum = m_tableEnumerator.Current.Value.GetEnumerator();

                        //If it has items, then move to the next and set it as the current enumerator
                        if (subDictEnum.MoveNext())
                        {
                            //Found a sub dict with a collection not empty
                            m_currentSubDictEnumerator = subDictEnum;
                            m_hasCurrentSubDictEnum = true;
                            SetCurrent();

                            return true;
                        }
                        else
                        {
                            //Else empty, keep looking at table enumerator
                            m_currentSubDictEnumerator = new Dictionary<TMinorKey, TValue>.Enumerator();
                            m_hasCurrentSubDictEnum = false;
                        }
                    }
                    else
                    {
                        //If no more items in table, then we hit the end of the our enumeration
                        SetCurrent(true);
                        m_currentSubDictEnumerator = new Dictionary<TMinorKey, TValue>.Enumerator();
                        m_tableEnumerator = new Dictionary<TMajorKey, Dictionary<TMinorKey, TValue>>.Enumerator();
                        m_hasCurrentSubDictEnum = false;
                        m_hasTableEnum = false;
                        return false;
                    }
                }

                return false;
            }

            private void SetCurrent(bool nullOut = false)
            {
                if(nullOut)
                {
                    m_current = new KeyValuePair<MultiKey<TMajorKey, TMinorKey>, TValue>();
                    return;
                }

                KeyValuePair<TMinorKey, TValue> minorKv = m_currentSubDictEnumerator.Current;
                m_current = new KeyValuePair<MultiKey<TMajorKey, TMinorKey>, TValue>(
                    new MultiKey<TMajorKey, TMinorKey>(m_tableEnumerator.Current.Key, minorKv.Key), minorKv.Value);
            }
        }

        #endregion

        #region KeyCollection

        /// <summary>
        /// Represents the collection of keys in a <see cref="MultiKeyDictionary{TMajorKey, TMinorKey, TValue}"/>.
        /// </summary>
        [DebuggerTypeProxy(typeof(MultiKeyDictionary<,,>.KeyCollectionDebugView)), DebuggerDisplay("Count = {Count}")]
        public sealed class KeyCollection : ICollection<MultiKey<TMajorKey, TMinorKey>>, IEnumerable<MultiKey<TMajorKey, TMinorKey>>, IReadOnlyCollection<MultiKey<TMajorKey, TMinorKey>>
        {
            private MultiKeyDictionary<TMajorKey, TMinorKey, TValue> m_dictionary;

            /// <summary>
            /// Gets the number of keys in the dictionary.
            /// </summary>
            public int Count
            {
                get
                {
                    return m_dictionary.Count;
                }
            }

            /// <summary>
            /// Gets if this collection is read only. This will always return true.
            /// </summary>
            public bool IsReadOnly
            {
                get
                {
                    return true;
                }
            }

            internal KeyCollection(MultiKeyDictionary<TMajorKey, TMinorKey, TValue> dictionary)
            {
                m_dictionary = dictionary;
            }

            /// <summary>
            /// Copies the contents of the collection to the array.
            /// </summary>
            /// <param name="array">Array to copy data to.</param>
            /// <param name="arrayIndex">Zero-based starting index in the array at which to start copying to.</param>
            public void CopyTo(MultiKey<TMajorKey, TMinorKey>[] array, int arrayIndex)
            {
                if (array == null)
                    throw new ArgumentNullException("array");

                if (arrayIndex < 0 || arrayIndex > array.Length)
                    throw new ArgumentOutOfRangeException("arrayIndex", StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));

                if ((array.Length - arrayIndex) < m_dictionary.Count)
                    throw new ArgumentException(StringLocalizer.Instance.GetLocalizedString("WriteBuffeOverflow"));

                int currentIndex = arrayIndex;

                foreach (KeyValuePair<TMajorKey, Dictionary<TMinorKey, TValue>> kv in m_dictionary.m_table)
                {
                    Dictionary<TMinorKey, TValue> subDictionary = kv.Value;
                    foreach (KeyValuePair<TMinorKey, TValue> subKv in subDictionary)
                        array[currentIndex++] = new MultiKey<TMajorKey, TMinorKey>(kv.Key, subKv.Key);
                }
            }

            /// <summary>
            /// Returns an enumerator that iterates through the collection.
            /// </summary>
            /// <returns>An enumerator that can be used to iterate through the collection.</returns>
            public Enumerator GetEnumerator()
            {
                return new Enumerator(this);
            }

            /// <summary>
            /// Returns an enumerator that iterates through the collection.
            /// </summary>
            /// <returns>An enumerator that can be used to iterate through the collection.</returns>
            IEnumerator<MultiKey<TMajorKey, TMinorKey>> IEnumerable<MultiKey<TMajorKey, TMinorKey>>.GetEnumerator()
            {
                return new Enumerator(this);
            }

            /// <summary>
            /// Returns an enumerator that iterates through the collection.
            /// </summary>
            /// <returns>An enumerator that can be used to iterate through the collection.</returns>
            IEnumerator IEnumerable.GetEnumerator()
            {
                return new Enumerator(this);
            }

            /// <summary>
            /// Not Supported.
            /// </summary>
            void ICollection<MultiKey<TMajorKey, TMinorKey>>.Add(MultiKey<TMajorKey, TMinorKey> item)
            {
                throw new NotSupportedException(StringLocalizer.Instance.GetLocalizedString("OperationNotSupportedIsReadOnly"));
            }

            /// <summary>
            /// Not Supported.
            /// </summary>
            void ICollection<MultiKey<TMajorKey, TMinorKey>>.Clear()
            {
                throw new NotSupportedException(StringLocalizer.Instance.GetLocalizedString("OperationNotSupportedIsReadOnly"));
            }

            /// <summary>
            /// Not Supported.
            /// </summary>
            bool ICollection<MultiKey<TMajorKey, TMinorKey>>.Remove(MultiKey<TMajorKey, TMinorKey> item)
            {
                throw new NotSupportedException(StringLocalizer.Instance.GetLocalizedString("OperationNotSupportedIsReadOnly"));
            }

            /// <summary>
            /// Determines if the value is contained in the collection.
            /// </summary>
            /// <param name="item">Item to check.</param>
            /// <returns>True if the value is in the collection, false if not.</returns>
            bool ICollection<MultiKey<TMajorKey, TMinorKey>>.Contains(MultiKey<TMajorKey, TMinorKey> item)
            {
                return m_dictionary.ContainsKey(item);
            }

            /// <summary>
            /// Enumerator for <see cref="KeyCollection"/>.
            /// </summary>
            public struct Enumerator : IEnumerator<MultiKey<TMajorKey, TMinorKey>>
            {
                private MultiKeyDictionary<TMajorKey, TMinorKey, TValue>.Enumerator m_dictEnumerator;

                /// <summary>
                /// Gets the current enumerated key.
                /// </summary>
                public MultiKey<TMajorKey, TMinorKey> Current
                {
                    get
                    {
                        return m_dictEnumerator.Current.Key;
                    }
                }

                /// <summary>
                /// Gets the current enumerated key.
                /// </summary>
                Object IEnumerator.Current
                {
                    get
                    {
                        return m_dictEnumerator.Current.Key;
                    }
                }

                internal Enumerator(KeyCollection keys)
                {
                    m_dictEnumerator = keys.m_dictionary.GetEnumerator();
                }

                /// <summary>
                /// Advances the enumerator to the next element of the collection.
                /// </summary>
                /// <returns>True if the enumerator was successfully advanced to the next element; false if the enumerator has passed the end of the collection.</returns>
                public bool MoveNext()
                {
                    return m_dictEnumerator.MoveNext();
                }

                /// <summary>
                /// Sets the enumerator to its initial position, which is before the first element in the collection.
                /// </summary>
                public void Reset()
                {
                    m_dictEnumerator.Reset();
                }

                /// <summary>
                /// Not used.
                /// </summary>
                void IDisposable.Dispose() { }
            }
        }

        #endregion

        #region ValueCollection

        /// <summary>
        /// Represents the collection of values in the <see cref="MultiKeyDictionary{TMajorKey, TMinorKey, TValue}"/>.
        /// </summary>
        [DebuggerTypeProxy(typeof(MultiKeyDictionary<,,>.ValueCollectionDebugView)), DebuggerDisplay("Count = {Count}")]
        public sealed class ValueCollection : ICollection<TValue>, IEnumerable<TValue>, IReadOnlyCollection<TValue>
        {
            private MultiKeyDictionary<TMajorKey, TMinorKey, TValue> m_dictionary;

            /// <summary>
            /// Gets the number of values in the dictionary.
            /// </summary>
            public int Count
            {
                get
                {
                    return m_dictionary.Count;
                }
            }

            /// <summary>
            /// Gets if this collection is read only. This will always return true.
            /// </summary>
            public bool IsReadOnly
            {
                get
                {
                    return true;
                }
            }

            internal ValueCollection(MultiKeyDictionary<TMajorKey, TMinorKey, TValue> dictionary)
            {
                m_dictionary = dictionary;
            }

            /// <summary>
            /// Copies the contents of the collection to the array.
            /// </summary>
            /// <param name="array">Array to copy data to.</param>
            /// <param name="arrayIndex">Zero-based starting index in the array at which to start copying to.</param>
            public void CopyTo(TValue[] array, int arrayIndex)
            {
                if (array == null)
                    throw new ArgumentNullException("array");

                if (arrayIndex < 0 || arrayIndex > array.Length)
                    throw new ArgumentOutOfRangeException("arrayIndex", StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));

                if ((array.Length - arrayIndex) < m_dictionary.Count)
                    throw new ArgumentException(StringLocalizer.Instance.GetLocalizedString("WriteBuffeOverflow"));

                int currentIndex = arrayIndex;

                foreach (KeyValuePair<TMajorKey, Dictionary<TMinorKey, TValue>> kv in m_dictionary.m_table)
                {
                    Dictionary<TMinorKey, TValue> subDictionary = kv.Value;
                    foreach (KeyValuePair<TMinorKey, TValue> subKv in subDictionary)
                        array[currentIndex++] = subKv.Value;
                }
            }

            /// <summary>
            /// Returns an enumerator that iterates through the collection.
            /// </summary>
            /// <returns>An enumerator that can be used to iterate through the collection.</returns>
            public Enumerator GetEnumerator()
            {
                return new Enumerator(this);
            }

            /// <summary>
            /// Returns an enumerator that iterates through the collection.
            /// </summary>
            /// <returns>An enumerator that can be used to iterate through the collection.</returns>
            IEnumerator<TValue> IEnumerable<TValue>.GetEnumerator()
            {
                return new Enumerator(this);
            }

            /// <summary>
            /// Returns an enumerator that iterates through the collection.
            /// </summary>
            /// <returns>An enumerator that can be used to iterate through the collection.</returns>
            IEnumerator IEnumerable.GetEnumerator()
            {
                return new Enumerator(this);
            }

            /// <summary>
            /// Not Supported.
            /// </summary>
            void ICollection<TValue>.Add(TValue item)
            {
                throw new NotSupportedException(StringLocalizer.Instance.GetLocalizedString("OperationNotSupportedIsReadOnly"));
            }

            /// <summary>
            /// Not Supported.
            /// </summary>
            void ICollection<TValue>.Clear()
            {
                throw new NotSupportedException(StringLocalizer.Instance.GetLocalizedString("OperationNotSupportedIsReadOnly"));
            }

            /// <summary>
            /// Not Supported.
            /// </summary>
            bool ICollection<TValue>.Remove(TValue item)
            {
                throw new NotSupportedException(StringLocalizer.Instance.GetLocalizedString("OperationNotSupportedIsReadOnly"));
            }

            /// <summary>
            /// Determines if the value is contained in the collection.
            /// </summary>
            /// <param name="item">Item to check.</param>
            /// <returns>True if the value is in the collection, false if not.</returns>
            bool ICollection<TValue>.Contains(TValue item)
            {
                return m_dictionary.ContainsValue(item);
            }

            /// <summary>
            /// Enumerator for <see cref="ValueCollection"/>.
            /// </summary>
            public struct Enumerator : IEnumerator<TValue>
            {
                private MultiKeyDictionary<TMajorKey, TMinorKey, TValue>.Enumerator m_dictEnumerator;

                /// <summary>
                /// Gets the current enumerated value.
                /// </summary>
                public TValue Current
                {
                    get
                    {
                        return m_dictEnumerator.Current.Value;
                    }
                }

                /// <summary>
                /// Gets the current enumerated value.
                /// </summary>
                Object IEnumerator.Current
                {
                    get
                    {
                        return m_dictEnumerator.Current.Value;
                    }
                }

                internal Enumerator(ValueCollection values)
                {
                    m_dictEnumerator = values.m_dictionary.GetEnumerator();
                }

                /// <summary>
                /// Advances the enumerator to the next element of the collection.
                /// </summary>
                /// <returns>True if the enumerator was successfully advanced to the next element; false if the enumerator has passed the end of the collection.</returns>
                public bool MoveNext()
                {
                    return m_dictEnumerator.MoveNext();
                }

                /// <summary>
                /// Sets the enumerator to its initial position, which is before the first element in the collection.
                /// </summary>
                public void Reset()
                {
                    m_dictEnumerator.Reset();
                }

                /// <summary>
                /// Not used.
                /// </summary>
                void IDisposable.Dispose() { }
            }
        }

        #endregion

        #region Debug Proxies

        private sealed class DebugView
        {
            private MultiKeyDictionary<TMajorKey, TMinorKey, TValue> m_dict;

            public DebugView(MultiKeyDictionary<TMajorKey, TMinorKey, TValue> dict)
            {
                m_dict = dict;
            }

            [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
            public KeyValuePair<MultiKey<TMajorKey, TMinorKey>, TValue>[] Items
            {
                get
                {
                    KeyValuePair<MultiKey<TMajorKey, TMinorKey>, TValue>[] array = new KeyValuePair<MultiKey<TMajorKey, TMinorKey>, TValue>[m_dict.Count];
                    (m_dict as ICollection<KeyValuePair<MultiKey<TMajorKey, TMinorKey>, TValue>>).CopyTo(array, 0);

                    return array;
                }
            }
        }

        private sealed class KeyCollectionDebugView
        {
            private KeyCollection m_keys;

            public KeyCollectionDebugView(KeyCollection keys)
            {
                m_keys = keys;
            }

            [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
            public MultiKey<TMajorKey, TMinorKey>[] Items
            {
                get
                {
                    MultiKey<TMajorKey, TMinorKey>[] array = new MultiKey<TMajorKey, TMinorKey>[m_keys.Count];
                    m_keys.CopyTo(array, 0);

                    return array;
                }
            }
        }

        private sealed class ValueCollectionDebugView
        {
            private ValueCollection m_values;

            public ValueCollectionDebugView(ValueCollection values)
            {
                m_values = values;
            }

            [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
            public TValue[] Items
            {
                get
                {
                    TValue[] array = new TValue[m_values.Count];
                    m_values.CopyTo(array, 0);

                    return array;
                }
            }
        }

        #endregion
    }
}
