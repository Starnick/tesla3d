﻿/*
* Copyright (c) 2010-2017 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;

namespace Tesla
{
    //Array helper for ref-comparison sort and search
    internal interface IRefArraySortHelper<T> where T : struct
    {
        int BinarySearch(T[] keys, int index, int length, in T value, IRefComparer<T> comparer);
        void Sort(T[] keys, int index, int length, IRefComparer<T> comparer);
    }

    //For doing sort/search using types that implement IRefComparable OR a custom comparer.
    internal class RefArraySortHelper<T> : IRefArraySortHelper<T> where T : struct
    {
        internal const int IntroSortSizeThreshold = 16; //Threshold to when intro sort switches to insertion sort
        internal const int QuickSortDepthThreshold = 32;

        private static IRefArraySortHelper<T> s_defaultSortHelper;

        public static IRefArraySortHelper<T> Default
        {
            get
            {
                IRefArraySortHelper<T> sortHelper = s_defaultSortHelper;
                if (sortHelper == null)
                {
                    sortHelper = CreateArraySortHelper();
                    s_defaultSortHelper = sortHelper;
                }

                return sortHelper;
            }
        }

        public int BinarySearch(T[] keys, int index, int length, in T value, IRefComparer<T> comparer)
        {
            try
            {
                if (comparer == null)
                    comparer = RefComparer<T>.Default;

                return BinarySearchInternal(keys, index, length, in value, comparer);
            } 
            catch(Exception e)
            {
                throw new InvalidOperationException(StringLocalizer.Instance.GetLocalizedString("InvalidOperation_IRefComparerFailed"), e);
            }
        }

        public void Sort(T[] keys, int index, int length, IRefComparer<T> comparer)
        {
            try
            {
                if (comparer == null)
                    comparer = RefComparer<T>.Default;

                IntrospectiveSort(keys, index, length, comparer);
            }
            catch (Exception e)
            {
                throw new InvalidOperationException(StringLocalizer.Instance.GetLocalizedString("InvalidOperation_IRefComparerFailed"), e);
            }
        }

        private static IRefArraySortHelper<T> CreateArraySortHelper()
        {
            Type type = typeof(T);

            //If its a IRefComparable<T>, we can create a specialized helper that doesn't require a comparer
            if(typeof(IRefComparable<T>).IsAssignableFrom(type))
            {
                Type sortHelperType = typeof(RefComparableArraySortHelper<>);
                sortHelperType = sortHelperType.MakeGenericType(type);

                return (IRefArraySortHelper<T>)Activator.CreateInstance(sortHelperType);
            }

            return new RefArraySortHelper<T>();
        }

        #region C# Reference Source IntrospectiveSort and BinarySearch (With minor edits)

        internal static int BinarySearchInternal(T[] keys, int index, int length, in T value, IRefComparer<T> comparer)
        {
            //From C# reference source

            int lo = index;
            int hi = index + length - 1;

            while (lo <= hi)
            {
                int i = lo + ((hi - lo) >> 1);
                int order = comparer.Compare(in keys[i], in value);

                if (order == 0)
                    return i;

                if (order < 0)
                    lo = i + 1;
                else
                    hi = i - 1;
            }

            return ~lo;
        }

        internal static int FloorLog2(int n)
        {
            //From C# reference source

            int result = 0;
            while(n >= 1)
            {
                result++;
                n = n / 2;
            }

            return result;
        }

        internal static void IntrospectiveSort(T[] keys, int left, int length, IRefComparer<T> comparer)
        {
            //From C# reference source

            if (length < 2)
                return;

            IntroSort(keys, left, length + left - 1, 2 * FloorLog2(keys.Length), comparer);
        }

        private static void IntroSort(T[] keys, int lo, int hi, int depthLimit, IRefComparer<T> comparer)
        {
            //From C# reference source

            while (hi > lo)
            {
                int partitionSize = hi - lo + 1;
                if (partitionSize <= IntroSortSizeThreshold)
                {
                    if (partitionSize == 1)
                    {
                        return;
                    }

                    if (partitionSize == 2)
                    {
                        SwapIfGreater(keys, comparer, lo, hi);
                        return;
                    }

                    if (partitionSize == 3)
                    {
                        SwapIfGreater(keys, comparer, lo, hi - 1);
                        SwapIfGreater(keys, comparer, lo, hi);
                        SwapIfGreater(keys, comparer, hi - 1, hi);
                        return;
                    }

                    InsertionSort(keys, lo, hi, comparer);
                    return;
                }

                if (depthLimit == 0)
                {
                    Heapsort(keys, lo, hi, comparer);
                    return;
                }

                depthLimit--;

                int p = PickPivotAndPartition(keys, lo, hi, comparer);

                // Note we've already partitioned around the pivot and do not have to move the pivot again.
                IntroSort(keys, p + 1, hi, depthLimit, comparer);

                hi = p - 1;
            }
        }

        private static int PickPivotAndPartition(T[] keys, int lo, int hi, IRefComparer<T> comparer)
        {
            //From C# reference source

            // Compute median-of-three.  But also partition them, since we've done the comparison.
            int middle = lo + ((hi - lo) / 2);

            // Sort lo, mid and hi appropriately, then pick mid as the pivot.
            SwapIfGreater(keys, comparer, lo, middle);  // swap the low with the mid point
            SwapIfGreater(keys, comparer, lo, hi);   // swap the low with the high
            SwapIfGreater(keys, comparer, middle, hi); // swap the middle with the high

            T pivot = keys[middle];
            Swap(keys, middle, hi - 1);
            int left = lo, right = hi - 1;  // We already partitioned lo and hi and put the pivot in hi - 1.  And we pre-increment & decrement below.

            while (left < right)
            {
                while (comparer.Compare(in keys[++left], in pivot) < 0) ;
                while (comparer.Compare(in pivot, in keys[--right]) < 0) ;

                if (left >= right)
                    break;

                Swap(keys, left, right);
            }

            // Put pivot in the right location.
            Swap(keys, left, (hi - 1));
            return left;
        }

        private static void Heapsort(T[] keys, int lo, int hi, IRefComparer<T> comparer)
        {
            //From C# reference source

            int n = hi - lo + 1;
            for (int i = n / 2; i >= 1; i = i - 1)
                DownHeap(keys, i, n, lo, comparer);

            for (int i = n; i > 1; i = i - 1)
            {
                Swap(keys, lo, lo + i - 1);
                DownHeap(keys, 1, i - 1, lo, comparer);
            }
        }

        private static void DownHeap(T[] keys, int i, int n, int lo, IRefComparer<T> comparer)
        {
            //From C# reference source

            T d = keys[lo + i - 1];
            int child;
            while (i <= n / 2)
            {
                child = 2 * i;
                if (child < n && comparer.Compare(in keys[lo + child - 1], in keys[lo + child]) < 0)
                    child++;

                if (!(comparer.Compare(in d, in keys[lo + child - 1]) < 0))
                    break;

                keys[lo + i - 1] = keys[lo + child - 1];
                i = child;
            }

            keys[lo + i - 1] = d;
        }

        private static void InsertionSort(T[] keys, int lo, int hi, IRefComparer<T> comparer)
        {
            //From C# reference source

            int i, j;
            T t;
            for (i = lo; i < hi; i++)
            {
                j = i;
                t = keys[i + 1];

                while (j >= lo && comparer.Compare(in t, in keys[j]) < 0)
                {
                    keys[j + 1] = keys[j];
                    j--;
                }

                keys[j + 1] = t;
            }
        }

        private static void SwapIfGreater(T[] keys, IRefComparer<T> comparer, int a, int b)
        {
            //From C# reference source

            if (a != b)
            {
                if (comparer.Compare(in keys[a], in keys[b]) > 0)
                {
                    T key = keys[a];
                    keys[a] = keys[b];
                    keys[b] = key;
                }
            }
        }

        private static void Swap(T[] a, int i, int j)
        {
            //From C# reference source

            if (i != j)
            {
                T t = a[i];
                a[i] = a[j];
                a[j] = t;
            }
        }

        #endregion
    }

    //For types that explicitly implement IRefComparable and we either are using no or default comparer (it'll just use the CompareTo).
    internal class RefComparableArraySortHelper<T> : IRefArraySortHelper<T> where T : struct, IRefComparable<T>
    {
        public int BinarySearch(T[] keys, int index, int length, in T value, IRefComparer<T> comparer)
        {
            try
            {
                //If null or default comparer, we'll be using the type's CompareTo method anyways, so
                //we can save on the virtual call
                if (comparer == null || comparer == RefComparer<T>.Default)
                    return BinarySearchInternal(keys, index, length, in value);

                return RefArraySortHelper<T>.BinarySearchInternal(keys, index, length, in value, comparer);
            }
            catch (Exception e)
            {
                throw new InvalidOperationException(StringLocalizer.Instance.GetLocalizedString("InvalidOperation_IRefComparerFailed"), e);
            }
        }

        public void Sort(T[] keys, int index, int length, IRefComparer<T> comparer)
        {
            try
            {
                //If null or default comparer, we'll be using the type's CompareTo method anyways, so
                //we can save on the virtual call
                if(comparer == null || comparer == RefComparer<T>.Default)
                {
                    IntrospectiveSort(keys, index, length);
                }
                else
                {
                    RefArraySortHelper<T>.IntrospectiveSort(keys, index, length, comparer);
                }

            }
            catch (Exception e)
            {
                throw new InvalidOperationException(StringLocalizer.Instance.GetLocalizedString("InvalidOperation_IRefComparerFailed"), e);
            }
        }

        #region C# Reference Source IntrospectiveSort and BinarySearch (With minor edits)

        internal static int BinarySearchInternal(T[] keys, int index, int length, in T value)
        {
            //From C# reference source

            int lo = index;
            int hi = index + length - 1;

            while (lo <= hi)
            {
                int i = lo + ((hi - lo) >> 1);
                int order = keys[i].CompareTo(in value);

                if (order == 0)
                    return i;

                if (order < 0)
                    lo = i + 1;
                else
                    hi = i - 1;
            }

            return ~lo;
        }

        internal static void IntrospectiveSort(T[] keys, int left, int length)
        {
            //From C# reference source

            if (length < 2)
                return;

            IntroSort(keys, left, length + left - 1, 2 * RefArraySortHelper<T>.FloorLog2(keys.Length));
        }

        private static void IntroSort(T[] keys, int lo, int hi, int depthLimit)
        {
            //From C# reference source

            while (hi > lo)
            {
                int partitionSize = hi - lo + 1;
                if (partitionSize <= RefArraySortHelper<T>.IntroSortSizeThreshold)
                {
                    if (partitionSize == 1)
                    {
                        return;
                    }

                    if (partitionSize == 2)
                    {
                        SwapIfGreaterWithItems(keys, lo, hi);
                        return;
                    }

                    if (partitionSize == 3)
                    {
                        SwapIfGreaterWithItems(keys, lo, hi - 1);
                        SwapIfGreaterWithItems(keys, lo, hi);
                        SwapIfGreaterWithItems(keys, hi - 1, hi);
                        return;
                    }

                    InsertionSort(keys, lo, hi);
                    return;
                }

                if (depthLimit == 0)
                {
                    Heapsort(keys, lo, hi);
                    return;
                }

                depthLimit--;

                int p = PickPivotAndPartition(keys, lo, hi);

                // Note we've already partitioned around the pivot and do not have to move the pivot again.
                IntroSort(keys, p + 1, hi, depthLimit);

                hi = p - 1;
            }
        }

        private static int PickPivotAndPartition(T[] keys, int lo, int hi)
        {
            //From C# reference source

            // Compute median-of-three.  But also partition them, since we've done the comparison.
            int middle = lo + ((hi - lo) / 2);

            // Sort lo, mid and hi appropriately, then pick mid as the pivot.
            SwapIfGreaterWithItems(keys, lo, middle);  // swap the low with the mid point
            SwapIfGreaterWithItems(keys, lo, hi);   // swap the low with the high
            SwapIfGreaterWithItems(keys, middle, hi); // swap the middle with the high

            T pivot = keys[middle];
            Swap(keys, middle, hi - 1);
            int left = lo, right = hi - 1;  // We already partitioned lo and hi and put the pivot in hi - 1.  And we pre-increment & decrement below.

            while (left < right)
            {
                while (pivot.CompareTo(in keys[++left]) > 0) ;
                while (pivot.CompareTo(in keys[--right]) < 0) ;

                if (left >= right)
                    break;

                Swap(keys, left, right);
            }

            // Put pivot in the right location.
            Swap(keys, left, (hi - 1));
            return left;
        }

        private static void Heapsort(T[] keys, int lo, int hi)
        {
            //From C# reference source

            int n = hi - lo + 1;
            for (int i = n / 2; i >= 1; i = i - 1)
            {
                DownHeap(keys, i, n, lo);
            }

            for (int i = n; i > 1; i = i - 1)
            {
                Swap(keys, lo, lo + i - 1);
                DownHeap(keys, 1, i - 1, lo);
            }
        }

        private static void DownHeap(T[] keys, int i, int n, int lo)
        {
            //From C# reference source

            T d = keys[lo + i - 1];
            int child;
            while (i <= n / 2)
            {
                child = 2 * i;
                if (child < n && (keys[lo + child - 1].CompareTo(in keys[lo + child]) < 0))
                    child++;

                if (keys[lo + child - 1].CompareTo(in d) < 0)
                    break;

                keys[lo + i - 1] = keys[lo + child - 1];
                i = child;
            }

            keys[lo + i - 1] = d;
        }

        private static void InsertionSort(T[] keys, int lo, int hi)
        {
            //From C# reference source

            int i, j;
            T t;
            for (i = lo; i < hi; i++)
            {
                j = i;
                t = keys[i + 1];

                while (j >= lo && (t.CompareTo(in keys[j]) < 0))
                {
                    keys[j + 1] = keys[j];
                    j--;
                }

                keys[j + 1] = t;
            }
        }

        private static void SwapIfGreaterWithItems(T[] keys, int a, int b)
        {
            //From C# reference source

            if (a != b)
            {
                if (keys[a].CompareTo(in keys[b]) > 0)
                {
                    T key = keys[a];
                    keys[a] = keys[b];
                    keys[b] = key;
                }
            }
        }

        private static void Swap(T[] a, int i, int j)
        {
            //From C# reference source

            if (i != j)
            {
                T t = a[i];
                a[i] = a[j];
                a[j] = t;
            }
        }

        #endregion
    }
}
