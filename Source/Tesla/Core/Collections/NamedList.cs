﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections;
using System.Collections.Generic;

namespace Tesla
{
    /// <summary>
    /// Represents a list of named elements.
    /// </summary>
    /// <typeparam name="T">Named element type</typeparam>
    public class NamedList<T> : List<T>, INamedList<T> where T : INamed
    {
        /// <summary>
        /// Gets an element in the list that matches the specified name.
        /// </summary>
        /// <param name="name">Name of the element to find.</param>
        /// <returns>The element that corresponds to the name, or null if not found.</returns>
        public T this[String name]
        {
            get 
            {
                if(String.IsNullOrEmpty(name))
                    return default(T);

                for(int i = 0; i < Count; i++)
                {
                    T obj = this[i];

                    if(obj != null && obj.Name.Equals(name))
                        return obj;
                }

                return default(T);
            }
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="NamedList{T}"/> class.
        /// </summary>
        public NamedList() : base() { }

        /// <summary>
        /// Constructs a new instance of the <see cref="NamedList{T}"/> class.
        /// </summary>
        /// <param name="initialCapacity">The initial capacity of the list.</param>
        public NamedList(int initialCapacity) : base(initialCapacity) { }

        /// <summary>
        /// Constructs a new instance of the <see cref="NamedList{T}"/> class. This copies the elements from the enumerable, rather than
        /// wrappering a collection.
        /// </summary>
        /// <param name="elements">Elements to copy</param>
        /// <exception cref="System.ArgumentNullException">Thrown if the enumerable is null.</exception>
        public NamedList(IEnumerable<T> elements) : base(elements) { }

        /// <summary>
        /// Constructs a new instance of the <see cref="NamedList{T}"/> class. This copies the elements from the array, rather than
        /// wrappering a collection.
        /// </summary>
        /// <param name="elements">Elements to copy</param>
        /// <exception cref="System.ArgumentNullException">Thrown if the array is null.</exception>
        public NamedList(params T[] elements) : base(elements) { }

        /// <summary>
        /// Determines the index of an element in the list that matches the specified name.
        /// </summary>
        /// <param name="name">Name of the element.</param>
        /// <returns>Zero-based index indicating the position of the element in the list. A value of -1 denotes it was not found.</returns>
        public int IndexOf(String name)
        {
            if(String.IsNullOrEmpty(name))
                return -1;

            for(int i = 0; i < Count; i++)
            {
                T obj = this[i];

                if(obj != null && obj.Name.Equals(name))
                    return i;
            }

            return -1;
        }

        /// <summary>
        /// Determines if the list contains an element that matches the specified name.
        /// </summary>
        /// <param name="name">Name of the element.</param>
        /// <returns>True if an element with the name exists in the list, otherwise false.</returns>
        public bool Contains(String name)
        {
            return IndexOf(name) != -1;
        }
    }
}
