﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// A collection of extended properties for a <see cref="IExtendedProperties"/> object.
  /// </summary>
  public sealed class ExtendedPropertiesCollection : Dictionary<int, object>
  {
    /// <summary>
    /// Constructs a new instance of the <see cref="ExtendedPropertiesCollection"/> class.
    /// </summary>
    public ExtendedPropertiesCollection() { }

    /// <summary>
    /// Constructs a new instance of the <see cref="ExtendedPropertiesCollection"/> class.
    /// </summary>
    /// <param name="initialCapacity">The initial capacity.</param>
    public ExtendedPropertiesCollection(int initialCapacity) : base(initialCapacity) { }

    /// <summary>
    /// Tries to get a value in the dictionary associated with the key. This safely attempts to cast the value to the specified type,
    /// and if that fails then the method returns false.
    /// </summary>
    /// <typeparam name="T">Type of value to cast to.</typeparam>
    /// <param name="key">The key.</param>
    /// <param name="value">The return value.</param>
    /// <returns>True if the value was contained in the collection and was successfully casted to the type, false if it wasn't found or could not be casted.</returns>
    public bool TryGetValueAs<T>(int key, [NotNullWhen(true)] out T? value)
    {
      object? obj;
      if (TryGetValue(key, out obj))
      {
        if (obj is null)
        {
          value = default(T);
          return value is not null;
        }

        Type type = typeof(T);
        if (type.IsAssignableFrom(obj.GetType()))
        {
          value = (T) obj;
          return true;
        }
      }

      value = default(T);
      return false;
    }

    /// <summary>
    /// Counts the number of objects contained in the collection that are of the specified type.
    /// </summary>
    /// <typeparam name="T">Type of value to count.</typeparam>
    /// <returns>The number of objects in the collection that are of the type.</returns>
    public int GetCountOf<T>()
    {
      int count = 0;
      foreach (KeyValuePair<int, object> kv in this)
      {
        if (kv.Value is T)
          count++;
      }

      return count;
    }
  }
}
