﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace Tesla
{
    /// <summary>
    /// Represents a readonly collection of named objects that allows for fast look up by name, but acts like a list otherwise.
    /// </summary>
    /// <typeparam name="T">Named element type</typeparam>
    [DebuggerTypeProxy(typeof(DebugView_ReadOnlyCollection<>)), DebuggerDisplay("Count = {Count}")]
    public class ReadOnlyNamedListFast<T> : INamedList<T> where T : INamed
    {
        /// <summary>
        /// The underlying list.
        /// </summary>
        protected IList<T> m_list;
        private Dictionary<String, int> m_fastLookUpIndices;

        /// <summary>
        /// Gets the element at the specified index in the read-only list.
        /// </summary>
        /// <param name="index">Index of element</param>
        /// <returns>The element</returns>
        /// <exception cref="ArgumentOutOfRangeException">Thrown if the index is out of range.</exception>
        public T this[int index]
        {
            get
            {
                if(index < 0 || index >= m_list.Count)
                    throw new ArgumentOutOfRangeException("index", StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));

                return m_list[index];
            }
        }

        /// <summary>
        /// Gets an element in the list that matches the specified name.
        /// </summary>
        /// <param name="name">Name of the element to find.</param>
        /// <returns>The element that corresponds to the name, or null if not found.</returns>
        public T this[String name]
        {
            get
            {
                if(String.IsNullOrEmpty(name))
                    return default(T);

                int index;
                if(m_fastLookUpIndices.TryGetValue(name, out index))
                    return m_list[index];

                return default(T);
            }
        }

        /// <summary>
        /// Gets the number of elements in the collection.
        /// </summary>
        public int Count
        {
            get
            {
                return m_list.Count;
            }
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="ReadOnlyNamedListFast{T}"/> class.
        /// </summary>
        /// <param name="elements">Elements to copy</param>
        /// <exception cref="System.ArgumentNullException">Thrown if the enumerable is null.</exception>
        public ReadOnlyNamedListFast(IEnumerable<T> elements)
        {
            if(elements == null)
                throw new ArgumentNullException("elements");

            m_list = new List<T>(elements);
            m_fastLookUpIndices = new Dictionary<String, int>();

            PopulateFastLookupTable();
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="ReadOnlyNamedListFast{T}"/> class.
        /// </summary>
        /// <param name="elements">Elements to copy</param>
        /// <exception cref="System.ArgumentNullException">Thrown if the array is null.</exception>
        public ReadOnlyNamedListFast(params T[] elements)
        {
            if(elements == null)
                throw new ArgumentNullException("elements");

            m_list = new List<T>(elements);
            m_fastLookUpIndices = new Dictionary<String, int>();

            PopulateFastLookupTable();
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="ReadOnlyNamedListFast{T}"/> class.
        /// </summary>
        protected ReadOnlyNamedListFast() : this(0) { }

        /// <summary>
        /// Constructs a new instance of the <see cref="ReadOnlyNamedListFast{T}"/> class.
        /// </summary>
        protected ReadOnlyNamedListFast(int initialCapacity)
        {
            m_list = new List<T>(initialCapacity);
            m_fastLookUpIndices = new Dictionary<String, int>();
        }

        /// <summary>
        /// Queries the collection to see if the name of the effect part is present in the collection.
        /// </summary>
        /// <param name="name">Name of effect part to query.</param>
        /// <returns>True if the object is present in the collection, false otherwise.</returns>
        public bool Contains(String name)
        {
            if(String.IsNullOrEmpty(name))
                return false;

            return m_fastLookUpIndices.ContainsKey(name);
        }

        /// <summary>
        /// Tries to get the associated effect part with the name.
        /// </summary>
        /// <param name="name">Name of effect part to get.</param>
        /// <param name="value">Effect part, if it exists.</param>
        /// <returns>True if the value was found, false otherwise.</returns>
        public bool TryGetValue(String name, out T value)
        {
            value = default(T);

            if(String.IsNullOrEmpty(name))
                return false;

            int index;
            if(m_fastLookUpIndices.TryGetValue(name, out index))
            {
                value = m_list[index];
                return true;
            }

            return false;
        }

        /// <summary>
        /// Determines the index of an element in the list that matches the specified name.
        /// </summary>
        /// <param name="name">Name of the element.</param>
        /// <returns>Zero-based index indicating the position of the element in the list. A value of -1 denotes it was not found.</returns>
        public int IndexOf(String name)
        {
            if(String.IsNullOrEmpty(name))
                return -1;

            int index;
            if(m_fastLookUpIndices.TryGetValue(name, out index))
                return index;

            return -1;
        }

        /// <summary>
        /// Causes the fast look up cache to be updated.
        /// </summary>
        protected void PopulateFastLookupTable()
        {
            m_fastLookUpIndices.Clear();

            for(int i = 0; i < m_list.Count; i++)
            {
                T obj = m_list[i];
                if(obj != null && !m_fastLookUpIndices.ContainsKey(obj.Name))
                {
                    m_fastLookUpIndices.Add(obj.Name, i);
                }
            }
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.</returns>
        public IEnumerator<T> GetEnumerator()
        {
            return m_list.GetEnumerator();
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return m_list.GetEnumerator();
        }
    }
}
