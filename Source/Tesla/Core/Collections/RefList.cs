/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading;

namespace Tesla
{
  /// <summary>
  /// Implements a list that is optimized for value types by using reference semantics to avoid unnecessary copying.
  /// </summary>
  /// <typeparam name="T">Type of element in the list</typeparam>
  [DebuggerTypeProxy(typeof(DebugView_ReadOnlyRefList<>)), DebuggerDisplay("Count = {Count}")]
  public class RefList<T> : IList, IRefList<T>, IReadOnlyRefList<T> where T : struct
  {
    private const int MaxArrayLength = 0X7FEFFFFF;
    private const int DefaultCapacity = 4;
    private static readonly T[] s_emptyArray = new T[0];

    private Object m_syncRoot;
    private T[] m_items;
    private int m_size;
    private int m_version;

    /// <summary>
    /// Gets a reference to the element at the given index.
    /// </summary>
    /// <param name="index">Zero-based index of the element to get.</param>
    /// <returns>A reference to the element at the specified index.</returns>
    public ref T this[int index]
    {
      get
      {
        if ((uint) index >= (uint) m_size)
          throw new ArgumentOutOfRangeException("index", StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));

        return ref m_items[index];
      }
    }

    /// <summary>
    /// Gets a readonly reference to the element at the given index.
    /// </summary>
    /// <param name="index">Zero-based index of the element to get.</param>
    /// <returns>A readonly reference to the element at the specified index.</returns>
    ref readonly T IReadOnlyRefList<T>.this[int index]
    {
      get
      {
        if ((uint) index >= (uint) m_size)
          throw new ArgumentOutOfRangeException("index", StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));

        return ref m_items[index];
      }
    }

    /// <summary>
    /// Gets a span representing the underlying data. Do not add/remove to the list while this is active.
    /// </summary>
    public Span<T> Span
    {
      get
      {
        return new Span<T>(m_items, 0, m_size);
      }
    }

    /// <summary>
    /// Gets a span representing the underlying data. Do not add/remove to the list while this is active.
    /// </summary>
    ReadOnlySpan<T> IReadOnlyRefList<T>.Span
    {
      get
      {
        return new ReadOnlySpan<T>(m_items, 0, m_size);
      }
    }

    /// <summary>
    /// Gets or sets the element at the specified index.
    /// </summary>
    /// <param name="index">Zero-based index of the element to get.</param>
    /// <returns>The element at the specified index.</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if <paramref name="index"/> is less than zero -or- <paramref name="index"/> is equal to or greater than <see cref="Count"/>.</exception>
    T IList<T>.this[int index]
    {
      get
      {
        if ((uint) index >= (uint) m_size)
          throw new ArgumentOutOfRangeException("index", StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));

        return m_items[index];
      }
      set
      {
        if ((uint) index >= (uint) m_size)
          throw new ArgumentOutOfRangeException("index", StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));

        m_items[index] = value;
      }
    }

    /// <summary>
    /// Gets the element at the specified index.
    /// </summary>
    /// <param name="index">Zero-based index of the element to get.</param>
    /// <returns>The element at the specified index.</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if <paramref name="index"/> is less than zeor -or- <paramref name="index"/> is equal to or greater than <see cref="Count"/>.</exception>
    T IReadOnlyList<T>.this[int index]
    {
      get
      {
        if ((uint) index >= (uint) m_size)
          throw new ArgumentOutOfRangeException("index", StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));

        return m_items[index];
      }
    }

    /// <summary>
    /// Gets the element at the specified index.
    /// </summary>
    /// <param name="index">Zero-based index of the element to get.</param>
    /// <returns>The element at the specified index.</returns>
    /// <exception cref="ArgumentNullException">Thrown if setting a null value.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if <paramref name="index"/> is less than zeor -or- <paramref name="index"/> is equal to or greater than <see cref="Count"/>.</exception>
    object IList.this[int index]
    {
      get
      {
        return this[index];
      }
      set
      {
        if (value == null)
          throw new ArgumentNullException("value");

        this[index] = (T) value;
      }
    }

    /// <summary>
    /// Gets or sets the total number of elements the <see cref="RefList{T}" /> can hold without resizing its internal array.
    /// </summary>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the capacity is less than <see cref="Count"/>.</exception>
    /// <exception cref="OutOfMemoryException">Thrown if there is not enough memory available on the system.</exception>
    public int Capacity
    {
      get
      {
        return m_items.Length;
      }
      set
      {
        //Cannot set the new capacity to less than what we have. Works like List<T>
        if (value < m_size)
          throw new ArgumentOutOfRangeException("capacity", StringLocalizer.Instance.GetLocalizedString("ArgumentOutOfRange_SmallCapacity"));

        //Note: not changing version because the contents haven't changed, just our capacity
        if (value != m_items.Length)
        {
          if (value > 0)
          {
            T[] newItems = new T[value];

            //Copy elements to larger array only if necessary
            if (m_size > 0)
              Array.Copy(m_items, 0, newItems, 0, m_size);

            m_items = newItems;
          }
          else
          {
            //Set back to empty array
            m_items = s_emptyArray;
          }
        }
      }
    }

    /// <summary>
    /// Gets whether the collection is fixed size or not (e.g. an array with set length vs expandable list).
    /// </summary>
    public bool IsFixedSize
    {
      get
      {
        return false;
      }
    }

    /// <summary>
    /// Gets the number of elements contained in the <see cref="RefList{T}" />.
    /// </summary>
    public int Count
    {
      get
      {
        return m_size;
      }
    }

    /// <summary>
    /// Gets a value indicating whether the <see cref="RefList{T}" /> is read-only.
    /// </summary>
    public bool IsReadOnly
    {
      get
      {
        return false;
      }
    }

    /// <summary>
    /// Gets whether access to the collection is synchronized (thread safe).
    /// </summary>
    bool ICollection.IsSynchronized
    {
      get
      {
        return false;
      }
    }

    /// <summary>
    /// Gets an object that can be used to synchronize access to the collection.
    /// </summary>
    object ICollection.SyncRoot
    {
      get
      {
        if (m_syncRoot == null)
          Interlocked.CompareExchange<object>(ref m_syncRoot, new object(), null);

        return m_syncRoot;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="RefList{T}" /> class.
    /// </summary>
    public RefList()
    {
      m_items = s_emptyArray;
      m_size = 0;
      m_version = 0;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="RefList{T}" /> class.Ind
    /// </summary>
    /// <param name="capacity">Initial capacity of the list.</param>
    public RefList(int capacity)
    {
      if (capacity < 0)
        throw new ArgumentOutOfRangeException("capacity", StringLocalizer.Instance.GetLocalizedString("LengthMustBePositive"));

      m_items = (capacity > 0) ? new T[capacity] : s_emptyArray;
      m_size = 0;
      m_version = 0;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="RefList{T}" /> class.
    /// </summary>
    /// <param name="collection">Collection of elements to populate the list with.</param>
    public RefList(IEnumerable<T> collection)
    {
      if (collection == null)
        throw new ArgumentNullException("collection", StringLocalizer.Instance.GetLocalizedString("ArgumentNull"));

      m_items = s_emptyArray;
      m_size = 0;
      m_version = 0;

      int count = 0;

      //Test for better ways to treat the collection, to avoid unnecessary copying
      switch (collection)
      {
        case ICollection<T> c:
          count = c.Count;
          if (count > 0)
          {
            m_items = new T[count];
            c.CopyTo(m_items, 0);
            m_size = count;
          }
          break;
        case IReadOnlyRefList<T> l:
          count = l.Count;
          if (count > 0)
          {
            m_items = new T[count];
            l.CopyTo(m_items, 0);
            m_size = count;
          }
          break;
        default:
          //Don't know what else it is or what the count is, fallback to slow iteration, adding one by one. 
          //This will go to default capacity first rather than 1, 2, etc.
          foreach (T item in collection)
            Add(in item);

          break;
      }
    }

    /// <summary>
    /// Adds the item to the list.
    /// </summary>
    /// <param name="item">Item to add.</param>
    public void Add(in T item)
    {
      if (m_size == m_items.Length)
        EnsureCapacity(m_size + 1);

      m_items[m_size++] = item;
      m_version++;
    }

    /// <summary>
    /// Adds the item to the <see cref="RefList{T}" />.
    /// </summary>
    /// <param name="item">Item to add.</param>
    void ICollection<T>.Add(T item)
    {
      Add(in item);
    }

    /// <summary>
    /// Adds the items in the supplied collection to the end of the <see cref="RefList{T}" />.
    /// </summary>
    /// <param name="collection">Collection of elements to add.</param>
    /// <exception cref="ArgumentNullException">Thrown if the collection is null.</exception>
    public void AddRange(IEnumerable<T> collection)
    {
      InsertRange(m_size, collection);
    }

    /// <inheritdoc />
    public void AddRange(ReadOnlySpan<T> span)
    {
      // TODO - Have an InsertRange with a span too
      if (Span.IsEmpty)
        return;

      EnsureCapacity(m_size + span.Length);
      span.CopyTo(m_items.AsSpan(m_size, span.Length));

      m_size += span.Length;
      m_version++;
    }

    /// <inheritdoc />
    public Span<T> AddRange(int count)
    {
      // TODO - Have an insert like this too (or AllocateRange?)
      EnsureCapacity(m_size + count);

      Span<T> span = m_items.AsSpan(m_size, count);

      m_size += count;
      m_version++;

      return span;
    }

    /// <summary>
    /// Inserts the items in the supplied collection at the specified index of the <see cref="RefList{T}" />. Inserting at the end of the list is valid.
    /// </summary>
    /// <param name="index">The zero-based index at which the new elements should be inserted. Cannot be greater than <see cref="Count"/>.</param>
    /// <param name="collection">Collection of elements to add.</param>
    /// <exception cref="ArgumentNullException">Thrown if the collection is null.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the index is less than zero -or- greater than the current size of the list.</exception>
    public void InsertRange(int index, IEnumerable<T> collection)
    {
      if (collection == null)
        throw new ArgumentNullException("collection", StringLocalizer.Instance.GetLocalizedString("ArgumentNull"));

      //Insertions at the end are legal
      if ((uint) index > (uint) m_size)
        throw new ArgumentOutOfRangeException("index", StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));

      //Test for better ways to treat the collection, to avoid unnecessary copying
      ICollection<T> c = collection as ICollection<T>;

      //Note: IReadOnlyList doesn't have CopyTo, but we added it for IReadOnlyRefList, so pick up any *truly* read-only collections
      //like databuffers...
      IReadOnlyRefList<T> r = collection as IReadOnlyRefList<T>;
      if (c != null || r != null)
      {
        int count = (c == null) ? r.Count : c.Count;

        //If the collection is empty, early out. Nothing to do.
        if (count == 0)
          return;

        EnsureCapacity(m_size + count);

        //If inserting somewhere in the middle of the list, push the affected elements out so we don't overwrite them
        if (index < m_size)
          Array.Copy(m_items, index, m_items, index + count, m_size - index);

        //If another reflist, we can avoid some overhead
        RefList<T> anotherRefList = collection as RefList<T>;
        if (anotherRefList != null)
        {
          //Handle if the list is itself, especially if inserting into the middle of the list, since we pushed elements further down the array to make room
          if (this == anotherRefList)
          {
            //Copy first part of items to insert location
            Array.Copy(m_items, 0, m_items, index, index);

            //Copy last part of items back to insert location
            Array.Copy(m_items, index + count, m_items, index * 2, m_size - index);
          }
          else
          {
            //Direct copy the arrays from each list
            Array.Copy(anotherRefList.m_items, 0, m_items, index, count);
          }
        }
        else
        {
          //List<T> allocates a temporary array for security to hide the item array...we're trying to avoid unnecessary copies for RefList<T>...

          if (c != null)
            c.CopyTo(m_items, index);
          else
            r.CopyTo(m_items, index);
        }

        m_size += count;
      }
      else
      {
        //Don't know what else it is or what the count is, fallback to slow iteration, inserting one by one
        foreach (T item in collection)
          Insert(index++, in item);
      }

      m_version++;
    }

    /// <summary>
    /// Removes all items from the <see cref="RefList{T}"/>.
    /// </summary>
    public void Clear()
    {
      if (m_size == 0)
        return;

      Array.Clear(m_items, 0, m_size);
      m_size = 0;
      m_version++;
    }

    /// <summary>
    /// Determines whether an element is in the <see cref="RefList{T}"/>.
    /// </summary>
    /// <param name="item">The object to locate in the <see cref="RefList{T}"/>.</param>
    /// <returns>True if the <paramref name="item"/> is found, false if otherwise.</returns>
    public bool Contains(in T item)
    {
      RefEqualityComparer<T> comparer = RefEqualityComparer<T>.Default;
      for (int i = 0; i < m_size; i++)
      {
        if (comparer.Equals(in m_items[i], in item))
          return true;
      }

      return false;
    }

    /// <summary>
    /// Determines whether an element is in the <see cref="RefList{T}"/>.
    /// </summary>
    /// <param name="item">The object to locate in the <see cref="RefList{T}"/>.</param>
    /// <returns>True if the <paramref name="item"/> is found, false if otherwise.</returns>
    bool ICollection<T>.Contains(T item)
    {
      return Contains(in item);
    }

    /// <summary>
    /// Copies the contents of the entire <see cref="RefList{T}"/> into an array, starting at the beginning of the target array.
    /// </summary>
    /// <param name="array">Destination array to hold the elements.</param>
    /// <exception cref="ArgumentNullException">Thrown if the array is null.</exception>
    /// <exception cref="ArgumentException">Thrown if the number of elements to copy is greater than the destination array can contain.</exception>
    public void CopyTo(T[] array)
    {
      CopyTo(array, 0);
    }

    /// <summary>
    /// Copies the contents of the entire <see cref="RefList{T}"/> into an array, starting at the specified index of the target array.
    /// </summary>
    /// <param name="array">Destination array to hold the elements.</param>
    /// <param name="arrayIndex">Zero-based index in <paramref name="array" /> at which copying begins.</param>
    /// <exception cref="ArgumentNullException">Thrown if the <paramref name="array"/> is null.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the <paramref name="arrayIndex"/> is less than zero.</exception>
    /// <exception cref="ArgumentException">Thrown if the number of elements to copy is greater than the destination array can contain.</exception>
    public void CopyTo(T[] array, int arrayIndex)
    {
      CopyTo(0, array, arrayIndex, m_size);
    }

    /// <summary>
    /// Copies a range of elements from the <see cref="RefList{T}"/>> into an array, starting at the specified index of the target array.
    /// </summary>
    /// <param name="index">Zero-based index in the source <see cref="RefList{T}"/> at which copying begins.</param>
    /// <param name="array">Destination array to hold the elements.</param>
    /// <param name="arrayIndex">Zero-based index in <paramref name="array"/> at which copying begins.</param>
    /// <param name="count">Number of elements to copy.</param>
    /// <exception cref="ArgumentNullException">Thrown if the <paramref name="array"/> is null.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the <paramref name="index"/> is less than zero, -or- <paramref name="arrayIndex"/> is less than zero, -or- <paramref name="count"/> is less than 0.</exception>
    /// <exception cref="ArgumentException">Thrown if the number of elements to copy is greater than the destination array can contain.</exception>
    public void CopyTo(int index, T[] array, int arrayIndex, int count)
    {
      if ((m_size - index) < count)
        throw new ArgumentException(StringLocalizer.Instance.GetLocalizedString("IndexAndCountOutOfRange"));

      Array.Copy(m_items, index, array, arrayIndex, count);
    }

    /// <summary>
    /// Returns the index of the first occurance of a particular item, if it is in the <see cref="RefList{T}"/>. If not, a value of -1 is returned.
    /// </summary>
    /// <param name="item">Item to search for.</param>
    /// <returns>The index of the first occurance of the item, or -1 if it is not in the <see cref="RefList{T}"/>.</returns>
    public int IndexOf(in T item)
    {
      return RefArray.IndexOf<T>(m_items, in item, 0, m_size);
    }

    /// <summary>
    /// Returns the index of the first occurance of a particular item, if it is in the <see cref="RefList{T}"/>. If not, a value of -1 is returned.
    /// </summary>
    /// <param name="item">Item to search for.</param>
    /// <returns>The index of the first occurance of the item, or -1 if it is not in the <see cref="RefList{T}"/>.</returns>
    int IList<T>.IndexOf(T item)
    {
      return RefArray.IndexOf<T>(m_items, in item, 0, m_size);
    }

    /// <summary>
    /// Returns the index of the first occurance of a particular item, if it is in the specified range between the start index and the last element. If not, a value of -1 is returned.
    /// </summary>
    /// <param name="item">Item to search for.</param>
    /// <param name="index">Zero-based starting index of the search. A value of zero is valid for an empty list.</param>
    /// <returns>The index of the first occurance of the item, or -1 if it is not in the range.</returns>
    public int IndexOf(in T item, int index)
    {
      if (index > m_size)
        throw new ArgumentOutOfRangeException("index", StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));

      return RefArray.IndexOf<T>(m_items, in item, index, m_size - index);
    }

    /// <summary>
    /// Returns the index of the first occurance of a particular item, if it is in the specified range. If not, a value of -1 is returned.
    /// </summary>
    /// <param name="item">Item to search for.</param>
    /// <param name="index">Zero-based starting index of the search. A value of zero is valid for an empty list.</param>
    /// <param name="count">Number of elements to search.</param>
    /// <returns>The index of the first occurance of the item, or -1 if it is not in the range.</returns>
    public int IndexOf(in T item, int index, int count)
    {
      if (index > m_size)
        throw new ArgumentOutOfRangeException("index", StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));

      if (count < 0 || index > (m_size - count))
        throw new ArgumentOutOfRangeException("count", StringLocalizer.Instance.GetLocalizedString("IndexAndCountOutOfRange"));

      return RefArray.IndexOf<T>(m_items, in item, index, count);
    }

    /// <summary>
    /// Lasts the index of the last occurance of a particular item, if it is in the <see cref="RefList{T}"/>. If not, a value of -1 is returned.
    /// </summary>
    /// <param name="item">Item to search for.</param>
    /// <returns>The index of the last occurance of the item, or -1 if it is not in the <see cref="RefList{T}"/>.</returns>
    public int LastIndexOf(in T item)
    {
      if (m_size == 0)
        return -1;

      return LastIndexOf(in item, m_size - 1, m_size);
    }

    /// <summary>
    /// Returns the index of the last occurance of a particular item, if it is in the specified range between the start index and the last element. If not, a value of -1 is returned.
    /// </summary>
    /// <param name="item">Item to search for.</param>
    /// <param name="index">Zero-based starting index of the search. A value of zero is valid for an empty list.</param>
    /// <returns>The index of the last occurance of the item, or -1 if it is not in the range.</returns>
    public int LastIndexOf(in T item, int index)
    {
      if (index >= m_size)
        throw new ArgumentOutOfRangeException("index", StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));

      return LastIndexOf(in item, index, index + 1);
    }

    /// <summary>
    /// Returns the index of the last occurance of a particular item, if it is in the specified range. If not, a value of -1 is returned.
    /// </summary>
    /// <param name="item">Item to search for.</param>
    /// <param name="index">Zero-based starting index of the search. A value of zero is valid for an empty list.</param>
    /// <param name="count">Number of elements to search.</param>
    /// <returns>The index of the last occurance of the item, or -1 if it is not in the range.</returns>
    public int LastIndexOf(in T item, int index, int count)
    {
      if (m_size != 0 && index < 0)
        throw new ArgumentOutOfRangeException("index", StringLocalizer.Instance.GetLocalizedString("IndexAndCountOutOfRange"));

      if (m_size != 0 && m_size < 0)
        throw new ArgumentOutOfRangeException("count", StringLocalizer.Instance.GetLocalizedString("IndexAndCountOutOfRange"));

      //Special case for empty list
      if (m_size == 0)
        return -1;

      if (index >= m_size)
        throw new ArgumentOutOfRangeException("index", StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));

      if (count > (index + 1))
        throw new ArgumentOutOfRangeException("count", StringLocalizer.Instance.GetLocalizedString("IndexAndCountOutOfRange"));

      return RefArray.LastIndexOf<T>(m_items, in item, index, count);
    }

    /// <summary>
    /// Inserts the value into the list at the given index. The index must be non-negative and less than or equal
    /// to the number of elements in the list. If the index equals the number of items in the list, then the value is appended to the end
    /// of the list.
    /// </summary>
    /// <param name="index">Index at which to insert.</param>
    /// <param name="item">Item to insert.</param>
    public void Insert(int index, in T item)
    {
      //Insertions at the end are legal
      if ((uint) index > (uint) m_size)
        throw new ArgumentOutOfRangeException("index", StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));

      if (m_size == m_items.Length)
        EnsureCapacity(m_size + 1);

      //Push elements further down the array to allow for space
      if (index < m_size)
        Array.Copy(m_items, index, m_items, index + 1, m_size - index);

      m_items[index] = item;
      m_size++;
      m_version++;
    }

    /// <summary>
    /// Inserts the value into the list at the given index. The index must be non-negative and less than or equal
    /// to the number of elements in the list. If the index equals the number of items in the list, then the value is appended to the end
    /// of the list.
    /// </summary>
    /// <param name="index">Index at which to insert.</param>
    /// <param name="item">Item to insert.</param>
    void IList<T>.Insert(int index, T item)
    {
      Insert(index, in item);
    }

    /// <summary>
    /// Removes the item if it is contained in the <see cref="RefList{T}" />.
    /// </summary>
    /// <param name="item">Item to be removed.</param>
    /// <returns>True if the item was removed, false otherwise.</returns>
    public bool Remove(in T item)
    {
      int index = IndexOf(in item);
      if (index < 0)
        return false;

      RemoveAt(index);
      return true;
    }

    /// <summary>
    /// Removes the item if it is contained in the <see cref="RefList{T}" />.
    /// </summary>
    /// <param name="item">Item to be removed.</param>
    /// <returns>True if the item was removed, false otherwise.</returns>
    bool ICollection<T>.Remove(T item)
    {
      return Remove(in item);
    }

    /// <summary>
    /// Removes the item if it is contained in the <see cref="RefList{T}" />.
    /// </summary>
    /// <param name="index">Zero-based index of the item to remove.</param>
    /// <returns>True if the item was removed, false otherwise.</returns>
    public void RemoveAt(int index)
    {
      if ((uint) index >= (uint) m_size)
        throw new ArgumentOutOfRangeException("index", StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));

      m_size--;

      //If inserting somewhere in the middle, pulls elements closer
      if (index < m_size)
        Array.Copy(m_items, index + 1, m_items, index, m_size - index);

      m_items[m_size] = default(T);
      m_version++;
    }

    /// <summary>
    /// Removes all the elements that match the conditions of the predicate.
    /// </summary>
    /// <param name="match">The delegate that defines the conditions to remove an element.</param>
    /// <returns>Number of elements removed from the <see cref="RefList{T}"/>.</returns>
    /// <exception cref="ArgumentNullException">Thrown if <paramref name="match"/> is null.</exception>
    public int RemoveAll(RefPredicate<T> match)
    {
      if (match == null)
        throw new ArgumentNullException("match");

      //Find the first item which needs to be removed
      int freeIndex = 0;
      while (freeIndex < m_size && !match(in m_items[freeIndex]))
        freeIndex++;

      int current = freeIndex + 1;

      while (current < m_size)
      {
        //Find the first item which needs to be kept.
        while (current < m_size && match(m_items[current]))
          current++;

        //Copy item to the free slot
        if (current < m_size)
          m_items[freeIndex++] = m_items[current++];
      }

      Array.Clear(m_items, freeIndex, m_size - freeIndex);
      int numRemoved = m_size - freeIndex;
      m_size = freeIndex;
      m_version++;

      return numRemoved;
    }

    /// <summary>
    /// Removes a range of items from the <see cref="RefList{T}" />.
    /// </summary>
    /// <param name="index">Zero-based index at which to start removing items.</param>
    /// <param name="count">Number of items to remove.</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if <paramref name="index"/> is less than zero -or- <paramref name="count"/> is less than zero.</exception>
    /// <exception cref="ArgumentException">Thrown if <paramref name="index"/> and <paramref name="count"/> do not make a valid range of elements.</exception>
    public void RemoveRange(int index, int count)
    {
      if (count == 0)
        return;

      if (index < 0)
        throw new ArgumentOutOfRangeException("index", StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));

      if (count < 0)
        throw new ArgumentOutOfRangeException("count", StringLocalizer.Instance.GetLocalizedString("LengthMustBePositive"));

      if (m_size - index < count)
        throw new ArgumentException(StringLocalizer.Instance.GetLocalizedString("IndexAndCountOutOfRange"));

      int i = m_size;
      m_size -= count;

      //Pull the elements past the end of the removed range closer
      if (index < m_size)
        Array.Copy(m_items, index + count, m_items, index, m_size - index);

      Array.Clear(m_items, m_size, count);
      m_version++;
    }

    /// <summary>
    /// Reverses the order of the elements in the entire <see cref="RefList{T}" />.
    /// </summary>
    public void Reverse()
    {
      Reverse(0, m_size);
    }

    /// <summary>
    /// Reverses the order of the elements in the specified range.
    /// </summary>
    /// <param name="index">Zero-based starting index of the range to reverse.</param>
    /// <param name="count">Number of elements in the range to reverse.</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if <paramref name="index"/> is less than zero -or- <paramref name="count"/> is less than zero.</exception>
    /// <exception cref="ArgumentException">Thrown if <paramref name="index"/> and <paramref name="count"/> do not make a valid range of elements.</exception>
    public void Reverse(int index, int count)
    {
      if (count == 0)
        return;

      if (index < 0)
        throw new ArgumentOutOfRangeException("index", StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));

      if (count < 0)
        throw new ArgumentOutOfRangeException("count", StringLocalizer.Instance.GetLocalizedString("LengthMustBePositive"));

      if (m_size - index < count)
        throw new ArgumentException(StringLocalizer.Instance.GetLocalizedString("IndexAndCountOutOfRange"));

      Array.Reverse(m_items, index, count);
      m_version++;
    }

    /// <summary>
    /// Copies the elements of the <see cref="RefList{T}" /> to a new array.
    /// </summary>
    /// <returns>An array containing the elements.</returns>
    public T[] ToArray()
    {
      T[] dest = new T[m_size];
      Array.Copy(m_items, 0, dest, 0, m_size);

      return dest;
    }

    /// <summary>
    /// Sets the capacity to the actual number of elements in the <see cref="RefList{T}" />, if that number is less than
    /// a threshold value.
    /// </summary>
    public void TrimExcess()
    {
      int threshold = (int) (((double) m_items.Length) * 0.9);
      if (m_size < threshold)
        Capacity = m_size;
    }

    /// <summary>
    /// Creates a copy of a range of elements in the <see cref="RefList{T}" />.
    /// </summary>
    /// <param name="index">Zero-based starting index of the range to copy.</param>
    /// <param name="count">Number of elements in the range to copy</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if <paramref name="index"/> is less than zero -or- <paramref name="count"/> is less than zero.</exception>
    /// <exception cref="ArgumentException">Thrown if <paramref name="index"/> and <paramref name="count"/> do not make a valid range of elements.</exception>
    /// <returns></returns>
    public RefList<T> GetRange(int index, int count)
    {
      if (count == 0)
        return new RefList<T>();

      if (index < 0)
        throw new ArgumentOutOfRangeException("index", StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));

      if (count < 0)
        throw new ArgumentOutOfRangeException("count", StringLocalizer.Instance.GetLocalizedString("LengthMustBePositive"));

      if (m_size - index < count)
        throw new ArgumentException(StringLocalizer.Instance.GetLocalizedString("IndexAndCountOutOfRange"));

      RefList<T> list = new RefList<T>(count);
      Array.Copy(m_items, index, list.m_items, 0, count);
      list.m_size = count;

      return list;
    }

    /// <summary>
    /// Converts all elements in the current <see cref="RefList{T}"/> to another type, and returns a list
    /// of those converted elements.
    /// </summary>
    /// <typeparam name="TOutput">The type to convert elements into.</typeparam>
    /// <param name="converter">A <see cref="RefConverter{TInput, TOutput}"/> delegate that converts each element from one type to another.</param>
    /// <returns>A <see cref="RefList{T}"/> containing the converted elements.</returns>
    public RefList<TOutput> ConvertAll<TOutput>(RefConverter<T, TOutput> converter) where TOutput : struct
    {
      if (converter == null)
        throw new ArgumentNullException("converter");

      RefList<TOutput> list = new RefList<TOutput>(m_size);
      for (int i = 0; i < m_size; i++)
        converter(in m_items[i], out list.m_items[i]);

      list.m_size = m_size;

      return list;
    }

    /// <summary>
    /// Itreates through the <see cref="RefList{T}"/> and performs an action on each element.
    /// </summary>
    /// <param name="action">The action to perform on each element.</param>
    /// <exception cref="ArgumentNullException">Thrown if <paramref name="action"/> is null.</exception>
    public void ForEach(RefAction<T> action)
    {
      if (action == null)
        throw new ArgumentNullException("action");

      int version = m_version;

      for (int i = 0; i < m_size; i++)
      {
        if (version != m_version)
          break;

        action(in m_items[i]);
      }

      if (version != m_version)
        throw new InvalidOperationException(StringLocalizer.Instance.GetLocalizedString("CollectionModifiedDuringEnumeration"));
    }

    /// <summary>
    /// Determines whether every element in the <see cref="RefList{T}"/> matches the conditions defined by the predicate.
    /// </summary>
    /// <param name="match">The delegate that defines the conditions to check against each element.</param>
    /// <returns>True if every element matches the condition (or if there are no elements), false if otherwise.</returns>
    public bool TrueForAll(RefPredicate<T> match)
    {
      if (match == null)
        throw new ArgumentNullException("match");

      for (int i = 0; i < m_size; i++)
      {
        if (!match(in m_items[i]))
          return false;
      }

      return true;
    }

    /// <summary>
    /// Determines whether the <see cref="RefList{T}"/> contains any elements that matches the
    /// conditions defined by the predicate.
    /// </summary>
    /// <param name="match">The delegate that defines the conditions of the elements to search for.</param>
    /// <returns>True if one or more elements match the conditions defined by the predicate, false if otherwise.</returns>
    public bool Exists(RefPredicate<T> match)
    {
      return FindIndex(match) != -1;
    }

    /// <summary>
    /// Retrieves all the elements that match the conditions defined by the specified predicate.
    /// </summary>
    /// <param name="match">The delegate that defines the conditions of the elements to search for.</param>
    /// <returns>A <see cref="RefList{T}"/> containing all the elements that match the conditions defined by the predicate, if found, otherwise an empty <see cref="RefList{T}"/>.</returns>
    public RefList<T> FindAll(RefPredicate<T> match)
    {
      if (match == null)
        throw new ArgumentNullException("match");

      RefList<T> list = new RefList<T>();
      for (int i = 0; i < m_size; i++)
      {
        ref readonly T item = ref m_items[i];
        if (match(in item))
          list.Add(in item);
      }

      return list;
    }

    /// <summary>
    /// Searches for the first occurrence of an element that matches the conditions defined by the predicate.
    /// </summary>
    /// <param name="match">The delegate that defines the conditions of the elements to search for.</param>
    /// <param name="result">The first element that matches the conditions of the predicate, if found. Otherwise the default value for type <typeparamref name="T"/>.</param>
    /// <returns>True if the first element was found, false if otherwise.</returns>
    /// <exception cref="ArgumentNullException">Thrown if <paramref name="match"/> is null.</exception>
    public bool Find(RefPredicate<T> match, out T result)
    {
      if (match == null)
        throw new ArgumentNullException("match");

      for (int i = 0; i < m_size; i++)
      {
        ref readonly T item = ref m_items[i];

        if (match(in item))
        {
          result = item;
          return true;
        }
      }

      result = default(T);
      return false;
    }

    /// <summary>
    /// Searches for an element that matches the conditions defined by the specified predicate and returns the zero-based
    /// index of the first occurrence within the entire <see cref="RefList{T}"/>.
    /// </summary>
    /// <param name="match"></param>
    /// <returns></returns>
    public int FindIndex(RefPredicate<T> match)
    {
      return FindIndex(0, m_size, match);
    }

    /// <summary>
    /// Searches for an element that matches the conditions defined by the specified predicate and returns
    /// the zero-based index of the first occurrence between the start index and the last element of the <see cref="RefList{T}"/>.
    /// </summary>
    /// <param name="startIndex">Zero-based starting index of the search.</param>
    /// <param name="match">The delegate that defines the conditions of the element to search for.</param>
    /// <returns>The zero-based index of the first occurrence of an element that matches the conditions defined by <paramref name="match"/>, if found, otherwise -1.</returns>
    public int FindIndex(int startIndex, RefPredicate<T> match)
    {
      return FindIndex(startIndex, m_size - startIndex, match);
    }

    /// <summary>
    /// Searches for an element that matches the conditions defined by the specified predicate and returns
    /// the zero-based index of the first occurrence within the specified range of elements.
    /// </summary>
    /// <param name="startIndex">Zero-based starting index of the search.</param>
    /// <param name="count">Number of elements to search.</param>
    /// <param name="match">The delegate that defines the conditions of the element to search for.</param>
    /// <returns>The zero-based index of the first occurrence of an element that matches the conditions defined by <paramref name="match"/>, if found, otherwise -1.</returns>
    public int FindIndex(int startIndex, int count, RefPredicate<T> match)
    {
      if ((uint) startIndex > (uint) m_size)
        throw new ArgumentOutOfRangeException("index", StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));

      if (count < 0 || startIndex > (m_size - count))
        throw new ArgumentOutOfRangeException("count", StringLocalizer.Instance.GetLocalizedString("IndexAndCountOutOfRange"));

      if (match == null)
        throw new ArgumentNullException("match");

      int endIndex = startIndex + count;
      for (int i = startIndex; i < endIndex; i++)
      {
        if (match(in m_items[i]))
          return i;
      }

      return -1;
    }

    /// <summary>
    /// Searches for the last occurrence of an element that matches the conditions defined by the predicate.
    /// </summary>
    /// <param name="match">The delegate that defines the conditions of the elements to search for.</param>
    /// <param name="result">The last element that matches the conditions of the predicate, if found. Otherwise the default value for type <typeparamref name="T"/>.</param>
    /// <returns>True if the last element was found, false if otherwise.</returns>
    /// <exception cref="ArgumentNullException">Thrown if <paramref name="match"/> is null.</exception>
    public bool FindLast(RefPredicate<T> match, out T result)
    {
      if (match == null)
        throw new ArgumentNullException("match");

      for (int i = m_size - 1; i >= 0; i--)
      {
        ref readonly T item = ref m_items[i];

        if (match(in item))
        {
          result = item;
          return true;
        }
      }

      result = default(T);
      return false;
    }

    /// <summary>
    /// Searches for an element that matches the conditions defined by the specified predicate and returns
    /// the zero-based index of the last occurrence within the entire <see cref="RefList{T}"/>.
    /// </summary>
    /// <param name="match">The delegate that defines the conditions of the element to search for.</param>
    /// <returns>Zero-based index of the last occurrence of an element that matches the conditions defined by <paramref name="match"/>, if found, otherwise -1.</returns>
    /// <exception cref="ArgumentNullException">Thrown if <paramref name="match"/> is null.</exception>
    public int FindLastIndex(RefPredicate<T> match)
    {
      return FindLastIndex(m_size - 1, m_size, match);
    }

    /// <summary>
    /// Searches for an element that matches the conditions defined by the specified predicate and returns
    /// the zero-based index of the last occurrence within the range from the starting index to the first element.
    /// </summary>
    /// <param name="startIndex">Zero-based starting index of the backward search.</param>
    /// <param name="match">The delegate that defines the conditions of the element to search for.</param>
    /// <returns>Zero-based index of the last occurrence of an element that matches the conditions defined by <paramref name="match"/>, if found, otherwise -1.</returns>
    /// <exception cref="ArgumentNullException">Thrown if <paramref name="match"/> is null.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if <paramref name="startIndex"/> is outside the range of valid indices.</exception>
    public int FindLastIndex(int startIndex, RefPredicate<T> match)
    {
      return FindLastIndex(startIndex, startIndex + 1, match);
    }

    /// <summary>
    /// Searches for an element that matches the conditions defined by the specified predicate and returns
    /// the zero-based index of the last occurrence within the specified range of elements.
    /// </summary>
    /// <param name="startIndex">Zero-based starting index of the backward search.</param>
    /// <param name="count">The number of elements to search.</param>
    /// <param name="match">The delegate that defines the conditions of the element to search for.</param>
    /// <returns>Zero-based index of the last occurrence of an element that matches the conditions defined by <paramref name="match"/>, if found, otherwise -1.</returns>
    /// <exception cref="ArgumentNullException">Thrown if <paramref name="match"/> is null.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if <paramref name="startIndex"/> is outside the range of valid indices, -or- <paramref name="count"/> is less than zero, -or-
    /// <paramref name="startIndex"/> and <paramref name="count"/> do not specify a valid range.</exception>
    public int FindLastIndex(int startIndex, int count, RefPredicate<T> match)
    {
      if (match == null)
        throw new ArgumentNullException("match");

      if (m_size == 0)
      {
        //Special case for zero length
        if (startIndex != -1)
          throw new ArgumentOutOfRangeException("index", StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));
      }
      else
      {
        //Test if in range
        if ((uint) startIndex >= (uint) m_size)
          throw new ArgumentOutOfRangeException("index", StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));
      }

      //Catches if startIndex == MAXINT, MAXINT - 0 + 1 == -1
      if (count < 0 || ((startIndex - count) + 1) < 0)
        throw new ArgumentOutOfRangeException("count", StringLocalizer.Instance.GetLocalizedString("IndexAndCountOutOfRange"));

      int endIndex = startIndex - count;
      for (int i = startIndex; i > endIndex; i--)
      {
        if (match(in m_items[i]))
          return i;
      }

      return -1;
    }

    /// <summary>
    /// Searches the entire sorted <see cref="RefList{T}"/> for an element that corresponds to the
    /// specified value using the specified comparer, and returns the zero-based index of the element in the list, if it exists.
    /// </summary>
    /// <param name="value">An object to locate in the <see cref="RefList{T}"/>.</param>
    /// <returns>The zero-based index of <paramref name="value"/> in the sorted <see cref="RefList{T}"/>, if it is found, otherwise, a negative
    /// number that is the bitwise complement of the index of the next element that is larger than <paramref name="value"/> or if there is none, the bitwise complement of <see cref="Count"/>.</returns>
    /// <exception cref="InvalidOperationException">Thrown if the type cannot be compared, either a default comparer was not found or it does not implement comparable interfaces.</exception>
    public int BinarySearch(in T value)
    {
      if (RefComparer<T>.Default.IsWrappedComparer)
        return BinarySearch(0, m_size, in value, (IComparer<T>) null);
      else
        return BinarySearch(0, m_size, in value, (IRefComparer<T>) null);
    }

    /// <summary>
    /// Searches the entire sorted <see cref="RefList{T}"/> for an element that corresponds to the
    /// specified value using the specified comparer, and returns the zero-based index of the element in the list, if it exists.
    /// </summary>
    /// <param name="value">An object to locate in the <see cref="RefList{T}"/>.</param>
    /// <param name="comparer">The comparer to use, or null to use the default comparer for the type.</param>
    /// <returns>The zero-based index of <paramref name="value"/> in the sorted <see cref="RefList{T}"/>, if it is found, otherwise, a negative
    /// number that is the bitwise complement of the index of the next element that is larger than <paramref name="value"/> or if there is none, the bitwise complement of <see cref="Count"/>.</returns>
    /// <exception cref="InvalidOperationException">Thrown if the type cannot be compared, either a default comparer was not found or it does not implement comparable interfaces.</exception>
    public int BinarySearch(in T value, IRefComparer<T> comparer)
    {
      return BinarySearch(0, m_size, in value, comparer);
    }

    /// <summary>
    /// Searches the entire sorted <see cref="RefList{T}"/> for an element that corresponds to the
    /// specified value using the specified comparer, and returns the zero-based index of the element in the list, if it exists.
    /// </summary>
    /// <param name="value">An object to locate in the <see cref="RefList{T}"/>.</param>
    /// <param name="comparer">The comparer to use, or null to use the default comparer for the type.</param>
    /// <returns>The zero-based index of <paramref name="value"/> in the sorted <see cref="RefList{T}"/>, if it is found, otherwise, a negative
    /// number that is the bitwise complement of the index of the next element that is larger than <paramref name="value"/> or if there is none, the bitwise complement of <see cref="Count"/>.</returns>
    /// <exception cref="InvalidOperationException">Thrown if the type cannot be compared, either a default comparer was not found or it does not implement comparable interfaces.</exception>
    public int BinarySearch(in T value, IComparer<T> comparer)
    {
      return BinarySearch(0, m_size, in value, comparer);
    }

    /// <summary>
    /// Searches a range of elements in the sorted <see cref="RefList{T}"/> for an element that corresponds to the
    /// specified value using the specified comparer, and returns the zero-based index of the element in the list, if it exists.
    /// </summary>
    /// <param name="index">Zero-based starting index of the range to search.</param>
    /// <param name="count">Number of elements to search.</param>
    /// <param name="value">An object to locate in the <see cref="RefList{T}"/>.</param>
    /// <returns>The zero-based index of <paramref name="value"/> in the sorted <see cref="RefList{T}"/>, if it is found, otherwise, a negative
    /// number that is the bitwise complement of the index of the next element that is larger than <paramref name="value"/> or if there is none, the bitwise complement of the index of the last element plus 1.</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if <paramref name="index"/> is less than zero -or- <paramref name="count"/> is less than zero.</exception>
    /// <exception cref="ArgumentException">Thrown if <paramref name="index"/> and <paramref name="count"/> do not denote a valid range.</exception>
    /// <exception cref="InvalidOperationException">Thrown if the type cannot be compared, either a default comparer was not found or it does not implement comparable interfaces.</exception>
    public int BinarySearch(int index, int count, in T value)
    {
      if (RefComparer<T>.Default.IsWrappedComparer)
        return BinarySearch(index, count, in value, (IComparer<T>) null);
      else
        return BinarySearch(index, count, in value, (IRefComparer<T>) null);
    }

    /// <summary>
    /// Searches a range of elements in the sorted <see cref="RefList{T}"/> for an element that corresponds to the
    /// specified value using the specified comparer, and returns the zero-based index of the element in the list, if it exists.
    /// </summary>
    /// <param name="index">Zero-based starting index of the range to search.</param>
    /// <param name="count">Number of elements to search.</param>
    /// <param name="value">An object to locate in the <see cref="RefList{T}"/>.</param>
    /// <param name="comparer">The comparer to use, or null to use the default comparer for the type.</param>
    /// <returns>The zero-based index of <paramref name="value"/> in the sorted <see cref="RefList{T}"/>, if it is found, otherwise, a negative
    /// number that is the bitwise complement of the index of the next element that is larger than <paramref name="value"/> or if there is none, the bitwise complement of the index of the last element plus 1.</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if <paramref name="index"/> is less than zero -or- <paramref name="count"/> is less than zero.</exception>
    /// <exception cref="ArgumentException">Thrown if <paramref name="index"/> and <paramref name="count"/> do not denote a valid range.</exception>
    /// <exception cref="InvalidOperationException">Thrown if the type cannot be compared, either a default comparer was not found or it does not implement comparable interfaces.</exception>
    public int BinarySearch(int index, int count, in T value, IRefComparer<T> comparer)
    {
      if (index < 0)
        throw new ArgumentOutOfRangeException("index", StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));

      if (count < 0 || (m_size - index) < count)
        throw new ArgumentOutOfRangeException("count", StringLocalizer.Instance.GetLocalizedString("IndexAndCountOutOfRange"));

      return RefArray.BinarySearch<T>(m_items, index, count, in value, comparer);
    }

    /// <summary>
    /// Searches a range of elements in the sorted <see cref="RefList{T}"/> for an element that corresponds to the
    /// specified value using the specified comparer, and returns the zero-based index of the element in the list, if it exists.
    /// </summary>
    /// <param name="index">Zero-based starting index of the range to search.</param>
    /// <param name="count">Number of elements to search.</param>
    /// <param name="value">An object to locate in the <see cref="RefList{T}"/>.</param>
    /// <param name="comparer">The comparer to use, or null to use the default comparer for the type.</param>
    /// <returns>The zero-based index of <paramref name="value"/> in the sorted <see cref="RefList{T}"/>, if it is found, otherwise, a negative
    /// number that is the bitwise complement of the index of the next element that is larger than <paramref name="value"/> or if there is none, the bitwise complement of the index of the last element plus 1.</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if <paramref name="index"/> is less than zero -or- <paramref name="count"/> is less than zero.</exception>
    /// <exception cref="ArgumentException">Thrown if <paramref name="index"/> and <paramref name="count"/> do not denote a valid range.</exception>
    /// <exception cref="InvalidOperationException">Thrown if the type cannot be compared, either a default comparer was not found or it does not implement comparable interfaces.</exception>
    public int BinarySearch(int index, int count, in T value, IComparer<T> comparer)
    {
      if (index < 0)
        throw new ArgumentOutOfRangeException("index", StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));

      if (count < 0 || (m_size - index) < count)
        throw new ArgumentOutOfRangeException("count", StringLocalizer.Instance.GetLocalizedString("IndexAndCountOutOfRange"));

      return Array.BinarySearch<T>(m_items, index, count, value, comparer);
    }

    /// <summary>
    /// Sorts the entire<see cref="RefList{T}"/> using the default comparison for the element type.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the type cannot be compared, either a default comparer was not found or it does not implement comparable interfaces.</exception>
    public void Sort()
    {
      if (RefComparer<T>.Default.IsWrappedComparer)
        Sort(0, m_size, (IComparer<T>) null);
      else
        Sort(0, m_size, (IRefComparer<T>) null);
    }

    /// <summary>
    /// Sorts the entire<see cref="RefList{T}"/> using the default comparison for the element type.
    /// </summary>
    /// <param name="comparer">The comparer to use, or null to use the default comparer for the type.</param>
    /// <exception cref="InvalidOperationException">Thrown if the type cannot be compared, either a default comparer was not found or it does not implement comparable interfaces.</exception>
    public void Sort(IRefComparer<T> comparer)
    {
      Sort(0, m_size, comparer);
    }

    /// <summary>
    /// Sorts the entire<see cref="RefList{T}"/> using the default comparison for the element type.
    /// </summary>
    /// <param name="comparer">The comparer to use, or null to use the default comparer for the type.</param>
    /// <exception cref="InvalidOperationException">Thrown if the type cannot be compared, either a default comparer was not found or it does not implement comparable interfaces.</exception>
    public void Sort(IComparer<T> comparer)
    {
      Sort(0, m_size, comparer);
    }

    /// <summary>
    /// Sorts the entire<see cref="RefList{T}"/> using the default comparison for the element type.
    /// </summary>
    /// <param name="compareFunc">The comparison function to use.</param>
    /// <exception cref="ArgumentNullException">Thrown if <paramref name="compareFunc"/> is null.</exception>
    /// <exception cref="InvalidOperationException">Thrown if the type cannot be compared, either a default comparer was not found or it does not implement comparable interfaces.</exception>
    public void Sort(RefComparison<T> compareFunc)
    {
      if (compareFunc == null)
        throw new ArgumentNullException("compareFunc");

      Sort(0, m_size, RefComparer<T>.Create(compareFunc));
    }

    /// <summary>
    /// Sorts a range of elements in the <see cref="RefList{T}"/> using the default comparison for the element type.
    /// </summary>
    /// <param name="index">Zero-based starting index of the range to sort.</param>
    /// <param name="count">Number of elements to sort.</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if <paramref name="index"/> is less than zero -or- <paramref name="count"/> is less than zero.</exception>
    /// <exception cref="ArgumentException">Thrown if <paramref name="index"/> and <paramref name="count"/> do not denote a valid range.</exception>
    /// <exception cref="InvalidOperationException">Thrown if the type cannot be compared, either a default comparer was not found or it does not implement comparable interfaces.</exception>
    public void Sort(int index, int count)
    {
      if (RefComparer<T>.Default.IsWrappedComparer)
        Sort(index, count, (IComparer<T>) null);
      else
        Sort(index, count, (IRefComparer<T>) null);
    }

    /// <summary>
    /// Sorts a range of elements in the <see cref="RefList{T}"/> using the default comparison for the element type.
    /// </summary>
    /// <param name="index">Zero-based starting index of the range to sort.</param>
    /// <param name="count">Number of elements to sort.</param>
    /// <param name="comparer">The comparer to use, or null to use the default comparer for the type.</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if <paramref name="index"/> is less than zero -or- <paramref name="count"/> is less than zero.</exception>
    /// <exception cref="ArgumentException">Thrown if <paramref name="index"/> and <paramref name="count"/> do not denote a valid range.</exception>
    /// <exception cref="InvalidOperationException">Thrown if the type cannot be compared, either a default comparer was not found or it does not implement comparable interfaces.</exception>
    public void Sort(int index, int count, IRefComparer<T> comparer)
    {
      if (index < 0)
        throw new ArgumentOutOfRangeException("index", StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));

      if (count < 0 || (m_size - index) < count)
        throw new ArgumentOutOfRangeException("count", StringLocalizer.Instance.GetLocalizedString("IndexAndCountOutOfRange"));

      RefArray.Sort<T>(m_items, index, count, comparer);
      m_version++;
    }

    /// <summary>
    /// Sorts a range of elements in the <see cref="RefList{T}"/> using the default comparison for the element type.
    /// </summary>
    /// <param name="index">Zero-based starting index of the range to sort.</param>
    /// <param name="count">Number of elements to sort.</param>
    /// <param name="comparer">The comparer to use, or null to use the default comparer for the type.</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if <paramref name="index"/> is less than zero -or- <paramref name="count"/> is less than zero.</exception>
    /// <exception cref="ArgumentException">Thrown if <paramref name="index"/> and <paramref name="count"/> do not denote a valid range.</exception>
    /// <exception cref="InvalidOperationException">Thrown if the type cannot be compared, either a default comparer was not found or it does not implement comparable interfaces.</exception>
    public void Sort(int index, int count, IComparer<T> comparer)
    {
      if (index < 0)
        throw new ArgumentOutOfRangeException("index", StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));

      if (count < 0 || (m_size - index) < count)
        throw new ArgumentOutOfRangeException("count", StringLocalizer.Instance.GetLocalizedString("IndexAndCountOutOfRange"));

      Array.Sort<T>(m_items, index, count, comparer);
      m_version++;
    }

    /// <summary>
    /// Returns an enumerator that iterates through the collection.
    /// </summary>
    /// <returns>An enumerator that can be used to iterate through the collection.</returns>
    public Enumerator GetEnumerator()
    {
      return new Enumerator(this);
    }

    /// <summary>
    /// Returns an enumerator that iterates through the collection.
    /// </summary>
    /// <returns>An enumerator that can be used to iterate through the collection.</returns>
    IEnumerator<T> IEnumerable<T>.GetEnumerator()
    {
      return new Enumerator(this);
    }

    /// <summary>
    /// Returns an enumerator that iterates through a collection.
    /// </summary>
    /// <returns>An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.</returns>
    IEnumerator IEnumerable.GetEnumerator()
    {
      return new Enumerator(this);
    }

    //Ensures the list has the necessary capacity to support the requested amount of elements at least. If current capacity is less, then the capacity is increased to twice
    //the current capacity or to min, whichever is larger
    private void EnsureCapacity(int min)
    {
      if (m_items.Length < min)
      {
        int newCapacity = (m_items.Length == 0) ? DefaultCapacity : m_items.Length * 2;

        //Allow the list to grow to max possible capacity (~2G elements) before encountering overflow
        if ((uint) newCapacity > MaxArrayLength)
          newCapacity = MaxArrayLength;

        if (newCapacity < min)
          newCapacity = min;

        Capacity = newCapacity;
      }
    }

    #region IList Implementations

    private bool IsCompatibleObject(Object obj)
    {
      if (obj == null || !(obj is T))
        return false;

      return true;
    }

    /// <summary>
    /// Adds an item to the <see cref="RefList{T}"/>.
    /// </summary>
    /// <param name="value">Item to add.</param>
    /// <returns>The position the item was inserted.</returns>
    /// <exception cref="ArgumentNullException">Thrown if the value was null.</exception>
    /// <exception cref="InvalidCastException">Thrown if the value was not the correct type.</exception>
    int IList.Add(object value)
    {
      if (value == null)
        throw new ArgumentNullException("value");

      Add((T) value);

      return Count - 1;
    }

    /// <summary>
    /// Determines if the <see cref="RefList{T}"/> contains a specific value.
    /// </summary>
    /// <param name="value">Item to locate in the list.</param>
    /// <returns>True if the item is found in the list, false otherwise.</returns>
    bool IList.Contains(object value)
    {
      return IsCompatibleObject(value) && Contains((T) value);
    }

    /// <summary>
    /// Determines the index of a specific item in the <see cref="RefList{T}"/>.
    /// </summary>
    /// <param name="value">Item to locate in the list.</param>
    /// <returns>The index of the item if found in the list; otherwise -1.</returns>
    int IList.IndexOf(object value)
    {
      if (IsCompatibleObject(value))
        return IndexOf((T) value);

      return -1;
    }

    /// <summary>
    /// Inserts an item to the list at the specified index.
    /// </summary>
    /// <param name="index">Zero-based index at which the item is to be inserted.</param>
    /// <param name="value">The item to insert into the list.</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the index at which to insert is invalid.</exception>
    /// <exception cref="ArgumentNullException">Thrown if the value to insert is null.</exception>
    /// <exception cref="InvalidCastException">Thrown if the value to insert is not the correct type.</exception>
    void IList.Insert(int index, object value)
    {
      if (value == null)
        throw new ArgumentNullException("value");

      Insert(index, (T) value);
    }

    /// <summary>
    /// Removes the first occurrence of a specific item from the <see cref="RefList{T}"/>.
    /// </summary>
    /// <param name="value">The item to remove from the list.</param>
    void IList.Remove(object value)
    {
      if (IsCompatibleObject(value))
        Remove((T) value);
    }

    /// <summary>
    /// Copies the elements of the <see cref="RefList{T}"/> to an array, starting at a specified index.
    /// </summary>
    /// <param name="array">The one-dimensional array that will contain the copied elements.</param>
    /// <param name="index">The zero-based index in the array at which to start copying.</param>
    /// <exception cref="InvalidOperationException">Thrown if the array is not one-dimensional.</exception>
    void ICollection.CopyTo(Array array, int index)
    {
      if (array != null && array.Rank != 1)
        throw new InvalidOperationException();

      Array.Copy(m_items, 0, array, index, m_size);
    }

    #endregion

    /// <summary>
    /// Enumerates the elements of a <see cref="RefList{T}"/>.
    /// </summary>
    public struct Enumerator : IEnumerator<T>
    {
      private RefList<T> m_list;
      private int m_index;
      private int m_version;
      private T m_current;

      /// <summary>
      /// Gets the current.
      /// </summary>
      public T Current
      {
        get
        {
          return m_current;
        }
      }

      /// <summary>
      /// Gets the current.
      /// </summary>
      object IEnumerator.Current
      {
        get
        {
          return m_current;
        }
      }

      internal Enumerator(RefList<T> list)
      {
        m_list = list;
        m_index = 0;
        m_version = m_list.m_version;
        m_current = default(T);
      }

      /// <summary>
      /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
      /// </summary>
      public void Dispose() { }

      /// <summary>
      /// Advances the enumerator to the next element of the collection.
      /// </summary>
      /// <returns><see langword="true" /> if the enumerator was successfully advanced to the next element; <see langword="false" /> if the enumerator has passed the end of the collection.</returns>
      public bool MoveNext()
      {
        if (m_version == m_list.m_version && ((uint) m_index < (uint) m_list.m_size))
        {
          m_current = m_list.m_items[m_index];
          m_index++;
          return true;
        }

        return MoveNextRare();
      }

      /// <summary>
      /// Sets the enumerator to its initial position, which is before the first element in the collection.
      /// </summary>
      public void Reset()
      {
        if (m_version != m_list.m_version)
          throw new InvalidOperationException(StringLocalizer.Instance.GetLocalizedString("CollectionModifiedDuringEnumeration"));

        m_index = 0;
        m_current = default(T);
      }

      private bool MoveNextRare()
      {
        if (m_version != m_list.m_version)
          throw new InvalidOperationException(StringLocalizer.Instance.GetLocalizedString("CollectionModifiedDuringEnumeration"));

        m_index = m_list.m_size + 1;
        m_current = default(T);
        return false;
      }
    }
  }
}
