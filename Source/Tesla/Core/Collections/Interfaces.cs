/*
* Copyright (c) 2010-2017 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;

namespace Tesla
{
  /// <summary>
  /// Defines a generalized method that a type implements to create a type-specific method for determining equality of instances.
  /// </summary>
  /// <typeparam name="T">Type of object to compare.</typeparam>
  public interface IRefEquatable<T> where T : struct
  {
    /// <summary>
    /// Indicates whether the current object is equal to another object of the same type.
    /// </summary>
    /// <param name="other">An object to compare with this object.</param>
    /// <returns>True if the current instance is equal to the <paramref name="other"/> instance, false if otherwise.</returns>
    bool Equals(in T other);
  }

  /// <summary>
  /// Defines a generalized method that a type implements to create a type-specific method for ordering or sorting instances.
  /// </summary>
  /// <typeparam name="T">Type of object to compare.</typeparam>
  public interface IRefComparable<T> where T : struct
  {
    /// <summary>
    /// Compares the current instance with another instance of the same type and returns an integer that indicates whether the current instance precedes, follows,
    /// or occurs in the same position in the sort order as the other object.
    /// </summary>
    /// <param name="other">An object to compare with this instance.</param>
    /// <returns>A value that indicates the relative order of the objects being compared. If less than zero, this instance precedes <paramref name="other"/>, if zero then
    /// both occupy the same position, if greater than zero this instance follows <paramref name="other"/> in the sort order.</returns>
    int CompareTo(in T other);
  }

  /// <summary>
  /// Defines a comparison method to compare two objects for equality.
  /// </summary>
  /// <typeparam name="T">Type of objects to compare.</typeparam>
  public interface IRefEqualityComparer<T> where T : struct
  {
    /// <summary>
    /// Determines whether the specified objects are equal.
    /// </summary>
    /// <<param name="x">The first object to compare.</param>
    /// <param name="y">The second object to compare.</param>
    /// <returns>True if the objects are equal, false if otherwise.</returns>
    bool Equals(in T x, in T y);

    /// <summary>
    /// Returns a hash code for the specified object.
    /// </summary>
    /// <param name="obj">The object for which a hash code is computed.</param>
    /// <returns>A hash code for the specified object.</returns>
    int GetHashCode(in T obj);
  }

  /// <summary>
  /// Defines a comparison method to compare two objects.
  /// </summary>
  /// <typeparam name="T">Type of objects to compare.</typeparam>
  public interface IRefComparer<T> where T : struct
  {
    /// <summary>
    /// Compares two objects and returns value indicating whether one is less than, equal to, or greater than the other.
    /// </summary>
    /// <param name="x">The first object to compare.</param>
    /// <param name="y">The second object to compare.</param>
    /// <returns>A signed integer that indicates the relative values of <paramref name="x"/> and <paramref name="y"/>. A value less than zero means <paramref name="x"/> is less than <paramref name="y"/>,
    /// a value of zero means they are equal, and a value greater than zer means <paramref name="x"/> is greater than <paramref name="y"/>.</returns>
    int Compare(in T x, in T y);
  }

  /// <summary>
  /// Defines a list that is optimised for value types by using reference semantics to avoid unnecessary copying.
  /// </summary>
  /// <typeparam name="T">Type of element in the list</typeparam>
  public interface IRefList<T> : IList<T> where T : struct
  {
    /// <summary>
    /// Gets a reference to the element at the given index.
    /// </summary>
    new ref T this[int index] { get; }

    /// <summary>
    /// Gets a span representing the underlying data. Do not add/remove to the list while this is active.
    /// </summary>
    Span<T> Span { get; }

    /// <summary>
    /// Gets whether the collection is fixed size or not (e.g. an array with set length vs expandable list).
    /// </summary>
    bool IsFixedSize { get; }

    /// <summary>
    /// Adds the item to the list.
    /// </summary>
    /// <param name="item">Item to add.</param>
    void Add(in T item);

    /// <summary>
    /// Adds the items to the list.
    /// </summary>
    /// <param name="collection">Collection of elements to add.</param>
    void AddRange(IEnumerable<T> collection);

    /// <summary>
    /// Adds the items to the list.
    /// </summary>
    /// <param name="span">Span of elements to add.</param>
    void AddRange(ReadOnlySpan<T> span);

    /// <summary>
    /// Expands the storage of the list by the specified amount and returns a <see cref="Span{T}"/> to the section of the list
    /// to be filled with data. This is a dangerous operation, the data is expected to be filled immediately.
    /// </summary>
    /// <param name="count">Number of elements to add.</param>
    /// <returns>Span over the section of the list that was allocated.</returns>
    Span<T> AddRange(int count);

    /// <summary>
    /// Queries if the item is contained in the list.
    /// </summary>
    /// <param name="item">Item that may be in the list.</param>
    /// <returns>True if the item was found, false otherwise.</returns>
    bool Contains(in T item);

    /// <summary>
    /// Removes the item if it is contained in the list.
    /// </summary>
    /// <param name="item">Item to be removed.</param>
    /// <returns>True if the item was removed, false otherwise.</returns>
    bool Remove(in T item);

    /// <summary>
    /// Returns the index of a particular item, if it is in the list. If not, a value of -1 is returned.
    /// </summary>
    /// <param name="item">Item to search for.</param>
    /// <returns>Returns the index of the item, or -1 if it is not in the list.</returns>
    int IndexOf(in T item);

    /// <summary>
    /// Inserts the value into the list at the given index. The index must be non-negative and less than or equal
    /// to the number of elements in the list. If the index equals the number of items in the list, then the value is appended to the end 
    /// of the list.
    /// </summary>
    /// <param name="index">Index at which to insert.</param>
    /// <param name="item">Item to insert.</param>
    void Insert(int index, in T item);
  }

  /// <summary>
  /// Defines a readonly list that is optimised for value types by using reference semantics to avoid unnecessary copying.
  /// </summary>
  /// <typeparam name="T">Type of element in the list</typeparam>
  public interface IReadOnlyRefList<T> : IReadOnlyList<T> where T : struct
  {
    /// <summary>
    /// Gets a readonly reference to the element at the given index.
    /// </summary>
    new ref readonly T this[int index] { get; }

    /// <summary>
    /// Gets a span representing the underlying data. Do not add/remove to the list while this is active.
    /// </summary>
    ReadOnlySpan<T> Span { get; }

    /// <summary>
    /// Queries if the item is contained in the list.
    /// </summary>
    /// <param name="item">Item that may be in the list.</param>
    /// <returns>True if the item was found, false otherwise.</returns>
    bool Contains(in T item);

    /// <summary>
    /// Returns the index of a particular item, if it is in the list. If not, a value of -1 is returned.
    /// </summary>
    /// <param name="item">Item to search for.</param>
    /// <returns>Returns the index of the item, or -1 if it is not in the list.</returns>
    int IndexOf(in T item);

    /// <summary>
    /// Copies the contents of the entire list into an array, starting at the specified index of the target array.
    /// </summary>
    /// <param name="array">Destination array to hold the elements.</param>
    /// <param name="arrayIndex">Zero-based index in <paramref name="array" /> at which copying begins.</param>
    void CopyTo(T[] array, int arrayIndex);
  }

  /// <summary>
  /// Defines an indexable list of named objects.
  /// </summary>
  /// <typeparam name="T">Type of named object</typeparam>
  public interface INamedList<T> : IReadOnlyList<T> where T : INamed
  {
    /// <summary>
    /// Gets an element in the list that matches the specified name.
    /// </summary>
    /// <param name="name">Name of the element.</param>
    /// <returns>The element, or null if it could not be found.</returns>
    T this[String name] { get; }

    /// <summary>
    /// Determines the index of an element in the list that matches the specified name.
    /// </summary>
    /// <param name="name">Name of the element.</param>
    /// <returns>Zero-based index indicating the position of the element in the list. A value of -1 denotes it was not found.</returns>
    int IndexOf(String name);

    /// <summary>
    /// Determines if the list contains an element that matches the specified name.
    /// </summary>
    /// <param name="name">Name of the element.</param>
    /// <returns>True if an element with the name exists in the list, otherwise false.</returns>
    bool Contains(String name);
  }
}
