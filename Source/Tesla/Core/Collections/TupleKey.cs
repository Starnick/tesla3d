﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Globalization;
using System.Runtime.InteropServices;

namespace Tesla
{
    /// <summary>
    /// A key for two pieces of data that are linked together in some manner.
    /// </summary>
    /// <typeparam name="T1">First piece of data</typeparam>
    /// <typeparam name="T2">Second piece of data</typeparam>
    [Serializable]
    [StructLayout(LayoutKind.Sequential)]
    public struct TupleKey<T1, T2> : IEquatable<TupleKey<T1, T2>>, IComparable<TupleKey<T1, T2>>
    {
        private T1 m_first;
        private T2 m_second;
        private int m_firstHash;
        private int m_secondHash;
        private int m_hash;

        /// <summary>
        /// Gets the first piece of data the key represents.
        /// </summary>
        public T1 First
        {
            get
            {
                return m_first;
            }
        }

        /// <summary>
        /// Gets the second piece of data the key represents.
        /// </summary>
        public T2 Second
        {
            get
            {
                return m_second;
            }
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="TupleKey{T1, T2}"/> struct.
        /// </summary>
        /// <param name="first">The first piece of data.</param>
        /// <param name="second">The second piece of data.</param>
        public TupleKey(T1 first, T2 second)
        {
            m_first = first;
            m_second = second;

            m_firstHash = (m_first != null) ? m_first.GetHashCode() : 0;
            m_secondHash = (m_second != null) ? m_second.GetHashCode() : 0;

            m_hash = m_firstHash;
            m_hash = (m_hash * 31) + m_secondHash;
        }

        /// <summary>
        /// Tests equality between two keys.
        /// </summary>
        /// <param name="a">The first key</param>
        /// <param name="b">The second key</param>
        /// <returns>True if both keys are equal, false otherwise.</returns>
        public static bool operator ==(TupleKey<T1, T2> a, TupleKey<T1, T2> b)
        {
            return (a.m_firstHash == b.m_firstHash) && (a.m_secondHash == b.m_secondHash);
        }

        /// <summary>
        /// Tests inequality between two keys.
        /// </summary>
        /// <param name="a">The first key</param>
        /// <param name="b">The second key</param>
        /// <returns>True if both keys are not equal, false otherwise.</returns>
        public static bool operator !=(TupleKey<T1, T2> a, TupleKey<T1, T2> b)
        {
            return (a.m_firstHash != b.m_firstHash) || (a.m_secondHash != b.m_secondHash);
        }

        /// <summary>
        /// Indicates whether this instance and a specified object are equal.
        /// </summary>
        /// <param name="obj">Another object to compare to.</param>
        /// <returns>True if <paramref name="obj" /> and this instance are the same type and represent the same value; otherwise, false.</returns>
        public override bool Equals(Object obj)
        {
            if(obj is TupleKey<T1, T2>)
            {
                TupleKey<T1, T2> other = (TupleKey<T1, T2>) obj;
                return (m_firstHash == other.m_firstHash) && (m_secondHash == other.m_secondHash);
            }

            return false;
        }

        /// <summary>
        /// Tests equality between this key and another key.
        /// </summary>
        /// <param name="other">The other key to compare to</param>
        /// <returns>True if both are equal, false otherwise.</returns>
        public bool Equals(TupleKey<T1, T2> other)
        {
            return (m_firstHash == other.m_firstHash) && (m_secondHash == other.m_secondHash);
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns> A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.  </returns>
        public override int GetHashCode()
        {
            return m_hash;
        }

        /// <summary>
        /// Returns the fully qualified type name of this instance.
        /// </summary>
        /// <returns> A <see cref="T:System.String" /> containing a fully qualified type name.</returns>
        public override String ToString()
        {
            CultureInfo info = CultureInfo.CurrentCulture;
            return String.Format(info, "First: {0}, Second: {1}",
                new Object[] { (m_first != null) ? m_first.ToString() : "null", (m_second != null) ? m_second.ToString() : "null" });
        }

        /// <summary>
        /// Compares the current object with another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>
        /// A 32-bit signed integer that indicates the relative order of the objects being compared. The return value has the following meanings: 
        /// Less than zero: This object is less than the other. Zero: This object is equal to the other. Greater than Zero: This object is greater than the other.
        /// </returns>
        public int CompareTo(TupleKey<T1, T2> other)
        {
            int otherFirst = other.m_firstHash;
            int otherSecond = other.m_secondHash;

            int first = m_firstHash;
            int second = m_secondHash;

            //First comes...first, if this is less than it should always come before
            if(first < otherFirst)
            {
                return -1;
            }
            else if(first == otherFirst)
            {
                //First equal, second determines order
                if(second < otherSecond)
                {
                    //Second is less, this should always come before
                    return -1;
                }
                else if(second == otherSecond)
                {
                    //All equal, return 0
                    return 0;
                }
                else
                {
                    //Greater than, this should always come after
                    return 1;
                }
            }
            else
            {
                //Otherwise should come after
                return 1;
            }
        }
    }
}
