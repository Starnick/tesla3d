﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace Tesla
{
    /// <summary>
    /// A read-only list that can wrap modifiable lists or simply contain a non-modifiable collection of elements.
    /// </summary>
    /// <typeparam name="T">Element type</typeparam>
    [DebuggerTypeProxy(typeof(DebugView_ReadOnlyCollection<>)), DebuggerDisplay("Count = {Count}")]
    public class ReadOnlyList<T> : IReadOnlyList<T>
    {
        /// <summary>
        /// The underlying list.
        /// </summary>
        protected IList<T> m_list;

        /// <summary>
        /// Gets the number of elements in the collection.
        /// </summary>
        public int Count
        {
            get
            {
                return m_list.Count;
            }
        }

        /// <summary>
        /// Gets the element at the specified index.
        /// </summary>
        /// <param name="index">Index of element</param>
        /// <returns>The element</returns>
        /// <exception cref="ArgumentOutOfRangeException">Thrown if the index is out of range.</exception>
        public T this[int index]
        {
            get
            {
                if(index < 0 || index >= m_list.Count)
                    throw new ArgumentOutOfRangeException("index", StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));

                return m_list[index];
            }
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="ReadOnlyList{T}"/> class. This effectively creates a read only wrapper
        /// of the list, rather than copying each element.
        /// </summary>
        /// <param name="elements">Element list to hold</param>
        /// <exception cref="System.ArgumentNullException">Thrown if the list is null</exception>
        public ReadOnlyList(IList<T> elements)
        {
            if(elements == null)
                throw new ArgumentNullException("elements");

            m_list = elements;
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="ReadOnlyList{T}"/> class. This copies the elements from the enumerable, rather than
        /// wrappering a collection.
        /// </summary>
        /// <param name="elements">Elements to copy</param>
        /// <exception cref="System.ArgumentNullException">Thrown if the enumerable is null.</exception>
        public ReadOnlyList(IEnumerable<T> elements)
        {
            if(elements == null)
                throw new ArgumentNullException("elements");

            m_list = new List<T>(elements);
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="ReadOnlyList{T}"/> class. This copies the elements from the array, rather than
        /// wrappering a collection.
        /// </summary>
        /// <param name="elements">Elements to copy</param>
        /// <exception cref="System.ArgumentNullException">Thrown if the array is null.</exception>
        public ReadOnlyList(params T[] elements)
        {
            if(elements == null)
                throw new ArgumentNullException("elements");

            m_list = new List<T>(elements);
        }

        /// <summary>
        /// Determines the index of the specified item.
        /// </summary>
        /// <param name="item">Item to find in the list.</param>
        /// <returns>The index of the item, or -1 if it is not present in the list.</returns>
        public int IndexOf(T item)
        {
            return m_list.IndexOf(item);
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.</returns>
        public IEnumerator<T> GetEnumerator()
        {
            return m_list.GetEnumerator();
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return m_list.GetEnumerator();
        }
    }
}
