﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Tesla
{
    /// <summary>
    /// A read only list that pre-computes the hash of the elements contained within.
    /// </summary>
    [DebuggerDisplay("Count = {Count}, Hash = {GetHashCode()}")]
    public class HashedReadOnlyList<T> : ReadOnlyList<T>, IEquatable<HashedReadOnlyList<T>>
    {
        private int m_hashCode;

        /// <summary>
        /// Constructs a new instance of the <see cref="HashedReadOnlyList{T}"/> class.
        /// </summary>
        /// <param name="elements">Element list to hold</param>
        /// <exception cref="System.ArgumentNullException">Thrown if the list is null</exception>
        public HashedReadOnlyList(IList<T> elements)
            : base(elements)
        {
            ComputeHashCode();
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="HashedReadOnlyList{T}"/> class.
        /// </summary>
        /// <param name="elements">Elements to copy</param>
        /// <exception cref="System.ArgumentNullException">Thrown if the enumerable is null.</exception>
        public HashedReadOnlyList(IEnumerable<T> elements)
            : base(elements)
        {
            ComputeHashCode();
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="HashedReadOnlyList{T}"/> class.
        /// </summary>
        /// <param name="elements">Elements to copy</param>
        /// <exception cref="System.ArgumentNullException">Thrown if the array is null.</exception>
        public HashedReadOnlyList(params T[] elements)
            : base(elements)
        {
            ComputeHashCode();
        }

        /// <summary>
        /// Checks equality between two instances of hashed read only list.
        /// </summary>
        /// <param name="a">Left list</param>
        /// <param name="b">Right list</param>
        /// <returns>True if the lists are the same or contain the same elements.</returns>
        public static bool operator ==(HashedReadOnlyList<T> a, HashedReadOnlyList<T> b)
        {
            return Object.Equals(a, b);
        }

        /// <summary>
        /// Checks inequality between two instances of hashed read only list.
        /// </summary>
        /// <param name="a">Left list</param>
        /// <param name="b">Right list</param>
        /// <returns>True if the lists are the not the same or does not contain the same elements.</returns>
        public static bool operator !=(HashedReadOnlyList<T> a, HashedReadOnlyList<T> b)
        {
            return !Object.Equals(a, b);
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>True if the current object is equal to the <paramref name="other" /> parameter; otherwise, false.</returns>
        public bool Equals(HashedReadOnlyList<T> other)
        {
            if(other == null)
                return false;

            if(m_hashCode != other.m_hashCode)
                return false;

            if(!Object.ReferenceEquals(this, other))
            {
                if(Count != other.Count)
                    return false;

                for(int i = 0; i < Count; i++)
                {
                    if(!Object.Equals(this[i], other[i]))
                        return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object" /> is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>True if the specified <see cref="System.Object" /> is equal to this instance; otherwise, false.</returns>
        public override bool Equals(object obj)
        {
            if(obj is HashedReadOnlyList<T>)
                return Equals((HashedReadOnlyList<T>) obj);

            return false;
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            return m_hashCode;
        }

        private void ComputeHashCode()
        {
            unchecked
            {
                int hash = 17;
                hash = (hash * 31) + Count;

                for(int i = 0; i < Count; i++)
                {
                    T elem = this[i];

                    hash = (hash * 31) + ((elem != null) ? elem.GetHashCode() : 0);
                }

                m_hashCode = hash;
            }
        }
    }
}
