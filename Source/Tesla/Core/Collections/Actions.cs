/*
* Copyright (c) 2010-2017 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

namespace Tesla
{
    /// <summary>
    /// Defines a method that compares two objects and returns value indicating whether one is less than, equal to, or greater than the other.
    /// </summary>
    /// <typeparam name="T">Type of object to compare.</typeparam>
    /// <param name="x">The first object to compare.</param>
    /// <param name="y">The second object to compare.</param>
    /// <returns>A signed integer that indicates the relative values of <paramref name="x" /> and <paramref name="y" />. A value less than zero means <paramref name="x" /> is less than <paramref name="y" />,
    /// a value of zero means they are equal, and a value greater than zer means <paramref name="x" /> is greater than <paramref name="y" />.</returns>
    public delegate int RefComparison<T>(in T x, in T y);

    /// <summary>
    /// Defines a method that determines whether the specified object meets certain criteria.
    /// </summary>
    /// <typeparam name="T">Type of object to compare.</typeparam>
    /// <param name="obj">The object to compare.</param>
    /// <returns>True if the object meets the criteria, false if otherwise.</returns>
    public delegate bool RefPredicate<T>(in T obj);

    /// <summary>
    /// Defines a method that converts an object from one type to another type.
    /// </summary>
    /// <typeparam name="TInput">Type of input object to be converted.</typeparam>
    /// <typeparam name="TOutput">Type of output object to be created.</typeparam>
    /// <param name="input">Object to convert.</param>
    /// <param name="output">The converted object.</param>
    public delegate void RefConverter<TInput, TOutput>(in TInput input, out TOutput output);

    /// <summary>
    /// Defines a method that has one parameter and no return value.
    /// </summary>
    public delegate void RefAction<T>(in T arg);

    /// <summary>
    /// Defines a method that has two parameters and no return value.
    /// </summary>
    public delegate void RefAction<T1, T2>(in T1 arg1, in T2 arg2);

    /// <summary>
    /// Defines a method that has three parameters and no return value.
    /// </summary>
    public delegate void RefAction<T1, T2, T3>(in T1 arg1, in T2 arg2, in T3 arg3);

    /// <summary>
    /// Defines a method that has four parameters and no return value.
    /// </summary>
    public delegate void RefAction<T1, T2, T3, T4>(in T1 arg1, in T2 arg2, in T3 arg3, in T4 arg4);

    /// <summary>
    /// Defines a method that has five parameters and no return value.
    /// </summary>
    public delegate void RefAction<T1, T2, T3, T4, T5>(in T1 arg1, in T2 arg2, in T3 arg3, in T4 arg4, in T5 arg5);

    /// <summary>
    /// Defines a method that has six parameters and no return value.
    /// </summary>
    public delegate void RefAction<T1, T2, T3, T4, T5, T6>(in T1 arg1, in T2 arg2, in T3 arg3, in T4 arg4, in T5 arg5, in T6 arg6);

    /// <summary>
    /// Defines a method that has seven parameters and no return value.
    /// </summary>
    public delegate void RefAction<T1, T2, T3, T4, T5, T6, T7>(in T1 arg1, in T2 arg2, in T3 arg3, in T4 arg4, in T5 arg5, in T6 arg6, in T7 arg7);

    /// <summary>
    /// Defines a method that has eight parameters and no return value.
    /// </summary>
    public delegate void RefAction<T1, T2, T3, T4, T5, T6, T7, T8>(in T1 arg1, in T2 arg2, in T3 arg3, in T4 arg4, in T5 arg5, in T6 arg6, in T7 arg7, in T8 arg8);

    /// <summary>
    /// Defines a method that has a single out return parameter.
    /// </summary>
    public delegate void RefFunc<TResult>(out TResult result);

    /// <summary>
    /// Defines a method that has one parameter and a single out return parameter.
    /// </summary>
    public delegate void RefFunc<T, TResult>(in T arg, out TResult result);

    /// <summary>
    /// Defines a method that has two parameters and a single out return parameter.
    /// </summary>
    public delegate void RefFunc<T1, T2, TResult>(in T1 arg1, in T2 arg2, out TResult result);

    /// <summary>
    /// Defines a method that has three parameters and a single out return parameter.
    /// </summary>
    public delegate void RefFunc<T1, T2, T3, TResult>(in T1 arg1, in T2 arg2, in T3 arg3, out TResult result);

    /// <summary>
    /// Defines a method that has four parameters and a single out return parameter.
    /// </summary>
    public delegate void RefFunc<T1, T2, T3, T4, TResult>(in T1 arg1, in T2 arg2, in T3 arg3, in T4 arg4, out TResult result);

    /// <summary>
    /// Defines a method that has five parameters and a single out return parameter.
    /// </summary>
    public delegate void RefFunc<T1, T2, T3, T4, T5, TResult>(in T1 arg1, in T2 arg2, in T3 arg3, in T4 arg4, in T5 arg5, out TResult result);

    /// <summary>
    /// Defines a method that has six parameters and a single out return parameter.
    /// </summary>
    public delegate void RefFunc<T1, T2, T3, T4, T5, T6, TResult>(in T1 arg1, in T2 arg2, in T3 arg3, in T4 arg4, in T5 arg5, in T6 arg6, out TResult result);

    /// <summary>
    /// Defines a method that has seven parameters and a single out return parameter.
    /// </summary>
    public delegate void RefFunc<T1, T2, T3, T4, T5, T6, T7, TResult>(in T1 arg1, in T2 arg2, in T3 arg3, in T4 arg4, in T5 arg5, in T6 arg6, in T7 arg7, out TResult result);

    /// <summary>
    /// Defines a method that has eight parameters and a single out return parameter.
    /// </summary>
    public delegate void RefFunc<T1, T2, T3, T4, T5, T6, T7, T8, TResult>(in T1 arg1, in T2 arg2, in T3 arg3, in T4 arg4, in T5 arg5, in T6 arg6, in T7 arg7, in T8 arg8, out TResult result);
}
