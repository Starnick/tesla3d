/*
* Copyright (c) 2010-2017 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace Tesla
{
  /// <summary>
  /// Provides a base class implementation of <see cref="IRefEqualityComparer{T}"/>.
  /// </summary>
  /// <typeparam name="T">The type of object to compare.</typeparam>
  public abstract class RefEqualityComparer<T> : IRefEqualityComparer<T> where T : struct
  {
    private static volatile RefEqualityComparer<T> s_defaultComparer;

    /// <summary>
    /// Returns a default equality comparer for the type specified by the generic argument.
    /// </summary>
    public static RefEqualityComparer<T> Default
    {
      get
      {
        RefEqualityComparer<T> comparer = s_defaultComparer;
        if (comparer == null)
        {
          comparer = CreateComparer();
          s_defaultComparer = comparer;
        }

        return comparer;
      }
    }

    /// <summary>
    /// If this <see cref="RefEqualityComparer{T}"/> is a wrapper around an <see cref="IEqualityComparer{T}"/>, get the instance.
    /// </summary>
    public virtual IEqualityComparer<T> WrappedComparer
    {
      get
      {
        return null;
      }
    }

    /// <summary>
    /// Queries if this <see cref="RefEqualityComparer{T}"/> wraps an <see cref="IEqualityComparer{T}"/>.
    /// </summary>
    public bool IsWrappedComparer
    {
      get
      {
        return WrappedComparer != null;
      }
    }

    /// <summary>
    /// Determines whether the specified objects are equal.
    /// </summary>
    /// <<param name="x">The first object to compare.</param>
    /// <param name="y">The second object to compare.</param>
    /// <returns>True if the objects are equal, false if otherwise.</returns>
    public abstract bool Equals(in T x, in T y);

    /// <summary>
    /// Returns a hash code for the specified object.
    /// </summary>
    /// <param name="obj">The object for which a hash code is computed.</param>
    /// <returns>A hash code for the specified object.</returns>
    public abstract int GetHashCode(in T obj);

    /// <summary>
    /// Creates an adapter to an existing <see cref="IEqualityComparer{T}"/>.
    /// </summary>
    /// <param name="comparer">Comparer to wrap.</param>
    /// <returns>Wrapped comparer.</returns>
    public static RefEqualityComparer<T> CreateAdapter(IEqualityComparer<T> comparer)
    {
      if (comparer == null)
        throw new ArgumentNullException("comparer");

      return new RefUnknownEqualityComparer<T>(comparer);
    }

    private static RefEqualityComparer<T> CreateComparer()
    {
      Type type = typeof(T);

      if (type == typeof(Byte))
        return (RefEqualityComparer<T>)((object)new RefByteEqualityComparer());

      if (typeof(IRefEquatable<T>).IsAssignableFrom(type))
      {
        Type comparerType = typeof(RefGenericEqualityComparer<>);
        comparerType = comparerType.MakeGenericType(type);

        return (RefEqualityComparer<T>)Activator.CreateInstance(comparerType);
      }

      if (typeof(IEquatable<T>).IsAssignableFrom(type))
      {
        Type comparerType = typeof(RefGenericEqualityComparerAdapter<>);
        comparerType = comparerType.MakeGenericType(type);

        return (RefEqualityComparer<T>)Activator.CreateInstance(comparerType);
      }

      return new RefUnknownEqualityComparer<T>();
    }

    internal virtual int IndexOf(T[] array, in T value, int startIndex, int count)
    {
      int endIndex = startIndex + count;
      for (int i = startIndex; i < endIndex; i++)
      {
        if (Equals(in array[i], in value))
          return i;
      }

      return -1;
    }

    internal virtual int LastIndexOf(T[] array, in T value, int startIndex, int count)
    {
      int endIndex = startIndex - count + 1;
      for (int i = startIndex; i >= endIndex; i--)
      {
        if (Equals(in array[i], in value))
          return i;
      }

      return -1;
    }
  }

  //Equality comparer for most IRefEquatable<T> structs
  internal sealed class RefGenericEqualityComparer<T> : RefEqualityComparer<T> where T : struct, IRefEquatable<T>
  {
    public override bool Equals(in T x, in T y)
    {
      return x.Equals(in y);
    }

    public override int GetHashCode(in T obj)
    {
      return obj.GetHashCode();
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    internal override int IndexOf(T[] array, in T value, int startIndex, int count)
    {
      int endIndex = startIndex + count;
      for (int i = startIndex; i < endIndex; i++)
      {
        if (array[i].Equals(in value))
          return i;
      }

      return -1;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    internal override int LastIndexOf(T[] array, in T value, int startIndex, int count)
    {
      int endIndex = startIndex - count + 1;
      for (int i = startIndex; i >= endIndex; i--)
      {
        if (array[i].Equals(in value))
          return i;
      }

      return -1;
    }
  }

  //Equality comparer for structs that only implement IEquatable<T>
  internal sealed class RefGenericEqualityComparerAdapter<T> : RefEqualityComparer<T> where T : struct, IEquatable<T>
  {
    private IEqualityComparer<T> m_defaultComparer;

    public override IEqualityComparer<T> WrappedComparer
    {
      get
      {
        return m_defaultComparer;
      }
    }

    public RefGenericEqualityComparerAdapter()
    {
      //We get the default comparer for the type to show that we're an adapter, but otherwise we don't actually use it because we can directly call CompareTo
      m_defaultComparer = EqualityComparer<T>.Default;
    }

    public override bool Equals(in T x, in T y)
    {
      return x.Equals(y);
    }

    public override int GetHashCode(in T obj)
    {
      return obj.GetHashCode();
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    internal override int IndexOf(T[] array, in T value, int startIndex, int count)
    {
      int endIndex = startIndex + count;
      for (int i = startIndex; i < endIndex; i++)
      {
        if (array[i].Equals(value))
          return i;
      }

      return -1;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    internal override int LastIndexOf(T[] array, in T value, int startIndex, int count)
    {
      int endIndex = startIndex - count + 1;
      for (int i = startIndex; i >= endIndex; i--)
      {
        if (array[i].Equals(value))
          return i;
      }

      return -1;
    }
  }

  //Equality comparer shim for byte type. Non-ref comparer does a specialized IndexOf method for performance reasons, so we should forward that call.
  internal sealed class RefByteEqualityComparer : RefEqualityComparer<byte>
  {
    private EqualityComparer<byte> m_byteComparer;
    private Func<byte[], byte, int, int, int> m_indexOfMethod; //Array, value, startIndex, count, and returns index

    public override IEqualityComparer<byte> WrappedComparer
    {
      get
      {
        return m_byteComparer;
      }
    }

    public RefByteEqualityComparer()
    {
      m_byteComparer = EqualityComparer<byte>.Default;

      MethodInfo indexOfMethodInfo = m_byteComparer.GetType().GetMethod("IndexOf", BindingFlags.Instance | BindingFlags.NonPublic);
      m_indexOfMethod = (Func<byte[], byte, int, int, int>)Delegate.CreateDelegate(typeof(Func<byte[], byte, int, int, int>), m_byteComparer, indexOfMethodInfo);
    }

    public override bool Equals(in byte x, in byte y)
    {
      return x == y;
    }

    public override int GetHashCode(in byte obj)
    {
      return obj.GetHashCode();
    }

    internal unsafe override int IndexOf(byte[] array, in byte value, int startIndex, int count)
    {
      //Forward to the non-ref equality comparer
      return m_indexOfMethod(array, value, startIndex, count);
    }

    internal override int LastIndexOf(byte[] array, in byte value, int startIndex, int count)
    {
      int endIndex = startIndex - count + 1;
      for (int i = startIndex; i >= endIndex; i--)
      {
        if (array[i] == value)
          return i;
      }
      return -1;
    }
  }

  //Equality comparer for structs that do not implement either...or are enums...
  internal sealed class RefUnknownEqualityComparer<T> : RefEqualityComparer<T> where T : struct
  {
    private IEqualityComparer<T> m_comparer;

    public override IEqualityComparer<T> WrappedComparer
    {
      get
      {
        return m_comparer;
      }
    }

    public RefUnknownEqualityComparer()
    {
      m_comparer = EqualityComparer<T>.Default;
    }

    public RefUnknownEqualityComparer(IEqualityComparer<T> comparer)
    {
      m_comparer = comparer;
    }

    public override bool Equals(in T x, in T y)
    {
      return m_comparer.Equals(x, y);
    }

    public override int GetHashCode(in T obj)
    {
      return m_comparer.GetHashCode(obj);
    }
  }
}
