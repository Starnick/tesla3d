﻿/*
* Copyright (c) 2010-2017 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace Tesla
{
  /// <summary>
  /// A read-only ref list that can wrap modifiable lref ists or simply contain a non-modifiable collection of elements.
  /// </summary>
  /// <typeparam name="T">Type of element in the list</typeparam>
  [DebuggerTypeProxy(typeof(DebugView_ReadOnlyRefList<>)), DebuggerDisplay("Count = {Count}")]
  public class ReadOnlyRefList<T> : IReadOnlyRefList<T> where T : struct
  {
    /// <summary>
    /// The underlying ref list.
    /// </summary>
    protected IRefList<T> m_list;

    /// <summary>
    /// Gets a reference to the element at the given index.
    /// </summary>
    public ref readonly T this[int index]
    {
      get
      {
        return ref m_list[index];
      }
    }

    /// <summary>
    /// Gets the element at the given index.
    /// </summary>
    T IReadOnlyList<T>.this[int index]
    {
      get
      {
        return m_list[index];
      }
    }

    /// <summary>
    /// Gets a span representing the underlying data. Do not add/remove to the list while this is active.
    /// </summary>
    public Span<T> Span
    {
      get
      {
        return m_list.Span;
      }
    }

    /// <summary>
    /// Gets a span representing the underlying data. Do not add/remove to the list while this is active.
    /// </summary>
    ReadOnlySpan<T> IReadOnlyRefList<T>.Span
    {
      get
      {
        return m_list.Span;
      }
    }

    /// <summary>
    /// Gets the number of elements contained in the <see cref="ReadOnlyRefList{T}" />.
    /// </summary>
    public int Count
    {
      get
      {
        return m_list.Count;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="ReadOnlyRefList{T}"/> class. This effectively creates a read only wrapper
    /// of the list, rather than copying each element.
    /// </summary>
    /// <param name="elements">Element list to hold</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the list is null</exception>
    public ReadOnlyRefList(IRefList<T> elements)
    {
      if (elements == null)
        throw new ArgumentNullException("elements");

      m_list = elements;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="ReadOnlyRefList{T}"/> class. This copies the elements from the enumerable, rather than
    /// wrappering a collection.
    /// </summary>
    /// <param name="elements">Elements to copy</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the enumerable is null.</exception>
    public ReadOnlyRefList(IEnumerable<T> elements)
    {
      if (elements == null)
        throw new ArgumentNullException("elements");

      m_list = new RefList<T>(elements);
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="ReadOnlyRefList{T}"/> class. This copies the elements from the array, rather than
    /// wrappering a collection.
    /// </summary>
    /// <param name="elements">Elements to copy</param>
    /// <exception cref="System.ArgumentNullException">Thrown if the array is null.</exception>
    public ReadOnlyRefList(params T[] elements)
    {
      if (elements == null)
        throw new ArgumentNullException("elements");

      m_list = new RefList<T>(elements);
    }

    /// <summary>
    /// Determines whether an element is in the <see cref="ReadOnlyRefList{T}"/>.
    /// </summary>
    /// <param name="item">The object to locate in the <see cref="ReadOnlyRefList{T}"/>.</param>
    /// <returns>True if the <paramref name="item"/> is found, false if otherwise.</returns>
    public bool Contains(in T item)
    {
      return m_list.Contains(item);
    }

    /// <summary>
    /// Copies the contents of the entire <see cref="ReadOnlyRefList{T}"/> into an array, starting at the specified index of the target array.
    /// </summary>
    /// <param name="array">Destination array to hold the elements.</param>
    /// <param name="arrayIndex">Zero-based index in <paramref name="array" /> at which copying begins.</param>
    public void CopyTo(T[] array, int arrayIndex)
    {
      m_list.CopyTo(array, arrayIndex);
    }

    /// <summary>
    /// Returns the index of the first occurance of a particular item, if it is in the <see cref="ReadOnlyRefList{T}"/>. If not, a value of -1 is returned.
    /// </summary>
    /// <param name="item">Item to search for.</param>
    /// <returns>The index of the first occurance of the item, or -1 if it is not in the <see cref="ReadOnlyRefList{T}"/>.</returns>
    public int IndexOf(in T item)
    {
      return m_list.IndexOf(in item);
    }

    /// <summary>
    /// Returns an enumerator that iterates through the collection.
    /// </summary>
    /// <returns>An enumerator that can be used to iterate through the collection.</returns>
    public IEnumerator<T> GetEnumerator()
    {
      return m_list.GetEnumerator();
    }

    /// <summary>
    /// Returns an enumerator that iterates through the collection.
    /// </summary>
    /// <returns>An enumerator that can be used to iterate through the collection.</returns>
    IEnumerator IEnumerable.GetEnumerator()
    {
      return m_list.GetEnumerator();
    }
  }
}
