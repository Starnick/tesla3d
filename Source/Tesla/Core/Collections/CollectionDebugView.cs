﻿/*
* Copyright (c) 2010-2017 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System.Collections.Generic;
using System.Diagnostics;

namespace Tesla
{
    //Debug view for readonly collections
    internal sealed class DebugView_ReadOnlyCollection<T>
    {
        private IReadOnlyCollection<T> m_collection;

        [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
        public T[] Items
        {
            get
            {
                T[] array = new T[m_collection.Count];

                int index = 0;
                foreach (T val in m_collection)
                    array[index++] = val;

                return array;
            }
        }

        public DebugView_ReadOnlyCollection(IReadOnlyCollection<T> collection)
        {
            m_collection = collection;
        }
    }

    //Debug view for readonly dictionaries
    internal sealed class DebugView_ReadOnlyDictionary<K, V>
    {
        private IReadOnlyDictionary<K, V> m_dict;

        [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
        public KeyValuePair<K, V>[] Items
        {
            get
            {
                KeyValuePair<K, V>[] array = new KeyValuePair<K, V>[m_dict.Count];

                int index = 0;
                foreach (KeyValuePair<K, V> kv in m_dict)
                    array[index++] = kv;

                return array;
            }
        }

        public DebugView_ReadOnlyDictionary(IReadOnlyDictionary<K, V> dict)
        {
            m_dict = dict;
        }
    }

    //Debug view for ref lists, should be faster because of the CopyTo
    internal sealed class DebugView_ReadOnlyRefList<T> where T : struct
    {
        private IReadOnlyRefList<T> m_list;

        [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
        public T[] Items
        {
            get
            {
                T[] array = new T[m_list.Count];
                m_list.CopyTo(array, 0);

                return array;
            }
        }

        public DebugView_ReadOnlyRefList(IReadOnlyRefList<T> list)
        {
            m_list = list;
        }
    }
}
