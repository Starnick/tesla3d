﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// GameTime is a snapshot of time during the simulation. It is managed by the GameTimer class for
  /// its values to be set.
  /// </summary>
  public sealed class GameTime : IGameTime
  {
    private static readonly IGameTime s_zeroTime = new GameTime(TimeSpan.Zero, TimeSpan.Zero);

    private TimeSpan m_elapsedGameTime;
    private TimeSpan m_totalGameTime;
    private bool m_isRunningSlowly;
    private double m_tpf;

    /// <summary>
    /// Gets a game time that has zero elapsed time, representing a "null" value.
    /// </summary>
    public static IGameTime ZeroTime
    {
      get
      {
        return s_zeroTime;
      }
    }

    /// <summary>
    /// Gets the elapsed time span since the last snapshot.
    /// </summary>
    public TimeSpan ElapsedGameTime
    {
      get
      {
        return m_elapsedGameTime;
      }
      set
      {
        m_elapsedGameTime = value;
        m_tpf = value.TotalSeconds;
      }
    }

    /// <summary>
    /// Gets the total time the simulation has been running.
    /// </summary>
    public TimeSpan TotalGameTime
    {
      get
      {
        return m_totalGameTime;
      }
      set
      {
        m_totalGameTime = value;
      }
    }

    /// <summary>
    /// Gets the elapsed time since the last snapshot in seconds, in single precision.
    /// </summary>
    public float ElapsedTimeInSeconds
    {
      get
      {
        return (float) m_tpf;
      }
    }

    /// <summary>
    /// Gets the elapsed time since the last snapshot in seconds, in double precision.
    /// </summary>
    public double ElapsedTimeInSecondsDouble
    {
      get
      {
        return m_tpf;
      }
    }

    /// <summary>
    /// Gets if the game update is running slowly, that is the actual
    /// elapsed time is greater than the target elapsed time.
    /// </summary>
    public bool IsRunningSlowly
    {
      get
      {
        return m_isRunningSlowly;
      }
      set
      {
        m_isRunningSlowly = value;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="GameTime"/> class.
    /// </summary>
    public GameTime() : this(TimeSpan.Zero, TimeSpan.Zero, false) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="GameTime"/> class.
    /// </summary>
    /// <param name="elapsedGameTime">The elapsed game time.</param>
    /// <param name="totalGameTime">The total game time.</param>
    /// <param name="isRunningSlowly">If the update is running slowly, that is the actual elapsed time is more than the target elapsed time.</param>
    public GameTime(TimeSpan elapsedGameTime, TimeSpan totalGameTime, bool isRunningSlowly = false)
    {
      m_elapsedGameTime = elapsedGameTime;
      m_totalGameTime = totalGameTime;
      m_tpf = elapsedGameTime.TotalSeconds;
      m_isRunningSlowly = isRunningSlowly;
    }

    /// <summary>
    /// Populates the game time with the specified values.
    /// </summary>
    /// <param name="elapsedGameTime">The elapsed game time.</param>
    /// <param name="totalGameTime">The total game time.</param>
    /// <param name="isRunningSlowly">If the update is running slowly, that is the actual elapsed time is more than the target elapsed time.</param>
    public void Set(TimeSpan elapsedGameTime, TimeSpan totalGameTime, bool isRunningSlowly = false)
    {
      m_elapsedGameTime = elapsedGameTime;
      m_totalGameTime = totalGameTime;
      m_tpf = elapsedGameTime.TotalSeconds;
      m_isRunningSlowly = isRunningSlowly;
    }

    /// <summary>
    /// Populates the game time from another instance.
    /// </summary>
    /// <param name="time">Other game time instance.</param>
    public void Set(IGameTime time)
    {
      if (time is null)
        return;

      m_elapsedGameTime = time.ElapsedGameTime;
      m_totalGameTime = time.TotalGameTime;
      m_tpf = m_elapsedGameTime.TotalSeconds;
      m_isRunningSlowly = time.IsRunningSlowly;
    }
  }
}
