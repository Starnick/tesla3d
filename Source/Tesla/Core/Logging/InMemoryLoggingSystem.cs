﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// A logging system that captures all the log information as a list of strings that can then be displayed in a UI system.
  /// </summary>
  public class InMemoryLoggingSystem : ConsoleLoggingSystem
  {
    private object m_writeSync = new object();
    private List<string> m_logLines;

    /// <summary>
    /// Gets the list of log lines.
    /// </summary>
    public IReadOnlyList<string> Log { get { return m_logLines; } }

    /// <summary>
    /// Constructs a new instance of the <see cref="InMemoryLoggingSystem"/> class.
    /// </summary>
    public InMemoryLoggingSystem()
    {
      m_logLines = new List<string>();
    }

    /// <inheritdoc />
    protected override void WriteLogLine(string line)
    {
      lock (m_writeSync)
      {
        OnLogLine(line);
        m_logLines.Add(line);
      }
    }

    /// <summary>
    /// Does additional processing when a log line is submitted.
    /// </summary>
    /// <param name="line">Log line.</param>
    protected virtual void OnLogLine(string line) { }
  }
}
