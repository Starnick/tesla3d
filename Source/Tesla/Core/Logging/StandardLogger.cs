﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Logger that outputs to the console.
  /// </summary>
  public class StandardLogger : ILogger
  {
    /// <summary>
    /// Default time stamp pattern.
    /// </summary>
    public const string DEFAULT_TIMESTAMP_PATTERN = "MMM d, yyyy h:mm:ss tt";

    private string m_name;
    private string m_pattern = DEFAULT_TIMESTAMP_PATTERN;
    private ILoggingSystem m_parent;
    private Action<string> m_writeLineImpl;

    /// <summary>
    /// Gets if fatal level logging is enabled.
    /// </summary>
    public bool IsFatalEnabled { get { return IsEnabledFor(LogLevel.Fatal); } }

    /// <summary>
    /// Gets if error level logging is enabled.
    /// </summary>
    public bool IsErrorEnabled { get { return IsEnabledFor(LogLevel.Error); } }

    /// <summary>
    /// Gets if warning level logging is enabled.
    /// </summary>
    public bool IsWarnEnabled { get { return IsEnabledFor(LogLevel.Warn); } }

    /// <summary>
    /// Gets if debug level logging is enabled.
    /// </summary>
    public bool IsDebugEnabled { get { return IsEnabledFor(LogLevel.Debug); } }

    /// <summary>
    /// Gets the name of the logger.
    /// </summary>
    public string Name { get { return m_name; } }

    /// <summary>
    /// Gets or sets the time stamp pattern.
    /// </summary>
    public string TimeStampPattern
    {
      get
      {
        return m_pattern;
      }
      set
      {
        if (!String.IsNullOrEmpty(value))
          value = DEFAULT_TIMESTAMP_PATTERN;

        m_pattern = value;
      }
    }

    public StandardLogger(string name, ILoggingSystem parent, Action<string> writeLineImpl)
    {
      m_name = name;
      m_parent = parent;
      m_writeLineImpl = writeLineImpl;
    }

    /// <summary>
    /// Checks if the logger is enabled for the specified level.
    /// </summary>
    /// <param name="level">Log level to check</param>
    /// <returns>True if the logger is enabled for that level</returns>
    public bool IsEnabledFor(LogLevel level)
    {
      if (!m_parent.LoggingEnabled)
        return false;

      LogLevel thresold = m_parent.Threshold;

      if (level > thresold)
        return false;

      return true;
    }

    /// <summary>
    /// Logs a message.
    /// </summary>
    /// <param name="level">Level of message</param>
    /// <param name="message">Message to log</param>
    public void Log(LogLevel level, string message)
    {
      if (IsEnabledFor(level))
      {
        if (message is null)
          return;

        m_writeLineImpl($"{GetTimeStamp()} {GetLogLevelName(level)} {message}");
      }
    }

    private string GetTimeStamp()
    {
      if (!m_parent.DisplayLoggerName)
      {
        return DateTime.Now.ToString(m_pattern);
      }
      else
      {
        return $"{m_name} {DateTime.Now.ToString(m_pattern)}";
      }
    }

    private string GetLogLevelName(LogLevel level)
    {
      switch (level)
      {
        case LogLevel.Debug:
          return "DEBUG:";
        case LogLevel.Info:
          return "INFO:";
        case LogLevel.Warn:
          return "WARN:";
        case LogLevel.Error:
          return "ERROR:";
        case LogLevel.Fatal:
          return "FATAL:";
        default:
          return String.Empty;
      }
    }
  }
}
