﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Interface for a logger.
  /// </summary>
  public interface ILogger
  {
    /// <summary>
    /// Gets if fatal level logging is enabled.
    /// </summary>
    bool IsFatalEnabled { get; }

    /// <summary>
    /// Gets if error level logging is enabled.
    /// </summary>
    bool IsErrorEnabled { get; }

    /// <summary>
    /// Gets if warning level logging is enabled.
    /// </summary>
    bool IsWarnEnabled { get; }

    /// <summary>
    /// Gets if debug level logging is enabled.
    /// </summary>
    bool IsDebugEnabled { get; }

    /// <summary>
    /// Gets the name of the logger.
    /// </summary>
    string Name { get; }

    /// <summary>
    /// Checks if the logger is enabled for the specified level.
    /// </summary>
    /// <param name="level">Log level to check</param>
    /// <returns>True if the logger is enabled for that level</returns>
    bool IsEnabledFor(LogLevel level);

    /// <summary>
    /// Logs a message.
    /// </summary>
    /// <param name="level">Level of message</param>
    /// <param name="message">Message to log</param>
    void Log(LogLevel level, string message);
  }

  /// <summary>
  /// Interface for a provider of loggers.
  /// </summary>
  public interface ILoggingSystem : IDisposableEngineService
  {
    /// <summary>
    /// Gets the current <see cref="ILoggingSystem"/> registered to the <see cref="Engine.Instance"/>. This will return
    /// a default <see cref="ConsoleLoggingSystem"/> if <see cref="Engine.Initialize"/> has not yet been called -or- the current instance is null.
    /// </summary>
    public static ILoggingSystem Current { get { return CoreServices.LoggingSystemOrDefault; } }

    /// <summary>
    /// Gets the default logger.
    /// </summary>
    ILogger DefaultLogger { get; }

    /// <summary>
    /// Gets or sets the logging level threshold. Messages with a level above this are not logged.
    /// </summary>
    LogLevel Threshold { get; set; }

    /// <summary>
    /// Gets or sets if logging is enabled.
    /// </summary>
    bool LoggingEnabled { get; set; }

    /// <summary>
    /// Gets or sets if the logger's name should be displayed in the message.
    /// </summary>
    bool DisplayLoggerName { get; set; }

    /// <summary>
    /// Gets an attached logger specified by its name. If it does not exist, it is created.
    /// </summary>
    /// <param name="name">Name of the logger</param>
    /// <returns>The logger</returns>
    ILogger GetLogger(string name);

    /// <summary>
    /// Gets an attached logger specified by a type. If it does not exist, it is created.
    /// </summary>
    /// <param name="type">Type that the logger is bound to</param>
    /// <returns>The logger</returns>
    ILogger GetLogger(Type type);
  }
}
