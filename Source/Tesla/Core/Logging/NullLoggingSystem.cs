﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// A Null Logger that does not output any log messages.
  /// </summary>
  public sealed class NullLogger : ILogger
  {
    /// <inheritdoc />
    public bool IsFatalEnabled { get { return false; } }

    /// <inheritdoc />
    public bool IsErrorEnabled { get { return false; } }

    /// <inheritdoc />
    public bool IsWarnEnabled { get { return false; } }

    /// <inheritdoc />
    public bool IsDebugEnabled { get { return false; } }

    /// <inheritdoc />
    public string Name { get { return "NullLogger"; } }

    /// <inheritdoc />
    public bool IsEnabledFor(LogLevel level) { return false; }

    /// <inheritdoc />
    public void Log(LogLevel level, string message) { }
  }

  /// <summary>
  /// A null logging system that does not log anything.
  /// </summary>
  public sealed class NullLoggingSystem : ILoggingSystem
  {
    private NullLogger m_nullLogger;
    private bool m_isEnabled;
    private bool m_isDisposed;
    private LogLevel m_threshold;
    private bool m_displayName;

    /// <inheritdoc />
    public ILogger DefaultLogger { get { return m_nullLogger; } }

    /// <inheritdoc />
    public LogLevel Threshold { get { return m_threshold; } set { m_threshold = value; } }

    /// <inheritdoc />
    public bool LoggingEnabled { get { return m_isEnabled; } set { m_isEnabled = value; } }

    /// <inheritdoc />
    public bool DisplayLoggerName { get { return m_displayName; } set { m_displayName = value; } }

    /// <inheritdoc />
    public bool IsDisposed { get { return m_isDisposed; } }

    /// <inheritdoc />
    public string Name { get { return "NullLoggingSystem"; } }

    /// <summary>
    /// Constructs a new instance of the <see cref="NullLoggingSystem"/> class.
    /// </summary>
    public NullLoggingSystem()
    {
      m_nullLogger = new NullLogger();
      m_isEnabled = false;
      m_isDisposed = false;
      m_displayName = false;
      m_threshold = LogLevel.Fatal;
    }

    /// <inheritdoc />
    public ILogger GetLogger(string name)
    {
      return m_nullLogger;
    }

    /// <inheritdoc />
    public ILogger GetLogger(Type type)
    {
      return m_nullLogger;
    }

    /// <inheritdoc />
    public void Initialize(Engine engine) { }

    /// <inheritdoc />
    public void Dispose()
    {
      m_isDisposed = true;
    }
  }
}
