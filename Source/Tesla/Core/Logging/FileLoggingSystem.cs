﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Tesla.Content;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Logging system that creates and manages loggers that log messages to a file.
  /// </summary>
  public sealed class FileLoggingSystem : ILoggingSystem
  {
    private static NullLogger s_nullLogger = new NullLogger();

    private Dictionary<string, StandardLogger> m_loggers;
    private LogLevel m_threshold;
    private bool m_displayLoggerName;
    private bool m_loggingEnabled;
    private bool m_isDisposed;
    private StreamWriter m_output;
    private object m_sync = new object();
    private object m_writeSync = new object();
    private IResourceRepository m_fileSystem;

    /// <inheritdoc />
    public ILogger DefaultLogger { get { return GetLogger("Tesla"); } }

    /// <inheritdoc />
    public LogLevel Threshold { get { return m_threshold; } set { m_threshold = value; } }

    /// <inheritdoc />
    public bool LoggingEnabled { get { return m_loggingEnabled; } set { m_loggingEnabled = value; } }

    /// <inheritdoc />
    public bool DisplayLoggerName { get { return m_displayLoggerName; } set { m_displayLoggerName = value; } }

    /// <inheritdoc />
    public bool IsDisposed { get { return m_isDisposed; } }

    /// <inheritdoc />
    public string Name { get { return "FileLoggingSystem"; } }

    /// <summary>
    /// Constructs a new instance of the <see cref="FileLoggingSystem"/> class.
    /// </summary>
    public FileLoggingSystem() : this("Tesla.log") { }

    /// <summary>
    /// Constructs a new instance of the <see cref="FileLoggingSystem"/> class.
    /// </summary>
    /// <param name="fileName">Log file name</param>
    public FileLoggingSystem(string fileName)
    {
      m_loggingEnabled = true;
      m_displayLoggerName = false;
      m_threshold = LogLevel.Info;
      m_loggers = new Dictionary<string, StandardLogger>();

      if (!Path.IsPathRooted(fileName))
        fileName = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly()!.Location)!, fileName);

      m_fileSystem = new FileResourceRepository(Path.GetDirectoryName(fileName)!);
      m_fileSystem.OpenConnection(ResourceFileMode.CreateNew, ResourceFileShare.None);

      IResourceFile file = m_fileSystem.GetResourceFile(fileName);

      if (file.Exists)
        file.Delete();

      m_output = new StreamWriter(file.OpenWrite());
      m_output.AutoFlush = true;
    }

    /// <inheritdoc />
    /// <exception cref="System.ArgumentNullException">Thrown if the name is null or empty</exception>
    public ILogger GetLogger(string name)
    {
      if (String.IsNullOrEmpty(name))
        throw new ArgumentNullException(nameof(name), StringLocalizer.Instance.GetLocalizedString("NameIsNull"));

      CheckDisposed();

      lock (m_sync)
      {
        if (!m_loggingEnabled)
          return s_nullLogger;

        if (!m_loggers.TryGetValue(name, out StandardLogger? logger))
        {
          logger = new StandardLogger(name, this, WriteLogLine);
          m_loggers.Add(name, logger);
        }

        return logger;
      }
    }

    /// <inheritdoc />
    /// <exception cref="System.ArgumentNullException">Thrown if the type is null</exception>
    public ILogger GetLogger(Type type)
    {
      if (type is null)
        throw new ArgumentNullException(nameof(type), StringLocalizer.Instance.GetLocalizedString("TypeIsNull"));

      return GetLogger(type.FullName!);
    }

    /// <inheritdoc />
    public void Initialize(Engine engine) { }

    /// <inheritdoc />
    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    private void Dispose(bool disposing)
    {
      if (!m_isDisposed)
      {
        if (m_output != null)
        {
          m_fileSystem.CloseConnection();
          m_output.Close();
          m_output = null!;
        }

        m_isDisposed = true;
      }
    }

    private void CheckDisposed()
    {
      if (m_isDisposed)
        throw new ObjectDisposedException(StringLocalizer.Instance.GetLocalizedString("ObjectDisposed"));
    }

    private void WriteLogLine(string line)
    {
      lock (m_writeSync)
        m_output.WriteLine(line);
    }
  }
}
