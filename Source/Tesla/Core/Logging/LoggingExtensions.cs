﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Extensions for logging methods.
  /// </summary>
  public static class LoggingExtensions
  {
    /// <summary>
    /// Logs a message.
    /// </summary>
    /// <param name="logger">Logger</param>
    /// <param name="level">Level of the message</param>
    /// <param name="format">Composite message format</param>
    /// <param name="args">Objects to format string representation</param>
    public static void Log(this ILogger logger, LogLevel level, string format, params object?[] args)
    {
      if (String.IsNullOrEmpty(format) || args is null || args.Length == 0)
        return;

      logger.Log(level, String.Format(format, args));
    }

    /// <summary>
    /// Logs a message.
    /// </summary>
    /// <param name="logger">Logger</param>
    /// <param name="level">Level of the message</param>
    /// <param name="provider">Format provider that provides culture-specific formatting</param>
    /// <param name="format">Composite message format</param>
    /// <param name="args">Objects to format string representations</param>
    public static void Log(this ILogger logger, LogLevel level, IFormatProvider provider, string format, params object?[] args)
    {
      if (provider is null || String.IsNullOrEmpty(format) || args is null || args.Length == 0)
        return;

      logger.Log(level, String.Format(provider, format, args));
    }

    /// <summary>
    /// Logs a message.
    /// </summary>
    /// <param name="logger">Logger</param>
    /// <param name="level">Level of the message</param>
    /// <param name="format">Composite message format</param>
    /// <param name="arg">Object to format string representation</param>
    public static void Log(this ILogger logger, LogLevel level, string format, object arg)
    {
      if (String.IsNullOrEmpty(format) || arg is null)
        return;

      logger.Log(level, String.Format(format, arg));
    }

    /// <summary>
    /// Logs a message.
    /// </summary>
    /// <param name="logger">Logger</param>
    /// <param name="level">Level of the message</param>
    /// <param name="format">Composite message format</param>
    /// <param name="arg0">First object to format string representation</param>
    /// <param name="arg1">Second object to format string representation</param>
    public static void Log(this ILogger logger, LogLevel level, string format, object arg0, object arg1)
    {
      if (String.IsNullOrEmpty(format) || arg0 is null || arg1 is null)
        return;

      logger.Log(level, String.Format(format, arg0, arg1));
    }

    /// <summary>
    /// Logs a message.
    /// </summary>
    /// <param name="logger">Logger</param>
    /// <param name="level">Level of the message</param>
    /// <param name="format">Composite message format</param>
    /// <param name="arg0">First object to format string representation</param>
    /// <param name="arg1">Second object to format string representation</param>
    /// <param name="arg2">Third object to format string representation</param>
    public static void Log(this ILogger logger, LogLevel level, string format, object arg0, object arg1, object arg2)
    {
      if (String.IsNullOrEmpty(format) || arg0 is null || arg1 is null || arg2 is null)
        return;

      logger.Log(level, String.Format(format, arg0, arg1, arg2));
    }

    /// <summary>
    /// Logs an exception.
    /// </summary>
    /// <param name="logger">Logger</param>
    /// <param name="level">Level of the message</param>
    /// <param name="message">Message to log</param>
    /// <param name="exception">Exception to log</param>
    public static void LogException(this ILogger logger, LogLevel level, string message, Exception? exception)
    {
      if (exception is null)
      {
        logger.Log(level, GetExceptionThrownMessage(exception, message));
      }
      else
      {
        string text = $"{GetExceptionThrownMessage(exception, message)}\n\n{exception.Message}\n\n{exception.StackTrace}\n\n";
        logger.Log(level, text);
      }
    }

    /// <summary>
    /// Logs an exception.
    /// </summary>
    /// <param name="logger">Logger</param>
    /// <param name="level">Level of the message</param>
    /// <param name="exception">Exception to log</param>
    public static void LogException(this ILogger logger, LogLevel level, Exception exception)
    {
      if (exception is null)
        return;

      string text = $"{GetExceptionThrownMessage(exception, null)}\n\n{exception.Message}\n\n{exception.StackTrace}\n\n";
      logger.Log(level, text);
    }

    private static string GetExceptionThrownMessage(Exception? e, string? altText)
    {
      if (String.IsNullOrEmpty(altText))
        altText = StringLocalizer.Instance.GetLocalizedString("AnExceptionWasThrown");

      if (e is null)
      {
        return altText;
      }
      else
      {
        return StringLocalizer.Instance.GetLocalizedString("ThisExceptionWasThrown", e.GetType().FullName);
      }
    }
  }
}
