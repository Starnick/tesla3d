﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections;
using System.Collections.Generic;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Registry that manages core engine services. An engine service typically is registered by an interface that it implements, e.g. a render system
  /// implementation to an <code>IRenderSystem</code> interface.
  /// </summary>
  public sealed class EngineServiceRegistry : IServiceProvider, IEnumerable<IEngineService>
  {
    private Dictionary<Type, IEngineService> m_services;
    private List<IUpdatableEngineService> m_updateServices;
    private Engine m_engine;

    internal EngineServiceRegistry(Engine engine)
    {
      m_services = new Dictionary<Type, IEngineService>();
      m_updateServices = new List<IUpdatableEngineService>();
      m_engine = engine;
    }

    /// <summary>
    /// Gets the service object of the specified type.
    /// </summary>
    /// <param name="serviceType">An object that specifies the type of service object to get.</param>
    /// <returns>
    /// A service object of type <paramref name="serviceType" />.-or- null if there is no service object of type <paramref name="serviceType" />.
    /// </returns>
    public object? GetService(Type serviceType)
    {
      if (serviceType is null)
        return null;

      IEngineService? service;
      if (m_services.TryGetValue(serviceType, out service))
        return service;

      return GetNextBestService(serviceType);
    }

    /// <summary>
    /// Adds a service to the registry.
    /// </summary>
    /// <typeparam name="T">Service type - commonly an interface.</typeparam>
    /// <param name="service">The service to add</param>
    /// <exception cref="ArgumentNullException">Thrown if the service is null.</exception>
    /// <exception cref="ArgumentException">Thrown if the specified type is not assignable from the actual service type.</exception>
    /// <exception cref="TeslaException">Thrown if the registry already contains the service type</exception>
    public void AddService<T>(IEngineService service) where T : class, IEngineService
    {
      AddService(typeof(T), service);
    }

    /// <summary>
    /// Adds a service to the registry.
    /// </summary>
    /// <param name="type">Type of service to add - commonly an interface.</param>
    /// <param name="service">The service to add</param>
    /// <exception cref="ArgumentNullException">Thrown if the type or service is null.</exception>
    /// <exception cref="ArgumentException">Thrown if the specified type is not assignable from the actual service type -or- the registry already contains the service type -or- the engine
    /// registry is missing dependencies as defined by <see cref="RequiredDependencyAttribute"/>.</exception>
    public void AddService(Type type, IEngineService service)
    {
      ArgumentNullException.ThrowIfNull(type, nameof(type));

      if (service is null)
        throw new ArgumentNullException(nameof(service), StringLocalizer.Instance.GetLocalizedString("EngineServiceCannotBeNull"));

      if (!type.IsAssignableFrom(service.GetType()))
        throw new ArgumentException(StringLocalizer.Instance.GetLocalizedString("EngineServiceNotAssignableToType"));

      if (m_services.ContainsKey(type))
        throw new ArgumentException(StringLocalizer.Instance.GetLocalizedString("EngineServiceAlreadyRegistered"));

      ThrowIfMissingRequiredDependencies(type, service);

      m_services.Add(type, service);

      if (service is IUpdatableEngineService updatableService)
        m_updateServices.Add(updatableService);

      service.Initialize(m_engine);
      OnServiceAdded(type, service);
    }

    /// <summary>
    /// Gets a service from the registry.
    /// </summary>
    /// <typeparam name="T">Service type - commonly an interface.</typeparam>
    /// <returns>The service, or null if it does not exist or the service type is not assignable from the actual object type</returns>
    public T GetService<T>() where T : class?, IEngineService?
    {
      Type type = typeof(T);

      IEngineService? service;
      if (m_services.TryGetValue(type, out service))
        return (service as T)!;

      return (GetNextBestService(type) as T)!;
    }

    /// <summary>
    /// Replaces a service in the registry. If an already registered service is not found, the new one is simply added. Otherwise,
    /// the old one is replaced. This is mainly used to "hot swap" currently running implementations.
    /// </summary>
    /// <typeparam name="T">Service type - commonly an interface.</typeparam>
    /// <param name="service">The service</param>
    /// <param name="disposeOldService">True if the old service should be disposed, false otherwise. By default this is true.</param>
    /// <returns>The old engine service, if there was one.</returns>
    /// <exception cref="ArgumentNullException">Thrown if the service is null.</exception>
    /// <exception cref="ArgumentException">Thrown if the specified type is not assignable from the actual service type.</exception>
    public IEngineService? ReplaceService<T>(IEngineService service, bool disposeOldService = true) where T : class, IEngineService
    {
      return ReplaceService(typeof(T), service, disposeOldService);
    }

    /// <summary>
    /// Replaces a service in the registry. If an already registered service is not found, the new one is simply added. Otherwise,
    /// the old one is replaced. This is mainly used to "hot swap" currently running implementations. The order of services that can be
    /// updated is preserved.
    /// </summary>
    /// <param name="type">Type of service to replace - commonly an interface.</param>
    /// <param name="service">The service</param>
    /// <param name="disposeOldService">True if the old service should be disposed, false otherwise. By default this is true.</param>
    /// <returns>The old engine service, if there was one.</returns>
    /// <exception cref="ArgumentNullException">Thrown if the type or service is null.</exception>
    /// <exception cref="ArgumentException">Thrown if the specified type is not assignable from the actual service type.</exception>
    public IEngineService? ReplaceService(Type type, IEngineService service, bool disposeOldService = true)
    {
      ArgumentNullException.ThrowIfNull(type, nameof(type));

      if (service is null)
        throw new ArgumentNullException(nameof(service), StringLocalizer.Instance.GetLocalizedString("EngineServiceCannotBeNull"));

      if (!type.IsAssignableFrom(service.GetType()))
        throw new ArgumentException(StringLocalizer.Instance.GetLocalizedString("EngineServiceNotAssignableToType"));

      IEngineService? oldService;
      if (!m_services.TryGetValue(type, out oldService))
      {
        m_services.Add(type, service);

        if (service is IUpdatableEngineService updatableService)
          m_updateServices.Add(updatableService);

        service.Initialize(m_engine);
        OnServiceAdded(type, service);
      }
      else
      {
        m_services[type] = service;

        //Replace at the same index...
        IUpdatableEngineService? oldServiceUpdateable = oldService as IUpdatableEngineService;
        IUpdatableEngineService? newServiceUpdateable = service as IUpdatableEngineService;
        if (oldServiceUpdateable is not null && newServiceUpdateable is not null)
        {
          int index = m_updateServices.IndexOf(oldServiceUpdateable);
          System.Diagnostics.Debug.Assert(index != -1); //Sanity check

          m_updateServices[index] = newServiceUpdateable;
        }
        else if (oldServiceUpdateable is not null)
        {
          //Should rarely, if ever, happen that the new service type would differ on interfaces
          m_updateServices.Remove(oldServiceUpdateable);
        }

        service.Initialize(m_engine);
        OnServiceReplaced(type, oldService, service);

        if (disposeOldService)
          TryDisposeService(type, oldService);
      }

      return oldService;
    }

    /// <summary>
    /// Updates all engine services (in the order they were registered) in the registry that implement the <see cref="IUpdatableEngineService"/> interface.
    /// </summary>
    /// <param name="time">Elapsed time since the last update.</param>
    public void UpdateServices(IGameTime time)
    {
      for (int i = 0; i < m_updateServices.Count; i++)
        m_updateServices[i].Update(time);
    }

    /// <summary>
    /// Called when the engine is destroyed to clear the registry but also dispose of all resources.
    /// </summary>
    internal void RemoveAllAndDispose(List<ILoggingSystem> loggers)
    {
      foreach (KeyValuePair<Type, IEngineService> kv in m_services)
      {
        if (kv.Value is ILoggingSystem logger)
        {
          loggers.Add(logger);
          continue;
        }

        TryDisposeService(kv.Key, kv.Value);
      }

      m_updateServices.Clear();
      m_services.Clear();
    }

    /// <summary>
    /// Returns an enumerator that iterates through the collection.
    /// </summary>
    /// <returns>A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.</returns>
    public IEnumerator<IEngineService> GetEnumerator()
    {
      return m_services.Values.GetEnumerator();
    }

    /// <summary>
    /// Returns an enumerator that iterates through a collection.
    /// </summary>
    /// <returns>An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.</returns>
    IEnumerator IEnumerable.GetEnumerator()
    {
      return m_services.Values.GetEnumerator();
    }

    private void TryDisposeService(Type serviceType, IEngineService service)
    {
      IDisposableEngineService? disposable = service as IDisposableEngineService;

      if (disposable is not null)
      {
        disposable.Dispose();
        EngineLog.Log(LogLevel.Info, $"Destroyed Engine Service: {service.Name} [{serviceType.ToString()}]");
      }
    }

    private IEngineService? GetNextBestService(Type type)
    {
      foreach (KeyValuePair<Type, IEngineService> kv in m_services)
      {
        if (kv.Key.IsAssignableFrom(type))
          return kv.Value;
      }

      return null;
    }

    private void OnServiceAdded(Type type, IEngineService service)
    {
      EngineServiceChangedEventArgs args = new EngineServiceChangedEventArgs(type, service);
      NotifyDependencyChanged(args);
      m_engine.NotifyServiceChanged(args);
    }

    private void OnServiceReplaced(Type type, IEngineService oldService, IEngineService newService)
    {
      EngineServiceChangedEventArgs args = new EngineServiceChangedEventArgs(type, newService, oldService);
      NotifyDependencyChanged(args);
      m_engine.NotifyServiceChanged(args);
    }

    private void NotifyDependencyChanged(EngineServiceChangedEventArgs args)
    {
      foreach (KeyValuePair<Type, IEngineService> kv in m_services)
      {
        if (kv.Value is not IEngineServiceDependencyChanged serviceToNotify)
          continue;

        if (args.ChangeEventType == ServiceChangeEventType.Added)
        {
          if (IsOptionalDependency(args.ServiceType, serviceToNotify))
            serviceToNotify.OnDependencyChanged(args);
        }
        else
        {
          if (IsOptionalDependency(args.ServiceType, serviceToNotify) || IsRequiredDependency(args.ServiceType, serviceToNotify))
            serviceToNotify.OnDependencyChanged(args);
        }
      }
    }

    private bool IsOptionalDependency(Type changingServiceType, IEngineService service)
    {
      object[] attrs = service.GetType().GetCustomAttributes(typeof(OptionalDependencyAttribute), true);
      if (attrs is null || attrs.Length == 0)
        return false;

      foreach (object obj in attrs)
      {
        OptionalDependencyAttribute? attr = obj as OptionalDependencyAttribute;
        if (attr is null)
          continue;

        foreach (Type targetType in attr.Dependencies)
        {
          if (changingServiceType == targetType)
            return true;
        }
      }

      return false;
    }

    private bool IsRequiredDependency(Type changingServiceType, IEngineService service)
    {
      object[] attrs = service.GetType().GetCustomAttributes(typeof(RequiredDependencyAttribute), true);
      if (attrs is null || attrs.Length == 0)
        return false;

      foreach (object obj in attrs)
      {
        RequiredDependencyAttribute? attr = obj as RequiredDependencyAttribute;
        if (attr is null)
          continue;

        foreach (Type targetType in attr.Dependencies)
        {
          if (changingServiceType == targetType)
            return true;
        }
      }

      return false;
    }

    private void ThrowIfMissingRequiredDependencies(Type serviceType, IEngineService service)
    {
      object[] attrs = service.GetType().GetCustomAttributes(typeof(RequiredDependencyAttribute), true);
      if (attrs is null || attrs.Length == 0)
        return;

      foreach (object obj in attrs)
      {
        RequiredDependencyAttribute? attr = obj as RequiredDependencyAttribute;
        if (attr is null)
          continue;

        foreach (Type targetType in attr.Dependencies)
        {
          if (m_services.ContainsKey(targetType))
            continue;

          string errorMsg = $"Missing dependency [{targetType.ToString()}] for '{service.Name}' [{serviceType.ToString()}]";
          EngineLog.Log(LogLevel.Error, errorMsg);

          throw new ArgumentException(errorMsg);
        }
      }

      return;
    }
  }
}
