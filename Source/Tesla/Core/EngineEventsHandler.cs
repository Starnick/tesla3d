﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// A listener that tracks the static <see cref="Engine"/> events and listens for <see cref="EngineServiceRegistry"/> changes.
  /// When disposed, the listeners are removed from the static <see cref="Engine"/> events.
  /// </summary>
  public class EngineEventsHandler : IDisposable
  {
    /// <summary>
    /// Occurs when the engine is about to be initialized, before any resources have been initialized.
    /// </summary>
    public static event TypedEventHandler<Engine>? EngineInitializing;

    /// <summary>
    /// Occurs when the engine has been initialized, after all resources have been initialized.
    /// </summary>
    public static event TypedEventHandler<Engine>? EngineInitialized;

    /// <summary>
    /// Occurs when the engine is about to be destroyed, before any resource is disposed.
    /// </summary>
    public event TypedEventHandler<Engine>? EngineDestroying;

    /// <summary>
    /// Occurs when the engine has been destroyed, disposing of all resources.
    /// </summary>
    public event TypedEventHandler<Engine>? EngineDestroyed;

    /// <summary>
    /// Occurs when any engine service has changed - either added or replaced.
    /// </summary>
    public event TypedEventHandler<Engine, EngineServiceChangedEventArgs>? ServiceChanged;

    /// <summary>
    /// Constructs a new instance of <see cref="EngineEventsHandler"/>.
    /// </summary>
    /// <param name="handlerIfAlreadyInitialized">If the engine is already initialized, this is immediately invoked.</param>
    public EngineEventsHandler(TypedEventHandler<Engine>? handlerIfAlreadyInitialized = null)
    {
      Engine.Initializing += HandleEngineInitializing;
      Engine.Initialized += HandleEngineInitialized;
      Engine.Destroying += HandleEngineDestroying;
      Engine.Destroyed += HandleEngineDestroyed;
      Engine.ServiceChanged += HandleEngineServiceChanged;

      if (handlerIfAlreadyInitialized is not null && Engine.IsInitialized)
        handlerIfAlreadyInitialized(Engine.Instance, EventArgs.Empty);
    }

    /// <summary>
    /// Cleans up engine events.
    /// </summary>
    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    /// <summary>
    /// Cleans up engine events.
    /// </summary>
    protected virtual void Dispose(bool disposing)
    {
      Engine.Initializing -= HandleEngineInitializing;
      Engine.Initialized -= HandleEngineInitialized;
      Engine.Destroying -= HandleEngineDestroying;
      Engine.Destroyed -= HandleEngineDestroyed;
      Engine.ServiceChanged -= HandleEngineServiceChanged;
    }

    /// <summary>
    /// Invoked when the <see cref="Engine.Initializing"/> event is invoked.
    /// </summary>
    /// <param name="engine">Engine instance.</param>
    /// <param name="args">Empty args.</param>
    protected virtual void HandleEngineInitializing(Engine engine, EventArgs args)
    {
      EngineInitializing?.Invoke(engine, args);
    }

    /// <summary>
    /// Invoked when the <see cref="Engine.Initialized"/> event is invoked.
    /// </summary>
    /// <param name="engine">Engine instance.</param>
    /// <param name="args">Empty args.</param>
    protected virtual void HandleEngineInitialized(Engine engine, EventArgs args)
    {
      EngineInitialized?.Invoke(engine, args);
    }

    /// <summary>
    /// Invoked when the <see cref="Engine.Destroying"/> event is invoked.
    /// </summary>
    /// <param name="engine">Engine instance.</param>
    /// <param name="args">Empty args.</param>
    protected virtual void HandleEngineDestroying(Engine engine, EventArgs args)
    {
      EngineDestroying?.Invoke(engine, args);
    }

    /// <summary>
    /// Invoked when the <see cref="Engine.Destroyed"/> event is invoked.
    /// </summary>
    /// <param name="engine">Engine instance.</param>
    /// <param name="args">Empty args.</param>
    protected virtual void HandleEngineDestroyed(Engine engine, EventArgs args)
    {
      EngineDestroyed?.Invoke(engine, args);
    }

    /// <summary>
    /// Invoked when the <see cref="Engine.ServiceChanged"/> event is invoked.
    /// </summary>
    /// <param name="engine"></param>
    /// <param name="args"></param>
    protected virtual void HandleEngineServiceChanged(Engine engine, EngineServiceChangedEventArgs args)
    {
      ServiceChanged?.Invoke(engine, args);
    }
  }

  /// <summary>
  /// Listens to engine events and tracks a single <see cref="IEngineService"/> type for when it is added or replaced.
  /// </summary>
  /// <typeparam name="T">Service type.</typeparam>
  public sealed class EngineServiceTracker<T> : EngineEventsHandler where T : class, IEngineService
  {
    // Backing store for the service added, we want to let handlers be notified if things already are up and running.
    private event TypedEventHandler<Engine, EngineServiceChangedEventArgs>? m_trackedServiceChanged;
    private T? m_service;

    /// <summary>
    /// Occurs when the tracked engine service has been added or replaced to the engine. This will invoke
    /// if the service is already present when the event is added.
    /// </summary>
    public event TypedEventHandler<Engine, EngineServiceChangedEventArgs>? TrackedServiceChanged
    {
      add
      {
        if (value is null)
          return;

        m_trackedServiceChanged += value;

        // If service already there, invoke immediately.
        if (m_service is not null)
          value(Engine.Instance, new EngineServiceChangedEventArgs(typeof(T), m_service));
      }
      remove
      {
        m_trackedServiceChanged -= value;
      }
    }

    /// <summary>
    /// Gets the tracked service, if it exists.
    /// </summary>
    public T? Service { [MethodImpl(MethodImplOptions.AggressiveInlining)] get { return m_service; } }

    /// <summary>
    /// Gets the tracked service or throws an exception if it doesn't exist.
    /// </summary>
    /// <exception cref="ArgumentNullException">Thrown if the service is null.</exception>
    public T ServiceOrThrow
    {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get
      {
        if (m_service is null)
          throw new ArgumentNullException(typeof(T).FullName, StringLocalizer.Instance.GetLocalizedString("EngineServiceDoesNotExist", typeof(T).FullName));

        return m_service;
      }
    }

    /// <summary>
    /// Constructs a new instance of <see cref="EngineServiceTracker{T}"/>.
    /// </summary>
    /// <param name="handlerIfAlreadyExists">If the engine is already initialized and service exists, this is immediately invoked.</param>
    public EngineServiceTracker(TypedEventHandler<Engine, EngineServiceChangedEventArgs>? handlerIfAlreadyExists = null)
    {
      // If the engine is already initialized, try and get the service
      if (Engine.IsInitialized)
      {
        m_service = Engine.Instance.Services.GetService<T?>();

        if (m_service is not null && handlerIfAlreadyExists is not null)
          handlerIfAlreadyExists(Engine.Instance, new EngineServiceChangedEventArgs(typeof(T), m_service));
      }
    }

    /// <inheritdoc />
    protected override void HandleEngineServiceChanged(Engine engine, EngineServiceChangedEventArgs args)
    {
      if (args.IsService<T>())
      {
        m_service = args.ServiceAs<T>();
        m_trackedServiceChanged?.Invoke(engine, args);
      }

      base.HandleEngineServiceChanged(engine, args);
    }

    /// <inheritdoc />
    protected override void HandleEngineDestroying(Engine engine, EventArgs args)
    {
      base.HandleEngineDestroying(engine, args);

      m_service = null;
    }
  }
}
