﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Debug-only wrapper around the engine's logging sevice's default logger.
  /// </summary>
  public static class EngineLog
  {
    /// <summary>
    /// Logs a message.
    /// </summary>
    /// <param name="level">Level of message</param>
    /// <param name="message">Message to log</param>
    [Conditional("DEBUG")]
    public static void Log(LogLevel level, string message)
    {
      ILoggingSystem.Current.DefaultLogger.Log(level, message);
    }

    /// <summary>
    /// Logs a message.
    /// </summary>
    /// <param name="level">Level of the message</param>
    /// <param name="format">Composite message format</param>
    /// <param name="args">Objects to format string representation</param>
    [Conditional("DEBUG")]
    public static void Log(LogLevel level, string format, params object?[] args)
    {
      ILoggingSystem.Current.DefaultLogger.Log(level, format, args);
    }

    /// <summary>
    /// Logs a message.
    /// </summary>
    /// <param name="level">Level of the message</param>
    /// <param name="format">Composite message format</param>
    /// <param name="arg">Object to format string representation</param>
    [Conditional("DEBUG")]
    public static void Log(LogLevel level, string format, object arg)
    {
      ILoggingSystem.Current.DefaultLogger.Log(level, format, arg);
    }

    /// <summary>
    /// Logs a message.
    /// </summary>
    /// <param name="level">Level of the message</param>
    /// <param name="provider">Format provider that provides culture-specific formatting</param>
    /// <param name="format">Composite message format</param>
    /// <param name="args">Objects to format string representations</param>
    [Conditional("DEBUG")]
    public static void Log(LogLevel level, IFormatProvider provider, string format, params object?[] args)
    {
      ILoggingSystem.Current.DefaultLogger.Log(level, provider, format, args);
    }

    /// <summary>
    /// Logs a message.
    /// </summary>
    /// <param name="level">Level of the message</param>
    /// <param name="format">Composite message format</param>
    /// <param name="arg0">First object to format string representation</param>
    /// <param name="arg1">Second object to format string representation</param>
    [Conditional("DEBUG")]
    public static void Log(LogLevel level, string format, object arg0, object arg1)
    {
      ILoggingSystem.Current.DefaultLogger.Log(level, format, arg0, arg1);
    }

    /// <summary>
    /// Logs a message.
    /// </summary>
    /// <param name="level">Level of the message</param>
    /// <param name="format">Composite message format</param>
    /// <param name="arg0">First object to format string representation</param>
    /// <param name="arg1">Second object to format string representation</param>
    /// <param name="arg2">Third object to format string representation</param>
    [Conditional("DEBUG")]
    public static void Log(LogLevel level, string format, object arg0, object arg1, object arg2)
    {
      ILoggingSystem.Current.DefaultLogger.Log(level, format, arg0, arg1, arg2);
    }

    /// <summary>
    /// Logs an exception.
    /// </summary>
    /// <param name="level">Level of the message</param>
    /// <param name="message">Message to log</param>
    /// <param name="exception">Exception to log</param>
    [Conditional("DEBUG")]
    public static void LogException(LogLevel level, string message, Exception? exception)
    {
      ILoggingSystem.Current.DefaultLogger.LogException(level, message, exception);
    }

    /// <summary>
    /// Logs an exception.
    /// </summary>
    /// <param name="level">Level of the message</param>
    /// <param name="exception">Exception to log</param>
    [Conditional("DEBUG")]
    public static void LogException(LogLevel level, Exception exception)
    {
      ILoggingSystem.Current.DefaultLogger.LogException(level, exception);
    }
  }
}
