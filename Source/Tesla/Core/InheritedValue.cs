﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System.Diagnostics;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Defines a value that has additional metadata denoting whether the value has been inherited or not.
  /// </summary>
  /// <typeparam name="T">Type of value.</typeparam>
  [DebuggerDisplay("{Value}, IsInherited = {IsInherited}")]
  public struct InheritedValue<T>
  {
    /// <summary>
    /// The underlying value.
    /// </summary>
    public T Value;

    /// <summary>
    /// Whether or not the value is inherited.
    /// </summary>
    public bool IsInherited;

    /// <summary>
    /// Constructs a new instance of the <see cref="InheritedValue{T}"/> struct.
    /// </summary>
    /// <param name="value">The value, set to not inherited.</param>
    public InheritedValue(in T value)
    {
      Value = value;
      IsInherited = false;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="InheritedValue{T}"/> struct.
    /// </summary>
    /// <param name="value">The value.</param>
    /// <param name="isInherited">True if the value is inherited, false if otherwise.</param>
    public InheritedValue(in T value, bool isInherited)
    {
      Value = value;
      IsInherited = isInherited;
    }

    /// <summary>
    /// Performs an implicit conversion from <see cref="Tesla.InheritedValue{T}" /> to <see cref="T" />.
    /// </summary>
    /// <param name="rhs">The inherited value.</param>
    /// <returns>The underlying value.</returns>
    public static implicit operator T(InheritedValue<T> rhs)
    {
      return rhs.Value;
    }

    /// <summary>
    /// Performs an implicit conversion from <see cref="T" /> to <see cref="Tesla.InheritedValue{T}" />.
    /// </summary>
    /// <param name="rhs">The value.</param>
    /// <returns>Inherited value, set to false.</returns>
    public static implicit operator InheritedValue<T>(T rhs)
    {
      return new InheritedValue<T>(rhs, false);
    }
  }
}
