﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

#nullable enable

namespace Tesla
{
  /// <summary>
  /// Represents a method that takes a strongly typed sender and event argument.
  /// </summary>
  /// <typeparam name="TSender">Sender object type</typeparam>
  /// <typeparam name="TEventArgs">Event args type</typeparam>
  /// <param name="sender">Source of the event</param>
  /// <param name="args">Event arguments</param>
  [SuppressMessage("Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly", Justification = "Using strong-typed TypedEventHandler<TSender, TEventArgs> event handler pattern.")]
  [Serializable]
  public delegate void TypedEventHandler<TSender, TEventArgs>(TSender sender, TEventArgs args);

  /// <summary>
  /// Represents a method that takes a strongly typed sender.
  /// </summary>
  /// <typeparam name="TSender">Sender object type</typeparam>
  /// <param name="sender">Source of the event</param>
  /// <param name="args">Event arguments</param>
  [SuppressMessage("Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly", Justification = "Using strong-typed TypedEventHandler<TSender> event handler pattern.")]
  [Serializable]
  public delegate void TypedEventHandler<TSender>(TSender sender, EventArgs args);

  /// <summary>
  /// Represents an event that can trigger a number of event handlers. The event filters duplicate event handlers
  /// and minimizes allocations. This is not a thread-safe class.
  /// </summary>
  /// <typeparam name="TSender">Event sender object type</typeparam>
  /// <typeparam name="TEventArgs">Event args type</typeparam>
  public sealed class TypedEvent<TSender, TEventArgs>
  {
    private List<TypedEventHandler<TSender, TEventArgs>> m_invocationList;

    /// <summary>
    /// Constructs a new instance of the <see cref="TypedEvent{TSender, TEventArgs}"/> class.
    /// </summary>
    public TypedEvent()
    {
      m_invocationList = new List<TypedEventHandler<TSender, TEventArgs>>();
    }

    /// <summary>
    /// Adds the event handler to the event.
    /// </summary>
    /// <param name="handler">Event handler to add, if not already present in the event.</param>
    public void Add(TypedEventHandler<TSender, TEventArgs>? handler)
    {
      if (handler is null)
        return;

      for (int i = 0; i < m_invocationList.Count; i++)
      {
        if (Object.ReferenceEquals(m_invocationList[i], handler))
          return;
      }

      m_invocationList.Add(handler);
    }

    /// <summary>
    /// Removes the event handler from the event.
    /// </summary>
    /// <param name="handler">Event handler to remove.</param>
    public void Remove(TypedEventHandler<TSender, TEventArgs>? handler)
    {
      if (handler is null)
        return;

      for (int i = 0; i < m_invocationList.Count; i++)
      {
        if (Object.ReferenceEquals(m_invocationList[i], handler))
        {
          m_invocationList.RemoveAt(i);
          return;
        }
      }
    }

    /// <summary>
    /// Invokes the event and calls each handler.
    /// </summary>
    /// <param name="sender">Object on which the event is being handled.</param>
    /// <param name="args">Event data to deliver to handlers.</param>
    public void Invoke(TSender sender, TEventArgs args)
    {
      for (int i = 0; i < m_invocationList.Count; i++)
      {
        TypedEventHandler<TSender, TEventArgs> handler = m_invocationList[i];
        handler(sender, args);
      }
    }
  }

  /// <summary>
  /// Represents an event that can trigger a number of event handlers. The event filters duplicate event handlers
  /// and minimizes allocations. This is not a thread-safe class.
  /// </summary>
  /// <typeparam name="TSender">Event sender object type</typeparam>
  public sealed class TypedEvent<TSender>
  {
    private List<TypedEventHandler<TSender>> m_invocationList;

    /// <summary>
    /// Constructs a new instance of the <see cref="TypedEvent{TSender}"/> class.
    /// </summary>
    public TypedEvent()
    {
      m_invocationList = new List<TypedEventHandler<TSender>>();
    }

    /// <summary>
    /// Adds the event handler to the event.
    /// </summary>
    /// <param name="handler">Event handler to add, if not already present in the event.</param>
    public void Add(TypedEventHandler<TSender>? handler)
    {
      if (handler is null)
        return;

      for (int i = 0; i < m_invocationList.Count; i++)
      {
        if (Object.ReferenceEquals(m_invocationList[i], handler))
          return;
      }

      m_invocationList.Add(handler);
    }

    /// <summary>
    /// Removes the event handler from the event.
    /// </summary>
    /// <param name="handler">Event handler to remove.</param>
    public void Remove(TypedEventHandler<TSender>? handler)
    {
      if (handler is null)
        return;

      for (int i = 0; i < m_invocationList.Count; i++)
      {
        if (Object.ReferenceEquals(m_invocationList[i], handler))
        {
          m_invocationList.RemoveAt(i);
          return;
        }
      }
    }

    /// <summary>
    /// Invokes the event and calls each handler.
    /// </summary>
    /// <param name="sender">Object on which the event is being handled.</param>
    public void Invoke(TSender sender)
    {
      for (int i = 0; i < m_invocationList.Count; i++)
      {
        TypedEventHandler<TSender> handler = m_invocationList[i];
        handler(sender, EventArgs.Empty);
      }
    }
  }

  #region EngineServiceRegistry Events

  /// <summary>
  /// Event arguments for when an <see cref="IEngineService"/> is added or replaced.
  /// </summary>
  public sealed class EngineServiceChangedEventArgs : EventArgs
  {
    private Type m_serviceType;
    private IEngineService m_service;
    private IEngineService? m_oldService;

    /// <summary>
    /// Gets the type that the service is registered to.
    /// </summary>
    public Type ServiceType { get { return m_serviceType; } }

    /// <summary>
    /// Gets the service that was added.
    /// </summary>
    public IEngineService Service { get { return m_service; } }

    /// <summary>
    /// Gets the old service (if any), if the service was replaced.
    /// </summary>
    public IEngineService? OldService { get { return m_oldService; } }

    /// <summary>
    /// Gets the change event type.
    /// </summary>
    public ServiceChangeEventType ChangeEventType { get { return m_oldService is null ? ServiceChangeEventType.Added : ServiceChangeEventType.Replaced; } } 

    internal EngineServiceChangedEventArgs(Type serviceType, IEngineService service, IEngineService? oldService = null)
    {
      m_serviceType = serviceType;
      m_service = service;
      m_oldService = oldService;
    }

    /// <summary>
    /// Determines if the service is of the specified type.
    /// </summary>
    /// <typeparam name="T">Engine service type.</typeparam>
    /// <returns>True if the service is of that type, false if not.</returns>
    public bool IsService<T>() where T : IEngineService
    {
      return typeof(T) == m_serviceType;
    }

    /// <summary>
    /// Casts the modified service to the specified type.
    /// </summary>
    /// <typeparam name="T">Engine service type.</typeparam>
    /// <returns>Engine Service.</returns>
    public T ServiceAs<T>() where T : class?, IEngineService?
    {
      return (m_service as T)!;
    }

    /// <summary>
    /// Casts the old service, if any, to the specified type.
    /// </summary>
    /// <typeparam name="T">Engine service type.</typeparam>
    /// <returns>Engine Service.</returns>
    public T OldServiceAs<T>() where T : class?, IEngineService?
    {
      return (m_oldService as T)!;
    }
  }

  /// <summary>
  /// Provides data for an event that is triggered when an engine service is added or removed.
  /// </summary>
  public sealed class EngineServiceEventArgs : EventArgs
  {
    private Type m_serviceType;
    private IEngineService m_service;
    private ServiceChangeEventType m_eventType;

    /// <summary>
    /// Gets the type that the service is registered to.
    /// </summary>
    public Type ServiceType
    {
      get
      {
        return m_serviceType;
      }
    }

    /// <summary>
    /// Gets the service that was modified.
    /// </summary>
    public IEngineService Service
    {
      get
      {
        return m_service;
      }
    }

    /// <summary>
    /// Gets the change event type.
    /// </summary>
    public ServiceChangeEventType ChangedEventType
    {
      get
      {
        return m_eventType;
      }
    }

    internal EngineServiceEventArgs(Type serviceType, IEngineService service, ServiceChangeEventType eventType)
    {
      m_serviceType = serviceType;
      m_service = service;
      m_eventType = eventType;
    }

    /// <summary>
    /// Determines if the service is of the specified type.
    /// </summary>
    /// <typeparam name="T">Engine service type.</typeparam>
    /// <returns>True if the service is of that type, false if not.</returns>
    public bool IsService<T>() where T : IEngineService
    {
      return typeof(T) == m_serviceType;
    }

    /// <summary>
    /// Casts the modified service to the specified type.
    /// </summary>
    /// <typeparam name="T">Engine service type.</typeparam>
    /// <returns>Engine Service.</returns>
    public T ServiceAs<T>() where T : class?, IEngineService?
    {
      return (m_service as T)!;
    }
  }

  /// <summary>
  /// Provides data for an event that is triggered when an engine service is replaced by a new service.
  /// </summary>
  public sealed class EngineServiceReplacedEventArgs : EventArgs
  {
    private Type m_serviceType;
    private IEngineService m_oldService;
    private IEngineService m_newService;

    /// <summary>
    /// Gets the type that the service is registered to.
    /// </summary>
    public Type ServiceType
    {
      get
      {
        return m_serviceType;
      }
    }

    /// <summary>
    /// Gets the old service that has been removed.
    /// </summary>
    public IEngineService OldService
    {
      get
      {
        return m_oldService;
      }
    }

    /// <summary>
    /// Gets the new service that has been added.
    /// </summary>
    public IEngineService NewService
    {
      get
      {
        return m_newService;
      }
    }

    internal EngineServiceReplacedEventArgs(Type serviceType, IEngineService oldService, IEngineService newService)
    {
      m_serviceType = serviceType;
      m_oldService = oldService;
      m_newService = newService;
    }

    /// <summary>
    /// Casts the old service to the specified type.
    /// </summary>
    /// <typeparam name="T">Engine service type.</typeparam>
    /// <returns>Service.</returns>
    public T OldServiceAs<T>() where T : class?, IEngineService?
    {
      return (m_oldService as T)!;
    }

    /// <summary>
    /// Casts the new service to the specified type.
    /// </summary>
    /// <typeparam name="T">Engine service type.</typeparam>
    /// <returns>Service.</returns>
    public T NewServiceAs<T>() where T : class?, IEngineService?
    {
      return (m_newService as T)!;
    }
  }

  #endregion
}
