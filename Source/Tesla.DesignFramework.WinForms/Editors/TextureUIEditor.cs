﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/


using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using Tesla.Graphics;
using System.IO;

namespace Tesla.DesignFramework
{
  public class TextureUIEditor : UITypeEditor
  {
    private OpenFileDialog m_fileDialog;

    public TextureUIEditor()
    {
      m_fileDialog = new OpenFileDialog();
    }
    public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
    {
      return UITypeEditorEditStyle.Modal;
    }

    public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
    {
      DialogResult result = m_fileDialog.ShowDialog();

      if (result == DialogResult.OK)
      {
        if (File.Exists(m_fileDialog.FileName))
        {
          Tesla.Content.TextureImporterParameters importParams = new Content.TextureImporterParameters();
          importParams.GenerateMipMaps = true;
          Texture tex = Engine.Instance.Services.GetService<IDesignApp>().ContentManager.LoadIsolated<Texture>(m_fileDialog.FileName, importParams, false);
          return tex;
        }
      }

      return base.EditValue(context, provider, value);
    }

    public override bool GetPaintValueSupported(ITypeDescriptorContext context)
    {
      return true;
    }

    public override void PaintValue(PaintValueEventArgs e)
    {
      Texture tex = e.Value as Texture;
      if (tex == null)
        return;

      IDesignApp designApp = Engine.Instance.Services.GetService<IDesignApp>();
      Bitmap thumbnail = designApp.ContentManager.GetThumbnail(tex);

      e.Graphics.DrawImage(thumbnail, e.Bounds);
    }
  }
}
