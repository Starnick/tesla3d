﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/


using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using System.Windows.Forms;

namespace Tesla.DesignFramework
{
  public class ColorUIEditor : UITypeEditor
  {
    private ColorDialog m_colorDialog;

    public ColorUIEditor()
    {
      m_colorDialog = new ColorDialog();
      m_colorDialog.FullOpen = true;
    }

    public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
    {
      return UITypeEditorEditStyle.Modal;
    }

    public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
    {
      DialogResult result = m_colorDialog.ShowDialog();

      if (result == DialogResult.OK)
      {
        System.Drawing.Color c = m_colorDialog.Color;
        return new Color(c.R, c.G, c.B, c.A);
      }

      return base.EditValue(context, provider, value);
    }

    public override bool GetPaintValueSupported(ITypeDescriptorContext context)
    {
      return true;
    }

    public override void PaintValue(PaintValueEventArgs e)
    {
      Tesla.Color c = (Tesla.Color) e.Value;

      SolidBrush brush = new SolidBrush(System.Drawing.Color.FromArgb(c.A, c.R, c.G, c.B));
      e.Graphics.FillRectangle(brush, e.Bounds);
      brush.Dispose();
    }
  }

  public class MaterialColorUIEditor : UITypeEditor
  {
    private ColorDialog m_colorDialog;

    public MaterialColorUIEditor()
    {
      m_colorDialog = new ColorDialog();
      m_colorDialog.FullOpen = true;
    }

    public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
    {
      return UITypeEditorEditStyle.Modal;
    }

    public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
    {
      DialogResult result = m_colorDialog.ShowDialog();

      if (result == DialogResult.OK)
      {
        System.Drawing.Color c = m_colorDialog.Color;
        return new Color(c.R, c.G, c.B, c.A);
      }

      return base.EditValue(context, provider, value);
    }

    public override bool GetPaintValueSupported(ITypeDescriptorContext context)
    {
      return true;
    }

    public override void PaintValue(PaintValueEventArgs e)
    {
      Tesla.Color c = (Tesla.Color) e.Value;

      SolidBrush brush = new SolidBrush(System.Drawing.Color.FromArgb(c.A, c.R, c.G, c.B));
      e.Graphics.FillRectangle(brush, e.Bounds);

      // String x = "X";
      System.Drawing.Rectangle r = e.Bounds;
      r.X += 10;
      e.Graphics.FillRectangle(brush, r);

      brush.Dispose();
    }
  }
}
