﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Application;
using Tesla.Input;
using Tesla.Input.Utilities;

namespace Tesla.Windows.Input
{
  /// <summary>
  /// Keyboard input system that acquires state data through the Win32 API.
  /// </summary>
  [OptionalDependency(typeof(IApplicationHost))]
  public sealed class Win32KeyboardInputSystem : IKeyboardInputSystem, IEngineServiceDependencyChanged
  {
    private KeyboardStateBuilder m_stateBuilder;
    private IApplicationHost? m_host;

    /// <inheritdoc />
    public String Name
    {
      get
      {
        return "Win32KeyboardInputSystem";
      }
    }

    /// <inheritdoc />
    public InputFocusState FocusState
    {
      get
      {
        return m_stateBuilder.FocusState;
      }
      set
      {
        m_stateBuilder.FocusState = value;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Win32KeyboardInputSystem"/> class.
    /// </summary>
    public Win32KeyboardInputSystem()
    {
      m_stateBuilder = new KeyboardStateBuilder();
    }

    /// <inheritdoc />
    public void GetKeyboardState(out KeyboardState state)
    {
      m_stateBuilder.ConstructState(out state);
    }

    /// <inheritdoc />
    public void Update(IGameTime time)
    {
      m_stateBuilder.Clear();

      if (m_host is not null)
        m_stateBuilder.FocusState = m_host.DetermineKeyboardFocusState();

      unsafe
      {
        KeyByteArray keys;

        if (!NativeMethods.GetKeyboardState((byte*) &keys))
          return;

        for (int i = 0; i < 256; i++)
        {
          byte key = keys.Keys[i];
          if ((key & 0x80) != 0)
          {
            //Make sure we filter out any mouse buttons!
            switch (i)
            {
              case 0x01: //Left
              case 0x02: //Right
              case 0x04: //Middle
              case 0x05: //XButton1
              case 0x06: //XButton2
                break;
              default:
                m_stateBuilder.AddPressedKey((Keys) i);
                break;
            }
          }
        }
      }
    }

    /// <inheritdoc />
    public void Initialize(Engine engine)
    {
      m_host = engine.Services.GetService<IApplicationHost?>();
    }

    /// <inheritdoc />
    public void OnDependencyChanged(EngineServiceChangedEventArgs args)
    {
      if (args.IsService<IApplicationHost>())
        m_host = args.ServiceAs<IApplicationHost>();
    }
  }
}
