﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Application;
using Tesla.Input;
using SWF = System.Windows.Forms;

namespace Tesla.Windows.Input
{
  /// <summary>
  /// Mouse input system that acquires state data from the Win32 API.
  /// </summary>
  [OptionalDependency(typeof(IApplicationHost))]
  public sealed class Win32MouseInputSystem : IMouseInputSystem, IDisposableEngineService, IEngineServiceDependencyChanged
  {
    private IntPtr m_handle;
    private MouseWheelListener m_wheelListener;
    private bool m_isDisposed;

    private POINT m_rawPos;
    private POINT m_currPos;
    private ButtonState m_lButton;
    private ButtonState m_rButton;
    private ButtonState m_mButton;
    private ButtonState m_xButton1;
    private ButtonState m_xButton2;
    private int m_wheelValue;
    private int m_deltaWheelValue;
    private InputFocusState m_focusState;
    private IApplicationHost? m_host;

    /// <inheritdoc />
    public String Name
    {
      get
      {
        return "Win32MouseInputSystem";
      }
    }

    /// <inheritdoc />
    public InputFocusState FocusState
    {
      get
      {
        return m_focusState;
      }
      set
      {
        m_focusState = value;
      }
    }

    /// <inheritdoc />
    public IntPtr WindowHandle
    {
      get
      {
        return m_handle;
      }
      set
      {
        if (value != m_handle)
        {
          m_handle = value;

          m_wheelListener.ReleaseHandle();
          if (m_handle != IntPtr.Zero)
            m_wheelListener.AssignHandle(m_handle);
        }
      }
    }

    /// <inheritdoc />
    public bool IsDisposed
    {
      get
      {
        return m_isDisposed;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="Win32MouseInputSystem"/> class.
    /// </summary>
    public Win32MouseInputSystem() : this(IntPtr.Zero) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="Win32MouseInputSystem"/> class.
    /// </summary>
    /// <param name="handle">The handle.</param>
    public Win32MouseInputSystem(IntPtr handle)
    {
      m_wheelListener = new MouseWheelListener();
      WindowHandle = handle;
    }

    /// <inheritdoc />
    public void GetMouseState(out MouseState state)
    {
      state = new MouseState(m_currPos.X, m_currPos.Y, m_wheelValue, m_deltaWheelValue, m_lButton, m_rButton, m_mButton, m_xButton1, m_xButton2, m_focusState);
    }

    /// <inheritdoc />
    public void GetMouseState(IntPtr windowHandle, out MouseState state)
    {
      POINT pos = m_rawPos;
      if (windowHandle != IntPtr.Zero)
        NativeMethods.ScreenToClient(windowHandle, ref pos);

      state = new MouseState(pos.X, pos.Y, m_wheelValue, m_deltaWheelValue, m_lButton, m_rButton, m_mButton, m_xButton1, m_xButton2, m_focusState);
    }

    /// <inheritdoc />
    public void SetPosition(int x, int y)
    {
      m_currPos = m_rawPos = new POINT(x, y);

      if (m_handle != IntPtr.Zero)
        NativeMethods.ClientToScreen(m_handle, ref m_rawPos);

      NativeMethods.SetCursorPos(m_rawPos.X, m_rawPos.Y);
    }

    /// <inheritdoc />
    public void Update(IGameTime time)
    {
      NativeMethods.GetCursorPos(out m_rawPos);
      m_currPos = m_rawPos;

      if (m_handle != IntPtr.Zero)
        NativeMethods.ScreenToClient(m_handle, ref m_currPos);

      m_lButton = (ButtonState) (((ushort) NativeMethods.GetAsyncKeyState(1)) >> 15);
      m_rButton = (ButtonState) (((ushort) NativeMethods.GetAsyncKeyState(2)) >> 15);
      m_mButton = (ButtonState) (((ushort) NativeMethods.GetAsyncKeyState(4)) >> 15);
      m_xButton1 = (ButtonState) (((ushort) NativeMethods.GetAsyncKeyState(5)) >> 15);
      m_xButton2 = (ButtonState) (((ushort) NativeMethods.GetAsyncKeyState(6)) >> 15);

      int scrollWheel = m_wheelListener.CurrentWheelValue;
      m_deltaWheelValue = scrollWheel - m_wheelValue;
      m_wheelValue = scrollWheel;

      if (m_host is not null)
        m_focusState = m_host.DetermineMouseFocusState(m_handle);
    }

    //// <inheritdoc />
    public void Initialize(Engine engine)
    {
      m_host = engine.Services.GetService<IApplicationHost?>();
    }

    /// <inheritdoc />
    public void OnDependencyChanged(EngineServiceChangedEventArgs args)
    {
      if (args.IsService<IApplicationHost>())
        m_host = args.ServiceAs<IApplicationHost>();
    }

    /// <inheritdoc />
    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    private void Dispose(bool disposing)
    {
      if (!m_isDisposed)
      {
        if (disposing)
        {
          m_wheelListener.ReleaseHandle();
        }

        m_isDisposed = true;
      }
    }

    private sealed class MouseWheelListener : SWF.NativeWindow
    {
      private int m_currentWheelValue;
      private int m_previousWheelValue;

      public int CurrentWheelValue
      {
        get
        {
          return m_currentWheelValue;
        }
      }

      public int CurrentWheelDelta
      {
        get
        {
          return m_currentWheelValue - m_previousWheelValue;
        }
      }

      protected override void WndProc(ref SWF.Message m)
      {
        if (m.Msg == 522)
        {
          int delta = SignedHIWORD(m.WParam);
          m_previousWheelValue = m_currentWheelValue;
          m_currentWheelValue += delta;
        }

        base.WndProc(ref m);
      }

      private static int SignedHIWORD(IntPtr n)
      {
        return SignedHIWORD((int) ((long) n));
      }

      private static int SignedHIWORD(int n)
      {
        return (short) ((n >> 16) & 65535);
      }
    }
  }
}
