﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Application;
using Tesla.Input;
using RawInput = SharpDX.RawInput;
using SDX = SharpDX.Multimedia;

namespace Tesla.Windows.Input
{
  /// <summary>
  /// Mouse input system that acquires state data from RawInput.
  /// </summary>
  [OptionalDependency(typeof(IApplicationHost))]
  public sealed class RawMouseInputSystem : IMouseInputSystem, IDisposableEngineService, IEngineServiceDependencyChanged
  {
    private IntPtr m_handle;
    private POINT m_pos;
    private POINT m_screenPos;
    private ButtonState m_left;
    private ButtonState m_right;
    private ButtonState m_middle;
    private ButtonState m_x1;
    private ButtonState m_x2;
    private int m_scrollWheelValue;
    private int m_scrollWheelDelta;
    private InputFocusState m_focusState;
    private IApplicationHost? m_host;

    private bool m_isInputSink;
    private bool m_eventFired;
    private bool m_isDisposed;

    /// <inheritdoc />
    public String Name
    {
      get
      {
        return "RawMouseInputSystem";
      }
    }

    /// <inheritdoc />
    public InputFocusState FocusState
    {
      get
      {
        return m_focusState;
      }
      set
      {
        m_focusState = value;
      }
    }

    /// <inheritdoc />
    public IntPtr WindowHandle
    {
      get
      {
        return m_handle;
      }
      set
      {
        if (value != m_handle)
        {
          //Re-register
          RemoveDevice(false);

          m_handle = value;
          RegisterDevice(false);
        }
      }
    }

    /// <summary>
    /// Gets or sets if the window should receive input events even if it is not a foreground window.
    /// </summary>
    public bool IsInputSink
    {
      get
      {
        return m_isInputSink;
      }
      set
      {
        if (value != m_isInputSink)
        {
          //Re-register
          RemoveDevice(false);

          m_isInputSink = value;
          RegisterDevice(false);
        }
      }
    }

    /// <inheritdoc />
    public bool IsDisposed
    {
      get
      {
        return m_isDisposed;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="RawMouseInputSystem"/> class.
    /// </summary>
    public RawMouseInputSystem() : this(IntPtr.Zero, false) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="RawMouseInputSystem"/> class.
    /// </summary>
    /// <param name="handle">The handle of the window the cursor should be relative to.</param>
    public RawMouseInputSystem(IntPtr handle) : this(handle, false) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="RawMouseInputSystem"/> class.
    /// </summary>
    /// <param name="handle">The handle of the window the cursor should be relative to.</param>
    /// <param name="isInputSink">True if the window should receive input events even if it is not a foreground window, false otherwise.</param>
    public RawMouseInputSystem(IntPtr handle, bool isInputSink)
    {
      m_handle = handle;
      m_isInputSink = isInputSink;
      NativeMethods.GetCursorPos(out m_pos);
      m_screenPos = m_pos;

      if (m_handle != IntPtr.Zero)
        NativeMethods.ScreenToClient(m_handle, ref m_screenPos);

      m_left = ButtonState.Released;
      m_right = ButtonState.Released;
      m_middle = ButtonState.Released;
      m_x1 = ButtonState.Released;
      m_x2 = ButtonState.Released;
      m_scrollWheelDelta = 0;
      m_scrollWheelValue = 0;
      m_focusState = InputFocusState.None;
      m_eventFired = false;

      RegisterDevice(true);
    }

    /// <inheritdoc />
    public void GetMouseState(out MouseState state)
    {
      //Do the screen to client lazily, rather than every time we get a raw input event
      if (m_eventFired)
      {
        m_screenPos = m_pos;
        if (m_handle != IntPtr.Zero)
          NativeMethods.ScreenToClient(m_handle, ref m_screenPos);

        m_eventFired = false;
      }

      state = new MouseState(m_screenPos.X, m_screenPos.Y, m_scrollWheelValue, m_scrollWheelDelta, m_left, m_right, m_middle, m_x1, m_x2);
    }

    /// <inheritdoc />
    public void GetMouseState(IntPtr windowHandle, out MouseState state)
    {
      POINT pt = m_pos;
      if (windowHandle != IntPtr.Zero)
        NativeMethods.ScreenToClient(windowHandle, ref pt);

      state = new MouseState(pt.X, pt.Y, m_scrollWheelValue, m_scrollWheelDelta, m_left, m_right, m_middle, m_x1, m_x2);
    }

    /// <inheritdoc />
    public void SetPosition(int x, int y)
    {
      m_screenPos = new POINT(x, y);
      POINT pt = m_screenPos;

      if (m_handle != IntPtr.Zero)
      {
        NativeMethods.ClientToScreen(m_handle, ref pt);
        NativeMethods.SetCursorPos(pt.X, pt.Y);
        NativeMethods.GetCursorPos(out m_pos);
      }
    }

    /// <inheritdoc />
    public void Update(IGameTime time)
    {
      if (m_host is not null)
        m_focusState = m_host.DetermineMouseFocusState(m_handle);
    }

    /// <inheritdoc />
    public void Initialize(Engine engine)
    {
      m_host = engine.Services.GetService<IApplicationHost?>();
    }

    /// <inheritdoc />
    public void OnDependencyChanged(EngineServiceChangedEventArgs args)
    {
      if (args.IsService<IApplicationHost>())
        m_host = args.ServiceAs<IApplicationHost>();
    }

    /// <inheritdoc />
    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    private void Dispose(bool disposing)
    {
      if (!m_isDisposed)
      {
        if (disposing)
        {
          RemoveDevice(true);
        }

        m_isDisposed = true;
      }
    }

    private void RemoveDevice(bool applyEvent)
    {
      RawInput.Device.RegisterDevice(SDX.UsagePage.Generic, SDX.UsageId.GenericMouse, RawInput.DeviceFlags.Remove, m_handle, RawInput.RegisterDeviceOptions.NoFiltering);

      if (applyEvent)
        RawInput.Device.MouseInput -= Device_MouseInput;
    }

    private void RegisterDevice(bool applyEvent)
    {
      RawInput.DeviceFlags flags = (m_isInputSink) ? RawInput.DeviceFlags.InputSink : RawInput.DeviceFlags.None;
      RawInput.Device.RegisterDevice(SDX.UsagePage.Generic, SDX.UsageId.GenericMouse, flags, m_handle);

      if (applyEvent)
        RawInput.Device.MouseInput += Device_MouseInput;
    }

    private void Device_MouseInput(object? sender, RawInput.MouseInputEventArgs e)
    {
      RawInput.MouseButtonFlags flags = e.ButtonFlags;

      if ((flags & RawInput.MouseButtonFlags.LeftButtonDown) == RawInput.MouseButtonFlags.LeftButtonDown)
      {
        m_left = ButtonState.Pressed;
      }
      else if ((flags & RawInput.MouseButtonFlags.LeftButtonUp) == RawInput.MouseButtonFlags.LeftButtonUp)
      {
        m_left = ButtonState.Released;
      }

      if ((flags & RawInput.MouseButtonFlags.RightButtonDown) == RawInput.MouseButtonFlags.RightButtonDown)
      {
        m_right = ButtonState.Pressed;
      }
      else if ((flags & RawInput.MouseButtonFlags.RightButtonUp) == RawInput.MouseButtonFlags.RightButtonUp)
      {
        m_right = ButtonState.Released;
      }

      if ((flags & RawInput.MouseButtonFlags.MiddleButtonDown) == RawInput.MouseButtonFlags.MiddleButtonDown)
      {
        m_middle = ButtonState.Pressed;
      }
      else if ((flags & RawInput.MouseButtonFlags.MiddleButtonUp) == RawInput.MouseButtonFlags.MiddleButtonUp)
      {
        m_middle = ButtonState.Released;
      }

      if ((flags & RawInput.MouseButtonFlags.Button4Down) == RawInput.MouseButtonFlags.Button4Down)
      {
        m_x1 = ButtonState.Pressed;
      }
      else if ((flags & RawInput.MouseButtonFlags.Button4Up) == RawInput.MouseButtonFlags.Button4Up)
      {
        m_x1 = ButtonState.Released;
      }

      if ((flags & RawInput.MouseButtonFlags.Button5Down) == RawInput.MouseButtonFlags.Button5Down)
      {
        m_x2 = ButtonState.Pressed;
      }
      else if ((flags & RawInput.MouseButtonFlags.Button5Up) == RawInput.MouseButtonFlags.Button5Up)
      {
        m_x2 = ButtonState.Released;
      }

      m_scrollWheelDelta = e.WheelDelta;
      m_scrollWheelValue += e.WheelDelta;
      m_pos.X += e.X;
      m_pos.Y += e.Y;
      m_eventFired = true;
    }
  }
}
