﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Application;
using Tesla.Input;
using Tesla.Input.Utilities;
using RawInput = SharpDX.RawInput;
using SDX = SharpDX.Multimedia;

namespace Tesla.Windows.Input
{
  /// <summary>
  /// Keyboard input system that acquires state data from RawInput.
  /// </summary>
  [OptionalDependency(typeof(IApplicationHost))]
  public sealed class RawKeyboardInputSystem : IKeyboardInputSystem, IDisposableEngineService, IEngineServiceDependencyChanged
  {
    private IntPtr m_handle;
    private KeyboardStateBuilder m_stateBuilder;
    private IApplicationHost? m_host;
    private bool m_isInputSink;
    private bool m_isDisposed;

    /// <inheritdoc />
    public String Name
    {
      get
      {
        return "RawKeyboardInputSystem";
      }
    }

    /// <inheritdoc />
    public InputFocusState FocusState
    { 
      get
      {
        return m_stateBuilder.FocusState;
      }
      set
      {
        m_stateBuilder.FocusState = value;
      }
    }

    /// <summary>
    /// Gets the window that will receive raw input messages for the keyboard.
    /// </summary>
    public IntPtr WindowHandle
    {
      get
      {
        return m_handle;
      }
      set
      {
        if (value != m_handle)
        {
          //Re-register
          RemoveDevice(false);

          m_handle = value;
          RegisterDevice(false);
        }
      }
    }

    /// <summary>
    /// Gets or sets if the window should receive input events even if it is not a foreground window.
    /// </summary>
    public bool IsInputSink
    {
      get
      {
        return m_isInputSink;
      }
      set
      {
        if (value != m_isInputSink)
        {
          //Re-Register
          RemoveDevice(false);

          m_isInputSink = value;
          RegisterDevice(false);
        }
      }
    }

    /// <inheritdoc />
    public bool IsDisposed
    {
      get
      {
        return m_isDisposed;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="RawKeyboardInputSystem"/> class.
    /// </summary>
    public RawKeyboardInputSystem() : this(IntPtr.Zero, false) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="RawKeyboardInputSystem"/> class.
    /// </summary>
    /// <param name="handle">Handle to the target window that will receive events.</param>
    public RawKeyboardInputSystem(IntPtr handle) : this(handle, false) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="RawKeyboardInputSystem"/> class.
    /// </summary>
    /// <param name="handle">Handle to the target window that will receive events.</param>
    /// <param name="isInputSink">True if the window should receive input events even if it is not a foreground window, false otherwise.</param>
    public RawKeyboardInputSystem(IntPtr handle, bool isInputSink)
    {
      m_handle = handle;
      m_isDisposed = false;
      m_stateBuilder = new KeyboardStateBuilder();

      RegisterDevice(true);
    }

    /// <inheritdoc />
    public void GetKeyboardState(out KeyboardState state)
    {
      m_stateBuilder.ConstructState(out state);
    }

    /// <inheritdoc />
    public void Update(IGameTime time)
    {
      if (m_host is not null)
        m_stateBuilder.FocusState = m_host.DetermineKeyboardFocusState();
    }

    /// <inheritdoc />
    public void Initialize(Engine engine)
    {
      m_host = engine.Services.GetService<IApplicationHost?>();
    }

    /// <inheritdoc />
    public void OnDependencyChanged(EngineServiceChangedEventArgs args)
    {
      if (args.IsService<IApplicationHost>())
        m_host = args.ServiceAs<IApplicationHost>();
    }

    /// <inheritdoc />
    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    private void Dispose(bool disposing)
    {
      if (!m_isDisposed)
      {
        if (disposing)
          RemoveDevice(true);

        m_isDisposed = true;
      }
    }

    private void RemoveDevice(bool applyEvent)
    {
      RawInput.Device.RegisterDevice(SDX.UsagePage.Generic, SDX.UsageId.GenericKeyboard, RawInput.DeviceFlags.Remove, m_handle, RawInput.RegisterDeviceOptions.NoFiltering);

      if (applyEvent)
        RawInput.Device.KeyboardInput -= Device_KeyboardInput;
    }

    private void RegisterDevice(bool applyEvent)
    {
      RawInput.DeviceFlags flags = (m_isInputSink) ? RawInput.DeviceFlags.InputSink : RawInput.DeviceFlags.None;
      RawInput.Device.RegisterDevice(SDX.UsagePage.Generic, SDX.UsageId.GenericKeyboard, flags, m_handle);

      if (applyEvent)
        RawInput.Device.KeyboardInput += Device_KeyboardInput;
    }

    private void Device_KeyboardInput(object? sender, RawInput.KeyboardInputEventArgs e)
    {
      switch (e.State)
      {
        case RawInput.KeyState.KeyDown:
          m_stateBuilder.AddPressedKey((Keys) e.Key);
          break;
        case RawInput.KeyState.KeyUp:
          m_stateBuilder.RemovePressedKey((Keys) e.Key);
          break;
      }
    }
  }
}
