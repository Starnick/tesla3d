﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Windows.Forms;
using Tesla.Application;
using SD = System.Drawing;

namespace Tesla.Windows.Application
{
  /// <summary>
  /// Adapter for any WinForms control that will be used to render graphics to.
  /// </summary>
  public sealed class ControlAdapter : ISWFControlAdapter
  {
    private Control m_control;
    private Form? m_parentForm;
    private bool m_enableUserResize;
    private bool m_isMouseVisible;
    private bool m_mouseIsHidden;
    private FormWindowModeState m_windowModeState;

    /// <inheritdoc />
    public event EventHandler? GotFocus;

    /// <inheritdoc />
    public event EventHandler? LostFocus;

    /// <inheritdoc />
    public event EventHandler? ResumeRendering;

    /// <inheritdoc />
    public event EventHandler? SuspendRendering;

    /// <inheritdoc />
    public event EventHandler? Closed;

    /// <inheritdoc />
    public event EventHandler? Render;

    /// <inheritdoc />
    public event EventHandler? Resize;

    /// <inheritdoc />
    public event EventHandler? Disposed;

    /// <inheritdoc />
    public event KeyEventHandler? KeyDown;

    /// <inheritdoc />
    public event KeyPressEventHandler? KeyPress;

    /// <inheritdoc />
    public event KeyEventHandler? KeyUp;

    /// <inheritdoc />
    public event MouseEventHandler? MouseClick;

    /// <inheritdoc />
    public event MouseEventHandler? MouseDoubleClick;

    /// <inheritdoc />
    public event MouseEventHandler? MouseDown;

    /// <inheritdoc />
    public event MouseEventHandler? MouseUp;

    /// <inheritdoc />
    public event EventHandler? MouseEnter;

    /// <inheritdoc />
    public event EventHandler? MouseHover;

    /// <inheritdoc />
    public event EventHandler? MouseLeave;

    /// <inheritdoc />
    public event MouseEventHandler? MouseMove;

    /// <inheritdoc />
    public event MouseEventHandler? MouseWheel;

    /// <inheritdoc />
    public bool IsDisposed
    {
      get
      {
        return m_control is null || m_control.IsDisposed;
      }
    }

    /// <inheritdoc />
    public bool EnableUserResizing
    {
      get
      {
        return m_enableUserResize;
      }
      set
      {
        m_enableUserResize = value;
        m_windowModeState.ApplyBorder(m_parentForm, m_enableUserResize);
      }
    }

    /// <inheritdoc />
    public bool EnableInputEvents
    {
      get
      {
        return true;
      }
      set
      {
        //Do nothing, will always receive input events
      }
    }

    /// <inheritdoc />
    public bool EnablePaintEvents
    {
      get
      {
        return true;
      }
      set
      {
        //Do nothing, will always receive paint events
      }
    }

    /// <inheritdoc />
    public WindowMode WindowMode
    {
      get
      {
        return m_windowModeState.WindowMode;
      }
      set
      {
        m_windowModeState.SetWindowMode(value, m_parentForm, m_enableUserResize);
      }
    }

    /// <inheritdoc />
    public bool Focused
    {
      get
      {
        return m_control.Focused;
      }
    }

    /// <inheritdoc />
    Int2 ISWFControlAdapter.Location
    {
      get
      {
        if (m_parentForm is not null)
        {
          SD.Point pt = m_parentForm.Location;
          return new Int2(pt.X, pt.Y);
        }

        SD.Point ptC = m_control.Location;
        return new Int2(ptC.X, ptC.Y);
      }
      set
      {
        if (m_parentForm is not null)
          m_parentForm.Location = new SD.Point(value.X, value.Y);
      }
    }

    /// <inheritdoc />
    Int2 ISWFControlAdapter.ClientSize
    {
      get
      {
        SD.Size clientSize = m_control.ClientSize;
        return new Int2(clientSize.Width, clientSize.Height);
      }
      set
      {
        m_control.ClientSize = new SD.Size(value.X, value.Y);
      }
    }

    /// <inheritdoc />
    public Rectangle ClientBounds
    {
      get
      {
        SD.Size clientSize = m_control.ClientSize;
        return new Rectangle(0, 0, clientSize.Width, clientSize.Height);
      }
    }

    /// <inheritdoc />
    public Rectangle ScreenBounds
    {
      get
      {
        SD.Rectangle screenBounds = Screen.GetBounds(m_control);
        return new Rectangle(screenBounds.X, screenBounds.Y, screenBounds.Width, screenBounds.Height);
      }
    }

    /// <inheritdoc />
    public bool IsMouseVisible
    {
      get
      {
        return m_isMouseVisible;
      }
      set
      {
        if (m_isMouseVisible != value)
        {
          m_isMouseVisible = value;
          ToggleCursorVisible();
        }
      }
    }

    /// <inheritdoc />
    public bool IsMouseInsideWindow
    {
      get
      {
        return m_control.ClientRectangle.Contains(m_control.PointToClient(Cursor.Position));
      }
    }

    /// <inheritdoc />
    public bool IsMinimized
    {
      get
      {
        return m_control.ClientSize == SD.Size.Empty;
      }
    }

    /// <inheritdoc />
    public Control Control
    {
      get
      {
        return m_control;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="ControlAdapter"/> class.
    /// </summary>
    /// <param name="control">Control to adapt.</param>
    public ControlAdapter(Control control)
    {
      m_control = control;
      m_isMouseVisible = true;
      m_mouseIsHidden = false;
      m_enableUserResize = false;
      m_windowModeState = new FormWindowModeState();

      m_control.CausesValidation = false;
      m_parentForm = m_control.FindForm();

      StartEvents();
    }

    /// <summary>
    /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
    /// </summary>
    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    private void Dispose(bool isDisposing)
    {
      if (isDisposing)
      {
        EventHandler? disposeEvt = Disposed;
        if (disposeEvt is not null)
          disposeEvt(this, EventArgs.Empty);

        if (m_control is not null && !m_control.IsDisposed && !m_control.Disposing)
          StopEvents();
      }
    }

    private void ToggleCursorVisible()
    {
      if (m_isMouseVisible && m_mouseIsHidden)
      {
        Cursor.Show();
        m_mouseIsHidden = false;
      }
      else if (!m_isMouseVisible && !m_mouseIsHidden)
      {
        Cursor.Hide();
        m_mouseIsHidden = true;
      }
    }

    private void StartEvents()
    {
      if (m_control is null)
        return;

      AddParentOnResizeEvents();

      m_control.Disposed += Control_Disposed;
      m_control.ParentChanged += Control_ParentChanged;
      m_control.GotFocus += Control_GotFocus;
      m_control.LostFocus += Control_LostFocus;
      m_control.Paint += Control_Paint;
      m_control.Resize += Control_Resize;
      m_control.KeyDown += Control_KeyDown;
      m_control.KeyPress += Control_KeyPress;
      m_control.KeyUp += Control_KeyUp;
      m_control.MouseClick += Control_MouseClick;
      m_control.MouseDoubleClick += Control_MouseDoubleClick;
      m_control.MouseDown += Control_MouseDown;
      m_control.MouseUp += Control_MouseUp;
      m_control.MouseEnter += Control_MouseEnter;
      m_control.MouseHover += Control_MouseHover;
      m_control.MouseLeave += Control_MouseLeave;
      m_control.MouseMove += Control_MouseMove;
      m_control.MouseWheel += Control_MouseWheel;
    }

    private void StopEvents()
    {
      if (m_control is null)
        return;

      RemoveParentOnResizeEvents();

      m_control.Disposed -= Control_Disposed;
      m_control.ParentChanged -= Control_ParentChanged;
      m_control.GotFocus -= Control_GotFocus;
      m_control.LostFocus -= Control_LostFocus;
      m_control.Paint -= Control_Paint;
      m_control.Resize -= Control_Resize;
      m_control.KeyDown -= Control_KeyDown;
      m_control.KeyPress -= Control_KeyPress;
      m_control.KeyUp -= Control_KeyUp;
      m_control.MouseClick -= Control_MouseClick;
      m_control.MouseDoubleClick -= Control_MouseDoubleClick;
      m_control.MouseDown -= Control_MouseDown;
      m_control.MouseUp -= Control_MouseUp;
      m_control.MouseEnter -= Control_MouseEnter;
      m_control.MouseHover -= Control_MouseHover;
      m_control.MouseLeave -= Control_MouseLeave;
      m_control.MouseMove -= Control_MouseMove;
      m_control.MouseWheel -= Control_MouseWheel;
    }

    private void Control_Disposed(object? sender, EventArgs e)
    {
      StopEvents();
    }

    private void Control_ParentChanged(object? sender, EventArgs e)
    {
      RemoveParentOnResizeEvents();
      m_parentForm = m_control.FindForm();
      AddParentOnResizeEvents();

      m_windowModeState.ApplyWindowMode(m_parentForm, m_enableUserResize);
    }

    private void Control_GotFocus(object? sender, EventArgs e)
    {
      EventHandler? activate = GotFocus;
      if (activate is not null)
        activate(this, e);
    }

    private void Control_LostFocus(object? sender, EventArgs e)
    {
      EventHandler? deactivate = LostFocus;
      if (deactivate is not null)
        deactivate(this, e);
    }

    private void Control_Paint(object? sender, PaintEventArgs e)
    {
      EventHandler? render = Render;
      if (render is not null)
        render(this, e);
    }

    private void Control_Resize(object? sender, EventArgs e)
    {
      EventHandler? resize = Resize;
      if (resize is not null)
        resize(this, e);
    }

    private void Control_ResizeBegin(object? sender, EventArgs e)
    {
      EventHandler? suspend = SuspendRendering;
      if (suspend is not null)
        suspend(this, e);
    }

    private void Control_ResizeEnd(object? sender, EventArgs e)
    {
      EventHandler? resume = ResumeRendering;
      if (resume is not null)
        resume(this, e);
    }

    private void Control_FormClosed(object? sender, FormClosedEventArgs e)
    {
      EventHandler? closed = Closed;
      if (closed is not null)
        closed(this, e);
    }

    private void Control_KeyDown(object? sender, KeyEventArgs e)
    {
      KeyEventHandler? handler = KeyDown;
      if (handler is not null)
        handler(this, e);
    }

    private void Control_KeyPress(object? sender, KeyPressEventArgs e)
    {
      KeyPressEventHandler? handler = KeyPress;
      if (handler is not null)
        handler(this, e);
    }

    private void Control_KeyUp(object? sender, KeyEventArgs e)
    {
      KeyEventHandler? handler = KeyUp;
      if (handler is not null)
        handler(this, e);
    }

    private void Control_MouseClick(object? sender, MouseEventArgs e)
    {
      MouseEventHandler? handler = MouseClick;
      if (handler is not null)
        handler(this, e);
    }

    private void Control_MouseDoubleClick(object? sender, MouseEventArgs e)
    {
      MouseEventHandler? handler = MouseDoubleClick;
      if (handler is not null)
        handler(this, e);
    }

    private void Control_MouseDown(object? sender, MouseEventArgs e)
    {
      MouseEventHandler? handler = MouseDown;
      if (handler is not null)
        handler(this, e);
    }

    private void Control_MouseUp(object? sender, MouseEventArgs e)
    {
      MouseEventHandler? handler = MouseUp;
      if (handler is not null)
        handler(this, e);
    }

    private void Control_MouseEnter(object? sender, EventArgs e)
    {
      EventHandler? handler = MouseEnter;
      if (handler is not null)
        handler(this, e);

      ToggleCursorVisible();
    }

    private void Control_MouseHover(object? sender, EventArgs e)
    {
      EventHandler? handler = MouseHover;
      if (handler is not null)
        handler(this, e);
    }

    private void Control_MouseLeave(object? sender, EventArgs e)
    {
      EventHandler? handler = MouseLeave;
      if (handler is not null)
        handler(this, e);

      ToggleCursorVisible();
    }

    private void Control_MouseMove(object? sender, MouseEventArgs e)
    {
      MouseEventHandler? handler = MouseMove;
      if (handler is not null)
        handler(this, e);
    }

    private void Control_MouseWheel(object? sender, MouseEventArgs e)
    {
      MouseEventHandler? handler = MouseWheel;
      if (handler is not null)
        handler(this, e);
    }

    private void RemoveParentOnResizeEvents()
    {
      if (m_parentForm is not null)
      {
        m_parentForm.ResizeBegin -= Control_ResizeBegin;
        m_parentForm.ResizeEnd -= Control_ResizeEnd;
        m_parentForm.FormClosed -= Control_FormClosed;
      }
    }

    private void AddParentOnResizeEvents()
    {
      if (m_parentForm is not null)
      {
        m_parentForm.ResizeBegin += Control_ResizeBegin;
        m_parentForm.ResizeEnd += Control_ResizeEnd;
        m_parentForm.FormClosed += Control_FormClosed;
      }
    }
  }
}
