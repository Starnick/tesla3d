﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Runtime.InteropServices;
using System.Windows.Interop;
using SWF = System.Windows.Forms;
using Tesla.Application;

namespace Tesla.Windows.Application
{
  /// <summary>
  /// Specialized <see cref="HwndHost"/> for rendering graphics in a Windows Presentation Foundation (WPF) application by way of a
  /// hosted <see cref="RenderControl"/>.
  /// </summary>
  public sealed class RenderHwndHost : HwndHost, ISWFControlAdapter
  {
    private const int WS_CAPTION = 12582912;
    private const int WS_THICKFRAME = 262144;
    private const int WS_CHILD = 1073741824;

    private ISWFControlAdapter m_swfControl;
    private bool m_isDisposed;

    /// <inheritdoc />
    event EventHandler ISWFControlAdapter.GotFocus
    {
      add
      {
        m_swfControl.GotFocus += value;
      }
      remove
      {
        m_swfControl.GotFocus -= value;
      }
    }

    /// <inheritdoc />
    event EventHandler ISWFControlAdapter.LostFocus
    {
      add
      {
        m_swfControl.LostFocus += value;
      }
      remove
      {
        m_swfControl.LostFocus -= value;
      }
    }

    /// <inheritdoc />
    event EventHandler ISWFControlAdapter.ResumeRendering
    {
      add
      {
        m_swfControl.ResumeRendering += value;
      }
      remove
      {
        m_swfControl.ResumeRendering -= value;
      }
    }

    /// <inheritdoc />
    event EventHandler ISWFControlAdapter.SuspendRendering
    {
      add
      {
        m_swfControl.SuspendRendering += value;
      }
      remove
      {
        m_swfControl.SuspendRendering -= value;
      }
    }

    /// <inheritdoc />
    event EventHandler ISWFControlAdapter.Render
    {
      add
      {
        m_swfControl.Render += value;
      }
      remove
      {
        m_swfControl.Render -= value;
      }
    }

    /// <inheritdoc />
    event EventHandler ISWFControlAdapter.Resize
    {
      add
      {
        m_swfControl.Resize += value;
      }
      remove
      {
        m_swfControl.Resize -= value;
      }
    }

    /// <inheritdoc />
    event EventHandler ISWFControlAdapter.Closed
    {
      add
      {
        m_swfControl.Closed += value;
      }
      remove
      {
        m_swfControl.Closed -= value;
      }
    }

    /// <inheritdoc />
    event EventHandler ISWFControlAdapter.Disposed
    {
      add
      {
        m_swfControl.Disposed += value;
      }
      remove
      {
        m_swfControl.Disposed -= value;
      }
    }

    /// <inheritdoc />
    event SWF.KeyEventHandler ISWFControlAdapter.KeyDown
    {
      add
      {
        m_swfControl.KeyDown += value;
      }
      remove
      {
        m_swfControl.KeyDown -= value;
      }
    }

    /// <inheritdoc />
    event SWF.KeyPressEventHandler ISWFControlAdapter.KeyPress
    {
      add
      {
        m_swfControl.KeyPress += value;
      }
      remove
      {
        m_swfControl.KeyPress -= value;
      }
    }

    /// <inheritdoc />
    event SWF.KeyEventHandler ISWFControlAdapter.KeyUp
    {
      add
      {
        m_swfControl.KeyUp += value;
      }
      remove
      {
        m_swfControl.KeyUp -= value;
      }
    }

    /// <inheritdoc />
    event SWF.MouseEventHandler ISWFControlAdapter.MouseClick
    {
      add
      {
        m_swfControl.MouseClick += value;
      }
      remove
      {
        m_swfControl.MouseClick -= value;
      }
    }

    /// <inheritdoc />
    event SWF.MouseEventHandler ISWFControlAdapter.MouseDoubleClick
    {
      add
      {
        m_swfControl.MouseDoubleClick += value;
      }
      remove
      {
        m_swfControl.MouseDoubleClick -= value;
      }
    }

    /// <inheritdoc />
    event SWF.MouseEventHandler ISWFControlAdapter.MouseDown
    {
      add
      {
        m_swfControl.MouseDown += value;
      }
      remove
      {
        m_swfControl.MouseDown -= value;
      }
    }

    /// <inheritdoc />
    event SWF.MouseEventHandler ISWFControlAdapter.MouseUp
    {
      add
      {
        m_swfControl.MouseUp += value;
      }
      remove
      {
        m_swfControl.MouseUp -= value;
      }
    }

    /// <inheritdoc />
    event EventHandler ISWFControlAdapter.MouseEnter
    {
      add
      {
        m_swfControl.MouseEnter += value;
      }
      remove
      {
        m_swfControl.MouseEnter -= value;
      }
    }

    /// <inheritdoc />
    event EventHandler ISWFControlAdapter.MouseHover
    {
      add
      {
        m_swfControl.MouseHover += value;
      }
      remove
      {
        m_swfControl.MouseHover -= value;
      }
    }

    /// <inheritdoc />
    event EventHandler ISWFControlAdapter.MouseLeave
    {
      add
      {
        m_swfControl.MouseLeave += value;
      }
      remove
      {
        m_swfControl.MouseLeave -= value;
      }
    }

    /// <inheritdoc />
    event SWF.MouseEventHandler ISWFControlAdapter.MouseMove
    {
      add
      {
        m_swfControl.MouseMove += value;
      }
      remove
      {
        m_swfControl.MouseMove -= value;
      }
    }

    /// <inheritdoc />
    event SWF.MouseEventHandler ISWFControlAdapter.MouseWheel
    {
      add
      {
        m_swfControl.MouseWheel += value;
      }
      remove
      {
        m_swfControl.MouseWheel -= value;
      }
    }

    /// <inheritdoc />
    bool ISWFControlAdapter.IsDisposed
    {
      get
      {
        return m_isDisposed;
      }
    }

    /// <inheritdoc />
    bool ISWFControlAdapter.EnableUserResizing
    {
      get
      {
        return m_swfControl.EnableUserResizing;
      }
      set
      {
        m_swfControl.EnableUserResizing = value;
      }
    }

    /// <inheritdoc />
    bool ISWFControlAdapter.EnableInputEvents
    {
      get
      {
        return m_swfControl.EnableInputEvents;
      }
      set
      {
        m_swfControl.EnableInputEvents = value;
      }
    }

    /// <inheritdoc />
    bool ISWFControlAdapter.EnablePaintEvents
    {
      get
      {
        return m_swfControl.EnablePaintEvents;
      }
      set
      {
        m_swfControl.EnablePaintEvents = value;
      }
    }

    /// <inheritdoc />
    WindowMode ISWFControlAdapter.WindowMode
    {
      get
      {
        return m_swfControl.WindowMode;
      }
      set
      {
        m_swfControl.WindowMode = value;
      }
    }

    /// <inheritdoc />
    bool ISWFControlAdapter.Focused
    {
      get
      {
        return m_swfControl.Focused;
      }
    }

    /// <inheritdoc />
    Int2 ISWFControlAdapter.Location
    {
      get
      {
        return m_swfControl.Location;
      }
      set
      {
        m_swfControl.Location = value;
      }
    }

    /// <inheritdoc />
    Int2 ISWFControlAdapter.ClientSize
    {
      get
      {
        return m_swfControl.ClientSize;
      }
      set
      {
        m_swfControl.ClientSize = value;
      }
    }

    /// <inheritdoc />
    Rectangle ISWFControlAdapter.ClientBounds
    {
      get
      {
        return m_swfControl.ClientBounds;
      }
    }

    /// <inheritdoc />
    Rectangle ISWFControlAdapter.ScreenBounds
    {
      get
      {
        return m_swfControl.ScreenBounds;
      }
    }

    /// <inheritdoc />
    bool ISWFControlAdapter.IsMouseVisible
    {
      get
      {
        return m_swfControl.IsMouseVisible;
      }
      set
      {
        m_swfControl.IsMouseVisible = value;
      }
    }

    /// <inheritdoc />
    public bool IsMouseInsideWindow
    {
      get
      {
        return m_swfControl.IsMouseInsideWindow;
      }
    }

    /// <inheritdoc />
    bool ISWFControlAdapter.IsMinimized
    {
      get
      {
        return m_swfControl.IsMinimized;
      }
    }

    /// <inheritdoc />
    SWF.Control ISWFControlAdapter.Control
    {
      get
      {
        return m_swfControl.Control;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="RenderHwndHost"/> class.
    /// </summary>
    public RenderHwndHost() : this("Tesla3D RenderElement", 200, 100) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="RenderHwndHost"/> class.
    /// </summary>
    /// <param name="width">Width of the control.</param>
    /// <param name="height">Height of the control.</param>
    public RenderHwndHost(int width, int height) : this("Tesla3D RenderElement", width, height) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="RenderHwndHost"/> class.
    /// </summary>
    /// <param name="title">Title text of the control.</param>
    /// <param name="width">Width of the control.</param>
    /// <param name="height">Height of the control.</param>
    public RenderHwndHost(string title, int width, int height)
    {
      m_swfControl = CreateRenderControl(title, width, height);

      Focusable = true;
    }

    /// <summary>
    /// When overridden in a derived class, creates the window to be hosted.
    /// </summary>
    /// <param name="hwndParent">The window handle of the parent window.</param>
    /// <returns>The handle to the child Win32 window to create.</returns>
    protected override HandleRef BuildWindowCore(HandleRef hwndParent)
    {
      HandleRef childRef = new HandleRef(this, m_swfControl.Control.Handle);

      int style = NativeMethods.GetWindowLong(childRef, WindowLongType.Style).ToInt32();
      style = style & ~WS_CAPTION & ~WS_THICKFRAME;
      style |= WS_CHILD;

      NativeMethods.SetWindowLong(childRef, WindowLongType.Style, new IntPtr(style));
      NativeMethods.SetParent(childRef, hwndParent.Handle);
      NativeMethods.ShowWindow(childRef, false);

      return childRef;
    }

    /// <summary>
    /// When overridden in a derived class, destroys the hosted window.
    /// </summary>
    /// <param name="hwnd">A structure that contains the window handle.</param>
    protected override void DestroyWindowCore(HandleRef hwnd)
    {
      NativeMethods.SetParent(hwnd, IntPtr.Zero);
    }

    /// <summary>
    /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Integration.WindowsFormsHost" />, and optionally releases the managed resources.
    /// </summary>
    /// <param name="disposing">True to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
    protected override void Dispose(bool disposing)
    {
      base.Dispose(disposing);

      if (!m_isDisposed && disposing)
        m_swfControl.Dispose();

      m_isDisposed = true;
    }

    private ISWFControlAdapter CreateRenderControl(string title, int width, int height)
    {
      RenderControl rc = new RenderControl(title, width, height);
      rc.EnableInputEvents = true;
      rc.EnablePaintEvents = true;
      rc.EnableUserResizing = true;
      rc.Dock = SWF.DockStyle.Fill;

      return rc;
    }
  }
}
