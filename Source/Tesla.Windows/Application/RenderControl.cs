﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Windows.Forms;
using Tesla.Application;
using SD = System.Drawing;

namespace Tesla.Windows.Application
{
  /// <summary>
  /// Specialized <see cref="UserControl"/> for rendering graphics in a WinForms application.
  /// </summary>
  public class RenderControl : UserControl, ISWFControlAdapter
  {
    private Form? m_parentForm;
    private bool m_enableUserResize;
    private bool m_enableInputEvents;
    private bool m_enablePaintEvents;
    private bool m_isMouseVisible;
    private bool m_mouseIsHidden;
    private bool m_mouseEntered;
    private FormWindowModeState m_windowModeState;

    private SD.Font? m_fontForDesignMode;

    /// <inheritdoc />
    public event EventHandler? ResumeRendering;

    /// <inheritdoc />
    public event EventHandler? SuspendRendering;

    /// <inheritdoc />
    public event EventHandler? Closed;

    /// <inheritdoc />
    public event EventHandler? Render;

    /// <inheritdoc />
    public bool EnableUserResizing
    {
      get
      {
        return m_enableUserResize;
      }
      set
      {
        m_enableUserResize = value;
        m_windowModeState.ApplyBorder(m_parentForm, m_enableUserResize);
      }
    }

    /// <inheritdoc />
    public bool EnableInputEvents
    {
      get
      {
        return m_enableInputEvents;
      }
      set
      {
        m_enableInputEvents = value;
      }
    }

    /// <inheritdoc />
    public bool EnablePaintEvents
    {
      get
      {
        return m_enablePaintEvents;
      }
      set
      {
        m_enablePaintEvents = value;
      }
    }

    /// <inheritdoc />
    public WindowMode WindowMode
    {
      get
      {
        return m_windowModeState.WindowMode;
      }
      set
      {
        m_windowModeState.SetWindowMode(value, m_parentForm, m_enableUserResize);
      }
    }

    /// <inheritdoc />
    Int2 ISWFControlAdapter.Location
    {
      get
      {
        if (m_parentForm is not null)
        {
          SD.Point pt = m_parentForm.Location;
          return new Int2(pt.X, pt.Y);
        }

        SD.Point ptC = base.Location;
        return new Int2(ptC.X, ptC.Y);
      }
      set
      {
        if (m_parentForm is not null)
          m_parentForm.Location = new SD.Point(value.X, value.Y);
      }
    }

    /// <inheritdoc />
    Int2 ISWFControlAdapter.ClientSize
    {
      get
      {
        SD.Size clientSize = ClientSize;
        return new Int2(clientSize.Width, clientSize.Height);
      }
      set
      {
        base.ClientSize = new SD.Size(value.X, value.Y);
      }
    }

    /// <inheritdoc />
    public Rectangle ClientBounds
    {
      get
      {
        SD.Size clientSize = ClientSize;
        return new Rectangle(0, 0, clientSize.Width, clientSize.Height);
      }
    }

    /// <inheritdoc />
    public Rectangle ScreenBounds
    {
      get
      {
        SD.Rectangle screenBounds = Screen.GetBounds(this);
        return new Rectangle(screenBounds.X, screenBounds.Y, screenBounds.Width, screenBounds.Height);
      }
    }

    /// <inheritdoc />
    public bool IsMouseVisible
    {
      get
      {
        return m_isMouseVisible;
      }
      set
      {
        if (m_isMouseVisible != value)
        {
          m_isMouseVisible = value;
          ToggleCursorVisible();
        }
      }
    }

    /// <inheritdoc />
    public bool IsMouseInsideWindow
    {
      get
      {
        return ClientRectangle.Contains(PointToClient(Cursor.Position));
      }
    }

    /// <inheritdoc />
    public bool IsMinimized
    {
      get
      {
        return ClientSize == SD.Size.Empty;
      }
    }

    /// <inheritdoc />
    public Control Control
    {
      get
      {
        return this;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="RenderControl"/> class.
    /// </summary>
    public RenderControl() : this("Tesla3D RenderControl", 200, 100) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="RenderControl"/> class.
    /// </summary>
    /// <param name="width">Width of the control.</param>
    /// <param name="height">Height of the control.</param>
    public RenderControl(int width, int height) : this("Tesla3D RenderControl", width, height) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="RenderControl"/> class.
    /// </summary>
    /// <param name="title">Title text of the control.</param>
    /// <param name="width">Width of the control.</param>
    /// <param name="height">Height of the control.</param>
    public RenderControl(string title, int width, int height)
    {
      m_isMouseVisible = true;
      m_mouseIsHidden = false;
      m_enableInputEvents = false;
      m_enablePaintEvents = false;
      m_windowModeState = new FormWindowModeState();

      SuspendLayout();

      SetStyle(ControlStyles.ContainerControl, false);
      SetStyle(ControlStyles.Selectable, true);

      EnableUserResizing = true;
      ClientSize = new SD.Size(width, height);
      Text = title;
      SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint | ControlStyles.Opaque, true);
      CausesValidation = false;
      ResizeRedraw = true;
      AutoScaleMode = AutoScaleMode.None;

      ResumeLayout();
    }

    /// <inheritdoc />
    protected override void OnParentChanged(EventArgs e)
    {
      base.OnParentChanged(e);

      RemoveParentOnResizeEvents();
      m_parentForm = FindForm();
      AddParentOnResizeEvents();

      m_windowModeState.ApplyWindowMode(m_parentForm, m_enableUserResize);
    }

    /// <inheritdoc />
    protected override void OnMouseEnter(EventArgs e)
    {
      m_mouseEntered = true;

      if (m_enableInputEvents)
        base.OnMouseEnter(e);

      ToggleCursorVisible();
    }

    /// <inheritdoc />
    protected override void OnMouseLeave(EventArgs e)
    {
      m_mouseEntered = false;

      if (m_enableInputEvents)
        base.OnMouseLeave(e);

      ToggleCursorVisible();
    }

    /// <summary>
    /// Raises the <see cref="E:ResumeRendering" /> event.
    /// </summary>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected virtual void OnResumeRendering(EventArgs e)
    {
      EventHandler? resume = ResumeRendering;
      if (resume is not null)
        resume(this, e);
    }

    /// <summary>
    /// Raises the <see cref="E:SuspendRendering" /> event.
    /// </summary>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected virtual void OnSuspendRendering(EventArgs e)
    {
      EventHandler? suspend = SuspendRendering;
      if (suspend is not null)
        suspend(this, e);
    }

    /// <summary>
    /// Raises the <see cref="E:Render" /> event.
    /// </summary>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected virtual void OnRender(EventArgs e)
    {
      EventHandler? render = Render;
      if (render is not null)
        render(this, e);
    }

    /// <summary>
    /// Raises the <see cref="E:Closed" /> event.
    /// </summary>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected virtual void OnClosed(EventArgs e)
    {
      EventHandler? closed = Closed;
      if (closed is not null)
        closed(this, e);
    }

    /// <inheritdoc />
    protected override void OnPaint(PaintEventArgs e)
    {
      base.OnPaint(e);

      if (DesignMode)
      {
        if (m_fontForDesignMode is null)
          m_fontForDesignMode = new SD.Font("Calibri", 24f, SD.FontStyle.Regular);

        e.Graphics.Clear(SD.Color.CornflowerBlue);
        string text = "Tesla3D RenderControl";
        SD.SizeF size = e.Graphics.MeasureString(text, m_fontForDesignMode);
        SD.PointF pt = new SD.PointF();
        pt.X = ((float) (base.Width - size.Width)) / 2.0f;
        pt.Y = ((float) (base.Height - size.Height)) / 2.0f;
        e.Graphics.DrawString(text, m_fontForDesignMode, new SD.SolidBrush(SD.Color.Black), pt.X, pt.Y);
      }
      else if (m_enablePaintEvents)
      {
        OnRender(e);
      }
    }

    /// <inheritdoc />
    protected override void OnPaintBackground(PaintEventArgs pevent)
    {
      if (DesignMode)
        base.OnPaintBackground(pevent);
    }

    /// <inheritdoc />
    protected override void WndProc(ref System.Windows.Forms.Message m)
    {
      //Regardless if we're handling input, if mouse entered and any button was clicked, make sure this gives us focus
      switch (m.Msg)
      {
        //Left mouse button
        case 513:
        case 514:
        case 515:

        //Right mouse button
        case 516:
        case 517:
        case 518:

        //Middle mouse button
        case 519:
        case 520:
        case 521:

        //Mouse xbuttons
        case 523:
        case 524:
        case 525:
          {
            if (m_mouseEntered && !Focused)
              Focus();

            if (!m_enableInputEvents)
              return;
          }
          break;
      }

      if (!m_enableInputEvents)
      {
        switch (m.Msg)
        {
          //Keys
          case 256:
          case 257:
          case 258:
          case 260:
          case 261:

          //Mouse move
          case 512:

          //Mouse wheel
          case 522:

          //Mouse hover
          case 673:
            return;
        }
      }

      //Try and handle certain WM messages if we don't have a parent form (e.g. the control is a child of a native Win32 window that doesn't have
      //a .NET equivalent
      if (m_parentForm is null)
      {
        switch (m.Msg)
        {
          //WM_SIZE
          case 5:
            SetClientSizeFromParent();
            OnResize(EventArgs.Empty);
            break;
          //WM_ENTERSIZEMOVE
          case 561:
            OnSuspendRendering(EventArgs.Empty);
            break;
          //WM_EXITSIZEMOVE
          case 562:
            OnResumeRendering(EventArgs.Empty);
            break;
          //WM_DESTROY
          case 2:
            OnClosed(EventArgs.Empty);
            break;
        }
      }

      base.WndProc(ref m);
    }

    private void SetClientSizeFromParent()
    {
      IntPtr parentHwnd = NativeMethods.GetParent(Handle);
      if (parentHwnd == IntPtr.Zero)
        return;

      RECT clientRect;
      NativeMethods.GetClientRect(parentHwnd, out clientRect);
      ClientSize = new SD.Size(clientRect.Right, clientRect.Bottom);
    }
    private void ToggleCursorVisible()
    {
      if (m_isMouseVisible && m_mouseIsHidden)
      {
        Cursor.Show();
        m_mouseIsHidden = false;
      }
      else if (!m_isMouseVisible && !m_mouseIsHidden)
      {
        Cursor.Hide();
        m_mouseIsHidden = true;
      }
    }

    private void RemoveParentOnResizeEvents()
    {
      if (m_parentForm is not null)
      {
        m_parentForm.ResizeBegin -= RenderControl_ResizeBegin;
        m_parentForm.ResizeEnd -= RenderControl_ResizeEnd;
        m_parentForm.FormClosed -= RenderControl_FormClosed;
      }
    }

    private void AddParentOnResizeEvents()
    {
      if (m_parentForm is not null)
      {
        m_parentForm.ResizeBegin += RenderControl_ResizeBegin;
        m_parentForm.ResizeEnd += RenderControl_ResizeEnd;
        m_parentForm.FormClosed += RenderControl_FormClosed;
      }
    }

    private void RenderControl_ResizeBegin(object? sender, EventArgs e)
    {
      OnSuspendRendering(e);
    }

    private void RenderControl_ResizeEnd(object? sender, EventArgs e)
    {
      OnResumeRendering(e);
    }

    private void RenderControl_FormClosed(object? sender, FormClosedEventArgs e)
    {
      OnClosed(e);
    }
  }
}
