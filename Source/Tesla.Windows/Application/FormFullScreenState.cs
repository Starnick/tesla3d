﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System.Windows.Forms;
using Tesla.Application;

namespace Tesla.Windows.Application
{
  /// <summary>
  /// Encapsulates state and logic for changing form styles between windowed/fullscreen (borderless). Full screen exclusive state changing
  /// is handled by the swap chain.
  /// </summary>
  public class FormWindowModeState
  {
    private WindowMode m_mode;
    private bool m_wantMaximized;

    /// <summary>
    /// Gets the window mode.
    /// </summary>
    public WindowMode WindowMode { get { return m_mode; } }

    public FormWindowModeState()
    {
      m_mode = WindowMode.Normal;
      m_wantMaximized = false;
    }

    /// <summary>
    /// Sets the window mode. If the mode is unchanged, no effect.
    /// </summary>
    /// <param name="mode">Mode to change to.</param>
    /// <param name="f">Form, which may or may not actually exist.</param>
    /// <param name="enableUserResize">Hint for if the form is user resizeable, e.g. a border that can be dragged.</param>
    public void SetWindowMode(WindowMode mode, Form? f, bool enableUserResize)
    {
      if (m_mode == mode)
        return;

      m_mode = mode;
      if (f is not null)
        ApplyWindowMode(f, enableUserResize);
    }

    /// <summary>
    /// Applies the stored state to the form.
    /// </summary>
    /// <param name="f">Form.</param>
    /// <param name="enableUserResize">Hint for if the form is user resizeable, e.g. a border that can be dragged.</param>
    public void ApplyWindowMode(Form? f, bool enableUserResize)
    {
      if (f is null)
        return;

      switch (m_mode)
      {
        case WindowMode.Normal:
          if (m_wantMaximized)
            f.WindowState = FormWindowState.Maximized;
          else
            f.WindowState = FormWindowState.Normal;

          m_wantMaximized = false;
          ApplyBorder(f, enableUserResize);
          break;
        case WindowMode.FullScreenBorderless:
          // Seem to need to put it borderless first, else taskbar is visible
          f.FormBorderStyle = FormBorderStyle.None;

          // If already maximized, seems like we need to force it normal first, otherwise the task bar remains visible
          if (f.WindowState == FormWindowState.Maximized)
          {
            f.WindowState = FormWindowState.Normal;

            // When we go back to normal, make sure we preserve the user-maximized window
            m_wantMaximized = true;
          }

          f.WindowState = FormWindowState.Maximized;
          break;
      }
    }

    /// <summary>
    /// Applies border changes to the form, but only if the mode is <see cref="WindowMode.Normal"/> (e.g. windowed).
    /// </summary>
    /// <param name="f">Form.</param>
    /// <param name="enableUserResize">Hint for if the form is user resizeable, e.g. a border that can be dragged.</param>
    public void ApplyBorder(Form? f, bool enableUserResize)
    {
      if (f is null || m_mode != WindowMode.Normal)
        return;

      f.FormBorderStyle = (enableUserResize) ? FormBorderStyle.Sizable : FormBorderStyle.FixedSingle;
    }
  }
}
