﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tesla.Application;
using Tesla.Graphics;
using Tesla.Input;

namespace Tesla.Windows.Application
{
  /// <summary>
  /// Application host implementation for Windows Forms (aka System.Windows.Forms).
  /// </summary>
  public sealed class SWFApplicationHost : IApplicationHost
  {
    private bool m_isDisposed;
    private Thread? m_thread;
    private IWindow? m_mainWindow;
    private List<IWindow> m_windowCollection;
    private WindowCollection m_readOnlyWindowCollection;
    private ConcurrentQueue<InvokeOperation> m_invokeQueue;
    private bool m_isRunning;
    private bool m_shutdownRequested;

    /// <inheritdoc />
    public event TypedEventHandler<IApplicationHost>? ShuttingDown;

    /// <inheritdoc />
    public bool IsDisposed { get { return m_isDisposed; } }

    /// <inheritdoc />
    public string Platform { get { return "WindowsForms"; } }

    /// <inheritdoc />
    public string Name { get { return "WindowsForms_ApplicationHost"; } }

    /// <inheritdoc />
    public bool IsRunning { get { return m_isRunning; } }

    /// <inheritdoc />
    public Thread Thread { get { return m_thread ?? Thread.CurrentThread; } }

    /// <inheritdoc />
    public IWindow? MainWindow { get { return m_mainWindow; } }

    /// <inheritdoc />
    public WindowCollection Windows { get { return m_readOnlyWindowCollection; } }

    /// <summary>
    /// Constructs a new instance of <see cref="SWFApplicationHost"/>.
    /// </summary>
    public SWFApplicationHost()
    {
      m_windowCollection = new List<IWindow>();
      m_readOnlyWindowCollection = new WindowCollection(m_windowCollection);
      m_isDisposed = false;
      m_isRunning = false;
      m_shutdownRequested = false;
      m_invokeQueue = new ConcurrentQueue<InvokeOperation>();
    }

    /// <inheritdoc />
    public IWindow CreateWindow(PresentationParameters presentParams, bool isTopLevelWindow)
    {
      SWFWindow swfWindow = new SWFWindow(this, presentParams, isTopLevelWindow);
      StartWindowEvents(swfWindow);
      m_windowCollection.Add(swfWindow);
      return swfWindow;
    }

    /// <inheritdoc />
    /// <exception cref="ArgumentException">Thrown if the native window type is unknown and cannot be wrapped.</exception>
    public IWindow FromNativeWindow(object nativeWindow, PresentationParameters? templatePresentationParameters, bool resizeNativeWindow)
    {
      SWFWindow? swfWindow = null;

      if (nativeWindow is Control control)
      {
        swfWindow = new SWFWindow(this, control, templatePresentationParameters, resizeNativeWindow);
      }
      else if (nativeWindow is ISWFControlAdapter adapter)
      {
        swfWindow = new SWFWindow(this, adapter, templatePresentationParameters, resizeNativeWindow);
      }

      if (swfWindow is null)
        throw new ArgumentException(StringLocalizer.Instance.GetLocalizedString("UnknownNativeWindowType"));

      StartWindowEvents(swfWindow);
      m_windowCollection.Add(swfWindow);

      return swfWindow;
    }

    /// <inheritdoc />
    public void Initialize(Engine engine)
    {
      // No-op
    }

    /// <inheritdoc />
    /// <exception cref="ArgumentNullException">Thrown if the action is null.</exception>
    public ValueTask Invoke(Action action)
    {
      ArgumentNullException.ThrowIfNull(action, nameof(action));

      if (m_thread == Thread.CurrentThread)
      {
        action();
        return ValueTask.CompletedTask;
      }

      return new ValueTask(Queue(action));
    }

    /// <inheritdoc />
    /// <exception cref="ArgumentNullException">Thrown if the action is null.</exception>
    public ValueTask<TResult> Invoke<TResult>(Func<TResult> action)
    {
      ArgumentNullException.ThrowIfNull(action, nameof(action));

      if (m_thread == Thread.CurrentThread)
        return ValueTask.FromResult<TResult>(action());

      return new ValueTask<TResult>(Queue<TResult>(action));
    }

    /// <inheritdoc />
    /// <exception cref="ArgumentNullException">Thrown if the action is null.</exception>
    public Task Queue(Action action)
    {
      ArgumentNullException.ThrowIfNull(action, nameof(action));

      InvokeOperationAction operation = new InvokeOperationAction(action);
      m_invokeQueue.Enqueue(operation);

      return operation.GetTask();
    }

    /// <inheritdoc />
    /// <exception cref="ArgumentNullException">Thrown if the action is null.</exception>
    public Task<TResult> Queue<TResult>(Func<TResult> action)
    {
      ArgumentNullException.ThrowIfNull(action, nameof(action));

      InvokeOperationFunc<TResult> operation = new InvokeOperationFunc<TResult>(action);
      m_invokeQueue.Enqueue(operation);

      return operation.GetTaskWithResult();
    }

    /// <inheritdoc />
    /// <exception cref="ArgumentNullException">Thrown if the onIdle callback is null or if the main window was not created from this host.</exception>
    /// <exception cref="ArgumentException">Thrown if the host is already running.</exception>
    /// <exception cref="InvalidOperationException">Thrown if an invalid operation happened during message processing in the event loop.</exception>
    public void Run(Action onIdleCallback, IWindow? mainWindow = null)
    {
      if (onIdleCallback is null)
        throw new ArgumentNullException(nameof(onIdleCallback), StringLocalizer.Instance.GetLocalizedString("AppLoopMustNotBeNull"));

      if (mainWindow is not null && mainWindow.Host != this)
        throw new ArgumentNullException(nameof(mainWindow), StringLocalizer.Instance.GetLocalizedString("WindowNotOwnedByHost"));

      if (m_isRunning || m_shutdownRequested)
        throw new ArgumentException(StringLocalizer.Instance.GetLocalizedString("AppHostAlreadyRunning"));

      m_thread = Thread.CurrentThread;
      m_mainWindow = mainWindow;

      // Set the mouse's window handle to that of the main window, if we have one
      if (m_mainWindow is not null)
        Mouse.WindowHandle = m_mainWindow.Handle;

      m_isRunning = true;

      while (m_isRunning)
      {
        Message msg;
        while (NativeMethods.PeekMessage(out msg, IntPtr.Zero, 0, 0, 0) != 0)
        {
          if (!m_isRunning)
            break;

          if (NativeMethods.GetMessage(out msg, IntPtr.Zero, 0, 0) == -1)
          {
            throw new InvalidOperationException(String.Format(CultureInfo.InvariantCulture,
                "Error occured while processing window messages: {0}", Marshal.GetLastWin32Error()));
          }

          //NCDESTROY event
          if (msg.msg == 130)
            m_isRunning = false;

          System.Windows.Forms.Message swfMsg = new System.Windows.Forms.Message()
          {
            HWnd = msg.hWnd,
            LParam = msg.lParam,
            Msg = (int) msg.msg,
            WParam = msg.wParam
          };

          if (!System.Windows.Forms.Application.FilterMessage(ref swfMsg))
          {
            NativeMethods.TranslateMessage(ref msg);
            NativeMethods.DispatchMessage(ref msg);
          }
        }

        if (m_isRunning)
        {
          InvokeCallbackQueue();

          // In case anyone requested a shutdown in the callback queue
          if (m_isRunning)
            onIdleCallback();
        }
      }

      // Cleanup state, sets the host back to a ready-mode
      ShuttingDown?.Invoke(this, EventArgs.Empty);

      m_shutdownRequested = false;
      m_isRunning = false;
      m_mainWindow = null;
      m_invokeQueue.Clear();
      DisposeAllWindows();
    }

    /// <inheritdoc />
    public void Shutdown()
    {
      // Mark no longer running, but guard against another call to Run() to say we're in the process of shutting down
      m_shutdownRequested = true;
      m_isRunning = false;
    }

    /// <inheritdoc />
    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    private void Dispose(bool disposing)
    {
      if (!m_isDisposed)
      {
        if (disposing)
          DisposeAllWindows();

        m_isDisposed = true;
      }
    }

    private void DisposeAllWindows()
    {
      foreach(IWindow window in m_windowCollection)
      {
        StopWindowEvents(window);
        window.Close();
        window.Dispose();
      }

      m_windowCollection.Clear();
      m_mainWindow = null;
    }

    private void StartWindowEvents(IWindow window)
    {
      // This seems to be redundant, but there may be some cases where our wrapper closes but doesn't dispose? Maybe just to be safe...
      window.Closed += HandleWindowClosed;
      window.Disposed += HandleWindowDisposed;
    }

    private void StopWindowEvents(IWindow window)
    {
      window.Closed -= HandleWindowClosed;
      window.Disposed -= HandleWindowDisposed;
    }

    private void HandleWindowClosed(IWindow sender, EventArgs args)
    {
      if (m_mainWindow == sender)
        Shutdown();
    }

    private void HandleWindowDisposed(IWindow sender, EventArgs args)
    {
      if (sender is null)
        return;

      m_windowCollection.Remove(sender);

      if (m_mainWindow == sender)
      {
        m_mainWindow = null;
        Shutdown();
      }
    }

    private void InvokeCallbackQueue()
    {
      while (m_invokeQueue.TryDequeue(out InvokeOperation? operation))
      {
        // Early out if requested to shut down
        if (!m_isRunning)
          return;

        operation?.Invoke();
      }
    }

    #region InvokeOperation

    private abstract class InvokeOperation
    {
      public abstract Task GetTask();
      public abstract void Invoke();
    }

    private sealed class InvokeOperationAction : InvokeOperation
    {
      private Action m_action;
      private TaskCompletionSource<object?> m_taskSource;

      public InvokeOperationAction(Action action)
      {
        m_action = action;
        m_taskSource = new TaskCompletionSource<object?>();
      }

      public override Task GetTask()
      {
        return m_taskSource.Task;
      }

      public override void Invoke()
      {
        try
        {
          m_action();
          m_taskSource.SetResult(null);
        }
        catch (Exception e)
        {
          m_taskSource.SetException(e);
        }
      }
    }

    private sealed class InvokeOperationFunc<TResult> : InvokeOperation
    {
      private Func<TResult> m_action;
      private TaskCompletionSource<TResult> m_taskSource;

      public InvokeOperationFunc(Func<TResult> func)
      {
        m_action = func;
        m_taskSource = new TaskCompletionSource<TResult>();
      }

      public override Task GetTask()
      {
        return m_taskSource.Task;
      }

      public Task<TResult> GetTaskWithResult()
      {
        return m_taskSource.Task;
      }

      public override void Invoke()
      {
        try
        {
          TResult result = m_action();
          m_taskSource.SetResult(result);
        }
        catch (Exception e)
        {
          m_taskSource.SetException(e);
        }
      }
    }

    #endregion
  }
}
