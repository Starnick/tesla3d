﻿/*
* Copyright (c) 2010-2023 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Windows.Forms;
using Tesla.Application;
using SD = System.Drawing;

namespace Tesla.Windows.Application
{
  /// <summary>
  /// Specialized <see cref="Form" /> for rendering graphics in a WinForms application.
  /// </summary>
  public class RenderForm : Form, ISWFControlAdapter
  {
    private bool m_enableUserResize;
    private bool m_enableInputEvents;
    private bool m_enablePaintEvents;
    private bool m_isMouseVisible;
    private bool m_mouseIsHidden;
    private FormWindowModeState m_windowModeState;

    private SD.Font? m_fontForDesignMode;

    /// <inheritdoc />
    public event EventHandler? ResumeRendering;

    /// <inheritdoc />
    public event EventHandler? SuspendRendering;

    /// <inheritdoc />
    public event EventHandler? Render;

    /// <inheritdoc />
    public bool EnableUserResizing
    {
      get
      {
        return m_enableUserResize;
      }
      set
      {
        m_enableUserResize = value;
        m_windowModeState.ApplyBorder(this, m_enableUserResize);
      }
    }

    /// <inheritdoc />
    public bool EnableInputEvents
    {
      get
      {
        return m_enableInputEvents;
      }
      set
      {
        m_enableInputEvents = value;
      }
    }

    /// <inheritdoc />
    public bool EnablePaintEvents
    {
      get
      {
        return m_enablePaintEvents;
      }
      set
      {
        m_enablePaintEvents = value;
      }
    }

    /// <inheritdoc />
    public WindowMode WindowMode
    {
      get
      {
        return m_windowModeState.WindowMode;
      }
      set
      {
        m_windowModeState.SetWindowMode(value, this, m_enableUserResize);
      }
    }

    /// <inheritdoc />
    Int2 ISWFControlAdapter.Location
    {
      get
      {
        SD.Point ptC = base.Location;
        return new Int2(ptC.X, ptC.Y);
      }
      set
      {
        base.Location = new SD.Point(value.X, value.Y);
      }
    }

    /// <inheritdoc />
    Int2 ISWFControlAdapter.ClientSize
    {
      get
      {
        SD.Size clientSize = ClientSize;
        return new Int2(clientSize.Width, clientSize.Height);
      }
      set
      {
        base.ClientSize = new SD.Size(value.X, value.Y);
      }
    }

    /// <inheritdoc />
    public Rectangle ClientBounds
    {
      get
      {
        SD.Size clientSize = ClientSize;
        return new Rectangle(0, 0, clientSize.Width, clientSize.Height);
      }
    }

    /// <inheritdoc />
    public Rectangle ScreenBounds
    {
      get
      {
        SD.Rectangle screenBounds = Screen.GetBounds(this);
        return new Rectangle(screenBounds.X, screenBounds.Y, screenBounds.Width, screenBounds.Height);
      }
    }

    /// <inheritdoc />
    public bool IsMouseVisible
    {
      get
      {
        return m_isMouseVisible;
      }
      set
      {
        if (m_isMouseVisible != value)
        {
          m_isMouseVisible = value;
          ToggleCursorVisible();
        }
      }
    }

    /// <inheritdoc />
    public bool IsMouseInsideWindow
    {
      get
      {
        return ClientRectangle.Contains(PointToClient(Cursor.Position));
      }
    }

    /// <inheritdoc />
    public bool IsMinimized
    {
      get
      {
        return ClientSize == SD.Size.Empty;
      }
    }

    /// <inheritdoc />
    public Control Control
    {
      get
      {
        return this;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="RenderForm"/> class.
    /// </summary>
    public RenderForm() : this("Tesla3D RenderForm", 300, 300) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="RenderForm"/> class.
    /// </summary>
    /// <param name="width">Width of the control.</param>
    /// <param name="height">Height of the control.</param>
    public RenderForm(int width, int height) : this("Tesla3D RenderForm", width, height) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="RenderForm"/> class.
    /// </summary>
    /// <param name="title">Title text of the control.</param>
    /// <param name="width">Width of the control.</param>
    /// <param name="height">Height of the control.</param>
    public RenderForm(string title, int width, int height)
    {
      m_isMouseVisible = true;
      m_mouseIsHidden = false;
      m_enableInputEvents = false;
      m_enablePaintEvents = false;
      m_windowModeState = new FormWindowModeState();

      SuspendLayout();

      EnableUserResizing = true;
      ClientSize = new SD.Size(width, height);
      StartPosition = FormStartPosition.CenterScreen;
      Text = title;
      SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint | ControlStyles.Opaque, true);
      CausesValidation = false;
      AutoScaleMode = AutoScaleMode.None;
      ResizeRedraw = true;

      ResumeLayout();
    }

    /// <summary>
    /// Raises the <see cref="E:System.Windows.Forms.Control.MouseEnter" /> event.
    /// </summary>
    /// <param name="e">An <see cref="T:System.EventArgs" /> that contains the event data.</param>
    protected override void OnMouseEnter(EventArgs e)
    {
      if (m_enableInputEvents)
        base.OnMouseEnter(e);

      ToggleCursorVisible();
    }

    /// <summary>
    /// Raises the <see cref="E:System.Windows.Forms.Control.MouseLeave" /> event.
    /// </summary>
    /// <param name="e">An <see cref="T:System.EventArgs" /> that contains the event data.</param>
    protected override void OnMouseLeave(EventArgs e)
    {
      if (m_enableInputEvents)
        base.OnMouseLeave(e);

      ToggleCursorVisible();
    }

    /// <summary>
    /// Raises the <see cref="E:System.Windows.Forms.Form.ResizeBegin" /> event.
    /// </summary>
    /// <param name="e">A <see cref="T:System.EventArgs" /> that contains the event data.</param>
    protected override void OnResizeBegin(EventArgs e)
    {
      base.OnResizeBegin(e);
      OnSuspendRendering(e);
    }

    /// <summary>
    /// Raises the <see cref="E:System.Windows.Forms.Form.ResizeEnd" /> event.
    /// </summary>
    /// <param name="e">A <see cref="T:System.EventArgs" /> that contains the event data.</param>
    protected override void OnResizeEnd(EventArgs e)
    {
      base.OnResizeEnd(e);
      OnResumeRendering(e);
    }

    /// <summary>
    /// Raises the <see cref="E:ResumeRendering" /> event.
    /// </summary>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected virtual void OnResumeRendering(EventArgs e)
    {
      EventHandler? resume = ResumeRendering;
      if (resume is not null)
        resume(this, e);
    }

    /// <summary>
    /// Raises the <see cref="E:SuspendRendering" /> event.
    /// </summary>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected virtual void OnSuspendRendering(EventArgs e)
    {
      EventHandler? suspend = SuspendRendering;
      if (suspend is not null)
        suspend(this, e);
    }

    /// <summary>
    /// Raises the <see cref="E:Render" /> event.
    /// </summary>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected virtual void OnRender(EventArgs e)
    {
      EventHandler? render = Render;
      if (render is not null)
        render(this, e);
    }

    /// <summary>
    /// Called when the form needs to be repainted.
    /// </summary>
    /// <param name="e">A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the event data.</param>
    protected override void OnPaint(PaintEventArgs e)
    {
      base.OnPaint(e);
      if (DesignMode)
      {
        if (m_fontForDesignMode is null)
          m_fontForDesignMode = new SD.Font("Calibri", 24f, SD.FontStyle.Regular);

        e.Graphics.Clear(SD.Color.CornflowerBlue);
        string text = "Tesla3D RenderForm";
        SD.SizeF size = e.Graphics.MeasureString(text, m_fontForDesignMode);
        SD.PointF pt = new SD.PointF();
        pt.X = ((float) (base.Width - size.Width)) / 2.0f;
        pt.Y = ((float) (base.Height - size.Height)) / 2.0f;
        e.Graphics.DrawString(text, m_fontForDesignMode, new SD.SolidBrush(SD.Color.Black), pt.X, pt.Y);
      }
      else if (m_enablePaintEvents)
      {
        OnRender(e);
      }
    }

    /// <summary>
    /// Paints the background of the control.
    /// </summary>
    /// <param name="pevent">A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains information about the control to paint.</param>
    protected override void OnPaintBackground(PaintEventArgs pevent)
    {
      if (DesignMode)
        base.OnPaintBackground(pevent);
    }

    /// <summary>
    /// Windows proc handler.
    /// </summary>
    /// <param name="m">The Windows <see cref="T:System.Windows.Forms.Message" /> to process.</param>
    protected override void WndProc(ref System.Windows.Forms.Message m)
    {
      if (!m_enableInputEvents)
      {
        switch (m.Msg)
        {
          //Keys
          case 256:
          case 257:
          case 258:
          case 260:
          case 261:

          //Mouse move
          case 512:

          //Left mouse button
          case 513:
          case 514:
          case 515:

          //Right mouse button
          case 516:
          case 517:
          case 518:

          //Middle mouse button
          case 519:
          case 520:
          case 521:

          //Mouse wheel
          case 522:

          //Mouse xbuttons
          case 523:
          case 524:
          case 525:

          //Mouse hover
          case 673:
            return;
        }
      }

      base.WndProc(ref m);
    }

    private void ToggleCursorVisible()
    {
      if (m_isMouseVisible && m_mouseIsHidden)
      {
        Cursor.Show();
        m_mouseIsHidden = false;
      }
      else if (!m_isMouseVisible && !m_mouseIsHidden)
      {
        Cursor.Hide();
        m_mouseIsHidden = true;
      }
    }
  }
}
