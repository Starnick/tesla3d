﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Windows.Forms;
using Tesla.Application;
using Tesla.Graphics;
using Tesla.Input;
using Tesla.Input.Utilities;
using SD = System.Drawing;

namespace Tesla.Windows.Application
{
  public sealed class SWFWindow : IWindow
  {
    private IApplicationHost m_host;
    private SwapChain m_swapChain;
    private ISWFControlAdapter m_controlAdapter;
    private KeyboardStateBuilder m_keystateBuilder;
    private MouseStateBuilder m_mousestateBuilder;
    private object? m_tag;
    private bool m_isTopLevelWindow;
    private PresentationParameters m_originalPP;
    private bool m_inSuspended;
    private bool m_isDisposed;
    private bool m_enableResizeRedraw;

    /// <inheritdoc />
    public event TypedEventHandler<IWindow>? ClientSizeChanged;

    /// <inheritdoc />
    public event TypedEventHandler<IWindow>? OrientationChanged;

    /// <inheritdoc />
    public event TypedEventHandler<IWindow>? GotFocus;

    /// <inheritdoc />
    public event TypedEventHandler<IWindow>? LostFocus;

    /// <inheritdoc />
    public event TypedEventHandler<IWindow>? ResumeRendering;

    /// <inheritdoc />
    public event TypedEventHandler<IWindow>? SuspendRendering;

    /// <inheritdoc />
    public event TypedEventHandler<IWindow>? Closed;

    /// <inheritdoc />
    public event TypedEventHandler<IWindow>? Paint;

    /// <inheritdoc />
    public event TypedEventHandler<IWindow>? Disposed;

    /// <inheritdoc />
    public event TypedEventHandler<IWindow, MouseState>? MouseClick;

    /// <inheritdoc />
    public event TypedEventHandler<IWindow, MouseState>? MouseDoubleClick;

    /// <inheritdoc />
    public event TypedEventHandler<IWindow, MouseState>? MouseUp;

    /// <inheritdoc />
    public event TypedEventHandler<IWindow, MouseState>? MouseDown;

    /// <inheritdoc />
    public event TypedEventHandler<IWindow, MouseState>? MouseMove;

    /// <inheritdoc />
    public event TypedEventHandler<IWindow, MouseState>? MouseWheel;

    /// <inheritdoc />
    public event TypedEventHandler<IWindow, KeyboardState>? KeyDown;

    /// <inheritdoc />
    public event TypedEventHandler<IWindow, KeyboardState>? KeyPress;

    /// <inheritdoc />
    public event TypedEventHandler<IWindow, KeyboardState>? KeyUp;

    /// <inheritdoc />
    public event TypedEventHandler<IWindow, TextInputKey>? TextInput;

    /// <inheritdoc />
    public event TypedEventHandler<IWindow>? MouseEnter;

    /// <inheritdoc />
    public event TypedEventHandler<IWindow>? MouseLeave;

    /// <inheritdoc />
    public event TypedEventHandler<IWindow>? MouseHover;

    /// <inheritdoc />
    public bool EnableUserResizing { get { return m_controlAdapter.EnableUserResizing; } set { m_controlAdapter.EnableUserResizing = value; } }

    /// <inheritdoc />
    public bool EnableInputEvents { get { return m_controlAdapter.EnableInputEvents; } set { m_controlAdapter.EnableInputEvents = value; } }

    /// <inheritdoc />
    public bool EnablePaintEvents { get { return m_controlAdapter.EnablePaintEvents; } set { m_controlAdapter.EnablePaintEvents = value; } }

    /// <inheritdoc />
    public bool EnableResizeRedraw { get { return m_enableResizeRedraw; } set { m_enableResizeRedraw = value; } }

    /// <inheritdoc />
    public bool IsMinimized { get { return m_controlAdapter.IsMinimized; } }

    /// <inheritdoc />
    public bool IsMouseVisible { get { return m_controlAdapter.IsMouseVisible; } set { m_controlAdapter.IsMouseVisible = value; } }

    /// <inheritdoc />
    public bool IsMouseInsideWindow { get { return m_controlAdapter.IsMouseInsideWindow; } }

    /// <inheritdoc />
    public bool IsVisible
    {
      get
      {
        Control c = m_controlAdapter.Control;
        if (c is null)
          return false;

        return c.Visible;
      }
      set
      {
        Control c = m_controlAdapter.Control;
        if (c is not null && c.Visible != value)
          c.Visible = value;
      }
    }

    /// <inheritdoc />
    public bool IsFullScreen { get { return m_controlAdapter.WindowMode != WindowMode.Normal; } }

    /// <inheritdoc />
    public WindowMode WindowMode
    {
      get
      {
        return m_controlAdapter.WindowMode;
      }
      set
      {
        if (m_controlAdapter.WindowMode == value)
          return;

        switch (value)
        {
          // If switching to normal or FS borderless, make sure we get out of FS exclusive
          case WindowMode.Normal:
          case WindowMode.FullScreenBorderless:
            if (m_swapChain.IsFullScreen)
            {
              IsVisible = true;
              m_swapChain.ToggleFullScreen();
            }
            break;
          case WindowMode.FullScreen:
            if (!m_swapChain.IsFullScreen)
              m_swapChain.ToggleFullScreen();
            break;
        }

        m_controlAdapter.WindowMode = value;
      }
    }

    /// <inheritdoc />
    public bool Focused { get { return m_controlAdapter.Focused; } }

    /// <inheritdoc />
    public bool IsTopLevelWindow { get { return m_isTopLevelWindow; } }

    /// <inheritdoc />
    public bool IsDisposed { get { return m_isDisposed; } }

    /// <inheritdoc />
    public Int2 Location { get { return m_controlAdapter.Location; } set { m_controlAdapter.Location = value; } }

    /// <inheritdoc />
    public Rectangle ClientBounds { get { return m_controlAdapter.ClientBounds; } }

    /// <inheritdoc />
    public Rectangle ScreenBounds { get { return m_controlAdapter.ScreenBounds; } }

    /// <inheritdoc />
    public IntPtr Handle
    {
      get
      {
        Control c = m_controlAdapter.Control;
        if (c is null)
          return IntPtr.Zero;

        return c.Handle;
      }
    }

    /// <inheritdoc />
    public SwapChain SwapChain { get { return m_swapChain; } }

    /// <inheritdoc />
    public string Title
    {
      get
      {
        Control c = m_controlAdapter.Control;
        if (c is null)
          return String.Empty;

        return c.Text;
      }
      set
      {
        Control c = m_controlAdapter.Control;
        if (c is not null)
          c.Text = value;
      }
    }

    /// <inheritdoc />
    public object NativeWindow { get { return m_controlAdapter.Control; } }

    /// <inheritdoc />
    public object? Tag { get { return m_tag; } set { m_tag = value; } }

    /// <inheritdoc />
    public IApplicationHost Host { get { return m_host; } }

    private bool InvokeRequired
    {
      get
      {
        Control c = m_controlAdapter.Control;
        if (c is not null)
        {
          int currThreadId = NativeMethods.GetCurrentThreadId();
          int windowThreadId;
          NativeMethods.GetWindowThreadProcessId(c.Handle, out windowThreadId);

          return windowThreadId != currThreadId;
        }

        return false;
      }
    }

    internal SWFWindow(IApplicationHost host, PresentationParameters pp, bool isTopLevelWindow)
    {
      m_host = host;
      m_originalPP = pp;
      m_isTopLevelWindow = isTopLevelWindow;
      m_keystateBuilder = new KeyboardStateBuilder();
      m_mousestateBuilder = new MouseStateBuilder();
      m_isDisposed = false;
      m_inSuspended = false;
      m_enableResizeRedraw = false;

      if (m_isTopLevelWindow)
      {
        m_controlAdapter = new RenderForm(pp.BackBufferWidth, pp.BackBufferHeight);
      }
      else
      {
        m_controlAdapter = new RenderControl(pp.BackBufferWidth, pp.BackBufferHeight);
      }

      Control c = m_controlAdapter.Control;
      c.Show();

      m_controlAdapter.WindowMode = (pp.IsFullScreen) ? WindowMode.FullScreen : WindowMode.Normal;

      StartEvents();

      m_swapChain = new SwapChain(IRenderSystem.Current, Handle, pp);
    }

    internal SWFWindow(IApplicationHost host, Control existingControl, PresentationParameters? pp, bool sizeNativeWindow)
        : this(host, CreateSWFAdapter(existingControl), pp, sizeNativeWindow) { }

    internal SWFWindow(IApplicationHost host, ISWFControlAdapter existingControl, PresentationParameters? pp, bool resizeNativeWindow)
    {
      m_host = host;
      m_keystateBuilder = new KeyboardStateBuilder();
      m_mousestateBuilder = new MouseStateBuilder();
      m_isDisposed = false;
      m_inSuspended = false;
      m_enableResizeRedraw = false;
      m_controlAdapter = existingControl;

      m_isTopLevelWindow = existingControl is Form;
      m_originalPP = (pp.HasValue) ? pp.Value : PresentParamsFromControl(existingControl);

      if (resizeNativeWindow)
      {
        existingControl.ClientSize = new Int2(m_originalPP.BackBufferWidth, m_originalPP.BackBufferHeight);

        Int2 size = existingControl.ClientSize;

        //Ensure resize can stick
        m_originalPP.BackBufferWidth = Math.Max(size.X, 1);
        m_originalPP.BackBufferHeight = Math.Max(size.Y, 1);
        m_originalPP.IsFullScreen = (m_isTopLevelWindow) ? m_originalPP.IsFullScreen : false;
      }
      else
      {
        Int2 size = existingControl.ClientSize;

        m_originalPP.BackBufferWidth = Math.Max(size.X, 1);
        m_originalPP.BackBufferHeight = Math.Max(size.Y, 1);
        m_originalPP.IsFullScreen = false;
      }

      if (existingControl.Control is not null)
        existingControl.Control.Show();

      m_controlAdapter.WindowMode = (m_originalPP.IsFullScreen) ? WindowMode.FullScreen : WindowMode.Normal;

      StartEvents();

      m_swapChain = new SwapChain(IRenderSystem.Current, Handle, m_originalPP);
    }

    public void Repaint()
    {
      Control c = m_controlAdapter.Control;
      if (c is not null)
      {
        if (InvokeRequired)
        {
          c.Invoke(new Action(() => c.Invalidate()));
        }
        else
        {
          c.Invalidate();
        }
      }
    }

    public void Focus()
    {
      Control c = m_controlAdapter.Control;
      if (c is not null)
      {
        if (InvokeRequired)
        {
          c.Invoke(new Action(() => c.Focus()));
        }
        else
        {
          c.Focus();
        }
      }
    }

    public void Resize(int width, int height)
    {
      if (m_swapChain is null)
        return;

      if (m_swapChain.IsFullScreen)
      {
        if (width == m_originalPP.BackBufferWidth && height == m_originalPP.BackBufferHeight)
          return;

        m_originalPP.BackBufferWidth = width;
        m_originalPP.BackBufferHeight = height;

        PresentationParameters pp = m_originalPP;
        pp.IsFullScreen = true;
        m_swapChain.Reset(Handle, pp);
        OnResize(this, EventArgs.Empty);
      }
      //Otherwise resize the control directly, which will cause a WM_Size message
      else
      {
        Control c = m_controlAdapter.Control;
        if (c is not null)
        {
          SD.Size clientSize = c.ClientSize;
          if (clientSize.Width == width && clientSize.Height == height)
            return;

          Form? f = c as Form;
          if (f is not null && f.WindowState == FormWindowState.Maximized)
            f.WindowState = FormWindowState.Normal;

          c.ClientSize = new SD.Size(width, height); ;
        }
      }
    }

    public void Reset(PresentationParameters pp)
    {
      if (m_swapChain is null)
        return;

      m_originalPP = pp;

      Control c = m_controlAdapter.Control;
      if (c is not null)
        c.ClientSize = new SD.Size(pp.BackBufferWidth, pp.BackBufferHeight);

      m_swapChain.Reset(Handle, pp);
    }

    public void Close()
    {
      Control c = m_controlAdapter.Control;
      if (c is not null)
      {
        Form? f = c.FindForm();
        if (f is not null)
          f.Close();
      }
    }

    public void CenterToScreen()
    {
      Control c = m_controlAdapter.Control;
      if (c is not null)
      {
        Form? f = c.FindForm();
        if (f is not null)
        {
          SD.Rectangle screenRect = Screen.GetBounds(f);
          SD.Size windSize = f.Size;
          SD.Point topLeftLoc = f.Location;
          SD.Point windCenter = new SD.Point(topLeftLoc.X + (windSize.Width / 2), topLeftLoc.Y + (windSize.Height / 2));
          SD.Point screenCenter = new SD.Point(screenRect.X + (screenRect.Width / 2), screenRect.Y + (screenRect.Height / 2));
          f.Location = new SD.Point((screenCenter.X - windCenter.X) + topLeftLoc.X, (screenCenter.Y - windCenter.Y) + topLeftLoc.Y);
        }
      }
    }

    public void Dispose()
    {
      Dispose(true, false);
      GC.SuppressFinalize(this);
    }

    private void Dispose(bool disposing, bool disposeExternallyCalled)
    {
      if (!m_isDisposed)
      {
        m_isDisposed = true;

        if (disposing)
        {
          if (m_controlAdapter is not null)
          {
            StopEvents();

            if (!disposeExternallyCalled)
              m_controlAdapter.Dispose();
          }

          if (m_swapChain is not null)
            m_swapChain.Dispose();

          OnDisposed();
        }
      }
    }

    private void StartEvents()
    {
      m_controlAdapter.GotFocus += OnGotFocus;
      m_controlAdapter.LostFocus += OnLostFocus;
      m_controlAdapter.ResumeRendering += OnResumeRendering;
      m_controlAdapter.SuspendRendering += OnSuspendRendering;
      m_controlAdapter.Render += OnPaint;
      m_controlAdapter.Resize += OnResize;
      m_controlAdapter.Closed += OnClosed;
      m_controlAdapter.Disposed += OnDisposed;
      m_controlAdapter.KeyDown += OnKeyDown;
      m_controlAdapter.KeyPress += OnKeyPress;
      m_controlAdapter.KeyUp += OnKeyUp;
      m_controlAdapter.MouseClick += OnMouseClick;
      m_controlAdapter.MouseDoubleClick += OnMouseDoubleClick;
      m_controlAdapter.MouseDown += OnMouseDown;
      m_controlAdapter.MouseUp += OnMouseUp;
      m_controlAdapter.MouseEnter += OnMouseEnter;
      m_controlAdapter.MouseHover += OnMouseHover;
      m_controlAdapter.MouseLeave += OnMouseLeave;
      m_controlAdapter.MouseMove += OnMouseMove;
      m_controlAdapter.MouseWheel += OnMouseWheel;
    }

    private void StopEvents()
    {
      m_controlAdapter.GotFocus -= OnGotFocus;
      m_controlAdapter.LostFocus -= OnLostFocus;
      m_controlAdapter.ResumeRendering -= OnResumeRendering;
      m_controlAdapter.SuspendRendering -= OnSuspendRendering;
      m_controlAdapter.Render -= OnPaint;
      m_controlAdapter.Resize -= OnResize;
      m_controlAdapter.Closed -= OnClosed;
      m_controlAdapter.Disposed -= OnDisposed;
      m_controlAdapter.KeyDown -= OnKeyDown;
      m_controlAdapter.KeyPress -= OnKeyPress;
      m_controlAdapter.KeyUp -= OnKeyUp;
      m_controlAdapter.MouseClick -= OnMouseClick;
      m_controlAdapter.MouseDoubleClick -= OnMouseDoubleClick;
      m_controlAdapter.MouseDown -= OnMouseDown;
      m_controlAdapter.MouseUp -= OnMouseUp;
      m_controlAdapter.MouseEnter -= OnMouseEnter;
      m_controlAdapter.MouseHover -= OnMouseHover;
      m_controlAdapter.MouseLeave -= OnMouseLeave;
      m_controlAdapter.MouseMove -= OnMouseMove;
      m_controlAdapter.MouseWheel -= OnMouseWheel;
    }

    private void OnGotFocus(object? sender, EventArgs e)
    {
      TypedEventHandler<IWindow>? gotFocus = GotFocus;
      if (gotFocus is not null)
        gotFocus(this, EventArgs.Empty);
    }

    private void OnLostFocus(object? sender, EventArgs e)
    {
      // Losing focus means we lost FSE if we had it
      if (WindowMode == WindowMode.FullScreen)
        WindowMode = WindowMode.Normal;

      TypedEventHandler<IWindow>? lostFocus = LostFocus;
      if (lostFocus is not null)
        lostFocus(this, EventArgs.Empty);
    }

    private void OnResumeRendering(object? sender, EventArgs e)
    {
      m_inSuspended = false;

      //If suspending resize until it ends, then we need to make one call to OnResize, if allowing swapChain resizing,
      //then this will be an extra one
      if (!m_enableResizeRedraw)
        OnResize(this, e);

      TypedEventHandler<IWindow>? resume = ResumeRendering;
      if (resume is not null)
        resume(this, EventArgs.Empty);
    }

    private void OnSuspendRendering(object? sender, EventArgs e)
    {
      m_inSuspended = true;

      TypedEventHandler<IWindow>? suspend = SuspendRendering;
      if (suspend is not null)
        suspend(this, EventArgs.Empty);
    }

    private void OnPaint(object? sender, EventArgs e)
    {
      TypedEventHandler<IWindow>? paint = Paint;
      if (paint is not null)
        paint(this, EventArgs.Empty);
    }

    private void OnClosed(object? sender, EventArgs e)
    {
      TypedEventHandler<IWindow>? closed = Closed;
      if (closed is not null)
        closed(this, EventArgs.Empty);
    }

    private void OnResize(object? sender, EventArgs e)
    {
      bool isNotSuspended = m_enableResizeRedraw || !m_inSuspended;

      if (!IsMinimized && isNotSuspended)
      {
        if (m_swapChain is not null)
        {
          if (m_swapChain.IsFullScreen)
          {
            m_swapChain.Resize(m_originalPP.BackBufferWidth, m_originalPP.BackBufferHeight);
          }
          else
          {
            Int2 size = m_controlAdapter.ClientSize;
            m_swapChain.Resize(size.X, size.Y);
          }
        }

        TypedEventHandler<IWindow>? resize = ClientSizeChanged;
        if (resize is not null)
          resize(this, EventArgs.Empty);
      }
    }

    private void OnDisposed(object? sender, EventArgs e)
    {
      //If diposing by calling this Window, we don't want to call Dispose() again. If externally, then yes we do.
      if (!m_isDisposed)
      {
        Dispose(true, true);
        GC.SuppressFinalize(this);
      }
    }

    private void OnKeyDown(object? sender, KeyEventArgs e)
    {
      ApplyState(this, m_keystateBuilder, e, true);
      m_keystateBuilder.ConstructState(out KeyboardState state);

      TypedEventHandler<IWindow, KeyboardState>? keyDown = KeyDown;
      if (keyDown is not null)
        keyDown(this, state);
    }

    private void OnKeyPress(object? sender, KeyPressEventArgs e)
    {
      ApplyState(this, m_keystateBuilder, e);
      m_keystateBuilder.ConstructState(out KeyboardState state);

      TypedEventHandler<IWindow, KeyboardState>? keyPress = KeyPress;
      if (keyPress is not null)
        keyPress(this, state);

      TypedEventHandler<IWindow, TextInputKey>? textInput = TextInput;
      if (textInput is not null)
      {
        TextInputKey inputChar = new TextInputKey(e.KeyChar, (Tesla.Input.Keys) char.ToUpperInvariant(e.KeyChar));
        textInput(this, inputChar);
      }
    }

    private void OnKeyUp(object? sender, KeyEventArgs e)
    {
      ApplyState(this, m_keystateBuilder, e, false);
      m_keystateBuilder.ConstructState(out KeyboardState state);

      TypedEventHandler<IWindow, KeyboardState>? keyUp = KeyUp;
      if (keyUp is not null)
        keyUp(this, state);
    }

    private void OnMouseClick(object? sender, MouseEventArgs e)
    {
      ApplyState(this, m_mousestateBuilder, e, true);
      m_mousestateBuilder.ConstructState(out MouseState state);

      TypedEventHandler<IWindow, MouseState>? mouseClick = MouseClick;
      if (mouseClick is not null)
        mouseClick(this, state);
    }

    private void OnMouseDoubleClick(object? sender, MouseEventArgs e)
    {
      ApplyState(this, m_mousestateBuilder, e, true);
      m_mousestateBuilder.ConstructState(out MouseState state);

      TypedEventHandler<IWindow, MouseState>? mouseDoubleClick = MouseDoubleClick;
      if (mouseDoubleClick is not null)
        mouseDoubleClick(this, state);
    }

    private void OnMouseDown(object? sender, MouseEventArgs e)
    {
      ApplyState(this, m_mousestateBuilder, e, true);
      m_mousestateBuilder.ConstructState(out MouseState state);

      TypedEventHandler<IWindow, MouseState>? mouseDown = MouseDown;
      if (mouseDown is not null)
        mouseDown(this, state);
    }

    private void OnMouseUp(object? sender, MouseEventArgs e)
    {
      ApplyState(this, m_mousestateBuilder, e, false);
      m_mousestateBuilder.ConstructState(out MouseState state);

      TypedEventHandler<IWindow, MouseState>? mouseUp = MouseUp;
      if (mouseUp is not null)
        mouseUp(this, state);
    }

    private void OnMouseEnter(object? sender, EventArgs e)
    {
      TypedEventHandler<IWindow>? mouseEnter = MouseEnter;
      if (mouseEnter is not null)
        mouseEnter(this, EventArgs.Empty);
    }

    private void OnMouseHover(object? sender, EventArgs e)
    {
      TypedEventHandler<IWindow>? mouseHover = MouseHover;
      if (mouseHover is not null)
        mouseHover(this, EventArgs.Empty);
    }

    private void OnMouseLeave(object? sender, EventArgs e)
    {
      TypedEventHandler<IWindow>? mouseLeave = MouseLeave;
      if (mouseLeave is not null)
        mouseLeave(this, EventArgs.Empty);
    }

    private void OnMouseMove(object? sender, MouseEventArgs e)
    {
      ApplyState(this, m_mousestateBuilder, e, true);
      m_mousestateBuilder.ConstructState(out MouseState state);

      TypedEventHandler<IWindow, MouseState>? mouseMove = MouseMove;
      if (mouseMove is not null)
        mouseMove(this, state);
    }

    private void OnMouseWheel(object? sender, MouseEventArgs e)
    {
      ApplyState(this, m_mousestateBuilder, e, true);
      m_mousestateBuilder.ConstructState(out MouseState state);

      TypedEventHandler<IWindow, MouseState>? mouseWheel = MouseWheel;
      if (mouseWheel is not null)
        mouseWheel(this, state);
    }

    private void OnDisposed()
    {
      TypedEventHandler<IWindow>? disposed = Disposed;
      if (disposed is not null)
        disposed(this, EventArgs.Empty);
    }

    private void OnOrientationChanged()
    {
      TypedEventHandler<IWindow>? orientationChanged = OrientationChanged;
      if (orientationChanged is not null)
        orientationChanged(this, EventArgs.Empty);
    }

    private static void ApplyState(IWindow window, MouseStateBuilder builder, MouseEventArgs args, bool pressed)
    {
      builder.WheelValue += args.Delta;
      builder.CursorPosition = new Int2(args.X, args.Y);
      builder.FocusState = window.Host.DetermineMouseFocusState(window);

      Tesla.Input.ButtonState state = (pressed) ? Tesla.Input.ButtonState.Pressed : Tesla.Input.ButtonState.Released;

      if ((args.Button & MouseButtons.Left) == MouseButtons.Left)
        builder.SetButtonState(MouseButton.Left, state);

      if ((args.Button & MouseButtons.Middle) == MouseButtons.Middle)
        builder.SetButtonState(MouseButton.Middle, state);

      if ((args.Button & MouseButtons.Right) == MouseButtons.Right)
        builder.SetButtonState(MouseButton.Right, state);

      if ((args.Button & MouseButtons.XButton1) == MouseButtons.XButton1)
        builder.SetButtonState(MouseButton.XButton1, state);

      if ((args.Button & MouseButtons.XButton2) == MouseButtons.XButton2)
        builder.SetButtonState(MouseButton.XButton2, state);
    }

    private static void ApplyState(IWindow window, KeyboardStateBuilder builder, KeyEventArgs args, bool pressed)
    {
      Tesla.Input.KeyState state = (pressed) ? Tesla.Input.KeyState.Down : Tesla.Input.KeyState.Up;

      builder.SetKeyState((Tesla.Input.Keys) args.KeyValue, state);
      builder.FocusState = window.Host.DetermineMouseFocusState(window);
    }

    private static void ApplyState(IWindow window, KeyboardStateBuilder builder, KeyPressEventArgs args)
    {
      builder.AddPressedKey((Tesla.Input.Keys) char.ToUpperInvariant(args.KeyChar));
      builder.FocusState = window.Host.DetermineMouseFocusState(window);
    }

    private static PresentationParameters PresentParamsFromControl(ISWFControlAdapter c)
    {
      PresentationParameters pp = new PresentationParameters();
      pp.BackBufferFormat = SurfaceFormat.Color;

      Int2 size = c.ClientSize;
      pp.BackBufferWidth = size.X;
      pp.BackBufferHeight = size.Y;

      pp.DepthStencilFormat = DepthFormat.Depth32Stencil8;
      pp.DisplayOrientation = DisplayOrientation.Default;
      pp.IsFullScreen = false;
      pp.PresentInterval = PresentInterval.Immediate;
      pp.RenderTargetUsage = RenderTargetUsage.PlatformDefault;
      pp.MultiSampleQuality = 0;
      pp.MultiSampleCount = 0;

      return pp;
    }

    private static ISWFControlAdapter CreateSWFAdapter(Control existingControl)
    {
      if (existingControl is ISWFControlAdapter)
        return (existingControl as ISWFControlAdapter)!;
      else
        return new ControlAdapter(existingControl);
    }
  }
}
