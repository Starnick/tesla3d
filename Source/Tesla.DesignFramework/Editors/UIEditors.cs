﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;

namespace Tesla.DesignFramework
{
    public static class UIEditors
    {
        private static Dictionary<Type, object> s_editorMap = new Dictionary<Type, object>();

        public static void RegisterEditor(Type proxyType, object editor)
        {
            if(proxyType == null)
                throw new ArgumentNullException("proxyType");

            if(editor == null)
                throw new ArgumentNullException("editor");

            lock(s_editorMap)
            {
                s_editorMap.Add(proxyType, editor);
            }
        }

        public static void RegisterEditor<T>(object editor) where T : UIEditorProxy
        {
            RegisterEditor(typeof(T), editor);
        }

        public static object GetEditor(Type proxyType)
        {
            if(proxyType == null)
                return null;

            lock (s_editorMap)
            {
                object editor;
                s_editorMap.TryGetValue(proxyType, out editor);
                return editor;
            }
        }
    }
}
