﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesla;

namespace Tesla.DesignFramework
{
    public class CameraSettings
    {
        private static CameraSettings s_instance;

        public Angle FieldOfView { get; set; }
        public float Near { get; set; }
        public float Far { get; set; }
        public Color ClearColor { get; set; }

        public static CameraSettings Instance
        {
            get
            {
                if(s_instance == null)
                {
                    s_instance = new CameraSettings();
                    s_instance.LoadFromSettings();
                }

                return s_instance;
            }
        }

        public CameraSettings()
        {
            FieldOfView = Angle.FromDegrees(85.0f);
            Near = .5f;
            Far = 10000;
            ClearColor = Color.Black;
        }

        public void LoadFromSettings()
        {
            Settings.Camera settings = Settings.Camera.Default;

            FieldOfView = Angle.FromDegrees(settings.FieldOfView);
            Near = settings.Near;
            Far = settings.Far;

            System.Drawing.Color c = settings.ClearColor;
            ClearColor = new Color(c.R, c.G, c.B, c.A);
        }

        public void SaveToSettings()
        {
            Settings.Camera settings = Settings.Camera.Default;

            settings.FieldOfView = FieldOfView.Degrees;
            settings.Near = Near;
            settings.Far = Far;
            settings.ClearColor = System.Drawing.Color.FromArgb(ClearColor.A, ClearColor.R, ClearColor.G, ClearColor.B);
            settings.Save();
        }
    }
}
