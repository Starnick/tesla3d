﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesla;
using Tesla.Content;

namespace Tesla.DesignFramework
{
    public class ImportSettings
    {
        private static ImportSettings s_instance;

        public TangentBasisGeneration TangentGeneration { get; set; }
        public NormalGeneration NormalGeneration { get; set; }
        public bool OptimizeGraph { get; set; }
        public bool OptimizeMeshes { get; set; }
        public bool FixInFacingNormals { get; set; }
        public bool FlipUV { get; set; }
        public bool SwapWindingOrder { get; set; }
        public Angle CreaseAngle { get; set; }
        public int MaxTrianglesPerMesh { get; set; }
        public float Scale { get; set; }
        public Angle XAxisRotation { get; set; }
        public Angle YAxisRotation { get; set; }
        public Angle ZAxisRotation { get; set; }

        public static ImportSettings Instance
        {
            get
            {
                if(s_instance == null)
                {
                    s_instance = new ImportSettings();
                    s_instance.LoadFromSettings();
                }

                return s_instance;
            }
        }

        public ImportSettings() { }

        public void LoadFromSettings()
        {
            Settings.Import settings = Settings.Import.Default;

            TangentGeneration = settings.TangentGeneration;
            NormalGeneration = settings.NormalGeneration;
            OptimizeGraph = settings.OptimizeGraph;
            OptimizeMeshes = settings.OptimizeMeshes;
            FixInFacingNormals = settings.FixInFacingNormals;
            FlipUV = settings.FlipUV;
            SwapWindingOrder = settings.SwapWindingOrder;
            CreaseAngle = Angle.FromDegrees(settings.CreaseAngle);
            MaxTrianglesPerMesh = settings.MaxTrianglesPerMesh;
            Scale = settings.Scale;
            XAxisRotation = Angle.FromDegrees(settings.XAxisRotation);
            YAxisRotation = Angle.FromDegrees(settings.YAxisRotation);
            ZAxisRotation = Angle.FromDegrees(settings.ZAxisRotation);
        }

        public void SaveToSettings()
        {
            Settings.Import settings = Settings.Import.Default;

            settings.TangentGeneration = TangentGeneration;
            settings.NormalGeneration = NormalGeneration;
            settings.OptimizeGraph = OptimizeGraph;
            settings.OptimizeMeshes = OptimizeMeshes;
            settings.FixInFacingNormals = FixInFacingNormals;
            settings.FlipUV = FlipUV;
            settings.SwapWindingOrder = SwapWindingOrder;
            settings.CreaseAngle = CreaseAngle.Degrees;
            settings.MaxTrianglesPerMesh = MaxTrianglesPerMesh;
            settings.Scale = Scale;
            settings.XAxisRotation = XAxisRotation.Degrees;
            settings.YAxisRotation = YAxisRotation.Degrees;
            settings.ZAxisRotation = ZAxisRotation.Degrees;

            settings.Save();
        }

        public ModelImporterParameters ConfigureParameters(ModelImporterParameters param)
        {
            if(param == null)
                param = new ModelImporterParameters();

            param.CreaseAngle = CreaseAngle;
            param.NormalGenerationOptions = NormalGeneration;
            param.TangentBasisGenerationOptions = TangentGeneration;

            param.ExtendedParameters[AssimpModelImporter.ExtendedParam_OptimizeGraph] = OptimizeGraph.ToString();
            param.ExtendedParameters[AssimpModelImporter.ExtendedParam_OptimizeMeshes] = OptimizeMeshes.ToString();
            param.ExtendedParameters[AssimpModelImporter.ExtendedParam_FixInFacingNormals] = FixInFacingNormals.ToString();

            if(MaxTrianglesPerMesh > 0)
                param.ExtendedParameters[AssimpModelImporter.ExtendedParam_SplitLargeMeshes] = MaxTrianglesPerMesh.ToString();
            else
                param.ExtendedParameters.Remove(AssimpModelImporter.ExtendedParam_SplitLargeMeshes);

            param.FlipUVs = FlipUV;
            param.SwapWindingOrder = SwapWindingOrder;
            param.Scale = Scale;
            param.XAngle = XAxisRotation;
            param.YAngle = YAxisRotation;
            param.ZAngle = ZAxisRotation;

            param.MaterialParameters.TextureParameters.GenerateMipMaps = true;

            return param;
        }
    }
}
