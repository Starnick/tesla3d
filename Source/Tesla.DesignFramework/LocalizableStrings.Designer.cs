﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tesla.DesignFramework {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class LocalizableStrings {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal LocalizableStrings() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Tesla.DesignFramework.LocalizableStrings", typeof(LocalizableStrings).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Alpha component..
        /// </summary>
        internal static string AlphaComponentDisplay {
            get {
                return ResourceManager.GetString("AlphaComponentDisplay", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Blue component..
        /// </summary>
        internal static string BlueComponentDisplay {
            get {
                return ResourceManager.GetString("BlueComponentDisplay", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Element at index %1..
        /// </summary>
        internal static string ElementAtIndexDisplay {
            get {
                return ResourceManager.GetString("ElementAtIndexDisplay", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Green component..
        /// </summary>
        internal static string GreenComponentDisplay {
            get {
                return ResourceManager.GetString("GreenComponentDisplay", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Index is out of range..
        /// </summary>
        internal static string IndexOutOfRange {
            get {
                return ResourceManager.GetString("IndexOutOfRange", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Property already belongs to another UI instance..
        /// </summary>
        internal static string PropertyBelongsToAnother {
            get {
                return ResourceManager.GetString("PropertyBelongsToAnother", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Red component..
        /// </summary>
        internal static string RedComponentDisplay {
            get {
                return ResourceManager.GetString("RedComponentDisplay", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Value is read-only..
        /// </summary>
        internal static string ValueIsReadOnly {
            get {
                return ResourceManager.GetString("ValueIsReadOnly", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to W coordinate value..
        /// </summary>
        internal static string WCoordinateDisplay {
            get {
                return ResourceManager.GetString("WCoordinateDisplay", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to X coordinate value..
        /// </summary>
        internal static string XCoordinateDisplay {
            get {
                return ResourceManager.GetString("XCoordinateDisplay", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Y coordinate value..
        /// </summary>
        internal static string YCoordinateDisplay {
            get {
                return ResourceManager.GetString("YCoordinateDisplay", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Z coordinate value..
        /// </summary>
        internal static string ZCoordinateDisplay {
            get {
                return ResourceManager.GetString("ZCoordinateDisplay", resourceCulture);
            }
        }
    }
}
