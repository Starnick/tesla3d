﻿using System;
using System.Collections.Generic;
using Tesla.Graphics;

namespace Tesla.DesignFramework
{
    public class DesignScene
    {
        private bool m_drawGrid;
        private IGizmo m_activeGizmo;
        private IDesignApp m_app;
        private DesignGrid m_grid;
        private List<ILayer> m_layers;

        public IGizmo ActiveGizmo
        {
            get
            {
                return m_activeGizmo;
            }

            set
            {
                m_activeGizmo = value;
            }
        }

        public bool DrawGrid
        {
            get
            {
                return m_drawGrid;
            }

            set
            {
                m_drawGrid = value;
            }
        }

        public DesignGrid Grid
        {
            get
            {
                return m_grid;
            }
        }

        public IList<ILayer> Layers
        {
            get
            {
                return m_layers;
            }
        }

        public String Name
        {
            get
            {
                return "ViewerScene";
            }
        }

        public DesignScene(IDesignApp app)
        {
            m_layers = new List<ILayer>();
            m_app = app;
            m_drawGrid = true;
            m_grid = new DesignGrid();
        }

        public T FindLayer<T>() where T : class, ILayer
        {
            for(int i = 0; i < m_layers.Count; i++)
            {
                T layer = m_layers[i] as T;
                if(layer != null)
                    return layer;
            }

            return null;
        }

        public void Render(IRenderer renderer)
        {
            if(renderer == null)
                return;

            //Render all the objects in view on each layer
            for (int i = 0; i < m_layers.Count; i++)
            {
                ILayer layer = m_layers[i];
                if (layer != null)
                    layer.Render(renderer);
            }

            renderer.Render(true, true);

            //Draw grid if applicable
            RenderGrid(renderer);

            //If any objects are selected, draw the selection cues
            m_app.SelectionManager.DrawSelection(this);

            //Lastly, draw any manipulators
            if (m_activeGizmo != null)
                m_activeGizmo.Render(renderer);
        }

        public void Update(IGameTime time)
        {
            if (m_activeGizmo != null)
                m_activeGizmo.Update(time);

            for(int i = 0; i < m_layers.Count; i++)
            {
                ILayer layer = m_layers[i];
                if (layer != null)
                    layer.Update(time);
            }
        }

        private void RenderGrid(IRenderer renderer)
        {
            if (!m_drawGrid)
                return;

            m_app.BatchManager.LineDrawer.Begin(renderer.RenderContext);
            m_grid.Draw(renderer.RenderContext.Camera, m_app.BatchManager.LineDrawer);
            m_app.BatchManager.LineDrawer.End();
        }
    }
}
