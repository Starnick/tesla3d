﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesla;
using Tesla.Application;
using Tesla.Graphics;

namespace Tesla.DesignFramework
{
  public class ViewManager
  {
    private List<View> m_views;
    private IApplicationHost m_host;
    private View m_activeView;

    public int ViewCount
    {
      get
      {
        return m_views.Count;
      }
    }

    public View ActiveView
    {
      get
      {
        return m_activeView;
      }
      internal set
      {
        m_activeView = value;
      }
    }

    public ViewManager(IApplicationHost host)
    {
      m_host = host;
      m_views = new List<View>();

      CreateViewsFromExisting();

      if (m_views.Count > 0)
        m_activeView = m_views[0];
    }

    public View GetView(int index)
    {
      if (index < 0 || index >= m_views.Count)
        return null;

      return m_views[index];
    }

    public View CreateView(PresentationParameters? presentationParams = null)
    {
      PresentationParameters pp = new PresentationParameters();

      if (presentationParams.HasValue)
      {
        pp = presentationParams.Value;
      }
      else
      {
        pp.BackBufferFormat = SurfaceFormat.Color;
        pp.BackBufferHeight = 1;
        pp.BackBufferWidth = 1;
        pp.DepthStencilFormat = DepthFormat.Depth32Stencil8;
        pp.DisplayOrientation = DisplayOrientation.Default;
        pp.IsFullScreen = false;
        pp.MultiSampleCount = 4;
        pp.MultiSampleQuality = 0;
        pp.PresentInterval = PresentInterval.Immediate;
        pp.RenderTargetUsage = RenderTargetUsage.DiscardContents;
      }

      IWindow window = m_host.CreateWindow(pp, false);

      View view = new View(this, window, m_views.Count);
      m_views.Add(view);

      if (m_activeView == null)
        m_activeView = view;

      return view;
    }

    private void CreateViewsFromExisting()
    {
      foreach (IWindow window in m_host.Windows)
      {
        View view = new View(this, window, m_views.Count);
        m_views.Add(view);
      }
    }
  }
}
