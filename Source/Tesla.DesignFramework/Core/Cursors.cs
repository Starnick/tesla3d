﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tesla.Application;

namespace Tesla.DesignFramework
{
    //TODO - Abstract this out
    public static class Cursors
    {
        private static IWindow m_viewport;

        public static IWindow ActiveViewport
        {
            get
            {
                return m_viewport;
            }
            set
            {
                m_viewport = value;
            }
        }

        public static void SetNormalCursor()
        {
            Control c = m_viewport.NativeWindow as Control;
            if(c != null)
                c.Cursor = System.Windows.Forms.Cursors.Default;
        }

        public static void SetHandCursor()
        {
            Control c = m_viewport.NativeWindow as Control;
            if(c != null)
                c.Cursor = System.Windows.Forms.Cursors.NoMove2D;
        }
    }
}
