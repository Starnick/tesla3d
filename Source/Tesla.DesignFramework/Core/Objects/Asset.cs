﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Tesla.DesignFramework
{
  public class Asset
  {
    private Guid m_id;
    private AssetLibrary m_library;
    private String m_displayLabel;
    private String m_filepath;
    private bool m_isNotLoaded;
    private bool m_isMissing;
    private Image m_thumbnail;
    private DateTime m_lastUsed;
    private Type m_assetType;
    private Object m_assetData;

    public Guid ID
    {
      get
      {
        return m_id;
      }
    }

    public String DisplayLabel
    {
      get
      {
        return m_displayLabel;
      }
      set
      {
        m_displayLabel = value;
      }
    }

    public String FilePath
    {
      get
      {
        return m_filepath;
      }
      set
      {
        m_filepath = value;
      }
    }

    public bool IsInLibrary
    {
      get
      {
        return m_library != null;
      }
    }

    public AssetLibrary Library
    {
      get
      {
        return m_library;
      }
    }

    public Image Thumbnail
    {
      get
      {
        return m_thumbnail;
      }
      set
      {
        m_thumbnail = value;
      }
    }

    public bool IsNotLoaded
    {
      get
      {
        return m_isNotLoaded;
      }
    }

    public bool IsMissing
    {
      get
      {
        return m_isMissing;
      }
    }

    public DateTime LastUsed
    {
      get
      {
        return m_lastUsed;
      }
    }

    public Type AssetType
    {
      get
      {
        return m_assetType;
      }
      set
      {
        m_assetType = value;
      }
    }

    public Object AssetData
    {
      get
      {
        return m_assetData;
      }
      set
      {
        m_assetData = value;
      }
    }

    public Asset() { }

    public Object Load()
    {
      return null;
    }

    public void Unload()
    {
      if (m_assetData is IDisposable)
        (m_assetData as IDisposable).Dispose();

      m_assetData = null;
      m_isNotLoaded = true;
      m_isMissing = false;
      m_lastUsed = DateTime.Now;
    }
  }
}
