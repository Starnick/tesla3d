﻿/*
* Copyright (c) 2010-2017 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace Tesla.DesignFramework
{
    public class ElementManager : IReadOnlyDictionary<ElementId, Element>
    {
        private static int s_currId = 0;
        private Dictionary<ElementId, Element> m_elements;
        private IDesignApp m_app;

        public Element this[ElementId id]
        {
            get
            {
                if (!id.IsValid)
                    return null;

                Element el;
                if (m_elements.TryGetValue(id, out el))
                    return el;

                return null;
            }
        }

        public IEnumerable<ElementId> Keys
        {
            get
            {
                return m_elements.Keys;
            }
        }

        public IEnumerable<Element> Values
        {
            get
            {
                return m_elements.Values;
            }
        }

        public int Count
        {
            get
            {
                return m_elements.Count;
            }
        }

        public ElementManager(IDesignApp app)
        {
            m_app = app;
            m_elements = new Dictionary<ElementId, Element>();
        }

        public bool Delete(ElementId id)
        {
            Element el;
            if(id.IsValid && m_elements.TryGetValue(id, out el))
            {
                //TODO - Some sort of event?
                m_elements.Remove(id);

                IDisposable disposable = el.Data as IDisposable;
                if (disposable != null)
                    disposable.Dispose();

                return true;
            }

            return false;
        }

        public Element Create(Object data)
        {
            //Get a new ID for the data
            int newIdValue = Interlocked.Increment(ref s_currId);
            ElementId id = new ElementId((uint)newIdValue);

            Element el = new Element(id, data);
            m_elements.Add(id, el);

            //TODO - Some sort of event?

            return el;
        }

        public Element Resurrect(ElementId oldId, Object data)
        {
            System.Diagnostics.Debug.Assert(m_elements.ContainsKey(oldId), "Resurrecting an active object is an error...");

            Element el = new Element(oldId, data);
            m_elements.Add(oldId, el);

            //TODO - Some sort of event?

            return el;
        }

        public bool TryGetValue(ElementId id, out Element value)
        {
            return m_elements.TryGetValue(id, out value);
        }

        public bool ContainsKey(ElementId id)
        {
            return m_elements.ContainsKey(id);
        }

        public IEnumerator<KeyValuePair<ElementId, Element>> GetEnumerator()
        {
            return m_elements.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return m_elements.GetEnumerator();
        }
    }
}
