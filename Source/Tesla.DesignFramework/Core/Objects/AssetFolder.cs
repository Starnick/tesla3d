﻿/*
* Copyright (c) 2010-2017 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tesla.DesignFramework
{
    public class AssetFolder
    {
        private String m_displayLabel;
        private String m_folderPath;
        private List<AssetFolder> m_subFolders;
        private List<Asset> m_assets;

        public String DisplayLabel
        {
            get
            {
                return m_displayLabel;
            }
            set
            {
                m_displayLabel = value;
            }
        }

        public String FolderPath
        {
            get
            {
                return m_folderPath;
            }
            set
            {
                m_folderPath = value;
            }
        }

        public List<AssetFolder> SubFolders
        {
            get
            {
                return m_subFolders;
            }
        }

        public List<Asset> Files
        {
            get
            {
                return m_assets;
            }
        }

        public AssetFolder()
        {
            m_subFolders = new List<AssetFolder>();
            m_assets = new List<Asset>();
            m_displayLabel = String.Empty;
        }
    }
}
