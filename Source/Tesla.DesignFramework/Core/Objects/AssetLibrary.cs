﻿/*
* Copyright (c) 2010-2017 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tesla.DesignFramework
{
    public class AssetLibrary : IDictionary<Guid, Asset>
    {
        private Dictionary<Guid, Asset> m_assets;
        private String m_displayLabel;

        public String DisplayLabel
        {
            get
            {
                return m_displayLabel;
            }
            set
            {
                m_displayLabel = value;
            }
        }

        public Asset this[Guid key]
        {
            get
            {
                return m_assets[key];
            }
            set
            {
                m_assets[key] = value;
            }
        }

        public ICollection<Guid> Keys
        {
            get
            {
                return m_assets.Keys;
            }
        }

        public ICollection<Asset> Values
        {
            get
            {
                return m_assets.Values;
            }
        }

        public int Count
        {
            get
            {
                return m_assets.Count;
            }
        }

        bool ICollection<KeyValuePair<Guid, Asset>>.IsReadOnly
        {
            get
            {
                return (m_assets as ICollection<KeyValuePair<Guid, Asset>>).IsReadOnly;
            }
        }

        public AssetLibrary()
        {
            m_assets = new Dictionary<Guid, Asset>();
            m_displayLabel = String.Empty;
        }

        public void Add(Asset value)
        {
            if (value == null)
                return;

            (this as IDictionary<Guid, Asset>).Add(value.ID, value);
        }

        public void Clear()
        {
            m_assets.Clear();
        }

        public bool ContainsKey(Guid key)
        {
            return m_assets.ContainsKey(key);
        }

        public bool Remove(Guid key)
        {
            return m_assets.Remove(key);
        }

        public bool TryGetValue(Guid key, out Asset value)
        {
            return m_assets.TryGetValue(key, out value);
        }

        public IEnumerator<KeyValuePair<Guid, Asset>> GetEnumerator()
        {
            return m_assets.GetEnumerator();
        }

        #region Explicit methods

        IEnumerator IEnumerable.GetEnumerator()
        {
            return m_assets.GetEnumerator();
        }

        void IDictionary<Guid, Asset>.Add(Guid key, Asset value)
        {
            if (value == null)
                return;

            m_assets.Add(key, value);
        }

        bool ICollection<KeyValuePair<Guid, Asset>>.Contains(KeyValuePair<Guid, Asset> item)
        {
            throw new NotImplementedException();
        }

        void ICollection<KeyValuePair<Guid, Asset>>.Add(KeyValuePair<Guid, Asset> item)
        {
            throw new NotImplementedException();
        }

        void ICollection<KeyValuePair<Guid, Asset>>.CopyTo(KeyValuePair<Guid, Asset>[] array, int arrayIndex)
        {
            (m_assets as ICollection<KeyValuePair<Guid, Asset>>).CopyTo(array, arrayIndex);
        }

        bool ICollection<KeyValuePair<Guid, Asset>>.Remove(KeyValuePair<Guid, Asset> item)
        {
            return m_assets.Remove(item.Key);
        }

        #endregion
    }
}
