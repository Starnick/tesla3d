﻿/*
* Copyright (c) 2010-2017 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Runtime.InteropServices;

namespace Tesla.DesignFramework
{
    public class Element
    {
        private ElementId m_id;
        private Object m_data;

        public ElementId ID
        {
            get
            {
                return m_id;
            }
        }

        public String DisplayLabel
        {
            get
            {
                INamed named = m_data as INamed;
                if (named != null)
                    return named.Name;

                return String.Empty;
            }
            set
            {
                INamable namable = m_data as INamable;
                if (namable != null)
                    namable.Name = value;
            }
        }

        public Object Data
        {
            get
            {
                return m_data;
            }
            set
            {
                m_data = value;
            }
        }

        internal Element(ElementId id, Object data)
        {
            m_id = id;
            m_data = data;
        }

        public T As<T>() where T : class
        {
            return m_data as T;
        }
    }

    /// <summary>
    /// A unique, non-persistent identifier for an <see cref="Element"/>. Every new object during the session is assigned a
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct ElementId
    {
        private uint m_idValue;

        /// <summary>
        /// Gets an invalid element ID instance.
        /// </summary>
        public static ElementId Invalid
        {
            get
            {
                return new ElementId(0);
            }
        }

        /// <summary>
        /// Checks to see if this instance is an invalid ID or not.
        /// </summary>
        public bool IsValid
        {
            get
            {
                return m_idValue > 0;
            }
        }

        /// <summary>
        /// Gets the underlying numerical value of the ID.
        /// </summary>
        public uint Value
        {
            get
            {
                return m_idValue;
            }
        }

        /// <summary>
        /// Creates a new instance of <see cref="ElementId"/>.
        /// </summary>
        /// <param name="value">Underlying numerical value of the ID.</param>
        public ElementId(uint value)
        {
            m_idValue = value;
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
        public override int GetHashCode()
        {
            unchecked
            {
                return (int)m_idValue;
            }
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object" />, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns><c>True</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.</returns>
        public override bool Equals(object obj)
        {
            if (obj is ElementId)
                return ((ElementId)obj).m_idValue == m_idValue;

            return false;
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>True if the current object is equal to the <paramref name="other" /> parameter; otherwise, false.</returns>
        public bool Equals(ElementId other)
        {
            return other.m_idValue == m_idValue;
        }

        /// <summary>
        /// Compares the current object with another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>A value that indicates the relative order of the objects being compared. The return value has the following meanings: Value Meaning Less than zero This object is less than the <paramref name="other" /> parameter.Zero This object is equal to <paramref name="other" />. Greater than zero This object is greater than <paramref name="other" />.</returns>
        public int CompareTo(ElementId other)
        {
            if (m_idValue < other.m_idValue)
                return -1;

            if (m_idValue > other.m_idValue)
                return 1;

            return 0;
        }

        /// <summary>
        /// Checks inequality between two <see cref="ElementId"/> instances.
        /// </summary>
        /// <param name="a">First ID.</param>
        /// <param name="b">Second ID.</param>
        /// <returns>True if the two values are not the same, false otherwise.</returns>
        public static bool operator !=(ElementId a, ElementId b)
        {
            return a.m_idValue != b.m_idValue;
        }

        /// <summary>
        /// Checks equality between two <see cref="ElementId"/> instances.
        /// </summary>
        /// <param name="a">First ID.</param>
        /// <param name="b">Second ID.</param>
        /// <returns>True if the two values are the same, false otherwise.</returns>
        public static bool operator ==(ElementId a, ElementId b)
        {
            return a.m_idValue == b.m_idValue;
        }

        /// <summary>
        /// Checks if the first <see cref="ElementId"/> is greater than the second.
        /// </summary>
        /// <param name="a">First ID.</param>
        /// <param name="b">Second ID.</param>
        /// <returns>True if the first value is greater than the second, false otherwise.</returns>
        public static bool operator >(ElementId a, ElementId b)
        {
            return a.m_idValue > b.m_idValue;
        }

        /// <summary>
        /// Checks if the first <see cref="ElementId"/> is less than the second.
        /// </summary>
        /// <param name="a">First ID.</param>
        /// <param name="b">Second ID.</param>
        /// <returns>True if the first value is less than the second, false otherwise.</returns>
        public static bool operator <(ElementId a, ElementId b)
        {
            return a.m_idValue < b.m_idValue;
        }

        /// <summary>
        /// Checks if the first <see cref="ElementId"/> is greater than or equal to the second.
        /// </summary>
        /// <param name="a">First ID.</param>
        /// <param name="b">Second ID.</param>
        /// <returns>True if the first value is greater than or equal to the second, false otherwise.</returns>
        public static bool operator >=(ElementId a, ElementId b)
        {
            return a.m_idValue >= b.m_idValue;
        }

        /// <summary>
        /// Checks if the first <see cref="ElementId"/> is less than or equal to the second.
        /// </summary>
        /// <param name="a">First ID.</param>
        /// <param name="b">Second ID.</param>
        /// <returns>True if the first value is less than or equal to the second, false otherwise.</returns>
        public static bool operator <=(ElementId a, ElementId b)
        {
            return a.m_idValue <= b.m_idValue;
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
        public override String ToString()
        {
            return (IsValid) ? m_idValue.ToString() : "Invalid";
        }
    }
}
