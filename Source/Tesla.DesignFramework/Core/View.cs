﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesla;
using Tesla.Application;
using Tesla.Graphics;
using Tesla.Input;

namespace Tesla.DesignFramework
{
    public class View
    {
        private Camera m_camera;
        private ViewManager m_viewManager;
        private IViewController m_controller;
        private IWindow m_window;
        private DesignScene m_scene;
        private RenderTarget2D m_tempRenderTarget;
        private int m_viewIndex;

        public Camera Camera
        {
            get
            {
                return m_camera;
            }
        }

        public IViewController Controller
        {
            get
            {
                return m_controller;
            }

            set
            {
                m_controller = value;
            }
        }

        public DesignScene Scene
        {
            get
            {
                return m_scene;
            }
            set
            {
                m_scene = value;
            }
        }

        public int ViewIndex
        {
            get
            {
                return m_viewIndex;
            }
        }

        public IWindow Window
        {
            get
            {
                return m_window;
            }
        }

        internal View(ViewManager viewManager, IWindow window, int viewIndex)
        {
            m_viewManager = viewManager;
            m_window = window;
            m_camera = new Camera();
            m_viewIndex = viewIndex;
            m_scene = null;
            m_tempRenderTarget = null;
            
            if(window != null)
            {
                Rectangle clientRect = m_window.ClientBounds;
                if(clientRect.Width == 0)
                    clientRect.Width = 1;

                if(clientRect.Height == 0)
                    clientRect.Height = 1;

                m_camera.Viewport = new Viewport(0, 0, clientRect.Width, clientRect.Height);
                window.SwapChain.Name = String.Format("View {0}", m_viewIndex.ToString());

                window.EnableResizeRedraw = true;
                window.IsMouseVisible = true;
                window.Title = String.Format("DesignForge View {0}", viewIndex.ToString());

                window.ClientSizeChanged += Window_ClientSizeChanged;
                window.GotFocus += Window_GotFocus;
                window.LostFocus += Window_LostFocus;
                window.Disposed += Window_Disposed;

                //Initialize camera with viewport and projection
                Window_ClientSizeChanged(window, EventArgs.Empty);
            }
        }

        public RenderTarget2D GetTemporaryRenderTarget()
        {
            PresentationParameters pp = m_window.SwapChain.PresentationParameters;

            bool recreate = m_tempRenderTarget == null || (m_tempRenderTarget.Width != pp.BackBufferWidth || m_tempRenderTarget.Height != pp.BackBufferHeight);
            
            if(recreate)
            {
                if (m_tempRenderTarget != null)
                    m_tempRenderTarget.Dispose();

                m_tempRenderTarget = new RenderTarget2D(m_window.SwapChain.RenderSystem, pp.BackBufferWidth, pp.BackBufferHeight, false, SurfaceFormat.Color, DepthFormat.Depth32Stencil8);
            }

            return m_tempRenderTarget;
        }

        private void Window_Disposed(IWindow sender, EventArgs args)
        {
            if(m_tempRenderTarget != null)
                m_tempRenderTarget.Dispose();
        }

        public void Draw(IRenderer renderer)
        {
            if(m_scene == null || renderer == null || m_window.IsDisposed)
                return;

            if(!m_window.IsVisible)
                return;

            m_window.SwapChain.SetActiveAndClear(renderer.RenderContext, CameraSettings.Instance.ClearColor);
            renderer.RenderContext.Camera = m_camera;

            m_scene.Render(renderer);

            if(m_controller != null)
            {
                IList<ILayer> overlays = m_controller.Overlays;
                for(int i = 0; i < overlays.Count; i++)
                    overlays[i].Render(renderer);
            }

            m_window.SwapChain.Present();
            renderer.RenderContext.BackBuffer = null;
            renderer.RenderContext.Camera = null;
        }

        public void Update(IGameTime time)
        {
            if(m_controller != null && m_window.Focused)
                m_controller.Update(time);
        }

        private void Window_LostFocus(IWindow sender, EventArgs args)
        {
            Mouse.WindowHandle = IntPtr.Zero;
        }

        private void Window_GotFocus(IWindow sender, EventArgs args)
        {
            Mouse.WindowHandle = sender.Handle;
            m_viewManager.ActiveView = this;
        }

        private void Window_ClientSizeChanged(IWindow sender, EventArgs args)
        {
            Rectangle clientBounds = sender.ClientBounds;

            m_camera.Viewport = new Viewport(0, 0, clientBounds.Width, clientBounds.Height);

            CameraSettings settings = CameraSettings.Instance;
            m_camera.SetProjection(settings.FieldOfView, settings.Near, settings.Far);
            m_camera.Update();
        }
    }
}
