﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tesla.DesignFramework
{
    public sealed class EventDispatcher
    {
        private Dictionary<Type, List<Delegate>> m_eventDelegates;

        public EventDispatcher()
        {
            m_eventDelegates = new Dictionary<Type, List<Delegate>>();
        }

        public bool Subscribe<T>(EventHandler<T> handler)
        {
            if(handler == null)
                return false;

            Type type = typeof(T);
            List<Delegate> list;
            if(!m_eventDelegates.TryGetValue(type, out list))
            {
                list = new List<Delegate>();
                m_eventDelegates.Add(type, list);
            }

            list.Add(handler);
            return true;
        }

        public bool IsSubscribed<T>(EventHandler<T> handler)
        {
            if(handler == null)
                return false;

            Type type = typeof(T);
            List<Delegate> list;
            if(m_eventDelegates.TryGetValue(type, out list))
                return list.Contains(handler);

            return false;
        }

        public bool Unsubscribe<T>(EventHandler<T> handler)
        {
            if(handler == null)
                return false;

            Type type = typeof(T);
            List<Delegate> list;
            if(m_eventDelegates.TryGetValue(type, out list))
                return list.Remove(handler);

            return false;
        }

        public void Unsubscribe<T>()
        {
            Type type = typeof(T);
            List<Delegate> list;
            if(m_eventDelegates.TryGetValue(type, out list))
                list.Clear();
        }

        public void UnsubscribeAll()
        {
            m_eventDelegates.Clear();
        }

        public void Dispatch<T>(Object sender, ref T msg)
        {
            if(sender == null)
                return;

            Type type = typeof(T);
            List<Delegate> list;
            if(m_eventDelegates.TryGetValue(type, out list))
            {
                foreach(EventHandler<T> handler in list)
                    handler(sender, msg);
            }
        }
    }
}
