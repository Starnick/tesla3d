﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesla;
using Tesla.Graphics;

namespace Tesla.DesignFramework
{
    public class BatchDrawManager
    {
        private LineBatch m_lineBatch;
        private SpriteBatch m_spriteBatch;
        private Dictionary<Type, Object> m_primBatches;

        public LineBatch LineDrawer { get { return m_lineBatch; } }

        public SpriteBatch SpriteDrawer {  get { return m_spriteBatch; } }

        public BatchDrawManager()
        {
            m_lineBatch = new LineBatch();
            m_spriteBatch = new SpriteBatch();
            m_primBatches = new Dictionary<Type, Object>();
        }

        public PrimitiveBatch<T> GetPrimitiveDrawer<T>() where T : unmanaged, IVertexType
        {
            Type type = typeof(T);
            Object primBatcher;
            if(m_primBatches.TryGetValue(type, out primBatcher))
            {
                return primBatcher as PrimitiveBatch<T>;
            }

            PrimitiveBatch<T> typedBatcher = new PrimitiveBatch<T>();
            m_primBatches.Add(type, typedBatcher);

            return typedBatcher;
        }
    }
}
