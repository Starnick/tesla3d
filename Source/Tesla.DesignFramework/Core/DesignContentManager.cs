﻿using System;
using System.Collections.Generic;
using System.IO;
using Tesla.Content;
using Tesla.Graphics;
using System.Drawing;

namespace Tesla.DesignFramework
{
    public class DesignContentManager
    {
        private IDesignApp m_app;
        private Dictionary<Texture, Bitmap> m_bitmapLookup;
        private ThumbnailGenerator m_thumbnailGenerator;
        private Dictionary<String, ContentManager> m_contentManagers;
        private ContentManager m_isolatedContentManager;

        public ContentManager Default
        {
            get
            {
                ContentManager contentMan;
                m_contentManagers.TryGetValue("Default", out contentMan);

                return contentMan;
            }
        }

        public DesignContentManager(IDesignApp app, ContentManager defaultContentManager)
        {
            m_contentManagers = new Dictionary<String, ContentManager>();
            m_thumbnailGenerator = new ThumbnailGenerator();
            m_app = app;
            m_bitmapLookup = new Dictionary<Texture, Bitmap>();

            SetupDefaultMissingHandlers(defaultContentManager);

            m_isolatedContentManager = new ContentManager(defaultContentManager.ServiceProvider);
            CopyMissingContentHandlers(defaultContentManager, m_isolatedContentManager);
            m_contentManagers.Add("Default", defaultContentManager);
        }

        public ContentManager GetOrCreateContentManager(String contentKey)
        {
            if (String.IsNullOrEmpty(contentKey))
                return null;

            ContentManager contentMan;
            if(!m_contentManagers.TryGetValue(contentKey, out contentMan))
            {
                ContentManager defMan = Default;
                contentMan = new ContentManager(defMan.ServiceProvider);
                CopyImporters(defMan, contentMan);
                CopyMissingContentHandlers(defMan, contentMan);
                m_contentManagers.Add(contentKey, contentMan);
            }

            return contentMan;
        }

        public T LoadIsolated<T>(String contentPath, ImporterParameters importParams = null, bool throwIfMissing = true, IResourceRepository optionalRepo = null) where T : class
        {
            if (String.IsNullOrEmpty(contentPath))
                return null;

            bool closeRepo = false;

            if (optionalRepo == null)
            {
                String dir = Directory.GetDirectoryRoot(contentPath);
                if (String.IsNullOrEmpty(dir) || !Directory.Exists(dir))
                    return null;

                optionalRepo = new FileResourceRepository(dir);
            }

            if(!optionalRepo.IsOpen)
            {
                optionalRepo.OpenConnection(ResourceFileMode.Open, ResourceFileShare.Read);
                closeRepo = true;
            }

            IResourceRepository prevRepo = m_isolatedContentManager.ResourceRepository;
            m_isolatedContentManager.ResourceRepository = optionalRepo;
            m_isolatedContentManager.ThrowForMissingContent = throwIfMissing;
            CopyImporters(Default, m_isolatedContentManager);

            try
            {
                T obj = m_isolatedContentManager.Load<T>(contentPath, importParams);

                return obj;
            }
            finally
            {
                if (closeRepo)
                    optionalRepo.CloseConnection();

                m_isolatedContentManager.ResourceRepository = prevRepo;

                m_isolatedContentManager.Unload(false);
            }
        }

        public Bitmap GetThumbnail(Texture texture, int minDimensions = 128)
        {
            if (texture == null)
                return null;

            Bitmap bitmap;
            if (!m_bitmapLookup.TryGetValue(texture, out bitmap))
            {
                switch (texture.Dimension)
                {
                    case TextureDimension.Two:
                        bitmap = m_thumbnailGenerator.RenderToBitmap(texture as Texture2D, m_app.BatchManager.SpriteDrawer, m_app.Renderer.RenderContext, minDimensions, minDimensions);
                        break;
                    default:
                        return null;
                }

                m_bitmapLookup.Add(texture, bitmap);
                bitmap.Tag = texture;
            }

            return bitmap;
        }

        public void RemoveThumbnail(Texture texture)
        {
            if (texture == null)
                return;

            Bitmap bitmap;
            if (m_bitmapLookup.TryGetValue(texture, out bitmap))
            {
                bitmap.Tag = null;
                bitmap.Dispose();
                m_bitmapLookup.Remove(texture);
            }
        }

        public void ClearThumbnails()
        {
            foreach (KeyValuePair<Texture, Bitmap> kv in m_bitmapLookup)
            {
                kv.Value.Tag = null;
                kv.Value.Dispose();
            }

            m_bitmapLookup.Clear();
        }

        private void CopyImporters(ContentManager defaultManager, ContentManager other)
        {
            other.ResourceImporters.Clear();
            
            foreach(IResourceImporter importer in defaultManager.ResourceImporters)
            {
                other.ResourceImporters.Add(importer);
            }
        }

        private void CopyMissingContentHandlers(ContentManager defaultManager, ContentManager other)
        {
            other.MissingContentHandlers.Clear();

            foreach(IMissingContentHandler handler in defaultManager.MissingContentHandlers)
            {
                other.MissingContentHandlers.Add(handler);
            }
        }

        private void SetupDefaultMissingHandlers(ContentManager content)
        {
            IRenderSystem renderSystem = GraphicsHelper.GetRenderSystem(content.ServiceProvider);

            content.MissingContentHandlers.Add(new MissingTextureHandler<Texture1D>(renderSystem, 32));
            content.MissingContentHandlers.Add(new MissingTextureHandler<Texture2D>(renderSystem, 32));
            content.MissingContentHandlers.Add(new MissingTextureHandler<TextureCube>(renderSystem, 32));
        }
    }
}
