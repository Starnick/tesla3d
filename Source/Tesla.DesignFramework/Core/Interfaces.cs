﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesla;
using Tesla.Graphics;
using Tesla.DesignFramework;
using Tesla.Application;
using Tesla.Content;

namespace Tesla.DesignFramework
{
    public interface IDesignApp : IEngineService
    {
        EventDispatcher Events { get; }
        DesignScene Scene { get; }
        ViewManager ViewManager { get; }
        ElementManager ElementManager { get; }
        SelectionManager SelectionManager { get; }
        DesignContentManager ContentManager { get; }
        BatchDrawManager BatchManager { get; }
        IRenderer Renderer { get; set; }
    }

    public interface ILayer : INamable
    {
        void Update(IGameTime time);

        void Render(IRenderer renderer);
    }

    public interface IGizmo : INamable
    {
        void Update(IGameTime time);

        void Render(IRenderer renderer);
    }

    public interface IViewController
    {
        Camera Camera { get; }
        View View { get; }
        IList<ILayer> Overlays { get; }

        void Update(IGameTime time);
    }
}
