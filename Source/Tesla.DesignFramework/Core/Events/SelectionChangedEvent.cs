﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesla.Graphics;

namespace Tesla.DesignFramework
{
    public class SelectionChangedEvent : EventArgs
    {
        private SelectionSet m_selectionSet;
        private List<ISpatial> m_changedObjects;
        private bool m_wasAdded;

        public SelectionSet SelectionSet
        {
            get
            {
                return m_selectionSet;
            }
        }

        public ISpatial FirstSelectedObject
        {
            get
            {
                if(m_changedObjects.Count > 0)
                    return m_changedObjects[0];

                return null;
            }
        }

        public IReadOnlyList<ISpatial> SelectedObjects
        {
            get
            {
                return m_changedObjects;
            }
        }

        public bool WasAdded
        {
            get
            {
                return m_wasAdded;
            }
        }

        public SelectionChangedEvent(SelectionSet parentSelectionSet, ISpatial selectedObj, bool wasAdded)
        {
            m_selectionSet = parentSelectionSet;
            m_changedObjects = new List<ISpatial>(1);
            m_changedObjects.Add(selectedObj);
            m_wasAdded = wasAdded;
        }

        public SelectionChangedEvent(SelectionSet parentSelectionSet, IEnumerable<ISpatial> selectedObjs, bool wasAdded)
        {
            m_selectionSet = parentSelectionSet;
            m_changedObjects = new List<ISpatial>(selectedObjs);
            m_wasAdded = wasAdded;
        }
    }
}
