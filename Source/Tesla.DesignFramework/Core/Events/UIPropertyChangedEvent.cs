﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesla.DesignFramework;

namespace Tesla.DesignFramework
{
    public class UIPropertyChangedEvent : EventArgs
    {
        private UIProperty m_prop;

        public UIProperty Property
        {
            get
            {
                return m_prop;
            }
        }

        public UIPropertyChangedEvent(UIProperty prop)
        {
            m_prop = prop;
        }
    }
}
