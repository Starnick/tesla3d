﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using Tesla.Graphics;

namespace Tesla.DesignFramework
{
  public class ThumbnailGenerator
  {
    public Bitmap RenderToBitmap(Texture2D texture, SpriteBatch batch, IRenderContext context, int minWidth, int minHeight)
    {
      int width = Math.Min(minWidth, texture.Width);
      int height = Math.Min(minHeight, texture.Height);

      RenderTarget2D rt = new RenderTarget2D(context.RenderSystem, width, height, false, SurfaceFormat.Color, DepthFormat.None);

      context.SetRenderTarget(rt);
      context.Clear(Color.Black);

      Camera cam = new Camera(new Viewport(0, 0, width, height));
      Camera oldcam = context.Camera;
      context.Camera = cam;

      batch.Begin(context);
      batch.Draw(texture, new Rectangle(0, 0, width, height), Color.White);
      batch.End();

      context.Camera = oldcam;
      context.SetRenderTarget(null);

      Bitmap image = new Bitmap(width, height, PixelFormat.Format32bppArgb);

      DataBuffer<Color> colors = DataBuffer.Create<Color>(width * height, MemoryAllocatorStrategy.DefaultPooled);
      rt.GetData<Color>(colors.Span);
      rt.Dispose();

      BitmapData data = image.LockBits(new System.Drawing.Rectangle(0, 0, width, height), ImageLockMode.WriteOnly, PixelFormat.Format32bppArgb);

      try
      {
        IntPtr ptr = data.Scan0;
        int index = 0;
        for (int y = 0; y < data.Height; y++)
        {
          for (int x = 0; x < data.Width; x++)
          {
            Color c = colors[index];
            index++;

            ColorBGRA bgra = new ColorBGRA(c.B, c.G, c.R, c.A);

            BufferHelper.Write<ColorBGRA>(ptr, bgra);
            ptr += ColorBGRA.SizeInBytes;
          }

          ptr = data.Scan0 + (data.Stride * y);
        }
      }
      finally
      {
        image.UnlockBits(data);
        colors.Dispose();
      }

      return image;
    }
  }
}
