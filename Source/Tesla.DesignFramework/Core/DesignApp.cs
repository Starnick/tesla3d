﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesla;
using Tesla.Application;
using Tesla.Graphics;
using Tesla.Content;

namespace Tesla.DesignFramework
{
  public abstract class DesignApp : IDesignApp
  {
    private ViewManager m_viewManager;
    private SelectionManager m_selectionManager;
    private ElementManager m_elementManager;
    private BatchDrawManager m_batchManager;
    private DesignContentManager m_contentManager;
    private DesignScene m_scene;
    private EventDispatcher m_events;
    private IRenderer m_renderer;

    private AppCore m_appCore;

    public ViewManager ViewManager
    {
      get
      {
        return m_viewManager;
      }
    }

    public SelectionManager SelectionManager
    {
      get
      {
        return m_selectionManager;
      }
    }

    public ElementManager ElementManager
    {
      get
      {
        return m_elementManager;
      }
    }

    public DesignContentManager ContentManager
    {
      get
      {
        return m_contentManager;
      }
    }

    public EventDispatcher Events
    {
      get
      {
        return m_events;
      }
    }

    public DesignScene Scene
    {
      get
      {
        return m_scene;
      }
    }

    public virtual String Name
    {
      get
      {
        return "DesignApp";
      }
    }

    public BatchDrawManager BatchManager
    {
      get
      {
        return m_batchManager;
      }
    }

    public IRenderer Renderer
    {
      get
      {
        return m_renderer;
      }
      set
      {
        m_renderer = value;
      }
    }

    public DesignApp()
    {
      SetupApp();
    }

    void IEngineService.Initialize(Engine engine) { }

    public void Run()
    {
      m_appCore.Run(ApplicationMode.Headless);
    }

    protected void SetupApp()
    {
      PresentationParameters pp = new PresentationParameters();
      pp.BackBufferFormat = SurfaceFormat.Color;
      pp.BackBufferWidth = 1;
      pp.BackBufferHeight = 1;
      pp.DepthStencilFormat = DepthFormat.Depth32Stencil8;
      pp.IsFullScreen = false;
      pp.PresentInterval = PresentInterval.One;
      pp.MultiSampleCount = 0;
      pp.MultiSampleQuality = 0;

      IPlatformInitializer initer = CreatePlatformInitializer();

      m_appCore = new AppCore(initer, this, pp);
    }

    protected virtual IPlatformInitializer CreatePlatformInitializer()
    {
      PresentationParameters pp = new PresentationParameters();
      pp.BackBufferFormat = SurfaceFormat.Color;
      pp.BackBufferWidth = 1;
      pp.BackBufferHeight = 1;
      pp.DepthStencilFormat = DepthFormat.Depth32Stencil8;
      pp.IsFullScreen = false;
      pp.PresentInterval = PresentInterval.One;
      pp.MultiSampleCount = 0;
      pp.MultiSampleQuality = 0;

      IPlatformInitializer initer = Platforms.CreateWindowsStandardInput();

      return initer;
    }

    protected void Load()
    {
      m_appCore.Services.AddService<IDesignApp>(this);
      m_renderer = new ForwardRenderer(m_appCore.RenderSystem.ImmediateContext);
      m_batchManager = new BatchDrawManager();
      m_events = new EventDispatcher();
      m_viewManager = new ViewManager(m_appCore.GameHost);
      m_contentManager = new DesignContentManager(this, m_appCore.Content);
      m_selectionManager = new SelectionManager(this);
      m_elementManager = new ElementManager(this);
      m_scene = new DesignScene(this);

      OnLoaded();
    }

    protected abstract void OnLoaded();

    private class AppCore : GameApplication
    {
      private DesignApp m_app;

      public AppCore(IPlatformInitializer init, DesignApp app, PresentationParameters pp)
          : base(pp, init)
      {
        m_app = app;
      }

      protected override void Render(IRenderContext context, IGameTime time)
      {
        int viewCount = m_app.ViewManager.ViewCount;

        for (int i = 0; i < viewCount; i++)
        {
          View view = m_app.ViewManager.GetView(i);
          if (view != null)
            view.Draw(m_app.m_renderer);
        }
      }

      protected override void Update(IGameTime time)
      {
        int viewCount = m_app.ViewManager.ViewCount;

        for (int i = 0; i < viewCount; i++)
        {
          View view = m_app.ViewManager.GetView(i);
          if (view != null)
            view.Update(time);
        }

        m_app.m_scene.Update(time);
      }

      protected override void ConfigureContentManager(ContentManager content)
      {
        content.ResourceImporters.Add(new TeximpTextureImporter());
        content.ResourceImporters.Add(new AssimpModelImporter());
        content.ResourceImporters.Add(new BMFontImporter());
        content.ResourceImporters.Add(new Tesla.Direct3D11.Graphics.Effects11ResourceImporter());
      }

      protected override void OnInitialize(Engine engine)
      {
        base.OnInitialize(engine);

        m_app.Load();
      }
    }
  }
}
