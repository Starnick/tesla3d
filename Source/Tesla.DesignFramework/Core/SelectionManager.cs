﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesla.Graphics;

namespace Tesla.DesignFramework
{
    public sealed class SelectedObject
    {
        private Element m_element;
        private ISpatial m_object;

        public Element RootElement
        {
            get
            {
                return m_element;
            }
        }

        public ISpatial Object
        {
            get
            {
                return m_object;
            }
        }

        public SelectedObject(Element rootElement, ISpatial selectedObject)
        {
            m_element = rootElement;
            m_object = selectedObject;
        }
    }

    public sealed class SelectionSet : IReadOnlyCollection<ISpatial>
    {
        private DesignScene m_scene;
        private HashSet<ISpatial> m_selected;

        public int Count
        {
            get
            {
                return m_selected.Count;
            }
        }

        public DesignScene Scene
        {
            get
            {
                return m_scene;
            }
        }

        public ISpatial FirstSelectedObject
        {
            get
            {
                if(m_selected.Count == 0)
                    return null;

                HashSet<ISpatial>.Enumerator enumerator = m_selected.GetEnumerator();
                if(enumerator.MoveNext())
                    return enumerator.Current;

                return null;
            }
        }

        internal SelectionSet(DesignScene scene)
        {
            m_scene = scene;
            m_selected = new HashSet<ISpatial>();
        }

        internal bool Add(ISpatial spatial)
        {
            return m_selected.Add(spatial);
        }

        internal bool AddRange(IEnumerable<ISpatial> spatials)
        {
            bool wasAdded = false;

            foreach(ISpatial s in spatials)
            {
                if(s != null)
                    wasAdded |= m_selected.Add(s);
            }

            return wasAdded;
        }

        internal bool Remove(ISpatial spatial)
        {
            return m_selected.Remove(spatial);
        }

        internal bool RemoveRange(IEnumerable<ISpatial> spatials)
        {
            bool wasRemoved = false;

            foreach(ISpatial s in spatials)
            {
                if(s != null)
                    wasRemoved |= m_selected.Remove(s);
            }

            return wasRemoved;
        }

        internal void Clear()
        {
            m_selected.Clear();
        }

        public HashSet<ISpatial>.Enumerator GetEnumerator()
        {
            return m_selected.GetEnumerator();
        }

        IEnumerator<ISpatial> IEnumerable<ISpatial>.GetEnumerator()
        {
            return m_selected.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return m_selected.GetEnumerator();
        }
    }

    public class SelectionManager
    {
        private IDesignApp m_app;
        private Dictionary<DesignScene, SelectionSet> m_selectionSets;
        private SelectedObjectRenderer m_selectedObjRenderer;

        public Color SelectedColor
        {
            get
            {
                return m_selectedObjRenderer.SelectedColor;
            }
            set
            {
                m_selectedObjRenderer.SelectedColor = value;
            }
        }

        public SelectionManager(IDesignApp app)
        {
            m_app = app;
            m_selectionSets = new Dictionary<DesignScene, SelectionSet>();
            m_selectedObjRenderer = new SelectedObjectRenderer(app, app.Renderer.RenderContext);
        }

        public bool AddToSelection(DesignScene scene, ISpatial spatial, bool removePrevious = false)
        {
            if(scene == null || spatial == null)
                return false;

            SelectionSet set;
            if(!m_selectionSets.TryGetValue(scene, out set))
            {
                set = new SelectionSet(scene);
                m_selectionSets.Add(scene, set);
            }

            if(removePrevious)
                set.Clear();

            bool wasAdded = set.Add(spatial);

            if(wasAdded)
                OnSelectionChanged(set, spatial, true);

            return wasAdded;
        } 

        public bool AddToSelection(DesignScene scene, IEnumerable<ISpatial> spatials, bool removePrevious = false)
        {
            if(scene == null || spatials == null)
                return false;

            SelectionSet set;
            if(!m_selectionSets.TryGetValue(scene, out set))
            {
                set = new SelectionSet(scene);
                m_selectionSets.Add(scene, set);
            }

            if(removePrevious)
                set.Clear();

            bool wasAdded = set.AddRange(spatials);

            if(wasAdded)
                OnSelectionChanged(set, spatials, true);

            return wasAdded;
        }

        public bool RemoveFromSelection(DesignScene scene, ISpatial spatial)
        {
            if(scene == null || spatial == null)
                return false;

            SelectionSet set;
            if(m_selectionSets.TryGetValue(scene, out set))
            {
                bool wasRemoved = set.Remove(spatial);

                if(wasRemoved)
                    OnSelectionChanged(set, spatial, false);

                return wasRemoved;
            }

            return false;
        }

        public bool RemoveFromSelection(DesignScene scene, IEnumerable<ISpatial> spatials)
        {
            if(scene == null || spatials == null)
                return false;

            SelectionSet set;
            if(m_selectionSets.TryGetValue(scene, out set))
            {
                bool wasRemoved = set.RemoveRange(spatials);

                if(wasRemoved)
                    OnSelectionChanged(set, spatials, false);

                return wasRemoved;
            }

            return false;
        }

        public SelectionSet GetSelectionSet(DesignScene scene)
        {
            if(scene == null)
                return null;

            SelectionSet set;
            if(!m_selectionSets.TryGetValue(scene, out set))
            {
                set = new SelectionSet(scene);
                m_selectionSets.Add(scene, set);
            }

            return set;
        }

        public ISpatial GetFirstSelectedObject(DesignScene scene)
        {
            if(scene == null)
                return null;

            SelectionSet set;
            if(m_selectionSets.TryGetValue(scene, out set))
            {
                return set.FirstSelectedObject;
            }

            return null;
        }

        public void ClearSelection(DesignScene scene)
        {
            if(scene == null)
                return;

            SelectionSet set;
            if(m_selectionSets.TryGetValue(scene, out set))
            {
                SelectionChangedEvent evt = new SelectionChangedEvent(set, set, false);
                set.Clear();

                m_app.Events.Dispatch<SelectionChangedEvent>(this, ref evt);
            }
        }

        internal void DrawSelection(DesignScene scene)
        {
            if (scene == null)
                return;

            SelectionSet set;
            if(m_selectionSets.TryGetValue(scene, out set) && set.Count > 0)
            {
                //Inherit the render context/camera from the main app (may have changed since we created the
                //selected obj renderer)
                m_selectedObjRenderer.RenderContext = m_app.Renderer.RenderContext;

                foreach (ISpatial s in set)
                    s.ProcessVisibleSet(m_selectedObjRenderer, false);

                m_selectedObjRenderer.Render();
            }
        }

        private void OnSelectionChanged(SelectionSet set, ISpatial obj, bool wasAdded)
        {
            SelectionChangedEvent evt = new SelectionChangedEvent(set, obj, wasAdded);
            m_app.Events.Dispatch<SelectionChangedEvent>(this, ref evt);
        }

        private void OnSelectionChanged(SelectionSet set, IEnumerable<ISpatial> objs, bool wasAdded)
        {
            SelectionChangedEvent evt = new SelectionChangedEvent(set, objs, wasAdded);
            m_app.Events.Dispatch<SelectionChangedEvent>(this, ref evt);
        }

        private class SelectedObjectRenderer : BaseRenderer
        {
            private RenderBucket m_bucket;
            private Material m_selectedObjMaterial;
            private Color m_selectedColor;

            public Color SelectedColor
            {
                get
                {
                    return m_selectedColor;
                }
                set
                {
                    m_selectedColor = value;

                    Vector4 c = m_selectedColor.ToVector4();
                    m_selectedObjMaterial.SetParameterValue<Vector3>("MatDiffuse", new Vector3(c.X, c.Y, c.Z));
                    m_selectedObjMaterial.SetParameterValue<float>("Alpha", c.Z);
                }
            }

            public SelectedObjectRenderer(IDesignApp app, IRenderContext context)
                : base(context)
            {
                CreateSelectionMaterial(app);

                //Mimic transparent render stage, but since we don't care about single-sided ness we only want the objects
                //to be sorted back-to-front
                m_bucket = new RenderBucket(RenderBucketID.Transparent, new TransparentRenderBucketComparer(), 32);
                RenderQueue.AddBucket(m_bucket);
                RenderStages.AddStage(new SimpleRenderStage(RenderBucketID.Transparent));
            }
            
            public override bool Process(IRenderable renderable)
            {
                if (renderable == null || !renderable.IsValidForDraw)
                    return false;

                return m_bucket.Add(renderable, m_selectedObjMaterial);
            }

            private void CreateSelectionMaterial(IDesignApp app)
            {
                m_selectedObjMaterial = StandardMaterialScriptLibrary.CreateStandardMaterial("Standard_Color", app.ContentManager.Default);
                SelectedColor = new Color(Color.Magenta.ToVector3(), .15f);

                DepthStencilState dss = new DepthStencilState(IRenderSystem.Current);
                dss.DepthEnable = true;
                dss.DepthWriteEnable = false;
                dss.DepthFunction = ComparisonFunction.LessEqual;

                m_selectedObjMaterial.Passes[0].DepthStencilState = dss;
                m_selectedObjMaterial.Passes[0].BlendState = BlendState.AdditiveBlend;
                m_selectedObjMaterial.Passes[0].RasterizerState = RasterizerState.CullNone;
            }
        }
    }
}
