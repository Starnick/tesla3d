﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla;
using Tesla.Graphics;

namespace Tesla.DesignFramework
{
  public class DesignGrid : IRenderable
  {
    private Vector3 m_center;
    private Triad m_axes;
    private float m_majorTileSize;
    private int m_extentMajorTileCount;
    private int m_extentMinorTileCount;
    private Color m_majorGridLineColor;
    private Color m_minorGridLineColor;
    private bool m_drawMainAxes;
    private Color m_xAxiscolor;
    private Color m_yAxisColor;
    private GridThickness m_majorGridLineThickness;
    private GridThickness m_minorGridLineThickness;
    private float m_distanceToHideMinorGrid;
    private bool m_drawMinorGrid;

    //Renderable properties
    private RenderPropertyCollection m_dummyProperties;
    private Transform m_worldTransform;
    private MaterialDefinition m_matDef;

    public Vector3 Center
    {
      get
      {
        return m_center;
      }
      set
      {
        m_center = value;
      }
    }

    public Triad Axes
    {
      get
      {
        return m_axes;
      }
      set
      {
        m_axes = value;
        m_axes.Normalize();
      }
    }

    public Plane Plane
    {
      get
      {
        return new Plane(m_axes.YAxis, m_center);
      }
      set
      {
        m_center = value.Origin;
        Triad.FromYComplementBasis(value.Normal, out m_axes);
      }
    }

    public float MajorTileSize
    {
      get
      {
        return m_majorTileSize;
      }
      set
      {
        m_majorTileSize = value;
      }
    }

    public Color MajorGridLineColor
    {
      get
      {
        return m_majorGridLineColor;
      }
      set
      {
        m_majorGridLineColor = value;
      }
    }

    public GridThickness MajorGridLineThickness
    {
      get
      {
        return m_majorGridLineThickness;
      }
      set
      {
        m_majorGridLineThickness = value;
      }
    }

    public Color MinorGridLineColor
    {
      get
      {
        return m_minorGridLineColor;
      }
      set
      {
        m_minorGridLineColor = value;
      }
    }

    public GridThickness MinorGridLineThickness
    {
      get
      {
        return m_minorGridLineThickness;
      }
      set
      {
        m_minorGridLineThickness = value;
      }
    }

    public int ExtentMajorTileCount
    {
      get
      {
        return m_extentMajorTileCount;
      }
      set
      {
        m_extentMajorTileCount = value;
      }
    }

    public int ExtentMinorTileCount
    {
      get
      {
        return m_extentMinorTileCount;
      }
      set
      {
        m_extentMinorTileCount = value;
      }
    }

    public float DistanceToHideMinorGrid
    {
      get
      {
        return m_distanceToHideMinorGrid;
      }
      set
      {
        m_distanceToHideMinorGrid = value;
      }
    }

    public bool DrawMinorGrid
    {
      get
      {
        return m_drawMinorGrid;
      }
      set
      {
        m_drawMinorGrid = value;
      }
    }

    public bool DrawMainAxes
    {
      get
      {
        return m_drawMainAxes;
      }
      set
      {
        m_drawMainAxes = value;
      }
    }

    public OrientedBoundingBox.Data Bounding
    {
      get
      {
        float extent = m_majorTileSize * m_extentMajorTileCount;
        return new OrientedBoundingBox.Data(m_center, m_axes, new Vector3(extent, 0.1f, extent));
      }
    }

    public MaterialDefinition MaterialDefinition
    {
      get
      {
        return m_matDef;
      }
      set
      {
        m_matDef = value;
      }
    }

    public Transform WorldTransform
    {
      get
      {
        return m_worldTransform;
      }
    }

    public RenderPropertyCollection RenderProperties
    {
      get
      {
        return m_dummyProperties;
      }
    }

    public bool IsValidForDraw
    {
      get
      {
        return true;
      }
    }

    public DesignGrid()
    {
      m_center = Vector3.Zero;
      m_axes = Triad.UnitAxes;
      m_majorTileSize = 1.0f;
      m_extentMajorTileCount = 20;
      m_extentMinorTileCount = 1;
      m_majorGridLineColor = Color.SlateGray;
      m_majorGridLineThickness = new GridThickness(0.5f, 0.01f, true);
      m_minorGridLineColor = Color.DarkRed;
      m_minorGridLineThickness = new GridThickness(0.05f, 0.008f, true);
      m_distanceToHideMinorGrid = float.MaxValue;
      m_drawMainAxes = true;
      m_drawMinorGrid = false;
      m_xAxiscolor = Color.Red;
      m_yAxisColor = Color.Green;

      m_matDef = new MaterialDefinition();
      m_matDef.Add(RenderBucketID.Opaque, null);

      m_worldTransform = new Transform();
      m_dummyProperties = new RenderPropertyCollection();
      m_dummyProperties.Add(new WorldTransformProperty(m_worldTransform));
    }

    public Int2 GetGridCoordinates(in Vector3 worldCoords)
    {
      Int2 gridCoords;
      GetGridCoordinates(worldCoords, m_center, m_axes, m_majorTileSize, out gridCoords);

      return gridCoords;
    }

    public bool Pick(in Ray ray, out MajorGridPickResult result)
    {
      return Pick(ray, true, true, out result);
    }

    public bool Pick(in Ray ray, bool clampOutsideGrid, out MajorGridPickResult result)
    {
      return Pick(ray, clampOutsideGrid, true, out result);
    }

    public bool Pick(in Ray ray, bool clampOutsideGrid, bool includeMinorGrid, out MajorGridPickResult result)
    {
      Plane p = Plane;
      LineIntersectionResult rayResult;
      if (!p.Intersects(ray, out rayResult))
      {
        result = new MajorGridPickResult();
        return false;
      }

      Vector3 pt = rayResult.Point;
      ComputeMajorGrid(pt, clampOutsideGrid, includeMinorGrid, out result);
      return true;
    }

    public void SetupDrawCall(IRenderContext renderContext, RenderBucketID currentBucketID, MaterialPass currentPass)
    {
    }

    public void Draw(LineBatch batch)
    {
      Draw(null, batch);
    }

    public void Draw(Camera camera, LineBatch batch)
    {
      if (batch == null)
        return;

      Vector3 right = m_axes.XAxis;
      Vector3 forward = m_axes.ZAxis;

      float minorTileSize = m_majorTileSize / (2.0f * m_extentMinorTileCount);
      float majorGridExtent = m_majorTileSize * m_extentMajorTileCount;
      Vector3 extentRight, extentForward;
      Vector3.Multiply(right, majorGridExtent, out extentRight);
      Vector3.Multiply(forward, majorGridExtent, out extentForward);

      Vector3 lowerLeft, lowerRight, upperLeft;
      Vector3.Subtract(m_center, extentRight, out lowerLeft);
      Vector3.Subtract(lowerLeft, extentForward, out lowerLeft);
      Vector3.Add(m_center, extentRight, out lowerRight);
      Vector3.Subtract(lowerRight, extentForward, out lowerRight);
      Vector3.Subtract(m_center, extentRight, out upperLeft);
      Vector3.Add(upperLeft, extentForward, out upperLeft);

      Vector3 horizStart = lowerLeft;
      Vector3 horizEnd = lowerRight;
      Vector3 vertStart = lowerLeft;
      Vector3 vertEnd = upperLeft;

      Vector3 horizStep, vertStep;
      Vector3.Multiply(forward, m_majorTileSize, out horizStep);
      Vector3.Multiply(right, m_majorTileSize, out vertStep);

      Vector3 minorHorizStep, minorVertStep;
      Vector3.Multiply(forward, minorTileSize, out minorHorizStep);
      Vector3.Multiply(right, minorTileSize, out minorVertStep);

      Vector3 minorHorizStart = horizStart;
      Vector3 minorHorizEnd = horizEnd;
      Vector3 minorVertStart = vertStart;
      Vector3 minorVertEnd = vertEnd;

      float majorGridThickness, minorGridThickness;
      int totalMajorTiles = m_extentMajorTileCount * 2;
      bool drawMinorTiles = m_drawMinorGrid;

      if (camera != null)
      {
        Plane gridPlane = new Plane(m_axes.YAxis, m_center);
        Vector3 camPos = camera.Position;
        float distToPlane = gridPlane.DistanceTo(camPos);
        drawMinorTiles = m_drawMinorGrid && distToPlane <= m_distanceToHideMinorGrid;

        if (m_majorGridLineThickness.UseCameraDistance)
          majorGridThickness = MathF.Max(m_majorGridLineThickness.MinThickness, MathF.Min(0.002f * distToPlane, m_majorGridLineThickness.MaxThickness));
        else
          majorGridThickness = (m_majorGridLineThickness.MinThickness + m_majorGridLineThickness.MaxThickness) * 0.5f;

        if (m_majorGridLineThickness.UseCameraDistance)
          minorGridThickness = MathF.Max(m_minorGridLineThickness.MinThickness, MathF.Min(0.002f * distToPlane, m_minorGridLineThickness.MaxThickness));
        else
          minorGridThickness = (m_majorGridLineThickness.MinThickness + m_majorGridLineThickness.MaxThickness) * 0.5f;
      }
      else
      {
        majorGridThickness = (m_majorGridLineThickness.MinThickness + m_majorGridLineThickness.MaxThickness) * 0.5f;
        minorGridThickness = (m_majorGridLineThickness.MinThickness + m_majorGridLineThickness.MaxThickness) * 0.5f;
      }

      for (int i = 0; i <= totalMajorTiles; i++)
      {
        if (m_drawMainAxes && i != m_extentMajorTileCount)
        {
          batch.DrawLine(horizStart, horizEnd, m_majorGridLineColor, majorGridThickness, majorGridThickness);
          batch.DrawLine(vertStart, vertEnd, m_majorGridLineColor, majorGridThickness, majorGridThickness);
        }

        minorHorizStart = horizStart;
        minorHorizEnd = horizEnd;
        minorVertStart = vertStart;
        minorVertEnd = vertEnd;

        if (drawMinorTiles && i < totalMajorTiles)
        {
          for (int j = 0; j < m_extentMinorTileCount * 2; j++)
          {
            Vector3.Add(minorHorizStart, minorHorizStep, out minorHorizStart);
            Vector3.Add(minorHorizEnd, minorHorizStep, out minorHorizEnd);
            Vector3.Add(minorVertStart, minorVertStep, out minorVertStart);
            Vector3.Add(minorVertEnd, minorVertStep, out minorVertEnd);

            batch.DrawLine(minorHorizStart, minorHorizEnd, m_minorGridLineColor, minorGridThickness, minorGridThickness);
            batch.DrawLine(minorVertStart, minorVertEnd, m_minorGridLineColor, minorGridThickness, minorGridThickness);
          }
        }

        Vector3.Add(horizStart, horizStep, out horizStart);
        Vector3.Add(horizEnd, horizStep, out horizEnd);
        Vector3.Add(vertStart, vertStep, out vertStart);
        Vector3.Add(vertEnd, vertStep, out vertEnd);
      }

      if (m_drawMainAxes)
      {
        Vector3.Multiply(horizStep, m_extentMajorTileCount, out horizStep);
        Vector3.Multiply(vertStep, m_extentMajorTileCount, out vertStep);

        Vector3.Add(lowerLeft, horizStep, out horizStart);
        Vector3.Add(lowerRight, horizStep, out horizEnd);

        Vector3.Add(lowerLeft, vertStep, out vertStart);
        Vector3.Add(upperLeft, vertStep, out vertEnd);

        Color xAxisColor = Color.Red;
        Color yAxisColor = Color.Green;
        batch.DrawLine(horizStart, horizEnd, xAxisColor, majorGridThickness, majorGridThickness);
        batch.DrawLine(vertStart, vertEnd, yAxisColor, majorGridThickness, majorGridThickness);
      }
    }

    private void ComputeMajorGrid(in Vector3 ptOnGrid, bool clamp, bool computeMinor, out MajorGridPickResult result)
    {
      Int2 gridCoords;
      GetGridCoordinates(ptOnGrid, m_center, m_axes, m_majorTileSize, out gridCoords);

      if (clamp)
      {
        gridCoords.X = MathHelper.Clamp(gridCoords.X, -m_extentMajorTileCount, m_extentMajorTileCount);
        gridCoords.Y = MathHelper.Clamp(gridCoords.Y, -m_extentMajorTileCount, m_extentMajorTileCount);
      }

      Vector3 right = m_axes.XAxis;
      Vector3 up = m_axes.ZAxis;

      Vector3.Multiply(right, gridCoords.X * m_majorTileSize, out right);
      Vector3.Multiply(up, gridCoords.Y * m_majorTileSize, out up);

      Vector3 upperRight;
      Vector3.Add(m_center, right, out upperRight);
      Vector3.Add(upperRight, up, out upperRight);

      Vector3 upperLeft;
      Vector3.Add(m_center, up, out upperLeft);

      Vector3 lowerRight;
      Vector3.Add(m_center, right, out lowerRight);

      right = m_axes.XAxis;
      up = m_axes.ZAxis;

      int xFactor = (gridCoords.X > 0) ? -1 : 1;
      int yFactor = (gridCoords.Y > 0) ? -1 : 1;
      Vector3.Multiply(right, (gridCoords.X + xFactor) * m_majorTileSize, out right);
      Vector3.Multiply(up, (gridCoords.Y + yFactor) * m_majorTileSize, out up);

      Vector3.Add(upperLeft, right, out upperLeft);
      Vector3.Add(lowerRight, up, out lowerRight);

      Vector3 lowerLeft;
      Vector3.Add(m_center, right, out lowerLeft);
      Vector3.Add(lowerLeft, up, out lowerLeft);

      float dist1 = Vector3.DistanceSquared(ptOnGrid, lowerLeft);
      float dist2 = Vector3.DistanceSquared(ptOnGrid, upperLeft);
      float dist3 = Vector3.DistanceSquared(ptOnGrid, upperRight);
      float dist4 = Vector3.DistanceSquared(ptOnGrid, lowerRight);

      int closestVertex = 0;
      float closestDist = dist1;
      for (int i = 1; i < 4; i++)
      {
        if (i == 1)
        {
          if (closestDist > dist2)
          {
            closestVertex = 1;
            closestDist = dist2;
          }
        }
        else if (i == 2)
        {
          if (closestDist > dist3)
          {
            closestVertex = 2;
            closestDist = dist3;
          }
        }
        else if (i == 3)
        {
          if (closestDist > dist4)
          {
            closestVertex = 3;
            closestDist = dist4;
          }
        }
      }

      Vector3 boxCenter;
      Vector3.Lerp(upperLeft, lowerRight, 0.5f, out boxCenter);
      float boxExtent = m_majorTileSize * 0.5f;
      OrientedBoundingBox.Data bounding = new OrientedBoundingBox.Data(boxCenter, m_axes, new Vector3(boxExtent, 0.1f, boxExtent));
      MinorGridPickResult? minorResult = null;

      if (computeMinor)
      {
        MinorGridPickResult minor;
        ComputeMinorGrid(ptOnGrid, boxCenter, out minor);
        minorResult = minor;
      }

      result = new MajorGridPickResult(gridCoords, lowerLeft, upperLeft, upperRight, lowerRight, closestVertex, ptOnGrid, bounding, minorResult);
    }

    private void ComputeMinorGrid(in Vector3 ptOnGrid, in Vector3 majorGridCenter, out MinorGridPickResult result)
    {
      float minorTileSize = m_majorTileSize / (2.0f * m_extentMinorTileCount);

      Int2 gridCoords;
      GetGridCoordinates(ptOnGrid, majorGridCenter, m_axes, minorTileSize, out gridCoords);

      //Always clamp
      gridCoords.X = MathHelper.Clamp(gridCoords.X, -m_extentMinorTileCount, m_extentMinorTileCount);
      gridCoords.Y = MathHelper.Clamp(gridCoords.Y, -m_extentMinorTileCount, m_extentMinorTileCount);

      Vector3 right = m_axes.XAxis;
      Vector3 up = m_axes.ZAxis;

      Vector3.Multiply(right, gridCoords.X * minorTileSize, out right);
      Vector3.Multiply(up, gridCoords.Y * minorTileSize, out up);

      Vector3 upperRight;
      Vector3.Add(majorGridCenter, right, out upperRight);
      Vector3.Add(upperRight, up, out upperRight);

      Vector3 upperLeft;
      Vector3.Add(majorGridCenter, up, out upperLeft);

      Vector3 lowerRight;
      Vector3.Add(majorGridCenter, right, out lowerRight);

      right = m_axes.XAxis;
      up = m_axes.ZAxis;

      int xFactor = (gridCoords.X > 0) ? -1 : 1;
      int yFactor = (gridCoords.Y > 0) ? -1 : 1;
      Vector3.Multiply(right, (gridCoords.X + xFactor) * minorTileSize, out right);
      Vector3.Multiply(up, (gridCoords.Y + yFactor) * minorTileSize, out up);

      Vector3.Add(upperLeft, right, out upperLeft);
      Vector3.Add(lowerRight, up, out lowerRight);

      Vector3 lowerLeft;
      Vector3.Add(majorGridCenter, right, out lowerLeft);
      Vector3.Add(lowerLeft, up, out lowerLeft);

      float dist1 = Vector3.DistanceSquared(ptOnGrid, lowerLeft);
      float dist2 = Vector3.DistanceSquared(ptOnGrid, upperLeft);
      float dist3 = Vector3.DistanceSquared(ptOnGrid, upperRight);
      float dist4 = Vector3.DistanceSquared(ptOnGrid, lowerRight);

      int closestVertex = 0;
      float closestDist = dist1;
      for (int i = 1; i < 4; i++)
      {
        if (i == 1)
        {
          if (closestDist > dist2)
          {
            closestVertex = 1;
            closestDist = dist2;
          }
        }
        else if (i == 2)
        {
          if (closestDist > dist3)
          {
            closestVertex = 2;
            closestDist = dist3;
          }
        }
        else if (i == 3)
        {
          if (closestDist > dist4)
          {
            closestVertex = 3;
            closestDist = dist4;
          }
        }
      }

      Vector3 boxCenter;
      Vector3.Lerp(upperLeft, lowerRight, 0.5f, out boxCenter);
      float boxExtent = minorTileSize * 0.5f;
      OrientedBoundingBox.Data bounding = new OrientedBoundingBox.Data(boxCenter, m_axes, new Vector3(boxExtent, 0.1f, boxExtent));

      result = new MinorGridPickResult(gridCoords, lowerLeft, upperLeft, upperRight, lowerRight, closestVertex, bounding);
    }

    private void GetGridCoordinates(in Vector3 worldCoords, in Vector3 gridCenter, in Triad gridAxes, float tileSize, out Int2 gridCoords)
    {
      float x, z;
      GetCoordinates(gridCenter, gridAxes, worldCoords, out x, out z);

      float xSign = (x >= 0) ? 1 : -1;
      float zSign = (z >= 0) ? 1 : -1;

      x = MathF.Floor(MathF.Abs(x) / tileSize);
      z = MathF.Floor(MathF.Abs(z) / tileSize);

      gridCoords = new Int2((int) ((x + 1) * xSign), (int) ((z + 1) * zSign));
    }

    private void GetCoordinates(in Vector3 gridCenter, in Triad gridAxes, in Vector3 ptOnPlane, out float xCoord, out float zCoord)
    {
      Vector3 diff, temp;
      Vector3.Subtract(ptOnPlane, gridCenter, out diff);

      float dot = Vector3.Dot(gridAxes.XAxis, diff);
      float sign = dot >= 0.0f ? 1.0f : -1.0f;

      Vector3.Multiply(gridAxes.XAxis, dot, out temp);
      xCoord = temp.Length() * sign;

      dot = Vector3.Dot(gridAxes.ZAxis, diff);
      sign = dot >= 0.0f ? 1.0f : -1.0f;

      Vector3.Multiply(gridAxes.ZAxis, dot, out temp);
      zCoord = temp.Length() * sign;
    }
  }
}
