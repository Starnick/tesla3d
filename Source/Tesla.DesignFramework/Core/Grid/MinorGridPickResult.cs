﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.InteropServices;
using Tesla.Content;

#nullable enable

namespace Tesla.DesignFramework
{
  /// <summary>
  /// Represents a pick result for minor grid tiles. Major pick results contain spatial information of where the
  /// grid minor-tile exists relative to the center of the major-tile as well as information on the tile itself.
  /// </summary>
  [Serializable]
  [StructLayout(LayoutKind.Sequential)]
  public struct MinorGridPickResult : IEquatable<MinorGridPickResult>, IRefEquatable<MinorGridPickResult>, IPrimitiveValue
  {
    private Int2 m_gridCoordinates;
    private Vector3 m_lowerLeft, m_upperLeft, m_upperRight, m_lowerRight;
    private int m_closestVertexIndex;
    private OrientedBoundingBox.Data m_bounding;

    /// <summary>
    /// Gets the grid coordinates of the tile in the grid, relative to the grid center.
    /// </summary>
    public readonly Int2 GridCoordinates
    {
      get
      {
        return m_gridCoordinates;
      }
    }

    /// <summary>
    /// Gets the vertex corner of the tile in the grid based on index. Vertices are indexed clockwise starting from the lower left. The index
    /// range is therefore [0, 3] exclusive.
    /// </summary>
    /// <param name="index">Zero-based corner vertex index.</param>
    /// <returns>Corner vertex</returns>
    /// <exception cref="ArgumentOutOfRange">Thrown if the index is out of range.</exception>
    public readonly Vector3 this[int index]
    {
      get
      {
        switch (index)
        {
          case 0:
            return m_lowerLeft;
          case 1:
            return m_upperLeft;
          case 2:
            return m_upperRight;
          case 3:
            return m_lowerRight;
          default:
            throw new ArgumentOutOfRangeException(StringLocalizer.Instance.GetLocalizedString("IndexOutOfRange"));
        }
      }
    }

    /// <summary>
    /// Gets the closest vertex index to the picked point.
    /// </summary>
    public readonly int ClosestVertexIndex
    {
      get
      {
        return m_closestVertexIndex;
      }
    }

    /// <summary>
    /// Gets the bounding data of the tile in the grid;
    /// </summary>
    public readonly OrientedBoundingBox.Data Bounding
    {
      get
      {
        return m_bounding;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="MinorGridPickResult"/> struct.
    /// </summary>
    /// <param name="gridCoords">Grid coordinates of the picked tile in the grid.</param>
    /// <param name="lowerLeft">Lower left vertex of the picked tile in the grid.</param>
    /// <param name="upperLeft">Upper left vertex of the picked tile in the grid.</param>
    /// <param name="upperRight">Upper right vertex of the picked tile in the grid.</param>
    /// <param name="lowerRight">Lower right vertex of the picked tile in the grid.</param>
    /// <param name="closestVertexIndex">Zero-based index of the closest vertex to the pick point, vertices are indexed clockwise starting from the lower left vertex.</param>
    /// <param name="bounding">Bounding of the tile in the grid.</param>
    public MinorGridPickResult(in Int2 gridCoords, in Vector3 lowerLeft, in Vector3 upperLeft, in Vector3 upperRight, in Vector3 lowerRight, int closestVertexIndex, in OrientedBoundingBox.Data bounding)
    {
      m_gridCoordinates = gridCoords;
      m_lowerLeft = lowerLeft;
      m_upperLeft = upperLeft;
      m_upperRight = upperRight;
      m_lowerRight = lowerRight;
      m_closestVertexIndex = MathHelper.Clamp(closestVertexIndex, 0, 3);
      m_bounding = bounding;
    }

    /// <summary>
    /// Determines whether the specified <see cref="System.Object" />, is equal to this instance.
    /// </summary>
    /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
    /// <returns><c>True</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.</returns>
    public readonly override bool Equals([NotNullWhen(true)] object? obj)
    {
      if (obj is MinorGridPickResult)
        return Equals((MinorGridPickResult) obj);

      return false;
    }

    /// <summary>
    /// Tests equality between this pick result and another.
    /// </summary>
    /// <param name="other">Pick result</param>
    /// <returns>True if equal, false otherwise.</returns>
    readonly bool IEquatable<MinorGridPickResult>.Equals(MinorGridPickResult other)
    {
      return m_gridCoordinates.Equals(other.m_gridCoordinates) && m_lowerLeft.Equals(other.m_lowerLeft) &&
          m_upperLeft.Equals(other.m_upperLeft) && m_upperRight.Equals(other.m_upperRight) &&
          m_lowerRight.Equals(other.m_lowerRight) && m_closestVertexIndex == other.m_closestVertexIndex &&
          m_bounding.Equals(other.m_bounding);
    }

    /// <summary>
    /// Tests equality between this pick result and another.
    /// </summary>
    /// <param name="other">Pick result</param>
    /// <returns>True if equal, false otherwise.</returns>
    public readonly bool Equals(in MinorGridPickResult other)
    {
      return m_gridCoordinates.Equals(other.m_gridCoordinates) && m_lowerLeft.Equals(other.m_lowerLeft) &&
          m_upperLeft.Equals(other.m_upperLeft) && m_upperRight.Equals(other.m_upperRight) &&
          m_lowerRight.Equals(other.m_lowerRight) && m_closestVertexIndex == other.m_closestVertexIndex &&
          m_bounding.Equals(other.m_bounding);
    }

    /// <summary>
    /// Tests equality between this pick result and another.
    /// </summary>
    /// <param name="other">Pick result</param>
    /// <param name="tolerance">Tolerance</param>
    /// <returns>True if equal, false otherwise.</returns>
    public readonly bool Equals(in MinorGridPickResult other, float tolerance)
    {
      return m_gridCoordinates.Equals(other.m_gridCoordinates) && m_lowerLeft.Equals(other.m_lowerLeft, tolerance) &&
          m_upperLeft.Equals(other.m_upperLeft, tolerance) && m_upperRight.Equals(other.m_upperRight, tolerance) &&
          m_lowerRight.Equals(other.m_lowerRight, tolerance) && m_closestVertexIndex == other.m_closestVertexIndex &&
          m_bounding.Equals(other.m_bounding, tolerance);
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.</returns>
    public readonly override int GetHashCode()
    {
      unchecked
      {
        return m_gridCoordinates.GetHashCode() + m_lowerLeft.GetHashCode() + m_upperLeft.GetHashCode() + m_upperRight.GetHashCode() + m_lowerRight.GetHashCode()
            + m_closestVertexIndex.GetHashCode() + m_bounding.GetHashCode();
      }
    }

    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
    public readonly override string ToString()
    {
      return String.Format("Minor Grid Coordinates: ({0}, {1}), Closest Vertex: {3}", m_gridCoordinates.X.ToString(), m_gridCoordinates.Y.ToString(), m_closestVertexIndex.ToString());
    }

    /// <summary>
    /// Writes the primitive data to the output.
    /// </summary>
    /// <param name="output">Primitive writer.</param>
    public readonly void Write(IPrimitiveWriter output)
    {
      output.Write<Int2>("GridCoordinates", m_gridCoordinates);
      output.Write<Vector3>("LowerLeftVertex", m_lowerLeft);
      output.Write<Vector3>("UpperLeftVertex", m_upperLeft);
      output.Write<Vector3>("UpperRightVertex", m_upperRight);
      output.Write<Vector3>("LowerRightVertex", m_lowerLeft);
      output.Write("ClosestVertexIndex", m_closestVertexIndex);
      output.Write<OrientedBoundingBox.Data>("Bounding", m_bounding);
    }

    /// <summary>
    /// Reads the primitive data from the input.
    /// </summary>
    /// <param name="input">Primitive reader.</param>
    public void Read(IPrimitiveReader input)
    {
      input.Read<Int2>("GridCoordinates", out m_gridCoordinates);
      input.Read<Vector3>("LowerLeftVertex", out m_lowerLeft);
      input.Read<Vector3>("UpperLeftVertex", out m_upperLeft);
      input.Read<Vector3>("UpperRightVertex", out m_upperRight);
      input.Read<Vector3>("LowerRightVertex", out m_lowerRight);
      m_closestVertexIndex = input.ReadInt32("ClosestVertexIndex");
      input.Read<OrientedBoundingBox.Data>("Bounding", out m_bounding);
    }
  }
}
