﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;

namespace Tesla.DesignFramework
{
    public class AngleFormatter : INumericalFormatter
    {
        private static char[] s_trimChars = new char[] { 'r', '°' };

        private AngleDisplayUnit m_unit;

        public AngleDisplayUnit DisplayUnit
        {
            get
            {
                return m_unit;
            }
            set
            {
                m_unit = value;
            }
        }

        public AngleFormatter()
        {
            m_unit = AngleDisplayUnit.Degrees;
        }

        public String Format(Object value, uint numPrecision, uint flags)
        {
            if(value is Angle)
            {
                Angle angle = (Angle)value;
                String format = FormatterHelper.GetFormatString(numPrecision);

                switch(m_unit)
                {
                    case AngleDisplayUnit.Degrees:
                        return angle.Degrees.ToString(format) + "°";
                    default:
                        return angle.Radians.ToString(format) + "r";
                }
            }

            return String.Empty;
        }

        public object Parse(String value, Type destinationType, uint flags)
        {
            if (String.IsNullOrEmpty(value))
                return Angle.Zero;

            value = value.TrimEnd(s_trimChars);

            float val;
            if(float.TryParse(value, out val))
            {
                switch(m_unit)
                {
                    case AngleDisplayUnit.Degrees:
                        return Angle.FromDegrees(val);
                    case AngleDisplayUnit.Radians:
                        return Angle.FromRadians(val);
                }
            }
            else
            {
                throw new ArgumentException("Invalid angle");
            }

            return Angle.Zero;
        }
    }
}
