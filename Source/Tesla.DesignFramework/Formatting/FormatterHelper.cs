﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Text;
using Tesla.Utilities;

namespace Tesla.DesignFramework
{
    public static class FormatterHelper
    {
        private static Dictionary<uint, String> s_formatStrings;
        private static Dictionary<Type, INumericalFormatter> s_formatters;

        static FormatterHelper()
        {
            s_formatStrings = new Dictionary<uint, String>();
            s_formatters = new Dictionary<Type, INumericalFormatter>();
        }

        public static String GetFormatString(uint numPrecision)
        {
            if (numPrecision == 0)
                return String.Empty;

            lock(s_formatStrings)
            {
                String returnFormat = String.Empty;

                if (!s_formatStrings.TryGetValue(numPrecision, out returnFormat))
                {
                    StringBuilder format = new StringBuilder();
                    format.Append("#0.");
                    for (int i = 0; i < numPrecision; i++)
                        format.Append("#");

                    returnFormat = format.ToString();
                    s_formatStrings.Add(numPrecision, returnFormat);
                }

                return returnFormat;
            }
        }

        public static INumericalFormatter GetFormatter(Type type)
        {
            lock(s_formatters)
            {
                INumericalFormatter formatter;
                if(!s_formatters.TryGetValue(type, out formatter))
                {
                    formatter = SmartActivator.CreateInstance(type) as INumericalFormatter;
                    s_formatters.Add(type, formatter);
                }

                return formatter;
            }
        }
    }
}
