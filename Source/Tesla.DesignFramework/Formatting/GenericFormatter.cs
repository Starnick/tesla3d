﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.ComponentModel;
using System.Globalization;

namespace Tesla.DesignFramework
{
    public class GenericFormatter : INumericalFormatter
    {
        public String Format(Object value, uint numPrecision, uint flags)
        {
            if (value == null)
                return String.Empty;

            TypeConverter converter = TypeDescriptor.GetConverter(value.GetType());
            if (converter == null)
            {
                if (value is IFormattable)
                    return FormatNumber<IFormattable>(value as IFormattable, numPrecision);

                return String.Empty;
            }

            return FormatMultipleNumbers(converter, value, numPrecision);
        }

        public Object Parse(String value, Type destinationType, uint flags)
        {
            if (destinationType == null)
                return null;

            TypeConverter converter = TypeDescriptor.GetConverter(destinationType);
            if (converter == null)
                return null;

            return converter.ConvertFromString(value);
        }

        private String FormatNumber<T>(T value, uint numPrecision) where T : IFormattable
        {
            String format = FormatterHelper.GetFormatString(numPrecision);

            return value.ToString(format, NumberFormatInfo.CurrentInfo);
        }

        private String FormatMultipleNumbers(TypeConverter converter, Object value, uint numPrecision)
        {
            PropertyDescriptorCollection props = converter.GetProperties(value);
            if(props == null || props.Count == 0)
            {
                if (value is IFormattable)
                    return FormatNumber<IFormattable>(value as IFormattable, numPrecision);

                return converter.ConvertToString(value);
            }           

            String[] values = new String[props.Count];
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                Object propValue = prop.GetValue(value);
                if (propValue == null)
                {
                    values[i] = String.Empty;
                }
                else if (propValue is IFormattable)
                {
                    values[i] = FormatNumber<IFormattable>(propValue as IFormattable, numPrecision);
                }
                else
                {
                    TypeConverter propConverter = prop.Converter;
                    if (propConverter == null)
                    {
                        values[i] = String.Empty;
                    }
                    else
                    {
                        values[i] = propConverter.ConvertToString(propValue);
                    }
                }
            }

            String separator = CultureInfo.CurrentCulture.TextInfo.ListSeparator + " ";
            return String.Join(separator, values);
        }
    }
}
