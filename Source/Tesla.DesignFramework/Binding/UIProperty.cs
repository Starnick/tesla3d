﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.ComponentModel;
using System.Drawing.Design;

namespace Tesla.DesignFramework
{
    // [TypeConverter(typeof(UIPropertyTypeConverter))]
    [RefreshProperties(RefreshProperties.All)]
    public class UIProperty
    {
        private UIInstance m_containingInstance;
        private String m_memberName;
        private UIInstance m_instance;
        private bool m_isArray;

        private String m_displayName;
        private String m_categoryName;
        private String m_description;
        private bool m_isBrowsable;
        private UITypeEditor m_editor;

        public UIInstance ContainingInstance
        {
            get
            {
                return m_containingInstance;
            }
        }

        public String MemberName
        {
            get
            {
                return m_memberName;
            }
        }

        public UIInstance Instance
        {
            get
            {
                return m_instance;
            }
        }

        public bool IsArray
        {
            get
            {
                return m_isArray;
            }
        }


        public String DisplayName
        {
            get
            {
                return m_displayName;
            }
            set
            {
                m_displayName = value;
            }
        }

        public String CategoryName
        {
            get
            {
                return m_categoryName;
            }
            set
            {
                m_categoryName = value;
            }
        }

        public String Description
        {
            get
            {
                return m_description;
            }
            set
            {
                m_description = value;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return m_instance.IsReadOnly;
            }
            set
            {
                m_instance.IsReadOnly = value;
            }
        }

        public bool IsBrowsable
        {
            get
            {
                return m_isBrowsable;
            }
            set
            {
                m_isBrowsable = value;
            }
        }

        public UITypeEditor Editor
        {
            get
            {
                return m_editor;
            }
            set
            {
                m_editor = value;
            }
        }

        public UIProperty(String memberName, UIInstance instance)
        {
            if(String.IsNullOrEmpty(memberName))
                throw new ArgumentNullException("memberName");

            if(instance == null)
                throw new ArgumentNullException("instance");

            m_memberName = memberName;
            m_instance = instance;
            m_isArray = instance.ObjectType.IsArray;

            m_displayName = memberName;
            m_categoryName = String.Empty;
            m_description = String.Empty;
            m_isBrowsable = true;
        }

        internal void SetContainingInstance(UIInstance owner)
        {
            m_containingInstance = owner;
            m_instance.SetContainingInstance(owner);
        }

        public UIInstance GetRoot()
        {
            return m_instance.GetRoot();
        }
    }
}
