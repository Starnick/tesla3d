﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

namespace Tesla.DesignFramework
{
    public sealed class Vector4UIInstance : UIInstance<Vector4>
    {
        public Vector4UIInstance()
        {
            CreateProperties();
        }

        public Vector4UIInstance(GetValue<Vector4> getter)
            : base(getter)
        {
            CreateProperties();
        }

        public Vector4UIInstance(GetValue<Vector4> getter, SetValue<Vector4> setter)
            : base(getter, setter)
        {
            CreateProperties();
        }

        public Vector4UIInstance(Vector4 value)
            : base(value)
        {
            CreateProperties();
        }

        private void CreateProperties()
        {
            AddProperty(new UIProperty("X", new UIInstance<float>(GetX, SetX))
            {
                DisplayName = "X",
                Description = StringLocalizer.Instance.GetLocalizedString("XCoordinateDisplay")
            });

            AddProperty(new UIProperty("Y", new UIInstance<float>(GetY, SetY))
            {
                DisplayName = "Y",
                Description = StringLocalizer.Instance.GetLocalizedString("YCoordinateDisplay")
            });

            AddProperty(new UIProperty("Z", new UIInstance<float>(GetZ, SetZ))
            {
                DisplayName = "Z",
                Description = StringLocalizer.Instance.GetLocalizedString("ZCoordinateDisplay")
            });

            AddProperty(new UIProperty("W", new UIInstance<float>(GetW, SetW))
            {
                DisplayName = "W",
                Description = StringLocalizer.Instance.GetLocalizedString("WCoordinateDisplay")
            });
        }

        private void GetX(out float x)
        {
            Vector4 v;
            GetValue(out v);

            x = v.X;
        }

        private void SetX(ref float x)
        {
            Vector4 v;
            GetValue(out v);

            v.X = x;
            SetValue(ref v);
        }

        private void GetY(out float y)
        {
            Vector4 v;
            GetValue(out v);

            y = v.Y;
        }

        private void SetY(ref float y)
        {
            Vector4 v;
            GetValue(out v);

            v.Y = y;
            SetValue(ref v);
        }

        private void GetZ(out float z)
        {
            Vector4 v;
            GetValue(out v);

            z = v.Z;
        }

        private void SetZ(ref float z)
        {
            Vector4 v;
            GetValue(out v);

            v.Z = z;
            SetValue(ref v);
        }

        private void GetW(out float w)
        {
            Vector4 v;
            GetValue(out v);

            w = v.W;
        }

        private void SetW(ref float w)
        {
            Vector4 v;
            GetValue(out v);

            v.W = w;
            SetValue(ref v);
        }
    }
}
