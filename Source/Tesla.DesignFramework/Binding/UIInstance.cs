﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;

namespace Tesla.DesignFramework
{
    // [TypeConverter(typeof(UIPropertyTypeConverter))]
    public abstract class UIInstance
    {
        private Type m_objectType;
        private List<UIProperty> m_properties;
        private UIInstance m_containingInstance;
        private bool m_isReadOnly;
        private object m_defaultValue;
        private UIEditorProxy m_editorProxy;

        public UIInstance ContainingInstance
        {
            get
            {
                return m_containingInstance;
            }
        }

        public Type ObjectType
        {
            get
            {
                return m_objectType;
            }
        }

        public IReadOnlyList<UIProperty> Properties
        {
            get
            {
                return m_properties;
            }
        }

        public abstract Object NativeValue
        {
            get;
            set;
        }

        public virtual Object DefaultNativeValue
        {
            get
            {
                return m_defaultValue;
            }
            set
            {
                if(value != null && !m_objectType.IsAssignableFrom(value.GetType()))
                    throw new InvalidCastException();

                m_defaultValue = value;
            }
        }

        public virtual Type FormatterType
        {
            get
            {
                return null;
            }
        }

        public virtual bool IsReadOnly
        {
            get
            {
                return m_isReadOnly;
            }
            set
            {
                m_isReadOnly = value;
                foreach (UIProperty prop in m_properties)
                    prop.IsReadOnly = value;
            }
        }

        public UIEditorProxy EditorProxy
        {
            get
            {
                return m_editorProxy;
            }
            set
            {
                m_editorProxy = value;
            }
        }

        protected UIInstance(Type type)
        {
            m_objectType = type;
            m_properties = new List<UIProperty>();
            m_containingInstance = null;
            m_isReadOnly = false;
            m_editorProxy = null;
        }

        public void AddProperty(UIProperty property)
        {
            if(property == null)
                throw new ArgumentNullException("property");

            if(property.ContainingInstance != null)
                throw new InvalidOperationException(StringLocalizer.Instance.GetLocalizedString("PropertyBelongsToAnother"));

            m_properties.Add(property);
            property.SetContainingInstance(this);
        }

        public UIProperty GetProperty(int index)
        {
            if(index >= 0 && index < m_properties.Count)
                return m_properties[index];

            return null;
        }

        public bool RemoveProperty(UIProperty property)
        {
            if(property == null || property.ContainingInstance != this)
                return false;

            m_properties.Remove(property);
            property.SetContainingInstance(null);

            return true;
        }

        public void ClearProperties()
        {
            foreach(UIProperty prop in m_properties)
                prop.SetContainingInstance(null);

            m_properties.Clear();
        }

        public UIInstance GetRoot()
        {
            if(m_containingInstance == null)
                return this;

            return m_containingInstance.GetRoot();
        }

        internal void SetContainingInstance(UIInstance instance)
        {
            m_containingInstance = instance;
        }
    }

    public class UIInstance<T> : UIInstance
    {
        private GetValue<T> m_getter;
        private SetValue<T> m_setter;
        private T m_backingValue;

        public GetValue<T> Getter
        {
            get
            {
                return m_getter;
            }
            set
            {
                m_getter = value;
            }
        }

        public SetValue<T> Setter
        {
            get
            {
                return m_setter;
            }
            set
            {
                m_setter = value;
            }
        }

        public override object NativeValue
        {
            get
            {
                T v;
                m_getter(out v);

                return v;
            }
            set
            {
                ThrowIfReadOnly();

                T v = (T) value;
                m_setter(ref v);
            }
        }

        public T Value
        {
            get
            {
                T v;
                m_getter(out v);

                return v;
            }
            set
            {
                ThrowIfReadOnly();

                m_setter(ref value);
            }
        }

        public UIInstance()
            : this(default(T))
        {
        }

        public UIInstance(GetValue<T> getter)
            : base(typeof(T))
        {
            m_getter = getter;
            m_setter = null;
            m_backingValue = default(T);
            DefaultNativeValue = default(T);
        }

        public UIInstance(GetValue<T> getter, SetValue<T> setter)
            : base(typeof(T))
        {
            m_getter = getter;
            m_setter = setter;
            m_backingValue = default(T);
            DefaultNativeValue = default(T);
        }

        public UIInstance(T value)
            : base(typeof(T))
        {
            m_getter = GetBackingValueInternal;
            m_setter = SetBackingValueInternal;
            m_backingValue = value;
            DefaultNativeValue = default(T);
        }

        public void GetValue(out T value)
        {
            m_getter(out value);
        }

        public void SetValue(ref T value)
        {
            ThrowIfReadOnly();

            m_setter(ref value);
        }

        private void GetBackingValueInternal(out T value)
        {
            value = m_backingValue;
        }

        private void SetBackingValueInternal(ref T value)
        {
            m_backingValue = value;
        }

        protected void ThrowIfReadOnly()
        {
            if(IsReadOnly || m_setter == null)
                throw new InvalidOperationException(StringLocalizer.Instance.GetLocalizedString("ValueIsReadOnly"));
        }
    }
}
