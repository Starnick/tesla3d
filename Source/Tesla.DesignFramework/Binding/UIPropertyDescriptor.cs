﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Tesla.DesignFramework
{
    public class UIPropertyDescriptor : PropertyDescriptor, ICustomTypeDescriptor
    {
        private UIProperty m_prop;
        private bool m_isExpandable;

        public UIProperty UIProperty
        {
            get
            {
                return m_prop;
            }
        }

        public bool IsExpandable
        {
            get
            {
                return m_isExpandable;
            }
        }

        public override Type ComponentType
        {
            get
            {
                return typeof(UIInstance);
            }
        }

        public override bool IsReadOnly
        {
            get
            {
                return m_prop.IsReadOnly;
            }
        }

        public override Type PropertyType
        {
            get
            {
                return typeof(UIProperty);
            }
        }

        public override string Category
        {
            get
            {
                return m_prop.CategoryName;
            }
        }

        public override string DisplayName
        {
            get
            {
                return m_prop.DisplayName;
            }
        }

        public override string Description
        {
            get
            {
                return m_prop.Description;
            }
        }

        public override bool IsBrowsable
        {
            get
            {
                return m_prop.IsBrowsable;
            }
        }

        public override string Name
        {
            get
            {
                return m_prop.MemberName;
            }
        }

        public UIPropertyDescriptor(UIProperty prop)
            : base(prop.MemberName, null)
        {
            m_prop = prop;
            m_isExpandable = prop.Instance.Properties.Count > 0 || prop.IsArray;
        }

        public override bool CanResetValue(object component)
        {
            return false;
        }

        protected override AttributeCollection CreateAttributeCollection()
        {
            return GetAttributes();
        }

        public AttributeCollection GetAttributes()
        {
            return TypeDescriptor.GetAttributes(m_prop);
        }

        public string GetClassName()
        {
            return typeof(UIProperty).Name;
        }

        public string GetComponentName()
        {
            return typeof(UIInstance).Name;
        }

        public TypeConverter GetConverter()
        {
            return null;
        }

        public EventDescriptor GetDefaultEvent()
        {
            return null;
        }

        public PropertyDescriptor GetDefaultProperty()
        {
            return null;
        }

        public EventDescriptorCollection GetEvents()
        {
            return TypeDescriptor.GetEvents(m_prop);
        }

        public EventDescriptorCollection GetEvents(Attribute[] attributes)
        {
            return TypeDescriptor.GetEvents(m_prop);
        }

        public PropertyDescriptorCollection GetProperties()
        {
            if (m_prop.Instance.Properties.Count == 0)
                return PropertyDescriptorCollection.Empty;

            IReadOnlyList<UIProperty> subProps = m_prop.Instance.Properties;
            List<UIPropertyDescriptor> propDescrs = new List<UIPropertyDescriptor>(subProps.Count);
            for (int i = 0; i < subProps.Count; i++)
            {
                UIProperty prop = subProps[i];
                if (prop.IsBrowsable)
                    propDescrs.Add(new UIPropertyDescriptor(prop));
            }

            return new PropertyDescriptorCollection(propDescrs.ToArray());
        }

        public PropertyDescriptorCollection GetProperties(Attribute[] attributes)
        {
            return GetProperties();
        }

        public override PropertyDescriptorCollection GetChildProperties(object instance, Attribute[] filter)
        {
            return GetProperties();
        }

        public object GetPropertyOwner(PropertyDescriptor pd)
        {
            return m_prop;
        }

        public override object GetValue(object component)
        {
            return m_prop.Instance.NativeValue;
        }

        public override void ResetValue(object component)
        {
        }

        public override void SetValue(object component, object value)
        {
            m_prop.Instance.NativeValue = value;
        }

        public override bool ShouldSerializeValue(object component)
        {
            return false;
        }

        public override object GetEditor(Type editorBaseType)
        {
            UIEditorProxy proxy = m_prop.Instance.EditorProxy;
            if(proxy == null)
                return null;

            return proxy.ResolveEditor();
        }
    }
}
