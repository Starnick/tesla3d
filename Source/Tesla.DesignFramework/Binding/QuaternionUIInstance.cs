﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

namespace Tesla.DesignFramework
{
    public class QuaternionUIInstance : UIInstance<Quaternion>
    {
        //TEMP 
        private Angle m_yaw, m_pitch, m_roll;

        public QuaternionUIInstance(GetValue<Quaternion> getter, SetValue<Quaternion> setter)
            : base(getter, setter)
        {
            AddProperty(new UIProperty("X", new UIInstance<float>(GetX, SetX)));
            AddProperty(new UIProperty("Y", new UIInstance<float>(GetY, SetY)));
            AddProperty(new UIProperty("Z", new UIInstance<float>(GetZ, SetZ)));
            AddProperty(new UIProperty("W", new UIInstance<float>(GetW, SetW)));

            Quaternion q;
            GetValue(out q);
            q.ToEulerAngles(out m_yaw, out m_pitch, out m_roll);        

            AddProperty(new UIProperty("Rotation X", new AngleUIInstance(GetRotationX, SetRotationX)) { IsBrowsable = false });
            AddProperty(new UIProperty("Rotation Y", new AngleUIInstance(GetRotationY, SetRotationY)) { IsBrowsable = false });
            AddProperty(new UIProperty("Rotation Z", new AngleUIInstance(GetRotationZ, SetRotationZ)) { IsBrowsable = false });
        }

        public void ToggleFormat(bool toAngles = false)
        {
            Quaternion q;
            GetValue(out q);
            q.ToEulerAngles(out m_yaw, out m_pitch, out m_roll);

            for (int i = 0; i < Properties.Count; i++)
            {
                //First four are the XYZW quaternion properties, last three are euler angles
                if(i < 4)
                {
                    Properties[i].IsBrowsable = !toAngles;
                }
                else
                {
                    Properties[i].IsBrowsable = toAngles;
                }
            }
        }

        private void GetRotationX(out Angle xRot)
        {
            xRot = m_yaw;
        }

        private void SetRotationX(ref Angle xRot)
        {
            m_yaw = xRot;

            Quaternion q;
            Quaternion.FromEulerAngles(m_yaw, m_pitch, m_roll, out q);
            SetValue(ref q);
        }

        private void GetRotationY(out Angle yRot)
        {
            yRot = m_pitch;
        }

        private void SetRotationY(ref Angle yRot)
        {
            m_pitch = yRot;

            Quaternion q;
            Quaternion.FromEulerAngles(m_yaw, m_pitch, m_roll, out q);
            SetValue(ref q);
        }

        private void GetRotationZ(out Angle zRot)
        {
            zRot = m_roll;
        }

        private void SetRotationZ(ref Angle zRot) 
        {
            m_roll = zRot;

            Quaternion q;
            Quaternion.FromEulerAngles(m_yaw, m_pitch, m_roll, out q);
            SetValue(ref q);
        }

        private void GetX(out float x)
        {
            Quaternion v;
            GetValue(out v);

            x = v.X;
        }

        private void SetX(ref float x)
        {
            Quaternion v;
            GetValue(out v);

            v.X = x;
            SetValue(ref v);
        }

        private void GetY(out float y)
        {
            Quaternion v;
            GetValue(out v);

            y = v.Y;
        }

        private void SetY(ref float y)
        {
            Quaternion v;
            GetValue(out v);

            v.Y = y;
            SetValue(ref v);
        }

        private void GetZ(out float z)
        {
            Quaternion v;
            GetValue(out v);

            z = v.Z;
        }

        private void SetZ(ref float z)
        {
            Quaternion v;
            GetValue(out v);

            v.Z = z;
            SetValue(ref v);
        }

        private void GetW(out float w)
        {
            Quaternion v;
            GetValue(out v);

            w = v.W;
        }

        private void SetW(ref float w)
        {
            Quaternion v;
            GetValue(out v);

            v.W = w;
            SetValue(ref v);
        }
    }
}
