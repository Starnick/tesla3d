﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;

namespace Tesla.DesignFramework
{
    public class UIPropertyTypeConverter : ExpandableObjectConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return (sourceType == typeof(String)) || base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            //If UIProperty, the value is the "native value" that the property instance represents, so try and get
            //a type converter to convert - if none, then return the default value
            UIPropertyDescriptor propDescr = context.PropertyDescriptor as UIPropertyDescriptor;
            if(propDescr != null)
            {
                Type targetType = propDescr.UIProperty.Instance.ObjectType;

                if (value is String && propDescr.UIProperty.Instance.FormatterType != null)
                {
                    INumericalFormatter formatter = FormatterHelper.GetFormatter(propDescr.UIProperty.Instance.FormatterType);
                    if (formatter != null)
                        return formatter.Parse(value as String, targetType, 0);
                }

                TypeConverter converter = TypeDescriptor.GetConverter(targetType);
                if(converter != null)
                {
                    return converter.ConvertFrom(context, culture, value);
                }
                else
                {
                    return Activator.CreateInstance(targetType);
                }
            }

            return base.ConvertFrom(context, culture, value);
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            //If UIProperty, the value is the "native value" that the property instance represents, so try and get a type converter
            //to convert it - if none, then do the default conversion
            UIPropertyDescriptor propDescr = context.PropertyDescriptor as UIPropertyDescriptor;
            if(propDescr != null)
            {
                if(destinationType == typeof(String) && propDescr.UIProperty.Instance.FormatterType != null)
                {
                    INumericalFormatter formatter = FormatterHelper.GetFormatter(propDescr.UIProperty.Instance.FormatterType);
                    if (formatter != null)
                        return formatter.Format(value, 3, 0);
                }

                Type srcType = propDescr.UIProperty.Instance.ObjectType;
                TypeConverter converter = TypeDescriptor.GetConverter(srcType);
                if(srcType != null)
                    return converter.ConvertTo(value, destinationType);
            }

            if(value is Tesla.Graphics.Texture2D)
            {
                return value.ToString() + "\n" + value.ToString();
            }

            return base.ConvertTo(context, culture, value, destinationType);
        }

        public override PropertyDescriptorCollection GetProperties(ITypeDescriptorContext context, object value, Attribute[] attributes)
        {
            if (context != null)
            {
                //First case - prop descriptor is a UIPropertyDescriptor so we just get the properties from that
                UIPropertyDescriptor propDescr = context.PropertyDescriptor as UIPropertyDescriptor;
                if (propDescr != null)
                    return propDescr.GetProperties();
            }

            //Second case - we bound an UIInstance directly and we need to gather properties from it
            UIInstance instance = value as UIInstance;
            if(instance != null)
            {
                IReadOnlyList<UIProperty> props = instance.Properties;
                List<UIPropertyDescriptor> propDescrs = new List<UIPropertyDescriptor>(props.Count);
                for(int i = 0; i < props.Count; i++)
                {
                    UIProperty prop = props[i];
                    if(prop.IsBrowsable)
                        propDescrs.Add(new UIPropertyDescriptor(prop));

                }

                return new PropertyDescriptorCollection(propDescrs.ToArray());
            }

            return PropertyDescriptorCollection.Empty;
        }

        public override bool GetPropertiesSupported(ITypeDescriptorContext context)
        {
            if (context == null)
                return true;

            //First case - if a UI prop descriptor then check if its expandable
            UIPropertyDescriptor propDescr = context.PropertyDescriptor as UIPropertyDescriptor;
            if(propDescr != null)
                return propDescr.IsExpandable;

            //Second case - if bound an UIInstance directly and it has properties, then return true
            UIInstance instance = context.Instance as UIInstance;
            if(instance != null && instance.Properties.Count > 0)
                return true;

            return false;
        }
    }
}
