﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;

namespace Tesla.DesignFramework
{
    public class ArrayUIInstance<T, ElementInstanceType> : UIInstance where ElementInstanceType : UIInstance<T>
    {
        private GetArray<T> m_getter;
        private SetArray<T> m_setter;
        private T[] m_backingArray;

        public GetArray<T> Getter
        {
            get
            {
                return m_getter;
            }
            set
            {
                m_getter = value;
            }
        }

        public SetArray<T> Setter
        {
            get
            {
                return m_setter;
            }
            set
            {
                m_setter = value;
            }
        }

        public override object NativeValue
        {
            get
            {
                T[] array;
                m_getter(out array);

                return array;
            }
            set
            {
                T[] array = (T[]) value;
                m_setter(ref array);

                GenerateElementProperties();
            }
        }

        public T[] Value
        {
            get
            {
                T[] array;
                m_getter(out array);

                return array;
            }
            set
            {
                ThrowIfReadOnly();

                m_setter(ref value);

                GenerateElementProperties();
            }
        }

        public T this[int index]
        {
            get
            {
                T value;
                GetElementValue(index, out value);

                return value;
            }
            set
            {
                SetElementValue(index, ref value);
            }
        }

        public ArrayUIInstance()
            : this((T[]) null)
        {
        }

        public ArrayUIInstance(GetArray<T> getter)
            : base(typeof(T[]))
        {
            m_getter = getter;
            m_setter = null;
            m_backingArray = null;
            DefaultNativeValue = null;

            GenerateElementProperties();
        }

        public ArrayUIInstance(GetArray<T> getter, SetArray<T> setter)
            : base(typeof(T[]))
        {
            m_getter = getter;
            m_setter = setter;
            m_backingArray = null;
            DefaultNativeValue = null;

            GenerateElementProperties();
        }

        public ArrayUIInstance(T[] array)
            : base(typeof(T[]))
        {
            m_backingArray = array;
            m_getter = GetBackingArrayInternal;
            m_setter = SetBackingArrayInternal;
            DefaultNativeValue = null;

            GenerateElementProperties();
        }

        public void GetValue(out T[] array)
        {
            m_getter(out array);
        }

        public void SetValue(ref T[] array)
        {
            ThrowIfReadOnly();

            m_setter(ref array);
        }

        public void GetElementValue(int index, out T value)
        {
            T[] array;
            m_getter(out array);

            if(array != null && index < array.Length)
                value = array[index];
            else
                value = default(T);
        }

        public void SetElementValue(int index, ref T value)
        {
            ThrowIfReadOnly();

            T[] array;
            m_getter(out array);

            if(array != null && index < array.Length)
                array[index] = value;
        }

        public void GenerateElementProperties()
        {
            ClearProperties();

            T[] array;
            m_getter(out array);

            if(array == null || array.Length == 0)
                return;

            object[] args = new object[2];
            Type elemInstanceType = typeof(ElementInstanceType);

            for(int i = 0; i < array.Length; i++)
            {
                int index = i;
                args[0] = new GetValue<T>((out T value) =>
                {
                    GetElementValue(index, out value);
                });

                //Set setter delegate even if read-only, so we don't have to fixup if array instance is set to writable after
                args[1] = new SetValue<T>((ref T value) =>
                {
                    SetElementValue(index, ref value);
                });

                ElementInstanceType elemInstance = (ElementInstanceType) Activator.CreateInstance(elemInstanceType, args);
                String indexText = i.ToString();
                String displayText = String.Format("[{0}]", indexText);
                UIProperty prop = new UIProperty(displayText, elemInstance);
                prop.DisplayName = displayText;
                prop.Description = StringLocalizer.Instance.GetLocalizedString("ElementAtIndexDisplay", indexText);
                AddProperty(prop);
            }
        }

        protected void ThrowIfReadOnly()
        {
            if(IsReadOnly || m_setter == null)
                throw new InvalidOperationException(StringLocalizer.Instance.GetLocalizedString("ValueIsReadOnly"));
        }

        private void GetBackingArrayInternal(out T[] array)
        {
            array = m_backingArray;
        }

        private void SetBackingArrayInternal(ref T[] array)
        {
            m_backingArray = array;
        }
    }
}
