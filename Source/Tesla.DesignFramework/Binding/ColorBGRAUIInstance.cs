﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

namespace Tesla.DesignFramework
{
    public sealed class ColorBGRAUIInstance : UIInstance<ColorBGRA>
    {
        public ColorBGRAUIInstance()
        {
            CreateProperties();
        }

        public ColorBGRAUIInstance(GetValue<ColorBGRA> getter)
            : base(getter)
        {
            CreateProperties();
        }

        public ColorBGRAUIInstance(GetValue<ColorBGRA> getter, SetValue<ColorBGRA> setter)
            : base(getter, setter)
        {
            CreateProperties();
        }

        public ColorBGRAUIInstance(ColorBGRA value)
            : base(value)
        {
            CreateProperties();
        }

        private void CreateProperties()
        {
            AddProperty(new UIProperty("B", new UIInstance<byte>(GetBlue, SetBlue))
            {
                DisplayName = "B",
                Description = StringLocalizer.Instance.GetLocalizedString("BlueComponentDisplay")
            });

            AddProperty(new UIProperty("G", new UIInstance<byte>(GetGreen, SetGreen))
            {
                DisplayName = "G",
                Description = StringLocalizer.Instance.GetLocalizedString("GreenComponentDisplay")
            });

            AddProperty(new UIProperty("R", new UIInstance<byte>(GetRed, SetRed))
            {
                DisplayName = "R",
                Description = StringLocalizer.Instance.GetLocalizedString("RedComponentDisplay")
            });

            AddProperty(new UIProperty("A", new UIInstance<byte>(GetAlpha, SetAlpha))
            {
                DisplayName = "A",
                Description = StringLocalizer.Instance.GetLocalizedString("AlphaComponentDisplay")
            });
        }

        private void GetRed(out byte r)
        {
            ColorBGRA c;
            GetValue(out c);

            r = c.R;
        }

        private void SetRed(ref byte r)
        {
            ColorBGRA c;
            GetValue(out c);

            c.R = r;
            SetValue(ref c);
        }

        private void GetGreen(out byte g)
        {
            ColorBGRA c;
            GetValue(out c);

            g = c.G;
        }

        private void SetGreen(ref byte g)
        {
            ColorBGRA c;
            GetValue(out c);

            c.G = g;
            SetValue(ref c);
        }

        private void GetBlue(out byte b)
        {
            ColorBGRA c;
            GetValue(out c);

            b = c.B;
        }

        private void SetBlue(ref byte b)
        {
            ColorBGRA c;
            GetValue(out c);

            c.B = b;
            SetValue(ref c);
        }

        private void GetAlpha(out byte a)
        {
            ColorBGRA c;
            GetValue(out c);

            a = c.A;
        }

        private void SetAlpha(ref byte a)
        {
            ColorBGRA c;
            GetValue(out c);

            c.A = a;
            SetValue(ref c);
        }
    }
}
