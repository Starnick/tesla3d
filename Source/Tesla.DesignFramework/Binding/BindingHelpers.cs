﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Tesla.DesignFramework
{
    public static class BindingHelpers
    {
        //TEMP
        public static void InitializeForWinforms()
        {
            TypeDescriptor.AddAttributes(typeof(UIProperty), new TypeConverterAttribute(typeof(UIPropertyTypeConverter)));
            TypeDescriptor.AddAttributes(typeof(UIInstance), new TypeConverterAttribute(typeof(UIPropertyTypeConverter)));
        }

        //TEMP
        public static void InitializeForWPF()
        {
            TypeDescriptor.AddAttributes(typeof(UIInstance), new TypeConverterAttribute(typeof(UIPropertyTypeConverter)));
        }

        public static UIInstance<T> CreateInstance<T, InstanceType>(GetValue<T> getter, SetValue<T> setter = null) where InstanceType : UIInstance<T>
        {
            Object[] args = new Object[2];
            args[0] = getter;
            args[1] = setter;

            return (UIInstance<T>) Activator.CreateInstance(typeof(InstanceType), args);
        }
    }
}
