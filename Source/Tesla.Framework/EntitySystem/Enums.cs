﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tesla.Framework
{
    [Flags]
    public enum TransformComponents
    {
        None = 0,
        Scale = 1,
        Rotation = 2,
        Translation = 4,
        All = Scale | Rotation | Translation
    }
}
