﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesla.Content;

namespace Tesla.Framework
{
    /// <summary>
    /// A transform changed event.
    /// </summary>
    public struct TransformChangedEvent
    {
        /// <summary>
        /// Property that has changed.
        /// </summary>
        public readonly TransformProperty Property;

        /// <summary>
        /// Which components of the transform has changed.
        /// </summary>
        public readonly TransformComponents Changes;

        /// <summary>
        /// Constructs a new instance of the <see cref="TransformChangedEvent"/> struct.
        /// </summary>
        /// <param name="property">The changed property.</param>
        /// <param name="whichComponentsChanged">Which transform components changed.</param>
        public TransformChangedEvent(TransformProperty property, TransformComponents whichComponentsChanged)
        {
            Property = property;
            Changes = whichComponentsChanged;
        }
    }

    /// <summary>
    /// A property that wraps a <see cref="Transform"/> object. If the property needs to send change events, then use the setters defined on the property rather than
    /// setting directly on the transform object. For better performance, if you need to set all the transform components, use the appropiate bulk setters so only one
    /// change event is sent.
    /// </summary>
    [SavableVersion(1)]
    public class TransformProperty : Property<Transform>
    {
        public Vector3 Scale
        {
            get
            {
                Vector3 scale;
                Value.GetScale(out scale);

                return scale;
            }
            set
            {
                SetScale(value);
            }
        }

        public Quaternion Rotation
        {
            get
            {
                Quaternion rot;
                Value.GetRotation(out rot);

                return rot;
            }
            set
            {
                SetRotation(value);
            }
        }

        public Vector3 Translation
        {
            get
            {
                Vector3 trans;
                Value.GetTranslation(out trans);

                return trans;
            }
            set
            {
                SetTranslation(value);
            }
        }

        public TransformProperty() : base(new Transform(), true) { }

        public TransformProperty(Transform transform) : base(transform, true) { }

        public TransformProperty(Transform transform, bool notifyPropertyChanged) : base(transform, notifyPropertyChanged) { }

        protected override void OnChanged()
        {
            TransformChangedEvent evt = new TransformChangedEvent(this, TransformComponents.All);
            Dispatch<TransformChangedEvent>(ref evt);
        }

        protected void NotifyComponentsChanged(TransformComponents componentsThatChanged)
        {
            if(NotifyPropertyChanged)
            {
                InNotifyChange = true;
                TransformChangedEvent evt = new TransformChangedEvent(this, componentsThatChanged);
                Dispatch<TransformChangedEvent>(ref evt);
                InNotifyChange = false;
            }
        }

        public void Set(Transform other)
        {
            if(other == null || InNotifyChange)
                return;

            Value.Set(other);

            NotifyComponentsChanged(TransformComponents.All);
        }

        public void Set(in Matrix transform)
        {
            if(InNotifyChange)
                return;

            Value.Set(transform);

            NotifyComponentsChanged(TransformComponents.All);
        }

        public void SetComponents(in Vector3 scale, in Quaternion rotation, in Vector3 translation)
        {
            if(InNotifyChange)
                return;

            Value.SetComponents(scale, rotation, translation);

            NotifyComponentsChanged(TransformComponents.All);
        }

        public void SetComponents(Quaternion rotation, Vector3 translation)
        {
            if(InNotifyChange)
                return;

            Transform transform = Value;
            transform.SetRotation(rotation);
            transform.SetTranslation(translation);

            NotifyComponentsChanged(TransformComponents.Rotation | TransformComponents.Translation);
        }

        public void SetScale(in Vector3 scale)
        {
            if(InNotifyChange)
                return;

            Value.SetScale(scale);

            NotifyComponentsChanged(TransformComponents.Scale);
        }

        public void SetRotation(in Matrix matrix)
        {
            if(InNotifyChange)
                return;

            Value.SetRotation(matrix);

            NotifyComponentsChanged(TransformComponents.Rotation);
        }

        public void SetRotation(in Quaternion rotation)
        {
            if(InNotifyChange)
                return;

            Value.SetRotation(rotation);

            NotifyComponentsChanged(TransformComponents.Rotation);
        }

        public void SetTranslation(in Vector3 translation)
        {
            if(InNotifyChange)
                return;

            Value.SetTranslation(translation);

            NotifyComponentsChanged(TransformComponents.Translation);
        }

        public override void Read(ISavableReader input)
        {
            base.Read(input);

            Vector3 scale, translation;
            Quaternion rot;

            input.Read<Vector3>("Scale", out scale);
            input.Read<Quaternion>("Rotation", out rot);
            input.Read<Vector3>("Translation", out translation);

            Transform transform = new Transform();
            transform.SetComponents(scale, rot, translation);

            SetValueNoChangeEvents(transform);
        }

        public override void Write(ISavableWriter output)
        {
            base.Write(output);

            Transform transform = Value;

            Vector3 scale, translation;
            Quaternion rot;

            transform.GetComponents(out scale, out rot, out translation);
            
            //Writing out the components ourselves rather than savable, to make writing this property in XML a little nicer
            output.Write<Vector3>("Scale", scale);
            output.Write<Quaternion>("Rotation", rot);
            output.Write<Vector3>("Translation", translation);
        }
    }
}
