﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using Tesla.Content;

namespace Tesla.Framework
{
    [SavableVersion(1)]
    public class Entity : ISavable
    {
        private String m_name;
        private Dictionary<ComponentTypeID, IComponent> m_components;
        private List<IBehavior> m_behaviors;
        private BehaviorComparer m_sorter;
        private bool m_behaviorsNeedSorting;
        private EventDispatcher m_dispatcher;
        private World m_world;
        private int m_id;
        private int m_templateId;

        public EventDispatcher EventDispatcher
        {
            get
            {
                return m_dispatcher;
            }
        }

        public int ID
        {
            get
            {
                return m_id;
            }
        }

        public int TemplateId
        {
            get
            {
                return m_templateId;
            }
            set
            {
                m_templateId = value;
            }
        }

        public World World
        {
            get
            {
                return m_world;
            }
        }

        public String Name
        {
            get
            {
                return m_name;
            }
            set
            {
                m_name = value;
            }
        }

        public int ComponentCount
        {
            get
            {
                return m_components.Count;
            }
        }

        public IEnumerable<IComponent> Components
        {
            get
            {
                return m_components.Values;
            }
        }

        public IReadOnlyList<IBehavior> Behaviors
        {
            get
            {
                return m_behaviors;
            }
        }

        internal bool BehaviorsNeedSorting
        {
            get
            {
                return m_behaviorsNeedSorting;
            }
            set
            {
                m_behaviorsNeedSorting = value;
            }
        }

        public Entity() : this("Entity") { }

        public Entity(String name)
        {
            m_components = new Dictionary<ComponentTypeID, IComponent>();
            m_name = name;
            m_dispatcher = new EventDispatcher();
            m_behaviors = new List<IBehavior>();
            m_sorter = new BehaviorComparer();
            m_behaviorsNeedSorting = true;
        }

        public bool Add(IComponent component)
        {
            if(component == null || component.TypeID == ComponentTypeID.NullTypeID || m_components.ContainsKey(component.TypeID))
                return false;

            m_components.Add(component.TypeID, component);
            component.OnAttach(this);

            if(component is IBehavior)
            {
                m_behaviors.Add(component as IBehavior);
                m_behaviorsNeedSorting = true;
            }

            return true;
        }

        public bool Remove<T>() where T : class, IComponent
        {
            ComponentTypeID typeId = ComponentTypeID.GetTypeID<T>();

            if(typeId == ComponentTypeID.NullTypeID)
                return false;

            IComponent component;
            if(m_components.TryGetValue(typeId, out component))
            {
                if(component is IBehavior)
                {
                    m_behaviors.Remove(component as IBehavior);
                    m_behaviorsNeedSorting = true;
                }

                m_components.Remove(typeId);
                component.OnRemove(this);
                return true;
            }

            return false;
        }

        public bool Remove(ComponentTypeID typeId)
        {
            if(typeId == ComponentTypeID.NullTypeID)
                return false;

            IComponent component;
            if(m_components.TryGetValue(typeId, out component))
            {
                if(component is IBehavior)
                {
                    m_behaviors.Remove(component as IBehavior);
                    m_behaviorsNeedSorting = true;
                }

                m_components.Remove(typeId);
                component.OnRemove(this);
                return true;
            }

            return false;
        }

        public T Get<T>() where T : class, IComponent
        {
            ComponentTypeID typeId = ComponentTypeID.GetTypeID<T>();

            if(typeId == ComponentTypeID.NullTypeID)
                return default(T);

            IComponent component;
            if(m_components.TryGetValue(typeId, out component))
                return component as T;

            return default(T);
        }

        public IComponent Get(ComponentTypeID typeId)
        {
            if(typeId == ComponentTypeID.NullTypeID)
                return null;

            IComponent component;
            if(m_components.TryGetValue(typeId, out component))
                return component;

            return null;
        }

        public bool Contains<T>() where T : class, IComponent
        {
            ComponentTypeID typeId = ComponentTypeID.GetTypeID<T>();

            return m_components.ContainsKey(typeId);
        }

        public bool Contains(ComponentTypeID typeId)
        {
            return m_components.ContainsKey(typeId);
        }

        public bool TryGet<T>(out T component) where T : class, IComponent
        {
            component = null;

            ComponentTypeID typeId = ComponentTypeID.GetTypeID<T>();

            if(typeId == ComponentTypeID.NullTypeID)
                return false;

            IComponent comp;
            if(m_components.TryGetValue(typeId, out comp))
                component = comp as T;

            return component != null;
        }

        public bool TryGet(ComponentTypeID typeId, out IComponent component)
        {
            component = null;

            if(typeId == ComponentTypeID.NullTypeID)
                return false;

            IComponent comp;
            if(m_components.TryGetValue(typeId, out comp))
                component = comp;

            return component != null;
        }

        public void Update(IGameTime time)
        {
            for(int i = 0; i < m_behaviors.Count; i++)
                m_behaviors[i].Update(time);
        }

        public void SortBehaviors()
        {
            if(m_behaviorsNeedSorting)
                m_behaviors.Sort(m_sorter);

            m_behaviorsNeedSorting = false;
        }

        internal void SetWorldInfo(int id, World world)
        {
            m_id = id;
            m_world = world;
            m_dispatcher.SetParent((world != null) ? world.EventDispatcher : null);

            SortBehaviors();

            bool isBorn = world != null;
            foreach(KeyValuePair<ComponentTypeID, IComponent> kv in m_components)
            {
                if(isBorn)
                    kv.Value.OnEntityBorn();
                else
                    kv.Value.OnEntityDead();
            }
        }

        public void Read(ISavableReader input)
        {
            m_name = input.ReadString("Name");
            int count = input.BeginReadGroup("Components");

            for(int i = 0; i < count; i++)
            {
                IComponent component = input.ReadSavable<IComponent>("Component");
                Add(component);
            }

            input.EndReadGroup();
        }

        public void Write(ISavableWriter output)
        {
            output.Write("Name", m_name);
            output.BeginWriteGroup("Components", m_components.Count);

            foreach(KeyValuePair<ComponentTypeID, IComponent> kv in m_components)
            {
                output.WriteSavable<IComponent>("Component", kv.Value);
            }

            output.EndWriteGroup();
        }

        private sealed class BehaviorComparer : IComparer<IBehavior>
        {
            public int Compare(IBehavior x, IBehavior y)
            {
                //Smaller numbers denote higher priority
                int xOrder = x.UpdatePriority;
                int yOrder = y.UpdatePriority;

                if(xOrder < yOrder)
                    return -1;
                else if(xOrder > yOrder)
                    return 1;

                return 0;
            }
        }
    }
}
