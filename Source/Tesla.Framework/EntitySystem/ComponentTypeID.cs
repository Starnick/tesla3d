﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading;

namespace Tesla.Framework
{
    /// <summary>
    /// Represents a unique value for identifying components.
    /// </summary>
    [Serializable]
    [StructLayout(LayoutKind.Sequential, Size = 4)]
    public struct ComponentTypeID : IEquatable<ComponentTypeID>, IComparable<ComponentTypeID>
    {
        private int m_idValue;

        /// <summary>
        /// Gets the null type ID value.
        /// </summary>
        public static ComponentTypeID NullTypeID
        {
            get
            {
                return new ComponentTypeID(0);
            }
        }

        /// <summary>
        /// Gets the integer value of the Type ID.
        /// </summary>
        public int Value
        {
            get
            {
                return m_idValue;
            }
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="ComponentTypeID"/> struct.
        /// </summary>
        /// <param name="idValue">The integer Type ID value.</param>
        internal ComponentTypeID(int idValue)
        {
            m_idValue = idValue;
        }

        /// <summary>
        /// Gets the <see cref="ComponentTypeID"/> associated with the <see cref="Type"/>.
        /// </summary>
        /// <typeparam name="T">Type of component.</typeparam>
        /// <returns>Type ID of component.</returns>
        public static ComponentTypeID GetTypeID<T>()
        {
            return ComponentTypeIDHolder<T>.TypeID;
        }

        /// <summary>
        /// Gets the <see cref="ComponentTypeID"/> associated with the <see cref="Type"/>.
        /// </summary>
        /// <param name="typeOf">Type of component.</param>
        /// <returns>Type ID of component.</returns>
        public static ComponentTypeID GetTypeID(Type typeOf)
        {
            if(typeOf == null)
                return NullTypeID;

            return ComponentTypeIDGenerator.GetID(typeOf);
        }

        /// <summary>
        /// Gets the <see cref="Type"/> associated with the <see cref="ComponentTypeID"/>.
        /// </summary>
        /// <param name="typeID">Type ID of component.</param>
        /// <returns>Type of component.</returns>
        public static Type GetTypeOf(ComponentTypeID typeID)
        {
            if(typeID == ComponentTypeID.NullTypeID)
                return typeof(NullComponent);

            return ComponentTypeIDGenerator.GetType(typeID);
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
        public override int GetHashCode()
        {
            unchecked
            {
                return m_idValue;
            }
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object" />, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns><c>True</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.</returns>
        public override bool Equals(object obj)
        {
            if(obj is ComponentTypeID)
                return ((ComponentTypeID) obj).m_idValue == m_idValue;

            return false;
        }

        /// <summary>
        /// Compares the current object with another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>A value that indicates the relative order of the objects being compared. The return value has the following meanings: Value Meaning Less than zero This object is less than the <paramref name="other" /> parameter.Zero This object is equal to <paramref name="other" />. Greater than zero This object is greater than <paramref name="other" />.</returns>
        public int CompareTo(ComponentTypeID other)
        {
            if(m_idValue < other.m_idValue)
                return -1;

            if(m_idValue > other.m_idValue)
                return 1;

            return 0;
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>True if the current object is equal to the <paramref name="other" /> parameter; otherwise, false.</returns>
        public bool Equals(ComponentTypeID other)
        {
            return other.m_idValue == m_idValue;
        }

        /// <summary>
        /// Implicitly converts the Type ID to an integer value.
        /// </summary>
        /// <param name="id">Type ID.</param>
        /// <returns>Integer value.</returns>
        public static implicit operator int(ComponentTypeID id)
        {
            return id.m_idValue;
        }

        /// <summary>
        /// Implicitly converts the integer value to a Type ID.
        /// </summary>
        /// <param name="idValue">Integer value.</param>
        /// <returns>Render property ID.</returns>
        public static implicit operator ComponentTypeID(int idValue)
        {
            return new ComponentTypeID(idValue);
        }

        /// <summary>
        /// Checks inequality between two Type IDs.
        /// </summary>
        /// <param name="a">First ID.</param>
        /// <param name="b">Second ID.</param>
        /// <returns>True if the two values are not the same, false otherwise.</returns>
        public static bool operator !=(ComponentTypeID a, ComponentTypeID b)
        {
            return a.m_idValue != b.m_idValue;
        }

        /// <summary>
        /// Checks equality between two Type IDs.
        /// </summary>
        /// <param name="a">First ID.</param>
        /// <param name="b">Second ID.</param>
        /// <returns>True if the two values are the same, false otherwise.</returns>
        public static bool operator ==(ComponentTypeID a, ComponentTypeID b)
        {
            return a.m_idValue == b.m_idValue;
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
        public override String ToString()
        {
            return m_idValue.ToString();
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <param name="formatProvider">The format provider.</param>
        /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
        public String ToString(IFormatProvider formatProvider)
        {
            if(formatProvider == null)
                return ToString();

            return m_idValue.ToString(formatProvider);
        }
    }

    internal static class ComponentTypeIDHolder<T>
    {
        public static readonly ComponentTypeID TypeID;

        static ComponentTypeIDHolder()
        {
            TypeID = ComponentTypeIDGenerator.GetID(typeof(T));
        }
    }

    internal static class ComponentTypeIDGenerator
    {
        private const UInt32 DefaultPolynomial = 0xedb88320;
        private const UInt32 DefaultSeed = 0xffffffff;

        private static Dictionary<Type, ComponentTypeID> s_typesToIDs = new Dictionary<Type, ComponentTypeID>();
        private static Dictionary<ComponentTypeID, Type> s_IDsToTypes = new Dictionary<ComponentTypeID, Type>();
        private static UInt32[] s_defaultTable;

        static ComponentTypeIDGenerator()
        {
            s_defaultTable = InitializeTable(DefaultPolynomial);

            s_typesToIDs = new Dictionary<Type, ComponentTypeID>();
            s_IDsToTypes = new Dictionary<ComponentTypeID, Type>();
        }

        private static UInt32[] InitializeTable(UInt32 polynomial)
        {
            UInt32[] createTable = new UInt32[256];
            for (int i = 0; i < 256; i++)
            {
                UInt32 entry = (UInt32)i;
                for (int j = 0; j < 8; j++)
                    if ((entry & 1) == 1)
                        entry = (entry >> 1) ^ polynomial;
                    else
                        entry = entry >> 1;
                createTable[i] = entry;
            }

            return createTable;
        }

        //CRC-32 algorithm to generate a consistent hash for a type. The hash is generated based on the assembly qualified name
        public static ComponentTypeID GenerateID(Type type)
        {
            if (type == null)
                throw new ArgumentNullException("type");

            String name = type.AssemblyQualifiedName;

            UInt32 crc = DefaultSeed;
            for (int i = 0; i < name.Length; i++)
            {
                char c = name[i];

                byte lower = (byte)c;
                byte upper = (byte)(c << 8);

                crc = (crc >> 8) ^ s_defaultTable[lower ^ crc & 0xff];
                crc = (crc >> 8) ^ s_defaultTable[upper ^ crc & 0xff];
            }

            ComponentTypeID typeId = new ComponentTypeID((int)~crc);

            System.Diagnostics.Debug.Assert(typeId != ComponentTypeID.NullTypeID);
            return typeId;
        }

        public static ComponentTypeID GetID(Type type)
        {
            if (type == null)
                throw new ArgumentNullException("type");

            ComponentTypeID typeId;
            if (s_typesToIDs.TryGetValue(type, out typeId))
            {
                return typeId;
            }
            else
            {
                lock (s_typesToIDs)
                {
                    typeId = (type == typeof(NullComponent)) ? ComponentTypeID.NullTypeID : GenerateID(type);
                    s_typesToIDs.Add(type, typeId);
                    s_IDsToTypes.Add(typeId, type);

                    return typeId;
                }
            }
        }

        public static Type GetType(ComponentTypeID typeId)
        {
            if (typeId == ComponentTypeID.NullTypeID)
                return typeof(NullComponent);

            Type type;
            if (s_IDsToTypes.TryGetValue(typeId, out type))
                return type;

            return null;
        }
    }
}
