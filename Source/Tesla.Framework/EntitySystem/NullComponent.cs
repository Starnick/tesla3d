﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Content;

namespace Tesla.Framework
{
    /// <summary>
    /// Represents the null component.
    /// </summary>
    [SavableVersion(1)]
    public sealed class NullComponent : IComponent
    {
        /// <summary>
        /// The null component value.
        /// </summary>
        public static readonly NullComponent Value = new NullComponent();

        /// <summary>
        /// Gets the unique type ID that represents this component.
        /// </summary>
        public ComponentTypeID TypeID
        {
            get 
            {
                return ComponentTypeID.NullTypeID;
            }
        }

        /// <summary>
        /// Gets the entity that this component is attached to. Each component can only be attached to one parent at a time.
        /// </summary>
        public Entity Parent
        {
            get 
            {
                return null;
            }
        }

        private NullComponent() { }

        /// <summary>
        /// Called when the component is added to the entity.
        /// </summary>
        /// <param name="parent">Entity</param>
        public void OnAttach(Entity parent)
        {
        }

        /// <summary>
        /// Called when the component is removed from an entity.
        /// </summary>
        /// <param name="parent">Entity</param>
        public void OnRemove(Entity parent)
        {
        }

        /// <summary>
        /// Called when the entity has been added to the world.
        /// </summary>
        public void OnEntityBorn()
        {
        }

        /// <summary>
        /// Called when the entity has been removed from the world.
        /// </summary>
        public void OnEntityDead()
        {
        }

        /// <summary>
        /// Reads the object data from the input.
        /// </summary>
        /// <param name="input">Savable reader</param>
        public void Read(ISavableReader input)
        {
        }

        /// <summary>
        /// Writes the object data to the output.
        /// </summary>
        /// <param name="output">Savable writer</param>
        public void Write(ISavableWriter output)
        {
        }
    }
}
