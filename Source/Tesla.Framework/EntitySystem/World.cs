﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace Tesla.Framework
{
    public class World : IReadOnlyDictionary<int, Entity>
    {
        private EventDispatcher m_dispatcher;
        private Dictionary<int, Entity> m_entities;
        private int m_currId;

        public event TypedEventHandler<World, Entity> AddedEntity;
        public event TypedEventHandler<World, Entity> RemovedEntity;

        public EventDispatcher EventDispatcher
        {
            get
            {
                return m_dispatcher;
            }
        }

        public Dictionary<int, Entity>.KeyCollection EntityIDs
        {
            get
            {
                return m_entities.Keys;
            }
        }

        public Dictionary<int, Entity>.ValueCollection Entities
        {
            get
            {
                return m_entities.Values;
            }
        }

        IEnumerable<int> IReadOnlyDictionary<int, Entity>.Keys
        {
            get
            {
                return m_entities.Keys;
            }
        }

        IEnumerable<Entity> IReadOnlyDictionary<int, Entity>.Values
        {
            get
            {
                return m_entities.Values;
            }
        }

        public int Count
        {
            get
            {
                return m_entities.Count;
            }
        }

        public Entity this[int entityId]
        {
            get
            {
                Entity entity;
                if(m_entities.TryGetValue(entityId, out entity))
                    return entity;

                return null;
            }
        }

        public World()
        {
            m_dispatcher = new EventDispatcher();
            m_entities = new Dictionary<int, Entity>();
            m_currId = 0;
        }

        public void DispatchToAll<T>(Entity source, ref T msg)
        {
            //Will it be legal to have the world dispatcher to have a parent?
            m_dispatcher.Dispatch<T>(source, ref msg, true);

            //Dispatch to each entity, but do not propagate up
            foreach (KeyValuePair<int, Entity> kv in m_entities)
                kv.Value.EventDispatcher.Dispatch<T>(source, ref msg, false); 
        }

        public void Add(Entity entity)
        {
            if(entity == null || entity.World != null)
                return;

            entity.SetWorldInfo(GetNewID(), this);
            m_entities.Add(entity.ID, entity);

            NotifyEntityAdded(entity);
        }

        public bool Contains(int entityId)
        {
            return m_entities.ContainsKey(entityId);
        }

        public bool Remove(Entity entity)
        {
            if(entity == null || entity.World != this)
                return false;

            NotifyEntityRemoved(entity);

            m_entities.Remove(entity.ID);
            entity.SetWorldInfo(0, null);

            if (m_entities.Count == 0)
                ResetIDs();

            return true;
        }

        public bool Remove(int entityId)
        {
            if(entityId <= 0)
                return false;

            Entity entToRemove;
            if(!m_entities.TryGetValue(entityId, out entToRemove))
                return false;

            NotifyEntityRemoved(entToRemove);

            m_entities.Remove(entityId);
            entToRemove.SetWorldInfo(0, null);

            if(m_entities.Count == 0)
                ResetIDs();

            return true;
        }

        public void Clear()
        {
            foreach(KeyValuePair<int, Entity> kv in m_entities)
            {
                NotifyEntityRemoved(kv.Value);
                kv.Value.SetWorldInfo(0, null);
            }

            m_entities.Clear();
            ResetIDs();
        }

        public void Update(IGameTime time)
        {
            foreach(KeyValuePair<int, Entity> kv in m_entities)
                kv.Value.Update(time);
        }

        public void RenumberEntities()
        {
            ResetIDs();

            Dictionary<int, Entity> newEntities = new Dictionary<int, Entity>();

            foreach(KeyValuePair<int, Entity> kv in m_entities)
            {
                int oldId = kv.Value.ID;
                kv.Value.SetWorldInfo(GetNewID(), this);

                EntityIDChangedEvent evt = new EntityIDChangedEvent(kv.Value, oldId);
                DispatchToAll<EntityIDChangedEvent>(kv.Value, ref evt);

                newEntities.Add(kv.Value.ID, kv.Value);
            }

            m_entities.Clear();
            m_entities = newEntities;
        }

        private int GetNewID()
        {
            int nextId = Interlocked.Increment(ref m_currId);

            return nextId;
        }

        private void ResetIDs()
        {
            Interlocked.Exchange(ref m_currId, 0);
        }

        private void NotifyEntityAdded(Entity entity)
        {
            AddedEntity?.Invoke(this, entity);
        }

        private void NotifyEntityRemoved(Entity entity)
        {
            RemovedEntity?.Invoke(this, entity);
        }

        bool IReadOnlyDictionary<int, Entity>.ContainsKey(int key)
        {
            return m_entities.ContainsKey(key);
        }

        bool IReadOnlyDictionary<int, Entity>.TryGetValue(int key, out Entity value)
        {
            return m_entities.TryGetValue(key, out value);
        }

        public Dictionary<int, Entity>.Enumerator GetEnumerator()
        {
            return m_entities.GetEnumerator();
        }

        IEnumerator<KeyValuePair<int, Entity>> IEnumerable<KeyValuePair<int, Entity>>.GetEnumerator()
        {
            return m_entities.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return m_entities.GetEnumerator();
        }
    }
}
