﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using Tesla.Content;

namespace Tesla.Framework
{
    /// <summary>
    /// A behavior is a component that contains logic.
    /// </summary>
    [SavableVersion(1)]
    public abstract class Behavior : Component, IBehavior
    {
        private int m_updatePriority;

        public int UpdatePriority
        {
            get 
            {
                return m_updatePriority;
            }
            set
            {
                m_updatePriority = value;

                if(m_updatePriority != value && Parent != null)
                    Parent.BehaviorsNeedSorting = true;
            }
        }

        public abstract void Update(IGameTime gameTime);

        public override void Read(ISavableReader input)
        {
            base.Read(input);

            m_updatePriority = input.ReadInt32("UpdatePriority");
        }

        public override void Write(ISavableWriter output)
        {
            base.Write(output);

            output.Write("UpdatePriority", m_updatePriority);
        }
    }
}
