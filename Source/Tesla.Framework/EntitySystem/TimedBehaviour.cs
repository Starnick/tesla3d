﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Content;

namespace Tesla.Framework
{
    [SavableVersion(1)]
    public abstract class TimedBehaviour : Behavior
    {
        private TimedAction m_timer;

        public TimeSpan UpdateInterval
        {
            get
            {
                return m_timer.UpdateInterval;
            }
            set
            {
                m_timer.UpdateInterval = value;
            }
        }

        public bool TriggerOnce
        {
            get
            {
                return m_timer.TriggerOnce;
            }
            set
            {
                m_timer.TriggerOnce = value;
            }
        }

        public TimedBehaviour() : this(TimeSpan.Zero) { }

        public TimedBehaviour(TimeSpan timeSpan)
        {
            m_timer = new TimedAction(timeSpan, DoUpdate);
        }
 
        public override void Update(IGameTime gameTime)
        {
            m_timer.CheckAndPerform(gameTime);
        }

        protected abstract void DoUpdate(IGameTime gameTime);
    }
}
