﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using Tesla.Content;

namespace Tesla.Framework
{
    [SavableVersion(1)]
    public abstract class StateBehavior : Behavior
    {
        private Dictionary<Type, StateLogic> m_statePool;
        private StateLogic m_currentState;

        public StateBehavior()
        {
            m_statePool = new Dictionary<Type, StateLogic>();
        }

        public override void Update(IGameTime gameTime)
        {
            if(m_currentState != null)
            {
                StateLogic oldState = null;

                //If we transitioned to the next state, perform an update on it
                while(oldState != m_currentState)
                {
                    oldState = m_currentState;
                    m_currentState.Update(gameTime);
                }
            }
        }

        protected void Prompt<T>() where T : StateLogic, new()
        {
            Type type = typeof(T);
            StateLogic state;
            if(m_statePool.TryGetValue(type, out state))
            {
                m_currentState = state;
                state.Initialize();
            }
            else
            {
                m_currentState = new T();
                m_currentState.SetBehaviorParent(this);
                m_statePool.Add(type, m_currentState);
                m_currentState.Initialize();
            }
        }

        protected abstract class StateLogic
        {
            private StateBehavior m_behavior;

            public StateBehavior Behavior
            {
                get
                {
                    return m_behavior;
                }
            }

            public Entity ParentEntity
            {
                get
                {
                    return m_behavior.Parent;
                }
            }

            internal void SetBehaviorParent(StateBehavior parent)
            {
                m_behavior = parent;
            }

            internal virtual void Initialize() 
            {
                OnInitialize();
            }

            internal virtual void CleanUp()
            {
                OnCleanUp();
            }

            internal virtual void Update(IGameTime time)
            {
                OnUpdate(time);
            }

            protected virtual void OnInitialize() { }
            protected virtual void OnCleanUp() { }
            protected virtual void OnUpdate(IGameTime time) { }

            protected T BehaviorAs<T>() where T : StateBehavior
            {
                return m_behavior as T;
            }

            protected void Prompt<T>() where T : StateLogic, new()
            {
                m_behavior.Prompt<T>();
            }
        }

        protected abstract class TimedStateLogic : StateLogic
        {
            private TimedAction m_timer;
            private bool m_needsReset = false;

            public TimeSpan UpdateInterval
            {
                get
                {
                    return m_timer.UpdateInterval;
                }
                set
                {
                    m_timer.UpdateInterval = value;
                }
            }

            public bool TriggerOnce
            {
                get
                {
                    return m_timer.TriggerOnce;
                }
                set
                {
                    m_timer.TriggerOnce = value;
                }
            }

            public TimedStateLogic()
            {
                m_timer = new TimedAction(TimeSpan.Zero, OnUpdate);
            }

            internal override void Initialize()
            {
                m_needsReset = true;

                OnInitialize();
            }

            internal override void Update(IGameTime time)
            {
                if(m_needsReset)
                {
                    m_timer.Reset(time);
                    m_needsReset = false;
                }

                m_timer.CheckAndPerform(time);
            }
        }
    }
}
