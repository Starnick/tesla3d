﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesla.Content;

namespace Tesla.Framework
{
    [SavableVersion(1)]
    public abstract class Property<T> : Component, IProperty<T>
    {
        private T m_value;
        private bool m_notifyChanged;
        private bool m_inNotify;

        public T Value
        {
            get
            {
                return m_value;
            }
            set
            {
                if(!m_inNotify && !IsEqual(ref m_value, ref value))
                {
                    m_value = value;

                    if(m_notifyChanged)
                    {
                        m_inNotify = true;
                        OnChanged();
                        m_inNotify = false;
                    }
                }
            }
        }

        public bool NotifyPropertyChanged
        {
            get
            {
                return m_notifyChanged;
            }

            set
            {
                m_notifyChanged = value;
            }
        }

        protected bool InNotifyChange
        {
            get
            {
                return m_inNotify;
            }
            set
            {
                m_inNotify = value;
            }
        }

        protected Property(T value, bool notifyPropertyChanged)
        {
            m_value = value;
            m_notifyChanged = notifyPropertyChanged;
        }

        protected abstract void OnChanged();

        protected virtual bool IsEqual(ref T oldValue, ref T newValue)
        {
            return EqualityComparer<T>.Default.Equals(oldValue, newValue);
        }

        protected void Dispatch<TMessage>(ref TMessage msg)
        {
            Entity parent = Parent;
            System.Diagnostics.Debug.Assert(parent != null);

            if(parent != null)
                parent.EventDispatcher.Dispatch<TMessage>(parent, ref msg);
        }

        protected void Dispatch<TMessage>(TMessage msg)
        {
            Dispatch<TMessage>(ref msg);
        }

        protected void SetValueNoChangeEvents(in T value)
        {
            m_value = value;
        }

        public void GetValue(out T value)
        {
            value = m_value;
        }

        public override void Read(ISavableReader input)
        {
            m_notifyChanged = input.ReadBoolean("NotifyPropertyChanged");
        }

        public override void Write(ISavableWriter output)
        {
            output.Write("NotifyPropertyChanged", m_notifyChanged);
        }
    }

    /// <summary>
    /// Contains abstract subclasses of <see cref="Property{T}"/> where read/write methods are implemented.
    /// </summary>
    public static class Property
    {
        [SavableVersion(1)]
        public abstract class Savable<TSavable> : Property<TSavable> where TSavable : class, ISavable
        {
            protected Savable(TSavable value, bool notifyPropertyChanged) : base(value, notifyPropertyChanged) { }

            public override void Read(ISavableReader input)
            {
                base.Read(input);

                TSavable savable = input.ReadSavable<TSavable>("Value");
                SetValueNoChangeEvents(savable);
            }

            public override void Write(ISavableWriter output)
            {
                base.Write(output);

                output.WriteSavable<TSavable>("Value", Value);
            }
        }

        [SavableVersion(1)]
        public abstract class Primitive<TPrimitive> : Property<TPrimitive> where TPrimitive : struct, IPrimitiveValue
        {
            protected Primitive(TPrimitive value, bool notifyPropertyChanged) : base(value, notifyPropertyChanged) { }

            public override void Read(ISavableReader input)
            {
                base.Read(input);

                TPrimitive value;
                input.Read<TPrimitive>("Value", out value);
                SetValueNoChangeEvents(value);
            }

            public override void Write(ISavableWriter output)
            {
                base.Write(output);

                TPrimitive value = Value;
                output.Write<TPrimitive>("Value", value);
            }
        }

        [SavableVersion(1)]
        public abstract class Vector2 : Property.Primitive<Tesla.Vector2>
        {
            private float m_tolerance = MathHelper.ZeroTolerance;

            public float Tolerance
            {
                get
                {
                    return m_tolerance;
                }
                set
                {
                    m_tolerance = value;
                }
            }

            protected Vector2(Tesla.Vector2 value, bool notifyPropertyChanged) : base(value, notifyPropertyChanged) { }

            protected override bool IsEqual(ref Tesla.Vector2 oldValue, ref Tesla.Vector2 newValue)
            {
                return oldValue.Equals(newValue, m_tolerance);
            }
        }

        [SavableVersion(1)]
        public abstract class Vector3 : Property.Primitive<Tesla.Vector3>
        {
            private float m_tolerance = MathHelper.ZeroTolerance;

            public float Tolerance
            {
                get
                {
                    return m_tolerance;
                }
                set
                {
                    m_tolerance = value;
                }
            }

            protected Vector3(Tesla.Vector3 value, bool notifyPropertyChanged) : base(value, notifyPropertyChanged) { }

            protected override bool IsEqual(ref Tesla.Vector3 oldValue, ref Tesla.Vector3 newValue)
            {
                return oldValue.Equals(newValue, m_tolerance);
            }
        }

        [SavableVersion(1)]
        public abstract class Vector4 : Property.Primitive<Tesla.Vector4>
        {
            private float m_tolerance = MathHelper.ZeroTolerance;

            public float Tolerance
            {
                get
                {
                    return m_tolerance;
                }
                set
                {
                    m_tolerance = value;
                }
            }

            protected Vector4(Tesla.Vector4 value, bool notifyPropertyChanged) : base(value, notifyPropertyChanged) { }

            protected override bool IsEqual(ref Tesla.Vector4 oldValue, ref Tesla.Vector4 newValue)
            {
                return oldValue.Equals(newValue, m_tolerance);
            }
        }

        [SavableVersion(1)]
        public abstract class Angle : Property.Primitive<Tesla.Angle>
        {
            private float m_tolerance = MathHelper.ZeroTolerance;

            public float Tolerance
            {
                get
                {
                    return m_tolerance;
                }
                set
                {
                    m_tolerance = value;
                }
            }

            protected Angle(Tesla.Angle value, bool notifyPropertyChanged) : base(value, notifyPropertyChanged) { }

            protected override bool IsEqual(ref Tesla.Angle oldValue, ref Tesla.Angle newValue)
            {
                return oldValue.Equals(newValue, m_tolerance);
            }
        }

        [SavableVersion(1)]
        public abstract class Byte : Property<byte>
        {
            protected Byte(byte value, bool notifyPropertyChanged) : base(value, notifyPropertyChanged) { }

            public override void Read(ISavableReader input)
            {
                base.Read(input);

                byte value = input.ReadByte("Value");
                SetValueNoChangeEvents(value);
            }

            public override void Write(ISavableWriter output)
            {
                base.Write(output);

                output.Write("Value", Value);
            }
        }

        [SavableVersion(1)]
        public abstract class Short : Property<short>
        {
            protected Short(short value, bool notifyPropertyChanged) : base(value, notifyPropertyChanged) { }

            public override void Read(ISavableReader input)
            {
                base.Read(input);

                short value = input.ReadInt16("Value");
                SetValueNoChangeEvents(value);
            }

            public override void Write(ISavableWriter output)
            {
                base.Write(output);

                output.Write("Value", Value);
            }
        }

        [SavableVersion(1)]
        public abstract class Int : Property<int>
        {
            protected Int(int value, bool notifyPropertyChanged) : base(value, notifyPropertyChanged) { }

            public override void Read(ISavableReader input)
            {
                base.Read(input);

                int value = input.ReadInt32("Value");
                SetValueNoChangeEvents(value);
            }

            public override void Write(ISavableWriter output)
            {
                base.Write(output);

                output.Write("Value", Value);
            }
        }

        [SavableVersion(1)]
        public abstract class Long : Property<long>
        {
            protected Long(long value, bool notifyPropertyChanged) : base(value, notifyPropertyChanged) { }

            public override void Read(ISavableReader input)
            {
                base.Read(input);

                long value = input.ReadInt64("Value");
                SetValueNoChangeEvents(value);
            }

            public override void Write(ISavableWriter output)
            {
                base.Write(output);

                output.Write("Value", Value);
            }
        }

        [SavableVersion(1)]
        public abstract class Float : Property<float>
        {
            private float m_tolerance = MathHelper.ZeroTolerance;

            public float Tolerance
            {
                get
                {
                    return m_tolerance;
                }
                set
                {
                    m_tolerance = value;
                }
            }

            protected Float(float value, bool notifyPropertyChanged) : base(value, notifyPropertyChanged) { }

            public override void Read(ISavableReader input)
            {
                base.Read(input);

                float value = input.ReadSingle("Value");
                SetValueNoChangeEvents(value);
            }

            public override void Write(ISavableWriter output)
            {
                base.Write(output);

                output.Write("Value", Value);
            }

            protected override bool IsEqual(ref float oldValue, ref float newValue)
            {
                return MathHelper.IsEqual(oldValue, newValue, m_tolerance);
            }
        }

        [SavableVersion(1)]
        public abstract class Double : Property<double>
        {
            private double m_tolerance = (double)MathHelper.ZeroTolerance;

            public double Tolerance
            {
                get
                {
                    return m_tolerance;
                }
                set
                {
                    m_tolerance = value;
                }
            }

            protected Double(double value, bool notifyPropertyChanged) : base(value, notifyPropertyChanged) { }

            public override void Read(ISavableReader input)
            {
                base.Read(input);

                double value = input.ReadDouble("Value");
                SetValueNoChangeEvents(value);
            }

            public override void Write(ISavableWriter output)
            {
                base.Write(output);

                output.Write("Value", Value);
            }

            protected override bool IsEqual(ref double oldValue, ref double newValue)
            {
                return Math.Abs(oldValue - newValue) <= m_tolerance;
            }
        }
    }
}
