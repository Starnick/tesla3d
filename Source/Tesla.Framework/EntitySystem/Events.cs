﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tesla.Framework
{
    public struct EntityIDChangedEvent
    {
        public readonly Entity Entity;
        public readonly int OldID;
        public readonly int NewID;

        public EntityIDChangedEvent(Entity entity, int oldID)
        {
            Entity = entity;
            OldID = oldID;
            NewID = entity.ID;
        }
    }
}
