﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using Tesla.Content;

namespace Tesla.Framework
{
    /// <summary>
    /// Defines a component that can be added to an entity. A component is either data ("Attributes") or logic ("Behaviors").
    /// Each component has a unique ID associated with its runtime type and each entity can have only one type of component.
    /// </summary>
    public interface IComponent : ISavable
    {
        /// <summary>
        /// Gets the unique type ID that represents this component.
        /// </summary>
        ComponentTypeID TypeID { get; }

        /// <summary>
        /// Gets the entity that this component is attached to. Each component can only be attached to one parent at a time.
        /// </summary>
        Entity Parent { get; }

        /// <summary>
        /// Called when the component is added to the entity.
        /// </summary>
        /// <param name="parent">Entity</param>
        void OnAttach(Entity parent);

        /// <summary>
        /// Called when the component is removed from an entity.
        /// </summary>
        /// <param name="parent">Entity</param>
        void OnRemove(Entity parent);

        /// <summary>
        /// Called when the entity has been added to the world.
        /// </summary>
        void OnEntityBorn();

        /// <summary>
        /// Called when the entity has been removed from the world.
        /// </summary>
        void OnEntityDead();
    }

    /// <summary>
    /// Defines a component that contains logic.
    /// </summary>
    public interface IBehavior : IComponent
    {
        /// <summary>
        /// Gets or sets the update priority. Smaller values represent a higher priority.
        /// </summary>
        int UpdatePriority { get; set; }

        /// <summary>
        /// Performs the logic update.
        /// </summary>
        /// <param name="gameTime">Time elapsed since the last update.</param>
        void Update(IGameTime gameTime);
    }

    /// <summary>
    /// Defines a component that contains data.
    /// </summary>
    public interface IProperty : IComponent
    {
        /// <summary>
        /// Gets or sets if an event should be sent if the property changes. The event is sent through the dispatcher of the entity that the component is attached to.
        /// </summary>
        bool NotifyPropertyChanged { get; set; }
    }


    /// <summary>
    /// Defines a component that contains data.
    /// </summary>
    public interface IProperty<T> : IProperty
    {
        /// <summary>
        /// Gets or sets the property value.
        /// </summary>
        T Value { get; set; }
    }
}
