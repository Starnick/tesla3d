﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;

namespace Tesla.Framework
{
    /// <summary>
    /// Handler that is registered to an entity's event dispatcher and that is called when the event type is sent (e.g. a property changed). 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="source"></param>
    /// <param name="msg"></param>
    public delegate void EntityEventHandler<T>(Entity source, ref T msg);

    /// <summary>
    /// Dispatcher for entity events. Every entity and world has a dispatcher that handlers can subscribe to. This allows for
    /// communication between components of the same entity so they don't have to be coupled, or between entities, or for interested
    /// parties to listen for messages from any entity at the world level.
    /// messages at the world level
    /// </summary>
    public sealed class EventDispatcher
    {
        private Dictionary<Type, List<Delegate>> m_eventDelegates;
        private EventDispatcher m_parent;

        /// <summary>
        /// Constructs a new instance of the <see cref="EventDispatcher"/> class.
        /// </summary>
        public EventDispatcher()
        {
            m_eventDelegates = new Dictionary<Type, List<Delegate>>();
        }

        /// <summary>
        /// Subscribes the handler to the specified event type. This does not filter out duplicates.
        /// </summary>
        /// <typeparam name="T">Event type</typeparam>
        /// <param name="handler">Handler that is to be added.</param>
        /// <returns>True if the handler was added, false if otherwise.</returns>
        public bool Subscribe<T>(EntityEventHandler<T> handler)
        {
            if(handler == null)
                return false;

            Type type = typeof(T);
            List<Delegate> list;
            if(!m_eventDelegates.TryGetValue(type, out list))
            {
                list = new List<Delegate>(1);
                m_eventDelegates.Add(type, list);
            }

            list.Add(handler);
            return true;
        }

        /// <summary>
        /// Queries if the handler is already subscribed to the specified event type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="handler"></param>
        /// <returns></returns>
        public bool IsSubscribed<T>(EntityEventHandler<T> handler)
        {
            if(handler == null)
                return false;

            Type type = typeof(T);
            List<Delegate> list;
            if(m_eventDelegates.TryGetValue(type, out list))
                return list.Contains(handler);

            return false;
        }

        /// <summary>
        /// Removes the handler that is subscribed to the specified event type.
        /// </summary>
        /// <typeparam name="T">Event type</typeparam>
        /// <param name="handler">Handler that is to be removed.</param>
        /// <returns>True if the handler was removed, false if otherwise.</returns>
        public bool Unsubscribe<T>(EntityEventHandler<T> handler)
        {
            if(handler == null)
                return false;

            Type type = typeof(T);
            List<Delegate> list;
            if(m_eventDelegates.TryGetValue(type, out list))
                return list.Remove(handler);

            return false;
        }

        /// <summary>
        /// Clears all event subscriptions for the specified event type.
        /// </summary>
        /// <typeparam name="T">Event type</typeparam>
        public void Unsubscribe<T>()
        {
            Type type = typeof(T);
            List<Delegate> list;
            if(m_eventDelegates.TryGetValue(type, out list))
                list.Clear();
        }

        /// <summary>
        /// Clears all event subscriptions for every event type.
        /// </summary>
        public void UnsubscribeAll()
        {
            m_eventDelegates.Clear();
        }

        /// <summary>
        /// Dispatches the specified event to all handlers that are subscribed to it.
        /// </summary>
        /// <typeparam name="T">Entity event type.</typeparam>
        /// <param name="source">Entity that is sender the event.</param>
        /// <param name="msg">Event message</param>
        /// <param name="propagateToParent">True if the event should propagate upward to the dispatcher's parent (e.g. another entity, the world). By default this is true.</param>
        public void Dispatch<T>(Entity source, ref T msg, bool propagateToParent = true)
        {
            if(source == null)
                return;

            Type type = typeof(T);
            List<Delegate> list;
            if(m_eventDelegates.TryGetValue(type, out list))
            {
                foreach(EntityEventHandler<T> handler in list)
                    handler(source, ref msg);
            }

            if(propagateToParent && m_parent != null)
                m_parent.Dispatch<T>(source, ref msg, propagateToParent);
        }

        internal void SetParent(EventDispatcher parent)
        {
            m_parent = parent;
        }
    }
}
