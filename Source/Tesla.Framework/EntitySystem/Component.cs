﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using Tesla.Content;

namespace Tesla.Framework
{
    /// <summary>
    /// Base class for components.
    /// </summary>
    [SavableVersion(1)]
    public abstract class Component : IComponent
    {
        private ComponentTypeID m_typeID;
        private Entity m_parent;

        /// <summary>
        /// Gets the unique type ID that represents this component.
        /// </summary>
        public ComponentTypeID TypeID
        {
            get
            {
                return m_typeID;
            }
        }

        /// <summary>
        /// Gets the entity that this component is attached to. Each component can only be attached to one parent at a time.
        /// </summary>
        public Entity Parent
        {
            get
            {
                return m_parent;
            }
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="Component"/> class.
        /// </summary>
        protected Component()
        {
            m_typeID = ComponentTypeID.GetTypeID(GetType());
        }

        /// <summary>
        /// Called when the component is added to the entity.
        /// </summary>
        /// <param name="parent">Entity</param>
        public virtual void OnAttach(Entity parent)
        {
            m_parent = parent;
        }

        /// <summary>
        /// Called when the component is removed from an entity.
        /// </summary>
        /// <param name="parent">Entity</param>
        public virtual void OnRemove(Entity parent)
        {
            m_parent = null;
        }

        /// <summary>
        /// Called when the entity has been added to the world.
        /// </summary>
        public virtual void OnEntityBorn() { }

        /// <summary>
        /// Called when the entity has been removed from the world.
        /// </summary>
        public virtual void OnEntityDead() { }

        /// <summary>
        /// Reads the object data from the input.
        /// </summary>
        /// <param name="input">Savable reader</param>
        public virtual void Read(ISavableReader input) { }

        /// <summary>
        /// Writes the object data to the output.
        /// </summary>
        /// <param name="output">Savable writer</param>
        public virtual void Write(ISavableWriter output) { }
    }
}
