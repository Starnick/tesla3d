﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Input;

#nullable enable

namespace Tesla.Framework
{
  /// <summary>
  /// Input trigger that responds to input and performs a <see cref="ZoomInputAction"/>.
  /// </summary>
  public class ZoomInputTrigger : InputTrigger
  {
    /// <summary>
    /// Constructs a new instance of the <see cref="ZoomInputTrigger"/> class.
    /// </summary>
    /// <param name="zoomCmd">Zoom action delegate.</param>
    public ZoomInputTrigger(Action<int, IGameTime> zoomCmd)
        : this("Zoom", zoomCmd)
    {
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="ZoomInputTrigger"/> class.
    /// </summary>
    /// <param name="name">Trigger name.</param>
    /// <param name="zoomCmd">Zoom action delegate.</param>
    public ZoomInputTrigger(String name, Action<int, IGameTime> zoomCmd)
        : base(name, new MouseWheelScrollCondition(), new ZoomInputAction(zoomCmd))
    {
    }
  }

  /// <summary>
  /// Zoom input action where mouse wheel movements determine how much a camera should zoom in/out (usually translated along a direction vector).
  /// </summary>
  public class ZoomInputAction : CursorInputAction
  {
    private int m_prevWheelValue;
    private Action<int, IGameTime> m_zoomCmd;

    /// <summary>
    /// Constructs a new instance of the <see cref="ZoomInputAction"/> class.
    /// </summary>
    /// <param name="zoomCmd">Zoom action delegate.</param>
    /// <exception cref="ArgumentNullException">Thrown if the zoom action delegate is null.</exception>
    public ZoomInputAction(Action<int, IGameTime> zoomCmd)
    {
      if (zoomCmd is null)
        throw new ArgumentNullException(nameof(zoomCmd));

      m_zoomCmd = zoomCmd;
    }

    /// <summary>
    /// Performs the action.
    /// </summary>
    /// <param name="time">Time elapsed since the last update.</param>
    public override void Perform(IGameTime time)
    {
      MouseState ms = Mouse.GetMouseState();

      int wheelValue = ms.ScrollWheelValue;
      int delta = wheelValue - m_prevWheelValue;

      if (delta != 0)
        m_zoomCmd(delta, time);

      m_prevWheelValue = wheelValue;
    }
  }
}
