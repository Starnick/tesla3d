﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Input;

#nullable enable

namespace Tesla.Framework
{
  /// <summary>
  /// Input trigger that responds to input and performs a <see cref="RotateInputAction"/>.
  /// </summary>
  public class RotateInputTrigger : InputTrigger
  {
    /// <summary>
    /// Constructs a new instance of the <see cref="RotateInputTrigger"/> class.
    /// </summary>
    /// <param name="binding">Button binding.</param>
    /// <param name="rotateCmd">Rotate action delegate.</param>
    public RotateInputTrigger(KeyOrMouseButton binding, Action<Int2, IGameTime> rotateCmd)
        : this("Rotate", binding, rotateCmd)
    {
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="RotateInputTrigger"/> class.
    /// </summary>
    /// <param name="name">Trigger name.</param>
    /// <param name="binding">Button binding.</param>
    /// <param name="rotateCmd">Rotate action delegate.</param>
    public RotateInputTrigger(String name, KeyOrMouseButton binding, Action<Int2, IGameTime> rotateCmd)
        : base(name, KeyOrMouseButton.CreateInputCondition(binding, false, true), new RotateInputAction(rotateCmd))
    {
    }
  }

  /// <summary>
  /// Rotate input action where mouse movements determine how much a camera should be rotated.
  /// </summary>
  public class RotateInputAction : CursorInputAction
  {
    private Int2 m_prevScreenPt;
    private Action<Int2, IGameTime> m_rotateCmd;

    /// <summary>
    /// Constructs a new instance of the <see cref="RotateInputAction"/> class.
    /// </summary>
    /// <param name="rotateCmd">Rotate action delegate.</param>
    /// <exception cref="ArgumentNullException">Thrown if the rotate action delegate is null.</exception>
    public RotateInputAction(Action<Int2, IGameTime> rotateCmd)
    {
      if (rotateCmd is null)
        throw new ArgumentNullException(nameof(rotateCmd));

      m_rotateCmd = rotateCmd;
      Cursor = Cursors.Rotate;
    }

    /// <summary>
    /// Called by an input trigger when the condition first succeeds, allowing the action to do first-time setup.
    /// </summary>
    /// <param name="time"></param>
    public override void OnBegin(IGameTime time)
    {
      base.OnBegin(time);

      MouseState ms = Mouse.GetMouseState();
      m_prevScreenPt = ms.PositionInt;
    }

    /// <summary>
    /// Performs the action.
    /// </summary>
    /// <param name="time">Time elapsed since the last update.</param>
    public override void Perform(IGameTime time)
    {
      MouseState ms = Mouse.GetMouseState();

      Int2 screenPt = ms.PositionInt;
      Int2 delta;
      Int2.Subtract(screenPt, m_prevScreenPt, out delta);

      if (delta != Int2.Zero)
        m_rotateCmd(delta, time);

      m_prevScreenPt = screenPt;
    }
  }
}
