﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Input;

#nullable enable

namespace Tesla.Framework
{
  /// <summary>
  /// Input action that applies a cursor at the start of the action and restores the previous cursor at the end.
  /// </summary>
  public abstract class CursorInputAction : InputAction
  {
    private String m_cursor;
    private String m_prevCursor;

    /// <summary>
    /// Gets or sets the cursor name that will be applied.
    /// </summary>
    public String Cursor
    {
      get
      {
        return m_cursor;
      }
      set
      {
        if (String.IsNullOrEmpty(value))
          m_cursor = Cursors.Default;
        else
          m_cursor = value;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="CursorInputAction"/> class.
    /// </summary>
    public CursorInputAction()
    {
      m_cursor = m_prevCursor = Cursors.Default;
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="CursorInputAction"/> class.
    /// </summary>
    /// <param name="action">Input action delegate</param>
    public CursorInputAction(InputActionDelegate action)
        : base(action)
    {
      m_cursor = m_prevCursor = Cursors.Default;
    }

    /// <summary>
    /// Called by an input trigger when the condition first succeeds, allowing the action to do first-time setup.
    /// </summary>
    /// <param name="time"></param>
    public override void OnBegin(IGameTime time)
    {
      SetCursor();
    }

    /// <summary>
    /// Called by an input trigger when the condition fails, allowing the action to do last-time cleanup.
    /// </summary>
    /// <param name="time"></param>
    public override void OnEnd(IGameTime time)
    {
      ResetCursor();
    }

    /// <summary>
    /// Applies the cursor.
    /// </summary>
    protected void SetCursor()
    {
      m_prevCursor = Cursors.Cursor;
      Cursors.Cursor = m_cursor;
    }

    /// <summary>
    /// Restores the previous cursor state.
    /// </summary>
    protected void ResetCursor()
    {
      Cursors.Cursor = m_prevCursor;
    }
  }
}
