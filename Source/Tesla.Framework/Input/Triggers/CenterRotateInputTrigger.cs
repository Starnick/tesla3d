﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Graphics;
using Tesla.Input;

#nullable enable

namespace Tesla.Framework
{
  /// <summary>
  /// Input trigger that responds to input and performs a <see cref="CenterRotateInputAction"/>.
  /// </summary>
  public class CenterRotateInputTrigger : InputTrigger
  {
    /// <summary>
    /// Constructs a new instance of the <see cref="CenterRotateInputTrigger"/> class. This uses an input condition where the
    /// mouse has to move.
    /// </summary>
    /// <param name="rotateCmd">Rotate action delegate.</param>
    /// <param name="cam">The camera that has a viewport whose center point is used for the input action.</param>
    public CenterRotateInputTrigger(Action<Int2, IGameTime> rotateCmd, Camera cam)
        : this("Rotate", rotateCmd, cam)
    {
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="CenterRotateInputTrigger"/> class. This uses an input condition where the
    /// mouse has to move.
    /// </summary>
    /// <param name="name">Trigger name.</param>
    /// <param name="rotateCmd">Rotate action delegate.</param>
    /// <param name="cam">The camera that has a viewport whose center point is used for the input action.</param>
    public CenterRotateInputTrigger(String name, Action<Int2, IGameTime> rotateCmd, Camera cam)
        : base(name, new MouseMovedCondition(), new CenterRotateInputAction(rotateCmd, cam))
    {
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="CenterRotateInputTrigger"/> class.
    /// </summary>
    /// <param name="binding">Button binding.</param>
    /// <param name="rotateCmd">Rotate action delegate.</param>
    /// <param name="cam">The camera that has a viewport whose center point is used for the input action.</param>
    public CenterRotateInputTrigger(KeyOrMouseButton binding, Action<Int2, IGameTime> rotateCmd, Camera cam)
        : this("Rotate", binding, rotateCmd, cam)
    {
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="CenterRotateInputTrigger"/> class.
    /// </summary>
    /// <param name="name">Trigger name.</param>
    /// <param name="binding">Button binding.</param>
    /// <param name="rotateCmd">Rotate action delegate.</param>
    /// <param name="cam">The camera that has a viewport whose center point is used for the input action.</param>
    public CenterRotateInputTrigger(String name, KeyOrMouseButton binding, Action<Int2, IGameTime> rotateCmd, Camera cam)
        : base(name, KeyOrMouseButton.CreateInputCondition(binding, false, true), new CenterRotateInputAction(rotateCmd, cam))
    {
    }
  }

  /// <summary>
  /// Rotate input action where the rotation always happens from the center of the viewport of the defined camera (e.g. a first person
  /// look around action). This will always set the mouse to the center of the viewport.
  /// </summary>
  public class CenterRotateInputAction : CursorInputAction
  {
    private Action<Int2, IGameTime> m_rotateCmd;
    private Camera m_camera;

    /// <summary>
    /// Constructs a new instance of the <see cref="CenterRotateInputAction"/> class.
    /// </summary>
    /// <param name="rotateCmd">Rotate action delegate.</param>
    /// <param name="cam">Camera to center the mouse to.</param>
    /// <exception cref="ArgumentNullException">Thrown if the rotate action delegate or the camera is null.</exception>
    public CenterRotateInputAction(Action<Int2, IGameTime> rotateCmd, Camera cam)
    {
      if (rotateCmd is null)
        throw new ArgumentNullException(nameof(rotateCmd));

      if (cam is null)
        throw new ArgumentNullException(nameof(cam));

      m_rotateCmd = rotateCmd;
      m_camera = cam;
      Cursor = Cursors.Default;
    }

    /// <summary>
    /// Performs the action.
    /// </summary>
    /// <param name="time">Time elapsed since the last update.</param>
    public override void Perform(IGameTime time)
    {
      MouseState ms = Mouse.GetMouseState();

      Int2 screenPt = ms.PositionInt;
      Int2 delta;

      //Get the camera's viewport and determine a delta from the center to the current mouse location
      Viewport vp = m_camera.Viewport;
      Int2 center = new Int2(vp.Width / 2, vp.Height / 2);

      Int2.Subtract(screenPt, center, out delta);

      if (delta != Int2.Zero)
        m_rotateCmd(delta, time);

      //Set the the mouse location to the center of the viewport
      Mouse.SetPosition(center.X, center.Y);
    }
  }
}
