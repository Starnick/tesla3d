﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Input;

#nullable enable

namespace Tesla.Framework
{
  /// <summary>
  /// Input trigger that responds to input and performs a <see cref="PanInputAction"/>.
  /// </summary>
  public class PanInputTrigger : InputTrigger
  {
    /// <summary>
    /// Constructs a new instance of the <see cref="PanInputTrigger"/> class.
    /// </summary>
    /// <param name="binding">Button binding.</param>
    /// <param name="panCmd">Pan action delegate.</param>
    public PanInputTrigger(KeyOrMouseButton binding, Action<Int2, IGameTime> panCmd)
        : this("Pan", binding, panCmd)
    {
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="PanInputTrigger"/> class.
    /// </summary>
    /// <param name="name">Trigger name.</param>
    /// <param name="binding">Button binding.</param>
    /// <param name="panCmd">Pan action delegate.</param>
    public PanInputTrigger(String name, KeyOrMouseButton binding, Action<Int2, IGameTime> panCmd)
        : base(name, KeyOrMouseButton.CreateInputCondition(binding, false, true), new PanInputAction(panCmd))
    {
    }
  }

  /// <summary>
  /// Pan input action where mouse movements determine how much a camera should be translated across the pan axes.
  /// </summary>
  public class PanInputAction : CursorInputAction
  {
    private Action<Int2, IGameTime> m_panCmd;
    private Int2 m_prevScreenPt;

    /// <summary>
    /// Constructs a new instance of the <see cref="PanInputAction"/> class.
    /// </summary>
    /// <param name="panCmd">Pan action delegate</param>
    /// <exception cref="ArgumentNullException">Thrown if the pan action delegate is null.</exception>
    public PanInputAction(Action<Int2, IGameTime> panCmd)
    {
      if (panCmd is null)
        throw new ArgumentNullException(nameof(panCmd));

      m_panCmd = panCmd;
      Cursor = Cursors.ClosedHand;
    }

    /// <summary>
    /// Called by an input trigger when the condition first succeeds, allowing the action to do first-time setup.
    /// </summary>
    /// <param name="time"></param>
    public override void OnBegin(IGameTime time)
    {
      base.OnBegin(time);

      MouseState ms = Mouse.GetMouseState();
      m_prevScreenPt = ms.PositionInt;
    }

    /// <summary>
    /// Performs the action.
    /// </summary>
    /// <param name="time">Time elapsed since the last update.</param>
    public override void Perform(IGameTime time)
    {
      MouseState ms = Mouse.GetMouseState();

      Int2 screenPt = ms.PositionInt;
      Int2 delta;
      Int2.Subtract(screenPt, m_prevScreenPt, out delta);

      if (delta != Int2.Zero)
        m_panCmd(delta, time);

      m_prevScreenPt = screenPt;
    }
  }
}
