﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using Tesla.Graphics;
using Tesla.Input;

namespace Tesla.Framework
{
    /// <summary>
    /// Defines the basics of a camera controller. Controllers contain state and constrain / move the camera in some specific way, according to input.
    /// </summary>
    public interface ICameraController
    {
        /// <summary>
        /// Gets or sets if the controller is active.
        /// </summary>
        bool IsEnabled { get; set; }

        /// <summary>
        /// Gets or sets the camera that the controller manipulates.
        /// </summary>
        Camera Camera { get; set; }

        /// <summary>
        /// Gets the input group the controller creates triggers for.
        /// </summary>
        InputGroup InputGroup { get; }

        /// <summary>
        /// Populates the controller's input grouping with triggers to manipulate the camera. If the keybindings is null or a key is not present, then the default binding is used (see implementors).
        /// </summary>
        /// <param name="keyBindings">The key bindings.</param>
        void MapControls(IReadOnlyDictionary<String, KeyOrMouseButton[]> keyBindings);

        /// <summary>
        /// Checks if input has happened and updates the controller state, then updates the camera if anything has changed.
        /// </summary>
        /// <param name="time">The time between updates.</param>
        /// <param name="checkInput">True if the controller's input triggers should be checked (and performed, as necessary), false if only the camera should be updated, if necessary.</param>
        void Update(IGameTime time, bool checkInput);

        /// <summary>
        /// Updates the state of the controller, based on the current camera.
        /// </summary>
        void UpdateFromCamera();
    }
}
