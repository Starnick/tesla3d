﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using Tesla.Graphics;
using Tesla.Input;

namespace Tesla.Framework
{
  /// <summary>
  /// A controller that orbits a camera around a target point, at a certain distance away from that point. This is defined by three values, yaw/pitch angles and a zoom distance. The pitch angle
  /// is always limited between (-90, 90) degrees, not inclusive, while the yaw angle can rotate a full 360 degrees. The controller defines a number of methods that take in delta values (e.g. mouse positions) 
  /// to update the controller state.
  /// </summary>
  public class OrbitCameraController : ICameraController
  {
    /// <summary>
    /// Default yaw rotate speed.
    /// </summary>
    public static readonly Angle DefaultYawRotateSpeed = Angle.FromDegrees(20.0f);

    /// <summary>
    /// Default pitch rotate speed.
    /// </summary>
    public static readonly Angle DefaultPitchRotateSpeed = Angle.FromDegrees(20.0f);

    /// <summary>
    /// Default pan speed.
    /// </summary>
    public static readonly float DefaultPanSpeed = 30;

    /// <summary>
    /// Default zoom speed.
    /// </summary>
    public static readonly float DefaultZoomSpeed = 10.0f;

    private static readonly Angle MinPitch = Angle.FromDegrees(-89.9f);
    private static readonly Angle MaxPitch = Angle.FromDegrees(89.9f);
    private static readonly float MinZoom = 0.3f;

    /// <summary>
    /// Binding name for the Rotate action.
    /// </summary>
    public static readonly String RotateBindingName = "Rotate";

    /// <summary>
    /// Binding name for the Zoom action.
    /// </summary>
    public static readonly String ZoomBindingName = "Zoom";

    /// <summary>
    /// Binding name for the Pan action.
    /// </summary>
    public static readonly String PanBindingName = "Pan";

    /// <summary>
    /// Default input binding names.
    /// </summary>
    public static readonly String[] InputBindingNames = new String[] { RotateBindingName, ZoomBindingName, PanBindingName };

    private Camera m_camera;
    private Vector3 m_camPos;
    private Vector3 m_targetPoint;
    private float m_distance;
    private Angle m_yaw;
    private Angle m_pitch;

    private Angle m_yawRotateSpeed;
    private Angle m_pitchRotateSpeed;
    private float m_zoomSpeed;
    private float m_panSpeed;
    private bool m_isEnabled;
    private bool m_isDirty;
    private InputGroup m_inputGroup;
    private bool m_scalePanSpeed;
    private bool m_zoomRecreatesTarget;

    /// <summary>
    /// Gets or sets the camera that the controller manipulates.
    /// </summary>
    public Camera Camera
    {
      get
      {
        return m_camera;
      }
      set
      {
        m_camera = value;
        UpdateFromCamera();
      }
    }

    /// <summary>
    /// Gets the input group the controller creates triggers for.
    /// </summary>
    public InputGroup InputGroup
    {
      get
      {
        return m_inputGroup;
      }
    }

    /// <summary>
    /// Gets or sets the look at point that the camera orbits around.
    /// </summary>
    public Vector3 TargetLookAtPoint
    {
      get
      {
        return m_targetPoint;
      }
      set
      {
        m_targetPoint = value;
        Distance = Vector3.Distance(m_targetPoint, m_camera.Position); //Sets is dirty
      }
    }

    /// <summary>
    /// Gets or sets the distance from the look at point to the camera position.
    /// </summary>
    public float Distance
    {
      get
      {
        return m_distance;
      }
      set
      {
        m_distance = MathHelper.Clamp(value, MinZoom, float.MaxValue);
        m_isDirty = true;
      }
    }

    /// <summary>
    /// Gets or sets the current yaw angle.
    /// </summary>
    public Angle Yaw
    {
      get
      {
        return m_yaw;
      }
      set
      {
        m_yaw = value;
        m_isDirty = true;
      }
    }

    /// <summary>
    /// Gets or sets the current pitch angle.
    /// </summary>
    public Angle Pitch
    {
      get
      {
        return m_pitch;
      }
      set
      {

        m_pitch = Angle.Clamp(value, MinPitch, MaxPitch);
        m_isDirty = true;
      }
    }

    /// <summary>
    /// Gets or sets the yaw rotate speed.
    /// </summary>
    public Angle YawRotateSpeed
    {
      get
      {
        return m_yawRotateSpeed;
      }
      set
      {
        m_yawRotateSpeed = Angle.Max(value, Angle.Zero);
      }
    }

    /// <summary>
    /// Gets or sets the pitch rotate speed.
    /// </summary>
    public Angle PitchRotateSpeed
    {
      get
      {
        return m_pitchRotateSpeed;
      }
      set
      {
        m_pitchRotateSpeed = Angle.Max(value, Angle.Zero);
      }
    }

    /// <summary>
    /// Gets or sets the pan speed.
    /// </summary>
    public float PanSpeed
    {
      get
      {
        return m_panSpeed;
      }
      set
      {
        m_panSpeed = Math.Max(value, 0.0f);
      }
    }

    /// <summary>
    /// Gets or sets the zoom speed.
    /// </summary>
    public float ZoomSpeed
    {
      get
      {
        return m_zoomSpeed;
      }
      set
      {
        m_zoomSpeed = Math.Max(value, 0.0f);
      }
    }

    /// <summary>
    /// Gets or sets when zooming, the target is recomputed so the scene can be navigated just with zoom in/out. Uses Z=0 plane to pick a point.
    /// </summary>
    public bool ZoomRecreatesTarget
    {
      get
      {
        return m_zoomRecreatesTarget;
      }
      set
      {
        m_zoomRecreatesTarget = value;
      }
    }

    /// <summary>
    /// Gets or sets if the pan speed should be scaled based on distance to target. If further out, the faster it pans to account for the distances.
    /// </summary>
    public bool ScalePanSpeed
    {
      get
      {
        return m_scalePanSpeed;
      }
      set
      {
        m_scalePanSpeed = value;
      }
    }

    /// <summary>
    /// Gets or sets if the controller is active.
    /// </summary>
    public bool IsEnabled
    {
      get
      {
        return m_isEnabled;
      }
      set
      {
        m_isEnabled = value;
      }
    }

    /// <summary>
    /// Constructs a new instance of the <see cref="OrbitCameraController"/> class.
    /// </summary>
    /// <param name="camera">The camera to control</param>
    public OrbitCameraController(Camera camera) : this(camera, null, DefaultYawRotateSpeed, DefaultPitchRotateSpeed, DefaultZoomSpeed, DefaultPanSpeed) { }

    /// <summary>
    /// Constructs a new instance of the <see cref="OrbitCameraController"/> class.
    /// </summary>
    /// <param name="camera">The camera to control</param>
    /// <param name="lookAtPoint">The look at point to orbit around. Distance is based on the camera's current position and the look at point.</param>
    public OrbitCameraController(Camera camera, Vector3 lookAtPoint) : this(camera, lookAtPoint, DefaultYawRotateSpeed, DefaultPitchRotateSpeed, DefaultZoomSpeed, DefaultPanSpeed) { }


    /// <summary>
    /// Constructs a new instance of the <see cref="OrbitCameraController"/> class.
    /// </summary>
    /// <param name="camera">The camera to control</param>
    /// <param name="lookAtPoint">Optional look at point to orbit around. Distance is based on the camera's current position and the look at point.</param>
    /// <param name="yawRotateSpeed">The yaw rotate speed.</param>
    /// <param name="pitchRotateSpeed">The pitch rotate speed.</param>
    /// <param name="zoomSpeed">The zoom speed.</param>
    /// <param name="panSpeed">The pan speed.</param>
    public OrbitCameraController(Camera camera, Vector3? lookAtPoint, Angle yawRotateSpeed, Angle pitchRotateSpeed, float zoomSpeed, float panSpeed)
    {
      m_camera = camera;
      m_camPos = (camera == null) ? camera.Position : Vector3.Zero;
      m_isEnabled = true;
      m_scalePanSpeed = true;
      m_zoomRecreatesTarget = false;
      m_isDirty = true;

      m_distance = MinZoom;
      if (lookAtPoint.HasValue)
      {
        m_targetPoint = lookAtPoint.Value;
        m_camera.LookAt(lookAtPoint.Value, Vector3.Up);
        m_camera.Update();

        m_distance = Vector3.Distance(m_camPos, m_targetPoint);
      }

      UpdateFromCamera(m_distance);

      m_yawRotateSpeed = yawRotateSpeed;
      m_pitchRotateSpeed = pitchRotateSpeed;
      m_panSpeed = panSpeed;
      m_zoomSpeed = zoomSpeed;

      m_inputGroup = new InputGroup(GetType().Name);
    }

    /// <summary>
    /// Updates the state of the controller, based on the current camera.
    /// </summary>
    public void UpdateFromCamera()
    {
      UpdateFromCamera(0.0f);
    }

    /// <summary>
    /// Updates the state of the controller, based on the current camera.
    /// </summary>
    /// <param name="lookAtDistance">Distance from the look at point.</param>
    public void UpdateFromCamera(float lookAtDistance)
    {
      if (m_camera == null)
        return;

      m_camPos = m_camera.Position;

      //If look at distance is almost zero, take a look at the current target point. Otherwise create a new target point based on what the camera is positioned
      //and looking towards
      if (MathHelper.IsNearlyZero(lookAtDistance))
      {
        m_distance = Vector3.Distance(m_camPos, m_targetPoint);
      }
      else
      {
        Vector3 dir = m_camera.Direction;
        Vector3.Multiply(dir, lookAtDistance, out dir);
        Vector3.Add(dir, m_camPos, out m_targetPoint);
        m_distance = lookAtDistance;
      }

      Angle roll;
      m_camera.ViewMatrix.ToEulerAngles(out m_yaw, out m_pitch, out roll);

      m_isDirty = true;
    }

    /// <summary>
    /// Checks if input has happened and updates the controller state, then updates the camera if anything has changed.
    /// </summary>
    /// <param name="time">The time between updates.</param>
    /// <param name="checkInput">True if the controller's input triggers should be checked (and performed, as necessary), false if only the camera should be updated, if necessary.</param>
    public void Update(IGameTime time, bool checkInput)
    {
      if (m_camera == null || !m_isEnabled)
        return;

      if (checkInput)
        m_inputGroup.CheckAndPerformTriggers(time);

      if (m_isDirty)
      {
        Quaternion rot;
        Vector3 pos;
        Quaternion.FromEulerAngles(m_yaw, m_pitch, Angle.Zero, out rot);
        Quaternion.GetRotationVector(rot, 2, out pos);

        Vector3.Multiply(pos, m_distance, out pos);
        Vector3.Add(pos, m_targetPoint, out m_camPos);

        m_camera.Position = m_camPos;
        m_camera.LookAt(m_targetPoint, Vector3.Up);
        m_camera.Update();

        m_isDirty = false;
      }
    }

    /// <summary>
    /// Rotate the camera about both the camera Y (yaw) and X (pitch) axes.
    /// </summary>
    /// <param name="delta">Mouse position delta along the X, Y screen axis (origin upper left). The delta values scales the rotation speed. -X rotates right, +X rotates left (yaw) and +Y rotates up, -Y rotates down (pitch)</param>
    /// <param name="time">Elapsed time.</param>
    public void Rotate(Int2 delta, IGameTime time)
    {
      RotateYaw(delta.X, time);
      RotatePitch(delta.Y, time);
    }

    /// <summary>
    /// Rotate the camera about the camera X axis.
    /// </summary>
    /// <param name="deltaX">Mouse position delta along the X screen axis (origin upper left). The delta values scales the rotation speed. Negative rotates right, positive rotates left.</param>
    /// <param name="time">Elapsed time.</param>
    public void RotateYaw(int deltaX, IGameTime time)
    {
      if (deltaX == 0)
        return;

      m_yaw += deltaX * -m_yawRotateSpeed * time.ElapsedTimeInSeconds;

      if (m_yaw.Radians > MathHelper.TwoPi || m_yaw.Radians <= -MathHelper.TwoPi)
        m_yaw = Angle.Zero;

      m_isDirty = true;
    }

    /// <summary>
    /// Rotate the camera about the camera Y axis.
    /// </summary>
    /// <param name="deltaY">Mouse position delta along the Y screen axis (origin upper left). The delta values scales the rotation speed. Positive rotates up, negative rotates down.<param>
    /// <param name="time">Elapsed time.</param>
    public void RotatePitch(int deltaY, IGameTime time)
    {
      if (deltaY == 0)
        return;

      m_pitch += deltaY * -m_pitchRotateSpeed * time.ElapsedTimeInSeconds;

      m_pitch = Angle.Clamp(m_pitch, MinPitch, MaxPitch);

      m_isDirty = true;
    }

    /// <summary>
    /// Zooms the camera in/out along the camera's direction axis.
    /// </summary>
    /// <param name="delta">Mouse wheel delta (increments of 120). The delta values scales the zoom speed. Positive zooms in, negative zooms out.</param>
    /// <param name="time">Elapsed time.</param>
    public void Zoom(int delta, IGameTime time)
    {
      if (delta == 0)
        return;

      if (m_zoomRecreatesTarget)
      {
        float wheelZoomRatio = 1.15f;
        if (delta > 0)
          wheelZoomRatio = 1.0f / wheelZoomRatio;

        Ray pickRay = m_camera.CreatePickRay(Mouse.GetMouseState().Position, true);

        if (pickRay.Intersects(Plane.UnitY, out LineIntersectionResult result))
          m_targetPoint = result.Point;

        Matrix transform = Matrix.FromFixedPoint(m_targetPoint, Matrix.FromScale(wheelZoomRatio));
        Vector3 newPos = Vector3.Transform(m_camPos, transform);

        Vector3 dir = m_camera.Direction;
        float dirDot = Vector3.Dot(dir, m_targetPoint - newPos);
        m_targetPoint = newPos + (dir * dirDot);

        Distance = Vector3.Distance(m_targetPoint, newPos);

        m_camPos = newPos;
        m_camera.Position = newPos;
        m_camera.LookAt(m_targetPoint, Vector3.Up);
        m_camera.ViewMatrix.ToEulerAngles(out m_yaw, out m_pitch, out _);
      }
      else
      {

        Distance = m_distance * (float) Math.Pow(1.00105f, -delta);

        Vector3 dir;
        Vector3.Subtract(m_camPos, m_targetPoint, out dir);
        dir.Normalize();

        Vector3.Multiply(dir, m_distance, out dir);
        Vector3.Add(m_targetPoint, dir, out m_camPos);
      }

      m_isDirty = true;
    }

    /// <summary>
    /// Pans the camera by a certain amount along the Right/Up axes. This translates both the camera position and target point.
    /// </summary>
    /// <param name="delta">Mouse position delta along the X, Y screen axis (origin upper left). The delta values scales the pan speed. -X moves right, +X moves left and -Y moves down, +Y moves up</param>
    /// <param name="time">Elapsed time.</param>
    public void Pan(Int2 delta, IGameTime time)
    {
      if (delta == Int2.Zero)
        return;

      float panSpeed = m_panSpeed;

      if (m_scalePanSpeed)
      {
        float factor = Math.Max(.25f, (m_distance / m_panSpeed) / 4);
        panSpeed = m_panSpeed * factor;
      }

      Vector3 right = m_camera.Right;
      Vector3.Multiply(right, -delta.X * panSpeed * time.ElapsedTimeInSeconds, out right);

      Vector3 up = m_camera.Up;
      Vector3.Multiply(up, delta.Y * panSpeed * time.ElapsedTimeInSeconds, out up);

      Vector3 translation;
      Vector3.Add(right, up, out translation);

      Vector3.Add(m_camPos, translation, out m_camPos);
      Vector3.Add(m_targetPoint, translation, out m_targetPoint);

      m_isDirty = true;
    }

    /// <summary>
    /// Populates the controller's input grouping with triggers to manipulate the camera. If the keybindings is null or a key is not present, then the default binding is used (see implementors).
    /// </summary>
    /// <param name="keyBindings">The key bindings.</param>
    public void MapControls(IReadOnlyDictionary<String, KeyOrMouseButton[]> keyBindings)
    {
      m_inputGroup.Clear();

      m_inputGroup.Add(new InputTrigger(RotateBindingName,
          KeyOrMouseButton.CreateInputCondition(keyBindings, RotateBindingName, MouseButton.Right, false, true), new RotateInputAction(Rotate)));

      m_inputGroup.Add(new InputTrigger(PanBindingName,
          KeyOrMouseButton.CreateInputCondition(keyBindings, PanBindingName, MouseButton.Middle, false, true), new PanInputAction(Pan)));

      m_inputGroup.Add(CreateZoomTrigger(keyBindings));
    }

    private InputTrigger CreateZoomTrigger(IReadOnlyDictionary<String, KeyOrMouseButton[]> keyBindings)
    {
      KeyOrMouseButton[] buttons;

      if (keyBindings == null || !keyBindings.TryGetValue(ZoomBindingName, out buttons))
        return new ZoomInputTrigger(ZoomBindingName, Zoom);

      CompositeInputCondition compositeCondition = new CompositeInputCondition(new MouseWheelScrollCondition(),
          KeyOrMouseButton.CreateInputCondition(false, true, buttons));

      return new InputTrigger(ZoomBindingName, compositeCondition, new ZoomInputAction(Zoom));
    }
  }
}
