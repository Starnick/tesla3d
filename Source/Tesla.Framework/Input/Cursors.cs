﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;

#nullable enable

namespace Tesla.Framework
{
  /// <summary>
  /// Abstract manager for handling cursor changes. Implementations can hook into the cursor change event to do the
  /// actual cursor setup, identified by named values.
  /// </summary>
  public static class Cursors
  {
    /// <summary>
    /// Default cursor name. Usually a pointer.
    /// </summary>
    public static readonly String Default = "Default";

    /// <summary>
    /// Arrow cursor name.
    /// </summary>
    public static readonly String Arrow = "Arrow";

    /// <summary>
    /// Cross cursor name.
    /// </summary>
    public static readonly String Cross = "Cross";

    /// <summary>
    /// Open hand cursor name.
    /// </summary>
    public static readonly String Hand = "Hand";

    /// <summary>
    /// Closed hand cursor name.
    /// </summary>
    public static readonly String ClosedHand = "ClosedHand";

    /// <summary>
    /// Horizontal split cursor name.
    /// </summary>
    public static readonly String HSplit = "HSplit";

    /// <summary>
    /// Vertical split cursor name.
    /// </summary>
    public static readonly String VSplit = "VSplit";

    /// <summary>
    /// Pan cursor name.
    /// </summary>
    public static readonly String Pan = "Pan";

    /// <summary>
    /// Pan east cursor name.
    /// </summary>
    public static readonly String PanEast = "PanEast";

    /// <summary>
    /// Pan north-east cursor name.
    /// </summary>
    public static readonly String PanNE = "PanNE";

    /// <summary>
    /// Pan north cursor name.
    /// </summary>
    public static readonly String PanNorth = "PanNorth";

    /// <summary>
    /// Pan north-west cursor name.
    /// </summary>
    public static readonly String PanNW = "PanNW";

    /// <summary>
    /// Pan south-east cursor name.
    /// </summary>
    public static readonly String PanSE = "PanSE";

    /// <summary>
    /// Pan south cursor name.
    /// </summary>
    public static readonly String PanSouth = "PanSouth";

    /// <summary>
    /// Pan south-west cursor name.
    /// </summary>
    public static readonly String PanSW = "PanSW";

    /// <summary>
    /// Pan west cursor name.
    /// </summary>
    public static readonly String PanWest = "PanWest";

    /// <summary>
    /// Pan size all cursor name.
    /// </summary>
    public static readonly String SizeAll = "SizeAll";

    /// <summary>
    /// Wait cursor name.
    /// </summary>
    public static readonly String Wait = "Wait";

    /// <summary>
    /// Rotate cursor name.
    /// </summary>
    public static readonly String Rotate = "Rotate";

    /// <summary>
    /// Occurs when the cursor name changes.
    /// </summary>
    public static event EventHandler<String>? CursorChanged;

    private static String m_activeCursor = Default;

    /// <summary>
    /// Get or set the current cursor name.
    /// </summary>
    public static String Cursor
    {
      get
      {
        return m_activeCursor;
      }
      set
      {
        if (String.IsNullOrEmpty(value))
          m_activeCursor = Default;
        else
          m_activeCursor = value;

        CursorChanged?.Invoke(null, m_activeCursor);
      }
    }
  }
}
