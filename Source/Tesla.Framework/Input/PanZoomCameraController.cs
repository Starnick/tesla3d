﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using Tesla.Graphics;
using Tesla.Input;

namespace Tesla.Framework
{
    /// <summary>
    /// A controller that pans the camera along two axes and zoom along hte camera's direction vector. The methods that update the pan/zoom state
    /// take in delta values (e.g. mouse positions) that update the controller state.
    /// </summary>
    public class PanZoomCameraController : ICameraController
    {
        /// <summary>
        /// Default pan speed.
        /// </summary>
        public static readonly float DefaultPanSpeed = 30;

        /// <summary>
        /// Default zoom speed.
        /// </summary>
        public static readonly float DefaultZoomSpeed = 500.0f;

        /// <summary>
        /// Binding name for the Pan action.
        /// </summary>
        public static readonly String PanBindingName = "Pan";

        /// <summary>
        /// Binding name for the Pan Left action.
        /// </summary>
        public static readonly String PanLeftBindingName = "PanLeft";

        /// <summary>
        /// Binding name for the Pan Right action.
        /// </summary>
        public static readonly String PanRightBindingName = "PanRight";

        /// <summary>
        /// Binding name for the Pan Up action.
        /// </summary>
        public static readonly String PanUpBindingName = "PanUp";

        /// <summary>
        /// Binding name for the Pan Down action.
        /// </summary>
        public static readonly String PanDownBindingName = "PanDown";

        /// <summary>
        /// Binding name for the Zoom action.
        /// </summary>
        public static readonly String ZoomBindingName = "Zoom";

        /// <summary>
        /// Default input binding names.
        /// </summary>
        public static readonly String[] InputBindingNames = new String[] { PanBindingName, PanLeftBindingName, PanRightBindingName, PanUpBindingName, PanDownBindingName, ZoomBindingName };

        private Camera m_camera;
        private Vector3 m_camPos;
        private Triad m_axes;
        private Vector3? m_panRightAxis;
        private Vector3? m_panUpAxis;
        private float m_panSpeed;
        private float m_zoomSpeed;
        private bool m_isEnabled;
        private bool m_isDirty;
        private PanControllerMapOptions m_mapOptions;
        private InputGroup m_inputGroup;

        /// <summary>
        /// Gets or sets the camera that the controller manipulates.
        /// </summary>
        public Camera Camera
        {
            get
            {
                return m_camera;
            }
            set
            {
                m_camera = value;
                UpdateFromCamera();
            }
        }

        /// <summary>
        /// Gets the input group the controller creates triggers for.
        /// </summary>
        public InputGroup InputGroup
        {
            get
            {
                return m_inputGroup;
            }
        }

        /// <summary>
        /// Gets or sets the current position of the camera.
        /// </summary>
        public Vector3 Position
        {
            get
            {
                return m_camPos;
            }
            set
            {
                m_camPos = value;
                m_isDirty = true;
            }
        }

        /// <summary>
        /// Gets or sets the pan speed.
        /// </summary>
        public float PanSpeed
        {
            get
            {
                return m_panSpeed;
            }
            set
            {
                m_panSpeed = Math.Max(value, 0.0f);
            }
        }

        /// <summary>
        /// Gets or sets the zoom speed.
        /// </summary>
        public float ZoomSpeed
        {
            get
            {
                return m_zoomSpeed;
            }
            set
            {
                m_zoomSpeed = Math.Max(value, 0.0f);
            }
        }

        /// <summary>
        /// Gets or sets the Right axis of the pan action. If no axis is set, then this is derived from the camera's right axis.
        /// </summary>
        public Vector3 PanRightAxis
        {
            get
            {
                return m_axes.XAxis;
            }
            set
            {
                m_panRightAxis = value;
                m_axes.XAxis = value;
            }
        }

        /// <summary>
        /// Gets or sets the Up axis of the pan action. If no axis is set, then this is derived from the camera's up axis.
        /// </summary>
        public Vector3 PanUpAxis
        {
            get
            {
                return m_axes.YAxis;
            }
            set
            {
                m_panUpAxis = value;
                m_axes.YAxis = value;
            }
        }

        /// <summary>
        /// Gets or sets options that define how input triggers are created by the <see cref="MapControls(InputGroup, IReadOnlyDictionary{string, KeyOrMouseButton[]})"/> method.
        /// </summary>
        public PanControllerMapOptions MapOptions
        {
            get
            {
                return m_mapOptions;
            }
            set
            {
                m_mapOptions = value;
            }
        }

        /// <summary>
        /// Gets or sets if the controller is active.
        /// </summary>
        public bool IsEnabled
        {
            get
            {
                return m_isEnabled;
            }
            set
            {
                m_isEnabled = value;
            }
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="PanZoomCameraController"/> class.
        /// </summary>
        /// <param name="camera">The camera to control</param>
        public PanZoomCameraController(Camera camera) : this(camera, DefaultPanSpeed, DefaultZoomSpeed) { }

        /// <summary>
        /// Constructs a new instance of the <see cref="PanZoomCameraController"/> class.
        /// </summary>
        /// <param name="camera">The camera to control</param>
        /// <param name="panSpeed">The pan speed.</param>
        /// <param name="zoomSpeed">The zoom speed.</param>
        public PanZoomCameraController(Camera camera, float panSpeed, float zoomSpeed)
        {
            m_camera = camera;
            m_panSpeed = panSpeed;
            m_zoomSpeed = zoomSpeed;
            m_isEnabled = true;
            m_isDirty = true;
            m_axes = Triad.UnitAxes;
            m_panRightAxis = null;
            m_panUpAxis = null;
            m_mapOptions = PanControllerMapOptions.Standard;
            m_inputGroup = new InputGroup(GetType().Name);

            UpdateFromCamera();
        }

        /// <summary>
        /// Resets the pan axes to that of the camera's right and up axes.
        /// </summary>
        public void ResetPanAxes()
        {
            m_panRightAxis = null;
            m_panUpAxis = null;

            if (m_camera != null)
            {
                m_axes.XAxis = m_camera.Right;
                m_axes.YAxis = m_camera.Up;
            }
        }

        /// <summary>
        /// Pans the camera by a certain amount along the Right/Up axes.
        /// </summary>
        /// <param name="delta">Mouse position delta along the X, Y screen axis (origin upper left). The delta values scale the pan speed. -X moves right, +X moves left and -Y moves down, +Y moves up</param>
        /// <param name="time">Elapsed time.</param>
        public void Pan(Int2 delta, IGameTime time)
        {
            if (delta == Int2.Zero)
                return;

            Vector3 right = m_axes.XAxis;
            Vector3.Multiply(right, -delta.X * m_panSpeed * time.ElapsedTimeInSeconds, out right);

            Vector3 up = m_axes.YAxis;
            Vector3.Multiply(up, delta.Y * m_panSpeed * time.ElapsedTimeInSeconds, out up);

            Vector3 translation;
            Vector3.Add(right, up, out translation);

            Vector3.Add(m_camPos, translation, out m_camPos);

            m_isDirty = true;
        }
        /// <summary>
        /// Zooms the camera in/out along the camera's direction axis.
        /// </summary>
        /// <param name="delta">Mouse wheel delta (increments of 120, usually). Only the sign of the delta is used, it does not scale the zoom speed. Positive zooms in, negative zooms out.</param>
        /// <param name="time">Elapsed time.</param>

        public void Zoom(int delta, IGameTime time)
        {
            if (delta == 0)
                return;

            int sign = (delta > 0) ? 1 : -1;
            float amt = sign * m_zoomSpeed * time.ElapsedTimeInSeconds;

            Vector3 dir = m_axes.ZAxis;
            Vector3.Multiply(dir, amt, out dir);
            Vector3.Add(m_camPos, dir, out m_camPos);

            m_isDirty = true;
        }

        /// <summary>
        /// Updates the state of the controller, based on the current camera.
        /// </summary>
        public void UpdateFromCamera()
        {
            if (m_camera == null)
                return;

            m_camPos = m_camera.Position;
            m_axes.XAxis = m_camera.Right;
            m_axes.YAxis = m_camera.Up;
            m_axes.ZAxis = m_camera.Direction;

            if (m_panRightAxis.HasValue)
                m_axes.XAxis = m_panRightAxis.Value;

            if (m_panUpAxis.HasValue)
                m_axes.YAxis = m_panUpAxis.Value;

            m_isDirty = true;
        }

        /// <summary>
        /// Checks if input has happened and updates the controller state, then updates the camera if anything has changed.
        /// </summary>
        /// <param name="time">The time between updates.</param>
        /// <param name="checkInput">True if the controller's input triggers should be checked (and performed, as necessary), false if only the camera should be updated, if necessary.</param>
        public void Update(IGameTime time, bool checkInput)
        {
            if (m_camera == null || !m_isEnabled)
                return;

            if(checkInput)
                m_inputGroup.CheckAndPerformTriggers(time);

            if (m_isDirty)
            {
                m_camera.Position = m_camPos;
                m_camera.Update();

                m_isDirty = false;
            }
        }

        /// <summary>
        /// Populates the controller's input grouping with triggers to manipulate the camera. If the keybindings is null or a key is not present, then the default binding is used (see implementors).
        /// </summary>
        /// <param name="keyBindings">The key bindings.</param>
        public void MapControls(IReadOnlyDictionary<String, KeyOrMouseButton[]> keyBindings)
        {
            m_inputGroup.Clear();

            if ((m_mapOptions & PanControllerMapOptions.FourwayPan) == PanControllerMapOptions.FourwayPan)
            {
                m_inputGroup.Add(new InputTrigger(PanLeftBindingName,
                    KeyOrMouseButton.CreateInputCondition(keyBindings, PanLeftBindingName, Keys.A, false, true), new InputAction(delegate (IGameTime time)
                {
                    Pan(new Int2(1, 0), time);
                })));

                m_inputGroup.Add(new InputTrigger(PanRightBindingName,
                    KeyOrMouseButton.CreateInputCondition(keyBindings, PanRightBindingName, Keys.D, false, true), new InputAction(delegate (IGameTime time)
                {
                    Pan(new Int2(-1, 0), time);
                })));

                m_inputGroup.Add(new InputTrigger(PanUpBindingName,
                    KeyOrMouseButton.CreateInputCondition(keyBindings, PanUpBindingName, Keys.W, false, true), new InputAction(delegate (IGameTime time)
                {
                    Pan(new Int2(0, 1), time);
                })));

                m_inputGroup.Add(new InputTrigger(PanDownBindingName, 
                    KeyOrMouseButton.CreateInputCondition(keyBindings, PanDownBindingName, Keys.S, false, true), new InputAction(delegate (IGameTime time)
                {
                    Pan(new Int2(0, -1), time);
                })));
            }
            else
            {
                m_inputGroup.Add(new InputTrigger(PanBindingName, 
                    KeyOrMouseButton.CreateInputCondition(keyBindings, PanBindingName, MouseButton.Middle, false, true), new PanInputAction(Pan)));
            }

            m_inputGroup.Add(CreateZoomTrigger(keyBindings));
        }
        private InputTrigger CreateZoomTrigger(IReadOnlyDictionary<String, KeyOrMouseButton[]> keyBindings)
        {
            KeyOrMouseButton[] buttons;

            if (keyBindings == null || !keyBindings.TryGetValue(ZoomBindingName, out buttons))
                return new ZoomInputTrigger(ZoomBindingName, Zoom);

            CompositeInputCondition compositeCondition = new CompositeInputCondition(new MouseWheelScrollCondition(),
                KeyOrMouseButton.CreateInputCondition(false, true, buttons));

            return new InputTrigger(ZoomBindingName, compositeCondition, new ZoomInputAction(Zoom));
        }

    }
}
