﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using Tesla.Input;

#nullable enable

namespace Tesla.Framework
{
  /// <summary>
  /// Enumerates options for mapping controls of <see cref="PanZoomCameraController"/>.
  /// </summary>
  [Flags]
  public enum PanControllerMapOptions
  {
    /// <summary>
    /// Standard setup, there's a single trigger for zoom and one for pan (e.g. mouse held and moved to pan).
    /// </summary>
    Standard = 0,

    /// <summary>
    /// Replaces a single pan trigger for four separate triggers, each mapped to an input key (e.g. WSAD keys that
    /// move the camera in each direction).
    /// </summary>
    FourwayPan = 1
  }

  /// <summary>
  /// Enumerates options for mapping controls of <see cref="FirstPersonCameraController"/>.
  /// </summary>
  [Flags]
  public enum FPSControllerMapOptions
  {
    /// <summary>
    /// Standard setup, the mouse position is always set to the center of the viewport and the controller operates as a typical
    /// first person view.
    /// </summary>
    Standard = 0,

    /// <summary>
    /// The controller does not recenter the mouse and instead requires additional input, such as a key press, to rotate the camera
    /// and look around.
    /// </summary>
    NoCenterMouse = 1
  }
}
