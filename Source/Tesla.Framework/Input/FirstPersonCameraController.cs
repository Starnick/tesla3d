﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using Tesla.Graphics;
using Tesla.Input;

namespace Tesla.Framework
{
    /// <summary>
    /// A controller that moves the camera in a typical first-person perspective, there are actions to strafe and move as well as to rotate the camera.
    /// Standard setup is usually keys strafe/move and the mouse movement looks around, but there are additional options.
    /// </summary>
    public sealed class FirstPersonCameraController : ICameraController
    {
        /// <summary>
        /// Binding name for the Strafe Left action.
        /// </summary>
        public static readonly String StrafeLeftBindingName = "StrafeLeft";

        /// <summary>
        /// Binding name for the Strafe Right action.
        /// </summary>
        public static readonly String StrafeRightBindingName = "StrafeRight";

        /// <summary>
        /// Binding name for the Move Forward action.
        /// </summary>
        public static readonly String MoveForwardBindingName = "MoveForward";

        /// <summary>
        /// Binding name for the Moved Backward action.
        /// </summary>
        public static readonly String MoveBackwardBindingName = "MoveBackward";

        /// <summary>
        /// Binding name for the Rotate action.
        /// </summary>
        public static readonly String RotateBindingName = "Rotate";

        /// <summary>
        /// Default input binding names.
        /// </summary>
        public static readonly String[] InputBindingNames = new String[] { StrafeLeftBindingName, StrafeRightBindingName, MoveForwardBindingName, MoveBackwardBindingName };

        /// <summary>
        /// Default movement speed.
        /// </summary>
        public static readonly float DefaultMoveSpeed = 35.0f;

        /// <summary>
        /// Default rotation speed.
        /// </summary>
        public static readonly Angle DefaulteRotateSpeed = Angle.FromDegrees(10.0f);

        private Camera m_camera;
        private Vector3 m_camPos;
        private Triad m_axes;
        private float m_moveSpeed;
        private Angle m_rotateSpeed;
        private FPSControllerMapOptions m_mapOptions;
        private bool m_isEnabled;
        private bool m_isDirty;
        private InputGroup m_inputGroup;

        /// <summary>
        /// Gets or sets the camera that the controller manipulates.
        /// </summary>
        public Camera Camera
        {
            get
            {
                return m_camera;
            }
            set
            {
                m_camera = value;
                UpdateFromCamera();
            }
        }

        /// <summary>
        /// Gets the input group the controller creates triggers for.
        /// </summary>
        public InputGroup InputGroup
        {
            get
            {
                return m_inputGroup;
            }
        }

        /// <summary>
        /// Gets or sets the current position of the camera.
        /// </summary>
        public Vector3 Position
        {
            get
            {
                return m_camPos;
            }
            set
            {
                m_camPos = value;
                m_isDirty = true;
            }
        }

        /// <summary>
        /// Gets or sets the move speed.
        /// </summary>
        public float MoveSpeed
        {
            get
            {
                return m_moveSpeed;
            }
            set
            {
                m_moveSpeed = value;
            }
        }

        /// <summary>
        /// Gets or sets the rotate speed.
        /// </summary>
        public Angle RotateSpeed
        {
            get
            {
                return m_rotateSpeed;
            }
            set
            {
                m_rotateSpeed = value;
            }
        }

        /// <summary>
        /// Gets or sets options that define how input triggers are created by the <see cref="MapControls(InputGroup, IReadOnlyDictionary{string, KeyOrMouseButton[]})"/> method.
        /// </summary>
        public FPSControllerMapOptions MapOptions
        {
            get
            {
                return m_mapOptions;
            }
            set
            {
                m_mapOptions = value;
            }
        }

        /// <summary>
        /// Gets or sets if the controller is active.
        /// </summary>
        public bool IsEnabled
        {
            get
            {
                return m_isEnabled;
            }
            set
            {
                m_isEnabled = value;
            }
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="FirstPersonCameraController"/> class.
        /// </summary>
        /// <param name="camera">The camera to control</param>
        public FirstPersonCameraController(Camera camera) : this(camera, DefaultMoveSpeed, DefaulteRotateSpeed) { }

        /// <summary>
        /// Constructs a new instance of the <see cref="FirstPersonCameraController"/> class.
        /// </summary>
        /// <param name="camera">The camera to control</param>
        /// <param name="moveSpeed">The move speed.</param>
        /// <param name="rotateSpeed">The rotate speed.</param>
        public FirstPersonCameraController(Camera camera, float moveSpeed, Angle rotateSpeed)
        {
            m_mapOptions = FPSControllerMapOptions.Standard;
            m_camera = camera;
            m_moveSpeed = moveSpeed;
            m_rotateSpeed = rotateSpeed;
            m_isEnabled = true;
            m_isDirty = true;
            m_inputGroup = new InputGroup(GetType().Name);

            UpdateFromCamera();
        }

        /// <summary>
        /// Populates the controller's input grouping with triggers to manipulate the camera. If the keybindings is null or a key is not present, then the default binding is used (see implementors).
        /// </summary>
        /// <param name="keyBindings">The key bindings.</param>
        public void MapControls(IReadOnlyDictionary<String, KeyOrMouseButton[]> keyBindings)
        {
            m_inputGroup.Clear();

            bool noCenterMouse = (m_mapOptions & FPSControllerMapOptions.NoCenterMouse) == FPSControllerMapOptions.NoCenterMouse;

            if (!noCenterMouse)
            {
                m_inputGroup.Add(new CenterRotateInputTrigger(RotateBindingName, Rotate, m_camera));
            }
            else
            {
                m_inputGroup.Add(new InputTrigger(RotateBindingName, 
                    KeyOrMouseButton.CreateInputCondition(keyBindings, RotateBindingName, MouseButton.Right, false, true), 
                    new RotateInputAction(Rotate)));
            }

            m_inputGroup.Add(new InputTrigger(StrafeLeftBindingName, 
                KeyOrMouseButton.CreateInputCondition(keyBindings, StrafeLeftBindingName, Keys.A, false, true),
                new InputAction(delegate(IGameTime time)
                {
                    Move(-m_axes.XAxis, time);
                })));

            m_inputGroup.Add(new InputTrigger(StrafeRightBindingName, 
                KeyOrMouseButton.CreateInputCondition(keyBindings, StrafeRightBindingName, Keys.D, false, true),
                new InputAction(delegate(IGameTime time)
                {
                    Move(m_axes.XAxis, time);
                })));

            m_inputGroup.Add(new InputTrigger(MoveForwardBindingName, 
                KeyOrMouseButton.CreateInputCondition(keyBindings, MoveForwardBindingName, Keys.W, false, true),
                new InputAction(delegate(IGameTime time)
                {
                    Move(m_axes.ZAxis, time);
                })));

            m_inputGroup.Add(new InputTrigger(MoveBackwardBindingName, 
                KeyOrMouseButton.CreateInputCondition(keyBindings, MoveBackwardBindingName, Keys.S, false, true),
                new InputAction(delegate(IGameTime time)
                {
                    Move(-m_axes.ZAxis, time);
                })));
        }

        /// <summary>
        /// Makes the camera look at the target point and updates the controller state.
        /// </summary>
        /// <param name="target">Target point.</param>
        public void LookAt(Vector3 target)
        {
            if (m_camera == null)
                return;

            m_camera.LookAt(target, Vector3.Up);
            m_axes.XAxis = m_camera.Right;
            m_axes.YAxis = m_camera.Up;
            m_axes.ZAxis = m_camera.Direction;

            m_isDirty = true;
        }

        /// <summary>
        /// Moves the camera along some delta direction. This corresponds to the 3D space of the camera and not mouse positions. The direction
        /// doesn't have to be normalized, which means it can scale the move speed.
        /// </summary>
        /// <param name="dir">Direction delta to move towards.</param>
        /// <param name="time">Elapsed time.</param>
        public void Move(Vector3 dir, IGameTime time)
        {
            Vector3.Multiply(dir, time.ElapsedTimeInSeconds * m_moveSpeed, out dir);
            Vector3.Add(m_camPos, dir, out m_camPos);

            m_isDirty = true;
        }

        /// <summary>
        /// Rotates the camera where it's stationary at a point, looking around (as in First-person view).
        /// </summary>
        /// <param name="delta">Mouse position delta along the X, Y screen axis (origin upper left). The delta values scales the rotation speed. -X rotates right, +x rotates left (yaw) and +Y rotates up, -Y rotates down. (pitch)</param>
        /// <param name="time">Elapsed time.</param>
        public void Rotate(Int2 delta, IGameTime time)
        {
            if (delta == Int2.Zero)
                return;

            Quaternion finalRot = Quaternion.Identity;

            if (delta.X != 0)
            {
                Quaternion yawRot;
                Angle deltaAngle = m_rotateSpeed * -delta.X * time.ElapsedTimeInSeconds;
                Vector3 axis = Vector3.Up; //Always yaw around global Up
                Quaternion.FromAngleAxis(deltaAngle, axis, out yawRot);

                finalRot = yawRot;
            }

            if (delta.Y != 0)
            {
                Quaternion pitchRot;
                Angle deltaAngle = m_rotateSpeed * -delta.Y * time.ElapsedTimeInSeconds;
                Vector3 axis = m_axes.XAxis; //Always pitch relative to the current axes
                Quaternion.FromAngleAxis(deltaAngle, axis, out pitchRot);

                Quaternion.Multiply(finalRot, pitchRot, out finalRot);
            }

            Triad.Transform(m_axes, finalRot, out m_axes);
            m_axes.Normalize();

            m_isDirty = true;
        }

        /// <summary>
        /// Checks if input has happened and updates the controller state, then updates the camera if anything has changed.
        /// </summary>
        /// <param name="time">The time between updates.</param>
        /// <param name="checkInput">True if the controller's input triggers should be checked (and performed, as necessary), false if only the camera should be updated, if necessary.</param>
        public void Update(IGameTime time, bool checkInput)
        {
            if(m_camera == null || !m_isEnabled)
                return;

            if(checkInput)
                m_inputGroup.CheckAndPerformTriggers(time);

            if (m_isDirty)
            {
                m_camera.SetFrame(m_camPos, m_axes.XAxis, m_axes.YAxis, m_axes.ZAxis);
                m_camera.Update();

                m_isDirty = false;
            }
        }

        /// <summary>
        /// Updates the state of the controller, based on the current camera.
        /// </summary>
        public void UpdateFromCamera()
        {
            if(m_camera == null)
                return;

            m_camPos = m_camera.Position;
            m_axes.XAxis = m_camera.Right;
            m_axes.YAxis = m_camera.Up;
            m_axes.ZAxis = m_camera.Direction;
            m_isDirty = true;

            bool noCenterMouse = (m_mapOptions & FPSControllerMapOptions.NoCenterMouse) == FPSControllerMapOptions.NoCenterMouse;
            if(!noCenterMouse)
            {
                Viewport vp = m_camera.Viewport;
                Int2 center = new Int2(vp.Width / 2, vp.Height / 2);

                Mouse.SetPosition(center.X, center.Y);
            }
        }
    }
}
