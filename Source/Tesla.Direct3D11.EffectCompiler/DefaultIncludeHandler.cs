﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.IO;
using Tesla.Graphics;

namespace Tesla.Direct3D11.Graphics
{
  public sealed class DefaultIncludeHandler : IIncludeHandler
  {
    private Dictionary<String, String> m_includeDirs;
    private List<String> m_tempIncludeDirs;
    private Stack<String> m_currDirStack;

    public IEnumerable<String> IncludeDirectories
    {
      get
      {
        return m_includeDirs.Values;
      }
    }

    public DefaultIncludeHandler() : this(null) { }

    public DefaultIncludeHandler(params String[] includeDirectories)
    {
      m_includeDirs = new Dictionary<String, String>();

      if (includeDirectories != null)
      {
        foreach (String includeDir in includeDirectories)
          AddIncludeDirectory(includeDir);
      }

      m_currDirStack = new Stack<String>();
      m_tempIncludeDirs = new List<String>(m_includeDirs.Count);
    }

    public bool AddIncludeDirectory(String directory)
    {
      if (String.IsNullOrEmpty(directory))
        return false;

      if (!Directory.Exists(directory))
        return false;

      m_includeDirs[directory] = directory;
      return true;
    }

    public bool RemoveIncludeDirectory(String directory)
    {
      if (String.IsNullOrEmpty(directory))
        return false;

      return m_includeDirs.Remove(directory);
    }

    public void Close(Stream stream)
    {
      stream.Close();
      m_currDirStack.Pop();
    }

    public Stream Open(IncludeType includeType, String fileName, Stream parentStream)
    {
      String path = fileName;
      if (!Path.IsPathRooted(path))
      {
        //Add the current path on the stack so that gets searched first, most likely include files are grouped spatially near each other
        String currPath = (m_currDirStack.Count > 0) ? m_currDirStack.Peek() : null;
        if (currPath != null)
          m_tempIncludeDirs.Add(currPath);

        m_tempIncludeDirs.AddRange(m_includeDirs.Values);

        //Search in each directory
        foreach (String includeDir in m_tempIncludeDirs)
        {
          String testFilePath = Path.Combine(includeDir, fileName);
          if (File.Exists(testFilePath))
          {
            path = testFilePath;
            break;
          }
        }
      }

      if (String.IsNullOrEmpty(path) || !File.Exists(path))
        return null;

      m_currDirStack.Push(Path.GetDirectoryName(path));
      return new FileStream(path, FileMode.Open, FileAccess.Read);
    }
  }
}
