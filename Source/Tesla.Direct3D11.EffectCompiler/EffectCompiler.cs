﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Tesla.Content;
using Tesla.Graphics;
using Tesla.Utilities;
using System.Text.RegularExpressions;
using D3D = SharpDX.Direct3D;
using D3DC = SharpDX.D3DCompiler;

namespace Tesla.Direct3D11.Graphics
{
  public sealed class EffectCompiler : IEffectCompiler
  {
    private IncludeAdapter m_include;

    public EffectCompiler()
    {
      m_include = null;
    }

    public void SetIncludeHandler(IIncludeHandler includeHandler)
    {
      if (includeHandler == null)
      {
        m_include = null;
      }
      else
      {
        m_include = new IncludeAdapter(includeHandler);
      }
    }

    #region Compilation

    public EffectCompilerResult CompileFromFile(String effectFileName, CompileFlags flags)
    {
      return CompileFromFile(effectFileName, flags, null);
    }

    public EffectCompilerResult Compile(String effectCode, CompileFlags flags)
    {
      return Compile(effectCode, flags, StringLocalizer.Instance.GetLocalizedString("Unknown"), null);
    }

    public EffectCompilerResult Compile(String effectCode, CompileFlags flags, ShaderMacro[] macros)
    {
      return Compile(effectCode, flags, StringLocalizer.Instance.GetLocalizedString("Unknown"), null);
    }

    public EffectCompilerResult Compile(TEFXContent effectContent, CompileFlags flags)
    {
      return Compile(effectContent, flags, null);
    }

    public EffectCompilerResult CompileFromFile(String effectFileName, CompileFlags flags, ShaderMacro[] macros)
    {
      if (!File.Exists(effectFileName))
        return new EffectCompilerResult(null, new String[] { StringLocalizer.Instance.GetLocalizedString("FileNotFound", effectFileName) });

      String effectCode = File.ReadAllText(effectFileName);
      return Compile(effectCode, flags, effectFileName, macros);
    }

    public EffectCompilerResult Compile(String effectCode, CompileFlags flags, String sourceFileName, ShaderMacro[] macros)
    {
      if (String.IsNullOrEmpty(effectCode))
        return new EffectCompilerResult(null, new String[] { StringLocalizer.Instance.GetLocalizedString("EffectCodeEmpty") });

      //Parse FX file
      String parseErrors;
      TEFXContent effectContent = Parse(effectCode, sourceFileName, out parseErrors);

      if (effectContent == null || !String.IsNullOrEmpty(parseErrors))
        return new EffectCompilerResult(null, new String[] { parseErrors });

      return Compile(effectContent, flags, macros);
    }

    public EffectCompilerResult Compile(TEFXContent effectContent, CompileFlags flags, ShaderMacro[] macros)
    {
      if (effectContent == null)
        return new EffectCompilerResult(null, new String[] { StringLocalizer.Instance.GetLocalizedString("EffectContentNull") });

      if (!HasAtLeastOneValidShaderGroup(effectContent))
        return new EffectCompilerResult(null, new String[] { StringLocalizer.Instance.GetLocalizedString("NoShadersToCompile") });

      D3DC.ShaderFlags d3dCompileFlags = (D3DC.ShaderFlags) flags;
      D3D.ShaderMacro[] d3dMacros = ConvertShaderMacros(macros);

      //Now we actually evoke the D3D HLSL effect compiler here, then reflect the compiled shaders
      ShaderMetaData metaData = new ShaderMetaData((m_include != null) ? m_include.IncludeHandler : null);

      //Ensure each shader group name is unique
      Dictionary<String, String> uniqueGroupNameCheck = new Dictionary<String, String>();

      foreach (TEFXContent.ShaderGroupContent grp in effectContent)
      {
        if (uniqueGroupNameCheck.ContainsKey(grp.Name))
          return new EffectCompilerResult(null, new String[] { StringLocalizer.Instance.GetLocalizedString("ShaderGroupNameNotUnique", grp.Name) });

        foreach (TEFXContent.ShaderContent shader in grp)
        {
          String errors = CompileShader(shader, effectContent.FileName, d3dCompileFlags, d3dMacros, metaData);

          if (errors != null)
            return new EffectCompilerResult(null, errors.Split(new String[] { "\n" }, StringSplitOptions.RemoveEmptyEntries));
        }
      }

      //Now we have a map of every unique compiled shader and reflected data, so create the effect data
      EffectData data = CreateEffectData(effectContent, metaData);
      return new EffectCompilerResult(data, null);
    }

    private String CompileShader(TEFXContent.ShaderContent shaderContent, String sourceFileName, D3DC.ShaderFlags compileFlags, D3D.ShaderMacro[] macros, ShaderMetaData metaData)
    {
      if (shaderContent == null)
        return StringLocalizer.Instance.GetLocalizedString("ShaderContentNull");

      //Have we seen this content already? Don't try to compile if so
      if (metaData.ContainsShader(shaderContent))
        return null;

      //Compile HLSL
      D3DC.CompilationResult compResult;
      try
      {
        compResult = D3DC.ShaderBytecode.Compile(shaderContent.SourceCode, shaderContent.EntryPoint, shaderContent.ShaderProfile, compileFlags,
            D3DC.EffectFlags.None, macros, m_include, sourceFileName);
        if (compResult.HasErrors)
          return compResult.Message;
      }
      catch (SharpDX.CompilationException e)
      {
        return e.Message;
      }

      //Reflect compiled bytecode
      D3DC.ShaderBytecode byteCode = compResult.Bytecode;
      CompiledShader compiledShader = new CompiledShader(byteCode);
      byteCode.Dispose();

      metaData.AddCompiledShader(shaderContent, compiledShader);

      return null;
    }

    private D3D.ShaderMacro[] ConvertShaderMacros(ShaderMacro[] macros)
    {
      if (macros == null || macros.Length == 0)
        return null;

      D3D.ShaderMacro[] d3dMacros = new D3D.ShaderMacro[macros.Length];

      for (int i = 0; i < macros.Length; i++)
      {
        ShaderMacro macro = macros[i];
        if (macro.IsValid)
          d3dMacros[i] = new D3D.ShaderMacro(macro.Name, macro.Definition);
      }

      return d3dMacros;
    }

    private bool HasAtLeastOneValidShaderGroup(TEFXContent effectContent)
    {
      int validPasses = 0;

      foreach (TEFXContent.ShaderGroupContent group in effectContent)
        validPasses += group.ShaderCount;

      return validPasses > 0;
    }

    private void AddDefaultIncludeDirectory(String sourceFileName)
    {
      if (File.Exists(sourceFileName))
      {
        String dir = Path.GetDirectoryName(sourceFileName);

        if (!String.IsNullOrEmpty(dir))
        {
          if (m_include == null && m_include.IncludeHandler != null)
          {
            m_include.IncludeHandler.AddIncludeDirectory(dir);
          }
        }
      }
    }

    #endregion

    #region EffectData structure setup

    private EffectData CreateEffectData(TEFXContent effectContent, ShaderMetaData metaData)
    {
      EffectData effectData = new EffectData();
      effectData.ShaderGroups = new EffectData.ShaderGroup[effectContent.ShaderGroupCount];

      //Process shader groups
      for (int i = 0; i < effectData.ShaderGroups.Length; i++)
        effectData.ShaderGroups[i] = CreateShaderGroup(effectContent[i], metaData);

      effectData.Shaders = metaData.Shaders.ToArray();
      effectData.ConstantBuffers = metaData.ConstantBuffers.ToArray();
      effectData.ResourceVariables = metaData.ResourceVariables.ToArray();

      return effectData;
    }

    private EffectData.ShaderGroup CreateShaderGroup(TEFXContent.ShaderGroupContent shaderGroupContent, ShaderMetaData metaData)
    {
      EffectData.ShaderGroup group = new EffectData.ShaderGroup();
      group.Name = shaderGroupContent.Name;
      group.ShaderIndices = new int[shaderGroupContent.ShaderCount];

      //Not directly setting shaders, instead look up based on the shader content and find the index
      int index = 0;
      foreach (TEFXContent.ShaderContent shaderContent in shaderGroupContent)
      {
        group.ShaderIndices[index] = metaData.GetShaderIndex(shaderContent);
        index++;
      }

      return group;
    }

    #endregion

    #region TEFX Content Parsing

    public TEFXContent Parse(String effectCode)
    {
      String errors;
      return Parse(effectCode, StringLocalizer.Instance.GetLocalizedString("Unknown"), out errors);
    }

    public TEFXContent Parse(String effectCode, out String parseErrors)
    {
      return Parse(effectCode, StringLocalizer.Instance.GetLocalizedString("Unknown"), out parseErrors);
    }

    public TEFXContent Parse(String effectCode, String sourceFileName)
    {
      String errors;
      return Parse(effectCode, sourceFileName, out errors);
    }

    public TEFXContent Parse(String effectCode, String sourceFileName, out String parseErrors)
    {
      parseErrors = null;

      if (String.IsNullOrEmpty(effectCode))
        return null;

      Parser tinyParser = new Parser(new Scanner());
      ParseTree tree = tinyParser.Parse(effectCode);

      if (tree.Errors.Count > 0)
      {
        parseErrors = GetParseErrors(effectCode, tree);
        return null;
      }

      TEFXContent effect = tree.Eval() as TEFXContent;
      if (effect != null)
      {
        effect.FileName = String.IsNullOrEmpty(sourceFileName) ? String.Empty : Path.GetFileName(sourceFileName);

        SetSourceCode(effectCode, effect);
      }
      else
      {
        parseErrors = StringLocalizer.Instance.GetLocalizedString("UnknownError");
      }

      return effect;
    }

    private void SetSourceCode(String sourceCode, TEFXContent effect)
    {
      foreach (TEFXContent.ShaderGroupContent group in effect)
      {
        foreach (TEFXContent.ShaderContent shader in group)
        {
          shader.SourceCode = sourceCode;
        }
      }
    }

    private String GetParseErrors(String effectCode, ParseTree tree)
    {
      StringBuilder errors = new StringBuilder();
      foreach (ParseError error in tree.Errors)
      {
        int line;
        int col;
        FindLineAndColumn(effectCode, error.Position, out line, out col);
        errors.AppendLine(StringLocalizer.Instance.GetLocalizedString("ParseError", line.ToString(), col.ToString(), error.Message));
      }

      return errors.ToString();
    }

    private void FindLineAndColumn(String src, int pos, out int line, out int col)
    {
      line = 1;
      col = 1;

      for (int i = 0; i < pos; i++)
      {
        if (src[i] == '\n')
        {
          line++;
          col = 1;
        }
        else
        {
          col++;
        }
      }
    }

    #endregion

    #region Internal Structures

    private struct CompiledShader
    {
      public byte[] CompiledByteCode;
      public D3DShaderReflection MetaData;
      public TEFXContent.ShaderContent ShaderContent;
      public int Index;

      public CompiledShader(byte[] compiledByteCode, D3DShaderReflection metaData)
      {
        CompiledByteCode = compiledByteCode;
        MetaData = metaData;
        ShaderContent = null;
        Index = -1;
      }

      public CompiledShader(D3DC.ShaderBytecode byteCode)
      {
        CompiledByteCode = byteCode.Data.Clone() as byte[];
        MetaData = new D3DShaderReflection(byteCode);
        ShaderContent = null;
        Index = -1;
      }
    }

    private class ShaderMetaData
    {
      private Dictionary<TEFXContent.ShaderContent, CompiledShader> m_uniqueCompiledShadersMap;
      private Dictionary<TEFXContent.ShaderContent, CompiledShader> m_completeCompiledShadersMap;
      private Dictionary<String, Dictionary<String, EffectData.SamplerStateData>> m_parsedSamplerStates;
      private List<D3DConstantBuffer> m_reflectedConstantBuffers;
      public List<EffectData.Shader> Shaders;
      public List<EffectData.ConstantBuffer> ConstantBuffers;
      public List<EffectData.ResourceVariable> ResourceVariables;

      private SamplerDataParser m_samplerParser;
      private IIncludeHandler m_includeHandler;

      public ShaderMetaData(IIncludeHandler includeHandler)
      {
        m_includeHandler = includeHandler;
        m_uniqueCompiledShadersMap = new Dictionary<TEFXContent.ShaderContent, CompiledShader>(new ReferenceEqualityComparer<TEFXContent.ShaderContent>());
        m_completeCompiledShadersMap = new Dictionary<TEFXContent.ShaderContent, CompiledShader>(new ReferenceEqualityComparer<TEFXContent.ShaderContent>());
        m_parsedSamplerStates = new Dictionary<String, Dictionary<String, EffectData.SamplerStateData>>();
        m_reflectedConstantBuffers = new List<D3DConstantBuffer>();
        Shaders = new List<EffectData.Shader>();
        ConstantBuffers = new List<EffectData.ConstantBuffer>();
        ResourceVariables = new List<EffectData.ResourceVariable>();
      }

      public int GetShaderIndex(TEFXContent.ShaderContent shaderContent)
      {
        CompiledShader shader;
        if (m_completeCompiledShadersMap.TryGetValue(shaderContent, out shader))
          return shader.Index;

        return -1;
      }

      public bool ContainsShader(TEFXContent.ShaderContent shaderContent)
      {
        CompiledShader shader;
        return m_completeCompiledShadersMap.TryGetValue(shaderContent, out shader);
      }

      public bool AddCompiledShader(TEFXContent.ShaderContent shaderContent, CompiledShader compiledShader)
      {
        if (ContainsShader(shaderContent))
          return false;

        bool addToMap = true;
        bool addDuplicate = false;
        String reasonWhyNotSame;
        CompiledShader shaderContained = compiledShader;
        foreach (KeyValuePair<TEFXContent.ShaderContent, CompiledShader> kv in m_uniqueCompiledShadersMap)
        {
          if (kv.Value.MetaData.AreSame(compiledShader.MetaData, out reasonWhyNotSame))
          {
            addToMap = false;
            addDuplicate = true;
            shaderContained = kv.Value;
            break;
          }
        }

        if (addToMap)
        {
          compiledShader.Index = Shaders.Count;
          compiledShader.ShaderContent = shaderContent;
          Shaders.Add(CreateShader(compiledShader));
          m_uniqueCompiledShadersMap.Add(shaderContent, compiledShader);
          m_completeCompiledShadersMap[shaderContent] = compiledShader;
        }
        else if (addDuplicate)
        {
          m_completeCompiledShadersMap[shaderContent] = shaderContained;
        }

        return addToMap;
      }

      public int GetConstantBufferIndex(D3DConstantBuffer constantBuffer)
      {
        String reasonWhyNotSame;
        for (int i = 0; i < m_reflectedConstantBuffers.Count; i++)
        {
          if (m_reflectedConstantBuffers[i].AreSame(constantBuffer, out reasonWhyNotSame))
          {
            return i;
          }
        }

        m_reflectedConstantBuffers.Add(constantBuffer);
        ConstantBuffers.Add(CreateConstantBuffer(constantBuffer));
        return m_reflectedConstantBuffers.Count - 1;
      }

      public int GetResourceBindingIndex(D3DResourceBinding resourceBinding, D3DShaderReflection parent, CompiledShader shader)
      {
        if (resourceBinding.IsConstantBuffer)
        {
          return GetConstantBufferIndex(parent.GetConstantBufferByName(resourceBinding.Name));
        }
        else
        {
          for (int i = 0; i < ResourceVariables.Count; i++)
          {
            EffectData.ResourceVariable resource = ResourceVariables[i];
            if (resourceBinding.InputFlags == resource.InputFlags && resourceBinding.Name == resource.Name && resourceBinding.ResourceDimension == resource.ResourceDimension &&
                resourceBinding.ResourceType == resource.ResourceType && resourceBinding.ReturnType == resource.ReturnType && resourceBinding.SampleCount == resource.SampleCount)
            {
              //Check element count, noticed that the BindCount will be up to the maximum index used by the shader. So a Texture2D[3], and only first index is used
              //bind count will be 1 - probably because the HLSL compiler unrolls this into three Texture2Ds. So be sure to take the maximum bindcount as the elementcount
              resource.ElementCount = Math.Max(resource.ElementCount, (resourceBinding.BindCount > 1) ? resourceBinding.BindCount : 0);

              return i;
            }
          }

          ResourceVariables.Add(CreateResourceVariable(resourceBinding, shader));
          return ResourceVariables.Count - 1;
        }
      }

      #region EffectData creation

      private EffectData.ResourceVariable CreateResourceVariable(D3DResourceBinding resourceBinding, CompiledShader shader)
      {
        EffectData.ResourceVariable variable = new EffectData.ResourceVariable();
        variable.InputFlags = resourceBinding.InputFlags;
        variable.Name = resourceBinding.Name;
        variable.ResourceDimension = resourceBinding.ResourceDimension;
        variable.ResourceType = resourceBinding.ResourceType;
        variable.ReturnType = resourceBinding.ReturnType;
        variable.SampleCount = resourceBinding.SampleCount;
        variable.ElementCount = (resourceBinding.BindCount > 1) ? resourceBinding.BindCount : 0;
        variable.SamplerData = null;
        variable.StructuredBufferLayout = null;

        //Sampler arrays not supported, for now. Not sure how much used they are
        if (variable.IsSampler && variable.ElementCount == 0)
          variable.SamplerData = GetSamplerStateData(shader.ShaderContent, variable.Name);

        //If a structured buffer
        if (variable.IsStructuredBuffer)
        {
          //There will be a constant buffer by the same name
          D3DConstantBuffer cb = shader.MetaData.GetConstantBufferByName(variable.Name);
          if (cb != null && cb.Variables.Length > 0)
            variable.StructuredBufferLayout = CreateValueVariable(cb.Variables[0], false);
        }

        return variable;
      }

      private EffectData.SamplerStateData? GetSamplerStateData(TEFXContent.ShaderContent shader, String variableName)
      {
        if (m_samplerParser == null)
          m_samplerParser = new SamplerDataParser();

        Dictionary<String, EffectData.SamplerStateData> samplerStates;
        if (!m_parsedSamplerStates.TryGetValue(shader.SourceCode, out samplerStates))
        {
          samplerStates = new Dictionary<String, EffectData.SamplerStateData>();
          m_samplerParser.ParseAllSamplerStates(shader.SourceCode, m_includeHandler, samplerStates);
          m_parsedSamplerStates.Add(shader.SourceCode, samplerStates);
        }

        EffectData.SamplerStateData sampler;
        if (samplerStates.TryGetValue(variableName, out sampler))
          return sampler;

        return null;
      }

      private EffectData.Shader CreateShader(CompiledShader compiledShader)
      {
        EffectData.Shader shader = new EffectData.Shader();
        D3DShaderReflection reflect = compiledShader.MetaData;
        shader.ShaderType = compiledShader.ShaderContent.ShaderType;
        shader.ShaderProfile = compiledShader.ShaderContent.ShaderProfile;
        shader.ShaderFlags = reflect.ShaderFlags;
        shader.ShaderByteCode = compiledShader.CompiledByteCode;
        shader.HashCode = BufferHelper.ComputeFNVModifiedHashCode(shader.ShaderByteCode);
        shader.GeometryShaderInputPrimitive = reflect.GeometryShaderInputPrimitive;
        shader.GeometryShaderOutputTopology = reflect.GeometryShaderOutputTopology;
        shader.GeometryShaderInstanceCount = reflect.GeometryShaderInstanceCount;
        shader.MaxGeometryShaderOutputVertexCount = reflect.MaxGeometryShaderOutputVertexCount;
        shader.GeometryOrHullShaderInputPrimitive = reflect.GeometryOrHullShaderInputPrimitive;
        shader.HullShaderOutputPrimitive = reflect.HullShaderOutputPrimitive;
        shader.HullShaderPartitioning = reflect.HullShaderPartitioning;
        shader.TessellatorDomain = reflect.TessellatorDomain;
        shader.IsSampleFrequencyShader = reflect.IsSampleFrequencyShader;

        //Bound resources
        shader.BoundResources = new EffectData.BoundResource[reflect.BoundResources.Length];
        for (int i = 0; i < reflect.BoundResources.Length; i++)
        {
          D3DResourceBinding binding = reflect.BoundResources[i];
          EffectData.BoundResource bResource = new EffectData.BoundResource();
          bResource.ResourceIndex = GetResourceBindingIndex(binding, reflect, compiledShader);
          bResource.ResourceType = binding.ResourceType;
          bResource.BindPoint = binding.BindPoint;
          bResource.BindCount = binding.BindCount;

          shader.BoundResources[i] = bResource;
        }

        //Add signatures
        shader.InputSignature = CreateSignature(reflect.InputSignatureByteCode, reflect.InputSignatureByteCodeHash, reflect.InputSignature);
        shader.OutputSignature = CreateSignature(reflect.OutputSignatureByteCode, reflect.OutputSignatureByteCodeHash, reflect.OutputSignature);

        return shader;
      }

      private EffectData.Signature CreateSignature(byte[] sigByteCode, int sigHash, D3DShaderParameter[] d3dSig)
      {
        EffectData.Signature sig = new EffectData.Signature();
        sig.ByteCode = sigByteCode;
        sig.HashCode = sigHash;
        sig.Parameters = new EffectData.SignatureParameter[d3dSig.Length];
        for (int i = 0; i < d3dSig.Length; i++)
        {
          D3DShaderParameter d3dSigParam = d3dSig[i];
          EffectData.SignatureParameter sigParam = new EffectData.SignatureParameter();
          sigParam.ComponentType = d3dSigParam.ComponentType;
          sigParam.ReadWriteMask = d3dSigParam.ReadWriteMask;
          sigParam.Register = d3dSigParam.Register;
          sigParam.SemanticIndex = d3dSigParam.SemanticIndex;
          sigParam.SemanticName = d3dSigParam.SemanticName;
          sigParam.StreamIndex = d3dSigParam.StreamIndex;
          sigParam.SystemType = d3dSigParam.SystemType;
          sigParam.UsageMask = d3dSigParam.UsageMask;

          sig.Parameters[i] = sigParam;
        }

        return sig;
      }

      private EffectData.ConstantBuffer CreateConstantBuffer(D3DConstantBuffer reflectedBuffer)
      {
        EffectData.ConstantBuffer cb = new EffectData.ConstantBuffer();
        cb.BufferType = reflectedBuffer.BufferType;
        cb.Flags = reflectedBuffer.Flags;
        cb.Name = reflectedBuffer.Name;
        cb.SizeInBytes = reflectedBuffer.SizeInBytes;

        cb.Variables = new EffectData.ValueVariable[reflectedBuffer.Variables.Length];
        for (int i = 0; i < cb.Variables.Length; i++)
        {
          cb.Variables[i] = CreateValueVariable(reflectedBuffer.Variables[i]);
        }

        return cb;
      }

      private EffectData.ValueVariable CreateValueVariable(D3DShaderVariable reflectedVariable, bool assignDefaultValue = true)
      {
        EffectData.ValueVariable variable = new EffectData.ValueVariable();
        variable.Name = reflectedVariable.Name;
        variable.SizeInBytes = reflectedVariable.SizeInBytes;
        variable.AlignedElementSizeInBytes = reflectedVariable.AlignedElementSizeInBytes;
        variable.StartOffset = reflectedVariable.StartOffset;
        variable.VariableClass = reflectedVariable.VariableClass;
        variable.VariableType = reflectedVariable.VariableType;
        variable.ColumnCount = reflectedVariable.ColumnCount;
        variable.RowCount = reflectedVariable.RowCount;
        variable.Flags = reflectedVariable.Flags;
        variable.StartSampler = reflectedVariable.StartSampler;
        variable.SamplerSize = reflectedVariable.SamplerSize;
        variable.StartTexture = reflectedVariable.StartTexture;
        variable.TextureSize = reflectedVariable.TextureSize;
        variable.DefaultValue = assignDefaultValue ? reflectedVariable.DefaultValue : Array.Empty<byte>();
        variable.ElementCount = reflectedVariable.ElementCount;

        variable.Members = new EffectData.ValueVariableMember[reflectedVariable.Members.Length];
        for (int i = 0; i < variable.Members.Length; i++)
        {
          variable.Members[i] = CreateValueVariableMember(reflectedVariable.Members[i]);
        }

        return variable;
      }

      private EffectData.ValueVariableMember CreateValueVariableMember(D3DShaderVariableMember reflectedMember)
      {
        EffectData.ValueVariableMember member = new EffectData.ValueVariableMember();
        member.Name = reflectedMember.Name;
        member.VariableType = reflectedMember.VariableType;
        member.VariableClass = reflectedMember.Class;
        member.SizeInBytes = reflectedMember.SizeInBytes;
        member.AlignedElementSizeInBytes = reflectedMember.AlignedElementSizeInBytes;
        member.RowCount = reflectedMember.RowCount;
        member.ColumnCount = reflectedMember.ColumnCount;
        member.ElementCount = reflectedMember.ElementCount;
        member.OffsetFromParentStructure = reflectedMember.OffsetFromParent;

        member.Members = new EffectData.ValueVariableMember[reflectedMember.Members.Length];
        for (int i = 0; i < member.Members.Length; i++)
        {
          member.Members[i] = CreateValueVariableMember(reflectedMember.Members[i]);
        }

        return member;
      }

      #endregion

      #region Sampler Parsing

      private class SamplerDataParser
      {
        private StringTokenizer m_tokenizer;
        private char[] m_delims;


        public SamplerDataParser()
        {
          m_tokenizer = new StringTokenizer();
          m_delims = new char[] { 'f', ';' };
        }

        public void ParseAllSamplerStates(String shaderSourceCode, IIncludeHandler includeHandler, Dictionary<String, EffectData.SamplerStateData> samplerStates)
        {
          m_tokenizer.Initialize(shaderSourceCode);

          while (m_tokenizer.HasNext())
          {
            String nextToken = m_tokenizer.NextToken();

            if (StringCompare(nextToken, "#include"))
            {
              String filePath = SanitizeIncludePath(m_tokenizer.NextToken());
              ParseIncludeHeader(filePath, includeHandler, samplerStates);
              continue;
            }

            if (StringCompare(nextToken, "SamplerState") ||
                StringCompare(nextToken, "SamplerComparisonState") ||
                StringCompare(nextToken, "sampler1D") ||
                StringCompare(nextToken, "sampler2D") ||
                StringCompare(nextToken, "sampler3D") ||
                StringCompare(nextToken, "samplerCUBE") ||
                StringCompare(nextToken, "sampler") ||
                StringCompare(nextToken, "sampler_state"))
            {
              String samplerName = m_tokenizer.NextToken();

              if (samplerStates.ContainsKey(samplerName))
                continue;

              nextToken = m_tokenizer.NextToken();

              //Sometimes the sampler is set to a register...skip over it
              if (StringCompare(nextToken, ":"))
              {
                m_tokenizer.NextToken(); //Register
                nextToken = m_tokenizer.NextToken(); //Should be opening brace
              }

              if (StringCompare(nextToken, "{"))
              {
                //Create a sampler only when we really have a bunch of properties to set. If just declared, then we should be using the default sampler.
                EffectData.SamplerStateData data;

                //Default values...
                data.AddressU = TextureAddressMode.Clamp;
                data.AddressV = TextureAddressMode.Clamp;
                data.AddressW = TextureAddressMode.Clamp;
                data.BorderColor = Color.White;
                data.ComparisonFunction = ComparisonFunction.Never;
                data.Filter = TextureFilter.Linear;
                data.MaxAnisotropy = 1;
                data.MipMapLevelOfDetailBias = 0.0f;
                data.MinMipMapLevel = 0;
                data.MaxMipMapLevel = int.MaxValue;

                nextToken = m_tokenizer.NextToken();

                while (!StringCompare(nextToken, "}") || !nextToken.EndsWith("}"))
                {
                  if (StringCompare(nextToken, "AddressU"))
                  {
                    data.AddressU = ParseAddressMode();
                  }
                  else if (StringCompare(nextToken, "AddressV"))
                  {
                    data.AddressV = ParseAddressMode();
                  }
                  else if (StringCompare(nextToken, "AddressW"))
                  {
                    data.AddressW = ParseAddressMode();
                  }
                  else if (StringCompare(nextToken, "BorderColor"))
                  {
                    data.BorderColor = ParseBorderColor();
                  }
                  else if (StringCompare(nextToken, "Filter"))
                  {
                    data.Filter = ParseFilter();
                  }
                  else if (StringCompare(nextToken, "MaxAnisotropy"))
                  {
                    data.MaxAnisotropy = ParseInt(1);
                  }
                  else if (StringCompare(nextToken, "MaxLOD"))
                  {
                    float val = ParseFloat(float.MaxValue);
                    if (val == float.MaxValue)
                      data.MaxMipMapLevel = int.MaxValue;
                    else
                      data.MaxMipMapLevel = (int) val;
                  }
                  else if (StringCompare(nextToken, "MinLOD"))
                  {
                    data.MinMipMapLevel = (int) ParseFloat(0.0f);
                  }
                  else if (StringCompare(nextToken, "MipLODBias"))
                  {
                    data.MipMapLevelOfDetailBias = ParseFloat(0.0f);
                  }
                  else if (StringCompare(nextToken, "ComparisonFunc"))
                  {
                    data.ComparisonFunction = ParseComparisonFunction();
                  }

                  //Unexpected end of file
                  if (!m_tokenizer.HasNext())
                    return;

                  nextToken = m_tokenizer.NextToken();
                }

                samplerStates.Add(samplerName, data);
              }
            }
          }

        }

        private void ParseIncludeHeader(String includePath, IIncludeHandler includeHandler, Dictionary<String, EffectData.SamplerStateData> samplerStates)
        {
          if (String.IsNullOrEmpty(includePath) || includeHandler == null)
            return;

          Stream stream = includeHandler.Open(IncludeType.Local, includePath, null);

          if (stream == null)
            return;

          String headerCode = null;
          StreamReader reader = new StreamReader(stream);

          try
          {
            headerCode = reader.ReadToEnd();
          }
          finally
          {
            reader.Dispose();
            includeHandler.Close(stream);
          }

          if (String.IsNullOrEmpty(headerCode))
            return;

          StringTokenizer oldTokenizer = m_tokenizer;
          m_tokenizer = new StringTokenizer();

          ParseAllSamplerStates(headerCode, includeHandler, samplerStates);

          m_tokenizer = oldTokenizer;
        }

        private int ParseInt(int defaultVal)
        {
          String nextToken = m_tokenizer.NextToken();
          if (StringCompare(nextToken, "="))
          {
            return m_tokenizer.NextInt(); //Handles the ";" at the end.
          }

          return defaultVal;
        }

        private float ParseFloat(float defaultVal)
        {
          String nextToken = m_tokenizer.NextToken();
          if (StringCompare(nextToken, "="))
          {
            return m_tokenizer.NextSingle(); //handles the ";" at the end.
          }

          return defaultVal;
        }

        private TextureAddressMode ParseAddressMode()
        {
          String nextToken = m_tokenizer.NextToken();
          if (StringCompare(nextToken, "="))
          {
            String addressMode = m_tokenizer.NextToken();
            if (StringCompare(addressMode, "Wrap"))
            {
              return TextureAddressMode.Wrap;
            }
            else if (StringCompare(addressMode, "Border"))
            {
              return TextureAddressMode.Border;
            }
            else if (StringCompare(addressMode, "Clamp"))
            {
              return TextureAddressMode.Clamp;
            }
            else if (StringCompare(addressMode, "Mirror"))
            {
              return TextureAddressMode.Mirror;
            }
            else if (StringCompare(addressMode, "Mirror_Once"))
            {
              return TextureAddressMode.MirrorOnce;
            }
          }

          return TextureAddressMode.Clamp;
        }

        private ComparisonFunction ParseComparisonFunction()
        {
          String nextToken = m_tokenizer.NextToken();
          if (StringCompare(nextToken, "="))
          {
            String compFunc = m_tokenizer.NextToken();
            if (StringCompare(compFunc, "NEVER"))
            {
              return ComparisonFunction.Never;
            }
            else if (StringCompare(compFunc, "LESS"))
            {
              return ComparisonFunction.Less;
            }
            else if (StringCompare(compFunc, "EQUAL"))
            {
              return ComparisonFunction.Equal;
            }
            else if (StringCompare(compFunc, "LESS_EQUAL"))
            {
              return ComparisonFunction.LessEqual;
            }
            else if (StringCompare(compFunc, "GREATER"))
            {
              return ComparisonFunction.Greater;
            }
            else if (StringCompare(compFunc, "NOT_EQUAL"))
            {
              return ComparisonFunction.NotEqual;
            }
            else if (StringCompare(compFunc, "GREATER_EQUAL"))
            {
              return ComparisonFunction.GreaterEqual;
            }
            else if (StringCompare(compFunc, "ALWAYS"))
            {
              return ComparisonFunction.Always;
            }
          }

          return ComparisonFunction.Never;
        }

        private TextureFilter ParseFilter()
        {
          String nextToken = m_tokenizer.NextToken();
          if (StringCompare(nextToken, "="))
          {
            String filter = m_tokenizer.NextToken();
            if (StringCompare(filter, "MIN_MAG_MIP_POINT"))
            {
              return TextureFilter.Point;
            }
            else if (StringCompare(filter, "MIN_MAG_POINT_MIP_LINEAR"))
            {
              return TextureFilter.PointMipLinear;
            }
            else if (StringCompare(filter, "MIN_POINT_MAG_LINEAR_MIP_POINT"))
            {
              return TextureFilter.MinPointMagLinearMipPoint;
            }
            else if (StringCompare(filter, "MIN_POINT_MAG_MIP_LINEAR"))
            {
              return TextureFilter.MinPointMagLinearMipLinear;
            }
            else if (StringCompare(filter, "MIN_LINEAR_MAG_MIP_POINT"))
            {
              return TextureFilter.MinLinearMagPointMipPoint;
            }
            else if (StringCompare(filter, "MIN_LINEAR_MAG_POINT_MIP_LINEAR"))
            {
              return TextureFilter.MinLinearMagPointMipLinear;
            }
            else if (StringCompare(filter, "MIN_MAG_LINEAR_MIP_POINT"))
            {
              return TextureFilter.LinearMipPoint;
            }
            else if (StringCompare(filter, "MIN_MAG_MIP_LINEAR"))
            {
              return TextureFilter.Linear;
            }
            else if (StringCompare(filter, "ANISOTROPIC"))
            {
              return TextureFilter.Anisotropic;
            }
            else if (StringCompare(filter, "COMPARISON_MIN_MAG_MIP_POINT"))
            {
              return TextureFilter.Comparison_Point;
            }
            else if (StringCompare(filter, "COMPARISON_MIN_MAG_POINT_MIP_LINEAR"))
            {
              return TextureFilter.Comparison_PointMipLinear;
            }
            else if (StringCompare(filter, "COMPARISON_MIN_POINT_MAG_LINEAR_MIP_POINT"))
            {
              return TextureFilter.Comparison_MinPointMagLinearMipPoint;
            }
            else if (StringCompare(filter, "COMPARISON_MIN_POINT_MAG_MIP_LINEAR"))
            {
              return TextureFilter.Comparison_MinPointMagLinearMipLinear;
            }
            else if (StringCompare(filter, "COMPARISON_MIN_LINEAR_MAG_MIP_POINT"))
            {
              return TextureFilter.Comparison_MinLinearMagPointMipPoint;
            }
            else if (StringCompare(filter, "COMPARISON_MIN_LINEAR_MAG_POINT_MIP_LINEAR"))
            {
              return TextureFilter.Comparison_MinLinearMagPointMipLinear;
            }
            else if (StringCompare(filter, "COMPARISON_MIN_MAG_LINEAR_MIP_POINT"))
            {
              return TextureFilter.Comparison_LinearMipPoint;
            }
            else if (StringCompare(filter, "COMPARISON_MIN_MAG_MIP_LINEAR"))
            {
              return TextureFilter.Comparison_Linear;
            }
            else if (StringCompare(filter, "COMPARISON_ANISOTROPIC"))
            {
              return TextureFilter.Comparison_Anisotropic;
            }
          }

          return TextureFilter.Linear;
        }

        private Color ParseBorderColor()
        {
          String nextToken = m_tokenizer.NextToken();
          if (StringCompare(nextToken, "="))
          {
            StringBuilder builder = new StringBuilder();
            nextToken = m_tokenizer.NextToken();
            builder.Append(nextToken);
            nextToken = m_tokenizer.NextToken();

            while (!nextToken.EndsWith(")"))
            {
              builder.Append(nextToken);

              //Unexpected end of file
              if (!m_tokenizer.HasNext())
                return Color.White;

              nextToken = m_tokenizer.NextToken();
            }

            //Append last token since it's still part of float4(..)
            builder.Append(nextToken);

            //Builder now contains "float4(x, y, z, w)"
            float x, y, z, w;
            GetFloatValues(builder.ToString(), out x, out y, out z, out w);
            return new Color(x, y, z, w);
          }

          return Color.White;
        }

        private void GetFloatValues(String text, out float x, out float y, out float z, out float w)
        {
          //Sometime later clean this up to use Regex.

          if (text.StartsWith("float4("))
            text = text.Substring(7);

          text = text.TrimEnd(')');

          String[] splits = text.Split(',');

          x = 0.0f;
          y = 0.0f;
          z = 0.0f;
          w = 0.0f;

          if (splits.Length > 0)
            x = CleanFloat(splits[0]);

          if (splits.Length > 1)
            y = CleanFloat(splits[1]);

          if (splits.Length > 2)
            z = CleanFloat(splits[2]);

          if (splits.Length > 3)
            w = CleanFloat(splits[3]);
        }

        private float CleanFloat(String value)
        {
          float floatVal;
          if (float.TryParse(value.TrimEnd(m_delims), out floatVal))
            return floatVal;

          return 0.0f;
        }

        private static bool StringCompare(String left, String right)
        {
          return left.Equals(right, StringComparison.InvariantCultureIgnoreCase);
        }

        private static String SanitizeIncludePath(String value)
        {
          if (String.IsNullOrEmpty(value))
            return value;

          value = value.Replace(@"""", String.Empty);
          value = value.Replace("<", String.Empty);
          value = value.Replace(">", String.Empty);

          return value;
        }
      }

      #endregion
    }

    #endregion
  }
}
