﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.IO;
using System.Text;
using Tesla.Content;
using Tesla.Graphics;

namespace Tesla.Direct3D11.Graphics
{
  /// <summary>
  /// A resource importer that can load <see cref="Effect"/> objects from the Microsoft FX (Effects11) format.
  /// </summary>
  public sealed class Effects11ResourceImporter : ResourceImporter<Effect>
  {
    /// <summary>
    /// Constructs a new instance of the <see cref="Effects11ResourceImporter"/> class.
    /// </summary>
    public Effects11ResourceImporter() : base(".fx", ".hlsl") { }

    /// <summary>
    /// Loads content from the specified resource as the target runtime type.
    /// </summary>
    /// <param name="resourceFile">Resource file to read from</param>
    /// <param name="contentManager">Calling content manager</param>
    /// <param name="parameters">Optional loading parameters</param>
    /// <returns>The loaded object or null if it could not be loaded</returns>
    public override Effect Load(IResourceFile resourceFile, ContentManager contentManager, ImporterParameters parameters)
    {
      ValidateParameters(resourceFile, contentManager, ref parameters);

      EffectImporterParameters fxParams = parameters as EffectImporterParameters;

      //For now, ShaderCompileFlags == CompileFlags
      CompileFlags compileFlags = (fxParams == null) ? CompileFlags.None : (CompileFlags) fxParams.CompileFlags;
      DataCompressionMode compressionMode = (fxParams == null) ? DataCompressionMode.Gzip : fxParams.CompressionMode;
      ShaderMacro[] shaderMacros = (fxParams == null) ? null : fxParams.ShaderMacros;

      DefaultIncludeHandler includeHandler = new DefaultIncludeHandler();
      includeHandler.AddIncludeDirectory(Path.GetDirectoryName(resourceFile.FullName));
      if (fxParams != null && fxParams.IncludeDirectories != null)
      {
        foreach (String includeDir in fxParams.IncludeDirectories)
          includeHandler.AddIncludeDirectory(includeDir);
      }

      EffectCompiler compiler = new EffectCompiler();
      compiler.SetIncludeHandler(includeHandler);
      EffectCompilerResult result = compiler.CompileFromFile(resourceFile.FullName, compileFlags, shaderMacros);

      if (result.HasCompileErrors)
      {
        StringBuilder errorString = new StringBuilder();
        foreach (String text in result.CompileErrors)
          errorString.AppendLine(text);

        throw new TeslaContentException(errorString.ToString());
      }

      return new Effect(GraphicsHelper.GetRenderSystem(contentManager.ServiceProvider), EffectData.Write(result.EffectData, compressionMode));
    }

    /// <summary>
    /// Loads content from the specified stream as the target runtime type.
    /// </summary>
    /// <param name="input">Stream to read from.</param>
    /// <param name="contentManager">Calling content manager.</param>
    /// <param name="parameters">Optional loading parameters.</param>
    /// <returns>The loaded object or null if it could not be loaded.</returns>
    public override Effect Load(Stream input, ContentManager contentManager, ImporterParameters parameters)
    {
      ValidateParameters(input, contentManager, ref parameters);

      EffectImporterParameters fxParams = parameters as EffectImporterParameters;

      //For now, ShaderCompileFlags == CompileFlags
      CompileFlags compileFlags = (fxParams == null) ? CompileFlags.None : (CompileFlags) fxParams.CompileFlags;
      DataCompressionMode compressionMode = (fxParams == null) ? DataCompressionMode.Gzip : fxParams.CompressionMode;
      ShaderMacro[] shaderMacros = (fxParams == null) ? null : fxParams.ShaderMacros;

      using (StreamReader reader = new StreamReader(input, Encoding.UTF8, true, 1024, true))
      {
        EffectCompiler compiler = new EffectCompiler();
        EffectCompilerResult result = compiler.Compile(reader.ReadToEnd(), compileFlags, shaderMacros);

        if (result.HasCompileErrors)
        {
          StringBuilder errorString = new StringBuilder();
          foreach (String text in result.CompileErrors)
            errorString.AppendLine(text);

          throw new TeslaContentException(errorString.ToString());
        }

        return new Effect(GraphicsHelper.GetRenderSystem(contentManager.ServiceProvider), EffectData.Write(result.EffectData, compressionMode));
      }
    }
  }
}
