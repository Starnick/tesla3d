﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using D3D = SharpDX.D3DCompiler;

namespace Tesla.Direct3D11.Graphics
{
  public sealed class D3DShaderParameter
  {
    private String m_semanticName;
    private int m_semanticIndex;
    private int m_streamIndex;
    private int m_register;

    private D3DComponentMaskFlags m_usageMask;
    private D3DComponentMaskFlags m_readWriteMask;
    private D3DComponentType m_componentType;
    private D3DSystemValueType m_systemType;

    /// <summary>
    /// Gets the per-parameter string that identifies how the data will be used.
    /// </summary>
    public String SemanticName
    {
      get
      {
        return m_semanticName;
      }
    }

    /// <summary>
    /// Gets the semantic index the modifies the semantic name, used to differentiate different parameters that
    /// use the same semantic.
    /// </summary>
    public int SemanticIndex
    {
      get
      {
        return m_semanticIndex;
      }
    }

    /// <summary>
    /// Gets the index of which stream the geometry shader is using for the signature parameter.
    /// </summary>
    public int StreamIndex
    {
      get
      {
        return m_streamIndex;
      }
    }

    /// <summary>
    /// Gets the register that will contain the variable's data.
    /// </summary>
    public int Register
    {
      get
      {
        return m_register;
      }
    }

    /// <summary>
    /// Gets the mask that indicates which components of a register are used.
    /// </summary>
    public D3DComponentMaskFlags UsageMask
    {
      get
      {
        return m_usageMask;
      }
    }

    /// <summary>
    /// Gets the mask that indicates whether a given component is never written (if the signature is an output)
    /// or always read (if the signature is an input).
    /// </summary>
    public D3DComponentMaskFlags ReadWriteMask
    {
      get
      {
        return m_readWriteMask;
      }
    }

    /// <summary>
    /// Gets the per-component data type that is stored in the register. Each register can store up to four
    /// components of data.
    /// </summary>
    public D3DComponentType ComponentType
    {
      get
      {
        return m_componentType;
      }
    }

    /// <summary>
    /// Gets the predefined string that determines the functionality of certain pipeline stages.
    /// </summary>
    public D3DSystemValueType SystemType
    {
      get
      {
        return m_systemType;
      }
    }

    internal D3DShaderParameter(D3D.ShaderParameterDescription shaderParameter)
    {
      m_semanticName = shaderParameter.SemanticName;
      m_semanticIndex = shaderParameter.SemanticIndex;
      m_streamIndex = shaderParameter.Stream;
      m_register = shaderParameter.Register;

      //For some reason there's some gobbly gook in the usage mask value
      m_usageMask = (D3DComponentMaskFlags) (shaderParameter.UsageMask & D3D.RegisterComponentMaskFlags.All);
      m_readWriteMask = (D3DComponentMaskFlags) shaderParameter.ReadWriteMask;
      m_componentType = (D3DComponentType) shaderParameter.ComponentType;
      m_systemType = (D3DSystemValueType) shaderParameter.SystemValueType;
    }

    internal bool AreSame(D3DShaderParameter parameter)
    {
      if (!m_semanticName.Equals(parameter.SemanticName)
          || m_semanticIndex != parameter.SemanticIndex
          || m_streamIndex != parameter.StreamIndex
          || m_register != parameter.Register
          || m_usageMask != parameter.UsageMask
          || m_readWriteMask != parameter.ReadWriteMask
          || m_componentType != parameter.ComponentType
          || m_systemType != parameter.SystemType)
      {
        return false;
      }

      return true;
    }
  }
}
