﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using D3D = SharpDX.D3DCompiler;

namespace Tesla.Direct3D11.Graphics
{
  public sealed class D3DShaderVariableMember
  {
    //From ShaderTypeDescription
    private String m_name;
    private D3DShaderVariableClass m_class;
    private int m_numColumns;
    private int m_numRows;
    private int m_offsetFromParent; //From parent structure (if member)
    private int m_sizeInBytes; //Computed based on parent
    private int m_alignedElementSizeInBytes; //Computed, actual 4-byte aligned size of individual array element
    private D3DShaderVariableType m_type;
    private int m_elementCount;

    //From ShaderReflectionType
    private D3DShaderVariableMember[] m_members;

    public String Name
    {
      get
      {
        return m_name;
      }
    }

    public D3DShaderVariableClass Class
    {
      get
      {
        return m_class;
      }
    }

    public int ColumnCount
    {
      get
      {
        return m_numColumns;
      }
    }

    public int RowCount
    {
      get
      {
        return m_numRows;
      }
    }

    public int OffsetFromParent
    {
      get
      {
        return m_offsetFromParent;
      }
    }

    public int SizeInBytes
    {
      get
      {
        return m_sizeInBytes;
      }
    }

    public int AlignedElementSizeInBytes
    {
      get
      {
        return m_alignedElementSizeInBytes;
      }
    }

    public D3DShaderVariableType VariableType
    {
      get
      {
        return m_type;
      }
    }

    public int ElementCount
    {
      get
      {
        return m_elementCount;
      }
    }

    public D3DShaderVariableMember[] Members
    {
      get
      {
        return m_members;
      }
    }

    internal D3DShaderVariableMember(String typeName, D3D.ShaderReflectionType memberType)
    {
      D3D.ShaderTypeDescription desc = memberType.Description;

      m_name = typeName;
      m_class = (D3DShaderVariableClass) desc.Class;
      m_numColumns = desc.ColumnCount;
      m_numRows = desc.RowCount;
      m_offsetFromParent = desc.Offset;
      m_type = (D3DShaderVariableType) desc.Type;
      m_elementCount = desc.ElementCount;

      ComputeSizeInBytes();

      m_members = new D3DShaderVariableMember[desc.MemberCount];

      for (int i = 0; i < m_members.Length; i++)
      {
        D3D.ShaderReflectionType memberMemberType = memberType.GetMemberType(i);
        String memberMemberName = memberType.GetMemberTypeName(i);
        m_members[i] = new D3DShaderVariableMember(memberMemberName, memberMemberType);
        memberMemberType.Dispose();
      }
    }

    internal bool AreSame(D3DShaderVariableMember member, out String reasonWhyNotSame)
    {
      if (!m_name.Equals(member.m_name))
      {
        reasonWhyNotSame = "Type name not the same";
        return false;
      }

      if (m_class != member.Class
          || m_numColumns != member.ColumnCount
          || m_numRows != member.RowCount
          || m_offsetFromParent != member.OffsetFromParent
          || m_sizeInBytes != member.SizeInBytes
          || m_type != member.VariableType
          || m_elementCount != member.ElementCount)
      {
        reasonWhyNotSame = "Type not the same";
        return false;
      }

      D3DShaderVariableMember[] members = member.Members;

      if (m_members.Length != members.Length)
      {
        reasonWhyNotSame = "Member count not the same";
        return false;
      }

      for (int i = 0; i < m_members.Length; i++)
      {
        if (m_members[i].AreSame(members[i], out reasonWhyNotSame))
        {
          reasonWhyNotSame = String.Format("Member {0} not the same:\n{1}", i.ToString(), reasonWhyNotSame);
          return false;
        }
      }

      reasonWhyNotSame = "";
      return true;
    }

    private void ComputeSizeInBytes()
    {
      m_sizeInBytes = (m_numColumns * m_numRows) * GetSizeOfType();
      if (m_elementCount > 0)
        m_sizeInBytes *= m_elementCount;

      m_alignedElementSizeInBytes = D3DShaderVariable.CalculateAlignedElementSize(m_sizeInBytes, m_elementCount);
    }

    private int GetSizeOfType()
    {
      if (m_class == D3DShaderVariableClass.Object || m_class == D3DShaderVariableClass.InterfaceClass || m_class == D3DShaderVariableClass.InterfacePointer)
        return 0;

      if (m_class == D3DShaderVariableClass.Struct)
        return 4;

      switch (this.m_type)
      {
        case D3DShaderVariableType.Bool:
        case D3DShaderVariableType.Int:
        case D3DShaderVariableType.Float:
        case D3DShaderVariableType.UInt:
          return 4;

        case D3DShaderVariableType.UInt8:
          return 2;

        case D3DShaderVariableType.Double:
          return 8;
      }

      return 0;
    }
  }
}