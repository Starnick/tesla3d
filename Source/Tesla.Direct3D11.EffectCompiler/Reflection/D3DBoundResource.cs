﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics;

namespace Tesla.Direct3D11.Graphics
{
  [DebuggerDisplay("Name = {Name}, ResourceType = {ResourceType}, ResourceDimension = {ResourceDimension}")]
  public sealed class D3DBoundResource
  {
    private String m_name;
    private D3DResourceReturnType m_returnType;
    private D3DShaderInputType m_resourceType;
    private D3DResourceDimension m_resourceDimension;
    private D3DShaderInputFlags m_inputFlags;
    private int m_sampleCount;

    public String Name
    {
      get
      {
        return m_name;
      }
    }

    public D3DResourceReturnType ReturnType
    {
      get
      {
        return m_returnType;
      }
    }

    public D3DShaderInputType ResourceType
    {
      get
      {
        return m_resourceType;
      }
    }

    public D3DResourceDimension ResourceDimension
    {
      get
      {
        return m_resourceDimension;
      }
    }

    public D3DShaderInputFlags InputFlags
    {
      get
      {
        return m_inputFlags;
      }
    }

    public int SampleCount
    {
      get
      {
        return m_sampleCount;
      }
    }

    public bool IsConstantBuffer
    {
      get
      {
        return m_resourceType == D3DShaderInputType.ConstantBuffer || m_resourceType == D3DShaderInputType.TextureBuffer;
      }
    }

    public D3DBoundResource(D3DResourceBinding binding)
    {
      m_name = binding.Name;
      m_inputFlags = binding.InputFlags;
      m_returnType = binding.ReturnType;
      m_resourceType = binding.ResourceType;
      m_resourceDimension = binding.ResourceDimension;
      m_sampleCount = binding.SampleCount;
    }

    public bool AreSame(D3DBoundResource resourceVariable)
    {
      if (!m_name.Equals(resourceVariable.Name)
          || m_inputFlags != resourceVariable.InputFlags
          || m_returnType != resourceVariable.ReturnType
          || m_resourceType != resourceVariable.ResourceType
          || m_resourceDimension != resourceVariable.ResourceDimension
          || m_sampleCount != resourceVariable.SampleCount)
      {
        return false;
      }

      return true;
    }
  }
}
