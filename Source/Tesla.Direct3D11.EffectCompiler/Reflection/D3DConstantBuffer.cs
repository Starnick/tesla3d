﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics;
using D3D = SharpDX.D3DCompiler;

namespace Tesla.Direct3D11.Graphics
{
  [DebuggerDisplay("Name = {Name}, SizeInBytes = {SizeInBytes}, VariableCount = {Variables.Length}")]
  public class D3DConstantBuffer
  {
    private String m_name;
    private int m_sizeInBytes;
    private D3DShaderVariableFlags m_flags;
    private D3DConstantBufferType m_type;

    private D3DShaderVariable[] m_variables;

    public String Name
    {
      get
      {
        return m_name;
      }
    }

    public int SizeInBytes
    {
      get
      {
        return m_sizeInBytes;
      }
    }

    public D3DShaderVariableFlags Flags
    {
      get
      {
        return m_flags;
      }
    }

    public D3DConstantBufferType BufferType
    {
      get
      {
        return m_type;
      }
    }

    public D3DShaderVariable[] Variables
    {
      get
      {
        return m_variables;
      }
    }

    public D3DConstantBuffer(D3D.ConstantBuffer constantBuffer)
    {
      D3D.ConstantBufferDescription desc = constantBuffer.Description;

      m_name = desc.Name;
      m_type = (D3DConstantBufferType) desc.Type;
      m_flags = (D3DShaderVariableFlags) desc.Flags;
      m_sizeInBytes = desc.Size;

      m_variables = new D3DShaderVariable[desc.VariableCount];

      for (int i = 0; i < m_variables.Length; i++)
      {
        using (D3D.ShaderReflectionVariable variable = constantBuffer.GetVariable(i))
        {
          D3DShaderVariable myVariable = new D3DShaderVariable(variable);
          m_variables[i] = myVariable;
        }
      }
    }

    public bool AreSame(D3DConstantBuffer constantBuffer, out String reasonWhyNotSame)
    {
      if (GetHashCode() != constantBuffer.GetHashCode())
      {
        reasonWhyNotSame = "Hashcodes not the same";
        return false;
      }

      if (!m_name.Equals(constantBuffer.Name)
          || m_type != constantBuffer.BufferType
          || m_flags != constantBuffer.Flags
          || m_sizeInBytes != constantBuffer.SizeInBytes)
      {
        reasonWhyNotSame = "Description not same";
        return false;
      }

      D3DShaderVariable[] variables = constantBuffer.Variables;

      if (m_variables.Length != variables.Length)
      {
        reasonWhyNotSame = "Variable count not the same";
        return false;
      }

      for (int i = 0; i < m_variables.Length; i++)
      {
        if (m_variables[i].AreSame(variables[i], out reasonWhyNotSame))
        {
          reasonWhyNotSame = String.Format("Variable {0} not the same:\n{1}", i.ToString(), reasonWhyNotSame);
          return false;
        }
      }

      reasonWhyNotSame = String.Empty;
      return true;
    }

    public override int GetHashCode()
    {
      unchecked
      {
        int hash = 17;

        hash = (hash * 31) + m_name.GetHashCode();
        hash = (hash * 31) + m_sizeInBytes;
        hash = (hash * 31) + m_variables.Length;
        hash = (hash * 31) + m_type.GetHashCode();
        hash = (hash * 31) + m_flags.GetHashCode();

        return hash;
      }
    }
  }
}
