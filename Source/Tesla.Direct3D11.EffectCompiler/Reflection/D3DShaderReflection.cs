﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using D3D = SharpDX.D3DCompiler;

namespace Tesla.Direct3D11.Graphics
{
  public sealed class D3DShaderReflection
  {
    private D3DConstantBuffer[] m_constantBuffers;
    private D3DResourceBinding[] m_resourceBindings;
    private D3DShaderParameter[] m_inputSignature;
    private D3DShaderParameter[] m_outputSignature;
    private D3DShaderParameter[] m_patchConstantSignature;

    private int m_bitwiseInstructionCount;
    private int m_conditionalMoveInstructionCount;
    private int m_conversionInstructionCount;
    private D3DInputPrimitive m_geometryShaderInputPrimitive;
    private bool m_isSampleFrequencyShader;
    private int m_moveInstructionCount;

    private int m_arrayInstructionCount;
    private int m_barrierInstructionCount;
    private int m_controlPointCount;
    private String m_creator;
    private int m_cutInstructionCount;
    private int m_declarationCount;
    private int m_defineCount;
    private int m_dynamicFlowControlInstructionCount;
    private int m_emitInstructionCount;
    private CompileFlags m_shaderflags;
    private int m_floatInstructionCount;
    private int m_geometryShaderInstanceCount;
    private D3DPrimitiveTopology m_geometryShaderOutputTopology;
    private D3DTessellatorOutputPrimitive m_hullShaderOutputPrimitive;
    private D3DTessellatorPartitioning m_hullShaderPartitioning;
    private D3DInputPrimitive m_geometryOrHullShaderInputPrimitive;
    private int m_instructionCount;
    private int m_interlockedInstructionCount;
    private int m_integerInstructionCount;
    private int m_macroInstructionCount;
    private int m_maxGeometryShaderOutputVertexCount;
    private int m_staticFlowControlInstructionCount;
    private int m_tempArrayCount;
    private int m_tempRegisterCount;
    private D3DTessellatorDomain m_tessellatorDomain;
    private int m_textureBiasInstructionCount;
    private int m_textureCompareInstructionCount;
    private int m_textureGradientInstructionCount;
    private int m_textureLoadInstructionCount;
    private int m_textureNormalInstructionCount;
    private int m_textureStoreInstructionCount;
    private int m_unsignedIntegerInstructionCount;
    private int m_shaderVersion;
    private D3D11FeatureLevel m_minFeatureLevel;

    private int m_hashCode;
    private int m_inputSigHash;
    private int m_outputSigHash;
    private byte[] m_byteCode;
    private byte[] m_inputSigByteCode;
    private byte[] m_outputSigByteCode;

    public D3DConstantBuffer[] ConstantBuffers
    {
      get
      {
        return m_constantBuffers;
      }
    }

    public D3DResourceBinding[] BoundResources
    {
      get
      {
        return m_resourceBindings;
      }
    }

    public D3DShaderParameter[] InputSignature
    {
      get
      {
        return m_inputSignature;
      }
    }

    public D3DShaderParameter[] OutputSignature
    {
      get
      {
        return m_outputSignature;
      }
    }

    public D3DShaderParameter[] PatchConstantSignature
    {
      get
      {
        return m_patchConstantSignature;
      }
    }

    public int BitwiseInstructionCount
    {
      get
      {
        return m_bitwiseInstructionCount;
      }
    }

    public int ConditionalMoveInstructionCount
    {
      get
      {
        return m_conditionalMoveInstructionCount;
      }
    }

    public int ConversionInstructionCount
    {
      get
      {
        return m_conversionInstructionCount;
      }
    }

    public D3DInputPrimitive GeometryShaderInputPrimitive
    {
      get
      {
        return m_geometryShaderInputPrimitive;
      }
    }

    public bool IsSampleFrequencyShader
    {
      get
      {
        return m_isSampleFrequencyShader;
      }
    }

    public int MoveInstructionCount
    {
      get
      {
        return m_moveInstructionCount;
      }
    }

    public int ShaderVersion
    {
      get
      {
        return m_shaderVersion;
      }
    }

    public int ArrayInstructionCount
    {
      get
      {
        return m_arrayInstructionCount;
      }
    }

    public int BarrierInstructionCount
    {
      get
      {
        return m_barrierInstructionCount;
      }
    }

    public int ControlPointCount
    {
      get
      {
        return m_controlPointCount;
      }
    }

    public String Creator
    {
      get
      {
        return m_creator;
      }
    }

    public int CutInstructionCount
    {
      get
      {
        return m_cutInstructionCount;
      }
    }

    public int DeclarationCount
    {
      get
      {
        return m_declarationCount;
      }
    }

    public int DefineCount
    {
      get
      {
        return m_defineCount;
      }
    }

    public int DynamicFlowControlInstructionCount
    {
      get
      {
        return m_dynamicFlowControlInstructionCount;
      }
    }

    public int EmitInstructionCount
    {
      get
      {
        return m_emitInstructionCount;
      }
    }

    public CompileFlags ShaderFlags
    {
      get
      {
        return m_shaderflags;
      }
    }

    public int FloatInstructionCount
    {
      get
      {
        return m_floatInstructionCount;
      }
    }

    public int GeometryShaderInstanceCount
    {
      get
      {
        return m_geometryShaderInstanceCount;
      }
    }

    public D3DPrimitiveTopology GeometryShaderOutputTopology
    {
      get
      {
        return m_geometryShaderOutputTopology;
      }
    }

    public D3DTessellatorOutputPrimitive HullShaderOutputPrimitive
    {
      get
      {
        return m_hullShaderOutputPrimitive;
      }
    }

    public D3DTessellatorPartitioning HullShaderPartitioning
    {
      get
      {
        return m_hullShaderPartitioning;
      }
    }

    public D3DInputPrimitive GeometryOrHullShaderInputPrimitive
    {
      get
      {
        return m_geometryOrHullShaderInputPrimitive;
      }
    }

    public int InstructionCount
    {
      get
      {
        return m_instructionCount;
      }
    }

    public int InterlockedInstructionCount
    {
      get
      {
        return m_interlockedInstructionCount;
      }
    }

    public int IntegerInstructionCount
    {
      get
      {
        return m_integerInstructionCount;
      }
    }

    public int MacroInstructionCount
    {
      get
      {
        return m_macroInstructionCount;
      }
    }

    public int MaxGeometryShaderOutputVertexCount
    {
      get
      {
        return m_maxGeometryShaderOutputVertexCount;
      }
    }

    public int StaticFlowControlInstructionCount
    {
      get
      {
        return m_staticFlowControlInstructionCount;
      }
    }

    public int TempArrayCount
    {
      get
      {
        return m_tempArrayCount;
      }
    }

    public int TempRegisterCount
    {
      get
      {
        return m_tempRegisterCount;
      }
    }

    public D3DTessellatorDomain TessellatorDomain
    {
      get
      {
        return m_tessellatorDomain;
      }
    }

    public int TextureBiasInstructionCount
    {
      get
      {
        return m_textureBiasInstructionCount;
      }
    }

    public int TextureCompareInstructionCount
    {
      get
      {
        return m_textureCompareInstructionCount;
      }
    }

    public int TextureGradientInstructionCount
    {
      get
      {
        return m_textureGradientInstructionCount;
      }
    }

    public int TextureLoadInstructionCount
    {
      get
      {
        return m_textureLoadInstructionCount;
      }
    }

    public int TextureNormalInstructionCount
    {
      get
      {
        return m_textureNormalInstructionCount;
      }
    }

    public int TextureStoreInstructionCount
    {
      get
      {
        return m_textureStoreInstructionCount;
      }
    }

    public int UnsignedIntegerInstructionCount
    {
      get
      {
        return m_unsignedIntegerInstructionCount;
      }
    }

    public D3D11FeatureLevel MinFeatureLevel
    {
      get
      {
        return m_minFeatureLevel;
      }
    }

    public byte[] CompiledByteCode
    {
      get
      {
        return m_byteCode;
      }
    }

    public byte[] InputSignatureByteCode
    {
      get
      {
        return m_inputSigByteCode;
      }
    }

    public byte[] OutputSignatureByteCode
    {
      get
      {
        return m_outputSigByteCode;
      }
    }

    public int CompiledByteCodeHash
    {
      get
      {
        return m_hashCode;
      }
    }

    public int InputSignatureByteCodeHash
    {
      get
      {
        return m_inputSigHash;
      }
    }

    public int OutputSignatureByteCodeHash
    {
      get
      {
        return m_outputSigHash;
      }
    }

    public D3DShaderReflection(D3D.ShaderBytecode byteCode)
    {
      D3D.ShaderReflection reflection = new D3D.ShaderReflection(byteCode);

      Initialize(reflection);

      m_byteCode = byteCode.Data;
      m_inputSigByteCode = byteCode.GetPart(D3D.ShaderBytecodePart.InputSignatureBlob);
      m_outputSigByteCode = byteCode.GetPart(D3D.ShaderBytecodePart.OutputSignatureBlob);

      m_hashCode = BufferHelper.ComputeFNVModifiedHashCode(byteCode.Data);
      m_inputSigHash = BufferHelper.ComputeFNVModifiedHashCode(m_inputSigByteCode);
      m_outputSigHash = BufferHelper.ComputeFNVModifiedHashCode(m_outputSigByteCode);

      reflection.Dispose();
    }

    public D3DShaderReflection(byte[] byteCode)
    {
      D3D.ShaderBytecode shaderByteCode = new D3D.ShaderBytecode(byteCode);
      D3D.ShaderReflection reflection = new D3D.ShaderReflection(shaderByteCode);

      Initialize(reflection);

      m_byteCode = byteCode;
      m_inputSigByteCode = shaderByteCode.GetPart(D3D.ShaderBytecodePart.InputSignatureBlob);
      m_outputSigByteCode = shaderByteCode.GetPart(D3D.ShaderBytecodePart.OutputSignatureBlob);

      m_hashCode = BufferHelper.ComputeFNVModifiedHashCode(byteCode);
      m_inputSigHash = BufferHelper.ComputeFNVModifiedHashCode(m_inputSigByteCode);
      m_outputSigHash = BufferHelper.ComputeFNVModifiedHashCode(m_outputSigByteCode);

      shaderByteCode.Dispose();
      reflection.Dispose();
    }

    public D3DConstantBuffer GetConstantBufferByName(String name)
    {
      foreach (D3DConstantBuffer cb in m_constantBuffers)
      {
        if (cb.Name.Equals(name))
        {
          return cb;
        }
      }

      return null;
    }

    public override int GetHashCode()
    {
      return m_hashCode;
    }

    public bool AreSame(D3DShaderReflection shader, out String reasonWhyNotSame)
    {
      if (m_hashCode != shader.m_hashCode)
      {
        reasonWhyNotSame = "Bytecode hash not the same";
        return false;
      }
      if (m_shaderVersion != shader.ShaderVersion)
      {
        reasonWhyNotSame = "Shader version is not the same";
        return false;
      }

      if (m_bitwiseInstructionCount != shader.BitwiseInstructionCount
          || m_conditionalMoveInstructionCount != shader.ConditionalMoveInstructionCount
          || m_conversionInstructionCount != shader.ConversionInstructionCount
          || m_geometryShaderInputPrimitive != shader.GeometryShaderInputPrimitive
          || m_isSampleFrequencyShader != shader.IsSampleFrequencyShader
          || m_moveInstructionCount != shader.MoveInstructionCount
          || m_arrayInstructionCount != shader.ArrayInstructionCount
          || m_barrierInstructionCount != shader.BarrierInstructionCount
          || m_declarationCount != shader.DeclarationCount
          || m_defineCount != shader.DefineCount
          || m_dynamicFlowControlInstructionCount != shader.DynamicFlowControlInstructionCount
          || m_emitInstructionCount != shader.EmitInstructionCount
          || m_shaderflags != shader.ShaderFlags
          || m_floatInstructionCount != shader.FloatInstructionCount
          || m_geometryShaderInstanceCount != shader.GeometryShaderInstanceCount
          || m_geometryShaderOutputTopology != shader.GeometryShaderOutputTopology
          || m_hullShaderOutputPrimitive != shader.HullShaderOutputPrimitive
          || m_hullShaderPartitioning != shader.HullShaderPartitioning
          || m_geometryOrHullShaderInputPrimitive != shader.GeometryOrHullShaderInputPrimitive
          || m_instructionCount != shader.InstructionCount
          || m_interlockedInstructionCount != shader.InterlockedInstructionCount
          || m_integerInstructionCount != shader.IntegerInstructionCount
          || m_macroInstructionCount != shader.MacroInstructionCount
          || m_maxGeometryShaderOutputVertexCount != shader.MaxGeometryShaderOutputVertexCount
          || m_staticFlowControlInstructionCount != shader.StaticFlowControlInstructionCount
          || m_tempArrayCount != shader.TempArrayCount
          || m_tempRegisterCount != shader.TempRegisterCount
          || m_tessellatorDomain != shader.TessellatorDomain
          || m_textureBiasInstructionCount != shader.TextureBiasInstructionCount
          || m_textureCompareInstructionCount != shader.TextureCompareInstructionCount
          || m_textureGradientInstructionCount != shader.TextureGradientInstructionCount
          || m_textureLoadInstructionCount != shader.TextureLoadInstructionCount
          || m_textureNormalInstructionCount != shader.TextureNormalInstructionCount
          || m_textureStoreInstructionCount != shader.TextureStoreInstructionCount
          || m_unsignedIntegerInstructionCount != shader.UnsignedIntegerInstructionCount
          || m_minFeatureLevel != shader.MinFeatureLevel
          || !m_creator.Equals(shader.Creator))
      {
        reasonWhyNotSame = "Description not the same";
        return false;
      }

      if (!AreInputSignatureSame(shader, out reasonWhyNotSame)
          || !AreOutputSignatureSame(shader, out reasonWhyNotSame)
          || !AreBoundResourcesSame(shader, out reasonWhyNotSame)
          || !AreConstantBuffersSame(shader, out reasonWhyNotSame))
      {
        return false;
      }

      reasonWhyNotSame = String.Empty;
      return true;
    }

    private bool AreInputSignatureSame(D3DShaderReflection shader, out String reasonWhyNotSame)
    {
      D3DShaderParameter[] inputSig = shader.InputSignature;

      if (m_inputSignature.Length != inputSig.Length)
      {
        reasonWhyNotSame = "Input signature not the same count";
        return false;
      }

      for (int i = 0; i < m_inputSignature.Length; i++)
      {
        if (!m_inputSignature[i].AreSame(inputSig[i]))
        {
          reasonWhyNotSame = String.Format("Input Parameter {0} not the same", i.ToString());
          return false;
        }
      }

      reasonWhyNotSame = String.Empty;
      return true;
    }

    private bool AreOutputSignatureSame(D3DShaderReflection shader, out String reasonWhyNotSame)
    {
      D3DShaderParameter[] outputSig = shader.OutputSignature;

      if (m_outputSignature.Length != outputSig.Length)
      {
        reasonWhyNotSame = "Output signature not the same count";
        return false;
      }

      for (int i = 0; i < m_outputSignature.Length; i++)
      {
        if (!m_outputSignature[i].AreSame(outputSig[i]))
        {
          reasonWhyNotSame = String.Format("Output Parameter {0} not the same", i.ToString());
          return false;
        }
      }

      reasonWhyNotSame = String.Empty;
      return true;
    }

    private bool ArePatchSignatureSame(D3DShaderReflection shader, out String reasonWhyNotSame)
    {
      D3DShaderParameter[] patchSig = shader.PatchConstantSignature;

      if (m_patchConstantSignature.Length != patchSig.Length)
      {
        reasonWhyNotSame = "Patch Constant signature not the same count";
        return false;
      }

      for (int i = 0; i < m_patchConstantSignature.Length; i++)
      {
        if (!m_patchConstantSignature[i].AreSame(patchSig[i]))
        {
          reasonWhyNotSame = String.Format("Patch Constant Parameter {0} not the same", i.ToString());
          return false;
        }
      }

      reasonWhyNotSame = String.Empty;
      return true;
    }

    private bool AreBoundResourcesSame(D3DShaderReflection shader, out String reasonWhyNotSame)
    {
      D3DResourceBinding[] resources = shader.BoundResources;

      if (m_resourceBindings.Length != resources.Length)
      {
        reasonWhyNotSame = "Bound resource count not the same";
        return false;
      }

      for (int i = 0; i < m_resourceBindings.Length; i++)
      {
        if (!m_resourceBindings[i].AreSame(resources[i]))
        {
          reasonWhyNotSame = String.Format("Bound Resource {0} not the same", i.ToString());
          return false;
        }
      }

      reasonWhyNotSame = String.Empty;
      return true;
    }

    private bool AreConstantBuffersSame(D3DShaderReflection shader, out String reasonWhyNotSame)
    {
      D3DConstantBuffer[] buffers = shader.ConstantBuffers;

      if (m_constantBuffers.Length != buffers.Length)
      {
        reasonWhyNotSame = "Constant buffer count not the same";
        return false;
      }

      for (int i = 0; i < m_constantBuffers.Length; i++)
      {
        if (!m_constantBuffers[i].AreSame(buffers[i], out reasonWhyNotSame))
        {
          reasonWhyNotSame = String.Format("Constant Buffer {0} not the same:\n{1}", i.ToString(), reasonWhyNotSame);
          return false;
        }
      }

      reasonWhyNotSame = String.Empty;
      return true;
    }

    private void Initialize(D3D.ShaderReflection reflection)
    {
      D3D.ShaderDescription desc = reflection.Description;

      m_bitwiseInstructionCount = reflection.BitwiseInstructionCount;
      m_conditionalMoveInstructionCount = reflection.ConditionalMoveInstructionCount;
      m_conversionInstructionCount = reflection.ConversionInstructionCount;
      m_geometryShaderInputPrimitive = (D3DInputPrimitive) reflection.GeometryShaderSInputPrimitive;
      m_isSampleFrequencyShader = reflection.IsSampleFrequencyShader;
      m_moveInstructionCount = reflection.MoveInstructionCount;

      m_arrayInstructionCount = desc.ArrayInstructionCount;
      m_barrierInstructionCount = desc.BarrierInstructions;
      m_controlPointCount = desc.ControlPoints;
      m_creator = desc.Creator;
      m_cutInstructionCount = desc.CutInstructionCount;
      m_declarationCount = desc.DeclarationCount;
      m_defineCount = desc.DefineCount;
      m_dynamicFlowControlInstructionCount = desc.DynamicFlowControlCount;
      m_emitInstructionCount = desc.EmitInstructionCount;
      m_shaderflags = (CompileFlags) desc.Flags;
      m_floatInstructionCount = desc.FloatInstructionCount;
      m_geometryShaderInstanceCount = desc.GeometryShaderInstanceCount;
      m_geometryShaderOutputTopology = (D3DPrimitiveTopology) desc.GeometryShaderOutputTopology;
      m_hullShaderOutputPrimitive = (D3DTessellatorOutputPrimitive) desc.HullShaderOutputPrimitive;
      m_hullShaderPartitioning = (D3DTessellatorPartitioning) desc.HullShaderPartitioning;
      m_geometryOrHullShaderInputPrimitive = (D3DInputPrimitive) desc.InputPrimitive;
      m_instructionCount = desc.InstructionCount;
      m_interlockedInstructionCount = desc.InterlockedInstructions;
      m_integerInstructionCount = desc.IntInstructionCount;
      m_macroInstructionCount = desc.MacroInstructionCount;
      m_maxGeometryShaderOutputVertexCount = desc.GeometryShaderMaxOutputVertexCount;
      m_staticFlowControlInstructionCount = desc.StaticFlowControlCount;
      m_tempArrayCount = desc.TempArrayCount;
      m_tempRegisterCount = desc.TempRegisterCount;
      m_tessellatorDomain = (D3DTessellatorDomain) desc.TessellatorDomain;
      m_textureBiasInstructionCount = desc.TextureBiasInstructions;
      m_textureCompareInstructionCount = desc.TextureCompInstructions;
      m_textureGradientInstructionCount = desc.TextureGradientInstructions;
      m_textureLoadInstructionCount = desc.TextureLoadInstructions;
      m_textureNormalInstructionCount = desc.TextureNormalInstructions;
      m_textureStoreInstructionCount = desc.TextureStoreInstructions;
      m_unsignedIntegerInstructionCount = desc.UintInstructionCount;
      m_shaderVersion = desc.Version;
      m_minFeatureLevel = D3D11FeatureLevel.Level_11_0;// (D3D11FeatureLevel) reflection.MinFeatureLevel;

      m_constantBuffers = new D3DConstantBuffer[desc.ConstantBuffers];

      for (int i = 0; i < m_constantBuffers.Length; i++)
      {
        using (D3D.ConstantBuffer cb = reflection.GetConstantBuffer(i))
          m_constantBuffers[i] = new D3DConstantBuffer(cb);
      }

      m_resourceBindings = new D3DResourceBinding[desc.BoundResources];

      for (int i = 0; i < m_resourceBindings.Length; i++)
      {
        D3D.InputBindingDescription resource = reflection.GetResourceBindingDescription(i);
        m_resourceBindings[i] = new D3DResourceBinding(resource);
      }

      m_inputSignature = new D3DShaderParameter[desc.InputParameters];

      for (int i = 0; i < m_inputSignature.Length; i++)
      {
        D3D.ShaderParameterDescription param = reflection.GetInputParameterDescription(i);
        m_inputSignature[i] = new D3DShaderParameter(param);
      }

      m_outputSignature = new D3DShaderParameter[desc.OutputParameters];

      for (int i = 0; i < m_outputSignature.Length; i++)
      {
        D3D.ShaderParameterDescription param = reflection.GetOutputParameterDescription(i);
        m_outputSignature[i] = new D3DShaderParameter(param);
      }

      m_patchConstantSignature = new D3DShaderParameter[desc.PatchConstantParameters];

      for (int i = 0; i < m_patchConstantSignature.Length; i++)
      {
        D3D.ShaderParameterDescription param = reflection.GetPatchConstantParameterDescription(i);
        m_patchConstantSignature[i] = new D3DShaderParameter(param);
      }
    }
  }
}
