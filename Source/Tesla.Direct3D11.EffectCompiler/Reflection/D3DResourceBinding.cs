﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Diagnostics;
using D3D = SharpDX.D3DCompiler;

namespace Tesla.Direct3D11.Graphics
{
  [DebuggerDisplay("Name = {Name}, ResourceType = {ResourceType}, ResourceDimension = {ResourceDimension}")]
  public class D3DResourceBinding
  {
    private String m_name;
    private D3DShaderInputType m_resourceType;
    private D3DResourceDimension m_resourceDimension;
    private D3DResourceReturnType m_returnType;
    private D3DShaderInputFlags m_inputFlags;
    private int m_sampleCount;
    private int m_bindPoint;
    private int m_bindCount;

    public String Name
    {
      get
      {
        return m_name;
      }
    }

    public D3DShaderInputType ResourceType
    {
      get
      {
        return m_resourceType;
      }
    }

    public D3DResourceDimension ResourceDimension
    {
      get
      {
        return m_resourceDimension;
      }
    }

    public D3DResourceReturnType ReturnType
    {
      get
      {
        return m_returnType;
      }
    }

    public D3DShaderInputFlags InputFlags
    {
      get
      {
        return m_inputFlags;
      }
    }

    public int SampleCount
    {
      get
      {
        return m_sampleCount;
      }
    }

    public int BindPoint
    {
      get
      {
        return m_bindPoint;
      }
    }

    public int BindCount
    {
      get
      {
        return m_bindCount;
      }
    }

    public bool IsConstantBuffer
    {
      get
      {
        return m_resourceType == D3DShaderInputType.ConstantBuffer || m_resourceType == D3DShaderInputType.TextureBuffer;
      }
    }

    internal D3DResourceBinding(D3D.InputBindingDescription resourceBinding)
    {
      m_name = resourceBinding.Name;
      m_inputFlags = (D3DShaderInputFlags) resourceBinding.Flags;
      m_returnType = (D3DResourceReturnType) resourceBinding.ReturnType;
      m_resourceType = (D3DShaderInputType) resourceBinding.Type;
      m_resourceDimension = (D3DResourceDimension) resourceBinding.Dimension;
      m_sampleCount = resourceBinding.NumSamples;
      m_bindPoint = resourceBinding.BindPoint;
      m_bindCount = resourceBinding.BindCount;
    }

    internal bool AreSame(D3DResourceBinding resourceBinding)
    {
      if (!m_name.Equals(resourceBinding.Name)
          || m_inputFlags != resourceBinding.InputFlags
          || m_returnType != resourceBinding.ReturnType
          || m_resourceType != resourceBinding.ResourceType
          || m_resourceDimension != resourceBinding.ResourceDimension
          || m_sampleCount != resourceBinding.SampleCount
          || m_bindPoint != resourceBinding.BindPoint
          || m_bindCount != resourceBinding.BindCount)
      {
        return false;
      }

      return true;
    }
  }
}
