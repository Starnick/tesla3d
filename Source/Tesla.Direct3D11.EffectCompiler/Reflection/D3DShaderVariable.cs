﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using D3D = SharpDX.D3DCompiler;

namespace Tesla.Direct3D11.Graphics
{
  public class D3DShaderVariable
  {

    //From ShaderVariableDescription
    private String m_name;
    private int m_sizeInBytes;
    private int m_startOffset; //From Constant Buffer
    private D3DShaderVariableFlags m_flags;
    private int m_startSampler;
    private int m_samplerSize;
    private int m_startTexture;
    private int m_textureSize;
    private byte[] m_defaultValue;

    //From ShaderTypeDescription
    private D3DShaderVariableClass m_class;
    private int m_numColumns;
    private int m_numRows;
    private D3DShaderVariableType m_type;
    private int m_elementCount;

    private int m_alignedElementSizeInBytes; //Computed, actual 4-byte aligned size of individual array element

    //From ShaderReflectionType
    private D3DShaderVariableMember[] m_members;

    public String Name
    {
      get
      {
        return m_name;
      }
    }

    public int SizeInBytes
    {
      get
      {
        return m_sizeInBytes;
      }
    }

    public int AlignedElementSizeInBytes
    {
      get
      {
        return m_alignedElementSizeInBytes;
      }
    }

    public int StartOffset
    {
      get
      {
        return m_startOffset;
      }
    }

    public D3DShaderVariableFlags Flags
    {
      get
      {
        return m_flags;
      }
    }

    public int StartSampler
    {
      get
      {
        return m_startSampler;
      }
    }

    public int SamplerSize
    {
      get
      {
        return m_samplerSize;
      }
    }

    public int StartTexture
    {
      get
      {
        return m_startTexture;
      }
    }

    public int TextureSize
    {
      get
      {
        return m_textureSize;
      }
    }

    public byte[] DefaultValue
    {
      get
      {
        return m_defaultValue;
      }
    }

    public D3DShaderVariableClass VariableClass
    {
      get
      {
        return m_class;
      }
    }

    public int ColumnCount
    {
      get
      {
        return m_numColumns;
      }
    }

    public int RowCount
    {
      get
      {
        return m_numRows;
      }
    }

    public D3DShaderVariableType VariableType
    {
      get
      {
        return m_type;
      }
    }

    public int ElementCount
    {
      get
      {
        return m_elementCount;
      }
    }

    public D3DShaderVariableMember[] Members
    {
      get
      {
        return m_members;
      }
    }

    internal D3DShaderVariable(D3D.ShaderReflectionVariable variable)
    {
      D3D.ShaderVariableDescription desc = variable.Description;
      D3D.ShaderReflectionType type = variable.GetVariableType();
      D3D.ShaderTypeDescription typeDesc = type.Description;

      m_name = desc.Name;
      m_sizeInBytes = desc.Size;
      m_startOffset = desc.StartOffset;
      m_flags = (D3DShaderVariableFlags) desc.Flags;
      m_startSampler = desc.StartSampler;
      m_samplerSize = desc.SamplerSize;
      m_startTexture = desc.StartTexture;
      m_textureSize = desc.TextureSize;

      m_defaultValue = new byte[m_sizeInBytes];

      if (desc.DefaultValue != IntPtr.Zero)
        m_defaultValue.AsSpan().CopyFrom(desc.DefaultValue, m_defaultValue.Length);
      
      m_class = (D3DShaderVariableClass) typeDesc.Class;
      m_numColumns = typeDesc.ColumnCount;
      m_numRows = typeDesc.RowCount;
      m_type = (D3DShaderVariableType) typeDesc.Type;
      m_elementCount = typeDesc.ElementCount;

      m_alignedElementSizeInBytes = CalculateAlignedElementSize(m_sizeInBytes, m_elementCount);

      m_members = new D3DShaderVariableMember[typeDesc.MemberCount];

      for (int i = 0; i < m_members.Length; i++)
      {
        using (D3D.ShaderReflectionType memberType = type.GetMemberType(i))
        {
          String memberName = type.GetMemberTypeName(i);
          m_members[i] = new D3DShaderVariableMember(memberName, memberType);
        }
      }

      type.Dispose();
    }

    internal bool AreSame(D3DShaderVariable variable, out String reasonWhyNotSame)
    {
      if (!m_name.Equals(variable)
          || m_sizeInBytes != variable.SizeInBytes
          || m_startOffset != variable.StartOffset
          || m_flags != variable.Flags
          || m_startSampler != variable.StartSampler
          || m_samplerSize != variable.SamplerSize
          || m_startTexture != variable.StartTexture
          || m_textureSize != variable.TextureSize)
      {
        reasonWhyNotSame = "Description not the same";
        return false;
      }

      if (m_class != variable.VariableClass
          || m_numColumns != variable.ColumnCount
          || m_numRows != variable.RowCount
          || m_elementCount != variable.ElementCount)
      {
        reasonWhyNotSame = "Type not the same";
        return false;
      }

      D3DShaderVariableMember[] members = variable.Members;

      if (m_members.Length != members.Length)
      {
        reasonWhyNotSame = "Member count not the same";
        return false;
      }

      for (int i = 0; i < m_members.Length; i++)
      {
        if (m_members[i].AreSame(members[i], out reasonWhyNotSame))
        {
          reasonWhyNotSame = String.Format("Member {0} not the same:\n{1}", i.ToString(), reasonWhyNotSame);
          return false;
        }
      }

      reasonWhyNotSame = "";
      return true;
    }

    internal static int CalculateAlignedElementSize(int sizeInBytes, int elementCount)
    {
      int sizePerElement = (elementCount > 0) ? sizeInBytes / elementCount : sizeInBytes;

      bool needsAlignment = (sizePerElement & 0xF) != 0;
      if (needsAlignment)
        sizePerElement = ((sizePerElement >> 4) + 1) << 4;

      return sizePerElement;
    }
  }
}
