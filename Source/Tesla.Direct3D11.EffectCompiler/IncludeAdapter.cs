﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.IO;
using Tesla.Graphics;
using D3DC = SharpDX.D3DCompiler;
using SDX = SharpDX;

namespace Tesla.Direct3D11.Graphics
{
  public sealed class IncludeAdapter : SDX.CallbackBase, D3DC.Include
  {
    private IIncludeHandler m_handler;

    public IIncludeHandler IncludeHandler
    {
      get
      {
        return m_handler;
      }
    }

    public IncludeAdapter(IIncludeHandler handler)
    {
      m_handler = handler;
    }

    public void Close(Stream stream)
    {
      if (m_handler != null)
        m_handler.Close(stream);
    }

    public Stream Open(D3DC.IncludeType type, String fileName, Stream parentStream)
    {
      if (m_handler != null)
      {
        IncludeType incType = (type == D3DC.IncludeType.Local) ? IncludeType.Local : IncludeType.System;
        return m_handler.Open(incType, fileName, parentStream);
      }

      return null;
    }
  }
}
