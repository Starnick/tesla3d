﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesla;
using Tesla.Graphics;
using Tesla.SquidUI;
using Tesla.Content;
using Tesla.Scene;

namespace SampleBrowser.UI
{
    [AppDescription("Game Menu Sample", RenderSystemPlatform.Direct3D11, "GameMenu_Thumb", "GameMenu_Descr", "Graphics/InstancingSample.cs")]
    public class GameMenuSample : ForwardRendererSceneGraphApp
    {
        private Squid.Desktop m_screen;
        private Texture2D m_nebula;

        public static void RunApp(PresentationParameters pp, IPlatformInitializer init)
        {
            new GameMenuSample(pp, init).Run();
        }

        public GameMenuSample(PresentationParameters presentParams, IPlatformInitializer platformInitializer) 
            : base(presentParams, platformInitializer)
        {
        }

        protected override void OnInitialize(Engine engine)
        {
            base.OnInitialize(engine);

            SquidUISystem uiSystem = new SquidUISystem();
            uiSystem.InitializeRenderer(Content, RenderSystem.ImmediateContext);

            engine.Services.AddService<SquidUISystem>(uiSystem);
        }

        protected override void LoadContent(ContentManager content)
        {
            UISkin skin = new UISkin(Engine.Services.GetService<SquidUISystem>().Renderer.DefaultSkin);

            Squid.ControlStyle windowStyle = new Squid.ControlStyle();
            windowStyle.Tiling = Squid.TextureMode.Grid;
            windowStyle.Grid = new Squid.Margin(45, 45, 190, 45);
            windowStyle.Texture = "UI/Ion_Window.png";
            windowStyle.Tint = Squid.ColorInt.RGB(255, 148, 77);

            Squid.ControlStyle buttonStyle = new Squid.ControlStyle();
            buttonStyle.Grid = new Squid.Margin(25, 25, 25, 25);
            buttonStyle.Texture = "UI/Ion_Button.png";
            buttonStyle.Font = "Font/Stencil_32.fnt";
            buttonStyle.Tiling = Squid.TextureMode.Grid;
            buttonStyle.Tint = Squid.ColorInt.RGB(255, 148, 77);
            buttonStyle.Pressed.Tint = Squid.ColorInt.RGB(239, 125, 34);
            buttonStyle.Hot.Tint = Squid.ColorInt.RGB(255, 200, 120);

            skin.Add("window2", windowStyle);
            skin.Add("button2", buttonStyle);

            m_screen = new Squid.Desktop();
            m_screen.Skin = skin;
            m_screen.ShowCursor = false;
            base.GameWindow.IsMouseVisible = true;

            MainMenu menu = new MainMenu(this);
            menu.Show(m_screen);
            m_screen.PerformLayout();

            m_nebula = content.Load<Texture2D>("Textures/Nebula.png");

            ModelImporterParameters modelParams = new ModelImporterParameters();
            modelParams.MaterialScriptPath = "Models/Starfighter/fighter.temd";

            Spatial fighter = content.Load<Spatial>("Models/Starfighter/fighter.3ds", modelParams);

            PointLight pl = new PointLight();
            pl.Position = RenderSystem.ImmediateContext.Camera.Position;
            pl.Position = new Vector3(pl.Position.X, pl.Position.Y + 100, pl.Position.Z);
            pl.Attenuate = false;
            fighter.Lights.Add(pl);
            fighter.Translation = new Vector3(25, 0, -75);

            Matrix rotAboutX = Matrix.FromRotationX(Angle.FromDegrees(-65));
            Matrix rotAboutY = Matrix.FromRotationY(Angle.FromDegrees(45));
            Matrix rotAboutZ = Matrix.FromRotationZ(Angle.FromDegrees(15));
            Matrix rot = rotAboutX * rotAboutY * rotAboutZ;

            fighter.Rotation = Quaternion.FromRotationMatrix(rot);

            SceneRoot.Children.Add(fighter);
        }

        protected override void PreRenderScene(IRenderer renderer, IGameTime time)
        {
            SpriteBatch.Begin(renderer.RenderContext);
            SpriteBatch.Draw(m_nebula, new Rectangle(0, 0, GameWindow.ClientBounds.Width, GameWindow.ClientBounds.Height), Color.White);
            SpriteBatch.End();
        }

        protected override void PostRenderScene(IRenderer renderer, IGameTime time)
        {
            m_screen.Draw();
        }

        protected override void PreUpdateScene(IGameTime time)
        {
            m_screen.Size = new Squid.Point(RenderSystem.ImmediateContext.Camera.Viewport.Width, RenderSystem.ImmediateContext.Camera.Viewport.Height);
            m_screen.Update();
        }

        private class MainMenu : Squid.Window
        {
            public MainMenu(GameMenuSample app)
            {
                AllowDragOut = true;
                Resizable = true;
                Style = "window2";
                Size = new Squid.Point(500, 550);
                MaxSize = new Squid.Point(1000, 1000);
                Position = new Squid.Point(100, 200);

                Squid.Button startButton = CreateButton("New Game");
                startButton.Margin = new Squid.Margin(100, 75, 100, 0);
                Controls.Add(startButton);

                Squid.Button loadButton = CreateButton("Load Game");
                Controls.Add(loadButton);

                Squid.Button settingsButton = CreateButton("Settings");
                Controls.Add(settingsButton);

                Squid.Button exitButton = CreateButton("Exit to Desktop");
                exitButton.MousePress += (Squid.Control sender, Squid.MouseEventArgs args) =>
                {
                    app.Exit();
                };
                Controls.Add(exitButton);
            }

            private Squid.Button CreateButton(String text)
            {
                Squid.Button b = new Squid.Button();
                b.Style = "button2";
                b.Text = text;
                b.TextColor = Squid.ColorInt.RGB(1, 1, 1);
                b.TextAlign = Squid.Alignment.MiddleCenter;
                b.Dock = Squid.DockStyle.Top;
                b.Margin = new Squid.Margin(100, 25, 100, 0);
                b.Size = new Squid.Point(290, 75);
                b.Anchor = Squid.AnchorStyles.None;

                return b;
            }
        }
    }
}
