﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using ImGuiNET;
using Tesla;
using Tesla.Application;
using Tesla.Content;
using Tesla.Graphics;
using Tesla.Gui;
using Tesla.Input;
using Tesla.Scene;

namespace SampleBrowser.UI
{
  [AppDescription("ImGui Game Menu Sample", RenderSystemPlatform.Direct3D11, "ImGuiGameMenu_Thumb", "ImGuiGameMenu_Descr", "UI/ImGuiGameMenuSample.cs")]
  public class ImGuiGameMenuSample : ForwardRendererSceneGraphApp
  {
    private Texture2D m_nebula;
    private IWindow m_otherWindow;
    private bool m_showGuiDemo = false;
    private bool m_showFontDemo = false;
    private bool m_showConsole = false;
    private int m_currentFontFamily = 0;
    private int m_currentFontWeight = 5;
    private int m_currentFontWidth = 4;
    private List<string> m_fontFamilies;
    private List<string> m_fontDemoStrings;
    private List<string> m_fontDemoWeightStrings;
    private List<string> m_fontDemoWidthStrings;
    private List<FontWeight> m_fontDemoWeightValues;
    private List<FontWidth> m_fontDemoWidthValues;
    private bool[] m_fontDemoStyleChecks = new bool[] { false, false, false, false, false }; // italic, oblique, outline, thick outline, embedded
    private int[] m_fontDemoSizes = new int[] { 12, 13, 14, 18, 24, 36, 48, 60, 72 };
    private Vector4 m_outlineColor = new Vector4(0, 0, 0, 1);

    public static void RunApp(PresentationParameters pp, IPlatformInitializer init)
    {
      new ImGuiGameMenuSample(pp, init).Run();
    }

    public ImGuiGameMenuSample(PresentationParameters presentParams, IPlatformInitializer platformInitializer)
        : base(presentParams, platformInitializer)
    {
    }

    protected override void OnInitialize(Engine engine)
    {
      base.OnInitialize(engine);

      engine.Services.AddService<IGuiSystem>(new ImGuiSystem());

      AppGui.FontManager.AddFontPath(new FileResourceRepository("Content/Font/"));
      AppGui.FontManager.LoadFont(new GuiFont("Cascadia Mono", 14, FontStyle.Outline));
      AppGui.FontManager.LoadFont(new GuiFont("Cascadia Mono", 14, FontStyle.ThickOutline));
      AppGui.FontManager.LoadFont(new GuiFont("Cascadia Mono", 14, FontStyle.Normal));

      AppGui.FontManager.LoadFont(new GuiFont("Cascadia Mono", 22, FontStyle.Outline));
      AppGui.FontManager.LoadFont(new GuiFont("Cascadia Mono", 22, FontStyle.ThickOutline));
      AppGui.FontManager.LoadFont(new GuiFont("Cascadia Mono", 22, FontStyle.Normal));

      AppGui.FontManager.LoadFont(new GuiFont("Cascadia Mono", 60, FontStyle.Outline));
    }

    protected override void LoadContent(ContentManager content)
    {
      base.GameWindow.IsMouseVisible = true;
      base.GameWindow.EnableInputEvents = true;

      m_nebula = content.Load<Texture2D>("Textures/Nebula.png");

      ModelImporterParameters modelParams = new ModelImporterParameters();
      modelParams.MaterialScriptPath = "Models/Starfighter/fighter.temd";

      Spatial fighter = content.Load<Spatial>("Models/Starfighter/fighter.3ds", modelParams);

      PointLight pl = new PointLight();
      pl.Position = RenderSystem.ImmediateContext.Camera.Position;
      pl.Position = new Vector3(pl.Position.X, pl.Position.Y + 100, pl.Position.Z);
      pl.Attenuate = false;
      fighter.Lights.Add(pl);
      fighter.Translation = new Vector3(20, 0, -75);

      Matrix rotAboutX = Matrix.FromRotationX(Angle.FromDegrees(-65));
      Matrix rotAboutY = Matrix.FromRotationY(Angle.FromDegrees(45));
      Matrix rotAboutZ = Matrix.FromRotationZ(Angle.FromDegrees(15));
      Matrix rot = rotAboutX * rotAboutY * rotAboutZ;

      fighter.Rotation = Quaternion.FromRotationMatrix(rot);

      SceneRoot.Children.Add(fighter);

      m_fontDemoStrings = new List<string>();
      m_fontFamilies = new List<string>();
      for (int i = 0; i <= 72; i++)
        m_fontDemoStrings.Add($"[{i}] The quick brown fox jumps over the lazy dog");

      m_fontDemoWeightStrings = new List<string>(Enum.GetNames<FontWeight>());
      m_fontDemoWeightValues = new List<FontWeight>(Enum.GetValues<FontWeight>());
      m_fontDemoWidthStrings = new List<string>(Enum.GetNames<FontWidth>());
      m_fontDemoWidthValues = new List<FontWidth>(Enum.GetValues<FontWidth>());
      m_fontFamilies.Add("Cascadia Mono");
      m_fontFamilies.Add("Cascadia Code");
      m_fontFamilies.Add("Comic Sans");
      m_fontFamilies.Add("Droid Sans");
      m_fontFamilies.Add("Roboto");
      m_fontFamilies.Add("ProggyClean");
      m_fontFamilies.Add("QuickType II");
    }

    protected override void PreRenderScene(IRenderer renderer, IGameTime time)
    {
      SpriteBatch.Begin(renderer.RenderContext);
      SpriteBatch.Draw(m_nebula, new Rectangle(0, 0, GameWindow.ClientBounds.Width, GameWindow.ClientBounds.Height), Color.White);
      SpriteBatch.End();
    }

    protected override void PostRenderScene(IRenderer renderer, IGameTime time)
    {
      if (m_otherWindow is not null)
      {
        m_otherWindow.SwapChain.SetActiveAndClear(renderer.RenderContext, Color.CornflowerBlue);
        AppGui.StartOrResumeFrame(m_otherWindow);
        AppGui.FontManager.TrySetDefaultFont(GuiFont.Bold("Comic Sans", 20), m_otherWindow);

        ImGui.SetNextWindowPos(new Vector2(10, 10), ImGuiCond.Always);
        ImGui.Begin("Second Window Hello");
        ImGui.Text("Uncle Leo: Hello!");
        ImGui.End();

        AppGui.Render(renderer.RenderContext, m_otherWindow);
        m_otherWindow.SwapChain.Present();
      }
    }

    protected override void PreUpdateScene(IGameTime time)
    {
      ImGui.SetNextWindowPos(new Vector2(100, 200), ImGuiCond.Always);

      ImGui.PushStyleVar(ImGuiStyleVar.WindowPadding, new Vector2(40, 40));
      ImGui.PushStyleVar(ImGuiStyleVar.WindowRounding, 8);

      ImGui.Begin("MainMenu", ImGuiWindowFlags.NoCollapse | ImGuiWindowFlags.NoTitleBar | ImGuiWindowFlags.NoMove | ImGuiWindowFlags.AlwaysAutoResize | ImGuiWindowFlags.NoResize);

      ImGui.PushStyleVar(ImGuiStyleVar.FrameRounding, 12);

      if (ImGui.Button(m_showGuiDemo ? "Hide Demo" : "Show Demo", new Vector2(290, 75)))
        m_showGuiDemo = !m_showGuiDemo;

      ImGui.NewLine();
      if (ImGui.Button(m_otherWindow is not null ? "Close Other Window" : "Open Second Window", new Vector2(290, 75)))
      {
        if (m_otherWindow is not null)
        {
          m_otherWindow.Close();
          m_otherWindow = null;
        }
        else
        {
          PresentationParameters pp = this.GameWindow.SwapChain.PresentationParameters;
          pp.BackBufferWidth = 400;
          pp.BackBufferHeight = 200;
          m_otherWindow = this.GameHost.CreateWindow(pp, true);
          m_otherWindow.CenterToScreen();
          m_otherWindow.Closed += (IWindow sender, EventArgs e) =>
          {
            m_otherWindow = null;
          };
        }
      }

      ImGui.NewLine();
      if (ImGui.Button("Console", new Vector2(290, 75)))
        m_showConsole = !m_showConsole;

      ImGui.NewLine();
      if (ImGui.Button("Fonts", new Vector2(290, 75)))
        m_showFontDemo = !m_showFontDemo;

      if (ImGui.IsItemHovered())
      {
        ImGui.BeginTooltip();
        AppGui.PushFont(GuiFont.Bold("Arial", 25));
        ImGui.SetTooltip("Im a tooltip!");
        AppGui.PopFont();
        ImGui.EndTooltip();
      }

      ImGui.NewLine();

      if (ImGui.Button("Exit to Desktop", new Vector2(290, 75)))
        Exit();

      ImGui.PopStyleVar();

      ImGui.End();

      ImGui.PopStyleVar(2);

      Rectangle vpRect = this.GameWindow.ClientBounds;
      ImGui.SetNextWindowPos(new Vector2(vpRect.Width - 10 - 280, 10), ImGuiCond.Always);
      ImGui.SetNextWindowSize(new Vector2(280, 240), ImGuiCond.Always);
      ImGui.Begin("App Info", ImGuiWindowFlags.NoMove | ImGuiWindowFlags.NoTitleBar | ImGuiWindowFlags.NoResize);

      GuiFont appInfoHeaderFont = GuiFont.BoldItalic("Comic Sans", 18);
      GuiFont appInfoFontItalic = GuiFont.Italic("Consolas", 14);
      GuiFont appInfoFontBold = GuiFont.Bold("Cascadia Mono", 14);
      GuiFont appInfoFont = new GuiFont("Cascadia Mono", 14, FontWeight.Light);

      AppGui.PushFont(appInfoHeaderFont);
      ImGui.Text($"Frame Rate: {this.FPSCounter.CurrentFrameRate}");
      AppGui.PopFont();

      ImGui.Spacing();

      AppGui.PushFont(appInfoHeaderFont);
      ImGui.Text("Loaded Model");
      AppGui.PopFont();

      ImGui.Separator();
      ImGui.Spacing();

      AppGui.PushFont(appInfoFontItalic);
      ImGui.Text("Models/Starfighter/fighter.3ds");
      AppGui.PopFont();

      ImGui.Spacing();

      AppGui.PushFont(appInfoHeaderFont);
      ImGui.Text("App Info");
      AppGui.PopFont();

      ImGui.Separator();
      ImGui.Spacing();

      AppGui.PushFont(appInfoFont);

      AppGui.PushFont(appInfoFontBold);
      ImGui.Text("Tesla Engine");
      AppGui.PopFont();

      ImGui.SameLine();

      ImGui.Text("v1.0.0");

      AppGui.PushFont(appInfoFontBold);
      ImGui.Text("ImGui");
      AppGui.PopFont();

      ImGui.SameLine();

      ImGui.Text($"v{ImGui.GetVersion()}");
      ImGui.NewLine();
      ImGui.Text($"Window Focus: {this.GameWindow.Focused}");
      ImGui.Text($"Mouse Focus: {Mouse.FocusState.ToString()}");
      ImGui.Text($"Keyboard Focus: {Keyboard.FocusState.ToString()}");

      AppGui.PopFont();

      ImGui.End();

      if (m_showFontDemo)
      {
        ImGui.SetNextWindowPos(new Vector2(vpRect.Width * .25f, vpRect.Height * .25f), ImGuiCond.Once);
        ImGui.Begin("Fonts Demo", ImGuiWindowFlags.NoBackground);

        if (ImGui.BeginCombo("Family", m_fontFamilies[m_currentFontFamily]))
        {
          for (int i = 0; i < m_fontFamilies.Count; i++)
          {
            ImGui.PushID(i);
            if (ImGui.Selectable(m_fontFamilies[i], m_currentFontFamily == i))
              m_currentFontFamily = i;
            ImGui.PopID();
          }

          ImGui.EndCombo();
        }

        if (ImGui.BeginCombo("Weight", m_fontDemoWeightStrings[m_currentFontWeight]))
        {
          for (int i = 0; i < m_fontDemoWeightStrings.Count; i++)
          {
            ImGui.PushID(i);
            if (ImGui.Selectable(m_fontDemoWeightStrings[i], m_currentFontWeight == i))
              m_currentFontWeight = i;
            ImGui.PopID();
          }

          ImGui.EndCombo();
        }

        if (ImGui.BeginCombo("Width", m_fontDemoWidthStrings[m_currentFontWidth]))
        {
          for (int i = 0; i < m_fontDemoWidthStrings.Count; i++)
          {
            ImGui.PushID(i);
            if (ImGui.Selectable(m_fontDemoWidthStrings[i], m_currentFontWidth == i))
              m_currentFontWidth = i;
            ImGui.PopID();
          }

          ImGui.EndCombo();
        }

        ImGui.NewLine();

        ImGui.Checkbox("Italic", ref m_fontDemoStyleChecks[0]);
        ImGui.SameLine();
        ImGui.Checkbox("Oblique", ref m_fontDemoStyleChecks[1]);
        ImGui.SameLine();
        ImGui.Checkbox("Outline", ref m_fontDemoStyleChecks[2]);
        ImGui.SameLine();
        ImGui.Checkbox("Thick Outline", ref m_fontDemoStyleChecks[3]);
        ImGui.SameLine();
        ImGui.Checkbox("Embedded", ref m_fontDemoStyleChecks[4]);

        ImGui.SameLine();

        System.Numerics.Vector4 pickerValue = m_outlineColor;
        if (ImGui.ColorEdit4("Outline Color", ref pickerValue, ImGuiColorEditFlags.NoInputs))
          m_outlineColor = pickerValue;

        FontStyle fontDemoStyle = FontStyle.Normal;
        if (m_fontDemoStyleChecks[0])
          fontDemoStyle |= FontStyle.Italic;

        if (m_fontDemoStyleChecks[1])
          fontDemoStyle |= FontStyle.Oblique;

        if (m_fontDemoStyleChecks[2])
          fontDemoStyle |= FontStyle.Outline;

        if (m_fontDemoStyleChecks[3])
          fontDemoStyle |= FontStyle.ThickOutline;

        if (m_fontDemoStyleChecks[4])
          fontDemoStyle |= FontStyle.Embedded;

        ImGui.NewLine();
        ImGui.Separator();
        ImGui.Spacing();

        ImGuiEx.SetOutlineColor(new Color(m_outlineColor));

        for (int i = 0; i < m_fontDemoSizes.Length; i++)
        {
          int size = m_fontDemoSizes[i];
          GuiFont gf = new GuiFont(m_fontFamilies[m_currentFontFamily], size, m_fontDemoWeightValues[m_currentFontWeight], fontDemoStyle, m_fontDemoWidthValues[m_currentFontWidth]);
          AppGui.PushFont(gf);
          ImGui.Text(m_fontDemoStrings[size]);
          AppGui.PopFont();
          ImGui.NewLine();
        }

        ImGuiEx.ResetOutlineColor();

        ImGui.End();
      }

      if (m_showConsole && ILoggingSystem.Current is InMemoryLoggingSystem logger)
      {
        IReadOnlyList<string> log = logger.Log;
        ImGui.SetNextWindowPos(new Vector2(0, vpRect.Height - 300), ImGuiCond.Once);
        ImGui.SetNextWindowSize(new Vector2(vpRect.Width, 300), ImGuiCond.Once);
        ImGui.Begin("Console", ImGuiWindowFlags.NoCollapse);

        bool collapsed = ImGui.IsWindowCollapsed();

        if (!collapsed)
        {
          ImGuiListClipperPtr clipper = CreateListClipper();
          clipper.Begin(log.Count);
          while (clipper.Step())
          {
            for (int lineNo = clipper.DisplayStart; lineNo < clipper.DisplayEnd; lineNo++)
              ImGui.TextUnformatted(log[lineNo]);
          }
          clipper.End();
        }

        ImGui.End();
      }

      if (m_showGuiDemo)
        ImGui.ShowDemoWindow(ref m_showGuiDemo);

      AppGui.PushFont("Cascadia Mono", 14, FontWeight.Light, FontStyle.Embedded);
      ImGui.GetBackgroundDrawList().AddText(ImGui.GetMousePos(), Color.White.PackedValue, "Hello, I'm an Outlined Text Example ~1234567890-'/,.{}[]");
      AppGui.PushFont("Cascadia Mono", 14, FontWeight.Light, FontStyle.Outline);
      ImGui.GetBackgroundDrawList().AddText(ImGui.GetMousePos() + new System.Numerics.Vector2(0, 15), Color.White.PackedValue, "Hello, I'm an Outlined Text Example ~1234567890-'/,.{}[]");
      AppGui.PushFont("Cascadia Mono", 14, FontWeight.Light, FontStyle.ThickOutline);
      ImGui.GetBackgroundDrawList().AddText(ImGui.GetMousePos() + new System.Numerics.Vector2(0, 30), Color.White.PackedValue, "Hello, I'm an Outlined Text Example ~1234567890-'/,.{}[]");
      AppGui.PopFont(3);

      ImGui.GetBackgroundDrawList().AddCircleFilled(ImGui.GetMousePos(), 1, Color.Red.PackedValue);
      ImGui.GetBackgroundDrawList().AddCircleFilled(ImGui.GetMousePos() + new System.Numerics.Vector2(0, 15), 1, Color.Red.PackedValue);
      ImGui.GetBackgroundDrawList().AddCircleFilled(ImGui.GetMousePos() + new System.Numerics.Vector2(0, 30), 1, Color.Red.PackedValue);
    }

    private unsafe ImGuiListClipperPtr CreateListClipper()
    {
      return new ImGuiListClipperPtr(ImGuiNative.ImGuiListClipper_ImGuiListClipper());
    }
  }
}
