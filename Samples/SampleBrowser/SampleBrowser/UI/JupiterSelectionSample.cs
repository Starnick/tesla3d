﻿/*
* Copyright (c) 2010-2022 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.IO;
using ImGuiNET;
using Tesla;
using Tesla.Content;
using Tesla.Graphics;
using Tesla.Gui;
using Tesla.Input;
using Tesla.Scene;

namespace SampleBrowser.UI
{
  [AppDescription("Jupiter Selection Sample", RenderSystemPlatform.Direct3D11, "JupiterSelection_Thumb", "JupiterSelection_Descr", "UI/JupiterSelectionSample.cs")]
  public class JupiterSelectionSample : ForwardRendererSceneGraphApp
  {
    private Node m_jupiter, m_io, m_europa;
    private Angle m_jupiterRotSpeed = Angle.FromDegrees(-.25f);
    private Angle m_ioOrbitSpeed = Angle.FromDegrees(.1f);
    private Angle m_ioOrbitAngle = Angle.FromDegrees(-4f);
    private Angle m_europaOrbitSpeed = Angle.FromDegrees(.1f);
    private Angle m_europaOrbitAngle = Angle.FromDegrees(-3f);
    private System.Media.SoundPlayer m_music;

    private Spatial m_currentlySelected = null;
    private Spatial m_currentlyHighlighted = null;
    private PickQuery m_pickQuery = new PickQuery();

    public static void RunApp(PresentationParameters pp, IPlatformInitializer init)
    {
      new JupiterSelectionSample(pp, init).Run();
    }

    public JupiterSelectionSample(PresentationParameters presentParams, IPlatformInitializer platformInitializer)
        : base(presentParams, platformInitializer)
    {
    }

    protected override void OnInitialize(Engine engine)
    {
      base.OnInitialize(engine);

      engine.Services.AddService<IGuiSystem>(new ImGuiSystem());

      AppGui.FontManager.AddFontPath(new FileResourceRepository("Content/Font/"));
      AppGui.FontManager.LoadFont(new GuiFont("Roboto", 15));
      AppGui.FontManager.LoadFont(new GuiFont("Cascadia Mono", 14));
    }

    protected override void LoadContent(ContentManager content)
    {
      base.GameWindow.IsMouseVisible = true;
      base.GameWindow.EnableInputEvents = true;
      ClearColor = Color.Black;

      Camera.Position = new Vector3(0, 0, 2200);
      SetCameraProjection(Angle.FromDegrees(2f), Camera.NearPlaneDistance, Camera.FarPlaneDistance);
      Camera.LookAt(new Vector3(0, 0, 0), Vector3.Up);
      Camera.Update();
      CameraController.UpdateFromCamera();

      DirectionalLight dl = new DirectionalLight();
      dl.Direction = new Vector3(0.35f, -0.29f, -0.88f);
      dl.Attenuate = false;
      SceneRoot.Lights.Add(dl);

      m_jupiter = CreatePlanetaryBody("Jupiter", 69.911f, "Textures/Planets/Jupiter_albedo2.jpg");
      m_jupiter.Translation = new Vector3(0, 0, 0);
      SceneRoot.Children.Add(m_jupiter);

      m_io = CreatePlanetaryBody("Io", 1.821f, "Textures/Planets/Io_albedo.png");
      m_io.Translation = new Vector3(0, 1.5f, 493.432f);
      SceneRoot.Children.Add(m_io);

      m_europa = CreatePlanetaryBody("Europa", .97f, "Textures/Planets/Europa_albedo.png");
      m_europa.Translation = new Vector3(0, 0, 741.781f);
      SceneRoot.Children.Add(m_europa);
      SceneRoot.SetModelBounding(new BoundingSphere());

      // Our sound system seems to be broken atm
      Stream audioStream = Content.OpenStream("Audio/MistsOfMars.wav");
      m_music = new System.Media.SoundPlayer(audioStream);
      m_music.PlayLooping();

      InputTrigger selectTrigger = new InputTrigger(KeyOrMouseButton.CreateInputCondition(MouseButton.Left, false), new InputAction((IGameTime elapsed) =>
      {
        m_currentlySelected = m_currentlyHighlighted;
      }));

      Controls.InputGroup.Add(selectTrigger);
    }

    protected override void UnloadContent(ContentManager content)
    {
      base.UnloadContent(content);

      m_music.Stop();
      m_music.Dispose();
    }

    protected override void PreUpdateScene(IGameTime time)
    {
      AppGui.FontManager.TrySetDefaultFont(new GuiFont("Roboto", 15));
      m_jupiter.Rotation *= Quaternion.FromAngleAxis(m_jupiterRotSpeed * time.ElapsedTimeInSeconds, Vector3.Up);
      m_europa.Translation = CalculateOrbitPosition(ref m_europaOrbitAngle, m_europaOrbitSpeed, 0, 741.781f, time.ElapsedTimeInSeconds);
      m_io.Translation = CalculateOrbitPosition(ref m_ioOrbitAngle, m_ioOrbitSpeed, 1.5f, 493.432f, time.ElapsedTimeInSeconds);
    }

    protected override void PostUpdateScene(IGameTime time)
    {
      m_currentlyHighlighted = null;
      m_pickQuery.Clear();

      if (!GameWindow.IsMouseInsideWindow)
        return;

      m_pickQuery.Options = PickingOptions.None;
      m_pickQuery.PickRay = Camera.CreatePickRay(Mouse.GetMouseState().Position, DepthStencilState.IsReverseDepth);
      if (SceneRoot.FindPicks(m_pickQuery))
      {
        m_pickQuery.Sort();
        if (m_pickQuery.Count > 0)
        {
          PickResult hoverResult = m_pickQuery.ClosestPick;
          if (hoverResult.Target is Mesh m)
            m_currentlyHighlighted = m;
        }
      }
    }

    protected override void PostRenderScene(IRenderer renderer, IGameTime time)
    {
      ImDrawListPtr drawList = ImGui.GetBackgroundDrawList();

      if (m_currentlyHighlighted is not null && m_currentlyHighlighted != m_currentlySelected)
      {
        Rectangle boundingRect = Camera.GetScreenExtents(m_currentlyHighlighted.WorldBounding, 5);

        DrawSelectionReticle(drawList, Color.Red, (Vector2) boundingRect.Center, boundingRect.Width, boundingRect.Height, 3);   
      }

      if (m_currentlySelected is not null)
      {
        Rectangle boundingRect = Camera.GetScreenExtents(m_currentlySelected.WorldBounding, 5);

        DrawSelectionReticle(drawList, Color.Red, (Vector2) boundingRect.Center, boundingRect.Width, boundingRect.Height, 3);
        DrawPlanetToolTip(drawList, m_currentlySelected.Parent.Name, (Vector2) boundingRect.Center, 2);
      }
    }

    private void DrawPlanetToolTip(ImDrawListPtr drawList, string label, Vector2 pos, float thickness)
    {
      Span<Vector2> pts = stackalloc Vector2[3];

      pts[0] = pos;
      pts[1] = pos + new Vector2(25, 25);
      
      Vector2 labelSize = ImGui.CalcTextSize(label);
      Vector2 labelPos = (Vector2) pts[1] + new Vector2(5, -thickness - labelSize.Y);
      pts[2] = pos + new Vector2(25 + 5 + labelSize.X, 25);

      drawList.AddPolyline(pts, Color.DarkSlateBlue, thickness);

      AppGui.PushFont("Cascadia Mono", 14, FontWeight.Normal, FontStyle.Outline);
      drawList.AddText(labelPos, Color.White.PackedValue, label);
      AppGui.PopFont();
    }

    private void DrawSelectionReticle(ImDrawListPtr drawList, Color color, Vector2 pos, float width, float height, float thickness)
    {
      Span<Vector2> pts = stackalloc Vector2[4];

      float halfWidth = width * .5f;
      float halfHeight = height * .5f;
      float twentyPercentWidth = width * .2f;
      float fivePercentWidth = width * .05f;
      float twentyPercentHeight = height * .2f;
      float fivePercentHeight = height * .05f;

      float minX = pos.X - halfWidth;
      float maxX = pos.X + halfWidth;
      float minY = pos.Y - halfHeight;
      float maxY = pos.Y + halfHeight;

      // Top left
      pts[0] = new Vector2(minX, minY + twentyPercentHeight);
      pts[1] = new Vector2(minX, minY + fivePercentHeight);
      pts[2] = new Vector2(minX + fivePercentWidth, minY);
      pts[3] = new Vector2(minX + twentyPercentWidth, minY);

      drawList.AddPolyline(pts, color, thickness);

      // Top right
      pts[0] = new Vector2(maxX - twentyPercentWidth, minY);
      pts[1] = new Vector2(maxX - fivePercentWidth, minY);
      pts[2] = new Vector2(maxX, minY + fivePercentHeight);
      pts[3] = new Vector2(maxX, minY + twentyPercentHeight);

      drawList.AddPolyline(pts, color, thickness);

      // Bottom right
      pts[0] = new Vector2(maxX, maxY - twentyPercentHeight);
      pts[1] = new Vector2(maxX, maxY - fivePercentHeight);
      pts[2] = new Vector2(maxX - fivePercentWidth, maxY);
      pts[3] = new Vector2(maxX - twentyPercentWidth, maxY);

      drawList.AddPolyline(pts, color, thickness);

      // Bottom left
      pts[0] = new Vector2(minX + twentyPercentWidth, maxY);
      pts[1] = new Vector2(minX + fivePercentWidth, maxY);
      pts[2] = new Vector2(minX, maxY - fivePercentHeight);
      pts[3] = new Vector2(minX, maxY - twentyPercentHeight);

      drawList.AddPolyline(pts, color, thickness);
    }

    private Vector3 CalculateOrbitPosition(ref Angle currAngle, Angle orbitSpeed, float y, float orbitRadius, float elapsedTime)
    {
      currAngle = currAngle + (orbitSpeed * elapsedTime);
      float z = orbitRadius * currAngle.Cos;
      float x = orbitRadius * currAngle.Sin;

      return new Vector3(x, y, z);
    }

    private Node CreatePlanetaryBody(string name, float radius, string texture)
    {
      SphereGenerator sphereGen = new SphereGenerator();
      sphereGen.Radius = radius;
      sphereGen.Tessellation = 30;

      MaterialDefinition matDef = new MaterialDefinition();
      MaterialImporterParameters importParams = new MaterialImporterParameters();
      importParams.TextureParameters.GenerateMipMaps = true;
      Material mat = StandardMaterialScriptLibrary.CreateStandardMaterial("LitStandard_Texture", Content, importParams);
      mat.SetParameterResource("DiffuseMap", Content.Load<Texture2D>(texture, importParams.TextureParameters));
      matDef.Add(RenderBucketID.Opaque, mat);

      Mesh planetMesh = new Mesh($"{name}-Mesh");
      planetMesh.MaterialDefinition = matDef;
      sphereGen.BuildMeshData(planetMesh.MeshData);
      planetMesh.MeshData.Compile();
      planetMesh.Transform.SetRotation(Matrix.FromEulerAngles(Angle.FromDegrees(140), Angle.FromDegrees(0), Angle.FromDegrees(-12)));

      Node planet = new Node(name);
      planet.Children.Add(planetMesh);
      planet.SetModelBounding(new BoundingSphere());
      return planet;
    }
  }
}
