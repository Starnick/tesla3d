﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;
using Tesla.Graphics;
using Tesla;

namespace SampleBrowser
{
    public partial class BrowserForm : Form
    {
        public BrowserForm()
        {
            InitializeComponent();

            BuildSampleAppTree();

            m_sampleAppsTree.NodeMouseClick += SampleAppSelected;
            SetInfo(null);
            CenterToScreen();
        }

        private void SampleAppSelected(object sender, TreeNodeMouseClickEventArgs e)
        {
            AppReference app = e.Node.Tag as AppReference;
            SetValidOptions(app);
            SetInfo(app);
        }

        private void SetValidOptions(AppReference app)
        {
            if(app == null)
                m_renderOptionsControl.SetAllValidPlatforms();
            else
                m_renderOptionsControl.SetValidPlatforms(app.Attribute.Platforms);
        }

        private void SetInfo(AppReference app)
        {
            if(app == null || app.Attribute == null)
            {
                m_sampleThumbnail.Image = Resources.DefaultThumbnail;
                m_descriptionTextBox.Rtf = Resources.NoDescription;
            }
            else
            {
                m_sampleThumbnail.Image = app.Attribute.Thumbnail;
                m_descriptionTextBox.Rtf = app.Attribute.DescriptionText;
            }
        }

        private Int2 GetResolution(Resolution res)
        {
            switch(res)
            {
                case Resolution.Res_800x600:
                    return new Int2(800, 600);
                case Resolution.Res_1024x768:
                    return new Int2(1024, 768);
                case Resolution.Res_1280x720:
                    return new Int2(1280, 720);
                case Resolution.Res_1600x1024:
                    return new Int2(1600, 1024);
                case Resolution.Res_1920x1080:
                    return new Int2(1920, 1080);
                default:
                    return new Int2(800, 600);
            }
        }

        private MSAADescription GetMSAA(AntialiasingMode aa)
        {
            switch(aa)
            {
                case AntialiasingMode.None:
                    return MSAADescription.Default;
                case AntialiasingMode.MSAA2x:
                    return new MSAADescription(2);
                case AntialiasingMode.MSAA4x:
                    return new MSAADescription(4);
                case AntialiasingMode.MSAA8x:
                    return new MSAADescription(8);
                default:
                    return MSAADescription.Default;
            }
        }

        private ServiceDescriptor GetRenderSystem(RenderSystemPlatform platform)
        {
            switch(platform)
            {
                case RenderSystemPlatform.Direct3D11:
                    return new ServiceDescriptor("Tesla.Direct3D11.dll", "Tesla.Graphics.IRenderSystem", "Tesla.Direct3D11.Graphics.D3D11RenderSystem", true);
                default:
                    throw new ArgumentException();
            }
        }

        private void RunApp(AppReference app)
        {
            if(app == null)
                return;

            Int2 res = GetResolution(RendererSettings.Settings.Resolution);
            MSAADescription msaa = GetMSAA(RendererSettings.Settings.Antialiasing);

            PresentationParameters pp = new PresentationParameters();
            pp.BackBufferFormat = SurfaceFormat.Color;
            pp.BackBufferWidth = res.X;
            pp.BackBufferHeight = res.Y;
            pp.DepthStencilFormat = DepthFormat.Depth32Stencil8;
            pp.IsFullScreen = RendererSettings.Settings.IsFullScreen;
            pp.PresentInterval = (RendererSettings.Settings.IsVsyncEnabled) ? PresentInterval.One : PresentInterval.Immediate;
            pp.MultiSampleCount = msaa.Count;
            pp.MultiSampleQuality = msaa.QualityLevel;

            PlatformInitializer initer = new PlatformInitializer(new ServiceDescriptor[]
            {
                Platforms.GetServiceDescriptor(SubSystems.Logging.InMemory),
                GetRenderSystem(RendererSettings.Settings.RenderSystem),
                Platforms.GetServiceDescriptor(SubSystems.Application.WindowsForms),
                Platforms.GetKeyboardServiceDescriptor(SubSystems.Application.WindowsForms, SubSystems.Input.Polling),
                Platforms.GetMouseServiceDescriptor(SubSystems.Application.WindowsForms, SubSystems.Input.Polling),
            });

            Hide();

            app.RunAppInstance(pp, initer);

            Show();
        }

        private void BuildSampleAppTree()
        {
            Assembly assembly = Assembly.GetEntryAssembly();
            Type[] types = assembly.GetTypes();
            List<Type> list = new List<Type>();
            Dictionary<String, List<AppReference>> namespaces = new Dictionary<String, List<AppReference>>();

            //Collect all the types that correspond to the ISampleApplication interface and collect them into
            //lists by namespace
            foreach(Type type in types)
            {
                AppDescriptionAttribute appAtt = AppReference.GetAppDescription(type);
                if(appAtt == null)
                    continue;

                String name = type.Namespace;
                int index = name.LastIndexOf(".");
                if(index > 0)
                    name = name.Substring(index + 1);

                List<AppReference> itemList;
                if(!namespaces.TryGetValue(name, out itemList))
                {
                    itemList = new List<AppReference>();
                    namespaces.Add(name, itemList);
                }

                itemList.Add(new AppReference(type, appAtt));
            }

            //Add each namespace to the tree view
            foreach(KeyValuePair<String, List<AppReference>> kv in namespaces)
            {
                TreeNode node = new TreeNode(kv.Key);
                foreach(AppReference app in kv.Value)
                {
                    TreeNode child = new TreeNode(app.ToString());
                    child.Tag = app;
                    node.Nodes.Add(child);
                }

                m_sampleAppsTree.Nodes.Add(node);
            }

            m_sampleAppsTree.Sort();
            m_sampleAppsTree.ExpandAll();
        }

        private void OnAppRun(object sender, EventArgs e)
        {
            RunApp(m_sampleAppsTree.SelectedNode.Tag as AppReference);
        }
    }
}
