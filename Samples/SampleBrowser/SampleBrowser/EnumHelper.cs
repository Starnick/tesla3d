﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.ComponentModel;
using System.Windows.Forms;

namespace SampleBrowser
{
    public static class EnumHelper
    {
        public static bool IsBrowsable(Enum enumValue)
        {
            String description = enumValue.ToString();
            FieldInfo fieldInfo = enumValue.GetType().GetField(description);
            BrowsableAttribute attribute = fieldInfo.GetCustomAttribute<BrowsableAttribute>(false);

            return (attribute != null) ? attribute.Browsable : true;

        }

        public static String GetDescription(Enum enumValue)
        {
            String description = enumValue.ToString();
            FieldInfo fieldInfo = enumValue.GetType().GetField(description);
            DescriptionAttribute attribute = fieldInfo.GetCustomAttribute<DescriptionAttribute>(false);

            if(attribute != null)
                description = attribute.Description;

            return description;
        }

        public static IList<KeyValuePair<Enum, String>> GetEnumDescriptions(Type enumType)
        {
            Array enumValues = Enum.GetValues(enumType);
            List<KeyValuePair<Enum, String>> list = new List<KeyValuePair<Enum, String>>();

            foreach(Enum value in enumValues)
            {
                if(IsBrowsable(value))
                    list.Add(new KeyValuePair<Enum, String>(value, GetDescription(value)));
            }

            return list;
        }

        public static void BindToComboBox(ComboBox comboBox, Type enumType)
        {
            comboBox.DataSource = GetEnumDescriptions(enumType);
            comboBox.DisplayMember = "Value";
            comboBox.ValueMember = "Key";
        }

        public static void SetCurrentValue(ComboBox comboBox, Enum enumValue)
        {
            comboBox.SelectedItem = new KeyValuePair<Enum, String>(enumValue, GetDescription(enumValue));
        }

        public static T GetCurrentValue<T>(ComboBox comboBox)
        {
            KeyValuePair<Enum, String> kv = (KeyValuePair<Enum, String>) comboBox.SelectedItem;
            Object obj = kv.Key;

            return (T) obj;
        }
    }
}
