﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesla;
using Tesla.Graphics;
using Tesla.Application;
using Tesla.Content;
using Tesla.Input;
using Tesla.DesignFramework;
using Tesla.Framework;
using Tesla.Direct3D11.Graphics;
using D3D11 = SharpDX.Direct3D11;
using WayStar.Galaxy;

namespace SampleBrowser.Geometry
{
  public static class GalacticGCS
  {
    public static bool IsInGalacticCoordinates { get { return CurrentSystem is null; } }
    public static StarSystem CurrentSystem = null;

    public static float GalaxySize = 10_000;
    public static float SystemSizeLocal { get { return StarSystem.MaxStarSystemRadius; } }

    public static float SystemSizeGalactic = 10;

    public static float SystemStartScale { get { return 1.0f / GalacticStartScale; } } // 1 / 960
    public static float GalacticStartScale { get { return StarSystem.MaxStarSystemRadius / SystemSizeGalactic; } } // 9600 / 10 = 960

    public static float GalacticDistanceThresholdToSystem { get { return SystemSizeGalactic * 10; } }
    public static float LocalDistanceThresholdToGalaxy { get { return SystemSizeLocal * 1.25f; } }

    public static bool ProcessGCS(List<StarSystem> systems, Camera cam, out Vector3 newCamPos)
    {
      bool shiftedGCS = false;
      Vector3 galacticCamPos = cam.Position;

      if (!IsInGalacticCoordinates)
      {
        float dist = cam.Position.Length();
        
        // Staying in local GCS
        if (dist < SystemSizeLocal)
        {
          CurrentSystem.GalacticScale = 1.0f;
          newCamPos = cam.Position;
          return false;
        }

        // At or beyond the threshold, now shifting into galactic GCS
        Vector3 offsetDir = cam.Position;
        float offsetLength = offsetDir.Normalize();
        offsetDir *= (offsetLength * SystemStartScale);
        galacticCamPos = offsetDir + CurrentSystem.Center;

        CurrentSystem = null;
        shiftedGCS = true;
      }

      // Setup the scales for nearby star systems
      StarSystem localSystem = null;
      foreach(StarSystem system in systems)
      {
        float galDist = Vector3.Distance(galacticCamPos, system.Center);

        if (galDist >= GalacticDistanceThresholdToSystem)
        {
          system.GalacticScale = 0;// SystemStartScale;
        }
        else
        {
          // Close enough to start scaling
          if (galDist <= SystemSizeGalactic)
          {
            system.GalacticScale = 1;
          }
          else
          {
            float percent = galDist / GalacticDistanceThresholdToSystem;
            system.GalacticScale = MathHelper.Lerp(SystemStartScale, 0f, percent);
          }

          // One is at full scale, shift to local GCS
          if (localSystem is null && system.GalacticScale == 1)
            localSystem = system;
        }
      }

      // If we have a sytem at 1 scale, then shift to local GCS
      if (localSystem is not null)
      {
        shiftedGCS = true;
        Vector3 offsetDir = cam.Position - localSystem.Center;
        float offsetLength = offsetDir.Normalize();
        Vector3 systemCamPos = offsetDir * (offsetLength * GalacticStartScale);
        newCamPos = systemCamPos;
        CurrentSystem = localSystem;
      }
      else
      {
        newCamPos = galacticCamPos;
      }

      return shiftedGCS;
    }
  }

  [AppDescription("Polyline Sample", RenderSystemPlatform.Direct3D11, "", "", "")]
  public class PolylineSample : BasicCommonApp
  {
    private DesignGrid m_grid;
    private LineBatch m_lineBatcher;
    private LineString2[] m_linestrings;
    private LineString[] m_gridLines;
    private List<StarSystem> m_galaxy;

    public static void RunApp(PresentationParameters pp, IPlatformInitializer init)
    {
      new PolylineSample(pp, init).Run();
    }

    public PolylineSample(PresentationParameters presentParams, IPlatformInitializer platformInitializer)
        : base(presentParams, platformInitializer)
    {
    }

    protected override void LoadContent(ContentManager content)
    {
      Effect bufferEffect = content.Load<Effect>("Shaders/TestBuffer.fx");

      var d3d11 = RenderSystem as D3D11RenderSystem;

      D3D11.BufferDescription desc = new D3D11.BufferDescription();
      desc.BindFlags = D3D11.BindFlags.ShaderResource;
      desc.Usage = Direct3DHelper.ToD3DResourceUsage(ResourceUsage.Dynamic);
      desc.CpuAccessFlags = D3D11.CpuAccessFlags.Write;
      desc.OptionFlags = D3D11.ResourceOptionFlags.BufferStructured;
      desc.SizeInBytes = 24 * 100;
      desc.StructureByteStride = 24;

      var structuredBuffer = new D3D11.Buffer(d3d11.D3DDevice, desc);
      D3D11.ShaderResourceView v = new D3D11.ShaderResourceView(d3d11.D3DDevice, structuredBuffer);


      m_lineBatcher = new LineBatch();
      m_grid = new DesignGrid();
      m_grid.DrawMainAxes = true;
      m_grid.MajorTileSize = 1000;
      m_grid.ExtentMajorTileCount = 10;
      m_grid.MajorGridLineThickness = new GridThickness(.1f, .1f, false);

      ClearColor = Color.Black;

      Camera cam = RenderSystem.ImmediateContext.Camera;
      cam.Viewport = new Viewport(0, 0, GameWindow.ClientBounds.Width, GameWindow.ClientBounds.Height);
      cam.SetProjection(Angle.FromDegrees(65.0f), .1f, 1_000_000f, DepthStencilState.IsReverseDepth);
      cam.Position = new Vector3(-10, 60, 40);
      cam.LookAt(Vector3.Zero, Vector3.Up);

      CameraController.UpdateFromCamera();

      List<Vector3> pts = new List<Vector3>();
      pts.Add(new Vector3(0, 0, 0));
      pts.Add(new Vector3(10, 0, 0));
      pts.Add(new Vector3(10, 0, -10));
      pts.Add(new Vector3(15, 10, -10));
      pts.Add(new Vector3(25, 15, -10));
      pts.Add(new Vector3(30, 20, -15));

      List<Color> ptColors = new List<Color>();
      ptColors.Add(Color.Green);
      ptColors.Add(Color.Green);
      ptColors.Add(new Color(Color.Green, 250));
      ptColors.Add(new Color(Color.Green, 150));
      ptColors.Add(new Color(Color.Green, 75));
      ptColors.Add(new Color(Color.Green, 0));

      LineString border = LineString.CreateLineString(pts, 10, this.Content);
      // border.SetSymbology(Color.Black, 10);
      LineString2 fill = LineString2.CreateLineString(pts, 5, this.Content, ptColors);
      fill.SetSymbology(Color.CornflowerBlue, 5, 3);

      m_linestrings = new LineString2[] { /*border,*/ fill };
      this.CreateGridLines();

      m_galaxy = new List<StarSystem>();

      StarSystem systemA = new StarSystem(new Vector3(500, 0, 2500));
      systemA.LoadContent(content);

      m_galaxy.Add(systemA);

      StarSystem systemB = new StarSystem(new Vector3(-1500, 0, 4545));
      systemB.LoadContent(content);
      m_galaxy.Add(systemB);

      StarSystem systemC = new StarSystem(new Vector3(25, 0, -5));
      systemC.LoadContent(content);
      m_galaxy.Add(systemC);

      StarGenerator starGen = new StarGenerator();
      /*
      starGen.RadiusGalaxy = 13000;
      starGen.RadiusCore = 1000;
      starGen.RadiusfarField = starGen.RadiusGalaxy * 2;
      starGen.AngleOffset = Angle.FromRadians(.85f);
      starGen.Ex1 = .85;
      starGen.Ex2 = .95;
      starGen.NumStars = 40_000;
      starGen.HasDarkMatter = true;
      starGen.PertN = 2;
      starGen.PertAmp = 40;
      starGen.BaseTemp = 4_000;*/
      starGen.RadiusGalaxy = 100;
      starGen.RadiusCore = 1000;
      starGen.RadiusfarField = 2000;
      starGen.PertN = 3;
      starGen.PertAmp = 80;
      starGen.NumStars = 1_000_000;

      DataBufferBuilder<StarPoint> galaxyStars = new DataBufferBuilder<StarPoint>((int) starGen.NumStars);
      galaxyStars.SetRange(starGen.GenerateStars(2, 1));

      starGen.NumStars = 500;
      galaxyStars.SetRange(starGen.GenerateStars(6, 3));
      //Matrix scale = Matrix.FromScale(50, 0, 50);
      /*
      for (int i = 0; i < m_galaxyStars.Count; i++)
      {
        StarPoint pt = m_galaxyStars[i];
        Vector3.Transform(pt.Position, scale, out pt.Position);
        m_galaxyStars[i] = pt;
      }*/
      foreach (StarSystem s in m_galaxy)
        galaxyStars.Set(new StarPoint(s.Center, Color.Yellow, 6f));

      m_galaxyStars = galaxyStars.Claim(true);

      StarBatcher.Init(content);
    }

    private DataBuffer<StarPoint> m_galaxyStars;

    protected override void UpdateSample(IGameTime time)
    {
      var controller = this.CameraController as OrbitCameraController;
      if (GalacticGCS.ProcessGCS(m_galaxy, controller.Camera, out Vector3 newCamPos))
      {
        controller.Camera.Position = newCamPos;
        float dist = controller.Distance;
        dist *= GalacticGCS.IsInGalacticCoordinates ? GalacticGCS.SystemStartScale : GalacticGCS.GalacticStartScale;
        controller.UpdateFromCamera(dist);
      }

      controller.ZoomRecreatesTarget = true;
      var lookAt = controller.TargetLookAtPoint;
      lookAt.Y = 0;
      controller.TargetLookAtPoint = lookAt;
      controller.Update(time, false);

      foreach (var system in m_galaxy)
        system.Update(time);

      this.GameWindow.Title = controller.Camera.Position.ToString() + (GalacticGCS.IsInGalacticCoordinates ? " - Galactic" : " - System"); 
    }

    protected override void RenderSample(IRenderer renderer, IGameTime time)
    {
      foreach (var system in m_galaxy)
        system.Render(renderer, time);

      renderer.Render(true, true);
      StarBatcher.Draw(renderer.RenderContext);

      if (!GalacticGCS.IsInGalacticCoordinates)
        return;

      /*
      if (m_gridLines != null)
      {
        foreach (LineString ls in m_gridLines)
          ls.Draw(renderer.RenderContext);
      }


      if (m_linestrings != null)
      {
        foreach (LineString2 ls in m_linestrings)
          ls.Draw(renderer.RenderContext);
      }
      */

      Timer.Restart();
      StarBatcher.Append(m_galaxyStars);
      StarBatcher.Draw(renderer.RenderContext);
      Timer.Stop();
      GameWindow.Title += $", {Timer.ElapsedMilliseconds} ms";
      Test(renderer.RenderContext.Camera);
    }

    private System.Diagnostics.Stopwatch Timer = new System.Diagnostics.Stopwatch();

    private void Test(Camera cam)
    {
      Vector3 pt1 = cam.Position + (cam.Right * 100) + (cam.Direction * 200);
      Vector3 pt2 = cam.Position + (cam.Right * 100) + (cam.Direction * 400);

      Vector4 pt1PS = Vector4.Transform(new Vector4(pt1, 1), cam.ViewProjectionMatrix);
      Vector4 pt2PS = Vector4.Transform(new Vector4(pt2, 1), cam.ViewProjectionMatrix);

      Viewport vp = cam.Viewport;

      // This is how you push verts around to get it to be constant pixel-size, use the W component and viewport width/height
      // to come up with a factor to add to the point in projection space
      int weight = 1;
      Vector4 addedPt1PS = pt1PS + new Vector4(2f * weight * (pt1PS.W / vp.Width), 0, 0, 0);
      Vector4 addedPt1PS_2 = pt1PS + new Vector4(2f * weight * pt1PS.W / vp.Width, 0, 0, 0);
      Vector4 addedPt2PS = pt2PS + new Vector4(2f * weight * pt2PS.W / vp.Width, 0, 0, 0);

      Vector4 pt1NDC = pt1PS / pt1PS.W;
      Vector4 pt2NDC = pt2PS / pt2PS.W;
      Vector4 addedPt1NDC = addedPt1PS / pt1PS.W;
      Vector4 addedPt2NDC = addedPt2PS / pt2PS.W;

      Vector2 ss = new Vector2(pt1PS.X * (cam.Viewport.Width / pt1PS.W), pt1PS.Y * (cam.Viewport.Height / pt1PS.W));

      Matrix vpTransform = CreateViewportMatrix(cam.Viewport.Width, cam.Viewport.Height);
      Vector4 pt1WindowCoords = Vector4.Transform(pt1NDC, vpTransform);
      Vector4 pt2WindowCoords = Vector4.Transform(pt2NDC, vpTransform);
      Vector4 addedPt1WindowCoords = Vector4.Transform(addedPt1NDC, vpTransform);
      Vector4 addedPt2WindowCoords = Vector4.Transform(addedPt2NDC, vpTransform);
    }

    private Matrix CreateViewportMatrix(float width, float height)
    {
      const float nearDepthRange = 0.0f;
      const float farDepthRange = 1.0f;
      const float x = 0;
      const float y = 0;
      float halfWidth = width * .5f;
      float halfHeight = height * .5f;
      float halfDepth = (farDepthRange - nearDepthRange) * .5f;

      float column0Row0 = halfWidth;
      float column1Row1 = halfHeight;
      float column2Row2 = halfDepth;
      float column3Row0 = x + halfWidth;
      float column3Row1 = y + halfHeight;
      float column3Row2 = nearDepthRange + halfDepth;
      float column3Row3 = 1.0f;

      return new Matrix(column0Row0, 0f, 0f, column3Row0, 
                        0f, column1Row1, 0f, column3Row1, 
                        0f, 0f, column2Row2, column3Row2, 
                        0f, 0f, 0f, column3Row3);
    }

    protected override IRenderer SetupRenderer()
    {
      return new ForwardRenderer(RenderSystem.ImmediateContext);
    }

    private void CreateGridLines()
    {
      Vector3 right = m_grid.Axes.XAxis;
      Vector3 forward = m_grid.Axes.ZAxis;

      float minorTileSize = m_grid.MajorTileSize / (2.0f * m_grid.ExtentMinorTileCount);
      float majorGridExtent = m_grid.MajorTileSize * m_grid.ExtentMajorTileCount;
      Vector3 extentRight, extentForward;
      Vector3.Multiply(right, majorGridExtent, out extentRight);
      Vector3.Multiply(forward, majorGridExtent, out extentForward);

      Vector3 lowerLeft, lowerRight, upperLeft;
      Vector3.Subtract(m_grid.Center, extentRight, out lowerLeft);
      Vector3.Subtract(lowerLeft, extentForward, out lowerLeft);
      Vector3.Add(m_grid.Center, extentRight, out lowerRight);
      Vector3.Subtract(lowerRight, extentForward, out lowerRight);
      Vector3.Subtract(m_grid.Center, extentRight, out upperLeft);
      Vector3.Add(upperLeft, extentForward, out upperLeft);

      Vector3 horizStart = lowerLeft;
      Vector3 horizEnd = lowerRight;
      Vector3 vertStart = lowerLeft;
      Vector3 vertEnd = upperLeft;

      Vector3 horizStep, vertStep;
      Vector3.Multiply(forward, m_grid.MajorTileSize, out horizStep);
      Vector3.Multiply(right, m_grid.MajorTileSize, out vertStep);

      Vector3 minorHorizStep, minorVertStep;
      Vector3.Multiply(forward, minorTileSize, out minorHorizStep);
      Vector3.Multiply(right, minorTileSize, out minorVertStep);

      Vector3 minorHorizStart = horizStart;
      Vector3 minorHorizEnd = horizEnd;
      Vector3 minorVertStart = vertStart;
      Vector3 minorVertEnd = vertEnd;

      int totalMajorTiles = m_grid.ExtentMajorTileCount * 2;
      bool drawMinorTiles = m_grid.DrawMinorGrid;

      List<LineString> lines = new List<LineString>();
      Material protoMat = LineString.CreateLineMaterial(this.Content);

      for (int i = 0; i <= totalMajorTiles; i++)
      {
        if (m_grid.DrawMainAxes && i != m_grid.ExtentMajorTileCount)
        {
          LineString ls = new LineString(new List<Vector3>(new []{ horizStart, horizEnd }), 1, protoMat);
          ls.SetSymbology(m_grid.MajorGridLineColor, 1);
          lines.Add(ls);

          ls = new LineString(new List<Vector3>(new[] { vertStart, vertEnd }), 1, protoMat);
          ls.SetSymbology(m_grid.MajorGridLineColor, 1);
          lines.Add(ls);
        }

        minorHorizStart = horizStart;
        minorHorizEnd = horizEnd;
        minorVertStart = vertStart;
        minorVertEnd = vertEnd;

        if (drawMinorTiles && i < totalMajorTiles)
        {
          for (int j = 0; j < m_grid.ExtentMinorTileCount * 2; j++)
          {
            Vector3.Add(minorHorizStart, minorHorizStep, out minorHorizStart);
            Vector3.Add(minorHorizEnd, minorHorizStep, out minorHorizEnd);
            Vector3.Add(minorVertStart, minorVertStep, out minorVertStart);
            Vector3.Add(minorVertEnd, minorVertStep, out minorVertEnd);

            LineString ls = new LineString(new List<Vector3>(new[] { minorHorizStart, minorHorizEnd }), 1, protoMat);
            ls.SetSymbology(m_grid.MinorGridLineColor, 1);
            lines.Add(ls);

            ls = new LineString(new List<Vector3>(new[] { minorVertStart, minorVertEnd }), 1, protoMat);
            ls.SetSymbology(m_grid.MinorGridLineColor, 1);
            lines.Add(ls);
          }
        }

        Vector3.Add(horizStart, horizStep, out horizStart);
        Vector3.Add(horizEnd, horizStep, out horizEnd);
        Vector3.Add(vertStart, vertStep, out vertStart);
        Vector3.Add(vertEnd, vertStep, out vertEnd);
      }

      if (m_grid.DrawMainAxes)
      {
        Vector3.Multiply(horizStep, m_grid.ExtentMajorTileCount, out horizStep);
        Vector3.Multiply(vertStep, m_grid.ExtentMajorTileCount, out vertStep);

        Vector3.Add(lowerLeft, horizStep, out horizStart);
        Vector3.Add(lowerRight, horizStep, out horizEnd);

        Vector3.Add(lowerLeft, vertStep, out vertStart);
        Vector3.Add(upperLeft, vertStep, out vertEnd);

        Color xAxisColor = Color.Red;
        Color yAxisColor = Color.Green;

        LineString ls = new LineString(new List<Vector3>(new[] { horizStart, horizEnd }), 1, protoMat);
        ls.SetSymbology(xAxisColor, 2);
        lines.Add(ls);

        ls = new LineString(new List<Vector3>(new[] { vertStart, vertEnd }), 1, protoMat);
        ls.SetSymbology(yAxisColor, 2);
        lines.Add(ls);
      }

      m_gridLines = lines.ToArray();
    }
  }
}
