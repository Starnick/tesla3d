﻿/*
* Copyright (c) 2010-2017 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.IO;
using Tesla;
using Tesla.Content;
using Tesla.Graphics;
using Tesla.Scene;
using System.Collections.Generic;
using Tesla.Input;

namespace SampleBrowser.Geometry
{
    [AppDescription("Mesh Clipping Sample", RenderSystemPlatform.Any, "MeshClipper_Thumb", "MeshClipper_Descr", "Geometry/MeshClipperSample.cs")]
    public class MeshClipperSample : ForwardRendererSceneGraphApp
    {
        private LineBatch m_lineBatcher;
        private BoundingBox m_tileBox;

        private Mesh m_meshToClip;
        private Mesh m_clippedMesh;
        private TimedAction m_clipAction;
        private bool m_clipDirty = false;

        public static void RunApp(PresentationParameters pp, IPlatformInitializer init)
        {
            new MeshClipperSample(pp, init).Run();
        }

        public MeshClipperSample(PresentationParameters presentParams, IPlatformInitializer platformInitializer)
            : base(presentParams, platformInitializer)
        {
        }

        protected override void LoadContent(ContentManager content)
        {
            ClearColor = Tesla.Color.Black;

            m_lineBatcher = new LineBatch();
            m_tileBox = new BoundingBox(Vector3.Zero, new Vector3(10, 30, 10));
            m_clipAction = new TimedAction(TimeSpan.FromMilliseconds(40), DoClipping);

            //Set the camera to be looking at the center of the terrain and up high a bit
            Camera.Position = new Vector3(10, 40, 70);
            Camera.LookAt(Vector3.Zero, Vector3.Up);
            CameraController.UpdateFromCamera();

            //Place a light directly above the center
            PointLight pl = new PointLight();
            pl.Position = new Vector3(0, 1000, 0);
            pl.Range = 1000;
            pl.Attenuate = false;

            SceneRoot.Lights.Add(pl);

            pl = pl.Clone() as PointLight;
            pl.Position = new Vector3(0, -1000, 0);
            SceneRoot.Lights.Add(pl);

            LoadModel(content, "Models/Lighthouse_Scene/Terrain.obj");

            Controls.InputGroup.Add(new InputTrigger(KeyOrMouseButton.CreateInputCondition(Keys.Left, false, true), new InputAction((IGameTime time) =>
            {
                float speed = 10;
                m_tileBox.Center += new Vector3(-speed * time.ElapsedTimeInSeconds, 0, 0);
                m_clipDirty = true;
            })));

            Controls.InputGroup.Add(new InputTrigger(KeyOrMouseButton.CreateInputCondition(Keys.Right, false, true), new InputAction((IGameTime time) =>
            {
                float speed = 10;
                m_tileBox.Center += new Vector3(speed * time.ElapsedTimeInSeconds, 0, 0);
                m_clipDirty = true;
            })));

            Controls.InputGroup.Add(new InputTrigger(KeyOrMouseButton.CreateInputCondition(Keys.Up, false, true), new InputAction((IGameTime time) =>
            {
                float speed = 10;
                m_tileBox.Center += new Vector3(0, 0, -speed * time.ElapsedTimeInSeconds);
                m_clipDirty = true;
            })));

            Controls.InputGroup.Add(new InputTrigger(KeyOrMouseButton.CreateInputCondition(Keys.Down, false, true), new InputAction((IGameTime time) =>
            {
                float speed = 10;
                m_tileBox.Center += new Vector3(0, 0, speed * time.ElapsedTimeInSeconds);
                m_clipDirty = true;
            })));
        }

        protected override void PreUpdateScene(IGameTime time)
        {
            m_clipAction.CheckAndPerform(time);
        }

        protected override void PostRenderScene(IRenderer renderer, IGameTime time)
        {
            m_lineBatcher.Begin(renderer.RenderContext);
            DrawTilePlanes(m_tileBox, m_lineBatcher, Color.White, 0);
            m_lineBatcher.End();
        }

        //Use this to test non-indexed mesh clipping
        private void MakeMeshNonIndexed(MeshData md)
        {
            if (!md.UseIndexedPrimitives)
                return;

            DataBuffer<Vector3> pos = md.Positions;
            DataBuffer<Vector3> norms = md.Normals;
            IndexData indices = md.Indices.Value;

            DataBufferBuilder<Vector3> newPos = new DataBufferBuilder<Vector3>(indices.Length);
            DataBufferBuilder<Vector3> newNorms = new DataBufferBuilder<Vector3>(indices.Length);

            for(int i = 0; i < md.PrimitiveCount; i++)
            {
                int index0, index1, index2;
                md.GetPrimitiveVertexIndices(i, out index0, out index1, out index2);

                newPos.Set(pos[index0]);
                newPos.Set(pos[index1]);
                newPos.Set(pos[index2]);

                newNorms.Set(norms[index0]);
                newNorms.Set(norms[index1]);
                newNorms.Set(norms[index2]);
            }

            md.ClearData();
            md.Positions = newPos.Claim(true);
            md.Normals = newNorms.Claim(true);
            md.Compile();
        }

        private void LoadModel(ContentManager content, String path)
        {
            ModelImporterParameters parameters = new ModelImporterParameters();
            //parameters.SwapWindingOrder = true;

            Spatial s = content.Load<Spatial>(path, parameters);
            if (s != null)
                SceneRoot.Children.Add(s);

            List<Mesh> meshes = GetMeshes(s);

            if (meshes.Count == 0)
                return;

            Mesh msh = meshes[0];
            m_meshToClip = msh;
            //MakeMeshNonIndexed(m_meshToClip.MeshData);

            MeshClipper clipper = new MeshClipper();
            Plane west, east, north, south;
            ComputeTilePlanes(m_tileBox, out west, out east, out north, out south);
            clipper.ClipPlanes.Add(west);
            clipper.ClipPlanes.Add(east);
            clipper.ClipPlanes.Add(north);
            clipper.ClipPlanes.Add(south);

            MeshData result = clipper.Clip(msh.MeshData);

            SceneRoot.Children.Clear();

            Mesh clippedMesh = new Mesh("Clippy", result);
            clippedMesh.MeshData.Compile();
            clippedMesh.MaterialDefinition = msh.MaterialDefinition.Clone();
            SceneRoot.Children.Add(clippedMesh);

            m_clippedMesh = clippedMesh;
            m_meshToClip.MaterialDefinition.GetOpaqueOrFirstMaterial().Passes[0].RasterizerState = RasterizerState.CullNoneWireframe;
            
            SceneRoot.Children.Add(m_meshToClip);
        }

        private void DoClipping(IGameTime time)
        {
            if (!m_clipDirty)
                return;

            MeshClipper clipper = new MeshClipper();
            Plane west, east, north, south;
            ComputeTilePlanes(m_tileBox, out west, out east, out north, out south);

            clipper.ClipPlanes.Add(west);
            clipper.ClipPlanes.Add(east);
            clipper.ClipPlanes.Add(north);
            clipper.ClipPlanes.Add(south);

            clipper.Clip(m_meshToClip.MeshData, m_clippedMesh.MeshData);
            m_clippedMesh.MeshData.Compile();
            m_clipDirty = false;
        }

        private static List<Mesh> GetMeshes(Spatial s)
        {
            List<Mesh> meshes = new List<Mesh>();

            s.AcceptVisitor((Spatial sp) =>
            {
                if (sp is Mesh)
                    meshes.Add(sp as Mesh);

                return true;
            });

            return meshes;
        }

        private void ComputeTilePlanes(BoundingBox box, out Plane west, out Plane east, out Plane north, out Plane south)
        {
            Vector3 extents = box.Extents;
            Vector3 origin = box.Center;

            Vector3 westOrigin = origin - new Vector3(extents.X, 0, 0);
            Vector3 eastOrigin = origin + new Vector3(extents.X, 0, 0);
            Vector3 northOrigin = origin + new Vector3(0, 0, -extents.Z);
            Vector3 southOrigin = origin + new Vector3(0, 0, extents.Z);

            west = new Plane(new Vector3(1, 0, 0), westOrigin);
            east = new Plane(new Vector3(-1, 0, 0), eastOrigin);
            north = new Plane(new Vector3(0, 0, 1), northOrigin);
            south = new Plane(new Vector3(0, 0, -1), southOrigin);
        }

        private void DrawTilePlanes(BoundingBox box, LineBatch batch, Color c, float thickness)
        {
            Vector3 extents = box.Extents;
            Vector3 origin = box.Center;

            Vector3 westOrigin = origin - new Vector3(extents.X, 0, 0);
            Vector3 eastOrigin = origin + new Vector3(extents.X, 0, 0);
            Vector3 northOrigin = origin + new Vector3(0, 0, -extents.Z);
            Vector3 southOrigin = origin + new Vector3(0, 0, extents.Z);

            float planeWidth = extents.X;// * 2;
            float planeHeight = extents.Y;// * 2;

            Vector3 westNormal = new Vector3(1, 0, 0);
            Vector3 eastNormal = new Vector3(-1, 0, 0);
            Vector3 northNormal = new Vector3(0, 0, 1);
            Vector3 southNormal = new Vector3(0, 0, -1);

            DrawPlane(batch, westOrigin, westNormal, planeWidth, planeHeight, c, thickness);
            DrawPlane(batch, eastOrigin, eastNormal, planeWidth, planeHeight, c, thickness);
            DrawPlane(batch, northOrigin, northNormal, planeWidth, planeHeight, c, thickness);
            DrawPlane(batch, southOrigin, southNormal, planeWidth, planeHeight, c, thickness);
        }

        private void DrawPlane(LineBatch batch, in Vector3 origin, in Vector3 normal, float planeWidth, float planeHeight, Color c, float thickness)
        {
            Triad axes = Triad.FromZComplementBasis(normal);

            Vector3 right = axes.XAxis;
            right *= planeWidth;

            Vector3 up = axes.YAxis;
            up *= planeHeight;

            Vector3 v0 = (origin - right) + up;
            Vector3 v1 = (origin + right) + up;
            Vector3 v2 = (origin + right) - up;
            Vector3 v3 = (origin - right) - up;

            m_lineBatcher.DrawLine(v0, v1, c, thickness);
            m_lineBatcher.DrawLine(v1, v2, c, thickness);
            m_lineBatcher.DrawLine(v2, v3, c, thickness);
            m_lineBatcher.DrawLine(v3, v0, c, thickness);
        }
    }
}
