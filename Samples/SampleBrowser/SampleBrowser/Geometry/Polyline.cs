﻿using System;
using System.Collections.Generic;
using Tesla;
using Tesla.Graphics;

namespace SampleBrowser.Geometry
{
  [Flags]
  public enum PolylineParams : byte
  {
    None = 0,
    Square = 1 * 3,
    Miter = 2 * 3,
    MiterInsideOnly = 3 * 3,
    JointBase = 4 * 3,
    NegatePerp = 8 * 3,
    NegateAlong = 16 * 3,
    NoneAdjustWeight = 32 * 3,
  }

  public struct PolylineVertex : IVertexType
  {
    public static readonly VertexLayout VertexLayout;
    public static readonly int SizeInBytes;

    public Vector3 Point;
    public Vector3 PrevPoint;
    public Vector4 NextPointAndParams;
    public Color Color;

    public PolylineParams LineParams
    {
      get
      {
        return (PolylineParams) NextPointAndParams.W;
      }
      set
      {
        NextPointAndParams.W = (float)value;
      }
    }

    static PolylineVertex()
    {
      SizeInBytes = BufferHelper.SizeOf<PolylineVertex>();
      VertexLayout = new VertexLayout(new VertexElement[] {
                new VertexElement(VertexSemantic.Position, 0, VertexFormat.Float3, 0),
                new VertexElement(VertexSemantic.TextureCoordinate, 0, VertexFormat.Float3, 12),
                new VertexElement(VertexSemantic.TextureCoordinate, 1, VertexFormat.Float4, 24),
                new VertexElement(VertexSemantic.Color, 0, VertexFormat.Color, 40)
            }); ;
    }

    public PolylineVertex(Vector3 p, Vector3 pp, Vector3 np, PolylineParams lineParams = PolylineParams.None)
    {
      Point = p;
      PrevPoint = pp;
      NextPointAndParams = new Vector4(np, (float)lineParams);
      Color = Color.White;
    }

    public VertexLayout GetVertexLayout()
    {
      return PolylineVertex.VertexLayout;
    }
  }

  public static class PolylineHelpers
  {
    public static bool WantJointTriangles(float lineWeight, bool is2D)
    {
      // Only generate joints on a sufficiently large line
      return is2D || lineWeight >= 3;
    }

    public static DataBuffer<PolylineVertex> CreatePolylineVertexData(DataBuffer<Vector3> pts, float lineWeight, DataBuffer<Color> colors = null)
    {
      bool doJoints = WantJointTriangles(lineWeight, false);
      float maxJointDot = -0.7f;
      int lastIndex = pts.Length - 1;
      bool isClosed = pts[0].Equals(pts[lastIndex]);

      List<PolylineVertex> vertices = new List<PolylineVertex>();

      for (int i = 0; i < lastIndex; i++)
      {
        Vector3 p0 = pts[i];
        Vector3 p1 = pts[i + 1];
        bool isStart = i == 0;
        bool isEnd = (lastIndex - 1) == i;
        Vector3 prevP0 = (isStart) ? (isClosed ? pts[lastIndex - 1] : p0) : pts[i - 1];
        Vector3 nextP1 = (isEnd) ? (isClosed ? pts[1] : p1) : pts[i + 2];

        bool isP0SegmentStart = true;
        bool isP1SegmentStart = false;
        bool isP0LineStringStartOrEnd = isStart && !isClosed;
        bool isP1LineStringStartOrEnd = isEnd && !isClosed;

        PolylineVertex v0 = new PolylineVertex(p0, prevP0, p1);
        PolylineVertex v1 = new PolylineVertex(p1, nextP1, p0);

        if (colors is not null)
        {
          v0.Color = colors[i];
          v1.Color = colors[i + 1];
        }

        bool jointAtP0 = doJoints && (isClosed || !isStart) && JointDotProduct(p0, prevP0, p1) > maxJointDot;
        bool jointAtP1 = doJoints && (isClosed || !isEnd) && JointDotProduct(p1, nextP1, p0) > maxJointDot;

        if (jointAtP0 || jointAtP1) 
        {
          AddVertex(vertices, v0, ComputeLineParam(isP0SegmentStart, isP0LineStringStartOrEnd, true, jointAtP0, false, false));
          AddVertex(vertices, v1, ComputeLineParam(isP1SegmentStart, isP1LineStringStartOrEnd, false, jointAtP1, false, false));
          AddVertex(vertices, v0, ComputeLineParam(isP0SegmentStart, isP0LineStringStartOrEnd, false, jointAtP0, false, true));

          AddVertex(vertices, v0, ComputeLineParam(isP0SegmentStart, isP0LineStringStartOrEnd, false, jointAtP0, false, true));
          AddVertex(vertices, v1, ComputeLineParam(isP1SegmentStart, isP1LineStringStartOrEnd, false, jointAtP1, false, false));
          AddVertex(vertices, v1, ComputeLineParam(isP1SegmentStart, isP1LineStringStartOrEnd, false, jointAtP1, false, true));

          AddVertex(vertices, v0, ComputeLineParam(isP0SegmentStart, isP0LineStringStartOrEnd, false, jointAtP0, false, true));
          AddVertex(vertices, v1, ComputeLineParam(isP1SegmentStart, isP1LineStringStartOrEnd, false, jointAtP1, false, true));
          AddVertex(vertices, v0, ComputeLineParam(isP0SegmentStart, isP0LineStringStartOrEnd, false, jointAtP0, false, false));

          AddVertex(vertices, v0, ComputeLineParam(isP0SegmentStart, isP0LineStringStartOrEnd, false, jointAtP0, false, false));
          AddVertex(vertices, v1, ComputeLineParam(isP1SegmentStart, isP1LineStringStartOrEnd, false, jointAtP1, false, true));
          AddVertex(vertices, v1, ComputeLineParam(isP1SegmentStart, isP1LineStringStartOrEnd, true, jointAtP1, false, false));

          if (jointAtP0)
            AddJointTriangles(vertices, v0, ComputeLineParam(isP0SegmentStart, isP0LineStringStartOrEnd, false, true, false, true), v0, ComputeLineParam(isP0SegmentStart, isP0LineStringStartOrEnd, false, false, true));

          if (jointAtP1)
            AddJointTriangles(vertices, v1, ComputeLineParam(isP1SegmentStart, isP1LineStringStartOrEnd, false, true, false, true), v1, ComputeLineParam(isP1SegmentStart, isP1LineStringStartOrEnd, false, false, true));
        } 
        else
        {
          AddVertex(vertices, v0, ComputeLineParam(isP0SegmentStart, isP0LineStringStartOrEnd, true));
          AddVertex(vertices, v1, ComputeLineParam(isP1SegmentStart, isP1LineStringStartOrEnd, false));
          AddVertex(vertices, v0, ComputeLineParam(isP0SegmentStart, isP0LineStringStartOrEnd, false));

          AddVertex(vertices, v0, ComputeLineParam(isP0SegmentStart, isP0LineStringStartOrEnd, false));
          AddVertex(vertices, v1, ComputeLineParam(isP1SegmentStart, isP1LineStringStartOrEnd, false));
          AddVertex(vertices, v1, ComputeLineParam(isP1SegmentStart, isP1LineStringStartOrEnd, true));
        }
      }

      return new DataBuffer<PolylineVertex>(vertices.ToArray());
    }

    private static PolylineParams ComputeLineParam(bool isSegmentStart, bool isLineStringStartOrEnd, bool negatePerp, bool adjacentToJoint = false, bool isJoint = false, bool noDisplacement = false)
    {
      if (isJoint)
        return PolylineParams.JointBase;

      int param = (int) PolylineParams.None;

      // Prevent being tossed before width adjustment
      if (noDisplacement)
        param = (int) PolylineParams.NoneAdjustWeight;
      else if (adjacentToJoint)
        param = (int) PolylineParams.MiterInsideOnly;
      else
        param = (int) ((isLineStringStartOrEnd) ? PolylineParams.Square : PolylineParams.Miter);

      int adjust = (int) PolylineParams.None;
      if (negatePerp)
        adjust = (int) PolylineParams.NegatePerp;

      if (!isSegmentStart)
        adjust += (int) PolylineParams.NegateAlong;

      return (PolylineParams) (param + adjust);
    }

    private static float JointDotProduct(in Vector3 pt, in Vector3 prevPt, in Vector3 nextPt)
    {
      Vector3 prevDir, nextDir;
      Vector3.Subtract(pt, prevPt, out prevDir);
      Vector3.Subtract(pt, nextPt, out nextDir);

      return Vector3.Dot(prevDir, nextDir);
    }

    private static void AddVertex(List<PolylineVertex> data, PolylineVertex v, PolylineParams lineParams = PolylineParams.None)
    {
      v.LineParams = lineParams;
      data.Add(v);
    }

    private static void AddJointTriangles(List<PolylineVertex> data, in PolylineVertex v0, PolylineParams lineParam0, in PolylineVertex v1, PolylineParams lineParam1)
    {
      for (int i = 0; i < 3; i++)
      {
        PolylineHelpers.AddVertex(data, v0, lineParam0);
        PolylineHelpers.AddVertex(data, v1, (PolylineParams) ((int)lineParam1 + i + 1));
        PolylineHelpers.AddVertex(data, v1, (PolylineParams) ((int)lineParam1 + i));
      }
    }
  }
}
