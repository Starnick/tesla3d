﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesla;
using Tesla.Content;
using Tesla.Graphics;
using Tesla.Scene;
using WayStar.Galaxy;
using WayStar.Spatial;

namespace SampleBrowser.Geometry
{
  public class StarSystem
  {
    private const float GridRatio = .96f;
    public const float MaxStarSystemRadius = 10_000 * GridRatio;// 100 * GridRatio

    private HexGrid<bool> m_hexGrid;
    private LineString2 m_perimeter;
    private LineString2 m_perimeterGalacticScale;
    private List<LineString2> m_hexLines;
    private Vector3 m_center;
    private Node m_scene;
    private Camera m_systemCamera;
    private float m_galacticScale;

    public Vector3 Center { get { return m_center; } set { m_center = value; } }
    public float GalacticScale { get { return m_galacticScale; } set { m_galacticScale = value; } }

    public StarSystem(Vector3 center)
    {
      m_center = center;
      m_hexGrid = new HexGrid<bool>(Vector3.Zero, (MaxStarSystemRadius * GridRatio) / 10);
      int radius = 5;
      for (int q = -radius; q <= radius; q++)
      {
        int r1 = Math.Max(-radius, -q - radius);
        int r2 = Math.Min(radius, -q + radius);
        for (int r = r1; r <= r2; r++)
          m_hexGrid.Add(new HexCoordinate(q, r), true);
      }
    }

    public void LoadContent(ContentManager content)
    {
      m_perimeter = LineString2.CreateCircle(Vector3.Zero, MaxStarSystemRadius, 1, content);
      m_perimeter.SetSymbology(Color.DeepSkyBlue, 1, 0);

      m_perimeterGalacticScale = LineString2.CreateCircle(m_center, GalacticGCS.SystemSizeGalactic, 1, content);
      m_perimeterGalacticScale.SetSymbology(Color.DeepSkyBlue, 1, 0);

      m_systemCamera = new Camera();
      CameraProperty camProperty = new CameraProperty(m_systemCamera);

      m_scene = new Node("System");

      Material starMat = StandardMaterialScriptLibrary.CreateStandardMaterial("Standard_Color", content);
      Material litMat = StandardMaterialScriptLibrary.CreateStandardMaterial("LitStandard_Color", content);
      MaterialDefinition starMatDef = new MaterialDefinition();
      starMatDef.Add(RenderBucketID.Opaque, starMat);
      starMatDef.GetOpaqueOrFirstMaterial().SetParameterValue<Vector3>("MatDiffuse", Color.Yellow.ToVector3());

      MaterialDefinition litMatDef = new MaterialDefinition();
      litMatDef.Add(RenderBucketID.Opaque, litMat);
      litMatDef.GetOpaqueOrFirstMaterial().SetParameterValue<Vector3>("MatDiffuse", Color.LightSkyBlue.ToVector3());

      SphereGenerator sphereGen = new SphereGenerator(250);
      Mesh star = new Mesh("Star");
      star.Translation = Vector3.Zero;
      sphereGen.BuildMeshData(star.MeshData, GenerateOptions.Positions | GenerateOptions.Normals);
      star.MeshData.Compile();
      star.MaterialDefinition = starMatDef;
      star.RenderProperties.Add(camProperty);

      Mesh planet = new Mesh("Planet");
      planet.Translation = new Vector3(1250, 0, 850);
      sphereGen.Radius = 10f;
      sphereGen.BuildMeshData(planet.MeshData, GenerateOptions.Positions | GenerateOptions.Normals);
      planet.MeshData.Compile();
      planet.MaterialDefinition = litMatDef;
      planet.RenderProperties.Add(camProperty);

      BoxGenerator boxGen = new BoxGenerator(new Vector3(2));
      Mesh borg = new Mesh("Borg");
      borg.Translation = new Vector3(1300, 0, 28);
      boxGen.BuildMeshData(borg.MeshData, GenerateOptions.Positions | GenerateOptions.Normals);
      borg.MeshData.Compile();
      borg.MaterialDefinition = litMatDef.Clone();
      borg.MaterialDefinition.GetOpaqueOrFirstMaterial().SetParameterValue<Vector3>("MatDiffuse", Color.Gray.ToVector3());
      borg.RenderProperties.Add(camProperty);


      Mesh testBox1 = new Mesh("testBox1");
      testBox1.Translation = new Vector3(0, -140, 0);
      boxGen.Extents = new Vector3(250, 50, 250);
      boxGen.BuildMeshData(testBox1.MeshData, GenerateOptions.Positions | GenerateOptions.Normals);
      testBox1.MeshData.Compile();
      testBox1.MaterialDefinition = litMatDef.Clone();
      testBox1.MaterialDefinition.GetOpaqueOrFirstMaterial().SetParameterValue<Vector3>("MatDiffuse", Color.Red.ToVector3());
      testBox1.RenderProperties.Add(camProperty);

      Mesh testBox2 = new Mesh("testBox2");
      testBox2.Translation = new Vector3(0, 180, 0);
      boxGen.Extents = new Vector3(250, 50, 250);
      boxGen.BuildMeshData(testBox2.MeshData, GenerateOptions.Positions | GenerateOptions.Normals);
      testBox2.MeshData.Compile();
      testBox2.MaterialDefinition = litMatDef.Clone();
      testBox2.MaterialDefinition.GetOpaqueOrFirstMaterial().SetParameterValue<Vector3>("MatDiffuse", Color.Green.ToVector3());
      testBox2.RenderProperties.Add(camProperty);

      m_scene.Children.Add(testBox1);
      m_scene.Children.Add(testBox2);

      PointLight sunLight = new PointLight();
      sunLight.Attenuate = false;
      
      m_scene.Lights.Add(sunLight);
      m_scene.Children.Add(star);
      m_scene.Children.Add(planet);
      m_scene.Children.Add(borg);

      m_scene.SetModelBounding(new BoundingBox());

      m_hexLines = new List<LineString2>();
      foreach(Hex<bool> hex in m_hexGrid)
      {
        List<Vector3> pts = new List<Vector3>(6);
        for (int i = 0; i < hex.CornerCount; i++)
        {
          hex.ComputeCorner((HexDiagonalDirection) i, out Vector3 pt);
          float tmp = pt.Y;
          pt.Y = pt.Z;
          pt.Z = tmp;
          pts.Add(pt);
        }

        pts.Add(pts[0]);

        LineString2 gLs = LineString2.CreateLineString(pts, 1, content);
        gLs.SetSymbology(Color.CornflowerBlue, 1, 3);
        m_hexLines.Add(gLs);
      }

      var segment = LineString2.CreateLineSegment(new Vector3(0, 0, 453), new Vector3(100, 0, 453), 4, content);
      segment.SetSymbology(Color.Red, 4);
      m_hexLines.Add(segment);

      StarBatcher.Init(content);
    }

    public void Update(IGameTime time)
    {
      if (m_galacticScale == 1)
      {
        m_scene.Scale = new Vector3(1);
      //  m_scene.Translation = Vector3.Zero;
      }
      else
      {
        m_scene.Scale = new Vector3(m_galacticScale);
       // m_scene.Translation = m_center;
      }

      m_scene.Update(time);
    }

    public void Render(IRenderer renderer, IGameTime time)
    {
      if (m_galacticScale == 1)
        m_perimeter.Draw(renderer.RenderContext);
      else if (GalacticGCS.IsInGalacticCoordinates)
        m_perimeterGalacticScale.Draw(renderer.RenderContext);

      if (m_galacticScale > 0)
      {
        // In system coords always zero
        Vector3 center = GalacticGCS.IsInGalacticCoordinates ? m_center : Vector3.Zero;
        Camera prevCam = renderer.RenderContext.Camera;
        Vector3.Subtract(prevCam.Position, center, out Vector3 dir);
        float dist = dir.Normalize();
        m_systemCamera.Set(prevCam);
        Vector3 systemCamPos = Vector3.Zero;
        systemCamPos += (dir * dist);
        m_systemCamera.Position = systemCamPos;
        m_systemCamera.Update();
        renderer.RenderContext.Camera = m_systemCamera;

        m_scene.ProcessVisibleSet(renderer);

        if (!GalacticGCS.IsInGalacticCoordinates)
        {
          foreach (LineString2 ls in this.m_hexLines)
            ls.Draw(renderer.RenderContext);
        }

        /*
        DataBuffer<StarPoint> lsPts = DataBuffer.CreatePooled<StarPoint>(2);
        lsPts[0] = new StarPoint(new Vector3(0, 0, 453), Color.Green, 8);
        lsPts[1] = new StarPoint(new Vector3(100, 0, 453), Color.Blue, 6);

        StarBatcher_GS.Append(lsPts);
        StarBatcher_GS.Draw(renderer.RenderContext);
        */
        renderer.RenderContext.Camera = prevCam;
      }
      /*
      Camera cam = renderer.RenderContext.Camera;
      Vector3.Subtract(cam.Position, m_center, out Vector3 dir);
      float dist = dir.Normalize();

      if (dist <= (MaxStarSystemRadius * 3))
      {
        m_systemCamera.Set(cam);
        Vector3 systemCamPos = Vector3.Zero;
        systemCamPos += (dir * dist);
        m_systemCamera.Position = systemCamPos;
        m_systemCamera.Update();

        Camera prevCam = renderer.RenderContext.Camera;
        renderer.RenderContext.Camera = m_systemCamera;
        m_scene.ProcessVisibleSet(renderer);

        foreach (LineString ls in this.m_hexLines)
          ls.Draw(renderer.RenderContext);

        renderer.RenderContext.Camera = prevCam;
      }*/
    }
  }
}
