﻿using System;
using System.Collections.Generic;
using Tesla;
using Tesla.Direct3D11.Graphics;
using Tesla.Direct3D11.Graphics.Implementation;
using Tesla.Graphics;

namespace SampleBrowser.Geometry
{
  public struct PolylineVertex2 : IVertexType
  {
    public static readonly VertexLayout VertexLayout;
    public static readonly int SizeInBytes;

    public PolylineParams LineParams;
    public uint PointIndex;
    public uint PrevPointIndex;
    public uint NextPointIndex;

    static PolylineVertex2()
    {
      SizeInBytes = BufferHelper.SizeOf<PolylineVertex>();
      VertexLayout = new VertexLayout(new VertexElement[] {
                new VertexElement(VertexSemantic.TextureCoordinate, 0, VertexFormat.UInt, 0),
                new VertexElement(VertexSemantic.TextureCoordinate, 1, VertexFormat.UInt, 4),
                new VertexElement(VertexSemantic.TextureCoordinate, 2, VertexFormat.UInt, 8),
                new VertexElement(VertexSemantic.TextureCoordinate, 3, VertexFormat.UInt, 12),
            });
    }

    public PolylineVertex2(uint pIndex, uint pPrevIndex, uint pNextIndex)
    {
      PointIndex = pIndex;
      PrevPointIndex = pPrevIndex;
      NextPointIndex = pNextIndex;
      LineParams = PolylineParams.None;
    }

    public VertexLayout GetVertexLayout()
    {
      return PolylineVertex.VertexLayout;
    }
  }

  public static class PolylineHelpers2
  {
    public static bool WantJointTriangles(float lineWeight, bool is2D)
    {
      // Only generate joints on a sufficiently large line
      return is2D || lineWeight >= 3;
    }

    public static VertexBuffer CreatePolylineVertexData(IRenderSystem renderSystem, DataBuffer<Vector3> pts, float lineWeight, DataBuffer<Color> colors, out IShaderResource pointShaderBuffer, out IShaderResource colorShaderBuffer)
    {
      bool doJoints = WantJointTriangles(lineWeight, false);
      float maxJointDot = -0.7f;
      int lastIndex = pts.Length - 1;
      bool isClosed = pts[0].Equals(pts[lastIndex]);

      pointShaderBuffer = new StructuredShaderBuffer(renderSystem, BufferOptions.Init(pts));
      if (colors is null || colors.Length != pts.Length)
      {
        colors = new DataBuffer<Color>(pts.Length, MemoryAllocator<Color>.Managed);
        for (int i = 0; i < pts.Length; i++)
          colors[i] = Color.White;
      }

      colorShaderBuffer = new ShaderBuffer(renderSystem, VertexFormat.Color, BufferOptions.Init(colors));

      DataBufferBuilder<PolylineVertex2> vertices = new DataBufferBuilder<PolylineVertex2>(doJoints ? 15 * pts.Length : 6 * pts.Length);

      for (int i = 0; i < lastIndex; i++)
      {
        bool isStart = i == 0;
        bool isEnd = (lastIndex - 1) == i;

        int p0 = i;
        int p1 = i + 1;
        int prevP0 = (isStart) ? (isClosed ? lastIndex - 1 : p0) : i - 1;
        int nextP1 = (isEnd) ? (isClosed ? 1 : p1) : i + 2;

        bool isP0SegmentStart = true;
        bool isP1SegmentStart = false;
        bool isP0LineStringStartOrEnd = isStart && !isClosed;
        bool isP1LineStringStartOrEnd = isEnd && !isClosed;

        PolylineVertex2 v0 = new PolylineVertex2((uint)p0, (uint)prevP0, (uint)p1);
        PolylineVertex2 v1 = new PolylineVertex2((uint)p1, (uint)nextP1, (uint)p0);


        bool jointAtP0 = doJoints && (isClosed || !isStart) && JointDotProduct(pts[p0], pts[prevP0], pts[p1]) > maxJointDot;
        bool jointAtP1 = doJoints && (isClosed || !isEnd) && JointDotProduct(pts[p1], pts[nextP1], pts[p0]) > maxJointDot;

        if (jointAtP0 || jointAtP1) 
        {
          AddVertex(vertices, v0, ComputeLineParam(isP0SegmentStart, isP0LineStringStartOrEnd, true, jointAtP0, false, false));
          AddVertex(vertices, v1, ComputeLineParam(isP1SegmentStart, isP1LineStringStartOrEnd, false, jointAtP1, false, false));
          AddVertex(vertices, v0, ComputeLineParam(isP0SegmentStart, isP0LineStringStartOrEnd, false, jointAtP0, false, true));

          AddVertex(vertices, v0, ComputeLineParam(isP0SegmentStart, isP0LineStringStartOrEnd, false, jointAtP0, false, true));
          AddVertex(vertices, v1, ComputeLineParam(isP1SegmentStart, isP1LineStringStartOrEnd, false, jointAtP1, false, false));
          AddVertex(vertices, v1, ComputeLineParam(isP1SegmentStart, isP1LineStringStartOrEnd, false, jointAtP1, false, true));

          AddVertex(vertices, v0, ComputeLineParam(isP0SegmentStart, isP0LineStringStartOrEnd, false, jointAtP0, false, true));
          AddVertex(vertices, v1, ComputeLineParam(isP1SegmentStart, isP1LineStringStartOrEnd, false, jointAtP1, false, true));
          AddVertex(vertices, v0, ComputeLineParam(isP0SegmentStart, isP0LineStringStartOrEnd, false, jointAtP0, false, false));

          AddVertex(vertices, v0, ComputeLineParam(isP0SegmentStart, isP0LineStringStartOrEnd, false, jointAtP0, false, false));
          AddVertex(vertices, v1, ComputeLineParam(isP1SegmentStart, isP1LineStringStartOrEnd, false, jointAtP1, false, true));
          AddVertex(vertices, v1, ComputeLineParam(isP1SegmentStart, isP1LineStringStartOrEnd, true, jointAtP1, false, false));

          if (jointAtP0)
            AddJointTriangles(vertices, v0, ComputeLineParam(isP0SegmentStart, isP0LineStringStartOrEnd, false, true, false, true), v0, ComputeLineParam(isP0SegmentStart, isP0LineStringStartOrEnd, false, false, true));

          if (jointAtP1)
            AddJointTriangles(vertices, v1, ComputeLineParam(isP1SegmentStart, isP1LineStringStartOrEnd, false, true, false, true), v1, ComputeLineParam(isP1SegmentStart, isP1LineStringStartOrEnd, false, false, true));
        } 
        else
        {
          AddVertex(vertices, v0, ComputeLineParam(isP0SegmentStart, isP0LineStringStartOrEnd, true));
          AddVertex(vertices, v1, ComputeLineParam(isP1SegmentStart, isP1LineStringStartOrEnd, false));
          AddVertex(vertices, v0, ComputeLineParam(isP0SegmentStart, isP0LineStringStartOrEnd, false));

          AddVertex(vertices, v0, ComputeLineParam(isP0SegmentStart, isP0LineStringStartOrEnd, false));
          AddVertex(vertices, v1, ComputeLineParam(isP1SegmentStart, isP1LineStringStartOrEnd, false));
          AddVertex(vertices, v1, ComputeLineParam(isP1SegmentStart, isP1LineStringStartOrEnd, true));
        }
      }

      DataBuffer<PolylineVertex2> verts = vertices.Claim(true);
      VertexBuffer vb = new VertexBuffer(renderSystem, PolylineVertex2.VertexLayout, VertexBufferOptions.Init(verts));

      return vb;
    }

    private static PolylineParams ComputeLineParam(bool isSegmentStart, bool isLineStringStartOrEnd, bool negatePerp, bool adjacentToJoint = false, bool isJoint = false, bool noDisplacement = false)
    {
      if (isJoint)
        return PolylineParams.JointBase;

      int param = (int) PolylineParams.None;

      // Prevent being tossed before width adjustment
      if (noDisplacement)
        param = (int) PolylineParams.NoneAdjustWeight;
      else if (adjacentToJoint)
        param = (int) PolylineParams.MiterInsideOnly;
      else
        param = (int) ((isLineStringStartOrEnd) ? PolylineParams.Square : PolylineParams.Miter);

      int adjust = (int) PolylineParams.None;
      if (negatePerp)
        adjust = (int) PolylineParams.NegatePerp;

      if (!isSegmentStart)
        adjust += (int) PolylineParams.NegateAlong;

      return (PolylineParams) (param + adjust);
    }

    private static float JointDotProduct(in Vector3 pt, in Vector3 prevPt, in Vector3 nextPt)
    {
      Vector3 prevDir, nextDir;
      Vector3.Subtract(pt, prevPt, out prevDir);
      Vector3.Subtract(pt, nextPt, out nextDir);

      return Vector3.Dot(prevDir, nextDir);
    }

    private static void AddVertex(DataBufferBuilder<PolylineVertex2> data, PolylineVertex2 v, PolylineParams lineParams = PolylineParams.None)
    {
      v.LineParams = lineParams;
      data.Set(v);
    }

    private static void AddJointTriangles(DataBufferBuilder<PolylineVertex2> data, in PolylineVertex2 v0, PolylineParams lineParam0, in PolylineVertex2 v1, PolylineParams lineParam1)
    {
      for (int i = 0; i < 3; i++)
      {
        AddVertex(data, v0, lineParam0);
        AddVertex(data, v1, (PolylineParams) ((int)lineParam1 + i + 1));
        AddVertex(data, v1, (PolylineParams) ((int)lineParam1 + i));
      }
    }
  }
}
