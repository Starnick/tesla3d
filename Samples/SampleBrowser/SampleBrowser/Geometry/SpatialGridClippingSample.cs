﻿/*
* Copyright (c) 2010-2017 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.IO;
using Tesla;
using Tesla.Content;
using Tesla.Graphics;
using Tesla.Scene;
using System.Collections.Generic;
using Tesla.Input;
using Tesla.Utilities;
using System.Threading.Tasks;

namespace SampleBrowser.Geometry
{
    [AppDescription("Spatial Grid Clipping Sample", RenderSystemPlatform.Any, "SpatialGridClipping_Thumb", "SpatialGridClipping_Descr", "Geometry/SpatialGridClippingSample.cs")]
    public class SpatialGridClippingSample : ForwardRendererSceneGraphApp
    {
        private GeometryDebugger m_geomDebug;
        private SpatialGrid m_grid;
        private int m_totalTrianglesProcessed = 0;
        private double m_totalClipTimeMS = 0;

        public static void RunApp(PresentationParameters pp, IPlatformInitializer init)
        {
            new SpatialGridClippingSample(pp, init).Run();
        }

        public SpatialGridClippingSample(PresentationParameters presentParams, IPlatformInitializer platformInitializer)
            : base(presentParams, platformInitializer)
        {
        }

        protected override void LoadContent(ContentManager content)
        {
            ClearColor = Tesla.Color.Black;

            m_grid = new SpatialGrid(25, 1);
            m_geomDebug = new GeometryDebugger();

            //Set the camera to be looking at the center of the terrain and up high a bit
            Camera.Position = new Vector3(10, 40, 70);
            Camera.LookAt(Vector3.Zero, Vector3.Up);
            CameraController.UpdateFromCamera();

            //Place a light directly above the center
            PointLight pl = new PointLight();
            pl.Position = new Vector3(0, 1000, 0);
            pl.Range = 1000;
            pl.Attenuate = false;

            SceneRoot.Lights.Add(pl);

            pl = pl.Clone() as PointLight;
            pl.Position = new Vector3(0, -1000, 0);
            SceneRoot.Lights.Add(pl);

            LoadModel(content, "Models/Lighthouse_Scene/Lighthouse.obj");
            LoadModel(content, "Models/Lighthouse_Scene/Terrain.obj");
            LoadModel(content, "Models/Lighthouse_Scene/OutBuilding.obj");
            LoadModel(content, "Models/Lighthouse_Scene/Trees.obj");

            Region r = m_grid.GetRegion(new Int2(-10, 10));

        }

        protected override void PreRenderScene(IRenderer renderer, IGameTime time)
        {
            m_grid.ProcessVisibleSet(renderer);

            m_geomDebug.Begin(renderer.RenderContext);

            foreach (Region r in m_grid)
                m_geomDebug.DrawBoundingVolume(r.WorldBounds);

            m_geomDebug.End();
        }

        private void LoadModel(ContentManager content, String path)
        {
            ModelImporterParameters parameters = new ModelImporterParameters();

            Spatial s = content.Load<Spatial>(path, parameters);
            if (s == null)
                return;

            PointLight pl = new PointLight();
            pl.Position = new Vector3(0, 1000, 0);
            pl.Range = 1000;
            pl.Attenuate = false;

            s.Lights.Add(pl);

            s.SetModelBounding(new BoundingBox());
            s.Update(GameTime.ZeroTime);

            //Use the model to create the tiles so we can look at them...need to come up with a better way to do this
            TileEntry firstEntry = m_grid.Add(s);

            System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();
            timer.Start();

            //If false, we won't pass in the mesh's bounding
            bool useBoundsCheck = true;

            List<int> primCounts = new List<int>();
            List<Task<Mesh>> clipTasks = new List<Task<Mesh>>();
            s.AcceptVisitor((Spatial spatial) =>
            {
                if (spatial is Mesh)
                {
                    Mesh meshToClip = spatial as Mesh;

                    foreach (Tile tile in firstEntry.CoveredTiles)
                    {
                        MeshClipper clipper = new MeshClipper();
                        Plane west, east, north, south;
                        ComputeTilePlanes(tile.WorldBounds, out west, out east, out north, out south);
                        clipper.ClipPlanes.Add(west);
                        clipper.ClipPlanes.Add(east);
                        clipper.ClipPlanes.Add(north);
                        clipper.ClipPlanes.Add(south);

                        Task<Mesh> t = new Task<Mesh>(() =>
                        {
                            Mesh result = new Mesh(String.Format("TileMesh: [{0},{1}]", tile.GlobalTileId.X, tile.GlobalTileId.Y));

                            if (!clipper.Clip(meshToClip.MeshData, result.MeshData, null, (useBoundsCheck) ? meshToClip.WorldBounding : null))
                               return null;

                          //  if (tile.Parent.RegionId == new Int2(-10, 10))
                            //    System.Diagnostics.Debugger.Break();

                            result.MeshData.Compile();
                            result.MaterialDefinition = meshToClip.MaterialDefinition.Clone();
                            result.SetModelBounding(tile.WorldBounds.Clone(), false);
                            result.Lights.Add(pl);
                            result.Update(GameTime.ZeroTime);
                            return result;
                        });

                        t.Start();
                        clipTasks.Add(t);
                        primCounts.Add(meshToClip.MeshData.PrimitiveCount);
                    }
                }

                return true;
            });

            for(int i = 0; i < clipTasks.Count; i++)
            {
                Task<Mesh> t = clipTasks[i];

                t.Wait();

                if (t.Result != null)
                {
                    m_grid.Add(t.Result);

                    //Since we pass in the bounds of the mesh, if we have a null mesh, no clipping should have occured
                    m_totalTrianglesProcessed += primCounts[i];
                }
                else if(!useBoundsCheck)
                {
                    //If no bounds check...then we had to process the triangles anyways...so account even when mesh is null
                    m_totalTrianglesProcessed += primCounts[i];
                }
            }

            timer.Stop();
            m_totalClipTimeMS += timer.ElapsedMilliseconds;

            m_grid.Remove(firstEntry);
        }


        private void ComputeTilePlanes(BoundingBox box, out Plane west, out Plane east, out Plane north, out Plane south)
        {
            Vector3 extents = box.Extents;
            Vector3 origin = box.Center;

            Vector3 westOrigin = origin - new Vector3(extents.X, 0, 0);
            Vector3 eastOrigin = origin + new Vector3(extents.X, 0, 0);
            Vector3 northOrigin = origin + new Vector3(0, 0, -extents.Z);
            Vector3 southOrigin = origin + new Vector3(0, 0, extents.Z);

            west = new Plane(new Vector3(1, 0, 0), westOrigin);
            east = new Plane(new Vector3(-1, 0, 0), eastOrigin);
            north = new Plane(new Vector3(0, 0, 1), northOrigin);
            south = new Plane(new Vector3(0, 0, -1), southOrigin);
        }
    }
}
