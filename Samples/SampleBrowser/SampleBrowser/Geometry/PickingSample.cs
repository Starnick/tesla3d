﻿/*
* Copyright (c) 2010-2017 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.IO;
using Tesla;
using Tesla.Content;
using Tesla.Graphics;
using Tesla.Scene;
using System.Collections.Generic;
using Tesla.Input;
using Tesla.Framework;

namespace SampleBrowser.Geometry
{
  [AppDescription("Picking Sample", RenderSystemPlatform.Any, "Picking_Thumb", "Picking_Descr", "Geometry/PickingSample.cs")]
  public class PickingSample : ForwardRendererSceneGraphApp
  {
    private PickQuery m_pickQuery;
    private Mesh m_pickBall;
    private bool m_pickingFrozen = false;
    private bool m_pickingBoundsOnly = false;
    private TimedAction m_pickAction;

    //When freeze the picking, draw the pick ray...
    private Ray m_frozenPickRay;
    private LineBatch m_lineBatch;

    public static void RunApp(PresentationParameters pp, IPlatformInitializer init)
    {
      new PickingSample(pp, init).Run();
    }

    public PickingSample(PresentationParameters presentParams, IPlatformInitializer platformInitializer)
        : base(presentParams, platformInitializer)
    {
    }

    protected override void LoadContent(ContentManager content)
    {
      ClearColor = Color.Black;

      //Set the camera to be looking at the center of the terrain and up high a bit
      Camera.Position = new Vector3(10, 40, 70);
      Camera.LookAt(Vector3.Zero, Vector3.Up);
      CameraController.UpdateFromCamera();

      //Place a light directly above the center
      PointLight pl = new PointLight();
      pl.Position = new Vector3(0, 1000, 0);
      pl.Range = 1000;
      pl.Attenuate = false;

      SceneRoot.Lights.Add(pl);

      //Load some models to pick
      LoadModel(content, "Models/Lighthouse_Scene/Lighthouse.obj");
      LoadModel(content, "Models/Lighthouse_Scene/OutBuilding.obj");
      LoadModel(content, "Models/Lighthouse_Scene/Terrain.obj");
      LoadModel(content, "Models/Lighthouse_Scene/Trees.obj");

      //Create the pick query and a target indicator
      m_pickBall = CreatePickBall(content);
      m_pickQuery = new PickQuery();
      SceneRoot.Children.Add(m_pickBall);

      //Set up a timed action that will execute the picking about every 50 ms, so we don't kill our framerate
      m_pickAction = new TimedAction(TimeSpan.FromMilliseconds(50), DoPick);

      //Set up some controls to modify picking.
      Controls.InputGroup.Add(new InputTrigger(new KeyPressedCondition(MouseButton.Left, false), new InputAction((IGameTime time) =>
      {
        m_pickingFrozen = !m_pickingFrozen;

        //Set the camera controller to look at this
        if (m_pickingFrozen)
        {
          OrbitCameraController camController = CameraController as OrbitCameraController;
          if (camController != null)
            camController.TargetLookAtPoint = m_pickBall.Translation;

          m_frozenPickRay = Camera.CreatePickRay(Mouse.GetMouseState().Position);
        }
      })));

      Controls.InputGroup.Add(new InputTrigger(new KeyPressedCondition(Keys.H, false), new InputAction((IGameTime time) =>
      {
        m_pickingBoundsOnly = !m_pickingBoundsOnly;
      })));

      m_lineBatch = new LineBatch();
    }

    protected override void PreUpdateScene(IGameTime time)
    {
      m_pickAction.CheckAndPerform(time);
    }

    protected override void RenderSample(IRenderer renderer, IGameTime time)
    {
      base.RenderSample(renderer, time);
    }

    protected override void PostRenderScene(IRenderer renderer, IGameTime time)
    {
      base.PostRenderScene(renderer, time);

      //Draw the pick ray...
      if (m_pickingFrozen)
      {
        Vector3 endPt = m_pickBall.Transform.Translation;

        m_lineBatch.Begin(renderer.RenderContext);
        m_lineBatch.DrawLine(m_frozenPickRay.Origin, endPt, Color.Red, .05f);
        m_lineBatch.End();
      }
    }

    private void DoPick(IGameTime time)
    {
      if (m_pickingFrozen || !GameWindow.IsMouseInsideWindow)
        return;

      MouseState ms = Mouse.GetMouseState();
      Vector2 mousePos = ms.Position;

      Ray pickRay = Camera.CreatePickRay(mousePos);
      Vector3? pos = FindClosestPick(pickRay);

      if (pos != null)
        m_pickBall.Translation = pos.Value;
    }

    private Vector3? FindClosestPick(Ray pickRay, PickingOptions moreOptions = PickingOptions.None)
    {
      //We can set the pick query to only look at bounding volumes OR both bounds and primitives (triangles). An optimization is to do backface culling during the query
      m_pickQuery.Options = (m_pickingBoundsOnly) ? PickingOptions.None : PickingOptions.IgnoreBackfaces | PickingOptions.PrimitivePick;
      m_pickQuery.Options |= moreOptions;
      m_pickQuery.PickRay = pickRay;

      m_pickQuery.Clear();
      SceneRoot.FindPicks(m_pickQuery);

      //Sorts picks closest to farthest.
      m_pickQuery.Sort();

      PickResult closestPick = m_pickQuery.ClosestPick;
      if (closestPick != null)
      {
        Vector3 pickPoint = m_pickBall.Translation;

        //Look at the closest picked object -- a result at the very least contains a bounds pick, but it may optionally contain any number of primitive picks. We want the closest
        if (closestPick.Count > 0)
          pickPoint = closestPick.ClosestIntersection.First.Point; //Can also access an indexer, the list is sorted closest to farthest primitive pick
        else
          pickPoint = closestPick.BoundingIntersection[0].Point; //Bounds may have 0, 1, or 2 intersections at the most. Guaranteed to have at least one if we have a result.

        return pickPoint;
      }

      return null;
    }

    private Mesh CreatePickBall(ContentManager content)
    {
      SphereGenerator generator = new SphereGenerator(.1f);

      Mesh msh = new Mesh("Pickball");
      generator.BuildMeshData(msh.MeshData, GenerateOptions.Positions);
      msh.MeshData.Compile();

      Material mat = StandardMaterialScriptLibrary.CreateStandardMaterial("Standard_Color", content, "Pickball_Opaque");
      mat.SetParameterValue<Vector3>("MatDiffuse", Color.Red.ToVector3());

      MaterialDefinition matDef = new MaterialDefinition("Pickball");
      matDef.Add(RenderBucketID.Opaque, mat);

      msh.MaterialDefinition = matDef;

      return msh;
    }

    private void LoadModel(ContentManager content, String path)
    {
      ModelImporterParameters parameters = new ModelImporterParameters();

      Spatial s = content.Load<Spatial>(path, parameters);
      if (s != null)
      {
        SceneRoot.Children.Add(s);
        s.SetModelBounding(new BoundingBox());
      }
    }
  }
}
