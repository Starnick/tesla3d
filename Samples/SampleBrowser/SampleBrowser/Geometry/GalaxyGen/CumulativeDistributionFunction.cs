﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesla;

namespace WayStar.Galaxy
{
  public class CumulativeDistributionFunction
  {
    private double m_min;
    private double m_max;
    private int m_steps;
    private double m_i0;
    private double m_k;
    private double m_a;
    private double m_bulgeRadius;
    private List<double> m_m1 = new List<double>();
    private List<double> m_m2 = new List<double>();
    private List<double> m_y1 = new List<double>();
    private List<double> m_y2 = new List<double>();
    private List<double> m_x1 = new List<double>();
    private List<double> m_x2 = new List<double>();

    public double ProbFromVal(double val)
    {
      if (val < m_min || val > m_max)
        throw new ArgumentOutOfRangeException();

      double h = 2 * ((m_max - m_min) / m_steps);
      int i = (int) ((val - m_min) / h);
      double remainder = val - i * h;

      return (m_y1[i] + m_m1[i] * remainder);
    }

    public double ValFromProb(double val)
    {
      if (val < 0 || val > 1)
        throw new ArgumentOutOfRangeException();

      double h = 1.0 / (m_y2.Count - 1);
      int i = (int) Math.Floor(val / h);
      double remainder = val - i * h;

      return (m_y2[i] + m_m2[i] * remainder);
    }

    public void SetupRealistic(double i0, double k, double a, double bulgeRadius, double min, double max, int steps)
    {
      m_min = min;
      m_max = max;
      m_steps = steps;

      m_i0 = i0;
      m_k = k;
      m_a = a;
      m_bulgeRadius = bulgeRadius;

      BuildCdf();
    }

    private void BuildCdf()
    {
      double h = (m_max - m_min) / m_steps;
      double x = 0;
      double y = 0;

      m_y1.Add(0);
      m_x1.Add(0);
      for (int i = 0; i < m_steps; i += 2)
      {
        x = h * (i + 2);
        y += h / 3 * (Intensity(m_min + i * h) + 4 * Intensity(m_min + (i + 1) * h) + Intensity(m_min + (i + 2) * h));

        m_m1.Add((y - m_y1[m_y1.Count - 1]) / (2 * h));
        m_x1.Add(x);
        m_y1.Add(y);
      }

      m_m1.Add(0);

      Debug.Assert(m_m1.Count == m_x1.Count && m_m1.Count == m_y1.Count);

      for (int i = 0; i < m_y1.Count; ++i)
      {
        m_y1[i] /= m_y1[m_y1.Count - 1];
        m_m1[i] /= m_y1[m_y1.Count - 1];
      }

      m_x2.Add(0);
      m_y2.Add(0);

      double p = 0;
      h = 1.0d / m_steps;

      for (int i = 1, k = 0; i < m_steps; ++i)
      {

        p = i * h;

        for (; m_y1[k + 1] <= p; ++k) { }

        y = m_x1[k] + (p - m_y1[k] / m_m1[k]);

        m_m2.Add((y - m_y2[m_y2.Count - 1]) / h);
        m_x2.Add(p);
        m_y2.Add(y);
      }

      m_m2.Add(0);

      Debug.Assert(m_m2.Count == m_x2.Count && m_m2.Count == m_y2.Count);
    }

    private double IntensityBulge(double r, double i0, double k)
    {
      return i0 * Math.Exp(-k * Math.Pow(r, 0.25d));
    }

    private double IntensityDisc(double r, double i0, double a)
    {
      return i0 * Math.Exp(-r / a);
    }

    private double Intensity(double x)
    {
      return (x < m_bulgeRadius) ? IntensityBulge(x, m_i0, m_k) : IntensityDisc(x - m_bulgeRadius, IntensityBulge(m_bulgeRadius, m_i0, m_k), m_a);
    }
  }
}
