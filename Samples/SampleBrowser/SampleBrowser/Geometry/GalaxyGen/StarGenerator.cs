﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesla;
using Tesla.Graphics;

namespace WayStar.Galaxy
{
  public struct StarPoint : IVertexType
  {
    public Vector3 Position;
    public float Size;
    public Color Color;

    public StarPoint(Vector3 pos, Color color, float size = 1)
    {
      Position = pos;
      Color = color;
      Size = size;
    }

    public readonly VertexLayout GetVertexLayout()
    {
      VertexElement[] elems = new VertexElement[3];
      elems[0] = new VertexElement(VertexSemantic.Position, 0, VertexFormat.Float3);
      elems[1] = new VertexElement(VertexSemantic.PointSize, 0, VertexFormat.Float);
      elems[2] = new VertexElement(VertexSemantic.Color, 0, VertexFormat.Color);

      return new VertexLayout(elems);
    }
  }

  public class StarGenerator
  {
    public double RadiusGalaxy = 16000;
    public double RadiusCore = 50;
    public double RadiusfarField = 16000 * 2;
    public Angle AngleOffset = new Angle(.019f);
    public double Ex1 = .8;
    public double Ex2 = 1;
    public double NumStars = 60000;
    public double NumH2 = 400;
    public int PertN = 0;
    public double PertAmp = 0;
    public bool HasDarkMatter = true;
    public double BaseTemp = 4000;
    public double DustSize = 70;

    public List<StarPoint> GenerateStars(float baseSize = 10, float minSize = 1)
    {
      var cdf = new CumulativeDistributionFunction();
      cdf.SetupRealistic(1, .02, RadiusGalaxy / 3, RadiusCore, 0, RadiusfarField, 1000);

      List<StarPoint> stars = new List<StarPoint>();

      for (int i = 0; i < NumStars; i++)
      {
        double radius = cdf.ValFromProb(Random.Shared.NextDouble());
        double a = radius;
        double b = radius * GetExcentricity(radius);
        Angle tiltAngle = GetAngularOffset(radius);
        Angle theta0 = Angle.FromDegrees(360 * Random.Shared.NextSingle());
        double temp = 6000 + (4000 * Random.Shared.NextDouble() - 2000);
        float mag = .1f + .4f * Random.Shared.NextSingle();

        if (i < (NumStars / 60))
          mag = MathF.Min(mag + .1f + Random.Shared.NextSingle() * .4f, 1.0f);

        Vector3 pos = CalcPos((float) a, (float) b, theta0, tiltAngle);
        float size = Math.Max(minSize, baseSize * mag);
        Color color = GalaxyTemperatureColors.GetColorFromTemperature(temp);
        // color *= new Color(1, 1, 1, .25f);

        Angle randTheta = Angle.Pi * Random.Shared.NextSingle();
        Angle randPhi = Angle.TwoPi * Random.Shared.NextSingle();
        float randRadius = (float) RadiusGalaxy * (Random.Shared.NextSingle() * 100);
        pos.X = randRadius * randTheta.Sin * randPhi.Cos;
        pos.Y = randRadius * randTheta.Sin * randPhi.Sin;
        pos.Z = randRadius * randTheta.Cos;
       // color = Color.White;

        stars.Add(new StarPoint(pos, color, size));
      }

      return stars;
    }

    private Vector3 CalcPos(float a, float b, Angle theta0, Angle tiltAngle)
    {
      Angle beta = -tiltAngle;
      float cosTheta = theta0.Cos;
      float sinTheta = theta0.Sin;
      float cosBeta = beta.Cos;
      float sinBeta = beta.Sin;

      float x = a * cosTheta * cosBeta - b * sinTheta * sinBeta;
      float y = a * cosTheta * sinBeta + b * sinTheta * cosBeta;

      if (PertAmp > 0 && PertN > 0)
      {
        x += (a / (float)PertAmp) * MathF.Sin(theta0.Radians * 2.0f * PertN);
        y += (a / (float) PertAmp) * MathF.Cos(theta0.Radians * 2.0f * PertN);
      }

      return new Vector3(x, 0, y);
    }

    private Angle GetAngularOffset(double radius)
    {
      return ((float)radius) * AngleOffset;
    }

    private double GetExcentricity(double radius)
    {
      if (radius < RadiusCore)
      {
        return 1 + (radius / RadiusCore) * (Ex1 - 1);
      }
      else if (radius > RadiusCore && radius <= RadiusGalaxy)
      {
        return Ex1 + (radius - RadiusCore) / (RadiusGalaxy - RadiusCore) * (Ex2 - Ex1);
      }
      else if (radius > RadiusGalaxy && radius < RadiusfarField)
      {
        return Ex2 + (radius - RadiusGalaxy) / (RadiusfarField - RadiusGalaxy) * (1 - Ex2);
      }
      else
      {
        return 1;
      }
    }
  }
}
