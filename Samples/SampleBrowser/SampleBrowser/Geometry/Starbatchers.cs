﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesla.Content;
using Tesla.Graphics;
using Tesla;
using WayStar.Galaxy;

namespace SampleBrowser.Geometry
{
  public static class StarBatcher_Instanced_NullVB
  {
    private static Material s_mat;
    private static PrimitiveBatch<StarPoint> s_batcher;
    private static List<DataBuffer<StarPoint>> s_pending = new List<DataBuffer<StarPoint>>();

    public static void Init(ContentManager content)
    {
      if (s_batcher is not null)
        return;

      s_batcher = new PrimitiveBatch<StarPoint>(IRenderSystem.Current, 1_500_000, true);
      s_batcher.InstancedVertexBuffers = new VertexBufferBinding[2];
      s_batcher.Slot = 0;
      s_batcher.InstancedVertexBuffers[1] = new VertexBufferBinding(null);

      VertexBuffer texCoords = new VertexBuffer(IRenderSystem.Current, new VertexLayout(new VertexElement[]
      {
        new VertexElement(VertexSemantic.TextureCoordinate, 0, VertexFormat.Float2)
      }), VertexBufferOptions.Init(new DataBuffer<Vector2>(new Vector2[] { Vector2.Zero, new Vector2(0, 1), new Vector2(1, 0), Vector2.One })));
      s_batcher.InstancedVertexBuffers[0] = new VertexBufferBinding(texCoords);

      MaterialImporterParameters matParams = new MaterialImporterParameters();
      matParams.EffectParameters.CompileFlags = ShaderCompileFlags.Debug;
      s_mat = content.Load<Material>("Shaders/PolyPoint_Instanced_NullVb.tem");
      s_mat.Passes[0].BlendState = BlendState.AdditiveBlend;
      s_mat.Passes[0].DepthStencilState = DepthStencilState.DepthWriteOff;
      //s_mat.SetParameterResource("SpriteMap", content.Load<Texture2D>("Textures/PointSprite.png"));

      RasterizerState rs = new RasterizerState(IRenderSystem.Current);
      rs.DepthBias = -2;
      rs.Cull = CullMode.Back;
      rs.VertexWinding = VertexWinding.Clockwise;
      s_mat.Passes[0].RasterizerState = rs;
      s_mat.SetParameterResource("SpriteMap", Texture2D.Default2D);
      //s_mat.Passes[0].RasterizerState = RasterizerState.CullNone;
    }

    public static void Append(DataBuffer<StarPoint> stars)
    {
      if (stars.Length == 0)
        return;

      s_pending.Add(stars);
    }

    public static void Draw(IRenderContext renderContext)
    {
      if (s_pending.Count == 0)
        return;

      s_mat.ApplyMaterial(renderContext);
      s_mat.Passes[0].Apply(renderContext);
      s_batcher.Begin(renderContext);

      for (int i = 0; i < s_pending.Count; i++)
      {
        DataBuffer<StarPoint> list = s_pending[i];
        s_batcher.Draw(PrimitiveBatchTopology.PointList, list);
      }

      s_batcher.End();

      s_pending.Clear();
    }
  }

  public static class StarBatcher_Instanced
  {
    private static Material s_mat;
    private static PrimitiveBatch<StarPoint> s_batcher;
    private static List<DataBuffer<StarPoint>> s_pending = new List<DataBuffer<StarPoint>>();

    public static void Init(ContentManager content)
    {
      if (s_batcher is not null)
        return;

      s_batcher = new PrimitiveBatch<StarPoint>(IRenderSystem.Current, 1_500_000, true);
      s_batcher.InstancedVertexBuffers = new VertexBufferBinding[2];

      VertexBuffer texCoords = new VertexBuffer(IRenderSystem.Current, new VertexLayout(new VertexElement[]
      {
        new VertexElement(VertexSemantic.TextureCoordinate, 0, VertexFormat.Float2)
      }), VertexBufferOptions.Init(new DataBuffer<Vector2>(new Vector2[] { Vector2.Zero, new Vector2(0, 1), new Vector2(1, 0), Vector2.One })));
      s_batcher.InstancedVertexBuffers[0] = new VertexBufferBinding(texCoords);
      s_batcher.Slot = 1;

      MaterialImporterParameters matParams = new MaterialImporterParameters();
      matParams.EffectParameters.CompileFlags = ShaderCompileFlags.Debug;
      s_mat = content.Load<Material>("Shaders/PolyPoint_Instanced.tem");
      s_mat.Passes[0].BlendState = BlendState.AdditiveBlend;
      s_mat.Passes[0].DepthStencilState = DepthStencilState.DepthWriteOff;
      //s_mat.SetParameterResource("SpriteMap", content.Load<Texture2D>("Textures/PointSprite.png"));

      RasterizerState rs = new RasterizerState(IRenderSystem.Current);
      rs.DepthBias = -2;
      rs.Cull = CullMode.Back;
      rs.VertexWinding = VertexWinding.Clockwise;
      s_mat.Passes[0].RasterizerState = rs;
      s_mat.SetParameterResource("SpriteMap", Texture2D.Default2D);
      //s_mat.Passes[0].RasterizerState = RasterizerState.CullNone;
    }

    public static void Append(DataBuffer<StarPoint> stars)
    {
      if (stars.Length == 0)
        return;

      s_pending.Add(stars);
    }

    public static void Draw(IRenderContext renderContext)
    {
      if (s_pending.Count == 0)
        return;

      s_mat.ApplyMaterial(renderContext);
      s_mat.Passes[0].Apply(renderContext);
      s_batcher.Begin(renderContext);

      for (int i = 0; i < s_pending.Count; i++)
      {
        DataBuffer<StarPoint> list = s_pending[i];
        s_batcher.Draw(PrimitiveBatchTopology.PointList, list);
      }

      s_batcher.End();

      s_pending.Clear();
    }
  }

  public static class StarBatcher_GS
  {
    private static Material s_mat;
    private static PrimitiveBatch<StarPoint> s_batcher;
    private static List<DataBuffer<StarPoint>> s_pending = new List<DataBuffer<StarPoint>>();

    public static void Init(ContentManager content)
    {
      if (s_batcher is not null)
        return;

      s_batcher = new PrimitiveBatch<StarPoint>(IRenderSystem.Current, 1_500_000, true);
      MaterialImporterParameters matParams = new MaterialImporterParameters();
      matParams.EffectParameters.CompileFlags = ShaderCompileFlags.Debug;
      s_mat = content.Load<Material>("Shaders/PolyPoint.tem");
      s_mat.Passes[0].BlendState = BlendState.AdditiveBlend;
       s_mat.Passes[0].DepthStencilState = DepthStencilState.DepthWriteOff;
      //s_mat.SetParameterResource("SpriteMap", content.Load<Texture2D>("Textures/PointSprite.png"));

      RasterizerState rs = new RasterizerState(IRenderSystem.Current);
      rs.DepthBias = -2;
      rs.Cull = CullMode.Back;
      rs.VertexWinding = VertexWinding.Clockwise;
      s_mat.Passes[0].RasterizerState = rs;
      s_mat.SetParameterResource("SpriteMap", Texture2D.Default2D);
      //s_mat.Passes[0].RasterizerState = RasterizerState.CullNone;
    }

    public static void Append(DataBuffer<StarPoint> stars)
    {
      if (stars.Length == 0)
        return;

      s_pending.Add(stars);
    }

    public static void Draw(IRenderContext renderContext)
    {
      if (s_pending.Count == 0)
        return;

      s_mat.ApplyMaterial(renderContext);
      s_mat.Passes[0].Apply(renderContext);
      s_batcher.Begin(renderContext);

      for (int i = 0; i < s_pending.Count; i++)
      {
        DataBuffer<StarPoint> list = s_pending[i];
        s_batcher.Draw(PrimitiveBatchTopology.PointList, list);
      }

      s_batcher.End();

      s_pending.Clear();
    }
  }

  public static class StarBatcher
  {
    private static LineBatch s_batcher;
    private static Texture2D s_pointSprite;
    private static List<DataBuffer<StarPoint>> s_pending = new List<DataBuffer<StarPoint>>();

    public static void Init(ContentManager content)
    {
      if (s_batcher is not null)
        return;

      s_pointSprite = content.Load<Texture2D>("Textures/PointSprite.png");
      s_batcher = new LineBatch(IRenderSystem.Current);

      StarBatcher_GS.Init(content);
      StarBatcher_Instanced.Init(content);
      StarBatcher_Instanced_NullVB.Init(content);
    }
    public static void Append(DataBuffer<StarPoint> stars)
    {
      /*
      if (stars.Length == 0)
        return;

      s_pending.Add(stars);
      */
      StarBatcher_GS.Append(stars);
      //StarBatcher_Instanced.Append(stars);
      //StarBatcher_Instanced_NullVB.Append(stars);
    }

    public static void Draw(IRenderContext renderContext)
    {
      /*
      if (s_pending.Count == 0)
        return;

      s_batcher.Begin(renderContext, BlendState.AdditiveBlend, DepthStencilState.DepthWriteOff, RasterizerState.CullNone, s_pointSprite);

      for (int i = 0; i < s_pending.Count; i++)
      {
        DataBuffer<StarPoint> list = s_pending[i];
        for (int j = 0; j < list.Length; j++)
        {
          ref readonly StarPoint pt = ref list[i];
          s_batcher.DrawPoint(pt.Position, pt.Color, pt.Size);
        }
      }

      s_batcher.End();

      s_pending.Clear();
      */
       StarBatcher_GS.Draw(renderContext);
      //StarBatcher_Instanced.Draw(renderContext);
      //StarBatcher_Instanced_NullVB.Draw(renderContext);
    }
  }
}
