﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace SampleBrowser
{
    [Flags]
    public enum RenderSystemPlatform
    {
        [Description("Direct3D 11")]
        Direct3D11 = 1,

        [Description("MonoGame")]
        MonoGame = 2,

        [Description("OpenGL 4.5")]
        OpenGL45 = 4,

        [Description("Any")]
        [Browsable(false)]
        Any = Direct3D11 | MonoGame | OpenGL45
    }

    public enum AntialiasingMode
    {
        [Description("None")]
        None = 0,

        [Description("MSAA 2x")]
        MSAA2x = 1,

        [Description("MSAA 4x")]
        MSAA4x = 2,

        [Description("MSAA 8x")]
        MSAA8x = 4
    }

    public enum Resolution
    {
        [Description("800 x 600")]
        Res_800x600 = 0,

        [Description("1024 x 768")]
        Res_1024x768 = 1,

        [Description("1280 x 720")]
        Res_1280x720 = 2,

        [Description("1600 x 1024")]
        Res_1600x1024 = 4,

        [Description("1920 x 1080")]
        Res_1920x1080 = 8
    }
}
