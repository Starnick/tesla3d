﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesla.Graphics;
using Tesla;
using Tesla.Application;
using Tesla.Input;
using Tesla.Content;
using Tesla.Utilities;
using Tesla.Scene;

namespace SampleBrowser
{
    public abstract class ScenegraphApp : BasicCommonApp
    {
        private Node m_root;
        private GeometryDebuggerVisitor m_geomDebug;

        public Node SceneRoot
        {
            get
            {
                return m_root;
            }
        }

        public GeometryDebuggerVisitor GeometryDebug
        {
            get
            {
                return m_geomDebug;
            }
        }

        public ScenegraphApp(PresentationParameters presentParams, IPlatformInitializer platformInitializer) 
            : base(presentParams, platformInitializer)
        {
        }

        protected override void OnInitialize(Engine engine)
        {
            base.OnInitialize(engine);

            m_geomDebug = new GeometryDebuggerVisitor(RenderSystem);
            m_root = new Node("Root");
        }

        protected override void RenderSample(IRenderer renderer, IGameTime time)
        {
            PreRenderScene(renderer, time);

            m_root.ProcessVisibleSet(renderer);

            if(Controls.DisplayWireframe)
            {
                renderer.RenderContext.RasterizerState = RasterizerState.CullNoneWireframe;
                renderer.RenderContext.EnforcedRenderState = EnforcedRenderState.RasterizerState;
            }

            renderer.Render(true, true);

            if(Controls.DisplayWireframe)
                renderer.RenderContext.EnforcedRenderState = EnforcedRenderState.None;

            DrawDebugVisualization(renderer.RenderContext, time);

            PostRenderScene(renderer, time);
        }

        protected override void UpdateSample(IGameTime time)
        {
            PreUpdateScene(time);

            m_root.Update(time);

            PostUpdateScene(time);
        }

        protected override void DisplayBoundings(IRenderContext context, IGameTime time)
        {
            m_geomDebug.DrawOnlyLeafObjects = true;
            m_geomDebug.ViewFlags = DebugView.BoundingVolumes;
            m_geomDebug.Draw(context, SceneRoot, true);
        }

        protected override void DisplayNormals(IRenderContext context, IGameTime time)
        {
            m_geomDebug.GeometryDebugger.LineScaleAmount = .1f;
            m_geomDebug.ViewFlags = DebugView.Normals | DebugView.TangentBasis;
            m_geomDebug.Draw(context, SceneRoot, true);
        }

        protected virtual void PreRenderScene(IRenderer renderer, IGameTime time) { }
        protected virtual void PostRenderScene(IRenderer renderer, IGameTime time) { }

        protected virtual void PreUpdateScene(IGameTime time) { }
        protected virtual void PostUpdateScene(IGameTime time) { }
    }
}
