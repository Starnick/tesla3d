﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesla.Graphics;
using Tesla;
using Tesla.Application;
using Tesla.Input;
using Tesla.Content;
using Tesla.Utilities;
using Tesla.Framework;

namespace SampleBrowser
{
  public abstract class BasicCommonApp : GameApplication
  {
    private FrameRateCounter m_fpsCounter;
    private AppControls m_controls;
    private Vector3 m_camProjProps;
    private Camera m_cam;
    private IRenderer m_renderer;
    private ICameraController m_cameraController;
    private SpriteBatch m_spriteBatch;

    public AppControls Controls
    {
      get
      {
        return m_controls;
      }
    }

    public ICameraController CameraController
    {
      get
      {
        return m_cameraController;
      }
      set
      {
        m_cameraController = value;
        if (m_cameraController is not null && m_cameraController.InputGroup.Count == 0)
          m_cameraController.MapControls(null);
      }
    }

    public FrameRateCounter FPSCounter
    {
      get
      {
        return m_fpsCounter;
      }
    }

    public Camera Camera
    {
      get
      {
        return m_cam;
      }
    }

    public SpriteBatch SpriteBatch
    {
      get
      {
        return m_spriteBatch;
      }
    }

    public IRenderer Renderer
    {
      get
      {
        return m_renderer;
      }
    }

    public BasicCommonApp(PresentationParameters presentParams, IPlatformInitializer platformInitializer)
        : base(presentParams, new FileResourceRepository("Content"), platformInitializer)
    {
    }

    protected override void OnInitialize(Engine engine)
    {
      Content.ThrowForMissingContent = true;

      m_spriteBatch = new SpriteBatch(RenderSystem);
      m_cam = RenderSystem.ImmediateContext.Camera;
      m_cam.Viewport = new Viewport(0, 0, GameWindow.ClientBounds.Width, GameWindow.ClientBounds.Height);
      SetCameraProjection(Angle.FromDegrees(45.0f), 1, 10000);
      SetDefaultTitle();

      m_renderer = SetupRenderer();
      m_fpsCounter = new FrameRateCounter();
      m_controls = new AppControls();

      m_controls.SetFullScreenAction(new InputAction(delegate (IGameTime time)
      {
        if (GameWindow.IsFullScreen)
          GameWindow.WindowMode = WindowMode.Normal;
        else
          GameWindow.WindowMode = WindowMode.FullScreen;
      }));

      m_controls.SetExitAction(new InputAction(delegate (IGameTime time)
      {
        Exit();
      }));

      m_cameraController = new OrbitCameraController(m_cam);
      m_cameraController.MapControls(null);

      Content.ResourceImporters.Add(new TeximpTextureImporter());
      Content.ResourceImporters.Add(new AssimpModelImporter());
      Content.ResourceImporters.Add(new BMFontImporter());
      Content.ResourceImporters.Add(new Tesla.Direct3D11.Graphics.Effects11ResourceImporter());

      base.OnInitialize(engine);
    }

    protected void SetCameraProjection(Angle fieldOfview, float near, float far)
    {
      m_camProjProps = new Vector3(fieldOfview.Degrees, near, far);
      m_cam.SetProjection(fieldOfview, near, far);
      m_cam.Update();
    }

    protected override void OnViewportResized(IWindow gameWindow)
    {
      base.OnViewportResized(gameWindow);

      m_cam.Viewport = new Viewport(0, 0, gameWindow.ClientBounds.Width, gameWindow.ClientBounds.Height);
      m_cam.SetProjection(Angle.FromDegrees(m_camProjProps.X), m_camProjProps.Y, m_camProjProps.Z);
      m_cam.Update();
    }

    protected override void Update(IGameTime time)
    {
      bool checkInput = GameWindow.Focused;
      if (Keyboard.IsAvailable && Keyboard.FocusState != InputFocusState.App)
        checkInput = false;

      if (checkInput)
        m_controls.InputGroup.CheckAndPerformTriggers(time);

      if (m_cameraController is not null && checkInput)
        m_cameraController.Update(time, true);

      m_fpsCounter.Update(time);

      UpdateSample(time);
    }

    protected override void Render(IRenderContext context, IGameTime time)
    {
      m_fpsCounter.IncrementOneFrame();

      RenderSample(m_renderer, time);
    }

    protected abstract void UpdateSample(IGameTime time);
    protected abstract void RenderSample(IRenderer renderer, IGameTime time);

    protected abstract IRenderer SetupRenderer();

    protected virtual void DisplayNormals(IRenderContext context, IGameTime time) { }
    protected virtual void DisplayBoundings(IRenderContext context, IGameTime time) { }
    protected virtual void DisplayWireframe(IRenderContext context, IGameTime time) { }

    protected void DrawDebugVisualization(IRenderContext context, IGameTime time)
    {
      if (m_controls.DisplayNormals)
        DisplayNormals(context, time);

      if (m_controls.DisplayBoundings)
        DisplayBoundings(context, time);

      if (m_controls.DisplayWireframe)
        DisplayWireframe(context, time);
    }

    private void SetDefaultTitle()
    {
      AppDescriptionAttribute appDescr = AppReference.GetAppDescription(GetType());
      if (appDescr is not null)
        GameWindow.Title = appDescr.Name;
    }
  }
}
