﻿using System;
using System.Collections.Generic;
using Tesla;
using Tesla.Input;

namespace SampleBrowser
{
    public class AppControls
    {
        private const String DISPLAY_NORMALS = "DisplayNormals";
        private const String DISPLAY_BOUNDINGS = "DisplayBoundings";
        private const String DISPLAY_WIREFRAME = "DisplayWireframe";
        private const String DISPLAY_WIREFRAMEANDSOLID = "DisplayWireframeAndSolid";
        private const String TOGGLE_FULLSCREEN = "FullScreen";
        private const String EXIT = "Exit";

        private InputGroup m_inputGroup;
        private Dictionary<String, InputCondition> m_keymap;
        private Dictionary<String, bool> m_conditions;

        public Keys FullScreenKey
        {
            get
            {
                return GetKeyBinding(TOGGLE_FULLSCREEN);
            }
            set
            {
                SetKeyBinding(TOGGLE_FULLSCREEN, value);
            }
        }

        public Keys ExitKey
        {
            get
            {
                return GetKeyBinding(EXIT);
            }
            set
            {
                SetKeyBinding(EXIT, value);
            }
        }

        public Keys DisplayNormalsKey
        {
            get
            {
                return GetKeyBinding(DISPLAY_NORMALS);
            }
            set
            {
                SetKeyBinding(DISPLAY_NORMALS, value);
            }
        }

        public Keys DisplayBoundingsKey
        {
            get
            {
                return GetKeyBinding(DISPLAY_BOUNDINGS);
            }
            set
            {
                SetKeyBinding(DISPLAY_BOUNDINGS, value);
            }
        }

        public Keys DisplayWireframeKey
        {
            get
            {
                return GetKeyBinding(DISPLAY_WIREFRAME);
            }
            set
            {
                SetKeyBinding(DISPLAY_WIREFRAME, value);
            }
        }

        public bool DisplayNormals
        {
            get
            {
                return GetCondition(DISPLAY_NORMALS);
            }
        }

        public bool DisplayBoundings
        {
            get
            {
                return GetCondition(DISPLAY_BOUNDINGS);
            }
        }

        public bool DisplayWireframe
        {
            get
            {
                return GetCondition(DISPLAY_WIREFRAME);
            }
        }

        public InputGroup InputGroup
        {
            get
            {
                return m_inputGroup;
            }
        }
        
        public AppControls()
        {
            m_conditions = new Dictionary<String, bool>();
            m_inputGroup = new InputGroup("Controls");
            m_keymap = new Dictionary<String, InputCondition>();

            //Toggle full screen
            InputTrigger trigger = new InputTrigger(TOGGLE_FULLSCREEN, new KeyPressedCondition(Keys.Space, false), new InputAction(DoNothing));
            m_keymap.Add(TOGGLE_FULLSCREEN, trigger.Condition);
            m_inputGroup.Add(trigger);

            //Exit
            trigger = new InputTrigger(EXIT, new KeyPressedCondition(Keys.Escape, false), new InputAction(DoNothing));
            m_keymap.Add(EXIT, trigger.Condition);
            m_inputGroup.Add(trigger);

            //Debug keys
            trigger = new InputTrigger(DISPLAY_NORMALS, new KeyPressedCondition(Keys.N, false), new InputAction((IGameTime time) =>
            {
                ToggleCondition(DISPLAY_NORMALS);
            }));
            m_keymap.Add(DISPLAY_NORMALS, trigger.Condition);
            m_inputGroup.Add(trigger);

            trigger = new InputTrigger(DISPLAY_BOUNDINGS, new KeyPressedCondition(Keys.B, false), new InputAction((IGameTime time) =>
            {
                ToggleCondition(DISPLAY_BOUNDINGS);
            }));
            m_keymap.Add(DISPLAY_BOUNDINGS, trigger.Condition);
            m_inputGroup.Add(trigger);

            trigger = new InputTrigger(DISPLAY_WIREFRAME, new KeyPressedCondition(Keys.M, false), new InputAction((IGameTime time) =>
            {
                ToggleCondition(DISPLAY_WIREFRAME);
            }));
            m_keymap.Add(DISPLAY_WIREFRAME, trigger.Condition);
            m_inputGroup.Add(trigger);
        }

        private void DoNothing(IGameTime time) { }

        public void SetFullScreenAction(InputAction action)
        {
            SetAction(TOGGLE_FULLSCREEN, action);
        }

        public void SetExitAction(InputAction action)
        {
            SetAction(EXIT, action);
        }

        private void SetAction(String name, InputAction action)
        {
            if(action == null)
            {
                action = new InputAction(DoNothing);
            }

            InputTrigger trigger = m_inputGroup[name];
            if(trigger != null)
                trigger.Action = action;
        }

        private void SetKeyBinding(String binding, Keys key)
        {
           IKeyInputBinding condition = m_keymap[binding] as IKeyInputBinding;
            if(condition != null)
                condition.InputBinding = key;
        }

        private Keys GetKeyBinding(String binding)
        {
            IKeyInputBinding condition = m_keymap[binding] as IKeyInputBinding;
            if(condition != null)
                return condition.InputBinding.Key;

            return Keys.A;
        }

        private bool GetCondition(String name)
        {
            bool condition;
            if(m_conditions.TryGetValue(name, out condition))
                return condition;

            return false;
        }

        private void SetCondition(String name, bool value)
        {
            m_conditions[name] = value;
        }

        private void ToggleCondition(String name)
        {
            bool prevCond;
            if(m_conditions.TryGetValue(name, out prevCond))
            {
                prevCond = !prevCond;
            }
            else
            {
                //By default the condition will have been originall false
                prevCond = true;
            }

            m_conditions[name] = prevCond;
        }
    }
}
