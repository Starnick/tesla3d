﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesla;
using Tesla.Application;
using Tesla.Content;
using Tesla.Graphics;

namespace SampleBrowser
{
    public abstract class ForwardRendererSceneGraphApp : ScenegraphApp
    {
        public ForwardRendererSceneGraphApp(PresentationParameters presentParams, IPlatformInitializer platformInitializer) 
            : base(presentParams, platformInitializer)
        {
        }

        protected override IRenderer SetupRenderer()
        {
            return new ForwardRenderer(RenderSystem.ImmediateContext);
        }
    }
}
