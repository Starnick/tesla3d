﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesla;
using Tesla.Graphics;
using Tesla.Application;
using Tesla.Content;
using Tesla.Input;
using Tesla.Scene;
using Tesla.SquidUI;

namespace SampleBrowser.Input
{
    [AppDescription("Keymapping Sample", RenderSystemPlatform.Direct3D11, "KeymappingSample_Thumb", "KeymappingSample_Descr")]
    public class KeymappingSample : ForwardRendererSceneGraphApp
    {
        private Spatial m_jeep;
        private float m_speed;
        private float m_acceleration = 250;
        private float m_friction = 100;
        private Angle m_rotateAngle = Angle.FromDegrees(90);
        private Squid.Desktop m_uiScreen;
        private InputGroup m_jeepControls;
        private bool m_showUI = false;

        public static void RunApp(PresentationParameters pp, IPlatformInitializer init)
        {
            new KeymappingSample(pp, init).Run();
        }

        public KeymappingSample(PresentationParameters presentParams, IPlatformInitializer platformInitializer) 
            : base(presentParams, platformInitializer)
        {
        }

        protected override void OnInitialize(Engine engine)
        {
            base.OnInitialize(engine);
            m_jeepControls = new InputGroup("ShipControls");

            m_jeepControls.Add(new InputTrigger("Rotate Left", new KeyPressedCondition(Keys.A, true), new InputAction(delegate (IGameTime time)
            {
                 m_jeep.Rotation *= Quaternion.FromAngleAxis(m_rotateAngle * time.ElapsedTimeInSeconds, Vector3.Up);  
            })));


            m_jeepControls.Add(new InputTrigger("Rotate Right", new KeyPressedCondition(Keys.D, true), new InputAction(delegate (IGameTime time)
            {
                m_jeep.Rotation *= Quaternion.FromAngleAxis(-m_rotateAngle * time.ElapsedTimeInSeconds, Vector3.Up);
            })));


            m_jeepControls.Add(new InputTrigger("Move Forward", new KeyPressedCondition(Keys.W, true), new InputAction(delegate (IGameTime time)
            {
                m_speed += m_acceleration * time.ElapsedTimeInSeconds;
            })));


            m_jeepControls.Add(new InputTrigger("Move Backward", new KeyPressedCondition(Keys.S, true), new InputAction(delegate (IGameTime time)
            {
                m_speed -= m_acceleration * time.ElapsedTimeInSeconds;
            })));

            m_jeepControls.Add(new InputTrigger("Fire", new KeyPressedCondition(Keys.Space, true), new InputAction(delegate (IGameTime time)
            {
                //No op for now
            })));

            m_jeepControls.Add(new InputTrigger("Show Keymapping", new KeyPressedCondition(Keys.Tab, false), new InputAction(delegate (IGameTime time)
            {
                m_showUI = !m_showUI;

                bool enableControls = !m_showUI;
                foreach(InputTrigger trigger in m_jeepControls)
                {
                    if (!trigger.Name.Equals("Show Keymapping"))
                        trigger.IsEnabled = enableControls;
                }
            })));

            SetupUI();
        }

        private void SetupUI()
        {
            SquidUISystem uiSystem = new SquidUISystem();
            uiSystem.InitializeRenderer(Content, RenderSystem.ImmediateContext);
            Engine.Services.AddService<SquidUISystem>(uiSystem);

            m_uiScreen = new Squid.Desktop();
            m_uiScreen.ShowCursor = false;
            m_uiScreen.Skin = uiSystem.Renderer.DefaultSkin;

            Squid.Window window = new Squid.Window();
            window.Size = new Squid.Point((int) (Camera.Viewport.Width - Camera.Viewport.Width * .1f),
                (int) (Camera.Viewport.Height - Camera.Viewport.Height * .1f));
            window.Position = new Squid.Point((int) (Camera.Viewport.Width * .05f), (int) (Camera.Viewport.Height * .05f));
            window.Show(m_uiScreen);
            m_uiScreen.PerformLayout();
        }

        protected override void LoadContent(ContentManager content)
        {
            Spatial jeep = content.Load<Spatial>("Models/Jeep.tebo");
            jeep.Translation = new Vector3(0, 0, 25);

            //Offset to the front axial 
            Node jeepRoot = new Node("JeepRoot");
            jeepRoot.Children.Add(m_jeep);
            m_jeep = jeepRoot;
            SceneRoot.Children.Add(m_jeep);

            Mesh plane = new Mesh("Plane");
            PlaneGenerator gen = new PlaneGenerator();
            gen.Width = 1000;
            gen.Height = 1000;
            gen.Tessellation = 1;
            gen.BuildMeshData(plane.MeshData, GenerateOptions.All);
            plane.MeshData.Compile();
            plane.SetModelBounding(new BoundingBox());
            plane.MaterialDefinition = new MaterialDefinition();
            plane.MaterialDefinition.Add(RenderBucketID.Opaque, StandardMaterialScriptLibrary.CreateStandardMaterial("LitStandard_Color", content));
            plane.MaterialDefinition.GetOpaqueOrFirstMaterial().SetParameterValue<Vector3>("MatDiffuse", Color.Gray.ToVector3());

            SceneRoot.Children.Add(plane);

            PointLight pl = new PointLight();
            pl.Position = new Vector3(0, 100, 0);
            pl.Range = 300;
            SceneRoot.Lights.Add(pl);

            CameraController.Camera.Position = new Vector3(0, 500, 45);
            CameraController.Camera.LookAt(Vector3.Zero, Vector3.Up);
            CameraController.UpdateFromCamera();
        }

        protected override void PostRenderScene(IRenderer renderer, IGameTime time)
        {
            if(m_showUI)
            {
                m_uiScreen.Draw();
            }
        }

        protected override void PreUpdateScene(IGameTime time)
        {
            if (!m_showUI)
            {
                m_jeepControls.CheckAndPerformTriggers(time);

                if(m_speed < 0)
                {
                    m_speed += m_friction * time.ElapsedTimeInSeconds;
                    if(m_speed > 0)
                        m_speed = 0;
                }
                else if (m_speed > 0)
                {
                    m_speed -= m_friction * time.ElapsedTimeInSeconds;
                    if(m_speed < 0)
                        m_speed = 0;
                }

                m_jeep.Translation += -m_jeep.Transform.GetRotationVector(2) * m_speed * time.ElapsedTimeInSeconds;
            }
            else
            {
                m_uiScreen.Size = new Squid.Point(Camera.Viewport.Width, Camera.Viewport.Height);
                m_uiScreen.Update();
            }
        }

        //TODO - Convert to squid UI
        private class KeymapScreen
        {
            private List<KeymapAction> m_bindings;
            private Texture2D m_blankTex;
            private Vector2 m_topLeftPt;
            private Rectangle m_screenBounds;
            private float m_fadeAlpha = 1;
            private float m_fadeIncrement = .01f;
            private KeyPressedCondition m_pressCondition;
            private List<Keys> m_pressedKeys;
            private List<MouseButton> m_pressedButtons;
            private TimeSpan m_cooldownAssignment;
            private TimedAction m_assignmentAction;

            public Vector2  TopleftPosition
            {
                get
                {
                    return m_topLeftPt;
                }
                set
                {
                    m_topLeftPt = value;
                }
            }

            public Rectangle ScreenBounds
            {
                get
                {
                    return m_screenBounds;
                }
                set
                {
                    m_screenBounds = value;
                }
            }

            public KeymapScreen(InputGroup controls, Texture2D blankTex)
            {
                m_bindings = new List<KeymapAction>();
                m_blankTex = blankTex;

                foreach(InputTrigger trigger in controls)
                {
                    if(trigger.Condition is IKeyInputBinding)
                    {
                        KeymapAction action = new KeymapAction();
                        action.Name = trigger.Name;
                        action.Binding = trigger.Condition as IKeyInputBinding;
                        m_bindings.Add(action);
                    }
                }

                m_pressCondition = new KeyPressedCondition(MouseButton.Left, false);
                m_pressedKeys = new List<Keys>();
                m_pressedButtons = new List<MouseButton>();
                m_assignmentAction = new TimedAction(TimeSpan.FromMilliseconds(100), CheckAssignment);
            }

            public void Update(IGameTime time, SpriteFont font, Viewport vp)
            {
                int tenPercWidth = (int)(vp.Width * .1f);
                int ninetyPercWidth = (int)(vp.Width * .9f);
                int tenPercHeight = (int)(vp.Height * .1f);
                int ninetyPercHeight = (int)(vp.Height * .9f);
                int margin = 30;
                int buttonWidth = 100;
                int buttonHeight = 35;

                Vector2 topLeftCorner = m_topLeftPt;
                topLeftCorner.X += tenPercWidth + margin;
                topLeftCorner.Y += tenPercHeight + margin;

                MouseState msState = Mouse.GetMouseState();
                bool mouseClicked = m_pressCondition.Check(time);

                m_assignmentAction.CheckAndPerform(time);

                foreach (KeymapAction keymap in m_bindings)
                {
                    String keyString = (keymap.Binding.InputBinding.IsMouseButton) ? keymap.Binding.InputBinding.MouseButton.ToString() : keymap.Binding.InputBinding.Key.ToString();
                    Vector2 keySize = font.MeasureString(keyString);

                    Rectangle bounds = new Rectangle((int) topLeftCorner.X, (int) topLeftCorner.Y, buttonWidth, buttonHeight);
                    if(bounds.Contains(msState.PositionInt) && mouseClicked)
                    {
                        keymap.IsSelected = true;
                        keymap.AlphaFade = 1.0f;
                        m_assignmentAction.Reset(time);
                    }
                    else if(mouseClicked)
                    {
                        keymap.IsSelected = false;
                    }

                    topLeftCorner.Y += keySize.Y + margin;
                }
            }

            private void CheckAssignment(IGameTime time)
            {
                MouseState msState = Mouse.GetMouseState();
                KeyboardState ksState = Keyboard.GetKeyboardState();

                ksState.GetPressedKeys(m_pressedKeys);
                msState.GetPressedButtons(m_pressedButtons);

                if (m_pressedKeys.Count > 0)
                {
                    foreach (KeymapAction keymap in m_bindings)
                    {
                        if (keymap.IsSelected)
                        {
                            keymap.Binding.InputBinding = m_pressedKeys[0];
                            keymap.IsSelected = false;
                        }
                    }
                }

                if(m_pressedButtons.Count > 0)
                {
                    foreach (KeymapAction keymap in m_bindings)
                    {
                        if (keymap.IsSelected)
                        {
                            keymap.Binding.InputBinding = m_pressedButtons[0];
                            keymap.IsSelected = false;
                        }
                    }
                }

                m_pressedKeys.Clear();
                m_pressedButtons.Clear();
            }

            public void Draw(SpriteBatch batch, SpriteFont font, Viewport vp)
            {
                int tenPercWidth = (int) (vp.Width * .1f);
                int ninetyPercWidth = (int)(vp.Width * .9f);
                int tenPercHeight = (int)(vp.Height * .1f);
                int ninetyPercHeight = (int)(vp.Height * .9f);
                int margin = 30;
                int buttonWidth = 100;
                int buttonHeight = 35;

                batch.Draw(m_blankTex, new Rectangle(0, 0, vp.Width, vp.Height), null, new Color(.1f, .1f, .1f, .5f), Angle.Zero, Vector2.Zero, SpriteFlipEffect.None, 0);
                batch.Draw(m_blankTex, new Rectangle(tenPercWidth, tenPercHeight, ninetyPercWidth - tenPercWidth, ninetyPercHeight - tenPercHeight), null, new Color(.1f, .1f, .1f, 1f), Angle.Zero, Vector2.Zero, SpriteFlipEffect.None, 1);

                Vector2 topLeftCorner = m_topLeftPt;
                topLeftCorner.X += tenPercWidth + margin;
                topLeftCorner.Y += tenPercHeight + margin;

                foreach(KeymapAction keymap in m_bindings)
                {
                    String keyString = (keymap.Binding.InputBinding.IsMouseButton) ? keymap.Binding.InputBinding.MouseButton.ToString() : keymap.Binding.InputBinding.Key.ToString();

                    Vector2 pt = topLeftCorner;
                    Vector2 keySize = font.MeasureString(keyString);

                    if (keymap.IsSelected)
                    {
                        batch.Draw(m_blankTex, new Rectangle((int)pt.X, (int)pt.Y, buttonWidth, buttonHeight), Color.RosyBrown);


                        if (m_fadeAlpha <= 0)
                        {
                            m_fadeAlpha = 1;
                        }

                        m_fadeAlpha -= m_fadeIncrement;

                        Color c = Color.AntiqueWhite;
                        c.A = (byte) (m_fadeAlpha / 255.0f);

                        batch.DrawString(font, "|assasf", new Vector2(pt.X + (buttonWidth / 2), pt.Y), c);
                    }
                    else
                    {
                        batch.Draw(m_blankTex, new Rectangle((int)pt.X, (int)pt.Y, buttonWidth, buttonHeight), Color.RosyBrown);

                        batch.DrawString(font, keyString, new Vector2(pt.X + (buttonWidth / 2) - (keySize.X / 2), pt.Y), Color.AntiqueWhite);
                    }

                    pt.X += 150;

                    batch.DrawString(font, keymap.Name, pt, Color.AntiqueWhite);

                    topLeftCorner.Y += keySize.Y + margin;
                }
            }
        }

        private class KeymapAction
        {
            public String Name;
            public IKeyInputBinding Binding;
            public bool IsSelected;
            public float AlphaFade = 1;
        }
    }
}
