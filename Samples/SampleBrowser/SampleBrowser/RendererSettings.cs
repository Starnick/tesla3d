﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleBrowser
{
    public class RendererSettings
    {
        private static RendererSettings s_settings;

        public RenderSystemPlatform RenderSystem { get; set; }
        public AntialiasingMode Antialiasing { get; set; }
        public Resolution Resolution { get; set; }
        public bool IsVsyncEnabled { get; set; }
        public bool IsFullScreen { get; set; }

        public static RendererSettings Settings
        {
            get
            {
                if(s_settings == null)
                {
                    s_settings = new RendererSettings();
                    s_settings.Load();
                }

                return s_settings;
            }
        }

        private RendererSettings() { }

        public void Save()
        {
            SampleBrowserSettings settings = SampleBrowserSettings.Default;
            settings.RenderSystem = (int) s_settings.RenderSystem;
            settings.Antialiasing = (int) s_settings.Antialiasing;
            settings.Resolution = (int) s_settings.Resolution;
            settings.IsVsyncEnabled = s_settings.IsVsyncEnabled;
            settings.IsFullScreen = s_settings.IsFullScreen;

            settings.Save();
        }

        private void Load()
        {
            SampleBrowserSettings settings = SampleBrowserSettings.Default;
            s_settings.RenderSystem = (RenderSystemPlatform) settings.RenderSystem;
            s_settings.Antialiasing = (AntialiasingMode) settings.Antialiasing;
            s_settings.Resolution = (Resolution) settings.Resolution;
            s_settings.IsVsyncEnabled = settings.IsVsyncEnabled;
            s_settings.IsFullScreen = settings.IsFullScreen;
        }
    }
}
