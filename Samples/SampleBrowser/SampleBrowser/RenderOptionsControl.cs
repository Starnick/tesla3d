﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SampleBrowser
{
    public partial class RenderOptionsControl : UserControl
    {
        public RenderOptionsControl()
        {
            InitializeComponent();

            SetAllValidPlatforms();

           // EnumHelper.BindToComboBox(m_renderSystemCombobox, typeof(RenderSystemPlatform));
            EnumHelper.BindToComboBox(m_antialiasingList, typeof(AntialiasingMode));
            EnumHelper.BindToComboBox(m_resolutionList, typeof(Resolution));

            LoadFromSettings();

            m_renderSystemCombobox.SelectedValueChanged += RenderSystemChanged;
            m_antialiasingList.SelectedValueChanged += AntialiasingChanged;
            m_resolutionList.SelectedValueChanged += ResolutionChanged;
            m_enableVsyncCheckbox.CheckedChanged += EnableVsyncChanged;
            m_fullScreenCheckbox.CheckedChanged += FullScreenChanged;
        }

        public void SetAllValidPlatforms()
        {
            m_renderSystemCombobox.DataSource = EnumHelper.GetEnumDescriptions(typeof(RenderSystemPlatform));
            m_renderSystemCombobox.DisplayMember = "Value";
            m_renderSystemCombobox.ValueMember = "Key";
        }

        public void SetValidPlatforms(RenderSystemPlatform platforms)
        {
            Array enumValues = Enum.GetValues(typeof(RenderSystemPlatform));
            List<KeyValuePair<Enum, String>> list = new List<KeyValuePair<Enum, String>>();

            foreach(Enum val in enumValues)
            {
                if((RenderSystemPlatform)val == RenderSystemPlatform.Any)
                    continue;

                if(platforms.HasFlag(val))
                {
                    list.Add(new KeyValuePair<Enum, String>(val, EnumHelper.GetDescription(val)));
                }
            }

            if(list.Count > 0)
            {
                m_renderSystemCombobox.DataSource = list;
                m_renderSystemCombobox.DisplayMember = "Value";
                m_renderSystemCombobox.ValueMember = "Key";
            }
        }

        private void FullScreenChanged(object sender, EventArgs e)
        {
            RendererSettings.Settings.IsFullScreen = (sender as CheckBox).Checked;
            RendererSettings.Settings.Save();
        }

        private void EnableVsyncChanged(object sender, EventArgs e)
        {
            RendererSettings.Settings.IsVsyncEnabled = (sender as CheckBox).Checked;
            RendererSettings.Settings.Save();
        }

        private void ResolutionChanged(object sender, EventArgs e)
        {
            RendererSettings.Settings.Resolution = EnumHelper.GetCurrentValue<Resolution>(sender as ComboBox);
            RendererSettings.Settings.Save();
        }

        private void AntialiasingChanged(object sender, EventArgs e)
        {
            RendererSettings.Settings.Antialiasing = EnumHelper.GetCurrentValue<AntialiasingMode>(sender as ComboBox);
            RendererSettings.Settings.Save();
        }

        private void RenderSystemChanged(object sender, EventArgs e)
        {
            RendererSettings.Settings.RenderSystem = EnumHelper.GetCurrentValue<RenderSystemPlatform>(sender as ComboBox);
            RendererSettings.Settings.Save();
        }

        private void LoadFromSettings()
        {
            RendererSettings settings = RendererSettings.Settings;
            EnumHelper.SetCurrentValue(m_renderSystemCombobox, settings.RenderSystem);
            EnumHelper.SetCurrentValue(m_antialiasingList, settings.Antialiasing);
            EnumHelper.SetCurrentValue(m_resolutionList, settings.Resolution);

            m_fullScreenCheckbox.Checked = settings.IsFullScreen;
            m_enableVsyncCheckbox.Checked = settings.IsVsyncEnabled;
        }
    }
}
