﻿namespace SampleBrowser
{
    partial class BrowserForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BrowserForm));
            this.m_sampleThumbnail = new System.Windows.Forms.PictureBox();
            this.m_sampleAppsTree = new System.Windows.Forms.TreeView();
            this.m_contextMenuSamples = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.runToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_descriptionTextBox = new System.Windows.Forms.RichTextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.m_descriptionTab = new System.Windows.Forms.TabPage();
            this.m_sampleTreeLabel = new System.Windows.Forms.Label();
            this.m_renderOptionsControl = new SampleBrowser.RenderOptionsControl();
            ((System.ComponentModel.ISupportInitialize)(this.m_sampleThumbnail)).BeginInit();
            this.m_contextMenuSamples.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.m_descriptionTab.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_sampleThumbnail
            // 
            this.m_sampleThumbnail.Image = global::SampleBrowser.Resources.DefaultThumbnail;
            this.m_sampleThumbnail.InitialImage = global::SampleBrowser.Resources.DefaultThumbnail;
            this.m_sampleThumbnail.Location = new System.Drawing.Point(361, 16);
            this.m_sampleThumbnail.Name = "m_sampleThumbnail";
            this.m_sampleThumbnail.Size = new System.Drawing.Size(410, 180);
            this.m_sampleThumbnail.TabIndex = 1;
            this.m_sampleThumbnail.TabStop = false;
            // 
            // m_sampleAppsTree
            // 
            this.m_sampleAppsTree.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_sampleAppsTree.ContextMenuStrip = this.m_contextMenuSamples;
            this.m_sampleAppsTree.Location = new System.Drawing.Point(12, 156);
            this.m_sampleAppsTree.Name = "m_sampleAppsTree";
            this.m_sampleAppsTree.Size = new System.Drawing.Size(341, 393);
            this.m_sampleAppsTree.TabIndex = 2;
            // 
            // m_contextMenuSamples
            // 
            this.m_contextMenuSamples.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.runToolStripMenuItem});
            this.m_contextMenuSamples.Name = "m_contextMenuSamples";
            this.m_contextMenuSamples.Size = new System.Drawing.Size(96, 26);
            // 
            // runToolStripMenuItem
            // 
            this.runToolStripMenuItem.Name = "runToolStripMenuItem";
            this.runToolStripMenuItem.Size = new System.Drawing.Size(95, 22);
            this.runToolStripMenuItem.Text = "Run";
            this.runToolStripMenuItem.Click += new System.EventHandler(this.OnAppRun);
            // 
            // m_descriptionTextBox
            // 
            this.m_descriptionTextBox.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.m_descriptionTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.m_descriptionTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_descriptionTextBox.Location = new System.Drawing.Point(3, 3);
            this.m_descriptionTextBox.Name = "m_descriptionTextBox";
            this.m_descriptionTextBox.Size = new System.Drawing.Size(396, 313);
            this.m_descriptionTextBox.TabIndex = 3;
            this.m_descriptionTextBox.Text = "";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.m_descriptionTab);
            this.tabControl1.Location = new System.Drawing.Point(360, 202);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(412, 347);
            this.tabControl1.TabIndex = 6;
            // 
            // m_descriptionTab
            // 
            this.m_descriptionTab.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.m_descriptionTab.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_descriptionTab.Controls.Add(this.m_descriptionTextBox);
            this.m_descriptionTab.Location = new System.Drawing.Point(4, 22);
            this.m_descriptionTab.Name = "m_descriptionTab";
            this.m_descriptionTab.Padding = new System.Windows.Forms.Padding(3);
            this.m_descriptionTab.Size = new System.Drawing.Size(404, 321);
            this.m_descriptionTab.TabIndex = 0;
            this.m_descriptionTab.Text = "Description";
            // 
            // m_sampleTreeLabel
            // 
            this.m_sampleTreeLabel.AutoSize = true;
            this.m_sampleTreeLabel.Location = new System.Drawing.Point(9, 136);
            this.m_sampleTreeLabel.Name = "m_sampleTreeLabel";
            this.m_sampleTreeLabel.Size = new System.Drawing.Size(123, 13);
            this.m_sampleTreeLabel.TabIndex = 7;
            this.m_sampleTreeLabel.Text = "Choose Sample To Run:";
            // 
            // m_renderOptionsControl
            // 
            this.m_renderOptionsControl.Location = new System.Drawing.Point(12, 12);
            this.m_renderOptionsControl.Name = "m_renderOptionsControl";
            this.m_renderOptionsControl.Size = new System.Drawing.Size(342, 121);
            this.m_renderOptionsControl.TabIndex = 0;
            // 
            // BrowserForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.m_sampleTreeLabel);
            this.Controls.Add(this.m_sampleAppsTree);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.m_sampleThumbnail);
            this.Controls.Add(this.m_renderOptionsControl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(800, 600);
            this.MinimumSize = new System.Drawing.Size(800, 600);
            this.Name = "BrowserForm";
            this.Text = "Tesla 3D Engine Sampler Browser";
            ((System.ComponentModel.ISupportInitialize)(this.m_sampleThumbnail)).EndInit();
            this.m_contextMenuSamples.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.m_descriptionTab.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private RenderOptionsControl m_renderOptionsControl;
        private System.Windows.Forms.PictureBox m_sampleThumbnail;
        private System.Windows.Forms.TreeView m_sampleAppsTree;
        private System.Windows.Forms.RichTextBox m_descriptionTextBox;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage m_descriptionTab;
        private System.Windows.Forms.Label m_sampleTreeLabel;
        private System.Windows.Forms.ContextMenuStrip m_contextMenuSamples;
        private System.Windows.Forms.ToolStripMenuItem runToolStripMenuItem;
    }
}

