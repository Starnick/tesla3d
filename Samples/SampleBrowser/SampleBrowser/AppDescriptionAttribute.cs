﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;

namespace SampleBrowser
{
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class AppDescriptionAttribute : Attribute
    {
        public readonly String Name;
        public readonly RenderSystemPlatform Platforms;
        public readonly Image Thumbnail;
        public readonly String DescriptionText;
        public readonly String SourceDirectoryPath;

        public AppDescriptionAttribute(String name, RenderSystemPlatform platforms)
            : this(name, platforms, null, null, null)
        {
        }

        public AppDescriptionAttribute(String name, RenderSystemPlatform platforms, String thumbnailFileName)
            : this(name, platforms, thumbnailFileName, null, null)
        {

        }

        public AppDescriptionAttribute(String name, RenderSystemPlatform platforms, String thumbnailFileName, String descriptionFileName)
            : this(name, platforms, thumbnailFileName, descriptionFileName, null)
        {

        }

        public AppDescriptionAttribute(String name, RenderSystemPlatform platforms, String thumbnailFileName, String descriptionFileName, String sourceDirectory)
        {
            Name = name;
            Platforms = platforms;

            if(String.IsNullOrEmpty(thumbnailFileName))
            {
                Thumbnail = Resources.DefaultThumbnail;
            }
            else
            {
                Thumbnail = Resources.ResourceManager.GetObject(thumbnailFileName) as Image;
                if(Thumbnail == null)
                    Thumbnail = Resources.DefaultThumbnail;
            }

            if(String.IsNullOrEmpty(thumbnailFileName))
            {
                DescriptionText = Resources.NoDescription;
            }
            else
            {
                DescriptionText = Resources.ResourceManager.GetString(descriptionFileName);
                if(String.IsNullOrEmpty(DescriptionText))
                    DescriptionText = Resources.NoDescription;
            }

            if(String.IsNullOrEmpty(sourceDirectory) || !Directory.Exists(sourceDirectory))
            {
                SourceDirectoryPath = String.Empty;
            }
            else
            {
                SourceDirectoryPath = sourceDirectory;
            }
        }
    }
}
