﻿namespace SampleBrowser
{
    partial class RenderOptionsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.renderOptions = new System.Windows.Forms.GroupBox();
            this.m_renderSystemCombobox = new System.Windows.Forms.ComboBox();
            this.m_renderSystemLabel = new System.Windows.Forms.Label();
            this.m_enableVsyncCheckbox = new System.Windows.Forms.CheckBox();
            this.m_fullScreenCheckbox = new System.Windows.Forms.CheckBox();
            this.m_antialiasingLabel = new System.Windows.Forms.Label();
            this.m_antialiasingList = new System.Windows.Forms.ComboBox();
            this.m_resListLabel = new System.Windows.Forms.Label();
            this.m_resolutionList = new System.Windows.Forms.ComboBox();
            this.renderOptions.SuspendLayout();
            this.SuspendLayout();
            // 
            // renderOptions
            // 
            this.renderOptions.AutoSize = true;
            this.renderOptions.Controls.Add(this.m_renderSystemCombobox);
            this.renderOptions.Controls.Add(this.m_renderSystemLabel);
            this.renderOptions.Controls.Add(this.m_enableVsyncCheckbox);
            this.renderOptions.Controls.Add(this.m_fullScreenCheckbox);
            this.renderOptions.Controls.Add(this.m_antialiasingLabel);
            this.renderOptions.Controls.Add(this.m_antialiasingList);
            this.renderOptions.Controls.Add(this.m_resListLabel);
            this.renderOptions.Controls.Add(this.m_resolutionList);
            this.renderOptions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.renderOptions.Location = new System.Drawing.Point(0, 0);
            this.renderOptions.Name = "renderOptions";
            this.renderOptions.Size = new System.Drawing.Size(332, 121);
            this.renderOptions.TabIndex = 0;
            this.renderOptions.TabStop = false;
            this.renderOptions.Text = "Renderer Options";
            // 
            // m_renderSystemCombobox
            // 
            this.m_renderSystemCombobox.FormattingEnabled = true;
            this.m_renderSystemCombobox.Items.AddRange(new object[] {
            "Direct3D 11",
            "OpenGL",
            "XNA"});
            this.m_renderSystemCombobox.Location = new System.Drawing.Point(12, 42);
            this.m_renderSystemCombobox.Name = "m_renderSystemCombobox";
            this.m_renderSystemCombobox.Size = new System.Drawing.Size(121, 21);
            this.m_renderSystemCombobox.TabIndex = 7;
            this.m_renderSystemCombobox.Text = "Direct3D 11";
            // 
            // m_renderSystemLabel
            // 
            this.m_renderSystemLabel.AutoSize = true;
            this.m_renderSystemLabel.Location = new System.Drawing.Point(9, 25);
            this.m_renderSystemLabel.Name = "m_renderSystemLabel";
            this.m_renderSystemLabel.Size = new System.Drawing.Size(82, 13);
            this.m_renderSystemLabel.TabIndex = 6;
            this.m_renderSystemLabel.Text = "Render System:";
            // 
            // m_enableVsyncCheckbox
            // 
            this.m_enableVsyncCheckbox.AutoSize = true;
            this.m_enableVsyncCheckbox.Location = new System.Drawing.Point(234, 91);
            this.m_enableVsyncCheckbox.Name = "m_enableVsyncCheckbox";
            this.m_enableVsyncCheckbox.Size = new System.Drawing.Size(93, 17);
            this.m_enableVsyncCheckbox.TabIndex = 5;
            this.m_enableVsyncCheckbox.Text = "Enable VSync";
            this.m_enableVsyncCheckbox.UseVisualStyleBackColor = true;
            // 
            // m_fullScreenCheckbox
            // 
            this.m_fullScreenCheckbox.AutoSize = true;
            this.m_fullScreenCheckbox.Location = new System.Drawing.Point(149, 91);
            this.m_fullScreenCheckbox.Name = "m_fullScreenCheckbox";
            this.m_fullScreenCheckbox.Size = new System.Drawing.Size(79, 17);
            this.m_fullScreenCheckbox.TabIndex = 4;
            this.m_fullScreenCheckbox.Text = "Full Screen";
            this.m_fullScreenCheckbox.UseVisualStyleBackColor = true;
            // 
            // m_antialiasingLabel
            // 
            this.m_antialiasingLabel.AutoSize = true;
            this.m_antialiasingLabel.Location = new System.Drawing.Point(165, 26);
            this.m_antialiasingLabel.Name = "m_antialiasingLabel";
            this.m_antialiasingLabel.Size = new System.Drawing.Size(63, 13);
            this.m_antialiasingLabel.TabIndex = 3;
            this.m_antialiasingLabel.Text = "Anti-aliasing";
            // 
            // m_antialiasingList
            // 
            this.m_antialiasingList.FormattingEnabled = true;
            this.m_antialiasingList.Location = new System.Drawing.Point(168, 42);
            this.m_antialiasingList.Name = "m_antialiasingList";
            this.m_antialiasingList.Size = new System.Drawing.Size(121, 21);
            this.m_antialiasingList.TabIndex = 2;
            this.m_antialiasingList.Text = "None";
            // 
            // m_resListLabel
            // 
            this.m_resListLabel.AutoSize = true;
            this.m_resListLabel.Location = new System.Drawing.Point(9, 73);
            this.m_resListLabel.Name = "m_resListLabel";
            this.m_resListLabel.Size = new System.Drawing.Size(60, 13);
            this.m_resListLabel.TabIndex = 1;
            this.m_resListLabel.Text = "Resolution:";
            // 
            // m_resolutionList
            // 
            this.m_resolutionList.FormattingEnabled = true;
            this.m_resolutionList.Location = new System.Drawing.Point(12, 89);
            this.m_resolutionList.Name = "m_resolutionList";
            this.m_resolutionList.Size = new System.Drawing.Size(121, 21);
            this.m_resolutionList.TabIndex = 0;
            this.m_resolutionList.Text = "800 x 600";
            // 
            // RenderOptionsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.renderOptions);
            this.Name = "RenderOptionsControl";
            this.Size = new System.Drawing.Size(332, 121);
            this.renderOptions.ResumeLayout(false);
            this.renderOptions.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox renderOptions;
        private System.Windows.Forms.ComboBox m_resolutionList;
        private System.Windows.Forms.Label m_resListLabel;
        private System.Windows.Forms.Label m_antialiasingLabel;
        private System.Windows.Forms.ComboBox m_antialiasingList;
        private System.Windows.Forms.Label m_renderSystemLabel;
        private System.Windows.Forms.CheckBox m_enableVsyncCheckbox;
        private System.Windows.Forms.CheckBox m_fullScreenCheckbox;
        private System.Windows.Forms.ComboBox m_renderSystemCombobox;
    }
}
