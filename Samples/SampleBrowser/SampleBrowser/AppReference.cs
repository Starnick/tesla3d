﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesla.Graphics;
using Tesla;
using System.Reflection;

namespace SampleBrowser
{
    public class AppReference
    {
        private Type m_type;
        private AppDescriptionAttribute m_attribute;
        private Action<PresentationParameters, IPlatformInitializer> m_runMethod;

        public Type AppType
        {
            get
            {
                return m_type;
            }
        }

        public AppDescriptionAttribute Attribute
        {
            get
            {
                return m_attribute;
            }
        }

        public AppReference(Type type)
        {
            m_type = type;
            m_attribute = GetAppDescription(type);
        }

        public AppReference(Type type, AppDescriptionAttribute attribute)
        {
            m_type = type;
            m_attribute = attribute;
        }

        public static AppDescriptionAttribute GetAppDescription(Type type)
        {
            if(type == null)
                return null;

            object[] attributes = type.GetCustomAttributes(true);

            foreach(Attribute att in attributes)
            {
                AppDescriptionAttribute appAtt = att as AppDescriptionAttribute;
                if(appAtt != null)
                {
                    return appAtt;
                }
            }

            return null;
        }

        public void RunAppInstance(PresentationParameters pp, IPlatformInitializer init)
        {
            if(m_runMethod == null)
            {
                MethodInfo staticMethod = m_type.GetMethod("RunApp", BindingFlags.Public | BindingFlags.Static);
                if(staticMethod == null)
                    return;

                m_runMethod = (Action<PresentationParameters, IPlatformInitializer>) Delegate.CreateDelegate(typeof(Action<PresentationParameters, IPlatformInitializer>), null, staticMethod);
            }

            m_runMethod(pp, init);
        }

        public override string ToString()
        {
            if(m_attribute != null)
                return m_attribute.Name;

            return m_type.ToString();
        }
    }
}
