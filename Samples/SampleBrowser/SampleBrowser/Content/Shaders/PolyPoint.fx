﻿cbuffer PerFrame
{
  float4x4 WVP; // World view projection
  float2 ViewportSize; // Width, Height
  float2 UnitTexCoords[4] = { float2(0, 0), float2(0, 1), float2(1, 0), float2(1, 1) };
};

struct VSInputPolyPoint
{
  float3 Point : POSITION;
  float Size : PSIZE;
  float4 Color : COLOR;
};

struct VSOutputPolyPoint
{
  float4 PositionPS : SV_POSITION;
  float Size : PSIZE;
  float4 Color : COLOR;
};

struct GSOutputPolyPoint
{
  float4 PositionPS : SV_POSITION;
  float2 TexCoords : TEXCOORD0;
  float Size : PSIZE;
  float4 Color : COLOR;
};

Texture2D SpriteMap;

SamplerState MapSampler
{
  Filter = MIN_MAG_MIP_LINEAR;
  AddressU = Wrap;
  AddressV = Wrap;
  AddressW = Wrap;
};


// Test 1: Using a Geometry Shader to expand points.
// Code Input is a vertex buffer with: pos, size, color

VSOutputPolyPoint VS_PolyPoint(VSInputPolyPoint input)
{
  VSOutputPolyPoint output;
  output.PositionPS = mul(float4(input.Point, 1.0), WVP);
  output.Size = input.Size + (.5 * float(input.Size > 4.0));
  output.Color = input.Color;
  
  return output;
}

GSOutputPolyPoint CreateGSPoint(VSOutputPolyPoint pt, float4 offset, float2 texCoords)
{
  GSOutputPolyPoint gsPt;
  gsPt.PositionPS = pt.PositionPS + offset;
  gsPt.TexCoords = texCoords;
  gsPt.Size = pt.Size;
  gsPt.Color = pt.Color;
  
  return gsPt;
}

[maxvertexcount(4)]
void GS_PolyPoint(point VSOutputPolyPoint input[1], inout TriangleStream<GSOutputPolyPoint> output)
{
  VSOutputPolyPoint pt = input[0];
 
  // Expand point into a quad of two triangles, as a clockwise triangle strip
  float widthOffset = pt.Size * (pt.PositionPS.w / ViewportSize.x); // Divide by vp width
  float heightOffset = pt.Size * (pt.PositionPS.w / ViewportSize.y); // Divide by vp height
  
  // Top left
  output.Append(CreateGSPoint(pt, float4(-widthOffset, -heightOffset, 0, 0), float2(0, 0)));
  
  // Bottom left
  output.Append(CreateGSPoint(pt, float4(-widthOffset, heightOffset, 0, 0), float2(0, 1)));
  
  // Top right
  output.Append(CreateGSPoint(pt, float4(widthOffset, -heightOffset, 0, 0), float2(1, 0)));
  
  // Bottom right
  output.Append(CreateGSPoint(pt, float4(widthOffset, heightOffset, 0, 0), float2(1, 1)));
}

float4 PS_PolyPoint(GSOutputPolyPoint input) : SV_TARGET
{
  float roundCorners = float(input.Size > 4.0); // If true value of 1, if false value of 0

  // Use the tex coords as point coords, unit square (0,0) to (1,1)
  float2 ptCenter = float2(.5, .5);
  float2 vec = input.TexCoords - ptCenter;
  float vecDot = dot(vec, vec) * roundCorners;
  if (vecDot >= .25)
    discard;
  
  return input.Color;// * SpriteMap.Sample(MapSampler, input.TexCoords);
}

technique11 PolyPoint
{
  pass
  {
    SetVertexShader(CompileShader(vs_5_0, VS_PolyPoint()));
    SetGeometryShader(CompileShader(gs_5_0, GS_PolyPoint()));
    SetPixelShader(CompileShader(ps_5_0, PS_PolyPoint()));
  }
}

// Test 2: Using an instanced quad and each point resides in an instance vertexbuffer

struct VSInputPolyPoint_Instanced
{
  float2 TexCoords : TEXCOORD;
  float3 Point : POSITION;
  float Size : PSIZE;
  float4 Color : COLOR;
};

GSOutputPolyPoint VS_PolyPoint_Instanced(VSInputPolyPoint_Instanced instance)
{
  GSOutputPolyPoint output;
  output.PositionPS = mul(float4(instance.Point, 1.0), WVP);
  output.Size = instance.Size + (.5 * float(instance.Size > 4.0));
  output.Color = instance.Color;
  output.TexCoords = instance.TexCoords;
  
  return output;
}

technique11 PolyPoint_Instanced
{
  pass
  {
    SetVertexShader(CompileShader(vs_5_0, VS_PolyPoint_Instanced()));
    SetPixelShader(CompileShader(ps_5_0, PS_PolyPoint()));
  }
}

GSOutputPolyPoint VS_PolyPoint_Instanced_NullVB(VSInputPolyPoint_Instanced instance, uint vid: SV_VertexID)
{
  GSOutputPolyPoint output;
  output.PositionPS = mul(float4(instance.Point, 1.0), WVP);
  output.Size = instance.Size + (.5 * float(instance.Size > 4.0));
  output.Color = instance.Color;
  output.TexCoords = UnitTexCoords[vid % 4];
  
  return output;
}

technique11 PolyPoint_Instanced_NullVB
{
  pass
  {
    SetVertexShader(CompileShader(vs_5_0, VS_PolyPoint_Instanced_NullVB()));
    SetPixelShader(CompileShader(ps_5_0, PS_PolyPoint()));
  }
}