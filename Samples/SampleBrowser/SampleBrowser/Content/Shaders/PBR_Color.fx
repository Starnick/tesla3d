#include "Structures.fxh"
#include "Lighting.fxh"
#include "NormalUtils.fxh"

//=================================================================================================
// Constant Variables
//=================================================================================================
static const float Pi = 3.141592654f;
static const float Pi2 = 6.283185307f;
static const float Pi_2 = 1.570796327f;
static const float Pi_4 = 0.7853981635f;
static const float InvPi = 0.318309886f;
static const float InvPi2 = 0.159154943f;

// ===============================================================================================
// http://holger.dammertz.org/stuff/notes_HammersleyOnHemisphere.html
// ===============================================================================================
float2 Hammersley(uint i, uint N)
{
	float ri = reversebits(i) * 2.3283064365386963e-10f;
	return float2(float(i) / float(N), ri);
}

// ===============================================================================================
// http://graphicrants.blogspot.com.au/2013/08/specular-brdf-reference.html
// ===============================================================================================
float GGX(float NdotV, float a)
{
	float k = a / 2.0;
	return NdotV / (NdotV * (1.0f - k) + k);
}

// ===============================================================================================
// Lambertian Reflectance BRDF
// ===============================================================================================
float3 ComputeDiffuseBRDF(float3 diffuseAlbedo, float nDotL)
{
	return (diffuseAlbedo * nDotL) * InvPi;
}

//float3 ComputeSpecularBRDF(float metallic, float roughness, float nDotH, float nDotL, float nDotV)

//------------------------------------------------------------------------------------------------------------------------------//
//  Common file for ALL "LitStandard" shaders so we can avoid duplicate code and have separate effect files for each technique  //
//------------------------------------------------------------------------------------------------------------------------------//

//
// Variables
//

cbuffer PerFrame
{
	float4x4 WVP; //WorldViewProjection
	float4x4 World;
	float4x4 WorldIT; //WorldInverseTranspose
};

cbuffer PerFrame_Instanced
{
  float4x4 ViewProjection;
};

cbuffer PerFrame_Camera
{
  float3 EyePos;
};

//CBuffer for lighting values
cbuffer Lighting 
{
	float3 GlobalAmbient = { 1.0f, 1.0f, 1.0f };
	int NumLights = 0;
	Light Lights[4];
};

//CBuffer for material properties
cbuffer PerMaterial 
{
	float3 MatDiffuse = { 1.0f, 1.0f, 1.0f};
	float3 MatAmbient = { 0.2f, 0.2f, 0.2f};
	float3 MatEmissive = { 0.0f, 0.0f, 0.0f};
	float3 MatSpecular = { 0.0f, 0.0f, 0.0f};
	float MatShininess = 16.0f;
	float Alpha = 1.0f;
};

//
//  **** Uniform color****
//
VSOutputNm VS_PBR_Color(VSInputNm input)
{
	float4 pos = float4(input.Position, 1.0);

	VSOutputNm output;
	output.PositionPS = mul(pos, WVP);
	output.PositionWS = mul(pos, World).xyz;
	output.NormalWS = normalize(mul(float4(input.Normal, 0.0), WorldIT).xyz);
	
	return output;
}

//
//  **** Uniform Color ****
//
float4 PS_PBR_Color(VSOutputNm input) : SV_TARGET
{
	float3 normalWS = normalize(input.NormalWS);

	//Calculate the V vector for specular highlights
	float3 V = normalize(EyePos - input.PositionWS);
  float3 lightContribs = float3(0,0,0);
  float3 ambientTerm = float3(0.0f, 0.0f, 0.0f);
    
  for(int i = 0; i < NumLights; i++)
  {
    LightResult result = ComputeSingleLight(Lights[i], input.PositionWS, normalWS, V, MatShininess);
    
    //Sum ambient results
    ambientTerm += result.Ambient;
    
    //Get the diffuse term for this light (all diffuse constants modulated by the intensity)
    float3 diffuseTerm = result.Diffuse * MatDiffuse;
    
    //Get the specular term for this light (all specular constants modulated by the intensity)
    float3 specularTerm = result.Specular * MatSpecular;
    
    //Sum contributions with the running total and apply attenuation and spot effects to diminish as necessary
    lightContribs += result.Attenuation * (diffuseTerm + specularTerm);
  }
  
  //Final RGB color is the total light ambient modulated by ambient constants, then the summation of the diffuse/specular terms, then a constant emissive term
  float3 finalColor = (ambientTerm * MatAmbient * GlobalAmbient) + lightContribs + MatEmissive;
  
  return float4(finalColor, Alpha);
}

technique11 PBR_Color
{
	pass
	{
		SetVertexShader(CompileShader(vs_5_0, VS_PBR_Color()));
		SetPixelShader(CompileShader(ps_5_0, PS_PBR_Color()));
	}
}