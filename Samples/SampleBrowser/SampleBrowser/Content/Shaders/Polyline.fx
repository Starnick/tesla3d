﻿
cbuffer PerFrame
{
  float4x4 WorldViewMatrix;
  float4x4 ProjMatrix;
  float NearPlane;
  float2 ViewportSize; // Width, Height
  int AASampleCount;
};

cbuffer PerMaterial
{
  float4 Color = { 1.0f, 1.0f, 1.0f, 1.0f };
  float LineWeight = 1;
  float LineCode = 0;
};

struct VSInputPolyline 
{
  float3 Point : POSITION;
  float3 PrevPoint : TEXCOORD0;
  float4 NextPointAndParams : TEXCOORD1;
  float4 Color : COLOR;
};

struct VSOutputPolyline
{
  float4 PositionPS : SV_POSITION;
  float3 PositionVS : TEXCOORD0;
  float4 LineInfo : TEXCOORD1;
  float2 TexCoord : TEXCOORD2;
  float4 Color : COLOR;
  float4 PositionNDC: POSNDC;
  float2 DirectionNDC: DIRNDC;
};

// Const global data

Texture2D LineCodeTexture;

SamplerState MapSampler
{
  Filter = MIN_MAG_MIP_LINEAR;
  AddressU = Wrap;
  AddressV = Wrap;
  AddressW = Wrap;
};

// Helper functions

float4x4 CreateViewportMatrix(float width, float height)
{
  const float nearDepthRange = 0.0;
  const float farDepthRange = 1.0;
  const float x = 0;
  const float y = 0;
  float halfWidth = width * .5;
  float halfHeight = height * .5;
  float halfDepth = (farDepthRange - nearDepthRange) * .5;
  
  float column0Row0 = halfWidth;
  float column1Row1 = halfHeight;
  float column2Row2 = halfDepth;
  float column3Row0 = x + halfWidth;
  float column3Row1 = y + halfHeight;
  float column3Row2 = nearDepthRange + halfDepth;
  float column3Row3 = 1.0;

  return float4x4(column0Row0, 0.0, 0.0, column3Row0,
                  0.0, column1Row1, 0.0, column3Row1,
                  0.0, 0.0, column2Row2, column3Row2,
                  0.0, 0.0, 0.0, column3Row3);
}

struct WindowCoordinateConversionResult
{
  float4 PosWindowCoords;
  float4 PosClippedWVP;
  float3 PosClippedWV;
};

WindowCoordinateConversionResult WorldToWindowCoordinates(float4 pos, float4 nextPos, float nearPlane, float4x4 viewportTransform, float4x4 worldViewMatrix, float4x4 projMatrix)
{
  WindowCoordinateConversionResult result;
  result.PosWindowCoords = float4(0, 0, 1, 0);
  result.PosClippedWVP = float4(0, 0, 1, 0);
  result.PosClippedWV = float3(0, 0, 1);

  // Negative values are in front of the camera so they are visible
  float maxZ = -nearPlane;
  float4 q = mul(pos, worldViewMatrix);
  float4 n = mul(nextPos, worldViewMatrix);

  if (q.z > maxZ)
  {
    if (n.z > maxZ)
    {
      // Entire segment is behind near plane. Default values will ensure it's clipped
      return result;
    }

    float t = (maxZ - q.z) / (n.z - q.z);
    q.x += t * (n.x - q.x);
    q.y += t * (n.y - q.y);
    q.z = maxZ; // q.z + (s_maxZ - q.z) / (n.z - q.z) * (n.z - q.z) = s_maxZ
  }

  result.PosClippedWV = q.xyz;
  q = mul(q, projMatrix);

  result.PosClippedWVP = q;

  // Do perspective divide, transform into window coordinates
  q.xyz /= q.w;
  q.xyz = mul(float4(q.xyz, 1.0), viewportTransform).xyz;
  result.PosWindowCoords = q;
  return result;
}

struct ComputeLineWidthResult
{
  int LineWidth;
  float4 LineInfo;
};

ComputeLineWidthResult ComputeLineWidth(float lineWeight, float2 dir, float2 origin, int numAASamples)
{
  ComputeLineWidthResult result;
  result.LineWidth = lineWeight;
  result.LineInfo = float4(0, 0, 0, 0);

  if (numAASamples > 1)
  {
    if (result.LineWidth < 5.0)
      result.LineWidth += (5.0 - result.LineWidth) * 0.125;

    return result;
  }

  // Calculate slope based width adjustment for non-AA lines, widths 1 to 4
  float2 absDir = abs(dir);
  const float tol = 0.0001;
  if (absDir.y > tol && result.LineWidth < 4.5)
  {
    float len = length(absDir);
    float tan = absDir.x / absDir.y;

    // Width 1
    if (result.LineWidth < 1.5)
    {
      if (tan <= 1.0)
        result.LineWidth = absDir.y;
      else
        result.LineWidth = absDir.x;

      // Width 1 requires additional adjustment plus trimming in pixel shader
      result.LineWidth *= 1.01;
      result.LineInfo.xy = origin;
      result.LineInfo.w = 1.0; // Specifies trimming needs to be done

      // Set slope in the z field
      if (absDir.x - absDir.y > tol)
      {
        result.LineInfo.z = absDir.y / absDir.x;
        result.LineInfo.w += 2.0; // x-major flag
      }
      else
      {
        result.LineInfo.z = absDir.x / absDir.y;
      }
    }
    // Width 2
    else if (result.LineWidth < 2.5) // width 2
    {
      if (tan <= 0.5)
        result.LineWidth = 2.0 * absDir.y;
      else
        result.LineWidth = (absDir.y + 2.0 * absDir.x);
    }
    // Width 3
    else if (result.LineWidth < 3.5)
    {
      if (tan <= 1.0)
        result.LineWidth = (3.0 * absDir.y + absDir.x);
      else
        result.LineWidth = (absDir.y + 3.0 * absDir.x);
    }
    // Width 4+
    else
    {
      if (tan <= 0.5)
        result.LineWidth = (4.0 * absDir.y + absDir.x);
      else if (tan <= 2.0)
        result.LineWidth = (3.0 * absDir.y + 3.0 * absDir.x);
      else
        result.LineWidth = (absDir.y + 4.0 * absDir.x);
    }

    result.LineWidth /= len;
  }

  return result;
}

struct ComputePolylinePointResult
{
  float4 PositionNDC;
  float2 DirectionNDC;
  float4 PositionPS;
  float3 PositionVS;
  float4 LineInfo;
  float MiterAdjustment;
};

ComputePolylinePointResult ComputePolylinePoint(float4 pt, float4 prevPt, float4 nextPt, float lineParam, float lineWeight, float nearPlane, float numAASamples, float2 viewDims, float4x4 worldViewMatrix, float4x4 projMatrix)
{
  const float kNone = 0.0,
              kSquare = 1.0 * 3.0,
              kMiter = 2.0 * 3.0,
              kMiterInsideOnly = 3.0 * 3.0,
              kJointBase = 4.0 * 3.0,
              kNegatePerp = 8.0 * 3.0,
              kNegateAlong = 16.0 * 3.0,
              kNoneAdjWt = 32.0 * 3.0;

  float4x4 vpTransform = CreateViewportMatrix(viewDims.x, viewDims.y);

  ComputePolylinePointResult result;
  result.MiterAdjustment = 0;
  result.LineInfo = float4(0, 0, 0, 0);
  result.DirectionNDC = float2(0, 0);
  WindowCoordinateConversionResult windowPos = WorldToWindowCoordinates(pt, nextPt, nearPlane, vpTransform, worldViewMatrix, projMatrix);
  result.PositionNDC = windowPos.PosWindowCoords;
  if (result.PositionNDC.w == 0.0)
  {
    result.PositionPS = result.PositionNDC;
    result.PositionVS = result.PositionNDC.xyz;
    return result;
  }

  result.PositionVS = windowPos.PosClippedWV;
  result.PositionPS = windowPos.PosClippedWVP;

  float scale = 1.0;
  float directionScale = 1.0;

  if (lineParam >= kNoneAdjWt)
    lineParam -= kNoneAdjWt;

  if (lineParam >= kNegateAlong)
  {
    directionScale = -directionScale;
    lineParam -= kNegateAlong;
  }

  if (lineParam >= kNegatePerp)
  {
    scale = -1.0;
    lineParam -= kNegatePerp;
  }

  WindowCoordinateConversionResult nextWindowPos = WorldToWindowCoordinates(nextPt, pt, nearPlane, vpTransform, worldViewMatrix, projMatrix);
  float2 windowDir = result.DirectionNDC = nextWindowPos.PosWindowCoords.xy - windowPos.PosWindowCoords.xy;

  if (lineParam < kJointBase)
  {
    float2 dir = (directionScale > 0.0) ? windowDir : -windowDir;
    float2 pos = (directionScale > 0.0) ? windowPos.PosWindowCoords.xy : nextWindowPos.PosWindowCoords.xy;
    ComputeLineWidthResult lineWidthResult = ComputeLineWidth(lineWeight, dir, pos, numAASamples);
    result.LineInfo = lineWidthResult.LineInfo;
    lineWeight = lineWidthResult.LineWidth;
  }

  if (lineParam != kNone)
  {
    float2 delta = float2(0, 0);
    WindowCoordinateConversionResult prevWindowPos = WorldToWindowCoordinates(prevPt, pt, nearPlane, vpTransform, worldViewMatrix, projMatrix);
    float2 prevDir = windowPos.PosWindowCoords.xy - prevWindowPos.PosWindowCoords.xy;
    float thisLength = sqrt(windowDir.x * windowDir.x + windowDir.y * windowDir.y);
    const float minNormalizeLength = 1.0E-5; // Avoid normalizing zero length vectors
    float dist = lineWeight / 2.0;

    if (thisLength > minNormalizeLength)
    {
      result.DirectionNDC /= thisLength;

      float prevLength = sqrt(prevDir.x * prevDir.x + prevDir.y * prevDir.y);
      if (prevLength > minNormalizeLength)
      {
        prevDir /= prevLength;
        const float minParallelDot = -.9999;
        const float maxParallelDot = .9999;
        float prevNextDot = dot(prevDir, result.DirectionNDC);

        // No miter if parallel
        if (prevNextDot < minParallelDot || prevNextDot > maxParallelDot)
          lineParam = kSquare;
      }
      else
      {
        lineParam = kSquare;
      }
    }
    else
    {
      result.DirectionNDC = -normalize(prevDir);
      lineParam = kSquare;
    }

    float2 perp = scale * float2(-result.DirectionNDC.y, result.DirectionNDC.x);

    if (lineParam == kSquare)
    {
      delta = perp;
    }
    else
    {
      float2 bisector = normalize(prevDir - result.DirectionNDC);
      float dotP = dot(bisector, perp);

      // Avoid divide by zero...
      if (dotP != 0.0)
      {
        const float maxMiter = 3.0;
        float miterDistance = 1.0 / dotP;

        // Straight miter
        if (lineParam == kMiter)
        {
          delta = (abs(miterDistance) > maxMiter) ? perp : bisector * miterDistance;
        }
        // Miter at inside, square at outside to make room for joint
        else if (lineParam == kMiterInsideOnly)
        {
          delta = (dotP > 0.0 || abs(miterDistance) > maxMiter) ? perp : bisector * miterDistance;
        }
        else
        {
          const float jointTriangleCount = 3.0;
          float ratio = (lineParam - kJointBase) / jointTriangleCount; // 3 triangles per half-joint
          delta = normalize((1.0 - ratio) * bisector + (dotP < 0.0 ? -ratio : ratio) * perp); // Miter/straight combo
        }
      }

    }

    result.MiterAdjustment = dot(result.DirectionNDC, delta) * dist;
    result.PositionPS.x += dist * delta.x * 2.0 * result.PositionPS.w / viewDims.x;
    result.PositionPS.y += dist * delta.y * 2.0 * result.PositionPS.w / viewDims.y;
  }

  return result;
}

float2 ComputeLineCodeTextureCoords(float lineCode, float2 windowDir, float4 projPos, float miterAdjust)
{
  if (lineCode == 0.0)
    return float2(-1, -1); // Solid line, no lookup necessary

  const float imagesPerPixel = 1.0 / 32.0;
  const float textureCoordinateBase = 8192.0;

  float2 texCoords;
  if (abs(windowDir.x) > abs(windowDir.y))
    texCoords.x = textureCoordinateBase + imagesPerPixel * (projPos.x + miterAdjust * windowDir.x);
  else
    texCoords.x = textureCoordinateBase + imagesPerPixel * (projPos.y + miterAdjust * windowDir.y);

  const float numLineCodes = 16.0;
  const float rowsPerCode = 1.0;
  const float numRows = numLineCodes * rowsPerCode;
  const float centerY = 0.5 / numRows;
  const float stepY = rowsPerCode / numRows;
  texCoords.y = stepY * lineCode + centerY;

  return texCoords;

}

VSOutputPolyline VS_Polyline(VSInputPolyline input)
{
  ComputePolylinePointResult computePosResult = ComputePolylinePoint(float4(input.Point, 1.0), float4(input.PrevPoint, 1.0), float4(input.NextPointAndParams.xyz, 1.0), input.NextPointAndParams.w, LineWeight, NearPlane, AASampleCount, ViewportSize, WorldViewMatrix, ProjMatrix);
  VSOutputPolyline output;
  output.PositionPS = computePosResult.PositionPS;
  output.PositionVS = computePosResult.PositionVS;
  output.LineInfo = computePosResult.LineInfo;
  output.TexCoord = ComputeLineCodeTextureCoords(LineCode, computePosResult.DirectionNDC, computePosResult.PositionNDC, computePosResult.MiterAdjustment);
  output.PositionNDC = computePosResult.PositionNDC;
  output.DirectionNDC = computePosResult.DirectionNDC;
  output.Color = input.Color * Color;

  return output;
}

float4 PS_Polyline(VSOutputPolyline input) : SV_TARGET
{
  // If negative, then it's a solid line otherwise check discard for line pattern
  if (input.TexCoord.x >= 0.0)
  {
    float4 texColor = LineCodeTexture.Sample(MapSampler, input.TexCoord);
    if (texColor.r == 0.0)
      discard;
  }

  // Line needs pixel trimming
  if (input.LineInfo.w > 0.5)
  {
    // Calculate pixel distance from pixel center to expected line center, opposite dir from major
    float2 dxy = input.PositionPS.xy - input.LineInfo.xy;
    if (input.LineInfo.w < 1.5) // Not x-major
      dxy = dxy.yx;

    float dist = input.LineInfo.z * dxy.x - dxy.y;
    float distA = abs(dist);
    if (distA > 0.5 || (distA == 0.5 && dist < 0.0))
      discard;
  }

  return input.Color;
}

technique11 Polyline
{
  pass
  {
    SetVertexShader(CompileShader(vs_5_0, VS_Polyline()));
    SetPixelShader(CompileShader(ps_5_0, PS_Polyline()));
  }
}