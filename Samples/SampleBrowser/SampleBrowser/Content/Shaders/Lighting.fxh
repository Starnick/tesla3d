/*
* The shader code below is licensed under the Creative Commons Attribution-Share Alike 3.0 
* license. Copyright (c) 2010-2015 Tesla 3D Engine - Nicholas Woodfield
*
* You are free:
*	To Share: to copy, distribute, display, and perform the work
*	To Remix: to make derivative works 
*
*	Under the following conditions:
*		* Attribution - You must attribute the work in the manner specified by the author or
*			licensor (but not in any way that suggests that they endorse you or your use of the work).
*		* Share Alike - If you alter, transform, or build upon this work, you may distribute
*			the resulting work only under the same, similar or a compatible license.
*
*		For any reuse or distribution, you must make clear to others the license terms of this work.
*		The best way to do this is with a link to the above web page (http;//www.tesla-engine.net)
*
*		Any of the above conditions can be waived if you get permission from the copyright holder.
*
*		Apart from the remix rights granted under this license, nothing in this license impairs or
*		restricts the author's moral rights.
*/

#define POINT_LIGHT 0
#define SPOT_LIGHT 1
#define DIRECTIONAL_LIGHT 2

//Represents either a Point, Spot, or Directional light.
struct Light 
{
  // Layout was chosen to be as tightly packed as possible
  
  //Color property
	float3 Ambient;
	
	//If light attenuates
	bool Attenuate;
	
	//Color property
	float3 Diffuse;
	
	//Attenuation property based on light range
	float InverseRangeSquared;
	
	//Color property
	float3 Specular;
	
	//If spot light, this is Cosine(InnerAngle / 2.0f). Inside the inner angle is the brightest part of the cone.
	float CosInnerAngle;
	
	//Position is in world space, Note: w = 1 means direction is used
	float3 Position;
	
	//Type of light. 0 = Point [Position only], 1 = Spot [Position + Direction + Angles], 2 = Directional [Direction only]
	uint LightType;
	
	//Direction should be normalized
	float3 Direction;
	
	//If spot light, this is Cosine(OuterAngle / 2.0f). Between the inner and outer angle the light falls off and fades to nothing.
	float CosOuterAngle;
	
	//Factor that is used to increase/decrease energy of the light (default = 1.0)
	float Intensity;
};

//The resulting light color contribution from a single light,
//these are generally modulated by other material colors.
struct LightResult 
{
	float3 Ambient;
	float3 Diffuse;
	float3 Specular;
	float Attenuation;
};

struct Material 
{
	float3 Ambient;
	float3 Diffuse;
	float3 Specular;
	float Shininess;
};

//Compute the attenuation factor of a light based off of a world space
//position, if the light is set to attenuate or not.
float ComputeAttenuation(float3 position, Light light)
{
  float attenuationFactor = 1.0;
  
  if(light.Attenuate)
  {
    float3 l = light.Position.xyz - position;
    float sqrDist = dot(l, l);
    float atten = saturate(1.0 - sqrDist * light.InverseRangeSquared);
    
    attenuationFactor = (atten * atten);
  }
  
  return attenuationFactor;
}

//Compute the spot effect used by spot lights
float SpotEffect(float3 position, Light light)
{
  float spotFactor = 1.0;
  
  if(light.LightType == SPOT_LIGHT)
  {
    float3 V = normalize(position - light.Position.xyz);
    spotFactor = smoothstep(light.CosOuterAngle, light.CosInnerAngle, dot(V, light.Direction));
  }
  
  return spotFactor;
}

//Computes both attenuation and spot effects together based on the direction from the geometry point to light source
float ComputeAttenuationAndSpotEffect(float3 dirToLightSrc, Light light)
{
  float atten = 1.0;
  
  if(light.Attenuate)
  {
    float sqrDist = dot(dirToLightSrc, dirToLightSrc);
    atten = saturate(1.0 - sqrDist * light.InverseRangeSquared);
    atten *= atten;
  }
  
  float spot = 1.0;
  
  if(light.LightType == SPOT_LIGHT)
  {
    float3 V = normalize(-dirToLightSrc);
    spot = smoothstep(light.CosOuterAngle, light.CosInnerAngle, dot(V, light.Direction));
  }
  
  return atten * spot;
}

//Compute a single light, the result are just the RGB intensities of ambient, diffuse, and specular of the light.
//Requires the vertex position in world space, the normal in world space, the
//view vector V in world space, and the shininess of the material.
LightResult ComputeSingleLight(Light light, float3 position, float3 normal, float3 V, float shininess)
{
	//Direction to the light source
	float3 L;
	float attenuation;
	if(light.LightType == DIRECTIONAL_LIGHT)
	{
		L = -light.Direction; //Should already by normalized by the CPU
		attenuation = 1.0; //Directional lights never attenuate
	} 
	else 
	{
		L = light.Position.xyz - position;
		
		//Compute attenuation of spot and point lights
		attenuation = ComputeAttenuationAndSpotEffect(L, light);
		
		L = normalize(L);
	}
	
	//Saturating to keep dots within 0-1 range, because if negative then we don't want to be shading triangles facing away from the light source
	
  //Calculate diffuse contribution	
	float NDotL = saturate(dot(normal,L));
	float3 diffuse = NDotL  * light.Diffuse * light.Intensity;
	
	//Calculate specular contribution using the halfway vector between L and V
	float3 H = normalize(L + V);
	
	float NDotH = pow(saturate(dot(normal, H)), shininess);
	float3 specular = NDotH * light.Specular * light.Intensity;
	
	//Return result
	LightResult result;
	result.Ambient = light.Ambient;
	result.Diffuse = diffuse;
	result.Specular = specular;
	result.Attenuation = attenuation;
	
	return result;
}