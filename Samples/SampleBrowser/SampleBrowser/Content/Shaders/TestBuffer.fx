﻿Buffer<float2> points;

struct Light
{
  float3 Point;
  float radius;
  int Type;
  float Misc;
};

StructuredBuffer<Light> lights;
ByteAddressBuffer bytes;
StructuredBuffer<float4> notLights;
Texture1D<float4> tex;

float4 VS_Test(uint index: SV_VertexID) : SV_Position
{
  float4 pt = float4(points[index], 0, 0);
  Light l = lights[index];
  float b = asfloat(bytes.Load(index));
  float4 ll = notLights[index];
  
  return pt + float4(l.Point, b) + ll;
}

float4 PS_Test(float4 pt: SV_Position) : SV_Target
{
  return pt + tex.Load(pt.xy);
}

technique11 BufferTest
{
  pass
  {
    SetVertexShader(CompileShader(vs_5_0, VS_Test()));
    SetPixelShader(CompileShader(ps_5_0, PS_Test()));
  }
}