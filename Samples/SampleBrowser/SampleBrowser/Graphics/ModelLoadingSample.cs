﻿/*
* Copyright (c) 2010-2017 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.IO;
using Tesla;
using Tesla.Content;
using Tesla.Graphics;
using Tesla.Scene;
using System.Drawing;

namespace SampleBrowser.Graphics
{
    [AppDescription("Model Loading Sample", RenderSystemPlatform.Any, "ModelLoading_Thumb", "ModelLoading_Descr", "Graphics/ModelLoadingSample.cs")]
    public class ModelLoadingSample : ForwardRendererSceneGraphApp
    {
        public static void RunApp(PresentationParameters pp, IPlatformInitializer init)
        {
            new ModelLoadingSample(pp, init).Run();
        }

        public ModelLoadingSample(PresentationParameters presentParams, IPlatformInitializer platformInitializer)
            : base(presentParams, platformInitializer)
        {
        }

        protected override void LoadContent(ContentManager content)
        {
            ClearColor = Tesla.Color.Black;

            //Set the camera to be looking at the center of the terrain and up high a bit
            Camera.Position = new Vector3(10, 40, 70);
            Camera.LookAt(Vector3.Zero, Vector3.Up);
            CameraController.UpdateFromCamera();

            //Place a light directly above the center
            PointLight pl = new PointLight();
            pl.Position = new Vector3(0, 1000, 0);
            pl.Range = 1000;
            pl.Attenuate = false;

            SceneRoot.Lights.Add(pl);

            LoadModel(content, "Models/Lighthouse_Scene/Lighthouse.obj");
            LoadModel(content, "Models/Lighthouse_Scene/OutBuilding.obj");
            LoadModel(content, "Models/Lighthouse_Scene/Terrain.obj");
            LoadModel(content, "Models/Lighthouse_Scene/Trees.obj");
        }

        private void LoadModel(ContentManager content, String path)
        {
            ModelImporterParameters parameters = new ModelImporterParameters();

            //Enable this to override the normals in the model format
            //parameters.NormalGenerationOptions = NormalGeneration.Face;

            Spatial s = content.Load<Spatial>(path, parameters);
            if (s != null)
                SceneRoot.Children.Add(s);
        }
    }
}
