﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesla;
using Tesla.Graphics;
using Tesla.Application;
using Tesla.Content;
using Tesla.Input;
using Tesla.Scene;

namespace SampleBrowser.Graphics
{
    [AppDescription("Instancing Sample", RenderSystemPlatform.Direct3D11, "InstancingImage_Thumb", "InstancingImage_Descr", "Graphics/InstancingSample.cs")]
    public class InstancingSample : ForwardRendererSceneGraphApp
    {
        public static void RunApp(PresentationParameters pp, IPlatformInitializer init)
        {
            new InstancingSample(pp, init).Run();
        }

        public InstancingSample(PresentationParameters presentParams, IPlatformInitializer platformInitializer) 
            : base(presentParams, platformInitializer)
        {
        }

        protected override void LoadContent(ContentManager content)
        {
            base.LoadContent(content);

            BoxGenerator gen = new BoxGenerator();

            Mesh msh = new Mesh("Box");
            gen.BuildMeshData(msh.MeshData, GenerateOptions.All);
            msh.MeshData.Compile();
            MaterialDefinition matDef = new MaterialDefinition();
            matDef.Add(RenderBucketID.Opaque, StandardMaterialScriptLibrary.CreateStandardMaterial("LitStandard_Color", Content));
            msh.MaterialDefinition = matDef;

            SceneRoot.Children.Add(msh);

            PointLight pl = new PointLight();
            pl.Position = new Vector3(10, 0, 10);
            pl.Range = 100;
            pl.Diffuse = Color.CornflowerBlue;

            SceneRoot.Lights.Add(pl);

            Camera.Position = new Vector3(0, 0, 10);
            Camera.LookAt(Vector3.Zero, Vector3.Up);
            Camera.Update();
        }
    }
}
