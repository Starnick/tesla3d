﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesla;
using Tesla.Graphics;
using Tesla.Application;
using Tesla.Content;
using Tesla.Input;
using Tesla.Scene;

namespace SampleBrowser.Graphics
{
    [AppDescription("PBR Sample", RenderSystemPlatform.Direct3D11, "PBRImage_Thumb", "PBRImage_Descr", "Graphics/PBRSample.cs")]
    public class PBRSample : ForwardRendererSceneGraphApp
    {
        public static void RunApp(PresentationParameters pp, IPlatformInitializer init)
        {
            new PBRSample(pp, init).Run();
        }

        public PBRSample(PresentationParameters presentParams, IPlatformInitializer platformInitializer)
            : base(presentParams, platformInitializer)
        {
        }

        protected override void LoadContent(ContentManager content)
        {
            base.LoadContent(content);

            ClearColor = Color.CornflowerBlue;

            CreateSphereGrid(10, 10, .25f);

            ModelImporterParameters importParams = new ModelImporterParameters();
            importParams.SwapWindingOrder = true;
            importParams.MaterialParameters.TextureParameters.GenerateMipMaps = true;

            Spatial s = Content.Load<Spatial>("Models/domografica/model.dae", importParams);
            s.Translation = new Vector3(0, -5, 0);
            SceneRoot.Children.Add(s);

            PointLight pl = new PointLight();
            pl.Position = new Vector3(10, 25, 10);
            pl.Range = 1000;
            pl.Diffuse = Color.White;
            //pl.Ambient = Color.White;
            //pl.Specular = Color.Green;

            SceneRoot.Lights.Add(pl);

            PointLight ambientLight = new PointLight();
            ambientLight.Attenuate = false;
            ambientLight.Diffuse = Color.Black;
            ambientLight.Specular = Color.Black;
            ambientLight.Ambient = Color.White;

            //SceneRoot.Lights.Add(ambientLight);

            Camera.Position = new Vector3(0, 5, 10);
            Camera.LookAt(Vector3.Zero, Vector3.Up);
            Camera.Update();

            CameraController.UpdateFromCamera();
        }

        private void CreateSphereGrid(int numX, int numY, float radius)
        {
            MeshData sphereMesh = CreateSphere(radius);
            sphereMesh.Compile();

            float spaceBetweenSpheres = (radius * 2.0f) + (radius * .75f);
            float xOffset = ((numX - 1) * spaceBetweenSpheres) * 0.5f;
            float yOffset = ((numY - 1) * spaceBetweenSpheres) * 0.5f;

            BoundingBox box = new BoundingBox();
            box.ComputeFromIndexedPoints(sphereMesh.Positions, sphereMesh.Indices.Value);

            Material pbrMat = Content.Load<Material>("Shaders/PBR_Color.tem");

            for(int x = 0; x < numX; x++)
            {
                for(int y = 0; y < numY; y++)
                {
                    Vector3 pos = new Vector3((float)x * spaceBetweenSpheres - xOffset, 0, (float)y * spaceBetweenSpheres - yOffset);

                    Mesh msh = new Mesh(String.Format("Sphere {0}, {1}", x, y), sphereMesh);
                    msh.Translation = pos;
                    msh.SetModelBounding(box.Clone(), false);
                    msh.MaterialDefinition = new MaterialDefinition();
                    msh.MaterialDefinition.Add(RenderBucketID.Opaque, pbrMat.Clone());
                    msh.MaterialDefinition.GetOpaqueOrFirstMaterial().SetParameterValue<Vector3>("MatSpecular", new Vector3(1, 1, 1));
                    SceneRoot.Children.Add(msh);
                }
            }
        }

        private MeshData CreateSphere(float radius)
        {
            SphereGenerator gen = new SphereGenerator();
            gen.Radius = radius;

            MeshData md = new MeshData();
            gen.BuildMeshData(md, GenerateOptions.Normals | GenerateOptions.Positions);

            return md;
        }
    }
}
