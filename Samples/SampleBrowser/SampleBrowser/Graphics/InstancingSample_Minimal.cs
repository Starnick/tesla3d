﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesla;
using Tesla.Graphics;
using Tesla.Application;
using Tesla.Content;
using Tesla.Input;

namespace SampleBrowser.Graphics
{
    [AppDescription("Minimal Instancing Sample", RenderSystemPlatform.Direct3D11, "InstancingImage_Minimal_Thumb", "InstancingImage_Minimal_Descr", "Graphics/InstancingSample_Minimal.cs")]
    public class InstancingSample_Minimal : GameApplication
        {
            public static void RunApp(PresentationParameters pp, IPlatformInitializer init)
            {
                new InstancingSample_Minimal(pp, init).Run();
            }
            
            public InstancingSample_Minimal(PresentationParameters presentParams, IPlatformInitializer platformInitializer)
                : base(presentParams, platformInitializer)
            {     
            }
        
            protected override void Render(IRenderContext context, IGameTime time)
            {
            }

            protected override void Update(IGameTime time)
            {
                if(Keyboard.GetKeyboardState().IsKeyDown(Keys.Escape))
                    Exit();
            }
    }
}
