﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesla;
using Tesla.Graphics;
using Tesla.Application;
using Tesla.Content;
using Tesla.Input;
using Tesla.DesignFramework;
using Tesla.Framework;

namespace SampleBrowser.Graphics
{
    public struct LaserBoltData
    {
        public Segment Line;
        public float Width;
        public Color Color;

        public LaserBoltData(Segment line, float width, Color color)
        {
            Line = line;
            Width = width;
            Color = color;
        }
    }

    [AppDescription("Laser Bolt Sample", RenderSystemPlatform.Direct3D11, "InstancingImage_Minimal_Thumb", "InstancingImage_Minimal_Descr", "Graphics/InstancingSample_Minimal.cs")]
    public class LaserBoltSample : BasicCommonApp
    {
        private PrimitiveBatch<VertexPositionColorTexture> m_batcher;
        private LineBatch m_lineBatcher;
        private List<LaserBoltData> m_bolts;
        private DesignGrid m_grid;

        private Camera m_orthoCam;

        public static void RunApp(PresentationParameters pp, IPlatformInitializer init)
        {
            new LaserBoltSample(pp, init).Run();
        }

        public LaserBoltSample(PresentationParameters presentParams, IPlatformInitializer platformInitializer)
            : base(presentParams, platformInitializer)
        {
        }

        protected override void LoadContent(ContentManager content)
        {
            m_batcher = new PrimitiveBatch<VertexPositionColorTexture>();
            m_lineBatcher = new LineBatch();
            m_bolts = new List<LaserBoltData>();
            m_grid = new DesignGrid();
            m_grid.DrawMainAxes = true;
            m_grid.MajorTileSize = 10;
            m_grid.ExtentMajorTileCount = 10;
            m_grid.MajorGridLineThickness = new GridThickness(.1f, .01f, true);

            ClearColor = Color.Black;

            m_orthoCam = new Camera();
            m_orthoCam.Viewport = new Viewport(0, 0, GameWindow.ClientBounds.Width, GameWindow.ClientBounds.Height);

            Camera cam = RenderSystem.ImmediateContext.Camera;
            cam.Viewport = new Viewport(0, 0, GameWindow.ClientBounds.Width, GameWindow.ClientBounds.Height);
            cam.SetProjection(Angle.FromDegrees(65.0f), .1f, 1000f);
            cam.Position = new Vector3(-10, 60, 40);
            cam.LookAt(Vector3.Zero, Vector3.Up);

            CameraController.UpdateFromCamera();
           // (CameraController as OrbitCameraController).TargetLookAtPoint = Vector3.Zero;
        }

        protected override void UpdateSample(IGameTime time)
        {
        }

        protected override void RenderSample(IRenderer renderer, IGameTime time)
        {
            m_lineBatcher.Begin(renderer.RenderContext);
            m_grid.Draw(m_lineBatcher);
            m_lineBatcher.End();
        }

        protected override IRenderer SetupRenderer()
        {
            return new ForwardRenderer(RenderSystem.ImmediateContext);
        }
    }
}
