﻿/*
* Copyright (c) 2010-2016 Tesla 3D Engine - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;
using System.IO;
using Tesla;
using Tesla.Content;
using Tesla.Graphics;
using Tesla.Scene;
using System.Drawing;

namespace SampleBrowser.Graphics
{
    [AppDescription("Simple Terrain Sample", RenderSystemPlatform.Any, "SimpleTerrain_Thumb", "SimpleTerrain_Descr", "Graphics/SimpleTerrainSample.cs")]
    public class SimpleTerrainSample : ForwardRendererSceneGraphApp
    {
        public static void RunApp(PresentationParameters pp, IPlatformInitializer init)
        {
            new SimpleTerrainSample(pp, init).Run();
        }

        public SimpleTerrainSample(PresentationParameters presentParams, IPlatformInitializer platformInitializer) 
            : base(presentParams, platformInitializer)
        {
        }

        protected override void LoadContent(ContentManager content)
        {
            ClearColor = Tesla.Color.Black;

            //Set the camera to be looking at the center of the terrain and up high a bit
            Camera.Position = new Vector3(1100, 1500, 1100);
            Camera.LookAt(Vector3.Zero, Vector3.Up);
            CameraController.UpdateFromCamera();

            //Create a largeish terrain tile that is slightly exaggerated
            Mesh terrain = CreateHeightmap("Textures/Terrain.bmp", 2000, 2000, 500);

            //Place a light directly above the center
            PointLight pl = new PointLight();
            pl.Position = new Vector3(0, 1000, 0);
            pl.Range = 1000;

            SceneRoot.Lights.Add(pl);
            SceneRoot.Children.Add(terrain);
        }

        private Mesh CreateHeightmap(String heightmapFile, float width, float height, float scaleFactor)
        {
            Mesh terrain = new Mesh("Terrain");

            Bitmap heightmap;
            using(Stream str = Content.OpenStream(heightmapFile))
            {
                heightmap = Image.FromStream(str) as Bitmap;
            }

            //Create a grid that represents the terrain tile -- we can use the plane generator which allows us to create a sheet that has  tessellation
            PlaneGenerator gen = new PlaneGenerator();
            gen.Width = width;
            gen.Height = height;
            gen.Tessellation = 100;

            //Build the mesh data -- we'll use the UV coordinates to sample the heightmap
            gen.BuildMeshData(terrain.MeshData, GenerateOptions.Positions | GenerateOptions.TextureCoordinates);

            DataBuffer<Vector3> positions = terrain.MeshData.Positions;
            DataBuffer<Vector2> texCoords = terrain.MeshData.TextureCoordinates;

            //Add some vertex coloring to give the terrain a bit dimension -- just use the heightmap texture color
            //We don't generate normals for this sample -- By default the plane generator will produce normals all going in the same
            //direction, so we'd need to adjust them if we light the terrain.
            DataBuffer<Tesla.Color> vertexColors = new DataBuffer<Tesla.Color>(positions.Length);
            terrain.MeshData.Colors = vertexColors;

            for(int i = 0; i < positions.Length; i++)
            {
                Vector3 pos = positions[i];
                Vector2 uv = texCoords[i];

                Int2 texels = new Int2((int) (heightmap.Width * uv.X), (int) (heightmap.Height * uv.Y));
                texels = Int2.Clamp(texels, Int2.Zero, new Int2(heightmap.Width - 1, heightmap.Height - 1));

                System.Drawing.Color c = heightmap.GetPixel(texels.X, texels.Y);

                pos.Y = (c.R / 255f) * scaleFactor;

                positions[i] = pos;
                vertexColors[i] = new Tesla.Color(c.R, c.G, c.B);
            }

            //Don't forget to create the GPU resources
            terrain.MeshData.Compile();
            terrain.SetModelBounding(new BoundingBox());

            //Setup a basic material - nothing fancy
            terrain.MaterialDefinition = new MaterialDefinition();

            Material mat = StandardMaterialScriptLibrary.CreateStandardMaterial("Standard_Color_Vc", Content);
            terrain.MaterialDefinition.Add(RenderBucketID.Opaque, mat);

            return terrain;
        }
    }
}
